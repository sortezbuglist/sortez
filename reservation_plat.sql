-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  sam. 12 jan. 2019 à 10:17
-- Version du serveur :  5.7.21
-- Version de PHP :  7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sortez`
--

-- --------------------------------------------------------

--
-- Structure de la table `reservation_plat`
--

DROP TABLE IF EXISTS `reservation_plat`;
CREATE TABLE IF NOT EXISTS `reservation_plat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdPlat` int(10) DEFAULT NULL,
  `IdCommercant` varchar(10) DEFAULT NULL,
  `heure_reservation` varchar(10) DEFAULT NULL,
  `nbre_pers_reserved` int(2) DEFAULT NULL,
  `nbre_platDuJour` int(2) DEFAULT NULL,
  `num_carte` int(20) DEFAULT NULL,
  `date_reservation` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `reservation_plat`
--

INSERT INTO `reservation_plat` (`id`, `IdPlat`, `IdCommercant`, `heure_reservation`, `nbre_pers_reserved`, `nbre_platDuJour`, `num_carte`, `date_reservation`) VALUES
(3, 3, '301299', '11:00', 1, 0, 486961, '2019-01-12');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
