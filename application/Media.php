<?php


/**
 * Created by PhpStorm.
 * User: rand
 * Date: 25/10/2016
 * Time: 07:00
 */

class media extends CI_Controller
{

    function __construct()
    {

        parent::__construct();

        $this->load->model("mdlarticle");

        $this->load->model("mdlannonce");

        $this->load->model("mdlcommercant");

        $this->load->model("mdlbonplan");

        $this->load->model("mdlcategorie");

        $this->load->model("mdlville");

        $this->load->model("mdldepartement");

        $this->load->model("mdlcommercantpagination");

        $this->load->model("sousRubrique");

        $this->load->model("AssCommercantAbonnement");

        $this->load->model("AssCommercantSousRubrique");

        $this->load->model("Commercant");

        $this->load->model("user");

        $this->load->model("mdl_agenda");

        $this->load->model("mdl_categories_agenda");

        $this->load->model("mdl_categories_article");

        $this->load->model("mdlimagespub");

        $this->load->model("Abonnement");

        $this->load->Model("mdlagenda_perso");


        $this->load->library('user_agent');

        $this->load->library("pagination");

        $this->load->library('image_moo');

        $this->load->library('session');


        $this->load->library('ion_auth');

        $this->load->model("ion_auth_used_by_club");


        if (!$this->ion_auth->logged_in()) redirect("front/utilisateur/no_permission");


        check_vivresaville_id_ville();


    }


    function index($argument=0)
    {
$all=explode("-",$argument);

        $user_ion_auth = $this->ion_auth->user()->row();

        $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

        if ($iduser == null || $iduser == 0 || $iduser == "") {

            $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

        }

        $data['IdUser'] = $iduser;

        $data['user_ionauth_id'] = $user_ion_auth->id;

        $data['cat'] =$all[0];

        $data['icat'] =$all[1];

        $data['img'] = $all[2];

        /*$data['cat'] = $this->input->get('cat');

        $data['icat'] = $this->input->get('icat');

        $data['img'] = $this->input->get('img');*/

        $this->load->view('media/media_form', $data);

    }


    function save_temp_photo()
    {


        //print_r($_FILES);

        $fileName = $_FILES['ArticlePhoto1']['name'];

        $fileType = $_FILES['ArticlePhoto1']['type'];

        $fileError = $_FILES['ArticlePhoto1']['error'];

        $fileContent = file_get_contents($_FILES['ArticlePhoto1']['tmp_name']);

        $user_ionauth_id = $this->input->post("user_ionauth_id");

        $cat = $this->input->post("cat");


        if ($fileError == UPLOAD_ERR_OK) {

            //Processes your file here

            if (isset($_FILES["ArticlePhoto1"]["name"])) $photo1 = $_FILES["ArticlePhoto1"]["name"];

            if (isset($photo1) && $photo1 != "") {

                $photo1Associe = $photo1;

                if (isset($cat) && $cat != null && isset($user_ionauth_id) && $user_ionauth_id != null && ($cat == "bg" || $cat == "bn")) {

                    $user_dir_root = "application/resources/front/photoCommercant/imagesbank/$user_ionauth_id/";

                    if (!file_exists($user_dir_root)) {

                        mkdir($user_dir_root, 0777);

                    }

                    $user_dir = "application/resources/front/photoCommercant/imagesbank/$user_ionauth_id/$cat/";

                    if (!file_exists($user_dir)) {

                        mkdir($user_dir, 0777);

                    }

                    $ArticlePhoto1 = doUploadResize("ArticlePhoto1", $user_dir, $photo1Associe);

                } else {

                    $ArticlePhoto1 = doUploadResize("ArticlePhoto1", "application/resources/front/photoCommercant/imagesbank/tmp/", $photo1Associe);

                }

            } else $ArticlePhoto1 = "Erreur";

            echo base_url() . "application/resources/front/photoCommercant/imagesbank/tmp/" . $ArticlePhoto1;

        } else {

            switch ($fileError) {

                case UPLOAD_ERR_INI_SIZE:

                    $message = 'Erreur lorsque vous essayez de t�l�charger un fichier qui d�passe la taille autoris�e.';

                    break;

                case UPLOAD_ERR_FORM_SIZE:

                    $message = 'Erreur lorsque vous essayez de t�l�charger un fichier qui d�passe la taille autoris�e.';

                    break;

                case UPLOAD_ERR_PARTIAL:

                    $message = 'Erreur: aucune action termin�e t�l�charger le fichier.';

                    break;

                case UPLOAD_ERR_NO_FILE:

                    $message = 'Erreur: Aucun fichier a �t� t�l�charg�.';

                    break;

                case UPLOAD_ERR_NO_TMP_DIR:

                    $message = 'Erreur: serveur non configur� pour le t�l�chargement de fichiers.';

                    break;

                case UPLOAD_ERR_CANT_WRITE:

                    $message = 'Erreur: �chec possible d\'enregistrer le fichier.';

                    break;

                case  UPLOAD_ERR_EXTENSION:

                    $message = 'Erreur: fichier non termin� le chargement.';

                    break;

                default:
                    $message = 'Erreur: fichier non termin� le chargement.';

                    break;

            }

            echo json_encode(array(

                'error' => true,

                'message' => $message

            ));

        }

    }


    function save_definite_photo()
    {

        $newURL = $this->input->post("newURL");

        $localURL = $this->input->post("localURL");

        $user_ionauth_id = $this->input->post("user_ionauth_id");

        $cat = $this->input->post("cat");

        $img = $this->input->post("img");


        $main_path = 'application/resources/front/photoCommercant/imagesbank/';

        if (isset($cat) && $cat == "bg")

            $user_dir = $main_path . $user_ionauth_id . "/bg/";

        else if (isset($cat) && $cat == "bn")

            $user_dir = $main_path . $user_ionauth_id . "/bn/";

        else if (isset($cat) && $cat == "lg")

            $user_dir = $main_path . $user_ionauth_id . "/lg/";

        else

            $user_dir = $main_path . $user_ionauth_id . "/";


        if (isset($user_ionauth_id) && $user_ionauth_id != "" && $user_ionauth_id != null) {

            if (!file_exists($user_dir)) {

                mkdir($user_dir, 0777);

            }

        } else {

            $user_dir = $main_path . "files/";

        }


        copy($newURL, $user_dir . $localURL);

        echo 'OK';

    }


    function delete_definite_photo()
    {

        $img_id = $this->input->post("img_id");

        $img_file = $this->input->post("img_file");

        $user_ion_auth = $this->ion_auth->user()->row();

        $user_ion_auth_id = $user_ion_auth->id;


        $main_path = 'application/resources/front/photoCommercant/imagesbank/';

        $user_file_delete = $main_path . $user_ion_auth_id . "/" . $img_file;


        unlink($user_file_delete);

        echo 'OK';

    }


    function add_definite_photo()
    {

        $cat = $this->input->post("cat");

        $icat = $this->input->post("icat");

        $img = $this->input->post("img");

        $img_file = $this->input->post("img_file");


        $user_ion_auth = $this->ion_auth->user()->row();

        $user_ion_auth_id = $user_ion_auth->id;

        $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);


        if (isset($icat) && $icat != "") {

            switch ($cat) {

                case 'ar':

                    $this->load->model("mdlarticle");

                    $this->mdlarticle->file_manager_update($icat, $user_ion_auth_id, $img, $img_file);

                    echo 'ok';

                    break;

                case 'ag':

                    $this->load->model("mdl_agenda");

                    $this->mdl_agenda->file_manager_update($icat, $user_ion_auth_id, $img, $img_file);

                    echo 'ok';

                    break;

                case 'bp':

                    $this->load->model("mdlbonplan");

                    $this->mdlbonplan->file_manager_update($icat, $commercant_UserId, $img, $img_file);

                    echo 'ok';

                    break;

                case 'an':

                    $this->load->model("mdlannonce");

                    $this->mdlannonce->file_manager_update($icat, $commercant_UserId, $img, $img_file);

                    echo 'ok';

                    break;

                case 'fd':

                    echo 'ok';

                    break;

                case 'co':

                case 'bg':

                case 'bg_sample':

                case 'bn':

                case 'bn_sample':

                case 'lg':

                    $this->load->model("Commercant");

                    $this->Commercant->file_manager_update($icat, $user_ion_auth_id, $img, $img_file);

                    echo 'ok';

                    break;

            }

        } else {

            echo "error";

        }


    }


    function load_all_photo($repository = '0', $month = '', $day = '')
    {
        if ($month=='') $month = date("m");
        if ($day=='') $day = date("d");

        $user_ion_auth = $this->ion_auth->user()->row();
        $user_ionauth_id = $user_ion_auth->id;
        $main_path = 'application/resources/front/photoCommercant/imagesbank/';
        $folder = $main_path . $user_ionauth_id . "/";
        //var_dump($folder); die();
        if ($repository != '0' && $repository == 'bg_sample') {
            $folder = $main_path . $repository . "/";
        } else if ($repository != '0' && $repository == 'bn_sample') {
            $folder = $main_path . $repository . "/";
        } else if ($repository != '0' && $repository == 'lg') {
            $folder = $main_path . $user_ionauth_id . "/" . $repository . "/";
        } else if ($repository != '0') {
            $folder = $main_path . $user_ionauth_id . "/" . $repository . "/";
        }

        $filenameArray = array();

        $handle = opendir($folder);

        if (isset($month) && $month != '' && isset($day) && $day != '') {
            while ($file = readdir($handle)) {
                if ($file !== '.' && $file !== '..' && date("Y-m-d", filemtime($folder.$file)) == $month."-".$day) {
                    array_push($filenameArray, $folder.$file);
                }
            }
        } else if (isset($month) && $month != '' && $day == '') {
            while ($file = readdir($handle)) {
                if ($file !== '.' && $file !== '..' && date("Y-m", filemtime($folder.$file)) == $month) {
                    array_push($filenameArray, $folder.$file);
                }
            }
        } else if ($month == '' && isset($day) && $day != '') {
            while ($file = readdir($handle)) {
                if ($file !== '.' && $file !== '..' && date("d", filemtime($folder.$file)) == $day) {
                    array_push($filenameArray, $folder.$file);
                }
            }
        } else {
            while ($file = readdir($handle)) {
                if ($file !== '.' && $file !== '..') {
                    array_push($filenameArray, $folder.$file);
                }
            }
        }

        echo json_encode($filenameArray);
    }

}