<?php

class bonplan extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->output->set_header('Access-Control-Allow-Origin: null');  header('Access-Control-Allow-Credentials: omit');
        header('Access-Control-Allow-Method: PUT, GET, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');

        $this->load->model("mdlbonplan");
        $this->load->model("mdlcategorie");
        $this->load->model("mdlville");
        $this->load->model("mdldepartement");
        $this->load->model("mdlbonplanpagination");
        $this->load->model("mdlcommercant");
        $this->load->model("Commercant");
        $this->load->library('pagination');
        $this->load->model("AssCommercantAbonnement");
        $this->load->model("mdlannonce");
        $this->load->model("Abonnement");
        $this->load->model("rubrique");
        $this->load->model("mdl_agenda");
        $this->load->model("notification");

        $this->load->library('session');

        $this->load->library("pagination");

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");

        check_vivresaville_id_ville();
    }

    function index()
    {


        /*if ($this->ion_auth->is_admin()) {
                $this->session->set_flashdata('domain_from', '1');
                redirect("admin/home");
            }*/

        ////////////////DELETE OLD AGENDA date_fin past 8 days
        $this->mdl_agenda->deleteOldAgenda_fin8jours();


        ////START VERIFY SUBSCRIPTION
        @verify_all_pro_subscription();


        $argOffset = (isset($_SESSION["argOffset"])) ? $_SESSION["argOffset"] : 0;

        //if the link doesn't come from page navigation, clear category session
        $current_URI_club = $_SERVER['REQUEST_URI'];
        $current_URI_club_array = explode("bonplan/index", $current_URI_club);
        ////$this->firephp->log(count($current_URI_club_array), 'nb_array');
        if (count($current_URI_club_array) == 1) {
            $this->session->unset_userdata('iCategorieId_x');
            $this->session->unset_userdata('iVilleId_x');
            $this->session->unset_userdata('iDepartementId_x');
            $this->session->unset_userdata('iCommercantId_x');
            $this->session->unset_userdata('zMotCle_x');
            $this->session->unset_userdata('iOrderBy_x');
            $this->session->unset_userdata('iWhereMultiple_x');
            $this->session->unset_userdata('inputFromGeolocalisation_x');
            $this->session->unset_userdata('inputGeolocalisationValue_x');
            $this->session->unset_userdata('inputGeoLatitude_x');
            $this->session->unset_userdata('inputGeoLongitude_x');
        }
        //if the link doesn't come from page navigation, clear category session

        if ($this->input->post("inputStringHidden") == "0") unset($_SESSION['iCategorieId']);
        if ($this->input->post("inputStringCommercantHidden") == "0") unset($_SESSION['iCommercantId']);
        if ($this->input->post("zMotCle") == "") unset($_SESSION['zMotCle']);
        if ($this->input->post("inputStringVilleHidden") == "") unset($_SESSION['iVilleId']);
        if ($this->input->post("inputStringDepartementHidden") == "") unset($_SESSION['iDepartementId']);
        if ($this->input->post("inputStringOrderByHidden") == "0") unset($_SESSION['iOrderBy']);
        if ($this->input->post("inputStringWhereMultiple") == "0") unset($_SESSION['iWhereMultiple']);
        if ($this->input->post("inputFromGeolocalisation") == "0" || $this->input->post("inputFromGeolocalisation") == "1") unset($_SESSION['inputFromGeolocalisation']);
        if ($this->input->post("inputGeolocalisationValue") == "10") unset($_SESSION['inputGeolocalisationValue']);
        if ($this->input->post("inputGeoLatitude") == "0") unset($_SESSION['inputGeoLatitude']);
        if ($this->input->post("inputGeoLongitude") == "0") unset($_SESSION['inputGeoLongitude']);


        $data["iFavoris"] = $this->input->post("hdnFavoris");


        $PerPage = 12;


        if (isset($_POST["inputStringHidden"])) {
            unset($_SESSION['iCategorieId']);
            unset($_SESSION['iVilleId']);
            unset($_SESSION['iDepartementId']);
            unset($_SESSION['iCommercantId']);
            unset($_SESSION['zMotCle']);
            unset($_SESSION['iOrderBy']);
            unset($_SESSION['iWhereMultiple']);
            unset($_SESSION['inputFromGeolocalisation']);
            unset($_SESSION['inputGeolocalisationValue']);
            unset($_SESSION['inputGeoLatitude']);
            unset($_SESSION['inputGeoLongitude']);

            //$iCategorieId = $_POST["inputStringHidden"] ; 
            $iCategorieId_all0 = $this->input->post("inputStringHidden");
            if (isset($iCategorieId_all0) && $iCategorieId_all0 != "" && $iCategorieId_all0 != NULL && $iCategorieId_all0 != '0') {
                $iCategorieId_all = substr($iCategorieId_all0, 1);
                $iCategorieId = explode(',', $iCategorieId_all);
            } else {
                $iCategorieId = '0';
            }
            if (isset($_POST["inputStringVilleHidden"])) $iVilleId = $_POST["inputStringVilleHidden"]; else $iVilleId = 0;
            if (isset($_POST["inputStringDepartementHidden"])) $iDepartementId = $_POST["inputStringDepartementHidden"]; else $iDepartementId = 0;
            if (isset($_POST["inputStringCommercantHidden"])) $iCommercantId = $_POST["inputStringCommercantHidden"]; else $iCommercantId = 0;
            if (isset($_POST["zMotCle"])) $zMotCle = $_POST["zMotCle"]; else $zMotCle = "";
            if (isset($_POST["inputStringOrderByHidden"])) $iOrderBy = $_POST["inputStringOrderByHidden"]; else $iOrderBy = '';
            if (isset($_POST["inputStringWhereMultiple"])) $iWhereMultiple = $_POST["inputStringWhereMultiple"]; else $iWhereMultiple = '';
            if (isset($_POST["inputFromGeolocalisation"])) $inputFromGeolocalisation = $_POST["inputFromGeolocalisation"]; else $inputFromGeolocalisation = "0";
            if (isset($_POST["inputGeolocalisationValue"])) $inputGeolocalisationValue = $_POST["inputGeolocalisationValue"]; else $inputGeolocalisationValue = "10";
            if (isset($_POST["inputGeoLatitude"])) $inputGeoLatitude = $_POST["inputGeoLatitude"]; else $inputGeoLatitude = "0";
            if (isset($_POST["inputGeoLongitude"])) $inputGeoLongitude = $_POST["inputGeoLongitude"]; else $inputGeoLongitude = "0";


            $_SESSION['iCategorieId'] = $iCategorieId;
            $this->session->set_userdata('iCategorieId_x', $iCategorieId);
            $_SESSION['iVilleId'] = $iVilleId;
            $this->session->set_userdata('iVilleId_x', $iVilleId);
            $_SESSION['iDepartementId'] = $iDepartementId;
            $this->session->set_userdata('iDepartementId_x', $iDepartementId);
            $_SESSION['iCommercantId'] = $iCommercantId;
            $this->session->set_userdata('iCommercantId_x', $iCommercantId);
            $_SESSION['zMotCle'] = $zMotCle;
            $this->session->set_userdata('zMotCle_x', $zMotCle);
            $_SESSION['iOrderBy'] = $iOrderBy;
            $this->session->set_userdata('iOrderBy_x', $iOrderBy);
            $_SESSION['iWhereMultiple'] = $iWhereMultiple;
            $this->session->set_userdata('iWhereMultiple_x', $iWhereMultiple);
            $_SESSION['inputFromGeolocalisation'] = $inputFromGeolocalisation;
            $this->session->set_userdata('inputFromGeolocalisation_x', $inputFromGeolocalisation);
            $_SESSION['inputGeolocalisationValue'] = $inputGeolocalisationValue;
            $this->session->set_userdata('inputGeolocalisationValue_x', $inputGeolocalisationValue);
            $_SESSION['inputGeoLatitude'] = $inputGeoLatitude;
            $this->session->set_userdata('inputGeoLatitude_x', $inputGeoLatitude);
            $_SESSION['inputGeoLongitude'] = $inputGeoLongitude;
            $this->session->set_userdata('inputGeoLongitude_x', $inputGeoLongitude);

            $data['iCategorieId'] = $iCategorieId;
            $data['iVilleId'] = $iVilleId;
            $data['iDepartementId'] = $iDepartementId;
            $data['iCommercantId'] = $iCommercantId;
            $data['zMotCle'] = $zMotCle;
            $data['iOrderBy'] = $iOrderBy;
            $data['iWhereMultiple'] = $iWhereMultiple;

            $session_iCategorieId = $this->session->userdata('iCategorieId_x');
            $session_iVilleId = $this->session->userdata('iVilleId_x');
            $session_iDepartementId = $this->session->userdata('iDepartementId_x');
            $session_iCommercantId = $this->session->userdata('iCommercantId_x');
            $session_zMotCle = $this->session->userdata('zMotCle_x');
            $session_iOrderBy = $this->session->userdata('iOrderBy_x');
            $session_iWhereMultiple = $this->session->userdata('iWhereMultiple_x');
            $session_inputFromGeolocalisation = $this->session->userdata('inputFromGeolocalisation_x');
            $session_inputGeolocalisationValue = $this->session->userdata('inputGeolocalisationValue_x');
            $session_inputGeoLatitude = $this->session->userdata('inputGeoLatitude_x');
            $session_inputGeoLongitude = $this->session->userdata('inputGeoLongitude_x');

            //$toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche($iCategorieId, $iVilleId, $zMotCle, $data["iFavoris"], $argOffset, $PerPage) ;
            //$toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche($_SESSION['iCategorieId'], $_SESSION['iCommercantId'], $_SESSION['zMotCle'], $data["iFavoris"], $argOffset, $PerPage, $_SESSION['iVilleId'], $iOrderBy, $_SESSION['inputFromGeolocalisation'], $_SESSION['inputGeolocalisationValue'], $_SESSION['inputGeoLatitude'], $_SESSION['inputGeoLongitude']) ;//, $argOffset, $PerPage
            $toListeBonPlan_all_pvc = $this->mdlbonplan->listeBonPlanRecherche($session_iCategorieId, $session_iCommercantId, $session_zMotCle, $data["iFavoris"], 0, 10000, $session_iVilleId, $session_iDepartementId, $session_iOrderBy, $session_inputFromGeolocalisation, $session_inputGeolocalisationValue, $session_inputGeoLatitude, $session_inputGeoLongitude, $session_iWhereMultiple);
            $TotalRows = count($this->mdlbonplan->listeBonPlanRecherche($session_iCategorieId, $session_iCommercantId, $session_zMotCle, $data["iFavoris"], 0, 10000, $session_iVilleId, $session_iDepartementId, $session_iOrderBy, $session_inputFromGeolocalisation, $session_inputGeolocalisationValue, $session_inputGeoLatitude, $session_inputGeoLongitude, $session_iWhereMultiple));

            $config_pagination = array();
            $config_pagination["base_url"] = base_url() . "front/bonplan/index/";
            $config_pagination["total_rows"] = $TotalRows;
            $config_pagination["per_page"] = $PerPage;
            $config_pagination["uri_segment"] = 4;
            $config_pagination['first_link'] = 'Première page';
            $config_pagination['last_link'] = 'Dernière page';
            $config_pagination['prev_link'] = 'Précédent';
            $config_pagination['next_link'] = 'Suivant';
            $this->pagination->initialize($config_pagination);

            $rsegment3 = $this->uri->segment(4);
            if (strpos($rsegment3, '&content_only_list') !== false) {
                $pieces_rseg = explode("&content_only_list", $rsegment3);
                $rsegment3 = $pieces_rseg[0];
            } else {
                $rsegment3 = $this->uri->segment(4);
            }
            $page_pagination = ($rsegment3) ? $rsegment3 : 0;

            //$toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche($_SESSION['iCategorieId'], $_SESSION['iCommercantId'], $_SESSION['zMotCle'], $data["iFavoris"], $page_pagination, $config_pagination["per_page"], $_SESSION['iVilleId'], $iOrderBy, $_SESSION['inputFromGeolocalisation'], $_SESSION['inputGeolocalisationValue'], $_SESSION['inputGeoLatitude'], $_SESSION['inputGeoLongitude']) ;//, $argOffset, $PerPage
            $toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche($session_iCategorieId, $session_iCommercantId, $session_zMotCle, $data["iFavoris"], $page_pagination, $config_pagination["per_page"], $session_iVilleId, $session_iDepartementId, $session_iOrderBy, $session_inputFromGeolocalisation, $session_inputGeolocalisationValue, $session_inputGeoLatitude, $session_inputGeoLongitude, $session_iWhereMultiple);
            $data["links_pagination"] = $this->pagination->create_links();

        } else {
            $data["iFavoris"] = "";
            $session_iCategorieId = $this->session->userdata('iCategorieId_x');
            $session_iVilleId = $this->session->userdata('iVilleId_x');
            $session_iDepartementId = $this->session->userdata('iDepartementId_x');
            $session_iCommercantId = $this->session->userdata('iCommercantId_x');
            $session_zMotCle = $this->session->userdata('zMotCle_x');
            $session_iOrderBy = $this->session->userdata('iOrderBy_x');
            $session_iWhereMultiple = $this->session->userdata('iWhereMultiple_x');
            $session_inputFromGeolocalisation = $this->session->userdata('inputFromGeolocalisation_x');
            $session_inputGeolocalisationValue = $this->session->userdata('inputGeolocalisationValue_x');
            $session_inputGeoLatitude = $this->session->userdata('inputGeoLatitude_x');
            $session_inputGeoLongitude = $this->session->userdata('inputGeoLongitude_x');

            $iCategorieId = (isset($session_iCategorieId)) ? $session_iCategorieId : "";
            $iVilleId = (isset($session_iVilleId)) ? $session_iVilleId : 0;
            $iDepartementId = (isset($session_iDepartementId)) ? $session_iDepartementId : 0;
            $iCommercantId = (isset($session_iCommercantId)) ? $session_iCommercantId : "";
            $zMotCle = (isset($session_zMotCle)) ? $session_zMotCle : "";
            $iOrderBy = (isset($session_iOrderBy)) ? $session_iOrderBy : "";
            $iWhereMultiple = (isset($session_iWhereMultiple)) ? $session_iWhereMultiple : "";
            $inputFromGeolocalisation = (isset($session_inputFromGeolocalisation)) ? $session_inputFromGeolocalisation : "0";
            $inputGeolocalisationValue = (isset($session_inputGeolocalisationValue)) ? $session_inputGeolocalisationValue : "10";
            $inputGeoLatitude = (isset($session_inputGeoLatitude)) ? $session_inputGeoLatitude : "0";
            $inputGeoLongitude = (isset($session_inputGeoLongitude)) ? $session_inputGeoLongitude : "0";
            //$toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche($iCategorieId, $iCommercantId, $zMotCle, $data["iFavoris"], $argOffset, $PerPage, $iVilleId, $iOrderBy, $inputFromGeolocalisation, $inputGeolocalisationValue, $inputGeoLatitude, $inputGeoLongitude, $iWhereMultiple) ;//, $argOffset, $PerPage
            $toListeBonPlan_all_pvc = $this->mdlbonplan->listeBonPlanRecherche($iCategorieId, $iCommercantId, $zMotCle, $data["iFavoris"], 0, 10000, $iVilleId, $iDepartementId, $iOrderBy, $inputFromGeolocalisation, $inputGeolocalisationValue, $inputGeoLatitude, $inputGeoLongitude, $iWhereMultiple);
            $TotalRows = count($this->mdlbonplan->listeBonPlanRecherche($iCategorieId, $iCommercantId, $zMotCle, $data["iFavoris"], 0, 10000, $iVilleId, $iDepartementId, $iOrderBy, $inputFromGeolocalisation, $inputGeolocalisationValue, $inputGeoLatitude, $inputGeoLongitude, $iWhereMultiple));

            $config_pagination = array();
            $config_pagination["base_url"] = base_url() . "front/bonplan/index/";
            $config_pagination["total_rows"] = $TotalRows;
            $config_pagination["per_page"] = $PerPage;
            $config_pagination["uri_segment"] = 4;
            $config_pagination['first_link'] = 'Première page';
            $config_pagination['last_link'] = 'Dernière page';
            $config_pagination['prev_link'] = 'Précédent';
            $config_pagination['next_link'] = 'Suivant';
            $this->pagination->initialize($config_pagination);

            $rsegment3 = $this->uri->segment(4);
            if (strpos($rsegment3, '&content_only_list') !== false) {
                $pieces_rseg = explode("&content_only_list", $rsegment3);
                $rsegment3 = $pieces_rseg[0];
            } else {
                $rsegment3 = $this->uri->segment(4);
            }
            $page_pagination = ($rsegment3) ? $rsegment3 : 0;

            $toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche($iCategorieId, $iCommercantId, $zMotCle, $data["iFavoris"], $page_pagination, $config_pagination["per_page"], $iVilleId, $iDepartementId, $iOrderBy, $inputFromGeolocalisation, $inputGeolocalisationValue, $inputGeoLatitude, $inputGeoLongitude, $iWhereMultiple);//, $argOffset, $PerPage
            $data["links_pagination"] = $this->pagination->create_links();

        }

        //save all bonplan adress into googlemap--------------------------------------------------------------------------------
        $this->load->model("mdlmapgoogle");
        $idBonplan_to_googlemap = array();
        $i_randcount = 0;
        foreach ($toListeBonPlan_all_pvc as $objBonPlan_all_pvc) {
            $idBonplan_to_googlemap[$i_randcount] = $objBonPlan_all_pvc->bonplan_id;
            $i_randcount++;
        }
        //var_dump($idBonplan_to_googlemap);
        //die("stop");
        //$this->mdlmapgoogle->setMap($idBonplan_to_googlemap, "bonplan");/***********************************/
        //save all bonplan adress into googlemap--------------------------------------------------------------------------------

        $this->load->model("mdlcategorie");
        $this->load->model("mdlville");
        $toCategorie = $this->mdlcategorie->GetBonplanSouscategorie();
        $toCategoriePrincipale = $this->mdlcategorie->GetBonplanCategoriePrincipale();
        $departement_check = $this->session->userdata('iDepartementId_x');
        if (isset($departement_check) && $departement_check != "" && $departement_check != null) {
            $toVille = $this->mdlville->GetBonplanVilles_by_departement($this->session->userdata('iDepartementId_x'));
        } else {
            $toVille = $this->mdlville->GetBonplanVilles();
        }

        $toDepartement = $this->mdldepartement->GetBonplanDepartements();
        $data['toVille'] = $toVille;
        $data['toDepartement'] = $toDepartement;
        $data['toCategorie'] = $toCategorie;
        $data['toCategoriePrincipale'] = $toCategoriePrincipale;
        $toCommercant = $this->mdlcommercant->GetAllCommercant_with_bonplan();
        $data['toCommercant'] = $toCommercant;


        $iNombreLiens = $TotalRows / $PerPage;
        if ($iNombreLiens > round($iNombreLiens)) {
            $iNombreLiens = round($iNombreLiens) + 1;
        } else {
            $iNombreLiens = round($iNombreLiens);
        }

        $data["iNombreLiens"] = $iNombreLiens;

        $data["PerPage"] = $PerPage;
        $data["TotalRows"] = $TotalRows;
        $data["argOffset"] = $argOffset;
        $argOffset_limit = round($argOffset) + $PerPage;


        ////$this->firephp->log($iOrderBy, 'iOrderBy_2');

        $data['toListeBonPlan'] = $toListeBonPlan;//william hack
        $data['pagecategory'] = 'bonplan';
        // print_r($data['toCommercant']);exit();
        // End OP

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;


        $data["mdldepartement"] = $this->mdldepartement;
        $data["mdlville"] = $this->mdlville;

        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            $data['IdUser'] = $iduser;
            $user_groups = $this->ion_auth->get_users_groups($user_ion_auth->id)->result();
            ////$this->firephp->log($user_groups, 'user_groups');
            if ($user_groups[0]->id == 1) $typeuser = "1";
            else if ($user_groups[0]->id == 2) $typeuser = "0";
            else if ($user_groups[0]->id == 3 || $user_groups[0]->id == 4 || $user_groups[0]->id == 5) $typeuser = "COMMERCANT";
            $data['typeuser'] = $typeuser;
        }
        $data["main_menu_content"] = "bonplan";
        $data["mdlannonce"] = $this->mdlannonce;

        $this->session->set_userdata('nohome', '1');


        //$this->load->view('front/vwListeBonPlan', $data) ;
        //$this->load->view('frontAout2013/bonplan_main', $data) ;
        //$this->load->view('privicarte/user_agent_main_home', $data) ;
        //$this->load->view('sortez/user_agent_main_home', $data);

        
        $http_code = 200;
        header('Content-type: json');
        header('HTTP/1.1: ' . $http_code);
        header('Status: ' . $http_code);
        exit(json_encode($data));
        
    }

    function localdata_IdVille($IdVille)
    {
        //echo "ok";
        //LOCALDATA FILTRE SET
        $this->session->set_userdata('localdata_IdVille', $IdVille);
        //LOCALDATA FILTRE SET
        redirect("front/bonplan");
    }


    function check_category_list()
    {
        $inputStringDepartementHidden_bonplans = $this->input->post("inputStringDepartementHidden_bonplans");
        $inputStringVilleHidden_bonplans = $this->input->post("inputStringVilleHidden_bonplans");

        //$toCategorie= $this->mdlcategorie->GetCommercantSouscategorie();
        //$toVille= $this->mdlville->GetCommercantVilles();//get ville list of commercant
        if ((isset($inputStringVilleHidden_bonplans) && $inputStringVilleHidden_bonplans != "" && $inputStringVilleHidden_bonplans != '0' && $inputStringVilleHidden_bonplans != NULL)
            || (isset($inputStringDepartementHidden_bonplans) && $inputStringDepartementHidden_bonplans != "" && $inputStringDepartementHidden_bonplans != '0' && $inputStringDepartementHidden_bonplans != NULL)
        ) {
            $toCategorie_principale = $this->mdlcategorie->GetBonplanCategoriePrincipaleByVille($inputStringDepartementHidden_bonplans, $inputStringVilleHidden_bonplans);
        } else {
            $toCategorie_principale = $this->mdlcategorie->GetBonplanCategoriePrincipale();
        }

        $result_to_show = '';

        if (isset($toCategorie_principale)) {
            foreach ($toCategorie_principale as $oCategorie_principale) {
                $result_to_show .= '<div class="leftcontener2013title btn btn-default left_category_list_bonplan" onClick="javascript:show_current_categ_subcateg(' . $oCategorie_principale->IdRubrique . ')" style="margin-top:7px;">';
                $result_to_show .= ucfirst(strtolower($oCategorie_principale->Nom)) . " (" . $oCategorie_principale->nb_bonplan . ")";
                $result_to_show .= '</div>';

                $result_to_show .= '<div class="leftcontener2013content" id="leftcontener2013content_' . $oCategorie_principale->IdRubrique . '" style="margin-bottom:5px; margin-top:10px;">';
                $OCommercantSousRubrique = $this->mdlcategorie->GetBonplanSouscategorieByRubrique($oCategorie_principale->IdRubrique);
                //$result_to_show .= '<br/>';


                /*if (isset($_SESSION['iCategorieId'])) {
                    $iCategorieId_sess = $_SESSION['iCategorieId'];
                }*/
                $session_iCategorieId = $this->session->userdata('iCategorieId_x');
                if (isset($session_iCategorieId)) {
                    $iCategorieId_sess = $session_iCategorieId;
                }


                if (count($OCommercantSousRubrique) > 0) {
                    $i_rand = 0;
                    foreach ($OCommercantSousRubrique as $ObjCommercantSousRubrique) {
                        $result_to_show .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>';
                        $result_to_show .= '<td width="22"><input  onClick="javascript:submit_search_bonplan();" name="check_part[' . $i_rand . ']" id="check_part_' . $ObjCommercantSousRubrique->IdSousRubrique . '" type="checkbox" value="' . $ObjCommercantSousRubrique->IdSousRubrique . '"';
                        if (isset($iCategorieId_sess) && is_array($iCategorieId_sess)) {
                            if (in_array($ObjCommercantSousRubrique->IdSousRubrique, $iCategorieId_sess)) $result_to_show .= 'checked';
                        }
                        $result_to_show .= '></td><td>';
                        $result_to_show .= ucfirst(strtolower($ObjCommercantSousRubrique->Nom)) . ' (' . $ObjCommercantSousRubrique->nb_bonplan . ')</td>';
                        $result_to_show .= '</tr></table>';
                        $i_rand++;
                    }
                }
                $result_to_show .= '</div>';
            }
        }

        echo $result_to_show;

    }


    function redirect_bonplan()
    {
        unset($_SESSION['iCategorieId']);
        unset($_SESSION['iCommercantId']);
        unset($_SESSION['iVilleId']);
        unset($_SESSION['zMotCle']);
        redirect("front/bonplan/");
    }

    function ficheBonplan($_iDCommercant = 0, $_iDBonplan = 0, $mssg = 0)
    {


        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        } else if ($this->ion_auth->in_group(2)) {
            redirect();
        }

        // check annonce limit number
        if ($_iDCommercant != 0 && $_iDBonplan==0){
            $toListeMesAnnonce__ = $this->mdlbonplan->listeMesBonplans($_iDCommercant);
            $objCommercant__ = $this->Commercant->GetById($_iDCommercant);
            if (isset($objCommercant__->limit_bonplan) && count($toListeMesAnnonce__)>=intval($objCommercant__->limit_bonplan)) {
                redirect("front/bonplan/listeMesBonplans/".$_iDCommercant);
            }
        }


        $data["no_main_menu_home"] = "1";
        $data["no_left_menu_home"] = "1";


        //start verify if merchant subscription contain bonplan WILLIAM
        $current_date = date("Y-m-d");
        $query_abcom_cond = " IdCommercant = " . $_iDCommercant . " AND ('" . $current_date . "' BETWEEN DateDebut AND DateFin)";
        $toAbonnementCommercant = $this->AssCommercantAbonnement->GetWhere($query_abcom_cond, 0, 1);
        if (sizeof($toAbonnementCommercant)) {
            foreach ($toAbonnementCommercant as $oAbonnementCommercant) {
                if (isset($oAbonnementCommercant) && ($oAbonnementCommercant->IdAbonnement == 1 || $oAbonnementCommercant->IdAbonnement == 3)) {//IdAbonnement doesn't contain annonce
                    /*redirect("front/professionnels/fiche/$_iDCommercant");*/
                }
            }
        } /*else redirect("front/professionnels/fiche/$_iDCommercant");*/
        //end verify if merchant subscription contain bonplan WILLIAM


        $this->load->helper('ckeditor');
        //Ckeditor's configuration
        $data['ckeditor0'] = array(

            //ID of the textarea that will be replaced
            'id' => 'idDescription',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'),
                    array('lineheight')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['pagecategory'] = "admin_commercant";

        $data["idCommercant"] = $_iDCommercant;
        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iDCommercant);
        $data['oInfoCommercant'] = $oInfoCommercant;

        $data['mssg'] = $mssg;

        $data["colCategorie"] = $this->mdlcategorie->GetAll();
        $data["listVille"] = $this->mdlville->GetAll();
        if (IsValidId($_iDBonplan) && $_iDBonplan != 0) {
            $data["title"] = "Modification Bonplan";
            $data["oBonplan"] = $this->mdlbonplan->GetBonplan($_iDBonplan);
            //$this->load->view('front/vwFicheBonplan', $data) ;
            //$this->load->view('front2013/vwFicheBonplan', $data) ;
            //$this->load->view('frontAout2013/vwFicheBonplan', $data) ;
            $this->load->view('privicarte/vwFicheBonplan', $data);
        } else {
            $data["title"] = "Créer un bonplan";
            //$this->load->view('front/vwFicheBonplan', $data) ;
            //$this->load->view('front2013/vwFicheBonplan', $data) ;
            //$this->load->view('frontAout2013/vwFicheBonplan', $data) ;
            $this->load->view('privicarte/vwFicheBonplan', $data);
        }
    }

    function creerBonplan($_iDCommercant = 0)
    {
        $oBonplan = $this->input->post("bonplan");
        $oBonplan['bonplan_quantite_depart_unique'] = $oBonplan['bp_unique_qttprop'];
        $oBonplan['bonplan_quantite_depart_multiple'] = $oBonplan['bp_multiple_qttprop'];

        //verify 3 bonplans limit
        /*if (isset($_iDCommercant) && $_iDCommercant != 0) {
            $toListeMesBonplans = $this->mdlbonplan->listeMesBonplans($_iDCommercant);
            if (count($toListeMesBonplans) >= 3)
                redirect("front/bonplan/listeMesBonplans/$_iDCommercant");
        }*/

        $oBonplan["bonplan_photo1"] = $this->input->post("photo1Associe");
        $oBonplan["bonplan_photo2"] = $this->input->post("photo2Associe");
        $oBonplan["bonplan_photo3"] = $this->input->post("photo3Associe");
        $oBonplan["bonplan_photo4"] = $this->input->post("photo4Associe");

        $oBonplan["bonplan_date_debut"] = convert_Frenchdate_to_Sqldate($oBonplan["bonplan_date_debut"]);
        if ($oBonplan["bonplan_date_debut"]=="--" || $oBonplan["bonplan_date_debut"]=="0000-00-00") $oBonplan["bonplan_date_debut"] = null;
        $oBonplan["bonplan_date_fin"] = convert_Frenchdate_to_Sqldate($oBonplan["bonplan_date_fin"]);
        if ($oBonplan["bonplan_date_fin"]=="--" || $oBonplan["bonplan_date_fin"]=="0000-00-00") $oBonplan["bonplan_date_fin"] = null;

        $oBonplan["bp_unique_date_debut"] = convert_Frenchdate_to_Sqldate($oBonplan["bp_unique_date_debut"]);
        if ($oBonplan["bp_unique_date_debut"]=="--" || $oBonplan["bp_unique_date_debut"]=="0000-00-00") $oBonplan["bp_unique_date_debut"] = null;
        $oBonplan["bp_unique_date_fin"] = convert_Frenchdate_to_Sqldate($oBonplan["bp_unique_date_fin"]);
        if ($oBonplan["bp_unique_date_fin"]=="--" || $oBonplan["bp_unique_date_fin"]=="0000-00-00") $oBonplan["bp_unique_date_fin"] = null;
        $oBonplan["bp_unique_date_visit"] = convert_Frenchdate_to_Sqldate($oBonplan["bp_unique_date_visit"]);
        if ($oBonplan["bp_unique_date_visit"]=="--" || $oBonplan["bp_unique_date_visit"]=="0000-00-00") $oBonplan["bp_unique_date_visit"] = null;

        $oBonplan["bp_multiple_date_debut"] = convert_Frenchdate_to_Sqldate($oBonplan["bp_multiple_date_debut"]);
        if ($oBonplan["bp_multiple_date_debut"]=="--" || $oBonplan["bp_multiple_date_debut"]=="0000-00-00") $oBonplan["bp_multiple_date_debut"] = null;
        $oBonplan["bp_multiple_date_fin"] = convert_Frenchdate_to_Sqldate($oBonplan["bp_multiple_date_fin"]);
        if ($oBonplan["bp_multiple_date_fin"]=="--" || $oBonplan["bp_multiple_date_fin"]=="0000-00-00") $oBonplan["bp_multiple_date_fin"] = null;
        $oBonplan["bp_multiple_date_visit"] = convert_Frenchdate_to_Sqldate($oBonplan["bp_multiple_date_visit"]);
        if ($oBonplan["bp_multiple_date_visit"]=="--" || $oBonplan["bp_multiple_date_visit"]=="0000-00-00") $oBonplan["bp_multiple_date_visit"] = null;

        $IdInsertedBonplan = $this->mdlbonplan->insertBonplan($oBonplan);
        $data["title"] = "Modification bonplan";
        $data["idCommercant"] = $_iDCommercant;
        $data["msg"] = "le bon plan a été bien crée";
        //$this->load->view('front/vwListeBonPlan', $data) ;
        // $this->load->view('front/vwFicheBonplan', $data) ;
        redirect("front/bonplan/ficheBonplan/$_iDCommercant/$IdInsertedBonplan/1");
    }

    function modifBonplan($_iDCommercant = 0)
    {
        $oBonplan = $this->input->post("bonplan");

        $oBonplan['bonplan_quantite_depart_unique'] = $oBonplan['bp_unique_qttprop'];
        $oBonplan['bonplan_quantite_depart_multiple'] = $oBonplan['bp_multiple_qttprop'];

        $oBonplan["bonplan_photo1"] = $this->input->post("photo1Associe");
        $oBonplan["bonplan_photo2"] = $this->input->post("photo2Associe");
        $oBonplan["bonplan_photo3"] = $this->input->post("photo3Associe");
        $oBonplan["bonplan_photo4"] = $this->input->post("photo4Associe");

        $oBonplan["bonplan_date_debut"] = convert_Frenchdate_to_Sqldate($oBonplan["bonplan_date_debut"]);
        if ($oBonplan["bonplan_date_debut"]=="--" || $oBonplan["bonplan_date_debut"]=="0000-00-00") $oBonplan["bonplan_date_debut"] = null;
        $oBonplan["bonplan_date_fin"] = convert_Frenchdate_to_Sqldate($oBonplan["bonplan_date_fin"]);
        if ($oBonplan["bonplan_date_fin"]=="--" || $oBonplan["bonplan_date_fin"]=="0000-00-00") $oBonplan["bonplan_date_fin"] = null;

        $oBonplan["bp_unique_date_debut"] = convert_Frenchdate_to_Sqldate($oBonplan["bp_unique_date_debut"]);
        if ($oBonplan["bp_unique_date_debut"]=="--" || $oBonplan["bp_unique_date_debut"]=="0000-00-00") $oBonplan["bp_unique_date_debut"] = null;
        $oBonplan["bp_unique_date_fin"] = convert_Frenchdate_to_Sqldate($oBonplan["bp_unique_date_fin"]);
        if ($oBonplan["bp_unique_date_fin"]=="--" || $oBonplan["bp_unique_date_fin"]=="0000-00-00") $oBonplan["bp_unique_date_fin"] = null;
        $oBonplan["bp_unique_date_visit"] = convert_Frenchdate_to_Sqldate($oBonplan["bp_unique_date_visit"]);
        if ($oBonplan["bp_unique_date_visit"]=="--" || $oBonplan["bp_unique_date_visit"]=="0000-00-00") $oBonplan["bp_unique_date_visit"] = null;

        $oBonplan["bp_multiple_date_debut"] = convert_Frenchdate_to_Sqldate($oBonplan["bp_multiple_date_debut"]);
        if ($oBonplan["bp_multiple_date_debut"]=="--" || $oBonplan["bp_multiple_date_debut"]=="0000-00-00") $oBonplan["bp_multiple_date_debut"] = null;
        $oBonplan["bp_multiple_date_fin"] = convert_Frenchdate_to_Sqldate($oBonplan["bp_multiple_date_fin"]);
        if ($oBonplan["bp_multiple_date_fin"]=="--" || $oBonplan["bp_multiple_date_fin"]=="0000-00-00") $oBonplan["bp_multiple_date_fin"] = null;
        $oBonplan["bp_multiple_date_visit"] = convert_Frenchdate_to_Sqldate($oBonplan["bp_multiple_date_visit"]);
        if ($oBonplan["bp_multiple_date_visit"]=="--" || $oBonplan["bp_multiple_date_visit"]=="0000-00-00") $oBonplan["bp_multiple_date_visit"] = null;

        $IdUpdatedBonplan = $this->mdlbonplan->updateBonplan($oBonplan);
        $data["title"] = "Créer un Bonplan";
        $data["idCommercant"] = $_iDCommercant;
        $data["msg"] = "le bon plan a été bien modifié";
        //$this->load->view('front/vwListeBonPlan', $data) ;
        // $this->load->view('front/vwFicheBonplan', $data) ;
        //redirect ("front/bonplan/listeMesBonplans/$_iDCommercant") ;
        redirect("front/bonplan/ficheBonplan/$_iDCommercant/$IdUpdatedBonplan/1");
        //echo $oBonplan["bonplan_date_debut"];
    }

    function listeMesBonplans($_iDCommercant = 0)
    {

        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        } else if ($this->ion_auth->in_group(2)) {
            redirect();
        }

        $data["no_main_menu_home"] = "1";
        $data["no_left_menu_home"] = "1";
        $data["idCommercant"] = $_iDCommercant;


        //start verify if merchant subscription contain bonplan WILLIAM
        $current_date = date("Y-m-d");
        $query_abcom_cond = " IdCommercant = " . $_iDCommercant . " AND ('" . $current_date . "' BETWEEN DateDebut AND DateFin)";
        $toAbonnementCommercant = $this->AssCommercantAbonnement->GetWhere($query_abcom_cond, 0, 1);
        if (sizeof($toAbonnementCommercant)) {
            foreach ($toAbonnementCommercant as $oAbonnementCommercant) {
                if (isset($oAbonnementCommercant) && ($oAbonnementCommercant->IdAbonnement == 1 || $oAbonnementCommercant->IdAbonnement == 3)) {//IdAbonnement doesn't contain annonce
                    /*redirect("front/professionnels/fiche/$_iDCommercant");*/
                }
            }
        } /*else redirect("front/professionnels/fiche/$_iDCommercant");*/
        //end verify if merchant subscription contain bonplan WILLIAM

        $data['pagecategory'] = "listBonplans";

        if ($_iDCommercant != 0) {
            $toListeMesBonplans = $this->mdlbonplan->listeMesBonplans($_iDCommercant);
            $data['toListeMesBonplans'] = $toListeMesBonplans;


            $objCommercant = $this->Commercant->GetById($_iDCommercant);
            $data["objCommercant"] = $objCommercant ;
            //verify 20 annonces limit
            if (isset($objCommercant->limit_bonplan) && count($toListeMesBonplans)>=intval($objCommercant->limit_bonplan)) {
                $data['limit_bonplan_add'] = 1;
            } else {
                $data['limit_bonplan_add'] = 0;
            }


            $data['pagecategory'] = "admin_commercant";

            //$this->load->view('front/vwCommercantBonPlans', $data) ;
            $this->load->view('privicarte/mes_bonplans', $data);
            /*if (count($toListeMesBonplans)==0) {
                redirect ("front/bonplan/ficheBonplan/$_iDCommercant");
            } else {
                redirect ("front/bonplan/ficheBonplan/$_iDCommercant/".$toListeMesBonplans->bonplan_id);
            }*/

        }
    }

    function supprimBonplan($_iDBonplan, $_iDCommercant)
    {

        $zReponse = $this->mdlbonplan->supprimeBonplans($_iDBonplan);
        redirect("front/bonplan/listeMesBonplans/$_iDCommercant");
    }

    function redirection($_iPage)
    {
        $_SESSION["argOffset"] = $_iPage;
        redirect("front/bonplan/");
    }

    function delete_files($prmIdBonplan = 0, $prmFiles)
    {
        $_objBonplan = $this->mdlbonplan->getById($prmIdBonplan);
        $this->load->helper('file');
        $path_parts_ = pathinfo("./application/resources/front/images/" . $_objBonplan->$prmFiles);
        @unlink("./application/resources/front/images/" . $_objBonplan->$prmFiles);//delete picture
        @unlink("./application/resources/front/images/" . $path_parts_['filename'] . "_thumb_100_100." . $path_parts_['extension']);//delete thumb
        $_objBonplan->$prmFiles = null;
        $IdUpdatedBonplan = $this->mdlbonplan->updateBonplan($_objBonplan);

        return true;
    }

    function photosbonplanmobile($bonplanId)
    {
        $oDetailbonplan = $this->mdlbonplan->getById($bonplanId);
        $data['oDetailbonplan'] = $oDetailbonplan;

        $_iCommercantId = $oDetailbonplan->bonplan_commercant_id;

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);
        $data['oInfoCommercant'] = $oInfoCommercant;
        //$data['active_link'] = "annonces";
        $oAssComRub = $this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if (isset($oAssComRub)) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom = $this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        //$this->load->view('front/vwPhotosannoncemobile', $data) ;
        $this->load->view('front2013/photosbonplanmobile', $data);
    }


    function print_coupon($_iDBonplan = 0)
    {

        if ($_iDBonplan == 0) {
            redirect("/");
        }

        $oDetailbonplan = $this->mdlbonplan->getById($_iDBonplan);


        $data['oDetailbonplan'] = $oDetailbonplan;
        $_iCommercantId = $oDetailbonplan->bonplan_commercant_id;

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);
        $data['oInfoCommercant'] = $oInfoCommercant;

        $toVille = $this->mdlville->getVilleById($oInfoCommercant->IdVille);
        $data['oVille'] = $toVille;

        $this->load->view('sortez/print_coupon', $data);

    }

    function get_bonplan_idcommercant()
    {
        //echo 'test';
        $IdCommercant = $this->input->post('IdCommercant');

        $data['obonplan_IdComemrcant'] = $this->mdlbonplan->getListeBonPlan($IdCommercant);
        //var_dump($data['obonplan_IdComemrcant']);

        $this->load->view("sortez/get_bonplan_idcommercant_article", $data);

    }


}