<?php
class commercant extends CI_Controller {
    
    function __construct() {
        parent::__construct();

        $this->output->set_header('Access-Control-Allow-Origin: null');  header('Access-Control-Allow-Credentials: omit');
        header('Access-Control-Allow-Method: PUT, GET, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');

		$this->load->model("mdlcommercant") ;
		$this->load->model("mdlville") ;
		$this->load->model("mdlbonplan") ;
		$this->load->model("mdlcadeau") ;
                
        $this->load->model("mdlannonce") ;
        $this->load->model("rubrique") ;
        $this->load->model("mdlimagespub") ;

        $this->load->model("Abonnement");
        $this->load->library('user_agent');

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");

        $this->load->library('session');

        check_vivresaville_id_ville();

		
	}
	function photoCommercant($_iCommercantId){
            
                $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
                $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);
            
		$oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
		$data['oInfoCommercant'] = $oInfoCommercant ;
                
                $data['active_link'] = "activite1";
                $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
                if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
                $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
                $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
                if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;
                
                $is_mobile = $this->agent->is_mobile();
                //test ipad user agent
                $is_mobile_ipad = $this->agent->is_mobile('ipad');
                $data['is_mobile_ipad'] = $is_mobile_ipad;
                $is_robot = $this->agent->is_robot();
                $is_browser = $this->agent->is_browser();
                $is_platform = $this->agent->platform();
                $data['is_mobile'] = $is_mobile;
                $data['is_robot'] = $is_robot;
                $data['is_browser'] = $is_browser;
                $data['is_platform'] = $is_platform;

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);
                
                
		//$this->load->view('front/vwPhotoCommercant', $data) ;
                $this->load->view('front2013/photoscommercant', $data) ;
	}
        
        
        function videoCommercant($_iCommercantId){
            
            $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
            $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

            $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
            $data['oInfoCommercant'] = $oInfoCommercant ;

            $data['active_link'] = "accueil";
            $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
            if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
            $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
            $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
            if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

            $is_mobile = $this->agent->is_mobile();
            //test ipad user agent
            $is_mobile_ipad = $this->agent->is_mobile('ipad');
            $data['is_mobile_ipad'] = $is_mobile_ipad;
            $is_robot = $this->agent->is_robot();
            $is_browser = $this->agent->is_browser();
            $is_platform = $this->agent->platform();
            $data['is_mobile'] = $is_mobile;
            $data['is_robot'] = $is_robot;
            $data['is_browser'] = $is_browser;
            $data['is_platform'] = $is_platform;

            $this->load->model("user") ;
            if ($this->ion_auth->logged_in()){
                $user_ion_auth = $this->ion_auth->user()->row();
                $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                if ($iduser==null || $iduser==0 || $iduser==""){
                    $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                }
            } else $iduser=0;
            $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));

            $this->load->model("mdl_agenda");
            $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

            $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId) ;
            $data['toBonPlan'] = $toBonPlan ;

            //$data['active_link2'] = '1';
                
                
		//$this->load->view('front/vwVideoCommercant', $data) ;
                $this->load->view('front2013/videocommercant', $data) ;
	}
        
	function plusInfoCommercant($_iCommercantId){
            
        $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
        $data['oInfoCommercant'] = $oInfoCommercant ;


        $data['active_link'] = "activite1";
        $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user") ;
        if ($this->ion_auth->logged_in()){
        $user_ion_auth = $this->ion_auth->user()->row();
        $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
        if ($iduser==null || $iduser==0 || $iduser==""){
            $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        }
        } else $iduser=0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $data['current_page'] = "page_infos";
        $data['pagecategory'] = "pro";

        $data['pageglissiere'] = "page1";
        $this->load->model("mdlglissiere") ;
        $data['mdlglissiere'] = $this->mdlglissiere;

        $data['link_partner_current_page'] = 'page1';

        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId) ;
        $data['toBonPlan'] = $toBonPlan ;

		//ajout bouton rond noire annonce/agenda
            $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
            if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
            else $data['result_check_commercant_annonce'] = '0';
			
			 $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
            if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
            else $data['result_check_commercant_agenda'] = '0';
		
		
        //$this->load->view('front/vwPlusInfoCommercant', $data) ;
        //$this->load->view('front2013/plusinfoscommercant', $data) ;
        //$this->load->view('privicarte/plusinfoscommercant', $data) ;

        $http_code = 200;
        header('Content-type: json');
        header('HTTP/1.1: ' . $http_code);
        header('Status: ' . $http_code);
        exit(json_encode($data));
	}
        
        function plusInfo2Commercant($_iCommercantId){
            
            $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
            $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

            $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
            $data['oInfoCommercant'] = $oInfoCommercant ;


            $data['active_link'] = "activite2";
            $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
            if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
            $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
            $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
            if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;


            $is_mobile = $this->agent->is_mobile();
            //test ipad user agent
            $is_mobile_ipad = $this->agent->is_mobile('ipad');
            $data['is_mobile_ipad'] = $is_mobile_ipad;
            $is_robot = $this->agent->is_robot();
            $is_browser = $this->agent->is_browser();
            $is_platform = $this->agent->platform();
            $data['is_mobile'] = $is_mobile;
            $data['is_robot'] = $is_robot;
            $data['is_browser'] = $is_browser;
            $data['is_platform'] = $is_platform;

            $this->load->model("user") ;
            if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            } else $iduser=0;
            $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));

            $this->load->model("mdl_agenda");
            $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

            $data['current_page'] = "page_infos2";

            $data['pageglissiere'] = "page2";
            $this->load->model("mdlglissiere") ;
            $data['mdlglissiere'] = $this->mdlglissiere;

            $data['link_partner_current_page'] = 'page2';

            $data['pagecategory'] = "pro";

            $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId) ;
            $data['toBonPlan'] = $toBonPlan ;

			//ajout bouton rond noire annonce/agenda
            $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
            if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
            else $data['result_check_commercant_annonce'] = '0';
			
			 $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
            if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
            else $data['result_check_commercant_agenda'] = '0';
		
			
            //$this->load->view('front/vwPlusInfo2Commercant', $data) ;
            //$this->load->view('front2013/plusinfos2commercant', $data) ;
            //$this->load->view('privicarte/plusinfos2commercant', $data) ;

            $http_code = 200;
            header('Content-type: json');
            header('HTTP/1.1: ' . $http_code);
            header('Status: ' . $http_code);
            exit(json_encode($data));
	}
        
        function mentionslegalesmobile($_iCommercantId){
            
            $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
            $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

            $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
            $data['oInfoCommercant'] = $oInfoCommercant ;


            $data['active_link'] = "activite2";
            $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);

            if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
            $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
            $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
            if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;


            $is_mobile = $this->agent->is_mobile();
            //test ipad user agent
            $is_mobile_ipad = $this->agent->is_mobile('ipad');
            $data['is_mobile_ipad'] = $is_mobile_ipad;
            $is_robot = $this->agent->is_robot();
            $is_browser = $this->agent->is_browser();
            $is_platform = $this->agent->platform();
            $data['is_mobile'] = $is_mobile;
            $data['is_robot'] = $is_robot;
            $data['is_browser'] = $is_browser;
            $data['is_platform'] = $is_platform;

            $this->load->model("mdl_agenda");
            $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);
                
                
		$this->load->view('front/vwMentionslegalesmobile', $data) ;
	}
        
	function nousSituerCommercant($_iCommercantId){
    
        /*
        // Load the library
        $this->load->library('googlemaps');
        // Initialize our map. Here you can also pass in additional parameters for customising the map (see below)
        $this->googlemaps->initialize();
        // Create the map. This will return the Javascript to be included in our pages <head></head> section and the HTML code to be
        // placed where we want the map to appear.
        // Set the marker parameters as an empty array. Especially important if we are using multiple markers
        $marker = array();
        // Specify an address or lat/long for where the marker should appear.
        //$marker['position '] = 'Crescent Park, Palo Alto';
        $zExemple = $marker['position '] = 'Crescent Park, Palo Alto';
        print_r($zExemple) ;
        // $marker['position'] = 'France, France, France';
        // Once all the marker parameters have been specified lets add the marker to our map
        $this->googlemaps->add_marker($marker);
        $data['map'] = $this->googlemaps->create_map();
        // Load our view, passing the map data that has just been created
        //$this->load->view('my_view', $data);
        */

        $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
        $data['mdlville'] = $this->mdlville ;
        $data['oInfoCommercant'] = $oInfoCommercant ;

        $data['active_link'] = "situer";
        $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;


        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user") ;
        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser=0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId) ;
        $data['toBonPlan'] = $toBonPlan ;


        //$this->load->view('front/vwNousSituerCommercant', $data) ;
        $this->load->view('front2013/noussituercommercant', $data) ;
	}
        
        
        function coordonneeshoraires($_iCommercantId){
            
            $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
            $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);
            
            $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
            $data['mdlville'] = $this->mdlville ;
            $data['oInfoCommercant'] = $oInfoCommercant ;

            $data['active_link'] = "accueil";
            $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
            if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
            $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
            $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
            if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;
            
            $is_mobile = $this->agent->is_mobile();
            //test ipad user agent
            $is_mobile_ipad = $this->agent->is_mobile('ipad');
            $data['is_mobile_ipad'] = $is_mobile_ipad;
            $is_robot = $this->agent->is_robot();
            $is_browser = $this->agent->is_browser();
            $is_platform = $this->agent->platform();
            $data['is_mobile'] = $is_mobile;
            $data['is_robot'] = $is_robot;
            $data['is_browser'] = $is_browser;
            $data['is_platform'] = $is_platform;

            $this->load->model("mdl_agenda");
            $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

            $this->load->view('front2013/coordonneeshoraires', $data) ;
        }

        function invalid_account() {
            $data['title'] = "Compte invalide";
            $data["zTitle"] = 'Erreur, Compte invalide';
            $this->load->view('privicarte/invalid_account', $data) ;
        }
        
        
        function presentation($_iCommercantId) {
            $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
            $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

            //var_dump($_iCommercantId); die("STOP");
            if (!isset($_iCommercantId) || $_iCommercantId == NULL || $_iCommercantId == "") redirect("front/commercant/invalid_account/");

            $oInfoCommercant = $this->mdlcommercant->infoCommercant(intval($_iCommercantId)) ;
            $nbPhoto =0;
            if($oInfoCommercant->Photo1!="" && $oInfoCommercant->Photo1!=null){
                $nbPhoto+=1;
            }
            if($oInfoCommercant->Photo2!="" && $oInfoCommercant->Photo2!=null){
                $nbPhoto+=1;
            }
            if($oInfoCommercant->Photo3!="" && $oInfoCommercant->Photo3!=null){
                $nbPhoto+=1;
            }
            if($oInfoCommercant->Photo4!="" && $oInfoCommercant->Photo4!=null){
                $nbPhoto+=1;
            }
            if($oInfoCommercant->Photo5!="" && $oInfoCommercant->Photo5!=null){
                $nbPhoto+=1;
            }

            $data['oInfoCommercant'] = $oInfoCommercant ;
            $data['nbPhoto'] = $nbPhoto ;

            //$data['mdlannonce'] = $this->mdlannonce ;
            $data['mdlbonplan'] = $this->mdlbonplan ;

            $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
            $oBonPlan = 	$this->mdlbonplan->bonPlanParCommercant($_iCommercantId);
            $data['nbBonPlan'] = sizeof($oBonPlan);
            //		$data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_titre : "";		// OP 27/11/2011
            $data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_texte : "";
            $data['oImagespub'] = $this->mdlimagespub->GetByImagespubActiv() ;
            $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

            $this->load->model("mdl_agenda");
            $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

            $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
            if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

            $data['active_link'] = "presentation";

            $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
            if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);

            $is_mobile = $this->agent->is_mobile();
            //test ipad user agent
            $is_mobile_ipad = $this->agent->is_mobile('ipad');
            $data['is_mobile_ipad'] = $is_mobile_ipad;
            $is_robot = $this->agent->is_robot();
            $is_browser = $this->agent->is_browser();
            $is_platform = $this->agent->platform();
            $data['is_mobile'] = $is_mobile;
            $data['is_robot'] = $is_robot;
            $data['is_browser'] = $is_browser;
            $data['is_platform'] = $is_platform;

            $this->load->model("user") ;
            if ($this->ion_auth->logged_in()){
                $user_ion_auth = $this->ion_auth->user()->row();
                $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                if ($iduser==null || $iduser==0 || $iduser==""){
                    $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                }
            } else $iduser=0;
            $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));
            $data['pagecategory'] = "pro";
            $data['link_partner_current_page'] = 'presentation';

            
            $data['pageglissiere'] = "presentation";
            $this->load->model("mdlglissiere");
            $data['mdlglissiere'] = $this->mdlglissiere;

            $data['current_partner_menu'] = "presentation";

            $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId) ;
            $data['toBonPlan'] = $toBonPlan ;
            
            //ajout bouton rond noire annonce/agenda
            $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
            if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
            else $data['result_check_commercant_annonce'] = '0';
			
			 $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
            if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
            else $data['result_check_commercant_agenda'] = '0';
		

            //$this->load->view('front/vwPresentationCommercant', $data);
            //$this->load->view('front2013/presentationcommercant', $data);
            //$this->load->view('privicarte/presentationcommercant', $data);

            $http_code = 200;
            header('Content-type: json');
            header('HTTP/1.1: ' . $http_code);
            header('Status: ' . $http_code);
            exit(json_encode($data));

        }
        
	function listeBonPlanParCommercant($_iCommercantId, $bonplan_id = 0, $mssg = 0){
            
        $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        if ($this->ion_auth->logged_in()){
        $user_ion_auth = $this->ion_auth->user()->row();
        $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
        if ($iduser==null || $iduser==0 || $iduser==""){
        $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        }
        $data['id_client'] = $iduser ;
        }

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
        $data['oInfoCommercant'] = $oInfoCommercant ;
        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId) ;
        $data['toBonPlan'] = $toBonPlan ;

        

        if ($bonplan_id == 0) {
            $oBonPlan2 = $this->mdlbonplan->lastBonplanCom2($_iCommercantId);    
        } else {
            $oBonPlan2 = $this->mdlbonplan->getById($bonplan_id);
        }
        $data['oBonPlan'] = $oBonPlan2;
        $data['toBonPlan2'] = $oBonPlan2;



        $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oBonPlan = 	$this->mdlbonplan->bonPlanParCommercant($_iCommercantId);
        $data['nbBonPlan'] = sizeof($oBonPlan);

        $data['active_link'] = "bonplans";
        $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user") ;
        if ($this->ion_auth->logged_in()){
        $user_ion_auth = $this->ion_auth->user()->row();
        $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
        if ($iduser==null || $iduser==0 || $iduser==""){
        $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        }
        } else $iduser=0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));

        //lien pour retour automatique
        $page_from = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $_SESSION['page_from'] = $page_from;
        //lien pour retour automatique

        $data['mssg'] = $mssg;
        $data['pagecategory_partner'] = 'bonplans_partner';


        if (isset($_POST['text_mail_form_module_detailbonnplan'])) {
            $text_mail_form_module_detailbonnplan = $this->input->post("text_mail_form_module_detailbonnplan") ;
            $nom_mail_form_module_detailbonnplan = $this->input->post("nom_mail_form_module_detailbonnplan") ;
            $tel_mail_form_module_detailbonnplan = $this->input->post("tel_mail_form_module_detailbonnplan") ;
            $email_mail_form_module_detailbonnplan = $this->input->post("email_mail_form_module_detailbonnplan") ;

            $colDestAdmin = array();
            $colDestAdmin[] = array("Email"=>$oInfoCommercant->Email,"Name"=>$oInfoCommercant->Nom." ".$oInfoCommercant->Prenom);

            // Sujet
            $txtSujetAdmin = "Demande d'information sur un Bonplan Privicarte";

            $txtContenuAdmin = "
            <p>Bonjour ,</p>
            <p>Une demande d'information vous est adressé suite au bon plan que vous avez déposé sur Privicarte.fr</p>
            <p>Détails du bonplan<br/>
            Désignation : ".$oBonPlan2->bonplan_titre."<br/>
            N° : ".ajoutZeroPourString($oBonPlan2->bonplan_id, 6)." du ".convertDateWithSlashes($oBonPlan2->bonplan_date_debut)."
            </p><p>
            Nom Client : ".$nom_mail_form_module_detailbonnplan."<br/>
            Téléphone Client : ".$tel_mail_form_module_detailbonnplan."<br/>
            Email Client : ".$email_mail_form_module_detailbonnplan."<br/><br/>
            Demande Client :<br/>
            ".$text_mail_form_module_detailbonnplan."<br/><br/>
            </p>";

            @envoi_notification($colDestAdmin,$txtSujetAdmin,$txtContenuAdmin);
            $data['mssg_envoi_module_detail_bonplan'] = '<font color="#00CC00">Votre demande est envoyée</font>';
            //$data['mssg_envoi_module_detail_bonplan'] = $txtContenuAdmin;

        } else $data['mssg_envoi_module_detail_bonplan'] = '';

        $data['pagecategory_partner'] = 'bonplans_partner';
        
        $data['pagecategory'] = "pro";

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $data['link_partner_current_page'] = 'bonplan';

        $data['current_partner_menu'] = "bonplan";
        

		//ajout bouton rond noire annonce/agenda
            $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
            if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
            else $data['result_check_commercant_annonce'] = '0';
			
			 $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
            if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
            else $data['result_check_commercant_agenda'] = '0';


        if ($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $this->load->model("assoc_client_bonplan_model") ;
            $user_ion_auth_verif = $this->ion_auth->user()->row();
            $iduser_verif = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth_verif->id);
            $data['bonplan_unique_verification'] = $this->assoc_client_bonplan_model->get_where(array('assoc_client_bonplan.id_client'=>$iduser_verif,'assoc_client_bonplan.id_bonplan'=>$bonplan_id));
        }
		
        
		//$this->load->view('front/vwBonPlanParCommercant', $data) ;
        //$this->load->view('front2013/page_bonplan_partenaire', $data) ;
        $this->load->view('privicarte/page_bonplan_partenaire', $data) ;
	}
	function recommanderAmi($_iCommercantId){
            
        $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant

        redirect(site_url($nom_url_commercant));

        if ($nom_url_commercant!="agenda" && $nom_url_commercant!="article") {

            $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

            $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
            $data['oInfoCommercant'] = $oInfoCommercant ;


            $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId) ;
            $data['toBonPlan'] = $toBonPlan ;

            $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
            $oBonPlan = 	$this->mdlbonplan->bonPlanParCommercant($_iCommercantId);
            $data['nbBonPlan'] = sizeof($oBonPlan);

            $data['active_link'] = "accueil";
            $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
            if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
            $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
            $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
            if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

            $is_mobile = $this->agent->is_mobile();
            //test ipad user agent
            $is_mobile_ipad = $this->agent->is_mobile('ipad');
            $data['is_mobile_ipad'] = $is_mobile_ipad;
            $is_robot = $this->agent->is_robot();
            $is_browser = $this->agent->is_browser();
            $is_platform = $this->agent->platform();
            $data['is_mobile'] = $is_mobile;
            $data['is_robot'] = $is_robot;
            $data['is_browser'] = $is_browser;
            $data['is_platform'] = $is_platform;

            $this->load->model("user") ;
            if ($this->ion_auth->logged_in()){
                $user_ion_auth = $this->ion_auth->user()->row();
                $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                if ($iduser==null || $iduser==0 || $iduser==""){
                    $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                }
            } else $iduser=0;
            $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));

            $this->load->model("mdl_agenda");
            $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

            //$this->load->view('front/vwRecommanderAmi', $data) ;
            $this->load->view('front2013/recommanderami', $data) ;

        } else if ($nom_url_commercant=="agenda") {
            $idAgenda = $this->uri->rsegment(4);
            $this->load->model("mdl_agenda");
            $oAgenda = $this->mdl_agenda->GetById($idAgenda);
            if (isset($oAgenda)) $data['IdCommercant'] = $oAgenda->IdCommercant; else $data['IdCommercant'] = "0";
            $data['idAgenda'] = $idAgenda;
            $this->load->view('frontAout2013/recommanderami', $data) ;
        } else if ($nom_url_commercant=="article") {
            $idAgenda = $this->uri->rsegment(4);
            $this->load->model("mdlarticle");
            $oAgenda = $this->mdlarticle->GetById($idAgenda);
            if (isset($oAgenda)) $data['IdCommercant'] = $oAgenda->IdCommercant; else $data['IdCommercant'] = "0";
            $data['idAgenda'] = $idAgenda;
            $this->load->view('sortez_mobile/recommanderami', $data) ;
        }

	}
	function envoiMailRecommander($_iCommercantId){
        $_iCommercantId = $this->uri->rsegment(3);
        $_iAgenda = $this->uri->rsegment(4);
		$oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
		/*$config = array();
		$config[ 'protocol' ] = 'mail' ;
		$config[ 'mailtype' ] = 'html' ;
		$config[ 'charset' ] = 'utf-8' ;*/
		$this->load->library('email');
		
                $errorMail    = "";

                $mail_to      = $_POST['zEmail'] ;
                //$mail_cc = $_POST['zMailTo']  ;
                $mail_to_name = "Privicarte";
                $mail_subject = "[Privicarte] Recommander " . $oInfoCommercant->NomSociete ;
                $mail_expediteur = $_POST['zNom'] ;
                $data = array() ;
                $data["zNom"] = $_POST['zNom'] ;
                $data["zTelephone"] = $_POST['zTelephone'] ;
                $data["zCommentaire"] = $_POST['zCommentaire'] ;
                //$mail_body = "Ceci est un mail de Recommandation \n\n";
                //$mail_body = $this->load->view("front/vwContenuMailNousContacterAnnonce", $data, true) ;
                $mail_body = '
Bonjour,


Une demande de contact a été formmulée provenant de Privicarte.fr

Expediteur : '.$_POST['zNom'].'

Nom : '.$_POST['zTelephone'].'


'.$_POST['zCommentaire'].'
';

        $this->email->from($mail_expediteur, "Privicarte");
		$this->email->to($mail_to);
		//$this->email->cc($mail_cc);
        $this->email->bcc("randawilly@gmail.com");
		$this->email->subject($mail_subject);
        //$this->email->attach($mail_Document);
		$this->email->message($mail_body);
  		
		
                
        $data['oInfoCommercant'] = $oInfoCommercant ;
        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId) ;
        $data['toBonPlan'] = $toBonPlan ;

        $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oBonPlan = 	$this->mdlbonplan->bonPlanParCommercant($_iCommercantId);
        $data['nbBonPlan'] = sizeof($oBonPlan);

        $data['active_link'] = "accueil";
        $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user") ;
        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser=0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));


        if ($this->email->send()) {
            //echo "Le mail est bien envoyé !" ;



            //$this->load->view('front/vwRecommanderAmiSucces', $data) ;
            if (isset($_iAgenda) && $_iAgenda=="agenda") {
                $this->load->view('frontAout2013/recommanderamisucces', $data) ;
            } else {
                $this->load->view('front2013/recommanderamisucces', $data) ;
            }

        } else {
            //echo "Un problème est survenu, veuillez renvoyer le mail !" ;
            //$this->load->view('front/vwRecommanderAmiErreur', $data) ;

            if (isset($_iAgenda) && $_iAgenda=="agenda") {
                $this->load->view('frontAout2013/recommanderamierreur', $data) ;
            } else {
                $this->load->view('front2013/recommanderamierreur', $data) ;
            }
        }
	}




    function manage_nbrevisites($_iCommercantId){

        //$_iCommercantId = $this->uri->rsegment(3);



        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;


        $nbvisite = intval($oInfoCommercant->nbrevisites);
        $nbvisite_nv = $nbvisite + 1;

        //$oInfoCommercant['nbrevisites'] = $nbvisite;


        $this->mdlcommercant->Update_nbrevisites_commercant($_iCommercantId, $nbvisite_nv);

        echo $nbvisite_nv;

    }





}