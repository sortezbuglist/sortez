<?php



class Plat_du_jour extends CI_Controller

{

    function __construct()

    {

        parent::__construct();

        $this->load->library('session');

        $this->load->model("user");

        $this->load->library('user_agent');

        $this->load->library('ion_auth');

        $this->load->model("ion_auth_used_by_club");

        $this->load->model("commercant");

        $this->load->model("mdlcommercant");

        $this->load->model("mdl_plat_du_jour");

        $this->load->model("mdlbonplan");

        $this->load->model("mdlannonce");

        $this->load->model("mdlcategorie");

        $this->load->model("mdlimagespub");

        $this->load->model("mdlarticle");

        $this->load->model("mdlcommercantpagination");

        $this->load->model("mdldepartement");

        $this->load->model("mdl_localisation");

        $this->load->library("pagination");

        check_vivresaville_id_ville();



    }

    public function Index(){

        $this->liste_plat();

    }

    public function liste(){



        if ($this->ion_auth->logged_in()) {

            $user_ion_auth = $this->ion_auth->user()->row();

            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

            if ($iduser == null || $iduser == 0 || $iduser == "") {

                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

            }

            $data['IdCommercant'] = $iduser;

            $data['plat']=$this->Mdl_plat_du_jour->getbyIdcommercant($iduser);

            $this->load->view('plat_du_jour/liste_plat_du_jour',$data);

        }else{

            redirect('auth/login');

        }

    }

    public function fiche(){

        if ($this->ion_auth->logged_in()) {

            $user_ion_auth = $this->ion_auth->user()->row();

            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

            if ($iduser == null || $iduser == 0 || $iduser == "") {

                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

            }

            $data['IdCommercant'] = $iduser;

            $data['oInfoCommercant']=$this->commercant->GetById($iduser);

            $this->load->view('plat_du_jour/vwfiche_plat',$data);

        }else{

            redirect('auth/login');

        }

    }



    public function save_plat(){



    }

    function liste_plat(){

        //var_dump($_POST);die();

        $data['infos'] = null;



        /*if ($this->ion_auth->is_admin()) {

            $this->session->set_flashdata('domain_from', '1');

            redirect("admin/home");

        }*/



        ////////////////DELETE OLD AGENDA date_fin past 8 days

        ///

        $this->mdl_plat_du_jour->deleteOldplat_fin8jours();



        $nom_url_commercant = $this->uri->rsegment(2);

        //var_dump($nom_url_commercant);die();

        //////$this->firephp->log($_POST, 'POST');

        if (isset($nom_url_commercant) && $nom_url_commercant != "" && !is_numeric($nom_url_commercant)) {

            $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

            //var_dump($_iCommercantId);die();

            $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);

            //var_dump($oInfoCommercant);die();

            $data['_iCommercantId'] = $oInfoCommercant->IdCommercant;

            //var_dump($data['_iCommercantId']);die();

            $data['oInfoCommercant'] = $oInfoCommercant;

            //////$this->firephp->log($oInfoCommercant, 'oInfoCommercant');



            //$data['mdlannonce'] = $this->mdlannonce ;

            $data['mdlbonplan'] = $this->mdlbonplan;



            $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

            $oBonPlan = $this->mdlbonplan->bonPlanParCommercant($_iCommercantId);

            $data['nbBonPlan'] = sizeof($oBonPlan);

            //		$data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_titre : "";		// OP 27/11/2011

            $data['sTitreBonPlan'] = (sizeof($oBonPlan) > 0) ? $oBonPlan->bonplan_texte : "";

            $data['oImagespub'] = $this->mdlimagespub->GetByImagespubActiv();

            $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

//            $data['photocom_default']=$this->mdl_plat_du_jour->get_photocom($_iCommercantId);

            $oLastbonplanCom = $this->mdlbonplan->lastBonplanCom($_iCommercantId);

            if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        }



        $argOffset = (isset($_SESSION["argOffset"])) ? $_SESSION["argOffset"] : 0;

        //$argOffset = $_iPage ;

        if (preg_match("/admin/", $_SERVER["PHP_SELF"])) {

            redirect("admin/home");

        } else {



            //if the link doesn't come from page navigation, clear category session

            /*$current_URI_club = $_SERVER['REQUEST_URI'];

            $current_URI_club_array = explode("accueil/index", $current_URI_club);

            ////$this->firephp->log(count($current_URI_club_array), 'nb_array');

            if (count($current_URI_club_array)==1) {

                $this->session->unset_userdata('iCategorieId_x');

                $this->session->unset_userdata('iVilleId_x');

                $this->session->unset_userdata('zMotCle_x');

                $this->session->unset_userdata('iOrderBy_x');

                $this->session->unset_userdata('inputStringQuandHidden_x');

                $this->session->unset_userdata('inputStringDatedebutHidden_x');

                $this->session->unset_userdata('inputStringDatefinHidden_x');

                $this->session->unset_userdata('inputGeoLongitude_x');

            }*/

            //if the link doesn't come from page navigation, clear catgory session





            if ($this->input->post("inputStringVilleHidden") == "0") unset($_SESSION['iVilleId']);

            if ($this->input->post("inputStringDepartementHidden") == "") unset($_SESSION['iDepartementId']);

            if ($this->input->post("zMotCle") == "") unset($_SESSION['zMotCle']);

            if ($this->input->post("inputStringOrderByHidden") == "0") unset($_SESSION['iOrderBy']);

            if ($this->input->post("inputStringQuandHidden")) unset($_SESSION['inputStringQuandHidden']);

            if ($this->input->post("inputStringDatedebutHidden")) unset($_SESSION['inputStringDatedebutHidden']);

            if ($this->input->post("inputStringDatefinHidden")) unset($_SESSION['inputStringDatefinHidden']);

            if ($this->input->post("inputIdCommercant")) unset($_SESSION['inputIdCommercant']);

            if ($this->input->post("inputStringidsousrubriqueHidden")) unset($_SESSION['inputStringidsousrubriqueHidden']);





            $toCategorie_principale = $this->mdlcategorie->getcategoriesplat();

            //var_dump($toCategorie_principale);die('koko');

            $data['toCategorie_principale'] = $toCategorie_principale;

            //$TotalRows = $this->mdlcommercantpagination->Compter();

            $PerPage = 12;

            $data["iFavoris"] = "";





            if (isset($_POST["inputStringHidden"])) {

                unset($_SESSION['iVilleId']);

                unset($_SESSION['iDepartementId']);

                unset($_SESSION['zMotCle']);

                unset($_SESSION['iOrderBy']);

                unset($_SESSION['inputStringQuandHidden']);

                unset($_SESSION['inputStringDatedebutHidden']);

                unset($_SESSION['inputStringDatefinHidden']);

                unset($_SESSION['inputIdCommercant']);





                if (isset($_POST["inputStringVilleHidden"])) $iVilleId = $_POST["inputStringVilleHidden"]; else $iVilleId = "0";

                if (isset($_POST["inputStringVilleHidden_sub"])) $iOrderBy = $_POST["inputStringVilleHidden_sub"]; else $iOrderBy = "";

                if (isset($_POST["inputStringDepartementHidden"])) $iDepartementId = $_POST["inputStringDepartementHidden"]; else $iDepartementId = 0;

                if (isset($_POST["inputStringOrderByHidden"])) $iOrderBy = $_POST["inputStringOrderByHidden"]; else $iOrderBy = "";

                if (isset($_POST["zMotCle"])) $zMotCle = $_POST["zMotCle"]; else $zMotCle = '';

                if (isset($_POST["inputStringQuandHidden"])) $inputStringQuandHidden = $_POST["inputStringQuandHidden"]; else $inputStringQuandHidden = "0";

                if (isset($_POST["inputStringDatedebutHidden"])) $inputStringDatedebutHidden = $_POST["inputStringDatedebutHidden"]; else $inputStringDatedebutHidden = "0000-00-00";

                if (isset($_POST["inputStringDatefinHidden"])) $inputStringDatefinHidden = $_POST["inputStringDatefinHidden"]; else $inputStringDatefinHidden = "0000-00-00";

                if (isset($_POST["inputIdCommercant"])) $inputIdCommercant = $_POST["inputIdCommercant"] ; else $inputIdCommercant = "0";

                if (isset($_POST["inputStringidsousrubriqueHidden"])) $inputStringidsousrubriqueHidden = $_POST["inputStringidsousrubriqueHidden"] ; else $inputStringidsousrubriqueHidden = "";



                $rsegment3 = $this->uri->rsegment(3);

                if (isset($rsegment3) && $rsegment3 != "" && !is_numeric($rsegment3)) {

                    $inputIdCommercant = $_iCommercantId;

                } else {

                    if (isset($_POST["inputIdCommercant"])) $inputIdCommercant = $_POST["inputIdCommercant"]; else $inputIdCommercant = "0";

                }





                $_SESSION['iVilleId'] = $iVilleId;

                $this->session->set_userdata('iVilleId_x', $iVilleId);

                $_SESSION['iDepartementId'] = $iDepartementId;

                $this->session->set_userdata('iDepartementId_x', $iDepartementId);

                $_SESSION['zMotCle'] = $zMotCle;

                $this->session->set_userdata('zMotCle_x', $zMotCle);

                $_SESSION['iOrderBy'] = $iOrderBy;

                $this->session->set_userdata('iOrderBy_x', $iOrderBy);

                $_SESSION['inputStringQuandHidden'] = $inputStringQuandHidden;

                $this->session->set_userdata('inputStringQuandHidden_x', $inputStringQuandHidden);

                $_SESSION['inputStringDatedebutHidden'] = $inputStringDatedebutHidden;

                $this->session->set_userdata('inputStringDatedebutHidden_x', $inputStringDatedebutHidden);

                $_SESSION['inputStringDatefinHidden'] = $inputStringDatefinHidden;

                $this->session->set_userdata('inputStringDatefinHidden_x', $inputStringDatefinHidden);

                $_SESSION['inputIdCommercant'] = $inputIdCommercant;

                $this->session->set_userdata('inputIdCommercant_x', $inputIdCommercant);

                $_SESSION['inputStringidsousrubriqueHidden'] = $inputStringidsousrubriqueHidden;

                $this->session->set_userdata('inputStringidsousrubriqueHidden_x', $inputStringidsousrubriqueHidden);

//var_dump($inputIdCommercant);die();



                $data['iVilleId'] = $iVilleId;

                $data['iDepartementId'] = $iDepartementId;

                $data['zMotCle'] = $zMotCle;

                $data['iOrderBy'] = $iOrderBy;

                $data['inputStringQuandHidden'] = $inputStringQuandHidden;

                $data['inputStringDatedebutHidden'] = $inputStringDatedebutHidden;

                $data['inputStringDatefinHidden'] = $inputStringDatefinHidden;

                $data['IdCommercant'] = $inputIdCommercant;

                $data['inputStringidsousrubriqueHidden'] = $inputStringidsousrubriqueHidden;





                $session_iVilleId = $this->session->userdata('iVilleId_x');

                $session_iDepartementId = $this->session->userdata('iDepartementId_x');

                $session_zMotCle = $this->session->userdata('zMotCle_x');

                $session_iOrderBy = $this->session->userdata('iOrderBy_x');

                $session_inputStringQuandHidden = $this->session->userdata('inputStringQuandHidden_x');

                $session_inputStringDatedebutHidden = $this->session->userdata('inputStringDatedebutHidden_x');

                $session_inputStringDatefinHidden = $this->session->userdata('inputStringDatefinHidden_x');

                $session_inputIdCommercant = $this->session->userdata('inputIdCommercant_x');

                $session_inputStringidsousrubriqueHidden = $this->session->userdata('inputStringidsousrubriqueHidden_x');



                ////$this->firephp->log($inputStringQuandHidden, 'inputStringQuandHidden');

                ////$this->firephp->log($session_inputStringQuandHidden, 'session_inputStringQuandHidden');



                //var_dump($_SESSION['inputStringQuandHidden']);die("k o !!!");

                //var_dump($inputStringidsousrubriqueHidden);

                $toPlat_du_jour = $this->mdl_plat_du_jour->listeplatRecherche($_SESSION['iVilleId'], $_SESSION['zMotCle'], $data["iFavoris"], $argOffset, $PerPage, $_SESSION['iOrderBy'], $_SESSION['inputStringQuandHidden'], $_SESSION['inputStringDatedebutHidden'], $_SESSION['inputStringDatefinHidden'],$_SESSION['inputIdCommercant'],$_SESSION['inputStringidsousrubriqueHidden']) ;

                log_message('error', 'william TotalRows 1 : ');

                $TotalRows = count($this->mdl_plat_du_jour->listeplatRecherche( $session_iVilleId, $session_iDepartementId, $session_zMotCle,  0, 10000, $session_iOrderBy, $session_inputStringQuandHidden, $session_inputStringDatedebutHidden, $session_inputStringDatefinHidden, $session_inputIdCommercant,$session_inputStringidsousrubriqueHidden));



                $config_pagination = array();

                $rsegment3 = $this->uri->rsegment(3);



                if (strpos($rsegment3, '&content_only_list') !== false) {

                    $pieces_rseg = explode("&content_only_list", $rsegment3);

                    $rsegment3 = $pieces_rseg[0];

                }



                if (isset($rsegment3) && $rsegment3 != "" && is_numeric($rsegment3)) {

                    $config_pagination["base_url"] = base_url() . "plat_du_jour/liste_plat/" . $rsegment3;

                } else {

                    $config_pagination["base_url"] = base_url() . "plat_du_jour/liste_plat/";

                }

                $config_pagination["total_rows"] = $TotalRows;

                $config_pagination["per_page"] = $PerPage;

                $config_pagination["uri_segment"] = 3;

                $config_pagination['first_link'] = 'Première page';

                $config_pagination['last_link'] = 'Dernière page';

                $config_pagination['prev_link'] = 'Précédent';

                $config_pagination['next_link'] = 'Suivant';

                $this->pagination->initialize($config_pagination);

                if (isset($rsegment3) && $rsegment3 != "" && is_numeric($rsegment3)) {

                    $page_pagination = ($rsegment3) ? $rsegment3 : 0;

                } else {

                    $page_pagination = ($rsegment3) ? $rsegment3 : 0;

                }

                $toCommercant = $this->mdl_plat_du_jour->listeplatRecherche($_SESSION['iVilleId'], $_SESSION['zMotCle'], $data["iFavoris"], $page_pagination, $config_pagination["per_page"], $_SESSION['iOrderBy'], $_SESSION['inputStringQuandHidden'], $_SESSION['inputStringDatedebutHidden'], $_SESSION['inputStringDatefinHidden'],$_SESSION['inputIdCommercant'],$_SESSION['inputStringidsousrubriqueHidden']) ;

                $data['tocommercant']=$this->mdl_plat_du_jour->listeplatRecherche_commercant_list( $session_iVilleId, $session_iDepartementId, $session_zMotCle,  $page_pagination, $config_pagination["per_page"], $session_iOrderBy, $session_inputStringQuandHidden, $session_inputStringDatedebutHidden, $session_inputStringDatefinHidden, $session_inputIdCommercant);



                $toPlat_du_jour = $this->mdl_plat_du_jour->listeplatRecherche( $session_iVilleId, $session_iDepartementId, $session_zMotCle,  $page_pagination, $config_pagination["per_page"], $session_iOrderBy, $session_inputStringQuandHidden, $session_inputStringDatedebutHidden, $session_inputStringDatefinHidden, $session_inputIdCommercant,$session_inputStringidsousrubriqueHidden);

                log_message('error', 'william toPlat_du_jour 1 : ');

                $data["links_pagination"] = $this->pagination->create_links();



            } else {

                $data["iFavoris"] = "";



                $session_iVilleId = $this->session->userdata('iVilleId_x');

                $session_iDepartementId = $this->session->userdata('iDepartementId_x');

                $session_zMotCle = $this->session->userdata('zMotCle_x');

                $session_iOrderBy = $this->session->userdata('iOrderBy_x');

                $session_inputStringQuandHidden = $this->session->userdata('inputStringQuandHidden_x');

                $session_inputStringDatedebutHidden = $this->session->userdata('inputStringDatedebutHidden_x');

                $session_inputStringDatefinHidden = $this->session->userdata('inputStringDatefinHidden_x');

                $session_inputIdCommercant = $this->session->userdata('inputIdCommercant_x');

                $session_inputStringidsousrubriqueHidden = $this->session->userdata('inputStringidsousrubriqueHidden_x');





                $iVilleId = (isset($session_iVilleId)) ? $session_iVilleId : "0";

                $iDepartementId = (isset($session_iDepartementId)) ? $session_iDepartementId : 0;

                $zMotCle = (isset($session_zMotCle)) ? $session_zMotCle : "";

                $iOrderBy = (isset($session_iOrderBy)) ? $session_iOrderBy : "0";

                $inputStringQuandHidden = (isset($session_inputStringQuandHidden)) ? $session_inputStringQuandHidden : "0";

                $inputStringDatedebutHidden = (isset($session_inputStringDatedebutHidden)) ? $session_inputStringDatedebutHidden : "0000-00-00";

                $inputStringDatefinHidden = (isset($session_inputStringDatefinHidden)) ? $session_inputStringDatefinHidden : "0000-00-00";

                $inputIdCommercant = (isset($session_inputIdCommercant)) ? $session_inputIdCommercant : "0" ;

                $inputStringidsousrubriqueHidden = (isset($session_inputStringidsousrubriqueHidden)) ? $session_inputStringidsousrubriqueHidden : "0" ;

                $rsegment3 = $this->uri->rsegment(3);

                if (isset($rsegment3) && $rsegment3 != "" && !is_numeric($rsegment3)) {

                    $inputIdCommercant = $_iCommercantId;

                } else {

                    $inputIdCommercant = (isset($session_inputIdCommercant)) ? $session_inputIdCommercant : "0";

                }

                $toPlat_du_jour = $this->mdl_plat_du_jour->listeplatRecherche( $iVilleId, $iDepartementId, $zMotCle,  0, 10000, $iOrderBy, $inputStringQuandHidden, $inputStringDatedebutHidden, $inputStringDatefinHidden, $inputIdCommercant,$inputStringidsousrubriqueHidden) ;

                //var_dump($iVilleId);die();

                log_message('error', 'william TotalRows 2 : ');

                $TotalRows = count($this->mdl_plat_du_jour->listeplatRecherche( $iVilleId, $iDepartementId, $zMotCle,  0, 10000, $iOrderBy, $inputStringQuandHidden, $inputStringDatedebutHidden, $inputStringDatefinHidden, $inputIdCommercant,$inputStringidsousrubriqueHidden));

                //var_dump($inputStringQuandHidden);die();

                $config_pagination = array();

                $rsegment3 = $this->uri->rsegment(3);



                if (strpos($rsegment3, '&content_only_list') !== false) {

                    $pieces_rseg = explode("&content_only_list", $rsegment3);

                    $rsegment3 = $pieces_rseg[0];

                }

                //die($rsegment3);



                if (isset($rsegment3) && $rsegment3 != "" && is_numeric($rsegment3)) {

                    $config_pagination["base_url"] = base_url() . "plat_du_jour/liste/" . $rsegment3;

                } else {

                    $config_pagination["base_url"] = base_url() . "plat_du_jour/liste/";

                }

                $config_pagination["total_rows"] = $TotalRows;

                $config_pagination["per_page"] = $PerPage;

                $config_pagination["uri_segment"] = 3;

                $config_pagination['first_link'] = 'Première page';

                $config_pagination['last_link'] = 'Dernière page';

                $config_pagination['prev_link'] = 'Précédent';

                $config_pagination['next_link'] = 'Suivant';

                $this->pagination->initialize($config_pagination);



                if (isset($rsegment3) && $rsegment3 != "" && is_numeric($rsegment3)) {

                    $page_pagination = ($rsegment3) ? $rsegment3 : 0;

                } else {

                    $page_pagination = ($rsegment3) ? $rsegment3 : 0;

                }

                $data['tocommercant']=$this->mdl_plat_du_jour->listeplatRecherche_commercant_list( $session_iVilleId, $session_iDepartementId, $session_zMotCle,  $page_pagination, $config_pagination["per_page"], $session_iOrderBy, $session_inputStringQuandHidden, $session_inputStringDatedebutHidden, $session_inputStringDatefinHidden, $session_inputIdCommercant,$session_inputStringidsousrubriqueHidden);

                //var_dump($data['tocommercant']);die('koko');

                $toPlat_du_jour = $this->mdl_plat_du_jour->listeplatRecherche( $session_iVilleId, $session_iDepartementId, $session_zMotCle,  $page_pagination, $config_pagination["per_page"], $session_iOrderBy, $session_inputStringQuandHidden, $session_inputStringDatedebutHidden, $session_inputStringDatefinHidden, $session_inputIdCommercant,$session_inputStringidsousrubriqueHidden);

                log_message('error', 'william toPlat_du_jour 2 : ');

                $data["links_pagination"] = $this->pagination->create_links();

            }



            ////$this->firephp->log($_SERVER['REQUEST_URI'], 'PATH_INFO');



            $iNombreLiens = $TotalRows / $PerPage;

            if ($iNombreLiens > round($iNombreLiens)) {

                $iNombreLiens = round($iNombreLiens) + 1;

            } else {

                $iNombreLiens = round($iNombreLiens);

            }

            //////////////////////////////////



            $data["iNombreLiens"] = $iNombreLiens;



            $data["PerPage"] = $PerPage;

            $data["TotalRows"] = $TotalRows;

            $data["argOffset"] = $argOffset;

            //$toCommercant= $this->mdlcommercantpagination->GetListeCommercantPagination($PerPage, $argOffset);

            $data['toPlat_du_jour'] = $toPlat_du_jour;//william hack

            // print_r($data['toCommercant']);exit();

            //$this->load->view('front/vwAccueil', $data) ;

            $data['pagecategory'] = 'plat_du_jour';





            $departement_check = $this->session->userdata('iDepartementId_x');



            //get ville list of article***********************************************************************************************************

                /*if (isset($departement_check) && $departement_check != "" && $departement_check != null) {

                    $toVille = $this->mdlville->GetplatVillesByIdCommercant_by_departement($_iCommercantId, $this->session->userdata('iDepartementId_x'));

                    $data['toVille'] = $toVille;

                } else{

                    if ($_iCommercantId !=null AND $_iCommercantId !='' AND $_iCommercantId !=0){

                        $toVille = $this->mdlville->GetplatVillesByIdCommercant_by_departement($_iCommercantId);

                        $data['toVille'] = $toVille;

                    }else{*/

                    $toVille = $this->mdlville->GetplatVillesByIdCommercant_by_departement($_iCommercantId,$departement_id = "0");

                    $data['toVille'] = $toVille;

                    /*}

                }*/

            //var_dump($toVille);die();

            //get ville list of article************************************************************************************************************



            $toDepartement = $this->mdldepartement->GetplatDepartements_pvc();

            //var_dump($toDepartement);die();

            $data['toDepartement'] = $toDepartement;



            $data["mdl_localisation"] = $this->mdl_localisation;

            $data["mdlville"] = $this->mdlville;





            $is_mobile = $this->agent->is_mobile();

            //test ipad user agent

            $is_mobile_ipad = $this->agent->is_mobile('ipad');

            $data['is_mobile_ipad'] = $is_mobile_ipad;

            $is_robot = $this->agent->is_robot();

            $is_browser = $this->agent->is_browser();

            $is_platform = $this->agent->platform();

            $data['is_mobile'] = $is_mobile;

            $data['is_robot'] = $is_robot;

            $data['is_browser'] = $is_browser;

            $data['is_platform'] = $is_platform;





            if ($this->ion_auth->logged_in()) {

                $user_ion_auth = $this->ion_auth->user()->row();

                $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

                if ($iduser == null || $iduser == 0 || $iduser == "") {

                    $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

                }

                $data['IdUser'] = $iduser;

            }



            ////$this->firephp->log($_REQUEST, '_REQUEST');





            if (!isset($session_iVilleId)) $session_iVilleId_to_count = 0; else $session_iVilleId_to_count = $session_iVilleId;

            //var_dump($session_iVilleId_to_count);die();

            if (!isset($session_iDepartementId)) $session_iDepartementId_to_count = 0; else $session_iDepartementId_to_count = $session_iDepartementId;

            if (!isset($session_zMotCle)) $session_zMotCle_to_count = ""; else $session_zMotCle_to_count = $session_zMotCle;

            if (!isset($session_iOrderBy)) $session_iOrderBy_to_count = ""; else $session_iOrderBy_to_count = $session_iOrderBy;

            $session_inputStringDatedebutHidden_to_count = "0000-00-00";

            $session_inputStringDatefinHidden_to_count = "0000-00-00";

            $session_inputIdCommercant_to_count = "0";

            $session_inputStringidsousrubriqueHidden_to_count = "0";



            $data['toPlatTout_global'] = count($this->mdl_plat_du_jour->listeplatRecherche( $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count,  0, 10000, $session_iOrderBy_to_count, "0", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,$session_inputStringidsousrubriqueHidden_to_count));

            //var_dump($data['toPlatTout_global']);die();

            $data['toPlatAujourdhui_global'] = count($this->mdl_plat_du_jour->listeplatRecherche( $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count,  0, 10000, $session_iOrderBy_to_count, "101", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,$session_inputStringidsousrubriqueHidden_to_count));

            //var_dump($data['toPlatAujourdhui_global']);

            $data['toPlatWeekend_global'] = count($this->mdl_plat_du_jour->listeplatRecherche( $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count,  0, 10000, $session_iOrderBy_to_count, "202", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,$session_inputStringidsousrubriqueHidden_to_count));

            $data['toPlatSemaine_global'] = count($this->mdl_plat_du_jour->listeplatRecherche( $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count,  0, 10000, $session_iOrderBy_to_count, "303", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,$session_inputStringidsousrubriqueHidden_to_count));

            $data['toPlatSemproch_global'] = count($this->mdl_plat_du_jour->listeplatRecherche( $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count,  0, 10000, $session_iOrderBy_to_count, "404", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,$session_inputStringidsousrubriqueHidden_to_count));

            $data['toPlatMois_global'] = count($this->mdl_plat_du_jour->listeplatRecherche( $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count,  0, 10000, $session_iOrderBy_to_count, "505", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,$session_inputStringidsousrubriqueHidden_to_count));

            //var_dump($data['toPlatMois_global']);die();

            $data['toPlatJanvier_global'] = count($this->mdl_plat_du_jour->listeplatRecherche( $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count,  0, 10000, $session_iOrderBy_to_count, "01", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,$session_inputStringidsousrubriqueHidden_to_count));

            $data['toPlatFevrier_global'] = count($this->mdl_plat_du_jour->listeplatRecherche( $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, 0, 10000, $session_iOrderBy_to_count, "02", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,$session_inputStringidsousrubriqueHidden_to_count));

            $data['toPlatMars_global'] = count($this->mdl_plat_du_jour->listeplatRecherche( $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count,  0, 10000, $session_iOrderBy_to_count, "03", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,$session_inputStringidsousrubriqueHidden_to_count));

            //var_dump($data['toPlatMars_global']);die();

            $data['toPlatAvril_global'] = count($this->mdl_plat_du_jour->listeplatRecherche( $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count,  0, 10000, $session_iOrderBy_to_count, "04", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,$session_inputStringidsousrubriqueHidden_to_count));

            $data['toPlatMai_global'] = count($this->mdl_plat_du_jour->listeplatRecherche( $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count,  0, 10000, $session_iOrderBy_to_count, "05", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,$session_inputStringidsousrubriqueHidden_to_count));

            $data['toPlatJuin_global'] = count($this->mdl_plat_du_jour->listeplatRecherche( $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count,  0, 10000, $session_iOrderBy_to_count, "06", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,$session_inputStringidsousrubriqueHidden_to_count));

            $data['toPlatJuillet_global'] = count($this->mdl_plat_du_jour->listeplatRecherche( $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count,  0, 10000, $session_iOrderBy_to_count, "07", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,$session_inputStringidsousrubriqueHidden_to_count));

            $data['toPlatAout_global'] = count($this->mdl_plat_du_jour->listeplatRecherche( $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count,  0, 10000, $session_iOrderBy_to_count, "08", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,$session_inputStringidsousrubriqueHidden_to_count));

            $data['toPlatSept_global'] = count($this->mdl_plat_du_jour->listeplatRecherche( $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count,  0, 10000, $session_iOrderBy_to_count, "09", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,$session_inputStringidsousrubriqueHidden_to_count));

            $data['toPlatOct_global'] = count($this->mdl_plat_du_jour->listeplatRecherche( $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count,  0, 10000, $session_iOrderBy_to_count, "10", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,$session_inputStringidsousrubriqueHidden_to_count));

            $data['toPlatNov_global'] = count($this->mdl_plat_du_jour->listeplatRecherche( $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count,  0, 10000, $session_iOrderBy_to_count, "11", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,$session_inputStringidsousrubriqueHidden_to_count));

            $data['toPlatDec_global'] = count($this->mdl_plat_du_jour->listeplatRecherche( $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count,  0, 10000, $session_iOrderBy_to_count, "12", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,$session_inputStringidsousrubriqueHidden_to_count));



            $data['mdlbonplan'] = $this->mdlbonplan;

            $data['mdlannonce'] = $this->mdlannonce;

            $data['Commercant'] = $this->commercant;



            //var_dump($oInfoCommercant->IdCommercant);die();

            //TO NOT REMOVE - for details agenda commercant

            if (isset($_iCommercantId) && isset($oInfoCommercant)) {







                $data['toPlatJanvier'] = $this->mdl_plat_du_jour->GetplatNbByMonth("01", $_iCommercantId);

                $data['toPlatFevrier'] = $this->mdl_plat_du_jour->GetplatNbByMonth("02", $_iCommercantId);

                $data['toPlatMars'] = $this->mdl_plat_du_jour->GetplatNbByMonth("03", $_iCommercantId);

                $data['toPlatAvril'] = $this->mdl_plat_du_jour->GetplatNbByMonth("04", $_iCommercantId);

                $data['toPlatMai'] = $this->mdl_plat_du_jour->GetplatNbByMonth("05", $_iCommercantId);

                $data['toPlatJuin'] = $this->mdl_plat_du_jour->GetplatNbByMonth("06", $_iCommercantId);

                $data['toPlatJuillet'] = $this->mdl_plat_du_jour->GetplatNbByMonth("07", $_iCommercantId);

                $data['toPlatAout'] = $this->mdl_plat_du_jour->GetplatNbByMonth("08", $_iCommercantId);

                $data['toPlatSept'] = $this->mdl_plat_du_jour->GetplatNbByMonth("09", $_iCommercantId);

                $data['toPlatOct'] = $this->mdl_plat_du_jour->GetplatNbByMonth("10", $_iCommercantId);

                $data['toPlatNov'] = $this->mdl_plat_du_jour->GetplatNbByMonth("11", $_iCommercantId);

                $data['toPlatDec'] = $this->mdl_plat_du_jour->GetplatNbByMonth("12", $_iCommercantId);



                $data['nombre_plat_com'] = $this->mdl_plat_du_jour->GetByIdCommercant_platActif($_iCommercantId);



                $data['pagecategory_partner'] = "list_plat";



                $this->load->view('sortez/partner_plat_list', $data);





            } else {



                $this->session->set_userdata('nohome', '1');



                $data["main_menu_content"] = "plat";



                if ($_SERVER['SERVER_NAME'] == DOMAIN_SORTEZ_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_SORTEZ_GLOBAL) {

                    if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {

                        //$this->load->view('sortez_mobile/liste_article', $data) ;

                        $this->load->view('sortez_vsv/plat_index', $data);

                    } else {

                        //$this->load->view('sortez/article', $data) ;

                        // var_dump($data);die("tapitra");

                        $this->load->view('sortez_vsv/plat_index', $data);

                    }

                } elseif ($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL) {

                    $this->load->view("vivresaville/plat_index", $data);

                }





            }



        }



    }

    public function get_count_plat(){

        $TotalRows = count($this->mdl_plat_du_jour->listeplatRecherche($_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "",  $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0",$inputStringidsousrubriqueHidden = ''));

        //var_dump($TotalRows);die('koko');

        echo 'Plats du jour('.$TotalRows.')';

    }

    function details($id_plat)

    {



        $data['current_page'] = "details_plat";

        $toVille = $this->mdlville->GetplatVilles_pvc();//get article list of agenda

        //var_dump($toVille);die();

        $data['toVille'] = $toVille;





        $id_plat = $this->uri->rsegment(3);



        $oDetailPlat = $this->mdl_plat_du_jour->GetById_IsActif($id_plat);

        //var_dump($oDetailPlat);die();

        $data['oDetailPlat'] = $oDetailPlat;



        /*send metadata for opengraph fb & twitter*/

        if (!empty($oDetailPlat)) {

            $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oDetailPlat->IdCommercant);

            if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;

            if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;

            $photoCommercant_path = "application/resources/front/photoCommercant/imagesbank/" . $user_ion_auth_id . "/";

            $photoCommercant_path_old = "application/resources/front/images/article/photoCommercant/";



            $image_home_vignette = "";

            if (isset($oDetailPlat->photo) && $oDetailPlat->photo != "" && is_file($photoCommercant_path . $oDetailPlat->photo) == true) {

                $image_home_vignette = $oDetailPlat->photo;

            }

        }



        //increment "accesscount" on agenda table

        //$this->mdlarticle->Increment_accesscount($oDetailPlat->id);



        //send info commercant to view

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($oDetailPlat->IdCommercant);

        $data['oInfoCommercant'] = $oInfoCommercant;



        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);

        if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;

        if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;

        $data['group_id_commercant_user'] = $group_id_commercant_user;





        //sending mail to event organiser **************************

       if (isset($_POST['text_mail_form_module_detailbonnplan'])) {

           $text_mail_form_module_detailbonnplan = $this->input->post("text_mail_form_module_detailbonnplan");

           $nom_mail_form_module_detailbonnplan = $this->input->post("nom_mail_form_module_detailbonnplan");

           $tel_mail_form_module_detailbonnplan = $this->input->post("tel_mail_form_module_detailbonnplan");

           $email_mail_form_module_detailbonnplan = $this->input->post("email_mail_form_module_detailbonnplan");



           $colDestAdmin = array();

           $colDestAdmin[] = array("Email" => $oDetailPlat->email, "Name" => $oDetailPlat->nom_manifestation . " " . $oDetailPlat->nom_societe);



           // Sujet

           $txtSujetAdmin = "Demande d'information sur un évennement sur Agenda/Club";



           $txtContenuAdmin = "

           <p>Bonjour ,</p>

           <p>Une demande d'information vous est adressé suite à un évennement que vous avez déposé sur le site Plat.</p>

           <p>Détails :<br/>

           Evennement : " . $oDetailPlat->description_plat . "

           <br/>

           Date : " . translate_date_to_fr($oDetailPlat->date_debut_plat) . "<br/>

           Lieu : " . $oDetailPlat->ville . " " . $oDetailPlat->adresse_localisation . " " . $oDetailPlat->codepostal_localisation . "<br/>

           Organisateur : " . $oDetailPlat->organisateur . "<br/><br/>

           Demande Client :<br/>

           " . $text_mail_form_module_detailbonnplan . "<br/><br/>

           Nom client : " . $nom_mail_form_module_detailbonnplan . "<br/>

           Tel client : " . $tel_mail_form_module_detailbonnplan . "<br/>

           Email client : " . $email_mail_form_module_detailbonnplan . "<br/>

           </p>";



           @envoi_notification($colDestAdmin, $txtSujetAdmin, $txtContenuAdmin);

           $data['mssg_envoi_module_detail_bonplan'] = '<font color="#00CC00">Votre demande est envoyée</font>';

           //$data['mssg_envoi_module_detail_bonplan'] = $txtContenuAdmin;



       } else $data['mssg_envoi_module_detail_bonplan'] = '';

        //sending mail to event organiser *******************************



        $is_mobile = $this->agent->is_mobile();

        //test ipad user agent

        $is_mobile_ipad = $this->agent->is_mobile('ipad');

        $data['is_mobile_ipad'] = $is_mobile_ipad;

        $is_robot = $this->agent->is_robot();

        $is_browser = $this->agent->is_browser();

        $is_platform = $this->agent->platform();

        $data['is_mobile'] = $is_mobile;

        $data['is_robot'] = $is_robot;

        $data['is_browser'] = $is_browser;

        $data['is_platform'] = $is_platform;



        $toDepartement = $this->mdldepartement->GetplatDepartements_pvc();

        $data['toDepartement'] = $toDepartement;



        $data["pagecategory"] = "plat";

        $data["main_menu_content"] = "plat";

        $data["mdlbonplan"] = $this->mdlbonplan;



        $data["mdl_localisation"] = $this->mdl_localisation;

        $data["mdlville"] = $this->mdlville;





        $data["pagecategory_partner"] = "plat_partner";









        if (isset($oDetailPlat->id)) {

            if ($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL) {

                $this->load->view('vivresaville/plat_details', $data);

            } else {

                if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {

                    //$this->load->view('privicarte_mobile/agenda_details_desk', $data);

                    //$this->load->view('sortez_mobile/article_details_desk', $data);

                    $this->load->view('sortez_vsv/plat_details', $data);

                } else {

                    //$this->load->view('privicarte/details_event', $data);

                    //$this->load->view('sortez/details_article', $data);

                    $this->load->view('sortez_vsv/plat_details', $data);

                }

            }



        } else {

            redirect('front/utilisateur/no_permission');

        }



    }

    function check_category_list()

    {

//        $toCategorie_principale = $this->mdl_plat_du_jour->listeplatRecherche_sousrubrique_list();

//        var_dump($toCategorie_principale);die('koko');

        $inputStringQuandHidden_partenaires = $this->input->post("iQuand_sess");

        if(!isset($inputStringQuandHidden_partenaires) || $inputStringQuandHidden_partenaires=="" || $inputStringQuandHidden_partenaires=='0'){

            $inputStringQuandHidden_partenaires = $this->session->userdata('inputStringQuandHidden_x');

        }

        $inputStringDatedebutHidden_partenaires = $this->input->post("iDatedebut_sess");

        if(!isset($inputStringDatedebutHidden_partenaires) || $inputStringDatedebutHidden_partenaires=="" || $inputStringDatedebutHidden_partenaires=='0' || $inputStringDatedebutHidden_partenaires=='0000-00-00'){

            $inputStringDatedebutHidden_partenaires = $this->session->userdata('inputStringDatedebutHidden_x');

        }

        $inputStringDatefinHidden_partenaires = $this->input->post("iDatefin_sess");

        if(!isset($inputStringDatefinHidden_partenaires) || $inputStringDatefinHidden_partenaires=="" || $inputStringDatefinHidden_partenaires=='0' || $inputStringDatefinHidden_partenaires=='0000-00-00'){

            $inputStringDatefinHidden_partenaires = $this->session->userdata('inputStringDatefinHidden_x');

        }

        $inputStringVilleHidden_partenaires = $this->input->post("iVilleId_sess");

        if(!isset($inputStringVilleHidden_partenaires) || $inputStringVilleHidden_partenaires=="" || $inputStringVilleHidden_partenaires=='0'){

            $inputStringVilleHidden_partenaires = $this->session->userdata('iVilleId_x');

        }

        $inputStringDepartementHidden_partenaires = $this->input->post("iDepartementId_sess");

        if(!isset($inputStringDepartementHidden_partenaires) || $inputStringDepartementHidden_partenaires=="" || $inputStringDepartementHidden_partenaires=='0'){

            $inputStringDepartementHidden_partenaires = $this->session->userdata('iDepartementId_x');

        }

        $inputStringIdCommercant = $this->input->post("inputIdCommercant");

        if(!isset($inputStringIdCommercant) || $inputStringIdCommercant=="" || $inputStringIdCommercant=='0'){

            $inputStringIdCommercant = $this->session->userdata('inputIdCommercant_x');

        }

        $toCategorie_principale = $this->mdl_plat_du_jour->listeplatRecherche_sousrubrique_list($inputStringDepartementHidden_partenaires, $inputStringVilleHidden_partenaires,$inputStringIdCommercant);

        $result_to_show = '';



        $session_iCategorieId = $this->session->userdata('iCategorieId_x');

        if (isset($session_iCategorieId)) {

            $iCategorieId_sess = $session_iCategorieId;

        }



        $ii_rand = 0;

        //var_dump($toCategorie_principale);die("koko");

        if (isset($toCategorie_principale)) {

            foreach ($toCategorie_principale as $oCategorie_principale) {

                //var_dump($oCategorie_principale);die('koko');

                //var_dump($oCategorie_principale->rubrique);die('koko');

                    if (isset($oCategorie_principale->nbcommercant) && $oCategorie_principale->nbcommercant != 0) {

                        $data['empty'] = null;

                        $data['inputStringIdCommercant']=$inputStringIdCommercant;

                        //$data['oInfoCommercant'] = $oInfoCommercant;

                        $data['oCategorie_principale'] = $oCategorie_principale;

                        $data['ii_rand'] = $ii_rand;

                        if (isset($iCategorieId_sess)) $data['iCategorieId_sess'] = $iCategorieId_sess;

                        $data['inputStringQuandHidden_partenaires'] = $inputStringQuandHidden_partenaires;

                        $data['inputStringDatedebutHidden_partenaires'] = $inputStringDatedebutHidden_partenaires;

                        $data['inputStringDatefinHidden_partenaires'] = $inputStringDatefinHidden_partenaires;

                        $data['inputStringDepartementHidden_partenaires'] = $inputStringDepartementHidden_partenaires;

                        $data['inputStringVilleHidden_partenaires'] = $inputStringVilleHidden_partenaires;

                        $data['session'] = $this->session;

                        $result_to_show .= $this->load->view('sortez_vsv/plat_check_category', $data, TRUE);

                    }

            }

        }

        echo $result_to_show;

    }

    function check_Idcategory_of_subCategory($id)

    {

//        $sousRubrique = $this->mdl_plat_du_jour->getByIdSousrubrique($id);

//        var_dump($sousRubrique);die('koko');

        $session_iCategorieId = $this->session->userdata('iSousCategorieId_x');

        if (isset($session_iCategorieId)) {

            $iCategorieId_sess = $session_iCategorieId;

            if (isset($iCategorieId_sess) && is_array($iCategorieId_sess)) {

                $all_subcategory = "";

                for ($i = 0; $i < count($iCategorieId_sess); $i++) {

                    $sousRubrique = $this->mdl_plat_du_jour->getByIdSousrubrique($iCategorieId_sess[$i]);

                    $all_subcategory .= "-" . $sousRubrique->IdSousRubrique;

                }

                echo substr($all_subcategory, 1);

                ////$this->firephp->log($all_subcategory, 'all_subcategory');

            } else {

                echo "0";

            }

        } else echo "0";

    }

}