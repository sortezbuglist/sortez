<?php
class payement extends CI_Controller{

    function __construct() {
        parent::__construct();

        $this->load->library('session');
        $this->load->library('ion_auth');
        $this->load->model("packarticle_payement");
        $this->load->model("ion_auth_used_by_club");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }

    function index(){
        $this->liste();
    }

     function liste() {
        $objPaye = $this->packarticle_payement->GetAll();
        $data["colPaye"] = $objPaye;

        if($this->session->flashdata('mess_editPackPayement')=='1') $data['msg'] = '<strong style="color:#060">Pyement enregistré !</strong>';
        if($this->session->flashdata(' mess_editPackPayement')=='2') $data['msg'] = '<strong style="color:#F00">Ce payement ne peut être supprimé ! Payement lié à une catégorie !</strong>';
        if($this->session->flashdata(' mess_editPackPayement')=='3') $data['msg'] = '<strong style="color:#060">Payement supprimé !</strong>';

        $this->load->view("packarticle/admin/vwListepayement",$data);
    }

    function fiche_payement($IdPackPayement) {

        if ($IdPackPayement != 0){
            $data["title"] = "Modification  Payement" ;
            $data["oPaye"] = $this->packarticle_payement->getById($IdPackPayement) ;
            $data["id"] = $IdPackPayement;
            $this->load->view('packarticle/admin/vwFichePayement', $data) ;

        }else{
            $data["title"] = "Creer un Payement" ;
            $this->load->view('packarticle/admin/vwFichePayement', $data) ;
        }
    }

    function creer_payement(){
        $oPaye = $this->input->post("payement") ;
        $this->packarticle_payement->insertpackarticle_payement($oPaye);
        $data["msg"] = "Payement ajouté" ;

        $objPaye = $this->packarticle_payement->GetAll();
        $data["colPaye"] = $objPaye;
        $this->load->view("packarticle/admin/vwListepayement",$data);
    }

    function modif_payement($IdPackPayement){
        $oPaye = $this->input->post("payement") ;
        $this->packarticle_payement->updatepackarticle_payement($oPaye);
        $data["msg"] = "Payement enregistré" ;

        $objPaye = $this->packarticle_payement->GetAll();
        $data["colPaye"] = $objPaye;
        $this->load->view("packarticle/admin/vwListepayement",$data);
    }

    
    function delete($prmId) {

        if(is_numeric(trim($prmId))) {
            //verify if category contain active article********
            $oCategorie = $this->packarticle_payement->verifier_payement($prmId) ;
            //$oCategorie = array();
            if (count($oCategorie)==0){
                $this->packarticle_payement->supprimepayement($prmId);
                $this->session->set_flashdata('mess_editPackPayement', '3');
            } else {
                $this->session->set_flashdata('mess_editPackPayement', '2');
            }

            //$this->firephp->log($oCategorie, 'oCategorie');

            redirect("admin/payement");
        }
        else {
            redirect("admin/payement");
        }

    }


}
    

