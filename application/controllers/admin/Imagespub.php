<?php
class imagespub extends CI_Controller {
    
    function __construct() {
        parent::__construct();
		$this->load->model("mdlimagespub") ;
		$this->load->library('image_lib');

        $this->load->library('ion_auth');

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
		
    }
    
    function index(){			
        $this->liste() ;
    }
	
	function liste(){
		
		$toListeImagespub = $this->mdlimagespub->listeImagespub() ;		
		$data['toListeImagespub'] = $toListeImagespub ;		
        $this->load->view('admin/vwListeImagepub', $data) ;
    }

	function ficheImagespub($_iDPub = 0){
		
		if ($_iDPub != 0){
			$data["title"] = "Modification imagespub" ;
			$data["oImagespub"] = $this->mdlimagespub->GetByImagespubId($_iDPub);
            $this->load->view('admin/vwFicheImagepub', $data) ;
		
		}else{
			$data["title"] = "Creer une image pub" ;
			$this->load->view('admin/vwFicheImagepub', $data) ;
		}
	}
	
	function creerImagespub(){
		if (isset($_POST["cmdCancel"])) {
			redirect ("admin/imagespub") ;
            return 0;			
        }
		$oImagespub = $this->input->post("imagespub") ;
		//print_r($oImagespub); exit();
		$IdInsertedImagespub = $this->mdlimagespub->SaveImagespub($oImagespub);
		$data["title"] = "Creer une imagespub" ;
		$data["msg"] = "Imagespub bien cree" ;
		redirect('admin/imagespub', $data) ;
	}
	
	function modifImagespub(){
		if (isset($_POST["cmdCancel"])) {
			redirect ("admin/imagespub") ;
            return 0;			
        }
		$oImagespub = $this->input->post("imagespub") ;
		$IdUpdatedImagespub = $this->mdlimagespub->SaveImagespub($oImagespub);
		$data["title"] = "Modification imagespub" ;
		$data["msg"] = "Annon bien ete modifier" ;
		redirect ("admin/imagespub/liste") ;
		//$this->load->view('front/vwFicheImagespub', $data) ;
	}
	
	
	function supprimImagespub($_iDImagespub){
		
		$zReponse = $this->mdlimagespub->supprimeImagespub($_iDImagespub) ;
		redirect ("admin/imagespub") ;
	}
	
	function uploadPub ($zNomChamp, $zExtension = "", $zExistNomImage = ""){
	
        $error = "" ;
        $msg = "" ;
        $srcFile = "" ;        
		$path = "application/resources/front/images/" ;				
        if (file_exists($_FILES["$zNomChamp"]["tmp_name"])){
            $fileType = $_FILES["$zNomChamp"]["type"];
            if ($fileType == "image/gif" || $fileType == "image/GIF" || $fileType == "image/jpeg" || $fileType == "image/JPEG" || $fileType == "image/bmp" || $fileType == "image/BMP" || $fileType == "image/tiff" || $fileType == "TIFF" || $fileType == "image/pjpeg" || $fileType == "image/PJPEG" || $fileType == "image/png" || $fileType == "image/PNG") {
                $srcFile = $this->genererLettreAleatoir(4) . rand(0,9999) . "." . $zExtension ;				
                if(move_uploaded_file($_FILES["$zNomChamp"]["tmp_name"], $path . basename($srcFile))) {
					if ($zExistNomImage != "" && file_exists($path.$zExistNomImage)){
						unlink($path.$zExistNomImage) ;
					}
					$this->resizeImage($srcFile) ;
                } else {
                    $error .= "D&eacute;sol&eacute;, il y avait un erreur lors du t&eacute;l&eacute;chargement de votre fichier.";
                }
            } else {
                $error = "Veuillez entrer une image";
            }
        } else {
            $error = "Le fichier n&apos;a pas &eacute;t&eacute; t&eacute;l&eacute;charg&eacute;";
        }

        $str = "{";
        $str .= "error: '" . $error . "',\n";
        $str.="msg: '" . $path . $srcFile . "'\n";
        $str .= "}";
        echo $str;
    }

	function genererLettreAleatoir ($long = 4)
	{
		$alphaB = "abcdefghijklmnopqrstuvwyxz";
		return substr(str_shuffle($alphaB), 0, $long);
	}
	
	function resizeImage ($_zImg) {
		
		$path = "application/resources/front/images/" ;
		$tFile = explode (".", $_zImg) ;
		$config['image_library'] = 'GD2';
		$config['source_image'] =  $path . $_zImg ;
		$config['new_image'] = $path . "ico" . $tFile[0] . "." . $tFile[1] ;
		$config['maintain_ratio'] = TRUE;
		$config['width'] = 200 ;
		$config['height'] = 85 ;

	
		$this->image_lib->initialize($config) ;

		$this->image_lib->resize() ;
		
		$this->image_lib->clear() ;
    }
	
	function active ($_iDImagespub = 0)
	{
		$zReponse = $this->mdlimagespub->activepub($_iDImagespub) ;
		redirect ("admin/imagespub") ;
	}

}