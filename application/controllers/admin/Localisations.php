<?php
class localisations extends CI_Controller{

    function __construct() {
        parent::__construct();

        $this->load->library('session');

        $this->load->model("mdl_categories_article");
        $this->load->model("mdl_types_article");
        $this->load->model("mdl_localisation");
        $this->load->model("mdlville");

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }

    function index(){
        $this->liste();
    }

    function liste() {
        $objLocalisations = $this->mdl_localisation->GetAll();
        $data["colLocalisations"] = $objLocalisations;
        $this->load->view("admin/vwListeLocalisations",$data);
    }

    function ficheLocalisation($IdLocalisation) {

        $data["colVilles"] = $this->mdlville->GetAll();

        if ($IdLocalisation != 0){
            $data["title"] = "Modification Localisation" ;
            $data["oLocalisation"] = $this->mdl_localisation->getById($IdLocalisation) ;
            $data["location_id"] = $IdLocalisation;

            $this->load->view('admin/vwFicheLocalisation', $data);

        }else{
            $data["title"] = "Creer une Localisation" ;
            $this->load->view('admin/vwFicheLocalisation', $data) ;
        }
    }

    function getPostalCode_localisation($idVille=0){
        $oville = $this->mdlville->getVilleById($idVille);

        if (count($oville)!=0) echo $oville->CodePostal;
        else echo "";

    }

    function creerLocalisation(){
        $oLocalisation = $this->input->post("Localisation") ;
        $this->mdl_localisation->insert($oLocalisation);
        $data["msg"] = "Localisation bien ajoutée" ;

        $objLocalisations = $this->mdl_localisation->GetAll();
        $data["colLocalisations"] = $objLocalisations;
        $this->load->view("admin/vwListeLocalisations",$data);
    }

    function modifLocalisation($IdLocalisation){
        $oLocalisation = $this->input->post("Localisation");
        $this->mdl_localisation->update($oLocalisation);
        $data["msg"] = "Localisation bien enregistrée" ;

        $objLocalisations = $this->mdl_localisation->GetAll();
        $data["colLocalisations"] = $objLocalisations;
        $this->load->view("admin/vwListeLocalisations",$data);
    }

    function delete($prmId) {
        $this->mdl_localisation->delete($prmId);
        $this->liste();
    }

}
