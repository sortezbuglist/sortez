<?php
/**
 * Created by PhpStorm.
 * User: rand
 * Date: 24/04/2018
 * Time: 14:11
 */

class xml extends CI_Controller
{

    public $datatourisme_imported = 0;
    public $datatourisme_exists = 0;
    public $datatourisme_skipped = 0;
    function __construct()
    {
        parent::__construct();

        $this->output->set_header('Access-Control-Allow-Origin: null');  header('Access-Control-Allow-Credentials: omit');
        header('Access-Control-Allow-Method: PUT, GET, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');

        $this->load->library('session');
        $this->load->library('ion_auth');

        // $this->load->model('wml_models');
        $this->load->model('xml_models');
        $this->load->model('mdlville');
        $this->load->model('mdl_agenda');
        $this->load->model('mdl_agenda_datetime');
        $this->load->model('mdl_categories_agenda');

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }

    function index()
    {
        $allrss=$this->xml_models->get_all_rss();
        if (isset($allrss) && count($allrss)>0){
            $first_id=0;
            $last_id=count($allrss);
            $data['firstid']=$first_id;
            if(isset($last_id) && is_object($last_id)) $data['lastid']=$last_id->id;
        }
        $links = $this->input->post("links");
        $departement = $this->xml_models->getdepartement();
        $data['departement'] = $departement;

        $categ = $this->xml_models->getcateg();
        $ville = $this->mdlville->grtall_ville_revue();
        $data["colCategorie"] = $categ;
        $data["colVille"] = $ville;
        $categid = $this->input->post("categ");
        $data["categid"] = $categid;
        $postalcode = $this->input->post("postalcode");
        $data["links"] = $links;
        $data["postalcode"] = $postalcode;
        if ($departement == null) {

            $data["departementauto"] = "invalide";

        } else {
            $data["departementauto"] = $departement;
        }
        $allink = $this->xml_models->getlink();
        $data["allink"] = $allink;
        $this->load->view("admin/Flux_rss/index", $data);

    }

    public function mail_report_rss_update(){
        $this->load->helper('clubproximite');
        $ip = $_SERVER['REMOTE_ADDR'];
        $date = date('d-m-Y');
        $heure = date('h:i');
        $mailtosend = 'The rss is did by this ip: '.$ip.' at '.$date.', '.$heure;
        $destinataire[0]['Email'] = 'webmaster@sortez.org';
        $destinataire[0]['Name'] = 'Webmaster Sortez';
        $prmSujet = "Notification Récupération Rss";
        $prmEnvoyeur = "contact@sortez.org";
        $prmEnvoyeurName = "Sortez";
        try{
            envoi_notification($destinataire, $prmSujet, $mailtosend, $prmEnvoyeur, $prmEnvoyeurName);
            echo 'Notification envoyé à '.$destinataire[0]['Email'];
        }catch(Exception $e){
            echo $e;
        }
        
        
    }
    //linksave
    public function import()
    {
        $links = $this->input->post("links");
        $categid = $this->input->post("categ");
        $ville = $this->input->post("IdVille");
        $type = $this->input->post("typeof");
        if ($links != "" AND $categid != null) {
            $field = array("source" => htmlspecialchars($links),
                "agenda_categid" => $categid,
                "IdVille"=>$ville,
                "type"=>$type,
            );
            $save = $this->xml_models->savelink($field);
            if ($save) {
                redirect("admin/xml/");
            } else {
                echo 'erreur de sauvegarde';
            }
        } else {
            echo "Formulaire non rempli correctement";
        }
    }
    function savetorsslist()
    {
        $final="";
        $image="";
        $file="";
        $contents='';
        $normal='';
        $id=$this->input->post('id');
        $id=(int)$id;
        $idlink = $this->input->post("linkid");
        $lien = $this->input->post("link");
        $villeid=$this->input->post("IdVille");
        $cache=$this->input->post("cache");
        $aboutlink = $this->xml_models->getlinkbyid($idlink);


        // start update link object on database
        $link_current_date_update = (array)$aboutlink;
        $link_current_date_update['getted'] = 1;
        $link_current_date_update['current_date_getted'] = date("Y-m-d");
        $link_current_date_update_result = $this->xml_models->update_current_date($link_current_date_update);
        // end update link object on database
        $categ = $aboutlink->agenda_categid;
        if ($lien != "" AND $categ != "" AND $lien != null  AND $categ != null) {
            $this->load->model('xml_models');
            $this->load->helper('clubproximite');
            if ($cache == 0 ){
                $this->delete_cache();
            }

            $xml = simplexml_load_file($lien);
            //var_dump($id);
           /* var_dump($xml->channel->item[0]);
die();*/
            //var_dump($xml->channel->item);die();
            if (isset($xml->channel->item)) {
                //var_dump($xml->channel->item);die();
               // foreach ($xml->channel->item as $xml->channel->item[$id]) {
                    // var_dump($xml->channel->item[$id]->description);die();
                    $is = (string)$xml->channel->item[$id]->description;
                    $exploded = explode(';', htmlspecialchars($is));

                    $results = "";

                    foreach ($exploded as $val) {

                        if (preg_match("/\jpg\b/i", $val)) {
                            $results = $val;
                        }
                    }
                    //var_dump($results);
                    if ($results != "") {
                        $finals = htmlentities($results, ENT_QUOTES);
                        $normal = str_replace("&amp;quot", '', $finals);
                    } else {
                        if (isset($xml->channel->item[$id]->enclosure)) {
                            $enclosure = $xml->channel->item[$id]->enclosure['url'];
                            // var_dump($enclosure);
                        }

                    }
                    if (isset($normal) AND $normal != null AND $normal != "") {
                        $final = $normal;
                    } else {
                        if (isset($enclosure) AND $enclosure != null AND $enclosure != "") {
                            //$final=$enclosure;
                                if(count(explode("filerobot" , $enclosure)) > 1 ){
                            // $test = explode("?", $enclosure);
                            // $utile = $test[0];
                            $finals = str_replace("amp;", '', $enclosure);
                            $final = $finals;
                            var_dump($final);
                            }else{
                                //$final=$enclosure;
                            $test = explode("?", $enclosure);
                            $utile = $test[0];
                            $finals = str_replace(" ", '', $utile);
                            $final = $finals;
                            }
                        }
                    }


                    if (isset($final) && $final != null AND $final !='') {
                        $info = pathinfo($final);
                        $contents = file_get_contents($final);
                        $_zExtension = strrchr($info['basename'], '.');
                        // ---------------------------
                        if($_zExtension != "jpeg" AND $_zExtension != "png" AND $_zExtension != "jpg"){
                            $_zFilename = random_string('unique', 10) .".jpg";
                        }else{
                        $_zFilename = random_string('unique', 10) . $_zExtension;
                        }
                        // -----------------------
                        $_zFilename = random_string('unique', 10) . $_zExtension;
                        $file = 'application/resources/front/photoCommercant/imagesbank/rss_image/' . $_zFilename;
                        $dir='application/resources/front/photoCommercant/imagesbank/rss_image/';
                        if (is_dir($dir) == true)
                        {
                        // var_dump($file);die();
                        @file_put_contents($file, $contents);
                        //$uploaded_file = new UploadedFile($file, $_zFilename);
                        if (isset($_zFilename)) $photo = $_zFilename;// save image name on agenda table
                        if ($file != "") {
                            if (is_file($file)) {

                                $base_path_system = str_replace('system/', '', BASEPATH);
                                $image_path_resize_home = $base_path_system . "/" . $file;
                                $image_path_resize_home_final = $base_path_system . "/" . $file;
                                $this_imgmoo =& get_instance();
                                $this_imgmoo->load->library('image_moo');
                                $this_imgmoo->image_moo
                                    ->load($image_path_resize_home)
                                    ->resize_crop(640, 480, false)
                                    ->save($image_path_resize_home_final, true)
                                    ->clear_temp()
                                    ->clear();
                            }
                        }
                    }else{
                            mkdir ($dir, 0777);
                            @file_put_contents($file, $contents);
                            //$uploaded_file = new UploadedFile($file, $_zFilename);
                            if (isset($_zFilename)) $photo = $_zFilename;// save image name on agenda table
                            if ($file != "") {
                                if (is_file($file)) {
                                    $base_path_system = str_replace('system/', '', BASEPATH);
                                    $image_path_resize_home = $base_path_system . "/" . $file;
                                    $image_path_resize_home_final = $base_path_system . "/" . $file;
                                    $this_imgmoo =& get_instance();
                                    $this_imgmoo->load->library('image_moo');
                                    $this_imgmoo->image_moo
                                        ->load($image_path_resize_home)
                                        ->resize_crop(640, 480, false)
                                        ->save($image_path_resize_home_final, true)
                                        ->clear_temp()
                                        ->clear();
                                }
                            }
                        }
                    }else{$photo=null;}

                    if (!isset($photo) OR $photo == "") {
                        $photo = null;
                    }
//var_dump($photo);
/////////////////////////formater date debut et date fin/////////////////////////////
                    if (isset($xml->channel->item[$id]->pubDate)) {
                        $pubdate = $xml->channel->item[$id]->pubDate;
                        $testdateformat = explode("-", $pubdate);
                        if (isset($testdateformat[2])) {
                            $day = explode(" ", $testdateformat[2]);
                            $date_pub = $testdateformat[0] . "-" . $testdateformat[1] . "-" . $day[0];
                        } else {
                            $this->load->helper("clubproximite_helper");
                            $datebrutr = $xml->channel->item[$id]->pubDate;
                            $date = convert_xml_date_to_sql_date($datebrutr);
                            $date_pub = $date;
                        }
                    } else {
                        $date_pub = null;
                    }
                    if (isset($villeid) AND $villeid !='' AND $villeid!=null AND $villeid!=null){
                    $ville_info=$this->mdlville->getVilleById($villeid);
                    }
////////////////////////////////////////////////////////////////////////////////////
                    $date_depot = date("y-m-d");
                    $fieldc = array('nom_manifestation' => (string)$xml->channel->item[$id]->title,
                        'siteweb' => (string)$xml->channel->item[$id]->link,
                        'description' => (string)$xml->channel->item[$id]->description,
                        'date_depot' => $date_depot,
                        "article_categid" => $categ,
                        "IsActif" => 1,
                        "IdCommercant" => 301299,
                        "IdUsers_ionauth" => 1992,
                        "photo1" => $photo,
                        "date_debut" => $date_pub,
                        "date_fin" => $date_pub,
                        "IdVille_localisation"=>$ville_info->IdVille ?? 0,
                        "codepostal_localisation" =>$ville_info->CodePostal,


                    );
                    // var_dump($fieldc);die();

                    $test = $this->xml_models->testexist((string)$xml->channel->item[$id]->title);

                if ($test == 1 AND (string)$xml->channel->item[$id]->title != "" AND (string)$xml->channel->item[$id]->link != "") {


                             $this->xml_models->save_to_sortez($fieldc);
                        
                      
                    }

               // }
                if (isset($lien)){
                        $this->lien=$lien;
                    }
                    if (isset($idlink)){
                        $this->idlink=$idlink;
                    }
                echo json_encode('ok');
            } else {
                echo "Lien non valide";
            }
        }

    }
    function savetorsslist2($ids)
    {

        $idlink = $ids;
        $lien_array=$this->xml_models->getlink_by_id($ids);
        $lien=$lien_array->source;
//        var_dump($idlink);
//        var_dump($lien);die();
                $data["lien"] = $lien;
                $data["idlink"] = $idlink;
                $this->load->view("admin/Flux_rss/preparetoimport", $data);
    }
    function gettypeofrss($id){
        $this->db->where("id",$id);
        $query = $this->db->get('rss_source');
        $row= $query->result();
        foreach ($query->result() as $row)
                {
                    return $row->type;
                }       
    }
    function test($id){
        $k=$this->gettypeofrss($id);
       echo "<pre>".var_dump($k)."</pre>";
    }
    function import_datatourisme_func()
    {
        $this->load->model('xml_models');
        $this->load->helper('clubproximite');
        $idrss_source = $this->input->post("lienrss");
        //$idlink = htmlspecialchars($idlinkbr);
        $current_id="";
        //$aboutlink = $this->gettypeofrss($idlink);
        $typeof=$this->gettypeofrss($idrss_source);
       

        
        $current_id = $this->input->post("toFirstIdDatatourisme");
        if (isset($current_id)) $objDatatourisme = $this->xml_models->getByIdDatatourisme($current_id);
        $objResult = array();
        $objAgenda = array();
//var_dump($objDatatourisme->nom_manifestation);die();
        if (isset($objDatatourisme) && $objDatatourisme != "") {

            $objAgenda["IdCommercant"] = '301299';
            $objAgenda["IdUsers_ionauth"] = '1992';
            if ($objDatatourisme->nom_manifestation !== "" AND $objDatatourisme->date_depot !== "" AND $objDatatourisme->article_categid !== "") {
                $objAgenda["date_debut"] = $objDatatourisme->date_debut;
                $objAgenda["nom_manifestation"] = $objDatatourisme->nom_manifestation;
                $objAgenda["description"] = $objDatatourisme->description;

                $objAgenda["IdVille_localisation"] = $objDatatourisme->IdVille_localisation;
                $objAgenda["codepostal_localisation"] = $objDatatourisme->codepostal_localisation;

                $objAgenda["id"] = null;
                $objAgenda["date_depot"] = $objDatatourisme->date_depot;
                if (isset(
                        $objAgenda["date_depot"]) && $objAgenda["date_depot"] == "0000-00-00") $objAgenda["date_depot"] = date("Y-m-d");
                if ($typeof=="1") {
                     $objAgenda["article_categid"] = $objDatatourisme->article_categid;
                }else if ($typeof=="0") {
                     $objAgenda["agenda_categid"] = $objDatatourisme->article_categid;                
                }

                $objAgenda["IsActif"] = '1';
                $objAgenda["siteweb"] = $objDatatourisme->siteweb;
                $objAgenda["photo1"] = $objDatatourisme->photo1;
                $objAgenda["photo1"] = $objDatatourisme->photo1;
                $objAgenda["agenda_article_type_id"] = 3;
                $test = $objDatatourisme->nom_manifestation;
                $testexist = $this->xml_models->testexist2($test);

                if ($testexist == 1) {

                    if ($typeof=="1") {
                        $idsave = $this->xml_models->save($objAgenda);
                    }elseif ($typeof=="0") {
                        $idsave = $this->xml_models->save_agenda($objAgenda);
                    }
                    

                    if ($typeof=="1") {
                        if (isset($objDatatourisme->date_debut) && $objDatatourisme->date_debut != "") {
                        $field2 = array('article_id' => (integer)$this->db->insert_id(), "date_debut" => $objDatatourisme->date_debut);
                    } else {
                        $field2 = array('article_id' => (integer)$this->db->insert_id(),);
                    }

                    }elseif ($typeof=="0") {
                          if (isset($objDatatourisme->date_debut) && $objDatatourisme->date_debut != "") {
                        $field2 = array('agenda_id' => (integer)$this->db->insert_id(), "date_debut" => $objDatatourisme->date_debut);
                    } else {
                        $field2 = array('agenda_id' => (integer)$this->db->insert_id(),);
                    }

                       }    

                  

                    if ($typeof=="1") {
                        $this->xml_models->save2($field2);
                    }elseif ($typeof=="0") {
                       $this->xml_models->save_22($field2);
                    }
                    

                    $field3 = array('article_id' => (integer)$this->db->insert_id(),
                        "idlien" => $aboutlink->id);
                    $this->xml_models->save3($field3);
                    if ($idsave) {
                        $this->datatourisme_imported++;
                    }
                } elseif ($testexist == 0) {
                    $this->datatourisme_exists++;
                }
            } else {
                $this->datatourisme_skipped++;
            }


            $objResult['datatourisme_imported'] = $this->datatourisme_imported;
            $objResult['datatourisme_exists'] = $this->datatourisme_exists;
            $objResult['datatourisme_skipped'] = $this->datatourisme_skipped;
            $objResult['datatourisme_title'] = $objDatatourisme->nom_manifestation;
        }

        $http_code = 200;
        header('Content-type: json');
        header('HTTP/1.1: ' . $http_code);
        header('Status: ' . $http_code);
        exit(json_encode((array)$objResult));

             //faranfaranfaranfaranfaranfaran
       
    } 

    public function delete_cache()
    {
        $this->xml_models->delete_cache();
    }

    public function prepare()
    {
        $idlink = $this->input->post("linkid");
//var_dump($idlink);die();
        $aboutlink = $this->xml_models->getlinkbyid($idlink);

        $data['link'] = $aboutlink;
        $this->load->view("admin/Flux_rss/preparetoimport", $data);

    }

    public function delete_link($id)
    {
        $request = $this->xml_models->delete_link($id);
        if ($request) {
            redirect("admin/xml/");
        } else {
            echo "Erreur de suppression";
        }
    }
    /*                  pick date
     * $string = 'Mr Roux said it will be argued that on July 11 2014 it was physically impossible for Ms Steenkamp to have continued to scream after being struck in the head by a bullet from gun, especially if the sound had to travel 177 metres from the bathroom where the young model cowered.\"A person with that brain damage will have no cognitive response,\" he said.A short time later Dr Burger’s evidence was complete, herself fighting back tears at having to relive the night in question.\"It was awful to hear her shout before the shots,\" she said, saying she was still plagued by the screams. \"Its still quite raw.\" The Oscar Pistorius';

    preg_match_all('/\b(?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May?|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sept(?:ember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?) (\d|\d{2}) \d{4}/', $string, $matches);

    //flattens the array, because it is bi-dimensional
    $flattened_array = new RecursiveIteratorIterator(new RecursiveArrayIterator($matches));

    //loop through this flattened array
    foreach($flattened_array as $k => $v)
    {
            //if the value of that array item is not a pure integer (11 or 12, etc..)
        if((int) $v === 0)
        {
                //add the value to the final output array
            $final_matches[] = $v;
        }
    }

    var_dump($final_matches);*/

    public function reset_data(){
        $this->data_deleted=0;
        $first_id=$this->input->post('firstId');
        $this->data_deleted=0;
        $this->xml_models->delete_all_rss($first_id);

      $this->xml_models->delete_rss_list();
      $this->data_deleted++;


        $reultat['data_deleted'] = $this->data_deleted;
        $http_code = 200;
        header('Content-type: json');
        header('HTTP/1.1: ' . $http_code);
        header('Status: ' . $http_code);
        exit(json_encode((array)$reultat));
    }
    public function count_data($ids){
        $lien_id=$ids;
        $lien=$this->xml_models->getlink_by_id($lien_id)->source;
        $xml = simplexml_load_file($lien);
        if (isset($xml->channel->item)) {
        echo count($xml->channel->item);
    }else{
            echo 'ko';
        }
    }

}