<?php
/**
 * Created by PhpStorm.
 * User: rand
 * Date: 24/04/2018
 * Time: 14:11
 */

class datatourisme extends CI_Controller
{

    public $datatourisme_imported = 0;
    public $datatourisme_exists = 0;
    public $datatourisme_skipped = 0;

    function __construct()
    {
        parent::__construct();

        $this->output->set_header('Access-Control-Allow-Origin: null');  header('Access-Control-Allow-Credentials: omit');
        header('Access-Control-Allow-Method: PUT, GET, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');

        $this->load->library('session');
        $this->load->library('ion_auth');

        $this->load->model('mdl_datatourisme_agenda');
        $this->load->model('mdlville');
        $this->load->model('mdl_agenda');
        $this->load->model('mdl_agenda_datetime');
        $this->load->model('mdl_categories_agenda');
        $this->load->model('Mdl_article_organiser');

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }

    function  index(){
        $data['empty'] = null;
        $this->load->view("admin/datatourisme/main", $data);
    }

    function import_datatourisme_func(){
        $current_id = $this->input->post("toFirstIdDatatourisme");
        if(isset($current_id)) $objDatatourisme = $this->mdl_datatourisme_agenda->getByIdDatatourisme($current_id);
        $objResult = array();
        $objAgenda = array();
        $objAgendaDatetime = array();
        $objDatatourismeAgenda = array();

        if (isset($objDatatourisme) && $objDatatourisme!="") {

            if (isset($objDatatourisme->identifier)) {
                $objDatatourismeAgenda["datatourisme_id"] = $objDatatourisme->identifier;
                $objIdentifier = $this->mdl_datatourisme_agenda->getByDatatourisme($objDatatourisme->identifier);

                // SET DATA STRUCTURE START ------------------------------------------
                //get postalcode
                if (isset($objDatatourisme->isLocatedAt)) {
                    $isLocatedAt_x = $objDatatourisme->isLocatedAt; //var_dump($isLocatedAt);
                    $isLocatedAt = json_decode($isLocatedAt_x);
                    if (isset($isLocatedAt->address)) {
                        $desc_short = $isLocatedAt->address;
                        if (is_object($desc_short)) {
                            $objAgenda["adresse1"] = "";
                            $objAgenda["adresse_localisation"] = "";
                            if(isset($desc_short->addressLocality)) {
                                $objAgenda["adresse1"] .= $desc_short->addressLocality;
                                $objAgenda["adresse_localisation"] .= $desc_short->addressLocality;
                            }
                            if(isset($desc_short->streetAddress)) {
                                $objAgenda["adresse1"] .= $desc_short->streetAddress;
                                $objAgenda["adresse_localisation"] .= $desc_short->streetAddress;
                            }
                            if(isset($desc_short->postalCode)) {
                                $objAgenda["codepostal"] = $desc_short->postalCode;
                                $objAgenda["codepostal_localisation"] = $desc_short->postalCode;
                            }
                            if(isset($desc_short->postalCode) && isset($desc_short->addressLocality)) {
                                $villeWhereClause = " CodePostal LIKE '%".$desc_short->postalCode."%' AND ville_nom LIKE '%".strtoupper(addslashes($desc_short->addressLocality))."%' LIMIT 1";
                                $objVilles = $this->mdlville->getWhere($villeWhereClause);
                                if(isset($objVilles) && count($objVilles)>0){
                                    $objAgenda["IdVille"] = $objVilles[0]->IdVille;
                                    $objAgenda["IdVille_localisation"] = $objVilles[0]->IdVille;

                                }
                            }
                        }
                    }else{
                        if (isset($objDatatourisme->hasDescription) AND $objDatatourisme->hasDescription !=""){
                            $all=$objDatatourisme->hasDescription;
                            $alldecoded=json_decode($all);
                            if (isset($alldecoded->shortDescription) AND $alldecoded->shortDescription !=null AND $alldecoded->shortDescription !=""){
                                $exploded=explode(" ",$alldecoded->shortDescription);
                                $test=$this->mdl_agenda->getallville();
                                foreach ($test as $villes){

                                    for ($i = 0; $i < count($exploded); $i++)

                                    {
                                        if ($exploded[$i]==$villes->Nom) {
                                            $objAgenda["adresse1"] = $villes->Nom;
                                            $objAgenda["IdVille"] = $villes->IdVille;
                                            $objAgenda["codepostal"] = $villes->CodePostal;
                                            $objAgenda["codepostal_localisation"] = $villes->CodePostal;

                                        }
                                    }
                                }

                            }else{
                                $objAgenda["codepostal_localisation"] ="";
                                $objAgenda["IdVille"] = "";
                                $objAgenda["codepostal"] = "";
                                $objAgenda["codepostal_localisation"] = "";}
                        }
                    }

                    if (isset($objDatatourisme->type)) {
                            $list_type = json_decode($objDatatourisme->type);
                            if (is_array($list_type)){
                                for ($x = 0; $x <= count($list_type); $x++) {
                                    if(isset($list_type[$x])){
                                        $objCategAgenda = $this->mdl_categories_agenda->getCodename($list_type[$x]);
                                        if (isset($objCategAgenda) && count($objCategAgenda)>0) {
                                            foreach ($objCategAgenda as $item_categ){
                                                if (isset($item_categ->agenda_categid) && $item_categ->agenda_categid != "") {
                                                    $objAgenda["agenda_categid"] = $item_categ->agenda_categid;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        $objAgenda["IdCommercant"] = '301299';
                        $objAgenda["IdUsers_ionauth"] = '1992';
                        if (isset($objDatatourisme->hasBeenPublishedBy)) {
                            $data_organiser = json_decode($objDatatourisme->hasBeenPublishedBy);
                            if (isset($data_organiser->legalName)) {
                                $objAgenda["nom_societe"] = $data_organiser->legalName;
                                $objAgenda["organisateur"] = $data_organiser->legalName;
                            }
                        }
                        if (isset($objDatatourisme->label)) {
                            $label = json_decode($objDatatourisme->label);
                            if (isset($label->value)) {
                                $objAgenda["nom_manifestation"] = $label->value;
                            } else {
                                $objAgenda["nom_manifestation"] = $objDatatourisme->label;
                            }
                        }
                        if (isset($objDatatourisme->hasContact)) {
                            $hasContact = json_decode($objDatatourisme->hasContact);
                            if (isset($hasContact->telephone)) $objAgenda["telephone"] = $hasContact->telephone;
                            if (isset($hasContact->email)) $objAgenda["email"] = $hasContact->email;
                            if (isset($hasContact->homepage)) $objAgenda["siteweb"] = $hasContact->homepage;
                        }
                        if (isset($objDatatourisme->hasDescription)) {
                            $hasDescription = json_decode($objDatatourisme->hasDescription);
                            if (isset($hasDescription->shortDescription)) {
                                $desc_short = $hasDescription->shortDescription;
                                if (is_object($desc_short) && isset($desc_short->value)) $objAgenda["description"] = $desc_short->value;
                                else $objAgenda["description"] = $hasDescription->shortDescription;
                            }
                        }
                        if (isset($objDatatourisme->offers_schema)) {
                            $offers_schema = json_decode($objDatatourisme->offers_schema);
                            if (isset($offers_schema->priceSpecification)) $priceSpecification = $offers_schema->priceSpecification;
                            if (isset($priceSpecification) && is_object($priceSpecification) && isset($priceSpecification->price)) $price = $priceSpecification->price;
                            if (isset($price) && is_object($price) && isset($price->value, $priceSpecification->priceCurrency)) $objAgenda["description_tarif"] = 'Tarification : '.$price->value.$priceSpecification->priceCurrency;
                        }
                        if (isset($objDatatourisme->hasMainRepresentation)) {
                            $data_item = json_decode($objDatatourisme->hasMainRepresentation);//var_dump($data_item);
                            if (isset($data_item->hasRelatedResource)) $data_item_locator = $data_item->hasRelatedResource;
                            if (isset($data_item_locator->locator)) $data_item_locator_value = $data_item_locator->locator;
                            if (isset($data_item_locator_value->value)) {
                                $info = pathinfo($data_item_locator_value->value);
                                $contents = file_get_contents($data_item_locator_value->value);
                                $_zExtension = strrchr($info['basename'], '.') ;
                                $_zFilename = random_string('unique', 10) . $_zExtension  ;
                                $file = 'application/resources/front/photoCommercant/imagesbank/1992/' . $_zFilename;
                                @file_put_contents($file, $contents);
                                //$uploaded_file = new UploadedFile($file, $_zFilename);
                                if (isset($_zFilename)) $objAgenda["photo1"] = $_zFilename;// save image name on agenda table
                                if ($file != ""){
                                    if (is_file($file)) {
                                        $base_path_system = str_replace('system/', '', BASEPATH);
                                        $image_path_resize_home = $base_path_system."/".$file;
                                        $image_path_resize_home_final = $base_path_system."/".$file;
                                        $this_imgmoo =& get_instance();
                                        $this_imgmoo->load->library('image_moo');
                                        $this_imgmoo->image_moo
                                            ->load($image_path_resize_home)
                                            ->resize_crop(640,480,false)
                                            ->save($image_path_resize_home_final,true);
                                    }
                                }
                            }
                    }
                }
                // SET DATA STRUCTURE END ------------------------------------------


                if (!isset($objIdentifier) || $objIdentifier==null || count($objIdentifier)==0){//oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
                    //$this->datatourisme_exists++;
                } else {
                    $this->datatourisme_exists++;
                }
            } else {
                $this->datatourisme_skipped++;
            }


            $objAgenda["id"] = null;
            $objAgenda["date_depot"] = date("Y-m-d");
            $objAgenda["IsActif"] = '1';


            if (isset($objDatatourisme->identifier)) {
                $objIdentifier = $this->mdl_datatourisme_agenda->getByDatatourisme($objDatatourisme->identifier);
                $datatourisme_status_item = 0;


                if (isset($objDatatourisme->startDate)) {
                    $startDate = json_decode($objDatatourisme->startDate);
                    if (isset($startDate->value)) {$objAgenda["date_debut"] = $startDate->value; }
                    ////////for those who have multiple value//////////////
                    elseif (isset($startDate[0])){
                        $objAgenda["date_debut"] = $startDate[0]->value;
                    }
                }

                if (isset($objDatatourisme->endDate)) {
                    $endDate = json_decode($objDatatourisme->endDate);
                    if (isset($endDate->value)) {$objAgenda["date_fin"] = $endDate->value;
                        ////////for those who have multiple value//////////////
                    }elseif (isset($endDate[0])){
                        $objAgenda["date_fin"] = $endDate[0]->value;
                    }
                }


                //***** insert/update agenda
                if (!isset($objIdentifier) || $objIdentifier==null || count($objIdentifier)==0){
                    if(isset($objAgenda["IdVille"]) && $objAgenda["IdVille"]!="" && $objAgenda["IdVille"]!='0' && isset($objAgenda["agenda_categid"]) && $objAgenda["agenda_categid"]!="" && $objAgenda["agenda_categid"]!="0") {

                        $field=array(
                            /////////////start from commercant datatourisme///////////////////////////
                            "IdCommercant"=>"301386",
                            "address1"=>$desc_short->streetAddress ?? "SAS 18, rue des Combattants d'Afrique du Nord 06000 Nice",
                            "ville_id"=>$objVilles[0]->IdVille ?? "2050",
                            "postal_code"=>$desc_short->postalCode ?? "06000",
                            //////////end from commercant datatourisme//////////////////////

                            //////////////////////start from flux datatourisme/////////////////////////
                            "website"=>$hasContact->homepage ?? null,
                            "tel"=>$hasContact->telephone ?? null,
                            "email"=>$hasContact->email ?? null,
                            "name"=>$data_organiser->legalName ?? "Datatourisme",
                            /////////////////end from flux datatourisme///////////////////////////////

                        );
                       $lastinsertid= $this->Mdl_article_organiser->saveDatatourismeOrganiser($field);

                        $objAgenda["organiser_id"]=$lastinsertid;
                        $IdUpdatedArticle = $this->mdl_agenda->insert($objAgenda);
                    }
                } else {
                    $objAgenda["id"] = $objIdentifier->agenda_id;
                    if(isset($objAgenda["IdVille"]) && $objAgenda["IdVille"]!="" && $objAgenda["IdVille"]!='0' && isset($objAgenda["agenda_categid"]) && $objAgenda["agenda_categid"]!="" && $objAgenda["agenda_categid"]!="0") {


                        $field=array(
                            /////////////start from commercant datatourisme///////////////////////////
                            "IdCommercant"=>"301386",
                            "address1"=>$desc_short->streetAddress ?? "",
                            "ville_id"=>$objVilles[0]->IdVille ?? "",
                            "postal_code"=>$desc_short->postalCode ?? "" ,
                            //////////end from commercant datatourisme//////////////////////

                            //////////////////////start from flux datatourisme/////////////////////////
                            "website"=>$hasContact->homepage ?? "",
                            "tel"=>$hasContact->telephone ?? "",
                            "email"=>$hasContact->email ?? "",
                            "name"=>$data_organiser->legalName ?? "",
                            /////////////////end from flux datatourisme///////////////////////////////

                        );
                        $lastinsertid= $this->Mdl_article_organiser->saveDatatourismeOrganiser($field);

                        $objAgenda["organiser_id"]=$lastinsertid;

                        $IdUpdatedArticle = $this->mdl_agenda->update($objAgenda);
                        $datatourisme_status_item = 1;
                    }
                }

                if (isset($objDatatourisme->startDate)) {
                    $startDate = json_decode($objDatatourisme->startDate);
                    if (isset($startDate->value)) {$objAgendaDatetime["date_debut"] = $startDate->value; }
                    ////////for those who have multiple value//////////////
                    elseif (isset($startDate[0])){
                        $objAgendaDatetime["date_debut"] = $startDate[0]->value;
                    }
                }

                if (isset($objDatatourisme->endDate)) {
                    $endDate = json_decode($objDatatourisme->endDate);
                    if (isset($endDate->value)) {$objAgendaDatetime["date_fin"] = $endDate->value;
                        ////////for those who have multiple value//////////////
                    }elseif (isset($endDate[0])){
                        $objAgendaDatetime["date_fin"] = $endDate[0]->value;
                    }
                }
                if (isset($IdUpdatedArticle)){
                    $objAgendaDatetime["agenda_id"] = $IdUpdatedArticle;
                    $objAgendaDatetime["id"] = null;
                    if (isset($objAgendaDatetime["date_fin"]) && $datatourisme_status_item == 0)
                        $IdUpdatedArticle_datetime = $this->mdl_agenda_datetime->insert($objAgendaDatetime);
                    $objDatatourismeAgenda["agenda_id"] = $IdUpdatedArticle;
                    if ($IdUpdatedArticle != '0' && $datatourisme_status_item == 0){
                        $IdUpdatedDatatourismeAgenda= $this->mdl_datatourisme_agenda->insert($objDatatourismeAgenda);
                        $this->datatourisme_imported++;
                    } else {
                        $this->datatourisme_skipped++;
                    }
                }
            }





            if (isset($objAgenda["nom_manifestation"])) $objResult['datatourisme_title'] = $objAgenda["nom_manifestation"];
            $objResult['datatourisme_imported'] = $this->datatourisme_imported;
            $objResult['datatourisme_exists'] = $this->datatourisme_exists;
            $objResult['datatourisme_skipped'] = $this->datatourisme_skipped;

        } else {
            $this->datatourisme_skipped++;
            $objResult['datatourisme_imported'] = $this->datatourisme_imported;
            $objResult['datatourisme_exists'] = $this->datatourisme_exists;
            $objResult['datatourisme_skipped'] = $this->datatourisme_skipped;
        }
//////////////////////////////////add organiser////////////////////////////////////////////////////////


        $http_code = 200;
        header('Content-type: json');
        header('HTTP/1.1: ' . $http_code);
        header('Status: ' . $http_code);
        exit(json_encode((array)$objResult));
    }

    function check_datatourisme_import_null(){
        /*$query = ' datatourisme_id = "0" ';
        $main_list_data_null = $this->mdl_datatourisme_agenda->getWhere($query);
        foreach ($main_list_data_null as $item) {
            $delete_agenda_confirm = $this->mdl_agenda->delete_definitif($item->agenda_id);
            if ($delete_agenda_confirm) $delete_agenda_date_time = $this->mdl_agenda_datetime->deleteByAgendaId($item->agenda_id);
            echo 'agenda removed '.$item->agenda_id;
        }*/
        //$main_list_data_null = $this->mdl_datatourisme_agenda->delet_by_datatourisme("0");
        echo "ok";

    }

    function check_id_ville_localisation_value(){
        /*$query = ' id > 7217 ';
        //$query = '';
        $main_list_data_null = $this->mdl_agenda->getWhere($query);
        foreach ($main_list_data_null as $item) {
            $agenda = (array) $item;
            $agenda['IdVille_localisation'] = $agenda['IdVille'];
            if ($agenda['date_depot'] != '0000-00-00')
            $id_update = $this->mdl_agenda->update($agenda);
            echo 'agenda edited '.$agenda['id'].' <br/>';
        }*/
        echo "ok";

    }

}