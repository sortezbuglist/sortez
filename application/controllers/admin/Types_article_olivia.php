<?php
class Types_article_olivia extends CI_Controller{

    function __construct() {
        parent::__construct();

        $this->load->library('session');
        $this->load->library('ion_auth');
        $this->load->model("mdl_types_article_olivia");
        $this->load->model("ion_auth_used_by_club");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }

    function index(){
        $this->liste();
    }

    function liste() {
        $objArticles = $this->mdl_types_article_olivia->GetAll();
        $data["colArticles"] = $objArticles;

        if($this->session->flashdata('mess_editTypeArticle')=='1') $data['msg'] = '<strong style="color:#060">Type enregistré !</strong>';
        if($this->session->flashdata('mess_editTypeArticle')=='2') $data['msg'] = '<strong style="color:#F00">Ce type d\'article ne peut être supprimé ! Type lié à une catégorie !</strong>';
        if($this->session->flashdata('mess_editTypeArticle')=='3') $data['msg'] = '<strong style="color:#060">Type supprimé !</strong>';

        $this->load->view("admin/vwListeTypeArticleOlivia",$data);
    }

    function fiche_types_article($IdTypeArticle) {

           if ($IdTypeArticle != 0){
               $data["title"] = "Modification Type Article" ;

               $data["oArticle"] = $this->mdl_types_article_olivia->getById($IdTypeArticle) ;
            $data["article_typeid"] = $IdTypeArticle;

        // $objArticles_sub = $this->mdl_types_article_olivia->GetAllsubcategbyidcateg($IdTypeArticle);
        // $data["colArticles_sub"] = $objArticles_sub;  
        $IdRubrique = $this->uri->rsegment(3);
        
        if(is_numeric(trim($IdRubrique))) {
            $data["no_main_menu_home"] = "1";
            $data["no_left_menu_home"] = "1";

            $toListeSousCategorie = $this->mdl_types_article_olivia->GetAllSousrubriqueByRubrique($IdRubrique) ;
            $toCategorie = $this->mdl_types_article_olivia->GetById($IdRubrique) ;
            $data['toListeSousCategorie'] = $toListeSousCategorie ;
            $data['toCategorie'] = $toCategorie ;

            if($this->session->flashdata('mess_editsouscategorie')=='1') $data['mess_editsouscategorie'] = '<strong style="color:#060">SousCatégorie enregistrée !</strong>';
            if($this->session->flashdata('mess_editsouscategorie')=='2') $data['mess_editsouscategorie'] = '<strong style="color:#F00">Cette SousCatégorie ne peut être supprimée ! catégorie liée à un partenaire !</strong>';
            if($this->session->flashdata('mess_editsouscategorie')=='3') $data['mess_editsouscategorie'] = '<strong style="color:#060">SousCatégorie supprimée !</strong>';

            $this->load->view("admin/vwSousCategory", $data);

         }
        else {
             redirect('admin/commercants/categories');
         }
    

        // else{
        //     $data["title"] = "Creer Type Article" ;
        //     $this->load->view('admin/vwFicheTypeArticle', $data) ;
        // }
    }

    function creer_types_article(){
        $oArticle = $this->input->post("types_article") ;
        $this->mdl_types_article_olivia->insertarticle_type($oArticle);
        $data["msg"] = "Type Article ajouté" ;

        $objArticles = $this->mdl_types_article_olivia->GetAll();
        $data["colArticles"] = $objArticles;
        $this->load->view("admin/vwListeTypeArticleOlivia",$data);
    }

    function modif_types_article($IdTypeArticle){
        $oArticle = $this->input->post("types_article") ;
        $this->mdl_types_article_olivia->updatearticle_type($oArticle);
        $data["msg"] = "Type Article enregistré" ;

        $objArticles = $this->mdl_types_article_olivia->GetAll();
        $data["colArticles"] = $objArticles;
        $this->load->view("admin/vwListeTypeArticleOlivia",$data);
    }

    function delete($prmId) {

        if(is_numeric(trim($prmId))) {
            //verify if category contain active article********
            $oCategorie = $this->mdl_types_article_olivia->verifier_type_article($prmId) ;
            //$oCategorie = array();
            if (count($oCategorie)==0){
                $this->mdl_types_article_olivia->supprimearticle_type($prmId);
                $this->session->set_flashdata('mess_editTypeArticle', '3');
            } else {
                $this->session->set_flashdata('mess_editTypeArticle', '2');
            }

            //$this->firephp->log($oCategorie, 'oCategorie');

            redirect("admin/types_article_olivia");
        }
        else {
            redirect("admin/types_article_olivia");
        }

    }


}