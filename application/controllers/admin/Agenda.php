<?php
class Agenda extends CI_Controller{
    
    function __construct() {
        parent::__construct();
		
        $this->load->model("Commercant");
        $this->load->model("mdlcategorie");
        $this->load->model("mdl_agenda");
        $this->load->model("user");
        $this->load->model("sousRubrique") ;
        $this->load->model("Abonnement");
        $this->load->model("mdlannonce");
        $this->load->model("mdlbonplan");
        $this->load->model("Abonnement");
        $this->load->model("AssCommercantAbonnement") ;
        $this->load->model("mdlville") ;

        $this->load->model("mdl_types_agenda") ;
        $this->load->model("mdl_categories_agenda") ;
        
        $this->load->library('session');
        $this->load->library('user_agent');
        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect();
        }
    }
    
    function index(){
        $this->agenda();
    }


    function no_permission(){
        $data['info'] = NULL;
        $this->load->view("front2013/no_permission", $data);
    }


    function agenda() {

        $toListeAgenda = $this->mdl_agenda->GetAll() ;
        $data['toListeAgenda'] = $toListeAgenda ;
        $this->load->view('admin/vwMesAgenda', $data) ;

    }

    function deleteAgenda($IdAgenda = 0, $prmIdCommercant = 0) {

        if (!$this->ion_auth->is_admin()) {
            //verify if $IdAgenda, $prmIdCommercant and $user_ion_auth->id are corrects and match between them
            if ($IdAgenda != 0 && $IdAgenda != "" && $IdAgenda != null && is_numeric($IdAgenda) == true) {
                $objAgenda_verification = $this->mdl_agenda->GetById($IdAgenda);
                if (isset($objAgenda_verification) && count($objAgenda_verification)!=0) {
                    if ($objAgenda_verification->IdCommercant != $prmIdCommercant) {
                        redirect("front/utilisateur/no_permission");
                    }
                } else redirect("front/utilisateur/no_permission");
            }
        }

        $this->mdl_agenda->delete_definitif($IdAgenda);
        redirect("admin/agenda/index");

    }


    function ficheAgenda($prmIdCommercant = 0, $IdAgenda = 0, $mssg = 0) {

        //verify if $IdAgenda, $prmIdCommercant and $user_ion_auth->id are corrects and match between them
        if ($IdAgenda != 0 && $IdAgenda != "" && $IdAgenda != null && is_numeric($IdAgenda) == true) {
            $objAgenda_verification = $this->mdl_agenda->GetById($IdAgenda);
            if (isset($objAgenda_verification) && count($objAgenda_verification)!=0) {
                if ($objAgenda_verification->IdCommercant != $prmIdCommercant) {
                    redirect("front/utilisateur/no_permission");
                }
            } else redirect("front/utilisateur/no_permission");
        }

        $data = null;

        $this->load->Model("Commercant");
        $this->load->Model("Rubrique");
        $this->load->Model("SousRubrique");
        $this->load->Model("Ville");
        $this->load->Model("Abonnement");
        $this->load->Model("AssCommercantRubrique");
        $this->load->Model("AssCommercantSousRubrique");
        $this->load->Model("AssCommercantAbonnement");

        $data["no_main_menu_home"] = "1";
        $data["no_left_menu_home"] = "1";


        $data["colVilles"] = $this->mdlville->GetAll();

        $data["objAgenda"] = $this->mdl_agenda->GetById($IdAgenda);
        $data["objCommercant"] = $this->Commercant->GetById($prmIdCommercant);


        $this->load->helper('ckeditor');


        //Ckeditor's configuration
        $data['ckeditor0'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'description',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '600px',    //Setting a custom width
                'height'     =>     '200px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ),
                    array( 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ),
                    array( 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ),
                    array( 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ),
                    array( 'Link','Unlink','Anchor' ),
                    array( 'Image','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ),
                    array( 'Styles','Format','Font','FontSize' ),
                    array( 'TextColor','BGColor' ),
                    array( 'Maximize', 'ShowBlocks','-','About' )
                )
            )
        );
//// OP END


        $data['ckeditor1'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'description_tarif',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '600px',    //Setting a custom width
                'height'     =>     '200px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ),
                    array( 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ),
                    array( 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ),
                    array( 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ),
                    array( 'Link','Unlink','Anchor' ),
                    array( 'Image','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ),
                    array( 'Styles','Format','Font','FontSize' ),
                    array( 'TextColor','BGColor' ),
                    array( 'Maximize', 'ShowBlocks','-','About' )
                )
            )
        );

        $data['ckeditor2'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'conditions_promo',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '600px',    //Setting a custom width
                'height'     =>     '200px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ),
                    array( 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ),
                    array( 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ),
                    array( 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ),
                    array( 'Link','Unlink','Anchor' ),
                    array( 'Image','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ),
                    array( 'Styles','Format','Font','FontSize' ),
                    array( 'TextColor','BGColor' ),
                    array( 'Maximize', 'ShowBlocks','-','About' )
                )
            )
        );

        $data['home_link_club'] = 1;
        $data['mssg'] = $mssg;

        $data['jquery_to_use'] = "1.4.2";

        $data['fiche_pro_platinum_detect'] = 1;

        $data["colTypeagenda"] = $this->mdl_types_agenda->GetAll();
        $data["colCategorie"] = $this->mdl_categories_agenda->GetAll();
        $data["colSousCategorie"] = $this->mdl_categories_agenda->GetAllSousrubrique();

        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($prmIdCommercant);
        if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
        $data["user_groups"] = $user_groups[0];


        $this->load->view("admin/vwFicheAgenda",$data);


    }


    function getPostalCode_localisation($idVille=0){
        $oville = $this->mdlville->getVilleById($idVille);
        if (count($oville)!=0) $data["cp"] =$oville->CodePostal; else $data["cp"] = "";

        echo $data["cp"];

    }


    function save() {
        if(!$this->ion_auth->logged_in()) {
            $this->session->set_userdata("cur_uri", $this->uri->uri_string());
            redirect();
        }
        $objAgenda = $this->input->post("Agenda");


        if (isset($_FILES["AgendaPhoto1"]["name"])) $photo1 =  $_FILES["AgendaPhoto1"]["name"];
        if(isset($photo1) && $photo1 !=""){
            $photo1Associe = $photo1;
            $AgendaPhoto1 = doUploadResize("AgendaPhoto1","application/resources/front/images/agenda/photoCommercant/",$photo1Associe);
            if (file_exists("application/resources/front/images/agenda/photoCommercant/".$AgendaPhoto1)== true) {
                image_thumb("application/resources/front/images/agenda/photoCommercant/" .$AgendaPhoto1, 200, 200,'','');
            }
        }else {
            $photo1Associe = $this->input->post("photo1Associe");
            $AgendaPhoto1 = $photo1Associe;
        }


        if (isset($_FILES["AgendaPhoto2"]["name"])) $photo2 =  $_FILES["AgendaPhoto2"]["name"];
        if(isset($photo2) && $photo2 !=""){
            $photo2Associe = $photo2;
            $AgendaPhoto2 = doUploadResize("AgendaPhoto2","application/resources/front/images/agenda/photoCommercant/",$photo2Associe);
            if (file_exists("application/resources/front/images/agenda/photoCommercant/".$AgendaPhoto2)== true) {
                image_thumb("application/resources/front/images/agenda/photoCommercant/" .$AgendaPhoto2, 200, 200,'','');
            }
        }else {
            $photo2Associe = $this->input->post("photo2Associe");
            $AgendaPhoto2 = $photo2Associe;
        }


        if (isset($_FILES["AgendaPhoto3"]["name"])) $photo3 =  $_FILES["AgendaPhoto3"]["name"];
        if(isset($photo3) && $photo3 !=""){
            $photo3Associe = $photo3;
            $AgendaPhoto3 = doUploadResize("AgendaPhoto3","application/resources/front/images/agenda/photoCommercant/",$photo3Associe);
            if (file_exists("application/resources/front/images/agenda/photoCommercant/".$AgendaPhoto3)== true) {
                image_thumb("application/resources/front/images/agenda/photoCommercant/" .$AgendaPhoto3, 200, 200,'','');
            }
        }else {
            $photo3Associe = $this->input->post("photo3Associe");
            $AgendaPhoto3 = $photo3Associe;
        }


        if (isset($_FILES["AgendaPhoto4"]["name"])) $photo4 =  $_FILES["AgendaPhoto4"]["name"];
        if(isset($photo4) && $photo4 !=""){
            $photo4Associe = $photo4;
            $AgendaPhoto4 = doUploadResize("AgendaPhoto4","application/resources/front/images/agenda/photoCommercant/",$photo4Associe);
            if (file_exists("application/resources/front/images/agenda/photoCommercant/".$AgendaPhoto4)== true) {
                image_thumb("application/resources/front/images/agenda/photoCommercant/" .$AgendaPhoto4, 200, 200,'','');
            }
        }else {
            $photo4Associe = $this->input->post("photo4Associe");
            $AgendaPhoto4 = $photo4Associe;
        }


        if (isset($_FILES["AgendaPhoto5"]["name"])) $photo5 =  $_FILES["AgendaPhoto5"]["name"];
        if(isset($photo5) && $photo5 !=""){
            $photo5Associe = $photo5;
            $AgendaPhoto5 = doUploadResize("AgendaPhoto5","application/resources/front/images/agenda/photoCommercant/",$photo5Associe);
            if (file_exists("application/resources/front/images/agenda/photoCommercant/".$AgendaPhoto5)== true) {
                image_thumb("application/resources/front/images/agenda/photoCommercant/" .$AgendaPhoto5, 200, 200,'','');
            }
        }else {
            $photo5Associe = $this->input->post("photo5Associe");
            $AgendaPhoto5 = $photo5Associe;
        }


        if(isset($_FILES["AgendaPdf"]["name"])) $pdf =  $_FILES["AgendaPdf"]["name"];
        if(isset($pdf) && $pdf !=""){
            $pdfAssocie = $pdf;
            $pdfCom = doUpload("AgendaPdf","application/resources/front/images/agenda/pdf/",$pdfAssocie);
        }else {
            $pdfAssocie = $this->input->post("pdfAssocie");
            $pdfCom = $pdfAssocie;
        }

        $objAgenda["pdf"] = $pdfCom;
        $objAgenda["photo1"] = $AgendaPhoto1;
        $objAgenda["photo2"] = $AgendaPhoto2;
        $objAgenda["photo3"] = $AgendaPhoto3;
        $objAgenda["photo4"] = $AgendaPhoto4;
        $objAgenda["photo5"] = $AgendaPhoto5;

        $objAgenda["last_update"] = date("Y-m-d");

        $objAgenda["date_debut"] = convert_Frenchdate_to_Sqldate($objAgenda["date_debut"]);
        $objAgenda["date_fin"] = convert_Frenchdate_to_Sqldate($objAgenda["date_fin"]);
        $objAgenda["date_depot"] = convert_Frenchdate_to_Sqldate($objAgenda["date_depot"]);

        if ($objAgenda["id"] == "" || $objAgenda["id"] == null || $objAgenda["id"] == "0") {
            $IdUpdatedAgenda = $this->mdl_agenda->insert($objAgenda);
        } else {
            $IdUpdatedAgenda = $this->mdl_agenda->update($objAgenda);
        }


        redirect("admin/agenda/index/");
    }

public function save_id_festival(){
   $data['objfest']=$this->mdl_agenda->get_festby_id_commercant();


}

/**hh*/

}
