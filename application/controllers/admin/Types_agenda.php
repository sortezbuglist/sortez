<?php
class types_agenda extends CI_Controller{

    function __construct() {
        parent::__construct();

        $this->load->library('session');
        $this->load->library('ion_auth');
        $this->load->model("mdl_types_agenda");
        $this->load->model("ion_auth_used_by_club");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }

    function index(){
        $this->liste();
    }

    function liste() {
        $objagendas = $this->mdl_types_agenda->GetAll();
        $data["colagendas"] = $objagendas;

        if($this->session->flashdata('mess_editTypeagenda')=='1') $data['msg'] = '<strong style="color:#060">Type enregistré !</strong>';
        if($this->session->flashdata('mess_editTypeagenda')=='2') $data['msg'] = '<strong style="color:#F00">Ce type d\'agenda ne peut être supprimé ! Type lié à une catégorie !</strong>';
        if($this->session->flashdata('mess_editTypeagenda')=='3') $data['msg'] = '<strong style="color:#060">Type supprimé !</strong>';

        $this->load->view("admin/vwListeTypeagenda",$data);
    }

    function fiche_types_agenda($IdTypeagenda) {

        if ($IdTypeagenda != 0){
            $data["title"] = "Modification Type agenda" ;
            $data["oagenda"] = $this->mdl_types_agenda->getById($IdTypeagenda) ;
            $data["agenda_typeid"] = $IdTypeagenda;
            $this->load->view('admin/vwFicheTypeagenda', $data) ;

        }else{
            $data["title"] = "Creer Type agenda" ;
            $this->load->view('admin/vwFicheTypeagenda', $data) ;
        }
    }

    function creer_types_agenda(){
        $oagenda = $this->input->post("types_agenda") ;
        $this->mdl_types_agenda->insertagenda_type($oagenda);
        $data["msg"] = "Type agenda ajouté" ;

        $objagendas = $this->mdl_types_agenda->GetAll();
        $data["colagendas"] = $objagendas;
        $this->load->view("admin/vwListeTypeagenda",$data);
    }

    function modif_types_agenda($IdTypeagenda){
        $oagenda = $this->input->post("types_agenda") ;
        $this->mdl_types_agenda->updateagenda_type($oagenda);
        $data["msg"] = "Type agenda enregistré" ;

        $objagendas = $this->mdl_types_agenda->GetAll();
        $data["colagendas"] = $objagendas;
        $this->load->view("admin/vwListeTypeagenda",$data);
    }

    function delete($prmId) {

        if(is_numeric(trim($prmId))) {
            //verify if category contain active agenda********
            $oCategorie = $this->mdl_types_agenda->verifier_type_agenda($prmId) ;
            //$oCategorie = array();
            if (count($oCategorie)==0){
                $this->mdl_types_agenda->supprimeagenda_type($prmId);
                $this->session->set_flashdata('mess_editTypeagenda', '3');
            } else {
                $this->session->set_flashdata('mess_editTypeagenda', '2');
            }

            //$this->firephp->log($oCategorie, 'oCategorie');

            redirect("admin/types_agenda");
        }
        else {
            redirect("admin/types_agenda");
        }

    }


}