<?php

class Info_menu_commercant extends CI_Controller

{

    function __construct()

    {

        parent::__construct();

        $this->load->model("mdlannonce");

        $this->load->model("mdlcommercant");

        $this->load->model("mdlbonplan");

        $this->load->model("rubrique");

        $this->load->model("Sousrubrique");

        $this->load->model("mdlimagespub");

        $this->load->model("AssCommercantAbonnement");

        $this->load->model("mdlville");

        $this->load->model("mdldepartement");

        $this->load->model("mdlfidelity");

        $this->load->library('user_agent');

        $this->load->model("Abonnement");

        $this->load->model("commercant");

        $this->load->Model("mdlannonce_perso");

        $this->load->model("mdlarticle");

        $this->load->model("Mdlcommercant");

        $this->load->model("mdl_agenda");

        $this->load->model("mdlcommune");

        $this->load->model("Mdl_menu");

        $this->load->model("mdl_categories_annonce");

        $this->load->model("Mdl_plat_du_jour");

        $this->load->library('session');

        $this->load->Model("mdl_localisation");

        $this->load->library("pagination");

        $this->load->library('image_moo');

        $this->load->library('ion_auth');

        $this->load->model("ion_auth_used_by_club");
        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");
        $group_proo_club = array(3, 4, 5,7);
        if ($this->ion_auth->is_admin()) {
            redirect('admin/home', 'refresh');
        } else if ($this->ion_auth->in_group(2)) {
            redirect('front/utilisateur/menuconsommateurs', 'refresh');
        } else if ($this->ion_auth->in_group($group_proo_club)) {
            //        redirect('front/utilisateur/contenupro', 'refresh');
        } else {
            redirect('/', 'refresh');
        }
    }

    function index(){
        $user_ion_auth = $this->ion_auth->user()->row();
        $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $data['infocom'] = $this->mdlcommercant->infocommercant($iduser);
        $data_commande = $this->Mdl_menu->get_menu_data_by_idcom($iduser);
        $data['data_gli1'] = $this->Mdl_menu->getdatagli1($iduser);
        $data['data_gli2'] = $this->Mdl_menu->getdatagli2($iduser);
        $data['data_gli3'] = $this->Mdl_menu->getdatagli3($iduser);
        $data['data_gli4'] = $this->Mdl_menu->getdatagli4($iduser);
        $data['data_gli6'] = $this->Mdl_menu->getdatagli6($iduser);
        $data['data_gli7'] = $this->Mdl_menu->getdatagli7($iduser);

        $data['title_gli1'] = $this->Mdl_menu->gettitlegli1($iduser);
        $data['title_gli2'] = $this->Mdl_menu->gettitlegli2($iduser);
        $data['title_gli3'] = $this->Mdl_menu->gettitlegli3($iduser);
        $data['title_gli4'] = $this->Mdl_menu->gettitlegli4($iduser);
        $data['title_gli6'] = $this->Mdl_menu->gettitlegli6($iduser);
        $data['title_gli7'] = $this->Mdl_menu->gettitlegli7($iduser);

        $data['user_ion_auth'] = $user_ion_auth->id;
        $data['data_menu'] = $data_commande;
        $data['idcom'] = $iduser;
        $this->load->view('admin/menu/vwfiche_info_menu_commercant',$data);
    }

    public function save_info_menu_commercant(){
        $user_ion_auth = $this->ion_auth->user()->row();
        $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $alldata = $this->input->post("data");
        $alldata['condition_content'] = html_entity_decode($alldata['condition_content']);
        // print_r($alldata);die();
        // var_dump(strlen($alldata['menu_value'])); die();
        if(strlen($alldata['menu_value']) < 23){
            // echo 'Ok';
            $alldata['idCommercant'] = $iduser;
            $this->Mdl_menu->save_command_data($alldata);
            redirect('admin/menu/Info_menu_commercant');
        }else{
            // echo 'NON';
            $data['error_message'] = "Le texte doit être inférieur à 22 caractères";
            $this->load->view('admin/menu/vwfiche_info_menu_commercant',$data);
        }
        
    }
    public function add_line(){
        $current = $this->input->post('current');
        $nbgliss = $this->input->post('nbgliss');
        $data['nbgliss'] = $nbgliss;
        $data['current'] = (int)$current+1;
        $this->load->view('sortez_soutenons/includes/line_form',$data);
    }
    public function save_gli(){
        $title = $this->input->post('title');
        $activ = $this->input->post('activ');
        $nbgli = $this->input->post('nbgli');
        $idcom = $this->input->post('idcom');
        $comment = $this->input->post('comment');
        $field = array(
            'titre_glissiere' => $title,
            'is_activ_glissiere' =>$activ,
            'idCom' =>$idcom,
            "description_gli"=>$comment
        );
        echo "dfsdfs".$nbgli;
        $this->Mdl_menu->save_glissiere1($field,$nbgli);
        echo '1';
    }
    public function save_art(){
        $title = $this->input->post('titre');
        $true_title = $this->input->post('true_title');
        $prix = $this->input->post('prix');
        $idcom = $this->input->post('idCom');
        $idgli = $this->input->post('idgli');
        $indexs = $this->input->post('indexs');
        $id = $this->input->post('id');

        $fields = array(
            "titre" => $title,
            "prix" => $prix,
            "idCom" => $idcom,
            "id_glissiere" => $idgli,
            "id" => $id,
            "true_title" =>$true_title,
        );

        $idart = $this->Mdl_menu->save_article($fields);
        $sites = "'".site_url("media/index/".$idart."-menugli1-photo".$idgli.$indexs)."','','width=1045, height=675, scrollbars=yes'";
        $return =array(
            "data"=>'<div id="Articlephoto'.$idgli.$indexs.'_container" onclick="javascript:window.open('.$sites.');" href="javascript:void(0);" class="w-100 img_add "><img class="w-100 h-100" src="'.base_url().'assets/images/download-icon-png.webp"></div>',
            "id" =>$idart,
        );
        echo json_encode($return);

    }
    public function save_paypal_data(){
        $is_active = $this->input->post('is_activ');
        $txt_paypal = $this->input->post('txt_paypal');
        $idcom = $this->input->post('idcom');
//        $pay_dom = $this->input->post("pay_dom");
//        $comment_livr_enlev = $this->input->post("comment_livr_enlev");
        $new_paypal_comment = $this->input->post("new_paypal_comment");

        $is_activ_comm_enlev = $this->input->post("is_activ_comm_enlev");
        $is_activ_com_spec = $this->input->post("is_activ_com_spec");
        $is_activ_comm_livr_dom = $this->input->post("is_activ_comm_livr_dom");
        $is_activ_comment_bottom = $this->input->post("is_activ_comment_bottom");
        $is_activ_card_bank_bottom = $this->input->post("is_activ_card_bank_bottom");
        $is_activ_cheque_bottom = $this->input->post("is_activ_cheque_bottom");
        $comment_comm_spec_bottom_txt = $this->input->post("comment_comm_spec_bottom_txt");


        $field = array(
            "is_activ_paypal" => $is_active,
            "paypal_content" => $txt_paypal,
            "idCommercant" => $idcom,
//            "pay_domicile" => $pay_dom,
//            "comment_livr_enlev" => $comment_livr_enlev,
            "new_paypal_comment" => $new_paypal_comment,
            "is_activ_comm_livr_dom" => $is_activ_comm_livr_dom,
            "is_activ_comm_enlev" => $is_activ_comm_enlev,
            "is_activ_com_spec" => $is_activ_com_spec,
            "is_activ_comment_bottom" => $is_activ_comment_bottom,
            "is_activ_card_bank_bottom" => $is_activ_card_bank_bottom,
            "is_activ_cheque_bottom" => $is_activ_cheque_bottom,
            "comment_comm_spec_bottom_txt" => $comment_comm_spec_bottom_txt,
        );
        $this->Mdl_menu->save_paypal_data($field);
    }
    public function delete_image(){
        $id_article = $this->input->post('id_art');
        $data_art = $this->Mdl_menu->get_art_by_id($id_article);
        if (isset($data_art) AND count($data_art) !=0){
            $image = $data_art->image;
            $idcom = $data_art->idCom;
            $infocom = $this->mdlcommercant->infoCommercant($idcom);
            $id_ionauth = $infocom->user_ionauth_id;
        }
        if (isset($image) AND isset($id_ionauth)){
            $this->Mdl_menu->delete_image_by_id($id_article);
            echo '1';
        }else{
            return '0';
        }
    }
    public function delete_image_menu_gen(){
        $idcom = $this->input->post('idcom');
        $data_com_menu = $this->Mdl_menu->get_menu_data_by_idcom($idcom);
        $infocom = $this->mdlcommercant->infoCommercant($idcom);
        if (is_file("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/menu_icon/".$data_com_menu->image_menu_gen)){
            unlink("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/menu_icon/".$data_com_menu->image_menu_gen);
        }
        $this->Mdl_menu->delete_image_menu_gen_by_idcom($idcom);
        echo '1';
    }
    public function delete_article(){
        $id_art = $this->input->post('id_art');
        $this->Mdl_menu->delete_art_by_id($id_art);
        echo '1';
    }
    public function delete_cgv(){
        $user_ion_auth = $this->ion_auth->user()->row();
        $cgv = $this->input->post('cgv');
        $id_com = $this->input->post('id_com');
        $this->Mdl_menu->delete_cgv($cgv,$id_com);
        if(unlink("application/resources/front/photoCommercant/cgv/".$user_ion_auth->id."/".$cgv)){
            echo '1';
        }else{
            echo "0";
        }

    }
    public function delete_cgv_livraison(){
        $user_ion_auth = $this->ion_auth->user()->row();
        $cgv = $this->input->post('cgv');
        $id_com = $this->input->post('id_com');
        $this->Mdl_menu->delete_cgv_livraison($cgv,$id_com);
        if(unlink("application/resources/front/photoCommercant/cgv/".$user_ion_auth->id."/".$cgv)){
            echo '1';
        }else{
            echo "0";
        }

    }
    public function delete_cgv_differe(){
        $user_ion_auth = $this->ion_auth->user()->row();
        $cgv = $this->input->post('cgv');
        $id_com = $this->input->post('id_com');
        $this->Mdl_menu->delete_cgv_differe($cgv,$id_com);
        if(unlink("application/resources/front/photoCommercant/cgv/".$user_ion_auth->id."/".$cgv)){
            echo '1';
        }else{
            echo "0";
        }

    }
    public function delete_cgv_specif(){
        $user_ion_auth = $this->ion_auth->user()->row();
        $cgv = $this->input->post('cgv');
        $id_com = $this->input->post('id_com');
        $this->Mdl_menu->delete_cgv_specif($cgv,$id_com);
        if(unlink("application/resources/front/photoCommercant/cgv/".$user_ion_auth->id."/".$cgv)){
            echo '1';
        }else{
            echo "0";
        }

    }
    public function delete_doc(){
        $user_ion_auth = $this->ion_auth->user()->row();
        $doc = $this->input->post('doc');
        $id_com = $this->input->post('id_com');
        $this->Mdl_menu->delete_doc($doc,$id_com);
        if(unlink("application/resources/front/photoCommercant/doc/".$user_ion_auth->id."/".$doc)){
            echo '1';
        }else{
            echo "0";
        }

    }
    public function get_ville_by_codepostal(){
        $codepostal = $this->input->post('code_postal_coms');
        //var_dump($codepostal);
        $resultat = $this->mdlville->GetVilleByCodePostal_localisation_res($codepostal);
        echo json_encode($resultat[0]);
    }
    public function get_nom_by_id(){
        $idville = $this->input->post('idville');
        $result = $this->mdlville->getVilleById($idville);
        echo json_encode($result);
    }
    public function get_all_data_coms(){
        $data['IdCommercant'] = $this->input->post('idcommercant');
        $data['NomSociete'] = $this->input->post('nomsociete');
        $data['adresse_localisation'] = $this->input->post('adresse');
        $data['CodePostal'] = $this->input->post('codepostal');
        $data['IdVille'] = $this->input->post('ville_id');
        $data['Email'] = $this->input->post('courriel_coms');
        $data['TelMobile'] = $this->input->post('numero_telephone');
        $data['Siret'] = $this->input->post('siret');
        $data['code_ape'] = $this->input->post('code_ape');
        $data['Nom'] = $this->input->post('nom_responsable');
        $data['Prenom'] = $this->input->post('prenom_responsable');
        $data['Responsabilite'] = $this->input->post('titre_responsable');
        //var_dump($data);die();
        if($this->commercant->Update($data)){
            echo "ok";
        }else{
            echo "ko";
        }
    }
    public function generate_qr_code(){
        $user_ion_auth = $this->ion_auth->user()->row();
        $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $infocom = $this->mdlcommercant->infocommercant($iduser);
        $to_generate = $this->input->post('qr_code_img');
        $id_menu = $this->input->post('id_menu_com');
        $base_path_system = str_replace('system/', '', BASEPATH);
        $qrcode_lib_path = $base_path_system."application/resources/phpqrcode/";
        $qrcode_lib_path_true = $base_path_system."application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/menu_qr_code/";
        if (!is_dir("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/")){
            mkdir("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id.'/',0777);
        }
        if (!is_dir("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/menu_qr_code/")){
            mkdir("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/menu_qr_code/",0777);
        }
        $qrcode_lib_images_url = base_url()."application/resources/phpqrcode/images/";

        include($qrcode_lib_path.'qrlib.php');

        $tempDir = $qrcode_lib_path_true;
        $tempURL = $qrcode_lib_images_url;

        $codeContents = $to_generate;

        $fileName = 'qrcode_menu_file_'.md5($codeContents).'.png';

        $pngAbsoluteFilePath = $tempDir.$fileName;

        if (!file_exists($pngAbsoluteFilePath)) {
            QRcode::png($codeContents, $pngAbsoluteFilePath, QR_ECLEVEL_L, 4);

            $field = array(
                'url_qr_code_menu' =>$fileName,
            );
            $this->Mdl_menu->Insert_qr_code_menu($field,$id_menu);
        } else {
            unlink($pngAbsoluteFilePath);
            QRcode::png($codeContents, $pngAbsoluteFilePath, QR_ECLEVEL_L, 4);

            $field = array(
                'url_qr_code_menu' =>$fileName,
            );
            $this->Mdl_menu->Insert_qr_code_menu($field,$id_menu);
        }
        redirect("admin/menu/Info_menu_commercant");
    }

}