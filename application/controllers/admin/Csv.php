<?php

class csv extends CI_Controller 
{
    function __construct()
    {
        parent::__construct();
        $this->load->Model("Commercant");
        $this->load->Model("Abonnement");
        $this->load->model("ion_auth_used_by_club");
        $this->load->library('session');
        $this->load->library('ion_auth');
        $this->load->model("mdldemande_abonnement");
        $this->load->Model("Mdlimportcommercant");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }

    function index()
    {
        $this->import_csv_client();
    }

    public function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function import_csv_client($suucees=0,$echec=0){
        $data['infocom']=$infocom;
        $data['suucees']=$suucees;
        $data['echec']=$echec;
        $this->load->view('admin/vwcsv',$data);
    }

    public function verify_csv_file(){
        $suucees= 0;
        $echec= 0;
        if (isset($_FILES["file"])){
            $file=$_FILES["file"]["tmp_name"];
            // The nested array to hold all the arrays
            $the_big_array = [];
            // Open the file for reading
            //var_dump($_FILES["file"]);
            if (($h = fopen("{$file}", "r")) !== FALSE)
            {
                // Each line in the file is converted into an individual array that we call $data
                // The items of the array are comma separated
                while (($data = fgetcsv($h, 10500, ";")) !== FALSE)
                {
                    // Each individual array is being pushed into the nested array
                    $the_big_array[] = $data;
                }
    
                // Close the file
                fclose($h);
                // var_dump($the_big_array);die();
                
                $AssCommercantRubrique = [];
                $Societe = [];
                $Autre = [];
                // $the_big_array = [];
                $j = 0;
                for($i = 1; $i < count($the_big_array); $i++){
                    // $the_big_array[$i] = explode(";",$the_big_array[$i][0]);
                    $j++; 
                    // for($j = 0; $j < count($the_big_array[$i]); $j++){
                        $AssCommercantRubrique[$j]["IdRubrique"]=$the_big_array[$i][0];
                        $objAssCommercantSousRubrique["IdSousRubrique"]=$the_big_array[$i][1];
                        $Societe[$j]["titre_entete"] = $the_big_array[$i][2];
                        $Societe[$j]["NomSociete"]=$the_big_array[$i][3];
                        $Societe[$j]["Adresse1"]=$the_big_array[$i][4];

                        $Societe[$j]["limit_article"] = 5;
                        $Societe[$j]["limit_agenda"] = 5;
                        $Societe[$j]["limit_annonce"] = 5;

                        $Societe[$j]["Adresse2"]=$the_big_array[$i][5];
                        $Societe[$j]["CodePostal"]=$the_big_array[$i][6];
                        $Societe[$j]["departement_id"]=$the_big_array[$i][7];
                        $Societe[$j]["IdVille"]=$the_big_array[$i][8];
                        $Societe[$j]["TelFixe"]=$the_big_array[$i][9];
                        $Societe[$j]["TelMobile"]=$the_big_array[$i][10];
                        $Societe[$j]["Email"]=$the_big_array[$i][11];
                        $Societe[$j]["Civilite"]=$the_big_array[$i][12];
                        $Societe[$j]["Nom"]=$the_big_array[$i][13];
                        $Societe[$j]["Prenom"]=$the_big_array[$i][14];
                        $Societe[$j]["Responsabilite"]=$the_big_array[$i][15];
                        $Societe[$j]["TelDirect"]=$the_big_array[$i][16];
                        $Societe[$j]["Email_decideur"]=$the_big_array[$i][17];
                        $Societe[$j]["Login"]=$the_big_array[$i][18];
                        $AssAbonnementCommercant[$j]["IdAbonnement"]=$the_big_array[$i][19];
                        // $societe[$j]["IsActif"]=$the_big_array[$i][20];
                    // }
                    // var_dump($Societe);die();
                    // if($j == count($the_big_array[$i])){
                    //     $j = 0;
                    // }
                    $objCommercant = $Societe[$j];
                    $objCommercant["IdVille"] = $Societe[$j]["IdVille"];
                    $objCommercant["NomSociete"] = $Societe[$j]["NomSociete"];
                    $objCommercant["IdVille_localisation"]=$objCommercant["IdVille"];
                    if (!$objCommercant) {
                        echo "<p style = 'color: red;'>Donnée non-recupéré sur le fichier, ligne :".($i+1). "</p><hr>";
                        continue;
                    }
                    $societe_Password = $this->randomPassword();

                    // start create nom_url *******************************************************************************
                        $this->load->model("mdlville");
                        $commercant_url_ville = $this->mdlville->getVilleById($objCommercant["IdVille"]);
                        $commercant_url_nom_com = RemoveExtendedChars2($objCommercant["NomSociete"]);
                        $commercant_url_nom_ville = RemoveExtendedChars2($commercant_url_ville->Nom);
                        $commercant_url_nom = $commercant_url_nom_com . "_" . $commercant_url_nom_ville;
                        $commercant_url_nom = $this->Commercant->setUrlFormat($commercant_url_nom, 0);
                        $objCommercant["nom_url"] = $commercant_url_nom;
                        $objCommercant["nom_url_vsv"] = $commercant_url_nom;
                    // end create nom_url *********************************************************************************
                    //record datecreation
                    $objCommercant["datecreation"] = time();

                    if ($objCommercant["IdVille"] == 0) {
                        echo "<p style = 'color: red;'>Donnée non-recupéré sur le fichier, ligne :".($i+1). "</p><hr>";
                        continue;
                    }

                    $objAssCommercantRubrique['IdRubrique'] = $AssCommercantRubrique[$j]["IdRubrique"];
                    // $objAssCommercantSousRubrique = $this->input->post("AssCommercantSousRubrique");
                    $objAssCommercantAbonnement["IdAbonnement"] = $AssAbonnementCommercant[$j]["IdAbonnement"];

                    //ion_auth register *****************
                    $username_ion_auth = $Societe[$j]["Login"];
                    $password_ion_auth = $societe_Password;
                    $email_ion_auth = $Societe[$j]["Email"];
                    $additional_data_ion_auth = array('first_name' => $objCommercant['NomSociete'], 'company' => $objCommercant['NomSociete'], 'phone' => $Societe[$j]["TelFixe"]);
                    $abonnement_verify_ionauth = $this->Abonnement->GetById($objAssCommercantAbonnement["IdAbonnement"]);
                    $value_abonnement_ionauth = '3';//sets default group to basic
                    // if ($abonnement_verify_ionauth->type == "gratuit") $value_abonnement_ionauth = '3';
                    // if ($abonnement_verify_ionauth->type == "premium") $value_abonnement_ionauth = '4';
                    // if ($abonnement_verify_ionauth->type == "platinum") $value_abonnement_ionauth = '5';
                    $group_ion_auth = array($value_abonnement_ionauth);


                    $willll_test = $this->ion_auth->register($username_ion_auth, $password_ion_auth, $email_ion_auth, $additional_data_ion_auth, $group_ion_auth);
                    $ion_ath_error = $this->ion_auth->errors();

                    //var_dump($ion_ath_error);

                    //var_dump($objCommercant);

                    ////$this->firephp->log($ion_ath_error, 'ion_ath_error');
                    $user_lastid_ionauth = $this->ion_auth_used_by_club->GetUserlast_id($objCommercant['NomSociete'], $username_ion_auth, $email_ion_auth);
                    $objCommercant["user_ionauth_id"] = $user_lastid_ionauth;
                    //var_dump($user_lastid_ionauth);
                    //die("STOP");
                    //ion_auth register ******************

                    if (isset($ion_ath_error) && $ion_ath_error != '' && $ion_ath_error != NULL && !$user_lastid_ionauth) {

                        echo "Une erreur s'est produite durant l'enregistrement de vos données. Veuillez recommencer s'il vous plait.";
                       continue; 
                    } else {

                        $this->load->Model("Commercant");
                            $objCommercant["blog"] = 1;
                            $objCommercant["agenda"] = 1;
                            $objCommercant["bonplan"] = 0;
                            $objCommercant["annonce"] = 1;
                            $objCommercant["limit_bonplan"] = 5;
                            $objCommercant['referencement_annuaire'] = 1;
                            $objCommercant['annonce'] = 1;
                            $objCommercant['agenda'] = 1;
                            $objCommercant['blog'] = 1;
                            $objCommercant['bonplan'] = 1;
                            $objCommercant['referencement_resto'] = 1;
                            $objCommercant['IsActif'] = 1;

                        //var_dump($objCommercant["bonplan"]);die();
                        $IdInsertedCommercant = $this->Commercant->Insert($objCommercant);
                        

                        $this->load->Model("AssCommercantRubrique");
                        $objAssCommercantRubrique["IdCommercant"] = $IdInsertedCommercant;

                        // var_dump($objAssCommercantRubrique);die();
                        $this->AssCommercantRubrique->Insert($objAssCommercantRubrique);

                        $this->load->Model("AssCommercantSousRubrique");
                        $objAssCommercantSousRubrique["IdCommercant"] = $IdInsertedCommercant;
                        $this->AssCommercantSousRubrique->Insert($objAssCommercantSousRubrique);

                        $this->load->Model("AssCommercantAbonnement");
                        $objAssCommercantAbonnement["DateDebut"] = "2022-02-01";
                        $objAssCommercantAbonnement["DateFin"] = "2023-02-01";
                        $objAssCommercantAbonnement["IdCommercant"] = $IdInsertedCommercant;
                        $this->AssCommercantAbonnement->Insert($objAssCommercantAbonnement);

                        if (!empty($IdInsertedCommercant)) {


                            //save demande abonnement--------------------------------------
                            // $demande_abonnement = $this->input->post("demande_abonnement");
                            $colAbonnements = $this->Abonnement->GetAll();
                            $abonnement_asked = array();
                            //var_dump($demande_abonnement);

                            foreach ($colAbonnements as $objAbonnements) {
                                $array_demande_abonnement = array('id' => NULL, 'idAbonnement' => $objAbonnements->IdAbonnement, 'idCommercant' => $IdInsertedCommercant, 'idUser_ionauth' => $user_lastid_ionauth);
                                $IdAbonnementExist_on_database = $this->mdldemande_abonnement->IdAbonnementExist($objAbonnements->IdAbonnement, $IdInsertedCommercant, $user_lastid_ionauth);
                                $IdAbonnementExist_on_demande = in_array($objAbonnements->IdAbonnement, $abonnement_asked);
                                if ($IdAbonnementExist_on_database && $IdAbonnementExist_on_demande) {
                                } elseif (!$IdAbonnementExist_on_database && $IdAbonnementExist_on_demande) {
                                    $insert_dmd_abonnement = $this->mdldemande_abonnement->insert($array_demande_abonnement);
                                } elseif (!$IdAbonnementExist_on_database && !$IdAbonnementExist_on_demande) {
                                    $this->mdldemande_abonnement->delete($objAbonnements->IdAbonnement);
                                }
                            }
                            //die("STOP");
                            // $this->load->view("privicarte/vwConfirmationInscriptionProfessionnels", $data);
                            $this->Mdlimportcommercant->insert($username_ion_auth,$societe_Password);
                            
                            // echo "<div><p style = 'margin: 0;'>LOGIN: ".$username_ion_auth."</p><br><p style = 'margin: 0;'>PASSWORD: ".$societe_Password."</p></div><hr>";
                        } else {
                            echo "<div><p style = 'color: red;'>Erreur sur".$objCommercant['NomSociete']." | ".$email_ion_auth."</p></div><hr>";
                        }
                    }
                    // var_dump($email,$nom,$adresse1,$adresse2,$codepostal,$ville,$tel1,$tel2,$fax,$activite,$activite1,$activite2,$activite3,$activite4);
                    // var_dump($AssCommercantRubrique,$Societe,$AssAbonnementCommercant);
                
                    // redirect('admin/csv/import_csv_client/'.$suucees.'/'.$echec.'/'.$idcom);
                }
            }
        }
        echo "OK";
    }
}