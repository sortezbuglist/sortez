<?php
class Articles extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model("user");
        $this->load->library('user_agent');
        $this->load->model("sousRubrique");
        $this->load->model("Rubrique");
        $this->load->model("AssCommercantAbonnement");
        $this->load->model("AssCommercantSousRubrique");
        $this->load->model("Abonnement");
        $this->load->model("mdlannonce");
        $this->load->model("mdlbonplan");
        $this->load->model("Abonnement");
        $this->load->model("mdlarticle");
        $this->load->model("mdlville");
        $this->load->model("mdldepartement");
        $this->load->model("mdlcommune");
        $this->load->model("mdl_localisation");

        $this->load->model("Commercant");
        $this->load->model("mdl_agenda");

        $this->load->model("mdl_types_article");
        $this->load->model("mdl_categories_article");
        $this->load->model("mdl_categories_agenda");
        $this->load->model("mdl_agenda_datetime");

        $this->load->model("mdl_article_datetime");
        $this->load->model("mdl_article_redacteur");
        $this->load->model("mdl_article_organiser");
        $this->load->model("mdl_article_validation_email");
        $this->load->model("mdl_article_agenda_ref");




        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");

        $group_proo_club = array(3, 4, 5);//only customer & basic user cannot add article
        if (!$this->ion_auth->logged_in()) {
            redirect("auth/login");
        } else if (!$this->ion_auth->in_group($group_proo_club)) {
            $this->session->set_flashdata('domain_from', '1');
            redirect();
        }

        check_vivresaville_id_ville();

    }

    function index() {
        $this->liste();
    }

    function liste() {


        $objarticle = $this->mdlarticle->GetAll();
        //$this->firephp->log($objarticle, 'objArticles');
        $data["colArticles"] = $objarticle;

        $this->load->view("admin/vwListeArticles",$data);
    }

    function fiche($prmIdCommercant = 0, $IdArticle = 0, $mssg = 0)
    {



        // check article limit number
        if ($prmIdCommercant != 0 && $IdArticle == 0) {
            $toListeArticles__ = $this->mdlarticle->GetByIdCommercant($prmIdCommercant);
            $objCommercant__ = $this->Commercant->GetById($prmIdCommercant);
            if (isset($objCommercant__->limit_articles) && intval($objCommercant__->limit_articles) != 0 && $objCommercant__->limit_articles != "" && count($toListeArticles__) > intval($objCommercant__->limit_articles)) {
                redirect("front/articles/liste/" . $prmIdCommercant);
            }
        }

        if ($this->ion_auth->logged_in() && ($prmIdCommercant == 0 || $prmIdCommercant == "")) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $prmIdCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        }

        //verify if $IdArticle, $prmIdCommercant and $user_ion_auth->id are corrects and match between them
        if ($IdArticle != 0 && $IdArticle != "" && $IdArticle != null && is_numeric($IdArticle) == true) {
            $objArticle_verification = $this->mdlarticle->GetById($IdArticle);
            if (isset($objArticle_verification) && count($objArticle_verification) != 0) {
                if ($objArticle_verification->IdCommercant != $prmIdCommercant && $objArticle_verification->IdUsers_ionauth != $user_ion_auth->id) {
                    redirect("front/utilisateur/no_permission");
                }
            } else redirect("front/utilisateur/no_permission");
        }

        $data = null;

        //start verify if merchant subscription is right WILLIAM
        $_iDCommercant = $prmIdCommercant;
        $current_date = date("Y-m-d");
        $query_abcom_cond = " IdCommercant = " . $_iDCommercant . " AND ('" . $current_date . "' BETWEEN DateDebut AND DateFin)";
        $toAbonnementCommercant = $this->AssCommercantAbonnement->GetWhere($query_abcom_cond, 0, 1);
        if (sizeof($toAbonnementCommercant)) {
            foreach ($toAbonnementCommercant as $oAbonnementCommercant) {
                if (isset($oAbonnementCommercant) && ($oAbonnementCommercant->IdAbonnement == 1 || $oAbonnementCommercant->IdAbonnement == 2)) {//IdAbonnement doesn't contain annonce
                    //redirect ("front/professionnels/fiche/$_iDCommercant") ;
                    $data['show_annonce_btn'] = 0;
                } else $data['show_annonce_btn'] = 1;

                if (isset($oAbonnementCommercant) && ($oAbonnementCommercant->IdAbonnement == 1 || $oAbonnementCommercant->IdAbonnement == 3)) {//IdAbonnement doesn't contain bonplan
                    //redirect ("front/professionnels/fiche/$_iDCommercant") ;
                    $data['show_bonplan_btn'] = 0;
                } else $data['show_bonplan_btn'] = 1;
            }
        } else {
            $data['show_annonce_btn'] = 0;
            $data['show_bonplan_btn'] = 0;
            //$this->load->view("front/vwFicheProfessionnelError",$data);
            redirect("front/professionnels/ficheError/");
            //exit();
        }
        //end verify if merchant subscription is right WILLIAM

        $user_ion_auth = $this->ion_auth->user()->row();
        $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $data["userId"] = $commercant_UserId;

        $this->load->Model("Commercant");
        $this->load->Model("Rubrique");
        $this->load->Model("SousRubrique");
        $this->load->Model("Ville");
        $this->load->Model("Abonnement");
        $this->load->Model("AssCommercantRubrique");
        $this->load->Model("AssCommercantSousRubrique");
        $this->load->Model("AssCommercantAbonnement");

        $data["no_main_menu_home"] = "1";
        $data["no_left_menu_home"] = "1";


        $data["colVilles"] = $this->mdlville->GetAll();

        $data["objArticle"] = $this->mdlarticle->GetById($IdArticle);
        $data["objCommercant"] = $this->Commercant->GetById($commercant_UserId);

        $this->load->helper('ckeditor');

        //Ckeditor's configuration
        $data['ckeditor0'] = array(

            //ID of the textarea that will be replaced
            'id' => 'description',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor'),
                    array('Image')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );
//// OP END
        $data['ckeditor1'] = array(

            //ID of the textarea that will be replaced
            'id' => 'description_tarif',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor'),
                    array('Image')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor2'] = array(

            //ID of the textarea that will be replaced
            'id' => 'conditions_promo',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor'),
                    array('Image')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['home_link_club'] = 1;
        $data['mssg'] = $mssg;

        $data['jquery_to_use'] = "1.4.2";

        // $data["colTypeagenda"] = $this->mdl_types_article->GetAll();
        // $data["colCategorie"] = $this->mdl_categories_article->GetAll();
        //$data["colSousCategorie"] = $this->mdl_categories_article->GetAllSousrubrique();

        // article use agenda's categories and subcategories **********************************
        $data["colCategorie"] = $this->mdl_categories_agenda->GetAll();
        $data["colSousCategorie"] = $this->mdl_categories_agenda->GetAllSousrubrique();


        $data["colDepartement"] = $this->mdldepartement->GetAll();
        $data['mdldepartement'] = $this->mdldepartement;
        $data["colCommune"] = $this->mdlcommune->GetAll();
        $data['mdlcommune'] = $this->mdlcommune;

        $data['toListBonplan'] = $this->mdlbonplan->getListeBonPlan($prmIdCommercant);

        $objAbonnementCommercant = $this->Abonnement->GetLastAbonnementCommercant($prmIdCommercant);
        $data['objAbonnementCommercant'] = $objAbonnementCommercant;

        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($prmIdCommercant);
        if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
        $data["user_groups"] = $user_groups[0];

        $data['defaul_thumb_width'] = $this->config->item('defaul_thumb_width');
        $data['defaul_thumb_height'] = $this->config->item('defaul_thumb_height');

        $data['mdlville'] = $this->mdlville;

        $data["colDepartement"] = $this->mdldepartement->GetAll();
        $data['mdldepartement'] = $this->mdldepartement;
        //devoir
        $data["col_article_redacteur"] = $this->mdl_article_redacteur->getByIdCommercant($prmIdCommercant);

        $data["col_article_organiser"] = $this->mdl_article_organiser->getByIdCommercant($prmIdCommercant);

        $data["col_article_location"] = $this->mdl_localisation->getByIdCommercant($prmIdCommercant);

        $data["col_article_validation_email"] = $this->mdl_article_validation_email->getByIdCommercant($prmIdCommercant);

        $data['obonplan_IdComemrcant'] = $this->mdlbonplan->getListeBonPlan($prmIdCommercant);

        $data['article_datetime_list'] = $this->mdl_article_datetime->getById($IdArticle);

        // cheking referencement agenda
        $verif_articles_agenda_ref = $this->mdl_article_agenda_ref->getById($IdArticle);
        $data["verif_articles_agenda_ref"] = $verif_articles_agenda_ref;
        if (isset($verif_articles_agenda_ref->agenda_id) && $verif_articles_agenda_ref->agenda_id != null && $verif_articles_agenda_ref->agenda_id != '0') {
            # code...
            $verif_obj_agenda = $this->mdl_agenda->GetById($verif_articles_agenda_ref->agenda_id);
            $data["verif_obj_agenda"] = $verif_obj_agenda;
            if (isset($verif_obj_agenda) && is_object($verif_obj_agenda) && $verif_obj_agenda->IsActif != "3" && $verif_obj_agenda->IsActif != 3) {
                $data["existsarticles_agenda_ref"] = "1";
            }
        }
        $data['pagecategory'] = "admin_commercant";

        $this->load->view("admin/vwFichearticle", $data);


    }



    function modification(){
        $oArticle = $this->input->post("Article") ;

        if (isset($_FILES["ArticleAudio"]["name"])) $ArticleAudio =  $_FILES["ArticleAudio"]["name"];
        if(isset($ArticleAudio) && $ArticleAudio !=""){
            $ArticleAudioAssocie = $ArticleAudio;
            $input_file_name = 'ArticleAudio';
            $base_path_system = str_replace('system/', '', BASEPATH);
            $upload_path = $base_path_system."/application/resources/front/audios/";
            $audio_upload_data = UploadFilesSiteUser($input_file_name,$upload_path,$isThumbImage='FALSE',$allowed_types='mp3',$max_size='10000000',$max_width='1024',$max_height='768');
            $Audio = $audio_upload_data['file_name'];
        }else {
            $ArticleAudioAssocie = $this->input->post("ArticleAudioAssocie");
            $Audio = $ArticleAudioAssocie;
        }

        if (isset($_FILES["ArticleVideo"]["name"])) $ArticleVideo =  $_FILES["ArticleVideo"]["name"];
        if(isset($ArticleVideo) && $ArticleVideo !=""){
            $ArticleVideoAssocie = $ArticleVideo;
            $input_file_name_video = 'ArticleVideo';
            $base_path_system = str_replace('system/', '', BASEPATH);
            $upload_path_video = $base_path_system."/application/resources/front/videos/";
            $video_upload_data = UploadFilesSiteUser($input_file_name_video,$upload_path_video,$isThumbImage='FALSE',$allowed_types_video='mp4|avi|MP4',$max_size='10000000',$max_width='1024',$max_height='768');
            $Video = $video_upload_data['file_name'];
        }else {
            $ArticleVideoAssocie = $this->input->post("ArticleVideoAssocie");
            $Video = $ArticleVideoAssocie;
        }

        $oArticle['audio'] = $Audio;
        $oArticle['video'] = $Video;
        $oArticle['lastupdate'] = date("Y-m-d H:i:s");

        $oArticle["event_startdate"] = convert_Frenchdate_to_Sqldate($oArticle["event_startdate"]);
        $oArticle["event_enddate"] = convert_Frenchdate_to_Sqldate($oArticle["event_enddate"]);

        if($oArticle['idarticle']=="0") {
            $this->mdlarticle->insert($oArticle);
            $data["msg"] = "Article bien ajouté" ;
        } else {
            $this->mdlarticle->update($oArticle);
            $data["msg"] = "Article bien enregistré" ;
        }

        redirect("admin/articles/liste");

        /*foreach($video_upload_data as $key_array => $value_array)
        {
            echo $key_array." => ". $value_array;
        }
        echo "<p>".$ArticleVideo."</p>";
        //$this->firephp->log($_FILES, '_FILES');*/

    }

    function suppression($prmId=0) {
        $oArticle = $this->mdlarticle->getById($prmId) ;
        //echo $oArticle->audio." ".$oArticle->video;
        $base_path_system = str_replace('system/', '', BASEPATH);
        if (isset($oArticle->audio) && $oArticle->audio != "" && $oArticle->audio != NULL && $oArticle->audio != "0") unlink($base_path_system."application/resources/front/audios/".$oArticle->audio);
        if (isset($oArticle->video) && $oArticle->video != "" && $oArticle->video != NULL && $oArticle->video != "0") unlink($base_path_system."application/resources/front/videos/".$oArticle->video);
        $this->mdlarticle->delete($prmId);
        $this->liste();
    }


    function getCategoryTypeArticle() {
        $article_typeid = $this->input->post("article_typeid") ;
        $data['colCategorie_xx'] = $this->mdl_categories_article->GetAllCategByTypeArticle($article_typeid);
        $this->load->view("front2013/SelectListeCategByTypeArticle",$data);
    }

    function getSousCategoryTypeArticle() {
        $article_categid = $this->input->post("article_categid") ;
        $data['colSousCategorie'] = $this->mdl_categories_article->GetAllSousCategByTypeArticle($article_categid);
        $this->load->view("front2013/SelectListeSousCategByTypeArticle",$data);
    }

    function add_redacteur_article()
    {
        $prmData['nom'] = $this->input->post("add_redacteur_name");
        $prmData['id'] = $this->input->post("add_redacteur_id");
        $prmData['IdCommercant'] = $this->input->post("IdCommercant");
        $verif_name = $this->mdl_article_redacteur->getByNom($prmData['nom'],$prmData['IdCommercant']);
        if (isset($verif_name) && count($verif_name) > 0) {
            echo 'error';
        } else {
            if (isset($prmData['id']) && $prmData['id']!=0 && $prmData['id']!='0' && $prmData['id'] != '' && $prmData['id'] != null)
                $insert_id = $this->mdl_article_redacteur->update($prmData);
            else $insert_id = $this->mdl_article_redacteur->insert($prmData);
            if (isset($insert_id)) {
                echo $insert_id->id;
            } else {
                echo 'error';
            }
        }
    }
    function check_redacteur_article() {
        $redacteur_id = $this->input->post("redacteur_id");
        $verif_name = $this->mdl_article_redacteur->getById($redacteur_id);

        if ($this->ion_auth->logged_in() && isset($verif_name) && is_object($verif_name)) {
            echo json_encode((array)$verif_name);
        } else {
            echo "error";
        }
    }
    function delete_redacteur_article(){
        $redacteur_id = $this->input->post("redacteur_id");
        if ($this->ion_auth->logged_in() && isset($redacteur_id)) {
            $verif_result = $this->mdl_article_redacteur->delete($redacteur_id);
            if ($verif_result) echo 'ok';
            else echo "error";
        } else {
            echo "error";
        }
    }
    function envoyer_validation_article_function()
    {
        $validation_nom_manifestation = $this->input->post("validation_nom_manifestation");
        $validation_description = $this->input->post("validation_description");
        $article_id = $this->input->post("article_id");
        $article_date_depot = $this->input->post("article_date_depot");
        $destinataire_validation_article = $this->input->post("destinataire_validation_article");
        $destinataire_validation = $this->mdl_article_validation_email->getById($destinataire_validation_article);

        $user_ion_auth = $this->ion_auth->user()->row();
        $prmIdCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $prmObjCommercant = $this->Commercant->GetById($prmIdCommercant);

        $contact_partner_nom = $destinataire_validation->nom;
        $contact_partner_mail = $prmObjCommercant->Email;
        $contact_partner_mailto = $destinataire_validation->email;

        $contact_partner_mailSubject = 'Demande validation article - Sortez.org';

        $message_html = $this->load->view('mail/send_validation_article', '', true);
        $message_html = str_replace('{mail_article_title}', $validation_nom_manifestation, $message_html);
        $message_html = str_replace('{mail_date_depot}', $article_date_depot, $message_html);
        $message_html = str_replace('{mail_IdCommercant}', $prmIdCommercant, $message_html);
        $message_html = str_replace('{mail_IdArticle}', $article_id, $message_html);
        $message_html = str_replace('{mail_nom_redacteur}', $prmObjCommercant->NomSociete, $message_html);
        $message_html = str_replace('{mail_contact_redacteur}', $contact_partner_mail, $message_html);

        $sending_mail_privicarte = false;

        //$message_html = utf8_encode($message_html);//encode mail content

        $zContactEmailCom_ = array();
        $zContactEmailCom_[] = array("Email" => $contact_partner_mailto, "Name" => $contact_partner_nom);
        $sending_mail_privicarte = envoi_notification($zContactEmailCom_, $contact_partner_mailSubject, $message_html, $contact_partner_mail);


        $zContactEmailCom_copy = array();
        $zContactEmailCom_copy[] = array("Email" => $contact_partner_mail, "Name" => $prmObjCommercant->NomSociete);

        $message_html_copy = $this->load->view('mail/send_validation_article', '', true);
        $message_html_copy = str_replace('{mail_article_title}', $validation_nom_manifestation, $message_html_copy);
        $message_html_copy = str_replace('{mail_date_depot}', $article_date_depot, $message_html_copy);
        $message_html_copy = str_replace('{mail_IdCommercant}', $prmIdCommercant, $message_html_copy);
        $message_html_copy = str_replace('{mail_IdArticle}', $article_id, $message_html_copy);
        $message_html_copy = str_replace('{mail_nom_redacteur}', $prmObjCommercant->NomSociete, $message_html_copy);
        $message_html_copy = str_replace('{mail_contact_redacteur}', $contact_partner_mail, $message_html_copy);


        //$message_html_copy = html_entity_decode(htmlentities($message_html_copy));
        $sending_mail_privicarte_copy = envoi_notification($zContactEmailCom_copy, $contact_partner_mailSubject, $message_html_copy);


        if ($sending_mail_privicarte) echo 'ok';
        else echo 'error';


    }
    function save()
    {
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_userdata("cur_uri", $this->uri->uri_string());
            redirect();
        }
        $objarticle = $this->input->post("articles");

        $user_ion_auth = $this->ion_auth->user()->row();
        $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $objarticle["IdCommercant"] = $commercant_UserId;
        $objarticle["IdUsers_ionauth"] = $user_ion_auth->id;


        $base_path_system = str_replace('system/', '', BASEPATH);
        $this->load->library('image_moo');

        if (isset($_FILES["ArticlePdf"]["name"])) $pdf = $_FILES["ArticlePdf"]["name"];
        if (isset($pdf) && $pdf != "") {
            $pdfAssocie = $pdf;
            $pdfCom = doUpload("ArticlePdf", "application/resources/front/images/article/pdf/", $pdfAssocie);
        } else {
            $pdfAssocie = $this->input->post("pdfAssocie");
            $pdfCom = $pdfAssocie;
        }


        $objArticle["pdf"] = $pdfCom;
        $objArticle["photo1"] = $this->input->post("photo1Associe");
        $objArticle["photo2"] = $this->input->post("photo2Associe");
        $objArticle["photo3"] = $this->input->post("photo3Associe");
        $objArticle["photo4"] = $this->input->post("photo4Associe");
        $objArticle["photo5"] = $this->input->post("photo5Associe");

        $objarticle["last_update"] = date("Y-m-d");

        $objarticle["date_depot"] = convert_Frenchdate_to_Sqldate($objarticle["date_depot"]);
        if ($objarticle["date_debut"]==!0){      $objarticle["date_debut"] = convert_Frenchdate_to_Sqldate($objarticle["date_debut"]);}else {$objarticle["date_debut"]=null;}
        if ($objarticle["date_fin"]==!0){   $objarticle["date_fin"] = convert_Frenchdate_to_Sqldate($objarticle["date_fin"]);}else {$objarticle["date_fin"]=null;}

        if ($objarticle["id"] == "" || $objarticle["id"] == null || $objarticle["id"] == "0") {
            $IdUpdatedarticle = $this->mdlarticle->insert($objarticle);
        } else {
            $IdUpdatedarticle = $this->mdlarticle->update($objarticle);
        }

        /*SAVING ALL DATETIME*/


        // article agenda referencement
        $referencement_agenda_article = $this->input->post("referencement_agenda_article");
        //$article_agenda_ref = $this->articles_agenda_ref($IdUpdatedArticle, $referencement_agenda_article);

        if (isset($IdUpdatedArticle) && $IdUpdatedArticle != "") $mssg = '1'; else $mssg = 'error';

        redirect("admin/articles/fiche/" . $commercant_UserId . "/" . $IdUpdatedArticle . "/" . $mssg);
    }
    function GetallSubcateg_by_categ($id = "0")
    {
        //$data["colSousCategorie"] = $this->mdl_categories_article->GetAllSousrubriqueByRubrique($id);
        $data["colSousCategorie"] = $this->mdl_categories_agenda->GetAllSousrubriqueByRubrique($id); // article use agenda's categories & subcategories

        $data['id'] = $id;
        $this->load->view("sortez/article/vwReponseSubcateg", $data);
    }
    function envoyer_validation_articles_function()
    {
        $validation_nom_manifestation = $this->input->post("validation_nom_manifestation");
        $validation_description = $this->input->post("validation_description");
        $articles_id = $this->input->post("article_id");
        $articles_date_depot = $this->input->post("article_date_depot");
        $destinataire_validation_article = $this->input->post("destinataire_validation_articles");
        $destinataire_validation = $this->mdl_article_validation_email->getById($destinataire_validation_articles);

        $user_ion_auth = $this->ion_auth->user()->row();
        $prmIdCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $prmObjCommercant = $this->Commercant->GetById($prmIdCommercant);

        $contact_partner_nom = $destinataire_validation->nom;
        $contact_partner_mail = $prmObjCommercant->Email;
        $contact_partner_mailto = $destinataire_validation->email;

        $contact_partner_mailSubject = 'Demande validation article - Sortez.org';

        $message_html = $this->load->view('mail/send_validation_article', '', true);
        $message_html = str_replace('{mail_article_title}', $validation_nom_manifestation, $message_html);
        $message_html = str_replace('{mail_date_depot}', $articles_date_depot, $message_html);
        $message_html = str_replace('{mail_IdCommercant}', $prmIdCommercant, $message_html);
        $message_html = str_replace('{mail_IdArticle}', $articles_id, $message_html);
        $message_html = str_replace('{mail_nom_redacteur}', $prmObjCommercant->NomSociete, $message_html);
        $message_html = str_replace('{mail_contact_redacteur}', $contact_partner_mail, $message_html);

        $sending_mail_privicarte = false;

        //$message_html = utf8_encode($message_html);//encode mail content

        $zContactEmailCom_ = array();
        $zContactEmailCom_[] = array("Email" => $contact_partner_mailto, "Name" => $contact_partner_nom);
        $sending_mail_privicarte = envoi_notification($zContactEmailCom_, $contact_partner_mailSubject, $message_html, $contact_partner_mail);


        $zContactEmailCom_copy = array();
        $zContactEmailCom_copy[] = array("Email" => $contact_partner_mail, "Name" => $prmObjCommercant->NomSociete);

        $message_html_copy = $this->load->view('mail/send_validation_articles', '', true);
        $message_html_copy = str_replace('{mail_articles_title}', $validation_nom_manifestation, $message_html_copy);
        $message_html_copy = str_replace('{mail_date_depot}', $articles_date_depot, $message_html_copy);
        $message_html_copy = str_replace('{mail_IdCommercant}', $prmIdCommercant, $message_html_copy);
        $message_html_copy = str_replace('{mail_Idarticles}', $articles_id, $message_html_copy);
        $message_html_copy = str_replace('{mail_nom_redacteur}', $prmObjCommercant->NomSociete, $message_html_copy);
        $message_html_copy = str_replace('{mail_contact_redacteur}', $contact_partner_mail, $message_html_copy);


        //$message_html_copy = html_entity_decode(htmlentities($message_html_copy));
        $sending_mail_privicarte_copy = envoi_notification($zContactEmailCom_copy, $contact_partner_mailSubject, $message_html_copy);


        if ($sending_mail_privicarte) echo 'ok';
        else echo 'error';


    }
}