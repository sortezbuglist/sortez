<?php
class Bonplan extends CI_Controller {
    
    function __construct() {
        parent::__construct();
		
		$this->load->model("mdlannonce") ;
		$this->load->model("mdlcommercant") ;
		$this->load->model("mdlbonplan") ;
		$this->load->model("SousRubrique") ;
		
		$this->load->library('session');

        $this->load->library('ion_auth');

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }
    
    function index(){			
        $this->liste() ;
    }
	 function liste(){
		
		$toListeBonplan = $this->mdlbonplan->listeBonPlan() ;		
		$data['toListeMesBonplan'] = $toListeBonplan ;		
                $this->load->view('admin/vwListeBonplan', $data) ;
    }
	
	
	
	
	
	function ficheBonplan($_iDCommercant = 0, $_iDBonplan = 0){
		$data["idCommercant"] = $_iDCommercant ;
		$data["colSousRubriques"] = $this->SousRubrique->GetAll();
		
		if ($_iDBonplan != 0){
			$data["title"] = "Modification BonPlan" ;
			$data["oBonplan"] = $this->mdlbonplan->getById($_iDBonplan);
                        $this->load->view('admin/vwFicheBonplan', $data) ;
		
		}else{
			$data["title"] = "Creer un Bonplan" ;
			$this->load->view('admin/vwFicheBonplan', $data) ;
		}
	}
	
	function creerBonplan($_iDCommercant = 0){
		$oBonplan = $this->input->post("bonplan") ;
		$IdInsertedBonplan = $this->mdlbonplan->insertBonplan($oBonplan);
		$data["title"] = "Creer une bonplan" ;
		$data["idCommercant"] = $_iDCommercant ;
		$data["msg"] = "Bonplan bien cree" ;
                $toListeBonplan = $this->mdlbonplan->listeBonPlan() ;		
		$data['toListeMesBonplan'] = $toListeBonplan ;	
		$this->load->view('admin/vwListeBonplan', $data) ;
	}
	
	function modifAnnonce($_iDCommercant = 0){
		$oAnnonce = $this->input->post("annonce") ;
		$IdUpdatedAnnonce = $this->mdlannonce->updateAnnonce($oAnnonce);
		$data["title"] = "Modification annonce" ;
		$data["idCommercant"] = $_iDCommercant ;
		$data["msg"] = "Annon bien ete modifier" ;
		$toListeAnnonce = $this->mdlannonce->getlisteAnnonce() ;		
		$data['toListeMesAnnonce'] = $toListeAnnonce ;	
		redirect ("admin/annonce/liste/$_iDCommercant") ;
		//$this->load->view('front/vwFicheAnnonce', $data) ;
	}
	
	// function listeMesAnnonces($_iDCommercant = 0){
		// if ($_iDCommercant != 0){
			// $toListeMesAnnonce = $this->mdlannonce->listeMesAnnonces($_iDCommercant) ;
			// $data['toListeMesAnnonce'] = $toListeMesAnnonce ;
			// $data["idCommercant"] = $_iDCommercant ;
			// $this->load->view('admin/vwDesAnnonces', $data) ;
		// }else{
			// $this->load->view('front/vwMesAnnonces', $data) ;
		// }
	// }
	
	function supprimAnnonce($_iDAnnonce=0, $_iDCommercant=0){
		
		$zReponse = $this->mdlannonce->supprimeAnnonces($_iDAnnonce) ;
		redirect ("admin/annonce/liste/$_iDCommercant") ;
	}
	
	function annulationAjout(){
		//Supprimme tous les images deja uploader.
		$tNameFile = explode("-", $this->input->post("zConcateNameFile")) ;
		//print_r ($tNameFile); exit ();
		$path =  "application/resources/front/images/" ;
		//echo $path . $tNameFile[0] ; exit ();
		if (count ($tNameFile) > 0){
			for ($i = 0; $i > count($tNameFile); $i ++){
				// echo "dfsdfs" ; exit ();
				if (file_exists($path.$tNameFile[$i])){
					unlink($path.$tNameFile[$i]) ;
					// echo $path . $tNameFile[0] ; exit ();
				}
			}
		}
		
		//return false;
	}
}