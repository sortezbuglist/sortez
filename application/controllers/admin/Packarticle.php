<?php

class packarticle extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->model("user");
        $this->load->library('user_agent');
        $this->load->model("sousRubrique");
        $this->load->model("Rubrique");
        $this->load->model("AssCommercantAbonnement");
        $this->load->model("AssCommercantSousRubrique");
        $this->load->model("Abonnement");
        $this->load->model("mdlannonce");
        $this->load->model("mdlbonplan");
        $this->load->model("Abonnement");
        $this->load->model("mdl_agenda");
        $this->load->model("mdlville");
        $this->load->Model("mdldepartement");
        $this->load->Model("mdlcommune");
        $this->load->Model("mdl_lightbox_mail");
        $this->load->model('pack_test_validation');
        $this->load->model("Commercant");
        $this->load->model("mdl_types_agenda");
        $this->load->model("mdl_categories_agenda");

        $this->load->model("packarticle_order");
        $this->load->model("packarticle_object");
        $this->load->model("packarticle_pack");
        $this->load->model("packarticle_payement");
        $this->load->model("packarticle_planing");
        $this->load->model("packarticle_price");
        $this->load->model("packarticle_quota");
        $this->load->model("packarticle_type");
        $this->load->model("packarticle_user");
        $this->load->model("packarticle_article");

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }

    function index()
    {
        $this->liste();
    }

    function liste()
    {
        $objOrder = $this->packarticle_order->getAll();
        $data["objOrder"] = $objOrder;
        $data['Commercant'] = $this->Commercant;
        $data['packarticle_quota'] = $this->packarticle_quota;
        $data['packarticle_pack'] = $this->packarticle_pack;
        $data['packarticle_article'] = $this->packarticle_article;

        $this->load->view("packarticle/admin/order_list", $data);
    }

    function statistics() {
        $data['empty']=null;

        $data['selected_user_id']=$this->input->post('user_id');
        $data['selected_id_planing']=$this->input->post('id_planing');
        $data['selected_id_status']=$this->input->post('id_status');

        $data['Commercant'] = $this->Commercant;
        $data['packarticle_quota'] = $this->packarticle_quota;
        $data['packarticle_pack'] = $this->packarticle_pack;
        $data['packarticle_article'] = $this->packarticle_article;
        $data['packarticle_order'] = $this->packarticle_order;
        $data['packarticle_planing'] = $this->packarticle_planing;

        $data['username']= $this->packarticle_article->getusernameionauth();
        $data['edition']= $this->packarticle_article->geteditionpackartplanning();
        $data['resultat']=$this->packarticle_article->filter();

        $objArticle = $this->packarticle_article->getAll_valuejoin($data['selected_user_id'],$data['selected_id_planing'],$data['selected_id_status']);
        $data["objArticle"] = $objArticle;

        $this->load->view("packarticle/statistics", $data);
    }

    function details_packarticle_order($id_order = 0)
    {
        $packarticle_article_submit = $this->input->post('packarticle_article_submit');
        if (isset($packarticle_article_submit) && $packarticle_article_submit != false) {
            $packarticle_order_update['planing_id'] = $this->input->post('packarticle_planing_id');
            $packarticle_order_update['title'] = $this->input->post('packarticle_article_title');
            $packarticle_order_update['status_id'] = $this->input->post('packarticle_article_status');
            $packarticle_order_update['id'] = $this->input->post('packarticle_article_id');
            $packarticle_order_update['date_update'] = date("Y-m-d");
            $packarticle_order_id_update = $this->packarticle_article->update($packarticle_order_update);
            if (isset($packarticle_order_id_update) && is_numeric($packarticle_order_id_update)) {
                $data['mssg_confirm_update'] = '1';
            }
        }
        if (isset($id_order) && $id_order != 0) {
            $data["oPack_order"] = $this->packarticle_order->getById($id_order);
            $data['Commercant'] = $this->Commercant;
            $data['packarticle_quota'] = $this->packarticle_quota;
            $data['packarticle_pack'] = $this->packarticle_pack;
            $data["packarticle_article"] = $this->packarticle_article->getWhere(" order_id=" . $id_order . " order by id asc");
            $data['packarticle_planing'] = $this->packarticle_planing->getAll();
            $this->load->view('packarticle/admin/articles_list', $data);
        } else {
            redirect('admin/packarticle');
        }
    }

    function fiche_packarticle_order($id_order = 0)
    {
        $packarticle_pack_submit = $this->input->post('packarticle_pack_submit');
        if (isset($packarticle_pack_submit) && $packarticle_pack_submit != false) {
            $packarticle_order_update['status_id'] = $this->input->post('packarticle_order_status_id');
            $packarticle_order_update['id'] = $this->input->post('packarticle_order_id');
            $packarticle_order_update['validation_date'] = date("Y-m-d");
            $quota_id_validation_value = $this->input->post('quota_id_validation_value');
            $pack_object = $this->packarticle_pack->getWhere(" quota_id=" . $quota_id_validation_value );
            $packarticle_order_update['pack_id'] = $pack_object[0]->id;
            $packarticle_order_id_update = $this->packarticle_order->update($packarticle_order_update);

            if (isset($packarticle_order_id_update) && is_numeric($packarticle_order_id_update)) {
                $data['mssg_confirm_update'] = '1';
                if (intval($packarticle_order_update['status_id']) == 2) {
                    $pack_quota_obj = $this->packarticle_quota->getById( $quota_id_validation_value );
                    if (isset($pack_quota_obj->quota) && is_numeric($pack_quota_obj->quota)) {
                        $packarticle_article_to_insert['id'] = null;
                        $packarticle_article_to_insert['order_id'] = $packarticle_order_update['id'];
                        $packarticle_article_to_insert['date_creation'] = date("Y-m-d");
                        for ($ii = 0; $ii < $pack_quota_obj->quota; $ii++) {
                            $this->packarticle_article->insert($packarticle_article_to_insert);
                        }
                    }
                    redirect('admin/packarticle');
                }
            }
        }
        if (isset($id_order) && $id_order != 0 && $id_order != '') {
            $data["oPack_order"] = $this->packarticle_order->getById($id_order);
            $data['Commercant'] = $this->Commercant;
            $data['packarticle_quota'] = $this->packarticle_quota;
            $data['all_quota'] = $this->packarticle_quota->getAll();
            $data['packarticle_pack'] = $this->packarticle_pack;
            $this->load->view('packarticle/admin/details_order', $data);
        } else {
            redirect('admin/packarticle');
        }
    }

    function delete_article_order()
    {
        $order_id = $this->input->post('order_id');
        //echo $order_id;
        $pack_order_sql = " order_id=" . $order_id . " and status_id != 3 ";
        $article_result = $this->packarticle_article->getWhere($pack_order_sql);
        if (isset($article_result) && count($article_result) > 0) {
            echo "Cette commande dispose encore des articles non édités !";
        } else {
            $pack_order_sql_to_delete = " order_id=" . $order_id . " and status_id=3 ";
            $article_result_to_delete = $this->packarticle_article->getWhere($pack_order_sql_to_delete);
            foreach ($article_result_to_delete as $item) {
                $this->packarticle_article->delete($item->id);
            }
            $pack_order_sql_to_check = " order_id=" . $order_id . " and status_id=3 ";
            $article_result_to_check = $this->packarticle_article->getWhere($pack_order_sql_to_check);
            if (!isset($article_result_to_check) || count($article_result_to_check) == 0) {
                $this->packarticle_order->delete($order_id);
            }
            echo "L'ensemble des contenus de la commande a été supprimé !";
        }
    }

    function creer_types_article()
    {
        $oArticle = $this->input->post("types_article");
        $this->mdl_types_article->insertarticle_type($oArticle);
        $data["msg"] = "Type Article ajouté";
        $objArticles = $this->mdl_types_article->GetAll();
        $data["colArticles"] = $objArticles;
        $this->load->view("admin/vwListeTypeArticle", $data);
    }

    function modif_types_article($IdTypeArticle)
    {
        $oArticle = $this->input->post("types_article");
        $this->mdl_types_article->updatearticle_type($oArticle);
        $data["msg"] = "Type Article enregistré";
        $objArticles = $this->mdl_types_article->GetAll();
        $data["colArticles"] = $objArticles;
        $this->load->view("admin/vwListeTypeArticle", $data);
    }


    function delete($prmId)
    {

        if (is_numeric(trim($prmId))) {
            //verify if category contain active article********
            $oCategorie = $this->mdl_types_article->verifier_type_article($prmId);
            //$oCategorie = array();
            if (count($oCategorie) == 0) {
                $this->mdl_types_article->supprimearticle_type($prmId);
                $this->packarticle_article->supprimepackarticle_objet($prmId);
                $this->session->set_flashdata('mess_editTypeArticle', '3');
            } else {
                $this->session->set_flashdata('mess_editTypeArticle', '2');
            }

            //$this->firephp->log($oCategorie, 'oCategorie');

            redirect("admin/types_article");
        } else {
            redirect("admin/types_article");
        }

    }



}