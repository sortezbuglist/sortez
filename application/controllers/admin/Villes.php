<?php
class villes extends CI_Controller{
    
    function __construct() {
        parent::__construct();
		
        $this->load->model("mdlville");
        $this->load->library('session');

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }
    
    function index(){
        $this->liste();
    }
    
    function liste() {
        $objVilles = $this->mdlville->GetAll();
        $data["colVilles"] = $objVilles;
        $this->load->view("admin/vwListeVilles",$data);
    }

    function ficheVille($IdVille) {

        if ($IdVille != 0){
            $data["title"] = "Modification Ville" ;
            $data["oVille"] = $this->mdlville->getVilleById($IdVille) ;
            $data["IdVille"] = $IdVille;
            $this->load->view('admin/vwFicheVille', $data) ;

        }else{
            $data["title"] = "Creer une Ville" ;
            $this->load->view('admin/vwFicheVille', $data) ;
        }
    }

    function creerVille(){
        $oVille = $this->input->post("ville") ;
        $IdInsertedVille = $this->mdlville->insertVille($oVille);
        $data["msg"] = "Ville bien ajoutée" ;

        $objVilles = $this->mdlville->GetAll();
        $data["colVilles"] = $objVilles;
        $this->load->view("admin/vwListeVilles",$data);
    }

    function modifVille($IdVille){
        $oVille = $this->input->post("ville") ;
        $IdUpdatedVille = $this->mdlville->updateVille($oVille);
        $data["msg"] = "Ville bien enregistrée" ;

        $objVilles = $this->mdlville->GetAll();
        $data["colVilles"] = $objVilles;
        $this->load->view("admin/vwListeVilles",$data);
    }
    
    function delete($prmId) {
        $objVillesDeleted = $this->mdlville->supprimeVille($prmId);
        $this->liste();
    }
    

}