<?php
class parametres extends CI_Controller{
    
    function __construct() {
        parent::__construct();
		
        $this->load->model("parametre");
		$this->load->library('session');

        $this->load->library('ion_auth');

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }
    
    function fiche($Type) {
        $this->load->helper('ckeditor');
 
        //Ckeditor's configuration
        $data['ckeditor0'] = array(
 
            //ID of the textarea that will be replaced
            'id'     =>     'Parametre0',
            'path'    =>    APPPATH . 'resources/ckeditor',
 
            //Optionnal values
            'config' => array(
                'width'     =>     "800px",    //Setting a custom width
                'height'     =>     '200px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ),
                    array( 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ),
                    array( 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ),
                    array( 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ),
                    array( 'Link','Unlink','Anchor' ),
                    array( 'Image','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ),
                    array( 'Styles','Format','Font','FontSize' ),
                    array( 'TextColor','BGColor' ),
                    array( 'Maximize', 'ShowBlocks','-','About' )
                )
            )
        );
        
        $data['ckeditor1'] = array(
 
            //ID of the textarea that will be replaced
            'id'     =>     'Parametre1',
            'path'    =>    APPPATH . 'resources/ckeditor',
 
            //Optionnal values
            'config' => array(
                'width'     =>     "800px",    //Setting a custom width
                'height'     =>     '200px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ),
                    array( 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ),
                    array( 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ),
                    array( 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ),
                    array( 'Link','Unlink','Anchor' ),
                    array( 'Image','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ),
                    array( 'Styles','Format','Font','FontSize' ),
                    array( 'TextColor','BGColor' ),
                    array( 'Maximize', 'ShowBlocks','-','About' )
                )
            )
        );
        switch($Type) {
            case "particulier" :
                $prmCode = array(
                    "ContenuMailConfirmationInscriptionUser",
                    "ContenuPageConfirmationInscriptionUser"
                );
                break;
            case "pro" :
                $prmCode = array(
                    "ContenuMailConfirmationInscriptionPro",
                    "ContenuPageConfirmationInscriptionPro"
                );
                break;
            case "logo" :
                $prmCode = array(
                    "PhotoHeader"
                );
                break;
        }
        $data["Type"] = $Type;
        $data["colParametres"] = $this->parametre->getWhereCodeIn($prmCode);
        $this->load->view("admin/vwFicheParametre",$data);
    }
    
    function modifier() {
        $this->load->helper('url');
        $Parametre = $this->input->post("Parametre");
        if(isset($Parametre["Type"]) && $Parametre["Type"] == "logo") {
            $config['upload_path'] = APPPATH . 'resources/front/images/logos/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '1000';
            
            $this->load->library('upload', $config);
            
            if ( ! $this->upload->do_upload("PhotoHeader")) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $data = array('upload_data' => $this->upload->data());
                unset($Parametre["Type"]);
                $Parametre["Contenu"] = $data["upload_data"]["file_name"];
                //debug($Parametre);
                $this->parametre->Update($Parametre);
            }
        } else {
            foreach($Parametre as $arrParametre) {
                $this->parametre->Update($arrParametre);
            }
        }
        redirect("admin");
    }
}