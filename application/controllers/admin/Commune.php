<?php
class commune extends CI_Controller{
	
	 function __construct() {
        parent::__construct();
		
        $this->load->model("mdl_commune");
        $this->load->library('session');
        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }
    
    function index(){
        $this->liste();
    }
    
	function liste() {
    
        $objCommune = $this->mdl_commune->GetAll();
        $data["colCommune"] = $objCommune;
        $this->load->view("admin/vwlistecommune", $data);
	
    }
	
	function ficheCommune($id_commune) {

        if ($id_commune != 0){
            $data["title"] = "Modification commune" ;
            $data["oCommune"] = $this->mdl_commune->getById($id_commune) ;
            $data["id_commune"] = $id_commune;
            $this->load->view('admin/vwfichecommune', $data) ;

        }else{
            $data["title"] = "Creer une commune" ;
            $this->load->view('admin/vwfichecommune', $data) ;
        }
    }

	function modifcommune($id_commune){
        $oCommune = $this->input->post("commune") ;
        $IdUpdatedCommune = $this->mdl_commune->update($oCommune);
        $data["msg"] = "Ville bien enregistrée" ;

        $objCommune = $this->mdl_commune->GetAll();
        $data["colCommune"] = $objCommune;
        $this->load->view("admin/vwlistecommune",$data); 
    }
	
	
}