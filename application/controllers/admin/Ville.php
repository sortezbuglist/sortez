<?php
class ville extends CI_Controller{
    
    function __construct() {
        parent::__construct();
		
        $this->load->model("mdl_ville");
        $this->load->library('session');

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }
    
    function index(){
        $this->liste();
    }
    
    function liste() {
        $objVilles = $this->mdl_ville->GetAll();
        $data["colVilles"] = $objVilles;
        $this->load->view("admin/viewListeVilles",$data);
    }

    function ficheVille($IdVille) {

        if ($IdVille != 0){
            $data["title"] = "Modification Ville" ;
            $data["oVille"] = $this->mdl_ville->getVilleById($IdVille) ;
            $data["IdVille"] = $IdVille;
            $this->load->view('admin/viewFicheVille', $data) ;

        }else{
            $data["title"] = "Creer une Ville" ;
            $this->load->view('admin/viewFicheVille', $data) ;
        }
    }

    function creerVille(){
        $oVille = $this->input->post("ville") ;
        $IdInsertedVille = $this->mdl_ville->insertVille($oVille);
        $data["msg"] = "Ville bien ajoutée" ;

        $objVilles = $this->mdl_ville->GetAll();
        $data["colVilles"] = $objVilles;
        $this->load->view("admin/viewListeVilles",$data);
    }

    function modifVille($IdVille){
        $oVille = $this->input->post("ville") ;
        $IdUpdatedVille = $this->mdl_ville->updateVille($oVille);
        $data["msg"] = "Ville bien enregistrée" ;

        $objVilles = $this->mdl_ville->GetAll();
        $data["colVilles"] = $objVilles;
        $this->load->view("admin/viewListeVilles",$data);
    }
    
    function delete($prmId) {
        $objVillesDeleted = $this->mdl_ville->supprimeVille($prmId);
        $this->liste();
    }
    

}