<?php
class departement extends CI_Controller{
	
	 function __construct() {
        parent::__construct();
		
        $this->load->model("mdl_departement");
        $this->load->library('session');

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }
    
    function index(){
        $this->liste();
    }
    
	function liste() {
        $objDepartement = $this->mdl_departement->GetAll();
        $data["colDepartement"] = $objDepartement;
        $this->load->view("admin/viewListeDepartement", $data);
    }
	
	function ficheDepartement($departement_id) {

        if ($departement_id != 0){
            $data["title"] = "Modification Departement" ;
            $data["oDepartement"] = $this->mdl_departement->getById($departement_id) ;
            $data["departement_id"] = $departement_id;
            $this->load->view('admin/viewFicheDepartement', $data) ;

        }else{
            $data["title"] = "Creer une Departement" ;
            $this->load->view('admin/viewFicheDepartement', $data) ;
        }
    }

	function modifDepartement($departement_id){
        $oDepartement = $this->input->post("departement") ;
        $IdUpdatedDepartement = $this->mdl_departement->update($oDepartement);
        $data["msg"] = "Ville bien enregistr�e" ;

        $objDepartement = $this->mdl_departement->GetAll();
        $data["colDepartement"] = $objDepartement;
        $this->load->view("admin/viewListeDepartement",$data); 
    }
	
	function creerDepartement(){
        $oDepartement = $this->input->post("departement") ;
        $IdInsertedDepartement = $this->mdl_departement->insertDepartement($oDepartement);
        $data["msg"] = "Ville bien ajout�e" ;

        $objDepartement = $this->mdl_departement->GetAll();
        $data["colDepartement"] = $objDepartement;
        $this->load->view("admin/viewListeDepartement",$data);
    }
}