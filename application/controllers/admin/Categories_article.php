<?php
class categories_article extends CI_Controller{

    function __construct() {
        parent::__construct();

        $this->load->library('session');
        $this->load->library('ion_auth');
        $this->load->model("mdl_categories_article");
        $this->load->model("mdl_types_article");
        $this->load->model("ion_auth_used_by_club");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }

    function index(){
        $this->liste();
    }

    function categories(){
        $this->liste();
    }

    function liste() {
        $toListeCategorie = $this->mdl_categories_article->GetAll() ;
        $data['toListeCategorie'] = $toListeCategorie ;

        if($this->session->flashdata('mess_editcategorie')=='1') $data['mess_editcategorie'] = '<strong style="color:#060">Catégorie enregistrée !</strong>';
        if($this->session->flashdata('mess_editcategorie')=='2') $data['mess_editcategorie'] = '<strong style="color:#F00">Cette catégorie ne peut être supprimée ! catégorie liée à un article !</strong>';
        if($this->session->flashdata('mess_editcategorie')=='3') $data['mess_editcategorie'] = '<strong style="color:#060">Catégorie supprimée !</strong>';

        $this->load->view("admin/vwCategoryArticle", $data);
    }



    function edit_categories_article(){
        $article_categid = $this->uri->rsegment(3);

        //echo $nom_url_commercant;
        $oCategorie = $this->mdl_categories_article->GetById($article_categid) ;
        $data['oCategorie'] = $oCategorie ;
        $data['oTypeArticle'] = $this->mdl_types_article->GetAll() ;

        //$this->firephp->log($oCategorie, 'oCategorie');

        if(is_numeric(trim($article_categid))) {
            $this->load->view("admin/vwEditCategoryArticle", $data);
        }
        else {
            redirect('admin/categories_article');
        }

    }

    function insert_categories_article(){
        $data['oCategorie'] = NULL;
        $data['oTypeArticle'] = $this->mdl_types_article->GetAll() ;
        //$this->firephp->log($data['oTypeArticle'], 'oTypeArticle');
        $this->load->view("admin/vwEditCategoryArticle", $data);
    }

    function insertsouscategorie(){
        $IdRubrique = $this->uri->rsegment(3);
        $toCategorie = $this->mdl_categories_article->GetById($IdRubrique) ;
        $data['toCategorie'] = $toCategorie ;
        $this->load->view("admin/vwEditSousCategoryArticle", $data);
    }


    function savecategorie(){
        $idrubrique_editcategorie = $this->input->post("idrubrique_editcategorie");
        $ocategorie['article_categid'] = $idrubrique_editcategorie;
        $article_typeid = $this->input->post("article_typeid");
        $ocategorie['article_typeid'] = $article_typeid;
        $inputcateg_editcategorie = $this->input->post("inputcateg_editcategorie");
        $ocategorie['category'] = $inputcateg_editcategorie;

        if ($idrubrique_editcategorie=="0") $this->mdl_categories_article->Insert($ocategorie);
        else $this->mdl_categories_article->Update($ocategorie);

        $this->session->set_flashdata('mess_editcategorie', '1');
        redirect('admin/categories_article');

    }

    function delete_categories_article(){
        $IdRubrique = $this->uri->rsegment(3);

        if(is_numeric(trim($IdRubrique))) {
            //verify if category contain active article********
            $oCategorie = $this->mdl_categories_article->verifier_categorie_article($IdRubrique) ;
            //$oCategorie = array();
            if (count($oCategorie)==0){
                $this->mdl_categories_article->Delete($IdRubrique);
                $this->session->set_flashdata('mess_editcategorie', '3');
            } else {
                $this->session->set_flashdata('mess_editcategorie', '2');
            }

            //$this->firephp->log($oCategorie, 'oCategorie');

            redirect('admin/categories_article/');
        }
        else {
            redirect('admin/categories_article/');
        }

    }



    function souscategorie(){
        $IdRubrique = $this->uri->rsegment(3);

        if(is_numeric(trim($IdRubrique))) {
            $data["no_main_menu_home"] = "1";
            $data["no_left_menu_home"] = "1";

            $toListeSousCategorie = $this->mdl_categories_article->GetAllSousrubriqueByRubrique($IdRubrique) ;
            $toCategorie = $this->mdl_categories_article->GetById($IdRubrique) ;
            $data['toListeSousCategorie'] = $toListeSousCategorie ;
            $data['toCategorie'] = $toCategorie ;

            if($this->session->flashdata('mess_editsouscategorie')=='1') $data['mess_editsouscategorie'] = '<strong style="color:#060">SousCatégorie enregistrée !</strong>';
            if($this->session->flashdata('mess_editsouscategorie')=='2') $data['mess_editsouscategorie'] = '<strong style="color:#F00">Cette SousCatégorie ne peut être supprimée ! catégorie liée à un partenaire !</strong>';
            if($this->session->flashdata('mess_editsouscategorie')=='3') $data['mess_editsouscategorie'] = '<strong style="color:#060">SousCatégorie supprimée !</strong>';

            $this->load->view("admin/vwSousCategoryArticle", $data);

        }
        else {
            redirect('admin/categories_article');
        }
    }


    function modifiersouscategorie(){
        $IdSousRubrique = $this->uri->rsegment(3);
        $IdRubrique = $this->uri->rsegment(4);

        //echo $nom_url_commercant;
        $oSousCategorie = $this->mdl_categories_article->getByIdSousCateg($IdSousRubrique);
        $data['oSousCategorie'] = $oSousCategorie ;
        $toCategorie = $this->mdl_categories_article->getById($IdRubrique) ;
        $data['toCategorie'] = $toCategorie ;

        //$this->firephp->log($oSousCategorie, 'oSousCategorie');

        if(is_numeric(trim($IdSousRubrique))) {
            $this->load->view("admin/vwEditSousCategoryArticle", $data);
        }
        else {
            redirect('admin/categories_article/souscategorie/'.$IdRubrique);
        }
    }


    function savesouscategorie(){
        $idrubrique_editcategorie = $this->input->post("idrubrique_editcategorie");
        $ocategorie['article_categid'] = $idrubrique_editcategorie;
        $idrubrique_editsouscategorie = $this->input->post("idrubrique_editsouscategorie");
        $ocategorie['article_subcategid'] = $idrubrique_editsouscategorie;
        $inputcateg_editsouscategorie = $this->input->post("inputcateg_editsouscategorie");
        $ocategorie['subcateg'] = $inputcateg_editsouscategorie;

        if ($idrubrique_editsouscategorie=="0") $this->mdl_categories_article->InsertSousCateg($ocategorie);
        else $this->mdl_categories_article->UpdateSousCateg($ocategorie);

        $this->session->set_flashdata('mess_editsouscategorie', '1');
        redirect('admin/categories_article/souscategorie/'.$idrubrique_editcategorie);

    }


    function supprimersouscategorie(){
        $IdSousRubrique = $this->uri->rsegment(3);
        $IdRubrique = $this->uri->rsegment(4);
        //$this->firephp->log($IdSousRubrique, 'IdSousRubrique');
        //$this->firephp->log($IdRubrique, 'IdRubrique');

        if(is_numeric(trim($IdSousRubrique))) {
            //verify if subcateg contains article
            $oSousCategorie = $this->mdl_categories_article->verifier_souscategorie_article($IdSousRubrique) ;
            //$oSousCategorie = array();
            if (count($oSousCategorie)==0){
                $this->mdl_categories_article->DeleteSousCateg($IdSousRubrique) ;
                $this->session->set_flashdata('mess_editsouscategorie', '3');
            } else {
                $this->session->set_flashdata('mess_editsouscategorie', '2');
            }

            //$this->firephp->log(count($oSousCategorie), 'oCategorie');

            redirect('admin/categories_article/souscategorie/'.$IdRubrique);
        }
        else {
            redirect('admin/categories_article');
        }

    }


}