<?php

class vivresaville extends CI_Controller{



    function __construct() {

        parent::__construct();



        $this->load->library('session');

        $this->load->library('ion_auth');

        $this->load->model("vivresaville_villes");

        $this->load->model("ion_auth_used_by_club");

        $this->load->model("mdldepartement");

        $this->load->model("mdlville");

        $this->load->model("vsv_ville_other");





        if (!$this->ion_auth->is_admin()) {

            $this->session->set_flashdata('domain_from', '1');

            redirect("connexion");

        }

    }



    function index(){

        $this->main_list();

    }



    function main_list() {

        $objlist = $this->vivresaville_villes->getAl1();

        $data["objlist"] = $objlist;



        /*if($this->session->flashdata('mess_editTypeagenda')=='1') $data['msg'] = '<strong style="color:#060">Type enregistré !</strong>';

        if($this->session->flashdata('mess_editTypeagenda')=='2') $data['msg'] = '<strong style="color:#F00">Ce type d\'agenda ne peut étre supprimé ! Type lié é une catégorie !</strong>';

        if($this->session->flashdata('mess_editTypeagenda')=='3') $data['msg'] = '<strong style="color:#060">Type supprimé !</strong>';*/



        $this->load->view("admin/vivresaville/index",$data);

    }

    //function controler for drag and drop
    function orderUpdate()
    {     
        echo'test';
        $ids = $this->input->post('ids');
        if(!empty($ids))
        { 
            $idArray = explode(",", $ids); 
            $count = 1; 
            foreach ($idArray as $id)
            { 
                $data = array('content_order' => $count);
                $update = $this->vivresaville_villes->update_order($data, $id); 
                $count ++;     
            } 
        }
    }



    function edit($id_vsv=0) {

        $vsv_object = $this->input->post("vsv_object");

        //saving form data start **************

        if (isset($vsv_object) && $vsv_object!=false) {

            //die("stop");

            if (!isset($vsv_object["id"]) || $vsv_object["id"]=="" || $vsv_object["id"]=="0") $vsv_object["id"] = null;



            if ($vsv_object["id"] == null) {

                $vsv_object_id_inserted = $this->vivresaville_villes->insert($vsv_object);

                redirect("admin/vivresaville/edit/".$vsv_object_id_inserted);

            }

            else {

                $vsv_object_id_inserted = $this->vivresaville_villes->update($vsv_object);

                redirect("admin/vivresaville/edit/".$vsv_object_id_inserted);

            }

        }

        //saving form data end **************



        if (isset($id_vsv) && $id_vsv != 0 && $id_vsv!= ""){

            $data["title"] = "Modification vivre sa ville" ;

            $data["oville"] = $this->vivresaville_villes->getById($id_vsv);



            $ville_data = $this->vsv_ville_other->getByIdVsv($id_vsv);

            $data['ville_data'] = $ville_data;



            $data["id_vsv"] = $id_vsv;

        }else{

            $data["title"] = "Creer vivre sa ville" ;

        }

        $data["colDepartement"] = $this->mdldepartement->GetAll();

        $this->load->view('admin/vivresaville/edit', $data) ;

    }



     function home_mgt($id_vsv=0) {

        $vsv_object = $this->input->post("vsv_object");

        $bloc_menu = $this->input->post("bloc_menu");

        $id = $_REQUEST['id'];

        $titre=$bloc_menu['titre'];


        //saving form data start **************

        if (isset($vsv_object) && $vsv_object!=false) {

            //die("stop");

            if (!isset($vsv_object["id"]) || $vsv_object["id"]=="" || $vsv_object["id"]=="0") redirect('admin/vivresaville/edit/'.$id_vsv);

            

            if (isset($_FILES["bloc_menu_photo_site_web"]["name"])) $vsv_home_photo_banner_file = $_FILES["bloc_menu_photo_site_web"]["name"];

            if (isset($vsv_home_photo_banner_file) && $vsv_home_photo_banner_file != "") {

                $fileAssocie = $vsv_home_photo_banner_file;

                $filename_saved = doUpload("bloc_menu_photo_site_web", "application/resources/front/images/vivresaville/", $fileAssocie);

                $bloc_menu["home_photo_site_web"]=$filename_saved;

            }

            if (isset($_FILES["vsv_logo_file"]["name"])) $vsv_logo_file = $_FILES["vsv_logo_file"]["name"];

            if (isset($vsv_logo_file) && $vsv_logo_file != "") {

                $file_logo_Associe = $vsv_logo_file;

                $filename_logo_saved = doUpload("vsv_logo_file", "application/resources/front/images/vivresaville/", $file_logo_Associe);

                $vsv_object["logo"]=$filename_logo_saved;

            }

            if (isset($_FILES["vsv_logo_droite_file"]["name"])) $vsv_logo_droite_file = $_FILES["vsv_logo_droite_file"]["name"];

            if (isset($vsv_logo_droite_file) && $vsv_logo_droite_file != "") {

                $file_logo_droite_Associe = $vsv_logo_droite_file;

                $filename_logo_droite_saved = doUpload("vsv_logo_droite_file", "application/resources/front/images/vivresaville/", $file_logo_droite_Associe);

                $vsv_object["logo_droite"]=$filename_logo_droite_saved;

            }

            



            if ($vsv_object["id"] != null && $vsv_object["id"] != 0){

                $vsv_object_id_inserted = $this->vivresaville_villes->update($vsv_object);

                $get_data_bloc_menu = $this->vivresaville_villes->getBloc_Menu_by_id_vivresaville_villes($vsv_object["id"]);

                if($get_data_bloc_menu != null){

                    $bloc_menu['id_vivresaville_villes'] = $get_data_bloc_menu->id_vivresaville_villes;


                }else{

                    $bloc_menu['id_vivresaville_villes'] = $vsv_object["id"];

                    
                }

            if(isset($id) && $id !=null) {

            

                     $this->vivresaville_villes->update_bloc($bloc_menu);

                    $this->vivresaville_villes->remove_vsv_home_photo_site_web($vsv_id);



                       }elseif(isset($bloc_menu) && $bloc_menu['titre'] != null && $id == ''){

                        $this->vivresaville_villes->insert_bloc_menu($bloc_menu);

                    };

                //var_dump($get_data_bloc_menu);

                redirect("admin/vivresaville/home_mgt/".$vsv_object_id_inserted);

            }


        }

        //saving form data end **************



        $data["title"] = "Gestion de la page d'accueil" ;

        if (isset($id_vsv) && $id_vsv != 0 && $id_vsv!= ""){

            $data["oville"] = $this->vivresaville_villes->getById($id_vsv);

           $data["bloc_menu1"] = $this->vivresaville_villes->getBloc_Menu_by_id_vivresaville_villes($id_vsv);

            $data["id_vsv"] = $id_vsv;

            $data["bloc_menu"] = $this->vivresaville_villes->can_edit($id_vsv);

            

            if(isset($bloc_menu["id_bloc_menu"])){

              

        

           $this->vivresaville_villes->update_bloc($bloc_menu,$bloc_menu['id_bloc_menu']);

               

              

        

            }

        }

      

                    if(isset($id) && $id !=null){



                       $data['bloc_menu']=$this->vivresaville_villes->can_edit($id);

                    }

                   

                   

        $this->load->view('admin/vivresaville/home_mgt', $data) ;

     

        /**UPDATE FORM DATA */

    



    }



    

    function delete($prmId) {



        if(is_numeric(trim($prmId))) {

            //verify if category contain active agenda********

            $oCategorie = $this->vivresaville_villes->verifier_type_agenda($prmId) ;

            //$oCategorie = array();

            if (count($oCategorie)==0){

                $this->vivresaville_villes->supprimeagenda_type($prmId);

                $this->session->set_flashdata('mess_editTypeagenda', '3');

            } else {

                $this->session->set_flashdata('mess_editTypeagenda', '2');

            }



            //$this->firephp->log($oCategorie, 'oCategorie');



            redirect("admin/vivresaville");

        }

        else {

            redirect("admin/vivresaville");

        }



    }



    function getDepartmentList()

    {

        $CodePostal = $this->input->post("vsv_postalcode_main");

        $departements = $this->mdldepartement->GetDepByCodePostal($CodePostal);

        $result_to_show = '<select id="vsv_id_department" name="vsv_object[id_department]" class="form-control p-1" onchange="javascript:vsv_getVille_list();">';

        if (isset($departements)) {

            foreach ($departements as $item) {

                $result_to_show .= '<option value="' . $item->departement_id . '">' . $item->departement_nom . '</option>';

            }

        }

        $result_to_show .= '</select>';

        echo $result_to_show;

    }



    function getVIlleList()

    {

        $CodePostal = $this->input->post("vsv_postalcode_main");

        $departement_id = $this->input->post("vsv_id_department");

        $departements = $this->mdlville->GetVilleByCodePostal($CodePostal, $departement_id);

        $result_to_show = '<select name="vsv_object[id_ville]" id="vsv_id_ville" class="form-control p-1">';

        if (isset($departements)) {

            foreach ($departements as $item) {

                $result_to_show .= '<option value="' . $item->IdVille . '">' . $item->Nom . '</option>';

            }

        }

        $result_to_show .= '</select>';

        echo $result_to_show;

    }





    function remove_vsv_logo(){

        $vsv_id = $this->input->post("vsv_id");

        if (isset($vsv_id) && $vsv_id!="0" && $vsv_id!="") {

            $vsv_obj = $this->vivresaville_villes->getById($vsv_id);

            if (file_exists("application/resources/front/images/vivresaville/".$vsv_obj->logo)) unlink("application/resources/front/images/vivresaville/".$vsv_obj->logo);

            $vsv_id_updated = $this->vivresaville_villes->remove_logo($vsv_id);

            if (isset($vsv_id_updated)) echo $vsv_id_updated;

            else echo "error";

        } else echo "error";

    }

    function remove_vsv_logo_droite(){

        $vsv_id = $this->input->post("vsv_id");

        if (isset($vsv_id) && $vsv_id!="0" && $vsv_id!="") {

            $vsv_obj = $this->vivresaville_villes->getById($vsv_id);

            if (file_exists("application/resources/front/images/vivresaville/".$vsv_obj->logo_droite)) unlink("application/resources/front/images/vivresaville/".$vsv_obj->logo_droite);

            $vsv_id_updated = $this->vivresaville_villes->remove_logo_droite($vsv_id);

            if (isset($vsv_id_updated)) echo $vsv_id_updated;

            else echo "error";

        } else echo "error";

    }

    function remove_vsv_home_photo_site_web(){

        $vsv_id = $this->input->post("vsv_id");

        if (isset($vsv_id) && $vsv_id!="0" && $vsv_id!="") {

            $vsv_obj = $this->vivresaville_villes->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

            if (file_exists("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_site_web)) unlink("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_site_web);

            $vsv_id_updated = $this->vivresaville_villes->remove_vsv_home_photo_site_web($vsv_id);

            if (isset($vsv_id_updated)) echo $vsv_id_updated;

            else echo "error";

        } else echo "error";

    }

    function remove_vsv_home_photo_articleb(){

        $vsv_id = $this->input->post("vsv_id");

        if (isset($vsv_id) && $vsv_id!="0" && $vsv_id!="") {

            $vsv_obj = $this->vivresaville_villes->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

            if (file_exists("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_articleb)) unlink("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_articleb);

            $vsv_id_updated = $this->vivresaville_villes->remove_vsv_home_photo_articleb($vsv_id);

            if (isset($vsv_id_updated)) echo $vsv_id_updated;

            else echo "error1";

        } else echo "error2";

    }

    function remove_vsv_home_photo_revue(){

        $vsv_id = $this->input->post("vsv_id");

        if (isset($vsv_id) && $vsv_id!="0" && $vsv_id!="") {

            $vsv_obj = $this->vivresaville_villes->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

            if (file_exists("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_revue)) unlink("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_revue);

            $vsv_id_updated = $this->vivresaville_villes->remove_vsv_home_photo_revue($vsv_id);

            if (isset($vsv_id_updated)) echo $vsv_id_updated;

            else echo "error";

        } else echo "error";

    }

    function remove_vsv_home_photo_bonnes_adresses(){

        $vsv_id = $this->input->post("vsv_id");

        if (isset($vsv_id) && $vsv_id!="0" && $vsv_id!="") {

            $vsv_obj = $this->vivresaville_villes->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

            if (file_exists("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_bonnes_adresses)) unlink("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_bonnes_adresses);

            $vsv_id_updated = $this->vivresaville_villes->remove_vsv_home_photo_bonnes_adresses($vsv_id);

            if (isset($vsv_id_updated)) echo $vsv_id_updated;

            else echo "error";

        } else echo "error";

    }

    function remove_vsv_home_photo_commerce(){

        $vsv_id = $this->input->post("vsv_id");

        if (isset($vsv_id) && $vsv_id!="0" && $vsv_id!="") {

            $vsv_obj = $this->vivresaville_villes->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

            if (file_exists("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_commerce)) unlink("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_commerce);

            $vsv_id_updated = $this->vivresaville_villes->remove_vsv_home_photo_commerce($vsv_id);

            if (isset($vsv_id_updated)) echo $vsv_id_updated;

            else echo "error";

        } else echo "error";

    }

    function remove_vsv_home_photo_form(){

        $vsv_id = $this->input->post("vsv_id");

        if (isset($vsv_id) && $vsv_id!="0" && $vsv_id!="") {

            $vsv_obj = $this->vivresaville_villes->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

            if (file_exists("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_form)) unlink("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_form);

            $vsv_id_updated = $this->vivresaville_villes->remove_vsv_home_photo_form($vsv_id);

            if (isset($vsv_id_updated)) echo $vsv_id_updated;

            else echo "error";

        } else echo "error";

    }

    function remove_vsv_home_photo_deal(){

        $vsv_id = $this->input->post("vsv_id");

        if (isset($vsv_id) && $vsv_id!="0" && $vsv_id!="") {

            $vsv_obj = $this->vivresaville_villes->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

            if (file_exists("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_deal)) unlink("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_deal);

            $vsv_id_updated = $this->vivresaville_villes->remove_vsv_home_photo_deal($vsv_id);

            if (isset($vsv_id_updated)) echo $vsv_id_updated;

            else echo "error";

        } else echo "error";

    }

    function remove_vsv_home_photo_carte(){

        $vsv_id = $this->input->post("vsv_id");

        if (isset($vsv_id) && $vsv_id!="0" && $vsv_id!="") {

            $vsv_obj = $this->vivresaville_villes->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

            if (file_exists("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_carte)) unlink("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_carte);

            $vsv_id_updated = $this->vivresaville_villes->remove_vsv_home_photo_carte($vsv_id);

            if (isset($vsv_id_updated)) echo $vsv_id_updated;

            else echo "error";

        } else echo "error";

    }





    function remove_vsv_background(){

        $vsv_id = $this->input->post("vsv_id");

        if (isset($vsv_id) && $vsv_id!="0" && $vsv_id!="") {

            $vsv_obj = $this->vivresaville_villes->getById($vsv_id);

            if (file_exists("application/resources/front/images/vivresaville/".$vsv_obj->background)) unlink("application/resources/front/images/vivresaville/".$vsv_obj->background);

            $vsv_id_updated = $this->vivresaville_villes->remove_background($vsv_id);

            if (isset($vsv_id_updated)) echo $vsv_id_updated;

            else echo "error";

        } else echo "error";

    }



    function remove_vsv_home_photo_banner(){

        $vsv_id = $this->input->post("vsv_id");

        if (isset($vsv_id) && $vsv_id!="0" && $vsv_id!="") {

            $vsv_obj = $this->vivresaville_villes->getById($vsv_id);

            if (file_exists("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_banner)) unlink("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_banner);

            $vsv_id_updated = $this->vivresaville_villes->remove_vsv_home_photo_banner($vsv_id);

            if (isset($vsv_id_updated)) echo $vsv_id_updated;

            else echo "error";

        } else echo "error";

    }



    function remove_vsv_home_photo_article(){

        $vsv_id = $this->input->post("vsv_id");

        if (isset($vsv_id) && $vsv_id!="0" && $vsv_id!="") {

            $vsv_obj = $this->vivresaville_villes->getById($vsv_id);

            if (file_exists("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_article)) unlink("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_article);

            $vsv_id_updated = $this->vivresaville_villes->remove_vsv_home_photo_article($vsv_id);

            if (isset($vsv_id_updated)) echo $vsv_id_updated;

            else echo "error";

        } else echo "error";

    }



    function remove_vsv_home_photo_agenda(){

        $vsv_id = $this->input->post("vsv_id");

        if (isset($vsv_id) && $vsv_id!="0" && $vsv_id!="") {

            $vsv_obj = $this->vivresaville_villes->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

            if (file_exists("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_agendab)) unlink("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_agendab);

            $vsv_id_updated = $this->vivresaville_villes->remove_vsv_home_photo_agenda($vsv_id);

            if (isset($vsv_id_updated)) echo $vsv_id_updated;

            else echo "error";

        } else echo "error";

    }



    function remove_vsv_home_photo_annonce(){

        $vsv_id = $this->input->post("vsv_id");

        if (isset($vsv_id) && $vsv_id!="0" && $vsv_id!="") {

            $vsv_obj = $this->vivresaville_villes->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

            if (file_exists("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_annonceb)) unlink("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_annonceb);

            $vsv_id_updated = $this->vivresaville_villes->remove_vsv_home_photo_annonce($vsv_id);

            if (isset($vsv_id_updated)) echo $vsv_id_updated;

            else echo "error";

        } else echo "error";

    }



    function remove_vsv_home_photo_annuaire(){

        $vsv_id = $this->input->post("vsv_id");

        if (isset($vsv_id) && $vsv_id!="0" && $vsv_id!="") {

            $vsv_obj = $this->vivresaville_villes->getById($vsv_id);

            if (file_exists("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_annuaire)) unlink("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_annuaire);

            $vsv_id_updated = $this->vivresaville_villes->remove_vsv_home_photo_annuaire($vsv_id);

            if (isset($vsv_id_updated)) echo $vsv_id_updated;

            else echo "error";

        } else echo "error";

    }



    function remove_vsv_home_photo_fidelite(){

        $vsv_id = $this->input->post("vsv_id");

        if (isset($vsv_id) && $vsv_id!="0" && $vsv_id!="") {

            $vsv_obj = $this->vivresaville_villes->getById($vsv_id);

            if (file_exists("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_fidelite)) unlink("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_fidelite);

            $vsv_id_updated = $this->vivresaville_villes->remove_vsv_home_photo_fidelite($vsv_id);

            if (isset($vsv_id_updated)) echo $vsv_id_updated;

            else echo "error";

        } else echo "error";

    }





    function remove_vsv_home_photo_bonplan(){

        $vsv_id = $this->input->post("vsv_id");

        if (isset($vsv_id) && $vsv_id!="0" && $vsv_id!="") {

            $vsv_obj = $this->vivresaville_villes->getById($vsv_id);

            if (file_exists("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_bonplan)) unlink("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_bonplan);

            $vsv_id_updated = $this->vivresaville_villes->remove_vsv_home_photo_bonplan($vsv_id);

            if (isset($vsv_id_updated)) echo $vsv_id_updated;

            else echo "error";

        } else echo "error";

    }



    function remove_vsv_home_photo_newsletter(){

        $vsv_id = $this->input->post("vsv_id");

        if (isset($vsv_id) && $vsv_id!="0" && $vsv_id!="") {

            $vsv_obj = $this->vivresaville_villes->getById($vsv_id);

            if (file_exists("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_newsletter)) unlink("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_newsletter);

            $vsv_id_updated = $this->vivresaville_villes->remove_vsv_home_photo_newsletter($vsv_id);

            if (isset($vsv_id_updated)) echo $vsv_id_updated;

            else echo "error";

        } else echo "error";

    }



    function remove_vsv_home_photo_gestion(){

        $vsv_id = $this->input->post("vsv_id");

        if (isset($vsv_id) && $vsv_id!="0" && $vsv_id!="") {

            $vsv_obj = $this->vivresaville_villes->getById($vsv_id);

            if (file_exists("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_gestion)) unlink("application/resources/front/images/vivresaville/".$vsv_obj->home_photo_gestion);

            $vsv_id_updated = $this->vivresaville_villes->remove_vsv_home_photo_gestion($vsv_id);

            if (isset($vsv_id_updated)) echo $vsv_id_updated;

            else echo "error";

        } else echo "error";

    }



    /**new code */

function edit_bloc($edit_id,$id_ville){



$data['bloc_menu']=$this->vivresaville_villes->can_edit($edit_id);





    $this->load->view('admin/vivresaville/home_mgt',$data); 

  



 

}



function update(){

// $data['bloc_menu']=$this->vivresaville_villes->can_edit($id);

        $bloc_menu=$this->input->post('bloc_menu');



        // $link=$this->input->post('link');

        // $titre=$this->input->post('titre');

        // $bloc_text=$this->input->post('bloc_text');

        // $bloc_id = $this->input->post('id_bloc_menu');

        

$this->vivresaville_villes->update_bloc($bloc_menu);

    $this->load->view('admin/vivresaville/home_mgt',$data);

}



function clear($clear_id,$id_ville){

    $this->vivresaville_villes->delete_bloc($clear_id);

    $data['h']=$this->vivresaville_villes->show_list($id_ville);



    redirect('admin/vivresaville/home_mgt/'.$id_ville,$data);





}

   



}





?>