<?php
class pack extends CI_Controller{

    function __construct() {
        parent::__construct();

        $this->load->library('session');
        $this->load->library('ion_auth');
        $this->load->model("packarticle_pack");
        $this->load->model("ion_auth_used_by_club");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }

    function index(){
        $this->liste();
    }

     function liste() {
        $objPack = $this->packarticle_pack->GetAll();
        $data["colPack"] = $objPack;

        if($this->session->flashdata('mess_editPack')=='1') $data['msg'] = '<strong style="color:#060">Type enregistré !</strong>';
        if($this->session->flashdata(' mess_editPack')=='2') $data['msg'] = '<strong style="color:#F00">Cet pack ne peut être supprimé ! Pack lié à une catégorie !</strong>';
        if($this->session->flashdata(' mess_editPack')=='3') $data['msg'] = '<strong style="color:#060">Pack supprimé !</strong>';

        $this->load->view("packarticle/admin/vwListePack",$data);
    }

    function fiche_pack($IdPack) {

        if ($IdPack != 0){
            $data["title"] = "Modification  Pack" ;
            $data["oPack"] = $this->packarticle_pack->getById($IdPack) ;
            $data["id"] = $IdPack;
            $this->load->view('packarticle/admin/vwFichePack', $data) ;

        }else{
            $data["title"] = "Creer un Pack" ;
            $this->load->view('packarticle/admin/vwFichePack', $data) ;
        }
    }

    function creer_pack_objet(){
        $oPack = $this->input->post("pack") ;
        $this->packarticle_pack->insertpackarticle_pack($oPack);
        $data["msg"] = "Pack ajouté" ;

        $objPack = $this->packarticle_pack->GetAll();
        $data["colPack"] = $objPack;
        $this->load->view("packarticle/admin/vwListePack",$data);
    }

    function modif_pack($IdPack){
        $oPack = $this->input->post("pack") ;
        $this->packarticle_pack->updatepackarticle_pack($oPack);
        $data["msg"] = "Pack enregistré" ;

        $objPack = $this->packarticle_pack->GetAll();
        $data["colPack"] = $objPack;
        $this->load->view("packarticle/admin/vwListePack",$data);
    }

    
    function delete($prmId) {

        if(is_numeric(trim($prmId))) {
            //verify if category contain active article********
            $oCategorie = $this->packarticle_pack->verifier_pack_objet($prmId) ;
            //$oCategorie = array();
            if (count($oCategorie)==0){
                $this->packarticle_pack->supprimepackarticle_pack($prmId);
                $this->session->set_flashdata('mess_editPack', '3');
            } else {
                $this->session->set_flashdata('mess_editPack', '2');
            }

            //$this->firephp->log($oCategorie, 'oCategorie');

            redirect("admin/pack");
        }
        else {
            redirect("admin/pack");
        }

    }


}
    

