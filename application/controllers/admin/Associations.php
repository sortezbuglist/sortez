<?php
class Associations extends CI_Controller{

    private $mCurrentFilter = 2; // Le type de filtre en cours (0 : exact, 1 : d?but, 2 : milieu)
    private $mSearchValue = ""; // La valeur ? rechercher
    private $mLinesPerPage = 50 ;// Le nombre de lignes (enregistrements) ? afficher par page
    
    function __construct() {
        parent::__construct();
        $this->load->Model("association");
        $this->load->Model("Commercant");
        $this->load->Model("Rubrique");
        $this->load->Model("SousRubrique");
        $this->load->Model("Ville");
        $this->load->Model("mdlville");
        $this->load->Model("mdldepartement");
        $this->load->Model("Abonnement");
        $this->load->Model("AssCommercantRubrique");
        $this->load->Model("AssCommercantSousRubrique");
        $this->load->Model("AssCommercantAbonnement");
        $this->load->Model("mdldemande_abonnement");

        $this->load->Model("mdlbonplan");
        $this->load->Model("mdlannonce");
        $this->load->Model("mdl_agenda");
        $this->load->Model("mdlagenda_perso");
		
        $this->load->model("mdlcategorie");
        
        $this->load->library('session');

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_model");
        $this->load->model("ion_auth_used_by_club");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }
    
    function index(){
        $this->liste();
    }


    public function GetCurrentFilter() {
        if (isset($_POST["optTypeFiltre"])) {
            return $_POST["optTypeFiltre"] ;
        } else {
            if ($this->session->userdata("curFilter")== null) {
                return 2;
            }
            return $this->session->userdata("curFilter") ;
        }
    }

    public function SetCurrentFilter($argValue) {
        $this->mCurrentFilter = $argValue ;
        $this->session->set_userdata("curFilter",$argValue) ;
    }

    public function GetSearchValue() {
        $SearchValue ;
        if (isset($_POST["txtSearch"])) {
            $SearchValue = $_POST["txtSearch"] ;
        } else {
            if ($this->session->userdata("SearchValue")== null) {
                $SearchValue = "";
            }
            $SearchValue = $this->session->userdata("SearchValue");
        }

        return addslashes($SearchValue) ;
    }
    public function SetSearchValue($argValue) {
        $this->mSearchValue = $argValue ;
        $this->session->set_userdata("SearchValue",$argValue) ;
    }

    public function GetLinesPerPage() {
        if (isset($_POST["cmbNbLignes"])) {
            return  $_POST["cmbNbLignes"];
        } else {
            if ($this->session->userdata("LinesPerPage")== null) {
                return 50;
            }
            return $this->session->userdata("LinesPerPage");
        }
    }

    public function SetLinesPerPage($argValue) {
        $this->mLinesPerPage = $argValue ;
        $this->session->set_userdata("LinesPerPage",$argValue) ;
    }
    
    public function GetOrder() {
        if (isset($_POST["hdnOrder"])) {
            return $_POST["hdnOrder"] ;
        } else {
            if ($this->session->userdata("UserOrder")== null) {
                return "";
            }
            return $this->session->userdata("UserOrder") ;
        }
    }

    public function SetOrder($argValue) {
        $this->session->set_userdata("UserOrder",$argValue) ;
    }
    
    function _clearAllSessionsData() {
        $this->SetCurrentFilter(2);
        $this->SetSearchValue("");
        $this->SetLinesPerPage(50);
        $this->SetOrder("");
    }       

    

    /**
     * Fontion utilis?e pour l'affichage de la liste des association (avec pagination)
     *
     * @param unknown_type $prmPagerIndex
     * @param unknown_type $prmIsCompleteList
     * @param unknown_type $prmOrder
     */
    public function liste($prmFilterCol = "-1", $prmFilterValue = "0", $prmPagerIndex = 0, $prmIsCompleteList = false, $prmOrder = "") {
        $_SESSION["User_FilterCol"] = $prmFilterCol;
        $_SESSION["User_FilterValue"] = $prmFilterValue;
        
        if ($prmIsCompleteList) {
            $this->_clearAllSessionsData();
        }

        if (isset($_POST["cmbNbLignes"])) {
            $this->SetLinesPerPage($_POST["cmbNbLignes"]);
        }

        if (isset($_POST["txtSearch"])) {
            $this->SetSearchValue($_POST["txtSearch"]);
        }

        if (isset($_POST["optTypeFiltre"])) {
           $this->SetCurrentFilter($_POST["optTypeFiltre"]);
        }
        
        if (isset($_POST["hdnOrder"])) {
            $this->SetOrder($_POST["hdnOrder"]);
        }
        
        $SearchedWordsString = str_replace("+", " ", $this->GetSearchValue()) ;
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString) ;
        $WhereKeyWords = " 0=0 " ;
        
               
        for ($i = 0; $i < sizeof($SearchedWords); $i ++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search) ;
            if ($Search != "") {
                if ($i == 0) {
                    $WhereKeyWords .= " AND " ;
                }
                                
                $WhereKeyWords .= " ( " .
                    " UPPER(associations.IdAssociation ) LIKE '%" . strtoupper($Search) . "%'" . 
                    " OR UPPER(associations.Nom ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(associations.NomResp ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(associations.PrenomResp ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(associations.Email ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(associations.adresse ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(associations.CodePostal ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(associations.tel ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(associations.mobile ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(associations.Siteweb ) LIKE '%" . strtoupper($Search) . "%'" .
                " )
                ";
                if ($i != (sizeof($SearchedWords) - 1)){
                    $WhereKeyWords .= " OR ";
                }
            }
        }
        
        // Pre-filtre sur une table parent (cas d'une liste fille dans une fiche maitre-detail)
        if ($prmFilterCol != "-1") {
            $WhereKeyWords .= " AND " . $prmFilterCol . " = '" . $prmFilterValue . "'" ;
        }

        
        
        // echo $WhereKeyWords; exit;
        //$objCommercants = $this->Commercant->GetAll();        
        $colusers = $this->association->getWhere($WhereKeyWords, $prmPagerIndex, $this->GetLinesPerPage(), $this->GetOrder());
        
        
        // Pagination (CI)
        $this->load->library("pagination");
        $this->config->load("config");
        $pagination_config['uri_segment'] = '6';
        $data["UriSegment"] = $pagination_config['uri_segment'];
        $pagination_config["base_url"] = site_url() . "admin/associations/liste/" . $prmFilterCol . "/" . $prmFilterValue ;
        $pagination_config["total_rows"] = $this->association->CountWhere_pvc($WhereKeyWords);
        $pagination_config["per_page"] = $this->GetLinesPerPage();
        $pagination_config["first_link"] = '&lsaquo;';
        $pagination_config["last_link"] = '&raquo';
        $pagination_config["next_link"] = '&gt;';
        $pagination_config["prev_link"] = '&lt;';
        $pagination_config["cur_tag_open"] = "<span style='font-weight: bold; padding-right: 5px; padding-left: 5px; '>" ;
        $pagination_config["cur_tag_close"] = "</span>" ;
        $pagination_config["num_tag_open"] = "&nbsp;<span>" ;
        $pagination_config["num_tag_close"] = "&nbsp;</span>" ;

        $this->pagination->initialize($pagination_config);
        // Effectuer un highlight des mots-cles recherches dans la liste des enregistrements trouv&s
        $data["highlight"] = "";
        if ($this->GetSearchValue() != "") {
            $data["highlight"] = "yes";
        } else {
            $data["highlight"] = "";
        }
        
        // Les numeros de page
        $NumerosPages = array();
        //$NumerosPages[] = 0;
        $n = 0;
        while($n < $pagination_config["total_rows"] - 1) {
            $NumerosPages[] = $n;
            $n += $pagination_config["per_page"];
        }
        $data["NumerosPages"] = $NumerosPages;
        
        
        // Preparer les variables pour le view
        $data["colUsers"] = $colusers ;

        $data["PaginationLinks"] = $this->pagination->create_links();
        $data["NbLignes"] = $this->GetLinesPerPage() ;
        $data["CurFiltre"] = $this->GetCurrentFilter();
        $data["SearchValue"] = $this->GetSearchValue();
        $data["SearchedWords"] = $SearchedWords;
        $data["Keywords"] = $this->GetSearchValue();
        $data["CountAllResults"] = $pagination_config["total_rows"];
        $data["Ordre"] = $this->GetOrder();
        
        $DepartCount = $prmPagerIndex;
        $data["DepartCount"] =  $DepartCount;
        
        // Data pour le pre-filtre
        $data["FilterCol"] = $prmFilterCol; 
        $data["FilterValue"] = $prmFilterValue;
        
        $data["no_main_menu_home"] = "1";

        $data["ion_auth_used_by_club"] = $this->ion_auth_used_by_club;
        $data["ion_auth"] = $this->ion_auth;
        
        //$this->load->view("admin/vwUsersadmin", $data) ;
        //$this->load->view("admin/vwListeCommercants", $data) ;
        $this->load->view("admin/vwListeAssociations", $data) ;
    }
        



    
    function fiche($prmId = "0") {
        $data = null;
        $this->load->Model("Ville");
        
        $data["colVilles"] = $this->Ville->GetAll();
        $data["objUser"] = $this->association->getById($prmId);
        $data['prmId'] = $prmId;

        $data['mdlville'] = $this->mdlville;
        $data["colDepartement"] = $this->mdldepartement->GetAll();
        
        $data["no_main_menu_home"] = "1";

        //$user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_user_id("1");
        //$user_ion_auth = $this->ion_auth->user()->row($user_ion_auth_id);
        ////$this->firephp->log($user_ion_auth, 'user_ion_auth');
        
        $this->load->view("admin/vwAssociation", $data);
    }



     function fiche_ajouter() {
        $User = $this->input->post("Particulier");
        //$User["DateNaissance"] = convert_Frenchdate_to_Sqldate($User["DateNaissance"]);
        $User["Login"] = $User["Email"];

        //ion_auth register
        $username_ion_auth = $User['Login'];
        $password_ion_auth = $this->input->post("Particulier_Password");
        $email_ion_auth = $User['Email'];
        $additional_data_ion_auth = array('first_name' => $User['NomResp'],'last_name' => $User['PrenomResp'],'company' => $User['Nom'], 'phone' => $User['Telephone']);
        $group_ion_auth = array('6'); // Sets user to association.
        $this->load->library('ion_auth');
        $this->ion_auth->register($username_ion_auth, $password_ion_auth, $email_ion_auth, $additional_data_ion_auth, $group_ion_auth);
        $ion_ath_error = $this->ion_auth->errors();
        $user_lastid_ionauth = $this->ion_auth_used_by_club->GetUserlast_id($User['Nom'], $username_ion_auth, $email_ion_auth);
        $User["user_ionauth_id"] = $user_lastid_ionauth;
        //ion_auth register

        if (isset($ion_ath_error) && $ion_ath_error!="") {
            $data["txtContenu"] = "Une erreur s'est produite durant l'enregistrement de vos données. Veuillez recommencer s'il vous plait.".$ion_ath_error;
            $this->load->view("front/vwErreurInscriptionPro", $data);
        } else {
            $this->association->Insert($User);
            $this->retour();
        }


    }

    function fiche_modifier() {
        $User = $this->input->post("Particulier");
        //$User["DateNaissance"] = convert_Frenchdate_to_Sqldate($User["DateNaissance"]);
        $User["Login"] = $User["Email"];

        //ion_auth_update
        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_association_id($User["IdAssociation"]);
        $user_ion_auth = $this->ion_auth->user()->row($user_ion_auth_id);
        $data_ion_auth_update = array(
            //'username' => $User['Login'],
            //'email' => $User['Email'],
            'first_name' => $User['NomResp'],
            'last_name' => $User['PrenomResp'],
            'company' => $User['Nom']
        );
        $this->ion_auth->update($user_ion_auth->id, $data_ion_auth_update);
        $ion_ath_error = $this->ion_auth->errors();
        //ion_auth_update

        if (isset($ion_ath_error) && $ion_ath_error!="") {
            $data["txtContenu"] = "Une erreur s'est produite durant l'enregistrement de vos données. Veuillez recommencer s'il vous plait.".$ion_ath_error;
            $this->load->view("front/vwErreurInscriptionPro", $data);
        } else {
            $this->association->Update($User);
            $this->retour();
        }
    }



    function retour() {
        if (isset($_POST["hdnNextUrl"])) {
            redirect("admin/" . $_POST["hdnNextUrl"]);
        } else {
            $FilterCol = "-1";
            $FilterValue = "0";
            if (isset($_SESSION["User_FilterCol"])) {
                $FilterCol = $_SESSION["User_FilterCol"];
            }
            if (isset($_SESSION["User_FilterValue"])) {
                $FilterValue = $_SESSION["User_FilterValue"];
            }
            redirect("admin/associations/liste/" . $FilterCol . "/" . $FilterValue . "/0/true");
        }
    }




    function supprimer ($prmIdCommercant = 0) {
        /*$data = null;

        $data["colRubriques"] = $this->Rubrique->GetAll();
        $data["colSousRubriques"] = $this->SousRubrique->GetAll();
        $data["colVilles"] = $this->Ville->GetAll();
        $data["colAbonnements"] = $this->Abonnement->GetAll();

        $objCommercant_to_send = $this->Commercant->GetById($prmIdCommercant);
        $data["objCommercant"] = $objCommercant_to_send;
        $data["objAssCommercantRubrique"] = $this->AssCommercantRubrique->GetWhere("ass_commercants_rubriques.IdCommercant = " . $prmIdCommercant);
        $data["objAssCommercantSousRubrique"] = $this->AssCommercantSousRubrique->GetWhere("ass_commercants_sousrubriques.IdCommercant = " . $prmIdCommercant);
        $data["objAssCommercantAbonnement"] = $this->AssCommercantAbonnement->GetWhere("ass_commercants_abonnements.IdCommercant = " . $prmIdCommercant);

        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($objCommercant_to_send->IdCommercant);
        if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
        $data["user_groups"] = $user_groups[0];
        //////$this->firephp->log($user_groups[0], 'user_groups');

        $data["colDemandeAbonnement"] = $this->mdldemande_abonnement->getByIdCommercantIdUser($prmIdCommercant, $user_ion_auth_id);

        $data["mdlville"] = $this->mdlville;
        //$this->load->view("admin/vwFicheCommercant",$data);

        $data["colBonplan"] = $this->mdlbonplan->lastBonplanCom2($prmIdCommercant);
        $data["colAnnonces"] = $this->mdlannonce->listeAnnonceParCommercant($prmIdCommercant);
        $data["colAgenda"] = $this->mdl_agenda->listeAgendaRecherche(0,0,0,"",0,0,10000,"","0","0000-00-00","0000-00-00",$prmIdCommercant,"0");
        $data["colAgendaPerso"] = $this->mdlagenda_perso->GetAgenda_agenda_persoByUser($prmIdCommercant, $user_ion_auth_id);

        $this->load->view("privicarte/admin/vwSupprimerCommercant",$data);*/
    }

    function supprimer_validate ($prmIdCommercant = 0) { 
        $data = null;

        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($prmIdCommercant);

        $colBonplan = $this->mdlbonplan->lastBonplanCom2($prmIdCommercant);
        if (isset($colBonplan) && count($colBonplan)>0) {
            foreach ($colBonplan as $key_bonplan) {
                $this->mdlbonplan->supprimeBonplans($key_bonplan->bonplan_id);
            }
        }

        $colAnnonces = $this->mdlannonce->listeAnnonceParCommercant($prmIdCommercant);
        if (isset($colAnnonces) && count($colAnnonces)>0) {
            foreach ($colAnnonces as $key_annonce) {
                $this->mdlannonce->supprimeAnnonces($key_annonce->annonce_id);
            }
        }

        $colAgenda = $this->mdl_agenda->listeAgendaRecherche(0,0,0,"",0,0,10000,"","0","0000-00-00","0000-00-00",$prmIdCommercant,"0");
        if (isset($colAgenda) && count($colAgenda)>0) {
            foreach ($colAgenda as $key_agenda) {
                $this->mdl_agenda->delete_definitif($key_annonce->id);
            }
        }

        $colAgendaPerso = $this->mdlagenda_perso->GetAgenda_agenda_persoByUser($prmIdCommercant, $user_ion_auth_id);
        if (isset($colAgendaPerso) && count($colAgendaPerso)>0) {
            foreach ($colAgendaPerso as $key_agendaPerso) {
                $this->mdlagenda_perso->delete_agenda_perso($key_agendaPerso->id);
            }
        }

        $this->AssCommercantAbonnement->DeleteByIdCommercant($prmIdCommercant);
        $this->AssCommercantRubrique->DeleteByIdCommercant($prmIdCommercant);
        $this->AssCommercantSousRubrique->DeleteByIdCommercant($prmIdCommercant);


        $this->Commercant->delete_validate($prmIdCommercant);

        $deactivate_final = $this->ion_auth_model->deactivate($user_ion_auth_id);
        $delete_final = $this->ion_auth_model->delete_user($user_ion_auth_id);
        

        
        
        redirect("admin/commercants/liste");


        //abonnement


    }    
    
    
    
   
    
    
}