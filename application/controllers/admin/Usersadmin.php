<?php
class usersadmin extends CI_Controller {
   
    private $mCurrentFilter = 2; // Le type de filtre en cours (0 : exact, 1 : d?but, 2 : milieu)
    private $mSearchValue = ""; // La valeur ? rechercher
    private $mLinesPerPage = 50 ;// Le nombre de lignes (enregistrements) ? afficher par page

    function __construct() {
        parent::__construct();

        $this->load->model("User");
        $this->load->library('session');

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }


    public function GetCurrentFilter() {
        if (isset($_POST["optTypeFiltre"])) {
            return $_POST["optTypeFiltre"] ;
        } else {
            if ($this->session->userdata("curFilter")== null) {
                return 2;
            }
            return $this->session->userdata("curFilter") ;
        }
    }

    public function SetCurrentFilter($argValue) {
        $this->mCurrentFilter = $argValue ;
        $this->session->set_userdata("curFilter",$argValue) ;
    }

    public function GetSearchValue() {
        $SearchValue ;
        if (isset($_POST["txtSearch"])) {
            $SearchValue = $_POST["txtSearch"] ;
        } else {
            if ($this->session->userdata("SearchValue")== null) {
                $SearchValue = "";
            }
            $SearchValue = $this->session->userdata("SearchValue");
        }

        return addslashes($SearchValue) ;
    }
    public function SetSearchValue($argValue) {
        $this->mSearchValue = $argValue ;
        $this->session->set_userdata("SearchValue",$argValue) ;
    }

    public function GetLinesPerPage() {
        if (isset($_POST["cmbNbLignes"])) {
            return  $_POST["cmbNbLignes"];
        } else {
            if ($this->session->userdata("LinesPerPage")== null) {
                return 50;
            }
            return $this->session->userdata("LinesPerPage");
        }
    }

    public function SetLinesPerPage($argValue) {
        $this->mLinesPerPage = $argValue ;
        $this->session->set_userdata("LinesPerPage",$argValue) ;
    }
    
    public function GetOrder() {
        if (isset($_POST["hdnOrder"])) {
            return $_POST["hdnOrder"] ;
        } else {
            if ($this->session->userdata("UserOrder")== null) {
                return "";
            }
            return $this->session->userdata("UserOrder") ;
        }
    }

    public function SetOrder($argValue) {
        $this->session->set_userdata("UserOrder",$argValue) ;
    }
    
    function _clearAllSessionsData() {
        $this->SetCurrentFilter(2);
        $this->SetSearchValue("");
        $this->SetLinesPerPage(50);
        $this->SetOrder("");
    }
       

    function index() {
        $this->liste();
    }
    
    /**
     * Fontion utilis?e pour l'affichage de la liste des users (avec pagination)
     *
     * @param unknown_type $prmPagerIndex
     * @param unknown_type $prmIsCompleteList
     * @param unknown_type $prmOrder
     */
    public function liste($prmFilterCol = "-1", $prmFilterValue = "0", $prmPagerIndex = 0, $prmIsCompleteList = false, $prmOrder = "") {
        $_SESSION["User_FilterCol"] = $prmFilterCol;
        $_SESSION["User_FilterValue"] = $prmFilterValue;
        
        if ($prmIsCompleteList) {
            $this->_clearAllSessionsData();
        }

        if (isset($_POST["cmbNbLignes"])) {
            $this->SetLinesPerPage($_POST["cmbNbLignes"]);
        }

        if (isset($_POST["txtSearch"])) {
            $this->SetSearchValue($_POST["txtSearch"]);
        }

        if (isset($_POST["optTypeFiltre"])) {
           $this->SetCurrentFilter($_POST["optTypeFiltre"]);
        }
        
        if (isset($_POST["hdnOrder"])) {
            $this->SetOrder($_POST["hdnOrder"]);
        }
        
        $SearchedWordsString = str_replace("+", " ", $this->GetSearchValue()) ;
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString) ;
        $WhereKeyWords = " 0=0 " ;
        
               
        for ($i = 0; $i < sizeof($SearchedWords); $i ++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search) ;
            if ($Search != "") {
                if ($i == 0) {
                    $WhereKeyWords .= " AND " ;
                }
                                
                $WhereKeyWords .= " ( " .
                    " UPPER(users.IdUser ) LIKE '%" . strtoupper($Search) . "%'" . 
                    " OR UPPER(users.Nom ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(users.Prenom ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(users.Email ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'" .
                " )
                ";
                if ($i != (sizeof($SearchedWords) - 1)){
                    $WhereKeyWords .= " OR ";
                }
            }
        }
        
        // Pre-filtre sur une table parent (cas d'une liste fille dans une fiche maitre-detail)
        if ($prmFilterCol != "-1") {
            $WhereKeyWords .= " AND " . $prmFilterCol . " = '" . $prmFilterValue . "'" ;
        }

        $WhereKeyWords .= " AND users.UserRole = '1' " ;//don't show Admin users
        
        // echo $WhereKeyWords; exit;
        
        $colusers = $this->User->GetWhere($WhereKeyWords, $prmPagerIndex, $this->GetLinesPerPage(), $this->GetOrder());
        
        
        // Pagination (CI)
        $this->load->library("pagination");
        $this->config->load("config");
        $pagination_config['uri_segment'] = '6';
        $data["UriSegment"] = $pagination_config['uri_segment'];
        $pagination_config["base_url"] = site_url() . "admin/usersadmin/liste/" . $prmFilterCol . "/" . $prmFilterValue ;
        $pagination_config["total_rows"] = $this->User->CountWhere($WhereKeyWords);
        $pagination_config["per_page"] = $this->GetLinesPerPage();
        $pagination_config["first_link"] = '&lsaquo;';
        $pagination_config["last_link"] = '&raquo';
        $pagination_config["next_link"] = '&gt;';
        $pagination_config["prev_link"] = '&lt;';
        $pagination_config["cur_tag_open"] = "<span style='font-weight: bold; padding-right: 5px; '>" ;
        $pagination_config["cur_tag_close"] = "</span>" ;
        $pagination_config["num_tag_open"] = "<span>" ;
        $pagination_config["num_tag_close"] = "</span>" ;

        $this->pagination->initialize($pagination_config);
        // Effectuer un highlight des mots-cles recherches dans la liste des enregistrements trouv&s
        $data["highlight"] = "";
        if ($this->GetSearchValue() != "") {
            $data["highlight"] = "yes";
        } else {
            $data["highlight"] = "";
        }
        
        // Les numeros de page
        $NumerosPages = array();
        //$NumerosPages[] = 0;
        $n = 0;
        while($n < $pagination_config["total_rows"] - 1) {
            $NumerosPages[] = $n;
            $n += $pagination_config["per_page"];
        }
        $data["NumerosPages"] = $NumerosPages;
        
        
        // Preparer les variables pour le view
        $data["colUsers"] = $colusers ;

        $data["PaginationLinks"] = $this->pagination->create_links();
        $data["NbLignes"] = $this->GetLinesPerPage() ;
        $data["CurFiltre"] = $this->GetCurrentFilter();
        $data["SearchValue"] = $this->GetSearchValue();
        $data["SearchedWords"] = $SearchedWords;
        $data["Keywords"] = $this->GetSearchValue();
        $data["CountAllResults"] = $pagination_config["total_rows"];
        $data["Ordre"] = $this->GetOrder();
        
        $DepartCount = $prmPagerIndex;
        $data["DepartCount"] =  $DepartCount;
        
        // Data pour le pre-filtre
        $data["FilterCol"] = $prmFilterCol; 
        $data["FilterValue"] = $prmFilterValue;
        
        $data["no_main_menu_home"] = "1";
        
            $this->load->view("admin/vwUsersadmin", $data) ;
    }
        
    function fiche($prmId = "0") {
        $data = null;
        $this->load->Model("Ville");
        
        $data["colVilles"] = $this->Ville->GetAll();
        $data["objUser"] = $this->User->getById($prmId);
        $data['prmId'] = $prmId;
        
        $data["no_main_menu_home"] = "1";
        
        $this->load->view("admin/vwUseradmin", $data);
    }
        
    function fiche_ajouter() {
        $User = $this->input->post("Particulier");
        $User["DateNaissance"] = convert_Frenchdate_to_Sqldate($User["DateNaissance"]);
        $User["Email"] = $User["Login"];
        $User['UserRole'] = '1';

        //ion_auth register
        $username_ion_auth = $User['Login'];
        $password_ion_auth = $this->input->post("Particulier_Password");
        $email_ion_auth = $User['Email'];
        $additional_data_ion_auth = array('first_name' => $User['Nom'],'last_name' => $User['Prenom'], 'phone' => $User['Telephone']);
        $group_ion_auth = array('1'); // Sets user to particulier.
        $this->load->library('ion_auth');
        $this->ion_auth->register($username_ion_auth, $password_ion_auth, $email_ion_auth, $additional_data_ion_auth, $group_ion_auth);
        $ion_ath_error = $this->ion_auth->errors();
        $user_lastid_ionauth = $this->ion_auth_used_by_club->GetUserlast_id($User['Nom'], $username_ion_auth, $email_ion_auth);
        $User["user_ionauth_id"] = $user_lastid_ionauth;
        //ion_auth register

        if (isset($ion_ath_error) && $ion_ath_error!="") {
            $data["txtContenu"] = "Une erreur s'est produite durant l'enregistrement de vos données. Veuillez recommencer s'il vous plait.".$ion_ath_error;
            $this->load->view("front/vwErreurInscriptionPro", $data);
        } else {
            $this->User->Insert($User);
            $this->retour();
        }

    }
        
    function fiche_modifier() {
        $User = $this->input->post("Particulier");
        $User["DateNaissance"] = convert_Frenchdate_to_Sqldate($User["DateNaissance"]);
        $User["Email"] = $User["Login"];
        $User['UserRole'] = '1';

        //ion_auth_update
        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_user_id($User["IdUser"]);
        $user_ion_auth = $this->ion_auth->user()->row($user_ion_auth_id);
        $data_ion_auth_update = array(
            //'username' => $User['Login'],
            //'email' => $User['Email'],
            'first_name' => $User['Nom'],
            'last_name' => $User['Prenom']
        );
        $this->ion_auth->update($user_ion_auth->id, $data_ion_auth_update);
        $ion_ath_error = $this->ion_auth->errors();
        //ion_auth_update

        if (isset($ion_ath_error) && $ion_ath_error!="") {
            $data["txtContenu"] = "Une erreur s'est produite durant l'enregistrement de vos données. Veuillez recommencer s'il vous plait.".$ion_ath_error;
            $this->load->view("front/vwErreurInscriptionPro", $data);
        } else {
            $this->User->Update($User);
            $this->retour();
        }


    }
        
    function fiche_supprimer() {
        $User = $this->input->post("User");
        $this->User->DeleteById($User["User_id"]);
        $this->retour();
    }

    function fiche_supprimer_user($User_id) {
        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_user_id($User_id);
        $this->ion_auth->delete_user($user_ion_auth_id);
        $this->User->DeleteById($User_id);
        $this->retour();
    }
        
    function fiche_annuler() {
        
        $this->retour();
    }
    
    function retour() {
        if (isset($_POST["hdnNextUrl"])) {
            redirect("admin/" . $_POST["hdnNextUrl"]);
        } else {
            $FilterCol = "-1";
            $FilterValue = "0";
            if (isset($_SESSION["User_FilterCol"])) {
                $FilterCol = $_SESSION["User_FilterCol"];
            }
            if (isset($_SESSION["User_FilterValue"])) {
                $FilterValue = $_SESSION["User_FilterValue"];
            }
            redirect("admin/usersadmin/liste/" . $FilterCol . "/" . $FilterValue . "/0/true");
        }
    }
    
    function deplacer($prmId, $prmDirection) {
        $this->User->MoveOrder($prmId, $prmDirection);
        $this->retour();
    }
}