<?php
class mail_activation_bon_plan extends CI_Controller{
    
    function __construct() {
        parent::__construct();
		
		$this->load->model("mdl_mail_activation_bon_plan");
		
		$this->load->library('session');

        $this->load->library('ion_auth');

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }
    
    function index(){
	    
        $this->edit(1);
    }
	function edit($idbon = 0)	{   
	     $this->load->helper('ckeditor');
 
        $data = array();
        //Ckeditor's configuration
        $data['ckeditor'] = array(
 
            //ID of the textarea that will be replaced
            'id'     =>     'txtContenu',
            'path'    =>    APPPATH . 'resources/ckeditor',
 
            //Optionnal values
            'config' => array(
                'width'     =>     "800px",    //Setting a custom width
                'height'     =>     '200px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ),
                    array( 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ),
                    array( 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ),
                    array( 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ),
                    array( 'Link','Unlink','Anchor' ),
                    array( 'Image','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ),
                    array( 'Styles','Format','Font','FontSize' ),
                    array( 'TextColor','BGColor' ),
                    array( 'Maximize', 'ShowBlocks','-','About' )
                )
            )
        );
 
        $data['ckeditor_2'] = array(
 
            //ID of the textarea that will be replaced
            'id'     =>     'txtContenu1',
            'path'    =>    APPPATH . 'resources/ckeditor',
 
            //Optionnal values
            'config' => array(
                'width'     =>     "800px",    //Setting a custom width
                'height'     =>     '200px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ),
                    array( 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ),
                    array( 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ),
                    array( 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ),
                    array( 'Link','Unlink','Anchor' ),
                    array( 'Image','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ),
                    array( 'Styles','Format','Font','FontSize' ),
                    array( 'TextColor','BGColor' ),
                    array( 'Maximize', 'ShowBlocks','-','About' )
                )
            )
        );
		if($idbon == 0){ 
		  $this->load->view("admin/vwMail_activation_bon_plan",$data);
		}else { 
		  
		  $mailActBonPlan = $this->mdl_mail_activation_bon_plan->getByID($idbon);		
		  $data["mailActBonPlan"] = $mailActBonPlan;
		  $this->load->view("admin/vwMail_activation_bon_plan", $data);
		}
	}
	function save() {
        
		$id = $this->input->post("Id");
		$contenu = $this->input->post("txtContenu");
		$contenu1 = $this->input->post("txtContenu1");		
		$data = array(		
		'contenu' => $contenu,
		'contenu1' => $contenu1,		
		'id' => $id,
		);
		// print_r($data);exit();
		if ($id>0 ) {
			if($contenu || $contenu1)
			{
				$this->db->where('id', $id);
				$this->db->update('mail_activation_bon_plan', $data);
				
			}
			else {
				$this->load->view("admin/vwMail_activation_bon_plan", $data);
			}
		} else {
			if($contenu && $contenu1)
			{
				$this->db->insert('mail_activation_bon_plan', $data);
			
				$id= $this->db->insert_id();
			}
			else {
				$this->load->view("admin/vwMail_activation_bon_plan", $data);
			}
		}
	
		 redirect("admin/mail_activation_bon_plan");

	}
	
}


