<?php
class Cadeau extends CI_Controller{
    
    function __construct() {
        parent::__construct();
		
		$this->load->model("mdlcadeau");
		$this->load->library('session');

        $this->load->library('ion_auth');

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }
    
    function index(){
        $this->liste();
    }
	function edit($id = 0)	{   
	    
		if($id == 0){ 
		  $this->load->view("admin/vwCadeau");
		}else { 
		  $data = array();
		  $cadeau = $this->mdlcadeau->getByID($id);		
		  $data["cadeau"] = $cadeau;
		  $this->load->view("admin/vwCadeau", $data);
		}
	}
	function save() {
        
		$idCadeau = $this->input->post("Idcadeau");
		$Nom = $this->input->post("txtNom");
		$txtComment = $this->input->post("txtComment");
		$txtPoint = $this->input->post("txtPoint");
		$txtDateDebut = $this->input->post("txtDateDebut");
		$txtDateFin = $this->input->post("txtDateFin");
		$txtNbDispo = $this->input->post("txtNbDispo");
		
		
		$hidImage = $this->input->post("hidImage");
		//$Photo1 = doUpload("txtImage","application/resources/front/images/catalogue_cadeaux_files/",$hidImage);
        $Photo1 = doUpload("txtImage","application/resources/front/images/cadeaux/",$hidImage);
		
		$txtDateCrea = $this->input->post("txtDateCrea");
		
		$data = array(		
		'Nomoffre' => $Nom,
		'Idcadeau' => $idCadeau,
		'Commentaire' => $txtComment,
		'valeurpoints' => $txtPoint,
		'Valabledebut' => $txtDateDebut,
		'Valablefin' => $txtDateFin,
		'Qtedisponible' => $txtNbDispo,
		'Imagecadeau  ' => $Photo1,
		'Ajoutele' => $txtDateCrea,
		);
		// print_r($data);exit();
		if ($idCadeau>0 ) {
			if($Nom)
			{
				$this->db->where('Idcadeau', $idCadeau);
				$this->db->update('cadeaux', $data);
				
			}
			else {
				$this->load->view("admin/vwCadeau", $data);
			}
		} else {
			if($Nom)
			{
				$this->db->insert('cadeaux', $data);
			
				$idCadeau = $this->db->insert_id();
			}
			else {
				$this->load->view("admin/vwCadeau", $data);
			}
		}
	
		 redirect("admin/cadeau");

	}
	function delete($id = 0) {
          $this->mdlcadeau->Delete($id);
		  redirect("admin/cadeau");
    }
	function liste($argOffset = 0) {
		$Criteria = 1;
		$TotalRows = $this->mdlcadeau->Compter($Criteria);
		$PerPage = 20;
		$data["PaginationLinks"] = GetPaginationLinks(GetAdminSiteUrl("cadeau/liste"), $TotalRows, $PerPage);
		
		$data["lstcadeau"] = $this->mdlcadeau->GetListe($PerPage, $argOffset, $Criteria);
		/* print_r($data["lstcadeau"] ); exit(); */
		$this->load->view("admin/vwCadeaux", $data);
	}
}