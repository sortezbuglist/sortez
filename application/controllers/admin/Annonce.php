<?php
class Annonce extends CI_Controller {
    
    function __construct() {
        parent::__construct();
		
		$this->load->model("mdlannonce") ;
		$this->load->model("mdlcommercant") ;
		$this->load->model("mdlbonplan") ;
		$this->load->model("SousRubrique") ;

		$this->load->library('session');

        $this->load->library('ion_auth');

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }
    
    function index(){			
        $this->liste() ;
    }
	 function liste(){

        $toListeAnnonce = $this->mdlannonce->getlisteAnnonce() ;
        $data['toListeMesAnnonce'] = $toListeAnnonce ;
        $this->load->view('admin/vwListeAnnonce', $data) ;
    }
	
	
	
	
	
	function ficheAnnonce($_iDCommercant = 0, $_iDAnnonce = 0){
            
            if(!$this->ion_auth->logged_in()) {
                $this->session->set_flashdata('domain_from', '1');
                redirect("connexion");
            } else if (!$this->ion_auth->is_admin()) {
                redirect();
            }
            
            
		$data["idCommercant"] = $_iDCommercant ;
		$data["colSousRubriques"] = $this->SousRubrique->GetAll();
		
		if ($_iDAnnonce != 0){
			$data["title"] = "Modification annonce" ;
			$data["oAnnonce"] = $this->mdlannonce->GetAnnonce($_iDAnnonce);
            $this->load->view('admin/vwFicheAnnonce', $data) ;
		
		}else{
			$data["title"] = "Creer une annonce" ;
			$this->load->view('admin/vwFicheAnnonce', $data) ;
		}
	}
	
	function creerAnnonce($_iDCommercant = 0){
		$oAnnonce = $this->input->post("annonce") ;
		$IdInsertedAnnonce = $this->mdlannonce->insertAnnonce($oAnnonce);
		$data["title"] = "Creer une annonce" ;
		$data["idCommercant"] = $_iDCommercant ;
		$data["msg"] = "Annonce bien cree" ;
	    $toListeAnnonce = $this->mdlannonce->getlisteAnnonce() ;		
		$data['toListeMesAnnonce'] = $toListeAnnonce ;	
		$this->load->view('admin/vwListeAnnonce', $data) ;
	}
	
	function modifAnnonce($_iDCommercant = 0){
		$oAnnonce = $this->input->post("annonce") ;
		$IdUpdatedAnnonce = $this->mdlannonce->updateAnnonce($oAnnonce);
		$data["title"] = "Modification annonce" ;
		$data["idCommercant"] = $_iDCommercant ;
		$data["msg"] = "Annon bien ete modifier" ;
		$toListeAnnonce = $this->mdlannonce->getlisteAnnonce() ;		
		$data['toListeMesAnnonce'] = $toListeAnnonce ;	
		redirect ("admin/annonce/liste/$_iDCommercant") ;
		//$this->load->view('front/vwFicheAnnonce', $data) ;
	}
	
	// function listeMesAnnonces($_iDCommercant = 0){
		// if ($_iDCommercant != 0){
			// $toListeMesAnnonce = $this->mdlannonce->listeMesAnnonces($_iDCommercant) ;
			// $data['toListeMesAnnonce'] = $toListeMesAnnonce ;
			// $data["idCommercant"] = $_iDCommercant ;
			// $this->load->view('admin/vwDesAnnonces', $data) ;
		// }else{
			// $this->load->view('front/vwMesAnnonces', $data) ;
		// }
	// }
	
	function supprimAnnonce($_iDAnnonce=0, $_iDCommercant=0){
		
		$zReponse = $this->mdlannonce->supprimeAnnonces($_iDAnnonce) ;
		redirect ("admin/annonce/liste/$_iDCommercant") ;
	}
	
	function annulationAjout(){
		//Supprimme tous les images deja uploader.
		$tNameFile = explode("-", $this->input->post("zConcateNameFile")) ;
		//print_r ($tNameFile); exit ();
		$path =  "application/resources/front/images/" ;
		//echo $path . $tNameFile[0] ; exit ();
		if (count ($tNameFile) > 0){
			for ($i = 0; $i > count($tNameFile); $i ++){
				// echo "dfsdfs" ; exit ();
				if (file_exists($path.$tNameFile[$i])){
					unlink($path.$tNameFile[$i]) ;
					// echo $path . $tNameFile[0] ; exit ();
				}
			}
		}
		
		//return false;
	}
}