<?php
class quota extends CI_Controller{

    function __construct() {
        parent::__construct();

        $this->load->library('session');
        $this->load->library('ion_auth');
        $this->load->model("packarticle_quota");
        $this->load->model("ion_auth_used_by_club");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }

    function index(){
        $this->liste();
    }

     function liste() {
        $objQuota = $this->packarticle_quota->GetAll();
        $data["colQuota"] = $objQuota;

        if($this->session->flashdata('mess_editQuota')=='1') $data['msg'] = '<strong style="color:#060">Quota enregistré !</strong>';
        if($this->session->flashdata(' mess_editQuota')=='2') $data['msg'] = '<strong style="color:#F00">Cette quota ne peut être supprimé ! quota lié à une catégorie !</strong>';
        if($this->session->flashdata(' mess_editQuota')=='3') $data['msg'] = '<strong style="color:#060">Quota supprimé !</strong>';

        $this->load->view("packarticle/admin/vwListequota",$data);
    }

    function fiche_quota($IdQuota) {

        if ($IdQuota != 0){
            $data["quota"] = "Modification  quota" ;
            $data["oQuota"] = $this->packarticle_quota->getById($IdQuota) ;
            $data["id"] = $IdQuota;
            $this->load->view('packarticle/admin/vwFichequota', $data) ;

        }else{
            $data["quota"] = "Creer une quota" ;
            $this->load->view('packarticle/admin/vwFichequota', $data) ;
        }
    }

    function creer_quota(){
        $objQuota = $this->input->post("quota") ;
        $this->packarticle_quota->insertpackarticle_quota($objQuota);
        $data["msg"] = "quota ajouté" ;

        $objQuota = $this->packarticle_quota->GetAll();
        $data["colQuota"] = $objQuota;
        $this->load->view("packarticle/admin/vwListequota",$data);
    }

    function modif_quota($IdQuota){
        $objQuota = $this->input->post("quota") ;
        $this->packarticle_quota->updatepackarticle_quota($objQuota);
        $data["msg"] = "Quota enregistré" ;

        $objQuota = $this->packarticle_quota->GetAll();
        $data["colQuota"] = $objQuota;
        $this->load->view("packarticle/admin/vwListequota",$data);
    }

    
    function delete($prmId) {

        if(is_numeric(trim($prmId))) {
            //verify if category contain active article********
            $oCategorie = $this->packarticle_quota->verifier_quota($prmId) ;
            //$oCategorie = array();
            if (count($oCategorie)==0){
                $this->packarticle_quota->supprimepackarticle_quota($prmId);
                $this->session->set_flashdata('mess_editQuota', '3');
            } else {
                $this->session->set_flashdata('mess_editQuota', '2');
            }

            //$this->firephp->log($oCategorie, 'oCategorie');

            redirect("admin/quota");
        }
        else {
            redirect("admin/quota");
        }

    }


}
    

