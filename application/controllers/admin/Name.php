<?php
class name extends CI_Controller{

    function __construct() {
        parent::__construct();

        $this->load->library('session');
        $this->load->library('ion_auth');
        $this->load->model("mdlname");
        $this->load->model("ion_auth_used_by_club");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }

    function index(){
        $this->liste();
    }

    function liste() {
        $objArticles = $this->mdlname->GetAll();
        $data["colArticles"] = $objArticles;

        if($this->session->flashdata('mess_editTypeArticle')=='1') $data['msg'] = '<strong style="color:#060">Type enregistré !</strong>';
        if($this->session->flashdata('mess_editTypeArticle')=='2') $data['msg'] = '<strong style="color:#F00">Ce type d\'article ne peut être supprimé ! Type lié à une catégorie !</strong>';
        if($this->session->flashdata('mess_editTypeArticle')=='3') $data['msg'] = '<strong style="color:#060">Type supprimé !</strong>';

        $this->load->view("admin/vwListename",$data);
    }

    function fiche_types_article($IdTypeArticle) {

        if ($IdTypeArticle != 0){
            $data["title"] = "Modification codename" ;
            $data["oArticle"] = $this->mdlname->getById($IdTypeArticle) ;
            $data["id"] = $IdTypeArticle;
            $this->load->view('admin/vwFichename', $data) ;

        }else{
            $data["title"] = "codename " ;
            $this->load->view('admin/vwFichename', $data) ;
        }
    }

    function creer_name(){
        $oArticle = $this->input->post("codename") ;
        $this->mdlname->insertarticle_type($oArticle);
        $data["msg"] = "codename ajouté" ;

        $objArticles = $this->mdlname->GetAll();
        $data["colArticles"] = $objArticles;
        $this->load->view("admin/vwListename",$data);
    }

    function modif_types_article($IdTypeArticle){
        $oArticle = $this->input->post("codename") ;
        $this->mdlname->updatearticle_type($oArticle);
        $data["msg"] = "Type Article enregistré" ;

        $objArticles = $this->mdlname->GetAll();
        $data["colArticles"] = $objArticles;
        $this->load->view("admin/vwListename",$data);
    }

    function delete($prmId) {

        if(is_numeric(trim($prmId))) {
            //verify if category contain active article********
            $oCategorie = $this->mdlname->verifier_type_article($prmId) ;
            //$oCategorie = array();
            if (count($oCategorie)==0){
                $this->mdlname->supprimearticle_type($prmId);
                $this->session->set_flashdata('mess_editTypeArticle', '3');
            } else {
                $this->session->set_flashdata('mess_editTypeArticle', '2');
            }

            //$this->firephp->log($oCategorie, 'oCategorie');

            redirect("admin/name");
        }
        else {
            redirect("admin/name");
        }

    }



}