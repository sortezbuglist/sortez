<?php
class Webscrapping extends CI_Controller {
    function __construct() {
        parent::__construct();

        $this->load->model("user");
        $this->load->model("mdlville");
        $this->load->Model("mdldepartement");
        
        $this->load->library('session');

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_model");
        $this->load->model("ion_auth_used_by_club");
        $this->load->model("Mdlscrapping");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }
    public function index(){
        $data['allapi'] = $this->Mdlscrapping->get_all();
        $this->load->view('admin/IndexScrape',$data);
    }
    public function save_api_data(){
        $api = $this->input->post('api');
        $token = $this->input->post('token');
        $idcom = $this->input->post('idcom');
        $data = array(
            'api' => $api,
            'token' => $token,
            'idcom' => $idcom
        );
        $this->Mdlscrapping->save_api_data($data);
        redirect('front/Webscrapping');
    }
    public function delete_api($id){
        $this->Mdlscrapping->delete_api($id);
        redirect('front/Webscrapping');
    }
    public function save_all_data($id){
        $data_to_parse = $this->Mdlscrapping->get_by_id($id);
        if(!empty($data_to_parse)){
           $test = $this->get_api($data_to_parse->api,$data_to_parse->token);
           if (isset($test) AND $test != null) {
            echo 'ok';
        } else {
            echo 'ko';
        }
        }else{
            echo 'ko';
        }
    }
    public function get_api($api_key, $token)
    {
        $params = array(
            "api_key" => $api_key,
            "start_template" => "main_template",
            "send_email" => "1"
        );

        $options = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-Type: application/x-www-form-urlencoded; charset=utf-8',
                'content' => http_build_query($params)
            )
        );
        $context = stream_context_create($options);
        $resultat = file_get_contents('https://www.parsehub.com/api/v2/projects/' . $token . '/run', false, $context);
        return $result;
    }
    public function get_run(){
            $seconds = 10;
            sleep($seconds);
            $params = http_build_query(array(
                "api_key" => $api_key,
                "format" => "json"
            ));
            $result = file_get_contents(
                'https://www.parsehub.com/api/v2/projects/' . $token . '/last_ready_run/data?' . $params,
                false,
                stream_context_create(array(
                    'http' => array(
                        'method' => 'GET'
                    )
                ))
            );
            $is = gzdecode($result);
            return ($is);
        
    }
    public function evented(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $data = null;
        $data = json_encode($_POST);
        if(isset($data) AND $data != null){
            $array_to = array(
                'status' =>'ok',
                'date_update' =>date("Y-m-d H:i:s")
            );
        $this->Mdlscrapping->update_status($array_to);
        }
    }
    public function check_run(){
       $status =  $this->Mdlscrapping->update_status();
    }
}