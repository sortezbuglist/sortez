

<?php
class Newsletter_commercant_backoffice extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("mdlcommercant");
        $this->load->model("mdlglissiere");
        $this->load->model("Mdl_news_letter_commercant");
        $this->load->model("mdlville") ;
        $this->load->model("mdlbonplan") ;
        $this->load->model("newsletter") ;

        $this->load->model("mdl_card_tampon") ;
        $this->load->model("mdl_card_capital") ;
        $this->load->model("mdl_card_remise") ;

        $this->load->model("mdlfidelity") ;
        $this->load->model("Mdl_plat_du_jour") ;
        $this->load->model("mdlbonplans") ;
        $this->load->model("mdlarticle") ;
        $this->load->model("mdl_agenda") ;
        $this->load->model("mdlcadeau") ;
        $this->load->model('Assoc_client_commercant_model');
        $this->load->model("mdlannonce") ;
        $this->load->model("rubrique") ;
        $this->load->model("mdlimagespub") ;
        $this->load->model('mdlannonce');
        $this->load->model("Abonnement");
        $this->load->library('user_agent');
        $this->load->model("user") ;
        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");

        $this->load->model("Mdl_card");

        $this->load->library('session');
        $this->load->helper('clubproximite');
        $this->load->model('Mdl_reservation');

        check_vivresaville_id_ville();


    }
    function index($error='ok',$success='ok'){
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $infocom = $this->mdlcommercant->gest_by_id_user($user_ion_auth->id);
            $data["IdCommercant"] = $infocom->IdCommercant;
            $iduser=$infocom->IdCommercant;
            //var_dump($iduser);die();

            if ($error !='ok' AND $success!='ok'){
                $data['status']='<sapn style="color: green">Succès:'.$success.'</sapn><br><sapn style="color: red">Echecs:'.$error.'</sapn><br>';
            }
//            $infocom=$this->mdlcommercant->infoCommercant($iduser);
            $data['saved_stat']=$this->Mdl_news_letter_commercant->get_stat_by_idcom($iduser);
//            $data['infocom']=$infocom;

            $get_all_true_custommer=$this->Assoc_client_commercant_model->get_by_commercant_id($iduser,$criteres=array());
            $get_all_false_customer=$this->Assoc_client_commercant_model->get_newsletter_customer_only($iduser);
            $get_all_comment_customer=$this->Assoc_client_commercant_model->get_comment_client_by_commercant_id($iduser);
            $get_all_csv_customer=$this->Mdl_news_letter_commercant->get_csv_client_by_commercant_id($iduser);
            //var_dump($get_all_true_custommer[0]);die();
            $data['nbtrueclient']=count($get_all_true_custommer);
            $data['nbfalseclient']=count($get_all_false_customer);
            $data['nbcommentclient']=count($get_all_comment_customer);
            $data['nbcsvclient']=count($get_all_csv_customer);
            $data['pagecategory']= 'admin_commercant';
            $objbloc_newsletter= $this->mdlglissiere->getById_bloc_newsletter($iduser);
            $data['objbloc_newsletter']=$objbloc_newsletter;
            $this->load->view('admin/newsletter_commercant/vwIndex',$data);
        }else{
            redirect('auth/login');
        }

    }
    public function envoyer_newletter_true($nb){

            $sendallnews=$this->input->post('sendallnews');
            $all_client_only_value=$this->input->post('all_client_only_value');
            //echo $all_client_only_value;die();
            $sendonemail=$this->input->post('sendonemail');
            $txtSujet=$this->input->post('txtSujet');
            $txtContenu=$this->input->post('txtContenu');
            $idcom=$this->input->post('IdCommercant');
            $this->error=0;
            $this->success=0;
            if ($sendallnews == '1'){
               // echo 'oked';die();
                if ($all_client_only_value=='1'){
                    $get_all_true_custommer=$this->Assoc_client_commercant_model->get_by_commercant_id($idcom,$criteres=array());
                    //var_dump($get_all_true_custommer);die();
                    if (count($get_all_true_custommer)!=0){
                            //var_dump($get_all_true_custommer[$nb]->Email);die();
                            $contact_to = $get_all_true_custommer[$nb]->Email;
                            $contact_object = utf8_decode($txtSujet);
                            $contact_message_content =utf8_decode($txtContenu);
                            //var_dump($contact_message_content);die();
                            $headers  = "MIME-Version: 1.0\r\n";
                            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                           if (mail($contact_to,$contact_object,$contact_message_content,$headers)){$this->success++;}else{
                               $this->error++;
                           }
                            $zContactEmailCom[] = array("Email" => $contact_to, "Name" => "");
                            $contact_partner_mailSubject=$contact_object;
                            $message_html=$contact_message_content;
                            $contact_partner_mail=$get_all_true_custommer[$nb]->Nom;
                            $sending_mail_privicarte= envoi_notification_standard($zContactEmailCom,$contact_partner_mailSubject,$message_html,$contact_partner_mail);
                            if ($sending_mail_privicarte) echo 'ok';
                            else echo 'error';
                    }
                }

            }elseif ($sendallnews =='2'){

                $contact_to_one = $sendonemail;
                $contact_object_one = utf8_decode($txtSujet);
                $contact_message_content_one =utf8_decode($txtContenu);
                $zContactEmailCom[] = array("Email" => $contact_to_one, "Name" => "");
                $contact_partner_mailSubject=$contact_object_one;
                $message_html=$contact_message_content_one;
                $contact_partner_mail="";
                $sending_mail_privicarte= envoi_notification_standard($zContactEmailCom,$contact_partner_mailSubject,$message_html,$contact_partner_mail);
                if ($sending_mail_privicarte) echo 'ok';
                else echo 'error';

            }

    }


    public function envoyer_newletter_false($nb){

        $sendallnews=$this->input->post('sendallnews');
        $all_false_client_only_value=$this->input->post('all_false_client_only_value');
        $sendonemail=$this->input->post('sendonemail');
        $txtSujet=$this->input->post('txtSujet');
        $txtContenu=$this->input->post('txtContenu');
        $idcom=$this->input->post('IdCommercant');
        $this->error=0;
        $this->success=0;
        // var_dump($sendallnews);die('K O');

        if ($sendallnews == '1'){

            if ($all_false_client_only_value =='1'){
                //send mail for true customer only
                $get_all_false_customer=$this->Assoc_client_commercant_model->get_newsletter_customer_only($idcom);
                if (count($get_all_false_customer)!=0){

                        $contact_to_fc = $get_all_false_customer[$nb]->Email;
                        $contact_object_fc = $txtSujet;
                        $contact_message_content_fc =$txtContenu;
                        $zContactEmailCom[] = array("Email" => $contact_to_fc, "Name" => "");
                        $contact_partner_mailSubject=$contact_object_fc;
                        $message_html=$contact_message_content_fc;
                        $contact_partner_mail=$get_all_false_customer[$nb]->nom ?? '';
                        $sending_mail_privicarte= envoi_notification_nwsltr($zContactEmailCom,$contact_partner_mailSubject,$message_html,$contact_partner_mail);
                        if ($sending_mail_privicarte){
                            echo '1';
                        }else{
                            echo 'error 3';
                        }

                }
            }else{echo "error 2";}
        }else{echo "error 1";}
    }

    public function envoyer_newletter_comment($nb){

        $sendallnews=$this->input->post('sendallnews');
        $sendonemail=$this->input->post('sendonemail');
        $all_comment_client_only_value=$this->input->post('all_comment_client_only_value');
        $txtSujet=$this->input->post('txtSujet');
        $txtContenu=$this->input->post('txtContenu');
        $idcom=$this->input->post('IdCommercant');
        $this->error=0;
        $this->success=0;
        if ($sendallnews == '1'){

         if (($all_comment_client_only_value=='1')){

                $get_all_comment_customer=$this->Assoc_client_commercant_model->get_comment_client_by_commercant_id($idcom);
                if (count($get_all_comment_customer)!=0){

                        $contact_to_fc = $get_all_comment_customer[$nb]->mail;
                        $contact_object_fc = $txtSujet;
                        $contact_message_content_fc =$txtContenu;
                        $zContactEmailCom[] = array("Email" => $contact_to_fc, "Name" => "");
                        $contact_partner_mailSubject=$contact_object_fc;
                        $message_html=$contact_message_content_fc;
                        $contact_partner_mail=$get_all_comment_customer[$nb]->nom ?? '';
                        $sending_mail_privicarte= envoi_notification_standard($zContactEmailCom,$contact_partner_mailSubject,$message_html,$contact_partner_mail);
                        if ($sending_mail_privicarte) echo 'ok';
                        else echo 'error';

                }

            }

        }elseif ($sendallnews =='2'){

            $contact_to_one = $sendonemail;
            $contact_object_one = utf8_decode($txtSujet);
            $contact_message_content_one =utf8_decode($txtContenu);
            $zContactEmailCom[] = array("Email" => $contact_to_one, "Name" => "");
            $contact_partner_mailSubject=$contact_object_one;
            $message_html=$contact_message_content_one;
            $contact_partner_mail="";
            $sending_mail_privicarte= envoi_notification_standard($zContactEmailCom,$contact_partner_mailSubject,$message_html,$contact_partner_mail);
            if ($sending_mail_privicarte) echo 'ok';
            else echo 'error';        }

    }

    public function envoyer_newletter_csv($nb){

        $sendallnews=$this->input->post('sendallnews');
        $sendonemail=$this->input->post('sendonemail');
        $all_csv_client_only_value=$this->input->post('all_csv_client_only_value');
        $txtSujet=$this->input->post('txtSujet');
        $txtContenu=$this->input->post('txtContenu');
        $idcom=$this->input->post('IdCommercant');
        $this->error=0;
        $this->success=0;
        if ($sendallnews == '1'){

            if ($all_csv_client_only_value=='1'){
                $get_all_csv_customer=$this->Mdl_news_letter_commercant->get_csv_client_by_commercant_id($idcom);
                if (count($get_all_csv_customer)!=0){
                    foreach ($get_all_csv_customer as $csv_client){
                        $contact_to_fc = $csv_client[$nb]->mail;
                        $contact_object_fc = $txtSujet;
                        $contact_message_content_fc =$txtContenu;
                        $zContactEmailCom[] = array("Email" => $contact_to_fc, "Name" => "");
                        $contact_partner_mailSubject=$contact_object_fc;
                        $message_html=$contact_message_content_fc;
                        $contact_partner_mail=$get_all_csv_customer[$nb]->nom ?? '';
                        $sending_mail_privicarte= envoi_notification_standard($zContactEmailCom,$contact_partner_mailSubject,$message_html,$contact_partner_mail);
                        if ($sending_mail_privicarte) echo 'ok';
                        else echo 'error';
                    }
                }
            }
        }elseif ($sendallnews =='2'){

            $contact_to_one = $sendonemail;
            $contact_object_one = utf8_decode($txtSujet);
            $contact_message_content_one =utf8_decode($txtContenu);
            $zContactEmailCom[] = array("Email" => $contact_to_one, "Name" => "");
            $contact_partner_mailSubject=$contact_object_one;
            $message_html=$contact_message_content_one;
            $contact_partner_mail="";
            $sending_mail_privicarte= envoi_notification_standard($zContactEmailCom,$contact_partner_mailSubject,$message_html,$contact_partner_mail);
            if ($sending_mail_privicarte) echo 'ok';
            else echo 'error';        }
    }

    public function get_commercant_data(){
            $idcom=$this->input->post('idcommercant');
            $categ=$this->input->post('categ');

            if ($categ =='article') {
                $toListeBonPlan = $this->mdlarticle->listeArticleRecherche(0,0,0,"",0,0,10,"","0","0000-00-00","0000-00-00",$idcom,"0");
                $data['toListeBonPlan'] = $toListeBonPlan;
            } else if ($categ=='agenda') {
                $toListeBonPlan = $this->mdl_agenda->listeAgendaRecherche(0,0,0,"",0,0,10,"","0","0000-00-00","0000-00-00",$idcom,"0");
                $data['toListeBonPlan'] = $toListeBonPlan;
            } else if ($categ=='boutique') {
                // var_dump($idcom);die("idcom");
                $toListeBonPlan = $this->mdlannonce->listeAnnonceRecherche(0, $idcom, "", "", 0, 10, 0, 0, '', "0", "10", "0", "0");
                // var_dump($toListeBonPlan);die();
                $data['toListeBonPlan'] = $toListeBonPlan;
            }else if ($categ=='bonplan'){
                $toListeBonPlan = $this->mdlbonplan->bonPlanParCommercant($idcom);
                $data['toListeBonPlan'] = $toListeBonPlan;
            }else if ($categ=='fidelite'){

                $objTampon = $this->mdl_card_tampon->getByIdCommercant($idcom);
                $objCapital = $this->mdl_card_capital->getByIdCommercant($idcom);
                $objRemise = $this->mdl_card_remise->getByIdCommercant($idcom);
                if (!empty($objTampon) && $objTampon->is_activ == '1'){
                    $data['toListeBonPlan']=$objTampon;
                }elseif (!empty($objCapital) && $objCapital->is_activ == '1'){
                    $data['toListeBonPlan']=$objCapital;
                }elseif (!empty($objRemise) && $objRemise->is_activ =='1'){
                    $data['toListeBonPlan']=$objRemise;
                }
            }else if ($categ == 'plat'){
                $objPlat=$this->Mdl_plat_du_jour->getbyIdcommercant($idcom);
                $data['toListePlat']=$objPlat;
            }

            if ($categ=='article') {
                $this->load->view('admin/newsletter/template_article', $data);
            } else if ($categ=='agenda') {
//                var_dump($data);die('kokokokoko');
                $this->load->view('admin/newsletter/template_agenda', $data);
            } else if ($categ=='boutique') {
                $this->load->view('admin/newsletter/template_annonce', $data);
            }else if ($categ=='bonplan'){
                $data['mail_content_newsletter'] = $this->load->view('admin/newsletter/template_bonplan', $data);
            }else if ($categ == 'fidelite'){
                $this->load->view('admin/newsletter/template_fidelity', $data);
            }else if ($categ =='plat'){
                //var_dump($data);die();
                $this->load->view('admin/newsletter/template_platJour', $data);
            }
    }

    public function liste_true_client($idcom){

        $liste_all=$this->Assoc_client_commercant_model->get_by_commercant_id($idcom,$criteres=array());
        $data['client']=$liste_all;
        $this->load->view('admin/newsletter_commercant/vwliste_true_client',$data);

    }

    public function liste_true_client_part(){
        $idcom=$this->input->post('idcom');
        $liste_all=$this->Assoc_client_commercant_model->get_by_commercant_id($idcom,$criteres=array());
        $data['client']=$liste_all;
        $this->load->view('admin/newsletter_commercant/vwliste_true_client_part',$data);

    }

    public function liste_false_client($idcom){

        $liste_all=$this->Assoc_client_commercant_model->get_false_client_by_commercant_id($idcom);
        $data['client']=$liste_all;
        $this->load->view('admin/newsletter_commercant/vwliste_false_client',$data);
    }
    public function liste_false_client_part(){
        $idcom=$this->input->post('idcom');
        $liste_all=$this->Assoc_client_commercant_model->get_false_client_by_commercant_id($idcom);
        $data['client']=$liste_all;
        $this->load->view('admin/newsletter_commercant/vwliste_false_client_part',$data);
    }
    public function liste_all_client($idcom){

        $data['idcom']=$idcom;
        $this->load->view('admin/newsletter_commercant/vwliste_all_client',$data);

    }
public function liste_comment_client($idcom){
    $data['idcom']=$idcom;
    $liste_all=$this->Assoc_client_commercant_model->get_comment_client_by_commercant_id($idcom);
    $data['client']=$liste_all;
    $this->load->view('admin/newsletter_commercant/vwliste_comment_client',$data);
}

public function save_stat_news(){
        $to_save=$this->input->post('tosave');
        $idcom=$this->input->post('idcom');
        $nom=$this->input->post('nom');
        $sendonemail=$this->input->post('sendonemail');
        $txtSujet=$this->input->post('txtSujet');
        $field=array('IdCommercant'=>$idcom,
                     "content"=>$to_save,
                        "date_ajout"=>date('Y-m-d'),
                    "send_mail_adress"=>$sendonemail,
                    "object"=>$txtSujet,
                    'nom'=>$nom);
        $save=$this->Mdl_news_letter_commercant->save_stat_newsletter($field);
        if ($save=='1'){
           redirect('admin/Newsletter_commercant_backoffice/');
        }
}
public function load_stat_news(){
        $id=$this->input->post('id');
        $loaded=$this->Mdl_news_letter_commercant->get_data_by_id($id);
         echo json_encode($loaded);
}

public function load_maquette_newsletter(){

        $user_ion_auth = $this->ion_auth->user()->row();
        $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
        if ($iduser == null || $iduser == 0 || $iduser == "") {
            $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        }

        $nbre=$this->input->post('nbr');
        $data['infocom']=$this->mdlcommercant->infoCommercant($iduser);
        if ($nbre == '1'){
            $this->load->view('admin/newsletter/maquette_1_colonne', $data);
        }elseif ($nbre == '2'){
            $this->load->view('admin/newsletter/maquette_2_colonne', $data);
        }elseif ($nbre== '3'){
            $this->load->view('admin/newsletter/maquette_3_colonne', $data);
        }
}

public function import_csv_client($suucees=0,$echec=0,$idcom){
    $infocom=$this->mdlcommercant->infoCommercant($idcom);
    $data['infocom']=$infocom;
    $data['suucees']=$suucees;
    $data['echec']=$echec;
    $this->load->view('admin/newsletter/vwIndexImportCsvClient',$data);
}
public function verify_csv_file($idcom){
    $suucees= 0;
    $echec= 0;
    if (isset($_FILES["file"])){
        //echo "Upload: " . $_FILES["file"]["name"] . "<br />";
        //echo "Type: " . $_FILES["file"]["type"] . "<br />";
        //echo "Size: " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
        //echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br />";
        $file=$_FILES["file"]["tmp_name"];
        // The nested array to hold all the arrays
        $the_big_array = [];

// Open the file for reading
        //var_dump($_FILES["file"]);
        if (($h = fopen("{$file}", "r")) !== FALSE)
        {
            // Each line in the file is converted into an individual array that we call $data
            // The items of the array are comma separated
            while (($data = fgetcsv($h, 1000, ",")) !== FALSE)
            {
                // Each individual array is being pushed into the nested array
                $the_big_array[] = $data;
            }

            // Close the file
            fclose($h);
            //var_dump($the_big_array[1][0]);die();
            $i=0;
            while ($i < count($the_big_array)) {
                if ($i !=0){

                 if (isset($the_big_array[$i][0]) ){
                     if (preg_match('/@/',$the_big_array[$i][0])){

                         $array=array("nom"=>$the_big_array[$i][7] ?? '',
                             "prenom"=>$the_big_array[$i][1]?? '',
                             "date_naissance "=>$the_big_array[$i][2] ?? null,
                             "adresse"=>$the_big_array[$i][3]?? '',
                             "code_postal "=>$the_big_array[$i][4]?? '',
                             "ville"=>$the_big_array[$i][5]?? '',
                             "mobile"=>$the_big_array[$i][6]?? '',
                             "mail"=>$the_big_array[$i][0],
                             "IdCommercant"=>$idcom);
                         $save= @$this->Mdl_news_letter_commercant->save_csv_file_client($array);
                         if ($save){
                             $suucees ++;
                             $i++;
                         }else{
                             $echec ++;
                             $i++;
                         }
                     }else{
                         $echec ++;
                         $i++;
                     }
                 }else{
                     $echec ++;
                     $i++;
                 }

                }else{
                    $i++;
                }

            }
            redirect('admin/Newsletter_commercant_backoffice/import_csv_client/'.$suucees.'/'.$echec.'/'.$idcom);
        }
    }
}
public function liste_csv_client($idcom){
    $data['idcom']=$idcom;
    $liste_all=$this->Mdl_news_letter_commercant->get_csv_client_by_commercant_id($idcom);
    $data['client']=$liste_all;
    $this->load->view('admin/newsletter_commercant/vwliste_csv_client',$data);
}
public function supprimer_csv($idcom){
    $this->Mdl_news_letter_commercant->delete_csv($idcom);
    redirect('admin/Newsletter_commercant_backoffice/index');
}
// public function envoyer_newletter_one(){
//     $sendonemail=$this->input->post('sendonemail');    
//     $txtSujet=$this->input->post('txtSujet');
//     $txtContenu=$this->input->post('txtContenu');
//     $contact_to_one = $sendonemail;
//     $contact_object_one = utf8_decode($txtSujet);
//     $contact_message_content_one =utf8_decode($txtContenu);
//     $zContactEmailCom[] = array("Email" => $contact_to_one, "Name" => "");    
//     $contact_partner_mailSubject=$contact_object_one;
//     $message_html=$contact_message_content_one;
//     $contact_partner_mail="";
//     // var_dump($zContactEmailCom);
    
//     $sending_mail_privicarte= envoi_notification_standard($zContactEmailCom,$contact_partner_mailSubject,$message_html,$contact_partner_mail);
    
//     if ($sending_mail_privicarte){
//         echo 'ok';
//     }else{
//         echo 'error';
//     } 
//     }

// script mamitiana 01
// public function envoyer_newletter_one(){
//     $contact_to = $this->input->post("contact_to");
//     $sendonemail=$this->input->post('sendonemail');    
//     $txtSujet=$this->input->post('txtSujet');
//     $txtContenu=$this->input->post('txtContenu');
//     $contact_to_one = $sendonemail;
//     $contact_object_one = utf8_decode($txtSujet);
//     $contact_message_content_one =utf8_decode($txtContenu);
//     $zContactEmailCom = array();
//     $zContactEmailCom[] = array("Email" => $contact_to_one, "Name" => "contact");    
//     $contact_partner_mailSubject=$contact_object_one;
//     $message_html=$contact_message_content_one;
//     $contact_partner_mail="";

//     $sending_mail_privicarte=envoi_notification($zContactEmailCom,$contact_partner_mailSubject,$message_html,$contact_partner_mail);
//     if ($sending_mail_privicarte) echo '1';
//     else echo '0';
// }

public function envoyer_newletter_one(){
    $sendonemail=$this->input->post('sendonemail');
    $txtSujet=$this->input->post('txtSujet');
    $txtContenu=$this->input->post('txtContenu');
    $idcom=$this->input->post('IdCommercant');
    $infocom = $this->mdlcommercant->infoCommercant($idcom);
    $mail = $infocom->Email;
    $nom = $infocom->NomSociete;
    $contact_to_one = $sendonemail;
    $contact_object_one = utf8_decode($txtSujet);
    // var_dump($txtContenu);die("ko");  
    // $contact_message_content_one =utf8_decode($txtContenu);
    $contact_message_content_one =utf8_decode($txtContenu);
    $zContactEmailCom= array();
    $zContactEmailCom[] = array("Email" => $contact_to_one, "Name" => "");    
    $contact_partner_mailSubject=$contact_object_one;
    $message_html=$contact_message_content_one;
    $contact_partner_nom=$nom;
    $contact_partner_mail= $mail;
    // var_dump($contact_message_content_one);die( "contenu envoi mail one");
    //var_dump($message_html);die();
    // var_dump($txtContenu);die();
    $sending_mail_privicarte= envoi_notification_nwsltr($zContactEmailCom,$contact_partner_mailSubject,$message_html,$contact_partner_mail,$contact_partner_nom);
    if ($sending_mail_privicarte){
        echo '1';
    }else{
        echo 'error';
    } 
}
    public function supprimer_stat_news(){
        $id=$this->input->post('id');
        $this->Mdl_news_letter_commercant->delete_stat_news($id);
        
    }
    public function Ajouter_bloc(){
        $user_ion_auth = $this->ion_auth->user()->row();
        $infocom = $this->mdlcommercant->gest_by_id_user($user_ion_auth->id);
        $data["IdCommercant"] = $infocom->IdCommercant;
        $iduser=$infocom->IdCommercant;
        //var_dump($objCommercant["IdCommercant"]); die();

        $objbloc_newsletter_brute = $this->input->post("bloc_newsletter");
        $objbloc_newsletter=remove_2500($objbloc_newsletter_brute);
        $bloc_newsletter = $this->mdlglissiere->getById_bloc_newsletter($iduser);

        if ($iduser != '0' AND $bloc_newsletter != NULL){

            $IdUpdatedCommercantBlocnewsletter = $this->mdlglissiere->Update_bloc_newsletter($objbloc_newsletter);
            // var_dump( $IdUpdatedCommercantBlocInfo);die('1');
        }else{

            $IdUpdatedCommercantBlocnewsletter = $this->mdlglissiere->Insert_bloc_newsletter($objbloc_newsletter);
            // var_dump( $IdUpdatedCommercantBlocInfo);die('2');
        }
        $data['save']=$IdUpdatedCommercantBlocnewsletter;
        //var_dump($objbloc_newsletter);die();
        redirect('admin/Newsletter_commercant_backoffice/index');
    }



//envoyer bloc
public function envoyer_newletter(){
     $user_ion_auth = $this->ion_auth->user()->row();
    $infocom = $this->mdlcommercant->gest_by_id_user($user_ion_auth->id);
    $idcom=$infocom->IdCommercant;
//enregistrer
    $activebloc1=$this->input->post('activebloc1');
    $typebloc1=$this->input->post('typebloc1');
//    $imagebloc1=$this->input->post('imagebloc1');
    $contentbloc1=$this->input->post('contentbloc1');
    $activeboutonbloc1=$this->input->post('activeboutonbloc1');
    $contentboutonbloc1=$this->input->post('contentboutonbloc1');
    $valuelinkbloc1=$this->input->post('valuelinkbloc1');
    $customlinkbloc1=$this->input->post('customlinkbloc1');
//    $imagebloc1_2=$this->input->post('imagebloc1_2');
    $contentbloc1_2=$this->input->post('contentbloc1_2');
    $activeboutonbloc1_2=$this->input->post('activeboutonbloc1_2');
    $contentboutonbloc1_2=$this->input->post('contentboutonbloc1_2');
    $valuelinkbloc1_2=$this->input->post('valuelinkbloc1_2');
    $customlinkbloc1_2=$this->input->post('customlinkbloc1_2');
    $activebloc2=$this->input->post('activebloc2');
    $typebloc2=$this->input->post('typebloc2');
//    $imagebloc2=$this->input->post('imagebloc2');
    $contentbloc2=$this->input->post('contentbloc2');
    $activeboutonbloc2=$this->input->post('activeboutonbloc2');
    $contentboutonbloc2=$this->input->post('contentboutonbloc2');
    $valuelinkbloc2=$this->input->post('valuelinkbloc2');
    $customlinkbloc2=$this->input->post('customlinkbloc2');
//    $imagebloc2_2=$this->input->post('imagebloc2_2');
    $contentbloc2_2=$this->input->post('contentbloc2_2');
    $activeboutonbloc2_2=$this->input->post('activeboutonbloc2_2');
    $contentboutonbloc2_2=$this->input->post('contentboutonbloc2_2');
    $valuelinkbloc2_2=$this->input->post('valuelinkbloc2_2');
    $customlinkbloc2_2=$this->input->post('customlinkbloc2_2');
    $activebloc3=$this->input->post('activebloc3');
    $typebloc3=$this->input->post('typebloc3');
//    $imagebloc3=$this->input->post('imagebloc3');
    $contentbloc3=$this->input->post('contentbloc3');
    $activeboutonbloc3=$this->input->post('activeboutonbloc3');
    $valuelinkbloc3=$this->input->post('valuelinkbloc3');
    $customlinkbloc3=$this->input->post('customlinkbloc3');
//    $imagebloc3_2=$this->input->post('imagebloc3_2');
    $contentbloc3_2=$this->input->post('contentbloc3_2');
    $activeboutonbloc3_2=$this->input->post('activeboutonbloc3_2');
    $contentboutonbloc3_2=$this->input->post('contentboutonbloc3_2');
    $contentboutonbloc3=$this->input->post('contentboutonbloc3');
    $valuelinkbloc3_2=$this->input->post('valuelinkbloc3_2');
    $customlinkbloc3_2=$this->input->post('customlinkbloc3_2');
    $activebloc4=$this->input->post('activebloc4');
    $typebloc4=$this->input->post('typebloc4');
//    $imagebloc4=$this->input->post('imagebloc4');
    $contentbloc4=$this->input->post('contentbloc4');
    $activeboutonbloc4=$this->input->post('activeboutonbloc4');
    $contentboutonbloc4=$this->input->post('contentboutonbloc4');
    $valuelinkbloc4=$this->input->post('valuelinkbloc4');
    $customlinkbloc4=$this->input->post('customlinkbloc4');
//    $imagebloc4_2=$this->input->post('imagebloc4_2');
    $contentbloc4_2=$this->input->post('contentbloc4_2');
    $activeboutonbloc4_2=$this->input->post('activeboutonbloc4_2');
    $contentboutonbloc4_2=$this->input->post('contentboutonbloc4_2');
    $valuelinkbloc4_2=$this->input->post('valuelinkbloc4_2');
    $customlinkbloc4_2=$this->input->post('customlinkbloc4_2');
    $bloc_newsletter= $this->mdlglissiere->getId($idcom);
    $IdCommercantbloc=$bloc_newsletter[0]->IdCommercant ?? $idcom ?? 0;
    //$bloc_newsletter = $this->mdlglissiere->getById_bloc_newsletter($iduser);
//var_dump($bloc_newsletter[0]->IdCommercant);die();
//var_dump($contentbloc3);die("xw");



        /*$bloc_newsletter=$this->input->post('bloc_newsletter');
        $imagebloc1=$this->input->post('bloc_1_image1_content');
        $imagebloc1_2=$this->input->post('bloc_1_image2_content');
        $imagebloc2=$this->input->post('bloc_1_2_image1_content');
        $imagebloc2_2=$this->input->post('bloc_1_2_image2_content');
        $imagebloc3=$this->input->post('bloc_1_3_image1_content');
        $imagebloc3_2=$this->input->post('bloc_1_3_image2_content');
        $imagebloc4=$this->input->post('bloc_1_4_image1_content');
        $imagebloc4_2=$this->input->post('bloc_1_4_image2_content');*/



        $field=array(
            "is_activ_bloc"=>$activebloc1,
            "type_bloc"=>$typebloc1,
//             "bloc_1_image1_content"=>$imagebloc1,
            "bloc1_content"=>$contentbloc1,
            "is_activ_btn_bloc1"=>$activeboutonbloc1,
            "btn_bloc1_content1"=>$contentboutonbloc1,
            "bloc1_existed_link1"=>$valuelinkbloc1,
            "bloc1_custom_link1"=>$customlinkbloc1,
//            "bloc_1_image2_content"=>$imagebloc1_2,
            "bloc1_content2"=>$contentbloc1_2,
            "is_activ_btn_bloc2"=>$activeboutonbloc1_2,
            "btn_bloc1_content2"=>$contentboutonbloc1_2,
            "bloc1_existed_link2"=>$valuelinkbloc1_2,
            "bloc1_custom_link2"=>$customlinkbloc1_2,
            "is_activ_bloc2"=>$activebloc2,
            "type_bloc2"=>$typebloc2,
//            "bloc_1_2_image1_content"=>$imagebloc2,
            "bloc1_2_content"=>$contentbloc2,
            "is_activ_btn_bloc1_2"=>$activeboutonbloc2,
            "btn_bloc1_2_content1"=>$contentboutonbloc2,
            "bloc1_2_existed_link1"=>$valuelinkbloc2,
            "bloc1_2_custom_link1"=>$customlinkbloc2,
//            "bloc_1_2_image2_content"=>$imagebloc2_2,
            "bloc1_2_content2"=>$contentbloc2_2,
            "is_activ_btn_bloc2_2"=>$activeboutonbloc2_2,
            "btn_bloc1_2_content2"=>$contentboutonbloc2_2,
            "bloc1_2_existed_link2"=>$valuelinkbloc2_2,
            "bloc1_2_custom_link2"=>$customlinkbloc2_2,
            "is_activ_bloc3"=>$activebloc3,
            "type_bloc3"=>$typebloc3,
//            "bloc_1_3_image1_content"=>$imagebloc3,
            "is_activ_btn_bloc1_3"=>$activeboutonbloc3,
            "btn_bloc1_3_content1"=>$contentboutonbloc3,
            "bloc1_3_existed_link1"=>$valuelinkbloc3,
            "bloc1_3_custom_link1"=>$customlinkbloc3,
//            "bloc_1_3_image2_content"=>$imagebloc3_2,
            "bloc1_3_content2"=>$contentbloc3_2,
            "is_activ_btn_bloc2_3"=>$activeboutonbloc3_2,
            "btn_bloc1_3_content2"=>$contentboutonbloc3_2,
            "bloc1_3_existed_link2"=>$valuelinkbloc3_2,
            "bloc1_3_custom_link2"=>$customlinkbloc3_2,
            "type_bloc4"=>$typebloc4,
            "is_activ_bloc4"=>$activebloc4,
//            "bloc_1_4_image1_content"=>$imagebloc4,
            "bloc_1_4_content"=>$contentbloc4,
            "is_activ_btn_bloc1_4"=>$activeboutonbloc4,
            "btn_bloc1_4_content"=>$contentboutonbloc4,
            "bloc1_4_existed_link1"=>$valuelinkbloc4,
            "bloc1_4_custom_link1"=>$customlinkbloc4,
//            "bloc_1_4_image2_content"=>$imagebloc4_2,
            "bloc1_4_content2"=>$contentbloc4_2,
            "is_activ_btn_bloc2_4"=>$activeboutonbloc4_2,
            "btn_bloc1_4_content2"=>$contentboutonbloc4_2,
            "bloc1_4_existed_link2"=>$valuelinkbloc4_2,
            "bloc1_4_custom_link2"=>$customlinkbloc4_2,
            "bloc1_3_content"=>$contentbloc3,
            "IdCommercant"=>$IdCommercantbloc
        );

            if ($bloc_newsletter != NULL){
            $this->mdlglissiere->Update_bloc_newsletter($field);
        }else{
            $this->mdlglissiere->Insert_bloc_newsletter($field);
        }
    $data["IdCommercant"] = $infocom->IdCommercant;
    $information=$this->Mdl_news_letter_commercant->recuperer($idcom);
    $data['information']=$information;
    $data['infocom']=$infocom;
foreach ($information as $info) { 
    //bloc1
    if($info->is_activ_bloc=='1' AND $info->type_bloc=='1' ){
    $this->load->view('admin/newsletter_commercant/vwBloc1',$data);
    }
    else if($info->is_activ_bloc=='1' AND $info->type_bloc=='2'){
     $this->load->view('admin/newsletter_commercant/vwBloc1_2',$data);   
    }
    //bloc2
    if($info->is_activ_bloc2=='1' AND $info->type_bloc2=='1'){
    $this->load->view('admin/newsletter_commercant/vwBloc2',$data);
    }
    else if($info->is_activ_bloc2=='1' AND $info->type_bloc2=='2'){
     $this->load->view('admin/newsletter_commercant/vwBloc2_2',$data);   
    }
    //bloc3
    if($info->is_activ_bloc3=='1' AND $info->type_bloc3=='1'){
    $this->load->view('admin/newsletter_commercant/vwBloc3',$data);
    }
    else if($info->is_activ_bloc3=='1' AND $info->type_bloc3=='2'){
     $this->load->view('admin/newsletter_commercant/vwBloc3_2',$data);   
    }
    //bloc4
    if($info->is_activ_bloc4=='1' AND $info->type_bloc4=='1'){
    $this->load->view('admin/newsletter_commercant/vwBloc4',$data);
    }
    else if($info->is_activ_bloc4=='1' AND $info->type_bloc4=='2'){
     $this->load->view('admin/newsletter_commercant/vwBloc4_2',$data);   
    }
}
   

    }
    public function supprimer_img_newsletter($iduser,$prmFiles){
        $user_ion_auth = $this->ion_auth->user()->row();
        $infocom = $this->mdlcommercant->gest_by_id_user($user_ion_auth->id);
        $data["IdCommercant"] = $infocom->IdCommercant;
        $idcom=$infocom->IdCommercant;
        $this->Mdl_news_letter_commercant->getId($idcom);
        $this->Mdl_news_letter_commercant->delete_img_newsletter($iduser,$prmFiles);
        if (file_exists("application/resources/front/images/article/photoCommercant/" . $user_ion_auth->id."/".$prmFiles) == true) {
            unlink("application/resources/front/images/article/photoCommercant/" . $user_ion_auth->id."/".$prmFiles);
        }  
    }
    public function get_commercant_agenda(){
        $idcom=$this->input->post('idcommercant');
        $filtre=$this->input->post('filtre');

        if($filtre=='1'){
            $toListeBonPlan = $this->mdl_agenda->listeAgendaRecherche(0,0,0,"",0,0,10,"","0","0000-00-00","0000-00-00",$idcom,"0");
        }
        else if($filtre=='101'){
            $toListeBonPlan = $this->mdl_agenda->listeAgendaRecherche(0,0,0,"",0,0,10,"","101","0000-00-00","0000-00-00",$idcom,"0");
        }
        else if($filtre=='202'){
            $toListeBonPlan = $this->mdl_agenda->listeAgendaRecherche(0,0,0,"",0,0,10,"","202","0000-00-00","0000-00-00",$idcom,"0");
        }
        else if($filtre=='303'){
            $toListeBonPlan = $this->mdl_agenda->listeAgendaRecherche(0,0,0,"",0,0,10,"","303","0000-00-00","0000-00-00",$idcom,"0");
        }
        else if($filtre=='404'){
            $toListeBonPlan = $this->mdl_agenda->listeAgendaRecherche(0,0,0,"",0,0,10,"","404","0000-00-00","0000-00-00",$idcom,"0");
        }
        else if($filtre=='505'){
            $toListeBonPlan = $this->mdl_agenda->listeAgendaRecherche(0,0,0,"",0,0,10,"","505","0000-00-00","0000-00-00",$idcom,"0");
        }
        $data['toListeBonPlan'] = $toListeBonPlan;
//        var_dump($data);die('kokokokok');
        $this->load->view('admin/newsletter/template_agenda', $data);
    }
}
