<?php

class newsletters_mails extends CI_Controller
{

    private $mCurrentFilter = 2; // Le type de filtre en cours (0 : exact, 1 : d?but, 2 : milieu)
    private $mSearchValue = ""; // La valeur ? rechercher
    private $mLinesPerPage = 50;// Le nombre de lignes (enregistrements) ? afficher par page

    function __construct()
    {
        parent::__construct();

        $this->load->model("mdl_lightbox_mail");
        $this->load->library('session');
        $this->load->library('ion_auth');
        $this->load->model("ion_auth_model");
        $this->load->model("ion_auth_used_by_club");
        $this->load->model("mdldepartement");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }


    public function GetCurrentFilter()
    {
        if (isset($_POST["optTypeFiltre"])) {
            return $_POST["optTypeFiltre"];
        } else {
            if ($this->session->userdata("curFilter") == null) {
                return 2;
            }
            return $this->session->userdata("curFilter");
        }
    }

    public function SetCurrentFilter($argValue)
    {
        $this->mCurrentFilter = $argValue;
        $this->session->set_userdata("curFilter", $argValue);
    }

    public function GetSearchValue()
    {
        $SearchValue;
        if (isset($_POST["txtSearch"])) {
            $SearchValue = $_POST["txtSearch"];
        } else {
            if ($this->session->userdata("SearchValue") == null) {
                $SearchValue = "";
            }
            $SearchValue = $this->session->userdata("SearchValue");
        }

        return addslashes($SearchValue);
    }

    public function SetSearchValue($argValue)
    {
        $this->mSearchValue = $argValue;
        $this->session->set_userdata("SearchValue", $argValue);
    }

    public function GetLinesPerPage()
    {
        if (isset($_POST["cmbNbLignes"])) {
            return $_POST["cmbNbLignes"];
        } else {
            if ($this->session->userdata("LinesPerPage") == null) {
                return 50;
            }
            return $this->session->userdata("LinesPerPage");
        }
    }

    public function SetLinesPerPage($argValue)
    {
        $this->mLinesPerPage = $argValue;
        $this->session->set_userdata("LinesPerPage", $argValue);
    }

    public function GetOrder()
    {
        if (isset($_POST["hdnOrder"])) {
            return $_POST["hdnOrder"];
        } else {
            if ($this->session->userdata("UserOrder") == null) {
                return "";
            }
            return $this->session->userdata("UserOrder");
        }
    }

    public function SetOrder($argValue)
    {
        $this->session->set_userdata("UserOrder", $argValue);
    }

    function _clearAllSessionsData()
    {
        $this->SetCurrentFilter(2);
        $this->SetSearchValue("");
        $this->SetLinesPerPage(50);
        $this->SetOrder("");
    }


    function index()
    {
        $this->liste();
    }

    /**
     * Fontion utilis?e pour l'affichage de la liste des users (avec pagination)
     *
     * @param unknown_type $prmPagerIndex
     * @param unknown_type $prmIsCompleteList
     * @param unknown_type $prmOrder
     */
    public function liste($prmFilterCol = "-1", $prmFilterValue = "0", $prmPagerIndex = 0, $prmIsCompleteList = false, $prmOrder = "")
    {
        $_SESSION["User_FilterCol"] = $prmFilterCol;
        $_SESSION["User_FilterValue"] = $prmFilterValue;

        if ($prmIsCompleteList) {
            $this->_clearAllSessionsData();
        }

        if (isset($_POST["cmbNbLignes"])) {
            $this->SetLinesPerPage($_POST["cmbNbLignes"]);
        }

        if (isset($_POST["txtSearch"])) {
            $this->SetSearchValue($_POST["txtSearch"]);
        }

        if (isset($_POST["optTypeFiltre"])) {
            $this->SetCurrentFilter($_POST["optTypeFiltre"]);
        }

        if (isset($_POST["hdnOrder"])) {
            $this->SetOrder($_POST["hdnOrder"]);
        }

        $SearchedWordsString = str_replace("+", " ", $this->GetSearchValue());
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);
        $WhereKeyWords = " 0=0 ";


        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $WhereKeyWords .= " AND ";
                }

                $WhereKeyWords .= " ( " .
                    " UPPER(lightbox_mail.id ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(lightbox_mail.mail ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(lightbox_mail.departement_code ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(lightbox_mail.name ) LIKE '%" . strtoupper($Search) . "%'" .
                    " )
                ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $WhereKeyWords .= " OR ";
                }
            }
        }

        // Pre-filtre sur une table parent (cas d'une liste fille dans une fiche maitre-detail)
        if ($prmFilterCol != "-1") {
            $WhereKeyWords .= " AND " . $prmFilterCol . " = '" . $prmFilterValue . "'";
        }

        // echo $WhereKeyWords; exit;

        $colusers = $this->mdl_lightbox_mail->GetWhere($WhereKeyWords, $prmPagerIndex, $this->GetLinesPerPage(), $this->GetOrder());


        // Pagination (CI)
        $this->load->library("pagination");
        $this->config->load("config");
        $pagination_config['uri_segment'] = '5';
        $data["UriSegment"] = $pagination_config['uri_segment'];
        $pagination_config["base_url"] = site_url() . "admin/newsletters_mails/liste/" . $prmFilterCol . "/" . $prmFilterValue;
        $pagination_config["total_rows"] = $this->mdl_lightbox_mail->CountWhere($WhereKeyWords);
        $pagination_config["per_page"] = $this->GetLinesPerPage();
        $pagination_config["first_link"] = '&lsaquo;';
        $pagination_config["last_link"] = '&raquo';
        $pagination_config["next_link"] = '&gt;';
        $pagination_config["prev_link"] = '&lt;';
        $pagination_config["cur_tag_open"] = "<span style='font-weight: bold; padding-right: 5px; padding-left: 5px;'>";
        $pagination_config["cur_tag_close"] = "</span>";
        $pagination_config["num_tag_open"] = "<span>&nbsp;";
        $pagination_config["num_tag_close"] = "&nbsp;</span>";

        $this->pagination->initialize($pagination_config);
        // Effectuer un highlight des mots-cles recherches dans la liste des enregistrements trouv&s
        $data["highlight"] = "";
        if ($this->GetSearchValue() != "") {
            $data["highlight"] = "yes";
        } else {
            $data["highlight"] = "";
        }

        // Les numeros de page
        $NumerosPages = array();
        //$NumerosPages[] = 0;
        $n = 0;
        while ($n < $pagination_config["total_rows"] - 1) {
            $NumerosPages[] = $n;
            $n += $pagination_config["per_page"];
        }
        $data["NumerosPages"] = $NumerosPages;


        // Preparer les variables pour le view
        $data["colUsers"] = $colusers;

        $data["PaginationLinks"] = $this->pagination->create_links();
        $data["NbLignes"] = $this->GetLinesPerPage();
        $data["CurFiltre"] = $this->GetCurrentFilter();
        $data["SearchValue"] = $this->GetSearchValue();
        $data["SearchedWords"] = $SearchedWords;
        $data["Keywords"] = $this->GetSearchValue();
        $data["CountAllResults"] = $pagination_config["total_rows"];
        $data["Ordre"] = $this->GetOrder();

        $DepartCount = $prmPagerIndex;
        $data["DepartCount"] = $DepartCount;

        // Data pour le pre-filtre
        $data["FilterCol"] = $prmFilterCol;
        $data["FilterValue"] = $prmFilterValue;

        $data["no_main_menu_home"] = "1";

        //$id_email_csv_import = $this->input->post('id_email_csv_import');
        //var_dump($_FILES['id_email_csv_import']['name']);

        if (isset($_FILES['id_email_csv_import']['name'])) {

            //get the csv file
            if (isset($_FILES["id_email_csv_import"]["name"])) $pdf = $_FILES["id_email_csv_import"]["name"];
            if (isset($pdf) && $pdf != "") {
                $pdfAssocie = $pdf;
                $pdfCom = doUploadCSV("id_email_csv_import", "application/resources/csv/", $pdfAssocie);
            }

            $this->load->database();
            $sqlname = $this->db->hostname;
            $username = $this->db->username;
            $table = "lightbox_mail";
            $password = $this->db->password;
            $db = $this->db->database;
            $file = "application/resources/csv/".$pdfCom;
            $cons = mysqli_connect("$sqlname", "$username", "$password", "$db") or die("rrrrrrrrrrrr".mysql_error());

            $result1 = mysqli_query($cons, "select count(*) count from $table");
            $r1 = mysqli_fetch_array($result1);
            $count1 = (int)$r1['count'];
            //If the fields in CSV are not seperated by comma(,)  replace comma(,) in the below query with that  delimiting character
            //If each tuple in CSV are not seperated by new line.  replace \n in the below query  the delimiting character which seperates two tuples in csv
            // for more information about the query http://dev.mysql.com/doc/refman/5.1/en/load-data.html
            mysqli_query($cons, '
                LOAD DATA LOCAL INFILE "' . $file . '"
                    INTO TABLE lightbox_mail 
                    FIELDS TERMINATED by ","
                    LINES TERMINATED BY "\n"
            ') or die("eeeeeeeeeeeee".mysql_error());

            $result2 = mysqli_query($cons, "select count(*) count from $table");
            $r2 = mysqli_fetch_array($result2);
            $count2 = (int)$r2['count'];

            $count = $count2 - $count1;
            if ($count > 0)
                $data['mssg_import'] = "Success";
            $data['mssg_import'] = "<b> $count enregistrement de nouvel adresse email </b> ";

        } else {
            $csv_dir_root = "application/resources/csv/";
            if (!file_exists($csv_dir_root)) {
                mkdir($csv_dir_root, 0777);
            } else {
                //unlink($csv_dir_root);
                //rmdir($csv_dir_root);
            }
        }


        $this->load->view("admin/newsletter/newsletters_mails_list", $data);
    }

    function fiche($prmId = "0")
    {
        $data = null;
        $this->load->Model("Ville");

        $data["objUser"] = $this->mdl_lightbox_mail->getById($prmId);
        $data['prmId'] = $prmId;

        $data["colDepartement"] = $this->mdldepartement->GetAll();

        $data["no_main_menu_home"] = "1";

        //$user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_user_id("1");
        //$user_ion_auth = $this->ion_auth->user()->row($user_ion_auth_id);
        ////$this->firephp->log($user_ion_auth, 'user_ion_auth');

        $this->load->view("admin/newsletter/newsletters_mails_fiche", $data);
    }

    function check_duplicate() {
        $duplicate_list = $this->mdl_lightbox_mail->duplicateEmailList();
        $var = 0;
        if($duplicate_list && count($duplicate_list)>0) {
            foreach ($duplicate_list as $item) {
                $var += intval($item->nb);
            }
        }
        $duplicate_remove = $this->mdl_lightbox_mail->removeDuplicateEmail();
        echo $var;
    }

    function fiche_modifier()
    {
        $User = $this->input->post("Prostect");
        if ($User['id'] == "0") {
            $this->mdl_lightbox_mail->insert($User);
            $this->retour();
        } else {
            $this->mdl_lightbox_mail->update($User);
            $this->retour();
        }
    }

    function fiche_supprimer()
    {
        $User = $this->input->post("User");
        $this->user->DeleteById($User["User_id"]);
        $this->retour();
    }

    function fiche_supprimer_user($User_id)
    {
        $ss = $this->mdl_lightbox_mail->delete($User_id);
        $this->retour();
        //redirect("admin/users/liste", 'refresh');
    }

    function fiche_annuler()
    {
        $this->retour();
    }

    function retour()
    {
        if (isset($_POST["hdnNextUrl"])) {
            redirect("admin/" . $_POST["hdnNextUrl"]);
        } else {
            $FilterCol = "-1";
            $FilterValue = "0";
            if (isset($_SESSION["User_FilterCol"])) {
                $FilterCol = $_SESSION["User_FilterCol"];
            }
            if (isset($_SESSION["User_FilterValue"])) {
                $FilterValue = $_SESSION["User_FilterValue"];
            }
            redirect("admin/newsletters_mails/liste/" . $FilterCol . "/" . $FilterValue . "/0/true");
        }
    }

    function deplacer($prmId, $prmDirection)
    {
        $this->user->MoveOrder($prmId, $prmDirection);
        $this->retour();
    }
}