<?php

class manage_abonnement extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model("Abonnement");
        $this->load->model('pack_test_validation');
        $this->load->library('image_lib');
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->library('ion_auth');

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }

    function index()
    {
        $data["no_main_menu_home"] = "1";
        $data["no_left_menu_home"] = "1";

        $toAbonnement = $this->Abonnement->GetAll();
        $data['toAbonnement'] = $toAbonnement;

        if ($this->session->flashdata('mess_editcategorie') == '1') $data['mess_editcategorie'] = '<strong style="color:#060">Abonnement enregistré !</strong>';
        if ($this->session->flashdata('mess_editcategorie') == '2') $data['mess_editcategorie'] = '<strong style="color:#F00">Cet abonnement ne peut être supprimé ! Abonnement lié à des partenaires actifs !</strong>';
        if ($this->session->flashdata('mess_editcategorie') == '3') $data['mess_editcategorie'] = '<strong style="color:#060">Abonnement supprimé !</strong>';
        $this->load->view("admin/vwAbonnement", $data);
    }


    function validation()
    {
        $data["no_main_menu_home"] = "1";
        $data["no_left_menu_home"] = "1";

        $toAbonnement = $this->pack_test_validation->getAll();

        $data['toAbonnement'] = $toAbonnement;

        $this->load->view("admin/vwAbonnement", $data);
    }


    function edit_abonnement()
    {
        $IdAbonnement = $this->uri->rsegment(3);

        $toAbonnement = $this->Abonnement->GetById($IdAbonnement);
        $data['toAbonnement'] = $toAbonnement;

        //$this->firephp->log($toAbonnement, 'toAbonnement');

        if (is_numeric(trim($IdAbonnement))) {
            $this->load->view("admin/vwEditAbonnement", $data);
        } else {
            redirect('admin/manage_abonnement/');
        }

    }

    function insert_abonnement()
    {
        $data['toAbonnement'] = NULL;
        $this->load->view("admin/vwEditAbonnement", $data);
    }


    function save_abonnement()

    {
        if (isset($_FILES['image']['tmp_name']) AND !empty($_FILES['image']['tmp_name'])) {

        $nom=$this->genererLettreAleatoir();
        $ocategorie['image']=$nom;
        $config['upload_path']          = './application/resources/front/images/image_module/';
        $config['allowed_types']        = 'jpg|png';
        $config['file_name']        = $nom;
        $this->load->library('upload', $config);
        if ( !$this->upload->do_upload('image')){
           echo $this->upload->display_errors();die();
        }
        }
        $idrubrique_editabonnement = $this->input->post("idrubrique_editabonnement");
        $ocategorie['IdAbonnement'] = $idrubrique_editabonnement;
        $inputcateg_editabonnement = $this->input->post("inputcateg_editabonnement");
        $ocategorie['Nom'] = $inputcateg_editabonnement;
        $inputtarif_editabonnement = $this->input->post("inputtarif_editabonnement");
        $ocategorie['tarif'] = $inputtarif_editabonnement;
        $inputdesc_editabonnement = $this->input->post("inputdesc_editabonnement");
        $ocategorie['Description'] = $inputdesc_editabonnement;
        $inputtype_editabonnement = $this->input->post("inputtype_editabonnement");
        $ocategorie['type'] = $inputtype_editabonnement;

        if ($idrubrique_editabonnement == "0") $this->Abonnement->Insert($ocategorie);
        else $this->Abonnement->Update($ocategorie);

        $this->session->set_flashdata('mess_editcategorie', '1');
        redirect('admin/manage_abonnement/');

    }

    function delete_abonnement()
    {
        $IdRubrique = $this->uri->rsegment(3);

        if (is_numeric(trim($IdRubrique))) {
            //echo $nom_url_commercant;
            $oCategorie = $this->Abonnement->verifier_abonnement_commercant($IdRubrique);
            if (count($oCategorie) == 0) {
                $this->Abonnement->Delete($IdRubrique);
                $this->session->set_flashdata('mess_editcategorie', '3');
            } else {
                $this->session->set_flashdata('mess_editcategorie', '2');
            }

            //$this->firephp->log($oCategorie, 'oCategorie');

            redirect('admin/manage_abonnement/');
        } else {
            redirect('admin/manage_abonnement/');
        }

    }

    function genererLettreAleatoir ($long = 4)
    {
        $alphaB = "abcdefghijklmnopqrstuvwyxz";
        return substr(str_shuffle($alphaB), 0, $long);
    }
    function delete_image_module(){
        $id=$this->input->post('id');
        $image_name=$this->input->post('image_name');
        if ($id !=null AND $id !=0){

            $field=array('image'=>'');
            $delete= $this->Abonnement->delete_image($field,$id);
           if ($delete == 1){
               if (file_exists('application/resources/front/images/image_module/'.$image_name.'.png')){
                   unlink("application/resources/front/images/image_module/'.$image_name.'.png");
                   echo 'ok';
               }elseif (file_exists('application/resources/front/images/image_module/'.$image_name.'.jpg')){
               unlink('application/resources/front/images/image_module/'.$image_name.'.jpg');
                   echo 'ok';
           }

           }else{
               echo 'ko';
           }
        }
    }
    function resizeImage ($_zImg) {

        $path = "application/resources/front/images/image_module/" ;
        $tFile = explode (".", $_zImg) ;
        $config['image_library'] = 'GD2';
        $config['source_image'] =  $path . $_zImg ;
        $config['new_image'] = $path . "ico" . $tFile[0] . "." . $tFile[1] ;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 200 ;
        $config['height'] = 200 ;

        $this->image_lib->initialize($config) ;

        $this->image_lib->resize() ;

        $this->image_lib->clear() ;
    }
}