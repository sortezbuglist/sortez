<?php
    
class newsletters extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->model("mdlbonplan");
        $this->load->model("mdlarticle");
        $this->load->model("mdl_lightbox_mail");
        $this->load->model("newsletter");
        $this->load->model("newsletter_unsubscribe");
        $this->load->model("user");
        $this->load->model("mdl_agenda");
        $this->load->model("mdlcommercantpagination");
        $this->load->model("mdlannonce");
        $this->load->model("mdlfidelity");

        $this->load->library('ion_auth');

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }

    function category($categ='bonplan'){
        $this->load->helper('ckeditor');

        $data['ckeditor_3'] = array(

            //ID of the textarea that will be replaced
            'id' => 'txtContenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => "100%",    //Setting a custom width
                'height' => '768px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'),
                    array('Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt'),
                    array('Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'),
                    array('NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'),
                    array('Link', 'Unlink', 'Anchor'),
                    array('Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'),
                    array('Styles', 'Format', 'Font', 'FontSize'),
                    array('TextColor', 'BGColor'),
                    array('Maximize', 'ShowBlocks', '-', 'About')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        if ($categ=='article') {
            $toListeBonPlan = $this->mdlarticle->listeArticleRecherche(0,0,0,"",0,0,10,"","0","0000-00-00","0000-00-00","0","0");
            $data['toListeBonPlan'] = $toListeBonPlan;
        } else if ($categ=='agenda') {
            $toListeBonPlan = $this->mdl_agenda->listeAgendaRecherche(0,0,0,"",0,0,10,"","0","0000-00-00","0000-00-00","0","0");
            $data['toListeBonPlan'] = $toListeBonPlan;
        } else if ($categ=='annuaire') {
            $toListeBonPlan = $this->mdlcommercantpagination->listePartenaireRecherche(0, 0, 0, "", "", 0, 10, "", "0", "10", "0", "0",  "0");
            $data['toListeBonPlan'] = $toListeBonPlan;
        } else if ($categ=='annonce') {
            $toListeBonPlan = $this->mdlannonce->listeAnnonceRecherche(0, 0, "", "", 0, 10, 0, 0, '', "0", "10", "0", "0");
            $data['toListeBonPlan'] = $toListeBonPlan;
        } else if ($categ=='fidelite') {
            $toListeFidelity_remise = $this->mdlfidelity->listeFidelityRecherche(0, 0, "", "", 0, 10000, 0, 0, "", "0", "10", "0", "0", 0, "remise");
            $toListeFidelity_tampon = $this->mdlfidelity->listeFidelityRecherche(0, 0, "", "", 0, 10000, 0, 0, "", "0", "10", "0", "0", 0, "tampon");
            $toListeFidelity_capital = $this->mdlfidelity->listeFidelityRecherche(0, 0, "", "", 0, 10000, 0, 0, "", "0", "10", "0", "0", 0, "capital");
            $data['toListeFidelity_remise'] = $toListeFidelity_remise;
            $data['toListeFidelity_tampon'] = $toListeFidelity_tampon;
            $data['toListeFidelity_capital'] = $toListeFidelity_capital;
        } else {
            $toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche(0,0,"","",0,10000,0,0,"","0","10","0","0","","");
            $data['toListeBonPlan'] = $toListeBonPlan;
        }

        /*var_dump($toListeBonPlan);
        die();*/

        $data['current_categ'] = $categ;

        $data['txtSujet'] = "Magazine Sortez - Newsletter ".$categ;

        if ($categ=='article') {
            $data['mail_content_newsletter'] = $this->load->view('admin/newsletter/template_article', $data, TRUE);
        } else if ($categ=='agenda') {
            $data['mail_content_newsletter'] = $this->load->view('admin/newsletter/template_agenda', $data, TRUE);
        } else if ($categ=='annuaire') {
            $data['mail_content_newsletter'] = $this->load->view('admin/newsletter/template_annuaire', $data, TRUE);
        } else if ($categ=='annonce') {
            $data['mail_content_newsletter'] = $this->load->view('admin/newsletter/template_annonce', $data, TRUE);
        } else if ($categ=='fidelite') {
            $data['mail_content_newsletter'] = $this->load->view('admin/newsletter/template_fidelity', $data, TRUE);
        } else {
            $data['mail_content_newsletter'] = $this->load->view('admin/newsletter/template', $data, TRUE);
        }

        $this->load->view("admin/newsletter/newsletter", $data);
        //$this->load->view('admin/newsletter/template',$data);
    }

    function index()
    {
        $this->load->helper('ckeditor');


        $data['ckeditor_3'] = array(

            //ID of the textarea that will be replaced
            'id' => 'txtContenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => "100%",    //Setting a custom width
                'height' => '768px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'),
                    array('Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt'),
                    array('Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'),
                    array('NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'),
                    array('Link', 'Unlink', 'Anchor'),
                    array('Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'),
                    array('Styles', 'Format', 'Font', 'FontSize'),
                    array('TextColor', 'BGColor'),
                    array('Maximize', 'ShowBlocks', '-', 'About')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche(0, 0, '', '', 0, 20, 0, 0, "");
        $data['toListeBonPlan'] = $toListeBonPlan;

        $data['current_categ'] = "bonplan";

        $data['txtSujet'] = "Magazine Sortez - Newsletter bonplan";
        $data['liste_saved_mail'] = $this->newsletter->get_saved_mail();
        $data['mail_content_newsletter'] = $this->load->view('admin/newsletter/template', $data, TRUE);

        $this->load->view("admin/newsletter/newsletter", $data);
        //$this->load->view('admin/newsletter/template',$data);
    }


    function envoyer()
    {
        //die("stop");
        $sendonemail = $this->input->post("sendonemail");
        $sendallnews = $this->input->post("sendallnews");
        $txtContenu = $this->input->post("txtContenu");
        $current_categ = $this->input->post("current_categ");
        $txtSujet = $this->input->post("txtSujet");

        //$txtContenu = str_replace('src="', 'src="http://www.club-proximite.com', $txtContenu);
        //$txtContenu = str_replace('href="', 'href="http://www.club-proximite.com', $txtContenu);


        if ($sendallnews == "" || $sendallnews == NULL || $txtContenu == "" || $txtContenu == NULL) {
            redirect('admin/newsletters');
        }
        $data = null;

        $this->load->model("user");
        $colUsers = $this->user->GetAllValidUserList_test();
        if (!isset($txtSujet) || $txtSujet=="")
        $txtSujet = "Magazine Sortez - Newsletter";


        if ($sendallnews == 1) {

            /*$nb_mail_sent = 0;
            $nb_mail_error = 0;
            foreach($colUsers as $objUser) {
                $colDest = array();
                $colDest[] = array("Email" => $objUser->Email, "Name" => $objUser->Prenom . " " . $objUser->Nom);

                if(envoi_notification($colDest, $txtSujet, $txtContenu)) {
                    //$data['mssgerrornews'] = "Emails sont envoy&eacute;s<br/>";
                    ++$nb_mail_sent;
                } else {
                    //$data['mssgerrornews'] = "Une erreur s'est produite lors de l'envoi des mails<br/>";
                    ++$nb_mail_error;
                }
            }
            $data['nb_mail_sent'] = $nb_mail_sent;
            $data['nb_mail_error'] = $nb_mail_error;*/
            $data["mObject"] = $txtSujet;
            $data["mContent"] = $txtContenu;
            $data["mCategory"] = "bonplan";
            $data["iDuserCapturedEmail"] = $this->input->post("iDuserCapturedEmail");
            $data["iDuserCustomerEmail"] = $this->input->post("iDuserCustomerEmail");

            $newsletter['subject'] = $txtSujet;
            $newsletter['category'] = $current_categ;
            $newsletter['status'] = "0";
            $newsletter['date_creation'] = date("Y-m-d");
            $newsletter['content'] = $txtContenu;
            $newsletter['id'] = null;
            if ((isset($data["iDuserCapturedEmail"]) && $data["iDuserCapturedEmail"]=='1') && (isset($data["iDuserCustomerEmail"]) && $data["iDuserCustomerEmail"]=='1'))
                $newsletter['type'] = '4';
            else if ((isset($data["iDuserCapturedEmail"]) && $data["iDuserCapturedEmail"]=='1') && (!isset($data["iDuserCustomerEmail"]) || $data["iDuserCustomerEmail"]=='0'))
                $newsletter['type'] = '2';
            else if ((!isset($data["iDuserCapturedEmail"]) || $data["iDuserCapturedEmail"]=='0') && (isset($data["iDuserCustomerEmail"]) && $data["iDuserCustomerEmail"]=='1'))
                $newsletter['type'] = '3';
            else $newsletter['type'] = '1';
            $idNewsletter = $this->newsletter->insert($newsletter);
            $data["idNewsletter"] = $idNewsletter;

            $this->load->view("admin/newsletter/send_multiple", $data);

        } else if ($sendallnews == 2) {

            $sendonemail_to_unsubscribe = $sendonemail;
            $sendonemail_to_unsubscribe = str_replace("@", ":AT:", $sendonemail_to_unsubscribe);
            $sendonemail_to_unsubscribe = str_replace(".", ":POINT:", $sendonemail_to_unsubscribe);
            $txtContenu = str_replace("::email::", $sendonemail_to_unsubscribe, $txtContenu);

            $sendonemail_to_unsubscribe_normal = $sendonemail;
            $txtContenu = str_replace("::email_normal::", $sendonemail_to_unsubscribe_normal, $txtContenu);

            $newsletter['subject'] = $txtSujet;
            $newsletter['category'] = $current_categ;
            $newsletter['status'] = "1";
            $newsletter['date_creation'] = date("Y-m-d");
            $newsletter['content'] = $txtContenu;
            $newsletter['id'] = null;
            $newsletter['type'] = '1';
            $newsletter['email_receiver'] = $sendonemail;
            $idNewsletter = $this->newsletter->insert($newsletter);
            $data["idNewsletter"] = $idNewsletter;

            $colDest[] = array("Email" => $sendonemail, "Name" => "Email_Club");
            if (envoi_notification($colDest, $txtSujet, $txtContenu)) $data['mssgerrornews'] = "Email envoy&eacute; !";
            $this->load->view("admin/newsletter/confirmation", $data);

        }


    }

    function send_mail_by_id_captured()
    {
        $current_id = $this->input->post("toFirstIdCapturedEmail");
        $idNewsletter = $this->input->post("idNewsletter");
        $toUserNews = $this->mdl_lightbox_mail->getById($current_id);
        $toContentNewsletter = $this->newsletter->getById($idNewsletter);
        $objResult = array();
        if (count($toUserNews) > 0) {

            $sendonemail_to_unsubscribe = $toUserNews->mail;
            $sendonemail_to_unsubscribe = str_replace("@", ":AT:", $sendonemail_to_unsubscribe);
            $sendonemail_to_unsubscribe = str_replace(".", ":POINT:", $sendonemail_to_unsubscribe);
            $toContentNewsletter->content = str_replace("::email::", $sendonemail_to_unsubscribe, $toContentNewsletter->content);
            $checkUnsubcribe = $this->newsletter_unsubscribe->get_by_mail($toUserNews->mail);

            $sendonemail_to_unsubscribe_normal = $toUserNews->mail;
            $toContentNewsletter->content = str_replace("::email_normal::", $sendonemail_to_unsubscribe_normal, $toContentNewsletter->content);

            //echo json_encode((array)$toUserNews);
            if (count($checkUnsubcribe) <= 0) {//(strpos($toUserNews->mail, 'randawilly') !== false || strpos($toUserNews->mail, 'woignier') !== false) &&
                $zEmailTo[] = array("Email" => $toUserNews->mail, "Name" => "Contact");
                $sending_mail_newsletter_status = envoi_notification($zEmailTo, $toContentNewsletter->subject, $toContentNewsletter->content);
                if ($sending_mail_newsletter_status) $objResult['send_status'] = '1'; else $objResult['send_status'] = '0';
                $objResult['idNewsletter'] = $idNewsletter;
                $objResult['mail'] = $toUserNews->mail;
            } else {
                $objResult['send_status'] = '0';
                $objResult['idNewsletter'] = $idNewsletter;
                $objResult['mail'] = $toUserNews->mail;
            }
            echo json_encode((array)$objResult);
        } else {
            echo json_encode((array)$objResult);
        }
    }

    function send_mail_by_id_users()
    {
        $current_id = $this->input->post("toFirstIdCapturedEmail");
        $idNewsletter = $this->input->post("idNewsletter");
        $toUserNews = $this->user->getMailByIdUser($current_id);
        $toContentNewsletter = $this->newsletter->getById($idNewsletter);
        $objResult = array();
        if (count($toUserNews) > 0) {
            if (!isset($toUserNews->mail) || $toUserNews->mail == "") $toUserNews->mail = $toUserNews->mail2;

            $sendonemail_to_unsubscribe = $toUserNews->mail;
            $sendonemail_to_unsubscribe = str_replace("@", ":AT:", $sendonemail_to_unsubscribe);
            $sendonemail_to_unsubscribe = str_replace(".", ":POINT:", $sendonemail_to_unsubscribe);
            $toContentNewsletter->content = str_replace("::email::", $sendonemail_to_unsubscribe, $toContentNewsletter->content);
            $checkUnsubcribe = $this->newsletter_unsubscribe->get_by_mail($toUserNews->mail);

            $sendonemail_to_unsubscribe_normal = $toUserNews->mail;
            $toContentNewsletter->content = str_replace("::email_normal::", $sendonemail_to_unsubscribe_normal, $toContentNewsletter->content);

            //echo json_encode((array)$toUserNews);
            if (count($checkUnsubcribe) <= 0) {//(strpos($toUserNews->mail, 'randawilly') !== false || strpos($toUserNews->mail, 'woignier') !== false) &&
                $zEmailTo[] = array("Email" => $toUserNews->mail, "Name" => "Contact");
                $sending_mail_newsletter_status = envoi_notification($zEmailTo, $toContentNewsletter->subject, $toContentNewsletter->content);
                if ($sending_mail_newsletter_status) $objResult['send_status'] = '1'; else $objResult['send_status'] = '0';
                $objResult['idNewsletter'] = $idNewsletter;
                $objResult['mail'] = $toUserNews->mail;
            } else {
                $objResult['send_status'] = '0';
                $objResult['idNewsletter'] = $idNewsletter;
                $objResult['mail'] = $toUserNews->mail;
            }
            echo json_encode((array)$objResult);
        } else {
            echo json_encode((array)$objResult);
        }
    }

    function update_newsletter()
    {
        $nb_success = $this->input->post("count_global_newsletter_send_success");
        $nb_failed = $this->input->post("count_global_newsletter_send_failed");
        $idNews = $this->input->post("idNewsletter");
        $newsStatus = $this->input->post("newsStatus");
        $newsletter = array();
        if (isset($idNews) && $idNews != "" && $idNews != "0" && is_numeric(intval($idNews))) {
            $toNewsletter = $this->newsletter->getById($idNews);
            $newsletter['id'] = $toNewsletter->id;
            $newsletter['subject'] = $toNewsletter->subject;
            $newsletter['category'] = $toNewsletter->category;
            $newsletter['date_creation'] = $toNewsletter->date_creation;
            $newsletter['date_sent'] = date("Y-m-d");
            $newsletter['content'] = $toNewsletter->content;
            if (!isset($newsStatus)||$newsStatus=="") $newsStatus=$toNewsletter->status;
            $newsletter['status'] = $newsStatus;
            $newsletter['receiver'] = $toNewsletter->receiver;
            $newsletter['email_receiver'] = $toNewsletter->email_receiver;
            $newsletter['error'] = $toNewsletter->error;
            if (!isset($nb_success)||$nb_success=="") $nb_success=$toNewsletter->nb_sent;
            $newsletter['nb_sent'] = $nb_success;
            if (!isset($nb_failed)||$nb_failed=="") $nb_failed=$toNewsletter->nb_blocked;
            $newsletter['nb_blocked'] = $nb_failed;
            $newsletter['type'] = $toNewsletter->type;
            $objNewsletter = $this->newsletter->update($newsletter);
            echo "1";
        } else {
            echo "0";
        }
    }

    function delete_sent_item() {
        $User_id = $this->input->post("iDItem");
        $ss = $this->newsletter->delete($User_id);
        $data['delete_result'] = $ss;
        $this->load->view('admin/newsletter/newsletter_sent_list', $data);
    }

    public function get_agenda_by_date(){
        $this->load->helper('ckeditor');

        $data['ckeditor_3'] = array(

            //ID of the textarea that will be replaced
            'id' => 'txtContenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => "100%",    //Setting a custom width
                'height' => '768px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'),
                    array('Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt'),
                    array('Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'),
                    array('NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'),
                    array('Link', 'Unlink', 'Anchor'),
                    array('Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'),
                    array('Styles', 'Format', 'Font', 'FontSize'),
                    array('TextColor', 'BGColor'),
                    array('Maximize', 'ShowBlocks', '-', 'About')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );
        $filtre=$this->input->post('filtre');

        if($filtre=='1'){
            $toListeBonPlan =  $this->mdl_agenda->listeAgendaRecherche(0,0,0,"",0,0,10,"","0","0000-00-00","0000-00-00","0","0");
        }
        else if($filtre=='101'){
            $toListeBonPlan =  $this->mdl_agenda->listeAgendaRecherche(0,0,0,"",0,0,10,"","101","0000-00-00","0000-00-00","0","0");
        }
        else if($filtre=='202'){
            $toListeBonPlan =  $this->mdl_agenda->listeAgendaRecherche(0,0,0,"",0,0,10,"","202","0000-00-00","0000-00-00","0","0");
        }
        else if($filtre=='303'){
            $toListeBonPlan =  $this->mdl_agenda->listeAgendaRecherche(0,0,0,"",0,0,10,"","303","0000-00-00","0000-00-00","0","0");
        }
        else if($filtre=='404'){
            $toListeBonPlan =  $this->mdl_agenda->listeAgendaRecherche(0,0,0,"",0,0,10,"","404","0000-00-00","0000-00-00","0","0");
        }
        else if($filtre=='505'){
            $toListeBonPlan =  $this->mdl_agenda->listeAgendaRecherche(0,0,0,"",0,0,10,"","505","0000-00-00","0000-00-00","0","0");
        }
        $data['toListeBonPlan'] = $toListeBonPlan;

        $data['mail_content_newsletter'] = $this->load->view('admin/newsletter/template_agenda', $data, TRUE);
        $this->load->view("admin/newsletter/template_agenda", $data);

    }public function get_article_by_date(){
        $this->load->helper('ckeditor');

        $data['ckeditor_3'] = array(

            //ID of the textarea that will be replaced
            'id' => 'txtContenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => "100%",    //Setting a custom width
                'height' => '768px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'),
                    array('Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt'),
                    array('Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'),
                    array('NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'),
                    array('Link', 'Unlink', 'Anchor'),
                    array('Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'),
                    array('Styles', 'Format', 'Font', 'FontSize'),
                    array('TextColor', 'BGColor'),
                    array('Maximize', 'ShowBlocks', '-', 'About')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );
        $filtre=$this->input->post('filtre');

        if($filtre=='1'){
            $toListeBonPlan = $this->mdlarticle->listeArticleRecherche(0,0,0,"",0,0,10,"","0","0000-00-00","0000-00-00","0","0");
        }
        else if($filtre=='101'){
            $toListeBonPlan = $this->mdlarticle->listeArticleRecherche(0,0,0,"",0,0,10,"","101","0000-00-00","0000-00-00","0","0");
        }
        else if($filtre=='202'){
            $toListeBonPlan = $this->mdlarticle->listeArticleRecherche(0,0,0,"",0,0,10,"","202","0000-00-00","0000-00-00","0","0");
        }
        else if($filtre=='303'){
            $toListeBonPlan = $this->mdlarticle->listeArticleRecherche(0,0,0,"",0,0,10,"","303","0000-00-00","0000-00-00","0","0");
        }
        else if($filtre=='404'){
            $toListeBonPlan = $this->mdlarticle->listeArticleRecherche(0,0,0,"",0,0,10,"","404","0000-00-00","0000-00-00","0","0");
        }
        else if($filtre=='505'){
            $toListeBonPlan = $this->mdlarticle->listeArticleRecherche(0,0,0,"",0,0,10,"","505","0000-00-00","0000-00-00","0","0");
        }
        $data['toListeBonPlan'] = $toListeBonPlan;

        $data['mail_content_newsletter'] = $this->load->view('admin/newsletter/template_article', $data, TRUE);
        $this->load->view("admin/newsletter/template_article", $data);

    }
//    public function get_article_by_date()
//    {
//        $this->load->helper('ckeditor');
//
//        $data['ckeditor_3'] = array(
//
//            //ID of the textarea that will be replaced
//            'id' => 'txtContenu',
//            'path' => APPPATH . 'resources/ckeditor',
//
//            //Optionnal values
//            'config' => array(
//                'width' => "100%",    //Setting a custom width
//                'height' => '768px',    //Setting a custom height
//                'toolbar' => array(    //Setting a custom toolbar
//                    array('Source'),
//                    array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'),
//                    array('Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt'),
//                    array('Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'),
//                    array('NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'),
//                    array('Link', 'Unlink', 'Anchor'),
//                    array('Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'),
//                    array('Styles', 'Format', 'Font', 'FontSize'),
//                    array('TextColor', 'BGColor'),
//                    array('Maximize', 'ShowBlocks', '-', 'About')
//                ),
//                'filebrowserUploadUrl' => site_url('uploadfile/index/')
//            )
//        );
//        $filtre = $this->input->post('filtre');
//
//        if ($filtre == '1') {
//            $toListeBonPlan = $this->mdlarticle->listeArticleRecherche(0, 0, 0, "", 0, 0, 10, "", "0", "0000-00-00", "0000-00-00", "0", "0");
//        } else if ($filtre == '101') {
//            $toListeBonPlan = $this->mdlarticle->listeArticleRecherche(0, 0, 0, "", 0, 0, 10, "", "101", "0000-00-00", "0000-00-00", "0", "0");
//        } else if ($filtre == '202') {
//            $toListeBonPlan = $this->mdlarticle->listeArticleRecherche(0, 0, 0, "", 0, 0, 10, "", "202", "0000-00-00", "0000-00-00", "0", "0");
//        } else if ($filtre == '303') {
//            $toListeBonPlan = $this->mdlarticle->listeArticleRecherche(0, 0, 0, "", 0, 0, 10, "", "303", "0000-00-00", "0000-00-00", "0", "0");
//        } else if ($filtre == '404') {
//            $toListeBonPlan = $this->mdlarticle->listeArticleRecherche(0, 0, 0, "", 0, 0, 10, "", "404", "0000-00-00", "0000-00-00", "0", "0");
//        } else if ($filtre == '505') {
//            $toListeBonPlan = $this->mdlarticle->listeArticleRecherche(0, 0, 0, "", 0, 0, 10, "", "505", "0000-00-00", "0000-00-00", "0", "0");
//        }
//        $data['toListeBonPlan'] = $toListeBonPlan;
//
//        $data['mail_content_newsletter'] = $this->load->view('admin/newsletter/template_article', $data, TRUE);
//        $this->load->view("admin/newsletter/template_article", $data);
//
//    }
        public function get_annonce_by_date(){
            $this->load->helper('ckeditor');

            $data['ckeditor_3'] = array(

                //ID of the textarea that will be replaced
                'id' => 'txtContenu',
                'path' => APPPATH . 'resources/ckeditor',

                //Optionnal values
                'config' => array(
                    'width' => "100%",    //Setting a custom width
                    'height' => '768px',    //Setting a custom height
                    'toolbar' => array(    //Setting a custom toolbar
                        array('Source'),
                        array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'),
                        array('Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt'),
                        array('Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'),
                        array('NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'),
                        array('Link', 'Unlink', 'Anchor'),
                        array('Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'),
                        array('Styles', 'Format', 'Font', 'FontSize'),
                        array('TextColor', 'BGColor'),
                        array('Maximize', 'ShowBlocks', '-', 'About')
                    ),
                    'filebrowserUploadUrl' => site_url('uploadfile/index/')
                )
            );
            $filtre=$this->input->post('filtre');

            if($filtre==''){
                $toListeBonPlan = $this->mdlannonce->listeAnnonceRecherche(0, 0, "", "", 0, 10, 0, 0, '', "0", "10", "0", "0");
            }
            else if($filtre=='0'){
                $toListeBonPlan = $this->mdlannonce->listeAnnonceRecherche(0, 0, "",0, 0, 10, 0, 0, '', "0", "10", "0", "0");
            }
            else if($filtre=='1'){
                $toListeBonPlan = $this->mdlannonce->listeAnnonceRecherche(0, 0, "",1, 0, 10, 0, 0, '', "0", "10", "0", "0");
            }

            $data['toListeBonPlan'] = $toListeBonPlan;

            $data['mail_content_newsletter'] = $this->load->view('admin/newsletter/template_annonce', $data, TRUE);
            $this->load->view("admin/newsletter/template_annonce", $data);

        }
    public function get_bonplan_by_date(){
        $this->load->helper('ckeditor');

        $data['ckeditor_3'] = array(

            //ID of the textarea that will be replaced
            'id' => 'txtContenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => "100%",    //Setting a custom width
                'height' => '768px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'),
                    array('Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt'),
                    array('Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'),
                    array('NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'),
                    array('Link', 'Unlink', 'Anchor'),
                    array('Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'),
                    array('Styles', 'Format', 'Font', 'FontSize'),
                    array('TextColor', 'BGColor'),
                    array('Maximize', 'ShowBlocks', '-', 'About')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );
        $filtre=$this->input->post('filtre');

        if($filtre=='0'){
            $toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche(0,0,"","",0,10000,0,0,"","0","10","0","0","","");
        }
        else if($filtre=='1'){
            $toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche(0,0,"","",0,10000,0,0,"","0","10","0","0","",1);
        }
        else if($filtre=='2'){
            $toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche(0,0,"","",0,10000,0,0,"","0","10","0","0","",2);
        }
        else if($filtre=='3'){
            $toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche(0,0,"","",0,10000,0,0,"","0","10","0","0","",3);
        }

        $data['toListeBonPlan'] = $toListeBonPlan;

        $data['mail_content_newsletter'] = $this->load->view('admin/newsletter/template', $data, TRUE);
        $this->load->view("admin/newsletter/template", $data);

    }
    public function get_fidelity_by_date(){
        $this->load->helper('ckeditor');

        $data['ckeditor_3'] = array(

            //ID of the textarea that will be replaced
            'id' => 'txtContenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => "100%",    //Setting a custom width
                'height' => '768px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'),
                    array('Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt'),
                    array('Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'),
                    array('NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'),
                    array('Link', 'Unlink', 'Anchor'),
                    array('Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'),
                    array('Styles', 'Format', 'Font', 'FontSize'),
                    array('TextColor', 'BGColor'),
                    array('Maximize', 'ShowBlocks', '-', 'About')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );
        $filtre=$this->input->post('filtre');

        if (isset($filtre) && $filtre == "1"){
            $toListeFidelity_remise = $this->mdlfidelity->listeFidelityRecherche(0, 0, "", "", 0, 10000, 0, 0, "", "0", "10", "0", "0", 0, "remise");
            $data['toListeFidelity_remise'] = $toListeFidelity_remise;
        }
        else if (isset($filtre) && $filtre == "3"){
            $toListeFidelity_tampon = $this->mdlfidelity->listeFidelityRecherche(0, 0, "", "", 0, 10000, 0, 0, "", "0", "10", "0", "0", 0, "tampon");
            $data['toListeFidelity_tampon'] = $toListeFidelity_tampon;
        }
        else if (isset($filtre) && $filtre == "2") {
            $toListeFidelity_capital = $this->mdlfidelity->listeFidelityRecherche(0, 0, "", "", 0, 10000, 0, 0, "", "0", "10", "0", "0", 0, "capital");
            $data['toListeFidelity_capital'] = $toListeFidelity_capital;
        }
        else {
            $toListeFidelity_remise = $this->mdlfidelity->listeFidelityRecherche(0, 0, "", "", 0, 10000, 0, 0, "", "0", "10", "0", "0", 0, "remise");
            $toListeFidelity_tampon = $this->mdlfidelity->listeFidelityRecherche(0, 0, "", "", 0, 10000, 0, 0, "", "0", "10", "0", "0", 0, "tampon");
            $toListeFidelity_capital = $this->mdlfidelity->listeFidelityRecherche(0, 0, "", "", 0, 10000, 0, 0, "", "0", "10", "0", "0", 0, "capital");
            $data['toListeFidelity_tampon'] = $toListeFidelity_tampon;
            $data['toListeFidelity_capital'] = $toListeFidelity_capital;
            $data['toListeFidelity_remise'] = $toListeFidelity_remise;
        }


        $data['mail_content_newsletter'] = $this->load->view('admin/newsletter/template_fidelity', $data, TRUE);
        $this->load->view("admin/newsletter/template_fidelity", $data);

    }
    public function save_newsletter_stat(){
        $mail_content=$this->input->post('mail_content');
        $txtSujet=$this->input->post('txtSujet');
        $nom=$this->input->post('nom_hidden');
        $field=array( "mail_content"=>$mail_content,
                        "date_ajout"=>date('Y-m-d'),
                    "object"=>$txtSujet,
                    'nom'=>$nom);
        $save=$this->newsletter->save_newsletter_stat($field);
        if ($save=='1'){
           redirect('admin/Newsletters/');
        }
    }
    public function delete_newsletter_stat(){
        $id=$this->input->post('id'); 
        $delete_stat=$this->newsletter->delete_newsletter_stat($id);
        if ($delete_stat=='1'){
           echo "1";
        }
    }
    public function change_stat_news(){
        $id=$this->input->post('id');
        $loaded=$this->newsletter->get_data_by_id($id);
         echo json_encode($loaded);
    }
    
}
