<?php

class Pictures extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        // LOADING MODEL
        $this->load->model('PicturesModel', 'newAdminModel');

        // LOADING LIBRARIES
        $this->load->library('form_validation');

        // LOADING HELPERS
        $this->load->helper('form');
    }

    public function form()
    {
        $this->create();
    }



    public function index()
    {
        $data = $this->newAdminModel->getAllItems();
        
       

        $this->load->view('sortez_vsv/wall_pictures', compact('data'));
    }


    public function create($data = NULL)
    {
        $this->load->view('admin/wallcrud/index.php', $data);
    }

    public function store()
    {
        $this->form_validation->set_rules(
            'title',
            'Title',
            'required|max_length[255]|min_length[3]'
        );
        $this->form_validation->set_rules(
            'description',
            'Description',
            'required|min_length[3]'
        );
        // $this->form_validation->set_rules(
        //     'image',
        //     'Image',
        //     'required'
        // );

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('admin/wallcrud/index');
        } else {
            // STORING THE IMAGE UPLOADED
            $new_name = time() . $_FILES["image"]['name']; //This line will be generating random name for images that are uploaded 

            $config['upload_path'] = './application/images/';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['file_name'] = $new_name;

            $this->load->library('upload', $config); //Loads the Uploader Library

            //$this->upload->initialize($config);        

            if (!$this->upload->do_upload('image')) {
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('admin/wallcrud/index', $error);
            } else {
                $data = $this->upload->data(); //This will upload the `image/file` using native image upload
            }

            // SETTING DATA TO BE REGISTERED IN THE DATABASE
            $data_value = [
                'title' => $this->input->post('title'),
                'description' => $this->input->post('description'),
                'image' => $data['file_name']
            ];

            // PASSING DATA TO MODEL AS THE ARRAY PARAMETER
            $this->newAdminModel->create($data_value); //save_employee is the function name in the Model 

            redirect('admin/Pictures/lists');
        }
    }

    public function lists()
    {
        $results = $this->newAdminModel->getAllItems();
        $datas = ['datas' => $results];
        $this->load->view('admin/wallcrud/lists', $datas);
    }

    public function show(int $id)
    {
        $results = $this->newAdminModel->getAnItem($id);
        $datas = ['datas' => $results];
        $this->load->view('admin/wallcrud/show', $datas);
    }

    public function edit(int $id)
    {
        $result = $this->newAdminModel->getAnItem($id);
        $data = ['data' => $result];
        $this->load->view('admin/wallcrud/edit', $data);
    }

    public function update(int $id)
    {
        $this->form_validation->set_rules(
            'title',
            'Title',
            'required|max_length[255]|min_length[3]'
        );
        $this->form_validation->set_rules(
            'description',
            'Description',
            'required|min_length[3]'
        );
        // $this->form_validation->set_rules(
        //     'image',
        //     'Image',
        //     'required'
        // );

        if ($this->form_validation->run() === FALSE) {
            $this->edit($id);
        } else {
            $store_new_image = false;
            if (!empty($_FILES['image'])) {
                // STORING THE IMAGE UPLOADED
                $new_name = time() . $_FILES["image"]['name']; //This line will be generating random name for images that are uploaded 

                $config['upload_path'] = './application/images/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config); //Loads the Uploader Library

                //$this->upload->initialize($config);        

                if (!$this->upload->do_upload('image')) {
                    $error = array('error' => $this->upload->display_errors());
                    $this->load->view('admin/wallcrud/edit', $error);
                } else {
                    $data = $this->upload->data(); //This will upload the `image/file` using native image upload
                    $store_new_image = true;
                }

                if ($store_new_image) {
                    $results = $this->newAdminModel->getAnItem($id);
                    unlink(APPPATH . 'views/newadmin/images/' . $results[0]->image);
                }
            }

            // SETTING DATA TO BE REGISTERED IN THE DATABASE
            $data_value = [
                'title' => $this->input->post('title'),
                'description' => $this->input->post('description')
            ];
            $data_value['image'] = isset($data) ? $data['file_name'] : NULL;

            // UPDATE THE ITEM id = $id IN THE DATABASE
            $this->newAdminModel->update($id, $data_value);

            // REDIRECTING
            redirect('admin/Pictures/show/' . $id);
        }
    }

    public function delete($id)
    {
        $this->newAdminModel->delete($id);
        $this->load->view('admin/wallcrud/index');
    }
}
