<?php
class test extends CI_Controller{
    private $mCurrentFilter = 2; // Le type de filtre en cours (0 : exact, 1 : d?but, 2 : milieu)
    private $mSearchValue = ""; // La valeur ? rechercher
    private $mLinesPerPage = 50 ;// Le nombre de lignes (enregistrements) ? afficher par page
    function __construct() {
        parent::__construct();

        $this->load->Model("mdl_test");
        $this->load->Model("Rubrique");
        $this->load->Model("SousRubrique");
        $this->load->Model("Ville");
        $this->load->Model("mdlville");
        $this->load->Model("mdldepartement");
        $this->load->Model("Abonnement");
        $this->load->Model("AssCommercantRubrique");
        $this->load->Model("AssCommercantSousRubrique");
        $this->load->Model("AssCommercantAbonnement");
        $this->load->Model("mdldemande_abonnement");

        $this->load->Model("mdlbonplan");
        $this->load->Model("mdlannonce");
        $this->load->Model("mdl_agenda");
        $this->load->Model("mdlagenda_perso");
        
        $this->load->model("mdlcategorie");

        $this->load->library('session');

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_model");
        $this->load->model("ion_auth_used_by_club");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }

    function index(){
        $this->liste();
    }


    public function GetSearchValue() {
        $SearchValue ;
        if (isset($_POST["txtSearch"])) {
            $SearchValue = $_POST["txtSearch"] ;
        } else {
            if ($this->session->userdata("SearchValue")== null) {
                $SearchValue = "";
            }
            $SearchValue = $this->session->userdata("SearchValue");
        }

        return addslashes($SearchValue) ;
    }





    public function liste($prmFilterCol = "-1", $prmFilterValue = "0", $prmPagerIndex = 0, $prmIsCompleteList = false, $prmOrder = "") {
        $_SESSION["User_FilterCol"] = $prmFilterCol;
        $_SESSION["User_FilterValue"] = $prmFilterValue;

        if ($prmIsCompleteList) {
            $this->_clearAllSessionsData();
        }

        if (isset($_POST["cmbNbLignes"])) {
            $this->SetLinesPerPage($_POST["cmbNbLignes"]);
        }

        if (isset($_POST["txtSearch"])) {
            $this->SetSearchValue($_POST["txtSearch"]);
        }

        if (isset($_POST["optTypeFiltre"])) {
            $this->SetCurrentFilter($_POST["optTypeFiltre"]);
        }

        if (isset($_POST["hdnOrder"])) {
            $this->SetOrder($_POST["hdnOrder"]);
        }

        $SearchedWordsString = str_replace("+", " ", $this->GetSearchValue()) ;
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString) ;
        $WhereKeyWords = " 0=0 " ;


        for ($i = 0; $i < sizeof($SearchedWords); $i ++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search) ;
            if ($Search != "") {
                if ($i == 0) {
                    $WhereKeyWords .= " AND " ;
                }


                $WhereKeyWords .= " ( " .
                    " UPPER(test1.testid ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(test1.test ) LIKE '%" . strtoupper($Search) . "%'" .
                    " )
                ";
                if ($i != (sizeof($SearchedWords) - 1)){
                    $WhereKeyWords .= " OR ";
                }
            }
        }

        // Pre-filtre sur une table parent (cas d'une liste fille dans une fiche maitre-detail)
        if ($prmFilterCol != "-1") {
            $WhereKeyWords .= " AND " . $prmFilterCol . " = '" . $prmFilterValue . "'" ;
        }



        // echo $WhereKeyWords; exit;
        //$objCommercants = $this->Commercant->GetAll();
        $colusers = $this->mdl_test->GetWhere_pvc($WhereKeyWords, $prmPagerIndex);


        // Pagination (CI)
        $this->load->library("pagination");
        $this->config->load("config");
        $pagination_config['uri_segment'] = '5';
        $data["UriSegment"] = $pagination_config['uri_segment'];
        $pagination_config["base_url"] = site_url() . "admin/test/liste/" . $prmFilterCol . "/" . $prmFilterValue ;
        $pagination_config["total_rows"] = $this->mdl_test->CountWhere_pvc($WhereKeyWords);
        $pagination_config["per_page"] = $this->GetLinesPerPage();
        $pagination_config["first_link"] = '&lsaquo;';
        $pagination_config["last_link"] = '&raquo';
        $pagination_config["next_link"] = '&gt;';
        $pagination_config["prev_link"] = '&lt;';
        $pagination_config["cur_tag_open"] = "<span style='font-weight: bold; padding-right: 5px; padding-left: 5px; '>" ;
        $pagination_config["cur_tag_close"] = "</span>" ;
        $pagination_config["num_tag_open"] = "&nbsp;<span>" ;
        $pagination_config["num_tag_close"] = "&nbsp;</span>" ;

        $this->pagination->initialize($pagination_config);
        // Effectuer un highlight des mots-cles recherches dans la liste des enregistrements trouv&s
        $data["highlight"] = "";
        if ($this->GetSearchValue() != "") {
            $data["highlight"] = "yes";
        } else {
            $data["highlight"] = "";
        }

        // Les numeros de page
        $NumerosPages = array();
        //$NumerosPages[] = 0;
        $n = 0;
        while($n < $pagination_config["total_rows"] - 1) {
            $NumerosPages[] = $n;
            $n += $pagination_config["per_page"];
        }
        $data["NumerosPages"] = $NumerosPages;


        // Preparer les variables pour le view
        $data["colUsers"] = $colusers ;

        $data["PaginationLinks"] = $this->pagination->create_links();
        $data["SearchValue"] = $this->GetSearchValue();
        $data["NbLignes"] = $this->GetLinesPerPage() ;
        $data["SearchedWords"] = $SearchedWords;
        $data["Keywords"] = $this->GetSearchValue();
        $data["CountAllResults"] = $pagination_config["total_rows"];


        $DepartCount = $prmPagerIndex;
        $data["DepartCount"] =  $DepartCount;

        // Data pour le pre-filtre
        $data["FilterCol"] = $prmFilterCol;
        $data["FilterValue"] = $prmFilterValue;

        $data["no_main_menu_home"] = "1";

        $data["ion_auth_used_by_club"] = $this->ion_auth_used_by_club;
        $data["ion_auth"] = $this->ion_auth;
        $data["colArticles"] =$this->mdl_test->GetAll();
        //$this->load->view("admin/vwUsersadmin", $data) ;
        $this->load->view("admin/vwListeTypetest", $data) ;
    }


    public function GetLinesPerPage() {
        if (isset($_POST["cmbNbLignes"])) {
            return  $_POST["cmbNbLignes"];
        } else {
            if ($this->session->userdata("LinesPerPage")== null) {
                return 50;
            }
            return $this->session->userdata("LinesPerPage");
        }
    }


    public function SetCurrentFilter($argValue) {
        $this->mCurrentFilter = $argValue ;
        $this->session->set_userdata("curFilter",$argValue) ;
    }


    public function GetOrder() {
        if (isset($_POST["hdnOrder"])) {
            return $_POST["hdnOrder"] ;
        } else {
            if ($this->session->userdata("UserOrder")== null) {
                return "";
            }
            return $this->session->userdata("UserOrder") ;
        }
    }

    public function SetOrder($argValue) {
        $this->session->set_userdata("UserOrder",$argValue) ;
    }

    function _clearAllSessionsData() {
        $this->SetCurrentFilter(2);
        $this->SetSearchValue("");
        $this->SetLinesPerPage(50);
        $this->SetOrder("");
    }




    public function SetSearchValue($argValue) {
        $this->mSearchValue = $argValue ;
        $this->session->set_userdata("SearchValue",$argValue) ;
    }

    function fiche_test($IdTest) {

        if ($IdTest != 0){
            $data["title"] = "Modification Test" ;
            $data["otest"] = $this->mdl_test->getById($IdTest) ;
            $data["testid"] = $IdTest;


            $this->load->view('admin/vwFicheTypetest', $data) ;

        }else{
            $data["title"] = "Creer Test" ;

            $this->load->view('admin/vwFicheTypetest', $data) ;
        }
    }

    function creer_test(){
        $oArticle['test'] = $this->input->post("test");
        $result = $this->mdl_test->inserttest($oArticle);
        $data["msg"] = "Test ajouté" ;


        $data["colArticles"] =$this->mdl_test->GetAll();

        $this->load->view("admin/vwListeTypetest",$data);
    }

    function modifier($ID)// model miantso anle fiche
{   $data['record'] = $this->mdl_test->getbyid2($ID);
    $this->load->view('admin/vwListeTytest', $data);
}

public function update()//mametaka any amle base de donnee
{
    $result=$this->mdl_test->update();//miantso anle fonction mametaka anle modification any amin model
    redirect(site_url('admin/test/liste'));//mamerina any amle liste
}

    function delete($prmId) {

        if(is_numeric(trim($prmId))) {
            //verify if category contain active article********
            $oCategorie = $this->mdl_test->verifier_test($prmId) ;
            //$oCategorie = array();
            if (count($oCategorie)==0){
                $this->mdl_test->supprimetest($prmId);
                $this->session->set_flashdata('mess_editTypetest', '3');
            } else {
                $this->session->set_flashdata('mess_editTypetest', '2');
            }

            //$this->firephp->log($oCategorie, 'oCategorie');

            redirect("admin/test");
        }
        else {
            redirect("admin/test");
        }

    }


}