<?php
class newsletters extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        
		$this->load->library('session');
        $this->load->model("mdlbonplan") ;

        $this->load->library('ion_auth');

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }
    
    function index(){
        $this->load->helper('ckeditor');
 
 
         $data['ckeditor_3'] = array(
 
            //ID of the textarea that will be replaced
            'id'     =>     'txtContenu',
            'path'    =>    APPPATH . 'resources/ckeditor',
 
            //Optionnal values
            'config' => array(
                'width'     =>     "100%",    //Setting a custom width
                'height'     =>     '768px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ),
                    array( 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ),
                    array( 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ),
                    array( 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ),
                    array( 'Link','Unlink','Anchor' ),
                    array( 'Image','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ),
                    array( 'Styles','Format','Font','FontSize' ),
                    array( 'TextColor','BGColor' ),
                    array( 'Maximize', 'ShowBlocks','-','About' )
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );
         
        $path_home_base_url_club = site_url();

        $toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche(0,0,'','',0,10000,0);



///debut template newsletter **************************************************************************************************
        $data['mail_content_newsletter'] = '


<div style="width:1024px; margin-left:auto; margin-right:auto; font-family:Verdana, Arial, Helvetica, sans-serif;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#E1E1E1; color:#333333;table-layout: fixed;">
  <tr>
    <td>


<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#000000; color:#FFFFFF; padding:15px;table-layout: fixed;">
  <tr>
    <td style="text-align:left; width:50%;"><a href="'.base_url().'" target="_blank"><img alt="Privicarte.fr" src="'.GetImagePath("privicarte/").'/logo.png" width="100%" style="border:none;"/></a></td>
    <td>
    <p style="text-align:right;"><a href="https://www.facebook.com/privicarte/" target="_blank"><img alt="facebook Privicarte" src="'.GetImagePath("privicarte/").'/fb_nws.png" style="padding:5px; border:none;"/></a><a href="https://twitter.com/Privicartefr" target="_blank"><img alt="twitter Privicarte" src="'.GetImagePath("privicarte/").'/twt_nws.png" style="padding:5px; border:none;"/></a></p>
    <p style="font-family: Arial, Helvetica, sans-serif;text-transform: uppercase;color: #FFFFFF;background-color: #000000; text-align:right;font-size: 22px;font-weight: bold;color: #FFFFFF;">ALPES-MARITIMES &amp; MONACO</p>
    </td>
  </tr>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding:20px 0;table-layout: fixed;">
  <tr>
    <td style="	text-transform: uppercase;	color: #333333;	text-align: center;"><span style="font-size: 30px;">Notre sélection de bon plans </span><span style="font-size: 24px;">'.translate_date_to_fr(date('Y-m-d')).'</span></td>
  </tr>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding:15px 15px 15px 0px; float:left;table-layout: fixed;">

	<tr><td>

';

        $iii = 1;
        $jjj = count($toListeBonPlan);
        foreach($toListeBonPlan as $oListeBonPlan){

            if ($oListeBonPlan->bonplan_photo1 != "" && file_exists("application/resources/front/images/".$oListeBonPlan->bonplan_photo1)==true){
                $main_photo_link_partner_plan =  base_url()."application/resources/front/images/".$oListeBonPlan->bonplan_photo1 ;
            }else{
                $main_photo_link_partner_plan = GetImagePath("front/")."/wp71b211d2_06.png";
            }
            if ($oListeBonPlan->bon_plan_utilise_plusieurs == '1') {
                $main_limite_offre = 'Offre <br/>illimit&eacute;e';
            } else if ($oListeBonPlan->bonplan_nombrepris != '0') {
                $main_limite_offre = 'Il resteà ce jour <br/>'.$oListeBonPlan->bonplan_nombrepris.' bonplans';
            }

            //<!-- debut boucle bonplan-->
            
           
            
            $data['mail_content_newsletter'] .= '

  

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding:15px 0px 0px 15px;"><tr><td>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="height:320px;table-layout: fixed; background-color:#FFFFFF;">
  <tr>
    <td style="height:320px; text-align:center; vertical-align:top; overflow:hidden;text-overflow: ellipsis; white-space: nowrap;"><a href="'.site_url($oListeBonPlan->nom_url.'/notre_bonplan').'" target="_blank"><img alt="Bonplan" src="'.$main_photo_link_partner_plan.'" width="100%" height="320" style="border:none;"/></a></td>
  </tr>
</table>
</td></tr></table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding:0px 0px 0px 15px;"><tr><td>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#FFFFFF; padding:15px;table-layout: fixed; height:143px;">
  <tr>
    <td style="width:100%;">
    	<span style="margin:0px !important; font-weight:bold; color:#E553B0 !important;">'.$oListeBonPlan->bonplan_titre.'</span><br/>
    	<span style="margin:0px !important;">'.$oListeBonPlan->NomSociete.'</span> - 
    	<span style="margin:0px !important;">'.$oListeBonPlan->Nom.'</span><br/>
    	<span style="margin:0px !important;">'.$oListeBonPlan->ville.'</span><br/>
    </td>
    
  </tr>
</table>
</td></tr></table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding:0px 0px 0px 15px;"><tr><td>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#FFFFFF; padding:15px;table-layout: fixed; height:143px;">
  <tr>
    <td style="width:20%;"><img src="'.GetImagePath("front/").'/wpimages2013/nv_31_bonplan.png" alt="Expire" style=" border:none;"/></td>
    <td style="width:60%;">Expire le<br/>'.translate_date_to_fr($oListeBonPlan->bonplan_date_fin).'</td>
    <td></td>
  </tr>
  <tr>
    <td style="width:20%;"><img src="'.GetImagePath("front/").'/wpimages2013/wp9e09b9c6_06.png" alt="status" style=" border:none;"/></td>
    <td style="width:60%;">'.$main_limite_offre.'</td>
    <td style="width:20%;text-align: right;">
    	<a href="'.site_url($oListeBonPlan->nom_url.'/notre_bonplan').'" target="_blank"><img src="'.GetImagePath("privicarte/").'/mobile/plus_mobile.png" alt="Details"/></a>
    </td>
  </tr>
</table>
</td></tr></table>


    

';//<!-- fin boucle bonplan-->


		if ($iii % 2 != 0 && $iii < $jjj) $data['mail_content_newsletter'] .= '</td><td>';
		else if ($iii % 2 == 0 && $iii < $jjj) $data['mail_content_newsletter'] .= '</td></tr><tr><td>';
		else if ($iii >= $jjj) $data['mail_content_newsletter'] .= '</td></tr>';
		else $data['mail_content_newsletter'] .= '</td></tr>';
		

		$iii=$iii+1; 
        }


        $data['mail_content_newsletter'] .= '


</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:center; padding:50px;table-layout: fixed;">
  <tr>
    <td><a href="'.site_url('front/bonplan').'" target="_blank" style="background-color: #000000;color: #ffffff;font-size: 24px;padding: 15px 30px;-moz-user-select: none;
    border: 1px solid transparent;
    border-radius: 4px;
    cursor: pointer;
    display: inline-block;
    font-weight: 400;
    line-height: 1.42857;
    margin-bottom: 0;
    text-align: center;
    vertical-align: middle;
    text-decoration:none;
    white-space: nowrap;">Voir tous les bons plans</a></td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding:15px; text-align:center; margin-bottom:300px;table-layout: fixed;">
  <tr>
    <td>
        <p>Vous recevez le présent e-mail parce que l\'adresse <a href="mailto:jpwoignier@gmail.com" target="_blank">jpwoignier@gmail.com</a> figure dans la liste de diffusion de Privicarte.</p>
        <p>Si vous ne souhaitez pas recevoir d\'emails de ce type, vous pouvez vous désincrire en cliquant <a href="#">ici</a>.</p>
        <p>Envoyé par Privicarte SAS sis au 11 allée des Villas Fleuries au Cros de Cagnes</p>
        <p>Besoin d’aide? Un commentaire à nous faire part? N’hésitez pas à <a href="'.base_url().'">nous contacter</a>.</p>
        <p>&nbsp;</p>
        <p>Visualiser le mail dans un navigateur <a href="'.site_url("front/utilisateur/newsletter").'" target="_blank">ICI</a></p>
    </td>
  </tr>
</table>



    </td>
  </tr>
</table>

</div>



 ';


      
        ///fin template newsletter **************************************************************************************************



        $this->load->view("admin/vwAccueilNewsletter", $data);
    }
    
    
    
    
    
    function envoyer() {
        //die("stop");
        $sendonemail = $this->input->post("sendonemail");
        $sendallnews = $this->input->post("sendallnews");
        $txtContenu = $this->input->post("txtContenu");
        
        //$txtContenu = str_replace('src="', 'src="http://www.club-proximite.com', $txtContenu);
        //$txtContenu = str_replace('href="', 'href="http://www.club-proximite.com', $txtContenu);
        
        
        if ($sendallnews=="" || $sendallnews == NULL || $txtContenu=="" || $txtContenu==NULL){
            redirect('admin/newsletters');
        }
        $data = null;
        
        $this->load->model("user");
        $colUsers = $this->user->GetAllValidUserList_test();
        $txtSujet = "Privicarte - Newsletter";
        
        
        if ($sendallnews==1) {
            
            $nb_mail_sent = 0;
            $nb_mail_error = 0;
            foreach($colUsers as $objUser) {
                $colDest = array();
                $colDest[] = array("Email" => $objUser->Email, "Name" => $objUser->Prenom . " " . $objUser->Nom);

                if(envoi_notification($colDest, $txtSujet, $txtContenu)) {
                    //$data['mssgerrornews'] = "Emails sont envoy&eacute;s<br/>";
                    ++$nb_mail_sent;
                } else {
                    //$data['mssgerrornews'] = "Une erreur s'est produite lors de l'envoi des mails<br/>";
                    ++$nb_mail_error;
                }
            }
            $data['nb_mail_sent'] = $nb_mail_sent;
            $data['nb_mail_error'] = $nb_mail_error;
            
        } else if ($sendallnews==2) {
            $colDest[] = array("Email" => $sendonemail, "Name" => "Email_Club");
            if(envoi_notification($colDest, $txtSujet, $txtContenu)) $data['mssgerrornews'] = "Email envoy&eacute; !";
        }
        
        
        $this->load->view("admin/vwAccueilNewsletterConfirmation", $data);
    }
}
