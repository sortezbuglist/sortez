<?php
class home extends CI_Controller{
    
    function __construct() {
        parent::__construct();

		$this->load->library('session');
        $this->load->library('ion_auth');

        $this->load->model("mdl_agenda");
        $this->load->helper('clubproximite');


        if (!$this->ion_auth->is_admin()) {
			$this->session->set_flashdata('domain_from', '1');
			redirect("connexion");
		}
    }
    
    function index(){

        
        $data["no_main_menu_home"] = "1";
        $data["no_left_menu_home"] = "1";

        //this function delete storage cache directory
        //$base_path_system_rand = str_replace('system/', '', BASEPATH);
        //$validation_delete_path_rand = delete_directory_rand($base_path_system_rand."/pagecaracteristics");
        //this function delete storage cache directory

        ////////////////DELETE OLD AGENDA date_fin past 8 days
        $this->mdl_agenda->deleteOldAgenda_fin8jours();

        $this->load->view("admin/vwHome", $data);
      
    }
}