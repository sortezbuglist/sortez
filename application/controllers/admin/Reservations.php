<?php
class Reservations extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("mdlcommercant") ;
        $this->load->model("mdlville") ;
        $this->load->model("mdlbonplan") ;
        $this->load->model("mdlbonplans") ;
        $this->load->model("mdlcadeau") ;

        $this->load->model("mdlannonce") ;
        $this->load->model("rubrique") ;
        $this->load->model("mdlimagespub") ;

        $this->load->model("Abonnement");
        $this->load->library('user_agent');

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");

        $this->load->model("Mdl_plat_du_jour");
        $this->load->model("Mdl_card");

        $this->load->library('session');
        $this->load->helper('clubproximite');
        $this->load->model('Mdl_reservation');

        check_vivresaville_id_ville();
    }
    public function index(){
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $_iCommercantId = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($_iCommercantId == null || $_iCommercantId == 0 || $_iCommercantId == "") {
                $_iCommercantId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
        $data['infocom'] = $oInfoCommercant ;
        $data['liste_gite']=$this->Mdl_reservation->get_reservation_gite_by_idcom($_iCommercantId);
        $data['idCommercant']=$_iCommercantId;
        $this->load->view('reservations/reervation_gite',$data);
    }
    }

    public function index_res_form(){
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $_iCommercantId = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($_iCommercantId == null || $_iCommercantId == 0 || $_iCommercantId == "") {
                $_iCommercantId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
            $data['infocom'] = $oInfoCommercant ;
            $data['liste_gite']=$this->Mdl_reservation->get_reservation_gite_by_idcom($_iCommercantId);
            $data['idCommercant']=$_iCommercantId;
            $this->load->view('reservations/reervation_gite_res_form',$data);
        }
    }

    public function ficheGite($id_gite=0,$idcom=0){
        if ($id_gite !=0){
            $data['infogite']=$this->Mdl_reservation->get_Gite_by_id($id_gite);
        }else{
            $data['infogite']=null;
        }
        $data['idcom']=$idcom;
        $this->load->view('reservations/vwFicheGite',$data);
    }
    public function savegite(){
        $id=$this->input->post('id_gite');
        $idcom=$this->input->post('idcommercant');
        $Is_activ_res_form=$this->input->post('Is_activ_res_form');
        $Is_activ_res_link=$this->input->post('Is_activ_res_link');
        $link_res=$this->input->post('link_res');
            $field=array(
                "id"=>$id ?? 0,
                "IdCommercant"=>$idcom,
                "isActif_link_gite"=>$Is_activ_res_link,
                "url"=>$link_res,
                "IsActif_form_gite"=>$Is_activ_res_form
            );


    if (isset($id) AND $id !=0 AND $id !='' AND $id !=null ){
        $save=$this->Mdl_reservation->update_gite($field);
    }else{
        $save= $this->Mdl_reservation->save_gite($field);
    }
   if ($save=='1'){echo 'ok';}
    else{
        echo 'ko';
        }
    }
    public function delete_gite($id){
        $this->Mdl_reservation->delete_gite($id);
        redirect('admin/Reservations');
    }
}