<?php
class Categories_agenda extends CI_Controller{

    function __construct() {
        parent::__construct();

        $this->load->library('session');
        $this->load->library('ion_auth');
        $this->load->model("mdl_categories_agenda");
        $this->load->model("mdl_types_agenda");
        $this->load->model("ion_auth_used_by_club");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }

    function index(){
        $this->liste();
    }

    function categories(){
        $this->liste();
    }

    function liste() {
        $toListeCategorie = $this->mdl_categories_agenda->GetAll() ;
        $data['toListeCategorie'] = $toListeCategorie ;
        if($this->session->flashdata('mess_editcategorie')=='1') $data['mess_editcategorie'] = '<strong style="color:#060">Catégorie enregistrée !</strong>';
        if($this->session->flashdata('mess_editcategorie')=='2') $data['mess_editcategorie'] = '<strong style="color:#F00">Cette catégorie ne peut être supprimée ! catégorie liée à un agenda !</strong>';
        if($this->session->flashdata('mess_editcategorie')=='3') $data['mess_editcategorie'] = '<strong style="color:#060">Catégorie supprimée !</strong>';
        $this->load->view("admin/vwCategoryAgenda", $data);
    }



    function edit_categories_agenda(){
        $agenda_categid = $this->uri->rsegment(3);

        //echo $nom_url_commercant;
        $oCategorie = $this->mdl_categories_agenda->GetById($agenda_categid) ;
        $data['oCategorie'] = $oCategorie ;
        $data['oTypeagenda'] = $this->mdl_types_agenda->GetAll() ;
        //$data['ocodename'] = $this->mdl_types_agenda->GetAllcodename() ;
        //$this->firephp->log($oCategorie, 'oCategorie');
        if(is_numeric(trim($agenda_categid))) {
            $this->load->view("admin/vwEditCategoryagenda", $data);
        }
        else {
            redirect('admin/categories_agenda');
        }

    }

    function insert_categories_agenda(){
        $data['oCategorie'] = NULL;
        $data['oTypeagenda'] = $this->mdl_types_agenda->GetAll() ;
        //$this->firephp->log($data['oTypeagenda'], 'oTypeagenda');
        //$data['ocodename'] = $this->mdl_types_agenda->GetAllcodename();
        $this->load->view("admin/vwEditCategoryagenda", $data);
    }

    function insertsouscategorie(){
        $IdRubrique = $this->uri->rsegment(3);
        $toCategorie = $this->mdl_categories_agenda->GetById($IdRubrique) ;
        //$toCategorie = $this->mdl_categories_agenda->GetByIdcodename($IdRubrique) ;
        $data['toCategorie'] = $toCategorie ;
        $this->load->view("admin/vwEditSousCategoryagenda", $data);
    }


    function savecategorie(){
        $idrubrique_editcategorie = $this->input->post("idrubrique_editcategorie");
        $ocategorie['agenda_categid'] = $idrubrique_editcategorie;
        $agenda_typeid = $this->input->post("agenda_typeid");
        $ocategorie['agenda_typeid'] = $agenda_typeid;
        //$agenda_typeid = $this->input->post("codenameid");
        //$ocategorie['codename'] = $agenda_typeid;

        $inputcateg_editcategorie = $this->input->post("inputcateg_editcategorie");
        $ocategorie['category'] = $inputcateg_editcategorie;

        if (isset($_FILES["inputcateg_images"]["name"])) $photo1 =  $_FILES["inputcateg_images"]["name"];
        if(isset($photo1) && $photo1 !=""){
            $photo1Associe = $photo1;
            $AgendaPhoto1 = doUploadResize("inputcateg_images","application/resources/front/images/agenda/category/",$photo1Associe);
            if (file_exists("application/resources/front/images/agenda/category/".$AgendaPhoto1)== true) {
                image_thumb("application/resources/front/images/agenda/category/" .$AgendaPhoto1, 100, 100,'','');
            }
        }else {
            $photo1Associe = $this->input->post("imagesAssocie");
            $AgendaPhoto1 = $photo1Associe;
        }

        $ocategorie["images"] = $AgendaPhoto1;


        if ($idrubrique_editcategorie=="0") $this->mdl_categories_agenda->Insert($ocategorie);
        else $this->mdl_categories_agenda->Update($ocategorie);

        $this->session->set_flashdata('mess_editcategorie', '1');
        redirect('admin/categories_agenda');

    }

    function delete_categories_agenda(){
        $IdRubrique = $this->uri->rsegment(3);

        if(is_numeric(trim($IdRubrique))) {
            //verify if category contain active agenda********
            $oCategorie = $this->mdl_categories_agenda->verifier_categorie_agenda($IdRubrique) ;
            //$oCategorie = array();
            if (count($oCategorie)==0){
                $this->mdl_categories_agenda->Delete($IdRubrique);
                $this->session->set_flashdata('mess_editcategorie', '3');
            } else {
                $this->session->set_flashdata('mess_editcategorie', '2');
            }

            //$this->firephp->log($oCategorie, 'oCategorie');

            redirect('admin/categories_agenda/');
        }
        else {
            redirect('admin/categories_agenda/');
        }

    }



    function souscategorie(){
        $IdRubrique = $this->uri->rsegment(3);

        if(is_numeric(trim($IdRubrique))) {
            $data["no_main_menu_home"] = "1";
            $data["no_left_menu_home"] = "1";

            $toListeSousCategorie = $this->mdl_categories_agenda->GetAllSousrubriqueByRubrique($IdRubrique) ;
            $toCategorie = $this->mdl_categories_agenda->GetById($IdRubrique) ;
            $data['toListeSousCategorie'] = $toListeSousCategorie ;
            $data['toCategorie'] = $toCategorie ;

            if($this->session->flashdata('mess_editsouscategorie')=='1') $data['mess_editsouscategorie'] = '<strong style="color:#060">SousCatégorie enregistrée !</strong>';
            if($this->session->flashdata('mess_editsouscategorie')=='2') $data['mess_editsouscategorie'] = '<strong style="color:#F00">Cette SousCatégorie ne peut être supprimée ! catégorie liée à un partenaire !</strong>';
            if($this->session->flashdata('mess_editsouscategorie')=='3') $data['mess_editsouscategorie'] = '<strong style="color:#060">SousCatégorie supprimée !</strong>';

            $this->load->view("admin/vwSousCategoryagenda", $data);

        }
        else {
            redirect('admin/categories_agenda');
        }
    }


    function modifiersouscategorie(){
        $IdSousRubrique = $this->uri->rsegment(3);
        $IdRubrique = $this->uri->rsegment(4);

        //echo $nom_url_commercant;
        $oSousCategorie = $this->mdl_categories_agenda->getByIdSousCateg($IdSousRubrique);
        $data['oSousCategorie'] = $oSousCategorie ;
        $toCategorie = $this->mdl_categories_agenda->getById($IdRubrique) ;
        $data['toCategorie'] = $toCategorie ;

        //$this->firephp->log($oSousCategorie, 'oSousCategorie');

        if(is_numeric(trim($IdSousRubrique))) {
            $this->load->view("admin/vwEditSousCategoryagenda", $data);
        }
        else {
            redirect('admin/categories_agenda/souscategorie/'.$IdRubrique);
        }
    }


    function savesouscategorie(){
        $idrubrique_editcategorie = $this->input->post("idrubrique_editcategorie");
        $ocategorie['agenda_categid'] = $idrubrique_editcategorie;
        $idrubrique_editsouscategorie = $this->input->post("idrubrique_editsouscategorie");
        $ocategorie['agenda_subcategid'] = $idrubrique_editsouscategorie;
        $inputcateg_editsouscategorie = $this->input->post("inputcateg_editsouscategorie");
        $ocategorie['subcateg'] = $inputcateg_editsouscategorie;

        if ($idrubrique_editsouscategorie=="0") $this->mdl_categories_agenda->InsertSousCateg($ocategorie);
        else $this->mdl_categories_agenda->UpdateSousCateg($ocategorie);

        $this->session->set_flashdata('mess_editsouscategorie', '1');
        redirect('admin/categories_agenda/souscategorie/'.$idrubrique_editcategorie);

    }


    function supprimersouscategorie(){
        $IdSousRubrique = $this->uri->rsegment(3);
        $IdRubrique = $this->uri->rsegment(4);
        //$this->firephp->log($IdSousRubrique, 'IdSousRubrique');
        //$this->firephp->log($IdRubrique, 'IdRubrique');

        if(is_numeric(trim($IdSousRubrique))) {
            //verify if subcateg contains agenda
            $oSousCategorie = $this->mdl_categories_agenda->verifier_souscategorie_agenda($IdSousRubrique) ;
            //$oSousCategorie = array();
            if (count($oSousCategorie)==0){
                $this->mdl_categories_agenda->DeleteSousCateg($IdSousRubrique) ;
                $this->session->set_flashdata('mess_editsouscategorie', '3');
            } else {
                $this->session->set_flashdata('mess_editsouscategorie', '2');
            }

            //$this->firephp->log(count($oSousCategorie), 'oCategorie');

            redirect('admin/categories_agenda/souscategorie/'.$IdRubrique);
        }
        else {
            redirect('admin/categories_agenda');
        }

    }


}