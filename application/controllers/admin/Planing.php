<?php
class planing extends CI_Controller{

    function __construct() {
        parent::__construct();

        $this->load->library('session');
        $this->load->library('ion_auth');
        $this->load->model("packarticle_planing");
        $this->load->model("ion_auth_used_by_club");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }

    function index(){
        $this->liste();
    }

     function liste() {
        $objPlan = $this->packarticle_planing->GetAll();
        $data["colPlan"] = $objPlan;

        if($this->session->flashdata('mess_editPlan')=='1') $data['msg'] = '<strong style="color:#060">planing enregistré !</strong>';
        if($this->session->flashdata(' mess_editPlan')=='2') $data['msg'] = '<strong style="color:#F00">Cette planing ne peut être supprimé ! planing lié à une catégorie !</strong>';
        if($this->session->flashdata(' mess_editPlan')=='3') $data['msg'] = '<strong style="color:#060">Planing supprimé !</strong>';

        $this->load->view("packarticle/admin/vwListeplaning",$data);
    }

    function fiche_planing($IdPlan) {

        if ($IdPlan != 0){
            $data["title"] = "Modification  planing" ;
            $data["oPlan"] = $this->packarticle_planing->getById($IdPlan) ;
            $data["id"] = $IdPlan;
            $this->load->view('packarticle/admin/vwFicheplaning', $data) ;

        }else{
            $data["title"] = "Creer une planing" ;
            $this->load->view('packarticle/admin/vwFicheplaning', $data) ;
        }
    }

    function creer_planing(){
        $objPlan = $this->input->post("planing") ;
        $this->packarticle_planing->insertpackarticle_planing($objPlan);
        $data["msg"] = "Planing ajouté" ;

        $objPlan = $this->packarticle_planing->GetAll();
        $data["colPlan"] = $objPlan;
        $this->load->view("packarticle/admin/vwListeplaning",$data);
    }

    function modif_planing($IdPlan){
        $objPlan = $this->input->post("planing");
        $this->packarticle_planing->updatepackarticle_planing($objPlan);
        $data["msg"] = "Planing enregistré" ;

        $objPlan = $this->packarticle_planing->GetAll();
        $data["colPlan"] = $objPlan;
        $this->load->view("packarticle/admin/vwListeplaning",$data);
    }

    
    function delete($prmId) {

        if(is_numeric(trim($prmId))) {
            //verify if category contain active article********
            $oCategorie = $this->packarticle_planing->verifierpackarticle_planing($prmId) ;
            //$oCategorie = array();
            if (count($oCategorie)==0){
                $this->packarticle_planing->supprimepackarticle_planing($prmId);
                $this->session->set_flashdata('mess_editPlan', '3');
            } else {
                $this->session->set_flashdata('mess_editPlan', '2');
            }

            //$this->firephp->log($oCategorie, 'oCategorie');

            redirect("packarticle/admin/planing");
        }
        else {
            redirect("packarticle/admin/planing");
        }

    }


}
    

