<?php
class commercants extends CI_Controller{

    private $mCurrentFilter = 2; // Le type de filtre en cours (0 : exact, 1 : d?but, 2 : milieu)
    private $mSearchValue = ""; // La valeur ? rechercher
    private $mLinesPerPage = 50 ;// Le nombre de lignes (enregistrements) ? afficher par page
    
    function __construct() {
        parent::__construct();
        $this->load->Model("Commercant");
        $this->load->Model("Rubrique");
        $this->load->Model("SousRubrique");
        $this->load->Model("Ville");
        $this->load->Model("mdlville");
        $this->load->Model("mdldepartement");
        $this->load->Model("Abonnement");
        $this->load->Model("AssCommercantRubrique");
        $this->load->Model("AssCommercantSousRubrique");
        $this->load->Model("AssCommercantAbonnement");
        $this->load->Model("mdldemande_abonnement");

        $this->load->Model("mdlbonplan");
        $this->load->Model("mdlannonce");
        $this->load->Model("mdl_agenda");
        $this->load->Model("mdlagenda_perso");
		
        $this->load->model("mdlcategorie");
        
        $this->load->library('session');

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_model");
        $this->load->model("ion_auth_used_by_club");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }
    
    function index(){
        $this->listeDerniereInscrit();
    }


    public function GetCurrentFilter() {
        if (isset($_POST["optTypeFiltre"])) {
            return $_POST["optTypeFiltre"] ;
        } else {
            if ($this->session->userdata("curFilter")== null) {
                return 2;
            }
            return $this->session->userdata("curFilter") ;
        }
    }

    public function SetCurrentFilter($argValue) {
        $this->mCurrentFilter = $argValue ;
        $this->session->set_userdata("curFilter",$argValue) ;
    }

    public function GetSearchValue() {
        $SearchValue ;
        if (isset($_POST["txtSearch"])) {
            $SearchValue = $_POST["txtSearch"] ;
        } else {
            if ($this->session->userdata("SearchValue")== null) {
                $SearchValue = "";
            }
            $SearchValue = $this->session->userdata("SearchValue");
        }

        return addslashes($SearchValue) ;
    }
    public function SetSearchValue($argValue) {
        $this->mSearchValue = $argValue ;
        $this->session->set_userdata("SearchValue",$argValue) ;
    }

    public function GetLinesPerPage() {
        if (isset($_POST["cmbNbLignes"])) {
            return  $_POST["cmbNbLignes"];
        } else {
            if ($this->session->userdata("LinesPerPage")== null) {
                return 50;
            }
            return $this->session->userdata("LinesPerPage");
        }
    }

    public function SetLinesPerPage($argValue) {
        $this->mLinesPerPage = $argValue ;
        $this->session->set_userdata("LinesPerPage",$argValue) ;
    }
    
    public function GetOrder() {
        if (isset($_POST["hdnOrder"])) {
            return $_POST["hdnOrder"] ;
        } else {
            if ($this->session->userdata("UserOrder")== null) {
                return "";
            }

            //$reculst = $this-commercant-GetWhere_pvc(" IdCommercant='300432' ", 0, 100000);
            return $this->session->userdata("UserOrder") ;
        }
    }

    public function SetOrder($argValue) {
        $this->session->set_userdata("UserOrder",$argValue) ;
    }
    
    function _clearAllSessionsData() {
        $this->SetCurrentFilter(2);
        $this->SetSearchValue("");
        $this->SetLinesPerPage(50);
        $this->SetOrder("");
    }    



    
    /*function liste() {
        $objCommercants = $this->Commercant->GetAll();
        $data["colCommercants"] = $objCommercants;
        $this->load->view("admin/vwListeCommercants",$data);
    }*/


    /**
     * Fontion utilis?e pour l'affichage de la liste des users (avec pagination)
     *
     * @param unknown_type $prmPagerIndex
     * @param unknown_type $prmIsCompleteList
     * @param unknown_type $prmOrder
     */
    public function liste($prmFilterCol = "-1", $prmFilterValue = "0", $prmPagerIndex = 0, $prmIsCompleteList = false, $prmOrder = "") {
        $_SESSION["User_FilterCol"] = $prmFilterCol;
        $_SESSION["User_FilterValue"] = $prmFilterValue;
        
        if ($prmIsCompleteList) {
            $this->_clearAllSessionsData();
        }

        if (isset($_POST["cmbNbLignes"])) {
            $this->SetLinesPerPage($_POST["cmbNbLignes"]);
        }

        if (isset($_POST["txtSearch"])) {
            $this->SetSearchValue($_POST["txtSearch"]);
        }

        if (isset($_POST["optTypeFiltre"])) {
           $this->SetCurrentFilter($_POST["optTypeFiltre"]);
        }
        
        if (isset($_POST["hdnOrder"])) {
            $this->SetOrder($_POST["hdnOrder"]);
        }
        
        $SearchedWordsString = str_replace("+", " ", $this->GetSearchValue()) ;
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString) ;
        $WhereKeyWords = " 0=0 " ;
        
               
        for ($i = 0; $i < sizeof($SearchedWords); $i ++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search) ;
            if ($Search != "") {
                if ($i == 0) {
                    $WhereKeyWords .= " AND " ;
                }
                                
                $WhereKeyWords .= " ( " .
                    " UPPER(commercants.IdCommercant ) LIKE '%" . strtoupper($Search) . "%'" . 
                    " OR UPPER(commercants.Nom ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.Prenom ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.Email ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.Email_decideur ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.Adresse1 ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.Adresse2 ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.CodePostal ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.page_web_marchand ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.google_plus ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.adresse_localisation ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.SiteWeb ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.titre_entete ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.Facebook ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.Twitter ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'" .
                " )
                ";
                if ($i != (sizeof($SearchedWords) - 1)){
                    $WhereKeyWords .= " OR ";
                }
            }
        }
        
        // Pre-filtre sur une table parent (cas d'une liste fille dans une fiche maitre-detail)
        if ($prmFilterCol != "-1") {
            $WhereKeyWords .= " AND " . $prmFilterCol . " = '" . $prmFilterValue . "'" ;
        }

        
        
        // echo $WhereKeyWords; exit;
        //$objCommercants = $this->Commercant->GetAll();        
        $colusers = $this->Commercant->GetWhere_pvc($WhereKeyWords, $prmPagerIndex, $this->GetLinesPerPage(), $this->GetOrder());
        
        
        // Pagination (CI)
        $this->load->library("pagination");
        $this->config->load("config");
        $pagination_config['uri_segment'] = '5';
        $data["UriSegment"] = $pagination_config['uri_segment'];
        // ito no tena izy $pagination_config["base_url"] = site_url() . "admin/commercants/liste/" . $prmFilterCol . "/" . $prmFilterValue ;
        $pagination_config["base_url"] = site_url() . "admin/commercants/liste" . $prmFilterCol . "/" . $prmFilterValue ;
        $pagination_config["total_rows"] = $this->Commercant->CountWhere_pvc($WhereKeyWords);
        $pagination_config["per_page"] = $this->GetLinesPerPage();
        $pagination_config["first_link"] = '&lsaquo;';
        $pagination_config["last_link"] = '&raquo';
        $pagination_config["next_link"] = '&gt;';
        $pagination_config["prev_link"] = '&lt;';
        $pagination_config["cur_tag_open"] = "<span style='font-weight: bold; padding-right: 5px; padding-left: 5px; '>" ;
        $pagination_config["cur_tag_close"] = "</span>" ;
        $pagination_config["num_tag_open"] = "&nbsp;<span>" ;
        $pagination_config["num_tag_close"] = "&nbsp;</span>" ;

        $this->pagination->initialize($pagination_config);
        // Effectuer un highlight des mots-cles recherches dans la liste des enregistrements trouv&s
        $data["highlight"] = "";
        if ($this->GetSearchValue() != "") {
            $data["highlight"] = "yes";
        } else {
            $data["highlight"] = "";
        }
        
        // Les numeros de page
        $NumerosPages = array();
        //$NumerosPages[] = 0;
        $n = 0;
        while($n < $pagination_config["total_rows"] - 1) {
            $NumerosPages[] = $n;
            $n += $pagination_config["per_page"];
        }
        $data["NumerosPages"] = $NumerosPages;
        
        
        // Preparer les variables pour le view
        $data["colUsers"] = $colusers ;

        $data["PaginationLinks"] = $this->pagination->create_links();
        $data["NbLignes"] = $this->GetLinesPerPage() ;
        $data["CurFiltre"] = $this->GetCurrentFilter();
        $data["SearchValue"] = $this->GetSearchValue();
        $data["SearchedWords"] = $SearchedWords;
        $data["Keywords"] = $this->GetSearchValue();
        $data["CountAllResults"] = $pagination_config["total_rows"];
        $data["Ordre"] = $this->GetOrder();
        
        $DepartCount = $prmPagerIndex;
        $data["DepartCount"] =  $DepartCount;
        
        // Data pour le pre-filtre
        $data["FilterCol"] = $prmFilterCol; 
        $data["FilterValue"] = $prmFilterValue;
        
        $data["no_main_menu_home"] = "1";

        $data["ion_auth_used_by_club"] = $this->ion_auth_used_by_club;
        $data["ion_auth"] = $this->ion_auth;
        
        //$this->load->view("admin/vwUsersadmin", $data) ;
        $this->load->view("admin/vwListeCommercants", $data) ;
    }

    //liste commercant 10 dernière inscrit par mamitiana
    //mila ts miala eo amin page dernier inscrit mits
    public function listeDerniereInscrit($prmFilterCol = "-1", $prmFilterValue = "0", $prmPagerIndex = 0, $prmIsCompleteList = false, $prmOrder = "") {
        $_SESSION["User_FilterCol"] = $prmFilterCol;
        $_SESSION["User_FilterValue"] = $prmFilterValue;
        
        if ($prmIsCompleteList) {
            $this->_clearAllSessionsData();
        }

        if (isset($_POST["cmbNbLignes"])) {
            $this->SetLinesPerPage($_POST["cmbNbLignes"]);
        }

        if (isset($_POST["txtSearch"])) {
            $this->SetSearchValue($_POST["txtSearch"]);
        }

        if (isset($_POST["optTypeFiltre"])) {
           $this->SetCurrentFilter($_POST["optTypeFiltre"]);
        }
        
        if (isset($_POST["hdnOrder"])) {
            $this->SetOrder($_POST["hdnOrder"]);
        }
        
        $SearchedWordsString = str_replace("+", " ", $this->GetSearchValue()) ;
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString) ;
        $WhereKeyWords = " 0=0 " ;
        
               
        for ($i = 0; $i < sizeof($SearchedWords); $i ++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search) ;
            if ($Search != "") {
                if ($i == 0) {
                    $WhereKeyWords .= " AND " ;
                }
                                
                $WhereKeyWords .= " ( " .
                    " UPPER(commercants.IdCommercant ) LIKE '%" . strtoupper($Search) . "%'" . 
                    " OR UPPER(commercants.Nom ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.Prenom ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.Email ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.Email_decideur ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.Adresse1 ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.Adresse2 ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.CodePostal ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.page_web_marchand ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.google_plus ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.adresse_localisation ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.SiteWeb ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.titre_entete ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.Facebook ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(commercants.Twitter ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'" .
                " )
                ";
                if ($i != (sizeof($SearchedWords) - 1)){
                    $WhereKeyWords .= " OR ";
                }
            }
        }
        
        // Pre-filtre sur une table parent (cas d'une liste fille dans une fiche maitre-detail)
        if ($prmFilterCol != "-1") {
            $WhereKeyWords .= " AND " . $prmFilterCol . " = '" . $prmFilterValue . "commercants.Nom ASC" ;
        }

        
        
        // echo $WhereKeyWords; exit;
        //$objCommercants = $this->Commercant->GetAll();        
        //$colusers = $this->Commercant->listeDerniereInscrit($WhereKeyWords, $prmPagerIndex, $this->GetLinesPerPage(), $this->GetOrder());
        $colusers = $this->Commercant->listeDerniereInscrit("",'1','50', "idcommercant DESC");
        //var_dump($colusers);die();
        
        // Pagination (CI)
        $this->load->library("pagination");
        $this->config->load("config");
        $pagination_config['uri_segment'] = '5';
        $data["UriSegment"] = $pagination_config['uri_segment'];
       // ito no tena izy $pagination_config["base_url"] = site_url() . "admin/commercants/liste/" . $prmFilterCol . "/" . $prmFilterValue ;
        $pagination_config["base_url"] = site_url() . "admin/commercants/listeDerniereInscrit/" . $prmFilterCol . "/" . $prmFilterValue ;
        $pagination_config["total_rows"] = $this->Commercant->CountWhere_pvc($WhereKeyWords);
        $pagination_config["per_page"] = $this->GetLinesPerPage();
        $pagination_config["first_link"] = '&lsaquo;';
        $pagination_config["last_link"] = '&raquo';
        $pagination_config["next_link"] = '&gt;';
        $pagination_config["prev_link"] = '&lt;';
        $pagination_config["cur_tag_open"] = "<span style='font-weight: bold; padding-right: 5px; padding-left: 5px; '>" ;
        $pagination_config["cur_tag_close"] = "</span>" ;
        $pagination_config["num_tag_open"] = "&nbsp;<span>" ;
        $pagination_config["num_tag_close"] = "&nbsp;</span>" ;

        $this->pagination->initialize($pagination_config);
        // Effectuer un highlight des mots-cles recherches dans la liste des enregistrements trouv&s
        $data["highlight"] = "";
        if ($this->GetSearchValue() != "") {
            $data["highlight"] = "yes";
        } else {
            $data["highlight"] = "";
        }
        
        // Les numeros de page
        $NumerosPages = array();
        //$NumerosPages[] = 0;
        $n = 0;
        while($n < $pagination_config["total_rows"] - 1) {
            $NumerosPages[] = $n;
            $n += $pagination_config["per_page"];
        }
        $data["NumerosPages"] = $NumerosPages;
        
        
        // Preparer les variables pour le view
        $data["colUsers"] = $colusers ;

        $data["PaginationLinks"] = $this->pagination->create_links();
        $data["NbLignes"] = $this->GetLinesPerPage() ;
        $data["CurFiltre"] = $this->GetCurrentFilter();
        $data["SearchValue"] = $this->GetSearchValue();
        $data["SearchedWords"] = $SearchedWords;
        $data["Keywords"] = $this->GetSearchValue();
        $data["CountAllResults"] = $pagination_config["total_rows"];
        $data["Ordre"] = $this->GetOrder();
        
        $DepartCount = $prmPagerIndex;
        $data["DepartCount"] =  $DepartCount;
        
        // Data pour le pre-filtre
        $data["FilterCol"] = $prmFilterCol; 
        $data["FilterValue"] = $prmFilterValue;
        
        $data["no_main_menu_home"] = "1";

        $data["ion_auth_used_by_club"] = $this->ion_auth_used_by_club;
        $data["ion_auth"] = $this->ion_auth;
        
        //$this->load->view("admin/vwUsersadmin", $data) ;
        $this->load->view("admin/vwListeDerniereInscrit", $data) ;
    }
        


    
    function fiche($prmIdCommercant = 0) {
        $data = null;

        $data["colRubriques"] = $this->Rubrique->GetAll();
        $data["colSousRubriques"] = $this->SousRubrique->GetAll();
        $data["colVilles"] = $this->Ville->GetAll();
        $data["colAbonnements"] = $this->Abonnement->GetAll();

        $objCommercant_to_send = $this->Commercant->GetById($prmIdCommercant);
        $data["objCommercant"] = $objCommercant_to_send;
        $data["objAssCommercantRubrique"] = $this->AssCommercantRubrique->GetWhere("ass_commercants_rubriques.IdCommercant = " . $prmIdCommercant);
        $data["objAssCommercantSousRubrique"] = $this->AssCommercantSousRubrique->GetWhere("ass_commercants_sousrubriques.IdCommercant = " . $prmIdCommercant);
        $data["objAssCommercantAbonnement"] = $this->AssCommercantAbonnement->GetWhere("ass_commercants_abonnements.IdCommercant = " . $prmIdCommercant);

        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($objCommercant_to_send->IdCommercant);
        if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
        $data["user_groups"] = $user_groups[0];
        //////$this->firephp->log($user_groups[0], 'user_groups');

        $data['objUserIonAuth_x'] = $this->ion_auth->user($user_ion_auth_id)->row();

        $data["colDemandeAbonnement"] = $this->mdldemande_abonnement->getByIdCommercantIdUser($prmIdCommercant, $user_ion_auth_id);

        $data["mdlville"] = $this->mdlville;
        $data["colDepartement"] = $this->mdldepartement->GetAll();
        //$this->load->view("admin/vwFicheCommercant",$data);
        $this->load->view("privicarte/admin/vwFicheCommercant",$data);
    }

    function supprimer ($prmIdCommercant = 0) {
        $data = null;

        $data["colRubriques"] = $this->Rubrique->GetAll();
        $data["colSousRubriques"] = $this->SousRubrique->GetAll();
        $data["colVilles"] = $this->Ville->GetAll();
        $data["colAbonnements"] = $this->Abonnement->GetAll();

        $objCommercant_to_send = $this->Commercant->GetById($prmIdCommercant);
        $data["objCommercant"] = $objCommercant_to_send;
        $data["objAssCommercantRubrique"] = $this->AssCommercantRubrique->GetWhere("ass_commercants_rubriques.IdCommercant = " . $prmIdCommercant);
        $data["objAssCommercantSousRubrique"] = $this->AssCommercantSousRubrique->GetWhere("ass_commercants_sousrubriques.IdCommercant = " . $prmIdCommercant);
        $data["objAssCommercantAbonnement"] = $this->AssCommercantAbonnement->GetWhere("ass_commercants_abonnements.IdCommercant = " . $prmIdCommercant);

        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($objCommercant_to_send->IdCommercant);
        if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
        $data["user_groups"] = $user_groups[0];
        //////$this->firephp->log($user_groups[0], 'user_groups');

        $data["colDemandeAbonnement"] = $this->mdldemande_abonnement->getByIdCommercantIdUser($prmIdCommercant, $user_ion_auth_id);

        $data["mdlville"] = $this->mdlville;
        //$this->load->view("admin/vwFicheCommercant",$data);

        $data["colBonplan"] = $this->mdlbonplan->lastBonplanCom2($prmIdCommercant);
        $data["colAnnonces"] = $this->mdlannonce->listeAnnonceParCommercant($prmIdCommercant);
        $data["colAgenda"] = $this->mdl_agenda->listeAgendaRecherche(0,0,0,"",0,0,10000,"","0","0000-00-00","0000-00-00",$prmIdCommercant,"0");
        $data["colAgendaPerso"] = $this->mdlagenda_perso->GetAgenda_agenda_persoByUser($prmIdCommercant, $user_ion_auth_id);

        $this->load->view("privicarte/admin/vwSupprimerCommercant",$data);
    }

    function supprimer_validate ($prmIdCommercant = 0) { 
        $data = null;

        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($prmIdCommercant);

        $colBonplan = $this->mdlbonplan->lastBonplanCom2($prmIdCommercant);
        if (isset($colBonplan) && count($colBonplan)>0) {
            foreach ($colBonplan as $key_bonplan) {
                $this->mdlbonplan->supprimeBonplans($key_bonplan->bonplan_id);
            }
        }

        $colAnnonces = $this->mdlannonce->listeAnnonceParCommercant($prmIdCommercant);
        if (isset($colAnnonces) && count($colAnnonces)>0) {
            foreach ($colAnnonces as $key_annonce) {
                $this->mdlannonce->supprimeAnnonces($key_annonce->annonce_id);
            }
        }

        $colAgenda = $this->mdl_agenda->listeAgendaRecherche(0,0,0,"",0,0,10000,"","0","0000-00-00","0000-00-00",$prmIdCommercant,"0");
        if (isset($colAgenda) && count($colAgenda)>0) {
            foreach ($colAgenda as $key_agenda) {
                $this->mdl_agenda->delete_definitif($key_annonce->id);
            }
        }

        $colAgendaPerso = $this->mdlagenda_perso->GetAgenda_agenda_persoByUser($prmIdCommercant, $user_ion_auth_id);
        if (isset($colAgendaPerso) && count($colAgendaPerso)>0) {
            foreach ($colAgendaPerso as $key_agendaPerso) {
                $this->mdlagenda_perso->delete_agenda_perso($key_agendaPerso->id);
            }
        }

        $this->AssCommercantAbonnement->DeleteByIdCommercant($prmIdCommercant);
        $this->AssCommercantRubrique->DeleteByIdCommercant($prmIdCommercant);
        $this->AssCommercantSousRubrique->DeleteByIdCommercant($prmIdCommercant);


        $this->Commercant->delete_validate($prmIdCommercant);

        $deactivate_final = $this->ion_auth_model->deactivate($user_ion_auth_id);
        $delete_final = $this->ion_auth_model->delete_user($user_ion_auth_id);
        

        
        //eto ko novaina
        redirect("admin/commercants/listeDerniereInscrit");


        //abonnement


    }    
    
    function modifier() {

        $objCommercant = $this->input->post("Societe");
        //////$this->firephp->log($objCommercant, 'objCommercant');

        if(!isset($objCommercant["IsActif"])) $objCommercant["IsActif"] = 0;

        $objCommercant["datecreation"] = strtotime(convert_Frenchdate_to_Sqldate($objCommercant["datecreation"]));

        $objAssCommercantRubrique = $this->input->post("AssCommercantRubrique");
        $objAssCommercantSousRubrique = $this->input->post("AssCommercantSousRubrique");
        $objAssCommercantAbonnement = $this->input->post("AssAbonnementCommercant");
        ////$this->firephp->log($objAssCommercantAbonnement, 'objAssCommercantAbonnement');
        
        /*$pdfAssocie = $this->input->post("pdfAssocie");
        $photo1Associe = $this->input->post("photo1Associe");
        $photo2Associe = $this->input->post("photo2Associe");
        $photo3Associe = $this->input->post("photo3Associe");
        $photo4Associe = $this->input->post("photo4Associe");
        $photo5Associe = $this->input->post("photo5Associe");
        $logoAssocie = $this->input->post("Societelogo");

        $photoAccueilAssocie = $this->input->post("photoAccueilAssocie");

        $pdfCom = doUpload("SocietePdf","application/resources/front/photoCommercant/images/",$pdfAssocie);
        $SocietePhoto1 = doUpload("SocietePhoto1","application/resources/front/photoCommercant/images/",$photo1Associe);
        $SocietePhoto2 = doUpload("SocietePhoto2","application/resources/front/photoCommercant/images/",$photo2Associe);
        $SocietePhoto3 = doUpload("SocietePhoto3","application/resources/front/photoCommercant/images/",$photo3Associe);
        $SocietePhoto4 = doUpload("SocietePhoto4","application/resources/front/photoCommercant/images/",$photo4Associe);
        $SocietePhoto5 = doUpload("SocietePhoto5","application/resources/front/photoCommercant/images/",$photo5Associe);
        $Societelogo = doUpload("Societelogo","application/resources/front/photoCommercant/images/",$logoAssocie);

        $SocietePhotoAccueil = doUpload("SocietePhotoAccueil","application/resources/front/photoCommercant/images/",$photoAccueilAssocie);


        $objCommercant["Pdf"] = $pdfCom==""?($pdfAssocie==""?"":$pdfAssocie):$pdfCom;
        $objCommercant["Photo1"] = $SocietePhoto1==""?($photo1Associe==""?"":$photo1Associe):$SocietePhoto1;
        $objCommercant["Photo2"] = $SocietePhoto2==""?($photo2Associe==""?"":$photo2Associe):$SocietePhoto2;
        $objCommercant["Photo3"] = $SocietePhoto3==""?($photo3Associe==""?"":$photo3Associe):$SocietePhoto3;
        $objCommercant["Photo4"] = $SocietePhoto4==""?($photo4Associe==""?"":$photo4Associe):$SocietePhoto4;
        $objCommercant["Photo5"] = $SocietePhoto5==""?($photo5Associe==""?"":$photo5Associe):$SocietePhoto5;
        $objCommercant["Logo"] = $Societelogo==""?($logoAssocie==""?"":$logoAssocie):$Societelogo;

        $objCommercant["PhotoAccueil"] = $SocietePhotoAccueil==""?($photoAccueilAssocie==""?"":$photoAccueilAssocie):$SocietePhotoAccueil;*/

        $this->load->Model("Commercant");





        //changing group
        $Abonnement_ionauth_user = $this->input->post("Abonnement_ionauth_user");
        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($objCommercant["IdCommercant"]);
        if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
        $this->ion_auth->remove_from_group($user_groups[0]->id, $user_ion_auth_id);
        $this->ion_auth->add_to_group($Abonnement_ionauth_user, $user_ion_auth_id);

        //ion_auth_update
        $data_ion_auth_update = array(
            //'username' => $objCommercant['Login'],
            //'email' => $objCommercant['Email'],
            'first_name' => $objCommercant['NomSociete'],
            'company' => $objCommercant['NomSociete']
        );

        $this->ion_auth->update($user_ion_auth_id, $data_ion_auth_update);
        //ion_auth_update

        $ion_ath_error = $this->ion_auth->errors();
        //changing group

        if (isset($ion_ath_error) && $ion_ath_error!="") {

            $_SESSION['error_fiche_com_admin'] = "Une erreur s'est produite durant l'enregistrement de vos données. Veuillez recommencer s'il vous plait.".$ion_ath_error;
            redirect("admin/commercants/fiche/".$objCommercant["IdCommercant"]);

        } else {

            $IdUpdatedCommercant = $this->Commercant->Update($objCommercant);
//var_dump($IdUpdatedCommercant);die();
            $this->load->Model("AssCommercantRubrique");
            $this->AssCommercantRubrique->DeleteByIdCommercant($IdUpdatedCommercant);
            $objAssCommercantRubrique["IdCommercant"] = $IdUpdatedCommercant;
            $this->AssCommercantRubrique->Insert($objAssCommercantRubrique);

            $this->load->Model("AssCommercantSousRubrique");
            $this->AssCommercantSousRubrique->DeleteByIdCommercant($IdUpdatedCommercant);
            $objAssCommercantSousRubrique["IdCommercant"] = $IdUpdatedCommercant;
            $this->AssCommercantSousRubrique->Insert($objAssCommercantSousRubrique);

            $this->load->Model("AssCommercantAbonnement");
            $this->AssCommercantAbonnement->DeleteByIdCommercant($IdUpdatedCommercant);
            $objAssCommercantAbonnement["IdCommercant"] = $IdUpdatedCommercant;
            //we replace IdAbonnement by Idgroup of ion_auth
            $objAssCommercantAbonnement["IdAbonnement"] = $Abonnement_ionauth_user;
            $objAssCommercantAbonnement["DateDebut"] = convert_Frenchdate_to_Sqldate($objAssCommercantAbonnement["DateDebut"]);
            $objAssCommercantAbonnement["DateFin"] = convert_Frenchdate_to_Sqldate($objAssCommercantAbonnement["DateFin"]);//
            $this->AssCommercantAbonnement->Insert($objAssCommercantAbonnement);
            ////$this->firephp->log($objAssCommercantAbonnement, 'objAssCommercantAbonnement');
//var_dump($objAssCommercantAbonnement["DateDebut"]);die();
            redirect("admin/commercants");

        }
    }
    function effacerPhotoCommercant(){
            $iCommercantId = $_POST["iCommercantId"] ;
            $oCommercant = $this->Commercant->GetById($iCommercantId) ;
            if(is_file("application/resources/front/photoCommercant/images/" . $oCommercant->PhotoAccueil)){
                    unlink ("application/resources/front/photoCommercant/images/" . $oCommercant->PhotoAccueil) ;
                    $oCommercant = $this->Commercant->effacerPhotoCommercant($iCommercantId) ;
            }
            else{
                    die ("Erreur de suppression") ;
            }
    }
    function effacerPhoto1Commercant(){
            $iCommercantId = $_POST["iCommercantId"] ;
            $oCommercant = $this->Commercant->GetById($iCommercantId) ;
            if(is_file("application/resources/front/photoCommercant/images/" . $oCommercant->Photo1)){
                    unlink ("application/resources/front/photoCommercant/images/" . $oCommercant->Photo1) ;
                    $oCommercant = $this->Commercant->effacerPhoto1($iCommercantId) ;
            }
            else{
                    die ("Erreur de suppression") ;
            }
    }
    function effacerPhoto2Commercant(){
            $iCommercantId = $_POST["iCommercantId"] ;
            $oCommercant = $this->Commercant->GetById($iCommercantId) ;
            if(is_file("application/resources/front/photoCommercant/images/" . $oCommercant->Photo2)){
                    unlink ("application/resources/front/photoCommercant/images/" . $oCommercant->Photo2) ;
                    $oCommercant = $this->Commercant->effacerPhoto2($iCommercantId) ;
            }
            else{
                    die ("Erreur de suppression") ;
            }
    }
    function effacerPhoto3Commercant(){
            $iCommercantId = $_POST["iCommercantId"] ;
            $oCommercant = $this->Commercant->GetById($iCommercantId) ;
            if(is_file("application/resources/front/photoCommercant/images/" . $oCommercant->Photo3)){
                    unlink ("application/resources/front/photoCommercant/images/" . $oCommercant->Photo3) ;
                    $oCommercant = $this->Commercant->effacerPhoto3($iCommercantId) ;
            }
            else{
                    die ("Erreur de suppression") ;
            }
    }
    function effacerPhoto4Commercant(){
            $iCommercantId = $_POST["iCommercantId"] ;
            $oCommercant = $this->Commercant->GetById($iCommercantId) ;
            if(is_file("application/resources/front/photoCommercant/images/" . $oCommercant->Photo4)){
                    unlink ("application/resources/front/photoCommercant/images/" . $oCommercant->Photo4) ;
                    $oCommercant = $this->Commercant->effacerPhoto4($iCommercantId) ;
            }
            else{
                    die ("Erreur de suppression") ;
            }
    }
    function effacerPhoto5Commercant(){
            $iCommercantId = $_POST["iCommercantId"] ;
            $oCommercant = $this->Commercant->GetById($iCommercantId) ;
            if(is_file("application/resources/front/photoCommercant/images/" . $oCommercant->Photo5)){
                    unlink ("application/resources/front/photoCommercant/images/" . $oCommercant->Photo5) ;
                    $oCommercant = $this->Commercant->effacerPhoto5($iCommercantId) ;
            }
            else{
                    die ("Erreur de suppression") ;
            }
    }
    function effacerPdfCommercant(){
            $iCommercantId = $_POST["iCommercantId"] ;
            $oCommercant = $this->Commercant->GetById($iCommercantId) ;
            if(is_file("application/resources/front/photoCommercant/images/" . $oCommercant->Pdf)){
                    unlink ("application/resources/front/photoCommercant/images/" . $oCommercant->Pdf) ;
                    $oCommercant = $this->Commercant->effacerPdf($iCommercantId) ;
            }
            else{
                    die ("Erreur de suppression") ;
            }
    }
    
    function categories(){
        $data["no_main_menu_home"] = "1";
        $data["no_left_menu_home"] = "1";
        
        //this function delete storage cache directory
        //$base_path_system_rand = str_replace('system/', '', BASEPATH);
	    //$validation_delete_path_rand = delete_directory_rand($base_path_system_rand."/pagecaracteristics");
        //this function delete storage cache directory
        
        $toListeCategorie = $this->mdlcategorie->GetAll() ;		
        $data['toListeCategorie'] = $toListeCategorie ;
        
        if($this->session->flashdata('mess_editcategorie')=='1') $data['mess_editcategorie'] = '<strong style="color:#060">Catégorie enregistrée !</strong>';
        if($this->session->flashdata('mess_editcategorie')=='2') $data['mess_editcategorie'] = '<strong style="color:#F00">Cette catégorie ne peut être supprimée ! catégorie liée à un partenaire !</strong>';
        if($this->session->flashdata('mess_editcategorie')=='3') $data['mess_editcategorie'] = '<strong style="color:#060">Catégorie supprimée !</strong>';
        
        $this->load->view("admin/vwCategory", $data);
        
    }
    
    
    function modifiercategorie(){
        $IdRubrique = $this->uri->rsegment(3);
        
        //echo $nom_url_commercant;
        $oCategorie = $this->mdlcategorie->GetById($IdRubrique) ;		
        $data['oCategorie'] = $oCategorie ;
        
        ////$this->firephp->log($oCategorie, 'oCategorie');
        
        if(is_numeric(trim($IdRubrique))) {
            $this->load->view("admin/vwEditCategory", $data);
        }
        else {
            redirect('admin/commercants/categories');
        }
        
    }
    
    function insertcategorie(){
        $data['oCategorie'] = NULL;
        $this->load->view("admin/vwEditCategory", $data);
    }
    
    function insertsouscategorie(){
        $IdRubrique = $this->uri->rsegment(3);
        $toCategorie = $this->mdlcategorie->GetById($IdRubrique) ;
        $data['toCategorie'] = $toCategorie ;
        $this->load->view("admin/vwEditSousCategory", $data);
    }
    
    
    function savecategorie(){
        $idrubrique_editcategorie = $this->input->post("idrubrique_editcategorie");
        $ocategorie['IdRubrique'] = $idrubrique_editcategorie;
        $inputcateg_editcategorie = $this->input->post("inputcateg_editcategorie");
        $ocategorie['Nom'] = $inputcateg_editcategorie;
        
        if ($idrubrique_editcategorie=="0") $this->mdlcategorie->Insert($ocategorie);
        else $this->mdlcategorie->Update($ocategorie);
        
        $this->session->set_flashdata('mess_editcategorie', '1');
        redirect('admin/commercants/categories');
       
    }
    
    function supprimercategorie(){
        $IdRubrique = $this->uri->rsegment(3);
        
        if(is_numeric(trim($IdRubrique))) {
            //echo $nom_url_commercant;
            $oCategorie = $this->mdlcategorie->verifier_categorie_commercant($IdRubrique) ;		
            if (count($oCategorie)==0){
                $this->mdlcategorie->Delete($IdRubrique) ;
                $this->session->set_flashdata('mess_editcategorie', '3');
            } else {
                $this->session->set_flashdata('mess_editcategorie', '2');
            }

            ////$this->firephp->log($oCategorie, 'oCategorie');
            
            redirect('admin/commercants/categories');
        }
        else {
            redirect('admin/commercants/categories');
        }
        
    }
    
    
    
    function souscategorie(){
        $IdRubrique = $this->uri->rsegment(3);
        
        if(is_numeric(trim($IdRubrique))) {
            $data["no_main_menu_home"] = "1";
            $data["no_left_menu_home"] = "1";

            //this function delete storage cache directory
            //$base_path_system_rand = str_replace('system/', '', BASEPATH);
            //$validation_delete_path_rand = delete_directory_rand($base_path_system_rand."/pagecaracteristics");
            //this function delete storage cache directory

            $toListeSousCategorie = $this->mdlcategorie->GetAllSousrubriqueByRubrique($IdRubrique) ;
            $toCategorie = $this->mdlcategorie->GetById($IdRubrique) ;
            $data['toListeSousCategorie'] = $toListeSousCategorie ;
            $data['toCategorie'] = $toCategorie ;

            if($this->session->flashdata('mess_editsouscategorie')=='1') $data['mess_editsouscategorie'] = '<strong style="color:#060">SousCatégorie enregistrée !</strong>';
            if($this->session->flashdata('mess_editsouscategorie')=='2') $data['mess_editsouscategorie'] = '<strong style="color:#F00">Cette SousCatégorie ne peut être supprimée ! catégorie liée à un partenaire !</strong>';
            if($this->session->flashdata('mess_editsouscategorie')=='3') $data['mess_editsouscategorie'] = '<strong style="color:#060">SousCatégorie supprimée !</strong>';

            $this->load->view("admin/vwSousCategory", $data);

        }
        else {
            redirect('admin/commercants/categories');
        }
    }
    
    
    function modifiersouscategorie(){
        $IdSousRubrique = $this->uri->rsegment(3);
        $IdRubrique = $this->uri->rsegment(4);
        
        //echo $nom_url_commercant;
        $oSousCategorie = $this->mdlcategorie->GetByIdSousCateg($IdSousRubrique);		
        $data['oSousCategorie'] = $oSousCategorie ;
        $toCategorie = $this->mdlcategorie->GetById($IdRubrique) ;
        $data['toCategorie'] = $toCategorie ;
        
        ////$this->firephp->log($oSousCategorie, 'oSousCategorie');
        
        if(is_numeric(trim($IdSousRubrique))) {
            $this->load->view("admin/vwEditSousCategory", $data);
        }
        else {
            redirect('admin/commercants/souscategorie/'.$IdRubrique);
        }
    }
    
    
    function savesouscategorie(){
        $idrubrique_editcategorie = $this->input->post("idrubrique_editcategorie");
        $ocategorie['IdRubrique'] = $idrubrique_editcategorie;
        $idrubrique_editsouscategorie = $this->input->post("idrubrique_editsouscategorie");
        $ocategorie['IdSousRubrique'] = $idrubrique_editsouscategorie;
        $inputcateg_editsouscategorie = $this->input->post("inputcateg_editsouscategorie");
        $ocategorie['Nom'] = $inputcateg_editsouscategorie;
        
        if ($idrubrique_editsouscategorie=="0") $this->mdlcategorie->InsertSousCateg($ocategorie);
        else $this->mdlcategorie->UpdateSousCateg($ocategorie);
        
        $this->session->set_flashdata('mess_editsouscategorie', '1');
        redirect('admin/commercants/souscategorie/'.$idrubrique_editcategorie);
       
    }
    
    
    function supprimersouscategorie(){
        $IdSousRubrique = $this->uri->rsegment(3);
        $IdRubrique = $this->uri->rsegment(4);
        ////$this->firephp->log($IdSousRubrique, 'IdSousRubrique');
        ////$this->firephp->log($IdRubrique, 'IdRubrique');
        
        if(is_numeric(trim($IdSousRubrique))) {
            //echo $nom_url_commercant;
            $oSousCategorie = $this->mdlcategorie->verifier_souscategorie_commercant($IdSousRubrique) ;		
            if (count($oSousCategorie)==0){
                $this->mdlcategorie->DeleteSousCateg($IdSousRubrique) ;
                $this->session->set_flashdata('mess_editsouscategorie', '3');
            } else {
                $this->session->set_flashdata('mess_editsouscategorie', '2');
            }

            ////$this->firephp->log(count($oSousCategorie), 'oCategorie');
            
            redirect('admin/commercants/souscategorie/'.$IdRubrique);
        }
        else {
            redirect('admin/commercants/categories');
        }
        
    }

    function add_datefin_plus_un_an(){
        $DateDebut = $this->input->post("DateDebut");
        $date_sql = convert_Frenchdate_to_Sqldate($DateDebut);
        $tzDateExploded = explode ("-", $date_sql) ;
        $tzDateExploded[0] = intval($tzDateExploded[0]) + 1;
        $zDateRetour = $tzDateExploded[0] . "-" . $tzDateExploded[1] . "-" . $tzDateExploded[2] ;
        echo convert_Sqldate_to_Frenchdate($zDateRetour);
    }



    function contenupro($IdCommercant=NULL){
        if (!isset($IdCommercant) || $IdCommercant==NULL || $IdCommercant=='0') {
            # code...
            redirect('admin/commercants/listeDerniereInscrit');
        } else {
            # code...
            $objCommercant = $this->Commercant->GetById($IdCommercant);

            //$this->ion_auth->logout();

            $query = $this->db->select('username, email, id, password, active, last_login')
                          ->where('username', $objCommercant->Login)
                          ->limit(1)
                          ->get('users_ionauth');
            if ($query->num_rows() === 1)
            {
                $user = $query->row();
                $this->load->Model("Ion_auth_model");
                $this->Ion_auth_model->set_session($user);
                $this->Ion_auth_model->update_last_login($user->id);
                $this->Ion_auth_model->clear_login_attempts($identity);
            }

            $this->session->set_userdata("from_super_admin_account",'1');

            redirect('front/utilisateur/contenupro');
       }
        
    }
    

    
}