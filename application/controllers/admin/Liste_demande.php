<?php
class liste_demande extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->output->set_header('Access-Control-Allow-Origin: null');  header('Access-Control-Allow-Credentials: omit');
        header('Access-Control-Allow-Method: PUT, GET, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');

        $this->load->library('session');
        $this->load->library('ion_auth');

        $this->load->model('mdlville');
        $this->load->model('mdlcommercant');
        $this->load->model('mdldemande');
        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }
    public function index(){
        $all = $this->mdldemande->getAllDemande();
        $data['demande'] =  $all;
        $this->load->view('admin/indexDemande',$data);
    }
    public function validate_domain(){
        $val = $this->input->post('value');
        $info = $this->mdldemande->getbyid($val);
        if(isset($info) AND $info != null AND $info !=''){
            $this->mdldemande->valid_demande($val);
            $nomcom = $this->mdldemande->getComNameById($info->idCom);
            $prmDestinataires[] = array("Email" => $info->Email, "Name" => $nomcom);
            //$prmDestinataires[] = array("Email" => 'alphadev@randevteam.com', "Name" => $nomcom);
            $prmContenu = "
            Votre démande de nom de domaine: ".$info->nomDomaine." a été validé. <br>
            Votre site est desormais accessible sur ".$info->nomDomaine;
            envoi_notification($prmDestinataires, $prmSujet = "Notification Sortez", $prmContenu, $prmEnvoyeur = "contact@sortez.org", $prmEnvoyeurName = "Sortez");
            echo 'ok';
        }
    }
}