<?php
class accueil extends CI_Controller{
    
    function __construct() {
        parent::__construct();
		$this->load->model("mdlannonce") ;
		$this->load->model("mdlcommercant") ;
		$this->load->model("mdlbonplan") ;
		$this->load->model("mdlcategorie") ;
		$this->load->model("mdlville") ;
		$this->load->model("mdlcommercantpagination") ;
        $this->load->model("sousRubrique") ;
        $this->load->model("AssCommercantAbonnement") ;
        $this->load->model("AssCommercantSousRubrique") ;
        $this->load->model("Commercant") ;
        $this->load->model("user") ;
        $this->load->model("mdl_agenda") ;
        $this->load->model("notification") ;
        $this->load->model("mdl_datatourisme_agenda") ;

        $this->load->library('user_agent');

        $this->load->library("pagination");

        $this->load->library('image_moo');

        $this->load->library('session');

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");
        $this->load->helpers("clubproximite_helper");
         $this->load->model("Mdl_card");


        check_vivresaville_id_ville();


    }

    function main_department_list(){
        $this->load->model("mdldepartement");

        $id_ville_selected = $this->input->post("id_departement_selected");
        if (isset($id_ville_selected) && $id_ville_selected != "0" && $id_ville_selected != "") {
            $vsv_object = $this->mdldepartement->getById($id_ville_selected);
            if (isset($vsv_object) && isset($vsv_object->departement_id)) {
                $this->session->set_userdata('iDepartementId_x', $vsv_object->departement_id);
                redirect("agenda");
            }
        }

        $toDepartement = $this->mdldepartement->GetAgendaDepartements_pvc();
        $data['toDepartement'] = $toDepartement;
        $requery_agenda = ' IsActif = 1 ';
        $data['total_agenda']  = count($this->mdl_agenda->getWhere($requery_agenda));
        $this->load->view("main_department_list_vw", $data);
    }
    
  /*  function index(){
		$argOffset = (isset($_SESSION["argOffset"])) ? $_SESSION["argOffset"] : 0 ;
		//$argOffset = $_iPage ;
        if (preg_match("/admin/", $_SERVER["PHP_SELF"])) {
            redirect("admin/home");
        } else {
            if(isset($_POST["inputStringHidden"])){
                    $iCategorieId = $_POST["inputStringHidden"] ;
                    $iVilleId = $_POST["inputStringVilleHidden"] ;
                    $zMotCle = $_POST["zMotCle"] ;
                    $toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche($iCategorieId, $iVilleId, $zMotCle) ;
            }else{
                    $toListeBonPlan = $this->mdlbonplan->listeBonPlan() ;
            }
            $toCategorie= $this->mdlcategorie->GetCommercantSouscategorie();
            $toVille= $this->mdlville->GetAll();
            $data['toVille'] = $toVille ;
            $data['toCategorie'] = $toCategorie ;
            $data['toListeBonPlan'] = $toListeBonPlan ;
            //$toCommercant= $this->mdlcommercant->GetAllCommercant();

            $TotalRows = $this->mdlcommercantpagination->Compter();
            $PerPage = 8;

            $iNombreLiens = $TotalRows / $PerPage ;
            if($iNombreLiens > round($iNombreLiens)){
                    $iNombreLiens = round($iNombreLiens) + 1 ;
            }
            else {
                    $iNombreLiens = round($iNombreLiens) ;
            }

            $data["iNombreLiens"] = $iNombreLiens ;

            $data["PerPage"] = $PerPage ;
            $data["TotalRows"] = $TotalRows ;
            $data["argOffset"] = $argOffset ;
            $toCommercant= $this->mdlcommercantpagination->GetListeCommercantPagination($PerPage, $argOffset);
            $data['toCommercant'] = $toCommercant ;
            // print_r($data['toCommercant']);exit();
            $this->load->view('front/vwAccueil', $data) ;
        }
    }*/

    // function genernumcardTEST(){
    //     // $this->load->view("generernumcard");
    //     // $iduser=$this->user->GetAll();
    //     $testid="37";
    //     $iduser=$this->user->getById_user($testid);
    //     var_dump($iduser);

    //     foreach( $iduser as $residuser ){
    //         $resultatuser= $residuser->idUser;
    //         $idusercard=$this->Mdl_card->GetAll();
    //         foreach ($idusercard as $residusercard) {
    //             $resultat=$residusercard->id_user;
    //             if (isset($resultat) AND $resultat!=NULL ) {
    //                 if($resultat==$resultatuser){
    //                     echo "efa ok! ";
    //                 }
    //             }else{
    //                 echo "kkkkkkkkkkkkkkk";
    //                 var_dump($iduser->user_ionauth_id);

    //                 // generate_client_card($iduser->user_ionauth_id);
                    
    //                 // var_dump($residuser->user_ionauth_id);
    //                 // echo "generation num card avec success<br>";
                    
    //             }
    //         }            
    //     }
    // }

    function genernumcard(){
        $resreq=$this->user->getById_user_idcart_nul();
        // var_dump($resreq);
        foreach ($resreq as $resultat) {
            // var_dump($resultat->user_ionauth_id);
            generate_client_card($resultat->user_ionauth_id);
        }
    }

        function index(){


            if ($this->ion_auth->is_admin()) {
                $this->session->set_flashdata('domain_from', '1');
                redirect("admin/home");
            }

            ////////////////DELETE OLD AGENDA date_fin past 8 days
            $this->mdl_agenda->deleteOldAgenda_fin8jours();


            ////START VERIFY SUBSCRIPTION
        	@verify_all_pro_subscription();    
                
                
		$argOffset = (isset($_SESSION["argOffset"])) ? $_SESSION["argOffset"] : 0 ;
		//$argOffset = $_iPage ;
        if (preg_match("/admin/", $_SERVER["PHP_SELF"])) {
            redirect("admin/home");
        } else {

            //if the link doesn't come from page navigation, clear category session
            $current_URI_club = $_SERVER['REQUEST_URI'];
            $current_URI_club_array = explode("accueil/index", $current_URI_club);
            ////$this->firephp->log(count($current_URI_club_array), 'nb_array');
            if (count($current_URI_club_array)==1) {
                $this->session->unset_userdata('iCategorieId_x');
                $this->session->unset_userdata('iVilleId_x');
                $this->session->unset_userdata('zMotCle_x');
                $this->session->unset_userdata('iOrderBy_x');
                $this->session->unset_userdata('inputFromGeolocalisation_x');
                $this->session->unset_userdata('inputGeolocalisationValue_x');
                $this->session->unset_userdata('inputGeoLatitude_x');
                $this->session->unset_userdata('inputGeoLongitude_x');
            }
            //if the link doesn't come from page navigation, clear catgory session

            
            if ($this->input->post("inputStringHidden") == "0") unset($_SESSION['iCategorieId']);
            if ($this->input->post("inputStringVilleHidden") == "0") unset($_SESSION['iCommercantId']);
            if ($this->input->post("zMotCle") == "") unset($_SESSION['zMotCle']);
            if ($this->input->post("inputStringOrderByHidden") == "0") unset($_SESSION['iOrderBy']);
            if ($this->input->post("inputFromGeolocalisation") == "0" || $this->input->post("inputFromGeolocalisation") == "1") unset($_SESSION['inputFromGeolocalisation']);
            if ($this->input->post("inputGeolocalisationValue") == "10") unset($_SESSION['inputGeolocalisationValue']);
            if ($this->input->post("inputGeoLatitude") == "0") unset($_SESSION['inputGeoLatitude']);
            if ($this->input->post("inputGeoLongitude") == "0") unset($_SESSION['inputGeoLongitude']);
            
            /*if(isset($_POST["inputStringHidden"])){
                    $iCategorieId = $_POST["inputStringHidden"] ;
                    $iVilleId = $_POST["inputStringVilleHidden"] ;
                    $zMotCle = $_POST["zMotCle"] ;
                    $toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche($iCategorieId, $iVilleId, $zMotCle) ;
            }else{
                    $toListeBonPlan = $this->mdlbonplan->listeBonPlan() ;
            }*/
            
            //$TotalRows = $this->mdlcommercantpagination->Compter();
            $PerPage = 15;
            $data["iFavoris"] = "";
            
            $toCategorie= $this->mdlcategorie->GetCommercantSouscategorie();
            $toVille= $this->mdlville->GetCommercantVilles();//get ville list of commercant
            $data['toVille'] = $toVille ;
            $data['toCategorie'] = $toCategorie ;
            $toCategorie_principale= $this->mdlcategorie->GetCommercantCategorie();
            $data['toCategorie_principale'] = $toCategorie_principale ;
            //$data['toListeBonPlan'] = $toListeBonPlan ;
            //$toCommercant= $this->mdlcommercant->GetAllCommercant();
			$toCommunes = $this->mdlville->GetCommercantCommunes();//get Commune list of commercant
            $data['toCommunes'] = $toCommunes;
			$toDepartements = $this->mdlville->GetCommercantDepartement();//get Departements list of commercant
            $data['toDepartements'] = $toDepartements;

        ////$this->firephp->log($_POST, '_POST');
            
        if(isset($_POST["inputStringHidden"])){
            unset($_SESSION['iCategorieId']);
            unset($_SESSION['iVilleId']);
            unset($_SESSION['zMotCle']);
            unset($_SESSION['iOrderBy']);
            unset($_SESSION['inputFromGeolocalisation']);
            unset($_SESSION['inputGeolocalisationValue']);
            unset($_SESSION['inputGeoLatitude']);
            unset($_SESSION['inputGeoLongitude']);
            
            //$iCategorieId = $_POST["inputStringHidden"] ; 
            $iCategorieId_all0 = $this->input->post("inputStringHidden");
            if (isset($iCategorieId_all0) && $iCategorieId_all0 != "" && $iCategorieId_all0 != NULL && $iCategorieId_all0 != '0') {
                $iCategorieId_all = substr($iCategorieId_all0,1);
                $iCategorieId = explode(',', $iCategorieId_all);
            } else {
                $iCategorieId = '0';
            }
            if (isset($_POST["inputStringVilleHidden"])) $iVilleId = $_POST["inputStringVilleHidden"] ; else $iVilleId = "0";
            if(isset($_POST["inputStringOrderByHidden"])) $iOrderBy = $_POST["inputStringOrderByHidden"] ; else $iOrderBy = '';
            if (isset($_POST["zMotCle"])) $zMotCle = $_POST["zMotCle"]; else $zMotCle = '';
            if (isset($_POST["inputFromGeolocalisation"])) $inputFromGeolocalisation = $_POST["inputFromGeolocalisation"] ; else $inputFromGeolocalisation = "0";
            if (isset($_POST["inputGeolocalisationValue"])) $inputGeolocalisationValue = $_POST["inputGeolocalisationValue"] ; else $inputGeolocalisationValue = "10";
            if (isset($_POST["inputGeoLatitude"])) $inputGeoLatitude = $_POST["inputGeoLatitude"] ; else $inputGeoLatitude = "0";
            if (isset($_POST["inputGeoLongitude"])) $inputGeoLongitude = $_POST["inputGeoLongitude"] ; else $inputGeoLongitude = "0";

            $_SESSION['iCategorieId'] = $iCategorieId;                              $this->session->set_userdata('iCategorieId_x', $iCategorieId);
            $_SESSION['iVilleId'] = $iVilleId;                                      $this->session->set_userdata('iVilleId_x', $iVilleId);
            $_SESSION['zMotCle'] = $zMotCle;                                        $this->session->set_userdata('zMotCle_x', $zMotCle);
            $_SESSION['iOrderBy'] = $iOrderBy;                                      $this->session->set_userdata('iOrderBy_x', $iOrderBy);
            $_SESSION['inputFromGeolocalisation'] = $inputFromGeolocalisation;      $this->session->set_userdata('inputFromGeolocalisation_x', $inputFromGeolocalisation);
            $_SESSION['inputGeolocalisationValue'] = $inputGeolocalisationValue;    $this->session->set_userdata('inputGeolocalisationValue_x', $inputGeolocalisationValue);
            $_SESSION['inputGeoLatitude'] = $inputGeoLatitude;                      $this->session->set_userdata('inputGeoLatitude_x', $inputGeoLatitude);
            $_SESSION['inputGeoLongitude'] = $inputGeoLongitude;                    $this->session->set_userdata('inputGeoLongitude_x', $inputGeoLongitude);
            
            $data['iCategorieId'] = $iCategorieId ;
            $data['iVilleId'] = $iVilleId ;
            $data['zMotCle'] = $zMotCle ;
            $data['iOrderBy'] = $iOrderBy ;

            $session_iCategorieId = $this->session->userdata('iCategorieId_x');
            $session_iVilleId = $this->session->userdata('iVilleId_x');
            $session_zMotCle = $this->session->userdata('zMotCle_x');
            $session_iOrderBy = $this->session->userdata('iOrderBy_x');
            $session_inputFromGeolocalisation = $this->session->userdata('inputFromGeolocalisation_x');
            $session_inputGeolocalisationValue = $this->session->userdata('inputGeolocalisationValue_x');
            $session_inputGeoLatitude = $this->session->userdata('inputGeoLatitude_x');
            $session_inputGeoLongitude = $this->session->userdata('inputGeoLongitude_x');

            //$toCommercant = $this->mdlcommercantpagination->listePartenaireRecherche($_SESSION['iCategorieId'], $_SESSION['iVilleId'], $_SESSION['zMotCle'], $data["iFavoris"], $argOffset, $PerPage, $_SESSION['iOrderBy'], $_SESSION['inputFromGeolocalisation'], $_SESSION['inputGeolocalisationValue'], $_SESSION['inputGeoLatitude'], $_SESSION['inputGeoLongitude']) ;
            $TotalRows = count($this->mdlcommercantpagination->listePartenaireRecherche($session_iCategorieId, $session_iVilleId, $session_zMotCle, $data["iFavoris"], 0, 10000, $session_iOrderBy, $session_inputFromGeolocalisation, $session_inputGeolocalisationValue, $session_inputGeoLatitude, $session_inputGeoLongitude));

            $config_pagination = array();
            $config_pagination["base_url"] = base_url()."accueil/index/";
            $config_pagination["total_rows"] = $TotalRows;
            $config_pagination["per_page"] = $PerPage;
            $config_pagination["uri_segment"] = 3;
            $config_pagination['first_link'] = '<<<';
            $config_pagination['last_link'] = '>>>';
            $this->pagination->initialize($config_pagination);
            $page_pagination = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            //$toCommercant = $this->mdlcommercantpagination->listePartenaireRecherche($session_iCategorieId, $_SESSION['iVilleId'], $_SESSION['zMotCle'], $data["iFavoris"], $page_pagination, $config_pagination["per_page"], $_SESSION['iOrderBy'], $_SESSION['inputFromGeolocalisation'], $_SESSION['inputGeolocalisationValue'], $_SESSION['inputGeoLatitude'], $_SESSION['inputGeoLongitude']) ;
            $toCommercant = $this->mdlcommercantpagination->listePartenaireRecherche($session_iCategorieId, $session_iVilleId, $session_zMotCle, $data["iFavoris"], $page_pagination, $config_pagination["per_page"], $session_iOrderBy, $session_inputFromGeolocalisation, $session_inputGeolocalisationValue, $session_inputGeoLatitude, $session_inputGeoLongitude);
            $data["links_pagination"] = $this->pagination->create_links();

        }else{
            $data["iFavoris"] = "";
            $session_iCategorieId = $this->session->userdata('iCategorieId_x');
            $session_iVilleId = $this->session->userdata('iVilleId_x');
            $session_zMotCle = $this->session->userdata('zMotCle_x');
            $session_iOrderBy = $this->session->userdata('iOrderBy_x');
            $session_inputFromGeolocalisation = $this->session->userdata('inputFromGeolocalisation_x');
            $session_inputGeolocalisationValue = $this->session->userdata('inputGeolocalisationValue_x');
            $session_inputGeoLatitude = $this->session->userdata('inputGeoLatitude_x');
            $session_inputGeoLongitude = $this->session->userdata('inputGeoLongitude_x');

            $iCategorieId = (isset($session_iCategorieId)) ? $session_iCategorieId : 0 ;
            $iVilleId = (isset($session_iVilleId)) ? $session_iVilleId : "" ;
            $zMotCle = (isset($session_zMotCle)) ? $session_zMotCle : "" ;
            $iOrderBy = (isset($session_iOrderBy)) ? $session_iOrderBy : " order by commercants.IdCommercant desc " ;
            $inputFromGeolocalisation = (isset($session_inputFromGeolocalisation)) ? $session_inputFromGeolocalisation : "0" ;
            $inputGeolocalisationValue = (isset($session_inputGeolocalisationValue)) ? $session_inputGeolocalisationValue : "10" ;
            $inputGeoLatitude = (isset($session_inputGeoLatitude)) ? $session_inputGeoLatitude : "0" ;
            $inputGeoLongitude = (isset($session_inputGeoLongitude)) ? $session_inputGeoLongitude : "0" ;
            //$toCommercant = $this->mdlcommercantpagination->listePartenaireRecherche($iCategorieId, $iVilleId, $zMotCle, $data["iFavoris"], $argOffset, $PerPage, $iOrderBy, $inputFromGeolocalisation, $inputGeolocalisationValue, $inputGeoLatitude, $inputGeoLongitude) ;
            $TotalRows = count($this->mdlcommercantpagination->listePartenaireRecherche($iCategorieId, $iVilleId, $zMotCle, $data["iFavoris"], 0, 10000, $iOrderBy, $inputFromGeolocalisation, $inputGeolocalisationValue, $inputGeoLatitude, $inputGeoLongitude));

            $config_pagination = array();
            $config_pagination["base_url"] = base_url()."accueil/index/";
            $config_pagination["total_rows"] = $TotalRows;
            $config_pagination["per_page"] = $PerPage;
            $config_pagination["uri_segment"] = 3;
            $config_pagination['first_link'] = '<<<';
            $config_pagination['last_link'] = '>>>';
            $this->pagination->initialize($config_pagination);
            $page_pagination = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $toCommercant = $this->mdlcommercantpagination->listePartenaireRecherche($iCategorieId, $iVilleId, $zMotCle, $data["iFavoris"], $page_pagination, $config_pagination["per_page"], $iOrderBy, $inputFromGeolocalisation, $inputGeolocalisationValue, $inputGeoLatitude, $inputGeoLongitude) ;
            $data["links_pagination"] = $this->pagination->create_links();
        }

            ////$this->firephp->log($_SERVER['REQUEST_URI'], 'PATH_INFO');

            $iNombreLiens = $TotalRows / $PerPage ;
            if($iNombreLiens > round($iNombreLiens)){
                    $iNombreLiens = round($iNombreLiens) + 1 ;
            }
            else {
                    $iNombreLiens = round($iNombreLiens) ;
            } 
            //////////////////////////////////

            $data["iNombreLiens"] = $iNombreLiens ;

            $data["PerPage"] = $PerPage ;
            $data["TotalRows"] = $TotalRows ;
            $data["argOffset"] = $argOffset ;
            //$toCommercant= $this->mdlcommercantpagination->GetListeCommercantPagination($PerPage, $argOffset);
            $data['toCommercant'] = $toCommercant ;//william hack
            // print_r($data['toCommercant']);exit();
            //$this->load->view('front/vwAccueil', $data) ;
            $data['pagecategory'] = 'partenaire';
            
            
            
            $is_mobile = $this->agent->is_mobile();
            //test ipad user agent
            $is_mobile_ipad = $this->agent->is_mobile('ipad');
            $data['is_mobile_ipad'] = $is_mobile_ipad;
            $is_robot = $this->agent->is_robot();
            $is_browser = $this->agent->is_browser();
            $is_platform = $this->agent->platform();
            $data['is_mobile'] = $is_mobile;
            $data['is_robot'] = $is_robot;
            $data['is_browser'] = $is_browser;
            $data['is_platform'] = $is_platform;
            
            if(isset($_POST["from_mobile_search_page_partner"])){
                $data['from_mobile_search_page_partner'] = $_POST["from_mobile_search_page_partner"];
                $this->session->set_userdata('from_mobile_search_page_partner', $_POST["from_mobile_search_page_partner"]);
            }
            $from_mobile_search_page_partner = $this->session->userdata('from_mobile_search_page_partner');
            if(isset($from_mobile_search_page_partner)) $data['from_mobile_search_page_partner'] = $from_mobile_search_page_partner;

            
            
            if ($this->ion_auth->logged_in()){
                $user_ion_auth = $this->ion_auth->user()->row();
                $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                if ($iduser==null || $iduser==0 || $iduser=="") {
                    $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                }
                $data['IdUser'] = $iduser;
            }
            
            
            //$this->load->view('frontAout2013/home_main', $data) ;
            $this->load->view('privicarte/annuaire_main', $data) ;
        }
    }
    
	function redirection($_iPage){
		$_SESSION["argOffset"] = $_iPage ;
        $this->session->set_userdata('from_mobile_search_page_partner', '1');
            redirect("/");
	}
        
        function redirect_home(){
		unset($_SESSION['iCategorieId']);
                unset($_SESSION['iVilleId']);
                unset($_SESSION['zMotCle']);
		redirect("/");
	}
        
        
        function check_category_list() {
            $inputStringVilleHidden_partenaires = $this->input->post("inputStringVilleHidden_partenaires");
            ////$this->firephp->log($inputStringVilleHidden_partenaires, 'inputStringVilleHidden_partenaires');
            //$toCategorie= $this->mdlcategorie->GetCommercantSouscategorie();
            //$toVille= $this->mdlville->GetCommercantVilles();//get ville list of commercant
            if (isset($inputStringVilleHidden_partenaires) && $inputStringVilleHidden_partenaires != "" && $inputStringVilleHidden_partenaires != '0' && $inputStringVilleHidden_partenaires != NULL) {
                $toCategorie_principale= $this->mdlcategorie->GetCommercantCategoriebyVille_x($inputStringVilleHidden_partenaires);
            } else {
                $toCategorie_principale= $this->mdlcategorie->GetCommercantCategorie_x();
            }
            
            $result_to_show = '';
            
            if (isset($toCategorie_principale)) {
                foreach($toCategorie_principale as $oCategorie_principale){
                    
                    //vérification nbCommercant par catégorie pple******************
                        $OCommercantSousRubrique_verifnb = $this->mdlcategorie->GetCommercantSouscategorieByRubrique_x($oCategorie_principale->IdRubrique);
                        $verifnb = 0;
                        if (isset($OCommercantSousRubrique_verifnb)) {
                            foreach($OCommercantSousRubrique_verifnb as $ObjCommercantSousRubrique_verifnb){
                                if (isset($inputStringVilleHidden_partenaires)) $iVilleId_verif = $inputStringVilleHidden_partenaires; else $iVilleId_verif = 0;
                                $IdSousRubrique_verif[0] = $ObjCommercantSousRubrique_verifnb->IdSousRubrique;
                                $toCommercant_verif = $this->mdlcommercantpagination->listePartenaireRecherche($IdSousRubrique_verif, $iVilleId_verif) ;
                                $verifnb = $verifnb + count($toCommercant_verif);
                            }
                        }
                    //vérification nbCommercant par catégorie pple******************
                        
                    if (isset($verifnb) && $verifnb != 0){
                        $result_to_show .= '<div class="leftcontener2013title" onClick="javascript:show_current_categ_subcateg('.$oCategorie_principale->IdRubrique.')" style="margin-top:5px;">';
                        $result_to_show .= ucfirst(strtolower($oCategorie_principale->Nom))." (".$verifnb.")";
                        $result_to_show .= '</div>';
                    }
                    $result_to_show .= '<div class="leftcontener2013content" id="leftcontener2013content_'.$oCategorie_principale->IdRubrique.'" style="margin-bottom:5px; margin-top:10px;">';
                    
                    $OCommercantSousRubrique = $this->mdlcategorie->GetCommercantSouscategorieByRubrique_x($oCategorie_principale->IdRubrique);
                    
                    //$result_to_show .= '<br/>';
                    
                    
                    /*if (isset($_SESSION['iCategorieId'])) {
                        $iCategorieId_sess = $_SESSION['iCategorieId'];
                    }*/

                    $session_iCategorieId = $this->session->userdata('iCategorieId_x');
                    if (isset($session_iCategorieId)) {
                        $iCategorieId_sess = $session_iCategorieId;
                    }
                    
                    
                    if (count($OCommercantSousRubrique)>0) {
                        $i_rand = 0;
                        foreach($OCommercantSousRubrique as $ObjCommercantSousRubrique){
                            
                            //Vérification nbCommercant par sous categorie **********************
                                $verifnb_souscat = 0;
                                if (isset($inputStringVilleHidden_partenaires)) $iVilleId_verif_souscat = $inputStringVilleHidden_partenaires; else $iVilleId_verif_souscat = 0;
                                $IdSousRubrique_verif_souscat[0] = $ObjCommercantSousRubrique->IdSousRubrique;
                                $toCommercant_verif_souscat = $this->mdlcommercantpagination->listePartenaireRecherche($IdSousRubrique_verif_souscat, $iVilleId_verif_souscat) ;
                                $verifnb_souscat = count($toCommercant_verif_souscat);
                            //Vérification nbCommercant par sous categorie **********************
                            
                            if (isset($verifnb_souscat) && $verifnb_souscat != 0) {
                                $result_to_show .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>';
                                $result_to_show .= '<td width="22"><input  onClick="javascript:submit_search_partner();" name="check_part['.$i_rand.']" id="check_part_'.$ObjCommercantSousRubrique->IdSousRubrique.'" type="checkbox" value="'.$ObjCommercantSousRubrique->IdSousRubrique.'"';
                                if (isset($iCategorieId_sess) && is_array($iCategorieId_sess)) {
                                    if (in_array($ObjCommercantSousRubrique->IdSousRubrique, $iCategorieId_sess)) $result_to_show .= 'checked';
                                }
                                $result_to_show .= '></td><td>';
                                $result_to_show .= ucfirst(strtolower($ObjCommercantSousRubrique->Nom)).' ('.$verifnb_souscat.')</td>';
                                $result_to_show .= '</tr></table>';
                            }
                            $i_rand++;
                        }
                    }
                    $result_to_show .= '</div>';
                }
            }
            
            echo $result_to_show;
            
        }


    function check_Idcategory_of_subCategory(){
        $session_iCategorieId = $this->session->userdata('iCategorieId_x');
        //var_dump($session_iCategorieId);
        if (isset($session_iCategorieId)) {
            $iCategorieId_sess = $session_iCategorieId;
            if (isset($iCategorieId_sess) && is_array($iCategorieId_sess)) {
                $all_subcategory = "";
                for($i=0;$i < count($iCategorieId_sess);$i++)
                {
                    $sousRubrique = $this->sousRubrique->GetById($iCategorieId_sess[$i]);
                    $all_subcategory .= "-".$sousRubrique->IdRubrique;
                }
                echo substr($all_subcategory,1);
            } else {
                echo "0";
            }
        } else echo "0";
    }

    /*function remove_all_datatourisme_data(){
        $this->load->model('mdl_datatourisme_agenda');
        $this->load->model('mdl_agenda');
        $this->load->model('mdl_agenda_datetime');
        $toresult = $this->mdl_datatourisme_agenda->getAll();
        foreach ($toresult as $item) {
            echo $item->agenda_id ." -> ".$item->datatourisme_id."<br/>";
            $toresult_updated = $this->mdl_agenda->delete_definitif($item->agenda_id);
            if ($toresult_updated) $this->mdl_datatourisme_agenda->delet_by_agenda($item->agenda_id);
            if ($toresult_updated) $this->mdl_agenda_datetime->deleteByAgendaId($item->agenda_id);
        }
    }*/

    /*function copy_lampe_pro_dev(){
        $this->load->model('Mdl_lampe_avenue');
        $toresult = $this->Mdl_lampe_avenue->GetAll_prod();
        foreach ($toresult as $item) {
            $toresult_updated = $this->Mdl_lampe_avenue->Get_prod_by_id_product_id_category($item->id_category, $item->id_product);
            if ($toresult_updated) {
                $toresult_updated = (array)$toresult_updated;
                $trying = $this->Mdl_lampe_avenue->Get_dev_by_id_product_id_category($toresult_updated['id_category'], $toresult_updated['id_product']);
                if ($trying) {
                    //$result_final = $this->Mdl_lampe_avenue->Update_dev($toresult_updated);
                    echo "position : " . $toresult_updated['position'] ." -> ";
                    echo "category : ". $item->id_category ." ->  Product : ".$item->id_product." - OK<br/>";
                } else {
                    echo "category : ". $item->id_category ." ->  Product : ".$item->id_product." - ERROR<br/>";
                }
            }
        }
    }*/

    function copy_lampe_pro_dev(){
        $this->load->model('Mdl_lampe_avenue');
        $toresult = $this->Mdl_lampe_avenue->Get_all_id_product();
        $data['toresult'] = $toresult;
        $data['Mdl_lampe_avenue'] = $this->Mdl_lampe_avenue;
        $this->load->view('copy_lampe_pro_dev', $data) ;
    }

    /*function move_glissieres_contents(){
        $this->load->model('Mdl_glissieres_prod');
        $this->load->model('Mdl_glissieres_dev');
        $toresult = $this->Mdl_glissieres_prod->GetAll();
        foreach ($toresult as $item) {

            //$to_integrate = array();
            $item_updated = (array) $item;

            $to_integrate['presentation_1_contenu1'] = $item_updated['presentation_1_contenu']; unset($item_updated['presentation_1_contenu']);
            $to_integrate['page1_1_contenu1'] = $item_updated['page1_1_contenu']; unset($item_updated['page1_1_contenu']);
            $to_integrate['page2_1_contenu1'] = $item_updated['page2_1_contenu']; unset($item_updated['page2_1_contenu']);
            unset($item_updated['presentation_3_image_3']);
            unset($item_updated['presentation_3_image_4']);
            unset($item_updated['presentation_4_image_3']);
            unset($item_updated['presentation_4_image_4']);
            $to_integrate['page1_3_image_1'] = $item_updated['page_3_image_1']; unset($item_updated['page_3_image_1']);
            $to_integrate['page1_3_image_2'] = $item_updated['page_3_image_2']; unset($item_updated['page_3_image_2']);
            $to_integrate['page1_3_image_3'] = $item_updated['page_3_image_3']; unset($item_updated['page_3_image_3']);
            $to_integrate['page1_3_image_4'] = $item_updated['page_3_image_4']; unset($item_updated['page_3_image_4']);
            $to_integrate['page1_4_image_1'] = $item_updated['page_4_image_1']; unset($item_updated['page_4_image_1']);
            $to_integrate['page1_4_image_2'] = $item_updated['page_4_image_2']; unset($item_updated['page_4_image_2']);
            $to_integrate['page1_4_image_3'] = $item_updated['page_4_image_3']; unset($item_updated['page_4_image_3']);
            $to_integrate['page1_4_image_4'] = $item_updated['page_4_image_4']; unset($item_updated['page_4_image_4']);
            unset($item_updated['presentation_3_contenu3']);
            unset($item_updated['presentation_3_contenu4']);
            unset($item_updated['presentation_4_contenu3']);
            unset($item_updated['presentation_4_contenu4']);
            unset($item_updated['page2_3_contenu3']);
            unset($item_updated['page2_3_contenu4']);
            unset($item_updated['page2_4_contenu3']);
            unset($item_updated['page2_4_contenu4']);

            $this->Mdl_glissieres_dev->insert($item_updated);


            echo "ok -> ".$item->id_glissiere."<br/>";
        }
    }*/

}