<?php
class createusers_sqfaqsfqs extends CI_Controller{
    
    function __construct() {
        parent::__construct();
		$this->load->model("user") ;
                
        $this->load->library('session');
        $this->load->library('user_agent');

        $this->load->library('ion_auth');


        $this->load->model("SousRubrique") ;
        $this->load->model("mdlcadeau") ;
        $this->load->Model("Commercant");
        $this->load->Model("mdl_mail_activation_bon_plan");
        $this->load->model("mdlville") ;
        $this->load->model("AssCommercantAbonnement") ;
        $this->load->Model("Rubrique");
        $this->load->Model("SousRubrique");
        $this->load->Model("Ville");
        $this->load->Model("Abonnement");
        $this->load->Model("mdlstatut");
        $this->load->helper('captcha');
        $this->load->library('image_moo');
        $this->load->model("ion_auth_used_by_club");


        check_vivresaville_id_ville();


    }
    
    function index(){
        redirect("auth/login");
    }

    public function createparticuliers() {
        $qryParticuliers = $this->db->query("SELECT * FROM users");
        if($qryParticuliers->num_rows() > 0) {
            foreach ($qryParticuliers->result_array() as $row) {
                $particulierId = $row['IdUser'];
                $userName = $row['Login'];
                $userPassword = $row['Password']; 
                $userEmail = $row['Email']; 
                $additional_data = array(
                    'first_name' => $row['Prenom'],
                    'last_name' => $row['Nom'],
                    'phone' => $row['Telephone'],
                );	
                // Group 'Particuliers" is 2
                $userGroup = array('2'); 
                
                // Insert the user "particulier" in the ion_auth users table
                
                $userId = $this->ion_auth->register($userName, $userPassword, $userEmail, $additional_data, $userGroup);
                echo "user id $userId created for $userName with password $userPassword<br>";
                if ($userId != "") {
                    $updateUserTable = "UPDATE `users` SET `user_ionauth_id` = $userId WHERE `IdUser` ='".$particulierId."';";
                    $this->db->query($updateUserTable);
                }
            }
        }
    }

    public function createcommercants() {
        $data["info_to_show"] = "";

        $qryCommercants = $this->db->query("
        SELECT
        content_type_depositor.vid,
        content_type_depositor.nid,
        content_type_depositor.field_depositor_name_value,
        content_type_depositor.field_depositor_address_value,
        content_type_depositor.field_depositor_address_cont_value,
        content_type_depositor.field_depositor_city_nid,
        content_type_depositor.field_depositor_telephone_value,
        content_type_depositor.field_depositor_facsimile_value,
        content_type_depositor.field_depositor_mobile_value,
        content_type_depositor.field_depositor_website_url,
        content_type_depositor.field_depositor_website_title,
        content_type_depositor.field_depositor_website_attributes,
        content_type_depositor.field_depositor_twitter_url,
        content_type_depositor.field_depositor_twitter_title,
        content_type_depositor.field_depositor_twitter_attributes,
        content_type_depositor.field_depositor_facebook_url,
        content_type_depositor.field_depositor_facebook_title,
        content_type_depositor.field_depositor_facebook_attributes,
        content_type_depositor.field_depositor_category_value,
        content_type_depositor.field_depositor_information_value,
        content_type_depositor.field_depositor_pa_stock_value,
        content_type_depositor.field_depositor_password_value,
        content_type_depositor.field_depositor_newsletter_value,
        content_type_depositor.field_depositor_dm_name_value,
        content_type_depositor.field_depositor_dm_position_value,
        content_type_depositor.field_depositor_dm_telephone_value,
        content_type_depositor.field_depositor_email_email,
        content_type_depositor.field_depositor_dm_email_email,
        content_type_depositor.isActivated,
        content_type_depositor.field_depositor_login,
        content_type_depositor.field_depositor_module,
        content_type_depositor.field_depositor_resp,
        content_type_depositor.field_num,
        content_type_depositor.field_statut,
        content_type_city.IdVille,
        term_data.IdSousRubrique,
        sous_rubriques.IdRubrique,
        villes.CodePostal
        FROM
        content_type_depositor
        INNER JOIN content_type_city ON content_type_depositor.field_depositor_city_nid = content_type_city.nid
        LEFT OUTER JOIN content_type_event ON content_type_event.field_event_depositor_nid = content_type_depositor.nid
        LEFT OUTER JOIN term_data ON term_data.tid = content_type_event.field_event_category_value
        LEFT OUTER JOIN sous_rubriques ON term_data.IdSousRubrique = sous_rubriques.IdSousRubrique
        INNER JOIN villes ON villes.IdVille = content_type_city.IdVille
        GROUP BY content_type_depositor.nid
        ");


        if($qryCommercants->num_rows() > 0) {
            foreach ($qryCommercants->result_array() as $row) {

                $userName = $row['field_depositor_login'];
                $userPassword = $row['field_depositor_password_value'];
                $userEmail = $row['field_depositor_email_email'];
                $additional_data = array(
                    'first_name' => $row['field_depositor_name_value'],
                    'phone' => $row['field_depositor_telephone_value'],
                );	

                $userGroup = array('3'); // Group by default is "basic"


                // Insert the user "commercant" in the ion_auth users table
                $userId = $this->ion_auth->register($userName, $userPassword, $userEmail, $additional_data, $userGroup);
                echo "user id $userId created for $userName with password $userPassword<br>";

                //$ion_ath_error = $this->ion_auth->errors();

                if (isset($userId) && $userId != "" && $userId != false) {

                    //all commercant data
                    $objCommercant['user_ionauth_id'] = $userId;
                    $objCommercant['NomSociete'] = $row['field_depositor_name_value'];
                    $objCommercant['Adresse1'] = $row['field_depositor_address_value'];
                    $objCommercant['Adresse2'] = $row['field_depositor_address_cont_value'];
                    $objCommercant['IdVille'] = $row['IdVille'];
                    $objCommercant['CodePostal'] = $row['CodePostal'];
                    $objCommercant['TelFixe'] = $row['field_depositor_telephone_value'];
                    $objCommercant['TelMobile'] = $row['field_depositor_mobile_value'];
                    $objCommercant['Email'] = $row['field_depositor_email_email'];
                    $objCommercant['Civilite'] = "0";
                    $objCommercant['Nom'] = $row['field_depositor_dm_name_value'];
                    $objCommercant['Responsabilite'] = $row['field_depositor_resp'];
                    $objCommercant['TelDirect'] = $row['field_depositor_email_email'];
                    $objCommercant['Email_decideur'] = $row['field_depositor_email_email'];
                    $objCommercant['Login'] = $row['field_depositor_login'];
                    $objCommercant['Email_decideur'] = $row['field_depositor_email_email'];




                    // start create nom_url *******************************************************************************
                    $this->load->model("mdlville") ;
                    $commercant_url_ville = $this->mdlville->getVilleById($objCommercant["IdVille"]);
                    $commercant_url_nom_com = RemoveExtendedChars2($objCommercant["NomSociete"]);
                    $commercant_url_nom_ville = RemoveExtendedChars2($commercant_url_ville->Nom);
                    $commercant_url_nom = $commercant_url_nom_com."_".$commercant_url_nom_ville;
                    $commercant_url_nom = $this->Commercant->setUrlFormat($commercant_url_nom, 0);
                    $objCommercant["nom_url"] = $commercant_url_nom;
                    // end create nom_url *********************************************************************************


                    //record datecreation
                    $objCommercant["datecreation"] = time();


                    if ($objCommercant["IdVille"] == 0) {
                        echo "Error : IdVille umpty<br/>";
                    }
                    if (isset($row['IdRubrique']) && $row['IdRubrique']!="") $objAssCommercantRubrique["IdRubrique"] = $row['IdRubrique']; else $objAssCommercantRubrique["IdRubrique"] = "63";
                    if (isset($row['IdSousRubrique']) && $row['IdSousRubrique']!="") $objAssCommercantSousRubrique["IdSousRubrique"] = $row['IdSousRubrique']; else $objAssCommercantSousRubrique["IdSousRubrique"] = "4";

                    $this->load->Model("Abonnement");
                    $obj_abonnement_gratuit = $this->Abonnement->GetWhere(" type='gratuit' ");
                    if (isset($obj_abonnement_gratuit) && $obj_abonnement_gratuit!="0") $value_abonnement_sub_pro = $obj_abonnement_gratuit->IdAbonnement; else $value_abonnement_sub_pro = "1";

                    $objAssCommercantAbonnement["IdAbonnement"] = $value_abonnement_sub_pro;




                    /*if (isset($ion_ath_error) && $ion_ath_error!="") {

                        echo "Une erreur s'est produite durant l'enregistrement de vos données. Veuillez recommencer s'il vous plait.".$ion_ath_error."<br/>";


                    } else {*/

                        $this->load->Model("Commercant");
                        $IdInsertedCommercant = $this->Commercant->Insert($objCommercant);

                        $this->load->Model("AssCommercantRubrique");
                        $objAssCommercantRubrique["IdCommercant"] = $IdInsertedCommercant;
                        $this->AssCommercantRubrique->Insert($objAssCommercantRubrique);

                        $this->load->Model("AssCommercantSousRubrique");
                        $objAssCommercantSousRubrique["IdCommercant"] = $IdInsertedCommercant;
                        $this->AssCommercantSousRubrique->Insert($objAssCommercantSousRubrique);

                        $this->load->Model("AssCommercantAbonnement");
                        $objAssCommercantAbonnement["IdCommercant"] = $IdInsertedCommercant;
                        $this->AssCommercantAbonnement->Insert($objAssCommercantAbonnement);

                        if(!empty($IdInsertedCommercant)) {

                            echo "commercant ".$IdInsertedCommercant." ok<br/>";
                            //ajouter dans content_type_depositor IdCommercant & ionauthId ...
                            $updateUserTable = "UPDATE `content_type_depositor` SET `idUserIonauth` = '".$objCommercant['user_ionauth_id']."', `idCommercant` = '".$IdInsertedCommercant."' WHERE `nid` ='".$row['nid']."';";
                            $this->db->query($updateUserTable);
                        }
                        else {
                            echo "Une erreur s'est produite durant l\'enregistrement de vos données. Veuillez recommencer s\'il vous plait.<br/>";

                        }

                    //}


                }
                else
                {
                    echo "Error : erreur ionauth <br/>";
                }

            }
        }
    }
    
 }