<?php
class fidelisation extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->Model("mdlcadeau");
        $this->load->Model("mdlville");
        $this->load->Model("user");
        $this->load->Model("mdlbonplan");
        $this->load->Model("commercant");
		
		$this->load->library('session');

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");
        $this->load->model("mdl_card");

        $this->load->library('session');

        check_vivresaville_id_ville();

         
    }
    
    function index(){
      $this->fiche();
         
    }
    
    function fiche() {

        if (!$this->ion_auth->logged_in()) {
            $this->session->set_userdata("cur_uri", $this->uri->uri_string());
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }else {
            if($this->ion_auth->is_admin()){
                redirect('admin/home', 'refresh');
            }
        }

        $data = null;
        $date = date('Y-m-d');   
        
        if(isset($_POST["Idcadeau"])) {
            $Idcadeau = $_POST["Idcadeau"];
                $data["Idcadeau"] = $Idcadeau ;
        }
        
        //echo  $date ; exit();
        $listCadeau = $this->mdlcadeau->getCadeauFidelisation($date);    
        $data["listCadeau"] = $listCadeau ;

        $user_ion_auth = $this->ion_auth->user()->row();
        $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
        if ($iduser==null || $iduser==0 || $iduser==""){
            $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        }
        $data['IdUser'] = $iduser;
        
        $oClient = $this->user->getById_client($iduser);
        
        
        $data["UserCivilite"] = "";
        $data["UserPrenom"] = $user_ion_auth->last_name;
        $data["NbrPoints"] = $oClient->NbrPoints;
        $data["Adresse"] = "";
        $data["CodePostal"] = "";
        $IdVille = 0;
        if($IdVille != 0){
        $oVille = $this->mdlville->getVilleById($IdVille);
        $nomVille = $oVille->Nom;
        } else {
         $nomVille = "";
        }
        $data["nomVille"] = $nomVille ;
        
        $this->load->view("front/vwFidelisation",$data);
    }
    
    
    function verification_code_cadeaux() {
        $code_cadeaux_client = $this->input->post("code_cadeaux_client");
        $code_cadeaux_commercant = $this->input->post("code_cadeaux_commercant");
        $idclient = $this->input->post("IdUser");
        
        if (!isset($code_cadeaux_client) || !isset($code_cadeaux_commercant) || $code_cadeaux_client=="" || $code_cadeaux_commercant==""){
            redirect("front/fidelisation/");
           }
        
        $oClient = $this->user->getById_client($idclient);
        
        $data = null;
        $date = date('Y-m-d');   
        
        if(isset($_POST["Idcadeau"])) {
            $Idcadeau = $_POST["Idcadeau"];
                $data["Idcadeau"] = $Idcadeau ;
        }
        
        //echo  $date ; exit();
        $listCadeau = $this->mdlcadeau->getCadeauFidelisation($date);    
        $data["listCadeau"] = $listCadeau ;

        $user_ion_auth = $this->ion_auth->user()->row();
        $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
        if ($iduser==null || $iduser==0 || $iduser==""){
            $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        }
        $data['IdUser'] = $iduser;
        $data["UserCivilite"] = "";
        $data["UserPrenom"] = $user_ion_auth->last_name;
        $data["NbrPoints"] = $oClient->NbrPoints;
        $data["Adresse"] = "";
        $data["CodePostal"] = "";
        $IdVille = 0;
        if($IdVille != 0){
        $oVille = $this->mdlville->getVilleById($IdVille);
        $nomVille = $oVille->Nom;
        } else {
         $nomVille = "";
        }
        $data["nomVille"] = $nomVille ;
        
        
        
        
        $data_codes = array();   
        $data_codes[0] = $code_cadeaux_client;
        $data_codes[1] = $code_cadeaux_commercant;   
        $zResultvalidation = $this->mdlcadeau->isValideNum($data_codes);
        
        if (count($zResultvalidation) == 0) {
            $data['zResultvalidation'] = "Les codes que vous avez ajout&eacute;s ne correspondent pas ou sont d&eacute;j&agrave; utilis&eacute;s.<br/>Veuillez indiquer des codes corrects !";
        } else {
            
            $incrementation_poins = $oClient->NbrPoints + 10;
            $this->mdlcadeau->AjouterPointsClients($oClient->IdUser,$incrementation_poins);
            $this->mdlcadeau->ValiderNum($zResultvalidation->id);
            
            $oClient_apres = $this->user->getById_client($oClient->IdUser);
            $data["NbrPoints"] = $oClient_apres->NbrPoints;
            
            $data['zResultvalidation'] = "Les codes que vous avez ajout&eacute; ont &eacute;t&eacute; valid&eacute;s.<br/>Vous avez 10 points cadeaux en plus !<br/>Vous pouvez commander des cadeaux en fonction de vos points.";
        }
            
            
        $this->load->view("front/vwFidelisation", $data);    
        
    }
    
    
    function commande(){
        $idCadeau = $this->input->post("x_designation");
        $idclient = $this->input->post("IdUser");
        // echo $idCadeau."et".$client ;exit();
        $quantite = $this->input->post("x_quantite");
        
        if ($quantite == "") $quantite = 1;
        if (is_nan($quantite)== true) $quantite = 0; 
        
       //verify if cadeau points with quantite match user points
       $ObjCadeau = $this->mdlcadeau->GetById($idCadeau);
       $oClient = $this->user->getById($idclient);
       $oAdmin = $this->user->getById(1);
       if ($ObjCadeau && $oClient){
           if (($ObjCadeau->valeurpoints*$quantite) > $oClient->NbrPoints){
               redirect("front/cadeau/commande/0");
           } 
           else if (($ObjCadeau->valeurpoints*$quantite) <= $oClient->NbrPoints){
                $date = date('Y-m-d');    
                $data = array(        
                'Idclient' => $idclient,
                'Idcadeau' => $idCadeau,
                'Date' => $date ,
                'Quantité' => $quantite,
                );
                $this->db->insert('commande', $data);            
                $idCommande = $this->db->insert_id();
                $idCommandeCadeau = $this->mdlcadeau->getMaxIdcadeau();
                
                $zMessage = "Bonjour \n\n\n";
                $zMessage .= "Vous avez command&eacute; ".$quantite." (quantit&eacute;) de cadeau :\"".$ObjCadeau->Nomoffre."\" \n";
                $zMessage .= "D&eacute;scription du cadeau :\"".$ObjCadeau->Commentaire."\" \n\n";
                $zMessage .= "Votre commande a bien &eacute;t&eacute; enregistr&eacute;e.\n Vous allez &ecirc;tre contact&eacute; pas l'&eacute;quipe Privicarte ";
                $zTitre = "[Privicarte]:Votre commande cadeau Privicarte" ;

                $zMessage1 = "Bonjour \n\n\n";
                $zMessage1 .= "Un consommateur a command&eacute; ".$quantite." (quantit&eacute;) de cadeau :\"".$ObjCadeau->Nomoffre."\" \n";
                $zMessage1 .= "D&eacute;scription du cadeau :\"".$ObjCadeau->Commentaire."\" \n\n";
                $zMessage1 .= "Valider ou refuser la commande sur le lien :".site_url("front/cadeau/validation/".$idCommandeCadeau."/")." \n\n";
                $zTitre1 = "[Privicarte]:Validation commande cadeau Privicarte" ;

                $zContactEmail = $oClient->Email ;
                $zContactEmailAdmin = $oAdmin->Email ;
                $headers ="From: " . "privicarte" . "< Privicarte >\n" ;
                $headers .="Reply-To: privicarte \n";
                $headers .= "Bcc:randawilly@gmail.com \n"; // Copies cachées

                $headers1 ="From: " . $oClient->Nom . "<" . $oClient->Email. ">\n" ;
                $headers1 .="Reply-To:" . $oClient->Email." \n";
                $headers1 .= "Bcc:randawilly@gmail.com \n"; // Copies cachées


                //$headers .='Content-Type: text/plain; charset="iso-8859-1"'."\n";
                $headers .='Content-Type: text/plain; charset=utf-8'."\n";
                $headers .='Content-Transfer-Encoding: 8bit';
                /* $this->activationBonPlan($iNumUnique);*/
                if(mail($zContactEmail, $zTitre, $zMessage, $headers)){
                   mail($zContactEmailAdmin, $zTitre1, $zMessage1, $headers1);
                }else {
                          //echo "mail non envoye";//exit();
                } 
                
                
                redirect("front/cadeau/commande/1");
           } 
           else {
               redirect("front/cadeau/commande/0");
           };
       }
        
        
    }
    
    function demandeBonPlan($id_bonplan, $bonplan_type = '1', $bp_multiple_nbmax_client = '1', $bp_multiple_date_visit_client = "0000-00-00"){


        if ($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)) {

            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            $user_groups = $this->ion_auth->get_users_groups($user_ion_auth->id)->result();
            ////$this->firephp->log($user_groups, 'user_groups');
            if ($user_groups[0]->id==1) $typeuser = "1";
            else if ($user_groups[0]->id==2) $typeuser = "0";
            else if ($user_groups[0]->id==3 || $user_groups[0]->id==4 || $user_groups[0]->id==5) $typeuser = "COMMERCANT";

            $id_client = $iduser;
            $type_client = $typeuser;
                // assoc_client_bonplan
            $idMax = $this->mdlcadeau->getMaxNumMailBonPlan();
            $id = $idMax+1;
            //generate code_cadeaux_client
            $iNumUnique = $id.$id_client.$id_bonplan ;
            //generate code_cadeaux_commercant
            $iNumUnique_commercant = newAleatoireChaine(8);
            if (isset($bp_multiple_date_visit_client) && $bp_multiple_date_visit_client == "0000-00-00") $bp_multiple_date_visit_client = null;
            $data = array(        
            'id' => null,
            'id_client' => $id_client,
            'id_bonplan' => $id_bonplan ,
            'code_cadeaux_client' => $iNumUnique ,
            'code_cadeaux_commercant' => $iNumUnique_commercant ,
            'date_validation' => date('Y-m-d'),
            'valide' => "0",
            'date_visit' => $bp_multiple_date_visit_client ,
            'datetime_validation'=>date("Y-m-d H:i:s"),
            'nb_place' => $bp_multiple_nbmax_client ,
            'bonplan_type' => $bonplan_type 
            );

            if ($bp_multiple_date_visit_client!=null) $bp_multiple_date_visit_client = convert_Sqldate_to_Frenchdate($bp_multiple_date_visit_client);
            
            $this->mdlcadeau->demanderBonPlan($data);

            //decrementation nb bonplan 
            $this->mdlbonplan->decremente_quantite_bonplan($id_bonplan, $bp_multiple_nbmax_client);
        
             
            $oClient = $this->user->getById($id_client); 
              
            
            //echo $id_client.$UserType;exit();
            //print_r($oClient);exit();
            $zContactEmail = $oClient->Email;
            $LoginClient = $oClient->Login;
            $NomClient= $oClient->Nom;
            $obonPlan = $this->mdlbonplan->getById($id_bonplan);
            $commercant_id = $obonPlan->bonplan_commercant_id;
            $ionauth_user_pro = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($commercant_id);

            $obj_card_client_number = $this->mdl_card->getWhere(' id_ionauth_user = '.$oClient->user_ionauth_id.' LIMIT 1 ');
            log_message('error', 'num_id_card_virtual : '.$obj_card_client_number);
            if (!isset($obj_card_client_number) || count($obj_card_client_number)==0)
            $client_card_number = generate_client_card($user_ion_auth->id);
            else if (isset($obj_card_client_number[0]->num_id_card_virtual)) $client_card_number = $obj_card_client_number[0]->num_id_card_virtual; //----------------------- client card number
            else $client_card_number = "";

            $commercant = $this->commercant->GetById($commercant_id);
            $commercant_categ = $this->commercant->GetCatById($commercant_id);
            $oVille_com = $this->mdlville->getVilleById($commercant->IdVille);
            $zContactCom = $commercant->Email;
            
            if  ($obonPlan->bonplan_type == '2') {
                $validite_offre_bp = 'Offre valable une seule fois';
            } else $validite_offre_bp = 'Offre valable plusieurs fois';
            
            // EMAIL FOR CLIENT  ****************************************************************
            $zMessage = '<div style="font-family:Arial, Helvetica, sans-serif;">';
            $zMessage .= "<strong><h1>Sortez</h1></strong>\n\n";
            $zMessage .= "<p>";
            $zMessage .= "Votre Numero de Carte : ".$client_card_number;
            $zMessage .= "</p>";
            $zMessage .= "<p>";
            $zMessage .= "Votre Pr&eacute;nom : ".$oClient->Prenom."<br/>";
            $zMessage .= "Votre Nom : ".$oClient->Nom." <br/>";
            $zMessage .= "Votre Adresse : ".$oClient->Adresse."<br/>";
            $zMessage .= "Votre Ville : ".$oClient->NomVille."<br/>";
            $zMessage .= "Votre T&eacute;l&eacute;phone : ".$oClient->Telephone." / ".$oClient->Portable."<br/>";
            $zMessage .= "Votre Email : <a href='mailto:".$oClient->Email."'>".$oClient->Email."</a>";
            $zMessage .= "</p>";
            $zMessage .= "<p><strong>N&deg; du Mail : ".$iNumUnique."</strong></p>";
            $zMessage .= "<p>";
            $zMessage .= "<strong>Votre demande de r&eacute;servation de Bonplan est bien enregistr&eacute;e,</strong><br/>";
            $zMessage .= "<br/><strong>Celui ci vous a &eacute;t&eacute; conc&eacute;d&eacute; par :</strong><br/>";
            $zMessage .= "Nom du Commercant : ".$commercant->Nom." ".$commercant->Prenom."<br/>";
            $zMessage .= "Adresse du Commercant : ".$commercant->Adresse1." ".$commercant->Adresse2."<br/>";
            $zMessage .= "Code Postal : ".$commercant->CodePostal."<br/>";
            $zMessage .= "Ville : ".$oVille_com->Nom."<br/>";
            $zMessage .= "</p>";
            $zMessage .= "<p>";
            $zMessage .= "Ouverture : Uniquement sur Rendez Vous<br/>";
            $zMessage .= "T&eacute;l&eacute;phone : ".$commercant->TelFixe." / ".$commercant->TelDirect." / ".$commercant->TelMobile."<br/>";
            $zMessage .= "Email : ".$commercant->Email."<br/>";
            $zMessage .= "Site internet du Commercant : <a href='".$commercant->SiteWeb."'>".$commercant->SiteWeb."</a><br/>";
            $zMessage .= "</p>";
            $zMessage .= "<p>";
            if (isset($commercant_categ)) $zMessage .= "Dans la cat&eacute;gorie : ".$commercant_categ->rubrique." - ".$commercant_categ->sous_rubrique."<br/>";
            $zMessage .= "<strong>Votre avantage : ".htmlentities($obonPlan->bonplan_titre, ENT_QUOTES, "UTF-8")."</strong><br/>";
            if  ($obonPlan->bonplan_type == '2') {
            	$zMessage .= "<strong>Condition d\'utilisation : Offre valable du ".translate_date_to_fr($obonPlan->bp_unique_date_debut)." au ".translate_date_to_fr($obonPlan->bp_unique_date_fin)." - ".$validite_offre_bp."</strong><br/>";
        	} else if  ($obonPlan->bonplan_type == '2')  {
        		$zMessage .= "<strong>Condition d\'utilisation : Offre valable du ".translate_date_to_fr($obonPlan->bp_multiple_date_debut)." au ".translate_date_to_fr($obonPlan->bp_multiple_date_fin)." - ".$validite_offre_bp."</strong><br/>";
        	}
            $zMessage .= "</p>";
            $zMessage .= "<p>";
            $zMessage .= "Nous vous conseillons de contacter notre diffuseur avant tout d&eacute;placement afin qu\'il vous r&eacute;serve le meilleur accueil.<br/>";
            $zMessage .= "Date pr&eacute;vue de votre visite : ".$bp_multiple_date_visit_client."<br/>";
            $zMessage .= "Nombre de place de votre r&eacute;servation : ".$bp_multiple_nbmax_client."<br/>";
            $zMessage .= "</p>";
            $zMessage .= "<p>";
            $zMessage .= "Pour information, un double de ce mail a &eacute;t&eacute; adress&eacute; &agrave; notre diffuseur.";
            $zMessage .= "</p>";
            $zMessage .= "<p>";
            $zMessage .= "<strong>Nous vous rappelons aussi que cette offre est valable 15 jours &agrave; partir de la date de reception de ce Mail.</strong><br/>";
            $zMessage .= "<strong>N\'oubliez pas d\'imprimer ce Mail et de le pr&eacute;senter &agrave; notre diffuseur.</strong><br/>";
            //$zMessage .= "<strong>Les points cadeaux : n'oubliez pas de récupérer le Mail du commercant, il vous sera utile pour ajouter 10 points sur votre capital-points.</strong><br/>";
            $zMessage .= "</p>";
            $zMessage .= "<p>L'&eacute;quipe Sortez vous souhaite une bonne utilisation de cet avantage </p>";
            $zMessage .= '</div>';
            $zTitre = "[Sortez] Votre code de validation Bon Plan" ;




            
            // EMAIL FOR COMMERCANT ********************************************************************
            $zMessage1 = '<div style="font-family:Arial, Helvetica, sans-serif;">';
            $zMessage1 .= "<strong><h1>Sortez</h1></strong>\n\n";
            $zMessage1 .= "<p>";
            $zMessage1 .= "Numero de la carte du client : ".$client_card_number;
            $zMessage1 .= "</p>";
            $zMessage1 .= "Nom : ".$commercant->Nom."<br/>";
            $zMessage1 .= "Pr&eacute;nom : ".$commercant->Prenom."<br/>";
            $zMessage1 .= "Soci&eacute;t&eacute; : ".$commercant->NomSociete."<br/>";
            $zMessage1 .= "<p><strong>N&deg; du Mail : ".$iNumUnique."</strong></p>";
            $zMessage1 .= "<p>";
            $zMessage1 .= "<strong>Ceci est une demande de r&eacute;servation &agrave; votre Bonplan par l\'un de nos adh&eacute;rents,</strong><br/>";
            $zMessage1 .= "<br/><strong>Nom du Client :</strong> ".$oClient->Nom." ".$oClient->Prenom."<br/>";
            $zMessage1 .= "<strong>Adresse du Client :</strong> ".$oClient->Adresse."<br/>";
            $zMessage1 .= "<strong>Code postal :</strong> ".$oClient->CodePostal."<br/>";
            $zMessage1 .= "<strong>Ville :</strong> ".$oClient->NomVille."<br/>";
            $zMessage1 .= "<strong>T&eacute;l&eacute;phone du Client :</strong> ".$oClient->Telephone." / ".$oClient->Portable."<br/>";
            $zMessage1 .= "<strong>Email du Client :</strong> <a href='mailto:".$oClient->Email."'>".$oClient->Email."</a>";
            $zMessage1 .= "</p>";

            $zMessage1 .= "<p>";
            $zMessage1 .= "<strong>D&eacute;tails de la r&eacute;servation du client : <br/></strong>";
            $zMessage1 .= "Date de visite pr&eacute;vue : ".$bp_multiple_date_visit_client."<br/>";
            $zMessage1 .= "Nombre de place r&eacute;serv&eacute;e : ".$bp_multiple_nbmax_client."<br/>";
            $zMessage1 .= "</p>";

            $zMessage1 .= "<p>";
            $zMessage1 .= "<strong>Nous vous rappelons vos engagements :</strong><br/><br/>";
            if (isset($commercant_categ)) $zMessage1 .= "<strong>Dans la cat&eacute;gorie :</strong> ".$commercant_categ->rubrique." - ".$commercant_categ->sous_rubrique."<br/><br/>";
            $zMessage1 .= "<strong>Votre Bon plan :</strong> ".htmlentities($obonPlan->bonplan_titre, ENT_QUOTES, "UTF-8")."<br/><br/>";
            $zMessage1 .= "<strong>Condition d\'utilisation :</strong> ".$commercant->Conditions."<br/><br/>";
            $zMessage1 .= "Lors de son arriv&eacute; dans votre &eacute;tablissement, le b&eacute;n&eacute;ficiaire vous pr&eacute;sente l\'email adress&eacute; par Sortez (impression ou pr&eacute;sent sur son smartphone).<br/>";
            //$zMessage1 .= "<strong>Les points cadeaux : lors de la concr&eacute;tisation de son achat, vous lui remettez votre propre email, ce qui lui permettra sur son compte personnaliser de rajouter 10 points sur son capital.</strong> <br/>";
            $zMessage1 .= "</p>";
            //$zMessage1 .= "<p>";
            //$zMessage1 .= "<strong>Code de confirmation pour le Consommateur : ". $iNumUnique_commercant."</strong>";
            //$zMessage1 .= "</p>";
            $zMessage1 .= "<p>L\'&eacute;quipe Sortez vous souhaite une bonne journ&eacute;e </p>";
            $zMessage1 .= '</div>';
            $zTitre1 = "[Sortez] Code de mail bon plan unique d\'un client pour acces bon plan" ;
            
            $zContactEmail_ = array();
            $zContactEmail_[] = array("Email"=>$zContactEmail,"Name"=>$NomClient);

            $zContactEmailCom_ = array();
            $zContactEmailCom_[] = array("Email"=>$zContactCom,"Name"=>$commercant->NomSociete);

            $zContactEmailBP_ = array();
            $zContactEmailBP_[] = array("Email"=>"bonplan@sortez.org","Name"=>"Sortez Bon Plan");

            
            $obj_bonplan = $this->mdlbonplan->getById($id_bonplan);
            $obj_commercant = $this->commercant->GetById($obj_bonplan->bonplan_commercant_id);
            //$url_to_redirect = base_url().$obj_commercant->nom_url."/notre_bonplan/bonplan_valide";
            $url_to_redirect = base_url().$obj_commercant->nom_url."/notre_bonplan/".$id_bonplan."/bonplan_valide";


            // $this->activationBonPlan($iNumUnique);
            if(@envoi_notification($zContactEmail_,$zTitre,html_entity_decode(htmlentities($zMessage)))) {
                @envoi_notification($zContactEmailCom_,$zTitre1,html_entity_decode(htmlentities($zMessage1)));
                @envoi_notification($zContactEmailBP_,$zTitre1,html_entity_decode(htmlentities($zMessage1)));
             
                //activationBonPlan($iNumUnique);
                //redirect("front/cadeau");
                //redirect("front/commercant/listeBonPlanParCommercant/".$commercant_id."/1");
                redirect($url_to_redirect);
            }else {
                      echo "mail non envoye";//exit();
                     
            }
            //redirect("front/commercant/listeBonPlanParCommercant/".$commercant_id."/1");
            redirect($url_to_redirect);
        
        } else if ($this->ion_auth->logged_in()) {

            $obj_bonplan = $this->mdlbonplan->getById($id_bonplan);
            $obj_commercant = $this->commercant->GetById($obj_bonplan->bonplan_commercant_id);
            //$url_to_redirect = base_url().$obj_commercant->nom_url."/notre_bonplan";
            $url_to_redirect = base_url().$obj_commercant->nom_url."/notre_bonplan/".$id_bonplan;
            //$last_url = base_url()."front/fidelisation/demandeBonPlan/".$id_bonplan."/".$bonplan_type."/".$bp_multiple_nbmax_client."/".$bp_multiple_date_visit_client;
            redirect($url_to_redirect);

        } else if (!$this->ion_auth->logged_in()) {
            $this->session->set_userdata("cur_uri", $this->uri->uri_string());
            $this->session->set_flashdata('domain_from', '1');
            //$last_url = base_url()."front/fidelisation/demandeBonPlan/".$id_bonplan;
            $last_url = base_url()."front/fidelisation/demandeBonPlan/".$id_bonplan."/".$bonplan_type."/".$bp_multiple_nbmax_client."/".$bp_multiple_date_visit_client;
            $this->session->set_flashdata('last_url', $last_url);
			//redirect("auth/login/?blackbackground=1");
            redirect("auth/login/");
        }

   }    
   
   function activationBonPlan($iNumUnique){
    $ObjAssocclentBonPlan = $this->mdlcadeau->isValideNum($iNumUnique);
     if($ObjAssocclentBonPlan){
        $id =$ObjAssocclentBonPlan->id;
        $id_client =$ObjAssocclentBonPlan->id_client;
        $oClient = $this->user->getById($id_client);
        $NbrPoints = $oClient->NbrPoints+10;
        $this->mdlcadeau->ValiderNum($id);
        $this->mdlcadeau->AjouterPointsClients($id_client,$NbrPoints);        
        redirect("front/cadeau");
     }
   }
   
   function verification_point_cadeau($id_Cadeau=0, $id_user=0){
       $ObjCadeau = $this->mdlcadeau->GetById($id_Cadeau);
       $oClient = $this->user->getById($id_user);
       if ($ObjCadeau && $oClient){
           if ($ObjCadeau->valeurpoints > $oClient->NbrPoints){
               echo "<span style='color:#F00;'><p>Ce cadeau nécessite <strong>".$ObjCadeau->valeurpoints."</strong> points pour être commandé.<br/>Vos points ne vous permettent pas d'en commander.</p></span>";
           } else if ($ObjCadeau->valeurpoints <= $oClient->NbrPoints){
               echo "<span style='color:#6C3;'><p>Ce cadeau nécessite <strong>".$ObjCadeau->valeurpoints."</strong> points pour être commandé.<br/>Vous pouvez en commander.</p></span>";
           } else echo "";
       }
       
       //echo "OK ".$id_Cadeau." ".$id_user;
   }
}