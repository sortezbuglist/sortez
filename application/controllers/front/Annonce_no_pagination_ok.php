<?php
class annonce extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model("mdlannonce") ;
        $this->load->model("mdlcommercant") ;
        $this->load->model("mdlbonplan") ;
        $this->load->model("rubrique") ;
        $this->load->model("sousrubrique") ;
        $this->load->model("mdlimagespub") ;
        $this->load->model("AssCommercantAbonnement") ;
        $this->load->model("mdlville") ;
        $this->load->library('user_agent');
        $this->load->model("Abonnement");

        $this->load->library('session');

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");
    }
    
    function index_old(){
                if ($this->input->post("inputStringHidden") == "0") unset($_SESSION['iCategorieId']);
                if ($this->input->post("inputStringCommercantHidden") == "0") unset($_SESSION['iCommercantId']);
                if ($this->input->post("zMotCle") == "") unset($_SESSION['zMotCle']);
        
		if(isset($_POST["inputStringHidden"])){
			$iCategorieId = $_POST["inputStringHidden"] ; 
			$iCommercantId = $_POST["inputStringCommercantHidden"] ; 
			$zMotCle = $_POST["zMotCle"] ;
			$iEtat = $_POST["iEtat"] ;
                        
                        $data['iCategorieId'] = $iCategorieId ;
                        $data['iCommercantId'] = $iCommercantId ;
                        $data['zMotCle'] = $zMotCle ;
                        $data['iEtat'] = $iEtat ;
                        
			$toListeAnnonce = $this->mdlannonce->listeAnnonceRecherche($iCategorieId, $iCommercantId, $zMotCle, $iEtat) ;
		}
		else{
			$toListeAnnonce = $this->mdlannonce->listeAnnonce() ;
		}
		$toSousRubrique= $this->mdlannonce->GetAnnonceSouscategorie();
		$data['toSousRubrique'] = $toSousRubrique ;
		$toCommercant= $this->mdlcommercant->GetAllCommercant_with_annonce();
		$data['toCommercant'] = $toCommercant ;
		$data['toListeAnnonce'] = $toListeAnnonce ;
		$data['mdlannonce'] = $this->mdlannonce ;
                $this->load->view('front/vwListeAnnonce', $data) ;
    }
    
    
    
    function index(){
        $argOffset = (isset($_SESSION["argOffset"])) ? $_SESSION["argOffset"] : 0 ;
        
        if ($this->input->post("inputStringHidden") == "0") unset($_SESSION['iCategorieId']);
        if ($this->input->post("inputStringCommercantHidden") == "0") unset($_SESSION['iCommercantId']);
        if ($this->input->post("zMotCle") == "") unset($_SESSION['zMotCle']);
        if ($this->input->post("inputStringVilleHidden") == "") unset($_SESSION['iVilleId']);
        if ($this->input->post("inputStringOrderByHidden") == "0") unset($_SESSION['iOrderBy']);
        if ($this->input->post("inputFromGeolocalisation") == "0" || $this->input->post("inputFromGeolocalisation") == "1") unset($_SESSION['inputFromGeolocalisation']);
        if ($this->input->post("inputGeolocalisationValue") == "10") unset($_SESSION['inputGeolocalisationValue']);
        if ($this->input->post("inputGeoLatitude") == "0") unset($_SESSION['inputGeoLatitude']);
        if ($this->input->post("inputGeoLongitude") == "0") unset($_SESSION['inputGeoLongitude']);
        
        
        $data["iFavoris"] = $this->input->post("hdnFavoris");
        
        $PerPage = 15;

        
        if(isset($_POST["inputStringHidden"])){
            unset($_SESSION['iCategorieId']);
            unset($_SESSION['iVilleId']);
            unset($_SESSION['iCommercantId']);
            unset($_SESSION['zMotCle']);
            unset($_SESSION['iOrderBy']);
            unset($_SESSION['inputFromGeolocalisation']);
            unset($_SESSION['inputGeolocalisationValue']);
            unset($_SESSION['inputGeoLatitude']);
            unset($_SESSION['inputGeoLongitude']);
            
            //$iCategorieId = $_POST["inputStringHidden"] ; 
            $iCategorieId_all0 = $this->input->post("inputStringHidden");
            if (isset($iCategorieId_all0) && $iCategorieId_all0 != "" && $iCategorieId_all0 != NULL && $iCategorieId_all0 != '0') {
                $iCategorieId_all = substr($iCategorieId_all0,1);
                $iCategorieId = explode(',', $iCategorieId_all);
            } else {
                $iCategorieId = '0';
            }
            if (isset($_POST["inputStringVilleHidden"])) $iVilleId = $_POST["inputStringVilleHidden"] ; else  $iVilleId = 0;
            if (isset($_POST["inputStringCommercantHidden"])) $iCommercantId = $_POST["inputStringCommercantHidden"] ; else $iCommercantId = 0;
            if (isset($_POST["zMotCle"])) $zMotCle = $_POST["zMotCle"] ; else $zMotCle = '';
            if (isset($_POST["iEtat"])) $iEtat = $_POST["iEtat"] ; else $iEtat = '';
            if (isset($_POST["inputStringOrderByHidden"])) $iOrderBy = $_POST["inputStringOrderByHidden"] ; else $iOrderBy = '';
            if (isset($_POST["inputFromGeolocalisation"])) $inputFromGeolocalisation = $_POST["inputFromGeolocalisation"] ; else $inputFromGeolocalisation = "0";
            if (isset($_POST["inputGeolocalisationValue"])) $inputGeolocalisationValue = $_POST["inputGeolocalisationValue"] ; else $inputGeolocalisationValue = "10";
            if (isset($_POST["inputGeoLatitude"])) $inputGeoLatitude = $_POST["inputGeoLatitude"] ; else $inputGeoLatitude = "0";
            if (isset($_POST["inputGeoLongitude"])) $inputGeoLongitude = $_POST["inputGeoLongitude"] ; else $inputGeoLongitude = "0";
            
            $_SESSION['iCategorieId'] = $iCategorieId;
            $_SESSION['iVilleId'] = $iVilleId;
            $_SESSION['iCommercantId'] = $iCommercantId;
            $_SESSION['zMotCle'] = $zMotCle;
            $_SESSION['iEtat'] = $iEtat;
            $_SESSION['iOrderBy'] = $iOrderBy;
            $_SESSION['inputFromGeolocalisation'] = $inputFromGeolocalisation;
            $_SESSION['inputGeolocalisationValue'] = $inputGeolocalisationValue;
            $_SESSION['inputGeoLatitude'] = $inputGeoLatitude;
            $_SESSION['inputGeoLongitude'] = $inputGeoLongitude;
            
            $data['iCategorieId'] = $iCategorieId ;
            $data['iVilleId'] = $iVilleId ;
            $data['iCommercantId'] = $iCommercantId ;
            $data['zMotCle'] = $zMotCle ;
            $data['iOrderBy'] = $iOrderBy ;
            $toListeAnnonce = $this->mdlannonce->listeAnnonceRecherche($_SESSION['iCategorieId'], $_SESSION['iCommercantId'], $_SESSION['zMotCle'], $_SESSION['iEtat'], $argOffset, $PerPage, $_SESSION['iVilleId'], $_SESSION['iOrderBy'], $_SESSION['inputFromGeolocalisation'], $_SESSION['inputGeolocalisationValue'], $_SESSION['inputGeoLatitude'], $_SESSION['inputGeoLongitude']) ;
            $TotalRows = count($this->mdlannonce->listeAnnonceRecherche($_SESSION['iCategorieId'], $_SESSION['iCommercantId'], $_SESSION['zMotCle'], $_SESSION['iEtat'], 0, 10000, $_SESSION['iVilleId'], $_SESSION['iOrderBy'], $_SESSION['inputFromGeolocalisation'], $_SESSION['inputGeolocalisationValue'], $_SESSION['inputGeoLatitude'], $_SESSION['inputGeoLongitude']));
            
        }else{
            $data["iFavoris"] = "";
            $iCategorieId = (isset($_SESSION['iCategorieId'])) ? $_SESSION['iCategorieId'] : 0 ;
            $iVilleId = (isset($_SESSION['iVilleId'])) ? $_SESSION['iVilleId'] : 0 ;
            $iCommercantId = (isset($_SESSION['iCommercantId'])) ? $_SESSION['iCommercantId'] : "" ;
            $zMotCle = (isset($_SESSION['zMotCle'])) ? $_SESSION['zMotCle'] : "" ;
            $iEtat = (isset($_SESSION['iEtat'])) ? $_SESSION['iEtat'] : "" ;
            $iOrderBy = (isset($_SESSION['iOrderBy'])) ? $_SESSION['iOrderBy'] : "" ;
            $inputFromGeolocalisation = (isset($_SESSION['inputFromGeolocalisation'])) ? $_SESSION['inputFromGeolocalisation'] : "0" ;
            $inputGeolocalisationValue = (isset($_SESSION['inputGeolocalisationValue'])) ? $_SESSION['inputGeolocalisationValue'] : "10" ;
            $inputGeoLatitude = (isset($_SESSION['inputGeoLatitude'])) ? $_SESSION['inputGeoLatitude'] : "0" ;
            $inputGeoLongitude = (isset($_SESSION['inputGeoLongitude'])) ? $_SESSION['inputGeoLongitude'] : "0" ;
            $toListeAnnonce = $this->mdlannonce->listeAnnonceRecherche($iCategorieId, $iCommercantId, $zMotCle, $iEtat, $argOffset, $PerPage, $iVilleId, $iOrderBy, $inputFromGeolocalisation, $inputGeolocalisationValue, $inputGeoLatitude, $inputGeoLongitude);
            $TotalRows = count($this->mdlannonce->listeAnnonceRecherche($iCategorieId, $iCommercantId, $zMotCle, $iEtat, 0, 10000, $iVilleId, $iOrderBy, $inputFromGeolocalisation, $inputGeolocalisationValue, $inputGeoLatitude, $inputGeoLongitude));
        }
        
        
        
        $iNombreLiens = $TotalRows / $PerPage ;
        if($iNombreLiens > round($iNombreLiens)){
                $iNombreLiens = round($iNombreLiens) + 1 ;
        }
        else {
                $iNombreLiens = round($iNombreLiens) ;
        }

        $data["iNombreLiens"] = $iNombreLiens ;

        $data["PerPage"] = $PerPage ;
        $data["TotalRows"] = $TotalRows ;
        $data["argOffset"] = $argOffset ;
        $argOffset_limit = round($argOffset) + $PerPage;
        
        
        $toSousRubrique= $this->mdlannonce->GetAnnonceSouscategorie();
        $data['toSousRubrique'] = $toSousRubrique ;
        $toRubriquePrincipale= $this->mdlannonce->GetAnnonceCategoriePrincipale();
        $data['toRubriquePrincipale'] = $toRubriquePrincipale ;
        $toCommercant= $this->mdlcommercant->GetAllCommercant_with_annonce();
        $data['toCommercant'] = $toCommercant ;
        $data['toListeAnnonce'] = $toListeAnnonce ;
        $data['mdlannonce'] = $this->mdlannonce ;
        $data['pagecategory'] = 'annonce';

        $this->load->model("mdlville") ;
        $toVille= $this->mdlville->GetAnnonceVilles();
        $data['toVille'] = $toVille ;

        
        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;
        
        
        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            $data['IdUser'] = $iduser;
            $user_groups = $this->ion_auth->get_users_groups($user_ion_auth->id)->result();
            //$this->firephp->log($user_groups, 'user_groups');
            if ($user_groups[0]->id==1) $typeuser = "1";
            else if ($user_groups[0]->id==2) $typeuser = "0";
            else if ($user_groups[0]->id==3 || $user_groups[0]->id==4 || $user_groups[0]->id==5) $typeuser = "COMMERCANT";
            $data['typeuser'] = $typeuser;
        }
        
        
        $this->load->view('front2013/annonce_main', $data) ;//here
    }
    
    
    function redirection($_iPage){
        $_SESSION["argOffset"] = $_iPage ;
        redirect("front/annonce/");
    }
    
    function redirect_annonce(){
            unset($_SESSION['iCategorieId']);
            unset($_SESSION['iCommercantId']);
            unset($_SESSION['iVilleId']);
            unset($_SESSION['zMotCle']);
            unset($_SESSION['iEtat']);
            redirect("front/annonce/");
    }
    
    function check_category_list() {
            $inputStringVilleHidden_annonces = $this->input->post("inputStringVilleHidden_annonces");
            
            //$toCategorie= $this->mdlcategorie->GetCommercantSouscategorie();
            //$toVille= $this->mdlville->GetCommercantVilles();//get ville list of commercant
            if (isset($inputStringVilleHidden_annonces) && $inputStringVilleHidden_annonces != "" && $inputStringVilleHidden_annonces != '0' && $inputStringVilleHidden_annonces != NULL) {
                $toCategorie_principale= $this->mdlannonce->GetAnnonceCategoriePrincipaleByVille($inputStringVilleHidden_annonces);
            } else {
                $toCategorie_principale= $this->mdlannonce->GetAnnonceCategoriePrincipale();
            }

            //$this->firephp->log($toCategorie_principale, 'toCategorie_principale');

            $result_to_show = '';
            
            if (isset($toCategorie_principale)) {
                foreach($toCategorie_principale as $oCategorie_principale){
                    $result_to_show .= '<div class="leftcontener2013title" style="margin-top:15px;">';
                    $result_to_show .= ucfirst(strtolower($oCategorie_principale->Nom))." (".$oCategorie_principale->nb_annonce.")";
                    $result_to_show .= '</div>';

                    $result_to_show .= '<div class="leftcontener2013content" style="margin-bottom:15px;">';
                    $OCommercantSousRubrique = $this->mdlannonce->GetAnnonceSouscategorieByRubrique($oCategorie_principale->IdRubrique);
                    $result_to_show .= '<br/>';
                    
                    
                    if (isset($_SESSION['iCategorieId'])) {
                        $iCategorieId_sess = $_SESSION['iCategorieId'];
                        //$this->firephp->log($iCategorieId_sess, 'iCategorieId_sess');
                    }
                    
                    
                    if (count($OCommercantSousRubrique)>0) {
                        $i_rand = 0;
                        foreach($OCommercantSousRubrique as $ObjCommercantSousRubrique){
                            $result_to_show .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>';
                            $result_to_show .= '<td width="22"><input  onClick="javascript:submit_search_annonce();" name="check_part['.$i_rand.']" id="check_part_'.$ObjCommercantSousRubrique->IdSousRubrique.'" type="checkbox" value="'.$ObjCommercantSousRubrique->IdSousRubrique.'"';
                            if (isset($iCategorieId_sess) && is_array($iCategorieId_sess)) {
                                if (in_array($ObjCommercantSousRubrique->IdSousRubrique, $iCategorieId_sess)) $result_to_show .= 'checked';
                            }
                            $result_to_show .= '></td><td>';
                            $result_to_show .= ucfirst(strtolower($ObjCommercantSousRubrique->Nom)).' ('.$ObjCommercantSousRubrique->nb_annonce.')</td>';
                            $result_to_show .= '</tr></table>';
                            $i_rand++;
                        }
                    }
                    $result_to_show .= '</div>';
                }
            }
            
            echo $result_to_show;
            
        }
    
    
    
	function detailAnnonce($_iAnnonceId){
            
        $_iAnnonceId = $this->uri->rsegment(3); //die($_iAnnonceId." stop");//reecuperation valeur $_iAnnonceId
            
        $oDetailAnnonce = $this->mdlannonce->detailAnnonce($_iAnnonceId) ;
        $data['oDetailAnnonce'] = $oDetailAnnonce ;

        $_iCommercantId = $oDetailAnnonce->id_commercant;

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
        $data['oInfoCommercant'] = $oInfoCommercant ;
        $data['active_link'] = "annonces";
        $data['active_link2'] = "detailannonce";
        $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user") ;
        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser=0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));
        $data['pagecategory_partner'] = 'details_annonces_partner';


        if (isset($_POST['text_mail_form_module_detailannnonce'])) {
            $text_mail_form_module_detailannnonce = $this->input->post("text_mail_form_module_detailannnonce") ;
            $nom_mail_form_module_detailannnonce = $this->input->post("nom_mail_form_module_detailannnonce") ;
            $tel_mail_form_module_detailannnonce = $this->input->post("tel_mail_form_module_detailannnonce") ;
            $email_mail_form_module_detailannnonce = $this->input->post("email_mail_form_module_detailannnonce") ;

            $colDestAdmin = array();
            $colDestAdmin[] = array("Email"=>$oInfoCommercant->Email,"Name"=>$oInfoCommercant->Nom." ".$oInfoCommercant->Prenom);

            // Sujet
            $txtSujetAdmin = "Demande d'information sur une annonce Privicarte";

            $txtContenuAdmin = "
            <p>Bonjour ,</p>
            <p>Une demande de contact vient du site ClubProxmité à propos d'une annonce que vous avez posté :</p>
            <p>Détails de l'annonce :<br/>
            Désignation : ".$oDetailAnnonce->texte_courte."<br/>
            N° : ".ajoutZeroPourString($oDetailAnnonce->annonce_id, 6)." du ".convertDateWithSlashes($oDetailAnnonce->annonce_date_debut)."
            Prix de vente : ".$oDetailAnnonce->ancien_prix."<br/></p><p>
            Nom Client : ".$nom_mail_form_module_detailannnonce."<br/>
            Téléphone Client : ".$tel_mail_form_module_detailannnonce."<br/>
            Email Client : ".$email_mail_form_module_detailannnonce."<br/>
            Demande Client :<br/>
            ".$text_mail_form_module_detailannnonce."<br/><br/>
            </p>";

            @envoi_notification($colDestAdmin,$txtSujetAdmin,$txtContenuAdmin);
            $data['mssg_envoi_module_detail_annonce'] = '<font color="#00CC00">Votre demande est envoyée</font>';
            //$data['mssg_envoi_module_detail_annonce'] = $txtContenuAdmin;

        } else $data['mssg_envoi_module_detail_annonce'] = '';



            //$this->load->view('front/vwDetailAnnonce', $data) ;
                $this->load->view('front2013/detailsannonce', $data) ;
	}
        
        function photosannoncemobile ($_iAnnonceId){
		$oDetailAnnonce = $this->mdlannonce->detailAnnonce($_iAnnonceId) ;
		$data['oDetailAnnonce'] = $oDetailAnnonce ;
                
                $_iCommercantId = $oDetailAnnonce->id_commercant;
                
                $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
		$data['oInfoCommercant'] = $oInfoCommercant ;
                $data['active_link'] = "annonces";
                $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
                if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
                $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
                $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
                if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;
                
                $is_mobile = $this->agent->is_mobile();
                //test ipad user agent
                $is_mobile_ipad = $this->agent->is_mobile('ipad');
                $data['is_mobile_ipad'] = $is_mobile_ipad;
                $is_robot = $this->agent->is_robot();
                $is_browser = $this->agent->is_browser();
                $is_platform = $this->agent->platform();
                $data['is_mobile'] = $is_mobile;
                $data['is_robot'] = $is_robot;
                $data['is_browser'] = $is_browser;
                $data['is_platform'] = $is_platform;
                
		//$this->load->view('front/vwPhotosannoncemobile', $data) ;
                $this->load->view('front2013/photosannoncemobile', $data) ;
	}
        
	function nousContacterAnnonce($_iAnnonceId, $_iCommercantId){
            
        $_iAnnonceId = $this->uri->rsegment(3);//die(" stop");//reecuperation valeur $_iAnnonceId
        $nom_url_commercant = $this->uri->rsegment(4);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
        $data['oInfoCommercant'] = $oInfoCommercant ;
        $data['iAnnonceId'] = $_iAnnonceId ;

        $data['active_link'] = "contacter";

        $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);

        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

        $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;



        $this->load->model("user") ;
        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser=0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));
        $data['pagecategory_partner'] = 'nous_contacter';

        //$this->load->view('front/vwNousContacterAnnonce', $data) ;
        $this->load->view('front2013/nouscontacterpartenaire', $data) ;
	}
	function photoAnnonce($_iAnnonceId){
		$oDetailAnnonce = $this->mdlannonce->detailAnnonce($_iAnnonceId) ;
		$data['oDetailAnnonce'] = $oDetailAnnonce ;
		$this->load->view('front/vwPhotoAnnonce', $data) ;
	}
	function menuannonce($_iCommercantId, $_iAnnonceId){
		$data['toListeAnnonceParCommercant'] = $this->mdlannonce->listeAnnonceParCommercant($_iCommercantId) ;
		$oDetailAnnonce = $this->mdlannonce->detailAnnonce($_iAnnonceId) ;
		$data['oDetailAnnonce'] = $oDetailAnnonce ;
                $this->load->view('front/vwMenuAnnonce', $data) ;
	}
	function ficheCommercantAnnonce($_iCommercantId){
            
        $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $numeric_verif_1 = preg_match("/^[0-9]+$/", $nom_url_commercant);

        if ($numeric_verif_1 == 0) {
            $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);
        } else {
            $_iCommercantId = $nom_url_commercant;
        }
                
            
		$oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
		$nbPhoto =0;
		if($oInfoCommercant->Photo1!="" && $oInfoCommercant->Photo1!=null){
		  $nbPhoto+=1;		  
		}
		if($oInfoCommercant->Photo2!="" && $oInfoCommercant->Photo2!=null){
		  $nbPhoto+=1;		  
		}
		if($oInfoCommercant->Photo3!="" && $oInfoCommercant->Photo3!=null){
		  $nbPhoto+=1;		  
		}
		if($oInfoCommercant->Photo4!="" && $oInfoCommercant->Photo4!=null){
		  $nbPhoto+=1;		  
		}
		if($oInfoCommercant->Photo5!="" && $oInfoCommercant->Photo5!=null){
		  $nbPhoto+=1;		  
		}
		
		$data['oInfoCommercant'] = $oInfoCommercant ;
		$data['nbPhoto'] = $nbPhoto ;
		
		//$data['mdlannonce'] = $this->mdlannonce ;
		$data['mdlbonplan'] = $this->mdlbonplan ;
		
		$data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);	
		$oBonPlan = 	$this->mdlbonplan->bonPlanParCommercant($_iCommercantId);
		$data['nbBonPlan'] = sizeof($oBonPlan);	
//		$data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_titre : "";		// OP 27/11/2011
		$data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_texte : "";
		$data['oImagespub'] = $this->mdlimagespub->GetByImagespubActiv() ;
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

        $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $data['active_link'] = "accueil";

        $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);

        $is_mobile = $this->agent->is_mobile();

        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $data['no_return_mobile'] = 1;

        $this->load->model("user") ;
        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser=0;
        if (isset($iduser) && $iduser!=0 && $iduser!= NULL && $iduser !="") {
            $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));
            $data['oCommercantFavoris'] = $this->user->verify_favoris($iduser, $_iCommercantId);
        }
        ////$this->firephp->log($data['oCommercantFavoris'], 'oCommercantFavoris');
		
		//$this->load->view('front/vwFicheCommercantAnnonce', $data) ;
        $this->load->view('front2013/index_partenaire_commercant', $data) ;
	}
        
        function menuMobile ($_iCommercantId) {

            $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
            $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

            $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
            $data['oInfoCommercant'] = $oInfoCommercant ;

            //$data['mdlannonce'] = $this->mdlannonce ;
            //$data['mdlannonce'] = $this->mdlannonce ;
            $data['mdlbonplan'] = $this->mdlbonplan ;

            $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
            $oBonPlan = 	$this->mdlbonplan->bonPlanParCommercant($_iCommercantId);
            $data['nbBonPlan'] = sizeof($oBonPlan);
            $data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_texte : "";
            $data['oImagespub'] = $this->mdlimagespub->GetByImagespubActiv() ;

            $data['active_link'] = "accueil";

            $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
            if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);

            $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
            $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
            if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;


            $this->load->model("user") ;
            if ($this->ion_auth->logged_in()){
                $user_ion_auth = $this->ion_auth->user()->row();
                $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                if ($iduser==null || $iduser==0 || $iduser==""){
                    $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                }
            } else $iduser=0;
            if (isset($iduser) && $iduser!=0 && $iduser!= NULL && $iduser !="") {
                $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));
                $data['oCommercantFavoris'] = $this->user->verify_favoris($iduser, $_iCommercantId);
            }

            $is_mobile = $this->agent->is_mobile();
            //test ipad user agent
            $is_mobile_ipad = $this->agent->is_mobile('ipad');
            $data['is_mobile_ipad'] = $is_mobile_ipad;
            $is_robot = $this->agent->is_robot();
            $is_browser = $this->agent->is_browser();
            $is_platform = $this->agent->platform();
            $data['is_mobile'] = $is_mobile;
            $data['is_robot'] = $is_robot;
            $data['is_browser'] = $is_browser;
            $data['is_platform'] = $is_platform;

            //$this->load->view('front/vwMenuMobile', $data) ;
            $this->load->view('front2013/menumobile', $data) ;
        }
        
	function envoiMailNousContacter(){ 
		$config = array(); 
		$config[ 'protocol' ] = 'mail' ;
		$config[ 'mailtype' ] = 'html' ;
		$config[ 'charset' ] = 'utf-8' ;
		$this->load->library('email', $config);
		
    	$errorMail    = "";
		
    	$mail_to      = $_POST['zMailTo'] ;
		//$mail_cc = "william.arthur@weburba.com";
        $mail_to_name = "Privicarte";
		$mail_subject = "[Privicarte] Contact";
        $mail_expediteur = $_POST['zEmail'] ;
        $data = array() ;
        $data["zNom"] = $_POST['zNom'] ;
        if (isset($_POST['zPrenom'])) $data["zPrenom"] = $_POST['zPrenom'] ; else $data["zPrenom"] = '';
        $data["zEmail"] = $_POST['zEmail'] ;
        $data["zTelephone"] = $_POST['zTelephone'] ;
        $data["zCommentaire"] = $_POST['zCommentaire'] ;
        /*
        $mail_body = $this->load->view("front/vwContenuMailNousContacterAnnonce", $data, true) ;

        $config_send_mail['mailtype'] = 'html';
        $config_send_mail['charset'] = 'utf-8';
        $config_send_mail['protocol'] = 'sendmail';
        $this->email->initialize($config_send_mail);

        $this->email->from($mail_expediteur, "Club proximité");
        $list_email_to = array($mail_to);
		$this->email->to($list_email_to);
		//$this->email->cc($mail_cc);
		$this->email->subject($mail_subject);
//		$this->email->attach($mail_Document);
		$this->email->message($mail_body);*/

        $zIdCommercant = $_POST['zIdCommercant'] ;
        $oInfoCommercant = $this->mdlcommercant->infoCommercant($zIdCommercant) ;

        $colDestAdmin = array();
        $colDestAdmin[] = array("Email"=>$mail_to,"Name"=>"Partenaire Privicarte");
        $txtSujetAdmin = "Demande de Contact Privicarte";
        $txtContenuAdmin = "
            <p>Bonjour ,</p>
            <p>Une demande de contact a été formmulée provenant de Privicarte.fr à propos de votre compte.</p>
            <p>Information du demandeur :<br/>
            Nom : ".$_POST['zNom']."<br/> Prénom : ".$_POST['zPrenom']."<br/> Email : ".$_POST['zEmail']."<br/>Téléphone :".$_POST['zTelephone']." <br/>
                Contenu du message :<br/><br/>".$_POST['zCommentaire']."<br/><br/>
            </p>
        ";
        @envoi_notification($colDestAdmin,$txtSujetAdmin,$txtContenuAdmin);




		//if ($this->email->send()) {
            $this->session->set_flashdata('message','<div style="color:#3C6;">Votre message a bien été envoyé !</div>');
            redirect(base_url().$oInfoCommercant->nom_url.'/nous_contacter');
        /*} else {
            $this->session->set_flashdata('message','<div style="color:#ff0000;">Un problème est survenu, veuillez refaire l\'opération</div>');
            redirect(base_url().$oInfoCommercant->nom_url.'/nous_contacter');
        }*/
	}
	function menuannonceCommercant($_iCommercantId){
            
                $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
                $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);
            
		$oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
		$data['oInfoCommercant'] = $oInfoCommercant ;
		$data['toListeAnnonceParCommercant'] = $this->mdlannonce->listeAnnonceParCommercant($_iCommercantId) ;
                
                $data['active_link'] = "annonces";
                $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
                if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
                $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
                $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
                if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;
                
                $is_mobile = $this->agent->is_mobile();
                //test ipad user agent
                $is_mobile_ipad = $this->agent->is_mobile('ipad');
                $data['is_mobile_ipad'] = $is_mobile_ipad;
                $is_robot = $this->agent->is_robot();
                $is_browser = $this->agent->is_browser();
                $is_platform = $this->agent->platform();
                $data['is_mobile'] = $is_mobile;
                $data['is_robot'] = $is_robot;
                $data['is_browser'] = $is_browser;
                $data['is_platform'] = $is_platform;
                
                $this->load->model("user") ;
                if ($this->ion_auth->logged_in()){
                    $user_ion_auth = $this->ion_auth->user()->row();
                    $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                    if ($iduser==null || $iduser==0 || $iduser==""){
                        $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                    }
                } else $iduser=0;
                $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));
                $data['pagecategory_partner'] = 'annonces_partner';
                
                $data['dontshowannoncecase']=1;
                
		//$this->load->view('front/vwMenuAnnonceCommercant', $data) ;
                    $this->load->view('front2013/menuannoncecommercant', $data) ;
	}
	
	
	function ficheAnnonce($_iDCommercant = 0, $_iDAnnonce = 0){

        if(!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        } else if ($this->ion_auth->in_group(2)) {
            redirect();
        }
            
		$data["idCommercant"] = $_iDCommercant ;
                $data["no_main_menu_home"] = "1";
                $data["no_left_menu_home"] = "1";
                
                //verify 20 annonces limit
                if (isset($_iDCommercant) && $_iDCommercant!=0){
                    $toListeMesAnnonce = $this->mdlannonce->listeMesAnnonces($_iDCommercant) ;
                    if (count($toListeMesAnnonce)==20 || count($toListeMesAnnonce)>=20) redirect ("front/annonce/listeMesAnnonces/$_iDCommercant") ;
                }
                
                
                //start verify if merchant subscription contain annonce WILLIAM
                $current_date = date("Y-m-d");
                $query_abcom_cond = " IdCommercant = ".$_iDCommercant." AND ('".$current_date."' BETWEEN DateDebut AND DateFin)";
                $toAbonnementCommercant = $this->AssCommercantAbonnement->GetWhere($query_abcom_cond, 0, 1) ;
                if(sizeof($toAbonnementCommercant)) { 
                    foreach ($toAbonnementCommercant as $oAbonnementCommercant) {
                                if(isset($oAbonnementCommercant) && ($oAbonnementCommercant->IdAbonnement==1 || $oAbonnementCommercant->IdAbonnement==2)) {//IdAbonnement doesn't contain annonce
                                        redirect ("front/professionnels/fiche/$_iDCommercant") ;
                                        }
                    }
                } else redirect ("front/professionnels/fiche/$_iDCommercant") ;
                //end verify if merchant subscription contain annonce WILLIAM
                
                
                
		$data["colSousRubriques"] = $this->sousrubrique->GetAll();
		if ($_iDAnnonce != 0){
			$data["title"] = "Modification annonce" ;
			$data["oAnnonce"] = $this->mdlannonce->GetAnnonce($_iDAnnonce);
            //$this->load->view('front/vwFicheAnnonce', $data) ;
            $this->load->view('front2013/vwFicheAnnonce', $data) ;
		}else{
			$data["title"] = "Creer une annonce" ;
			//$this->load->view('front/vwFicheAnnonce', $data) ;
            $this->load->view('front2013/vwFicheAnnonce', $data) ;
		}
	}
	
	function creerAnnonce($_iDCommercant = 0){
		$oAnnonce = $this->input->post("annonce") ;
		$IdInsertedAnnonce = $this->mdlannonce->insertAnnonce($oAnnonce);
		$data["title"] = "Creer une annonce" ;
		$data["idCommercant"] = $_iDCommercant ;
		$data["msg"] = "Annonce bien cree" ;
		// $this->load->view('front/vwFicheAnnonce', $data) ;
		//$this->load->view('front/vwMesAnnonces', $data) ;
		redirect ("front/annonce/listeMesAnnonces/$_iDCommercant") ;
	}
	
	function modifAnnonce($_iDCommercant = 0){
		$oAnnonce = $this->input->post("annonce") ;
		$IdUpdatedAnnonce = $this->mdlannonce->updateAnnonce($oAnnonce);
		$data["title"] = "Modification annonce" ;
		$data["idCommercant"] = $_iDCommercant ;
		$data["msg"] = "Annon bien ete modifier" ;
		redirect ("front/annonce/listeMesAnnonces/$_iDCommercant") ;
		//$this->load->view('front/vwFicheAnnonce', $data) ;
	}
	
	function listeMesAnnonces($_iDCommercant = 0){

        if(!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        } else if ($this->ion_auth->in_group(2)) {
            redirect();
        }

        $data["no_main_menu_home"] = "1";
        $data["no_left_menu_home"] = "1";

        //start verify if merchant subscription contain annonce WILLIAM
        $current_date = date("Y-m-d");
        $query_abcom_cond = " IdCommercant = ".$_iDCommercant." AND ('".$current_date."' BETWEEN DateDebut AND DateFin)";
        $toAbonnementCommercant = $this->AssCommercantAbonnement->GetWhere($query_abcom_cond, 0, 1) ;
        if(sizeof($toAbonnementCommercant)) {
            foreach ($toAbonnementCommercant as $oAbonnementCommercant) {
                        if(isset($oAbonnementCommercant) && ($oAbonnementCommercant->IdAbonnement==1 || $oAbonnementCommercant->IdAbonnement==2)) {//IdAbonnement doesn't contain annonce
                                redirect ("front/professionnels/fiche/$_iDCommercant") ;
                                }
            }
        } else redirect ("front/professionnels/fiche/$_iDCommercant") ;
        //end verify if merchant subscription contain annonce WILLIAM

                
		if ($_iDCommercant != 0){
			$toListeMesAnnonce = $this->mdlannonce->listeMesAnnonces($_iDCommercant) ;
                        //verify 20 annonces limit
                        if (count($toListeMesAnnonce)==20 || count($toListeMesAnnonce)>=20) $data['limit_annonce_add'] = 1;
			$data['toListeMesAnnonce'] = $toListeMesAnnonce ;
			$data["idCommercant"] = $_iDCommercant ;
			$this->load->view('front/vwMesAnnonces', $data) ;
		}else{
			//$this->load->view('front/vwMesAnnonces', $data) ;
		}
	}
	
	function supprimAnnonce($_iDAnnonce, $_iDCommercant){
		
		$zReponse = $this->mdlannonce->supprimeAnnonces($_iDAnnonce) ;
		redirect ("front/annonce/listeMesAnnonces/$_iDCommercant") ;
	}
	
	function annulationAjout(){
		//Supprimme tous les images deja uploader.
		$tNameFile = explode("-", $this->input->post("zConcateNameFile")) ;
		//print_r ($tNameFile); exit ();
		$path =  "application/resources/front/images/" ;
		//echo $path . $tNameFile[0] ; exit ();
		if (count ($tNameFile) > 0){
			for ($i = 0; $i > count($tNameFile); $i ++){
				// echo "dfsdfs" ; exit ();
				if (file_exists($path.$tNameFile[$i])){
					unlink($path.$tNameFile[$i]) ;
					// echo $path . $tNameFile[0] ; exit ();
				}
			}
		}
		
		//return false;
	}
}