<?php

class contact extends CI_Controller {
    
    function __construct() {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('clubproximite');
        statistiques();

        check_vivresaville_id_ville();
    }
    
    function index(){
        
        $this->fiche();
        
    }
    
    function fiche() {
        $data['zTitle'] = "Contact";
        $data['mail_sender_adress'] = $this->config->item("mail_sender_adress");
        $this->load->view('privicarte/contact', $data);
    }
    
    
    function envoyer(){
        $objContact = $this->input->post("contact");
        $objContact_interet = $this->input->post("contact_interet");

        
        /**
         * Envoi mail vers admin
         */
        $colDestAdmin = array();
        $colDestAdmin[] = array("Email"=>$this->config->item("mail_sender_adress"),"Name"=>$this->config->item("mail_sender_name"));

        // Sujet
        $txtSujetAdmin = "Contact Privicarte";

        $txtContenuAdmin = "
            <p>Bonjour ,</p>
            <p>Une demande de contact vient du site ClubProxmité.</p>
            <p>Information du demandeur :<br/>
            Nom : ".$objContact['nom']."<br/> Prénom : ".$objContact['prenom']."<br/> Email : ".$objContact['email']."<br/>Téléphone :".$objContact['tel']." <br/>
            ";
        $txtContenuAdmin .= "Centre d'intérêt : <br/>";
        if (isset($objContact_interet['proximite'])) $txtContenuAdmin .= "- Privicarte<br/>";
        if (isset($objContact_interet['agenda'])) $txtContenuAdmin .= "- Agenda<br/>";
        if (isset($objContact_interet['annuaire'])) $txtContenuAdmin .= "- Annuaire<br/>";
        if (isset($objContact_interet['selection'])) $txtContenuAdmin .= "- Sélection Privicarte<br/>";
        $txtContenuAdmin .= "Contenu du message :<br/><br/>".$objContact['contenu']."<br/><br/>
            </p>
        ";
        @envoi_notification($colDestAdmin,$txtSujetAdmin,$txtContenuAdmin);
        
        
        //save mail info into database
        $this->load->Model("mdlcontact");
        $IdInsertedContact = $this->mdlcontact->Insert($objContact);
        
        $data['IdInsertedContact'] = $IdInsertedContact;
        
        $this->load->view("frontAout2013/vwContactConfirmation",$data);
    }
    
    
}


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
