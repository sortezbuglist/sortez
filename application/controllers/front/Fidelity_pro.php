<?php
class fidelity_pro extends CI_Controller {
	function __construct() {
		parent::__construct ();
		
		$this->load->model ( "mdl_card" );
		$this->load->model ( "mdl_card_bonplan_used" );
		$this->load->model ( "mdl_card_capital" );
		$this->load->model ( "mdl_card_tampon" );
		$this->load->model ( "mdl_card_remise" );
		$this->load->model ( "Mdl_soutenons" );
		$this->load->model ( "mdl_card_user_link" );
		$this->load->model ( "mdl_card_fiche_client_tampon" );
		$this->load->model ( "mdl_card_fiche_client_capital" );
		$this->load->model ( "mdl_card_fiche_client_remise" );
		$this->load->model ( 'assoc_client_bonplan_model' );
		$this->load->model ( 'assoc_client_commercant_model' );
		$this->load->Model ( "Ville" );
		$this->load->model ( "user" );
		$this->load->model ( "Mdl_prospect" );
		$this->load->model ( "mdlbonplan" );
		$this->load->model ( "commercant" );
        $this->load->model ( "mdlcommercant");
		$this->load->model ( "assoc_client_plat_model" );
        $this->load->model ( "assoc_client_table_model" );
		$this->load->model ( "assoc_client_sejour_model" );
		$this->load->library ( 'session' );
		
		$this->load->library ( 'ion_auth' );
		$this->load->model ( "ion_auth_used_by_club" );
        $this->load->helper('clubproximite');
		
		$current_url = current_url ();
		
		$user_ion_auth_id = $this->ion_auth->user ()->row ();
		// //$this->firephp->log($user_ion_auth_id, 'user_ion_auth_id');
		if (isset ( $user_ion_auth_id ) && is_object ( $user_ion_auth_id ))
			$user_groups = $this->ion_auth->get_users_groups ( $user_ion_auth_id->id )->result ();
		else
			$user_groups = 0;
		
		if (! $this->ion_auth->logged_in ()) {
			$this->session->set_userdata ( 'last_url', $current_url );
			redirect ( "auth/login" );
		} else if (isset ( $user_groups ) && (is_object ( $user_groups ) && $user_groups->id == '2')) {
			redirect ( "utilisateur/no_permission" );
		}
	}
	function index() {
		$this->liste ();
	}
	function liste() {
		// $objfidelity = $this->mdlfidelity->GetAll();
		// $data"colfidelity"] = $objfidelity;
		$data ['pagetitle'] = "Menu fidelity";
		
		$is_mobile = TRUE;
		// test ipad user agent
		$is_mobile_ipad = FALSE;
		
		$is_robot = TRUE;
		$is_browser = $this->agent->is_browser ();
		$is_platform = $this->agent->platform ();
		$data ['is_mobile_ipad'] = $is_mobile_ipad;
		$data ['is_mobile'] = $is_mobile;
		$data ['is_robot'] = $is_robot;
		$data ['is_browser'] = $is_browser;
		$data ['is_platform'] = $is_platform;
		
		if ($is_mobile) {
			redirect ( 'front/utilisateur/contenupro' );
		}
		$this->load->view ( "fidelity/vwListefidelity", $data );
	}
	function my_parameters() {
		$data ['pagetitle'] = "parameters";
		
		$is_mobile = TRUE;
		// test ipad user agent
		$is_mobile_ipad = FALSE;
		$data ['is_mobile_ipad'] = $is_mobile_ipad;
		$is_robot = TRUE;
		$is_browser = $this->agent->is_browser ();
		$is_platform = $this->agent->platform ();
		$data ['is_mobile'] = $is_mobile;
		$data ['is_robot'] = $is_robot;
		$data ['is_browser'] = $is_browser;
		$data ['is_platform'] = $is_platform;
		
		$this->load->view ( "fidelity/vwMyParameters", $data );
	}
	function tampon() {
		$data ['pagetitle'] = "tampon";
		$user_ion_auth_id = $this->ion_auth->user ()->row ();
		$data ["oTampon"] = $this->mdl_card_tampon->getByIdIonauth ( $user_ion_auth_id->id );
		
		$is_mobile = TRUE;
		// test ipad user agent
		$is_mobile_ipad = FALSE;
		$data ['is_mobile_ipad'] = $is_mobile_ipad;
		$is_robot = TRUE;
		$is_browser = $this->agent->is_browser ();
		$is_platform = $this->agent->platform ();
		$data ['is_mobile'] = $is_mobile;
		$data ['is_robot'] = $is_robot;
		$data ['is_browser'] = $is_browser;
		$data ['is_platform'] = $is_platform;
		
		if ($is_mobile) {
			$data ['titre'] = 'Mes parametres <br/>Offres "coups de tampons"';
			$this->load->view ( 'mobile2014/pro/parametre-tampons', $data );
		} else {
			$this->load->view ( 'fidelity/vwFichetampon', $data );
		}
	}
	function remisedirect() {
		$data ['pagetitle'] = "Remise directe";
		$user_ion_auth_id = $this->ion_auth->user ()->row ();
		
		$is_mobile = TRUE;
		// test ipad user agent
		$is_mobile_ipad = FALSE;
		$data ['is_mobile_ipad'] = $is_mobile_ipad;
		$is_robot = TRUE;
		$is_browser = $this->agent->is_browser ();
		$is_platform = $this->agent->platform ();
		$data ['is_mobile'] = $is_mobile;
		$data ['is_robot'] = $is_robot;
		$data ['is_browser'] = $is_browser;
		$data ['is_platform'] = $is_platform;
		$data ["oRemise"] = $this->mdl_card_remise->getByIdIonauth ( $user_ion_auth_id->id );
		
		// $is_mobile = false;
		if ($is_mobile) {
			$data ['titre'] = 'Mes parametres <br/>Offre de "Remise directe"';
			$this->load->view ( 'mobile2014/pro/parametre-remise', $data );
		}
	}
	function capital() {
		$data ['pagetitle'] = "capital";
		$user_ion_auth_id = $this->ion_auth->user ()->row ();
		$data ["oCapital"] = $this->mdl_card_capital->getByIdIonauth ( $user_ion_auth_id->id );
		
		$is_mobile = TRUE;
		// test ipad user agent
		$is_mobile_ipad = FALSE;
		$data ['is_mobile_ipad'] = $is_mobile_ipad;
		$is_robot = TRUE;
		$is_browser = $this->agent->is_browser ();
		$is_platform = $this->agent->platform ();
		$data ['is_mobile'] = $is_mobile;
		$data ['is_robot'] = $is_robot;
		$data ['is_browser'] = $is_browser;
		$data ['is_platform'] = $is_platform;
		// $is_mobile = false;
		if ($is_mobile) {
			$data ['titre'] = 'Mes parametres <br/>Offre de "Capitalisation"';
			$this->load->view ( 'mobile2014/pro/parametre-capitalisation', $data );
		} else {
			$this->load->view ( 'fidelity/vwFichecapital', $data );
		}
	}
	function editremise() {
		try {
			$objRemise = $this->input->post ( "remise" );
			
			if (! empty ( $objRemise )) {
				$objRemise ["date_debut"] = convert_Frenchdate_to_Sqldate ( $objRemise ["date_debut"] );
				$objRemise ["date_fin"] = convert_Frenchdate_to_Sqldate ( $objRemise ["date_fin"] );
				
				$user_ion_auth_id = $this->ion_auth->user ()->row ();
				$objRemise ["id_ionauth"] = $user_ion_auth_id->id;
				$objRemise ["id_commercant"] = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
				
				$objRemise ["is_activ"] = (isset ( $objRemise ["is_activ"] )) ? 1 : 0;
				
				if ($objRemise ["id"] == "" || $objRemise ["id"] == null || $objRemise ["id"] == "0") {
					$this->mdl_card_remise->insert ( $objRemise );
				} else {
					$this->mdl_card_remise->update ( $objRemise );
				}
				if ($objRemise ["is_activ"]) {
					$objCapital = $this->mdl_card_capital->getByIdCommercant ( $objRemise ['id_commercant'] );
					if (is_object ( $objCapital )) {
						$arrayCapital ['id'] = $objCapital->id;
						$arrayCapital ['is_activ'] = 0;
						$this->mdl_card_capital->update ( $arrayCapital );
					}
					$objTampon = $this->mdl_card_tampon->getByIdCommercant ( $objRemise ['id_commercant'] );
					if (is_object ( $objTampon )) {
						$arrayTampon ['id'] = $objTampon->id;
						$arrayTampon ['is_activ'] = 0;
						$this->mdl_card_tampon->update ( $arrayTampon );
					}
				}
				$this->session->set_flashdata ( 'message_success', 'Vos paramètres ont été mis à jour' );
				redirect ( "front/fidelity/mes_parametres", 'refresh' );
			}
		} catch ( Exception $e ) {
			$this->session->set_flashdata ( 'message_error', 'Une erreur est survenue!!' );
			redirect ( "front/fidelity_pro/remisedirect", 'refresh' );
		}
	}
	function edittampon() {
		try {
			$objTampon = $this->input->post ( "tampon" );
			
			$objTampon ["date_debut"] = convert_Frenchdate_to_Sqldate ( $objTampon ["date_debut"] );
			$objTampon ["date_fin"] = convert_Frenchdate_to_Sqldate ( $objTampon ["date_fin"] );
			
			$objTampon ["tampon_value"] = trim ( $objTampon ["tampon_value"] );
			
			$user_ion_auth_id = $this->ion_auth->user ()->row ();
			$objTampon ["id_ionauth"] = $user_ion_auth_id->id;
			$objTampon ["id_commercant"] = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
			
			$objTampon ["is_activ"] = (isset ( $objTampon ["is_activ"] )) ? 1 : 0;
			
			if ($objTampon ["id"] == "" || $objTampon ["id"] == null || $objTampon ["id"] == "0") {
				$this->mdl_card_tampon->insert ( $objTampon );
			} else {
				$this->mdl_card_tampon->update ( $objTampon );
				$this->mdl_card_tampon->delete_where ( array (
						'id_commercant' => $objTampon ['id_commercant'],
						'id !=' => $objTampon ["id"] 
				) );
			}
			
			if ($objTampon ["is_activ"]) {
				$objCapital = $this->mdl_card_capital->getByIdCommercant ( $objTampon ['id_commercant'] );
				if (is_object ( $objCapital )) {
					$arrayCapital ['id'] = $objCapital->id;
					$arrayCapital ['is_activ'] = 0;
					$this->mdl_card_capital->update ( $arrayCapital );
				}
				$objRemise = $this->mdl_card_remise->getByIdCommercant ( $objTampon ['id_commercant'] );
				if (is_object ( $objRemise )) {
					$arrayRemise ['id'] = $objRemise->id;
					$arrayRemise ['is_activ'] = 0;
					$this->mdl_card_capital->update ( $arrayRemise );
				}
			}
			$this->session->set_flashdata ( 'message_success', 'Vos paramètres ont été mis à jour' );
			redirect ( "front/fidelity/mes_parametres", 'refresh' );
		} catch ( Exception $e ) {
			redirect ( "front/fidelity_pro/tampon", 'refresh' );
		}
	}
	function editcapital() {
		try {
			$objCapital = $this->input->post ( "capital" );
			$objCapital ["date_debut"] = convert_Frenchdate_to_Sqldate ( $objCapital ["date_debut"] );
			$objCapital ["date_fin"] = convert_Frenchdate_to_Sqldate ( $objCapital ["date_fin"] );
			
			// $objCapital["remise_value"] = trim($objCapital["remise_value"]);
			$objCapital ["montant"] = trim ( $objCapital ["montant"] );
			
			$user_ion_auth_id = $this->ion_auth->user ()->row ();
			$objCapital ["id_ionauth"] = $user_ion_auth_id->id;
			$objCapital ["id_commercant"] = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
			
			$objCapital ["is_activ"] = (isset ( $objCapital ["is_activ"] )) ? 1 : 0;
			
			if ($objCapital ["id"] == "" || $objCapital ["id"] == null || $objCapital ["id"] == "0") {
				$this->mdl_card_capital->insert ( $objCapital );
			} else {
				$this->mdl_card_capital->update ( $objCapital );
			}
			
			
			if ($objCapital ["is_activ"]) {
				$objTampon = $this->mdl_card_tampon->getByIdCommercant ( $objCapital ['id_commercant'] );
				if (is_object ( $objTampon )) {
					$arrayTampon ['id'] = $objTampon->id;
					$arrayTampon ['is_activ'] =0;
					$this->mdl_card_tampon->update ( $arrayTampon );
				}
				$objRemise = $this->mdl_card_remise->getByIdCommercant ( $objCapital ['id_commercant'] );
				if (is_object ( $objRemise )) {
					$arrayRemise ['id'] = $objRemise->id;
					$arrayRemise ['is_activ'] = 0;
					$this->mdl_card_remise->update ( $arrayRemise );
				}
			}
			$this->session->set_flashdata ( 'message_success', 'Vos paramètres ont été mis à jour' );
			redirect ( "front/fidelity/mes_parametres", 'refresh' );
		} catch ( Exception $e ) {
			redirect ( "front/fidelity_pro/capital", 'refresh' );
		}
	}
	function enregistremnet() {
		$data ['pagetitle'] = "Enregistrement fidelity";
		
		$is_mobile = TRUE;
		// test ipad user agent
		$is_mobile_ipad = FALSE;
		$data ['is_mobile_ipad'] = $is_mobile_ipad;
		$is_robot = TRUE;
		$is_browser = $this->agent->is_browser ();
		$is_platform = $this->agent->platform ();
		$data ['is_mobile'] = $is_mobile;
		$data ['is_robot'] = $is_robot;
		$data ['is_browser'] = $is_browser;
		$data ['is_platform'] = $is_platform;
		
		$this->load->view ( "fidelity/vwEnregistrement", $data );
	}
	function check_num_card() {
		$num_card_verification = $this->input->post ( 'num_card_verification' );
		$ocheck_num_card = $this->mdl_card->check_num_card ( $num_card_verification );
		
		if (count ( $ocheck_num_card ) > 0) {
			// echo "error";
			if (isset ( $ocheck_num_card ) && $ocheck_num_card->is_activ == "1")
				echo "1";
			else
				echo "2";
		} else {
			echo "error";
		}
	}
	function card_scan() {
		$data ['pagetitle'] = "Scanner une carte";
		
		$is_mobile = TRUE;
		// test ipad user agent
		$is_mobile_ipad = FALSE;
		$data ['is_mobile_ipad'] = $is_mobile_ipad;
		$is_robot = TRUE;
		$is_browser = $this->agent->is_browser ();
		$is_platform = $this->agent->platform ();
		$data ['is_mobile'] = $is_mobile;
		$data ['is_robot'] = $is_robot;
		$data ['is_browser'] = $is_browser;
		$data ['is_platform'] = $is_platform;
		
		$data ['titre'] = 'Reconnaissance du porteur de la carte';
		$this->load->view ( "mobile2014/pro/reconnaissance-carte", $data );
		
		/*
		if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
			$data ['titre'] = 'Reconnaissance du porteur de la carte';
			// $this->load->view("fidelity/vwCardScan_mobile",$data);
			$this->load->view ( "mobile2014/pro/reconnaissance-carte", $data );
		} else {
			$this->load->view ( "fidelity/vwCardScan", $data );
		}*/
	}
	function check_scan_card() {
		$this->load->model ( 'assoc_client_commercant_model' );
		$num_card_scan = $this->input->post ( 'num_card_scan' );
		$ocheck_num_card = $this->mdl_card->check_scan_card ( $num_card_scan );
		$reponse ['status'] = 0;
		if (count ( $ocheck_num_card ) > 0) {
			// echo "error";
			if (isset ( $ocheck_num_card ) && $ocheck_num_card->is_activ == "1") {
				$oCard = $this->mdl_card->getByCard ( $num_card_scan );
				$ocheck_card = $this->mdl_card_user_link->check_card ( $oCard->id );
				if (count ( $ocheck_card ) > 0) {
					$user_ion_auth_id = $this->ion_auth->user ()->row ();
					$id_ionauth = $user_ion_auth_id->id;
					$id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
					$id_user = $ocheck_card->id_user;
					
					$assoc_client_commercant = $this->assoc_client_commercant_model->get_by_id ( $id_user, $id_commercant );
					if (! $assoc_client_commercant) {
						$this->assoc_client_commercant_model->insert ( array (
								'id_client' => $id_user,
								'id_commercant' => $id_commercant 
						) );
					}
					$reponse ['status'] = 1;
				} else {
					$reponse ['status'] = 3;
				}
			} else {
				$reponse ['status'] = 2;
			}
		}
		echo json_encode ( $reponse );
	}

	function check_scan_card_command() {
		$this->load->model ( 'assoc_client_commercant_model' );
		$num_card_scan = $this->input->post ( 'num_card_scan' );
		$ocheck_num_card = $this->mdl_card->check_scan_card ( $num_card_scan );
		$reponse ['status'] = 0;
		if (count ( $ocheck_num_card ) > 0) {
			// echo "error";
			if (isset ( $ocheck_num_card ) && $ocheck_num_card->is_activ == "1") {
				$oCard = $this->mdl_card->getByCard ( $num_card_scan );
				$ocheck_card = $this->mdl_card_user_link->check_card_command( $oCard->id_user );
				if (!empty ( $ocheck_card )) {
					$user_ion_auth_id = $this->ion_auth->user ()->row ();
					$id_ionauth = $user_ion_auth_id->id;
					$id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
					$id_user = $ocheck_card->id_client;

					$assoc_client_commercant = $this->assoc_client_commercant_model->get_by_id ( $id_user, $id_commercant );
					if (! $assoc_client_commercant) {
						$this->assoc_client_commercant_model->insert ( array (
								'id_client' => $id_user,
								'id_commercant' => $id_commercant
						));
					}
					$reponse ['status'] = 1;
				} else {
					$reponse ['status'] = 3;
				}
			} else {
				$reponse ['status'] = 2;
			}
		}
		echo json_encode ( $reponse );
	}

	function check_scan_card_number($num_card_scan) {
		// $num_card_scan = $this->input->post('num_card_scan');
		$ocheck_num_card = $this->mdl_card->check_scan_card ( $num_card_scan );
		
		if (count ( $ocheck_num_card ) > 0) {
			// echo "error";
			if (isset ( $ocheck_num_card ) && is_object ( $ocheck_num_card ) && $ocheck_num_card->is_activ == "1") {
				$oCard = $this->mdl_card->getByCard ( $num_card_scan );
				$ocheck_card = $this->mdl_card_user_link->check_card ( $oCard->id );
				if (count ( $ocheck_card ) > 0) {
					// echo "1";
					$objnum_card_scan = $this->mdl_card->getByCard ( $num_card_scan );
					if (count ( $objnum_card_scan ) > 0 && is_object ( $objnum_card_scan ))
						$id_card_to_use = $objnum_card_scan->id_user;
					else
						$id_card_to_use = "0";
					redirect ( "front/fidelity/validation_operation/" . $id_card_to_use );
				} else {
					// echo "3";
					$data ['mssg'] = '<span style="color:#FF0000"><strong>Votre carte n\'est pas encore liée à un compte Client !</strong></span>';
				}
			} else {
				// echo "2";
				$data ['mssg'] = '<span style="color:#FF0000"><strong>Votre carte est répertorié, mais n\'est pas activé !</strong></span>';
			}
		} else {
			// echo "error";
			$data ['mssg'] = '<span style="color:#FF0000"><strong>Cette carte est invalide !</strong></span>';
		}
		
		// $data['mssg'] = "Scanner une carte";
		$this->load->view ( "fidelity/vwCardScan", $data );
	}
    function validation_operation($IdUser) {
        $user_ion_auth_id = $this->ion_auth->user ()->row ();
        $id_ionauth = $user_ion_auth_id->id;
        $id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );

        $criteres ['id_commercant'] = $id_commercant;
        $criteres ['id_user'] = $IdUser;
        $assoc_client_array = $this->assoc_client_bonplan_model->get_list_by_criteria ( $criteres );
        $assoc_client = ! empty ( $assoc_client_array ) ? current ( $assoc_client_array ) : array ();

        $data ['bonplanshow'] = ! empty ( $assoc_client_array ) ? 1 : 0;
        /*
         * $data['offre_active'] ='' ;
         * if($assoc_client->param_captital_active){
         * $data['offre_active'] = 'Offre "coup de capitalisation"';
         * }else if($assoc_client->param_tampon_active){
         * $data['offre_active'] = 'Offre "coup de tampons"';
         * }
         * else{
         * $data['offre_active'] = 'Offre "Remise directe"';
         * }
         *
         * }
         */

        $offre = get_fidelity ( $id_commercant );
        $data ['offre_active'] = 0;

        if ($offre->is_capital_active) {
            $data ['offre_active'] = 'Offre "coup de capitalisation"';
        } else if ($offre->is_tampon_active) {
            $data ['offre_active'] = 'Offre "coup de tampons"';
        } else if ($offre->is_remise_active) {
            $data ['offre_active'] = 'Offre "Remise directe"';
        }

        $client = $this->user->GetById ( $IdUser );
        $data ['titre'] = 'Client: ' . ucfirst ( $client->Nom ) . ' ' . ucfirst ( $client->Prenom );
        $data ['IdUser'] = $IdUser;
        $data ['id_client_bonplan'] = is_object ( $assoc_client ) ? $assoc_client->id : 0;
        $this->load->view ( "mobile2014/pro/validation_operation", $data );
    }
	function validation_operation_command($IdUser) {
		$user_ion_auth_id = $this->ion_auth->user ()->row ();
		$id_ionauth = $user_ion_auth_id->id;
		$id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );

		$criteres ['id_commercant'] = $id_commercant;
		$criteres ['id_user'] = $IdUser;
		$assoc_client_array = $this->assoc_client_commercant_model->get_command ( $criteres );
		$assoc_client = ! empty ( $assoc_client_array ) ? current ( $assoc_client_array ) : array ();
//		var_dump($assoc_client);
		$data ['commandshow'] = ! empty ( $assoc_client_array ) ? 1 : 0;

		$data ['offre_active'] = 'Commande';
        $data['commande'] = $assoc_client_array;
		$client = $this->user->GetById( $IdUser );
		$data ['titre'] = 'Client: ' . ucfirst ( $client->Nom ) . ' ' . ucfirst ( $client->Prenom );
		$data ['IdUser'] = $IdUser;
		$data ['id_client_command'] = is_object ( $assoc_client ) ? $assoc_client->id : 0;
		$this->load->view ( "mobile2014/pro/list_command", $data );
	}
	function fidelity_card_operations($IdUser, $bonplanshow = null, $id_client_bonplan = 0) {
        $ordre = null;
		$data ['pagetitle'] = "Offre de fidélité !";
		// $IdUser = $this->uri->rsegment(3);
		// $bonplanshow = $this->uri->rsegment(4);
		//var_dump($IdUser);die();
		$data ['oUser'] = $oUser = $this->user->GetById ( $IdUser );

		$data ['oCard'] = $oCard = $this->mdl_card->getByIdUser ( $IdUser );
        //var_dump($data ['oCard']);
        //var_dump($data ['oUser']);die();
		$user_ion_auth_id = $this->ion_auth->user()->row ();
		$id_ionauth = $user_ion_auth_id->id;
		$data ["id_ionauth"] = $id_ionauth;
		$id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
		$data ["id_commercant"] = $id_commercant;

		$objTampon = $this->mdl_card_tampon->getByIdIonauth ( $id_ionauth );
		$data ["oTampon"] = $objTampon;
		$objCapital = $this->mdl_card_capital->getByIdIonauth ( $id_ionauth );
		$data ["oCapital"] = $objCapital;
		$objRemise = $this->mdl_card_remise->getByIdIonauth ( $id_ionauth );
		$data ["objRemise"] = $objRemise;
		
		/*if (isset($id_client_bonplan) && $id_client_bonplan!=0 && $id_client_bonplan!="0") $res_client_bonplan_obj = $this->assoc_client_bonplan_model->getById ( $id_client_bonplan );

		if (isset($res_client_bonplan_obj->id_bonplan) && is_object($res_client_bonplan_obj)) {
			$objBonplan = $this->mdlbonplan->getById($res_client_bonplan_obj->id_bonplan);
			$data ["oBonplan"] = $objBonplan;
		}*/

		//$data ["objReservationNonValideCom"] = $this->assoc_client_bonplan_model->get_where(array('assoc_client_bonplan.id_bonplan'=>$res_client_bonplan_obj->id_bonplan,'assoc_client_bonplan.valide'=>0));

		if (is_object ( $oCard ) && is_object ( $oUser ) && $id_commercant) {
			$oFicheclient = $this->mdl_card_fiche_client_capital->getWhereOne ( " id_card='" . $oCard->id . "' AND id_user='" . $oUser->IdUser . "' AND id_commercant='" . $id_commercant . "' " );

			$data ['oFicheclient'] = $oFicheclient;
			if (isset ( $oFicheclient ) && count ( $oFicheclient ) > 0) {
				$capital_client_value = $oFicheclient->solde_capital;
			} else {
				$capital_client_value = 0;
			}
			
			$objCommercant_card_tampon = $this->mdl_card_tampon->getByIdCommercant ( $id_commercant );

			//echo $id_commercant;
			//var_dump($objCommercant_card_tampon); die();

			$tampon_commercant_value = (is_object ( $objCommercant_card_tampon )) ? $objCommercant_card_tampon->tampon_value : 0;
			$oFicheclient_tampon = $this->mdl_card_fiche_client_tampon->getWhereOne ( " id_card='" . $oCard->id . "' AND id_user='" . $oUser->IdUser . "' AND id_commercant='" . $id_commercant . "' " );



			if (isset ( $oFicheclient_tampon ) && count ( $oFicheclient_tampon ) > 0) {
				$tampon_client_value = $oFicheclient_tampon->solde_tampon;
				if ($tampon_client_value == $tampon_commercant_value) {
					$tampon_status = "0";
				}
			} else {
				$tampon_client_value = "0";
			}
			
			$data ['capital_client_value'] = $capital_client_value;
			$data ['tampon_client_value'] = $tampon_client_value;
			$data ['tampon_commercant_value'] = $tampon_commercant_value;
			$data ['bonplanshow'] = $bonplanshow;
			//$data ['id_client_bonplan'] = $id_client_bonplan;
			//var_dump($id_commercant);die();
			//$criteres['id_commercant'] = $id_commercant;
			// $criteres['id_user'] = $IdUser;
//            var_dump($id_client_bonplan);
//            var_dump($IdUser);
//            die();
			//$criteres ['id'] = $id_client_bonplan;
            $criteres ['valide'] = 0;
			$criteres['id_user']=(int)$IdUser;
            $user_ion_auth_id = $this->ion_auth->user ()->row ();
            $id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
            $order_by ['assoc_client_bonplan.date_validation'] = ($ordre == "desc") ? "desc" : "asc";
            $criteres ['id_commercant'] = $id_commercant;

			$assoc_client_array =  $this->assoc_client_bonplan_model->get_list_by_criteria ( $criteres, $order_by );
            //var_dump($assoc_client_array);
			// echo $this->db->last_query();
			// if(!is_array($assoc_client_array)){
			// redirect('auth/login');
			// }
            //var_dump($assoc_client_array);die();
			$data ['assoc_clients'] = $assoc_client_array;
			$data ['assoc_client_remise'] = $this->mdl_card_fiche_client_remise->getWhereOne(" id_card='" . $oCard->id . "' AND id_user='" . $oUser->IdUser . "' AND id_commercant='" . $id_commercant . "' ");
            //var_dump($assoc_client_array);die();
			$is_mobile = TRUE;
			$data ['titre'] = 'Client: ' . ucfirst ( $oUser->Nom ) . ' ' . ucfirst ( $oUser->Prenom );
				// echo $this->db->last_query();
			// var_dump($assoc_client_array);die("fin 1");
			// var_dump($assoc_client_array->bonplan_id);die("fin");
			// $residbonplan= $this->mdlbonplan->getById($assoc_client_array->id_bonplan);

			$this->load->view ( "mobile2014/pro/fidelity_operation", $data );
		} else {
			//redirect ( 'auth/login' );
			$data['error_msg'] = "Ce client ne dispose pas encore d'une carte Privilege !";
			$this->load->view ( "mobile2014/error", $data );
		}
	}
	function getuser_card_scan() {
		$num_card_scan = $this->input->post ( 'num_card_scan' );
		$objnum_card_scan = $this->mdl_card->getByCard ( $num_card_scan );
		if (count ( $objnum_card_scan ) > 0 && is_object ( $objnum_card_scan ))
			echo $objnum_card_scan->id_user;
		else
			echo "0";
	}
	function card_capital_client_add() {
		$id_card = $this->input->post ( 'id_card' );
		$id_bonplan = $this->input->post ( 'id_bonplan' );
		$id_commercant = $this->input->post ( 'id_commercant' );
		$id_user = $this->input->post ( 'id_user' );
		$card_capital_value = $this->input->post ( 'card_capital_value' );
		
		// checking on card_fiche_client_capital and save in it
		$oFicheclient = $this->mdl_card_fiche_client_capital->getWhereOne ( " id_card='" . $id_card . "' AND id_commercant='" . $id_commercant . "' AND id_user='" . $id_user . "'" );
		if (isset ( $oFicheclient ) && count ( $oFicheclient ) > 0) {
			$capital_client_value = intval ( $oFicheclient->solde_capital );
			$capital_client_value = intval ( $capital_client_value ) + intval ( $card_capital_value );
			$oCard_fiche_client_capital ['id'] = $oFicheclient->id;
			$oCard_fiche_client_capital ['id_card'] = $id_card;
			$oCard_fiche_client_capital ['id_commercant'] = $id_commercant;
			$oCard_fiche_client_capital ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
			$oCard_fiche_client_capital ['id_user'] = $id_user;
			$oCard_fiche_client_capital ['solde_capital'] = $capital_client_value;
			$solde_capital_client = $capital_client_value;
			
			$this->mdl_card_fiche_client_capital->update ( $oCard_fiche_client_capital );
		} else {
			$oCard_fiche_client_capital ['id_card'] = $id_card;
			$oCard_fiche_client_capital ['id_commercant'] = $id_commercant;
			$oCard_fiche_client_capital ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
			$oCard_fiche_client_capital ['id_user'] = $id_user;
			$oCard_fiche_client_capital ['solde_capital'] = $card_capital_value;
			$solde_capital_client = $card_capital_value;
			
			$this->mdl_card_fiche_client_capital->insert ( $oCard_fiche_client_capital );
		}
		
		// get the max capital value into commercant
		$oFichecommercant = $this->mdl_card_capital->getByIdCommercant ( $id_commercant );
		
		// saving into card_bonplan_used
		$card_bonplan_used ['id_card'] = $id_card;
		$card_bonplan_used ['id_commercant'] = $id_commercant;
		$card_bonplan_used ['id_user'] = $id_user;
		$card_bonplan_used ['id_bonplan'] = $id_bonplan;
		$card_bonplan_used ['scan_status'] = "OK";
		$card_bonplan_used ['capital_value_added'] = $card_capital_value;
		$card_bonplan_used ['date_use'] = date ( "Y-m-d h:m:s" );
		$card_bonplan_used ['description'] = "Ajout capital";
		$card_bonplan_used ['objectif_capital'] = $oFichecommercant->montant;
		$card_bonplan_used ['cumul_capital'] = $solde_capital_client;
		$card_bonplan_used ['datefin_capital'] = $oFichecommercant->date_fin;
		
		$this->mdl_card_bonplan_used->insert ( $card_bonplan_used );
		
		echo $id_user . "/" . $id_commercant;
	}
	function add_card_capital_mobile() {
		$id_card = $this->input->post ( 'id_card' );
		if ($this->input->post ( 'id_bonplan' )=="" OR $this->input->post ( 'id_bonplan' )==null){
            $id_bonplan=0;
        }

		$id_commercant = $this->input->post ( 'id_commercant' );
		$id_user = $this->input->post ( 'id_user' );
		$card_capital_value = $this->input->post ( 'unite_capital_value' );

		if ($id_card && $id_commercant && $id_user && $card_capital_value) {
			
			$assoc_client_commercant = $this->assoc_client_commercant_model->get_by_id ( $id_user, $id_commercant );
			if (! $assoc_client_commercant) {
				$this->assoc_client_commercant_model->insert ( array (
						'id_client' => $id_user,
						'id_commercant' => $id_commercant 
				) );
			}
			
			// $this->db->trans_start();
			$oCard_fiche_client_tampon ['id_card'] = $id_card;
			$oCard_fiche_client_tampon ['id_commercant'] = $id_commercant;
			$oCard_fiche_client_tampon ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
			$oCard_fiche_client_tampon ['id_user'] = $id_user;
			
			// checking on card_fiche_client_capital and save in it
			$oFicheclient = $this->mdl_card_fiche_client_capital->getWhereOne ( " id_card='" . $id_card . "' AND id_commercant='" . $id_commercant . "' AND id_user='" . $id_user . "'" );
			$oCard_fiche_client_capital ['id_card'] = $id_card;
			$oCard_fiche_client_capital ['id_commercant'] = $id_commercant;
			$oCard_fiche_client_capital ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
			$oCard_fiche_client_capital ['id_user'] = $id_user;
			
			// get the max capital value into commercant
			$oFichecommercant = $this->mdl_card_capital->getByIdCommercant ( $id_commercant );
			
			if (isset ( $oFicheclient ) && count ( $oFicheclient ) > 0) {
				$capital_client_value = intval ( $oFicheclient->solde_capital );
				$capital_client_value = intval ( $capital_client_value ) + intval ( $card_capital_value );
				
				if (intval ( $capital_client_value ) >= intval ( $oFichecommercant->montant )) {
					redirect ( '/front/fidelity_pro/actualise_compte/' . $id_user );
				}
				$oCard_fiche_client_capital ['id'] = $oFicheclient->id;
				$oCard_fiche_client_capital ['solde_capital'] = $capital_client_value;
                $oCard_fiche_client_capital['date_dernier_validation']=date('Y-m-d');
                $oCard_fiche_client_capital['dernier_value_added']=$card_capital_value;
                $solde_capital_client = $capital_client_value;
				
				$this->mdl_card_fiche_client_capital->update ( $oCard_fiche_client_capital );
				$this->mdl_card_fiche_client_capital->delete_where ( array (
						'id !=' => $oCard_fiche_client_capital ['id'],
						'id_user' => $oCard_fiche_client_capital ['id_user'],
						'id_commercant' => $oCard_fiche_client_capital ['id_commercant'] 
				) );
			} else {
				$oCard_fiche_client_capital ['solde_capital'] = $card_capital_value;
				$solde_capital_client = $card_capital_value;
				
				$this->mdl_card_fiche_client_capital->insert ( $oCard_fiche_client_capital );
			}
			
			// saving into card_bonplan_used
			$card_bonplan_used ['id_card'] = $id_card;
			$card_bonplan_used ['id_commercant'] = $id_commercant;
			$card_bonplan_used ['id_user'] = $id_user;
			$card_bonplan_used ['id_bonplan'] = $id_bonplan;
			$card_bonplan_used ['scan_status'] = "OK";
			$card_bonplan_used ['capital_value_added'] = $card_capital_value;
			$card_bonplan_used ['date_use'] = date ( "Y-m-d h:m:s" );
			$card_bonplan_used ['description'] = "Ajout capital";
			$card_bonplan_used ['objectif_capital'] = $oFichecommercant->montant;
			$card_bonplan_used ['cumul_capital'] = $solde_capital_client;
			$card_bonplan_used ['datefin_capital'] = $oFichecommercant->date_fin;
			
			$this->mdl_card_bonplan_used->insert ( $card_bonplan_used );
			// $this->db->trans_complete();
			$data ['user'] = $user = $this->user->GetById ( $id_user );
			$data ['titre'] = "Client: " . ucfirst ( $user->Prenom ) . ' ' . ucfirst ( $user->Nom );
			$data ['solde_capital_client'] = $solde_capital_client;
			$data ['objectif_capital'] = $oFichecommercant->montant;
			
			$commercant = $this->commercant->GetById($id_commercant);
			$data['client'] = ucfirst ( $user->Prenom ) . ' ' . ucfirst ( $user->Nom );
			$data['commercant'] = $commercant->NomSociete;
			$data['value_added'] = $card_capital_value;
			$data['solde'] = $solde_capital_client;
			$data['objectif'] = $oFichecommercant->montant;
			
			mail_fidelisation($data, $user->Email);
			
			$this->load->view ( 'mobile2014/pro/capital-confirmation-ajout', $data );
		} else {
			redirect ( 'auth/login' );//aut/login
		}
	}
	function add_card_remise_mobile() {
		$id_card = $this->input->post ( 'id_card' );
		$id_bonplan = $this->input->post ( 'id_bonplan' );
		$id_commercant = $this->input->post ( 'id_commercant' );
		$id_user = $this->input->post ( 'id_user' );
		$card_remise_value = $this->input->post ( 'unite_remise_value' );
		$percent_remise = $this->input->post ( 'percent_remise' );
		
		if ($id_card && $id_commercant && $id_user && $card_remise_value) {
			
			$assoc_client_commercant = $this->assoc_client_commercant_model->get_by_id ( $id_user, $id_commercant );
			if (! $assoc_client_commercant) {
				$this->assoc_client_commercant_model->insert ( array (
						'id_client' => $id_user,
						'id_commercant' => $id_commercant 
				) );
			}
			
			// $this->db->trans_start();
			$oCard_fiche_client_tampon ['id_card'] = $id_card;
			$oCard_fiche_client_tampon ['id_commercant'] = $id_commercant;
			$oCard_fiche_client_tampon ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
			$oCard_fiche_client_tampon ['id_user'] = $id_user;
			
			// checking on card_fiche_client_remise and save in it
			$oFicheclient = $this->mdl_card_fiche_client_remise->getWhereOne ( " id_card='" . $id_card . "' AND id_commercant='" . $id_commercant . "' AND id_user='" . $id_user . "'" );
			$oCard_fiche_client_remise ['id_card'] = $id_card;
			$oCard_fiche_client_remise ['id_commercant'] = $id_commercant;
			$oCard_fiche_client_remise ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
			$oCard_fiche_client_remise ['id_user'] = $id_user;
			
			if (isset ( $oFicheclient ) && count ( $oFicheclient ) > 0) {
				$remise_client_value = intval ( $oFicheclient->solde_remise );
				
				// get all card_remise_value but not percent value

				//$card_remise_value = (intval ( $card_remise_value ) * ($percent_remise / 100));
				
				$remise_client_value = intval ( $remise_client_value ) + intval ( $card_remise_value );
				$oCard_fiche_client_remise ['id'] = $oFicheclient->id;
				$oCard_fiche_client_remise ['solde_remise'] = $remise_client_value;
                $oCard_fiche_client_remise['date_dernier_validation']=date('Y-m-d');
                $solde_remise_client = $remise_client_value;
				
				$this->mdl_card_fiche_client_remise->update ( $oCard_fiche_client_remise );
				$this->mdl_card_fiche_client_remise->delete_where ( array (
						'id !=' => $oCard_fiche_client_remise ['id'],
						'id_user' => $oCard_fiche_client_remise ['id_user'],
						'id_commercant' => $oCard_fiche_client_remise ['id_commercant'] 
				) );
			} else {
				$oCard_fiche_client_remise ['solde_remise'] = $card_remise_value;
				$solde_remise_client = $card_remise_value;
				
				$this->mdl_card_fiche_client_remise->insert ( $oCard_fiche_client_remise );
			}
			
			// get the max remise value into commercant
			$oFichecommercant = $this->mdl_card_remise->getByIdCommercant ( $id_commercant );
			
			// saving into card_bonplan_used
			$card_bonplan_used ['id_card'] = $id_card;
			$card_bonplan_used ['id_commercant'] = $id_commercant;
			$card_bonplan_used ['id_user'] = $id_user;
			$card_bonplan_used ['id_bonplan'] = $id_bonplan;
			$card_bonplan_used ['scan_status'] = "OK";
			$card_bonplan_used ['remise_value_added'] = $card_remise_value;
			$card_bonplan_used ['date_use'] = date ( "Y-m-d h:m:s" );
			$card_bonplan_used ['description'] = "Ajout remise";
			$card_bonplan_used ['cumul_remise'] = $solde_remise_client;
			$card_bonplan_used ['datefin_remise'] = $oFichecommercant->date_fin;
			
			$this->mdl_card_bonplan_used->insert ( $card_bonplan_used );
			// $this->db->trans_complete();
			$data ['user'] = $user = $this->user->GetById ( $id_user );
			$data ['titre'] = $user->Prenom . ' ' . $user->Nom;
			$data ['solde_remise_client'] = $solde_remise_client;
			$data ['objectif_remise'] = $oFichecommercant->montant;
			$criteres_assoc ['valide'] = 1;
			$criteres_assoc ['id_commercant'] = $id_commercant;
			$criteres_assoc ['id_user'] = $id_user;
			$assoc_client_bonplan_array = $this->assoc_client_bonplan_model->get_list_by_criteria ( $criteres_assoc );
			
			
			$commercant = $this->commercant->GetById($id_commercant);
			$data['client'] = ucfirst ( $user->Prenom ) . ' ' . ucfirst ( $user->Nom );
			$data['commercant'] = $commercant->NomSociete;
			$data['value_added'] = $card_remise_value;
			$data['solde'] = $solde_remise_client;
			//$data['objectif'] = $oFichecommercant->montant;
				
			mail_fidelisation($data, $user->Email);

			// $data['assoc_client_bon_plan'] = (!empty($assoc_client_bonplan_array)) ? current($assoc_client_bonplan_array) : null;
			$data ['assoc_client_bon_plan'] = get_fiche_client ( "remise", $id_user, $id_commercant );
			$this->load->view ( 'mobile2014/pro/remise-confirmation-ajout', $data );
		} else {
			redirect ( "/front/fidelity/contenupro" );
		}
	}
	function add_card_tampon_mobile() {
		$id_card = $this->input->post ( 'id_card' );
		$id_bonplan = $this->input->post ( 'id_bonplan' );
		$id_commercant = $this->input->post ( 'id_commercant' );
		$id_user = $this->input->post ( 'id_user' );
		$new_tampon_value = $this->input->post ( 'unite_tampon_value' );
		
		if ($id_card && $id_commercant && $id_user && $new_tampon_value) {
			
			$assoc_client_commercant = $this->assoc_client_commercant_model->get_by_id ( $id_user, $id_commercant );
			if (! $assoc_client_commercant) {
				$this->assoc_client_commercant_model->insert ( array (
						'id_client' => $id_user,
						'id_commercant' => $id_commercant 
				) );
			}
			
			// get the max tampon value into commercant
			$oFichecommercant = $this->mdl_card_tampon->getByIdCommercant ( $id_commercant );
			
			// $this->db->trans_start();
			// checking on card_fiche_client_tampon and save in it
			$oFicheclient = $this->mdl_card_fiche_client_tampon->getWhereOne ( " id_card='" . $id_card . "' AND id_commercant='" . $id_commercant . "' AND id_user='" . $id_user . "'" );
			$oCard_fiche_client_tampon ['id_card'] = $id_card;
			$oCard_fiche_client_tampon ['id_commercant'] = $id_commercant;
			$oCard_fiche_client_tampon ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
			$oCard_fiche_client_tampon ['id_user'] = $id_user;
			
			if (isset ( $oFicheclient ) && count ( $oFicheclient ) > 0) {
				$tampon_client_value = intval ( $oFicheclient->solde_tampon );
				$tampon_client_value = intval ( $tampon_client_value ) + $new_tampon_value;
				
				if ($tampon_client_value >= $oFichecommercant->tampon_value) {
					redirect ( '/front/fidelity_pro/actualise_compte/' . $id_user );
				}
				
				$oCard_fiche_client_tampon ['id'] = $oFicheclient->id;
                $oCard_fiche_client_tampon['dernier_value_added']=$new_tampon_value;
                $oCard_fiche_client_tampon['date_dernier_validation']=date('Y-m-d');
                $oCard_fiche_client_tampon['dernier_value_added']=$new_tampon_value;
                $solde_tampon_client = $tampon_client_value;
				
				$oCard_fiche_client_tampon ['solde_tampon'] = $solde_tampon_client;
				$this->mdl_card_fiche_client_tampon->update ( $oCard_fiche_client_tampon );
				$this->mdl_card_fiche_client_tampon->delete_where ( array (
						'id !=' => $oCard_fiche_client_tampon ['id'],
						'id_user' => $oCard_fiche_client_tampon ['id_user'],
						'id_commercant' => $oCard_fiche_client_tampon ['id_commercant'] 
				) );
			} else {
				$solde_tampon_client = $new_tampon_value;
				$oCard_fiche_client_tampon ['solde_tampon'] = $solde_tampon_client;
				
				$this->mdl_card_fiche_client_tampon->insert ( $oCard_fiche_client_tampon );
			}
			
			// saving into card_bonplan_used
			$card_bonplan_used ['id_card'] = $id_card;
			$card_bonplan_used ['id_commercant'] = $id_commercant;
			$card_bonplan_used ['id_user'] = $id_user;
			$card_bonplan_used ['id_bonplan'] = $id_bonplan;
			$card_bonplan_used ['scan_status'] = "OK";
			$card_bonplan_used ['tampon_value_added'] = $new_tampon_value;
			$card_bonplan_used ['date_use'] = date ( "Y-m-d h:m:s" );
			$card_bonplan_used ['description'] = "Ajout tampon";
			$card_bonplan_used ['objectif_tampon'] = $oFichecommercant->tampon_value;
			$card_bonplan_used ['cumul_tampon'] = $solde_tampon_client;
			$card_bonplan_used ['datefin_tampon'] = $oFichecommercant->date_fin;
			$this->mdl_card_bonplan_used->insert ( $card_bonplan_used );
			// $this->db->trans_complete();
			$data ['user'] = $user = $this->user->GetById ( $id_user );
			$data ['titre'] = $user->Prenom . ' ' . $user->Nom;
			$data ['solde_tampon_client'] = $solde_tampon_client;
			$data ['objectif_tampon'] = $oFichecommercant->tampon_value;
			
			
			$commercant = $this->commercant->GetById($id_commercant);
			$data['client'] = ucfirst ( $user->Prenom ) . ' ' . ucfirst ( $user->Nom );
			$data['commercant'] = $commercant->NomSociete;
			$data['value_added'] = $new_tampon_value;
			$data['solde'] = $solde_tampon_client;
			$data['objectif'] = $oFichecommercant->tampon_value;
			
			mail_fidelisation($data, $user->Email);
			
			
			$this->load->view ( 'mobile2014/pro/tampon-confirmation-ajout', $data );
		} else {
			redirect ( 'front/fidelity/contenupro' );
		}
	}
	function card_bonplan_validate() {

		if ($this->input->post ( 'id_card' )) {
			$id_card = $this->input->post ( 'id_card' );
			$id_bonplan = $this->input->post ( 'id_bonplan' );
			$id_commercant = $this->input->post ( 'id_commercant' );
			$id_client_bonplan = $this->input->post ( 'id_client_bonplan' );
			//var_dump($id_card);
            //var_dump($id_bonplan);
            //var_dump($id_commercant);
            //var_dump($id_client_bonplan);die();
			// $this->assoc_client_bonplan_model->validate_bonplan($id_bonplan,$id_user);
			
			// confirmation by commercant
			$this->assoc_client_bonplan_model->validate_bonplan_by_id ( $id_client_bonplan );

			//decrementation nb bonplan
			//$this->mdlbonplan->decremente_quantite_bonplan($id_bonplan, $nb_decrement);
			$data ['id_user'] = $id_user = $this->input->post ( 'id_user' );
			

			$objBonplan = $this->mdlbonplan->getById ( $id_bonplan );
			/*$objBonplanCumule=$this->assoc_client_bonplan_model->get_cumule_by_id_com($id_commercant);
			$cumulate_bonplan=array(
			        'bonplan_id'=>$objBonplan->bonplan_id,
                    'commercant_id'=>$objBonplan->bonplan_commercant_id,
                    'cumule_conso'=>'25',
            );
			$this->assoc_client_bonplan_model->cumulate_bonplan_price();*/
			$data ["oBonplan"] = $objBonplan;

			$data ["objReservationNonValideCom"] = $this->assoc_client_bonplan_model->get_where(array('assoc_client_bonplan.id_bonplan'=>$id_bonplan,'assoc_client_bonplan.valide'=>0));
			
			$data ['user'] = $user = $this->user->GetById ( $id_user );
			$data ['titre'] = "Client: " . ucfirst ( $user->Prenom ) . ' ' . ucfirst ( $user->Nom );

			
			$assoc_client_commercant = $this->assoc_client_commercant_model->get_by_id ( $id_user, $id_commercant );
			if (! $assoc_client_commercant) {
				$this->assoc_client_commercant_model->insert ( array (
						'id_client' => $id_user,
						'id_commercant' => $id_commercant 
				) );
			}
			
			$commercant = $this->commercant->GetById($id_commercant);
			$data['client'] = ucfirst ( $user->Prenom ) . ' ' . ucfirst ( $user->Nom );
			$data['commercant'] = $commercant->NomSociete;
		
			$mail = $this->load->view('mobile2014/email/validation_bonplan',$data,TRUE);
			$zContactEmailCom_ = array();
            $zContactEmailCom_[] = array("Email"=>$user->Email,"Name"=>$user->Nom." ".$user->Prenom);
			@envoi_notification($zContactEmailCom_,'[Privicarte] Validation de bon plan',html_entity_decode(htmlentities($mail)), 'contact@privicarte.fr');
			

			$this->load->view ( 'mobile2014/pro/bonplan-confirmation-validation', $data );
		} else {
			redirect ( 'front/fidelity/contenupro' );
		}
	}
	function card_tampon_client_add() {
		$id_card = $this->input->post ( 'id_card' );
		$id_bonplan = $this->input->post ( 'id_bonplan' );
		$id_commercant = $this->input->post ( 'id_commercant' );
		$id_user = $this->input->post ( 'id_user' );
		
		// checking on card_fiche_client_tampon and save in it
		$oFicheclient = $this->mdl_card_fiche_client_tampon->getWhereOne ( " id_card='" . $id_card . "' AND id_commercant='" . $id_commercant . "' AND id_user='" . $id_user . "'" );
		if (isset ( $oFicheclient ) && count ( $oFicheclient ) > 0) {
			$tampon_client_value = intval ( $oFicheclient->solde_tampon );
			$tampon_client_value = intval ( $tampon_client_value ) + 1;
			$oCard_fiche_client_tampon ['id'] = $oFicheclient->id;
			$oCard_fiche_client_tampon ['id_card'] = $id_card;
			$oCard_fiche_client_tampon ['id_commercant'] = $id_commercant;
			$oCard_fiche_client_tampon ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
			$oCard_fiche_client_tampon ['id_user'] = $id_user;
			$oCard_fiche_client_tampon ['solde_tampon'] = $tampon_client_value;
			$solde_tampon_client = $tampon_client_value;
			
			$this->mdl_card_fiche_client_tampon->update ( $oCard_fiche_client_tampon );
		} else {
			$oCard_fiche_client_tampon ['id_card'] = $id_card;
			$oCard_fiche_client_tampon ['id_commercant'] = $id_commercant;
			$oCard_fiche_client_tampon ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
			$oCard_fiche_client_tampon ['id_user'] = $id_user;
			$oCard_fiche_client_tampon ['solde_tampon'] = "1";
			$solde_tampon_client = "1";
			
			$this->mdl_card_fiche_client_tampon->insert ( $oCard_fiche_client_tampon );
		}
		
		// get the max tampon value into commercant
		$oFichecommercant = $this->mdl_card_tampon->getByIdCommercant ( $id_commercant );
		
		// saving into card_bonplan_used
		$card_bonplan_used ['id_card'] = $id_card;
		$card_bonplan_used ['id_commercant'] = $id_commercant;
		$card_bonplan_used ['id_user'] = $id_user;
		$card_bonplan_used ['id_bonplan'] = $id_bonplan;
		$card_bonplan_used ['scan_status'] = "OK";
		$card_bonplan_used ['tampon_value_added'] = "1";
		$card_bonplan_used ['date_use'] = date ( "Y-m-d h:m:s" );
		$card_bonplan_used ['description'] = "Ajout tampon";
		$card_bonplan_used ['objectif_tampon'] = $oFichecommercant->tampon_value;
		$card_bonplan_used ['cumul_tampon'] = $solde_tampon_client;
		$card_bonplan_used ['datefin_tampon'] = $oFichecommercant->date_fin;
		
		$this->mdl_card_bonplan_used->insert ( $card_bonplan_used );
		
		echo $id_user . "/" . $id_commercant;
	}
	function card_capital_client_validate() {
		$data ['pagetitle'] = "Carte capitalisée !";
		$IdUser = $this->uri->rsegment ( 3 );
		$id_commercant = $this->uri->rsegment ( 4 );
		
		$oFicheclient = $this->mdl_card_fiche_client_capital->getWhereOne ( " id_user='" . $IdUser . "' AND id_commercant='" . $id_commercant . "' " );
		$data ['oFicheclient'] = $oFicheclient;
		
		$objcapital = $this->mdl_card_capital->getByIdCommercant ( $id_commercant );
		$data ["oCapital"] = $objcapital;
		
		$this->load->view ( "fidelity/vwcard_capital_client_validate", $data );
	}
	function card_tampon_client_validate() {
		$data ['pagetitle'] = "Carte tamponnée !";
		$IdUser = $this->uri->rsegment ( 3 );
		$id_commercant = $this->uri->rsegment ( 4 );
		
		$oFicheclient = $this->mdl_card_fiche_client_tampon->getWhereOne ( " id_user='" . $IdUser . "' AND id_commercant='" . $id_commercant . "' " );
		$data ['oFicheclient'] = $oFicheclient;
		
		$objTampon = $this->mdl_card_tampon->getByIdCommercant ( $id_commercant );
		$data ["oTampon"] = $objTampon;
		
		$this->load->view ( "fidelity/vwcard_tampon_client_validate", $data );
	}
	function card_capital_check_client() {
		$id_card = $this->input->post ( 'id_card' );
		$id_bonplan = $this->input->post ( 'id_bonplan' );
		$id_commercant = $this->input->post ( 'id_commercant' );
		$id_user = $this->input->post ( 'id_user' );
		
		$capital_status = "1";
		
		$objCommercant_card_capital = $this->mdl_card_capital->getByIdCommercant ( $id_commercant );
		$capital_commercant_value = is_object ( $objCommercant_card_capital ) ? $objCommercant_card_capital->montant : 0;
		
		$oFicheclient = $this->mdl_card_fiche_client_capital->getWhereOne ( " id_card='" . $id_card . "' AND id_user='" . $id_user . "' AND id_commercant='" . $id_commercant . "' " );
		if (isset ( $oFicheclient ) && count ( $oFicheclient ) > 0) {
			$capital_client_value = $oFicheclient->solde_capital;
			if ($capital_client_value == $capital_commercant_value) {
				$capital_status = "0";
			}
		} else {
			$capital_client_value = "0";
		}
		
		if ($capital_status == "1") {
			?>
<fieldset>
	<legend>Capital</legend>
	<table width="100%" border="0">
		<tr>
			<td>Solde Client : <strong><?php echo $capital_client_value;?>€</strong></td>
		</tr>
		<tr>
			<td>Montant à atteindre : <strong><?php echo $capital_commercant_value;?>€</strong></td>
		</tr>
		<tr>
			<td>Voulez-vous capitaliser cette carte ?<br /> <input type="button"
				name="btn_card_capital_oui" id="btn_card_capital_oui"
				onClick="javascript:btn_card_capital_oui();" value="Oui" />&nbsp; <input
				type="button" name="btn_card_capital_non" id="btn_card_capital_non"
				onClick="javascript:btn_card_capital_non();" value="Non" />
			</td>
		</tr>
	</table>
</fieldset>
<?php
		} else {
			?>
<fieldset>
	<legend>capital</legend>
	<table width="100%" border="0">
		<tr>
			<td>Solde Client : <strong><?php echo $capital_client_value;?>€</strong></td>
		</tr>
		<tr>
			<td>Montant à atteindre : <strong><?php echo $capital_commercant_value;?>€</strong></td>
		</tr>
		<tr>
			<td>Les points capitals sont atteints<br />Voulez-vous reinitialiser
				cette carte ? <br /> <input type="button"
				name="btn_card_capital_renew_oui" id="btn_card_capital_renew_oui"
				onClick="javascript:btn_card_capital_renew_oui();" value="Oui" />&nbsp;
				<input type="button" name="btn_card_capital_renew_non"
				id="btn_card_capital_renew_non"
				onClick="javascript:btn_card_capital_non();" value="Non" />
			</td>
		</tr>
	</table>
</fieldset>
<?php
		}
	}
	function card_tampon_check_client() {
		$id_card = $this->input->post ( 'id_card' );
		$id_bonplan = $this->input->post ( 'id_bonplan' );
		$id_commercant = $this->input->post ( 'id_commercant' );
		$id_user = $this->input->post ( 'id_user' );
		
		$tampon_status = "1";
		
		$objCommercant_card_tampon = $this->mdl_card_tampon->getByIdCommercant ( $id_commercant );
		$tampon_commercant_value = (is_object ( $objCommercant_card_tampon )) ? $objCommercant_card_tampon->tampon_value : 0;
		
		$oFicheclient = $this->mdl_card_fiche_client_tampon->getWhereOne ( " id_card='" . $id_card . "' AND id_user='" . $id_user . "' AND id_commercant='" . $id_commercant . "' " );
		if (isset ( $oFicheclient ) && count ( $oFicheclient ) > 0) {
			$tampon_client_value = $oFicheclient->solde_tampon;
			if ($tampon_client_value == $tampon_commercant_value) {
				$tampon_status = "0";
			}
		} else {
			$tampon_client_value = "0";
		}
		
		if ($tampon_status == "1") {
			?>
<fieldset>
	<legend>Tampon</legend>
	<table width="100%" border="0">
		<tr>
			<td>Points Client : <strong><?php echo $tampon_client_value;?></strong></td>
		</tr>
		<tr>
			<td>Points à atteindre : <strong><?php echo $tampon_commercant_value;?></strong></td>
		</tr>
		<tr>
			<td>Voulez-vous tamponner cette carte ?<br /> <input type="button"
				name="btn_card_tampon_oui" id="btn_card_tampon_oui"
				onClick="javascript:btn_card_tampon_oui();" value="Oui" />&nbsp; <input
				type="button" name="btn_card_tampon_non" id="btn_card_tampon_non"
				onClick="javascript:btn_card_tampon_non();" value="Non" />
			</td>
		</tr>
	</table>
</fieldset>
<?php
		} else {
			?>
<fieldset>
	<legend>Tampon</legend>
	<table width="100%" border="0">
		<tr>
			<td>Points Client : <strong><?php echo $tampon_client_value;?></strong></td>
		</tr>
		<tr>
			<td>Points à atteindre : <strong><?php echo $tampon_commercant_value;?></strong></td>
		</tr>
		<tr>
			<td>Les points Tampons sont atteints<br />Voulez-vous reinitialiser
				cette carte ? <br /> <input type="button"
				name="btn_card_tampon_renew_oui" id="btn_card_tampon_renew_oui"
				onClick="javascript:btn_card_tampon_renew_oui();" value="Oui" />&nbsp;
				<input type="button" name="btn_card_tampon_renew_non"
				id="btn_card_tampon_renew_non"
				onClick="javascript:btn_card_tampon_non();" value="Non" />
			</td>
		</tr>
	</table>
</fieldset>
<?php
		}
	}
	function contact_list() {
		$user_ion_auth_id = $this->ion_auth->user ()->row ();
		$id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
		
		$oContacts = $this->mdl_card_bonplan_used->getByIdCommercant ( $id_commercant );
		$data ['oContacts'] = $oContacts;
		
		$this->load->view ( "fidelity/vwcontact_list", $data );
	}
	function liste_reservation($filtre = null, $ordre = null) {
		if ($this->ion_auth->logged_in () && ! $this->ion_auth->in_group ( 2 )) {
			if ($filtre == "capitalisation" || $filtre == null) {
				$data ['capitalisation'] = 1;
			}
			if ($filtre == "tampon" || $filtre == null) {
				$data ['tampon'] = 1;
			}
			$criteres ['valide'] = 0;
			
			$user_ion_auth_id = $this->ion_auth->user ()->row ();
			$id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
			$criteres ['id_commercant'] = $id_commercant;
			$order_by ['assoc_client_bonplan.date_validation'] = ($ordre == "desc") ? "desc" : "asc";
			$data ['list_client'] = $this->assoc_client_bonplan_model->get_list_by_criteria ( $criteres, $order_by );
            //var_dump($data ['list_client']);
			// echo $this->db->last_query();
			$data ['titre'] = 'Liste des reservations en cours';

			$data ['mdlbonplan'] = $this->mdlbonplan;
			$data ['assoc_client_bonplan_model'] = $this->assoc_client_bonplan_model;

			$list_plat= $this->assoc_client_plat_model->get_list_reservation_plat_by_idcom ($id_commercant);
			$data['list_plat']=$list_plat;
			$list_table=$this->assoc_client_table_model->get_list_reservation_table_by_idcom ($id_commercant);
			//var_dump($list_table);die();
			$data['liste_table']=$list_table;
            $list_sejour=$this->assoc_client_sejour_model->get_list_reservation_sejour_by_idcom ($id_commercant);
            //var_dump($list_table);die();
            $data['liste_sejour']=$list_sejour;
			//var_dump($list_plat);die();

			$this->load->view ('mobile2014/pro/liste-clients_reservations', $data );
		} else {
			redirect ( 'auth/login' );
		}
	}
	public function liste_clients($action = false, $nb = null) {
		if ($this->ion_auth->logged_in () && ! $this->ion_auth->in_group ( 2 )) {
			$user_ion_auth_id = $this->ion_auth->user ()->row ();
			$id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
			
			$data ['titre'] = 'Liste de mes clients';
			$data ['print'] = 0;
			$data ['email_clients'] = array ();
			$data ['mail_to_clients'] = '';
			$data ['show_bonplan'] = 1;
			$data ['show_offre'] = 1;
			$criteres = array ();
			if ($action == "mois" && $nb != null) {
				$criteres ['month'] = $nb;
			}
			
			$mail = "";
			$list_client = $this->assoc_client_commercant_model->get_by_commercant_id ( $id_commercant, $criteres );
			$email_clients = array ();
			if (! empty ( $list_client )) {
				foreach ( $list_client as $cleint ) {
					$mail = "mailto:contact@sortez.org?subject=Message";
					$mail .= "&bcc=$cleint->Email";
					$email_clients [] = $cleint->Email;
				}
					
				$data ['email_clients'] = $mail;
					
				$clients = array ();
				foreach ( $list_client as $client ) {
					$fidelity = get_fidelity ( $client->id_commercant );
					$offre = get_offre_active ( $client->id_commercant );
					$fiche = get_fiche_client ( $offre, $client->id_client, $client->id_commercant );
					$user_bonplan = get_user_bonplan ( $client->id_client, $client->id_commercant );
			
					$client->offre = $offre;
					$client->solde = ! empty ( $fiche ) ? $fiche->solde : 0;
			
					switch ($offre) {
						case "capital" :
							$client->objectif = (is_object ( $fidelity ) && is_object ( $fidelity->capital )) ? $fidelity->capital->montant : 0;
							break;
						case "tampon" :
							$client->objectif = (is_object ( $fidelity ) && is_object ( $fidelity->tampon )) ? $fidelity->tampon->tampon_value : 0;
							break;
						case "remise" :
							$client->objectif = $client->solde;
							break;
					}
			
					$client->bonplan = $user_bonplan;
					$clients [] = $client;
					// $client->cumul = ($offre == "remise" && is_object($fidelity)) ? $fidelity->remise : 0;
				}
					
				if ($action == "solde_desc")
					usort ( $clients, "solde_desc" );
					if ($action == "solde_asc")
						usort ( $clients, "solde_asc" );
							
						$data ['clients'] = $clients;
			}
			
			if ($action == 'print') {
				$data ['print'] = 1;
				//$list_client = $this->assoc_client_commercant_model->get_by_commercant_id ( $id_commercant, $criteres );
				//$data ['clients'] = $list_client;
				echo $this->load->view ( 'mobile2014/pro/liste-clients-print', $data, true );
			} else if ($action == 'csv') {
				$this->load->dbutil ();
				$criteres ['query'] = 1;
				$query = $this->assoc_client_commercant_model->get_by_commercant_id_csv ( $id_commercant );
				$csv = $this->dbutil->csv_from_result ( $query );
				$this->load->helper ( 'download' );
				force_download ( 'liste_contact.csv', $csv );
			} else {
				
				
				if ($action == 'bonplan') {
					$data ['show_offre'] = 0;
				}
				if ($action == 'fidelisation') {
					$data ['show_bonplan'] = 0;
					$data ['show_offre'] = 1;
				}
				$this->load->view ( 'mobile2014/pro/liste-clients2', $data );
			}
		}
	}
	function liste_contact($filtre = null, $ordre = null, $print = null, $month = null) {
		/*
		 * if ($this->ion_auth->logged_in() && !$this->ion_auth->in_group(2)) {
		 *
		 * if($filtre == "capitalisation" || $filtre == null){
		 * $data['capitalisation'] = 1;
		 * }
		 * if($filtre == "tampon" || $filtre == null ){
		 * $data['tampon'] = 1;
		 * }
		 * if($filtre == "bonplan_valide" || $filtre == null || $filtre=='tous'){
		 * $data['bonplan_valide'] = 1;
		 * }
		 * if($filtre == "month" && $month){
		 * $criteres['month'] = $month;
		 * }
		 *
		 * $user_ion_auth_id = $this->ion_auth->user()->row();
		 * $id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth_id->id);
		 *
		 * $criteres['valide'] = 1;
		 * $criteres['id_commercant'] = $id_commercant;
		 * $order_by['assoc_client_bonplan.date_validation']= ($ordre=="desc") ? "desc" : "asc";
		 *
		 * $data['list_client'] = $this->assoc_client_bonplan_model->get_list_by_criteria($criteres,$order_by);
		 * $data['list_client'] = !empty($data['list_client']) ? $data['list_client'] : array();
		 * $email_clients = array();
		 * foreach ($data['list_client'] as $cleint){
		 * $email_clients[] = $cleint->user_email;
		 * }
		 * $data['email_clients'] = $email_clients;
		 * $data['mail_to_clients'] = implode(';', $email_clients);
		 * //echo $this->db->last_query();
		 * $data['titre'] = 'Liste de mes clients';
		 * $data['print'] = $print;
		 * if($print=='print'){
		 * echo $this->load->view('mobile2014/pro/liste-clients',$data,true);
		 * }else if($print=='csv'){
		 * $this->load->dbutil();
		 * $criteres['query'] = 1;
		 * $query = $this->assoc_client_bonplan_model->get_list_by_criteria($criteres,$order_by);;
		 * $csv = $this->dbutil->csv_from_result($query);
		 * $this->load->helper('download');
		 * force_download('liste_contact.csv', $csv);
		 * }else{
		 * $this->load->view('mobile2014/pro/liste-clients',$data,false);
		 * }
		 * }else{
		 * redirect('auth/login');
		 * }
		 */
		redirect ( 'front/fidelity/liste_clients' );
	}
	function send_message_client() {
		if ($this->ion_auth->logged_in () && ! $this->ion_auth->in_group ( 2 ) && $this->input->post ( 'email_clients' )) {
			$data ['titre'] = "Envoi message aux clients";
			$data ['email_clients'] = $this->input->post ( 'email_clients' );
			$this->load->view ( 'mobile2014/pro/send_message_client', $data );
		}
		// redirect('/');
	}
	function detail_client___($idUser) {
		if ($this->ion_auth->logged_in () && ! $this->ion_auth->in_group ( 2 )) {
			
			$user_ion_auth_id = $this->ion_auth->user ()->row ();
			$id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
			$data ['titre'] = "Historique consommation";
			$critere = array (
					'card_bonplan_used.id_commercant' => $id_commercant,
					'card_bonplan_used.id_user' => $idUser 
			);
			// $critere['card_bonplan_used.capital_value_added !='] ='';
			$order_by = array (
					'champ' => 'card_bonplan_used.id',
					'ordre' => 'ASC' 
			);
			$data ['list_activity'] = $this->mdl_card_bonplan_used->get_by_critere ( $critere );
			$criteres_assoc ['valide'] = 1;
			$criteres_assoc ['id_commercant'] = $id_commercant;
			$criteres_assoc ['id_user'] = $idUser;
			
			if ($assoc_client_bon_plan = $this->assoc_client_bonplan_model->get_list_by_criteria ( $criteres_assoc )) {
				$data ['assoc_client_bonplan'] = current ( $assoc_client_bon_plan );
				$this->load->view ( "mobile2014/pro/detail-capitalisation", $data );
			} else {
				redirect ( 'front/fidelity/liste_clients' );
			}
		} else {
			redirect ( 'auth/login' );
		}
	}
	function detail_client($idUser) {
		if ($this->ion_auth->logged_in () && ! $this->ion_auth->in_group ( 2 )) {
			
			$user_ion_auth_id = $this->ion_auth->user ()->row ();
			$id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
			//var_dump($id_commercant);
			$data ['titre'] = "Historique consommation";
			$critere = array (
					'card_bonplan_used.id_commercant' => $id_commercant,
					'card_bonplan_used.id_user' => $idUser 
			);
			$order_by = array (
					'champ' => 'card_bonplan_used.id',
					'ordre' => 'ASC' 
			);
			$data ['list_activity'] = $this->mdl_card_bonplan_used->get_by_critere ( $critere );
			$data ['offre'] = $offre = get_offre_active ( $id_commercant );
			$data ['client'] = $this->user->GetById ( $idUser );
			$data ['fidelity'] = get_fiche_client ( $offre, $idUser, $id_commercant );
			$data ['user_bonplan'] = get_user_bonplan ( $idUser, $id_commercant );
			$infocom=$this->mdlcommercant->gest_by_id_user($idUser);
			$data['idcom']=$id_commercant;
			//var_dump($infocom);die();
            $data ['infocom'] = $infocom;
			$this->load->view ( "mobile2014/pro/detail-capitalisation", $data );
		} else {
			redirect ( 'auth/login' );
		}
	}
	function delivrance_carte() {
		if ($this->ion_auth->logged_in () && ! $this->ion_auth->in_group ( 2 )) {
			$data ['titre'] = "Souscription et délivrance <br/>d'une carte physique";
			// $data["colVilles"] = $this->Ville->GetAll();
			$this->load->view ( 'mobile2014/pro/creation-consommateur', $data );
		} else {
			redirect ( 'auth/login' );
		}
	}
	function filtre_client() {
		if ($this->ion_auth->logged_in () && ! $this->ion_auth->in_group ( 2 )) {
			$data ['titre'] = "Filtrez les résultats";
			$this->load->view ( 'mobile2014/pro/filtre', $data );
		} else {
			redirect ( 'auth/login' );
		}
	}
	function filtre_mois() {
		if ($this->ion_auth->logged_in () && ! $this->ion_auth->in_group ( 2 )) {
			$user_ion_auth_id = $this->ion_auth->user ()->row ();
			$id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
			
			$criteres ['valide'] = 1;
			$criteres ['id_commercant'] = $id_commercant;
			$criteres ['num_rows'] = 1;
			$mois = array ();
			
			$criteres ['month'] = 1;
			$mois ['janvier'] = ($dec = $this->assoc_client_commercant_model->get_by_commercant_id ( $id_commercant, $criteres )) ? count ( $dec ) : 0;
			
			$criteres ['month'] = 2;
			$mois ['fevrier'] = ($dec = $this->assoc_client_commercant_model->get_by_commercant_id ( $id_commercant, $criteres )) ? count ( $dec ) : 0;
			
			$criteres ['month'] = 3;
			$mois ['mars'] = ($dec = $this->assoc_client_commercant_model->get_by_commercant_id ( $id_commercant, $criteres )) ? count ( $dec ) : 0;
			
			$criteres ['month'] = 4;
			$mois ['avril'] = ($dec = $this->assoc_client_commercant_model->get_by_commercant_id ( $id_commercant, $criteres )) ? count ( $dec ) : 0;
			
			$criteres ['month'] = 5;
			$mois ['mai'] = ($dec = $this->assoc_client_commercant_model->get_by_commercant_id ( $id_commercant, $criteres )) ? count ( $dec ) : 0;
			
			$criteres ['month'] = 6;
			$mois ['juin'] = ($dec = $this->assoc_client_commercant_model->get_by_commercant_id ( $id_commercant, $criteres )) ? count ( $dec ) : 0;
			
			$criteres ['month'] = 7;
			$mois ['juillet'] = ($juillet = $this->assoc_client_commercant_model->get_by_commercant_id ( $id_commercant, $criteres )) ? count ( $juillet ) : 0;
			
			$criteres ['month'] = 8;
			$mois ['aout'] = ($aout = $this->assoc_client_commercant_model->get_by_commercant_id ( $id_commercant, $criteres )) ? count ( $aout ) : 0;
			
			$criteres ['month'] = 9;
			$mois ['septembre'] = ($septembre = $this->assoc_client_commercant_model->get_by_commercant_id ( $id_commercant, $criteres )) ? count ( $septembre ) : 0;
			
			$criteres ['month'] = 10;
			$mois ['octobre'] = ($octobre = $this->assoc_client_commercant_model->get_by_commercant_id ( $id_commercant, $criteres )) ? count ( $octobre ) : 0;
			
			$criteres ['month'] = 11;
			$mois ['novembre'] = ($novembre = $this->assoc_client_commercant_model->get_by_commercant_id ( $id_commercant, $criteres )) ? count ( $novembre ) : 0;
			
			$criteres ['month'] = 12;
			$mois ['decembre'] = ($dec = $this->assoc_client_commercant_model->get_by_commercant_id ( $id_commercant, $criteres )) ? count ( $dec ) : 0;
			$data ['mois'] = $mois;
			$data ['titre'] = "Filtrez les résultats";
			$this->load->view ( 'mobile2014/pro/filtre_mois', $data );
		} else
			redirect ( $this->agent->referrer () );
	}
	function user($userId) {
		if (! empty ( $userId ) && $this->ion_auth->logged_in () && ! $this->ion_auth->in_group ( 2 )) {
			$oUser = $this->user->GetById ( $userId );
			if (! empty ( $oUser )) {
				$data ['titre'] = "Détail de l'utilisateur";
				$data ['user'] = $oUser;
				$this->load->view ( 'mobile2014/pro/user-detail', $data );
			} else
				redirect ( $this->agent->referrer () );
		} else {
			redirect ( $this->agent->referrer () );
		}
	}
	function actualise_compte($id_user) {
		if (! empty ( $id_user ) && $this->ion_auth->logged_in () && ! $this->ion_auth->in_group ( 2 )) {
			if (! empty ( $id_user )) {
				$user_ion_auth_id = $this->ion_auth->user ()->row ();
				$id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
				
				$criteres_assoc ['valide'] = 1;
				$criteres_assoc ['id_commercant'] = $id_commercant;
				$criteres_assoc ['id_user'] = $id_user;
				$assoc_client_commercant = $this->assoc_client_commercant_model->get_by_id ( $id_user, $id_commercant );
				
				if ($assoc_client_commercant) {
					$data ['titre'] = 'Actualisez un compte client';
					$data ['idUser'] = $id_user;
					$data ['idCommercant'] = $id_commercant;
					$data ['client'] = $this->user->GetById ( $id_user );
					$data ['offre'] = $offre = get_offre_active ( $id_commercant );
					$data ['client'] = $this->user->GetById ( $id_user );
					$data ['fidelity'] = get_fiche_client ( $offre, $id_user, $id_commercant );
					$data ['user_bonplan'] = get_user_bonplan ( $id_user, $id_commercant );
					
					$this->load->view ( 'mobile2014/pro/actualisez-compte', $data );
				}
			} else
				redirect ( $this->agent->referrer () );
		} else {
			redirect ( $this->agent->referrer () );
		}
	}
	function reinistialisation() {
		$user_ion_auth_id = $this->ion_auth->user ()->row ();
		$id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
		
		$id_user = $this->input->post ( 'id_user' );
		$montant_init = $this->input->post ( 'montant_init' );
		$bonplan_id = $this->input->post ( 'bonplan_id' );
		$fiche_id = $this->input->post ( 'fiche_id' );
		$montant_init = $this->input->post ( 'montant_init' );
		$montant_init = ! empty ( $montant_init ) ? $montant_init : 0;
		
		$assoc_client_commercant = $this->assoc_client_commercant_model->get_by_id ( $id_user, $id_commercant );
		//var_dump($assoc_client_commercant);die();
		$oCard = $this->mdl_card->getByIdUser ( $id_user );
		
		$data_fiche ['id_user'] = $oCard->id_user;
		$data_fiche ['id_card'] = $oCard->id;
		$data_fiche ['id_ionauth_client'] = $oCard->id_ionauth_user;
		$data_fiche ['id_commercant'] = $id_commercant;
		
		if ($assoc_client_commercant) {
			if ($this->input->post ( 'type_offre' )) {
				$offre = $this->input->post ( 'type_offre' );
				
				if ($offre == "tampon") {
					$card_bonplan_used ['cumul_tampon'] = $montant_init;
					$card_bonplan_used ['tampon_value_added'] = $montant_init;
					$card_bonplan_used ['description'] = "Reinitialisation tampon";
					if ($fiche_id) {
						$this->mdl_card_fiche_client_tampon->update_where ( array (
								'solde_tampon' => $montant_init 
						), array (
								'id' => $fiche_id 
						) );
					} else {
						$data_fiche ['solde_capital'] = $montant_init;
						$this->mdl_card_fiche_client_tampon->insert ( $data_fiche );
					}
				}
				if ($offre == "remise") {
					$card_bonplan_used ['cumul_remise'] = $montant_init;
					$card_bonplan_used ['remise_value_added'] = $montant_init;
					$card_bonplan_used ['description'] = "Reinitialisation remise";
					if ($fiche_id)
						$this->mdl_card_fiche_client_remise->update_where ( array (
								'solde_remise' => $montant_init 
						), array (
								'id' => $fiche_id 
						) );
					else {
						$data_fiche ['solde_remise'] = $montant_init;
						$this->mdl_card_fiche_client_remise->insert ( $data_fiche );
					}
				}
				if ($offre == "capital") {
					$card_bonplan_used ['cumul_capital'] = $montant_init;
					$card_bonplan_used ['capital_value_added'] = $montant_init;
					$card_bonplan_used ['description'] = "Reinitialisation capital";
					if ($fiche_id)
						$this->mdl_card_fiche_client_capital->update_where ( array (
								'solde_capital' => $montant_init 
						), array (
								'id' => $fiche_id 
						) );
					else {
						$data_fiche ['solde_remise'] = $montant_init;
						$this->mdl_card_fiche_client_remise->insert ( $data_fiche );
					}
				}
				
				// saving into card_bonplan_used
				$card_bonplan_used ['id_commercant'] = $id_commercant;
				$card_bonplan_used ['id_user'] = $id_user;
				 $card_bonplan_used['id_bonplan'] = $bonplan_id;
				$card_bonplan_used ['scan_status'] = "OK";
				$card_bonplan_used ['date_use'] = date ( "Y-m-d h:m:s" );
				//var_dump($card_bonplan_used);die();
				$this->mdl_card_bonplan_used->deleteByUCid ( $id_user, $id_commercant );
				$this->mdl_card_bonplan_used->insert ( $card_bonplan_used );
				if ($offre == "bonplan"){
                    $assoc_client_bonplan_id = $this->input->post ( 'assoc_client_bonplan_id' );
                    $this->assoc_client_bonplan_model->deleteById ( $assoc_client_bonplan_id );
                }
			} else if ($this->input->post ( 'assoc_client_bonplan_id' )) {
				$assoc_client_bonplan_id = $this->input->post ( 'assoc_client_bonplan_id' );
				$this->assoc_client_bonplan_model->deleteById ( $assoc_client_bonplan_id );
			}
		}
		redirect ( $this->agent->referrer () );
		/*
		 * $oCard = $this->mdl_card->getByIdUser($IdUser);
		 *
		 *
		 *
		 * /*if(!empty($id_user) && !empty($id_commercant)){
		 *
		 * $criteres_assoc['valide'] = 1;
		 * $criteres_assoc['id_commercant'] = $id_commercant;
		 * $criteres_assoc['id_user'] = $id_user;
		 *
		 * $assoc_client_bon_plan_array = $this->assoc_client_bonplan_model->get_list_by_criteria($criteres_assoc);
		 *
		 * if(!empty($assoc_client_bon_plan_array)){
		 *
		 * $assoc_client_bon_plan = current($assoc_client_bon_plan_array);
		 * // print_r($assoc_client_bon_plan);
		 * //die();
		 * $prmData['id_user'] = $id_user;
		 * $prmData['id_commercant'] = $id_commercant;
		 * if(!$remise_zero){
		 * if($assoc_client_bon_plan->param_remise_active){
		 * $prmData['solde_remise'] =(!empty($montant_init)) ? $montant_init: 0;
		 * $card_bonplan_used['cumul_remise'] = $montant_init;
		 * $card_bonplan_used['remise_value_added'] = $montant_init;
		 * $this->mdl_card_fiche_client_remise->updateByUCId($prmData);
		 * }elseif ($assoc_client_bon_plan->param_tampon_active){
		 * $prmData['solde_tampon'] =(!empty($montant_init)) ? $montant_init: 0;
		 * $card_bonplan_used['cumul_tampon'] = $montant_init;
		 * $card_bonplan_used['tampon_value_added'] = $montant_init;
		 * if(intval($assoc_client_bon_plan->tampon_value) > intval($montant_init)){
		 * $this->mdl_card_fiche_client_tampon->updateByUCId($prmData);
		 * }
		 * }elseif ($assoc_client_bon_plan->param_captital_active){
		 * $prmData['solde_capital'] =(!empty($montant_init)) ? $montant_init: 0;
		 * $card_bonplan_used['cumul_capital'] = $montant_init;
		 * $card_bonplan_used['capital_value_added'] = $montant_init;
		 * if($assoc_client_bon_plan->objectif_capital > $montant_init){
		 * $this->mdl_card_fiche_client_capital->updateByUCId($prmData);
		 * }
		 * }
		 * }else{
		 * $this->mdl_card_fiche_client_remise->deleteByUCid($id_user,$id_commercant);
		 * $this->mdl_card_fiche_client_tampon->deleteByUCid($id_user,$id_commercant);
		 * $this->mdl_card_fiche_client_capital->deleteByUCid($id_user,$id_commercant);
		 * }
		 *
		 * //saving into card_bonplan_used
		 * //$card_bonplan_used['id_card'] = $id_card;
		 * $card_bonplan_used['id_commercant'] = $id_commercant;
		 * $card_bonplan_used['id_user'] = $id_user;
		 * $card_bonplan_used['id_bonplan'] = $bonplan_id;
		 * $card_bonplan_used['scan_status'] = "OK";
		 * $card_bonplan_used['date_use'] = date("Y-m-d h:m:s");
		 * $card_bonplan_used['description'] = "Ajout remise";
		 * $this->mdl_card_bonplan_used->deleteByUCid($id_user,$id_commercant);
		 * if(!$remise_zero){
		 * $this->mdl_card_bonplan_used->insert($card_bonplan_used);
		 * }else{
		 * $this->assoc_client_bonplan_model->deleteByUBid($id_user,$bonplan_id);
		 * }
		 * redirect($this->agent->referrer());
		 * }
		 * }
		 */
	}

	public function details_fidelity($idUser){
        if ($this->ion_auth->logged_in () && ! $this->ion_auth->in_group ( 2 )) {

            $user_ion_auth_id = $this->ion_auth->user ()->row ();
            $id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
            $data ['titre'] = "Historique consommation";
            $critere = array (
                'card_bonplan_used.id_commercant' => $id_commercant,
                'card_bonplan_used.id_user' => $idUser
            );
            $order_by = array (
                'champ' => 'card_bonplan_used.id',
                'ordre' => 'ASC'
            );
            $data ['list_activity'] = $this->mdl_card_bonplan_used->get_by_critere ( $critere );
            $data ['offre'] = $offre = get_offre_active ( $id_commercant );
            $data ['client'] = $this->user->GetById ( $idUser );
            $data ['fidelity'] = get_fiche_client ( $offre, $idUser, $id_commercant );
            $data ['user_bonplan'] = get_user_bonplan ( $idUser, $id_commercant );
            $data ['idcom'] = $idUser;
            //var_dump($data ['fidelity']);die();
            $this->load->view ( "mobile2014/pro/detail-fidelity", $data );
        } else {
            redirect ( 'auth/login' );
        }
    }

    public function details_bonplan($idUser){

        if ($this->ion_auth->logged_in () && ! $this->ion_auth->in_group ( 2 )) {

            $user_ion_auth_id = $this->ion_auth->user ()->row ();
            $id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ($user_ion_auth_id->id);
            $data ['titre'] = "Historique consommation";
            $critere = array (
                'card_bonplan_used.id_commercant' => $id_commercant,
                'card_bonplan_used.id_user' => $idUser
            );
            $data ['list_activity'] = $this->mdl_card_bonplan_used->get_by_critere( $critere );
            $data ['offre'] = $offre = get_offre_active ( $id_commercant );
            $data ['client'] = $this->user->GetById ( $idUser );
            $data ['fidelity'] = get_fiche_client ( $offre, $idUser, $id_commercant );
            $critere['id_user']=$idUser;
            $critere['id_commercant']=$id_commercant;
            $criteres['valide']='1';
            $criteres['id_client']=$idUser;
            $criteres['id_commercant']=$id_commercant;
            $data ['user_bonplan'] = $this->assoc_client_bonplan_model->get_list_by_criteria($criteres,$order_by=array());
            //var_dump($data ['user_bonplan']);die();
            $data ['idcom'] = $idUser;
            //var_dump($data ['user_bonplan']);die();
            $this->load->view ( "mobile2014/pro/detail-bonplan", $data );
        } else {
            redirect ( 'auth/login' );
        }
    }
    public function reinitialise_bonplan(){
        $user_ion_auth_id = $this->ion_auth->user ()->row ();
        $id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
        $id_user = $this->input->post ( 'id_user' );
                $this->mdl_card_bonplan_used->deleteByUCid ( $id_user, $id_commercant );

        redirect ('front/fidelity_pro/details_bonplan/'.$id_user);
    }
    public function valid_plat($id,$idcom,$idclient){
	    $this->assoc_client_plat_model->valid_res_plat($id);
	    $data=array("id_client"=>$idclient,
                    "id_commercant"=>$idcom);
	    if ($this->assoc_client_commercant_model->get_by_id($idclient,$idcom)){
            redirect('front/fidelity_pro/liste_reservation');
        }else{
            $this->assoc_client_commercant_model->insert($data);
            redirect('front/fidelity_pro/liste_reservation');
        }
    }
    public function details_plat($id_commercant){
        if ($this->ion_auth->logged_in ()) {
        //var_dump($id_commercant);
        $list_conso=$this->assoc_client_plat_model->get_valided_by_idcom($id_commercant);
        $data ['titre'] = "Historique consommation plat";
        $data['conso']=$list_conso;
        $this->load->view ( "mobile2014/pro/info-consoPlat", $data );
    }else{
        redirect('auth/login');
        }
    }
    public function delete_res($idres){
        if ($this->ion_auth->logged_in () && ! $this->ion_auth->in_group ( 2 )) {

            $user_ion_auth_id = $this->ion_auth->user ()->row ();
            $id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
        $this->assoc_client_plat_model->delete_res_plat($idres);
        redirect('front/fidelity_pro/details_plat/'.$id_commercant);
    }
    }
    public function valid_table($idres){
	    $this->assoc_client_table_model->validate_table($idres);
	    redirect('front/fidelity_pro/liste_reservation');
    }
    public function valid_sejour($idres){
        $this->assoc_client_sejour_model->validate_sejour($idres);
        redirect('front/fidelity_pro/liste_reservation');
    }
    public function delete_remise_photo(){
    	// $user_ion_auth = $this->ion_auth->user()->row();
     //    $user_ion_auth_id = $user_ion_auth->id;
        // $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        // $myFile = "testFolder/sampleDeleteFile.txt";
        // unlink($myFile);

        $user_ion_auth = $this->ion_auth->user()->row();
        $user_ion_auth_id = $user_ion_auth->id;
        
    	$idcom = $this->input->post("idcommercant");
    	if(isset($idcom)){
    		$data = $this->mdl_card_remise->getByIdCommercant($idcom);
	        $myFile = "application/resources/front/photoCommercant/imagesbank/".$user_ion_auth_id."/remise_photo/".$data->image1;
	        unlink($myFile);
            $this->load->model("mdl_card_remise");
           	$this->mdl_card_remise->delete_manager_update_remise($idcom);
            echo 'ok';
        }else{
            echo "error1";
        } 
    }
    public function delete_capitalisation_photo(){  
    	$user_ion_auth = $this->ion_auth->user()->row();
        $user_ion_auth_id = $user_ion_auth->id;      
    	$idcom = $this->input->post("idcommercant");
    	if(isset($idcom)){
    		$data = $this->mdl_card_capital->getByIdCommercant($idcom);    		
	        $myFile = "application/resources/front/photoCommercant/imagesbank/".$user_ion_auth_id."/capitalisation_photo/".$data->image1;
	        unlink($myFile);
            $this->load->model("mdl_card_capital");
           	$this->mdl_card_capital->delete_manager_update_capitalisation($idcom);
            echo 'ok';
        }else{
            echo "error1";
        } 
    }
    public function delete_tampon_photo(){  
    	$user_ion_auth = $this->ion_auth->user()->row();
        $user_ion_auth_id = $user_ion_auth->id;      
    	$idcom = $this->input->post("idcommercant");
    	if(isset($idcom)){
    		$data = $this->mdl_card_tampon->getByIdCommercant($idcom); 	
	        $myFile = "application/resources/front/photoCommercant/imagesbank/".$user_ion_auth_id."/fidelity_photo/".$data->image1;
	        unlink($myFile);
            $this->load->model("mdl_card_tampon");
           	$this->mdl_card_tampon->delete_manager_update_capitalisation($idcom);
            echo 'ok';
        }else{
            echo "error1";
        } 
    }
    public function prospect(){

        if ($this->ion_auth->logged_in () && ! $this->ion_auth->in_group ( 2 )) {
            $user_ion_auth_id = $this->ion_auth->user ()->row ();
            $id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
            $oInfoCommercant = $this->mdlcommercant->infoCommercant($id_commercant) ;

            $data ['titre'] = 'Liste de mes clients';
            $data ['print'] = 0;
            $data ['email_clients'] = array ();
            $data ['mail_to_clients'] = '';
            $data ['show_bonplan'] = 1;
            $data ['show_offre'] = 1;
            $data ['oInfoCommercant'] = $oInfoCommercant;
            $prospect = $this->Mdl_prospect->getByIdcom_prospect($id_commercant);
            $data['prospect'] = $prospect;
            $this->load->view('mobile2014/pro/prospect_index', $data);
        }

    }
    public function upload_excel_prospect(){

        if ($this->ion_auth->logged_in ()) {
            $filename = $this->getRandomWord(10);
            $user_ion_auth_id = $this->ion_auth->user ()->row ();
            $id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
            $this->load->library('SimpleXLSX');
            $path = './application/resources/front/prospect/';
            $path2 = './application/resources/front/prospect/'.$user_ion_auth_id->id."/";
            if (!is_dir($path)){
                mkdir($path,0700);
                if (!is_dir($path2)){
                    mkdir($path2,0700);
                }
            }
            if (!is_dir($path2)){
                mkdir($path2,0700);
            }
            if (is_dir($path2)){
                $config['upload_path']          = $path2;
                $config['allowed_types']        = 'xlsx|xls';
                $config['file_name']        = $filename;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('prospect_file'))
                {
                    var_dump('ato');
                    echo $this->upload->display_errors();

                }
                else
                {
                    if($xlsx = SimpleXLSX::parse("./application/resources/front/prospect/".$user_ion_auth_id->id."/".$filename.$this->upload->data()['file_ext'])){
                        $allparsed = $xlsx->rows();
                        $row =0;
                        //var_dump($allparsed);die('en reste ici');
                        foreach ($allparsed as $path) {
                            if ($row > 0){
                            $fields = array("Nom" =>$path[0],
                                "Prenom"=>$path[1],
                                "Telephone" => $path[2],
                                "Email" => $path[3]
                            );
                            $fields["IdComprospect"] = $id_commercant;
                            $this->Mdl_prospect->save_all($fields);
                            }
                            $row++;
                        }
                        redirect('front/fidelity_pro/prospect');
                    }else{
                        echo "failed to parse";
                    }
                }
            }else{
                echo "dir_not";
            }
        }
    }
    function getRandomWord($len = 10) {
        $word = array_merge(range('a', 'z'), range('A', 'Z'));
        shuffle($word);
        return substr(implode($word), 0, $len);
    }
    function send_mail_clien_prospect()
    {

        $destinataire_mail = $this->input->post("destinataire_mail");
        $mail_to_contacter = $this->input->post("mail_to_contacter");
        $object_mail = $this->input->post("object_mail");
        $message_mail = $this->input->post("message_mail");
        $name_contacter = $this->input->post("name_contacter");
        $tel_contacter = $this->input->post("tel_contacter");
        $mail_contacter = $this->input->post("mail_contacter");


        $contact_partner_mailSubject = $object_mail;

        $message_html = '
        <p>Bonjour,<br/>
        Ceci est un mail de votre commercant de Sortez.org<br/>
        ci-dessous le contenu,</p>
        Nom : ' . $name_contacter . '<br/>
        T&eacute;l&eacute;phone : ' . $tel_contacter . '<br/>
        Email : ' . $mail_contacter . '<br/>
        Message : <br/>' . $message_mail . '
        ';

        $sending_mail_privicarte = false;

        $message_html = html_entity_decode(htmlentities($message_html));

        $zContactEmailCom_ = array();
        $mail_client = explode(",", $destinataire_mail);
        //var_dump(count($pieces));die();
        for($i = 0; $i < count($mail_client); $i++){
            $mail_name_client = explode("=", $mail_client[$i]);
            $zContactEmailCom_[] = array("Email" => $mail_name_client[0], "Name" => $mail_name_client[1] );
        }


        $sending_mail_privicarte = @envoi_notification_prospect($zContactEmailCom_, $contact_partner_mailSubject, $message_html, $mail_contacter);

        $zContactEmailCom_copy = array();
        $zContactEmailCom_copy[] = array("Email" => $mail_contacter, "Name" => $name_contacter);
        $message_html_copy = '
        <p>Bonjour,<br/>
        Ceci est une copie de votre mail Sortez.org<br/>
        ci-dessous le contenu,</p>
        Email : ' . $mail_to_contacter . '<br/>
        Message : <br/>' . $message_mail . '
        ';
        $message_html_copy = html_entity_decode(htmlentities($message_html_copy));
        $sending_mail_privicarte_copy = @envoi_notification($zContactEmailCom_copy, $contact_partner_mailSubject, $message_html_copy);


        if ($sending_mail_privicarte) echo '<span style="color:#006600;">Votre message à bien été envoyé !</span>';
        else echo '<span style="color:#FF0000;">Une erreur est survenue, veuillez réessayer dans un instant !</span>';
    }
    function suppression_tout(){
           $idcom = $this->input->post('IdCommercant');
        $resultat = $this->Mdl_prospect->delete_by_idcom($idcom);
        if($resultat == true){
            echo 'ok';
        }else{
            echo 'ko';
        }
    }
    function card_scan_command(){
        $data ['pagetitle'] = "Scanner une carte";

        $is_mobile = TRUE;
        // test ipad user agent
        $is_mobile_ipad = FALSE;
        $data ['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = TRUE;
        $is_browser = $this->agent->is_browser ();
        $is_platform = $this->agent->platform ();
        $data ['is_mobile'] = $is_mobile;
        $data ['is_robot'] = $is_robot;
        $data ['is_browser'] = $is_browser;
        $data ['is_platform'] = $is_platform;

        $data ['titre'] = 'Reconnaissance du porteur de la carte';
        $this->load->view ( "mobile2014/pro/reconnaissance-carte-commande", $data );
    }
    public function delete_command($id,$id_user){
	    $this->Mdl_soutenons->delete_commande($id);
	    redirect('front/Fidelity_pro/validation_operation_command/'.$id_user);
    }
    public function detail_command($id,$IdUser){
        $user_ion_auth_id = $this->ion_auth->user ()->row ();
        $id_ionauth = $user_ion_auth_id->id;
        $id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );

        $criteres ['id_commercant'] = $id_commercant;
        $criteres ['id_user'] = $IdUser;
        $assoc_client_array = $this->Mdl_soutenons->get_command_list($id);
        $command_detail = $this->Mdl_soutenons->get_command_details($id);
        $assoc_client = ! empty ( $assoc_client_array ) ? current ( $assoc_client_array ) : array ();
//		var_dump($command_detail);
        $data ['commandshow'] = ! empty ( $assoc_client_array ) ? 1 : 0;
        $data['details'] = $command_detail;
        $data ['offre_active'] = 'Commande';
        $data['commande'] = $assoc_client_array;
        $client = $this->user->GetById( $IdUser );
        $data ['titre'] = 'Client: ' . ucfirst ( $client->Nom ) . ' ' . ucfirst ( $client->Prenom );
        $data ['IdUser'] = $IdUser;
        $data ['id_client_command'] = is_object ( $assoc_client ) ? $assoc_client->id : 0;
        $this->load->view ( "mobile2014/pro/detail_command", $data );
    }
    public function all_command(){
        $user_ion_auth_id = $this->ion_auth->user ()->row ();
        $id_ionauth = $user_ion_auth_id->id;
        $id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
        $etat_commande = $this->input->post('etat_commande');
        if (isset($etat_commande) && $etat_commande !=null && $etat_commande != "Tout"){
            $filter = $etat_commande;
        }else{
            $filter = "";
        }
        $data['filter'] = $filter;
        $criteres ['id_commercant'] = $id_commercant;
        $command = $this->Mdl_soutenons->get_all_commmand_by_idcom($id_commercant,$filter);
        $data['commande'] = $command;


        $this->load->view ( "mobile2014/pro/list_command_global", $data );
    }
    public function detail_command_global($id){
        $user_ion_auth_id = $this->ion_auth->user ()->row ();
        $id_ionauth = $user_ion_auth_id->id;
        $id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
        // $id = $this->uri->rsegment(3);
        $criteres ['id_commercant'] = $id_commercant;
        $assoc_client_array = $this->Mdl_soutenons->get_command_list($id);
        $command_detail = $this->Mdl_soutenons->get_command_details($id);
        $assoc_client = ! empty ( $assoc_client_array ) ? current ( $assoc_client_array ) : array ();
//		var_dump($command_detail);
        $var=array();
        foreach($assoc_client_array as $item){
        	array_push($var,$item["id_client"]);
        }
        $data ['commandshow'] = ! empty ( $assoc_client_array ) ? 1 : 0;
        $data['details'] = $command_detail;
        $data ['offre_active'] = 'Commande';
        $data['commande'] = $assoc_client_array;
        $data['id_commande'] = $id;
        $user_info = $this->user->GetById($var[0]);
        $data['client'] = $user_info;
        
        
        // var_dump($user_info);
        $this->load->view ( "mobile2014/pro/detail_command_global",$data);
    }
    public function save_etat_commandes(){
	    $etat = $this->input->post('etat_commande');
	    $id_commande = $this->input->post('id_commande');
	    $field = array("etat_commande" =>$etat);
	    $this->Mdl_soutenons->save_etat_commande($id_commande,$field);
	    redirect('front/Fidelity_pro/detail_command_global/'.$id_commande);
    }
}