<?php
class mobile extends CI_Controller{
    
    function __construct() {
        parent::__construct();
		$this->load->model("mdlannonce") ;
		$this->load->model("mdlcommercant") ;
		$this->load->model("mdlbonplan") ;
		$this->load->model("mdlcategorie") ;
		$this->load->model("mdlville") ;
		$this->load->model("mdlcommercantpagination") ;
        $this->load->model("sousRubrique") ;
        $this->load->model("AssCommercantAbonnement") ;
        $this->load->model("AssCommercantSousRubrique") ;
        $this->load->model("Commercant") ;
        $this->load->library('session');
        $this->load->model("user") ;
        $this->load->model("mdl_agenda");
        $this->load->model("mdlarticle");
        $this->load->model("mdl_categories_article");

        $this->load->library('user_agent');

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");


        check_vivresaville_id_ville();
    }
    
    
    function index(){
        redirect("/");
    }
    
    
    function menu() {
        
        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;
        
        
        $this->load->view('front2013/home_menu_mobile', $data) ;
    }
    
    function identification() {
        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;
        
        
        $this->load->view('front2013/mobile_identification', $data) ;
    }
    
    function conditiongenerales() {
        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;
        
        if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) { 
            $this->load->view('front2013/mobile_conditiongenerales', $data) ;
        } else {
            redirect("/");
        }
        
        
    }
    
    
    function recherchepartenaires() {
        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;
        
        $toCategorie= $this->mdlcategorie->GetCommercantSouscategorie();
        ////$this->firephp->log($toCategorie, 'toCategorie');
        $toVille= $this->mdlville->GetCommercantVilles();//get ville list of commercant
        ////$this->firephp->log($toVille, 'toVille');
        $data['toVille'] = $toVille ;
        $data['toCategorie'] = $toCategorie ;
        $toCategorie_principale= $this->mdlcategorie->GetCommercantCategorie_x();
        ////$this->firephp->log($toCategorie_principale, 'toCategorie_principale');
        $data['toCategorie_principale'] = $toCategorie_principale ;
        
        
        if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) { 
                $this->load->view('mobile2013/mobile_recherchepartenaires_partenaires', $data) ;
        } else {
            redirect("/");
        }
        
        
    }

    function getsouscategorie_to_mobile() {
        $inputStringHidden_main_form_mobile = $this->input->post("inputStringHidden_main_form_mobile");
        $inputStringVilleHidden_form_mobile = $this->input->post("inputStringVilleHidden_form_mobile");

        $toSousCategorie= $this->mdlcategorie->GetCommercantSouscategorieByRubriqueVille_x($inputStringHidden_main_form_mobile,$inputStringVilleHidden_form_mobile);

        $result_to_show = '';
        if (isset($toSousCategorie)) {
            foreach ($toSousCategorie as $objCategorie) {
                $result_to_show .= $objCategorie->IdSousRubrique.',';
            }
        }

        //delete the last caracter of string => ','
        $result_to_show = substr($result_to_show,0,-1);

        echo $result_to_show;
    }

    function getsouscategorie_agenda_to_mobile() {
        $inputStringHidden_main_form_mobile = $this->input->post("inputStringHidden_main_form_mobile");
        $inputStringVilleHidden_form_mobile = $this->input->post("inputStringVilleHidden_form_mobile");

        $toSousCategorie= $this->mdl_agenda->GetAgendaSouscategorieByRubriqueVille_x($inputStringHidden_main_form_mobile,$inputStringVilleHidden_form_mobile);

        $result_to_show = '';
        if (isset($toSousCategorie)) {
            foreach ($toSousCategorie as $objCategorie) {
                $result_to_show .= $objCategorie->agenda_subcategid.',';
            }
        }

        //delete the last caracter of string => ','
        $result_to_show = substr($result_to_show,0,-1);

        echo $result_to_show;
    }
    
    function getsouscategorie(){
        $inputStringHidden_main_form_mobile = $this->input->post("inputStringHidden_main_form_mobile");
        $inputStringVilleHidden_form_mobile = $this->input->post("inputStringVilleHidden_form_mobile");
        
        $toSousCategorie= $this->mdlcategorie->GetCommercantSouscategorieByRubriqueVille_x($inputStringHidden_main_form_mobile,$inputStringVilleHidden_form_mobile);
        
        $result_to_show = '<select type="inputStringHidden_sous" name="inputStringHidden_sous" id="inputStringHidden_form_mobile" style="width:250px">
            <option value="0">Toutes les sous-rubriques</option>';
        if (isset($toSousCategorie)) {
            foreach ($toSousCategorie as $objCategorie) {
              $result_to_show .= '<option value="'.$objCategorie->IdSousRubrique.'">'.$objCategorie->Nom .' ('.$objCategorie->nbCommercant .')</option>';
           }
        }
        $result_to_show .= '</select>';
        
        echo $result_to_show;
        
    }


    function getcategorie(){
        $inputStringVilleHidden_form_mobile = $this->input->post("inputStringVilleHidden_form_mobile");

        $toCategorie= $this->mdlcategorie->GetCommercantCategoriebyVille_x($inputStringVilleHidden_form_mobile);

        $result_to_show = '<select type="inputStringHidden_main" name="inputStringHidden_main" id="inputStringHidden_main_form_mobile" style="width:250px" onChange="javascript:change_categ_getsouscateg();">
            <option value="0">Toutes les rubriques</option>';
        if (isset($toCategorie)) {
            foreach ($toCategorie as $objCategorie) {
                $result_to_show .= '<option value="'.$objCategorie->IdRubrique.'">'.$objCategorie->Nom .' ('.$objCategorie->nbCommercant .')</option>';
            }
        }
        $result_to_show .= '</select>';

        echo $result_to_show;

    }

    function getsouscategorieannonces_to_mobile(){
        $inputStringHidden_main_form_mobile = $this->input->post("inputStringHidden_main_form_mobile");

        $toSousCategorie= $this->mdlannonce->GetAnnonceSouscategorieByRubrique($inputStringHidden_main_form_mobile);

        $result_to_show = '';
        if (isset($toSousCategorie)) {
            foreach ($toSousCategorie as $objCategorie) {
                $result_to_show .= $objCategorie->IdSousRubrique.',';
            }

            //delete the last caracter of string => ','
            $result_to_show = substr($result_to_show,0,-1);
        }

        echo $result_to_show;
    }
    
    function getsouscategorieannonces(){
        $inputStringHidden_main_form_mobile = $this->input->post("inputStringHidden_main_form_mobile");
        
        $toSousCategorie= $this->mdlannonce->GetAnnonceSouscategorieByRubrique($inputStringHidden_main_form_mobile);
        
        $result_to_show = '<select type="inputStringHidden_sous" name="inputStringHidden_sous" id="inputStringHidden_form_mobile" style="width:250px">
            <option value="0">Toutes les sous-rubriques</option>';
        if (isset($toSousCategorie)) {
            foreach ($toSousCategorie as $objCategorie) {
               $result_to_show .= '<option value="'.$objCategorie->IdSousRubrique.'">'.$objCategorie->Nom .' ('.$objCategorie->nb_annonce .')</option>';
            }
        }
        $result_to_show .= '</select>';
        
        echo $result_to_show;
        
    }

    function getcategorieannonces(){
        $inputStringVilleHidden_annonces = $this->input->post("inputStringVilleHidden_annonces");

        $toSousCategorie= $this->mdlannonce->GetAnnonceCategoriePrincipaleByVille($inputStringVilleHidden_annonces);

        $result_to_show = '<select type="inputStringHidden_main" name="inputStringHidden_main" id="inputStringHidden_main_form_mobile" style="width:250px" onChange="javascript:change_categ_getsouscateg_annonces();">
            <option value="0">Toutes les rubriques</option>';
        if (isset($toSousCategorie)) {
            foreach ($toSousCategorie as $objCategorie) {
                $result_to_show .= '<option value="'.$objCategorie->IdRubrique.'">'.$objCategorie->Nom .' ('.$objCategorie->nb_annonce .')</option>';
            }
        }
        $result_to_show .= '</select>';

        echo $result_to_show;

    }

    function getsouscategoriebonplans_to_mobile(){
        $inputStringHidden_main_form_mobile = $this->input->post("inputStringHidden_main_form_mobile");

        $toSousCategorie= $this->mdlcategorie->GetBonplanSouscategorieByRubrique($inputStringHidden_main_form_mobile);

        $result_to_show = '';
        if (isset($toSousCategorie)) {
            foreach ($toSousCategorie as $objCategorie) {
                $result_to_show .= $objCategorie->IdSousRubrique.',';
            }

            //delete the last caracter of string => ','
            $result_to_show = substr($result_to_show,0,-1);
        }


        echo $result_to_show;
    }
    
    function getsouscategoriebonplans(){
        $inputStringHidden_main_form_mobile = $this->input->post("inputStringHidden_main_form_mobile");
        
        $toSousCategorie= $this->mdlcategorie->GetBonplanSouscategorieByRubrique($inputStringHidden_main_form_mobile);
        
        $result_to_show = '<select type="inputStringHidden_sous" name="inputStringHidden_sous" id="inputStringHidden_form_mobile" style="width:250px">
            <option value="0">Toutes les sous-rubriques</option>';
        if (isset($toSousCategorie)) {
            foreach ($toSousCategorie as $objCategorie) {
               $result_to_show .= '<option value="'.$objCategorie->IdSousRubrique.'">'.$objCategorie->Nom .' ('.$objCategorie->nb_bonplan .')</option>';
            }
        }
        $result_to_show .= '</select>';
        
        echo $result_to_show;
        
    }


    function getsouscategorieagenda(){
        $inputStringHidden_main_form_mobile = $this->input->post("inputStringHidden_main_form_mobile");
        $inputStringVilleHidden_agenda = $this->input->post("inputStringVilleHidden_agenda");

        $toCategorie_sub= $this->mdl_agenda->GetAgendaSubCateg_by_IdCateg_IdVille($inputStringHidden_main_form_mobile, $inputStringVilleHidden_agenda);
        $data['toCategorie_sub'] = $toCategorie_sub ;

        ?>

        <select type="inputStringHidden_sous" name="inputStringHidden_sous" id="inputStringHidden_form_mobile" style="width:250px">
            <option value="0">Toutes les sous-rubriques</option>
            <?php  foreach($toCategorie_sub as $oCategorie){ ?>
                <option value="<?php  echo $oCategorie->agenda_subcategid ; ?>"><?php  echo $oCategorie->subcateg ; ?>&nbsp;(<?php  echo $oCategorie->nb_agenda ; ?>)</option>
            <?php  } ?>
        </select>

        <?php

    }


    function getsouscategoriearticle(){
        $inputStringHidden_main_form_mobile = $this->input->post("inputStringHidden_main_form_mobile");
        $inputStringVilleHidden_agenda = $this->input->post("inputStringVilleHidden_agenda");

        $toCategorie_sub= $this->mdlarticle->GetArticleSubCateg_by_IdCateg_IdVille($inputStringHidden_main_form_mobile, $inputStringVilleHidden_agenda);
        $data['toCategorie_sub'] = $toCategorie_sub ;

        ?>

        <select type="inputStringHidden_sous" name="inputStringHidden_sous" id="inputStringHidden_form_mobile" style="width:250px">
            <option value="0">Toutes les sous-rubriques</option>
            <?php  foreach($toCategorie_sub as $oCategorie){ ?>
                <option value="<?php  echo $oCategorie->agenda_subcategid ; ?>"><?php  echo $oCategorie->subcateg ; ?>&nbsp;(<?php  echo $oCategorie->nb_article ; ?>)</option>
            <?php  } ?>
        </select>

        <?php

    }


    function getcategoriebonplans(){
        $inputStringVilleHidden_bonplans = $this->input->post("inputStringVilleHidden_bonplans");

        $toSousCategorie= $this->mdlcategorie->GetBonplanCategoriePrincipaleByVille($inputStringVilleHidden_bonplans);

        $result_to_show = '<select type="inputStringHidden_main" name="inputStringHidden_main" id="inputStringHidden_main_form_mobile" style="width:250px" onChange="javascript:change_categ_getsouscateg_bonplans();">
            <option value="0">Toutes les sous-rubriques</option>';
        if (isset($toSousCategorie)) {
            foreach ($toSousCategorie as $objCategorie) {
                $result_to_show .= '<option value="'.$objCategorie->IdRubrique.'">'.$objCategorie->Nom .' ('.$objCategorie->nb_bonplan .')</option>';
            }
        }
        $result_to_show .= '</select>';

        echo $result_to_show;

    }
    
    function menugenerale(){
        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;
        
        if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) { 
            $this->load->view('front2013/mobile_menugenerale', $data) ;
        } else {
            redirect("/");
        }
        
    }
    
    function mon_compte(){
        
        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;
        
        if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) { 
            
            if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)) {
                $this->load->view('front2013/mobile_moncompte', $data) ;
            } else if($this->ion_auth->logged_in()) {
                redirect("front/utilisateur/contenupro");
            } else if (!$this->ion_auth->logged_in()){
                $this->session->set_userdata("cur_uri", $this->uri->uri_string());
                $this->session->set_flashdata('domain_from', '1');
                redirect("connexion");
            }
            
        } else {
            redirect("/");
        }
        
        
    }
    
    
    function fonctionnement_club(){
        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;
        
        if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) { 
                $this->load->view('front2013/mobile_fonctionnement_club', $data) ;
        } else {
            redirect("/");
        }
    }
    
    function mention_legale(){
        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;
        
        if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) { 
            $this->load->view('front2013/mobile_mention_legale', $data) ;
        } else {
            redirect("/");
        }
    }
    
    
    function infospro(){
        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;
        
        if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) { 
            $this->load->view('front2013/mobile_infospro', $data) ;
        } else {
            redirect("/");
        }
    }
    
    
    
    function rechercheannonces() {
        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;
        
        $toSousRubrique= $this->mdlannonce->GetAnnonceSouscategorie();
        $data['toSousRubrique'] = $toSousRubrique ;
        $toRubriquePrincipale= $this->mdlannonce->GetAnnonceCategoriePrincipale();
        $data['toRubriquePrincipale'] = $toRubriquePrincipale ;
        $toCommercant= $this->mdlcommercant->GetAllCommercant_with_annonce();
        $data['toCommercant'] = $toCommercant ;

        $this->load->model("mdlville") ;
        $toVille= $this->mdlville->GetAnnonceVilles();
        $data['toVille'] = $toVille ;
        
        
        if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) { 
                $this->load->view('mobile2013/mobile_rechercheannonces_partenaires', $data) ;
        } else {
            redirect("/");
        }
        
        
    }


    function rechercheagenda() {
        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;


        $toRubriquePrincipale= $this->mdl_agenda->GetAgendaCategorie();
        $data['toRubriquePrincipale'] = $toRubriquePrincipale ;
        $toCategorie= $this->mdl_agenda->GetAgendaSubCateg_by_IdCateg("0");
        $data['toCategorie_sub'] = $toCategorie ;

        $toVille= $this->mdlville->GetAgendaVilles();//get ville list of agenda
        $data['toVille'] = $toVille ;

        //$toCommercant= $this->mdlcommercant->GetAllCommercant_with_bonplan();
        //$data['toCommercant'] = $toCommercant ;

        /*$session_iCategorieId = $this->session->userdata('iCategorieId_x');
        $session_iSousCategorieId = $this->session->userdata('iSousCategorieId_x');
        $session_iVilleId = $this->session->userdata('iVilleId_x');
        $session_zMotCle = $this->session->userdata('zMotCle_x');
        $session_iOrderBy = $this->session->userdata('iOrderBy_x');*/

        $session_iCategorieId = $this->session->userdata('iCategorieId_x');
        $session_iSousCategorieId = $this->session->userdata('iSousCategorieId_x');
        $session_iVilleId = $this->session->userdata('iVilleId_x');
        $session_iDepartementId = $this->session->userdata('iDepartementId');
        $session_zMotCle = $this->session->userdata('zMotCle_x');
        $session_iOrderBy = $this->session->userdata('iOrderBy_x');
        $session_inputStringQuandHidden = $this->session->userdata('inputStringQuandHidden_x');
        $session_inputStringDatedebutHidden = $this->session->userdata('inputStringDatedebutHidden_x');
        $session_inputStringDatefinHidden = $this->session->userdata('inputStringDatefinHidden_x');
        $session_inputIdCommercant = $this->session->userdata('inputIdCommercant_x');

        if (!isset($session_iCategorieId)) $session_iCategorieId_to_count = 0; else $session_iCategorieId_to_count=$session_iCategorieId;
        if (!isset($session_iVilleId)) $session_iVilleId_to_count = 0; else $session_iVilleId_to_count=$session_iVilleId;
        if (!isset($session_iDepartementId)) $session_iDepartementId_to_count = 0; else $session_iDepartementId_to_count=$session_iDepartementId;
        if (!isset($session_zMotCle)) $session_zMotCle_to_count = ""; else $session_zMotCle_to_count=$session_zMotCle;
        if (!isset($session_iSousCategorieId)) $session_iSousCategorieId_to_count = 0; else $session_iSousCategorieId_to_count=$session_iSousCategorieId;
        if (!isset($session_iOrderBy)) $session_iOrderBy_to_count = ""; else $session_iOrderBy_to_count=$session_iOrderBy;
        $session_inputStringDatedebutHidden_to_count = "0000-00-00";
        $session_inputStringDatefinHidden_to_count = "0000-00-00";
        $session_inputIdCommercant_to_count = "0";

        //die("stop");

        $data['toAgndaTout_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "0", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toAgndaAujourdhui_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "101", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toAgndaWeekend_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "202", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toAgndaSemaine_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "303", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toAgndaSemproch_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "404", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toAgndaMois_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "505", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));

        $data['toAgndaJanvier_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "01", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toAgndaFevrier_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "02", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toAgndaMars_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "03", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toAgndaAvril_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "04", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toAgndaMai_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "05", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toAgndaJuin_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "06", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toAgndaJuillet_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "07", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toAgndaAout_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "08", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toAgndaSept_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "09", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toAgndaOct_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "10", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toAgndaNov_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "11", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toAgndaDec_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "12", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));



        if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
            $this->load->view('mobile2013/mobile_rechercheagenda_partenaires', $data) ;
        } else {
            redirect("/");
        }


    }
	
	
	function recherchearticle() {
        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;


        $toRubriquePrincipale= $this->mdlarticle->GetArticleCategorie();
        $data['toRubriquePrincipale'] = $toRubriquePrincipale ;
        $toCategorie= $this->mdlarticle->GetArticleSubCateg_by_IdCateg("0");
        $data['toCategorie_sub'] = $toCategorie;

        $toVille= $this->mdlville->GetAgendaVilles();//get ville list of agenda
        $data['toVille'] = $toVille ;


        //$toCommercant= $this->mdlcommercant->GetAllCommercant_with_bonplan();
        //$data['toCommercant'] = $toCommercant ;

        /*$session_iCategorieId = $this->session->userdata('iCategorieId_x');
        $session_iSousCategorieId = $this->session->userdata('iSousCategorieId_x');
        $session_iVilleId = $this->session->userdata('iVilleId_x');
        $session_zMotCle = $this->session->userdata('zMotCle_x');
        $session_iOrderBy = $this->session->userdata('iOrderBy_x');*/

        $session_iCategorieId = $this->session->userdata('iCategorieId_x');
        $session_iSousCategorieId = $this->session->userdata('iSousCategorieId_x');
        $session_iVilleId = $this->session->userdata('iVilleId_x');
        $session_iDepartementId = $this->session->userdata('iDepartementId');
        $session_zMotCle = $this->session->userdata('zMotCle_x');
        $session_iOrderBy = $this->session->userdata('iOrderBy_x');
        $session_inputStringQuandHidden = $this->session->userdata('inputStringQuandHidden_x');
        $session_inputStringDatedebutHidden = $this->session->userdata('inputStringDatedebutHidden_x');
        $session_inputStringDatefinHidden = $this->session->userdata('inputStringDatefinHidden_x');
        $session_inputIdCommercant = $this->session->userdata('inputIdCommercant_x');

        if (!isset($session_iCategorieId)) $session_iCategorieId_to_count = 0; else $session_iCategorieId_to_count=$session_iCategorieId;
        if (!isset($session_iVilleId)) $session_iVilleId_to_count = 0; else $session_iVilleId_to_count=$session_iVilleId;
        if (!isset($session_iDepartementId)) $session_iDepartementId_to_count = 0; else $session_iDepartementId_to_count=$session_iDepartementId;
        if (!isset($session_zMotCle)) $session_zMotCle_to_count = ""; else $session_zMotCle_to_count=$session_zMotCle;
        if (!isset($session_iSousCategorieId)) $session_iSousCategorieId_to_count = 0; else $session_iSousCategorieId_to_count=$session_iSousCategorieId;
        if (!isset($session_iOrderBy)) $session_iOrderBy_to_count = ""; else $session_iOrderBy_to_count=$session_iOrderBy;
        $session_inputStringDatedebutHidden_to_count = "0000-00-00";
        $session_inputStringDatefinHidden_to_count = "0000-00-00";
        $session_inputIdCommercant_to_count = "0";

        //die("stop");

        $data['toArticleTout_global'] = count($this->mdl_agenda->listeAgendaRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "0", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toArticleAujourdhui_global'] = count($this->mdl_agenda->listeAgendaRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "101", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toArticleWeekend_global'] = count($this->mdl_agenda->listeAgendaRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "202", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toArticleSemaine_global'] = count($this->mdl_agenda->listeAgendaRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "303", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toArticleSemproch_global'] = count($this->mdl_agenda->listeAgendaRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "404", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toArticleMois_global'] = count($this->mdl_agenda->listeAgendaRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "505", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));

        $data['toArticleJanvier_global'] = count($this->mdl_agenda->listeAgendaRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "01", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toArticleFevrier_global'] = count($this->mdl_agenda->listeAgendaRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "02", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toArticleMars_global'] = count($this->mdl_agenda->listeAgendaRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "03", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toArticleAvril_global'] = count($this->mdl_agenda->listeAgendaRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "04", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toArticleMai_global'] = count($this->mdl_agenda->listeAgendaRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "05", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toArticleJuin_global'] = count($this->mdl_agenda->listeAgendaRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "06", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toArticleJuillet_global'] = count($this->mdl_agenda->listeAgendaRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "07", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toArticleAout_global'] = count($this->mdl_agenda->listeAgendaRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "08", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toArticleSept_global'] = count($this->mdl_agenda->listeAgendaRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "09", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toArticleOct_global'] = count($this->mdl_agenda->listeAgendaRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "10", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toArticleNov_global'] = count($this->mdl_agenda->listeAgendaRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "11", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));
        $data['toArticleDec_global'] = count($this->mdl_agenda->listeAgendaRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "12", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count, "0"));



        if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
            $this->load->view('mobile2013/mobile_recherchearticle_partenaires', $data) ;
        } else {
            redirect("/");
        }


    }


    function getcategorieagenda(){
        $session_iVilleId = $this->input->post("inputStringVilleHidden_agenda");


        if (!isset($session_iVilleId)) $session_iVilleId_to_count = 0; else $session_iVilleId_to_count=$session_iVilleId;


        $toRubriquePrincipale= $this->mdl_agenda->GetAgendaCategorie_by_params("0","0","0",$session_iVilleId_to_count);
        $data['toRubriquePrincipale'] = $toRubriquePrincipale;

        ?>


        <select type="inputStringHidden_main" name="inputStringHidden_main" id="inputStringHidden_main_form_mobile" style="width:250px" onchange="javascript:change_categ_getsouscateg_agenda();">
            <option value="0">Toutes les rubriques</option>
            <?php  foreach($toRubriquePrincipale as $oCategorie_main){ ?>
                <option value="<?php  echo $oCategorie_main->agenda_categid ; ?>"><?php  echo $oCategorie_main->category ; ?>&nbsp;(<?php  echo $oCategorie_main->nb_agenda ; ?>)</option>
            <?php  } ?>
        </select>

        <?php


    }


    function getcategoriearticle(){
        $session_iVilleId = $this->input->post("inputStringVilleHidden_agenda");


        if (!isset($session_iVilleId)) $session_iVilleId_to_count = 0; else $session_iVilleId_to_count=$session_iVilleId;


        $toRubriquePrincipale= $this->mdl_categories_article->GetArticleCategorie_by_params("0","0","0",$session_iVilleId_to_count);
        $data['toRubriquePrincipale'] = $toRubriquePrincipale;

        ?>


        <select type="inputStringHidden_main" name="inputStringHidden_main" id="inputStringHidden_main_form_mobile" style="width:250px" onchange="javascript:change_categ_getsouscateg_article();">
            <option value="0">Toutes les rubriques</option>
            <?php  foreach($toRubriquePrincipale as $oCategorie_main){ ?>
                <option value="<?php  echo $oCategorie_main->agenda_categid ; ?>"><?php  echo $oCategorie_main->category ; ?>&nbsp;(<?php  echo $oCategorie_main->nb_article; ?>)</option>
            <?php  } ?>
        </select>

        <?php


    }


    function recherchebonplans() {
        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;
        
        $toRubriquePrincipale= $this->mdlcategorie->GetBonplanCategoriePrincipale();
        $data['toRubriquePrincipale'] = $toRubriquePrincipale ;
        $toCategorie= $this->mdlcategorie->GetBonplanSouscategorie();
        $toVille= $this->mdlville->GetBonplanVilles();
        $data['toVille'] = $toVille ;
        $data['toCategorie'] = $toCategorie ;
        $toCommercant= $this->mdlcommercant->GetAllCommercant_with_bonplan();
	    $data['toCommercant'] = $toCommercant ;

        $this->load->model("mdlville") ;
        $toVille= $this->mdlville->GetBonplanVilles();
        $data['toVille'] = $toVille ;
        
        
        if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) { 
                $this->load->view('mobile2013/mobile_recherchebonplans_partenaires', $data) ;
        } else {
            redirect("/");
        }
        
        
    }
    
    
    function restomobile(){
        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;
        
        
        if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) { 
                $this->load->view('front2013/mobile_resto', $data) ;
        } else {
            redirect("/");
        }
    }



    function localisation() {

        if ($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){

            $is_mobile = $this->agent->is_mobile();
            //test ipad user agent
            $is_mobile_ipad = $this->agent->is_mobile('ipad');
            $data['is_mobile_ipad'] = $is_mobile_ipad;
            $is_robot = $this->agent->is_robot();
            $is_browser = $this->agent->is_browser();
            $is_platform = $this->agent->platform();
            $data['is_mobile'] = $is_mobile;
            $data['is_robot'] = $is_robot;
            $data['is_browser'] = $is_browser;
            $data['is_platform'] = $is_platform;

            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if (isset($_POST['geodistance'])) {
                $geodistance_value = $this->input->post('geodistance');
                $this->user->edit_geodistance($iduser,$geodistance_value);
                $data['mssg_return'] = "Configuration enregistrée";
            }

            $data['oUser'] = $this->user->GetById($iduser);

            $data['titre'] = 'Localisation';
            $this->load->view('mobile2014/localisation', $data) ;

        } else if (!$this->ion_auth->logged_in())  {
            $last_url = site_url("front/mobile/localisation");
            $this->session->set_flashdata('last_url', $last_url);
            redirect("auth/login");
        } else {
            redirect("/");
        }
    }


    function sommaire(){

        redirect("http://www.proximite-magazine.com/sommaire-mobile.html");
        /*
        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;


        if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
            $this->load->view('mobile2013/sommaire', $data);
        } else {
            redirect("/");
        }
        */
    }
/*
    function sitesweb(){

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;


        if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
            $this->load->view('mobile2013/sitesweb', $data);
        } else {
            redirect("/");
        }

    }


    function macarte(){

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;


        if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
            $this->load->view('mobile2013/macarte', $data);
        } else {
            redirect("/");
        }

    }

    function infosparticuliers(){

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;


        if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
            $this->load->view('mobile2013/infosparticuliers', $data);
        } else {
            redirect("/");
        }

    }*/
    
    
    
}    