<?php class commercant extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("mdlcommercant");
        $this->load->model("Mdl_news_letter_commercant");
        $this->load->model("mdlville");
        $this->load->model("mdlbonplan");
        $this->load->model("mdlbonplans");
        $this->load->model("mdlcadeau");
        $this->load->model("Mdl_soutenons");

        $this->load->model("mdlannonce");
        $this->load->model("Seo_model");
        $this->load->model("rubrique");
        $this->load->model("mdlimagespub");

        $this->load->model("Abonnement");
        $this->load->library('user_agent');

        $this->load->library('ion_auth');

        $this->load->model("ion_auth_used_by_club");

        $this->load->model("Mdl_plat_du_jour");
        $this->load->model("Mdl_card");
        $this->load->model("User");

        $this->load->library('session');
        $this->load->helper('clubproximite');
        $this->load->model('Mdl_reservation');
        $this->load->model('Seo_model');

        check_vivresaville_id_ville();
    }
    function photoCommercant($_iCommercantId)
    {

        $nom_url_commercant = $this->uri->rsegment(3); //die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);
        $data['oInfoCommercant'] = $oInfoCommercant;

        $data['active_link'] = "activite1";
        $oAssComRub =     $this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom =     $this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);


        //$this->load->view('front/vwPhotoCommercant', $data) ;
        $this->load->view('front2013/photoscommercant', $data);
    }


    function videoCommercant($_iCommercantId)
    {

        $nom_url_commercant = $this->uri->rsegment(3); //die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);
        $data['oInfoCommercant'] = $oInfoCommercant;

        $data['active_link'] = "accueil";
        $oAssComRub =     $this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom =     $this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user");
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser = 0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser, $_iCommercantId));

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId);
        $data['toBonPlan'] = $toBonPlan;

        //$data['active_link2'] = '1';


        //$this->load->view('front/vwVideoCommercant', $data) ;
        $this->load->view('front2013/videocommercant', $data);
    }

    function plusInfoCommercant($_iCommercantId)
    {

        $nom_url_commercant = $this->uri->rsegment(3); //die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);
        $data['oInfoCommercant'] = $oInfoCommercant;

        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;

        $data['active_link'] = "activite1";
        $oAssComRub =     $this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom =     $this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user");
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser = 0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser, $_iCommercantId));

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $data['current_page'] = "page_infos";
        $data['pagecategory'] = "pro";

        $data['pageglissiere'] = "page1";
        $this->load->model("mdlglissiere");
        $data['mdlglissiere'] = $this->mdlglissiere;

        $data['link_partner_current_page'] = 'page1';

        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId);
        $data['toBonPlan'] = $toBonPlan;

        //ajout bouton rond noire annonce/agenda
        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
        else $data['result_check_commercant_annonce'] = '0';

        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
        else $data['result_check_commercant_agenda'] = '0';

        $is_reserved_on = $this->Mdl_plat_du_jour->verify_reserved($_iCommercantId);
        $data['reservation'] = $is_reserved_on;

        //$this->load->view('front/vwPlusInfoCommercant', $data) ;
        //$this->load->view('front2013/plusinfoscommercant', $data) ;
        $this->load->view('privicarte/plusinfoscommercant', $data);
    }

    function plusInfo2Commercant($_iCommercantId)
    {

        $nom_url_commercant = $this->uri->rsegment(3); //die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);
        $data['oInfoCommercant'] = $oInfoCommercant;

        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;

        $data['active_link'] = "activite2";
        $oAssComRub =     $this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom =     $this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;


        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user");
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser = 0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser, $_iCommercantId));

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $data['current_page'] = "page_infos2";

        $data['pageglissiere'] = "page2";
        $this->load->model("mdlglissiere");
        $data['mdlglissiere'] = $this->mdlglissiere;

        $data['link_partner_current_page'] = 'page2';

        $data['pagecategory'] = "pro";

        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId);
        $data['toBonPlan'] = $toBonPlan;

        //ajout bouton rond noire annonce/agenda
        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
        else $data['result_check_commercant_annonce'] = '0';

        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
        else $data['result_check_commercant_agenda'] = '0';

        $is_reserved_on = $this->Mdl_plat_du_jour->verify_reserved($_iCommercantId);
        $data['reservation'] = $is_reserved_on;

        //$this->load->view('front/vwPlusInfo2Commercant', $data) ;
        //$this->load->view('front2013/plusinfos2commercant', $data) ;
        $this->load->view('privicarte/plusinfos2commercant', $data);
    }

    function mentionslegalesmobile($_iCommercantId)
    {

        $nom_url_commercant = $this->uri->rsegment(3); //die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);
        $data['oInfoCommercant'] = $oInfoCommercant;


        $data['active_link'] = "activite2";
        $oAssComRub =     $this->mdlcommercant->GetRubriqueId($_iCommercantId);

        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom =     $this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;


        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);


        $this->load->view('front/vwMentionslegalesmobile', $data);
    }

    function nousSituerCommercant($_iCommercantId)
    {

        /*
        // Load the library
        $this->load->library('googlemaps');
        // Initialize our map. Here you can also pass in additional parameters for customising the map (see below)
        $this->googlemaps->initialize();
        // Create the map. This will return the Javascript to be included in our pages <head></head> section and the HTML code to be
        // placed where we want the map to appear.
        // Set the marker parameters as an empty array. Especially important if we are using multiple markers
        $marker = array();
        // Specify an address or lat/long for where the marker should appear.
        //$marker['position '] = 'Crescent Park, Palo Alto';
        $zExemple = $marker['position '] = 'Crescent Park, Palo Alto';
        print_r($zExemple) ;
        // $marker['position'] = 'France, France, France';
        // Once all the marker parameters have been specified lets add the marker to our map
        $this->googlemaps->add_marker($marker);
        $data['map'] = $this->googlemaps->create_map();
        // Load our view, passing the map data that has just been created
        //$this->load->view('my_view', $data);
        */

        $nom_url_commercant = $this->uri->rsegment(3); //die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);
        $data['mdlville'] = $this->mdlville;
        $data['oInfoCommercant'] = $oInfoCommercant;

        $data['active_link'] = "situer";
        $oAssComRub =     $this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom =     $this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;


        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user");
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser = 0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser, $_iCommercantId));

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId);
        $data['toBonPlan'] = $toBonPlan;


        //$this->load->view('front/vwNousSituerCommercant', $data) ;
        $this->load->view('front2013/noussituercommercant', $data);
    }


    function coordonneeshoraires($_iCommercantId)
    {

        $nom_url_commercant = $this->uri->rsegment(3); //die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);
        $data['mdlville'] = $this->mdlville;
        $data['oInfoCommercant'] = $oInfoCommercant;

        $data['active_link'] = "accueil";
        $oAssComRub =     $this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom =     $this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $this->load->view('front2013/coordonneeshoraires', $data);
    }

    function invalid_account()
    {
        $data['title'] = "Compte invalide";
        $data["zTitle"] = 'Erreur, Compte invalide';
        $this->load->view('privicarte/invalid_account', $data);
    }


    function presentation($_iCommercantId)
    {
        statistiques();
        $nom_url_commercant = $this->uri->rsegment(3); //die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);
        // var_dump($_iCommercantId); die("STOP");
        if (!isset($_iCommercantId) || $_iCommercantId == NULL || $_iCommercantId == "") redirect("front/commercant/invalid_account/");

        $oInfoCommercant = $this->mdlcommercant->infoCommercant(intval($_iCommercantId));
        $nbPhoto = 0;
        if ($oInfoCommercant->Photo1 != "" && $oInfoCommercant->Photo1 != null) {
            $nbPhoto += 1;
        }
        if ($oInfoCommercant->Photo2 != "" && $oInfoCommercant->Photo2 != null) {
            $nbPhoto += 1;
        }
        if ($oInfoCommercant->Photo3 != "" && $oInfoCommercant->Photo3 != null) {
            $nbPhoto += 1;
        }
        if ($oInfoCommercant->Photo4 != "" && $oInfoCommercant->Photo4 != null) {
            $nbPhoto += 1;
        }
        if ($oInfoCommercant->Photo5 != "" && $oInfoCommercant->Photo5 != null) {
            $nbPhoto += 1;
        }
        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;
        $data['oInfoCommercant'] = $oInfoCommercant;
        $data['nbPhoto'] = $nbPhoto;

        //$data['mdlannonce'] = $this->mdlannonce ;
        $data['mdlbonplan'] = $this->mdlbonplan;

        $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oBonPlan =     $this->mdlbonplans->bonPlanParCommercant($_iCommercantId);
        $data['nbBonPlan'] = sizeof($oBonPlan);
        //      $data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_titre : "";     // OP 27/11/2011
        $data['sTitreBonPlan'] = (sizeof($oBonPlan) > 0) ? $oBonPlan->bonplan_texte : "";
        $data['oImagespub'] = $this->mdlimagespub->GetByImagespubActiv();
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $oLastbonplanCom =     $this->mdlbonplans->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $data['active_link'] = "presentation";

        $oAssComRub =     $this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user");
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser = 0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser, $_iCommercantId));
        $data['pagecategory'] = "pro";
        $data['link_partner_current_page'] = 'presentation';


        $data['pageglissiere'] = "presentation";
        $this->load->model("mdlglissiere");
        $data['mdlglissiere'] = $this->mdlglissiere;

        $data['current_partner_menu'] = "presentation";

        $toBonPlan = $this->mdlbonplans->getListeBonPlan($_iCommercantId);
        $data['toBonPlan'] = $toBonPlan;

        //ajout bouton rond noire annonce/agenda
        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
        else $data['result_check_commercant_annonce'] = '0';

        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
        else $data['result_check_commercant_agenda'] = '0';

        $is_reserved_on = $this->Mdl_plat_du_jour->verify_reserved($_iCommercantId);
        $data['reservation'] = $is_reserved_on;
        //var_dump($data);

        //$this->load->view('front/vwPresentationCommercant', $data) ;
        //$this->load->view('front2013/presentationcommercant', $data) ;
        //var_dump($data);die("zehahaha debug");
        $this->load->view('privicarte/presentationcommercant', $data);
    }

    function accueil($_iCommercantId)
    {
        statistiques();
        $nom_url_commercant = $this->uri->rsegment(3); //die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);
        // var_dump($_iCommercantId); die("STOP");
        if (!isset($_iCommercantId) || $_iCommercantId == NULL || $_iCommercantId == "") redirect("front/commercant/invalid_account/");

        $oInfoCommercant = $this->mdlcommercant->infoCommercant(intval($_iCommercantId));
        $nbPhoto = 0;
        if ($oInfoCommercant->Photo1 != "" && $oInfoCommercant->Photo1 != null) {
            $nbPhoto += 1;
        }
        if ($oInfoCommercant->Photo2 != "" && $oInfoCommercant->Photo2 != null) {
            $nbPhoto += 1;
        }
        if ($oInfoCommercant->Photo3 != "" && $oInfoCommercant->Photo3 != null) {
            $nbPhoto += 1;
        }
        if ($oInfoCommercant->Photo4 != "" && $oInfoCommercant->Photo4 != null) {
            $nbPhoto += 1;
        }
        if ($oInfoCommercant->Photo5 != "" && $oInfoCommercant->Photo5 != null) {
            $nbPhoto += 1;
        }
        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;
        $data['oInfoCommercant'] = $oInfoCommercant;
        $data['nbPhoto'] = $nbPhoto;

        //$data['mdlannonce'] = $this->mdlannonce ;
        $data['mdlbonplan'] = $this->mdlbonplan;

        $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oBonPlan =     $this->mdlbonplans->bonPlanParCommercant($_iCommercantId);
        $data['nbBonPlan'] = sizeof($oBonPlan);
        //      $data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_titre : "";     // OP 27/11/2011
        $data['sTitreBonPlan'] = (sizeof($oBonPlan) > 0) ? $oBonPlan->bonplan_texte : "";
        $data['oImagespub'] = $this->mdlimagespub->GetByImagespubActiv();
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $oLastbonplanCom =     $this->mdlbonplans->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $data['active_link'] = "presentation";

        $oAssComRub =     $this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user");
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser = 0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser, $_iCommercantId));
        $data['pagecategory'] = "pro";
        $data['link_partner_current_page'] = 'presentation';


        $data['pageglissiere'] = "presentation";
        $this->load->model("mdlglissiere");
        $data['mdlglissiere'] = $this->mdlglissiere;

        $data['current_partner_menu'] = "presentation";

        $toBonPlan = $this->mdlbonplans->getListeBonPlan($_iCommercantId);
        $data['toBonPlan'] = $toBonPlan;

        //ajout bouton rond noire annonce/agenda
        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
        else $data['result_check_commercant_annonce'] = '0';

        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
        else $data['result_check_commercant_agenda'] = '0';

        $is_reserved_on = $this->Mdl_plat_du_jour->verify_reserved($_iCommercantId);
        $data['reservation'] = $is_reserved_on;
        //var_dump($data);

        //$this->load->view('front/vwPresentationCommercant', $data) ;
        //$this->load->view('front2013/presentationcommercant', $data) ;
        //var_dump($data);die("zehahaha debug");
        $this->load->view('test', $data);
    }

    function listeBonPlanParCommercant($_iCommercantId, $bonplan_id = 0, $mssg = 0)
    {
        statistiques();

        $nom_url_commercant = $this->uri->rsegment(3); //die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            $data['id_client'] = $iduser;
        }

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);
        $data['oInfoCommercant'] = $oInfoCommercant;
        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId);
        $data['toBonPlan'] = $toBonPlan;



        if ($bonplan_id == 0) {
            $oBonPlan2 = $this->mdlbonplan->lastBonplanCom2($_iCommercantId);
        } else {
            $oBonPlan2 = $this->mdlbonplan->getById($bonplan_id);
        }
        $data['oBonPlan'] = $oBonPlan2;
        $data['toBonPlan2'] = $oBonPlan2;



        $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oBonPlan =     $this->mdlbonplan->bonPlanParCommercant($_iCommercantId);
        $data['nbBonPlan'] = sizeof($oBonPlan);

        $data['active_link'] = "bonplans";
        $oAssComRub =     $this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom =     $this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user");
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser = 0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser, $_iCommercantId));

        //lien pour retour automatique
        $page_from = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $_SESSION['page_from'] = $page_from;
        //lien pour retour automatique

        $data['mssg'] = $mssg;
        $data['pagecategory_partner'] = 'bonplans_partner';


        if (isset($_POST['text_mail_form_module_detailbonnplan'])) {
            $text_mail_form_module_detailbonnplan = $this->input->post("text_mail_form_module_detailbonnplan");
            $nom_mail_form_module_detailbonnplan = $this->input->post("nom_mail_form_module_detailbonnplan");
            $tel_mail_form_module_detailbonnplan = $this->input->post("tel_mail_form_module_detailbonnplan");
            $email_mail_form_module_detailbonnplan = $this->input->post("email_mail_form_module_detailbonnplan");

            $colDestAdmin = array();
            $colDestAdmin[] = array("Email" => $oInfoCommercant->Email, "Name" => $oInfoCommercant->Nom . " " . $oInfoCommercant->Prenom);

            // Sujet
            $txtSujetAdmin = "Demande d'information sur un Bonplan Privicarte";

            $txtContenuAdmin = "
            <p>Bonjour ,</p>
            <p>Une demande d'information vous est adressé suite au bon plan que vous avez déposé sur Privicarte.fr</p>
            <p>Détails du bonplan<br/>
            Désignation : " . $oBonPlan2->bonplan_titre . "<br/>
            N° : " . ajoutZeroPourString($oBonPlan2->bonplan_id, 6) . " du " . convertDateWithSlashes($oBonPlan2->bonplan_date_debut) . "
            </p><p>
            Nom Client : " . $nom_mail_form_module_detailbonnplan . "<br/>
            Téléphone Client : " . $tel_mail_form_module_detailbonnplan . "<br/>
            Email Client : " . $email_mail_form_module_detailbonnplan . "<br/><br/>
            Demande Client :<br/>
            " . $text_mail_form_module_detailbonnplan . "<br/><br/>
            </p>";

            @envoi_notification($colDestAdmin, $txtSujetAdmin, $txtContenuAdmin);
            $data['mssg_envoi_module_detail_bonplan'] = '<font color="#00CC00">Votre demande est envoyée</font>';
            //$data['mssg_envoi_module_detail_bonplan'] = $txtContenuAdmin;

        } else $data['mssg_envoi_module_detail_bonplan'] = '';

        $data['pagecategory_partner'] = 'bonplans_partner';

        $data['pagecategory'] = "pro";

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $data['link_partner_current_page'] = 'bonplan';

        $data['current_partner_menu'] = "bonplan";


        //ajout bouton rond noire annonce/agenda
        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
        else $data['result_check_commercant_annonce'] = '0';

        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
        else $data['result_check_commercant_agenda'] = '0';


        if ($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)) {
            $this->load->model("assoc_client_bonplan_model");
            $user_ion_auth_verif = $this->ion_auth->user()->row();
            $iduser_verif = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth_verif->id);
            $data['bonplan_unique_verification'] = $this->assoc_client_bonplan_model->get_where(array('assoc_client_bonplan.id_client' => $iduser_verif, 'assoc_client_bonplan.id_bonplan' => $bonplan_id));
        }


        //$this->load->view('front/vwBonPlanParCommercant', $data) ;
        //$this->load->view('front2013/page_bonplan_partenaire', $data) ;
        $this->load->view('privicarte/page_bonplan_partenaire', $data);
    }
    function recommanderAmi($_iCommercantId)
    {

        $nom_url_commercant = $this->uri->rsegment(3); //die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant

        redirect(site_url($nom_url_commercant));

        if ($nom_url_commercant != "agenda" && $nom_url_commercant != "article") {

            $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

            $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);
            $data['oInfoCommercant'] = $oInfoCommercant;


            $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId);
            $data['toBonPlan'] = $toBonPlan;

            $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
            $oBonPlan =     $this->mdlbonplan->bonPlanParCommercant($_iCommercantId);
            $data['nbBonPlan'] = sizeof($oBonPlan);

            $data['active_link'] = "accueil";
            $oAssComRub =     $this->mdlcommercant->GetRubriqueId($_iCommercantId);
            if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
            $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
            $oLastbonplanCom =     $this->mdlbonplan->lastBonplanCom($_iCommercantId);
            if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

            $is_mobile = $this->agent->is_mobile();
            //test ipad user agent
            $is_mobile_ipad = $this->agent->is_mobile('ipad');
            $data['is_mobile_ipad'] = $is_mobile_ipad;
            $is_robot = $this->agent->is_robot();
            $is_browser = $this->agent->is_browser();
            $is_platform = $this->agent->platform();
            $data['is_mobile'] = $is_mobile;
            $data['is_robot'] = $is_robot;
            $data['is_browser'] = $is_browser;
            $data['is_platform'] = $is_platform;

            $this->load->model("user");
            if ($this->ion_auth->logged_in()) {
                $user_ion_auth = $this->ion_auth->user()->row();
                $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                if ($iduser == null || $iduser == 0 || $iduser == "") {
                    $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                }
            } else $iduser = 0;
            $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser, $_iCommercantId));

            $this->load->model("mdl_agenda");
            $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

            //$this->load->view('front/vwRecommanderAmi', $data) ;
            $this->load->view('front2013/recommanderami', $data);
        } else if ($nom_url_commercant == "agenda") {
            $idAgenda = $this->uri->rsegment(4);
            $this->load->model("mdl_agenda");
            $oAgenda = $this->mdl_agenda->GetById($idAgenda);
            if (isset($oAgenda)) $data['IdCommercant'] = $oAgenda->IdCommercant;
            else $data['IdCommercant'] = "0";
            $data['idAgenda'] = $idAgenda;
            $this->load->view('frontAout2013/recommanderami', $data);
        } else if ($nom_url_commercant == "article") {
            $idAgenda = $this->uri->rsegment(4);
            $this->load->model("mdlarticle");
            $oAgenda = $this->mdlarticle->GetById($idAgenda);
            if (isset($oAgenda)) $data['IdCommercant'] = $oAgenda->IdCommercant;
            else $data['IdCommercant'] = "0";
            $data['idAgenda'] = $idAgenda;
            $this->load->view('sortez_mobile/recommanderami', $data);
        }
    }
    function envoiMailRecommander($_iCommercantId)
    {
        $_iCommercantId = $this->uri->rsegment(3);
        $_iAgenda = $this->uri->rsegment(4);
        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);
        /*$config = array();
        $config[ 'protocol' ] = 'mail' ;
        $config[ 'mailtype' ] = 'html' ;
        $config[ 'charset' ] = 'utf-8' ;*/
        $this->load->library('email');

        $errorMail    = "";

        $mail_to      = $_POST['zEmail'];
        //$mail_cc = $_POST['zMailTo']  ;
        $mail_to_name = "Privicarte";
        $mail_subject = "[Privicarte] Recommander " . $oInfoCommercant->NomSociete;
        $mail_expediteur = $_POST['zNom'];
        $data = array();
        $data["zNom"] = $_POST['zNom'];
        $data["zTelephone"] = $_POST['zTelephone'];
        $data["zCommentaire"] = $_POST['zCommentaire'];
        //$mail_body = "Ceci est un mail de Recommandation \n\n";
        //$mail_body = $this->load->view("front/vwContenuMailNousContacterAnnonce", $data, true) ;
        $mail_body = '
Bonjour,


Une demande de contact a été formmulée provenant de Privicarte.fr

Expediteur : ' . $_POST['zNom'] . '

Nom : ' . $_POST['zTelephone'] . '


' . $_POST['zCommentaire'] . '
';

        $this->email->from($mail_expediteur, "Privicarte");
        $this->email->to($mail_to);
        //$this->email->cc($mail_cc);
        $this->email->bcc("randawilly@gmail.com");
        $this->email->subject($mail_subject);
        //$this->email->attach($mail_Document);
        $this->email->message($mail_body);



        $data['oInfoCommercant'] = $oInfoCommercant;
        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId);
        $data['toBonPlan'] = $toBonPlan;

        $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oBonPlan =     $this->mdlbonplan->bonPlanParCommercant($_iCommercantId);
        $data['nbBonPlan'] = sizeof($oBonPlan);

        $data['active_link'] = "accueil";
        $oAssComRub =     $this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom =     $this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user");
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser = 0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser, $_iCommercantId));


        if ($this->email->send()) {
            //echo "Le mail est bien envoyé !" ;



            //$this->load->view('front/vwRecommanderAmiSucces', $data) ;
            if (isset($_iAgenda) && $_iAgenda == "agenda") {
                $this->load->view('frontAout2013/recommanderamisucces', $data);
            } else {
                $this->load->view('front2013/recommanderamisucces', $data);
            }
        } else {
            //echo "Un problème est survenu, veuillez renvoyer le mail !" ;
            //$this->load->view('front/vwRecommanderAmiErreur', $data) ;

            if (isset($_iAgenda) && $_iAgenda == "agenda") {
                $this->load->view('frontAout2013/recommanderamierreur', $data);
            } else {
                $this->load->view('front2013/recommanderamierreur', $data);
            }
        }
    }




    function manage_nbrevisites($_iCommercantId)
    {

        //$_iCommercantId = $this->uri->rsegment(3);



        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);


        $nbvisite = intval($oInfoCommercant->nbrevisites);
        $nbvisite_nv = $nbvisite + 1;

        //$oInfoCommercant['nbrevisites'] = $nbvisite;


        $this->mdlcommercant->Update_nbrevisites_commercant($_iCommercantId, $nbvisite_nv);

        echo $nbvisite_nv;
    }
    public function reservation()
    {

        $nom_url_commercant = $this->uri->rsegment(3); //die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);
        $data['oInfoCommercant'] = $oInfoCommercant;

        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;

        $data['active_link'] = "activite1";
        $oAssComRub =     $this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom =     $this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user");
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser = 0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser, $_iCommercantId));

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $data['current_page'] = "page_infos";
        $data['pagecategory'] = "pro";

        $data['pageglissiere'] = "page1";
        $this->load->model("mdlglissiere");
        $data['mdlglissiere'] = $this->mdlglissiere;
        $data['Mdl_plat_du_jour'] = $this->Mdl_plat_du_jour;

        $data['link_partner_current_page'] = 'page1';

        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId);
        $data['toBonPlan'] = $toBonPlan;

        //ajout bouton rond noire annonce/agenda
        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
        else $data['result_check_commercant_annonce'] = '0';

        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
        else $data['result_check_commercant_agenda'] = '0';

        $is_reserved_on = $this->Mdl_plat_du_jour->verify_reserved($_iCommercantId);
        $data['reservation'] = $is_reserved_on;

        //$this->load->view('front/vwPlusInfoCommercant', $data) ;
        //$this->load->view('front2013/plusinfoscommercant', $data) ;
        $this->load->view('privicarte/reservation', $data);
    }

    public function valid_reservation_client()
    {
        $IdPlat = $this->input->post('IdPlat');
        $IdCommercant = $this->input->post('IdCommercant');
        $heure_reservation = $this->input->post('heure_reservation');
        $nbre_pers_reserved = $this->input->post('nbre_pers_reserved');
        $nbre_platDuJour = $this->input->post('nbre_platDuJour');
        $num_carte = $this->input->post('num_carte');
        $date_reservation = date('Y-m-d');
        $datacom = $this->Mdl_plat_du_jour->get_resinfocom_by_idcom($IdCommercant);
        //    var_dump($datacom);die('test info com');

        if (isset($num_carte) and $num_carte != "" and $num_carte != null) {
            $is_valid_card = $this->verif_carte($num_carte);
            if ($is_valid_card == '0') {
                echo 'numero de carte invalide';
            } elseif (count($is_valid_card) != 0) {
                date_default_timezone_set('Europe/Paris');
                $field = array(
                    "IdPlat" => $IdPlat,
                    "IdCommercant" => $IdCommercant,
                    "heure_reservation" => $heure_reservation,
                    "nbre_pers_reserved" => (int)$nbre_pers_reserved,
                    "nbre_platDuJour" => (int)$nbre_platDuJour,
                    "num_carte" => (int)$num_carte,
                    "date_reservation" => $date_reservation,
                    "datetime_reservation" => date('Y-m-d H:i:s'),
                );
                $is_saved = $this->Mdl_plat_du_jour->save_reservation_client($field);

                if ($is_saved == 1) {
                    $about_plat = $this->Mdl_plat_du_jour->getbyidplat($IdPlat);
                    $aboutuser = $this->User->getById_client($is_valid_card->id_user);
                    if (isset($aboutuser->Telephone) and $aboutuser->Telephone != '' and $aboutuser->Telephone != null) {
                        $tel = $aboutuser->Telephone;
                    } elseif (isset($aboutuser->Portable) and $aboutuser->Portable != '' and $aboutuser->Portable != null) {
                        $tel = $aboutuser->Portable;
                    }
                    if (count($aboutuser) != 0) {

                        $contact_message_content_fc2 = "
                <p>Bonjour,<br/>
    Ceci est une mail de copie de votre réservation de plat dans Sortez.org<br/>
    ci-dessous le contenu,
</p>

<p>
    - Votre Nom du  :" . $aboutuser->Nom . " " . $aboutuser->Prenom . "<br/>
    - La date de réservation : " . date('Y-m-d') . "  Heure:" . $heure_reservation . "<br/>
    - Votre téléphone:" . $tel . "<br/>
    - Nombre de personne:" . $nbre_pers_reserved . " personne(s)<br/>
    - Nombre de plat réservé:" . $nbre_platDuJour . " plat(s)<br/>
    - Nom du plat réservé: " . $about_plat->description_plat . "<br>
</p>
Cordialement,
<br/>Sortez.org
                ";
                        $contact_to_fc = $datacom->mail_reservation_plat;
                        $contact_object_fc = 'Réservation d\'un plat';
                        $contact_message_content_fc = "<p>Bonjour,<br/>
    Ceci est une demande de réservation de plat de votre client sur Sortez.org<br/>
    ci-dessous le contenu,
</p>

<p>
    - Le Nom du client :" . $aboutuser->Nom . " " . $aboutuser->Prenom . "<br/>
    - La date de réservation : " . date('Y-m-d') . "  Heure:" . $heure_reservation . "<br/>
    - Téléphone du client:" . $tel . "<br/>
    - Email du client:" . $aboutuser->Email . "<br>
    - Nombre de personne:" . $nbre_pers_reserved . " personne(s)<br/>
    - Nombre de plat réservé:" . $nbre_platDuJour . " plat(s)<br/>
    - Nom du plat réservé: " . $about_plat->description_plat . "<br>
</p>
Cordialement,
<br/>Sortez.org,";
                        $headers_fc = 'From: ' . $aboutuser->Email . '' . "\r\n" .
                            'Reply-To: ' . $aboutuser->Email . '' . "\r\n" .
                            'X-Mailer: PHP/' . phpversion();
                        $contact_to_fc2 = $aboutuser->Email;
                        $headers_fc2 = 'From: sortez.org' . "\r\n" .
                            'Reply-To: ' . $datacom->mail_reservation_plat . '' . "\r\n" .
                            'X-Mailer: PHP/' . phpversion();
                        $headers_fc2 .= "MIME-Version: 1.0\r\n";
                        $headers_fc2 .= "Content-Type: text/html; charset=utf8\r\n";

                        $headers_fc .= "MIME-Version: 1.0\r\n";
                        $headers_fc .= "Content-Type: text/html; charset=utf8\r\n";
                        if (mail($contact_to_fc, $contact_object_fc, utf8_decode($contact_message_content_fc), $headers_fc)) {
                            //                    var_dump($contact_to_fc2);
                            //                    var_dump($contact_object_fc);
                            //                    var_dump($contact_message_content_fc2);
                            //                    var_dump($headers_fc2);die('test mail');
                            if (mail($contact_to_fc2, $contact_object_fc, utf8_decode($contact_message_content_fc2), $headers_fc2)) {
                                $this->Mdl_plat_du_jour->update_stat_plat($IdPlat, $nbre_platDuJour);
                                echo 'Plat reservé';
                            } else {
                                echo 'erreur1';
                            }
                        } else {
                            echo 'erreur2';
                        }
                    }
                } else {
                    echo 'erreur3';
                }
            } else {
                echo 'numero de carte invalide';
            }
        }
    }

    public function verif_carte($id_card)
    {
        $is_valid = $this->Mdl_card->getByCard($id_card);

        if (count($is_valid) > 0) {
            return $is_valid;
        } else {
            return 0;
        }
    }
    public function desactivate_plat()
    {
        $id_plat = $this->input->post('IdPlat');
        $unactive = $this->Mdl_plat_du_jour->desactive_plat($id_plat);
        if ($unactive == 'ok') {
            echo 'ok';
        }
    }

    public function submit_res_sejour()
    {
        $Id_commercant = $this->input->post('Id_commercant');
        $id_client = $this->input->post('id_client');
        $Pays = $this->input->post('Pays');
        $Nom = $this->input->post('Nom');
        $prenom = $this->input->post('prenom');
        $Adresse = $this->input->post('Adresse');
        $code_postal = $this->input->post('code_postal');
        $mail = $this->input->post('mail');
        $id_ville = $this->input->post('id_ville');
        $tel = $this->input->post('tel');
        $date_debut_res = $this->input->post('date_debut_res');
        $date_fin_res = $this->input->post('date_fin_res');
        $nbre_adulte = $this->input->post('nbre_adulte');
        $nbre_enfant = $this->input->post('nbre_enfant');
        $message_client = $this->input->post('message_client');
        $date_de_reservation = date('Y-m-d');
        $num_card = $this->input->post('num_card');
        $etat = '1';

        $field = array(
            "Id_commercant" => $Id_commercant,
            "id_client" => $id_client,
            "Pays" => $Pays,
            "Nom" => $Nom,
            "Adresse" => $Adresse,
            "code_postal" => $code_postal,
            "mail" => $mail,
            "id_ville" => $id_ville,
            "tel" => $tel,
            "date_debut_res" => $date_debut_res,
            "date_fin_res" => $date_fin_res,
            "nbre_adulte" => $nbre_adulte,
            "nbre_enfant" => $nbre_enfant,
            "message_client" => $message_client,
            "date_de_reservation" => $date_de_reservation,
            "num_card" => $num_card,
            "etat" => $etat,
            "prenom" => $prenom
        );
        $save = $this->Mdl_reservation->save_reservation_sejour($field);
        $infocm = $this->mdlcommercant->infoCommercant($Id_commercant);
        if ($infocm->mail_reservation != '' and $infocm->mail_reservation != null) {
            $mail_com = $infocm->mail_reservation;
        } else {
            $mail_com = $infocm->Email;
        }

        // $contact_to = $mail;
        $contact_to = $mail_com;
        $contact_object = "Reservation hébergement";
        $contact_message_content = "<p>Bonjour,<br/>
    Ceci est une demande de r&eacute;servation de s&eacute;jour de votre client sur Sortez.org<br/>
    ci-dessous le contenu,
</p>

<p>
    - Le Nom du client :" . $Nom . " " . $prenom . "<br/>
    - La date de r&eacute;servation : " . $date_de_reservation . "<br/>
    - La date d'arriv&eacute;e :  " . $date_debut_res . "<br/>
    - La date de d&eacute;part :" . $date_fin_res . "<br/>
    - T&eacute;l&eacute;phone du client:" . $tel . "<br/>
    - Mail du client:" . $mail . "<br/>
    - Nombre de personne:" . $nbre_adulte . " Adulte(s) et " . $nbre_enfant . " enfant(s)<br/>
</p>

<p>
    ci-dessous le message du client:<br>
   " . $message_client . "
</p>

Cordialement,
<br/>Sortez.org,";


        $headers = 'From: Sortez.org' . "\r\n" .
            'Reply-To: webmaster@example.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


        $contact_to2 = $mail;
        $contact_object2 = "Reservation de hébergement";
        $contact_message_content2 = "<p>Bonjour,<br/>
    Ceci est un mail de confirmation de votre r&eacute;servation de s&eacute;jour chez le commercant: " . $infocm->NomSociete . " de sortez.org<br/>
    ci-dessous le contenu,
</p>

<p>
    - Votre Nom  :" . $Nom . " " . $prenom . "<br/>
    - La date de votre r&eacute;servation : " . $date_de_reservation . "<br/>
    - La date d'arriv&eacute;e :  " . $date_debut_res . "<br/>
    - La date de d&eacute;part:" . $date_fin_res . "<br/>
    - Votre t&eacute;l&eacute;phone;" . $tel . "<br/>
    - Nombre de personne:" . $nbre_adulte . " Adulte(s) et " . $nbre_enfant . " enfant(s)<br/>
</p>
Cordialement,
<br/>Sortez.org,";


        $headers2 = 'From: Sortez.org' . "\r\n" .
            'Reply-To: sortez.org' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers2 .= "MIME-Version: 1.0\r\n";
        $headers2 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $contact_to3 = 'srova76@gmail.com';
        $contact_object3 = "Reservation de hébergement";
        $contact_message_content3 = "<p>Bonjour,<br/>
    Ceci est un mail de confirmation de  r&eacute;servation de s&eacute;jour d'un client sortez.org<br/>
    ci-dessous le contenu,
</p>

<p>
    - Le Nom du client :" . $Nom . " " . $prenom . "<br/>
    - La date de r&eacute;servation : " . $date_de_reservation . "<br/>
    - La date d'arriv&eacute;e :  " . $date_debut_res . "<br/>
    - La date de d&eacute;part :" . $date_fin_res . "<br/>
    - T&eacute;l&eacute;phone du client:" . $tel . "<br/>
    - Nombre de personne:" . $nbre_adulte . " Adulte(s) et " . $nbre_enfant . " enfant(s)<br/>
</p>
Cordialement,
<br/>Sortez.org,";


        $headers3 = 'From: Sortez.org' . "\r\n" .
            'Reply-To: sortez.org' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers3 .= "MIME-Version: 1.0\r\n";
        $headers3 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        if (mail($contact_to, $contact_object, $contact_message_content, $headers) and mail($contact_to2, $contact_object2, $contact_message_content2, $headers2) and mail($contact_to3, $contact_object3, $contact_message_content3, $headers3)) {
            if ($save == 1) {
                echo 'ok';
            } else {
                echo 'ko';
            }
        }
    }
    public function get_ville_by_code_postal()
    {

        $codepostal = $this->input->post('code_postal');
        $res = $this->mdlville->GetVilleByCodePostal_localisation_res($codepostal);
        print_r($res[0]->IdVille);
    }

    public function get_abonner_news_letter()
    {

        $nom_abonner = $this->input->post('nom_abonner');
        $email_abonner = $this->input->post('email_abonner');
        $mobile_abonner = $this->input->post('mobile_abonner');
        $idcom = $this->input->post('idcom');
        $abonner = array(
            "nom" => $nom_abonner,
            "email" => $email_abonner,
            "mobile" => $mobile_abonner,
            "id_commercant" => $idcom
        );
        $verify_exist = $this->Mdl_news_letter_commercant->verify($email_abonner);
        if ($verify_exist == 0) {
            $res = $this->Mdl_news_letter_commercant->save($abonner);
            if ($res == '1') {
                echo 'ok';
            } else {
                echo 'ko';
            }
        } else {
            echo 'exist';
        }
    }

    public function submit_res_table()
    {
        $this->load->library('email');

        $Id_commercant = $this->input->post('Id_commercant');
        $id_client = $this->input->post('id_client');
        $Pays = $this->input->post('Pays');
        $Nom = $this->input->post('Nom');
        $prenom = $this->input->post('prenom');
        $Adresse = $this->input->post('Adresse');
        $code_postal = $this->input->post('code_postal');
        $mail = $this->input->post('mail');
        $id_ville = $this->input->post('id_ville');
        $tel = $this->input->post('tel');
        $heure_midi = $this->input->post('heure_midi');
        $heure_soir = $this->input->post('heure_soir');
        $date_res = $this->input->post('date_res');
        $nbre_adulte = $this->input->post('nbre_adulte');
        $nbre_enfant = $this->input->post('nbre_enfant');
        $message_client = $this->input->post('message_client');
        $date_de_reservation = date('Y-m-d');
        $num_card = $this->input->post('num_card');
        $etat = '1';
        if (isset($id_ville) and $id_ville != "" and $id_ville != 0) {
            $ville_nom = $this->mdlville->getVilleById($id_ville)->NomSimple;
        } else {
            $ville_nom = "Inconnu";
        }
        $field = array(
            "Id_commercant" => $Id_commercant,
            "id_client" => $id_client,
            "Pays" => $Pays,
            "Nom" => $Nom,
            "Adresse" => $Adresse,
            "code_postal" => $code_postal,
            "mail" => $mail,
            "id_ville" => $id_ville,
            "tel" => $tel,
            "heure_midi" => $heure_midi,
            "heure_soir" => $heure_soir,
            "date_res" => $date_res,
            "nbre_adulte" => $nbre_adulte,
            "nbre_enfant" => $nbre_enfant,
            "message_client" => $message_client,
            "date_de_reservation" => $date_de_reservation,
            "num_card" => $num_card,
            "etat" => $etat,
            "prenom" => $prenom
        );
        $infocm = $this->mdlcommercant->infoCommercant($Id_commercant);
        // var_dump($infocm);
        // die();
        if ($infocm->mail_reservation != '' and $infocm->mail_reservation != null) {
            $mail_com = $infocm->mail_reservation;
        } else {
            $mail_com = $infocm->Email;
        }
        $save = $this->Mdl_reservation->save_reservation_table($field);

        $data['Nom'] = $Nom;
        $data['prenom'] = $prenom;
        $data['date_de_reservation'] = $date_de_reservation;
        $data['date_res'] = $date_res;
        $data['heure_midi'] = $heure_midi;
        $data['heure_soir'] = $heure_soir;
        $data['tel'] = $tel;
        $data['nbre_adulte'] = $nbre_adulte;
        $data['nbre_enfant'] = $nbre_enfant;
        $data['message_client'] = $message_client;

        $contact_to = $mail_com;
        $contact_to = array();
        $contact_to[] =  array(
            'Name' => $Nom,
            'Email' => $mail
            // array(
            //     'Name' => $infocm->NomSociete,
            //     'Email' => $mail_com
            // ),
        );

        $contact_to2 = array();
        $contact_to2[] =  array(
            'Name' => $infocm->NomSociete,
            'Email' => $mail_com
        );
        // $contact_to = array();
        // $contact_to[] =  array(
        //     "Email" => "etadev@randevteam.com",
        //     "Name" => "Nary"
        // );

        $contact_object = "Reservation de Table";
        $contact_message_content = "<p>Bonjour,<br/>
    Ceci est une demande de r&eacute;servation de table de votre client sur Sortez.org<br/>
    ci-dessous le contenu,
</p>

<p>
    - Le Nom du client :" . $Nom . " " . $prenom . "<br/>
    - La date de r&eacute;servation : " . $date_de_reservation . "<br/>
    - La date prevue :  " . $date_res . "<br/>
    - L'heure prevue :" . $heure_midi . "-" . $heure_soir . "<br/>
    - T&eacute;l&eacute;phone du client:" . $tel . "<br/>
    - Mail du client:" . $mail . "<br/>
    - Nombre de personne:" . $nbre_adulte . " Adulte(s) et " . $nbre_enfant . " enfant(s)<br/>
</p>

<p>
    ci-dessous le message du client:<br>
   " . $message_client . "
</p>

Cordialement,
<br/>Sortez.org,";


        $headers = 'From: Sortez.org' . "\r\n" .
            'Reply-To: webmaster@example.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


        // $contact_to2 = $mail;
        $contact_object2 = "Reservation de Table";
        $contact_message_content2 = "<p>Bonjour,<br/>
    Ceci est un mail de confirmation de votre r&eacute;servation de table chez le commercant : " . $infocm->NomSociete . " de  sortez.org<br/>
    ci-dessous le contenu,
</p>

<p>
    - Votre Nom  :" . $Nom . " " . $prenom . "<br/>
    - La date de votre r&eacute;servation : " . $date_de_reservation . "<br/>
    - La date prevue :  " . $date_res . "<br/>
    - L'heure prevue :" . $heure_midi . "-" . $heure_soir . "<br/>
    - Votre t&eacute;l&eacute;phone;" . $tel . "<br/>
    - Nombre de personne:" . $nbre_adulte . " Adulte(s) et " . $nbre_enfant . " enfant(s)<br/>
</p>
Cordialement,
<br/>Sortez.org,";


        $headers2 = 'From: Sortez.org' . "\r\n" .
            'Reply-To: sortez.org' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers2 .= "MIME-Version: 1.0\r\n";
        $headers2 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $contact_to3 = 'srova76@gmail.com';
        $contact_object3 = "Reservation de Table";
        $contact_message_content3 = "<p>Bonjour,<br/>
    Ceci est un mail de confirmation de  r&eacute;servation de table d'un client sortez.org<br/>
    ci-dessous le contenu,
</p>

<p>
    - Le Nom du client :" . $Nom . " " . $prenom . "<br/>
    - La date de r&eacute;servation : " . $date_de_reservation . "<br/>
    - La date prevue :  " . $date_res . "<br/>
    - L'heure prevue :" . $heure_midi . "-" . $heure_soir . "<br/>
    - T&eacute;l&eacute;phone du client:" . $tel . "<br/>
    - Nombre de personne:" . $nbre_adulte . " Adulte(s) et " . $nbre_enfant . " enfant(s)<br/>
</p>
Cordialement,
<br/>Sortez.org,";


        $headers3 = 'From: Sortez.org' . "\r\n" .
            'Reply-To: sortez.org' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers3 .= "MIME-Version: 1.0\r\n";
        $headers3 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $envoyeur = $mail_com;
        $nomenvoyeur = $infocm->NomSociete;
        // var_dump(@envoi_notification($contact_to, $contact_object, $contact_message_content, $envoyeur, $nomenvoyeur), @envoi_notification($contact_to2, $contact_object2, $contact_message_content2, $envoyeur, $nomenvoyeur));
        // die();

        if (@envoi_notification($contact_to, $contact_object, $contact_message_content, $envoyeur, $nomenvoyeur) and @envoi_notification($contact_to2, $contact_object2, $contact_message_content2, $envoyeur, $nomenvoyeur)) {
            if ($save == 1) {
                echo 'ok';
            } else {
                echo 'ko1';
            }
        } else {
            echo 'ko2';
        }

        // @envoi_notification($contact_to, $contact_object, $contact_message_content, $headers);

        // $contact_to = $mail_com;
        // var_dump(mail($contact_to, $contact_object, $contact_message_content, $headers, $headers) and mail($contact_to2, $contact_object2, $contact_message_content2, $headers2) and mail($contact_to3, $contact_object3, $contact_message_content3, $headers3));
        // die();
        // if (mail($contact_to, $contact_object, $contact_message_content, $headers) and mail($contact_to2, $contact_object2, $contact_message_content2, $headers2) and mail($contact_to3, $contact_object3, $contact_message_content3, $headers3)) {

        //     if ($save == 1) {
        //         echo 'ok';
        //     } else {
        //         echo 'ko1';
        //     }
        // } else {
        //     echo 'ko2';
        // }
        // $this->email->send();

    }
    public function get_users_by_id_card()
    {

        $num_card = $this->input->post('num_card');
        $client_fiche = $this->mdlcommercant->get_user_by_id_card($num_card);
        if ($client_fiche) {
            echo json_encode($client_fiche);
        } else {
            echo 'no';
        }
    }
    public function details_plat($id, $id2)
    {

        $nom_url_commercant = $this->uri->rsegment(3); //die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);
        $data['oInfoCommercant'] = $oInfoCommercant;


        $data['active_link'] = "activite1";
        $oAssComRub =     $this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom =     $this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user");
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser = 0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser, $_iCommercantId));

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $data['current_page'] = "page_infos";
        $data['pagecategory'] = "pro";

        $data['pageglissiere'] = "page1";
        $this->load->model("mdlglissiere");
        $data['mdlglissiere'] = $this->mdlglissiere;
        $data['Mdl_plat_du_jour'] = $this->Mdl_plat_du_jour;

        $data['link_partner_current_page'] = 'page1';

        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId);
        $data['toBonPlan'] = $toBonPlan;

        //ajout bouton rond noire annonce/agenda
        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
        else $data['result_check_commercant_annonce'] = '0';

        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
        else $data['result_check_commercant_agenda'] = '0';

        $is_reserved_on = $this->Mdl_plat_du_jour->verify_reserved_by_idplat($_iCommercantId, $id2);
        $data['reservation'] = $is_reserved_on;
        //$this->load->view('front/vwPlusInfoCommercant', $data) ;
        //$this->load->view('front2013/plusinfoscommercant', $data) ;
        $this->load->view('privicarte/details_plat_desk', $data);
    }
    public function reservation_plat($idplat, $nbre_pers_reserved, $nbre_platDuJour, $heure_reservation)
    {
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $data['iduser'] = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            $data['iduser'] = $iduser;
            $data['num_card'] = $this->Mdl_card->getByIdUser($iduser);
        } else $data['iduser'] = 0;

        $plat = $this->Mdl_plat_du_jour->getbyidplat($idplat);
        $data['plat'] = $plat;
        $data['active_link'] = "activite1";
        $infocom = $this->mdlcommercant->infoCommercant($plat->IdCommercant);
        $data['oInfoCommercant'] = $infocom;
        $data['nbre_pers_reserved'] = $nbre_pers_reserved;
        $data['nbre_platDuJour'] = $nbre_platDuJour;
        $data['heure_reservation'] = $heure_reservation;
        $oAssComRub =     $this->mdlcommercant->GetRubriqueId($plat->IdCommercant);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($plat->IdCommercant);
        $oLastbonplanCom =     $this->mdlbonplan->lastBonplanCom($plat->IdCommercant);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user");
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser = 0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser, $plat->IdCommercant));
        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($plat->IdCommercant);
        $data['current_page'] = "page_infos";
        $data['pagecategory'] = "pro";
        $data['pageglissiere'] = "page1";
        $this->load->model("mdlglissiere");
        $data['mdlglissiere'] = $this->mdlglissiere;

        $data['link_partner_current_page'] = 'page1';
        $toBonPlan = $this->mdlbonplan->getListeBonPlan($plat->IdCommercant);
        $data['toBonPlan'] = $toBonPlan;
        $this->load->view('privicarte/reservation_plat', $data);
    }
    public function commander()
    {
        $nom_url_commercant = $this->uri->rsegment(3); //die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);
        $data['oInfoCommercant'] = $oInfoCommercant;

        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;

        $data['active_link'] = "activite1";
        $oAssComRub =   $this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom =  $this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user");
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser = 0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser, $_iCommercantId));

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $data['current_page'] = "page_infos";
        $data['pagecategory'] = "pro";

        $data['pageglissiere'] = "page1";
        $this->load->model("mdlglissiere");
        $data['mdlglissiere'] = $this->mdlglissiere;
        $data['Mdl_plat_du_jour'] = $this->Mdl_plat_du_jour;

        $data['link_partner_current_page'] = 'page1';

        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId);
        $data['toBonPlan'] = $toBonPlan;

        //ajout bouton rond noire annonce/agenda
        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
        else $data['result_check_commercant_annonce'] = '0';

        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
        else $data['result_check_commercant_agenda'] = '0';

        $is_reserved_on = $this->Mdl_plat_du_jour->verify_reserved($_iCommercantId);
        $data['reservation'] = $is_reserved_on;

        //$this->load->view('front/vwPlusInfoCommercant', $data) ;
        //$this->load->view('front2013/plusinfoscommercant', $data) ;

        $this->load->view('sortez_soutenons/front/command_partner', $data);
    }

    function home_page_commercant($_iCommercantId)
    {


        $nom_url_commercant = $this->uri->rsegment(3); //die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant

        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);



        // var_dump($_iCommercantId); die("STOP");

        if (!isset($_iCommercantId) || $_iCommercantId == NULL || $_iCommercantId == "") redirect("front/commercant/invalid_account/");



        $oInfoCommercant = $this->mdlcommercant->infoCommercant(intval($_iCommercantId));
        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;

        $nbPhoto = 0;

        if ($oInfoCommercant->Photo1 != "" && $oInfoCommercant->Photo1 != null) {

            $nbPhoto += 1;
        }

        if ($oInfoCommercant->Photo2 != "" && $oInfoCommercant->Photo2 != null) {

            $nbPhoto += 1;
        }

        if ($oInfoCommercant->Photo3 != "" && $oInfoCommercant->Photo3 != null) {

            $nbPhoto += 1;
        }

        if ($oInfoCommercant->Photo4 != "" && $oInfoCommercant->Photo4 != null) {

            $nbPhoto += 1;
        }

        if ($oInfoCommercant->Photo5 != "" && $oInfoCommercant->Photo5 != null) {

            $nbPhoto += 1;
        }



        $data['oInfoCommercant'] = $oInfoCommercant;

        $data['nbPhoto'] = $nbPhoto;



        //$data['mdlannonce'] = $this->mdlannonce ;

        $data['mdlbonplan'] = $this->mdlbonplan;



        $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

        $oBonPlan = $this->mdlbonplans->bonPlanParCommercant($_iCommercantId);

        //$data['nbBonPlan'] = sizeof($oBonPlan);

        //      $data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_titre : "";     // OP 27/11/2011

        //$data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_texte : "";

        $data['oImagespub'] = $this->mdlimagespub->GetByImagespubActiv();

        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);



        $this->load->model("mdl_agenda");

        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);



        $oLastbonplanCom =     $this->mdlbonplans->lastBonplanCom($_iCommercantId);

        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;



        $data['active_link'] = "presentation";



        $oAssComRub =     $this->mdlcommercant->GetRubriqueId($_iCommercantId);

        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);



        $is_mobile = $this->agent->is_mobile();

        //test ipad user agent

        $is_mobile_ipad = $this->agent->is_mobile('ipad');

        $data['is_mobile_ipad'] = $is_mobile_ipad;

        $is_robot = $this->agent->is_robot();

        $is_browser = $this->agent->is_browser();

        $is_platform = $this->agent->platform();

        $data['is_mobile'] = $is_mobile;

        $data['is_robot'] = $is_robot;

        $data['is_browser'] = $is_browser;

        $data['is_platform'] = $is_platform;



        $this->load->model("user");

        if ($this->ion_auth->logged_in()) {

            $user_ion_auth = $this->ion_auth->user()->row();

            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

            if ($iduser == null || $iduser == 0 || $iduser == "") {

                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser = 0;

        //$data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));

        $data['pagecategory'] = "pro";

        $data['link_partner_current_page'] = 'presentation';





        $data['pageglissiere'] = "presentation";

        $this->load->model("mdlglissiere");

        $data['mdlglissiere'] = $this->mdlglissiere;



        $data['current_partner_menu'] = "presentation";



        $toBonPlan = $this->mdlbonplans->getListeBonPlan($_iCommercantId);

        $data['toBonPlan'] = $toBonPlan;



        //ajout bouton rond noire annonce/agenda

        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);

        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';

        else $data['result_check_commercant_annonce'] = '0';



        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);

        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';

        else $data['result_check_commercant_agenda'] = '0';



        $is_reserved_on = $this->Mdl_plat_du_jour->verify_reserved($_iCommercantId);

        $data['reservation'] = $is_reserved_on;

        //var_dump($data);
        $data_seo = $this->Seo_model->get_data_by_Idcommercant($_iCommercantId);
        $data['data_seo'] = $data_seo;


        //$this->load->view('front/vwPresentationCommercant', $data) ;

        //$this->load->view('front2013/presentationcommercant', $data) ;

        //var_dump($data);die("zehahaha debug");

        $this->load->view('Homepage/newpage', $data);
    }
}
