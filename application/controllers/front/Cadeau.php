<?php
class cadeau extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");

        $this->load->library('session');
        $this->load->model('User');

        check_vivresaville_id_ville();
    }
    
    function index(){
        $this->fiche();
    }
    
    function fiche() {
	$this->load->Model("mdlcadeau");
        $data = null; 
        $date = date('Y-m-d');
        $listCadeau = $this->mdlcadeau->GetListeCadeauxvalides($date);	
        $data["listCadeau"] = $listCadeau ;

        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            $id_client = $iduser;
            $user_groups = $this->ion_auth->get_users_groups($user_ion_auth->id)->result();
            ////$this->firephp->log($user_groups, 'user_groups');
            if ($user_groups[0]->id==1) $typeuser = "1";
            else if ($user_groups[0]->id==2) $typeuser = "0";
            else if ($user_groups[0]->id==3 || $user_groups[0]->id==4 || $user_groups[0]->id==5) $typeuser = "COMMERCANT";
            $type_client = $typeuser;

            $data['id_client'] = $id_client;
            $data['type_client'] = $type_client;
        }



        $this->load->view("front/vwCadeau",$data);
    }
    
    function commande($argValidate) {
	$this->load->Model("mdlcadeau");
        $data['argValidate'] = $argValidate; 
        
        $date = date('Y-m-d');
        $listCadeau = $this->mdlcadeau->GetListeCadeauxvalides($date);
        //$listCadeau = $this->mdlcadeau->getListe();	
	$data["listCadeau"] = $listCadeau ;

        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            $id_client = $iduser;
            $user_groups = $this->ion_auth->get_users_groups($user_ion_auth->id)->result();
            ////$this->firephp->log($user_groups, 'user_groups');
            if ($user_groups[0]->id==1) $typeuser = "1";
            else if ($user_groups[0]->id==2) $typeuser = "0";
            else if ($user_groups[0]->id==3 || $user_groups[0]->id==4 || $user_groups[0]->id==5) $typeuser = "COMMERCANT";
            $type_client = $typeuser;

            $data['id_client'] = $id_client;
            $data['type_client'] = $type_client;
        }
		
        $this->load->view("front/vwCadeau",$data);
    }
    
    /**
     * 06/06/2011
     * Liva :
     * Methode pour l'ajout d'un commercant 
     * (A ce niveau, il s'agit d'une demande d'inscription seulemet)
     * Seuls les administrateurs qui peuvent valider l'inscription
     * et envoient les codes d'acces vers le commercants
     */
    function ajouter() {
        $objCommercant = $this->input->post("Societe");
        $objAssCommercantRubrique = $this->input->post("AssCommercantRubrique");
        $objAssCommercantSousRubrique = $this->input->post("AssCommercantSousRubrique");
        $objAssCommercantAbonnement = $this->input->post("AssAbonnementCommercant");
        
        $this->load->Model("Commercant");
        $IdInsertedCommercant = $this->Commercant->Insert($objCommercant);
        
        $this->load->Model("AssCommercantRubrique");
        $objAssCommercantRubrique["IdCommercant"] = $IdInsertedCommercant;
        $this->AssCommercantRubrique->Insert($objAssCommercantRubrique);
        
        $this->load->Model("AssCommercantSousRubrique");
        $objAssCommercantSousRubrique["IdCommercant"] = $IdInsertedCommercant;
        $this->AssCommercantSousRubrique->Insert($objAssCommercantSousRubrique);
        
        $this->load->Model("AssCommercantAbonnement");
        $objAssCommercantAbonnement["IdCommercant"] = $IdInsertedCommercant;
        $this->AssCommercantAbonnement->Insert($objAssCommercantAbonnement);
        
        redirect("front/professionnels/confirmation_demande_adhesion/");
    }
    
    function confirmation_demande_adhesion() {
        $this->load->view("front/vwConfirmationInscriptionProfessionnels");
    }
    function get_click_day(){        
        //debut code iotadev
        $countal = 0;
        date_default_timezone_set('UTC');
        $date = date("Y-m-d");
        $time = date("H:i:s");
        // var_dump($date);die("tapitra");
        //$time1 = explode(':',$time);
        $countstatday = $this->User->get_all_statistiques_by_date($date);
        $nbr_internaut_oneline=count($countstatday);
        // echo "nbr internaute en ligne".$nbr_internaut_oneline."<br>";
        // echo strftime('%A %d %B %Y, %H:%M');
        // $a='<script type="text/javascript"> document.write(Date()) </script>';
        // echo $a."<br>";
        // $b= date('Y-m-d H:i:s');
        // echo "heure b =".$b."<br>";
        // date_default_timezone_set('Europe/Paris');
        // $timeParis = date('d-m-y H:i:s');
        // $timeParis2 = explode(" ",$timeParis);
        // echo $timeParis2[1]."<br>";
        // var_dump(count($countstatday));die("tapitra");
        //$countstatday = null;
        // if($countstatday == null || $countstatday = ""){
        if($nbr_internaut_oneline == null || $nbr_internaut_oneline = ""){    
            // $countstatday = 2;
            $nbr_internaut_oneline = 2;
            // $random_number = intval( "0" . rand(1,4) );
            $random_number = rand(1,4);
            // $countstatday += 1;
            $nbr_internaut_oneline += 1;
            // $counta = ($countstatday * $random_number);
             $counta = ($nbr_internaut_oneline * $random_number);
        }else{
            $countstatday = $this->User->get_all_statistiques_by_date($date);   
            $nbr_internaut_oneline=count($countstatday);  
            // foreach ($countstatday as $countstatdate){
            //     if($countstatdate->date == $date){
            //         if( "00:00:00" <= $time && "06:45:50" >= $time){
            //             $random_number = intval( "0" . rand(1,2) );
            //             $countal += 1;
            //             $counta = ($countal * $random_number);
            //             $isa=1;
            //         }
            //         else if( "06:45:50" <= $time && "08:13:28">= $time){
            //             $random_number = intval( "0" . rand(1,3) );
            //             $countal += 1;
            //             $counta = ($countal * $random_number);
            //             $isa=2;
            //         }else if("08:13:28"<= $time && "15:01:28">= $time){
            //             $random_number = intval( "0" . rand(1,4) );
            //             $countal += 1;
            //             $counta = ($countal * $random_number);
            //             $isa=3;
            //         }
            //         else if("15:01:28"<= $time && "19:01:28">= $time){
            //             $random_number = intval( "0" . rand(1,3) );
            //             $countal += 1;
            //             var_dump($countal);die("tapitra");
            //             $counta = ($countal * $random_number);
            //             $isa=4;
            //         }
            //         else if("19:01:28"<= $time && "22:01:28"<= $time){
            //             $random_number = intval( "0" . rand(1,2) );
            //             $countal += 1;

            //             $counta = ($countal * $random_number);
            //             $isa=5;
            //         }
            //         else if("22:01:28"<= $time && "23:59:01"<= $time){
            //             $random_number = intval( "0" . rand(1,2) );
            //             $countal += 1;
            //             $counta = ($countal * $random_number);
            //             $isa=6;
            //         }
            //     }
            // }
            //if( "00:00:00" <= $time && "06:45:50" >= $time){
            // "00:00:00" <= 4 <="06:45:50"
            date_default_timezone_set('Europe/Paris');
            $timeParis = date('d-m-y H:i:s');
            $timeParis2 = explode(" ",$timeParis);
            echo "heure Europe/Paris = ".$timeParis2[1]."<br>";
            if( "00:00:00" <= $timeParis2[1] && "06:45:50" >= $timeParis2[1]){
                // $random_number = rand(1,2);
                // $counta = ($nbr_internaut_oneline * $random_number);
                $counta = $nbr_internaut_oneline;
                echo "resultat truquer =".$counta;
                //$isa=1;
            }else if( "06:45:50" <= $timeParis2[1] && "12:00:00">= $timeParis2[1]){
                $random_number = rand(2,2);
                $counta = ($nbr_internaut_oneline * $random_number);
                echo "resultat truquer = ".$counta;
            }else if("12:01:00"<= $timeParis2[1] && "18:00:00">= $timeParis2[1]){
                $random_number = rand(2,2);
                $counta = ($nbr_internaut_oneline * $random_number);
                echo "resultat truquer =".$counta;
            }else if("18:01:28"<= $timeParis2[1] && "22:00:00">= $timeParis2[1]){
                // $random_number = rand(2,3);
                // $counta = ($nbr_internaut_oneline * $random_number);
                $counta = $nbr_internaut_oneline;
                echo "resultat truquer = ".$counta;
            }else if("22:01:00"<= $timeParis2[1] && "23:59:59"<= $time){
                $counta = $nbr_internaut_oneline;
                echo "resultat truquer = ".$counta;
            }
        }
        // if($counta >300){
        //     $random_number = intval( "0" . rand(1,7) . rand(0,9));
        //     $counta = $counta - (($counta / 2) - $random_number);
        //     $counta = explode('.',$counta);
        //     echo $counta[0];
        // }else{
        //     echo $counta;
        // }
            
            
    }
    
    
}