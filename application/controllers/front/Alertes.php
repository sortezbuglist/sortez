<?php
class alertes extends CI_Controller {
    
    function __construct() {
        parent::__construct();
		$this->load->library('session');

        $this->load->model("user") ;

        $this->load->library('ion_auth');


        check_vivresaville_id_ville();

    }
    
    function add($prmIdUser = "", $prmIdCommercant = "") {
        if($prmIdUser != "" && $prmIdUser != 0 && $prmIdCommercant != "" && $prmIdCommercant != 0) {
            $this->db->where("IdUser",$prmIdUser);
            $this->db->where("IdCommercant",$prmIdCommercant);
            $colAssUserCommercant = $this->db->get("ass_commercants_users");
            $objAssUserCommercant = $colAssUserCommercant->row_object();
            if(sizeof($objAssUserCommercant) > 0) {
                $objFavoris["IdAssCommercantUser"] = $objAssUserCommercant->IdAssCommercantUser;
                $objFavoris["Newsletter"] = 1;
                $this->load->model("user");
                $objFavoris = $this->user->UpdateFavoris($objFavoris);
            } else {
                $objFavoris["IdUser"] = $prmIdUser;
                $objFavoris["IdCommercant"] = $prmIdCommercant;
                $objFavoris["Newsletter"] = 1;
                $this->load->model("user");
                $objFavoris = $this->user->AddFavoris($objFavoris);
            }
            redirect("front/annonce/ficheCommercantAnnonce/".$prmIdCommercant);
        } else {
            $this->session->set_flashdata('domain_from', '1');
			redirect("connexion");
        }
    }
    
    
    function delete($prmIdUser = "", $prmIdCommercant = "") {
        if($prmIdUser != "" && $prmIdCommercant != "") {

            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            $UserComNewsletter = $this->user->verifUserComNewsletter($iduser ,$prmIdCommercant);
            
            $this->user->deleteUserComNewsletter($UserComNewsletter->IdAssCommercantUser);

            
            redirect("front/annonce/ficheCommercantAnnonce/".$prmIdCommercant);
            
        } 
    }
    
    
    
    
    
}