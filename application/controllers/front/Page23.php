<?php
class page23 extends CI_Controller {
    
    function __construct() {
        parent::__construct();
		$this->load->model("mdlcommercant") ;
    }
    
    function index($_iCommercantId=0){
		if($_iCommercantId == 0)
		 {
		   $this->load->view('front/vwPageFonctionnementPointCadeau') ;
		 }else{
			$oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
			$data['oInfoCommercant'] = $oInfoCommercant ;
			$this->load->view('front/vwPage23', $data) ;
			
		}
	}

   
}