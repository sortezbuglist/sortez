<?php
class particuliers extends CI_Controller {

    function __construct() {
        parent::__construct();
		$this->load->model("mdlville") ;
		$this->load->model("user") ;
		$this->load->library('session');
        $this->load->library('user_agent');
        $this->load->model("Abonnement");

        $this->load->model("mdlagenda_perso");
        $this->load->model("mdl_agenda");
        $this->load->model("Commercant");
        $this->load->model("mdlville");
        $this->load->model("mdl_categories_agenda");
        $this->load->helper('clubproximite');
        $this->load->Model("mdldepartement");
        $this->load->model("mdlcommercant");
        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");
        $this->load->helper('clubproximite');
        statistiques();

        check_vivresaville_id_ville();
    }
    
    function index(){
        if (!$this->ion_auth->logged_in()) {
            $this->inscription();
        } else {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            $this->inscription($iduser);
        }
    }
    
    function inscription($prmId = 0) {
        $group_proo_club = array(3,4,5);
        if ($prmId != 0 && ($this->ion_auth->in_group($group_proo_club))) {
            redirect("front/professionnels/fiche/".$prmId);
        } else if ($prmId != 0 && ($this->ion_auth->is_admin())) {
            redirect('admin/home', 'refresh');
        }

        if (!$this->ion_auth->logged_in() && $prmId != 0) {
            $this->session->set_userdata("cur_uri", $this->uri->uri_string());
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
        if ($this->ion_auth->logged_in() && ($prmId == 0 || $prmId == "") && (!$this->ion_auth->in_group($group_proo_club) && !$this->ion_auth->is_admin())) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $prmId = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
        }
        $data = null;
        $this->load->Model("Ville");
	    $this->load->Model("user");
        $this->load->Model("mdlville");
        
        $data["colVilles"] = $this->Ville->GetAll();
        ////$this->firephp->log($data["colVilles"], 'villes');
        $oParticulier = $this->user->GetById($prmId);
        ////$this->firephp->log($oParticulier, 'oParticulier');
        ////$this->firephp->log($prmId, 'prmId');
        $data["oParticulier"] = $oParticulier ;
        $data["prmId"] = $prmId ;
        
        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $data['mdlville'] = $this->mdlville;
        $data["colDepartement"] = $this->mdldepartement->GetAll();
        
        
        //$data = null;
        //$this->load->view("front/vwInscriptionParticulier",$data);
        //$this->load->view("frontAout2013/inscriptionparticulier_main",$data);
        $this->load->view("privicarte/inscriptionparticulier_main",$data);
        
    }
    
    function ajouter() {

        $objUser = $this->input->post("Particulier");

        $from_publightbox = $this->input->post("from_publightbox");
        if (isset($from_publightbox) && $from_publightbox=="1") {
                $objUser['Login'] = $this->input->post("publightbox_mail");
            $objUser["IdUser"] = 0;
            $objUser['IdVille'] = 0;
        }


        if (!$objUser) {
            redirect("front/particuliers/inscription");
        }

        $objUser['Email'] = $objUser ['Login'];
        $particulier_Password = $this->input->post("Particulier_Password");
        $from_menuconsommateurs = $this->input->post("from_menuconsommateurs");


        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;


        $Rappeldevosinformations = "";
        $Rappeldevosinformations .= "<p>Rappel de vos informations</p>";
        $Rappeldevosinformations .= "<p>";
        $Rappeldevosinformations .= "Nom : ".$objUser['Nom']."<br/>";
        $Rappeldevosinformations .= "DateNaissance : ".$objUser['DateNaissance']."<br/>";
        if ($objUser['Civilite'] == "0") $Rappeldevosinformations .= "Civilite : Monsieur<br/>";
        if ($objUser['Civilite'] == "1") $Rappeldevosinformations .= "Civilite : Madame<br/>";
        if ($objUser['Civilite'] == "2") $Rappeldevosinformations .= "Civilite : Mademoiselle<br/>";
        //$Rappeldevosinformations .= "Profession : ".$objUser['Profession']."<br/>";
        $Rappeldevosinformations .= "Adresse : ".$objUser['Adresse']."<br/>";
        $obj_ville_ = $this->mdlville->getVilleById($objUser['IdVille']);
        if ($obj_ville_) $Rappeldevosinformations .= "Ville : ".$obj_ville_->Nom."<br/>";
        $Rappeldevosinformations .= "CodePostal : ".$objUser['CodePostal']."<br/>";
        $Rappeldevosinformations .= "Telephone : ".$objUser['Telephone']."<br/>";
        $Rappeldevosinformations .= "Mobile : ".$objUser['Portable']."<br/>";
        //$Rappeldevosinformations .= "Fax : ".$objUser['Fax']."<br/>";
        $Rappeldevosinformations .= "Email : ".$objUser['Email']."<br/>";
        $Rappeldevosinformations .= "Login : ".$objUser['Login']."<br/>";
        $Rappeldevosinformations .= "</p>";
        
        $this->load->Model("user");
        $objUser["IsActif"] = "1";

        if($objUser['DateNaissance']!="") $objUser['DateNaissance'] = convert_Frenchdate_to_Sqldate($objUser['DateNaissance']);
        else $objUser['DateNaissance'] = null;
        if ($objUser['DateNaissance'] == "--") $objUser['DateNaissance'] = date("Y-m-d");


        if ($objUser["IdUser"] == 0){
            $objUser["DateCreation"] = date("Y-m-d");

            //ion_auth register
            $username_ion_auth = $objUser['Login'];
            ////$this->firephp->log($username_ion_auth, 'username_ion_auth');
            $password_ion_auth = $particulier_Password;
            ////$this->firephp->log($particulier_Password, 'particulier_Password');
            $email_ion_auth = $objUser['Email'];
            ////$this->firephp->log($email_ion_auth, 'email_ion_auth');
            $additional_data_ion_auth = array('first_name' => $objUser['Nom'],'last_name' => $objUser['Prenom'], 'phone' => $objUser['Telephone']);
            $group_ion_auth = array('2'); // Sets user to particulier.
            $this->load->library('ion_auth');
            $user_part_ion = $this->ion_auth->register($username_ion_auth, $password_ion_auth, $email_ion_auth, $additional_data_ion_auth, $group_ion_auth);
            $ion_ath_error = $this->ion_auth->errors();
            $user_lastid_ionauth = $this->ion_auth_used_by_club->GetUserlast_id($objUser['Nom'], $username_ion_auth, $email_ion_auth);
            $objUser["user_ionauth_id"] = $user_lastid_ionauth;
            ////$this->firephp->log($user_lastid_ionauth, 'user_lastid_ionauth');
            //echo "qsf : ".$user_lastid_ionauth;
            //ion_auth register

            //var_dump($ion_ath_error); die();


            if (!$user_part_ion && isset($ion_ath_error) && $ion_ath_error!="") {
                $verifuserprofil = 2;
            } else {
                $objUser_id = $this->user->Insert($objUser);
                /***insertion a la table liste_login_import_com***/
                $log_mdp = array('login' => $objUser['Login'], 'password' => $objUser['password']);
                $insert=$this->mdlcommercant->insertion_liste_login_import($log_mdp);
                /************************************************/                
                $objUser=$this->user->GetById($objUser_id);
                $id_ion_auth=$this->user->getbyids($objUser_id);
                $verifuserprofil = 0;
                if (isset($id_ion_auth->user_ionauth_id) AND $id_ion_auth->user_ionauth_id !=''  AND $id_ion_auth->user_ionauth_id!=null){
                    //var_dump($id_ion_auth->user_ionauth_id);die();
                    generate_client_card($id_ion_auth->user_ionauth_id);
                        }
            }
        } else {

            //ion_auth_update
            $user_ion_auth = $this->ion_auth->user()->row();
            $data_ion_auth_update = array(
                'first_name' => $objUser['Nom'],
                'last_name' => $objUser['Prenom']
            );
            $this->ion_auth->update($user_ion_auth->id, $data_ion_auth_update);
            $ion_ath_error = $this->ion_auth->errors();
            //ion_auth_update
            //var_dump($ion_ath_error);die();
            if (isset($ion_ath_error) && $ion_ath_error!="") {
                $verifuserprofil = 2;
            } else {
                $objUser = $this->user->Update($objUser);
                $verifuserprofil = 1;
            }

        }


        if (isset($from_menuconsommateurs) && $from_menuconsommateurs == "1") redirect('front/utilisateur/menuconsommateurs/?_msg_menuconsommateurs=1');
        
        if (!is_object($objUser)) $objUser = (object)$objUser;


        $this->load->model("parametre");
        
        $objParametreContenuMail = $this->parametre->getByCode("ContenuMailConfirmationInscriptionUser");
        $objParametreContenuPage = $this->parametre->getByCode("ContenuPageConfirmationInscriptionUser");
        $arrDest = array();
        $arrDest[] = array("Email" => $objUser->Email ?? '', "Name" => $objUser->Prenom ?? '' . " " .  $objUser->Nom ?? '');

        $colDest[] = array("Email"=>$this->config->item("mail_sender_adress"),"Name"=>$this->config->item("mail_sender_name"));
        $colSujet = "Notification inscription Consommateur";
        $nom__ = $objUser->Nom;
        $Prenom__ = $objUser->Prenom;
        $Email__ = $objUser->Email;
        $colContenu = '<div>
            <p>Bonjour,<br/>Ceci est une notification d\'inscription de nouveau consommateur sur Sortez.org</p>
            <p>Nom: '.$nom__.'<br/>Prenom: '.$Prenom__.'<br/>Email: '.$Email__.'</p>
        </div>';
        
        $txtSujet = "Confirmation inscription";
        $txtContenu = $objParametreContenuMail[0]->Contenu;
        $txtContenu .= $Rappeldevosinformations;

        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
        } else $iduser = 0;

        if ($verifuserprofil == 0) {

            @envoi_notification($arrDest,$txtSujet,$txtContenu);
            @envoi_notification($colDest,$colSujet,$colContenu);
            $data["txtContenu"] = $objParametreContenuPage[0]->Contenu;

        } else if($verifuserprofil == 2){

            $data["txtContenu"] = "Une erreur s'est produite durant l'enregistrement de vos données. Veuillez recommencer s'il vous plait.".$ion_ath_error;
            if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) $this->load->view("front2013/vwErreurInscriptionPro_mobile", $data);
            else $this->load->view("front/vwErreurInscriptionPro", $data);

        } else {
            //$data["txtContenu"] = "Votre profil a été mis à jour avec succès";
            if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
                redirect('front/mobile/mon_compte') ;
            } else {
                redirect("front/particuliers/inscription/".$iduser);
            }
        }

        $data["no_main_menu_home"] = "1";
        
        
        $this->load->view("privicarte/vwConfirmationInscriptionParticulier",$data);


    }

// function test_mdp(){
//     $objUser = $this->input->post("Particulier");
//     $log_mdp = array('login' => $objUser['Login'], 'password' => $objUser['password']);
//     $insert=$this->mdlcommercant->insertion_liste_login_import($log_mdp);
// //         // var_dump($objCommercant['password']);    
// }

    function fonctionnement() {
        $this->load->view("front/vwFonctionnementParticulier");
    }
	 function fiche($prmIdCommercant = 0) {
        $data = null;
        if( !$this->ion_auth->logged_in()) {
            $this->session->set_userdata("cur_uri", $this->uri->uri_string());
            $this->session->set_flashdata('domain_from', '1');
			redirect("connexion");
        }
        $this->load->Model("Commercant");
        $this->load->Model("Rubrique");
        $this->load->Model("SousRubrique");
        $this->load->Model("Ville");
        $this->load->Model("Abonnement");
        $this->load->Model("AssCommercantRubrique");
        $this->load->Model("AssCommercantSousRubrique");
        $this->load->Model("AssCommercantAbonnement");     
        
        $data["colVilles"] = $this->Ville->GetAll(); 
        $oParticulier = $this->user->GetById($prmIdCommercant);		
        $data["oParticulier"] = $oParticulier ;
        $this->load->view("front/vwInscriptionParticulier",$data);
    }
	function getPostalCode($idVille=0){
	    $oville = $this->mdlville->getVilleById($idVille);
		$data["cp"] =$oville->CodePostal;
		 
        $data['idVille']=$idVille;
		
        $this->load->view("front/vwReponseVilleParticulier", $data);
	 
	}

    function getPostalCodeadmin($idVille=0){
        $oville = $this->mdlville->getVilleById($idVille);
        $data["cp"] =$oville->CodePostal;

        $data['idVille']=$idVille;

        //$this->load->view("front/vwReponseVilleParticulier", $data);

        echo $oville->CodePostal;

    }
        
    function verifier_email(){
        $txtEmail = $this->input->post("txtEmail_var");
        $oEmail = $this->user->verifier_mailUser($txtEmail);
        $emailExist = count($oEmail);
        $reponse = "0";
        $data['reponse']="0";
        if ($emailExist != "0") {
            $reponse = "1";
            //$data['reponse']="1";
            echo $reponse;
        } else {
            $reponse = "0";
            //$data['reponse']="0";
            echo $reponse;
        }

        //$this->load->view("front/vwReponseInscriptionParticulier", $data);
        /*echo '<span style="color:#F00; font-weight:bold;">Ce mail est d&eacute;j&agrave; utilis&eacute;</span>';
        else echo '<span style="color:#3C0; font-weight:bold;">Ce mail est disponible</span>';*/
    }


    function verifier_login(){
        $txtLogin = $this->input->post("txtLogin_var");
        $oEmail = $this->user->verifier_loginUser($txtLogin);
        $emailExist = count($oEmail);
        $reponse = "0";
        $data['reponse']="0";
        if ($emailExist != "0") {
            $reponse = "1";
            //$data['reponse']="1";
            echo $reponse;
        } else {
            $reponse = "0";
            //$data['reponse']="0";
            echo $reponse;
        }

        //$this->load->view("front/vwReponseInscriptionParticulier", $data);
        /*echo '<span style="color:#F00; font-weight:bold;">Ce mail est d&eacute;j&agrave; utilis&eacute;</span>';
        else echo '<span style="color:#3C0; font-weight:bold;">Ce mail est disponible</span>';*/
    }


    function mon_agenda(){

        $data["current_page"] = "subscription_pro";//this is to differentiate js to use

        if (!$this->ion_auth->logged_in()){
            redirect("auth/login");
        } else {

            if (!$this->ion_auth->in_group(2)) {
                redirect('front/utilisateur/contenupro');
            } else {

                $user_ion_auth = $this->ion_auth->user()->row();
                $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

                $ObjAgenda_perso_list = $this->mdlagenda_perso->GetAgenda_agenda_persoByUser("0", $user_ion_auth->id);


                if (is_object($ObjAgenda_perso_list)) $tiDepartement = $ObjAgenda_perso_list->idCommune;
                if(isset($tiDepartement) && $tiDepartement!="") {
                    $tiDepartement = substr_replace($tiDepartement ,"",-1);
                    $tiDepartement_array = explode("_", $tiDepartement);
                    $data['tiDepartement_array'] = $tiDepartement;
                } else {
                    $tiDepartement_array = "0";
                }
                if (is_object($ObjAgenda_perso_list)) $tiDeposant = $ObjAgenda_perso_list->idDeposant;
                if(isset($tiDeposant) && $tiDeposant!="") {
                    $tiDeposant = substr_replace($tiDeposant ,"",-1);
                    $tiDeposant_array = explode("_", $tiDeposant);
                    $data['tiDeposant_array'] = $tiDeposant;
                }
                if (is_object($ObjAgenda_perso_list)) $tiCategorie = $ObjAgenda_perso_list->idCategorie;
                if(isset($tiCategorie) && $tiCategorie!="") {
                    $tiCategorie = substr_replace($tiCategorie ,"",-1);
                    $tiCategorie_array = explode("_", $tiCategorie);
                    //$data['tiCategorie_array'] = $tiCategorie_array;
                } else {
                    $tiCategorie_array = "0";
                }


                if (isset($tiDeposant_array)){
                    $idCommercant_to_search = $tiCategorie_array;
                } else {
                    $idCommercant_to_search = "0";
                }


                $toAgenda = $this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array,0 , $tiDepartement_array, "", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0");
                $data['toAgenda'] = $toAgenda;

                //die("STOP");

                $data['toAgndaTout_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array,0 , $tiDepartement_array,"", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaAujourdhui_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array,0 , $tiDepartement_array,"", 0, 0, 10000, "", "101", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaWeekend_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array,0 , $tiDepartement_array,"", 0, 0, 10000, "", "202", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaSemaine_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array,0 , $tiDepartement_array,"", 0, 0, 10000, "", "303", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaSemproch_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array,0 , $tiDepartement_array,"", 0, 0, 10000, "", "404", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaMois_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array,0 , $tiDepartement_array,"", 0, 0, 10000, "", "505", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));

                $data['toAgndaJanvier_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array,0 , $tiDepartement_array,"", 0, 0, 10000, "", "01", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaFevrier_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array,0 , $tiDepartement_array,"", 0, 0, 10000, "", "02", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaMars_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array,0 , $tiDepartement_array,"", 0, 0, 10000, "", "03", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaAvril_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array,0 , $tiDepartement_array,"", 0, 0, 10000, "", "04", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaMai_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array,0 , $tiDepartement_array,"", 0, 0, 10000, "", "05", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaJuin_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array,0 , $tiDepartement_array,"", 0, 0, 10000, "", "06", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaJuillet_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array,0 , $tiDepartement_array,"", 0, 0, 10000, "", "07", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaAout_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array,0 , $tiDepartement_array,"", 0, 0, 10000, "", "08", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaSept_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array,0 , $tiDepartement_array,"", 0, 0, 10000, "", "09", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaOct_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array,0 , $tiDepartement_array,"", 0, 0, 10000, "", "10", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaNov_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array,0 , $tiDepartement_array,"", 0, 0, 10000, "", "11", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaDec_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array,0 , $tiDepartement_array,"", 0, 0, 10000, "", "12", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));

                $tiCategorie_array = $this->mdl_agenda->listeAgendaRecherche_agenda_perso_liste_categ($tiCategorie_array,0 , $tiDepartement_array, "", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0");
                $data['tiCategorie_array'] = $tiCategorie_array;





                $is_mobile = $this->agent->is_mobile();
                //test ipad user agent
                $is_mobile_ipad = $this->agent->is_mobile('ipad');
                $data['is_mobile_ipad'] = $is_mobile_ipad;
                $is_robot = $this->agent->is_robot();
                $is_browser = $this->agent->is_browser();
                $is_platform = $this->agent->platform();
                $data['is_mobile'] = $is_mobile;
                $data['is_robot'] = $is_robot;
                $data['is_browser'] = $is_browser;
                $data['is_platform'] = $is_platform;


                if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
                    $this->load->view('mobile2013/mon_agenda_particulier', $data) ;
                } else {
                    $this->load->view('agendaAout2013/mon_agenda_particulier', $data) ;
                }


            }

        }

    }

    function mon_agenda_test(){

        $data["current_page"] = "subscription_pro";//this is to differentiate js to use

        if (!$this->ion_auth->logged_in()){
            redirect("auth/login");
        } else {

            if (!$this->ion_auth->in_group(2)) {
                redirect('front/utilisateur/contenupro');
            } else {

                $user_ion_auth = $this->ion_auth->user()->row();
                $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

                $ObjAgenda_perso_list = $this->mdlagenda_perso->GetAgenda_agenda_persoByUser("0", $user_ion_auth->id);


                $tiDepartement = $ObjAgenda_perso_list->idCommune;
                if(isset($tiDepartement) && $tiDepartement!="") {
                    $tiDepartement = substr_replace($tiDepartement ,"",-1);
                    $tiDepartement_array = explode("_", $tiDepartement);
                    $data['tiDepartement_array'] = $tiDepartement_array;
                } else {
                    $tiDepartement_array = "0";
                }
                $tiDeposant = $ObjAgenda_perso_list->idDeposant;
                if(isset($tiDeposant) && $tiDeposant!="") {
                    $tiDeposant = substr_replace($tiDeposant ,"",-1);
                    $tiDeposant_array = explode("_", $tiDeposant);
                    $data['tiDeposant_array'] = $tiDeposant_array;
                }
                $tiCategorie = $ObjAgenda_perso_list->idCategorie;
                if(isset($tiCategorie) && $tiCategorie!="") {
                    $tiCategorie = substr_replace($tiCategorie ,"",-1);
                    $tiCategorie_array = explode("_", $tiCategorie);
                    //$data['tiCategorie_array'] = $tiCategorie_array;
                } else {
                    $tiCategorie_array = "0";
                }


                if (isset($tiDeposant_array)){
                    $idCommercant_to_search = $tiCategorie_array;
                } else {
                    $idCommercant_to_search = "0";
                }


                $toAgenda = $this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, "", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0");
                $data['toAgenda'] = $toAgenda;

                /*
                $data['toAgndaTout_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array,"", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaAujourdhui_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array,"", 0, 0, 10000, "", "101", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaWeekend_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array,"", 0, 0, 10000, "", "202", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaSemaine_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array,"", 0, 0, 10000, "", "303", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaSemproch_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array,"", 0, 0, 10000, "", "404", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaMois_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array,"", 0, 0, 10000, "", "505", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));

                $data['toAgndaJanvier_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array,"", 0, 0, 10000, "", "01", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaFevrier_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array,"", 0, 0, 10000, "", "02", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaMars_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array,"", 0, 0, 10000, "", "03", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaAvril_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array,"", 0, 0, 10000, "", "04", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaMai_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array,"", 0, 0, 10000, "", "05", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaJuin_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array,"", 0, 0, 10000, "", "06", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaJuillet_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array,"", 0, 0, 10000, "", "07", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaAout_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array,"", 0, 0, 10000, "", "08", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaSept_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array,"", 0, 0, 10000, "", "09", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaOct_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array,"", 0, 0, 10000, "", "10", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaNov_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array,"", 0, 0, 10000, "", "11", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                $data['toAgndaDec_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array,"", 0, 0, 10000, "", "12", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0"));
                */

                $tiCategorie_array = $this->mdl_agenda->listeAgendaRecherche_agenda_perso_liste_categ($tiCategorie_array, $tiDepartement_array, "", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0");
                $data['tiCategorie_array'] = $tiCategorie_array;


                //$this->load->view('agendaAout2013/mon_agenda_particulier', $data) ;
                echo "ok";

            }

        }

    }


    public function ajouter_via_pro(){
        $this->load->helper('string');
        $objUser = $this->input->post("Particulier");
        if( $this->ion_auth->logged_in() && !empty($objUser) && !$this->ion_auth->username_check($objUser['Login'])) {
            try {
                $randompwd = random_string('alnum', 10);
                
                $objUser['DateNaissance'] = convert_Frenchdate_to_Sqldate($objUser['DateNaissance']);
                $login = $objUser['Login'].'@privicarte.fr';
                $objUser['Email'] =  $login;
                
                $username_ion_auth = $objUser['Email'];
                $password_ion_auth = $randompwd;
                $email_ion_auth = $objUser['Email'];
                $additional_data_ion_auth = array('first_name' => $objUser['Nom'],'last_name' => $objUser['Prenom'], 'phone' => $objUser['Telephone']);
                $group_ion_auth = array('2'); // Sets user to particulier.
                $this->load->library('ion_auth');
                $this->ion_auth->register($username_ion_auth, $password_ion_auth, $email_ion_auth, $additional_data_ion_auth, $group_ion_auth);
                $ion_ath_error = $this->ion_auth->errors();
                $user_lastid_ionauth = $this->ion_auth_used_by_club->GetUserlast_id($objUser['Nom'], $username_ion_auth, $email_ion_auth);
                $objUser["user_ionauth_id"] = $user_lastid_ionauth;
                $objUser_id = $this->user->Insert($objUser);
                $id_ion_auth=$this->user->getbyids($objUser_id);
                $objUser=$this->user->GetById($objUser_id);
                if (isset($id_ion_auth->user_ionauth_id) AND $id_ion_auth->user_ionauth_id !=''  AND $id_ion_auth->user_ionauth_id!=null){
                    //var_dump($id_ion_auth->user_ionauth_id);die();
                    generate_client_card($id_ion_auth->user_ionauth_id);
                }
                /*
                $mail_expediteur = $this->config->item("mail_sender_adress") ;
                $mail_name_sender = $this->config->item("mail_sender_name") ;
                 
                $body = "Le compte suivant a été créé : <br /><br />";
                $body .= "Login : <b> ".$login." </b><br />";
                $body .= "Mot de passe : <b>".$randompwd."</b>";
                $body .= "<br /><br />Merci";
                 
                 
                $this->email->from($mail_expediteur, $mail_name_sender);
                $this->email->to('hagamalala@yahoo.fr');
                 
                $this->email->subject("Delivrance d'une carte");
                $this->email->message($body);
                
                $this->email->send();
                */
                
                $data['login'] = $login;
                $data['pwd'] = $randompwd;
                $data['titre'] = 'Confirmation de la délivrance d’une carte';
                $this->load->view('mobile2014/pro/confirmaton-inscription-via-pro',$data);
            }catch (Exception $e) {
                    //alert the user then kill the process
                    redirect('front/fidelity_pro/delivrance_carte');
                }
        }else{
            redirect('front/fidelity_pro/delivrance_carte');
        }
    }


    function getListCityByCP($cp=null){
        if(!empty($cp)){
            //$cp = $this->input->post('cp');
            $data['oVilles'] = $this->mdlville->getVilleByCP($cp);
            $view_cp = $this->load->view('mobile2014/ajax/options_villes',$data,TRUE);
            
            $data['oVilles_departement'] = (!empty($data['oVilles'])) ? current($data['oVilles']) : false;
            $view_departement = $this->load->view('mobile2014/ajax/options_departement',$data,TRUE);
            
            echo json_encode(array('list'=>$view_cp,'list_departement'=>$view_departement));
        }
        //echo  (!empty($oville))  ? json_encode(array('ville_id'=>$oville->IdVille)) : json_encode(array('ville_id'=>0));
    }


    public function test_captcha(){
        $resp=$this->input->post("g-recaptcha-response");
        if (isset($resp) AND $resp != null && $resp !="") {
            $data["captcha"]="OK";
        }
        else {
            $data["captcha"]="NO";
        }
        $http_code = 200;
        header('Content-type: json');
        header('HTTP/1.1: ' . $http_code);
        header('Status: ' . $http_code);
        exit(json_encode((array)$data));
    }

        
}