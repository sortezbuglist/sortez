<?php

Class accueil extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("vivresaville_villes");
        $this->load->model("vsv_ville_other");
        $this->load->library('session');

        //check_vivresaville_id_ville();
    }

    function index()
    {
        $data['pagecategory'] = "vsv_home";
        /*$localdata_IdVille = $this->session->userdata('localdata_IdVille');
        if ($localdata_IdVille == false) $localdata_IdVille = null;
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        if ($localdata_IdVille_parent == false) $localdata_IdVille_parent = null;

        $vsv_other_ville_currenturl = $this->input->post('vsv_other_ville_currenturl');
        $vsv_other_ville_currenturl = str_replace(base_url(), "", $vsv_other_ville_currenturl);
        if ($vsv_other_ville_currenturl == "front/accueil" || $vsv_other_ville_currenturl == "front/ListVille")
            $vsv_other_ville_currenturl = null;

        $vsv_other_ville_select = $this->input->post('vsv_other_ville_select');
        if (isset($vsv_other_ville_select) && $vsv_other_ville_select != "" && $vsv_other_ville_select != false && $vsv_other_ville_select != "0") {
            $this->session->set_userdata('localdata_IdVille', $vsv_other_ville_select);
            $this->session->set_userdata('localdata_IdVille_all', null);
        } else if (isset($vsv_other_ville_select) && $vsv_other_ville_select == "0") {
            $this->session->set_userdata('localdata_IdVille', $localdata_IdVille_parent);
            $localdata_IdVsv = $this->session->userdata('localdata_IdVsv');
            if ($localdata_IdVsv == false) $localdata_IdVsv = null;
            $localdata_IdVille_all = array();
            $localdata_IdVille_all[] = $localdata_IdVille_parent;
            $vsv_ville_other_data = $this->vsv_ville_other->getByIdVsv($localdata_IdVsv);
            if (isset($vsv_ville_other_data) && count($vsv_ville_other_data) > 0) {
                foreach ($vsv_ville_other_data as $vsv_ville_other_datum) {
                    $localdata_IdVille_all[] = $vsv_ville_other_datum->id_ville;
                }
            }
            $this->session->set_userdata('localdata_IdVille_all', $localdata_IdVille_all);
        }

        //check_vivresaville_id_ville();//updating database session

        if (isset($localdata_IdVille_parent) && $localdata_IdVille_parent != "0" && $localdata_IdVille_parent != "" && $localdata_IdVille_parent != false) {
            $vsv_object = $this->vivresaville_villes->getByIdVille($localdata_IdVille_parent);
            if (isset($vsv_object) && isset($vsv_object->id_ville)) {
                $data['oville'] = $vsv_object;
                $data['main_menu_content'] = 'home';
                if (isset($vsv_other_ville_currenturl) && $vsv_other_ville_currenturl != "" && $vsv_other_ville_currenturl != false)
                    redirect($vsv_other_ville_currenturl);
                else $this->load->view('sortez_vsv/accueil_index', $data);
            } else {
                redirect("vivresaville/ListVille");
            }
        } else redirect("vivresaville/ListVille");*/

        $this->load->view('sortez_vsv/accueil_index', $data);
    }

    function set_width_device()
    {
        $main_width_device = $this->input->post('main_width_device');
        if (isset($main_width_device) && $main_width_device != "" && $main_width_device != "0" && is_numeric($main_width_device)) {
            $this->session->set_userdata('main_width_device', $main_width_device);
        } else {
            $this->session->unset_userdata('main_width_device');
        }
        return true;
    }

    function other_ville()
    {
        $this->session->unset_userdata('localdata_IdVille');
        $this->session->unset_userdata('localdata_IdVille_parent');
        redirect("sortez_vsv/ListVille");
    }

}

?>