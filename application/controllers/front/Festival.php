<?php

class festival extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model("user");
        $this->load->library('user_agent');
        $this->load->model("sousRubrique");
        $this->load->model("Rubrique");
        $this->load->model("AssCommercantAbonnement");
        $this->load->model("AssCommercantSousRubrique");
        $this->load->model("Abonnement");
        $this->load->model("mdlannonce");
        $this->load->model("mdlbonplan");
        $this->load->model("Abonnement");
        $this->load->model("mdlfestival");
        $this->load->model("mdlville");
        $this->load->model("mdldepartement");
        $this->load->model("mdlcommune");
        $this->load->model("mdl_localisation");
        $this->load->model("mdlarticle");
        $this->load->model("Commercant");
        $this->load->model("mdl_agenda");

        $this->load->model("mdl_types_article");
        $this->load->model("mdl_categories_article");
        $this->load->model("mdl_categories_agenda");
        $this->load->model("mdl_agenda_datetime");

        $this->load->model("mdl_festival_datetime");
        $this->load->model("mdl_festival_redacteur");
        $this->load->model("mdl_festival_organiser");
        $this->load->model("mdl_festival_validation_email");
         $this->load->model("mdl_festival_agenda_ref");
         $this->load->model("mdlfestival_perso");
         $this->load->model("mdlcommercant");




        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");

        $group_proo_club = array(3, 4, 5);//only customer & basic user cannot add article
        if (!$this->ion_auth->logged_in()) {
            redirect("auth/login");
        } else if (!$this->ion_auth->in_group($group_proo_club)) {
            $this->session->set_flashdata('domain_from', '1');
            redirect();
        }

        check_vivresaville_id_ville();

    }

    function index()
    {
        $this->liste();
    }


    function liste()
    {

$group_proo_club = array(3, 4, 5);//commercant only
if (!$this->ion_auth->logged_in()) {
$this->session->set_flashdata('domain_from', '1');
redirect("connexion");
} else if (!$this->ion_auth->in_group($group_proo_club)) {
    redirect("front/utilisateur/no_permission");
} else {
    $user_ion_auth = $this->ion_auth->user()->row();
    $prmIdCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

    $toListeFestival = $this->mdlfestival->GetByIdCommercant_no_filter_ville($prmIdCommercant);

    $objCommercant = $this->Commercant->GetById($prmIdCommercant);
    /* if (isset($objCommercant->limit_festival) && intval($objCommercant->limit_festival) != 0 && $objCommercant->limit_festival != "" && count($toListeFestival) > intval($objCommercant->limit_festival)) {
         $data['limit_festival_add'] = 1;
     } else {
         $data['limit_festival_add'] = 0;
     }*/

    $data['toListeFestival'] = $toListeFestival;
    $data['objCommercant'] = $objCommercant;
    $data['idCommercant'] = $prmIdCommercant;
    $data['pagecategory'] = "admin_commercant";
    // $data['limit_festival_value'] =$objCommercant->limit_festival;
    $data['count_toListeFestival_total'] = count($toListeFestival);
    $this->load->view("sortez/mes_festivals", $data);

}
}

    function fiche($prmIdCommercant = 0, $IdFestival = 0, $mssg = 0)
    {
        //var_dump($prmIdCommercant); die();
        if ($this->ion_auth->logged_in() && ($this->ion_auth->in_group(2))) {
            redirect("front/particuliers/inscription/");
        } else if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
            redirect('admin/home', 'refresh');
        } else if (!$this->ion_auth->logged_in()) {
            $this->session->set_userdata("cur_uri", $this->uri->uri_string());
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }

        // check article limit number
        if ($prmIdCommercant != 0 && $IdFestival == 0) {
            $toListeMesFestival__ = $this->mdlfestival->GetByIdCommercant($prmIdCommercant);
            $objCommercant__ = $this->Commercant->GetById($prmIdCommercant);
            if (isset($objCommercant__->limit_festival) && intval($objCommercant__->limit_festival) != 0 && $objCommercant__->limit_festival != "" && count($toListeMesFestival__) > intval($objCommercant__->limit_festival)) {
                redirect("front/festival/liste/" . $prmIdCommercant);
            }
        }

        if ($this->ion_auth->logged_in() && ($prmIdCommercant == 0 || $prmIdCommercant == "")) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $prmIdCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        }

        //verify if $IdArticle, $prmIdCommercant and $user_ion_auth->id are corrects and match between them
        if ($IdFestival != 0 && $IdFestival != "" && $IdFestival != null && is_numeric($IdFestival) == true) {
            $objArticle_verification = $this->mdlfestival->GetById($IdFestival);
            if (isset($objArticle_verification) && count($objArticle_verification) != 0) {
                if ($objArticle_verification->IdCommercant != $prmIdCommercant && $objArticle_verification->IdUsers_ionauth != $user_ion_auth->id) {
                    redirect("front/utilisateur/no_permission");
                }
            } else redirect("front/utilisateur/no_permission");
        }

        $data = null;

        //start verify if merchant subscription is right WILLIAM
        $_iDCommercant = $prmIdCommercant;
        $current_date = date("Y-m-d");
        $query_abcom_cond = " IdCommercant = " . $_iDCommercant . " AND ('" . $current_date . "' BETWEEN DateDebut AND DateFin)";
        $toAbonnementCommercant = $this->AssCommercantAbonnement->GetWhere($query_abcom_cond, 0, 1);
        if (sizeof($toAbonnementCommercant)) {
            foreach ($toAbonnementCommercant as $oAbonnementCommercant) {
                if (isset($oAbonnementCommercant) && ($oAbonnementCommercant->IdAbonnement == 1 || $oAbonnementCommercant->IdAbonnement == 2)) {//IdAbonnement doesn't contain annonce
                    //redirect ("front/professionnels/fiche/$_iDCommercant") ;
                    $data['show_annonce_btn'] = 0;
                } else $data['show_annonce_btn'] = 1;

                if (isset($oAbonnementCommercant) && ($oAbonnementCommercant->IdAbonnement == 1 || $oAbonnementCommercant->IdAbonnement == 3)) {//IdAbonnement doesn't contain bonplan
                    //redirect ("front/professionnels/fiche/$_iDCommercant") ;
                    $data['show_bonplan_btn'] = 0;
                } else $data['show_bonplan_btn'] = 1;
            }
        } else {
            $data['show_annonce_btn'] = 0;
            $data['show_bonplan_btn'] = 0;
            //$this->load->view("front/vwFicheProfessionnelError",$data);
            redirect("front/professionnels/ficheError/");
            //exit();
        }
        //end verify if merchant subscription is right WILLIAM

        $user_ion_auth = $this->ion_auth->user()->row();
        $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $data["userId"] = $commercant_UserId;

        $this->load->Model("Commercant");
        $this->load->Model("Rubrique");
        $this->load->Model("SousRubrique");
        $this->load->Model("Ville");
        $this->load->Model("Abonnement");
        $this->load->Model("AssCommercantRubrique");
        $this->load->Model("AssCommercantSousRubrique");
        $this->load->Model("AssCommercantAbonnement");

        $data["no_main_menu_home"] = "1";
        $data["no_left_menu_home"] = "1";


        $data["colVilles"] = $this->mdlville->GetAll();

        $data["objArticle"] = $this->mdlfestival->GetById($IdFestival);
        $data["objCommercant"] = $this->Commercant->GetById($commercant_UserId);

        $this->load->helper('ckeditor');

        //Ckeditor's configuration
        $data['ckeditor0'] = array(

            //ID of the textarea that will be replaced
            'id' => 'description',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor'),
                    array('Image')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );
//// OP END
        $data['ckeditor1'] = array(

            //ID of the textarea that will be replaced
            'id' => 'description_tarif',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor'),
                    array('Image')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor2'] = array(

            //ID of the textarea that will be replaced
            'id' => 'conditions_promo',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor'),
                    array('Image')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['home_link_club'] = 1;
        $data['mssg'] = $mssg;

        $data['jquery_to_use'] = "1.4.2";

       // $data["colTypeagenda"] = $this->mdl_types_article->GetAll();
       // $data["colCategorie"] = $this->mdl_categories_article->GetAll();
        //$data["colSousCategorie"] = $this->mdl_categories_article->GetAllSousrubrique();

        // article use agenda's categories and subcategories **********************************
        $data["colCategorie"] = $this->mdl_categories_agenda->GetAll();
        $data["colSousCategorie"] = $this->mdl_categories_agenda->GetAllSousrubrique();


        $data["colDepartement"] = $this->mdldepartement->GetAll();
        $data['mdldepartement'] = $this->mdldepartement;
        $data["colCommune"] = $this->mdlcommune->GetAll();
        $data['mdlcommune'] = $this->mdlcommune;

        $data['toListBonplan'] = $this->mdlbonplan->getListeBonPlan($prmIdCommercant);

        $objAbonnementCommercant = $this->Abonnement->GetLastAbonnementCommercant($prmIdCommercant);
        $data['objAbonnementCommercant'] = $objAbonnementCommercant;

        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($prmIdCommercant);
        if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
        $data["user_groups"] = $user_groups[0];

        $data['defaul_thumb_width'] = $this->config->item('defaul_thumb_width');
        $data['defaul_thumb_height'] = $this->config->item('defaul_thumb_height');

        $data['mdlville'] = $this->mdlville;

        $data["colDepartement"] = $this->mdldepartement->GetAll();
        $data['mdldepartement'] = $this->mdldepartement;
       //devoir
       $data["col_festival_redacteur"] = $this->mdl_festival_redacteur->getByIdCommercant($prmIdCommercant);

        $data["col_festival_organiser"] = $this->mdl_festival_organiser->getByIdCommercant($prmIdCommercant);

        $data["col_festival_location"] = $this->mdl_localisation->getByIdCommercant($prmIdCommercant);

         $data["col_festival_validation_email"] = $this->mdl_festival_validation_email->getByIdCommercant($prmIdCommercant);

        $data['obonplan_IdComemrcant'] = $this->mdlbonplan->getListeBonPlan($prmIdCommercant);

        $data['festival_datetime_list'] = $this->mdl_festival_datetime->getById($IdFestival);

        // cheking referencement agenda
        $verif_festival_agenda_ref = $this->mdl_festival_agenda_ref->getById($IdFestival);
        $data["verif_festival_agenda_ref"] = $verif_festival_agenda_ref;
        if (isset($verif_festival_agenda_ref->agenda_id) && $verif_festival_agenda_ref->agenda_id != null && $verif_festival_agenda_ref->agenda_id != '0') {
            # code...
            $verif_obj_agenda = $this->mdl_agenda->GetById($verif_festival_agenda_ref->agenda_id);
            $data["verif_obj_agenda"] = $verif_obj_agenda;
            if (isset($verif_obj_agenda) && is_object($verif_obj_agenda) && $verif_obj_agenda->IsActif != "3" && $verif_obj_agenda->IsActif != 3) {
                $data["existsfestival_agenda_ref"] = "1";
            }
        }
        $data['pagecategory'] = "admin_commercant";

        $this->load->view("sortez/vwFicheFestival", $data);


    }

    function save_temp_photo()
    {

        //print_r($_FILES);
        $fileName = $_FILES['FestivalPhoto1']['name'];
        $fileType = $_FILES['FestivalPhoto1']['type'];
        $fileError = $_FILES['FestivalPhoto1']['error'];
        $fileContent = file_get_contents($_FILES['FestivalPhoto1']['tmp_name']);

        if ($fileError == UPLOAD_ERR_OK) {
            //Processes your file here
            if (isset($_FILES["FestivalPhoto1"]["name"])) $photo1 = $_FILES["FestivalPhoto1"]["name"];
            if (isset($photo1) && $photo1 != "") {
                $photo1Associe = $photo1;
                $FestivalPhoto1 = doUploadResize("FestivalPhoto1", "application/resources/front/images/article/photoCommercant/", $photo1Associe);
            } else $ArticlePhoto1 = "Erreur";
            echo base_url() . "application/resources/front/images/article/photoCommercant/" . $FestivalPhoto1;
        } else {
            switch ($fileError) {
                case UPLOAD_ERR_INI_SIZE:
                    $message = 'Erreur lorsque vous essayez de télécharger un fichier qui dépasse la taille autorisée.';
                    break;
                case UPLOAD_ERR_FORM_SIZE:
                    $message = 'Erreur lorsque vous essayez de télécharger un fichier qui dépasse la taille autorisée.';
                    break;
                case UPLOAD_ERR_PARTIAL:
                    $message = 'Erreur: aucune action terminée télécharger le fichier.';
                    break;
                case UPLOAD_ERR_NO_FILE:
                    $message = 'Erreur: Aucun fichier a été téléchargé.';
                    break;
                case UPLOAD_ERR_NO_TMP_DIR:
                    $message = 'Erreur: serveur non configuré pour le téléchargement de fichiers.';
                    break;
                case UPLOAD_ERR_CANT_WRITE:
                    $message = 'Erreur: échec possible d\'enregistrer le fichier.';
                    break;
                case  UPLOAD_ERR_EXTENSION:
                    $message = 'Erreur: fichier non terminé le chargement.';
                    break;
                default:
                    $message = 'Erreur: fichier non terminé le chargement.';
                    break;
            }
            echo json_encode(array(
                'error' => true,
                'message' => $message
            ));
        }


    }

    function save_definite_photo()
    {
        $newURL = $this->input->post("newURL");
        $localURL = $this->input->post("localURL");
        copy($newURL, $localURL);
        echo 'OK';
    }

    function save()
    {
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_userdata("cur_uri", $this->uri->uri_string());
            redirect();
        }
        $objArticle_brute = $this->input->post("Article");

        $objArticle=remove_2500($objArticle_brute);

        $user_ion_auth = $this->ion_auth->user()->row();
        $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $objArticle["IdCommercant"] = $commercant_UserId;
        $objArticle["IdUsers_ionauth"] = $user_ion_auth->id;


        $base_path_system = str_replace('system/', '', BASEPATH);
        $this->load->library('image_moo');

        if (isset($_FILES["ArticlePdf"]["name"])) $pdf = $_FILES["ArticlePdf"]["name"];
        if (isset($pdf) && $pdf != "") {
            $pdfAssocie = $pdf;
            $pdfCom = doUpload("ArticlePdf", "application/resources/front/images/article/pdf/", $pdfAssocie);
        } else {
            $pdfAssocie = $this->input->post("pdfAssocie");
            $pdfCom = $pdfAssocie;
        }


        $objArticle["pdf"] = $pdfCom;
        $objArticle["photo1"] = $this->input->post("photo1Associe");
        $objArticle["photo2"] = $this->input->post("photo2Associe");
        $objArticle["photo3"] = $this->input->post("photo3Associe");
        $objArticle["photo4"] = $this->input->post("photo4Associe");
        $objArticle["photo5"] = $this->input->post("photo5Associe");

        $objArticle["last_update"] = date("Y-m-d");

        $objArticle["date_depot"] = convert_Frenchdate_to_Sqldate($objArticle["date_depot"]);
 if ($objArticle["date_debut"]==!0){      $objArticle["date_debut"] = convert_Frenchdate_to_Sqldate($objArticle["date_debut"]);}else {$objArticle["date_debut"]=null;}
        if ($objArticle["date_fin"]==!0){   $objArticle["date_fin"] = convert_Frenchdate_to_Sqldate($objArticle["date_fin"]);}else {$objArticle["date_fin"]=null;}


        if ($objArticle["id"] == "" || $objArticle["id"] == null || $objArticle["id"] == "0") {
            $IdUpdatedArticle = $this->mdlfestival->insert($objArticle);
        } else {
            $IdUpdatedArticle = $this->mdlfestival->update($objArticle);
        }

        /*SAVING ALL DATETIME*/


        // article agenda referencement
        $referencement_agenda_article = $this->input->post("referencement_agenda_article");
        //$article_agenda_ref = $this->festival_agenda_ref($IdUpdatedArticle, $referencement_agenda_article);

        if (isset($IdUpdatedArticle) && $IdUpdatedArticle != "") $mssg = '1'; else $mssg = 'error';

        redirect("front/festival/fiche/" . $commercant_UserId . "/" . $IdUpdatedArticle . "/" . $mssg);
    }

    function festival_agenda_ref($Idfestival = 0, $activate_ref = 0)
    {
        $objArticle = $this->mdlfestival->getById(intval($Idfestival));
        if (isset($objArticle->id)) $objArticleAgendaRef = $this->mdl_festival_agenda_ref->getByfestivalId($objArticle->id);
        if (isset($objArticle->id)) $festivalId_original = $objArticle->id;

        //checking if agenda exist
        $idAgenda_verif = false;
        if (isset($objArticleAgendaRef) && count($objArticleAgendaRef) > 0) {
            $objAgenda_corresp = $this->mdl_agenda->GetById($objArticleAgendaRef->agenda_id);
            //var_dump($objAgenda_corresp[0]);die();
            if (isset($objAgenda_corresp->id) && $objAgenda_corresp->id != 0 && $objAgenda_corresp->id != '0') {
                $idAgenda_verif = true;
            } else {
                $this->mdl_festival_agenda_ref->delete($objArticleAgendaRef->id);
            }
        }

        if (isset($objArticleAgendaRef) && count($objArticleAgendaRef) > 0 && $idAgenda_verif == true) {
            # code...
            $objArticle->id = $objArticleAgendaRef->agenda_id;
            $objArticle->agenda_categid = $objArticle->article_categid;
            unset($objArticle->article_categid);
            $objArticle->agenda_subcategid = $objArticle->article_subcategid;
            unset($objArticle->article_subcategid);

            if (isset($activate_ref) && $activate_ref == "1") {
                $idAgenda_updated = $this->mdl_agenda->update((array)$objArticle);
                //$this->festival_agenda_ref_copy_image($Idfestival, $objArticle->id);
                $this->mdl_agenda_datetime->deleteByAgendaId($idAgenda_updated); // remove all agenda datetime
                $festival_datetime_list = $this->mdl_festival_datetime->getByfestivalId($festivalId_original);
                if (isset($festival_datetime_list) && count($festival_datetime_list)>0) {
                    foreach ($festival_datetime_list as $festival_datetime_list_item) {
                        $agenda_datetime_content = array();
                        $agenda_datetime_content['agenda_id'] = $idAgenda_updated;
                        $agenda_datetime_content['date_debut'] = $festival_datetime_list_item->date_debut;
                        $agenda_datetime_content['date_fin'] = $festival_datetime_list_item->date_fin;
                        $agenda_datetime_content['heure_debut'] = $festival_datetime_list_item->heure_debut;
                        $article_datetime_id_inserted = $this->mdl_agenda_datetime->insert($agenda_datetime_content);
                    }
                }
            } else {
                $idAgenda_updated = $this->mdl_agenda->delete($objArticleAgendaRef->agenda_id);
            }

        } else {
            //var_dump($objArticle); die("NOT EXISTS");
            if ($activate_ref == 1 || $activate_ref == "1") {
                $objArticle->id = null;
                $objArticle->agenda_categid = $objArticle->article_categid;
                unset($objArticle->article_categid);
                $objArticle->agenda_subcategid = $objArticle->article_subcategid;
                unset($objArticle->article_subcategid);
                //var_dump($objArticle); die("NOT EXISTS");
                $idAgenda_updated = $this->mdl_agenda->insert($objArticle);

                $this->mdl_agenda_datetime->deleteByAgendaId($idAgenda_updated); // remove all agenda datetime
                $festival_datetime_list = $this->mdl_festival_datetime->getByfestivalId($festivalId_original);
                if (isset($festival_datetime_list) && count($festival_datetime_list)>0) {
                    foreach ($festival_datetime_list as $festival_datetime_list_item) {
                        $agenda_datetime_content = array();
                        $agenda_datetime_content['agenda_id'] = $idAgenda_updated;
                        $agenda_datetime_content['date_debut'] = $festival_datetime_list_item->date_debut;
                        $agenda_datetime_content['date_fin'] = $festival_datetime_list_item->date_fin;
                        $agenda_datetime_content['heure_debut'] = $festival_datetime_list_item->heure_debut;
                        $festival_datetime_id_inserted = $this->mdl_agenda_datetime->insert($agenda_datetime_content);
                    }
                }

                //register into article_agenda_ref
                $OFestivalAgendaRef['id'] = null;
                $OFestivalAgendaRef['article_id'] = $Idfestival;
                $OFestivalAgendaRef['agenda_id'] = $idAgenda_updated;
                $OFestivalAgendaRef_added = $this->mdl_festival_agenda_ref->insert($OFestivalAgendaRef);
                //$this->article_agenda_ref_copy_image($IdArticle, $OArticleAgendaRef_added->agenda_id);
            }
        }
        return true;
    }
    function deleteFestival($prmId)
    {
        $oFestival = $this->mdlfestival->getById($prmId);
        //echo $oArticle->audio." ".$oArticle->video;
        $base_path_system = str_replace('system/', '', BASEPATH);
        if (isset($oFestival->audio) && $oFestival->audio != "") unlink($base_path_system . "application/resources/front/audios/" . $oFestival->audio);
        if (isset($oFestival->video) && $oFestival->video != "") unlink($base_path_system . "application/resources/front/videos/" . $oFestival->video);
        $this->mdlfestival->delete($prmId);
        $this->liste();
    }

    function deleteFestival_com($IdAgenda = 0, $prmIdCommercant = 0)
    {

        if ($this->ion_auth->logged_in() && ($this->ion_auth->in_group(2))) {
            redirect("front/particuliers/inscription/");
        } else if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
            redirect('admin/home', 'refresh');
        } else if (!$this->ion_auth->logged_in()) {
            $this->session->set_userdata("cur_uri", $this->uri->uri_string());
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }

        if ($this->ion_auth->logged_in() && ($prmIdCommercant == 0 || $prmIdCommercant == "")) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $prmIdCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        }

        //verify if $IdAgenda, $prmIdCommercant and $user_ion_auth->id are corrects and match between them
        if ($IdAgenda != 0 && $IdAgenda != "" && $IdAgenda != null && is_numeric($IdAgenda) == true) {
            $objAgenda_verification = $this->mdlfestival->getById($IdAgenda);
            if (isset($objAgenda_verification) && count($objAgenda_verification) != 0) {
                if ($objAgenda_verification->IdCommercant != $prmIdCommercant && $objAgenda_verification->IdUsers_ionauth != $user_ion_auth->id) {
                    redirect("front/utilisateur/no_permission");
                }
            } else redirect("front/utilisateur/no_permission");
        }

        $this->mdlfestival->delete($IdAgenda);

        redirect("front/festival/liste/" . $prmIdCommercant);

    }
    function getPostalCode_localisation($idVille = 0)
    {
        $oville = $this->mdlville->getVilleById($idVille);
        if (count($oville) != 0) $data["cp"] = $oville->CodePostal; else $data["cp"] = "";

        echo $data["cp"];

    }

    function delete_files($prmIdfestival = 0, $prmFiles)
    {
        $oInfoFestival = $this-> mdlfestival->GetById($prmIdfestival);
        $filetodelete = "";
        if ($prmFiles == "doc_affiche") {
            $filetodelete = $oInfoFestival->doc_affiche;
            $this->mdlfestival->effacerdoc_affiche($prmIdfestival);
        }
        if ($prmFiles == "photo1") {
            $filetodelete = $oInfoFestival->photo1;
            $this->mdlfestival->effacerphoto1($prmIdfestival);
        }
        if ($prmFiles == "photo2") {
            $filetodelete = $oInfoFestival->photo2;
            $this->mdlfestival->effacerphoto2($prmIdfestival);
        }
        if ($prmFiles == "photo3") {
            $filetodelete = $oInfoFestival->photo3;
            $this->mdlfestival->effacerphoto3($prmIdfestival);
        }
        if ($prmFiles == "photo4") {
            $filetodelete = $oInfoFestival->photo4;
            $this->mdlfestival->effacerphoto4($prmIdfestival);
        }
        if ($prmFiles == "photo5") {
            $filetodelete = $oInfoFestival->photo5;
            $this->mdlfestival->effacerphoto5($prmIdfestival);
        }
        if ($prmFiles == "pdf") {
            $filetodelete = $oInfoFestival->pdf;
            $this->mdlfestival->effacerpdf($prmIdfestival);
        }
        if ($prmFiles == "autre_doc_1") {
            $filetodelete = $oInfoFestival->autre_doc_1;
            $this->mdlfestival->effacerautre_doc_1($prmIdfestival);
        }
        if ($prmFiles == "autre_doc_2") {
            $filetodelete = $oInfoFestival->autre_doc_2;
            $this->mdlfestival->effacerautre_doc_2($prmIdfestival);
        }
        if (file_exists("application/resources/front/images/article/photoCommercant/" . $filetodelete) == true) {
            unlink("application/resources/front/images/article/photoCommercant/" . $filetodelete);
            $img_photo2_split_array = explode('.', $filetodelete);
            $img_photo2_path = "application/resources/front/images/article/photoCommercant/" . $img_photo2_split_array[0] . "_thumb_100_100." . $img_photo2_split_array[1];
            if (file_exists($img_photo2_path) == true) unlink($img_photo2_path);
        }

    }

    function add_organiser_festival()
    {
        $prmData['IdCommercant'] = $this->input->post("IdCommercant");
        $prmData['id'] = $this->input->post("Article_id_organisateur");
        $prmData['name'] = $this->input->post("organisateur");
        $prmData['address1'] = $this->input->post("adresse1");
        $prmData['address2'] = $this->input->post("adresse2");
        $prmData['postal_code'] = $this->input->post("codepostal");
        $prmData['department_id'] = $this->input->post("DepartementArticle");
        $prmData['ville_id'] = $this->input->post("IdVille");
        $prmData['tel'] = $this->input->post("telephone");
        $prmData['mobile'] = $this->input->post("mobile");
        $prmData['fax'] = $this->input->post("fax");
        $prmData['email'] = $this->input->post("email");
        $prmData['website'] = $this->input->post("siteweb");
        $prmData['facebook'] = $this->input->post("facebook");
        $prmData['twitter'] = $this->input->post("twitter");
        $prmData['googleplus'] = $this->input->post("googleplus");

        $verif_name = $this->mdl_festival_organiser->getByName($prmData['name'],$prmData['IdCommercant']);
        if (isset($verif_name) && count($verif_name) > 0) {
            echo 'error';
        } else {
            if (isset($prmData['id']) && $prmData['id']!=0 && $prmData['id']!='0' && $prmData['id'] != '' && $prmData['id'] != null)
                $insert_id = $this->mdl_festival_organiser->update($prmData);
            else $insert_id = $this->mdl_festival_organiser->insert($prmData);
            if (isset($insert_id)) {
                echo $insert_id->id;
            } else {
                echo 'error';
            }
        }
    }
    function check_organiser_festival() {
        $organiser_id = $this->input->post("organiser_id");
        $verif_name = $this->mdl_festival_organiser->getById($organiser_id);

        if ($this->ion_auth->logged_in() && isset($verif_name) && is_object($verif_name)) {
            echo json_encode((array)$verif_name);
        } else {
            echo "error";
        }
    }
    function check_redacteur_festival() {
        $redacteur_id = $this->input->post("redacteur_id");
        $verif_name = $this->mdl_festival_redacteur->getById($redacteur_id);

        if ($this->ion_auth->logged_in() && isset($verif_name) && is_object($verif_name)) {
            echo json_encode((array)$verif_name);
        } else {
            echo "error";
        }
    }
    function delete_organiser_festival(){
        $organiser_id = $this->input->post("organiser_id");
        if ($this->ion_auth->logged_in() && isset($organiser_id)) {
            $verif_result = $this->mdl_festival_organiser->delete($organiser_id);
            if ($verif_result) echo 'ok';
            else echo "error";
        } else {
            echo "error";
        }
    }

    function delete_redacteur_festival(){
        $redacteur_id = $this->input->post("redacteur_id");
        if ($this->ion_auth->logged_in() && isset($redacteur_id)) {
            $verif_result = $this->mdl_festival_redacteur->delete($redacteur_id);
            if ($verif_result) echo 'ok';
            else echo "error";
        } else {
            echo "error";
        }
    }
    function add_validation_festival_function()

    {
        //recup IdCommercant en session
        $user_ion_auth = $this->ion_auth->user()->row();
        $IdCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

        $prmData['nom'] = $this->input->post("add_validation_article_name");

        $prmData['email'] = $this->input->post("add_validation_article_email");

        $prmData['IdCommercant'] = $IdCommercant;

        $verif_name = $this->mdl_festival_validation_email->getByNom($prmData['nom'],$prmData['IdCommercant']);

        if (isset($verif_name) && count($verif_name) > 0) {

            echo 'error';

        } else {

            if (isset($prmData['id']) && $prmData['id']!=0 && $prmData['id']!='0' && $prmData['id'] != '' && $prmData['id'] != null)

                $insert_id = $this->mdl_festival_validation_email->update($prmData);

            else $insert_id = $this->mdl_festival_validation_email->insert($prmData);

            if (isset($insert_id)) {

                echo $insert_id->id;

            } else {

                echo 'error';

            }
        }
    }



    function envoyer_validation_festival_function()
    {
        $validation_nom_manifestation = $this->input->post("validation_nom_manifestation");
        $validation_description = $this->input->post("validation_description");
        $festival_id = $this->input->post("festival_id");
        $festival_date_depot = $this->input->post("festival_date_depot");
       $destinataire_validation_festival = $this->input->post("destinataire_validation_festival");
        $destinataire_validation = $this->mdl_festival_validation_email->getById($destinataire_validation_festival);

        $user_ion_auth = $this->ion_auth->user()->row();
        $prmIdCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $prmObjCommercant = $this->Commercant->GetById($prmIdCommercant);

        $contact_partner_nom = $destinataire_validation->nom;
        $contact_partner_mail = $prmObjCommercant->Email;
        $contact_partner_mailto = $destinataire_validation->email;

        $contact_partner_mailSubject = 'Demande validation article - Sortez.org';

        $message_html = $this->load->view('mail/send_validation_article', '', true);
        $message_html = str_replace('{mail_article_title}', $validation_nom_manifestation, $message_html);
        $message_html = str_replace('{mail_date_depot}', $festival_date_depot, $message_html);
        $message_html = str_replace('{mail_IdCommercant}', $prmIdCommercant, $message_html);
        $message_html = str_replace('{mail_IdArticle}', $festival_id, $message_html);
        $message_html = str_replace('{mail_nom_redacteur}', $prmObjCommercant->NomSociete, $message_html);
        $message_html = str_replace('{mail_contact_redacteur}', $contact_partner_mail, $message_html);

        $sending_mail_privicarte = false;

        //$message_html = utf8_encode($message_html);//encode mail content

        $zContactEmailCom_ = array();
        $zContactEmailCom_[] = array("Email" => $contact_partner_mailto, "Name" => $contact_partner_nom);
        $sending_mail_privicarte = envoi_notification($zContactEmailCom_, $contact_partner_mailSubject, $message_html, $contact_partner_mail);


        $zContactEmailCom_copy = array();
        $zContactEmailCom_copy[] = array("Email" => $contact_partner_mail, "Name" => $prmObjCommercant->NomSociete);

        $message_html_copy = $this->load->view('mail/send_validation_festival', '', true);
        $message_html_copy = str_replace('{mail_festival_title}', $validation_nom_manifestation, $message_html_copy);
        $message_html_copy = str_replace('{mail_date_depot}', $festival_date_depot, $message_html_copy);
        $message_html_copy = str_replace('{mail_IdCommercant}', $prmIdCommercant, $message_html_copy);
        $message_html_copy = str_replace('{mail_Idfestival}', $festival_id, $message_html_copy);
        $message_html_copy = str_replace('{mail_nom_redacteur}', $prmObjCommercant->NomSociete, $message_html_copy);
        $message_html_copy = str_replace('{mail_contact_redacteur}', $contact_partner_mail, $message_html_copy);


        //$message_html_copy = html_entity_decode(htmlentities($message_html_copy));
        $sending_mail_privicarte_copy = envoi_notification($zContactEmailCom_copy, $contact_partner_mailSubject, $message_html_copy);


        if ($sending_mail_privicarte) echo 'ok';
        else echo 'error';


    }
    function departementcp_festival()
    {
        $CodePostal = $this->input->post("CodePostalSociete");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdldepartement->GetDepByCodePostal($CodePostal);
        //echo $CodePostal;
        $result_to_show = '<select name="Article[departement_id]" id="DepartementArticle" class="stl_long_input_platinum form-control" onchange="javascript:CP_getVille_D_CP();">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .= '<option value="' . $item->departement_id . '">' . $item->departement_nom . '</option>';
            }
        }
        $result_to_show .= '</select>';
        echo $result_to_show;
    }
    function departementcp_festival_localisation()
    {
        $CodePostal = $this->input->post("CodePostalSociete");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdldepartement->GetDepByCodePostal($CodePostal);
        //echo $CodePostal;
        $result_to_show = '<select name="Festival[departement_id_localisation]" id="DepartementArticleLocalisation" class="form-control" onchange="javascript:CP_getVille_D_CP_localisation();">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .= '<option value="' . $item->departement_id . '">' . $item->departement_nom . ' - ' . $item->departement_code . '</option>';
            }
        }
        $result_to_show .= '</select>';
        echo $result_to_show;
    }
    function get_location_festival()
    {
        $location_id = $this->input->post("location_id");
        $location = $this->mdl_localisation->getById($location_id);
        echo json_encode((array)$location);
    }
    function departementcp_festival_add_location()
    {
        $CodePostal = $this->input->post("CodePostalSociete");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdldepartement->GetDepByCodePostal($CodePostal);
        //echo $CodePostal;
        $result_to_show = '<select name="add_location_departementid" id="add_location_departementid" class="form-control" onchange="javascript:CP_getVille_D_CP_add_location();">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .= '<option value="' . $item->departement_id . '">' . $item->departement_nom . ' - ' . $item->departement_code . '</option>';
            }
        }
        $result_to_show .= '</select>';
        echo $result_to_show;
    }


    function villecp_festival()
    {
        $CodePostal = $this->input->post("CodePostalSociete");
        $departement_id = $this->input->post("departement_id");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdlville->GetVilleByCodePostal($CodePostal, $departement_id);
        //echo $CodePostal;
        $result_to_show = '<select name="Article[IdVille]" id="IdVille" class="stl_long_input_platinum form-control">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .= '<option value="' . $item->IdVille . '">' . $item->Nom . '</option>';
            }
        }
        $result_to_show .= '</select>';
        echo $result_to_show;

    }
    function villecp_article_localisation()
    {
        $CodePostal = $this->input->post("CodePostalSociete");
        $departement_id = $this->input->post("departement_id");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdlville->GetVilleByCodePostal($CodePostal, $departement_id);
        //echo $CodePostal;
        $result_to_show = '<select name="Article[IdVille_localisation]" id="IdVille_localisation" class="form-control">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .= '<option value="' . $item->IdVille . '">' . $item->Nom . '</option>';
            }
        }
        $result_to_show .= '</select>';
        echo $result_to_show;
    }

    function villecp_festivaal_add_location()
    {
        $CodePostal = $this->input->post("CodePostalSociete");
        $departement_id = $this->input->post("departement_id");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdlville->GetVilleByCodePostal($CodePostal, $departement_id);
        //echo $CodePostal;
        $result_to_show = '<select name="add_location_villeId" id="add_location_villeId" class="form-control">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .= '<option value="' . $item->IdVille . '">' . $item->Nom . '</option>';
            }
        }
        $result_to_show .= '</select>';
        echo $result_to_show;
    }


    function get_commercant_localisation()
    {
        if ($this->ion_auth->logged_in() && !$this->ion_auth->in_group(2)) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            $data["userId"] = $commercant_UserId;
            $objCommercant = $this->Commercant->GetById($commercant_UserId);
            echo json_encode((array)$objCommercant);

        } else {
            echo "error";
        }
    }
    function get_ville_localisation()
    {
        $IdVille = $this->input->post("IdVille");
        $objVille = $this->mdlville->getVilleById($IdVille);
        //var_dump($objVille);
        if(isset($objVille) && is_object($objVille)) echo $objVille->ville_nom;
    }
    function add_redacteur_festival()
    {
        $prmData['nom'] = $this->input->post("add_redacteur_name");
        $prmData['id'] = $this->input->post("add_redacteur_id");
        $prmData['IdCommercant'] = $this->input->post("IdCommercant");
        $verif_name = $this->mdl_festival_redacteur->getByNom($prmData['nom'],$prmData['IdCommercant']);
        if (isset($verif_name) && count($verif_name) > 0) {
            echo 'error';
        } else {
            if (isset($prmData['id']) && $prmData['id']!=0 && $prmData['id']!='0' && $prmData['id'] != '' && $prmData['id'] != null)
                $insert_id = $this->mdl_festival_redacteur->update($prmData);
            else $insert_id = $this->mdl_festival_redacteur->insert($prmData);
            if (isset($insert_id)) {
                echo $insert_id->id;
            } else {
                echo 'error';
            }
        }
    }
    function add_location_festival()
    {
        $Location['location_id'] = $this->input->post("add_location_id");
        $Location['location'] = $this->input->post("add_location_name");
        $Location['location_address'] = $this->input->post("add_location_adress");
        $Location['location_postcode'] = $this->input->post("add_location_codepostal");
        $Location['location_departementid'] = $this->input->post("add_location_departementid");
        $Location['location_villeid'] = $this->input->post("add_location_villeId");
        $Location['IdCommercant'] = $this->input->post("IdCommercant");

        if ($Location['location_id'] == "" || $Location['location_id'] == null) $Location['location_id'] = 0;

        $verif_id = $this->mdl_localisation->getById($Location['location_id']);
        if (isset($verif_id) && count($verif_id) > 0) {
            $insert_id = $this->mdl_localisation->update($Location);
            if (isset($insert_id)) {
                echo $insert_id;
            } else {
                echo 'error';
            }
        } else {
            $verif_name = $this->mdl_localisation->getByName($Location['location']);
            if (isset($verif_name) && count($verif_name) > 0) {
                echo 'error';
            } else {
                $insert_id = $this->mdl_localisation->insert($Location);
                if (isset($insert_id)) {
                    echo $insert_id;
                } else {
                    echo 'error';
                }
            }
        }
    }
    function villecp_article_add_location()
    {
        $CodePostal = $this->input->post("CodePostalSociete");
        $departement_id = $this->input->post("departement_id");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdlville->GetVilleByCodePostal($CodePostal, $departement_id);
        //echo $CodePostal;
        $result_to_show = '<select name="add_location_villeId" id="add_location_villeId" class="form-control">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .= '<option value="' . $item->IdVille . '">' . $item->Nom . '</option>';
            }
        }
        $result_to_show .= '</select>';
        echo $result_to_show;
    }
    function delete_location_article()
    {
        $location_id = $this->input->post("location_article");
        $verif_location = $this->mdl_localisation->delete($location_id);
        echo "error";
    }
    function GetallSubcateg_by_categ($id = "0")
    {
        //$data["colSousCategorie"] = $this->mdl_categories_article->GetAllSousrubriqueByRubrique($id);
        $data["colSousCategorie"] = $this->mdl_categories_agenda->GetAllSousrubriqueByRubrique($id); // article use agenda's categories & subcategories

        $data['id'] = $id;
        $this->load->view("sortez/article/vwReponseSubcateg", $data);
    }
    function personnalisation()
    {

        $data["current_page"] = "subscription_pro";//this is to differentiate js to use

        if (!$this->ion_auth->logged_in()) {
            redirect("auth/login");
        } else {


            $choix_dossier_perso_check_session = $this->session->userdata('choix_dossier_perso_check_session');
            if (isset($choix_dossier_perso_check_session)) {
                $data['choix_dossier_perso_check_session'] = $choix_dossier_perso_check_session;
                ////$this->firephp->log($choix_dossier_perso_check_session, 'choix_dossier_perso_check_session');
            }

            $agenda_perso = $this->input->post("agenda_perso");
            $choix_dossier_perso_value = $agenda_perso['dossperso'];
            ////$this->firephp->log($choix_dossier_perso_value, 'choix_dossier_perso_value');

            if (isset($choix_dossier_perso_value) && $choix_dossier_perso_value == "1") {
                $data['choix_dossier_perso_check_session'] = "1";
                $this->session->set_userdata('choix_dossier_perso_check_session', "1");

                $user_ion_auth = $this->ion_auth->user()->row();
                $iduser_verif = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                if ($iduser_verif == null || $iduser_verif == 0 || $iduser_verif == "") {
                    $_iCommercantId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                } else {
                    $_iCommercantId = "0";
                }
            } else {
                $_iCommercantId = "0";
                $data['choix_dossier_perso_check_session'] = "0";
                $this->session->set_userdata('choix_dossier_perso_check_session', "0");
            }

            if (isset($_iCommercantId) && $_iCommercantId != "0" && $_iCommercantId != null && $_iCommercantId != "") {
                $toVille = $this->mdlville->GetFestivalVillesByIdCommercant($_iCommercantId);//get ville list of agenda
                //var_dump($toVille);die();
                $data['toVille'] = $toVille;
                $toCategorie_principale = $this->mdlfestival->GetFestivalCategorie_ByIdCommercant($_iCommercantId);
                $data['toCategorie_principale'] = $toCategorie_principale;
                $toDeposant = $this->mdlfestival->getAlldeposantAgenda($_iCommercantId);
                $data['toDeposant'] = $toDeposant;
            } else {
                $toVille = $this->mdlville->GetFestivalVilles();//get ville list of agenda
                $data['toVille'] = $toVille;
               // var_dump($toVille);die();
                $toCategorie_principale = $this->mdlfestival->GetFestivalCategorie();
                $data['toCategorie_principale'] = $toCategorie_principale;
                $toDeposant = $this->mdlfestival->getAlldeposantAgenda();
                $data['toDeposant'] = $toDeposant;
            }

            $current_user_ion_auth = $this->ion_auth->user()->row();
            $current_iCommercantId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($current_user_ion_auth->id);
            $data['current_iCommercantId'] = $current_iCommercantId;
            $oAgenda_perso = $this->mdlfestival_perso->GetFestival_festival_persoByUser($current_iCommercantId, $current_user_ion_auth->id);
            ////$this->firephp->log($current_iCommercantId, 'current_iCommercantId');
            ////$this->firephp->log($current_user_ion_auth->id, 'current_user_ion_auth');
            ////$this->firephp->log($oAgenda_perso, 'oAgenda_perso');
            $data['oAgenda_perso'] = $oAgenda_perso;

            $group_user = $this->ion_auth->get_users_groups($current_user_ion_auth->id)->result();
            $data['group_user'] = $group_user[0]->id;

            $data['pagecategory'] = "admin_commercant";
            $data['currentpage'] = "export_festival";
            $this->load->view('privicarte/festival_personnalisation', $data);

        }
    }

    function festival_perso()
    {

        ////$this->firephp->log($_REQUEST, '_REQUEST');

        $zCouleur = $this->input->get("zCouleur");
        if (!isset($zCouleur)) $zCouleur = $this->input->post("zCouleur");
        if (isset($zCouleur) && $zCouleur != "") $data['zCouleur'] = $zCouleur;
        $zCouleurTitre = $this->input->get("zCouleurTitre");
        if (!isset($zCouleurTitre)) $zCouleurTitre = $this->input->post("zCouleurTitre");
        if (isset($zCouleurTitre) && $zCouleurTitre != "") $data['zCouleurTitre'] = $zCouleurTitre;
        $zCouleurTextBouton = $this->input->get("zCouleurTextBouton");
        if (!isset($zCouleurTextBouton)) $zCouleurTextBouton = $this->input->post("zCouleurTextBouton");
        if (isset($zCouleurTextBouton) && $zCouleurTextBouton != "") $data['zCouleurTextBouton'] = $zCouleurTextBouton;
        $zCouleurBgBouton = $this->input->get("zCouleurBgBouton");
        if (!isset($zCouleurBgBouton)) $zCouleurBgBouton = $this->input->post("zCouleurBgBouton");
        if (isset($zCouleurBgBouton) && $zCouleurBgBouton != "") $data['zCouleurBgBouton'] = $zCouleurBgBouton;
        $zCouleurNbBtn = $this->input->get("zCouleurNbBtn");
        if (!isset($zCouleurNbBtn)) $zCouleurNbBtn = $this->input->post("zCouleurNbBtn");
        if (isset($zCouleurNbBtn) && $zCouleurNbBtn != "") $data['zCouleurNbBtn'] = $zCouleurNbBtn;

        $tiDepartement = $this->input->get("tiDepartement");
        if (!isset($tiDepartement)) $tiDepartement = $this->input->post("tiDepartement");
        if (isset($tiDepartement) && $tiDepartement != "") {
            $tiDepartement = substr_replace($tiDepartement, "", -1);
            $tiDepartement_array = explode("_", $tiDepartement);
            $data['tiDepartement_array'] = $tiDepartement;
        } else {
            $tiDepartement_array = "0";
        }
        $tiDeposant = $this->input->get("tiDeposant");
        if (!isset($tiDeposant)) $tiDeposant = $this->input->post("tiDeposant");
        if (isset($tiDeposant) && $tiDeposant != "") {
            $tiDeposant = substr_replace($tiDeposant, "", -1);
            $tiDeposant_array = explode("_", $tiDeposant);
            $data['tiDeposant_array'] = $tiDeposant;
        }
        $tiCategorie = $this->input->get("tiCategorie");
        if (!isset($tiCategorie)) $tiCategorie = $this->input->post("tiCategorie");
        if (isset($tiCategorie) && $tiCategorie != "") {
            $tiCategorie = substr_replace($tiCategorie, "", -1);
            $data["tiCategorie_for_init"] = $tiCategorie;
            $tiCategorie_array = explode("_", $tiCategorie);
            //$data['tiCategorie_array'] = $tiCategorie_array;
        } else {
            $tiCategorie_array = "0";
        }
        $tiDossperso = $this->input->get("tiDossperso");
        if (!isset($tiDossperso)) $tiDossperso = $this->input->post("tiDossperso");
        $data["tiDossperso"] = $tiDossperso;
        if (isset($tiDossperso) && $tiDossperso != "0") {
            $idCommercant_to_search = $tiDossperso;
        } else if (isset($tiDeposant_array)) {
            $idCommercant_to_search = $tiDeposant_array;
        } else {
            $idCommercant_to_search = "0";
        }

        $contentonly = $this->input->get("contentonly");
        if (!isset($contentonly)) $contentonly = $this->input->post("contentonly");
        $data["contentonly"] = $contentonly;

        $proximite_tiDatefilter = $this->input->get("proximite_tiDatefilter");
        if (!isset($proximite_tiDatefilter)) $proximite_tiDatefilter = $this->input->post("proximite_tiDatefilter");
        if (!isset($proximite_tiDatefilter)) $proximite_tiDatefilter = "0";
        $data["proximite_tiDatefilter"] = $proximite_tiDatefilter;

        $keyword_input_export = $this->input->get("keyword_input_export");
        if (!isset($keyword_input_export)) $keyword_input_export = $this->input->post("keyword_input_export");
        $data["keyword_input_export"] = $keyword_input_export;

        //var_dump($data); die();


        $toAgenda = $this->mdlfestival->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", $proximite_tiDatefilter, "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0);
        $data['toAgenda'] = $toAgenda;


        //$data['toAgndaTout_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
        $data['toAgndaAujourdhui_global'] = count($this->mdlfestival->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "101", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
        $data['toAgndaWeekend_global'] = count($this->mdlfestival->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "202", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
        $data['toAgndaSemaine_global'] = count($this->mdlfestival->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "303", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
        $data['toAgndaSemproch_global'] = count($this->mdlfestival->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "404", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
        $data['toAgndaMois_global'] = count($this->mdlfestival->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "505", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
        /*
                $data['toAgndaJanvier_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "01", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                $data['toAgndaFevrier_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "02", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                $data['toAgndaMars_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "03", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                $data['toAgndaAvril_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "04", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                $data['toAgndaMai_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "05", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                $data['toAgndaJuin_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "06", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                $data['toAgndaJuillet_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "07", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                $data['toAgndaAout_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "08", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                $data['toAgndaSept_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "09", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                $data['toAgndaOct_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "10", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                $data['toAgndaNov_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "11", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                $data['toAgndaDec_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "12", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                */

        $tiCategorie_list_array = $this->mdlfestival->listeAgendaRecherche_agenda_perso_liste_categ($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0);
        $data['tiCategorie_list_array'] = $tiCategorie_list_array;

        $data['defaul_thumb_width'] = $this->config->item('defaul_thumb_width');
        $data['defaul_thumb_height'] = $this->config->item('defaul_thumb_height');
        $data['currentpage'] = "export_festival";
        //$this->load->view('agendaAout2013/agenda_perso', $data);
        //$this->load->view('privicarte/agenda_perso', $data);
        $this->load->view('export/festival_perso_respo', $data);

    }



    function article_perso_check_filterQuand() {
        $proximite_tiDepartement = $this->input->post("proximite_tiDepartement");
        $proximite_tiDepartement = explode("_", $proximite_tiDepartement);
        $proximite_tiDeposant = $this->input->post("proximite_tiDeposant");
        $proximite_tiDeposant = explode("_", $proximite_tiDeposant);
        $proximite_tiCategorie = $this->input->post("proximite_tiCategorie");
        $proximite_tiDossperso = $this->input->post("proximite_tiDossperso");

        if (isset($proximite_tiDossperso) && $proximite_tiDossperso!="0"){
            $proximite_tiDeposant = $proximite_tiDossperso;
        }

        $proximite_tiCategorie_array = array($proximite_tiCategorie);

        $toAgenda = $this->mdlfestival->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array,0, $proximite_tiDepartement, "", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0");

        $toAgndaTout_global = count($this->mdlfestival->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array,0, $proximite_tiDepartement, "", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaAujourdhui_global = count($this->mdlfestival->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array,0, $proximite_tiDepartement, "", 0, 0, 10000, "", "101", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaWeekend_global = count($this->mdlfestival->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array,0, $proximite_tiDepartement, "", 0, 0, 10000, "", "202", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaSemaine_global = count($this->mdlfestival->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array,0, $proximite_tiDepartement, "", 0, 0, 10000, "", "303", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaSemproch_global = count($this->mdlfestival->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array,0, $proximite_tiDepartement, "", 0, 0, 10000, "", "404", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaMois_global = count($this->mdlfestival->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array,0, $proximite_tiDepartement, "", 0, 0, 10000, "", "505", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));

        $toAgndaJanvier_global = count($this->mdlfestival->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array,0, $proximite_tiDepartement, "", 0, 0, 10000, "", "01", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaFevrier_global = count($this->mdlfestival->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array,0, $proximite_tiDepartement, "", 0, 0, 10000, "", "02", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaMars_global = count($this->mdlfestival->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array,0, $proximite_tiDepartement, "", 0, 0, 10000, "", "03", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaAvril_global = count($this->mdlfestival->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array,0, $proximite_tiDepartement, "", 0, 0, 10000, "", "04", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaMai_global = count($this->mdlfestival->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array,0, $proximite_tiDepartement, "", 0, 0, 10000, "", "05", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaJuin_global = count($this->mdlfestival->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array,0, $proximite_tiDepartement, "", 0, 0, 10000, "", "06", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaJuillet_global = count($this->mdlfestival->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array,0, $proximite_tiDepartement, "", 0, 0, 10000, "", "07", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaAout_global = count($this->mdlfestival->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array,0, $proximite_tiDepartement, "", 0, 0, 10000, "", "08", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaSept_global = count($this->mdlfestival->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array,0, $proximite_tiDepartement, "", 0, 0, 10000, "", "09", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaOct_global = count($this->mdlfestival->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array,0, $proximite_tiDepartement, "", 0, 0, 10000, "", "10", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaNov_global = count($this->mdlfestival->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array,0, $proximite_tiDepartement, "", 0, 0, 10000, "", "11", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaDec_global = count($this->mdlfestival->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array,0, $proximite_tiDepartement, "", 0, 0, 10000, "", "12", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));

        ?>

        <select id="inputStringQuandHidden_to_check" size="1" name="inputStringQuandHidden_to_check" onChange="javascript:change_list_agenda_perso_filtreIdQuand();">
            <option value="0" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="0") {?>selected<?php }?>>Toutes les dates <?php if (isset($toAgndaTout_global)) echo "(".$toAgndaTout_global.")"; else echo "(0)"; ?></option>
            <option value="101" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="101") {?>selected<?php }?>>Aujourd'hui <?php if (isset($toAgndaAujourdhui_global)) echo "(".$toAgndaAujourdhui_global.")"; else echo "(0)"; ?></option>
            <option value="202" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="202") {?>selected<?php }?>>Ce Week-end <?php if (isset($toAgndaWeekend_global)) echo "(".$toAgndaWeekend_global.")"; else echo "(0)"; ?></option>
            <option value="303" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="303") {?>selected<?php }?>>Cette semaine <?php if (isset($toAgndaSemaine_global)) echo "(".$toAgndaSemaine_global.")"; else echo "(0)"; ?></option>
            <option value="404" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="404") {?>selected<?php }?>>Semaine prochaine <?php if (isset($toAgndaSemproch_global)) echo "(".$toAgndaSemproch_global.")"; else echo "(0)"; ?></option>
            <!--<option value="505" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="505") {?>selected<?php // }?>>Ce mois <?php // if (isset($toAgndaMois_global)) echo "(".$toAgndaMois_global.")"; else echo "(0)"; ?></option>-->
            <option value="01" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="01") {?>selected<?php }?>>Janvier <?php if (isset($toAgndaJanvier_global)) echo "(".$toAgndaJanvier_global.")"; else echo "(0)"; ?></option>
            <option value="02" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="02") {?>selected<?php }?>>Février <?php if (isset($toAgndaFevrier_global)) echo "(".$toAgndaFevrier_global.")"; else echo "(0)"; ?></option>
            <option value="03" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="03") {?>selected<?php }?>>Mars <?php if (isset($toAgndaMars_global)) echo "(".$toAgndaMars_global.")"; else echo "(0)"; ?></option>
            <option value="04" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="04") {?>selected<?php }?>>Avril <?php if (isset($toAgndaAvril_global)) echo "(".$toAgndaAvril_global.")"; else echo "(0)"; ?></option>
            <option value="05" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="05") {?>selected<?php }?>>Mai <?php if (isset($toAgndaMai_global)) echo "(".$toAgndaMai_global.")"; else echo "(0)"; ?></option>
            <option value="06" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="06") {?>selected<?php }?>>Juin <?php if (isset($toAgndaJuin_global)) echo "(".$toAgndaJuin_global.")"; else echo "(0)"; ?></option>
            <option value="07" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="07") {?>selected<?php }?>>Juillet <?php if (isset($toAgndaJuillet_global)) echo "(".$toAgndaJuillet_global.")"; else echo "(0)"; ?></option>
            <option value="08" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="08") {?>selected<?php }?>>Août <?php if (isset($toAgndaAout_global)) echo "(".$toAgndaAout_global.")"; else echo "(0)"; ?></option>
            <option value="09" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="09") {?>selected<?php }?>>Septembre <?php if (isset($toAgndaSept_global)) echo "(".$toAgndaSept_global.")"; else echo "(0)"; ?></option>
            <option value="10" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="10") {?>selected<?php }?>>Octobre <?php if (isset($toAgndaOct_global)) echo "(".$toAgndaOct_global.")"; else echo "(0)"; ?></option>
            <option value="11" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="11") {?>selected<?php }?>>Novembre <?php if (isset($toAgndaNov_global)) echo "(".$toAgndaNov_global.")"; else echo "(0)"; ?></option>
            <option value="12" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="12") {?>selected<?php }?>>Décembre <?php if (isset($toAgndaDec_global)) echo "(".$toAgndaDec_global.")"; else echo "(0)"; ?></option>
        </select>

        <?php

    }


    function article_perso_check(){

        $proximite_tiDepartement = $this->input->post("proximite_tiDepartement");
        $proximite_tiDepartement = explode("_", $proximite_tiDepartement);
        $proximite_tiDeposant = $this->input->post("proximite_tiDeposant");
        $proximite_tiDeposant = explode("_", $proximite_tiDeposant);
        $proximite_tiCategorie = $this->input->post("proximite_tiCategorie");
        $proximite_tiDossperso = $this->input->post("proximite_tiDossperso");
        $inputStringQuandHidden_to_check = $this->input->post("inputStringQuandHidden_to_check");
        $defaul_thumb_width = $this->config->item('defaul_thumb_width');
        $defaul_thumb_height = $this->config->item('defaul_thumb_height');

        if (isset($proximite_tiDossperso) && $proximite_tiDossperso!="0"){
            $proximite_tiDeposant = $proximite_tiDossperso;
        }

        $proximite_tiCategorie_array = array($proximite_tiCategorie);

        if (isset($inputStringQuandHidden_to_check) && $inputStringQuandHidden_to_check!="0") {
            $idQuand = $inputStringQuandHidden_to_check;
        } else {
            $idQuand = "0";
        }

        $toAgenda = $this->mdlfestival->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array,0, $proximite_tiDepartement, "", 0, 0, 10000, "", $idQuand, "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0");

        ?>

        <?php  foreach($toAgenda as $oAgenda){ ?>

            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin:3px">
                <tr>

                    <td style="width:90px">
                        <a href="javascript:void(0);" onClick="javascript:proximite_perso_view_details(<?php echo $oAgenda->id; ?>);" style="color:#000000; text-decoration:none;">
                            <?php
                            $image_home_vignette = "";
                            if (isset($oAgenda->photo1) && $oAgenda->photo1 != "" && is_file("application/resources/front/images/festival/photoCommercant/".$oAgenda->photo1)==true){ $image_home_vignette = $oAgenda->photo1;}
                            else if ($image_home_vignette == "" && isset($oAgenda->photo2) && $oAgenda->photo2 != "" && is_file("application/resources/front/images/festival/photoCommercant/".$oAgenda->photo2)==true){ $image_home_vignette = $oAgenda->photo2;}
                            else if ($image_home_vignette == "" && isset($oAgenda->photo3) && $oAgenda->photo3 != "" && is_file("application/resources/front/images/festival/photoCommercant/".$oAgenda->photo3)==true){ $image_home_vignette = $oAgenda->photo3;}
                            else if ($image_home_vignette == "" && isset($oAgenda->photo4) && $oAgenda->photo4 != "" && is_file("application/resources/front/images/festival/photoCommercant/".$oAgenda->photo4)==true){ $image_home_vignette = $oAgenda->photo4;}
                            else if ($image_home_vignette == "" && isset($oAgenda->photo5) && $oAgenda->photo5 != "" && is_file("application/resources/front/images/festival/photoCommercant/".$oAgenda->photo5)==true){ $image_home_vignette = $oAgenda->photo5;}
                            ////$this->firephp->log($image_home_vignette, 'image_home_vignette');

                            //showing category img if all image of agenda is null
                            $this->load->model("mdl_categories_agenda");
                            $toCateg_for_agenda = $this->mdl_categories_agenda->getById($oAgenda->agenda_categid);
                            if ($image_home_vignette == "" && isset($toCateg_for_agenda->images) && $toCateg_for_agenda->images != "" && is_file("application/resources/front/images/festival/category/".$toCateg_for_agenda->images)==true){
                                echo '<img src="'.GetImagePath("front/").'/festival/category/'.$toCateg_for_agenda->images.'" width="90"/>';
                            } else {

                                if ($image_home_vignette != ""){
                                    $img_photo_split_array = explode('.',$image_home_vignette);
                                    $img_photo_path = "application/resources/front/images/festival/photoCommercant/".$img_photo_split_array[0]."_thumb_".$defaul_thumb_width."_".$defaul_thumb_height.".".$img_photo_split_array[1];
                                    if (is_file($img_photo_path)==false) {
                                        echo image_thumb("application/resources/front/images/festival/photoCommercant/" . $image_home_vignette, $defaul_thumb_width, $defaul_thumb_height,'','');
                                    } else echo '<img src="'.GetImagePath("front/").'/festival/photoCommercant/'.$img_photo_split_array[0]."_thumb_".$defaul_thumb_width."_".$defaul_thumb_height.".".$img_photo_split_array[1].'" width="90"/>';

                                } else {
                                    $image_home_vignette_to_show = GetImagePath("front/")."/wp71b211d2_06.png";
                                    echo '<img src="'.$image_home_vignette_to_show.'" width="90"/>';
                                }

                            }
                            ?>
                        </a>

                    </td>
                    <td width="450" valign="top" style="padding-left:15px; padding-right:0px;">
                        <a href="javascript:void(0);" onClick="javascript:proximite_perso_view_details(<?php echo $oAgenda->id; ?>);" style="color:#000000; text-decoration:none;">
                <span class="titre_agenda_perso">
                <strong>
                    <?php echo $oAgenda->category ; ?><br />
                    <?php echo $oAgenda->nom_manifestation ; ?><br />
                </strong>
                </span>
                            <span style="font-size:12px;">
              <?php echo $oAgenda->ville ; ?>, <?php echo $oAgenda->adresse_localisation ; ?>, <?php echo $oAgenda->codepostal_localisation ; ?><br />
                                <?php
                                if ($oAgenda->date_debut == $oAgenda->date_fin) echo "Le ".translate_date_to_fr($oAgenda->date_debut);
                                else echo "Du ".translate_date_to_fr($oAgenda->date_debut)." au ".translate_date_to_fr($oAgenda->date_fin);
                                ?>
              </span>
                        </a>
                    </td>

                    <td>
                        <a href="javascript:void(0);" onClick="javascript:proximite_perso_view_details(<?php echo $oAgenda->id; ?>);" style="color:#000000; text-decoration:none;">
                            <img src="<?php echo GetImagePath("front/"); ?>/mobile2013/fleche_agenda_mobile.png" width="30" height="62" alt="details" />
                        </a>
                    </td>



                </tr>
            </table>

            <div style="height:3px; width:100%; background-color:#000000"></div>
        <?php  } ?>
        <?php

    }
    function personnalisation_save(){
        $data["current_page"] = "subscription_pro";//this is to differentiate js to use

        if (!$this->ion_auth->logged_in()){
            redirect("auth/login");
        } else {

            /*if ($this->ion_auth->in_group(3)) {
                redirect('front/utilisateur/no_permission');
            }
            else
            {*/
            $objArticle_perso = $this->input->post("agenda_perso");

            $user_ion_auth = $this->ion_auth->user()->row();
            $_iCommercantId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            $objArticle_perso['IdUsers_ionauth'] = $user_ion_auth->id;
            $objArticle_perso['IdCommercant'] = $_iCommercantId;

            //$objArticle_perso['code'] = base64_encode($objArticle_perso['code']);
            /*$objArticle_perso['code'] = str_replace('<','&lt;',$objArticle_perso['code']);
            $objArticle_perso['code'] = str_replace('>','&gt;',$objArticle_perso['code']);*/

            //$this->firephp->log($objArticle_perso, 'objArticle_perso');

            if ($objArticle_perso['id']=="0") {
                $IdArticle_perso = $this->mdlfestival_perso->insert_article_perso($objArticle_perso);
            } else {
                $IdArticle_perso = $this->mdlfestival_perso->update_article_perso($objArticle_perso);
            }

            redirect('front/festival/personnalisation');

            //}
        }

    }
            function festival_perso_details($id_festival)
            {

                $data['current_page'] = "details_event";
                $toVille = $this->mdlville->GetFestivalVilles_pvc();//get ville list of agenda
                $data['toVille'] = $toVille;
                $toCategorie_principale = $this->mdlfestival->GetFestivalCategorie();
                $data['toCategorie_principale'] = $toCategorie_principale;
                //var_dump($toCategorie_principale);

                $data['zCouleurBgBouton'] = $this->input->get("zCouleurBgBouton");
                $data['zCouleurNbBtn'] = $this->input->get("zCouleurNbBtn");
                $data['zCouleurTextBouton'] = $this->input->get("zCouleurTextBouton");
                $data['zCouleur'] = $this->input->get("zCouleur");
                $data['zCouleurTitre'] = $this->input->get("zCouleurTitre");

                $id_agenda = $this->uri->rsegment(3); //die($id_agenda);
                if (isset($id_agenda)) $id_agenda = (intval($id_agenda));
                else $id_agenda = 0;

                $oDetailAgenda = $this->mdlfestival->GetById_IsActif($id_festival);

                $data['oDetailAgenda'] = $oDetailAgenda;//var_dump($oDetailAgenda); die();

                if (isset($oDetailAgenda->id)) {

                    //increment "accesscount" on agenda table
                    $this->mdlfestival->Increment_accesscount($oDetailAgenda->id);

                    //send info commercant to view
                    $oInfoCommercant = $this->mdlcommercant->infoCommercant($oDetailAgenda->IdCommercant);
                    $data['oInfoCommercant'] = $oInfoCommercant;

                    $toDepartement = $this->mdldepartement->GetFestivalDepartements_pvc();
                    $data['toDepartement'] = $toDepartement;

                    $data["pagecategory"] = "agenda";
                    $data["main_menu_content"] = "agenda";
                    $data["mdlbonplan"] = $this->mdlbonplan;

                    $data["mdl_localisation"] = $this->mdl_localisation;
                    $data["mdlville"] = $this->mdlville;
                    $data["mdl_article_organiser"] = $this->mdl_festival_organiser;

                    $data["pagecategory_partner"] = "agenda_partner";

                    //$data['toArticle_datetime'] = $this->mdl_agenda_datetime->getByAgendaId($id_agenda);

                    $data['currentpage'] = "export_festival";

                    //$this->load->view('privicarte/details_event', $data);
                    $this->load->view('export/festival_perso_details', $data);
                } else {
                    redirect('front/festival/festival_perso');
                }


            }

            function details_event_contact($id_festival, $contact_display = "contact")
            {

                $data['current_page'] = "details_event";
                $toVille = $this->mdlville->GetFestivalVilles_pvc();//get ville list of agenda
                $data['toVille'] = $toVille;
                $toCategorie_principale = $this->mdlfestival->GetFestivalCategorie();
                $data['toCategorie_principale'] = $toCategorie_principale;
                //var_dump($toCategorie_principale);

                $id_agenda = $this->uri->rsegment(3);

                $oDetailAgenda = $this->mdlfestival->GetById_IsActif($id_festival);
                $data['oDetailAgenda'] = $oDetailAgenda;

                //increment "accesscount" on agenda table
                $this->mdlfestival->Increment_accesscount($oDetailAgenda->id);

                //send info commercant to view
                $oInfoCommercant = $this->mdlcommercant->infoCommercant($oDetailAgenda->IdCommercant);
                $data['oInfoCommercant'] = $oInfoCommercant;

                $toDepartement = $this->mdldepartement->GetFestivalDepartements_pvc();
                $data['toDepartement'] = $toDepartement;

                $data["pagecategory"] = "festival";
                $data["main_menu_content"] = "festival";
                $data["mdlbonplan"] = $this->mdlbonplan;
                $data['contact_display'] = $contact_display;

                $this->load->view('privicarte/details_event_contact', $data);

            }


        }











