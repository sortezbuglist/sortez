<?php

class packarticle extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model("user");
        $this->load->library('user_agent');
        $this->load->model("sousRubrique");
        $this->load->model("Rubrique");
        $this->load->model("AssCommercantAbonnement");
        $this->load->model("AssCommercantSousRubrique");
        $this->load->model("Abonnement");
        $this->load->model("mdlannonce");
        $this->load->model("mdlbonplan");
        $this->load->model("Abonnement");
        $this->load->model("mdl_agenda");
        $this->load->model("mdlville");
        $this->load->Model("mdldepartement");
        $this->load->Model("mdlcommune");
        $this->load->Model("mdl_lightbox_mail");
        $this->load->model('pack_test_validation');
        $this->load->model("Commercant");
        $this->load->model("mdl_types_agenda");
        $this->load->model("mdl_categories_agenda");
        $this->load->model("mdlarticle");

        $this->load->model("packarticle_order");
        $this->load->model("packarticle_object");
        $this->load->model("packarticle_pack");
        $this->load->model("packarticle_payment");
        $this->load->model("packarticle_planing");
        $this->load->model("packarticle_price");
        $this->load->model("packarticle_quota");
        $this->load->model("packarticle_type");
        $this->load->model("packarticle_user");
        $this->load->model("packarticle_article");

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");

        $group_proo_club = array(3, 4, 5);

        if ($this->ion_auth->is_admin()) {
            redirect('admin/home', 'refresh');
        } else if (!$this->ion_auth->in_group($group_proo_club)) {
            redirect('auth/login', 'refresh');
        } else if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }

        //check_vivresaville_id_ville();

    }

    function index()
    {
        $packarticle_article_submit = $this->input->post('packarticle_article_submit');
        if (isset($packarticle_article_submit)&&$packarticle_article_submit!=false) {
            $packarticle_order_update['planing_id'] = $this->input->post('packarticle_planing_id');
            $packarticle_order_update['title'] = $this->input->post('packarticle_article_title');
            $packarticle_order_update['id'] = $this->input->post('packarticle_article_id');
            $packarticle_order_update['date_update'] = date("Y-m-d");
            $packarticle_order_id_update = $this->packarticle_article->update($packarticle_order_update);
            if (isset($packarticle_order_id_update) && is_numeric($packarticle_order_id_update)) {
                $data['mssg_confirm_update']='1';
            }
        }
        $data['empty'] = null;
        $data['pagecategory'] = 'packarticle';
        $data['packarticle_order_value'] = $this->packarticle_order->getWhere(" user_id = ".$this->ion_auth->user()->row()->id." and status_id=2 order by id desc" );
        $data['packarticle_pack_allpack'] = $this->packarticle_pack->getAll();
        $data['packarticle_payment'] = $this->packarticle_payment->getAll();
        $data['packarticle_object'] = $this->packarticle_object->getAll();
        $data['packarticle_planing'] = $this->packarticle_planing->getAll();
        $data['packarticle_article'] = $this->packarticle_article;
        $data['packarticle_pack'] = $this->packarticle_pack;
        $data['packarticle_quota'] = $this->packarticle_quota;
        $data['mdlarticle'] = $this->mdlarticle;
        $this->load->view('packarticle/index', $data);
    }

    function add_article_into_sortez(){
        $packarticle_article_id = $this->input->post('packarticle_article_id');
        if (isset($packarticle_article_id) && is_numeric($packarticle_article_id)) $packarticle_article_data = $this->packarticle_article->getById($packarticle_article_id);
        $packarticle_article_title = $this->input->post('packarticle_article_title');

        $objArticle = array();
        $user_ion_auth = $this->ion_auth->user()->row();
        $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $oInfoCommercant = $this->Commercant->GetById($commercant_UserId);
        $objArticle["IdCommercant"] = $commercant_UserId;
        $objArticle["IdUsers_ionauth"] = $user_ion_auth->id;
        $objArticle["nom_societe"] = $oInfoCommercant->NomSociete;
        $objArticle["nom_manifestation"] = $packarticle_article_title;
        $objArticle["organisateur"] = $oInfoCommercant->NomSociete;
        $objArticle["IdVille"] = $oInfoCommercant->IdVille;
        $objArticle["IdVille_localisation"] = $oInfoCommercant->IdVille;
        $objArticle["codepostal"] = $oInfoCommercant->CodePostal;
        $objArticle["codepostal_localisation"] = $oInfoCommercant->CodePostal;
        $objArticle["email"] = $oInfoCommercant->Email;
        $objArticle["date_depot"] = date("Y-m-d");
        $objArticle["article_categid"] = '32';
        $objArticle["article_subcategid"] = '286';
        $IdUpdatedArticle = $this->mdlarticle->insert($objArticle);
        $data['error'] = null;
        if(isset($IdUpdatedArticle) && is_numeric($IdUpdatedArticle) && isset($packarticle_article_data)) {
            $packarticle_article_data = (array)$packarticle_article_data;
            $packarticle_article_data['article_sortez_id'] = $IdUpdatedArticle;
            $packarticle_article_data['in_sortez'] = '1';
            $packarticle_article_update_id = $this->packarticle_article->update($packarticle_article_data);
            if(isset($packarticle_article_update_id) && is_numeric($packarticle_article_update_id)) $data['error'] = null;
            else $data['error'] = "Erreur ! une erreur est survenue, veuillez contacter un administrateur.";
        } else {
            $data['error'] = "Erreur ! l'article n'a pas pu être créé chez Sortez, veuillez contacter un administrateur.";
        }
        echo json_encode((array)$data);
    }

    function get_article_into_sortez(){
        $article_id = $this->input->post('article_id');
        $packarticle_article = $this->packarticle_article->getById($article_id);
        if (isset($packarticle_article->article_sortez_id) && $packarticle_article->article_sortez_id!="") {
            echo base_url()."front/articles/fiche/".$this->ion_auth_used_by_club->get_commercant_id_from_ion_id($this->ion_auth->user()->row()->id)."/".$packarticle_article->article_sortez_id;
        } else {
            echo "error";
        }
    }

    function saveorder()
    {
        $pack_post = $this->input->post('packarticle_pack_id');
        $data['empty']=null;
        if (isset($pack_post) && $pack_post!="" && $pack_post!="0") {

            $packarticle_order['pack_id'] = $this->input->post('packarticle_pack_id');
            $packarticle_order['payment_id'] = $this->input->post('packarticle_payment_id');
            $packarticle_order['object_id'] = $this->input->post('packarticle_object_id');
            $packarticle_order['user_id'] = $this->ion_auth->user()->row()->id;
            $packarticle_order['status_id'] = '1';
            $packarticle_order['creation_date'] = date("Y-m-d");
            $packarticle_order['id'] = 0;

            if (isset($packarticle_order['pack_id']) && $packarticle_order['pack_id']!="") {
                $oInfoPack = $this->packarticle_pack->getById($packarticle_order['pack_id']);
                $data['oInfoPack'] =  $oInfoPack;
                if (isset($oInfoPack) && is_object($oInfoPack)) {
                    $oInfoQuota = $this->packarticle_quota->getById($oInfoPack->quota_id);
                    $data['oInfoQuota'] =  $oInfoQuota;
                }
            }
            $data['oInfoCommercant'] = $this->Commercant->GetWhere(" user_ionauth_id=".$this->ion_auth->user()->row()->id);

            $notification_to = "contact@sortez.org";
            $notification_from = "postmaster@sortez.org";
            $notification_object = "Demande de quota Packarticle";
            $notification_message_content = $this->load->view('packarticle/mail/order_request',$data, true);
            $notification_colDestAdmin = array();
            $notification_colDestAdmin[] = array("Email"=>$notification_to,"Name"=>'Contact');


            //check if an order already exist
            $packarticle_order_check_exist = $this->packarticle_order->getWhere(" user_id = ".$this->ion_auth->user()->row()->id." order by id desc limit 1" );
            if (isset($packarticle_order_check_exist) && count($packarticle_order_check_exist)>0) {
                foreach ($packarticle_order_check_exist as $item) {
                    if (isset($item->status_id) && $item->status_id=='1') {
                        $this->load->view('packarticle/alert_order_exist', $data);
                    } elseif (isset($item->status_id) && $item->status_id=='2') {
                        //check if there are article not used related order
                        $packarticle_article_check_exist = $this->packarticle_article->getWhere(" order_id = ".$item->id." and status_id = '0' order by id desc" );
                        if (isset($packarticle_article_check_exist) && count($packarticle_article_check_exist)>0) {
                            $this->load->view('packarticle/alert_empty_article_exist', $data);
                        } else {
                            $pack_id = $this->packarticle_order->insert($packarticle_order);
                            if(isset($pack_id) && is_numeric($pack_id)) {
                                $this->load->view('packarticle/alert_save_order_success', $data);
                                @envoi_notification($notification_colDestAdmin,$notification_object,$notification_message_content,$notification_from);
                            } else $this->load->view('packarticle/alert_save_order_error', $data);
                        }
                    } elseif (isset($item->status_id) && $item->status_id=='3') {
                        $pack_id = $this->packarticle_order->insert($packarticle_order);
                        if(isset($pack_id) && is_numeric($pack_id)) {
                            $this->load->view('packarticle/alert_save_order_success', $data);
                            @envoi_notification($notification_colDestAdmin,$notification_object,$notification_message_content,$notification_from);
                        } else $this->load->view('packarticle/alert_save_order_error', $data);
                    }
                }
            } else {
                $pack_id = $this->packarticle_order->insert($packarticle_order);
                if(isset($pack_id) && is_numeric($pack_id)) {
                    $this->load->view('packarticle/alert_save_order_success', $data);
                    @envoi_notification($notification_colDestAdmin,$notification_object,$notification_message_content,$notification_from);
                } else $this->load->view('packarticle/alert_save_order_error', $data);
            }
        } else {
            $this->load->view('packarticle/alert_save_order_error', $data);
        }
    }

    function order_list() {
        $id_user = $this->ion_auth->user()->row()->id;
        $data['packarticle_order_value'] = $this->packarticle_order->getWhere('user_id='.$id_user.' order by id DESC');
        //echo 'test';
        //$this->load->view('packarticle/alert_save_order_error', $data);
        $this->load->view('packarticle/order_list_vw', $data);
    }

}
?>