<?php
class newsletter extends CI_Controller {
    
    function __construct() {
        parent::__construct();

        $this->load->library('session');
        $this->load->model("mdl_lightbox_mail");
        $this->load->library('ion_auth');
        $this->load->model("ion_auth_model");
        $this->load->model("ion_auth_used_by_club");
        $this->load->model("newsletter_unsubscribe");

        check_vivresaville_id_ville();
		
    }

    function index(){
        redirect("front/utilisateur/newsletter");
    }


	function unsubscribe ($zMail=""){
        // link format : http://localhost/sortez/front/newsletter/unsubscribe/randawilly:AT:qsdf:POINT:jkj
        $zMail = str_replace(":AT:","@",$zMail);
        $zMail = str_replace(":POINT:",".",$zMail);
        $mail_list = $this->newsletter_unsubscribe->get_by_mail($zMail);
        $prmData = array();
        if($mail_list==false){
            $prmData['id']=null;
            $prmData['mail']=$zMail;
            $mail_list = $this->newsletter_unsubscribe->insert($prmData);
        }
        $data['mssg'] = $zMail;
        $this->load->view("confirmation/confirm_unsubscribe", $data);
    }


	function genererLettreAleatoir ($long = 4)
	{
		$alphaB = "abcdefghijklmnopqrstuvwyxz";
		return substr(str_shuffle($alphaB), 0, $long);
	}

}