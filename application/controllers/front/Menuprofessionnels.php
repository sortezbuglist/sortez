<?php
class menuprofessionnels extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('session');

        check_vivresaville_id_ville();

    }
    
    function index(){
        $data["no_main_menu_home"] = "1";
        $this->load->view('front/vwMenuprofessionnels', $data) ;
    }

    function fonctionnement(){
        $this->load->view('front/vwFonctionnement') ;
    }
    
    function abonnements(){
        $data["no_main_menu_home"] = "1";
        $data["no_left_menu_home"] = "1";
        $this->load->view('front/vwAbonnements', $data) ;
    }
}