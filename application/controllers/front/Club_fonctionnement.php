<?php
class club_fonctionnement extends CI_Controller {
    
    function __construct() {
        parent::__construct();
		$this->load->model("mdlcommercant") ;

        $this->load->library('session');

        check_vivresaville_id_ville();

    }
    
    function index(){
		
        $this->load->view('front/vwclubfonctionnement') ;
    }

   
}