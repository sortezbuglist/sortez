<?php
class bonsplans extends CI_Controller {
    
    function __construct() {
        parent::__construct();

        $this->load->library('session');

        check_vivresaville_id_ville();
    }
    
    function index(){
        $this->load->view('front/vwBonsplans') ;
    }
    
    function game(){        
        $this->load->view('front/vwGame') ;
    }
    
}