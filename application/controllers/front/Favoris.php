<?php
class favoris extends CI_Controller {
    
    function __construct() {
        parent::__construct();
		$this->load->library('session');

        check_vivresaville_id_ville();
    }
    
    function add($prmIdUser = "", $prmIdCommercant = "") {
        if($prmIdUser != "" && $prmIdCommercant != "") {
            $this->db->where("IdUser",$prmIdUser);
            $this->db->where("IdCommercant",$prmIdCommercant);
            $colAssUserCommercant = $this->db->get("ass_commercants_users");
            $objAssUserCommercant = $colAssUserCommercant->row_object();
            if(sizeof($objAssUserCommercant) > 0) {
                $objFavoris["IdAssCommercantUser"] = $objAssUserCommercant->IdAssCommercantUser;
                $objFavoris["Favoris"] = 1;
                $this->load->model("user");
                $objFavoris = $this->user->UpdateFavoris($objFavoris);
            } else {
                $objFavoris["IdUser"] = $prmIdUser;
                $objFavoris["IdCommercant"] = $prmIdCommercant;
                $objFavoris["Favoris"] = 1;
                $this->load->model("user");
                $objFavoris = $this->user->AddFavoris($objFavoris);
            }
            redirect();
        } else {
            $this->session->set_userdata("cur_uri", $this->uri->uri_string());
            $this->session->set_flashdata('domain_from', '1');
			redirect("connexion");
        }
    }
}