<?php
class upload extends CI_Controller {
    
    function __construct() {
        parent::__construct();

        $this->load->library('session');

        check_vivresaville_id_ville();
		
    }
    /*
    function index(){
			
    }
	*/
	function index ($zNomChamp, $zExtension = "", $zExistNomImage = ""){
	
        $error = "" ;
        $msg = "" ;
        $srcFile = "" ;        
		$path = "application/resources/front/images/" ;				
        if (file_exists($_FILES["$zNomChamp"]["tmp_name"])){
            $fileType = $_FILES["$zNomChamp"]["type"];
            if ($fileType == "image/gif" || $fileType == "image/GIF" || $fileType == "image/jpeg" || $fileType == "image/JPEG" || $fileType == "image/bmp" || $fileType == "image/BMP" || $fileType == "image/tiff" || $fileType == "TIFF" || $fileType == "image/pjpeg" || $fileType == "image/PJPEG" || $fileType == "image/png" || $fileType == "image/PNG") {
                $srcFile = $this->genererLettreAleatoir(4) . rand(0,9999) . "." . $zExtension ;				
                if(move_uploaded_file($_FILES["$zNomChamp"]["tmp_name"], $path . basename($srcFile))) {
					if ($zExistNomImage != "" && file_exists($path.$zExistNomImage)){
						unlink($path.$zExistNomImage) ;
					}
                                        //resizing image with witdth > 560
                                        if(file_exists($path.basename($srcFile))) {
                                        $size_mage_image = getimagesize($path.basename($srcFile));
                                        if ($size_mage_image[0]>560) resize_file_to($path.basename($srcFile), 560, 560,'','');
                                        }
                                        //resizing image with witdth > 560
                } else {
                    $error .= "D&eacute;sol&eacute;, il y avait un erreur lors du t&eacute;l&eacute;chargement de votre fichier.";
                }
            } else {
                $error = "Veuillez entrer une image";
            }
        } else {
            $error = "Le fichier n&apos;a pas &eacute;t&eacute; t&eacute;l&eacute;charg&eacute;";
        }

        $str = "{";
        $str .= "error: '" . $error . "',\n";
        $str.="msg: '" . $path . $srcFile . "'\n";
        $str .= "}";
        echo $str;
    }

	function genererLettreAleatoir ($long = 4)
	{
		$alphaB = "abcdefghijklmnopqrstuvwyxz";
		return substr(str_shuffle($alphaB), 0, $long);
	}
    function getimage(){
        header('Access-Control-Allow-Origin: *');
        $target_path = "application/resources/front/images/";

        $target_path = $target_path . basename( $_FILES['file']['name']);

        if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
            echo "Upload and move success";
        } else {
            echo $target_path;
            echo "There was an error uploading the file, please try again!";
        }
        return json_encode($target_path);
    }
    function upload_logo(){
        header('Access-Control-Allow-Origin: *');
        $target_path = "application/resources/front/images/logo_mobile/";

        $target_path = $target_path . basename( $_FILES['file']['name']);

        if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
            echo "Upload and move success";
        } else {
            echo $target_path;
            echo "There was an error uploading the file, please try again!";
        }
        return json_encode($target_path);
    }
}