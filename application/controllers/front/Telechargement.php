<?php
class telechargement extends CI_Controller {
    
    function __construct() {
        parent::__construct();
		$this->load->helper('download');

        $this->load->library('session');

        check_vivresaville_id_ville();
    }
    
    function document($zNomfichier){
		$zRepertoire = RootPath() . "/application/resources/front/photoCommercant/images" ;    		
		$nom_fichier =  $zRepertoire . "/" . $zNomfichier ;   				
		$data = file_get_contents($nom_fichier);  
		force_download($nom_fichier, $data);  
    }
}