<?php
class bonplan extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model("mdlbonplan") ;
        $this->load->model("mdlcategorie") ;
        $this->load->model("mdlville") ;
        $this->load->model("mdlbonplanpagination") ;
        $this->load->model("mdlcommercant") ;
        $this->load->library('pagination');
        $this->load->model("AssCommercantAbonnement") ;
        $this->load->model("mdlannonce") ;
        $this->load->model("Abonnement");
        $this->load->model("rubrique") ;
		
		$this->load->library('session');

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");
    }
    
    function index(){
        $argOffset = (isset($_SESSION["argOffset"])) ? $_SESSION["argOffset"] : 0 ;
        
        if ($this->input->post("inputStringHidden") == "0") unset($_SESSION['iCategorieId']);
        if ($this->input->post("inputStringCommercantHidden") == "0") unset($_SESSION['iCommercantId']);
        if ($this->input->post("zMotCle") == "") unset($_SESSION['zMotCle']);
        if ($this->input->post("inputStringVilleHidden") == "") unset($_SESSION['iVilleId']);
        if ($this->input->post("inputStringOrderByHidden") == "0") unset($_SESSION['iOrderBy']);
        if ($this->input->post("inputFromGeolocalisation") == "0" || $this->input->post("inputFromGeolocalisation") == "1") unset($_SESSION['inputFromGeolocalisation']);
        if ($this->input->post("inputGeolocalisationValue") == "10") unset($_SESSION['inputGeolocalisationValue']);
        if ($this->input->post("inputGeoLatitude") == "0") unset($_SESSION['inputGeoLatitude']);
        if ($this->input->post("inputGeoLongitude") == "0") unset($_SESSION['inputGeoLongitude']);
        
        
        $data["iFavoris"] = $this->input->post("hdnFavoris");
        if(isset($_POST["inputStringHidden"])){
            unset($_SESSION['iCategorieId']);
            unset($_SESSION['iVilleId']);
            unset($_SESSION['iCommercantId']);
            unset($_SESSION['zMotCle']);
            unset($_SESSION['iOrderBy']);
            unset($_SESSION['inputFromGeolocalisation']);
            unset($_SESSION['inputGeolocalisationValue']);
            unset($_SESSION['inputGeoLatitude']);
            unset($_SESSION['inputGeoLongitude']);
            
            //$iCategorieId = $_POST["inputStringHidden"] ; 
            $iCategorieId_all0 = $this->input->post("inputStringHidden");
            if (isset($iCategorieId_all0) && $iCategorieId_all0 != "" && $iCategorieId_all0 != NULL && $iCategorieId_all0 != '0') {
                $iCategorieId_all = substr($iCategorieId_all0,1);
                $iCategorieId = explode(',', $iCategorieId_all);
            } else {
                $iCategorieId = '0';
            } 
            if(isset($_POST["inputStringVilleHidden"]))  $iVilleId = $_POST["inputStringVilleHidden"] ; else $iVilleId = 0;
            if(isset($_POST["inputStringCommercantHidden"])) $iCommercantId = $_POST["inputStringCommercantHidden"] ; else  $iCommercantId = 0;
            if (isset($_POST["zMotCle"])) $zMotCle = $_POST["zMotCle"] ; else $zMotCle = "";
            if (isset($_POST["inputStringOrderByHidden"])) $iOrderBy = $_POST["inputStringOrderByHidden"] ; else $iOrderBy = '';
            if (isset($_POST["inputFromGeolocalisation"])) $inputFromGeolocalisation = $_POST["inputFromGeolocalisation"] ; else $inputFromGeolocalisation = "0";
            if (isset($_POST["inputGeolocalisationValue"])) $inputGeolocalisationValue = $_POST["inputGeolocalisationValue"] ; else $inputGeolocalisationValue = "10";
            if (isset($_POST["inputGeoLatitude"])) $inputGeoLatitude = $_POST["inputGeoLatitude"] ; else $inputGeoLatitude = "0";
            if (isset($_POST["inputGeoLongitude"])) $inputGeoLongitude = $_POST["inputGeoLongitude"] ; else $inputGeoLongitude = "0";
            
            $_SESSION['iCategorieId'] = $iCategorieId;
            $_SESSION['iVilleId'] = $iVilleId;
            $_SESSION['iCommercantId'] = $iCommercantId;
            $_SESSION['zMotCle'] = $zMotCle;
            $_SESSION['iOrderBy'] = $iOrderBy;
            $_SESSION['inputFromGeolocalisation'] = $inputFromGeolocalisation;
            $_SESSION['inputGeolocalisationValue'] = $inputGeolocalisationValue;
            $_SESSION['inputGeoLatitude'] = $inputGeoLatitude;
            $_SESSION['inputGeoLongitude'] = $inputGeoLongitude;
            
            $data['iCategorieId'] = $iCategorieId ;
            $data['iVilleId'] = $iVilleId ;
            $data['iCommercantId'] = $iCommercantId ;
            $data['zMotCle'] = $zMotCle ;
            $data['iOrderBy'] = $iOrderBy ;
            $toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche($_SESSION['iCategorieId'], $_SESSION['iCommercantId'], $_SESSION['zMotCle'], $data["iFavoris"], 0, 10000, $_SESSION['iVilleId'], $iOrderBy, $_SESSION['inputFromGeolocalisation'], $_SESSION['inputGeolocalisationValue'], $_SESSION['inputGeoLatitude'], $_SESSION['inputGeoLongitude']) ;
            $TotalRows = count($this->mdlbonplan->listeBonPlanRecherche($_SESSION['iCategorieId'], $_SESSION['iCommercantId'], $_SESSION['zMotCle'], $data["iFavoris"], 0, 10000, $_SESSION['iVilleId'], $iOrderBy, $_SESSION['inputFromGeolocalisation'], $_SESSION['inputGeolocalisationValue'], $_SESSION['inputGeoLatitude'], $_SESSION['inputGeoLongitude']));
        }else{
            $data["iFavoris"] = "";
            //$toListeBonPlan = $this->mdlbonplan->listeBonPlan(0, 10000) ;
            //$TotalRows = count($this->mdlbonplan->listeBonPlan(0, 10000));
            $iCategorieId = (isset($_SESSION['iCategorieId'])) ? $_SESSION['iCategorieId'] : "" ;
            $iVilleId = (isset($_SESSION['iVilleId'])) ? $_SESSION['iVilleId'] : 0 ;
            $iCommercantId = (isset($_SESSION['iCommercantId'])) ? $_SESSION['iCommercantId'] : "" ;
            $zMotCle = (isset($_SESSION['zMotCle'])) ? $_SESSION['zMotCle'] : "" ;
            $iOrderBy = (isset($_SESSION['iOrderBy'])) ? $_SESSION['iOrderBy'] : "" ;
            $inputFromGeolocalisation = (isset($_SESSION['inputFromGeolocalisation'])) ? $_SESSION['inputFromGeolocalisation'] : "0" ;
            $inputGeolocalisationValue = (isset($_SESSION['inputGeolocalisationValue'])) ? $_SESSION['inputGeolocalisationValue'] : "10" ;
            $inputGeoLatitude = (isset($_SESSION['inputGeoLatitude'])) ? $_SESSION['inputGeoLatitude'] : "0" ;
            $inputGeoLongitude = (isset($_SESSION['inputGeoLongitude'])) ? $_SESSION['inputGeoLongitude'] : "0" ;
            $toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche($iCategorieId, $iCommercantId, $zMotCle, $data["iFavoris"], 0, 10000, $iVilleId, $iOrderBy, $inputFromGeolocalisation, $inputGeolocalisationValue, $inputGeoLatitude, $inputGeoLongitude) ;
            $TotalRows = count($this->mdlbonplan->listeBonPlanRecherche($iCategorieId, $iCommercantId, $zMotCle, $data["iFavoris"], 0, 10000, $iVilleId, $iOrderBy, $inputFromGeolocalisation, $inputGeolocalisationValue, $inputGeoLatitude, $inputGeoLongitude));
        }

        //$this->firephp->log($iOrderBy, 'iOrderBy');

        $this->load->model("mdlcategorie") ;
	    $this->load->model("mdlville") ;
        $toCategorie= $this->mdlcategorie->GetBonplanSouscategorie();
        $toCategoriePrincipale= $this->mdlcategorie->GetBonplanCategoriePrincipale();
        $toVille= $this->mdlville->GetBonplanVilles();
        $data['toVille'] = $toVille ;
        $data['toCategorie'] = $toCategorie ;
        $data['toCategoriePrincipale'] = $toCategoriePrincipale ;
        $toCommercant= $this->mdlcommercant->GetAllCommercant_with_bonplan();
	    $data['toCommercant'] = $toCommercant ;
//        $data['toListeBonPlan'] = $toListeBonPlan ;
        // updated by W 09/12/2011
        //$TotalRows = $this->mdlbonplanpagination->Compter();// mdlbonplanpagination doesn't contain $_POST's values
        $PerPage = 15;

        $iNombreLiens = $TotalRows / $PerPage ;
        if($iNombreLiens > round($iNombreLiens)){
                $iNombreLiens = round($iNombreLiens) + 1 ;
        }
        else {
                $iNombreLiens = round($iNombreLiens) ;
        }

        $data["iNombreLiens"] = $iNombreLiens ;

        $data["PerPage"] = $PerPage ;
        $data["TotalRows"] = $TotalRows ;
        $data["argOffset"] = $argOffset ;
        $argOffset_limit = round($argOffset) + $PerPage;
        //$toListeBonPlan= $this->mdlbonplanpagination->GetListeBonplanPagination($PerPage, $argOffset);// mdlbonplanpagination doesn't contain $_POST's values
        if(isset($_POST["inputStringHidden"])){
            unset($_SESSION['iCategorieId']);
            unset($_SESSION['iVilleId']);
            unset($_SESSION['iCommercantId']);
            unset($_SESSION['zMotCle']);
            unset($_SESSION['iOrderBy']);
            unset($_SESSION['inputFromGeolocalisation']);
            unset($_SESSION['inputGeolocalisationValue']);
            unset($_SESSION['inputGeoLatitude']);
            unset($_SESSION['inputGeoLongitude']);
            
            //$iCategorieId = $_POST["inputStringHidden"] ; 
            $iCategorieId_all0 = $this->input->post("inputStringHidden");
            if (isset($iCategorieId_all0) && $iCategorieId_all0 != "" && $iCategorieId_all0 != NULL && $iCategorieId_all0 != '0') {
                $iCategorieId_all = substr($iCategorieId_all0,1);
                $iCategorieId = explode(',', $iCategorieId_all);
            } else {
                $iCategorieId = '0';
            }
            if(isset($_POST["inputStringVilleHidden"]))  $iVilleId = $_POST["inputStringVilleHidden"] ; else $iVilleId = 0;
            if(isset($_POST["inputStringCommercantHidden"])) $iCommercantId = $_POST["inputStringCommercantHidden"] ; else $iCommercantId = 0;
            if (isset($_POST["zMotCle"])) $zMotCle = $_POST["zMotCle"] ; else $zMotCle = "";
            if (isset($_POST["inputStringOrderByHidden"])) $iOrderBy = $_POST["inputStringOrderByHidden"] ; else $iOrderBy = '';
            if (isset($_POST["inputFromGeolocalisation"])) $inputFromGeolocalisation = $_POST["inputFromGeolocalisation"] ; else $inputFromGeolocalisation = "0";
            if (isset($_POST["inputGeolocalisationValue"])) $inputGeolocalisationValue = $_POST["inputGeolocalisationValue"] ; else $inputGeolocalisationValue = "10";
            if (isset($_POST["inputGeoLatitude"])) $inputGeoLatitude = $_POST["inputGeoLatitude"] ; else $inputGeoLatitude = "0";
            if (isset($_POST["inputGeoLongitude"])) $inputGeoLongitude = $_POST["inputGeoLongitude"] ; else $inputGeoLongitude = "0";
            
            $_SESSION['iCategorieId'] = $iCategorieId;
            $_SESSION['iVilleId'] = $iVilleId;
            $_SESSION['iCommercantId'] = $iCommercantId;
            $_SESSION['zMotCle'] = $zMotCle;
            $_SESSION['iOrderBy'] = $iOrderBy;
            $_SESSION['inputFromGeolocalisation'] = $inputFromGeolocalisation;
            $_SESSION['inputGeolocalisationValue'] = $inputGeolocalisationValue;
            $_SESSION['inputGeoLatitude'] = $inputGeoLatitude;
            $_SESSION['inputGeoLongitude'] = $inputGeoLongitude;
            
            $data['iCategorieId'] = $iCategorieId ;
            $data['iVilleId'] = $iVilleId ;
            $data['iCommercantId'] = $iCommercantId ;
            $data['zMotCle'] = $zMotCle ;
            $data['iOrderBy'] = $iOrderBy ;
            //$toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche($iCategorieId, $iVilleId, $zMotCle, $data["iFavoris"], $argOffset, $PerPage) ;
            $toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche($_SESSION['iCategorieId'], $_SESSION['iCommercantId'], $_SESSION['zMotCle'], $data["iFavoris"], $argOffset, $PerPage, $_SESSION['iVilleId'], $iOrderBy, $_SESSION['inputFromGeolocalisation'], $_SESSION['inputGeolocalisationValue'], $_SESSION['inputGeoLatitude'], $_SESSION['inputGeoLongitude']) ;//, $argOffset, $PerPage
            
        }else{
            $data["iFavoris"] = "";
            $iCategorieId = (isset($_SESSION['iCategorieId'])) ? $_SESSION['iCategorieId'] : "" ;
            $iVilleId = (isset($_SESSION['iVilleId'])) ? $_SESSION['iVilleId'] : 0 ;
            $iCommercantId = (isset($_SESSION['iCommercantId'])) ? $_SESSION['iCommercantId'] : "" ;
            $zMotCle = (isset($_SESSION['zMotCle'])) ? $_SESSION['zMotCle'] : "" ;
            $iOrderBy = (isset($_SESSION['iOrderBy'])) ? $_SESSION['iOrderBy'] : "" ;
            $inputFromGeolocalisation = (isset($_SESSION['inputFromGeolocalisation'])) ? $_SESSION['inputFromGeolocalisation'] : "0" ;
            $inputGeolocalisationValue = (isset($_SESSION['inputGeolocalisationValue'])) ? $_SESSION['inputGeolocalisationValue'] : "10" ;
            $inputGeoLatitude = (isset($_SESSION['inputGeoLatitude'])) ? $_SESSION['inputGeoLatitude'] : "0" ;
            $inputGeoLongitude = (isset($_SESSION['inputGeoLongitude'])) ? $_SESSION['inputGeoLongitude'] : "0" ;
            $toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche($iCategorieId, $iCommercantId, $zMotCle, $data["iFavoris"], $argOffset, $PerPage, $iVilleId, $iOrderBy, $inputFromGeolocalisation, $inputGeolocalisationValue, $inputGeoLatitude, $inputGeoLongitude) ;//, $argOffset, $PerPage
            
        }

        //$this->firephp->log($iOrderBy, 'iOrderBy_2');

        $data['toListeBonPlan'] = $toListeBonPlan ;
        $data['pagecategory'] = 'bonplan';
	// print_r($data['toCommercant']);exit();
        // End OP
        
        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            $data['IdUser'] = $iduser;
            $user_groups = $this->ion_auth->get_users_groups($user_ion_auth->id)->result();
            //$this->firephp->log($user_groups, 'user_groups');
            if ($user_groups[0]->id==1) $typeuser = "1";
            else if ($user_groups[0]->id==2) $typeuser = "0";
            else if ($user_groups[0]->id==3 || $user_groups[0]->id==4 || $user_groups[0]->id==5) $typeuser = "COMMERCANT";
            $data['typeuser'] = $typeuser;
        }


        //$this->load->view('front/vwListeBonPlan', $data) ;
        $this->load->view('front2013/bonplan_main', $data) ;
    }
    
    
    function check_category_list() {
            $inputStringVilleHidden_bonplans = $this->input->post("inputStringVilleHidden_bonplans");
            
            //$toCategorie= $this->mdlcategorie->GetCommercantSouscategorie();
            //$toVille= $this->mdlville->GetCommercantVilles();//get ville list of commercant
            if (isset($inputStringVilleHidden_bonplans) && $inputStringVilleHidden_bonplans != "" && $inputStringVilleHidden_bonplans != '0' && $inputStringVilleHidden_bonplans != NULL) {
                $toCategorie_principale= $this->mdlcategorie->GetBonplanCategoriePrincipaleByVille($inputStringVilleHidden_bonplans);
            } else {
                $toCategorie_principale= $this->mdlcategorie->GetBonplanCategoriePrincipale();
            }
            
            $result_to_show = '';
            
            if (isset($toCategorie_principale)) {
                foreach($toCategorie_principale as $oCategorie_principale){
                    $result_to_show .= '<div class="leftcontener2013title" style="margin-top:15px;">';
                    $result_to_show .= ucfirst(strtolower($oCategorie_principale->Nom))." (".$oCategorie_principale->nb_bonplan.")";
                    $result_to_show .= '</div>';

                    $result_to_show .= '<div class="leftcontener2013content" style="margin-bottom:15px;">';
                    $OCommercantSousRubrique = $this->mdlcategorie->GetBonplanSouscategorieByRubrique($oCategorie_principale->IdRubrique);
                    $result_to_show .= '<br/>';
                    
                    
                    if (isset($_SESSION['iCategorieId'])) {
                        $iCategorieId_sess = $_SESSION['iCategorieId'];
                    }
                    
                    
                    if (count($OCommercantSousRubrique)>0) {
                        $i_rand = 0;
                        foreach($OCommercantSousRubrique as $ObjCommercantSousRubrique){
                            $result_to_show .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>';
                            $result_to_show .= '<td width="22"><input  onClick="javascript:submit_search_bonplan();" name="check_part['.$i_rand.']" id="check_part_'.$ObjCommercantSousRubrique->IdSousRubrique.'" type="checkbox" value="'.$ObjCommercantSousRubrique->IdSousRubrique.'"';
                            if (isset($iCategorieId_sess) && is_array($iCategorieId_sess)) {
                                if (in_array($ObjCommercantSousRubrique->IdSousRubrique, $iCategorieId_sess)) $result_to_show .= 'checked';
                            }
                            $result_to_show .= '></td><td>';
                            $result_to_show .= ucfirst(strtolower($ObjCommercantSousRubrique->Nom)).' ('.$ObjCommercantSousRubrique->nb_bonplan.')</td>';
                            $result_to_show .= '</tr></table>';
                            $i_rand++;
                        }
                    }
                    $result_to_show .= '</div>';
                }
            }
            
            echo $result_to_show;
            
        }
        
        
        function redirect_bonplan(){
                unset($_SESSION['iCategorieId']);
                unset($_SESSION['iCommercantId']);
                unset($_SESSION['iVilleId']);
                unset($_SESSION['zMotCle']);
                redirect("front/bonplan/");
        }
    
    function ficheBonplan($_iDCommercant = 0, $_iDBonplan = 0){


        if(!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        } else if ($this->ion_auth->in_group(2)) {
            redirect();
        }
   
        
        //commercant cannot have 2 BonPlan
        if ($_iDBonplan == 0) {
            $nb_bp_com = $this->mdlbonplan->bonPlanParCommercant($_iDCommercant);
            if (count($nb_bp_com) != 0) {
                redirect ("front/bonplan/listeMesBonplans/$_iDCommercant") ;
            }
        }
        
        $data["no_main_menu_home"] = "1";
        $data["no_left_menu_home"] = "1";
        
        
        //start verify if merchant subscription contain bonplan WILLIAM
        $current_date = date("Y-m-d");
        $query_abcom_cond = " IdCommercant = ".$_iDCommercant." AND ('".$current_date."' BETWEEN DateDebut AND DateFin)";
        $toAbonnementCommercant = $this->AssCommercantAbonnement->GetWhere($query_abcom_cond, 0, 1) ;
        if(sizeof($toAbonnementCommercant)) { 
            foreach ($toAbonnementCommercant as $oAbonnementCommercant) {
                        if(isset($oAbonnementCommercant) && ($oAbonnementCommercant->IdAbonnement==1 || $oAbonnementCommercant->IdAbonnement==3)) {//IdAbonnement doesn't contain annonce
                                redirect ("front/professionnels/fiche/$_iDCommercant") ;
                                }
            }
        } else redirect ("front/professionnels/fiche/$_iDCommercant") ;
        //end verify if merchant subscription contain bonplan WILLIAM
        
        
        
        $data["idCommercant"] = $_iDCommercant ;
        $data["colCategorie"] = $this->mdlcategorie->GetAll();
        $data["listVille"] = $this->mdlville->GetAll();
        if (IsValidId($_iDBonplan) && $_iDBonplan != 0){
            $data["title"] = "Modification Bonplan" ;
            $data["oBonplan"] = $this->mdlbonplan->GetBonplan($_iDBonplan);
            //$this->load->view('front/vwFicheBonplan', $data) ;
            $this->load->view('front2013/vwFicheBonplan', $data) ;
        }else{
            $data["title"] = "Créer un bonplan" ;
            //$this->load->view('front/vwFicheBonplan', $data) ;
            $this->load->view('front2013/vwFicheBonplan', $data) ;
        }
    }
    
    function creerBonplan($_iDCommercant = 0){
        $oBonplan = $this->input->post("bonplan") ;
        $oBonplan['bonplan_quantite_depart'] = $oBonplan['bonplan_quantite'];
        $IdInsertedBonplan = $this->mdlbonplan->insertBonplan($oBonplan);
        $data["title"] = "Modification bonplan" ;
        $data["idCommercant"] = $_iDCommercant ;
        $data["msg"] = "le bon plan a été bien crée" ;
        //$this->load->view('front/vwListeBonPlan', $data) ;
        // $this->load->view('front/vwFicheBonplan', $data) ;
        redirect ("front/bonplan/listeMesBonplans/$_iDCommercant") ;
    }
    
    function modifBonplan($_iDCommercant = 0){
        $oBonplan = $this->input->post("bonplan") ;

        $oBonplan['bonplan_quantite_depart'] = $oBonplan['bonplan_quantite'];
        
        if (isset($_FILES["bonplanPhoto1"])) {
            $photo1 =  $_FILES["bonplanPhoto1"]["name"];
            if($photo1 !=""){
                $photo1Associe = $photo1;
                    $bonplanPhoto1 = doUpload("bonplanPhoto1","application/resources/front/images/",$photo1Associe);
            }else {
                $photo1Associe = $this->input->post("photo1Associe");
                    $bonplanPhoto1 = $photo1Associe;
            }
        }

        if (isset($_FILES["bonplanPhoto2"])) { 
            $photo2 =  $_FILES["bonplanPhoto2"]["name"];
            if($photo2 !=""){
                $photo2Associe = $photo2;
                    $bonplanPhoto2 = doUpload("bonplanPhoto2","application/resources/front/images/",$photo2Associe);
            }else {
                $photo2Associe = $this->input->post("photo2Associe");
                    $bonplanPhoto2 = $photo2Associe;
            }
        }

        if (isset($_FILES["bonplanPhoto3"])) {
            $photo3 =  $_FILES["bonplanPhoto3"]["name"];
            if($photo3 !=""){
                $photo3Associe = $photo3;
                    $bonplanPhoto3 = doUpload("bonplanPhoto3","application/resources/front/images/",$photo3Associe);
            }else {
                $photo3Associe = $this->input->post("photo3Associe");
                    $bonplanPhoto3 = $photo3Associe;
            }
        }

        if (isset($_FILES["bonplanPhoto4"])) {
            $photo4 =  $_FILES["bonplanPhoto4"]["name"];
            if($photo4 !=""){
                $photo4Associe = $photo4;
                    $bonplanPhoto4 = doUpload("bonplanPhoto4","application/resources/front/images/",$photo4Associe);
            }else {
                $photo4Associe = $this->input->post("photo4Associe");
                    $bonplanPhoto4 = $photo4Associe;
            }
        }
        
        if (isset($bonplanPhoto1)) $oBonplan["bonplan_photo1"] = $bonplanPhoto1;
        if (isset($bonplanPhoto2)) $oBonplan["bonplan_photo2"] = $bonplanPhoto2;
        if (isset($bonplanPhoto3)) $oBonplan["bonplan_photo3"] = $bonplanPhoto3;
        if (isset($bonplanPhoto4)) $oBonplan["bonplan_photo4"] = $bonplanPhoto4;
        
        $IdUpdatedBonplan = $this->mdlbonplan->updateBonplan($oBonplan);
        $data["title"] = "Créer un Bonplan" ;
        $data["idCommercant"] = $_iDCommercant ;
        $data["msg"] = "le bon plan a été bien modifié" ;
        //$this->load->view('front/vwListeBonPlan', $data) ;
        // $this->load->view('front/vwFicheBonplan', $data) ;
        redirect ("front/bonplan/listeMesBonplans/$_iDCommercant") ;
    }
    
    function listeMesBonplans($_iDCommercant = 0){
        
        if(!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
		} else if ($this->ion_auth->in_group(2)) {
            redirect();
        }
        
        $data["no_main_menu_home"] = "1";
        $data["no_left_menu_home"] = "1";
        $data["idCommercant"] = $_iDCommercant ;
        
        
        //start verify if merchant subscription contain bonplan WILLIAM
        $current_date = date("Y-m-d");
        $query_abcom_cond = " IdCommercant = ".$_iDCommercant." AND ('".$current_date."' BETWEEN DateDebut AND DateFin)";
        $toAbonnementCommercant = $this->AssCommercantAbonnement->GetWhere($query_abcom_cond, 0, 1) ;
        if(sizeof($toAbonnementCommercant)) { 
            foreach ($toAbonnementCommercant as $oAbonnementCommercant) {
                        if(isset($oAbonnementCommercant) && ($oAbonnementCommercant->IdAbonnement==1 || $oAbonnementCommercant->IdAbonnement==3)) {//IdAbonnement doesn't contain annonce
                                redirect ("front/professionnels/fiche/$_iDCommercant") ;
                                }
            }
        } else redirect ("front/professionnels/fiche/$_iDCommercant") ;
        //end verify if merchant subscription contain bonplan WILLIAM
        
        
        if ($_iDCommercant != 0){
            $toListeMesBonplans = $this->mdlbonplan->listeMesBonplans($_iDCommercant) ;
            //$data['toListeMesBonplans'] = $toListeMesBonplans ;
            //$this->load->view('front/vwCommercantBonPlans', $data) ;
            if (count($toListeMesBonplans)==0) {
                redirect ("front/bonplan/ficheBonplan/$_iDCommercant");
            } else {
                redirect ("front/bonplan/ficheBonplan/$_iDCommercant/".$toListeMesBonplans->bonplan_id);
            }

        }
    }
    
    function supprimBonplan($_iDBonplan, $_iDCommercant){
        
        $zReponse = $this->mdlbonplan->supprimeBonplans($_iDBonplan) ;
        redirect ("front/bonplan/listeMesBonplans/$_iDCommercant") ;
    }

    function redirection($_iPage){
        $_SESSION["argOffset"] = $_iPage ;
        redirect("front/bonplan/");
    }
    
    function delete_files($prmIdBonplan = 0, $prmFiles) {
        $_objBonplan = $this->mdlbonplan->getById($prmIdBonplan);
        $this->load->helper('file'); 
        $path_parts_ = pathinfo("./application/resources/front/images/".$_objBonplan->$prmFiles);
        @unlink("./application/resources/front/images/".$_objBonplan->$prmFiles);//delete picture
        @unlink("./application/resources/front/images/".$path_parts_['filename']."_thumb_100_100.".$path_parts_['extension']);//delete thumb
        $_objBonplan->$prmFiles = null;
        $IdUpdatedBonplan = $this->mdlbonplan->updateBonplan($_objBonplan);
        
        return true;
    }

    function photosbonplanmobile ($bonplanId){
        $oDetailbonplan = $this->mdlbonplan->getById($bonplanId) ;
        $data['oDetailbonplan'] = $oDetailbonplan ;

        $_iCommercantId = $oDetailbonplan->bonplan_commercant_id;

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
        $data['oInfoCommercant'] = $oInfoCommercant ;
        //$data['active_link'] = "annonces";
        $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if (isset($oAssComRub)) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        //$this->load->view('front/vwPhotosannoncemobile', $data) ;
        $this->load->view('front2013/photosbonplanmobile', $data) ;
    }
    
}