<?php
class annoncespro extends CI_Controller {
    
    function __construct() {
        parent::__construct();

        $this->load->library('session');

        check_vivresaville_id_ville();
    }
    
    function index(){        
        $this->load->view('front/vwAnnoncespro') ;
    }    
    
    function detailgame(){        
        $this->load->view('front/vwAnnonces-game-detail') ;
    }
    
    function photosgame(){        
        $this->load->view('front/vwGame-photos-annonce') ;
    }
}