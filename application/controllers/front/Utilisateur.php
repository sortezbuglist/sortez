<?php

class utilisateur extends CI_Controller
{
    private $mCurrentFilter = 2; // Le type de filtre en cours (0 : exact, 1 : d?but, 2 : milieu)
    private $mSearchValue = ""; // La valeur ? rechercher
    private $mLinesPerPage = 50 ;// Le nombre de lignes (enregistrements) ? afficher par page
    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model("user");
        $this->load->library('user_agent');
        $this->load->model("sousRubrique");
        $this->load->model("Rubrique");
        $this->load->model("AssCommercantAbonnement");
        $this->load->model("AssCommercantSousRubrique");
        $this->load->model("Abonnement");
        $this->load->model("mdlannonce");
        $this->load->model("mdlbonplan");
        $this->load->model("mdlcommercant");
        $this->load->model("Abonnement");
        $this->load->model("mdl_agenda");
        $this->load->model("Seo_model");
        $this->load->model("mdlville");
        $this->load->Model("mdldepartement");
        $this->load->Model("mdlcommune");
        $this->load->Model("mdl_lightbox_mail");
        $this->load->model('pack_test_validation');
        $this->load->model("Commercant");
        $this->load->model("mdlarticle");
        $this->load->model("mdlcommercantpagination");
        $this->load->model("mdlfidelity");


        $this->load->model("mdl_types_agenda");
        $this->load->model("mdl_categories_agenda");

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");
        $this->load->helper('clubproximite');

        check_vivresaville_id_ville();

    }

    function index()
    {
        $group_proo_club = array(3, 4, 5);

        if ($this->ion_auth->is_admin()) {
            redirect('admin/home', 'refresh');
        } else if ($this->ion_auth->in_group(2)) {
            redirect('front/utilisateur/menuconsommateurs', 'refresh');
        } else if ($this->ion_auth->in_group($group_proo_club)) {
            redirect('front/utilisateur/contenupro', 'refresh');
        } else {
            redirect('/', 'refresh');
        }
    }

    function allparticulier()
    {
        $allparticulier = $this->user->GetAll();
        $all_lightbox_mail = $this->mdl_lightbox_mail->GetAll();
        if (isset($allparticulier)) echo count($allparticulier) + 12200 + count($all_lightbox_mail); else "0";
    }

    function contenupro()
    {
        $group_proo_club = array(3, 4, 5);
        if (!$this->ion_auth->logged_in()) {
            redirect("connexion");
        } else if ($this->ion_auth->in_group($group_proo_club)) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            ////////$this->firephp->log($iduser, 'iduser');
            $data['iduser'] = $iduser;
            ////////$this->firephp->log($data, 'data');
            $this->load->Model("Commercant");
            $data['toCommercant'] = $this->Commercant->GetById($iduser);
            $infocom = $this->Commercant->GetById($iduser);
            //var_dump($infocom);die();
            $data['toAbonnement'] = $this->Abonnement->GetCurrentCommercantAbonnement($iduser);
            $data['current_page'] = 'contenupro';
            ////////$this->firephp->log($data['toAbonnement'], 'toAbonnement');

            /*if (CURRENT_SITE == "agenda"){
                $this->load->view('agenda/contenupro', $data);
            } else {
                $this->load->view('front2013/contenupro', $data);
            }*/
            //$this->load->view('frontAout2013/contenupro', $data);
            if ($this->session->userdata("from_super_admin_account") == '1')
                $from_super_admin_account = $this->session->userdata("from_super_admin_account");
            else $from_super_admin_account = '0';
            $data['from_super_admin_account'] = $from_super_admin_account;
            $data['iduser'] = $iduser;
            $data['user_ion_auth']= $user_ion_auth;
            $data['group_proo_club']= $group_proo_club;

            $data['pagecategory'] = "admin_commercant";

            $data['validation_pro'] = $this->pack_test_validation->getByIdUser($user_ion_auth->id);// for trial essai de 15 jours
            if (isset($infocom) AND $infocom->IsActif == 1){
                $this->load->view('privicarte/contenupro', $data);
            }else{
                $this->load->view('privicarte/contenupro_not_active', $data);
            }

            //echo "test";
        } else if ($this->ion_auth->in_group(6)) {
            redirect('association/home', 'refresh');
        } else if ($this->ion_auth->is_admin()) {
            redirect('admin/home', 'refresh');
        } else {
            redirect();
        }
    }


    function back_super_admin_account($idCommercant = NULL)
    {

        $group_proo_club = array(3, 4, 5, 6);

        if (!$this->ion_auth->logged_in()) {
            redirect("connexion");
        } else if ($this->ion_auth->in_group($group_proo_club)) {

            if ($this->session->userdata("from_super_admin_account") == '1') {

                //$this->ion_auth->logout();

                $query = $this->db->select('username, email, id, password, active, last_login')
                    ->where('id', '1')
                    ->limit(1)
                    ->get('users_ionauth');
                if ($query->num_rows() === 1) {
                    $user = $query->row();
                    $this->load->Model("Ion_auth_model");
                    $this->Ion_auth_model->set_session($user);
                    $this->Ion_auth_model->update_last_login($user->id);
                    $this->Ion_auth_model->clear_login_attempts($identity);
                }

                $this->session->unset_userdata("from_super_admin_account");

                redirect('admin/commercants/fiche/' . $idCommercant);

            }

        }

    }


    function change_club_site()
    {
        define('CURRENT_SITE', 'privicarte');
        redirect("front/utilisateur/contenupro");
    }


    function menuconsommateurs()
    {
        statistiques();

        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }

        $data = array();
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            ////////$this->firephp->log($user_ion_auth, 'user_ion_auth');
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            //$user_groups = $this->ion_auth->get_users_groups($user_ion_auth->id)->result();
            ////////$this->firephp->log($user_groups, 'user_groups');
            $data['iduser'] = $iduser;
            $data['user_ion_auth'] = $user_ion_auth;
        }

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;


        $user_ion_auth = $this->ion_auth->user()->row();
        $prmId = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
        $oParticulier = $this->user->GetById($prmId);
        $data["oParticulier"] = $oParticulier;
        $data["prmId"] = $prmId;
        $data['mdlville'] = $this->mdlville;
        $data["colDepartement"] = $this->mdldepartement->GetAll();


        if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
            //$this->load->view('front2013/mobile_moncompte', $data);
            //$this->load->view('mobile2013/mobile_moncompte', $data);
            redirect("front/fidelity/compteconsommateur_mobile");
        } else {
            //$this->load->view('frontAout2013/menuconsommateurs', $data);
            $this->load->view('privicarte/menuconsommateurs', $data);
        }

        //$this->load->view('front/vwMenuconsommateurs', $data);

    }

    function compteconsommateur()
    {
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }

        $data = "";
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            $data['iduser'] = $iduser;
        }
        $this->load->view('front/vwCompteconsommateur', $data);
    }


    function avantagespro()
    {
        $data = "";

        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            $data['iduser'] = $iduser;
        }

        //$this->load->view('front/vwAvantagespro', $data);
        /*if (CURRENT_SITE == "agenda"){
            $this->load->view('agenda/avantagespro', $data);
        } else {
            $this->load->view('front2013/avantagespro', $data);
        }*/
        $this->load->view('frontAout2013/avantagespro', $data);
    }

    function alldepartement()
    {
        $data = array();
        $alldepartement = $this->mdldepartement->GetAll();
        $data['alldepartement'] = $alldepartement;
        $this->load->view('privicarte/alldepartement', $data);
    }

    //nouvelle ajoux adress mail
    function lightbox_mail_save()
    {
        $publightbox_mail = $this->input->post("publightbox_mail");
        $publightbox_name = $this->input->post("publightbox_name");
        $publightbox_departement_value = $this->input->post("publightbox_departement_value");
        $prmData = array();
        $prmData['id'] = NULL;
        $prmData['mail'] = $publightbox_mail;
        $prmData['name'] = $publightbox_name;
        $prmData['departement_code'] = $publightbox_departement_value;
        $mail_result = $this->mdl_lightbox_mail->insert($prmData);
        if (isset($mail_result) && $mail_result != 0) {
            $this->session->set_userdata("publightbox_email", $publightbox_mail);
            echo "1";
        } else {
            echo "0";
        }
    }

    // verification si l' adress mail exite
    function lightbox_mail_verify()
    {
        $publightbox_mail = $this->input->post("publightbox_mail");
        $mail_result = $this->mdl_lightbox_mail->getByMail($publightbox_mail);
        $oEmail = $this->user->verifier_mailUser_ionauth($publightbox_mail);
        if (count($mail_result) > 0 || count($oEmail) > 0) {
            echo "1";
        } else {
            echo "0";
        }
    }

    // verification si le nom existe

    function expired_account()
    {
        $data = "";
        //$this->load->view('front2013/expired_account', $data) ;
        $this->load->view('privicarte/expired_account', $data);
    }

    function session()
    {
        $data = '';
        if ($this->ion_auth->logged_in()) {
            //redirect('admin/home');
            if ($this->ion_auth->is_admin()) {
                $this->session->set_flashdata('domain_from', '1');
                redirect("admin/home");
            } else {
                $user_ion_auth_x = $this->ion_auth->user()->row();
                $user_groups_x = $this->ion_auth->get_users_groups($user_ion_auth_x->id)->result();
                $group_id_commercant_user = $user_groups_x[0]->id;
                if ($group_id_commercant_user != 2) redirect('front/utilisateur/contenupro', 'refresh');
                else  redirect('front/utilisateur/menuconsommateurs', 'refresh');
            }
        } else {
            redirect('front/utilisateur/espaceconsommateurs');
        }
    }

    function sessionp()
    {
        $data = '';
        if ($this->ion_auth->logged_in()) {
            //redirect('admin/home');
            if ($this->ion_auth->is_admin()) {
                $this->session->set_flashdata('domain_from', '1');
                redirect("admin/home");
            } else {
                $user_ion_auth_x = $this->ion_auth->user()->row();
                $user_groups_x = $this->ion_auth->get_users_groups($user_ion_auth_x->id)->result();
                $group_id_commercant_user = $user_groups_x[0]->id;
                if ($group_id_commercant_user != 2) redirect('front/utilisateur/contenupro', 'refresh');
                else  redirect('front/utilisateur/menuconsommateurs', 'refresh');
            }
        } else {
            redirect('front/utilisateur/avantagespro');
        }
    }


    function liste_favoris()
    {
        statistiques();
        $data=array();

        if ($this->ion_auth->logged_in()) {

            $user_ion_auth = $this->ion_auth->user()->row();
            // var_dump($user_ion_auth);die("K O");
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            //////$this->firephp->log($iduser, 'iduser'); 
            // var_dump($iduser);         
            $data["iduser"] = $iduser;
            // var_dump($data["iduser"]);die("K O");
            $data['toCommercant'] = $this->user->getFavoris($iduser);
            //////$this->firephp->log($data['toCommercant'], 'toCommercant');
            $data['oInfoCommercant'] = $data['toCommercant'];

            $is_mobile = $this->agent->is_mobile();
            //test ipad user agent
            $is_mobile_ipad = $this->agent->is_mobile('ipad');
            $data['is_mobile_ipad'] = $is_mobile_ipad;
            $is_robot = $this->agent->is_robot();
            $is_browser = $this->agent->is_browser();
            $is_platform = $this->agent->platform();
            $data['is_mobile'] = $is_mobile;
            $data['is_robot'] = $is_robot;
            $data['is_browser'] = $is_browser;
            $data['is_platform'] = $is_platform;

            $data['sousRubrique'] = $this->sousRubrique;
            $data['Rubrique'] = $this->Rubrique;

            $this->load->model("mdlcategorie");
            $toCategorie_principale = $this->mdlcategorie->GetCommercantCategorie();
            $data['toCategorie_principale'] = $toCategorie_principale;

            $data["main_menu_content"] = "favoris";

            $error = $this->session->flashdata('error');
            if (isset($error) && $error != "") $data['error'] = $error;

            $user_groups = $this->ion_auth->get_users_groups($user_ion_auth->id)->result();
            ////$this->firephp->log($user_groups, 'user_groups');
            $data['user_groups'] = $user_groups[0];


            $data['pagecategory'] = "list_favoris";

            //$this->load->view('frontAout2013/favoriscommercant', $data);
            $this->load->view('privicarte/favoriscommercant', $data);

        } else {
            redirect('auth/login/');
        }


    }


    function ajout_favoris($_iCommercantId)
    {
        $data = '';

        if ($this->ion_auth->logged_in()) {

            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            $data['iduser'] = $iduser;
            $user_groups = $this->ion_auth->get_users_groups($user_ion_auth->id)->result();
            //////$this->firephp->log($user_groups, 'user_groups');
            if ($user_groups[0]->id == 1) $typeuser = "1";
            else if ($user_groups[0]->id == 2) $typeuser = "0";
            else if ($user_groups[0]->id == 3 || $user_groups[0]->id == 4 || $user_groups[0]->id == 5) $typeuser = "COMMERCANT";
            $data['typeuser'] = $typeuser;

            if ($user_groups[0]->id == 2) {

                $_iCommercantId = $this->uri->rsegment(3);////reecuperation valeur $_iCommercantId

                if (is_numeric(trim($_iCommercantId))) {

                    $oFavorisCommercant = $this->user->verify_favoris($iduser, $_iCommercantId);
                    if ($oFavorisCommercant == 0) {
                        $prmData['IdUser'] = $iduser;
                        $prmData['IdCommercant'] = $_iCommercantId;
                        $prmData['Favoris'] = 1;
                        $this->user->AddFavoris($prmData);
                    } else {
                        $prmData['IdAssCommercantUser'] = $oFavorisCommercant->IdAssCommercantUser;
                        $prmData['IdUser'] = $iduser;
                        $prmData['IdCommercant'] = $_iCommercantId;
                        $prmData['Favoris'] = 1;
                        $this->user->UpdateFavoris($prmData);
                    }

                    redirect('front/utilisateur/liste_favoris');

                } else {
                    redirect('front/utilisateur/liste_favoris');
                }

            } else {
                $this->session->set_flashdata('error', "Votre Compte ne vous permet pas d'ajouter des favoris !");
                redirect('front/utilisateur/liste_favoris');
            }


        } else {
            $current_page = curPageURL();//echo $current_page;
            $this->session->set_flashdata('last_url', $current_page);
            redirect('auth/login');
        }

    }


    function delete_favoris($_iCommercantId)
    {
        $data = '';

        if ($this->ion_auth->logged_in()) {

            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            $data['iduser'] = $iduser;
            $user_groups = $this->ion_auth->get_users_groups($user_ion_auth->id)->result();
            //////$this->firephp->log($user_groups, 'user_groups');
            if ($user_groups[0]->id == 1) $typeuser = "1";
            else if ($user_groups[0]->id == 2) $typeuser = "0";
            else if ($user_groups[0]->id == 3 || $user_groups[0]->id == 4 || $user_groups[0]->id == 5) $typeuser = "COMMERCANT";
            $data['typeuser'] = $typeuser;


            $_iCommercantId = $this->uri->rsegment(3);////reecuperation valeur $_iCommercantId

            if (is_numeric(trim($_iCommercantId))) {

                $oFavorisCommercant = $this->user->verify_favoris($iduser, $_iCommercantId);
                if (var_dump($oFavorisCommercant) == 0) {
                    redirect('front/utilisateur/liste_favoris');
                } else {
                    $prmData['IdAssCommercantUser'] = $oFavorisCommercant->IdAssCommercantUser;
                    $prmData['IdUser'] = $iduser;
                    $prmData['IdCommercant'] = $_iCommercantId;
                    $prmData['Favoris'] = 0;
                    $this->user->UpdateFavoris($prmData);
                    redirect('front/utilisateur/liste_favoris');
                }

            } else {
                redirect('front/utilisateur/liste_favoris');
            }


        } else {
            redirect('connexion');
        }

    }


    function get_tarif_abonnement()
    {
        $this->load->model("Abonnement");
        $IdAbonnement = $this->input->post("IdAbonnement");
        $oAbonnement = $this->Abonnement->GetById($IdAbonnement);
        if (isset($oAbonnement) && $oAbonnement->tarif != "" && $oAbonnement->tarif != NULL) {
            echo $oAbonnement->tarif;
        } else echo 0;
    }

    function espaceconsommateurs()
    {
        $data['page_espace_conso_bg'] = 1;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $data['current_page'] = "espaceconsommateurs";

        if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
            $this->load->view('front2013/mobile_moncompte', $data);
        } else {
            /*if (CURRENT_SITE == "agenda"){
                $this->load->view('agenda/espaceconsommateurs', $data) ;
            } else {
                $this->load->view('front2013/espaceconsommateurs', $data) ;
            }*/
            $this->load->view('frontAout2013/espaceconsommateurs', $data);
        }

    }

    function conditionsgenerales()
    {
        $data['page_espace_conso_bg'] = 1;

        if (CURRENT_SITE == "agenda") {
            $this->load->view('agenda/conditionsgenerales', $data);
        } else {
            $this->load->view('front2013/conditionsgenerales', $data);
        }
    }

    function comparerabonnements()
    {
        $data['page_comparerabonnements_id'] = 1;

        if (CURRENT_SITE == "agenda") {
            $this->load->view('agenda/comparerabonnements', $data);
        } else {
            //$this->load->view('front2013/comparerabonnements', $data);
            $this->load->view('frontAout2013/comparerabonnements', $data);
        }
    }

    function infosabonnementbasic()
    {
        $data['page_comparerabonnements_id'] = 1;
        $this->load->view('front2013/infosabonnementbasic', $data);
    }

    function infospremium()
    {
        $data['page_comparerabonnements_id'] = 1;
        $this->load->view('front2013/infospremium', $data);
    }

    function infosplatinium()
    {
        $data['page_comparerabonnements_id'] = 1;
        $this->load->view('front2013/infosplatinium', $data);
    }

    function infosreferencement()
    {
        $data['page_comparerabonnements_id'] = 1;
        $this->load->view('front2013/infosreferencement', $data);
    }


    function qrcodes()
    {
        $data['page_comparerabonnements_id'] = 1;
        $this->load->view('frontAout2013/qrcodes', $data);
    }


    function newsletter($categ) {
        if ($categ=='article') {
            $toListeBonPlan = $this->mdlarticle->listeArticleRecherche(0,0,0,"",0,0,10,"","0","0000-00-00","0000-00-00","0","0");
            $data['toListeBonPlan'] = $toListeBonPlan;
            $this->load->view('admin/newsletter/template_'.$categ, $data);
        } else if ($categ=='agenda') {
            $toListeBonPlan = $this->mdl_agenda->listeAgendaRecherche(0,0,0,"",0,0,10,"","0","0000-00-00","0000-00-00","0","0");
            $data['toListeBonPlan'] = $toListeBonPlan;
            $this->load->view('admin/newsletter/template_'.$categ, $data);
        } else if ($categ=='annuaire') {
            $toListeBonPlan = $this->mdlcommercantpagination->listePartenaireRecherche(0, 0, 0, "", "", 0, 10, "", "0", "10", "0", "0",  "0");
            $data['toListeBonPlan'] = $toListeBonPlan;
            $this->load->view('admin/newsletter/template_'.$categ, $data);
        } else if ($categ=='annonce') {
            $toListeBonPlan = $this->mdlannonce->listeAnnonceRecherche(0, 0, "", "", 0, 10, 0, 0, '', "0", "10", "0", "0");
            $data['toListeBonPlan'] = $toListeBonPlan;
            $this->load->view('admin/newsletter/template_'.$categ, $data);
        } else if ($categ=='fidelite') {
            $toListeFidelity_remise = $this->mdlfidelity->listeFidelityRecherche(0, 0, "", "", 0, 10000, 0, 0, "", "0", "10", "0", "0", 0, "remise");
            $toListeFidelity_tampon = $this->mdlfidelity->listeFidelityRecherche(0, 0, "", "", 0, 10000, 0, 0, "", "0", "10", "0", "0", 0, "tampon");
            $toListeFidelity_capital = $this->mdlfidelity->listeFidelityRecherche(0, 0, "", "", 0, 10000, 0, 0, "", "0", "10", "0", "0", 0, "capital");
            $data['toListeFidelity_remise'] = $toListeFidelity_remise;
            $data['toListeFidelity_tampon'] = $toListeFidelity_tampon;
            $data['toListeFidelity_capital'] = $toListeFidelity_capital;
            $this->load->view('admin/newsletter/template_fidelity', $data);
        } else {
            $toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche(0,0,"","",0,10000,0,0,"","0","10","0","0","","");
            $data['toListeBonPlan'] = $toListeBonPlan;
            $this->load->view('admin/newsletter/template', $data);
        }
        
    }


    function no_permission()
    {
        $data['info'] = NULL;
        $this->load->view("front2013/no_permission", $data);
    }


    function agenda($_iDCommercant = 0,$prmFilterCol = "-1", $prmFilterValue = "0", $prmPagerIndex = 0, $prmIsCompleteList = false, $prmOrder = "")
    {

        $group_proo_club = array(3, 4, 5);
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        } else if (!$this->ion_auth->in_group($group_proo_club)) {
            redirect("front/utilisateur/no_permission");
        }

        if ($_iDCommercant != 0 && is_numeric($_iDCommercant) == true) {
            $toListeAgenda = $this->mdl_agenda->GetByIdCommercant_forall($_iDCommercant);
            $data['toListeAgenda'] = $toListeAgenda;
            $data["idCommercant"] = $_iDCommercant;

            $objCommercant = $this->Commercant->GetById($_iDCommercant);
            $data["objCommercant"] = $objCommercant ;
            //verify 20 annonces limit
            if (isset($objCommercant->limit_agenda) && intval($objCommercant->limit_agenda)!=0 && $objCommercant->limit_agenda!="" && count($toListeAgenda)>=intval($objCommercant->limit_agenda))
            {
                $data['limit_agenda_add'] = 1;
            } else {
                $data['limit_agenda_add'] = 0;
            }

            //$this->load->view('agendaAout2013/vwMesAgenda', $data);
            $data['pagecategory'] = "admin_commercant";
            $_SESSION["User_FilterCol"] = $prmFilterCol;
            $_SESSION["User_FilterValue"] = $prmFilterValue;

            if ($prmIsCompleteList) {
                $this->_clearAllSessionsData();
            }

            if (isset($_POST["cmbNbLignes"])) {
                $this->SetLinesPerPage($_POST["cmbNbLignes"]);
            }

            if (isset($_POST["txtSearch"])) {
                $this->SetSearchValue($_POST["txtSearch"]);
            }

            if (isset($_POST["optTypeFiltre"])) {
                $this->SetCurrentFilter($_POST["optTypeFiltre"]);
            }

            if (isset($_POST["hdnOrder"])) {
                $this->SetOrder($_POST["hdnOrder"]);
            }

            $SearchedWordsString = str_replace("+", " ", $this->GetSearchValue()) ;
            $SearchedWordsString = trim($SearchedWordsString);
            $SearchedWords = explode(" ", $SearchedWordsString) ;
            $WhereKeyWords = " 0=0 " ;


            for ($i = 0; $i < sizeof($SearchedWords); $i ++) {
                $Search = $SearchedWords[$i];
                $Search = str_replace(" ", "", $Search) ;
                if ($Search != "") {
                    if ($i == 0) {
                        $WhereKeyWords .= " AND " ;
                    }


                    $WhereKeyWords .= " ( " .
                        " UPPER(agenda.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'" .
                        " OR UPPER(agenda.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'" .
                        " )
                ";
                    if ($i != (sizeof($SearchedWords) - 1)){
                        $WhereKeyWords .= " OR ";
                    }
                }
            }

            // Pre-filtre sur une table parent (cas d'une liste fille dans une fiche maitre-detail)
            if ($prmFilterCol != "-1") {
                $WhereKeyWords .= " AND " . $prmFilterCol . " = '" . $prmFilterValue . "'" ;
            }



            // echo $WhereKeyWords; exit;
            //$objCommercants = $this->Commercant->GetAll();
            $colusers = $this->mdl_agenda->GetWhere_pvc($WhereKeyWords, $prmPagerIndex);


            // Pagination (CI)
            $this->load->library("pagination");
            $this->config->load("config");
            $pagination_config['uri_segment'] = '5';
            $data["UriSegment"] = $pagination_config['uri_segment'];
            $pagination_config["base_url"] = site_url() . "admin/utilisateur/agenda/" .$_iDCommercant ."/". $prmFilterCol . "/" . $prmFilterValue ;
            $pagination_config["total_rows"] = $this->mdl_agenda->CountWhere_pvc($WhereKeyWords);
            $pagination_config["per_page"] = $this->GetLinesPerPage();
            $pagination_config["first_link"] = '&lsaquo;';
            $pagination_config["last_link"] = '&raquo';
            $pagination_config["next_link"] = '&gt;';
            $pagination_config["prev_link"] = '&lt;';
            $pagination_config["cur_tag_open"] = "<span style='font-weight: bold; padding-right: 5px; padding-left: 5px; '>" ;
            $pagination_config["cur_tag_close"] = "</span>" ;
            $pagination_config["num_tag_open"] = "&nbsp;<span>" ;
            $pagination_config["num_tag_close"] = "&nbsp;</span>" ;

            $this->pagination->initialize($pagination_config);
            // Effectuer un highlight des mots-cles recherches dans la liste des enregistrements trouv&s
            $data["highlight"] = "";
            if ($this->GetSearchValue() != "") {
                $data["highlight"] = "yes";
            } else {
                $data["highlight"] = "";
            }

            // Les numeros de page
            $NumerosPages = array();
            //$NumerosPages[] = 0;
            $n = 0;
            while($n < $pagination_config["total_rows"] - 1) {
                $NumerosPages[] = $n;
                $n += $pagination_config["per_page"];
            }
            $data["NumerosPages"] = $NumerosPages;


            // Preparer les variables pour le view
            $data["colUsers"] = $colusers ;

            $data["PaginationLinks"] = $this->pagination->create_links();
            $data["SearchValue"] = $this->GetSearchValue();
            $data["NbLignes"] = $this->GetLinesPerPage() ;
            $data["SearchedWords"] = $SearchedWords;
            $data["Keywords"] = $this->GetSearchValue();
            $data["CountAllResults"] = $pagination_config["total_rows"];


            $DepartCount = $prmPagerIndex;
            $data["DepartCount"] =  $DepartCount;

            // Data pour le pre-filtre
            $data["FilterCol"] = $prmFilterCol;
            $data["FilterValue"] = $prmFilterValue;

            $data["no_main_menu_home"] = "1";

            $data["ion_auth_used_by_club"] = $this->ion_auth_used_by_club;
            $data["ion_auth"] = $this->ion_auth;
             $data["infocom"] = $this->mdlcommercant->infoCommercant($_iDCommercant);

            $this->load->view('privicarte/mes_agendas', $data);
        } else {
            redirect("front/utilisateur/no_permission");
        }
    }

    function save_order_partner()
    {

        $idCommercant = $this->input->post("idCommercant");

        $toListeAgenda = $this->mdl_agenda->GetByIdCommercant($idCommercant);

        foreach ($toListeAgenda as $key) {
            $agenda_id = $key->id;
            $inputOrderPartner = $this->input->post("inputOrderPartner_" . $key->id);
            $this->mdl_agenda->save_order_partner($idCommercant, $agenda_id, $inputOrderPartner);
        }


    }

    function deleteAgenda($IdAgenda = 0, $prmIdCommercant = 0)
    {

        if ($this->ion_auth->logged_in() && ($this->ion_auth->in_group(2))) {
            redirect("front/particuliers/inscription/");
        } else if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
            redirect('admin/home', 'refresh');
        } else if (!$this->ion_auth->logged_in()) {
            $this->session->set_userdata("cur_uri", $this->uri->uri_string());
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }

        if ($this->ion_auth->logged_in() && ($prmIdCommercant == 0 || $prmIdCommercant == "")) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $prmIdCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        }

        //verify if $IdAgenda, $prmIdCommercant and $user_ion_auth->id are corrects and match between them
        if ($IdAgenda != 0 && $IdAgenda != "" && $IdAgenda != null && is_numeric($IdAgenda) == true) {
            $objAgenda_verification = $this->mdl_agenda->GetById($IdAgenda);
            if (isset($objAgenda_verification) && count($objAgenda_verification) != 0) {
                if ($objAgenda_verification->IdCommercant != $prmIdCommercant && $objAgenda_verification->IdUsers_ionauth != $user_ion_auth->id) {
                    redirect("front/utilisateur/no_permission");
                }
            } else redirect("front/utilisateur/no_permission");
        }

        $this->mdl_agenda->delete($IdAgenda);

        redirect("front/utilisateur/agenda/" . $prmIdCommercant);

    }


    function ficheAgenda($prmIdCommercant = 0, $IdAgenda = 0, $mssg = 0)
    {

        if ($this->ion_auth->logged_in() && ($this->ion_auth->in_group(2))) {
            redirect("front/particuliers/inscription/");
        } else if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
            redirect('admin/home', 'refresh');
        } else if (!$this->ion_auth->logged_in()) {
            $this->session->set_userdata("cur_uri", $this->uri->uri_string());
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }

        if ($this->ion_auth->logged_in() && ($prmIdCommercant == 0 || $prmIdCommercant == "")) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $prmIdCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        }

        //verify if $IdAgenda, $prmIdCommercant and $user_ion_auth->id are corrects and match between them
        if ($IdAgenda != 0 && $IdAgenda != "" && $IdAgenda != null && is_numeric($IdAgenda) == true) {
            $objAgenda_verification = $this->mdl_agenda->GetById($IdAgenda);
            if (isset($objAgenda_verification) && count($objAgenda_verification) != 0) {
                if ($objAgenda_verification->IdCommercant != $prmIdCommercant && $objAgenda_verification->IdUsers_ionauth != $user_ion_auth->id) {
                    redirect("front/utilisateur/no_permission");
                }
            }
        }

        $data = null;


        //start verify if merchant subscription is right WILLIAM
        $_iDCommercant = $prmIdCommercant;
        $current_date = date("Y-m-d");
        $query_abcom_cond = " IdCommercant = " . $_iDCommercant . " AND ('" . $current_date . "' BETWEEN DateDebut AND DateFin)";
        $toAbonnementCommercant = $this->AssCommercantAbonnement->GetWhere($query_abcom_cond, 0, 1);
        if (sizeof($toAbonnementCommercant)) {
            foreach ($toAbonnementCommercant as $oAbonnementCommercant) {
                if (isset($oAbonnementCommercant) && ($oAbonnementCommercant->IdAbonnement == 1 || $oAbonnementCommercant->IdAbonnement == 2)) {//IdAbonnement doesn't contain annonce
                    //redirect ("front/professionnels/fiche/$_iDCommercant") ;
                    $data['show_annonce_btn'] = 0;
                } else $data['show_annonce_btn'] = 1;

                if (isset($oAbonnementCommercant) && ($oAbonnementCommercant->IdAbonnement == 1 || $oAbonnementCommercant->IdAbonnement == 3)) {//IdAbonnement doesn't contain bonplan
                    //redirect ("front/professionnels/fiche/$_iDCommercant") ;
                    $data['show_bonplan_btn'] = 0;
                } else $data['show_bonplan_btn'] = 1;
            }
        } else {
            $data['show_annonce_btn'] = 0;
            $data['show_bonplan_btn'] = 0;
            //$this->load->view("front/vwFicheProfessionnelError",$data);
            redirect("front/professionnels/ficheError/");
            //exit();
        }
        //end verify if merchant subscription is right WILLIAM

        $user_ion_auth = $this->ion_auth->user()->row();
        $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $data["userId"] = $commercant_UserId;

        $this->load->Model("Commercant");
        $this->load->Model("Rubrique");
        $this->load->Model("SousRubrique");
        $this->load->Model("Ville");
        $this->load->Model("Abonnement");
        $this->load->Model("AssCommercantRubrique");
        $this->load->Model("AssCommercantSousRubrique");
        $this->load->Model("AssCommercantAbonnement");

        $data["no_main_menu_home"] = "1";
        $data["no_left_menu_home"] = "1";


        $data["colVilles"] = $this->mdlville->GetAll();

        $data["objAgenda"] = $this->mdl_agenda->GetById($IdAgenda);
        $data["objCommercant"] = $this->Commercant->GetById($commercant_UserId);

        $this->load->helper('ckeditor');

        //Ckeditor's configuration
        $data['ckeditor0'] = array(

            //ID of the textarea that will be replaced
            'id' => 'description',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '590px',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor'),
                    array('Image')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );
//// OP END


        $data['ckeditor1'] = array(

            //ID of the textarea that will be replaced
            'id' => 'description_tarif',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '590px',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor'),
                    array('Image')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor2'] = array(

            //ID of the textarea that will be replaced
            'id' => 'conditions_promo',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '590px',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor'),
                    array('Image')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['home_link_club'] = 1;
        $data['mssg'] = $mssg;

        $data['jquery_to_use'] = "1.4.2";

        $data["colTypeagenda"] = $this->mdl_types_agenda->GetAll();
        $data["colCategorie"] = $this->mdl_categories_agenda->GetAll();
        $data["colSousCategorie"] = $this->mdl_categories_agenda->GetAllSousrubrique();

        $data["colDepartement"] = $this->mdldepartement->GetAll();
        $data['mdldepartement'] = $this->mdldepartement;
        $data["colCommune"] = $this->mdlcommune->GetAll();
        $data['mdlcommune'] = $this->mdlcommune;

        $objAbonnementCommercant = $this->Abonnement->GetLastAbonnementCommercant($prmIdCommercant);
        $data['objAbonnementCommercant'] = $objAbonnementCommercant;

        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($prmIdCommercant);
        if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
        $data["user_groups"] = $user_groups[0];

        $data['defaul_thumb_width'] = $this->config->item('defaul_thumb_width');
        $data['defaul_thumb_height'] = $this->config->item('defaul_thumb_height');

        $data['mdlville'] = $htis->mdlville;

        $data["colDepartement"] = $this->mdldepartement->GetAll();
        $data['mdldepartement'] = $this->mdldepartement;

        $data['pagecategory'] = "admin_commercant";

        //$this->load->view("agendaAout2013/vwFicheAgenda",$data);
        $this->load->view("privicarte/vwFicheAgenda", $data);


    }


    function getPostalCode_localisation($idVille = 0)
    {
        $oville = $this->mdlville->getVilleById($idVille);
        if (count($oville) != 0) $data["cp"] = $oville->CodePostal; else $data["cp"] = "";

        echo $data["cp"];

    }


    function save()
    {
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_userdata("cur_uri", $this->uri->uri_string());
            redirect();
        }
        $objAgenda = $this->input->post("Agenda");

        $user_ion_auth = $this->ion_auth->user()->row();
        $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $objAgenda["IdCommercant"] = $commercant_UserId;
        $objAgenda["IdUsers_ionauth"] = $user_ion_auth->id;

        if (isset($_FILES["AgendaPdf"]["name"])) $pdf = $_FILES["AgendaPdf"]["name"];
        if (isset($pdf) && $pdf != "") {
            $pdfAssocie = $pdf;
            $pdfCom = doUpload("AgendaPdf", "application/resources/front/images/agenda/pdf/", $pdfAssocie);
        } else {
            $pdfAssocie = $this->input->post("pdfAssocie");
            $pdfCom = $pdfAssocie;
        }

        if (isset($_FILES["Agendaautre_doc_1"]["name"])) $autre_doc_1 = $_FILES["Agendaautre_doc_1"]["name"];
        if (isset($autre_doc_1) && $autre_doc_1 != "") {
            $autre_doc_1Associe = $autre_doc_1;
            $autre_doc_1Com = doUpload("Agendaautre_doc_1", "application/resources/front/images/agenda/photoCommercant/", $autre_doc_1Associe);
        } else {
            $autre_doc_1Associe = $this->input->post("autre_doc_1Associe");
            $autre_doc_1Com = $autre_doc_1Associe;
        }

        if (isset($_FILES["Agendaautre_doc_2"]["name"])) $autre_doc_2 = $_FILES["Agendaautre_doc_2"]["name"];
        if (isset($autre_doc_2) && $autre_doc_2 != "") {
            $autre_doc_2Associe = $autre_doc_2;
            $autre_doc_2Com = doUpload("Agendaautre_doc_2", "application/resources/front/images/agenda/photoCommercant/", $autre_doc_2Associe);
        } else {
            $autre_doc_2Associe = $this->input->post("autre_doc_2Associe");
            $autre_doc_2Com = $autre_doc_2Associe;
        }

        $objAgenda["pdf"] = $pdfCom;
        $objAgenda["photo1"] = $this->input->post("photo1Associe");
        $objAgenda["photo2"] = $this->input->post("photo2Associe");
        $objAgenda["photo3"] = $this->input->post("photo3Associe");
        $objAgenda["photo4"] = $this->input->post("photo4Associe");
        $objAgenda["photo5"] = $this->input->post("photo5Associe");
        $objAgenda["doc_affiche"] = $Agendadoc_affiche;
        $objAgenda["autre_doc_1"] = $autre_doc_1Com;
        $objAgenda["autre_doc_2"] = $autre_doc_2Com;

        $objAgenda["last_update"] = date("Y-m-d");

        $objAgenda["date_depot"] = convert_Frenchdate_to_Sqldate($objAgenda["date_depot"]);
        $objAgenda["date_debut"] = convert_Frenchdate_to_Sqldate($objAgenda["date_debut"]);
        $objAgenda["date_fin"] = convert_Frenchdate_to_Sqldate($objAgenda["date_fin"]);

        if ($objAgenda["id"] == "" || $objAgenda["id"] == null || $objAgenda["id"] == "0") {
            $IdUpdatedAgenda = $this->mdl_agenda->insert($objAgenda);
        } else {
            $IdUpdatedAgenda = $this->mdl_agenda->update($objAgenda);
        }


        /*if(!empty($IdUpdatedAgenda)) {
            // Liste des destinataires
            $colDest = array();
            $colDest[] = array("Email"=>$this->config->item("mail_sender_adress"),"Name"=>$this->config->item("mail_sender_name"));

            // Sujet
            $txtSujet = "Commerçants";

            $txtContenu = "
                <p>Bonjour ,</p>
                <p>Un commer&ccedil;ant vient de modifier ses informations.</p>
                <p>Cordialement,</p>
            ";
            @envoi_notification($colDest,$txtSujet,$txtContenu);
        }*/


        //redirect("front/menuprofessionnels");
        //redirect("front/utilisateur/agenda/".$commercant_UserId);
        redirect("front/utilisateur/ficheAgenda/" . $commercant_UserId . "/" . $IdUpdatedAgenda . "/1");
    }


    function GetallSubcateg_by_categ($id = "0")
    {
        $data["colSousCategorie"] = $this->mdl_categories_agenda->GetAllSousrubriqueByRubrique($id);
        $data['id'] = $id;
        $this->load->view("admin/vwReponseSubcateg", $data);
    }
    public function GetSearchValue() {
        $SearchValue ;
        if (isset($_POST["txtSearch"])) {
            $SearchValue = $_POST["txtSearch"] ;
        } else {
            if ($this->session->userdata("SearchValue")== null) {
                $SearchValue = "";
            }
            $SearchValue = $this->session->userdata("SearchValue");
        }

        return addslashes($SearchValue) ;
    }
    public function liste($prmFilterCol = "-1", $prmFilterValue = "0", $prmPagerIndex = 0, $prmIsCompleteList = false, $prmOrder = "") {
        $_SESSION["User_FilterCol"] = $prmFilterCol;
        $_SESSION["User_FilterValue"] = $prmFilterValue;

        if ($prmIsCompleteList) {
            $this->_clearAllSessionsData();
        }

        if (isset($_POST["cmbNbLignes"])) {
            $this->SetLinesPerPage($_POST["cmbNbLignes"]);
        }

        if (isset($_POST["txtSearch"])) {
            $this->SetSearchValue($_POST["txtSearch"]);
        }

        if (isset($_POST["optTypeFiltre"])) {
            $this->SetCurrentFilter($_POST["optTypeFiltre"]);
        }

        if (isset($_POST["hdnOrder"])) {
            $this->SetOrder($_POST["hdnOrder"]);
        }

        $SearchedWordsString = str_replace("+", " ", $this->GetSearchValue()) ;
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString) ;
        $WhereKeyWords = " 0=0 " ;


        for ($i = 0; $i < sizeof($SearchedWords); $i ++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search) ;
            if ($Search != "") {
                if ($i == 0) {
                    $WhereKeyWords .= " AND " ;
                }


                $WhereKeyWords .= " ( " .
                    " UPPER(agenda.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'" .
                    " OR UPPER(agenda.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'" .
                    " ) 
                ";
                if ($i != (sizeof($SearchedWords) - 1)){
                    $WhereKeyWords .= " OR ";
                }
            }
        }

        // Pre-filtre sur une table parent (cas d'une liste fille dans une fiche maitre-detail)
        if ($prmFilterCol != "-1") {
            $WhereKeyWords .= " AND " . $prmFilterCol . " = '" . $prmFilterValue . "'" ;
        }



        // echo $WhereKeyWords; exit;
        //$objCommercants = $this->Commercant->GetAll();
        $colusers = $this->mdl_agenda->GetWhere_pvc($WhereKeyWords, $prmPagerIndex);


        // Pagination (CI)
        $this->load->library("pagination");
        $this->config->load("config");
        $pagination_config['uri_segment'] = '5';
        $data["UriSegment"] = $pagination_config['uri_segment'];
        $pagination_config["base_url"] = site_url() . "front/utilisateur/agenda/" . $prmFilterCol . "/" . $prmFilterValue ;
        $pagination_config["total_rows"] = $this->mdl_agenda->CountWhere_pvc($WhereKeyWords);
        $pagination_config["per_page"] = $this->GetLinesPerPage();
        $pagination_config["first_link"] = '&lsaquo;';
        $pagination_config["last_link"] = '&raquo';
        $pagination_config["next_link"] = '&gt;';
        $pagination_config["prev_link"] = '&lt;';
        $pagination_config["cur_tag_open"] = "<span style='font-weight: bold; padding-right: 5px; padding-left: 5px; '>" ;
        $pagination_config["cur_tag_close"] = "</span>" ;
        $pagination_config["num_tag_open"] = "&nbsp;<span>" ;
        $pagination_config["num_tag_close"] = "&nbsp;</span>" ;

        $this->pagination->initialize($pagination_config);
        // Effectuer un highlight des mots-cles recherches dans la liste des enregistrements trouv&s
        $data["highlight"] = "";
        if ($this->GetSearchValue() != "") {
            $data["highlight"] = "yes";
        } else {
            $data["highlight"] = "";
        }

        // Les numeros de page
        $NumerosPages = array();
        //$NumerosPages[] = 0;
        $n = 0;
        while($n < $pagination_config["total_rows"] - 1) {
            $NumerosPages[] = $n;
            $n += $pagination_config["per_page"];
        }
        $data["NumerosPages"] = $NumerosPages;


        // Preparer les variables pour le view
        $data["colUsers"] = $colusers ;

        $data["PaginationLinks"] = $this->pagination->create_links();
        $data["SearchValue"] = $this->GetSearchValue();
        $data["NbLignes"] = $this->GetLinesPerPage() ;
        $data["SearchedWords"] = $SearchedWords;
        $data["Keywords"] = $this->GetSearchValue();
        $data["CountAllResults"] = $pagination_config["total_rows"];


        $DepartCount = $prmPagerIndex;
        $data["DepartCount"] =  $DepartCount;

        // Data pour le pre-filtre
        $data["FilterCol"] = $prmFilterCol;
        $data["FilterValue"] = $prmFilterValue;

        $data["no_main_menu_home"] = "1";

        $data["ion_auth_used_by_club"] = $this->ion_auth_used_by_club;
        $data["ion_auth"] = $this->ion_auth;
        $data["colArticles"] =$this->mdl_agenda->GetAll();
        //$this->load->view("admin/vwUsersadmin", $data) ;
        $this->load->view("admin/vwMesAgenda", $data) ;
    }


    public function GetLinesPerPage() {
        if (isset($_POST["cmbNbLignes"])) {
            return  $_POST["cmbNbLignes"];
        } else {
            if ($this->session->userdata("LinesPerPage")== null) {
                return 50;
            }
            return $this->session->userdata("LinesPerPage");
        }
    }


    public function SetCurrentFilter($argValue) {
        $this->mCurrentFilter = $argValue ;
        $this->session->set_userdata("curFilter",$argValue) ;
    }


    public function GetOrder() {
        if (isset($_POST["hdnOrder"])) {
            return $_POST["hdnOrder"] ;
        } else {
            if ($this->session->userdata("UserOrder")== null) {
                return "";
            }
            return $this->session->userdata("UserOrder") ;
        }
    }

    public function SetOrder($argValue) {
        $this->session->set_userdata("UserOrder",$argValue) ;
    }

    function _clearAllSessionsData() {
        $this->SetCurrentFilter(2);
        $this->SetSearchValue("");
        $this->SetLinesPerPage(50);
        $this->SetOrder("");
    }




    public function SetSearchValue($argValue) {
        $this->mSearchValue = $argValue ;
        $this->session->set_userdata("SearchValue",$argValue) ;
    }
    public function get_count_prospect(){
       $all = $this->mdl_lightbox_mail->GetAll();
       $count = count($all);
       echo $count;
    }
    public function seo($id_commercant = 0){

			//check submit button
		if($this->input->post('submit'))
		{
			$data_seo = $this->input->post('seo_data');

            $data_seo_get = $this->Seo_model->get_data_by_Idcommercant($id_commercant);

			//call saverecords method of Domaine_model and pass variables as parameter
            if($data_seo_get != null){
                $save = $this->Seo_model->saverecords($data_seo,$id_commercant);
            }else{
                $save = $this->Seo_model->saverecords($data_seo,0);
            }

			if($save != ''){
                $data['message_success'] = 'Les modifications sont enregistrés';
            }else{
			    $data['message_error'] = 'Nous avons rencontrés un probléme';
            }
		}
        $infocommercant = $this->mdlcommercant->infoCommercant($id_commercant);
        $data_seo = $this->Seo_model->get_data_by_Idcommercant($id_commercant);
        $data['oInfocommercant'] = $infocommercant;
        $data['data_seo'] = $data_seo;

        $this->load->view('admin/seo',$data);
    }
    public function verification_domaine(){
        $domaine_name = $this->input->post('domaine_name');
        if ( gethostbyname($domaine_name) != $domaine_name ) {
            echo "1";
        }
        else {
            $get_data = $this->Seo_model->get_data_by_dommain_name($domaine_name);
            if(!empty($get_data)){
                echo "1";
            }else{
                echo "0";
            }
        }
    }
    public function google_analytics(){
        require_once(BASEPATH . '../application/third_party/Google/Client.php');
        require_once(BASEPATH . '../application/third_party/Google/Service/Analytics.php');

        session_start();

        $client_id = '100393126794635379397'; //Client ID
        $service_account_name = 'google-analytics@analytics-account-project.iam.gserviceaccount.com'; //Email Address
        //$key_file_location = BASEPATH . '../application/third_party/Google/<YOUR KEY.p12>'; //key.p12

        $client = new Google_Client();
        $client->setApplicationName("Analytics account project");
        $service = new Google_Service_Analytics($client);

        if (isset($_SESSION['service_token'])) {
            $client->setAccessToken($_SESSION['service_token']);
        }

        $key = "AIzaSyC5vQ1rdPi0DaxF5R20GQRslksnjX6HMHg";
        $cred = new Google_Auth_AssertionCredentials(
            $service_account_name,
            array(
                'https://www.googleapis.com/auth/analytics',
            ),
            $key,
            'notasecret'
        );
        $client->setAssertionCredentials($cred);
        if($client->getAuth()->isAccessTokenExpired()) {
            $client->getAuth()->refreshTokenWithAssertion($cred);
        }
        $_SESSION['service_token'] = $client->getAccessToken();
    }

    function change_mail_basic(){

        $this->load->view('front_soutenons/includes/login_mail');
    }

    function login_basic_sendmail()
    {

        $contact_partner_id = $this->input->post('contact_partner_id');
        $nom = $this->input->post('contact_partner_nom');
        $adresse = $this->input->post('contact_partner_adresse');
        $code = $this->input->post('contact_partner_code');
        $ville = $this->input->post('contact_partner_ville');

        $get_mail = $this->Commercant->GetById($contact_partner_id);
         $get_info = $this->mdlcommercant->get_log_pwd($get_mail->Email);

         if($get_info != null){
            $mdp = $get_info->password;
        }else{
             $mdp ='*****';
        }


        $login = $get_mail->Email;


        
      
        $contact_partner_mailto = $login;

        $contact_partner_mail = 'contact@sortez.org';
        $contact_partner_mailSubject = 'Contact partenaire - Sortez';

        $message_html ='
        
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="75%"  style="border: 1px solid #cccccc; border-collapse: collapse;">
            <tr>
                <td style="padding: 10px 0 30px 0;">
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="75%">
                            <tr>
                                <td style="text-align: center; padding: 0px 15px 0px 15px; font-family: Arial, sans-serif;">
                                      <h2>Madame, Monsieur,</h2>
                                    <h4>Suite à votre demande de modification de vos données, nous vous adressons les identifiants provisoires de votre compte.</h4>
                                </td>
                            </tr>
                             <tr>
                                    <td class="txt_id">Établissement : '.$nom.'</td>                                    
                            </tr>                                 
                                
                            <tr>

                            <td class="txt_id">Adresse  : '.$adresse.'</td>

                            </tr>
                            <tr>

                            <td class="txt_id">Code postale  : '.$code.'</td>

                            </tr>
                            <tr>

                            <td class="txt_id">Ville  : '.$ville.'</td>

                            </tr>

                    </table>
                   <table align="center" cellspacing="20px;">
                            <tr>
                                    <td class="txt_id">Identifiant : '.$login.'</td>                                    
                            </tr>                                 
                                
                            <tr>

                            <td class="txt_id">Mots de passe : '.$mdp.'</td>

                            </tr>

                        </table>
                    <tr>
                        <td align="center"><h4 style="font-size: 15px; font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;">Pour accéder à votre page admin, nous vous invitons à cliquer sur le lien suivant et à vous identifier.<a href="https://www.sortez.org/auth/login">login sortez</a></h4>
                        </td>
                    </tr>
                    <tr>
                     <td align="center"><h4 style="font-size: 15px; font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;">Nous vous souhaitons une bonne journée.<br>Cordialement </h4></td>
                    </tr>
                    <tr>
                        <td align="center"><h4 style="font-size: 15px; font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;">L’équipe du magazine Sortez<br>contact@sortez.org</h4></td>
                    </tr>
                        
                </td>
            </tr>
        </table>
';

        // $sending_mail_privicarte = false;

        $message_html = html_entity_decode($message_html);

        $zContactEmailCom_ = array();
        $zContactEmailCom_[] = array("Email" => $contact_partner_mailto, "Name" => $contact_partner_mailto);
        $sending_mail_privicarte = envoi_notification($zContactEmailCom_, $contact_partner_mailSubject, $message_html, $contact_partner_mail);

         if ($sending_mail_privicarte){

            header("Location : https://www.magazine-sortez.org/mail-basique");
            Exit(); 
              //var_dump($login);
           // $this->load->view('front_soutenons/includes/login_mail', $login);
         }else{
            echo 'echec';
            var_dump($get_info);
         }
    }

}

?>
