<?php

class Plat_du_jour extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model("user");
        $this->load->library('user_agent');
        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");
        $this->load->model("commercant");
        $this->load->model("mdlcommercant");
        $this->load->model("Mdl_plat_du_jour");
        $this->load->library('form_validation');
        $this->load->helper('clubproximite_helper');
        check_vivresaville_id_ville();

    }
    public function liste(){
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            $data['plat']=$this->Mdl_plat_du_jour->getAll();
            $data['infocom']=$this->commercant->GetById($iduser);
            var_dump($data['infocom']);
            $this->load->view('plat_du_jour/liste_plat_du_jour',$data);
        }else{
            redirect('auth/login');
        }

    }
    public function fiche(){
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            $data['IdCommercant'] = $iduser;
            $data['oInfoCommercant']=$this->commercant->GetById($iduser);
            $this->load->view('plat_du_jour/vwfiche_plat',$data);
        }else{
            redirect('auth/login');
        }
    }
    //kappadev_11_2_22

        public function delete_image_res_menu($idcom){
        $idcom = $this->input->post('idcom');
        $data_com_menu = $this->Mdl_plat_du_jour->get_resinfocom_by_idcom($idcom);
        $infocom = $this->mdlcommercant->infoCommercant($idcom);
        if (is_file("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/res_menu/".$data_com_menu->image_menu_gen)){
                unlink("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/res_menu/".$data_com_menu->image_menu_gen);
        }
        $this->Mdl_plat_du_jour->delete_res_menu_image($idcom);
        echo '1';
    }
//kappa_25.02.22
    function delete_img($id){

    $data_com_menu = $this->Mdl_plat_du_jour->get_titre_menu_by_idcom($id);
    $infocom = $this->mdlcommercant->infoCommercant($id);
    if (is_file("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/res_plat/".$data_com_menu->img)){
            unlink("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/res_plat/".$data_com_menu->img);
    }
    $this->Mdl_plat_du_jour->delete_img_db($id);
    echo '1';
    }

    //code betadev
    function listePlatDuJour()
    {
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            //$data['toListeplat']=$this->Mdl_plat_du_jour->getbyIdcommercant($iduser);
            $data['idCommercant']=$iduser;
            $data['IdUsers_ionauth']=$user_ion_auth->id;
            $data['infocom']=$this->commercant->GetById($iduser);
            $data['menu']=$this->Mdl_plat_du_jour->get_titre_menu_by_idcom($iduser);
            $data['content_plat1']=$this->Mdl_plat_du_jour->get_content_gli_plat1_by_idcom($iduser);
            $data['content_plat2']=$this->Mdl_plat_du_jour->get_content_gli_plat2_by_idcom($iduser);
            $data['content_plat3']=$this->Mdl_plat_du_jour->get_content_gli_plat3_by_idcom($iduser);
            $data['content_plat4']=$this->Mdl_plat_du_jour->get_content_gli_plat4_by_idcom($iduser);
            $data['content_plat5']=$this->Mdl_plat_du_jour->get_content_gli_plat5_by_idcom($iduser);
            $data['content_plat6']=$this->Mdl_plat_du_jour->get_content_gli_plat6_by_idcom($iduser);
            $data['content_plat7']=$this->Mdl_plat_du_jour->get_content_gli_plat7_by_idcom($iduser);
            $data['content_plat8']=$this->Mdl_plat_du_jour->get_content_gli_plat8_by_idcom($iduser);
            $this->load->view('front_plat_du_jour/admin/vw_plat_du_jour',$data);
        }else{
            redirect('auth/login');
        }

    }

    function listePlatDuJour_menu_carte()
    {
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            $data['toListeplat']=$this->Mdl_plat_du_jour->getbyIdcommercant($iduser);
            $data['idCommercant']=$iduser;
            $data['IdUsers_ionauth']=$user_ion_auth->id;
            $data['infocom']=$this->commercant->GetById($iduser);
            $data['infores']=$this->Mdl_plat_du_jour->get_resinfocom_by_idcom($iduser);
            $this->load->view('plat_du_jour/liste_plat_du_jour_menu_carte',$data);
        }else{
            redirect('auth/login');
        }

    }

    function listePlatDuJour_res_info()
    {
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            $data['toListeplat']=$this->Mdl_plat_du_jour->getbyIdcommercant($iduser);
            $data['idCommercant']=$iduser;
            $data['IdUsers_ionauth']=$user_ion_auth->id;
            $data['infocom']=$this->commercant->GetById($iduser);
            $data['infores']=$this->Mdl_plat_du_jour->get_resinfocom_by_idcom($iduser);
            $this->load->view('plat_du_jour/liste_plat_du_jour_res_info',$data);
        }else{
            redirect('auth/login');
        }

    }

    function fichePlat($_iDCommercant = 0, $_iDPlat = 0, $mssg = 0){

        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $prmIdCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            $data["idCommercant"] = $prmIdCommercant;
            $data["IdUsers_ionauth"] = $user_ion_auth->id;
            $this->load->view('plat_du_jour/vwFichePlat', $data);
        }else{
            redirect('/sortez7/admin/');
        }

    }

    public function saveplat(){


        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $prmIdCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            /*if($prmIdCommercant=TRUE){
                redirect('/front/Plat_du_jour/listePlatDuJour');
            }
            //redirect('/front/Plat_du_jour/fichePlat');*/
        }else{
            redirect('/sortez7/admin/');
        }


        $this->form_validation->set_rules('plat[description_plat]', 'plat[description_plat]', 'required');
        if ($this->form_validation->run() == FALSE) {
           echo'erreur';
        } else {
            /*
            $data = array();
            $data['Name'] = $this->input->post("name");
            $data['Lastname'] = $this->input->post("lastname");
            $data['Username'] = $this->input->post("username");
            $data['Email'] = $this->input->post("email");
            $data['Password'] = md5($this->input->post("password"));
            */

            //convert_Frenchdate_to_Sqldate ();
            $data= $this->input->post("plat");
            $this->Mdl_plat_du_jour->insert($data);
            if($prmIdCommercant=TRUE){
               redirect('/front/Plat_du_jour/listePlatDuJour');
           }
           //redirect('/front/Plat_du_jour/fichePlat');
           //var_dump($data);


        }
    }

    //suppression plat
    function supprimplat($_iDPlat, $_iDCommercant){
        $zReponse = $this->Mdl_plat_du_jour->supprimplat($_iDPlat);
        redirect("front/Plat_du_jour/listePlatDuJour/$_iDCommercant");
    }

    //interface modification plat
    public function vwmodifpalt($x){
        $this->load->model('Mdl_plat_du_jour');
        $result=$this->Mdl_plat_du_jour->vwmodifplat($x);
        $data['plat']=$result;
        $this->load->view('privicarte/vwFichePlatEdit',$data);
    }

    //validation modification plat
    public function savemodifplat(){
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $prmIdCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

        }else{
            redirect('/sortez7/admin/');
        }

        $this->form_validation->set_rules('plat[description_plat]', 'plat[description_plat]', 'required');
        if ($this->form_validation->run() == FALSE) {
            echo'erreur';
        } else {

                $data= $this->input->post("plat");
                $config['upload_path']          = 'application/resources/front/images/plat/pdf/';
                $config['allowed_types']        = 'pdf';
                $config['max_size']             = 3005000;
                $config['file_name']            =$this->input->post("plat[nom_pdf]");

            $this->load->library('upload', $config);
                //var_dump($data);
                $this->load->model('Mdl_plat_du_jour');
            if (! $this->upload->do_upload('PDfplat'))
           {

            }

                $result = $this->Mdl_plat_du_jour->savemodifplat($data);
                //$this->listePlatDuJour();
                redirect("front/Plat_du_jour/listePlatDuJour");

        }

    }
    public function delete_photo($id,$col){
            $oInfoPlat = $this->Mdl_plat_du_jour->GetById($id);
            $filetodelete = $oInfoPlat->photo;
            $pdf_to_delete=$oInfoPlat->nom_pdf;
            if ($col=='Nom_pdf_menu'){
                $this->Mdl_plat_du_jour->effacerpdf($id,$col);
            }else{
                $this->Mdl_plat_du_jour->effacerphoto($id,$col);
            }

        if (file_exists("application/resources/front/images/agenda/photoCommercant/imagesbank/".$oInfoPlat->IdUsers_ionauth.'/' . $filetodelete) == true AND $col =="photo") {
            unlink("application/resources/front/images/agenda/photoCommercant/imagesbank/" .$oInfoPlat->IdUsers_ionauth.'/'. $filetodelete);
        }elseif (file_exists("application/resources/front/images/plat/pdf/". $pdf_to_delete.'.pdf') == true AND $col=="nom_pdf"){
            unlink("application/resources/front/images/plat/pdf/". $pdf_to_delete.'.pdf');
        }
    }
    public function valid_mail(){
        $IdComm=$this->input->post('mail[IdCommercant]');
        $mail=$this->input->post('mail[mail_reservation]');
        $type=$this->input->post('mail[type]');
        $field=array("mail_reservation"=>$mail);
        $save=$this->Mdl_plat_du_jour->update_mail_reservation($field,$IdComm);
        if ($save==1){
            if ($type =='gites'){
            redirect('admin/Reservations/index');
            }elseif ($type=="plat"){
                redirect('front/Plat_du_jour/listePlatDuJour');
            }
        }else{
            echo 'error';
        }

    }
    public function save_reservation_commercant_info($idcom){
            $redirect=$this->input->post('page_parent');
            $idPlat=$this->input->post("res[id]");
            $objres=$this->input->post('res');
            $config['upload_path']          = 'application/resources/front/images/plat/pdf/';
            $config['allowed_types']        = 'pdf';
            $config['max_size']             = 3005000;
            $config['file_name']            =$this->input->post("res[Nom_pdf_menu]");
            $this->load->library('upload', $config);


            $config2['upload_path']          = 'application/resources/front/photoCommercant/imagesbank/'.$user_ion_auth->id.'/res_menu';
            $config2['allowed_types']        = 'gif|jpg|png|jpeg|pdf';
            $config2['max_size']             = 5000;
            $config2['file_name']            = $this->input->post("res[image_menu_gen]");
            $this->load->library('upload', $config2);
            $this->upload->initialize($config2);

            $this->load->model('Mdl_plat_du_jour');
            //var_dump($config);die();
            if (! $this->upload->do_upload('image_menu_gen'))
            {

            }
            if (isset($idPlat) AND $idPlat !='' AND $idPlat!=null){
                $save= $this->Mdl_plat_du_jour->update_reservation_commercant_info($objres,$idPlat);
            }else{
                $save= $this->Mdl_plat_du_jour->save_reservation_commercant_info($objres);
            }

            if ($save){
                if ($redirect=='menu_carte'){
                    redirect('front/Plat_du_jour/listePlatDuJour_menu_carte');
                }elseif ($redirect=='plat_global'){
                    redirect('front/Plat_du_jour/listePlatDuJour');
                }elseif($redirect=='res_form'){
                    redirect('front/Plat_du_jour/listePlatDuJour_res_info');
                }
            }else{echo 'erreur';}
    }
    public function save_entete(){
        $datamenu = $this->input->post('menu');
        $savemenu = $this->Mdl_plat_du_jour->insert_menu_plat($datamenu);
        redirect('front/Plat_du_jour/listePlatDuJour');
    }
    public function save_detail_plat(){
        $entete['id_gli'] = $this->input->post('id_gli');
        $entete['Idcommercant'] = $this->input->post('idcommercant_get');
        $entete['is_activ_glissiere'] = $this->input->post('title_gli_plat_is_activ');
        $entete['titre_gli'] = $this->input->post('title_gli_plat');
        $data['id_gli'] = $entete['id_gli'];
        $data['designation'] = $this->input->post('designation_gli_plat');
        $data['date1'] = $this->input->post('date1_plat');
        $data['date2'] = $this->input->post('date2_plat');
        $data['date3'] = $this->input->post('date3_plat');
        $data['date4'] = $this->input->post('date4_plat');
        $data['date5'] = $this->input->post('date5_plat');
        $data['nombre1'] = $this->input->post('nombre1_plat');
        $data['nombre2'] = $this->input->post('nombre2_plat');
        $data['nombre3'] = $this->input->post('nombre3_plat');
        $data['nombre4'] = $this->input->post('nombre4_plat');
        $data['nombre5'] = $this->input->post('nombre5_plat');
        $data['heure_fin'] = $this->input->post('heure_fin_plat');
        $data['prix_plat'] = $this->input->post('prix_plat');

        $save_entete = $this->Mdl_plat_du_jour->insert_entete_plat($entete);
        if(isset($save_entete) && $save_entete != null){
            $data['id_plat_gli'] = $save_entete;
            $save_detail = $this->Mdl_plat_du_jour->insert_details_plat($data);
            if(isset($save_detail) && $save_detail != null){
                echo json_encode('ok');
            }else{
                echo json_encode('ko');
            }
        }else{
            echo json_encode('ko');
        }
    }
    public function delete_image(){

        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $prmIdCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

            $id = $this->input->post('id');
            $img_file = $this->input->post('img_file');

            if(is_file("application/resources/front/photoCommercant/imagesbank/".$user_ion_auth->id."/plat_gli/".$img_file)){
                unlink("application/resources/front/photoCommercant/imagesbank/".$user_ion_auth->id."/plat_gli/".$img_file);
            }
            $this->Mdl_plat_du_jour->delete_image($id);
            echo json_encode("ok");

        }
    }
    public function delete_content(){

        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $prmIdCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

            $id = $this->input->post('id_plat');
            $data_by_id = $this->Mdl_plat_du_jour->get_by_id_plat($id);
            $titre['id_plat'] = $id;
            $titre['titre_gli'] = null;
            $titre['is_activ_glissiere'] = 0;
            $plat_details['id_plat_gli'] = $id;
            $plat_details['designation'] = null;
            $plat_details['date1'] = null;
            $plat_details['date2'] = null;
            $plat_details['date3'] = null;
            $plat_details['date4'] = null;
            $plat_details['date5'] = null;
            $plat_details['nombre1'] = null;
            $plat_details['nombre2'] = null;
            $plat_details['nombre3'] = null;
            $plat_details['nombre4'] = null;
            $plat_details['nombre5'] = null;
            $plat_details['heure_fin'] = null;
            $plat_details['prix_plat'] = null;
            $plat_details['image'] = null;
            //var_dump($data_by_id[0]->image);die();

            if(is_file("application/resources/front/photoCommercant/imagesbank/".$user_ion_auth->id."/plat_gli/".$data_by_id[0]->image)){
                unlink("application/resources/front/photoCommercant/imagesbank/".$user_ion_auth->id."/plat_gli/".$data_by_id[0]->image);
            }
            $this->Mdl_plat_du_jour->delete_titre($titre);
            $this->Mdl_plat_du_jour->delete_details($plat_details);
            echo json_encode("ok");

        }
    }
    public function generate_qr_code(){
        $user_ion_auth = $this->ion_auth->user()->row();
        $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $infocom = $this->mdlcommercant->infocommercant($iduser);
        $to_generate = $this->input->post('qr_code_img');
        $id_com = $this->input->post('id_plat_com');
        $base_path_system = str_replace('system/', '', BASEPATH);
        $qrcode_lib_path = $base_path_system."application/resources/phpqrcode/";
        $qrcode_lib_path_true = $base_path_system."application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/plat_gli_qr_code/";
        if (!is_dir("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/")){
            mkdir("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id.'/',0777);
        }
        if (!is_dir("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/plat_gli_qr_code/")){
            mkdir("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/plat_gli_qr_code/",0777);
        }
        $qrcode_lib_images_url = base_url()."application/resources/phpqrcode/images/";

        include($qrcode_lib_path.'qrlib.php');

        $tempDir = $qrcode_lib_path_true;
        $tempURL = $qrcode_lib_images_url;

        $codeContents = $to_generate;

        $fileName = 'qrcode_plat_file_'.md5($codeContents).'.png';

        $pngAbsoluteFilePath = $tempDir.$fileName;

        if (!file_exists($pngAbsoluteFilePath)) {
            QRcode::png($codeContents, $pngAbsoluteFilePath, QR_ECLEVEL_L, 4);

            $field = array(
                'id_commercant' => $id_com,
                'url_qr_code_plat' => $fileName,
            );
            $this->Mdl_plat_du_jour->insert_menu_plat($field);
        } else {

        }
        redirect("front/Plat_du_jour/listePlatDuJour");
    }
}