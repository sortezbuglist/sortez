<?php
class Gestion_livre_dor extends CI_Controller
{

    function __construct() {
        parent::__construct();

        $this->load->library('session');
        $this->load->library('ion_auth');
        $this->load->model("Mdl_gestion_livre_dor");
        $this->load->model("ion_auth_used_by_club");
        $this->load->model('Mdlcommercant');
        $this->load->model('Mdlglissiere');

    }


    function liste($idcom) {
        //echo $idcom;
        $objlivredor = $this->Mdl_gestion_livre_dor->getbyidcom($idcom);
        $data["collivredor"] = $objlivredor;
        $infocom=$this->Mdlcommercant->infoCommercant($idcom);
        $data['infocom']=$infocom;
        $data['glis']=$this->Mdlglissiere->get_by_idcom($idcom);
        $data['idcom']=$idcom;
        $this->load->view("admin/vwGestion_livre_dor",$data);
    }

    function comment($idcom) {
        //echo $idcom;
        $objlivredor = $this->Mdl_gestion_livre_dor->getbyidcom($idcom);
        $data["collivredor"] = $objlivredor;
        $infocom=$this->Mdlcommercant->infoCommercant($idcom);
        $data['infocom']=$infocom;
        $data['idcom']=$idcom;
        $this->load->view("admin/vwGestion_comment_google",$data);
    }

    function fiche_commentaire($IdTypeArticle) {

        if ($IdTypeArticle != 0){
            $data["title"] = "Modification du Commentaire" ;
            $data["oCom"] = $this->Mdl_gestion_livre_dor->getById($IdTypeArticle) ;
            $data["idclient"] = $IdTypeArticle;
            $this->load->view('admin/vwFicheGestionlivre_dor', $data) ;

        }else{
            $data["title"] = "Creer Type Article" ;
            $this->load->view('admin/vwFicheGestionlivre_dor', $data) ;
        }
    }

    //function creer_types_article(){
   //     $oArticle = $this->input->post("types_article") ;
    //    $this->Mdl_gestion_livre_dor->insertarticle_type($oArticle);
    //    $data["msg"] = "Type Article ajouté" ;

    //    $objArticles = $this->Mdl_gestion_livre_dor->GetAll();
     //   $data["colArticles"] = $objArticles;
    //    $this->load->view("admin/vwListeTypeArticle",$data);
   // }

    function modif_commentaire($idcom){
        $oCom = $this->input->post("Gestion_livre_dor") ;
        $this->Mdl_gestion_livre_dor->update_commentaire($oCom);
        $data["msg"] = "Commentaire enregistré" ;

        redirect('front/gestion_livre_dor/liste/'.$idcom);
    }

    function delete($prmId,$idcom)
    {
            $oCategorie = $this->Mdl_gestion_livre_dor->supprime_commentaire($prmId);
            if ($oCategorie == true){
             redirect('front/gestion_livre_dor/liste/'.$idcom);
            }


    }
public function save_actif_not_comment(){
        $choice=$this->input->post('com_google');
        $this->Mdl_gestion_livre_dor->save_comment_google($choice);
        redirect('front/Gestion_livre_dor/comment/'.$choice['idcommercant']);
}
public function update_livre_actif(){
        $champ=$this->input->post('gli');
        $this->Mdl_gestion_livre_dor->update_affich_livre($champ);
        redirect('front/Gestion_livre_dor/liste/'.$champ['IdCommercant']);
}
}