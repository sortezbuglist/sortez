<?php
class conditionsgenerales extends CI_Controller {
    
    function __construct() {
        parent::__construct();

        $this->load->library('session');

        check_vivresaville_id_ville();
		
    }
    
    function index(){       
        $this->load->view('front/vwConditionsGenerales') ;
    }

   
}