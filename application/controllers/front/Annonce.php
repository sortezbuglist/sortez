<?php



class annonce extends CI_Controller

{



    function __construct()

    {

        parent::__construct();

        $this->load->model("mdlannonce");

        $this->load->model("mdlcommercant");

        $this->load->model("mdlbonplan");

        $this->load->model("rubrique");

        $this->load->model("Sousrubrique");

        $this->load->model("mdlimagespub");

        $this->load->model("AssCommercantAbonnement");

        $this->load->model("mdlville");

        $this->load->model("mdldepartement");

        $this->load->library('user_agent');

        $this->load->model("Abonnement");

        $this->load->model("commercant");

        $this->load->Model("mdlannonce_perso");

        $this->load->model("mdlarticle");

        $this->load->model("Mdlcommercant");

        $this->load->model("mdl_agenda");

        $this->load->model("mdlcommune");

        $this->load->model("mdl_categories_annonce");



        $this->load->model("Mdl_plat_du_jour");



        $this->load->library('session');

        $this->load->Model("mdl_localisation");

        $this->load->library("pagination");

        $this->load->library('image_moo');

        $this->load->library('ion_auth');

        $this->load->model("ion_auth_used_by_club");



        delete_revue_fin_2journ();

        check_vivresaville_id_ville();

        statistiques();

    }





    function index()

    {

        $argOffset = (isset($_SESSION["argOffset"])) ? $_SESSION["argOffset"] : 0;





        //if the link doesn't come from page navigation, clear category session

        $current_URI_club = $_SERVER['REQUEST_URI'];

        $current_URI_club_array = explode("annonce/index", $current_URI_club);

        ////$this->firephp->log(count($current_URI_club_array), 'nb_array');

        if (count($current_URI_club_array) == 1) {

            $this->session->unset_userdata('iCategorieId_x');

            $this->session->unset_userdata('iVilleId_x');

            $this->session->unset_userdata('iDepartementId_x');

            $this->session->unset_userdata('iCommercantId_x');

            $this->session->unset_userdata('zMotCle_x');

            $this->session->unset_userdata('iEtat_x');

            $this->session->unset_userdata('iOrderBy_x');

            $this->session->unset_userdata('inputFromGeolocalisation_x');

            $this->session->unset_userdata('inputGeolocalisationValue_x');

            $this->session->unset_userdata('inputGeoLatitude_x');

            $this->session->unset_userdata('inputGeoLongitude_x');

        }

        //if the link doesn't come from page navigation, clear category session



        if ($this->input->post("inputStringVilleHidden_annonces") == 0) unset($_SESSION['inputStringVilleHidden_annonces']);

        if ($this->input->post("inputStringHidden") == "0") unset($_SESSION['iCategorieId']);

        if ($this->input->post("inputStringCommercantHidden") == "0") unset($_SESSION['iCommercantId']);

        if ($this->input->post("zMotCle") == "") unset($_SESSION['zMotCle']);

        if ($this->input->post("iEtat") == "") unset($_SESSION['iEtat']);

        if ($this->input->post("inputStringVilleHidden_annonces") == "") unset($_SESSION['iVilleId']);

        if ($this->input->post("inputStringDepartementHidden") == "") unset($_SESSION['iDepartementId']);

        if ($this->input->post("inputStringOrderByHidden") == "0") unset($_SESSION['iOrderBy']);

        if ($this->input->post("inputFromGeolocalisation") == "0" || $this->input->post("inputFromGeolocalisation") == "1") unset($_SESSION['inputFromGeolocalisation']);

        if ($this->input->post("inputGeolocalisationValue") == "10") unset($_SESSION['inputGeolocalisationValue']);

        if ($this->input->post("inputGeoLatitude") == "0") unset($_SESSION['inputGeoLatitude']);

        if ($this->input->post("inputGeoLongitude") == "0") unset($_SESSION['inputGeoLongitude']);





        $data["iFavoris"] = $this->input->post("hdnFavoris");



        $PerPage = 20;



        //log_message('debug', 'william : '.$_POST["inputStringHidden"]);





        if (isset($_POST["inputStringHidden"])) {

            unset($_SESSION['iCategorieId']);

            unset($_SESSION['iVilleId']);

            unset($_SESSION['iDepartementId']);

            unset($_SESSION['iCommercantId']);

            unset($_SESSION['zMotCle']);

            unset($_SESSION['iEtat']);

            unset($_SESSION['iOrderBy']);

            unset($_SESSION['inputFromGeolocalisation']);

            unset($_SESSION['inputGeolocalisationValue']);

            unset($_SESSION['inputGeoLatitude']);

            unset($_SESSION['inputGeoLongitude']);

            unset($_SESSION['inputStringVilleHidden_annonces']);



            //$iCategorieId = $_POST["inputStringHidden"] ;

            $iCategorieId_all0 = $this->input->post("inputStringHidden");

            if (isset($iCategorieId_all0) && $iCategorieId_all0 != "" && $iCategorieId_all0 != NULL && $iCategorieId_all0 != '0') {

                $iCategorieId_all = substr($iCategorieId_all0, 1);

                $iCategorieId = explode(',', $iCategorieId_all);

            } else {

                $iCategorieId = '0';

            }

            $villess = $this->input->post('inputStringVilleHidden_annonces');

            if ($villess) $session_iVilleId = $villess; else  $session_iVilleId = 0;

            if (isset($_POST["inputStringDepartementHidden"])) $iDepartementId = $_POST["inputStringDepartementHidden"]; else $iDepartementId = 0;

            if (isset($_POST["inputStringCommercantHidden"])) $iCommercantId = $_POST["inputStringCommercantHidden"]; else $iCommercantId = 0;

            if (isset($_POST["zMotCle"])) $zMotCle = $_POST["zMotCle"]; else $zMotCle = '';

            if (isset($_POST["iEtat"])) $iEtat = $_POST["iEtat"]; else $iEtat = '';

            if (isset($_POST["inputStringOrderByHidden"])) $iOrderBy = $_POST["inputStringOrderByHidden"]; else $iOrderBy = '';

            if (isset($_POST["inputFromGeolocalisation"])) $inputFromGeolocalisation = $_POST["inputFromGeolocalisation"]; else $inputFromGeolocalisation = "0";

            if (isset($_POST["inputGeolocalisationValue"])) $inputGeolocalisationValue = $_POST["inputGeolocalisationValue"]; else $inputGeolocalisationValue = "10";

            if (isset($_POST["inputGeoLatitude"])) $inputGeoLatitude = $_POST["inputGeoLatitude"]; else $inputGeoLatitude = "0";

            if (isset($_POST["inputGeoLongitude"])) $inputGeoLongitude = $_POST["inputGeoLongitude"]; else $inputGeoLongitude = "0";







            $_SESSION["inputStringVilleHidden_annonces"] = $session_iVilleId;

            $this->session->set_userdata('inputStringVilleHidden_annonces', $session_iVilleId);

            $_SESSION['iCategorieId'] = $iCategorieId;

            $this->session->set_userdata('iCategorieId_x', $iCategorieId);

            $_SESSION['iVilleId'] = $session_iVilleId;

            $this->session->set_userdata('iVilleId_x', $session_iVilleId);

            $_SESSION['iDepartementId'] = $iDepartementId;

            $this->session->set_userdata('iDepartementId_x', $iDepartementId);

            $_SESSION['iCommercantId'] = $iCommercantId;

            $this->session->set_userdata('iCommercantId_x', $iCommercantId);

            $_SESSION['zMotCle'] = $zMotCle;

            $this->session->set_userdata('zMotCle_x', $zMotCle);

            $_SESSION['iEtat'] = $iEtat;

            $this->session->set_userdata('iEtat_x', $iEtat);

            $_SESSION['iOrderBy'] = $iOrderBy;

            $this->session->set_userdata('iOrderBy_x', $iOrderBy);

            $_SESSION['inputFromGeolocalisation'] = $inputFromGeolocalisation;

            $this->session->set_userdata('inputFromGeolocalisation_x', $inputFromGeolocalisation);

            $_SESSION['inputGeolocalisationValue'] = $inputGeolocalisationValue;

            $this->session->set_userdata('inputGeolocalisationValue_x', $inputGeolocalisationValue);

            $_SESSION['inputGeoLatitude'] = $inputGeoLatitude;

            $this->session->set_userdata('inputGeoLatitude_x', $inputGeoLatitude);

            $_SESSION['inputGeoLongitude'] = $inputGeoLongitude;

            $this->session->set_userdata('inputGeoLongitude_x', $inputGeoLongitude);



            $data['iCategorieId'] = $iCategorieId;

            $data['iVilleId'] = $session_iVilleId;

            $data['iDepartementId'] = $iDepartementId;

            $data['iCommercantId'] = $iCommercantId;

            $data['zMotCle'] = $zMotCle;

            $data['iOrderBy'] = $iOrderBy;

            $data['iEtat'] = $iEtat;



            $session_iCategorieId = $this->session->userdata('iCategorieId_x');

            $session_iVilleId = $this->session->userdata('iVilleId_x');

            $session_iDepartementId = $this->session->userdata('iDepartementId_x');

            $session_iCommercantId = $this->session->userdata('iCommercantId_x');

            $session_zMotCle = $this->session->userdata('zMotCle_x');

            $session_iEtat = $this->session->userdata('iEtat_x');

            $session_iOrderBy = $this->session->userdata('iOrderBy_x');

            $session_inputFromGeolocalisation = $this->session->userdata('inputFromGeolocalisation_x');

            $session_inputGeolocalisationValue = $this->session->userdata('inputGeolocalisationValue_x');

            $session_inputGeoLatitude = $this->session->userdata('inputGeoLatitude_x');

            $session_inputGeoLongitude = $this->session->userdata('inputGeoLongitude_x');

            //$toListeAnnonce = $this->mdlannonce->listeAnnonceRecherche($_SESSION['iCategorieId'], $_SESSION['iCommercantId'], $_SESSION['zMotCle'], $_SESSION['iEtat'], $argOffset, $PerPage, $_SESSION['iVilleId'], $_SESSION['iOrderBy'], $_SESSION['inputFromGeolocalisation'], $_SESSION['inputGeolocalisationValue'], $_SESSION['inputGeoLatitude'], $_SESSION['inputGeoLongitude']) ;

            $TotalRows = count($this->mdlannonce->listeAnnonceRecherche($session_iCategorieId, $session_iCommercantId, $session_zMotCle, $session_iEtat, 0, 10000, $session_iVilleId, $session_iDepartementId, $session_iOrderBy, $session_inputFromGeolocalisation, $session_inputGeolocalisationValue, $session_inputGeoLatitude, $session_inputGeoLongitude));

            $config_pagination = array();

            $config_pagination["base_url"] = base_url() . "front/annonce/index/";

            $config_pagination["total_rows"] = $TotalRows;

            $config_pagination["per_page"] = $PerPage;

            $config_pagination["uri_segment"] = 4;

            $config_pagination['first_link'] = 'Première page';

            $config_pagination['last_link'] = 'Dernière page';

            $config_pagination['prev_link'] = 'Précédent';

            $config_pagination['next_link'] = 'Suivant';

            $this->pagination->initialize($config_pagination);



            $rsegment3 = $this->uri->segment(4);

            if (strpos($rsegment3, '&content_only_list') !== false) {

                $pieces_rseg = explode("&content_only_list", $rsegment3);

                $rsegment3 = $pieces_rseg[0];

            } else {

                $rsegment3 = $this->uri->segment(4);

            }

            $page_pagination = ($rsegment3) ? $rsegment3 : 0;



            //$toListeAnnonce = $this->mdlannonce->listeAnnonceRecherche($_SESSION['iCategorieId'], $_SESSION['iCommercantId'], $_SESSION['zMotCle'], $_SESSION['iEtat'], $page_pagination, $config_pagination["per_page"], $_SESSION['iVilleId'], $_SESSION['iOrderBy'], $_SESSION['inputFromGeolocalisation'], $_SESSION['inputGeolocalisationValue'], $_SESSION['inputGeoLatitude'], $_SESSION['inputGeoLongitude']) ;

            $toListeAnnonce = $this->mdlannonce->listeAnnonceRecherche($session_iCategorieId, $session_iCommercantId, $session_zMotCle, $session_iEtat, $page_pagination, $config_pagination["per_page"], $session_iVilleId, $session_iDepartementId, $session_iOrderBy, $session_inputFromGeolocalisation, $session_inputGeolocalisationValue, $session_inputGeoLatitude, $session_inputGeoLongitude);

            $data["links_pagination"] = $this->pagination->create_links();

        } else {

            $data["iFavoris"] = "";

            $session_iCategorieId = $this->session->userdata('iCategorieId_x');

            $session_iVilleId = $this->session->userdata('iVilleId_x');

            $session_iDepartementId = $this->session->userdata('iDepartementId_x');

            $session_iCommercantId = $this->session->userdata('iCommercantId_x');

            $session_zMotCle = $this->session->userdata('zMotCle_x');

            $session_iEtat = $this->session->userdata('iEtat_x');

            $session_iOrderBy = $this->session->userdata('iOrderBy_x');

            $session_inputFromGeolocalisation = $this->session->userdata('inputFromGeolocalisation_x');

            $session_inputGeolocalisationValue = $this->session->userdata('inputGeolocalisationValue_x');

            $session_inputGeoLatitude = $this->session->userdata('inputGeoLatitude_x');

            $session_inputGeoLongitude = $this->session->userdata('inputGeoLongitude_x');



            $iCategorieId = (isset($session_iCategorieId)) ? $session_iCategorieId : 0;

            $iVilleId = (isset($session_iVilleId)) ? $session_iVilleId : 0;

            $iDepartementId = (isset($session_iDepartementId)) ? $session_iDepartementId : 0;

            $iCommercantId = (isset($session_iCommercantId)) ? $session_iCommercantId : "";

            $zMotCle = (isset($session_zMotCle)) ? $session_zMotCle : "";

            $iEtat = (isset($session_iEtat)) ? $session_iEtat : "";

            $iOrderBy = (isset($session_iOrderBy)) ? $session_iOrderBy : "";

            $inputFromGeolocalisation = (isset($session_inputFromGeolocalisation)) ? $session_inputFromGeolocalisation : "0";

            $inputGeolocalisationValue = (isset($session_inputGeolocalisationValue)) ? $session_inputGeolocalisationValue : "10";

            $inputGeoLatitude = (isset($session_inputGeoLatitude)) ? $session_inputGeoLatitude : "0";

            $inputGeoLongitude = (isset($session_inputGeoLongitude)) ? $session_inputGeoLongitude : "0";

            //$toListeAnnonce = $this->mdlannonce->listeAnnonceRecherche($iCategorieId, $iCommercantId, $zMotCle, $iEtat, $argOffset, $PerPage, $iVilleId, $iOrderBy, $inputFromGeolocalisation, $inputGeolocalisationValue, $inputGeoLatitude, $inputGeoLongitude);

            $TotalRows = count($this->mdlannonce->listeAnnonceRecherche($iCategorieId, $iCommercantId, $zMotCle, $iEtat, 0, 10000, $iVilleId, $iDepartementId, $iOrderBy, $inputFromGeolocalisation, $inputGeolocalisationValue, $inputGeoLatitude, $inputGeoLongitude));



            $config_pagination = array();

            $config_pagination["base_url"] = base_url() . "front/annonce/index/";

            $config_pagination["total_rows"] = $TotalRows;

            $config_pagination["per_page"] = $PerPage;

            $config_pagination["uri_segment"] = 4;

            $config_pagination['first_link'] = 'Première page';

            $config_pagination['last_link'] = 'Dernière page';

            $config_pagination['prev_link'] = 'Précédent';

            $config_pagination['next_link'] = 'Suivant';

            $this->pagination->initialize($config_pagination);



            $rsegment3 = $this->uri->segment(4);

            if (strpos($rsegment3, '&content_only_list') !== false) {

                $pieces_rseg = explode("&content_only_list", $rsegment3);

                $rsegment3 = $pieces_rseg[0];

            } else {

                $rsegment3 = $this->uri->segment(4);

            }

            $page_pagination = ($rsegment3) ? $rsegment3 : 0;



            $toListeAnnonce = $this->mdlannonce->listeAnnonceRecherche($iCategorieId, $iCommercantId, $zMotCle, $iEtat, $page_pagination, $config_pagination["per_page"], $iVilleId, $iDepartementId, $iOrderBy, $inputFromGeolocalisation, $inputGeolocalisationValue, $inputGeoLatitude, $inputGeoLongitude);

            $data["links_pagination"] = $this->pagination->create_links();

        }





        $iNombreLiens = $TotalRows / $PerPage;

        if ($iNombreLiens > round($iNombreLiens)) {

            $iNombreLiens = round($iNombreLiens) + 1;

        } else {

            $iNombreLiens = round($iNombreLiens);

        }



        $data["iNombreLiens"] = $iNombreLiens;



        $data["PerPage"] = $PerPage;

        $data["TotalRows"] = $TotalRows;

        $data["argOffset"] = $argOffset;

        $argOffset_limit = round($argOffset) + $PerPage;





        $toSousRubrique = $this->mdlannonce->GetAnnonceSouscategorie();

        $data['toSousRubrique'] = $toSousRubrique;

        $toRubriquePrincipale = $this->mdlannonce->GetAnnonceCategoriePrincipale();

        $data['toRubriquePrincipale'] = $toRubriquePrincipale;

        $toCommercant = $this->mdlcommercant->GetAllCommercant_with_annonce();

        $data['toCommercant'] = $toCommercant;

        $data['toListeAnnonce'] = $toListeAnnonce;//william hack

        $data['mdlannonce'] = $this->mdlannonce;

        $data['pagecategory'] = 'annonce';



        $this->load->model("mdlville");



        $departement_check = $this->session->userdata('iDepartementId_x');

        if (isset($departement_check) && $departement_check != "" && $departement_check != null) {

            //$toVille = $this->mdlville->GetAnnonceVilles_pvc_by_departement($this->session->userdata('iDepartementId_x'));

            $toVille = $this->mdlville->GetAnnonceVilles_pvc_varest($this->session->userdata('iDepartementId_x'));

            $data['toVille'] = $toVille;

        } else {

            $toVille = $this->mdlville->GetAnnonceVilles_pvc_varest();

            $data['toVille'] = $toVille;

        } /*--------------------------------------------------------------------------------------------------------------------------------------*/





        $toDepartement = $this->mdldepartement->GetAnnonceDepartements();

        $data['toDepartement'] = $toDepartement;





        $is_mobile = $this->agent->is_mobile();

        //test ipad user agent

        $is_mobile_ipad = $this->agent->is_mobile('ipad');

        $data['is_mobile_ipad'] = $is_mobile_ipad;

        $is_robot = $this->agent->is_robot();

        $is_browser = $this->agent->is_browser();

        $is_platform = $this->agent->platform();

        $data['is_mobile'] = $is_mobile;

        $data['is_robot'] = $is_robot;

        $data['is_browser'] = $is_browser;

        $data['is_platform'] = $is_platform;





        if ($this->ion_auth->logged_in()) {

            $user_ion_auth = $this->ion_auth->user()->row();

            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

            if ($iduser == null || $iduser == 0 || $iduser == "") {

                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

            }

            $data['IdUser'] = $iduser;

            $user_groups = $this->ion_auth->get_users_groups($user_ion_auth->id)->result();

            ////$this->firephp->log($user_groups, 'user_groups');

            if ($user_groups[0]->id == 1) $typeuser = "1";

            else if ($user_groups[0]->id == 2) $typeuser = "0";

            else if ($user_groups[0]->id == 3 || $user_groups[0]->id == 4 || $user_groups[0]->id == 5) $typeuser = "COMMERCANT";

            $data['typeuser'] = $typeuser;

        }



        $data["main_menu_content"] = "boutique";



        $this->session->set_userdata('nohome', '1');





        //$this->load->view('frontAout2013/annonce_main', $data) ;

        //$this->load->view('privicarte/annonce_main', $data) ;

        if ($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL) {

            //$this->load->view("vivresavilleView/ville-test/boutique-index", $data);

            $this->load->view('vivresaville/annonce_index', $data);

        } else {

            //$this->load->view('sortez/annonce_main', $data);

            $this->load->view('sortez_vsv/annonce_index', $data);

        }

    }





    function redirection($_iPage)

    {

        $_SESSION["argOffset"] = $_iPage;

        redirect("front/annonce/");

    }



    function redirect_annonce()

    {

        unset($_SESSION['iCategorieId']);

        unset($_SESSION['iCommercantId']);

        unset($_SESSION['iVilleId']);

        unset($_SESSION['zMotCle']);

        unset($_SESSION['iEtat']);

        redirect("front/annonce/");

    }



    function check_category_list()

    {

        $inputStringDepartementHidden_annnonces = $this->input->post("inputStringDepartementHidden_annnonces");

        $inputStringVilleHidden_annonces = $this->input->post("inputStringVilleHidden_annonces");



        //$toCategorie= $this->mdlcategorie->GetCommercantSouscategorie();

        //$toVille= $this->mdlville->GetCommercantVilles();//get ville list of commercant

        if ((isset($inputStringVilleHidden_annonces) && $inputStringVilleHidden_annonces != "" && $inputStringVilleHidden_annonces != '0' && $inputStringVilleHidden_annonces != NULL)

            || (isset($inputStringDepartementHidden_annnonces) && $inputStringDepartementHidden_annnonces != "" && $inputStringDepartementHidden_annnonces != '0' && $inputStringDepartementHidden_annnonces != NULL)

        ) {

            $toCategorie_principale = $this->mdlannonce->GetAnnonceCategoriePrincipaleByVille($inputStringDepartementHidden_annnonces, $inputStringVilleHidden_annonces);

        } else {

            $toCategorie_principale = $this->mdlannonce->GetAnnonceCategoriePrincipale();

        }



        ////$this->firephp->log($toCategorie_principale, 'toCategorie_principale');



        $result_to_show = '';



        if (isset($toCategorie_principale)) {

            foreach ($toCategorie_principale as $oCategorie_principale) {



                //if ($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL) {

                $result_to_show .= '<div  id="vsv_categmain_' . $oCategorie_principale->IdRubrique . '" class="vsv_subcateg_filter col-3 pl-0 pt-0 pr-2 pb-2 m-0" style="padding-bottom: 5px"><div class="leftcontener2013title_vs btn btn-default bt_categorie_list_vivresaville subcateg_' . $oCategorie_principale->IdRubrique . '" onClick="javascript:show_current_categ_subcateg(' . $oCategorie_principale->IdRubrique . ')" style="padding: 10px 0 !important;">';

                $result_to_show .= ucfirst(strtolower($oCategorie_principale->Nom)) . " (" . $oCategorie_principale->nb_annonce . ")";

                $result_to_show .= '</div></div>';

                /*} else {

                    $result_to_show .= '<div class="leftcontener2013title btn btn-default left_category_list_annonce" onClick="javascript:show_current_categ_subcateg(' . $oCategorie_principale->IdRubrique . ')" style="margin-top:7px;">';

                    $result_to_show .= ucfirst(strtolower($oCategorie_principale->Nom)) . " (" . $oCategorie_principale->nb_annonce . ")";

                    $result_to_show .= '</div>';

                }*/



                $result_to_show .= '<div class="leftcontener2013content d-lg-none d-xl-none" id="leftcontener2013content_' . $oCategorie_principale->IdRubrique . '" style="margin-bottom:5px; margin-top:10px;">';

                $OCommercantSousRubrique = $this->mdlannonce->GetAnnonceSouscategorieByRubrique($oCategorie_principale->IdRubrique);

                //$result_to_show .= '<br/>';





                /*if (isset($_SESSION['iCategorieId'])) {

                    $iCategorieId_sess = $_SESSION['iCategorieId'];

                    ////$this->firephp->log($iCategorieId_sess, 'iCategorieId_sess');

                }*/



                $session_iCategorieId = $this->session->userdata('iCategorieId_x');

                if (isset($session_iCategorieId)) {

                    $iCategorieId_sess = $session_iCategorieId;

                }





                if (count($OCommercantSousRubrique) > 0) {

                    //if ($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL) {



                    //adding 'toutes les sous-categories' button

                    $result_to_show .= '<div class="vsv_subcateg_filter ';

                    if (count($OCommercantSousRubrique) <= 4) $result_to_show .= 'col'; else $result_to_show .= 'col-sm-6 col-md-4 col-xl-3';

                    $result_to_show .= ' paddingleft0 p-2" style="padding-bottom: 5px">';

                    $result_to_show .= '<label class="form-check-label btn subcateb_filter_banner w-100 all_subcategories_label" onClick="javascript:all_subcategories_func(' . $oCategorie_principale->IdRubrique . ');">';

                    $result_to_show .= '<span>Toutes</span>';

                    $result_to_show .= '</label>';

                    $result_to_show .= '</div>';

                    //end adding 'toutes les sous-categories' button



                    $i_rand = 0;

                    foreach ($OCommercantSousRubrique as $ObjCommercantSousRubrique) {

                        $result_to_show .= '<div class="vsv_subcateg_filter ';

                        if (count($OCommercantSousRubrique) <= 3) $result_to_show .= 'col'; else $result_to_show .= 'col-sm-6 col-md-4 col-xl-3';

                        $result_to_show .= ' paddingleft0 p-2" style="padding-bottom: 5px">';

                        $result_to_show .= '<label class="form-check-label btn subcateb_filter_banner w-100">';

                        $result_to_show .= '<input  onClick="javascript:submit_search_annonce();" name="check_part[' . $i_rand . ']" id="check_part_' . $ObjCommercantSousRubrique->IdSousRubrique . '" type="checkbox" value="' . $ObjCommercantSousRubrique->IdSousRubrique . '"';

                        if (isset($iCategorieId_sess) && is_array($iCategorieId_sess)) {

                            if (in_array($ObjCommercantSousRubrique->IdSousRubrique, $iCategorieId_sess)) $result_to_show .= 'checked';

                        }

                        $result_to_show .= '>';

                        $result_to_show .= '<span>' . ucfirst(strtolower($ObjCommercantSousRubrique->Nom)) . ' (' . $ObjCommercantSousRubrique->nb_annonce . ')</span>';

                        $result_to_show .= '</label>';

                        $result_to_show .= '</div>';

                        $i_rand++;

                    }

                    $result_to_show .= '<div class="row justify-content-center m-0 d-md-none d-xl-none">

                                                <div class="col-12 main_subcateg_tab_init">

                                                    <button class="btn subcateb_filter_banner w-100" onclick="btn_re_init_annonce_list();">R&eacute;initialiser</button>

                                                </div>

                                            </div>';



                    /*} else {

                        $i_rand = 0;

                        foreach ($OCommercantSousRubrique as $ObjCommercantSousRubrique) {

                            $result_to_show .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>';

                            $result_to_show .= '<td width="22"><input  onClick="javascript:submit_search_annonce();" name="check_part[' . $i_rand . ']" id="check_part_' . $ObjCommercantSousRubrique->IdSousRubrique . '" type="checkbox" value="' . $ObjCommercantSousRubrique->IdSousRubrique . '"';

                            if (isset($iCategorieId_sess) && is_array($iCategorieId_sess)) {

                                if (in_array($ObjCommercantSousRubrique->IdSousRubrique, $iCategorieId_sess)) $result_to_show .= 'checked';

                            }

                            $result_to_show .= '></td><td>';

                            $result_to_show .= ucfirst(strtolower($ObjCommercantSousRubrique->Nom)) . ' (' . $ObjCommercantSousRubrique->nb_annonce . ')</td>';

                            $result_to_show .= '</tr></table>';

                            $i_rand++;

                        }

                    }*/

                }

                $result_to_show .= '</div>';

            }

        }



        echo $result_to_show;



    }





    function detailAnnonce($_iAnnonceId)

    {



        $_iAnnonceId = $this->uri->rsegment(3); //die($_iAnnonceId." stop");//reecuperation valeur $_iAnnonceId



        $oDetailAnnonce = $this->mdlannonce->detailAnnonce($_iAnnonceId);

        $data['oDetailAnnonce'] = $oDetailAnnonce;



        $_iCommercantId = $oDetailAnnonce->id_commercant;



        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);

        $data['oInfoCommercant'] = $oInfoCommercant;

        $data['active_link'] = "annonces";

        $data['active_link2'] = "detailannonce";

        $oAssComRub = $this->mdlcommercant->GetRubriqueId($_iCommercantId);

        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);

        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

        $oLastbonplanCom = $this->mdlbonplan->lastBonplanCom($_iCommercantId);

        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;



        $is_mobile = $this->agent->is_mobile();

        //test ipad user agent

        $is_mobile_ipad = $this->agent->is_mobile('ipad');

        $data['is_mobile_ipad'] = $is_mobile_ipad;

        $is_robot = $this->agent->is_robot();

        $is_browser = $this->agent->is_browser();

        $is_platform = $this->agent->platform();

        $data['is_mobile'] = $is_mobile;

        $data['is_robot'] = $is_robot;

        $data['is_browser'] = $is_browser;

        $data['is_platform'] = $is_platform;



        $this->load->model("user");

        if ($this->ion_auth->logged_in()) {

            $user_ion_auth = $this->ion_auth->user()->row();

            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

            if ($iduser == null || $iduser == 0 || $iduser == "") {

                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

            }

        } else $iduser = 0;

        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser, $_iCommercantId));

        $data['pagecategory_partner'] = 'details_annonces_partner';



        $data['mdlville'] = $this->mdlville;



        $this->load->model("mdl_agenda");

        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);



        if (isset($_POST['text_mail_form_module_detailannnonce'])) {

            $text_mail_form_module_detailannnonce = $this->input->post("text_mail_form_module_detailannnonce");

            $nom_mail_form_module_detailannnonce = $this->input->post("nom_mail_form_module_detailannnonce");

            $tel_mail_form_module_detailannnonce = $this->input->post("tel_mail_form_module_detailannnonce");

            $email_mail_form_module_detailannnonce = $this->input->post("email_mail_form_module_detailannnonce");



            $colDestAdmin = array();

            $colDestAdmin[] = array("Email" => $oInfoCommercant->Email, "Name" => $oInfoCommercant->Nom . " " . $oInfoCommercant->Prenom);



            // Sujet

            $txtSujetAdmin = "Demande d'information sur une annonce Privicarte";



            $txtContenuAdmin = "

            <p>Bonjour ,</p>

            <p>Une demande de contact vient du site ClubProxmité à propos d'une annonce que vous avez posté :</p>

            <p>Détails de l'annonce :<br/>

            Désignation : " . $oDetailAnnonce->texte_courte . "<br/>

            N° : " . ajoutZeroPourString($oDetailAnnonce->annonce_id, 6) . " du " . convertDateWithSlashes($oDetailAnnonce->annonce_date_debut) . "

            Prix de vente : " . $oDetailAnnonce->ancien_prix . "<br/></p><p>

            Nom Client : " . $nom_mail_form_module_detailannnonce . "<br/>

            Téléphone Client : " . $tel_mail_form_module_detailannnonce . "<br/>

            Email Client : " . $email_mail_form_module_detailannnonce . "<br/>

            Demande Client :<br/>

            " . $text_mail_form_module_detailannnonce . "<br/><br/>

            </p>";



            @envoi_notification($colDestAdmin, $txtSujetAdmin, $txtContenuAdmin);

            $data['mssg_envoi_module_detail_annonce'] = '<font color="#00CC00">Votre demande est envoyée</font>';

            //$data['mssg_envoi_module_detail_annonce'] = $txtContenuAdmin;



        } else $data['mssg_envoi_module_detail_annonce'] = '';





        $data['link_partner_current_page'] = 'boutique';

        $data['pagecategory'] = "pro";



        //ajout bouton rond noire annonce/agenda

        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);

        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';

        else $data['result_check_commercant_annonce'] = '0';



        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);

        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';

        else $data['result_check_commercant_agenda'] = '0';



        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId);

        $data['toBonPlan'] = $toBonPlan;



        $is_reserved_on=$this->Mdl_plat_du_jour->verify_reserved($_iCommercantId);

        $data['reservation']=$is_reserved_on;



        //$this->load->view('front/vwDetailAnnonce', $data) ;

        //$this->load->view('front2013/detailsannonce', $data) ;

        $this->load->view('privicarte/detailsannonce', $data);

    }



    function photosannoncemobile($_iAnnonceId)

    {

        $oDetailAnnonce = $this->mdlannonce->detailAnnonce($_iAnnonceId);

        $data['oDetailAnnonce'] = $oDetailAnnonce;



        $_iCommercantId = $oDetailAnnonce->id_commercant;



        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);

        $data['oInfoCommercant'] = $oInfoCommercant;

        $data['active_link'] = "annonces";

        $oAssComRub = $this->mdlcommercant->GetRubriqueId($_iCommercantId);

        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);

        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

        $oLastbonplanCom = $this->mdlbonplan->lastBonplanCom($_iCommercantId);

        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;



        $is_mobile = $this->agent->is_mobile();

        //test ipad user agent

        $is_mobile_ipad = $this->agent->is_mobile('ipad');

        $data['is_mobile_ipad'] = $is_mobile_ipad;

        $is_robot = $this->agent->is_robot();

        $is_browser = $this->agent->is_browser();

        $is_platform = $this->agent->platform();

        $data['is_mobile'] = $is_mobile;

        $data['is_robot'] = $is_robot;

        $data['is_browser'] = $is_browser;

        $data['is_platform'] = $is_platform;



        //$this->load->view('front/vwPhotosannoncemobile', $data) ;

        $this->load->view('front2013/photosannoncemobile', $data);

    }



    function nousContacterAnnonce($_iAnnonceId, $_iCommercantId)

    {



        $_iAnnonceId = $this->uri->rsegment(3);//die(" stop");//reecuperation valeur $_iAnnonceId

        $nom_url_commercant = $this->uri->rsegment(4);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant

        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);



        redirect(site_url($nom_url_commercant));



        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);

        $data['oInfoCommercant'] = $oInfoCommercant;

        $data['iAnnonceId'] = $_iAnnonceId;



        $data['active_link'] = "contacter";



        $oAssComRub = $this->mdlcommercant->GetRubriqueId($_iCommercantId);

        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);



        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);



        $oLastbonplanCom = $this->mdlbonplan->lastBonplanCom($_iCommercantId);

        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;



        $is_mobile = $this->agent->is_mobile();

        //test ipad user agent

        $is_mobile_ipad = $this->agent->is_mobile('ipad');

        $data['is_mobile_ipad'] = $is_mobile_ipad;

        $is_robot = $this->agent->is_robot();

        $is_browser = $this->agent->is_browser();

        $is_platform = $this->agent->platform();

        $data['is_mobile'] = $is_mobile;

        $data['is_robot'] = $is_robot;

        $data['is_browser'] = $is_browser;

        $data['is_platform'] = $is_platform;





        $this->load->model("user");

        if ($this->ion_auth->logged_in()) {

            $user_ion_auth = $this->ion_auth->user()->row();

            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

            if ($iduser == null || $iduser == 0 || $iduser == "") {

                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

            }

        } else $iduser = 0;

        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser, $_iCommercantId));

        $data['pagecategory_partner'] = 'nous_contacter';



        $this->load->model("mdl_agenda");

        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);



        //$this->load->view('front/vwNousContacterAnnonce', $data) ;

        $this->load->view('front2013/nouscontacterpartenaire', $data);

    }



    function photoAnnonce($_iAnnonceId)

    {

        $oDetailAnnonce = $this->mdlannonce->detailAnnonce($_iAnnonceId);

        $data['oDetailAnnonce'] = $oDetailAnnonce;

        $this->load->view('front/vwPhotoAnnonce', $data);

    }



    function menuannonce($_iCommercantId, $_iAnnonceId)

    {

        $data['toListeAnnonceParCommercant'] = $this->mdlannonce->listeAnnonceParCommercant($_iCommercantId);

        $oDetailAnnonce = $this->mdlannonce->detailAnnonce($_iAnnonceId);

        $data['oDetailAnnonce'] = $oDetailAnnonce;

        $this->load->view('front/vwMenuAnnonce', $data);

    }



    function ficheCommercantAnnonce($_iCommercantId)

    {



        $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant

        //redirect(base_url().$nom_url_commercant.'/presentation');



        $numeric_verif_1 = preg_match("/^[0-9]+$/", $nom_url_commercant);



        if ($numeric_verif_1 == 0) {

            $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

            if (!isset($_iCommercantId) || $_iCommercantId == NULL || $_iCommercantId == "") redirect("front/commercant/invalid_account/");

        } else {

            $_iCommercantId = $nom_url_commercant;

        }





        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);

        $nbPhoto = 0;

        if ($oInfoCommercant->Photo1 != "" && $oInfoCommercant->Photo1 != null) {

            $nbPhoto += 1;

        }

        if ($oInfoCommercant->Photo2 != "" && $oInfoCommercant->Photo2 != null) {

            $nbPhoto += 1;

        }

        if ($oInfoCommercant->Photo3 != "" && $oInfoCommercant->Photo3 != null) {

            $nbPhoto += 1;

        }

        if ($oInfoCommercant->Photo4 != "" && $oInfoCommercant->Photo4 != null) {

            $nbPhoto += 1;

        }

        if ($oInfoCommercant->Photo5 != "" && $oInfoCommercant->Photo5 != null) {

            $nbPhoto += 1;

        }



        $data['oInfoCommercant'] = $oInfoCommercant;

        $data['nbPhoto'] = $nbPhoto;



        //$data['mdlannonce'] = $this->mdlannonce ;

        $data['mdlbonplan'] = $this->mdlbonplan;



        $this->load->model("mdl_agenda");

        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);



        $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

        $oBonPlan = $this->mdlbonplan->bonPlanParCommercant($_iCommercantId);

        $data['nbBonPlan'] = sizeof($oBonPlan);

//		$data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_titre : "";		// OP 27/11/2011

        $data['sTitreBonPlan'] = (sizeof($oBonPlan) > 0) ? $oBonPlan->bonplan_texte : "";

        $data['oImagespub'] = $this->mdlimagespub->GetByImagespubActiv();

        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);



        $oLastbonplanCom = $this->mdlbonplan->lastBonplanCom($_iCommercantId);

        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;



        $data['active_link'] = "accueil";



        $oAssComRub = $this->mdlcommercant->GetRubriqueId($_iCommercantId);

        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);



        $is_mobile = $this->agent->is_mobile();



        //test ipad user agent

        $is_mobile_ipad = $this->agent->is_mobile('ipad');

        $data['is_mobile_ipad'] = $is_mobile_ipad;

        $is_robot = $this->agent->is_robot();

        $is_browser = $this->agent->is_browser();

        $is_platform = $this->agent->platform();

        $data['is_mobile'] = $is_mobile;

        $data['is_robot'] = $is_robot;

        $data['is_browser'] = $is_browser;

        $data['is_platform'] = $is_platform;



        $data['no_return_mobile'] = 1;



        $this->load->model("user");

        if ($this->ion_auth->logged_in()) {

            $user_ion_auth = $this->ion_auth->user()->row();

            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

            if ($iduser == null || $iduser == 0 || $iduser == "") {

                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

            }

        } else $iduser = 0;

        if (isset($iduser) && $iduser != 0 && $iduser != NULL && $iduser != "") {

            $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser, $_iCommercantId));

            $data['oCommercantFavoris'] = $this->user->verify_favoris($iduser, $_iCommercantId);

        }



        $data['pagecategory_partner'] = 'annuaire_partner';



        //////$this->firephp->log($data['oCommercantFavoris'], 'oCommercantFavoris');



        //$this->load->view('front/vwFicheCommercantAnnonce', $data) ;

        $this->load->view('front2013/index_partenaire_commercant', $data);

    }



    function menuMobile($_iCommercantId)

    {



        $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant



        redirect(site_url($nom_url_commercant));



        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);



        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);

        $data['oInfoCommercant'] = $oInfoCommercant;



        //$data['mdlannonce'] = $this->mdlannonce ;

        //$data['mdlannonce'] = $this->mdlannonce ;

        $data['mdlbonplan'] = $this->mdlbonplan;



        $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

        $oBonPlan = $this->mdlbonplan->bonPlanParCommercant($_iCommercantId);

        $data['nbBonPlan'] = sizeof($oBonPlan);

        $data['sTitreBonPlan'] = (sizeof($oBonPlan) > 0) ? $oBonPlan->bonplan_texte : "";

        $data['oImagespub'] = $this->mdlimagespub->GetByImagespubActiv();



        $data['active_link'] = "accueil";



        $oAssComRub = $this->mdlcommercant->GetRubriqueId($_iCommercantId);

        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);



        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

        $oLastbonplanCom = $this->mdlbonplan->lastBonplanCom($_iCommercantId);

        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;





        $this->load->model("user");

        if ($this->ion_auth->logged_in()) {

            $user_ion_auth = $this->ion_auth->user()->row();

            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

            if ($iduser == null || $iduser == 0 || $iduser == "") {

                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

            }

        } else $iduser = 0;

        if (isset($iduser) && $iduser != 0 && $iduser != NULL && $iduser != "") {

            $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser, $_iCommercantId));

            $data['oCommercantFavoris'] = $this->user->verify_favoris($iduser, $_iCommercantId);

        }



        $is_mobile = $this->agent->is_mobile();

        //test ipad user agent

        $is_mobile_ipad = $this->agent->is_mobile('ipad');

        $data['is_mobile_ipad'] = $is_mobile_ipad;

        $is_robot = $this->agent->is_robot();

        $is_browser = $this->agent->is_browser();

        $is_platform = $this->agent->platform();

        $data['is_mobile'] = $is_mobile;

        $data['is_robot'] = $is_robot;

        $data['is_browser'] = $is_browser;

        $data['is_platform'] = $is_platform;





        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($_iCommercantId);

        if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;

        $data["user_groups"] = $user_groups[0];



        $this->load->model("mdl_agenda");

        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);



        //$this->load->view('front/vwMenuMobile', $data) ;

        $this->load->view('front2013/menumobile', $data);

    }



    function envoiMailNousContacter()

    {

        $config = array();

        $config['protocol'] = 'mail';

        $config['mailtype'] = 'html';

        $config['charset'] = 'utf-8';

        $this->load->library('email', $config);



        $errorMail = "";



        $mail_to = $_POST['zMailTo'];

        //$mail_cc = "randawilly@gmail.com";

        $mail_to_name = "Sortez";

        $mail_subject = "[Sortez] Contact";

        $mail_expediteur = $_POST['zEmail'];

        $data = array();

        $data["zNom"] = $_POST['zNom'];

        if (isset($_POST['zPrenom'])) $data["zPrenom"] = $_POST['zPrenom']; else $data["zPrenom"] = '';

        $data["zEmail"] = $_POST['zEmail'];

        $data["zTelephone"] = $_POST['zTelephone'];

        $data["zCommentaire"] = $_POST['zCommentaire'];





        $zIdCommercant = $_POST['zIdCommercant'];

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($zIdCommercant);



        $colDestAdmin = array();

        $colDestAdmin[] = array("Email" => $mail_to, "Name" => "Partenaire Sortez");

        $txtSujetAdmin = "Demande de Contact Sortez";

        $txtContenuAdmin = "

            <p>Bonjour ,</p>

            <p>Une demande de contact a été formmulée provenant du site Sortez.</p>

            <p>Information du demandeur :<br/>

            Nom : " . $_POST['zNom'] . "<br/> Prénom : " . $_POST['zPrenom'] . "<br/> Email : " . $_POST['zEmail'] . "<br/>Téléphone :" . $_POST['zTelephone'] . " <br/>

                Contenu du message :<br/><br/>" . $_POST['zCommentaire'] . "<br/><br/>

            </p>

        ";

        @envoi_notification($colDestAdmin, $txtSujetAdmin, $txtContenuAdmin);





        $this->session->set_flashdata('message', ' Votre message a bien été envoyé !');

        redirect(base_url() . $oInfoCommercant->nom_url . '/nous_contacter');



    }



    function menuannonceCommercant($_iCommercantId)

    {

        $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant

        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);



        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);

        $data['oInfoCommercant'] = $oInfoCommercant;

        $data['toListeAnnonceParCommercant'] = $this->mdlannonce->listeAnnonceParCommercant($_iCommercantId);



        $data['active_link'] = "annonces";

        $oAssComRub = $this->mdlcommercant->GetRubriqueId($_iCommercantId);

        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);

        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

        $oLastbonplanCom = $this->mdlbonplan->lastBonplanCom($_iCommercantId);

        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;



        $is_mobile = $this->agent->is_mobile();

        //test ipad user agent

        $is_mobile_ipad = $this->agent->is_mobile('ipad');

        $data['is_mobile_ipad'] = $is_mobile_ipad;

        $is_robot = $this->agent->is_robot();

        $is_browser = $this->agent->is_browser();

        $is_platform = $this->agent->platform();

        $data['is_mobile'] = $is_mobile;

        $data['is_robot'] = $is_robot;

        $data['is_browser'] = $is_browser;

        $data['is_platform'] = $is_platform;



        $this->load->model("user");

        if ($this->ion_auth->logged_in()) {

            $user_ion_auth = $this->ion_auth->user()->row();

            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

            if ($iduser == null || $iduser == 0 || $iduser == "") {

                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

            }

        } else $iduser = 0;

        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser, $_iCommercantId));

        $data['pagecategory_partner'] = 'annonces_partner';



        $data['dontshowannoncecase'] = 1;



        $data['link_partner_current_page'] = 'boutique';



        $this->load->model("mdl_agenda");

        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $data['pagecategory'] = "pro";



        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId);

        $data['toBonPlan'] = $toBonPlan;





        //ajout bouton rond noire annonce/agenda

        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);

        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';

        else $data['result_check_commercant_annonce'] = '0';



        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);

        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';

        else $data['result_check_commercant_agenda'] = '0';



        $is_reserved_on=$this->Mdl_plat_du_jour->verify_reserved($_iCommercantId);

        $data['reservation']=$is_reserved_on;



        //$this->load->view('front/vwMenuAnnonceCommercant', $data) ;

        //$this->load->view('front2013/menuannoncecommercant', $data) ;

        $this->load->view('privicarte/menuannoncecommercant', $data);

    }





    function ficheAnnonce($_iDCommercant = 0, $_iDAnnonce = 0, $mssg = 0)

    {



        if (!$this->ion_auth->logged_in()) {

            $this->session->set_flashdata('domain_from', '1');

            redirect("connexion");

        } else if ($this->ion_auth->in_group(2)) {

            redirect();

        }



        // check annonce limit number

        if ($_iDCommercant != 0 && $_iDAnnonce == 0) {

            $toListeMesAnnonce__ = $this->mdlannonce->listeMesAnnonces($_iDCommercant);

            $objCommercant__ = $this->commercant->GetById($_iDCommercant);

            if (isset($objCommercant__->limit_annonce) && count($toListeMesAnnonce__) >= intval($objCommercant__->limit_annonce)) {

                redirect("front/annonce/listeMesAnnonces/" . $_iDCommercant);

            }

        }



        $toListeMesAnnonce = $this->mdlannonce->listeMesAnnonces($_iDCommercant);

        $data['limit_annonce']=$toListeMesAnnonce;

        $data["idCommercant"] = $_iDCommercant;

        $data["no_main_menu_home"] = "1";

        $data["no_left_menu_home"] = "1";



        //verify 20 annonces limit

        if (isset($_iDCommercant) && $_iDCommercant != 0) {

            $toListeMesAnnonce = $this->mdlannonce->listeMesAnnonces($_iDCommercant);

            $objCommercant_verification = $this->commercant->GetById($_iDCommercant);

            if (isset($objCommercant_verification->annonce_25) && $objCommercant_verification->annonce_25 == "1" && (count($toListeMesAnnonce) == 26 || count($toListeMesAnnonce) >= 26)) redirect("front/annonce/listeMesAnnonces/$_iDCommercant");

            if (isset($objCommercant_verification->annonce) && $objCommercant_verification->annonce == "1" && (count($toListeMesAnnonce) == 101 || count($toListeMesAnnonce) >= 101)) redirect("front/annonce/listeMesAnnonces/$_iDCommercant");

        }





        //start verify if merchant subscription contain annonce WILLIAM

        /*$current_date = date("Y-m-d");

        $query_abcom_cond = " IdCommercant = ".$_iDCommercant." AND ('".$current_date."' BETWEEN DateDebut AND DateFin)";

        $toAbonnementCommercant = $this->AssCommercantAbonnement->GetWhere($query_abcom_cond, 0, 1) ;

        if(sizeof($toAbonnementCommercant)) {

            foreach ($toAbonnementCommercant as $oAbonnementCommercant) {

                        if(isset($oAbonnementCommercant) && ($oAbonnementCommercant->IdAbonnement==1 || $oAbonnementCommercant->IdAbonnement==2)) {//IdAbonnement doesn't contain annonce

                                redirect ("front/professionnels/fiche/$_iDCommercant") ;

                                }

            }

        } else redirect ("front/professionnels/fiche/$_iDCommercant") ;*/

        //end verify if merchant subscription contain annonce WILLIAM





        $this->load->helper('ckeditor');

        //Ckeditor's configuration

        $data['ckeditor0'] = array(



            //ID of the textarea that will be replaced

            'id' => 'idDescriptionLong',

            'path' => APPPATH . 'resources/ckeditor',



            //Optionnal values

            'config' => array(

                'width' => '100%',    //Setting a custom width

                'height' => '350px',    //Setting a custom height

                'toolbar' => array(    //Setting a custom toolbar

                    array('Source'),

                    array('Bold', 'Italic', 'Underline'),

                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),

                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),

                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')

                ),

                'filebrowserUploadUrl' => site_url('uploadfile/index/')

            )

        );





        $data['pagecategory'] = "admin_commercant";



        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iDCommercant);

        $data['oInfoCommercant'] = $oInfoCommercant;

        $data['mssg'] = $mssg;

        $data["colSousRubriques"] = $this->Sousrubrique->GetAll();



        if ($_iDAnnonce != 0) {

            $data["title"] = "Modification annonce";

            $data["oAnnonce"] = $this->mdlannonce->GetAnnonce($_iDAnnonce);

            //$this->load->view('front/vwFicheAnnonce', $data) ;

            //$this->load->view('frontAout2013/vwFicheAnnonce', $data) ;

            $this->load->view('privicarte/vwFicheAnnonce', $data);

        } else {

            $data["title"] = "Creer une annonce";

            //$this->load->view('front/vwFicheAnnonce', $data) ;

            //$this->load->view('frontAout2013/vwFicheAnnonce', $data) ;

            $this->load->view('privicarte/vwFicheAnnonce', $data);

        }

    }



    function creerAnnonce($_iDCommercant = 0)

    {

        $oAnnonce_brute = $this->input->post("annonce");

        $oAnnonce=remove_2500($oAnnonce_brute);

        $oAnnonce["annonce_date_debut"] = convert_Frenchdate_to_Sqldate($oAnnonce["annonce_date_debut"]);

        $oAnnonce["annonce_date_fin"] = convert_Frenchdate_to_Sqldate($oAnnonce["annonce_date_fin"]);

        $oAnnonce['module_paypal_btnpaypal'] = htmlspecialchars_decode($this->input->post("annonce_module_paypal_btnpaypal"));

        $oAnnonce['module_paypal_panierpaypal'] = htmlspecialchars_decode($this->input->post("annonce_module_paypal_panierpaypal"));

        $IdInsertedAnnonce = $this->mdlannonce->insertAnnonce($oAnnonce);

        $data["title"] = "Creer une annonce";

        $data["idCommercant"] = $_iDCommercant;

        $data["msg"] = "Annonce bien cree";

        // $this->load->view('front/vwFicheAnnonce', $data) ;

        //$this->load->view('front/vwMesAnnonces', $data) ;

        redirect("front/annonce/listeMesAnnonces/$_iDCommercant");

    }



    function modifAnnonce($_iDCommercant = 0)

    {

        $oAnnonce_brute = $this->input->post("annonce");

        $oAnnonce=remove_2500($oAnnonce_brute);

        $oAnnonce["annonce_date_debut"] = convert_Frenchdate_to_Sqldate($oAnnonce["annonce_date_debut"]);

        $oAnnonce["annonce_date_fin"] = convert_Frenchdate_to_Sqldate($oAnnonce["annonce_date_fin"]);

        $oAnnonce['module_paypal_btnpaypal'] = htmlspecialchars_decode($this->input->post("annonce_module_paypal_btnpaypal"));

        $oAnnonce['module_paypal_panierpaypal'] = htmlspecialchars_decode($this->input->post("annonce_module_paypal_panierpaypal"));

        ////$this->firephp->log($oAnnonce, 'oAnnonce');

        $IdUpdatedAnnonce = $this->mdlannonce->updateAnnonce($oAnnonce);

        $data["title"] = "Modification annonce";

        $data["idCommercant"] = $_iDCommercant;

        $data["msg"] = "Annon bien ete modifier";

        redirect("front/annonce/listeMesAnnonces/$_iDCommercant");

    }



    function delete_files($prmIdAnnonce = "0", $prmFiles = "")

    {

        $oInfoAnnonce = $this->mdlannonce->GetById($prmIdAnnonce);

        $filetodelete = "";

        if (isset($oInfoAnnonce) && $oInfoAnnonce != "0" && isset($prmFiles) && $prmFiles != "") {

            if ($prmFiles == "annonce_photo1") $filetodelete = $oInfoAnnonce->annonce_photo1;

            if ($prmFiles == "annonce_photo2") $filetodelete = $oInfoAnnonce->annonce_photo2;

            if ($prmFiles == "annonce_photo3") $filetodelete = $oInfoAnnonce->annonce_photo3;

            if ($prmFiles == "annonce_photo4") $filetodelete = $oInfoAnnonce->annonce_photo4;

            $this->mdlannonce->effacer_annonce_photo($prmIdAnnonce, $prmFiles);

            @unlink("application/resources/front/images/" . $filetodelete);

        }

        return true;

    }



    function savetextmenuannonce()

    {

        $idtextmenuannonce = $this->input->post("idtextmenuannonce");

        $idCommercant = $this->input->post("idCommercant");

        $is_actif_affichBoutique_Linkicon=$this->input->post("is_actif_affichBoutique_Linkicon");

        $array=array('is_actif_affichBoutique_Linkicon'=>$is_actif_affichBoutique_Linkicon);

        $this->commercant->updateaffichbouton($array,$idCommercant);

        $this->commercant->Updatetextmenuannonce($idCommercant, $idtextmenuannonce);

    }



    function save_order_partner()

    {

        if (!$this->ion_auth->logged_in()) {

            $this->session->set_flashdata('domain_from', '1');

            redirect("connexion");

        } else if ($this->ion_auth->in_group(2)) {

            redirect();

        }



        $idCommercant = $this->input->post("idCommercant");



        $toListeMesAnnonce = $this->mdlannonce->listeMesAnnonces($idCommercant);

        $objCommercant = $this->commercant->GetById($idCommercant);



        foreach ($toListeMesAnnonce as $key) {

            $annonce_id = $key->annonce_id;

            $inputOrderPartner = $this->input->post("inputOrderPartner_" . $key->annonce_id);

            $this->mdlannonce->save_order_partner($idCommercant, $annonce_id, $inputOrderPartner);

        }





    }
//kappa_17.02.22
 function save_icon_btc(){
   
$data= $this->input->post('data_btc');
   //var_dump($data['IdCommercant']);
$this->mdlannonce->save_data_btc_icon($data);
 redirect('front/annonce/listeMesAnnonces/'.$data['IdCommercant']);
 }

 //kappadev_18_2_22

        public function delete_image_icon_btc($idcom){

        //$idcom = $this->input->post('idcom');
        var_dump($idcom);
        $data_com_menu = $this->mdlannonce->get_btc_icon($idcom);
        $infocom = $this->mdlcommercant->infoCommercant($idcom);
        if (is_file("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/icon_btc/".$data_com_menu->icon_btc)){
                unlink("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/icon_btc/".$data_com_menu->icon_btc);
        }
        $this->mdlannonce->delete_icon_btc($idcom);
        echo '1';
    }

    function listeMesAnnonces($_iDCommercant = 0)

    {



        if (!$this->ion_auth->logged_in()) {

            $this->session->set_flashdata('domain_from', '1');

            redirect("connexion");

        } else if ($this->ion_auth->in_group(2)) {

            redirect();

        }



        $data["no_main_menu_home"] = "1";

        $data["no_left_menu_home"] = "1";



        if ($_iDCommercant != 0) {

            $toListeMesAnnonce = $this->mdlannonce->listeMesAnnonces($_iDCommercant);

            $objCommercant = $this->commercant->GetById($_iDCommercant);

            $data["objCommercant"] = $objCommercant;

            //verify 20 annonces limit

            //var_dump($objCommercant->limit_annonce);

            if (isset($objCommercant->limit_annonce) && count($toListeMesAnnonce) >= intval($objCommercant->limit_annonce) || $objCommercant->limit_annonce=="0") {

                $data['limit_annonce_add'] = 1;

            } else {

                $data['limit_annonce_add'] = 0;

            }

            //var_dump($data['limit_annonce_add']);

            $data['toListeMesAnnonce'] = $toListeMesAnnonce;

            $data["idCommercant"] = $_iDCommercant;

            $data['infocom']=$this->Mdlcommercant->infoCommercant($_iDCommercant);

            $data['get']=$this->mdlannonce->get_btc_icon($_iDCommercant);

            $data['pagecategory'] = "admin_commercant";

            // var_dump($data);die("fin");

            $this->load->view('privicarte/mes_annonces', $data);

        } else {

            //$this->load->view('front/vwMesAnnonces', $data) ;

        }

    }



    function supprimAnnonce($_iDAnnonce, $_iDCommercant)

    {



        $zReponse = $this->mdlannonce->supprimeAnnonces($_iDAnnonce);

        redirect("front/annonce/listeMesAnnonces/$_iDCommercant");

    }



    function annulationAjout()

    {

        //Supprimme tous les images deja uploader.

        $tNameFile = explode("-", $this->input->post("zConcateNameFile"));

        //print_r ($tNameFile); exit ();

        $path = "application/resources/front/images/";

        //echo $path . $tNameFile[0] ; exit ();

        if (count($tNameFile) > 0) {

            for ($i = 0; $i > count($tNameFile); $i++) {

                // echo "dfsdfs" ; exit ();

                if (file_exists($path . $tNameFile[$i])) {

                    unlink($path . $tNameFile[$i]);

                    // echo $path . $tNameFile[0] ; exit ();

                }

            }

        }



        //return false;

    }



    function personnalisation()

    {



        $data["current_page"] = "subscription_pro";//this is to differentiate js to use



        if (!$this->ion_auth->logged_in()) {

            redirect("auth/login");

        } else {





            $choix_dossier_perso_check_session = $this->session->userdata('choix_dossier_perso_check_session');

            if (isset($choix_dossier_perso_check_session)) {

                $data['choix_dossier_perso_check_session'] = $choix_dossier_perso_check_session;

                ////$this->firephp->log($choix_dossier_perso_check_session, 'choix_dossier_perso_check_session');

            }



            $agenda_perso = $this->input->post("agenda_perso");

            $choix_dossier_perso_value = $agenda_perso['dossperso'];

            ////$this->firephp->log($choix_dossier_perso_value, 'choix_dossier_perso_value');



            if (isset($choix_dossier_perso_value) && $choix_dossier_perso_value == "1") {

                $data['choix_dossier_perso_check_session'] = "1";

                $this->session->set_userdata('choix_dossier_perso_check_session', "1");



                $user_ion_auth = $this->ion_auth->user()->row();

                $iduser_verif = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

                if ($iduser_verif == null || $iduser_verif == 0 || $iduser_verif == "") {

                    $_iCommercantId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

                } else {

                    $_iCommercantId = "0";

                }

            } else {

                $_iCommercantId = "0";

                $data['choix_dossier_perso_check_session'] = "0";

                $this->session->set_userdata('choix_dossier_perso_check_session', "0");

            }



            if (isset($_iCommercantId) && $_iCommercantId != "0" && $_iCommercantId != null && $_iCommercantId != "") {

                $toVille = $this->mdlville->GetAnnonceVillesByIdCommercant($_iCommercantId);//get ville list of agenda

                //var_dump($toVille);die();

                $data['toVille'] = $toVille;

                $toCategorie_principale = $this->mdlannonce->GetAnnonceCategorie_ByIdCommercant($_iCommercantId);

                $data['toCategorie_principale'] = $toCategorie_principale;

                $toDeposant = $this->mdlannonce->getAlldeposantAnnonce($_iCommercantId);

                $data['toDeposant'] = $toDeposant;

            } else {

                $toVille = $this->mdlville->GetAnnonceVilles();//get ville list of agenda

                $data['toVille'] = $toVille;

                // var_dump($toVille);die();

                $toCategorie_principale = $this->mdlannonce->GetAnnonceCategorie();

                $data['toCategorie_principale'] = $toCategorie_principale;

                $toDeposant = $this->mdlannonce->getAlldeposantAnnonce();

                $data['toDeposant'] = $toDeposant;

            }



            $current_user_ion_auth = $this->ion_auth->user()->row();

            $current_iCommercantId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($current_user_ion_auth->id);

            $data['current_iCommercantId'] = $current_iCommercantId;

            $oAgenda_perso = $this->mdlannonce_perso->GetAnnonce_annonce_persoByUser($current_iCommercantId, $current_user_ion_auth->id);

            ////$this->firephp->log($current_iCommercantId, 'current_iCommercantId');

            ////$this->firephp->log($current_user_ion_auth->id, 'current_user_ion_auth');

            ////$this->firephp->log($oAgenda_perso, 'oAgenda_perso');

            //var_dump($oAgenda_perso);die();

            $data['oAgenda_perso'] = $oAgenda_perso;



            $group_user = $this->ion_auth->get_users_groups($current_user_ion_auth->id)->result();

            $data['group_user'] = $group_user[0]->id;



            $data['pagecategory'] = "admin_commercant";

            $data['currentpage'] = "export_annonce";

            $this->load->view('privicarte/annonce_personnalisation', $data);



        }

    }

public function get_count_annonce(){

    $TotalRows = count($this->mdlannonce->listeAnnonceRecherche(0, 0, "", "", 0, 10000, 0, 0, '', "0", "10", "0", "0"));

    echo '('.$TotalRows.')';

}

}

