<?php
class Webscrapping extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model("user");
        $this->load->model("mdlville");
        $this->load->Model("mdldepartement");

        $this->load->library('session');

        $this->load->library('ion_auth');
        $this->load->model("mdlcategorie");
        $this->load->model("ion_auth_model");
        $this->load->model("ion_auth_used_by_club");
        $this->load->model("mdlcommercant");
        $this->load->model("Mdlscrapping");
        $this->load->model("mdlarticle");
        $this->load->model("mdl_agenda");
        $this->load->helper('webscrape_date');
        $this->load->model("mdl_categories_agenda");
        $this->load->model("mdl_localisation");
    }

    function get_all_bycron($first)
    {
        $all_brute = $this->Mdlscrapping->get_all($first);
        $all = json_decode(json_encode($all_brute));
        foreach ($all as $api) {
            $this->make_run($api->id);
        }
    }

    public function slaunchsave_all($page)
    {
        $all = $this->Mdlscrapping->get_all($page);
        foreach ($all as $toget) {
            $this->save_all($toget->id, $page);
        }
    }

    public function index()
    {
        $data["no_main_menu_home"] = "1";
        $data["no_left_menu_home"] = "1";
        $data['allapi'] = $this->Mdlscrapping->get_all_api();
        $this->load->view('admin/IndexScrape', $data);
    }

    public function save_api_data()
    {
        $api = $this->input->post('api');
        $token = $this->input->post('token');
        $idcom = $this->input->post('idcom');
        $data = array(
            'api' => $api,
            'token' => $token,
            'idcom' => $idcom
        );
        $this->Mdlscrapping->save_api_data($data);
        redirect('admin/Webscrapping');
    }

    public function delete_api($id)
    {
        $this->Mdlscrapping->delete_api($id);
        redirect('admin/Webscrapping');
    }

    public function make_run($id)
    {
        $this->Mdlscrapping->reset_status();
        $data_to_parse = $this->Mdlscrapping->get_by_id($id);
        if (!empty($data_to_parse)) {
            $test = $this->get_api($data_to_parse->api, $data_to_parse->token);
            if (isset($test) and $test != null) {
                //            echo 'ok';
                return 'ok';
            } else {
                return 'ko';
            }
        } else {
            return 'ko';
        }
    }

    public function get_api($api_key, $token)
    {
        $this->Mdlscrapping->reset_status();
        $params = array(
            "api_key" => $api_key,
            "start_template" => "main_template",
            "send_email" => "1"
        );

        $options = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-Type: application/x-www-form-urlencoded; charset=utf-8',
                'content' => http_build_query($params)
            )
        );
        $context = stream_context_create($options);
        $resultat = file_get_contents('https://www.parsehub.com/api/v2/projects/' . $token . '/run', false, $context);
        return $resultat;
    }
    
    public function reportError($number,$IdVille,$name,$page){
        // Envoi email d'erreur
        $to = array();
        $to[] =  array(
                "Email" => "randev@randev.ovh",
                "Name" => "William"
        );
        $to1[] =  array(
                "Email" => "alphadev@randevteam.com",
                "Name" => "Alphadev"
        );

            @envoi_notification($to, "Erreur Webscrapping Agenda Sortez","Erreur lors du traitement du donnée N° ".$number." - ".$name." > page: ".$page);
            @envoi_notification($to1, "Erreur Webscrapping Agenda Sortez","Erreur lors du traitement du donnée N° ".$number." - ".$name." > page: ".$page);
       
        // Afficher erreur
        echo "<h5 style='color:red;'>Erreur pour ".$number."-".$name." IdVille: ".$IdVille."</h5><br>"; 
    }

    public function save_all($id, $page)
    {
        $this->Mdlscrapping->reset_status();
        $type = "agenda";
        $data_to_parse = $this->Mdlscrapping->get_by_id($id);
        if (!empty($data_to_parse)) {
            $infocom = $this->mdlcommercant->infoCommercant($data_to_parse->idcom);
            if (isset($infocom) and $infocom != null) {
                $json = $this->get_run($id);
                if (isset($json) and $json != null) {
                    $iduser = $data_to_parse->idcom;
                    $alldecode = json_decode($json);
                    // var_dump($alldecode); die();
                    if (isset($alldecode->url)) {
                        $count_all_ag = count($alldecode->url);
                        if (!isset($i)) {
                            $i = 0;
                        };
                        $compteur = 0;
                        while ($i < $count_all_ag) {
                            
                            // category
                            ////////////////////////////////////////////////////////////////////////////////////////////////////////
                                $categIdName = [];    
                                if($alldecode->url[$i]->categorie != NULL && $alldecode->url[$i]->categorie != ""){
                                    $exploded1 = explode(",",$alldecode->url[$i]->categorie);
                                
                                    for($k = 0; $k<count($exploded1) ; $k++){
                                        $exploded2 = explode(" ", $exploded1[$k]);
                                        if(count($exploded2)>1){
                                            for($j = 0; $j<count($exploded2); $j++){
                                                if(strlen($exploded2[$j])>2){
                                                    $similarCateg = $this->mdl_categories_agenda->searchBycategName($exploded2[$j]);
                                                    if($similarCateg != null){
                                                        $IdCateg = $similarCateg[0]->agenda_categid;
                                                        $categName = $similarCateg[0]->category;
                                                        $categTypeId = $similarCateg[0]->agenda_typeid;
                                                        array_push($categIdName, $IdCateg, $categName);
                                                    }
                                                }    
                                            }
                                        }else{
                                            $similarCateg = $this->mdl_categories_agenda->searchBycategName($exploded1[$k]);
                                            if($similarCateg != null){
                                                $IdCateg = $similarCateg[0]->agenda_categid;
                                                $categName = $similarCateg[0]->category;
                                                $categTypeId = $similarCateg[0]->agenda_typeid;
                                                array_push($categIdName, $IdCateg, $categName);
                                            }
                                        }
                                    }
                                    $categIdName = array_unique($categIdName);
                                    if(count($categIdName)>0){
                                        if(count($categIdName)>4){
                                            $idCategory = $categIdName[count($categIdName)-4];
                                            $nameCategory = $categIdName[count($categIdName)-3];
                                        } else {
                                            $idCategory = $categIdName[count($categIdName)-2];
                                            $nameCategory = end($categIdName);
                                        }
                                    }else{
                                        $categIdName["agenda_typeid"] = 7;
                                        if(explode(" ", $exploded1[0])){
                                            $exploded2 = explode(" ", $exploded1[0]);
                                            $categIdName["category"] = $exploded2[0];
                                            $NewCategId = $this->mdl_categories_agenda->Insert($categIdName);
                                            array_push($categIdName,$NewCategId,$exploded2[0]);
                                        }else{
                                            $categIdName["category"] = $exploded1[0];
                                            $NewCategId = $this->mdl_categories_agenda->Insert($categIdName);
                                            array_push($categIdName,$NewCategId,$exploded1[0]);
                                        }
                                        if(count($categIdName)>4){
                                            $idCategory = $categIdName[count($categIdName)-4];
                                            $nameCategory = $categIdName[count($categIdName)-3];
                                        } else {
                                            $idCategory = $categIdName[count($categIdName)-2];
                                            $nameCategory = end($categIdName);
                                        }
                                    }
                                }
                            ////////////////////////////////////////////////////////////////////////////////////////////////////////
                            //////////////////date/////////////////////////////
                            ////////////////////////////////////////////////////
                                    $date_complet = explode("\n", $alldecode->url[$i]->date);
                                    //echo $date_complet[0];
                                    //echo "<br>";
                            
                                    $date_debut = explode(' ', $date_complet[0]);
                            
                                    if ($date_debut[2] == 'janvier') $date_debut[2] = "01";
                                    if ($date_debut[2] == 'février' || $date_debut[2] == 'Feb') $date_debut[2] = "02";
                                    if ($date_debut[2] == 'mars') $date_debut[2] = "03";
                                    if ($date_debut[2] == 'avril') $date_debut[2] = "04";
                                    if ($date_debut[2] == 'mai') $date_debut[2] = "05";
                                    if ($date_debut[2] == 'juin') $date_debut[2] = "06";
                                    if ($date_debut[2] == 'juillet') $date_debutdate_debut[2] = "07";
                                    if ($date_debut[2] == 'Août' || $date_debut[2] == 'Aug') $date_debut[2] = "08";
                                    if ($date_debut[2] == 'septembre') $date_debut[2] = "09";
                                    if ($date_debut[2] == 'octobre') $date_debut[2] = "10";
                                    if ($date_debut[2] == 'novembre') $date_debut[2] = "11";
                                    if ($date_debut[2] == 'décembre' || $date_debut[2] == 'Dec') $date_debut[2] = "12";
                                    if(strlen($date_debut[1]) ==1){
                                        $date_debut[1] = "0".$date_debut[1];
                                    }
                                    
                                    //affichage date format yy-mm-dd	
                                    $dateDebutFinal =  $date_debut[3]."-". $date_debut[2] ."-". $date_debut[1]; 
                                    
                        
                                    if ($date_debut[0] == 'Le') {
                                        $heure_debut=$date_debut[5];
                                        $heure_fin=$date_debut[7];
                                    }
                        
                                    elseif (($date_fin = explode(' ', $date_complet[1])) and ($date_fin[0] == 'au')) {
                        
                                        if ($date_fin[2] == 'janvier') $date_fin[2] = "01";
                                        if ($date_fin[2] == 'février' || $date_fin[2] == 'Feb') $date_fin[2] = "02";
                                        if ($date_fin[2] == 'mars') $date_fin[2] = "03";
                                        if ($date_fin[2] == 'avril') $date_fin[2] = "04";
                                        if ($date_fin[2] == 'mai') $date_fin[2] = "05";
                                        if ($date_fin[2] == 'juin') $date_fin[2] = "06";
                                        if ($date_fin[2] == 'juillet') $date_fin[2] = "07";
                                        if ($date_fin[2] == 'Août' || $date_fin[2] == 'Aug') $date_fin[2] = "08";
                                        if ($date_fin[2] == 'septembre') $date_fin[2] = "09";
                                        if ($date_fin[2] == 'octobre') $date_fin[2] = "10";
                                        if ($date_fin[2] == 'novembre') $date_fin[2] = "11";
                                        if ($date_fin[2] == 'décembre' || $date_fin[2] == 'Dec') $date_fin[2] = "12";
                                        if(strlen($date_fin[1]) ==1){
                                            $date_fin[1] = "0".$date_fin[1];
                                        }
                                        $dateFinFinal = $date_fin[3]."-".$date_fin[2]."-".$date_fin[1];
                        
                                    }
                                    else {
                                        if ($date_debut[6] == 'janvier') $date_debut[6] = "01";
                                        if ($date_debut[6] == 'février' || $date_debut[6] == 'Feb') $date_debut[6] = "02";
                                        if ($date_debut[6] == 'mars') $date_debut[6] = "03";
                                        if ($date_debut[6] == 'avril') $date_debut[6] = "04";
                                        if ($date_debut[6] == 'mai') $date_debut[6] = "05";
                                        if ($date_debut[6] == 'juin') $date_debut[6] = "06";
                                        if ($date_debut[6] == 'juillet') $date_debut[6] = "07";
                                        if ($date_debut[6] == 'Août' || $date_debut[6] == 'Aug') $date_debut[6] = "08";
                                        if ($date_debut[6] == 'septembre') $date_debut[6] = "09";
                                        if ($date_debut[6] == 'octobre') $date_debut[6] = "10";
                                        if ($date_debut[6] == 'novembre') $date_debut[6] = "11";
                                        if ($date_debut[6] == 'décembre' || $date_debut[6] == 'Dec') $date_debut[6] = "12";               
                                        if(strlen($date_debut[5]) ==1){
                                            $date_debut[5] = "0".$date_debut[5];
                                        }
                                        $dateFinFinal = $date_debut[7]."-".$date_debut[6]."-".$date_debut[5];
                                    }   
                            ////////////////////////////////////////////////////

                            //////////////IMAGE/////////////////////////////////
                                    $final= $alldecode->url[$i]->img_url;

                                    if (isset($final) && $final != null) {
                                        if ($filesize = getimagesize($final)) {
                                            
                                            if (isset($filesize[0])) {

                                                $info = pathinfo($final);

                                                $contents = file_get_contents($final);

                                                $_zExtension = strrchr($info['basename'], '.');

                                                $_zFilename = random_string('unique', 10) . $_zExtension;

                                                $file = 'application/resources/front/photoCommercant/imagesbank/scrapped_image/' . $_zFilename;
                                                $dir = 'application/resources/front/photoCommercant/imagesbank/scrapped_image';

                                                if (is_dir($dir) == true) {
                                                    
                                                
                                                    @file_put_contents($file, $contents);
                                                    if (isset($_zFilename)) $photo = $_zFilename; // save image name on agenda table
                                                    //ar_dump($photo);
                                                    
                                                    if ($file != "") {
                                                        if (is_file($file)) {
                                                            $base_path_system = str_replace('system/', '', BASEPATH);

                                                            $image_path_resize_home = $base_path_system . "/" . $file;

                                                    

                                                            $image_path_resize_home_final = $base_path_system . "/" . $file;
                                                            $this_imgmoo = &get_instance();
                                                        
                                                            $this_imgmoo->load->library('image_moo');
                                                            $this_imgmoo->image_moo
                                                                ->load($image_path_resize_home)
                                                                ->resize_crop(640, 480, false)
                                                                ->save($image_path_resize_home_final, true);

                                                        }
                                                    } else {
                                                        $_zFilename = "";
                                                    }
                                                } else {
                                                
                                                    mkdir($dir, 0777);
                                                    @file_put_contents($file, $contents);
                                                    if (isset($_zFilename)) $photo = $_zFilename; // save image name on agenda table
                                                    if ($file != "") {
                                                        if (is_file($file)) {
                                                            $base_path_system = str_replace('system/', '', BASEPATH);
                                                            $image_path_resize_home = $base_path_system . "/" . $file;
                                                            $image_path_resize_home_final = $base_path_system . "/" . $file;
                                                            $this_imgmoo = &get_instance();
                                                            $this_imgmoo->load->library('image_moo');
                                                            $this_imgmoo->image_moo
                                                                ->load($image_path_resize_home)
                                                                ->resize_crop(640, 480, false)
                                                                ->save($image_path_resize_home_final, true);
                                                        } else {
                                                            $_zFilename = "";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        $_zFilename = "";
                                    }
                            ////////////////////////////////////////////////////

                            // /////////////// GET ID VILLE AND LOCATION ////////////////
                            if(isset($alldecode->url[$i]->code_postal) && isset($alldecode->url[$i]->ville)){
                                
                                $Villes =[];
                                $locationAdresse = explode(" ",$alldecode->url[$i]->adresse);
                                $CodepostalSearch = preg_replace('/[^0-9]/', '',$alldecode->url[$i]->adresse);
                                $CodepostalSearch = substr($CodepostalSearch,-5);
                                $locationId = [];

                                // changer 06300 to 06000
                                // if($alldecode->url[$i]->code_postal == "06300"){
                                //     $alldecode->url[$i]->code_postal = "06000";
                                // }
                                // Recuperation IdVille
                                if(strlen($alldecode->url[$i]->ville) < 30 && strlen($alldecode->url[$i]->code_postal) < 6){
                                    $IdVille = $this->mdlville->getIdVilleByNameByCodePostal($alldecode->url[$i]->ville,$alldecode->url[$i]->code_postal); 

                                    if($IdVille == NULL){
                                        $search = end($locationAdresse);
                                        // Recup Idville dans adresse si IdVille null
                                            if(strlen($search)>3){
                                                $IdVille = $this->mdlville->getIdVilleByNameByCodePostal($search,$alldecode->url[$i]->code_postal);
                                            }
                                        // Recup Code postal si invalid 
                                            if($IdVille == NULL){
                                                $IdVille = $this->mdlville->GetAutocompleteVille($alldecode->url[$i]->ville);
                                                // Recup Code Postal dans la base par Ville dans adresse si ville invalid 
                                                if($IdVille == NULL){
                                                    $this->mdlville->GetAutocompleteVille($search);
                                                }
            
                                                if($IdVille != NULL){
                                                    $alldecode->url[$i]->code_postal = $IdVille[0]->CodePostal;
                                                    $IdVille = $IdVille[0];
                                                } else {
                                                    // Envoie email d'erreur si IdVille null
                                                    $this->reportError($i,$alldecode->url[$i]->ville,$alldecode->url[$i]->name,$page);
                                                }
                                            }
                                    }
                                } else {
                                    if(strlen($alldecode->url[$i]->ville) < 30 && strlen($alldecode->url[$i]->code_postal) > 6){
                                        if(strlen($CodepostalSearch) == 5){
                                            $IdVille = $this->mdlville->getIdVilleByNameByCodePostal($alldecode->url[$i]->ville,$CodepostalSearch);
                                        } else {
                                            $IdVille = $this->mdlville->GetAutocompleteVille($alldecode->url[$i]->ville);

                                             // Recup Code Postal dans la base par Ville dans adresse si ville invalid 
                                             if($IdVille == NULL){
                                                $this->mdlville->GetAutocompleteVille($search);
                                            }
            
                                            if($IdVille != NULL){
                                                $alldecode->url[$i]->code_postal = $IdVille[0]->CodePostal;
                                                $IdVille = $IdVille[0];
                                            } else {
                                                $this->reportError($i,$alldecode->url[$i]->ville,$alldecode->url[$i]->name,$page);
                                            }
                                        }
                                    }
                                    
                                    if(strlen($alldecode->url[$i]->ville) > 30 && strlen($alldecode->url[$i]->code_postal) < 6){
                                            $search = end($locationAdresse);
                                            if(strlen($search)>3){
                                                $IdVille = $this->mdlville->getIdVilleByNameByCodePostal($search,$alldecode->url[$i]->code_postal);
                                            }
                                            if($IdVille == NULL){
                                                // $IdVille = $this->mdlville->GetAutocompleteVille($alldecode->url[$i]->ville);
                                                 // Recup Code Postal dans la base par Ville dans adresse si ville invalid 
                                                
                                                 $IdVille = $this->mdlville->GetAutocompleteVille($search);
                                                
            
                                                if($IdVille != NULL){
                                                    $alldecode->url[$i]->code_postal = $IdVille[0]->CodePostal;
                                                    $IdVille = $IdVille[0];
                                                } else {
                                                    $this->reportError($i,$alldecode->url[$i]->ville,$alldecode->url[$i]->name,$page);
                                                }
                                            }
                                    } elseif(strlen($alldecode->url[$i]->ville) > 30 && strlen($alldecode->url[$i]->code_postal) > 6) {
                                            $search = end($locationAdresse);
                                            $CodepostalSearch = preg_replace('/[^0-9]/', '',$alldecode->url[$i]->adresse);
                                            $CodepostalSearch = substr($CodepostalSearch,-5);
                                            if(strlen($search)>3){
                                                $IdVille = $this->mdlville->getIdVilleByNameByCodePostal($search,$CodepostalSearch);
                                            }
                                            if($IdVille == NULL){
                                                $IdVille = $this->mdlville->GetAutocompleteVille($search);
            
                                                if($IdVille != NULL){
                                                    $alldecode->url[$i]->code_postal = $IdVille[0]->CodePostal;
                                                    $IdVille = $IdVille[0];
                                                } else {
                                                    $this->reportError($i,$alldecode->url[$i]->ville,$alldecode->url[$i]->name,$page);
                                                }
                                            }
                                    // }else{
                                    //     $this->reportError($i,$alldecode->url[$i]->ville,$alldecode->url[$i]->name);
                                    }
                                }

                                // Get Location adresse 
                                for($h = 0; $h < count($locationAdresse); $h++){
                                    $search = preg_replace('/[^a-zA-Z]/', '',$locationAdresse[$h]);
                                    if(isset($IdVille->IdVille) && $IdVille->IdVille != NULL && strlen($alldecode->url[$i]->code_postal) < 6){
                                        $locations = $this->mdl_localisation->getLocationIdByAdresse($alldecode->url[$i]->code_postal,$IdVille->IdVille,$search, $alldecode->url[$i]->titre);
                                    } else {
                                        if(isset($IdVille->IdVille) && $IdVille->IdVille != NULL){
                                            $codepostalVille1 = $this->mdlville->getVilleById($IdVille->IdVille);
                                            // var_dump($codepostalVille1); die();
                                            $locations = $this->mdl_localisation->getLocationIdByAdresse($codepostalVille1->CodePostal,$IdVille->IdVille, $search,$alldecode->url[$i]->titre);
                                        }
                                    }
                                    
                                    if($locations != NULL){
                                        array_push($locationId, $locations->location_id);
                                    }
                                }
                                $locationId = array_unique($locationId);
                                // var_dump($locationId); die();
                                if(count($locationId)>0){
                                    $location_id = $locationId[0];
                                } else {
                                    $location_datas = array(
                                        "location" => $alldecode->url[$i]->adresse ?? $infocom->adresse_localisation  ?? '',
                                        "location_address" => $alldecode->url[$i]->adresse ?? $infocom->adresse_localisation  ?? '',
                                        "location_postcode" => $alldecode->url[$i]->code_postal ?? $infocom->CodePostal  ?? '',
                                        "location_villeid" => $IdVille->IdVille ?? $IdVille2 ?? $infocom->IdVille  ?? '',
                                        "location_departementid" => $IdVille->ville_departement ??$infocom->departement_id  ?? '',
                                        "IdCommercant" => $infocom->IdCommercant  ?? ''
                                    );
    
                                    $location_id = $this->mdlarticle->save_location($location_datas);
                                } 
                            }
                             $alldecode->url[$i]->description = str_replace("\n", "<br>", $alldecode->url[$i]->description);
                             $alldecode->url[$i]->description = str_replace("Afficher moins", "<br>", $alldecode->url[$i]->description);
                             $alldecode->url[$i]->description = str_replace("Lire la suite", "<br>", $alldecode->url[$i]->description);
                             
                            /////////////////////////////////////////////////
                            $description = $alldecode->url[$i]->description."<br>".$alldecode->url[$i]->categorie;
                            if(isset($alldecode->url[$i]->name)){
                                $titre = $alldecode->url[$i]->name;
                            } else {
                                $titre = $alldecode->url[$i]->titre;
                            }

                            $field_agenda = array(
                                'nom_manifestation' => $titre,
                                'date_debut' => $dateDebutFinal,
                                'Adresse1' => $alldecode->url[$i]->adresse,
                                'codepostal_localisation' => $alldecode->url[$i]->code_postal,
                                'adresse_localisation' => $alldecode->url[$i]->adresse,
                                'telephone' => $alldecode->url[$i]->contact ?? $alldecode->url[$i]->num_tel ?? $infocom->TelFixe ?? '',
                                'nom_societe' => $infocom->NomSociete ?? '',
                                "organiser_id" => $save_organiser ?? '',
                                "description_tarif" => $alldecode->url[$i]->tarifs ?? $infocom->Horaires ?? '',
                                "reservation_enligne" => $alldecode->url[$i]->reserver_url ?? '',
                                'date_fin' => $dateFinFinal ?? null,
                                'photo1' => $_zFilename,
                                'IdCommercant' => $iduser,
                                "IdUsers_ionauth" => $infocom->user_ionauth_id,
                                "IsActif" => "1",
                                "codepostal" => $alldecode->url[$i]->code_postal ?? $infocom->CodePostal,
                                "IdVille" => $IdVille->IdVille ?? $infocom->IdVille_localisation ?? $infocom->IdVille,
                                "agenda_categid" => $idCategory  ?? '',
                                "siteweb" => $infocom->SiteWeb ?? '',
                                "description" => $description,
                                "agenda_article_type_id" => 7,
                                "date_depot" => date("y-m-d"),
                                "IdVille_localisation" => $IdVille->IdVille ?? $IdVille2 ?? $infocom->IdVille ?? $infocom->IdVille_localisation,
                                "location_id" => $location_id ?? '0',
                                "facebook" => $alldecode->url[$i]->facebook_url ?? null,
                                "twitter" => $alldecode->url[$i]->twitter_url ?? null,
                                "url" => $alldecode->url[$i]->url ?? '',
                                "date_complet" => $alldecode->url[$i]->date,
                            );

                            if (isset($type) and $type == "agenda" or $type == 'Agenda') {

                                    if (isset($alldecode->url[$i])) {
                                        $objarticle = $this->mdl_agenda->save_revue_data($field_agenda);
                                        if($objarticle){
                                            echo "OK pour ".$i."-".$titre."<br>"; 
                                            $compteur++;
                                        } else {
                                            $this->reportError($i,$alldecode->url[$i]->ville,$alldecode->url[$i]->name,$page);
                                        }
                                    }

                                    if ($objarticle != false) {

                                        $field_datetime = array(
                                            "agenda_id" => $objarticle,
                                            "date_debut" => $dateDebutFinal,
                                            "date_fin" => $dateFinFinal,
                                            "heure_debut" => $heure_debut ?? null
                                        );
                                        $dateime = $this->mdl_agenda->save_datetime_revue($field_datetime);
                                    }
                            }
                            
                            $http_code = 200;
                            
                            $i++;
                            if (isset($alldecode->url[$i]->titre)) {
                                mail("alphadev@randevteam.com", "webscrapping" . $this->getNomcombyidcom($data_to_parse->idcom), "ok pour" . $alldecode->url[$i]->titre);
                            }
                            
                        }
                        echo $compteur." sur ".count($alldecode->url). " enregistré";
                    }
                }
            } else {
                mail("alphadev@randevteam.com", "webscrapping cron", "erreur pour" . $this->getNomcombyidcom($data_to_parse->idcom));
            }
        } else {
            mail("randev@randev.ovh", "webscrapping cron", "erreur pour" . $this->getNomcombyidcom($data_to_parse->idcom));
        }
    }

    public function get_run($id)
    {
        $data_to_parse = $this->Mdlscrapping->get_by_id($id);
        if (!empty($data_to_parse)) {
            $seconds = 10;
            sleep($seconds);
            $params = http_build_query(array(
                "api_key" => $data_to_parse->api,
                "format" => "json"
            ));
            $result = file_get_contents(
                'https://www.parsehub.com/api/v2/projects/' . $data_to_parse->token . '/last_ready_run/data?' . $params,
                false,
                stream_context_create(array(
                    'http' => array(
                        'method' => 'GET'
                    )
                ))
            );
            $is = gzdecode($result);
            return ($is);
        } else {
            echo 'ko';
        }
    }

    public function check_run()
    {
        $status =  $this->Mdlscrapping->check();
        if ($status == true) {
            return 'ok';
        } else {
            return 'ko';
        }
    }
    public function getNomcombyidcom($idcom)
    {
        $res = $this->Mdlscrapping->getNomcomByidCom($idcom);
        if (isset($res) and $res != null) {
            return $res->NomSociete;
        }
    }
    public function resetAgenda(){
        $empty = $this->mdl_agenda->emptyAgenda();
        $reset = $this->mdl_agenda->resetAgendaId();
        if($empty && $reset){
            echo "Table agenda vidé et id reinitialisé a 1";
        }
        if($empty && !$reset){
            echo "Table agenda vidé mais l'id n'est pas reinitialisé";
        }
        if(!$empty && $reset){
            echo "Erreur lors de la suppression contenu du table agenda mais l'id reinitialisé";
        }
        if(!$empty && !$reset){
            echo "Erreur lors de la suppression contenu du table agenda et reinitialisation Id";
        }
        
    }
}
