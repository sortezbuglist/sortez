<?php
class game extends CI_Controller {
    
    function __construct() {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('clubproximite');

        check_vivresaville_id_ville();
    }
    
    function index(){        
        $this->load->view('front/vwGame') ;
    }

    function gameannonces(){        
        $this->load->view('front/vwGame-annonces') ;
    }
    
    function gamebonplan(){        
        $this->load->view('front/vwGame-bon-plan') ;
    }
    
    function gameinfos(){        
        $this->load->view('front/vwGame-plus-infos') ;
    }
    
    function gamephotos(){        
        $this->load->view('front/vwGame-photos') ;
    }
    
    function gamenouscontacter(){        
        $this->load->view('front/vwGame-nous-contacter') ;
    }
    
    function gamenoussituer(){        
        $this->load->view('front/vwGame-nous-situer') ;
    }
    function stat(){
        statistiques();
        echo json_encode('ok');
    }
}