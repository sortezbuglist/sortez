<?php
class autocomplete extends CI_Controller {
	function __construct() {
        parent::__construct();
		$this->load->model("mdlcategorie") ;
		$this->load->model("mdlcommercant") ;
		$this->load->model("mdlville") ;

        $this->load->library('session');

		check_vivresaville_id_ville();

    }
	function indexListeCategorie($queryString){		
		$toCategorie= $this->mdlcategorie->GetAutocompleteCategorie($queryString);
		$dataCategorie = array();
		echo "<ul>" ;
		foreach ($toCategorie as $oCategorie){
		   
		   echo '<li  id="'.$oCategorie->IdSousRubrique.'" onClick="fill(\''.addslashes($oCategorie->Nom).'\' ,\' '  .  $oCategorie->IdSousRubrique .'\');">'.$oCategorie->Nom.'</li>';
		}
		echo "</ul>" ;
	}
	function indexListeCommercant($queryString){		
		$toCommercant= $this->mdlcommercant->GetAutocompleteCommercant($queryString);
		$dataCommercant = array();
		echo "<ul>" ;
		foreach ($toCommercant as $oCommercant){
		   
		   echo '<li  id="'.$oCommercant->IdCommercant.'" onClick="fillCommercant(\''.$oCommercant->NomSociete.'\' ,\' '  .  $oCommercant->IdCommercant .'\');">'.$oCommercant->NomSociete.'</li>';
		}
		echo "</ul>" ;
	} 	
	function indexListeVille($queryString){		
		$toVille= $this->mdlville->GetAutocompleteVille($queryString);
		$dataVille = array();
		echo "<ul>" ;
		foreach ($toVille as $oVille){
		   
		   echo '<li  id="'.$oVille->IdVille.'" onClick="fillVille(\''.$oVille->Nom.'\' ,\' '  .  $oVille->IdVille .'\');">'.$oVille->Nom.'</li>';
		}
		echo "</ul>" ;
	} 	
} 
/* End of file autocomplete.php */