<?php
class user_fidelity extends CI_Controller{
    
    function __construct() {
        parent::__construct();

        $this->load->model("mdl_card");
        $this->load->model("mdl_card_bonplan_used");
        $this->load->model("mdl_card_capital");
        $this->load->model("mdl_card_tampon");
        $this->load->model("mdl_card_user_link");
        $this->load->model("user");
        $this->load->model("Commercant");
        $this->load->model("mdlbonplan");
        $this->load->model('assoc_client_bonplan_model');
        $this->load->model('mdl_card_fiche_client_capital');
        $this->load->model('mdl_card_fiche_client_tampon');
        $this->load->model('mdl_card_fiche_client_remise');
        $this->load->model('assoc_client_plat_model');

        $this->load->library('session');

        $this->load->library("pagination");

        $this->load->library('ion_auth');
        //$this->load->library('DateUtils');
        $this->load->model("ion_auth_used_by_club");
        $this->load->helper('clubproximite');
        statistiques();

       if (!$this->ion_auth->logged_in()) {
            redirect("auth/login");
        }
    }
    
    function index(){
    	
        $this->liste();
    }
    
    function liste() {
        //$objfidelity = $this->mdlfidelity->GetAll();
        //$data["colfidelity"] = $objfidelity;

        if($this->agent->is_mobile()){
    		redirect('front/utilisateur/menuconsommateurs');
    	}
    	
        $user_ion_auth = $this->ion_auth->user()->row();
        $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

        $where_clause = " id_user='".$iduser."' AND id_ionauth_user='".$user_ion_auth->id."' ";
        $obj_user_card = $this->mdl_card->getWhereOne($where_clause);
        $data['obj_user_card'] = $obj_user_card;
        $data['iduser'] = $iduser;
        ////$this->firephp->log($obj_user_card, 'obj_user_card');

        $data['pagetitle'] = "Menu fidelity";
        
        
        $this->load->view("fidelityuser/vwListefidelity",$data);
    }


    function show_fidelity_card(){
    	if ($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
	        $user_ion_auth = $this->ion_auth->user()->row();
	        $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
	
	        $where_clause = " id_user='".$iduser."' AND id_ionauth_user='".$user_ion_auth->id."' ";
	        $obj_user_card = $this->mdl_card->getWhereOne($where_clause);
	
	        $base_path_system = str_replace('system/', '', BASEPATH);
	        $file_to_resize_zzz = $base_path_system."application/resources/front/images/cards/".$obj_user_card->virtual_card_img;
	
	        if (is_object($obj_user_card) && file_exists($file_to_resize_zzz)) {
	        	
	    	 $is_mobile = TRUE;
	        //test ipad user agent
	         $is_mobile_ipad = FALSE;
	         $is_robot = TRUE;
	            $data['obj_user_card'] = $obj_user_card;
	           // print_r($obj_user_card);
	         	if (($is_mobile == true) || $is_robot == true) {
	         		$data['titre'] = 'Ma carte';
	         		$this->load->view('mobile2014/ma-carte',$data);
	         	}else{
	            	$this->load->view("fidelityuser/vwShowCard",$data);
	         	}	
	        } else {
	           redirect("front/user_fidelity");
	        }
    	}else{
    		redirect('auth/login');
    	}
    }

    function order_fidelity_card(){

        $user_ion_auth = $this->ion_auth->user()->row();
        $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

        $where_clause = " id_user='".$iduser."' AND id_ionauth_user='".$user_ion_auth->id."' ";
        $obj_user_card = $this->mdl_card->getWhereOne($where_clause);
        ////$this->firephp->log(count($obj_user_card), 'obj_user_card');

        if (!is_object($obj_user_card)) {


            $client_card_number = generate_client_card($user_ion_auth->id); //generating image CARD and idCard now********************************************************************

            redirect("front/user_fidelity/show_fidelity_card");




        } else if (is_object($obj_user_card) && isset($obj_user_card->num_id_card_virtual)) {

        	
            $num_card_to_generate = $obj_user_card->num_id_card_virtual;

            $base_path_system = str_replace('system/', '', BASEPATH);

            //generating image QRCODE ---------------------------------
            $this->load->library('ciqrcode');
            $params_qrcode['data'] = $num_card_to_generate;
            $params_qrcode['level'] = 'H';
            $params_qrcode['size'] = 10;
            $params_qrcode['savename'] = $base_path_system."application/resources/front/images/cards/qrcode_".$num_card_to_generate.".png";
            $this->ciqrcode->generate($params_qrcode);

            //generating image CARD now********************************************************************
            $this->load->library('image_moo');
            $file_to_resize_xxx = $base_path_system."application/resources/front/images/cards/cartefidelite.png";
            $bandeau_color_image = $num_card_to_generate.'.png';
            $file_to_resize_yyy = $base_path_system."application/resources/front/images/cards/".$bandeau_color_image;
            if(file_exists($file_to_resize_xxx)) {
                $this->image_moo
                    ->load($file_to_resize_xxx)
                    ->load_watermark($base_path_system."application/resources/front/images/cards/qrcode_".$num_card_to_generate.".png")
                    ->resize(750,492,true)
                    ->watermark(9)
                    ->save($file_to_resize_yyy,true);
                $error_image_imoo_xxx = $this->image_moo->display_errors();
            }
            $file_to_resize_zzz = $base_path_system."application/resources/front/images/cards/".$bandeau_color_image;
            if(file_exists($file_to_resize_yyy)) {
                $this->image_moo
                    ->load($file_to_resize_yyy)
                    ->make_watermark_text("N° ".$num_card_to_generate,$base_path_system."system/fonts/ARIALN.TTF", 16, "#000000")
                    ->resize(750,492,true)
                    ->watermark(6)
                    ->save($file_to_resize_zzz,true);
                $error_image_imoo_xxx = $this->image_moo->display_errors();
            }
            if(file_exists($file_to_resize_zzz)) {
                $where_clause_data = " id_user='".$iduser."' AND id_ionauth_user='".$user_ion_auth->id."' ";
                $obj_user_card_data = $this->mdl_card->getWhereOne($where_clause_data);
                $obj_card_to_edit['id'] = $obj_user_card_data->id;
                $obj_card_to_edit['num_id_card_virtual'] = $obj_user_card_data->num_id_card_virtual;
                $obj_card_to_edit['num_id_card_physical'] = $obj_user_card_data->num_id_card_physical;
                $obj_card_to_edit['pin_code'] = $obj_user_card_data->pin_code;
                $obj_card_to_edit['qrcode'] = $obj_user_card_data->qrcode;
                $obj_card_to_edit['description'] = $obj_user_card_data->description;
                $obj_card_to_edit['is_activ'] = $obj_user_card_data->is_activ;
                $obj_card_to_edit['id_user'] = $obj_user_card_data->id_user;
                $obj_card_to_edit['id_ionauth_user'] = $obj_user_card_data->id_ionauth_user;
                $obj_card_to_edit['virtual_card_img'] = $bandeau_color_image;
                $this->mdl_card->update($obj_card_to_edit);
            }

            redirect("front/user_fidelity/show_fidelity_card");
            //generating image CARD now********************************************************************

        }

    }

    function save_card(){
        $data['pagetitle'] = "Enregistrement de carte";
        $this->load->view("fidelityuser/vwMyParameters",$data);
    }
    
 	function save_card_mobile(){
        $data['titre'] = "Demander une carte physique";
        $this->load->view("mobile2014/ma-carte-physique",$data);
    }


    function tampon() {
        $data['pagetitle'] = "tampon";
        $user_ion_auth_id = $this->ion_auth->user()->row();
        $data["oTampon"] = $this->mdl_card_tampon->getByIdIonauth($user_ion_auth_id->id);
        $this->load->view('fidelity/vwFichetampon', $data) ;
    }

    function capital() {
        $data['pagetitle'] = "capital";
        $user_ion_auth_id = $this->ion_auth->user()->row();
        $data["oCapital"] = $this->mdl_card_capital->getByIdIonauth($user_ion_auth_id->id);
        $this->load->view('fidelity/vwFichecapital', $data) ;
    }

    function edittampon(){
        $objTampon = $this->input->post("tampon");

        $objTampon["date_debut"] = convert_Frenchdate_to_Sqldate($objTampon["date_debut"]);
        $objTampon["date_fin"] = convert_Frenchdate_to_Sqldate($objTampon["date_fin"]);

        $user_ion_auth_id = $this->ion_auth->user()->row();
        $objTampon["id_ionauth"] = $user_ion_auth_id->id;
        $objTampon["id_commercant"] = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth_id->id);

        $objTampon["is_activ"] = "1";

        if ($objTampon["id"] == "" || $objTampon["id"] == null || $objTampon["id"] == "0") {
            $this->mdl_card_tampon->insert($objTampon);
        } else {
            $this->mdl_card_tampon->update($objTampon);
        }

        redirect("front/fidelity/tampon", 'refresh');
    }

    function editcapital(){
        $objCapital = $this->input->post("capital");

        $objCapital["date_debut"] = convert_Frenchdate_to_Sqldate($objCapital["date_debut"]);
        $objCapital["date_fin"] = convert_Frenchdate_to_Sqldate($objCapital["date_fin"]);

        $user_ion_auth_id = $this->ion_auth->user()->row();
        $objCapital["id_ionauth"] = $user_ion_auth_id->id;
        $objCapital["id_commercant"] = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth_id->id);

        $objCapital["is_activ"] = "1";

        if ($objCapital["id"] == "" || $objCapital["id"] == null || $objCapital["id"] == "0") {
            $this->mdl_card_capital->insert($objCapital);
        } else {
            $this->mdl_card_capital->update($objCapital);
        }

        redirect("front/fidelity/capital", 'refresh');
    }


    function enregistremnet(){
        $data['pagetitle'] = "Enregistrement fidelity";
        $this->load->view("fidelityuser/vwEnregistrement",$data);
    }

    function check_num_card(){
        $num_card_verification = $this->input->post('num_card_verification');
        $ocheck_num_card = $this->mdl_card->check_num_card($num_card_verification);

        if (count($ocheck_num_card)>0) {
            if (isset($ocheck_num_card) && $ocheck_num_card->is_activ=="1") {
                $ocheck_num_card_linked = $this->mdl_card->check_num_card_linked($ocheck_num_card->id);
                if (count($ocheck_num_card_linked)>0){
                    echo "2";
                } else {
                    $objCardLink['id_card'] = $ocheck_num_card->id;
                    $user_ion_auth_id = $this->ion_auth->user()->row();
                    $objCardLink['id_ionauth'] = $user_ion_auth_id->id;
                    $objCardLink['id_user'] = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth_id->id);
                    $objCardLink['date_link'] = date('Y-m-d');
                    $this->mdl_card_user_link->insert($objCardLink);
                    echo "1";
                }
            }
            else {
                echo "3";
            }
        } else {
            echo "error";
        }

    }

    function fidelity_event_list(){
        $data['pagetitle'] = "La liste de mes achats bon plans";

        $user_ion_auth_id = $this->ion_auth->user()->row();
        $id_user = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth_id->id);

        $PerPage = 10;
        $TotalRows = count($this->mdl_card_bonplan_used->getWhereLimit(" id_user='".$id_user."' ORDER BY date_use DESC",0,1000000));
        $config_pagination = array();
        $config_pagination["base_url"] = base_url()."front/user_fidelity/fidelity_event_list/";
        $config_pagination["total_rows"] = $TotalRows;
        $config_pagination["per_page"] = $PerPage;
        $config_pagination["uri_segment"] = 4;
        $config_pagination['first_link'] = '<<<';
        $config_pagination['last_link'] = '>>>';
        $this->pagination->initialize($config_pagination);
        $page_pagination = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $objcard_event_list = $this->mdl_card_bonplan_used->getWhereLimit(" id_user='".$id_user."' ORDER BY date_use DESC",$page_pagination,$PerPage);
        $data['oColCard_event_list'] = $objcard_event_list;
        $data['TotalRows'] = $this->mdl_card_bonplan_used->getWhereLimit(" id_user='".$id_user."' ORDER BY date_use DESC",0,1000000);
        $data["links_pagination"] = $this->pagination->create_links();
        $this->load->view("fidelityuser/vwListefidelityEvent",$data);
    }

 function fidelity_event_list_mobile(){
 	if ($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
 			$user_ion_auth_id = $this->ion_auth->user()->row();
 			$id_user = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth_id->id);
 		
 			$data['titre'] = "Ma consommation";
 			$data['fiche_capital'] = $this->mdl_card_fiche_client_capital->getByCritere(array('id_user'=>$id_user));
 			$data['fiche_tampon'] = $this->mdl_card_fiche_client_tampon->getByCritere(array('id_user'=>$id_user));
 			$data['fiche_remise'] = $this->mdl_card_fiche_client_remise->getByCritere(array('id_user'=>$id_user));
            $data['id_user'] = $id_user;
 			//var_dump($data['fiche_tampon']);die();
 			
 			//$this->load->view("mobile2014/ma-consommation",$data);
        $this->load->view("mobile2014/ma-consommation_menu",$data);
 		}
 }  
 
 
 function fidelity_event_list_mobile__old($filter=null){
 	
 	if ($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
	 	if(!empty($filter) && $filter == 'date_asc'){
	 		$order_by=array('champ'=>'card_bonplan_used.date_use','ordre'=> 'ASC');
	 	}else if(!empty($filter) && $filter == 'date_desc'){
	 		$order_by=array('champ'=>'card_bonplan_used.date_use','ordre'=> 'DESC');
	 	}else if(!empty($filter) && $filter == 'asc'){
	 		$order_by=array('champ'=>'card_bonplan_used.id','ordre'=> 'ASC');
	 	}else if(!empty($filter) && $filter == 'capital'){
	 		$critere['card_bonplan_used.capital_value_added !='] ='';
	 		$order_by=array('champ'=>'card_bonplan_used.id','ordre'=> 'ASC');
	 	}else if(!empty($filter) && $filter == 'tampon'){
	 		$critere['card_bonplan_used.tampon_value_added !='] ='';
	 		$order_by=array('champ'=>'card_bonplan_used.id','ordre'=> 'ASC');
	 	}
	 	else{
	 		$order_by=array('champ'=>'card_bonplan_used.id','ordre'=> 'DESC');
	 	}
 	
        $data['titre'] = "Ma consommation";

        $user_ion_auth_id = $this->ion_auth->user()->row();
        $id_user = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth_id->id);
        $critere['card_bonplan_used.id_user'] = $id_user;
        $group_by = array("card_bonplan_used.description","card_bonplan_used.id_commercant","card_bonplan_used.id_bonplan");
        $objcard_event_list = $this->mdl_card_bonplan_used->get_by_critere($critere,$order_by,$group_by,1);
        $data['oColCard_event_list2'] = !empty($objcard_event_list) ? $objcard_event_list : array();
        $this->load->view("mobile2014/ma-consommation",$data);
 	}
 	else{
 		redirect('auth/login');
 	}
 }
    
 	function detail_offre($id_commercant){
 		if ($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
 			$this->load->model('commercant');
 			$user_ion_auth_id = $this->ion_auth->user()->row();
 			$id_user = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth_id->id);
 			
 			$critere=array('card_bonplan_used.id_commercant'=>$id_commercant,'id_user'=>$id_user);
 			$offres = get_fidelity($id_commercant);
 			$list_activity = $this->mdl_card_bonplan_used->get_by_critere($critere);
 			
 			$fiche_capitals = $this->mdl_card_fiche_client_capital->getByCritere(array('id_user'=>$id_user,'card_fiche_client_capital.id_commercant'=>$id_commercant));
 			$fiche_tampons = $this->mdl_card_fiche_client_tampon->getByCritere(array('id_user'=>$id_user,'card_fiche_client_tampon.id_commercant'=>$id_commercant));
 			$fiche_remises = $this->mdl_card_fiche_client_remise->getByCritere(array('id_user'=>$id_user,'card_fiche_client_remise.id_commercant'=>$id_commercant));
 			
 			$commercant = $this->commercant->getByCritere(array('IdCommercant'=>$id_commercant));
 			
 			$data['titre'] = "Ma consommation";
 			$data['commercant']  = $commercant;
 			$data['offre'] = $offres;
 			$data['fiche_tampon'] = (!empty($fiche_tampons)) ? current($fiche_tampons): false;
 			$data['fiche_capital'] = (!empty($fiche_capitals)) ? current($fiche_capitals): false;
 			$data['fiche_remise'] = (!empty($fiche_remises)) ? current($fiche_remises): false; 
 			$data['list_activity'] = $list_activity;
 			$data['user_bonplan'] = get_user_bonplan($id_user, $id_commercant);
 			
 			
 			$this->load->view('mobile2014/detail-offre',$data);
 		}
 	}
 	
 	
    function fidelity_event_details($prmId = 0){
        $data['oFidelityDetails'] = $this->mdl_card_bonplan_used->getById($prmId);
        $this->load->view("fidelityuser/vwDetailsfidelityEvent",$data);
    }
    
	function fidelity_event_details_mobile($prmId = 0){
		$data['titre'] = "Ma consommation";
        $bon_plan_used = $this->mdl_card_bonplan_used->get_with_commercant_by_id($prmId);
		if(!empty($bon_plan_used)){
	        $data['oFidelityDetails']=$bon_plan_used;
	        $critere = array('card_bonplan_used.id_commercant'=>$bon_plan_used->id_commercant,'card_bonplan_used.id_user'=>$bon_plan_used->id_user);
	        //$critere['card_bonplan_used.id <=']  = $bon_plan_used->id;
	        $data['list_activity'] = $this->mdl_card_bonplan_used->get_by_critere($critere);
	        $this->load->view("mobile2014/ma-consommation-tampon-detail",$data);
		}
    }
    
    function fidelity_event_details_mobile2($id_user,$id_commercant){
    	$this->load->model('commercant');
    	$data['titre'] = "Ma consommation";
    	$commercant = $this->commercant->getByCritere(array('IdCommercant'=>$id_commercant));
    	if(!empty($commercant)){
    		$data['commercant']=$commercant;
    		$data['list_activity'] = $this->mdl_card_bonplan_used->get_by_critere(array('id_user'=>$id_user, 'id_commercant'=>$id_commercant));
    		$this->load->view("mobile2014/ma-consommation-tampon-detail",$data);
    	}
    }

    function fidelity_tampon_list(){
        $data['pagetitle'] = "La Liste de mes Tampons";

        $user_ion_auth_id = $this->ion_auth->user()->row();
        $id_user = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth_id->id);

        $PerPage = 10;
        $TotalRows = count($this->mdl_card_bonplan_used->getAllTamponByIdUser($id_user,0,100000));
        $config_pagination = array();
        $config_pagination["base_url"] = base_url()."front/user_fidelity/fidelity_tampon_list/";
        $config_pagination["total_rows"] = $TotalRows;
        $config_pagination["per_page"] = $PerPage;
        $config_pagination["uri_segment"] = 4;
        $config_pagination['first_link'] = '<<<';
        $config_pagination['last_link'] = '>>>';
        $this->pagination->initialize($config_pagination);
        $page_pagination = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $objcard_event_list = $this->mdl_card_bonplan_used->getAllTamponByIdUser($id_user,$page_pagination,$PerPage);
        $data['oColCard_event_list'] = $objcard_event_list;
        $data["links_pagination"] = $this->pagination->create_links();
        $this->load->view("fidelityuser/vwListefidelityTampon",$data);
    }

    function fidelity_capital_list(){
        $data['pagetitle'] = "La liste de mes achats cumulés";

        $user_ion_auth_id = $this->ion_auth->user()->row();
        $id_user = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth_id->id);

        $PerPage = 10;
        $TotalRows = count($this->mdl_card_bonplan_used->getAllCapitalByIdUser($id_user,0,100000));
        $config_pagination = array();
        $config_pagination["base_url"] = base_url()."front/user_fidelity/fidelity_capital_list/";
        $config_pagination["total_rows"] = $TotalRows;
        $config_pagination["per_page"] = $PerPage;
        $config_pagination["uri_segment"] = 4;
        $config_pagination['first_link'] = '<<<';
        $config_pagination['last_link'] = '>>>';
        $this->pagination->initialize($config_pagination);
        $page_pagination = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $objcard_event_list = $this->mdl_card_bonplan_used->getAllCapitalByIdUser($id_user,$page_pagination,$PerPage);
        $data["links_pagination"] = $this->pagination->create_links();
        $data['oColCard_event_list'] = $objcard_event_list;
        $this->load->view("fidelityuser/vwListefidelityCapital",$data);
    }


    function my_data($prmId = 0) {
        $group_proo_club = array(3,4,5);
        if ($prmId != 0 && ($this->ion_auth->in_group($group_proo_club))) {
            redirect("front/professionnels/fiche/".$prmId);
        } else if ($prmId != 0 && ($this->ion_auth->is_admin())) {
            redirect('admin/home', 'refresh');
        }

        if (!$this->ion_auth->logged_in() && $prmId != 0) {
            $this->session->set_userdata("cur_uri", $this->uri->uri_string());
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
        if ($this->ion_auth->logged_in() && ($prmId == 0 || $prmId == "") && (!$this->ion_auth->in_group($group_proo_club) && !$this->ion_auth->is_admin())) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $prmId = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
        }
        $data = null;
        $this->load->Model("Ville");
        $this->load->Model("user");
        $this->load->Model("mdlville");

        $data["colVilles"] = $this->Ville->GetAll();
        ////$this->firephp->log($data["colVilles"], 'villes');
        $oParticulier = $this->user->GetById($prmId);
        ////$this->firephp->log($oParticulier, 'oParticulier');
        ////$this->firephp->log($prmId, 'prmId');
        $data["oParticulier"] = $oParticulier ;
        $data["prmId"] = $prmId ;

        $is_mobile = TRUE;
        //test ipad user agent
        $is_mobile_ipad = FALSE;
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = TRUE;
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;


        //$data = null;
        //$this->load->view("front/vwInscriptionParticulier",$data);
        $this->load->view("fidelityuser/inscriptionparticulier_main",$data);

    }

    function filter(){
    	$data['titre'] = 'Filtrez les résultats';
    	$this->load->view("mobile2014/filtre-consommation",$data);
    }

	function mes_reservations_mobie(){
		$user_ion_auth_id = $this->ion_auth->user()->row();
        $id_user = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth_id->id);
        
        $criteres['valide'] = 0;
        $criteres['id_user'] = $id_user;
        
        $resa = $this->assoc_client_bonplan_model->get_where(array('assoc_client_bonplan.id_client'=>$id_user,'assoc_client_bonplan.valide'=>0));
       // echo $this->db->last_query();
        //print_r($resa);
       // die();
       // $data['reservations_attente_validation'] = $this->assoc_client_bonplan_model->get_list_by_criteria($criteres);
       // $data['reservations_attente_validation'] = array();
		//$data['reservations_attente_validation'] = $this->mdl_card->get_reservation_by_user_id($id_user,1);
		//echo $this->db->last_query();
        $list_res_plat=$this->assoc_client_plat_model->get_res_plat_user_by_iduser($id_user);
        $data['list_plat']=$list_res_plat;
		$data['titre'] = 'Mes reservations en cours';
		$data['reservations'] = $resa;
        $this->load->view('mobile2014/mes-reservations',$data);
	}
	function print_card(){
		$data['titre'] = 'Bon de retrait';
		$data['user'] =  $this->ion_auth->user()->row();
    	$this->load->view("mobile2014/impression-bon-retrait",$data);
	}
public function details_fidelity(){
    if ($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
        $user_ion_auth_id = $this->ion_auth->user()->row();
        $id_user = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth_id->id);
        // $id_com = $this->Commercant->GetById($prmId)
        $data['titre'] = "Ma consommation fidelité";
        $data['fiche_capital'] = $this->mdl_card_fiche_client_capital->getByCritere(array('id_user'=>$id_user));
        $data['fiche_tampon'] = $this->mdl_card_fiche_client_tampon->getByCritere(array('id_user'=>$id_user));
        $data['fiche_remise'] = $this->mdl_card_fiche_client_remise->getByCritere(array('id_user'=>$id_user));
        $data['id_user'] = $id_user;
        // var_dump($data['fiche_tampon']);
        // var_dump($data['fiche_tampon']);die( "ko");
        

        $this->load->view("mobile2014/ma-consommation",$data);
        //$this->load->view("mobile2014/ma-consommation_menu",$data);
    }
}
function user($userId) {        
            $oCommercant = $this->Commercant->GetById ( $userId );
            if (! empty ( $oCommercant )) {
                $data ['titre'] = "Détail Commercant";
                $data ['Commercant'] = $oCommercant;
                $this->load->view ( 'mobile2014/pro/commercant-detail', $data );
            } else{
                redirect ( $this->agent->referrer () );
            }
}
public function details_bonplan(){

    if ($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
        $user_ion_auth_id = $this->ion_auth->user()->row();
        $id_user = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth_id->id);
        $data['titre'] = "Ma consommation bonplan";
        $data['fiche_bonplan'] = $this->assoc_client_bonplan_model->get_list_by_criteria(array('id_user'=>$id_user,'valide'=>'1'));
        $data['id_user'] = $id_user;
        $this->load->view("mobile2014/ma-consommation_bonplan",$data);
    }
}
public function delete_historical_bonplan($id){
        $criteres['id']=$id;
        $tosave_to_used=$this->assoc_client_bonplan_model->get_list_by_criteria($criteres,$order_by=array());
        //var_dump($tosave_to_used[0]);die();
        $card_bonplan_used['id_commercant'] = $tosave_to_used[0]->IdCommercant;
        $card_bonplan_used['id_user'] = $tosave_to_used[0]->id_client;
        $card_bonplan_used['id_bonplan'] = $tosave_to_used[0]->id_bonplan;
        $card_bonplan_used['scan_status'] = "OK";
        $card_bonplan_used['date_use'] = date ( "Y-m-d h:m:s" );
        $this->mdl_card_bonplan_used->insert ( $card_bonplan_used );
        $assoc_client_bonplan_id = $tosave_to_used[0]->id;
        $this->assoc_client_bonplan_model->deleteById ( $assoc_client_bonplan_id );
        redirect('front/user_fidelity/details_bonplan');
}
public function details_plat(){
    if ($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
        $user_ion_auth_id = $this->ion_auth->user()->row();
        $id_user = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth_id->id);
        $data['titre'] = "Ma consommation Plat";
        $list_res_plat=$this->assoc_client_plat_model->get_res_plat_user_by_iduser_valided($id_user);
        $data['list_plat']=$list_res_plat;
        $data['id_user'] = $id_user;
        $this->load->view("mobile2014/ma-consommation_plat",$data);
    }
}
public function delete_historical_plat($id){
    $this->assoc_client_plat_model->delete_res_plat($id);
    redirect('front/user_fidelity/details_plat/');
}
}