<?php

class professionnels extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("SousRubrique");
        $this->load->model("mdlcadeau");
        $this->load->model("mdlbonplan");
        $this->load->model("mdlannonce");
        $this->load->model("mdlcommercant");
        $this->load->model("user");
        $this->load->Model("Commercant");
        $this->load->Model("mdl_mail_activation_bon_plan");
        $this->load->model("mdlville");
        $this->load->model("mdlglissiere");
        $this->load->model("AssCommercantAbonnement");
        $this->load->Model("Rubrique");
        $this->load->Model("SousRubrique");
        $this->load->Model("Ville");
        $this->load->Model("Abonnement");
        $this->load->Model("mdlstatut");
        $this->load->helper('captcha'); //
        $this->load->library('image_moo');
        $this->load->Model("mdldepartement");
        $this->load->Model("mdlcommune");
        $this->load->Model("mdldemande_abonnement");
        $this->load->model("parametre");
        $this->load->model('pack_test_validation');
        $this->load->model('Mdlarticle');
        $this->load->model('Mdl_agenda');
        $this->load->model('mdldemande');
        $this->load->model('Mdl_abonnementInscription');

        $this->load->library('session'); //

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");
        $this->load->helper('clubproximite'); //
        statistiques();

        check_vivresaville_id_ville();

    }

    function index()
    {
        $group_proo_club = array(3, 4, 5);
        if (!$this->ion_auth->in_group($group_proo_club)) {
            $this->session->set_userdata("cur_uri", $this->uri->uri_string());
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }
    public function save_vivresaville_account(){
        $data = $this->input->post("Societe");

            $data['mdp']= $this->input->post('mdp');
            $data['confirmmdp']= $this->input->post('confirmmdp');
           
    $save = $this->Mdl_abonnementInscription->create($data);

// var_dump($save);

    if (!empty($save)) {
        // code...
    

                $colDestAdmin = array();
                $colDestAdmin[] = array("Email" => $this->config->item("mail_sender_adress"), "Name" => $this->config->item("mail_sender_name"));
                // $colDestAdmin[] = array("Email" => "alphadev@randevteam.com", "Name" => $this->config->item("mail_sender_name"));
                $txtSujetAdmin = "Notification inscription nouveau Compte vivresaville";
                 $message_html_admin = '
            <p>Bonjour ,</p>
                    <p>Quelqu\'un vient de cr&eacute;er un compte Vivresaville gratuit chez Sortez.org</p>
                    <p>ci-dessous les details : <br/>
        ';
           $message_html_admin .= "Nom : " . $data['NomSociete'] . "<br/>";
           $message_html_admin .= "Login : " . $data['Login'] . "<br/>";
           $message_html_admin .= "Mot de passe : " . $data['mdp'] . "<br/></p>";
           


        
        $send_mail_admin = @envoi_notification($colDestAdmin,$txtSujetAdmin,$message_html_admin);

        $txtSujet = "Notification inscription à un compte Vivresaville";

        $colDest[] = array("Email" => $data['Email'], "Name" => $data['NomSociete']);
        // $colDest[] = array("Email" => "kappadev@randevteam.com", "Name" => $this->config->item("mail_sender_name"));
                $txtSujet = "Notification inscription nouveau Compte vivresaville";
                 $message = '
            <p>Bonjour ,</p>
                    <p>Vous venez de cr&eacute;er un compte Vivresaville gratuit chez Sortez.org</p>
                    <p>ci-dessous les accès : 
        ';
        $message .= "Nom : " . $data['NomSociete'] . "<br/>";
        $message .= "Votre Login : " . $data['Login'] . "<br/>";
        $message .= "Mot de passe : " . $data['mdp'] . "<br/></p>";
         
        $send_mail = @envoi_notification($colDest,$txtSujet,$message);
        // var_dump($message_html_admin);



        }
        $this->load->view('privicarte/remerciment', $data);

    }  

    function createAbonnementInscription(){

        $data["colStatut"] = $this->mdlstatut->GetAll();
        $data["colVilles"] = $this->mdlville->GetAll();
        $data["colDepartement"] = $this->mdldepartement->GetAll();
    
       

        $this->load->view('privicarte/vwFormulaireinscription', $data);
        
            
             

    }

    function send_confirmation(){
        // $contact_nom = $this->input->post("Name");
        // $contact_tel = $this->input->post("mobile");
        // $contact_mail = $this->input->post("email");
        
        // $message_html = '
        //     <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
        //         <p>Bonjour et bienvenu sur Sortez.org</p>
        //     </div>
        // ';
        
        // $base_path_system = str_replace('system/', '', BASEPATH);
        // require_once($base_path_system . 'application/resources/phpmailer/PHPMailerAutoload.php');

        // $mail = new PHPMailer();

        // $mail->IsHTML(true);
        // $mail->Charset = "utf-8";
        // $mail->IsSMTP();
        // $mail->Host = "mail.sortez.org";
        // $mail->SMTPDebug = 0;

        // $mail->SMTPAuth = true;                  // enable SMTP authentication
        // $mail->Host = "ssl0.ovh.net"; // sets the SMTP server
        // $mail->SMTPSecure = "ssl";
        // $mail->Port = 465;                    // set the SMTP port for the GMAIL server
        // $mail->Username = "mail@sortez.org"; // SMTP account username
        // $mail->Password = 'J^ou~#|~[{`\'5((*_~#{[]@_(-)';        // SMTP account password
        // $mail->SetFrom('rocksonmichael8@gmail.com');
        // $mail->Subject = 'Validation de votre compte.';
        // $mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

        // //$mail->MsgHTML($body);
        // $mail->MsgHTML($message_html);

        // $address = $contact_mail;
        // $mail->AddAddress($contact_mail, $contact_nom);

        // if (!$mail->Send()) {
        //     $zContactEmailCom_copy = array();
        //     $zContactEmailCom_copy[] = array("Email" => $contact_privicarte_mail, "Name" => $contact_privicarte_nom);
        //     $message_html_copy = '
        //         <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
        //         <p>Bonjour,<br/>
        //         Ceci est une copie de votre contact Sortez.org<br/>
        //         Ci-dessous le contenu,</p>
        //         <p>Nom : ' . $contact_privicarte_nom . '<br/>
        //         T&eacute;l&eacute;phone : ' . $contact_privicarte_tel . '<br/>
        //         Email : ' . $contact_privicarte_mail . '<br/>
        //         Message : <br/><br/>' . $contact_privicarte_msg . '</p>
        //         <p><hr><a href="http://Sortez.org/"><img alt="Sortez" src="http://privicarte.fr/portail/application/resources/privicarte/images/logo.png"></a></p>
        //         </div>
        //         ';
        //     $message_html_copy = html_entity_decode(htmlentities($message_html_copy));
        //     $sending_mail_privicarte_copy = @envoi_notification($zContactEmailCom_copy, $contact_privicarte_mailSubject, $message_html_copy);


        //     echo $mail->ErrorInfo;
        // } else {
        //     echo '<span style="color:#009900">Veuillez consulter votre mail pour procèder à votre confirmation.</span>';
        // }

    }

    function allcommercant()
    {
        $allcommercant = $this->Commercant->GetAll();
        if (isset($allcommercant)) echo count($allcommercant); else "0";
    }

    function villecp()
    {
        $CodePostal = $this->input->post("CodePostalSociete");
        $departement_id = $this->input->post("departement_id");
        if (isset($departement_id) && $departement_id!="" && $departement_id!="0") $departement_id = $this->mdldepartement->getById($departement_id)->departement_code;
        $departements = $this->mdlville->GetVilleByCodePostal($CodePostal, $departement_id);
        //echo $CodePostal;
        $result_to_show = '<select name="Societe[IdVille]" id="VilleSociete" class="stl_long_input_platinum form-control"  onchange="getLatitudeLongitudeAdresse();" style="height: 35px;">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .= '<option value="' . $item->IdVille . '">' . $item->Nom . '</option>';
            }
        }
        $result_to_show .= '</select>';
        echo $result_to_show;

    }

    function villecp_agenda()
    {
        $CodePostal = $this->input->post("CodePostalSociete");
        $departement_id = $this->input->post("departement_id");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdlville->GetVilleByCodePostal($CodePostal, $departement_id);
        //echo $CodePostal;
        $result_to_show = '<select name="Agenda[IdVille]" id="IdVille" class="form-control stl_long_input_platinum">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .= '<option value="' . $item->IdVille . '">' . $item->Nom . '</option>';
            }
        }
        $result_to_show .= '</select>';
        echo $result_to_show;

    }

    function villecp_agenda_localisation()
    {
        $CodePostal = $this->input->post("CodePostalSociete");
        $departement_id = $this->input->post("departement_id");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdlville->GetVilleByCodePostal($CodePostal, $departement_id);
        //echo $CodePostal;
        $result_to_show = '<select name="Agenda[IdVille_localisation]" id="IdVille_localisation" class="form-control stl_long_input_platinum">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .= '<option value="' . $item->IdVille . '">' . $item->Nom . '</option>';
            }
        }
        $result_to_show .= '</select>';
        echo $result_to_show;

    }


    function villecp_particulier()
    {
        $CodePostal = $this->input->post("CodePostalSociete");
        $departement_id = $this->input->post("departement_id");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdlville->GetVilleByCodePostal($CodePostal, $departement_id);
        //echo $CodePostal;
        $result_to_show = '<select name="Particulier[IdVille]" id="txtVille" class="stl_long_input_platinum form-control" style="width:273px;">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .= '<option value="' . $item->IdVille . '">' . $item->Nom . '</option>';
            }
        }
        $result_to_show .= '</select>';
        echo $result_to_show;

    }

    function villecp_localisation()
    {
        $CodePostal = $this->input->post("CodePostalSociete");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdlville->GetVilleByCodePostal_localisation($CodePostal);
        //echo $CodePostal;
        $result_to_show = '<select name="Societe[IdVille_localisation]" id="IdVille_localisationSociete" onchange="getLatitudeLongitudeLocalisation();" style="width:413px;">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .= '<option value="' . $item->IdVille . '">' . $item->Nom . '</option>';
            }
        }
        $result_to_show .= '</select>';
        echo $result_to_show;

    }

    function departementcp()
    {
        $CodePostal = $this->input->post("CodePostalSociete");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdldepartement->GetDepByCodePostal($CodePostal);
        //echo $CodePostal;
        $result_to_show = '<select name="Societe[departement_id]" id="departement_id" class="form-control stl_long_input_platinum" onchange="javascript:CP_getVille_D_CP();" style="height: 35px;">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .= '<option value="' . $item->departement_id . '">' . $item->departement_nom . '</option>';
            }
        }
        $result_to_show .= '</select>';
        echo $result_to_show;
    }

    function departementcp_agenda()
    {
        $CodePostal = $this->input->post("CodePostalSociete");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdldepartement->GetDepByCodePostal($CodePostal);
        //echo $CodePostal;
        $result_to_show = '<select name="Agenda[departement_id]" id="DepartementAgenda" class="form-control stl_long_input_platinum" onchange="javascript:CP_getVille_D_CP();">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .= '<option value="' . $item->departement_id . '">' . $item->departement_nom . '</option>';
            }
        }
        $result_to_show .= '</select>';
        echo $result_to_show;
    }


    function departementcp_agenda_localisation()
    {
        $CodePostal = $this->input->post("CodePostalSociete");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdldepartement->GetDepByCodePostal($CodePostal);
        //echo $CodePostal;
        $result_to_show = '<select name="Agenda[departement_id_localisation]" id="DepartementAgendaLocalisation" class="form-control stl_long_input_platinum" onchange="javascript:CP_getVille_D_CP_localisation();">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .= '<option value="' . $item->departement_id . '">' . $item->departement_nom . '</option>';
            }
        }
        $result_to_show .= '</select>';
        echo $result_to_show;
    }


    function departementcp_particulier()
    {
        $CodePostal = $this->input->post("CodePostalSociete");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdldepartement->GetDepByCodePostal($CodePostal);
        //echo $CodePostal;
        $result_to_show = '<select name="Societe[departement_id]" id="departement_id" class="stl_long_input_platinum form-control" onchange="javascript:CP_getVille_D_CP();"  style="width:273px;">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .= '<option value="' . $item->departement_id . '">' . $item->departement_nom . '</option>';
            }
        }
        $result_to_show .= '</select>';
        echo $result_to_show;
    }

    function ajaxRubrique()
    {
        // GetByRubrique
    }

    function testAjax($id = "")
    {
        if ($id != ""){
            $data["colSousRubriques"] = $this->SousRubrique->GetByRubrique($id);
            $data['id'] = $id;
            $this->load->view("front/vwReponseRub", $data);
        }
    }

    function TwitterPrivicarteForm()
    {
        $data['data'] = "";
        $this->load->view("privicarte/TwitterPrivicarteForm", $data);
    }

    function TwitterProForm($IdCommercant)
    {
        $data['IdCommercant'] = $IdCommercant;
        $data['oInfoCommercant'] = $this->Commercant->getById($IdCommercant);
        $this->load->view("privicarte/TwitterProForm", $data);
    }

    function FacebookPrivicarteForm()
    {
        $data['data'] = "";
        $this->load->view("privicarte/FacebookPrivicarteForm", $data);
    }

    function FacebookProForm($IdCommercant)
    {
        $data['IdCommercant'] = $IdCommercant;
        $data['oInfoCommercant'] = $this->Commercant->getById($IdCommercant);
        $this->load->view("privicarte/FacebookProForm", $data);
    }

    function GoogleplusPrivicarteForm()
    {
        $data['data'] = "";
        $this->load->view("privicarte/GoogleplusPrivicarteForm", $data);
    }

    function GoogleplusProForm($IdCommercant)
    {
        $data['IdCommercant'] = $IdCommercant;
        $data['oInfoCommercant'] = $this->Commercant->getById($IdCommercant);
        $this->load->view("privicarte/GoogleplusProForm", $data);
    }

    function FacebookPrivicarteForm_share()
    {
        $data['data'] = "";
        $this->load->view("privicarte/FacebookPrivicarteForm_share", $data);
    }

    function TwitterPrivicarteForm_share()
    {
        $data['data'] = "";
        $this->load->view("privicarte/TwitterPrivicarteForm_share", $data);
    }

    function GoogleplusPrivicarteForm_share()
    {
        $data['data'] = "";
        $this->load->view("privicarte/GoogleplusPrivicarteForm_share", $data);
    }


    function contact_partner_sendmail()
    {

        $contact_partner_nom = $this->input->post("contact_partner_nom");
        $contact_partner_tel = $this->input->post("contact_partner_tel");
        $contact_partner_mail = $this->input->post("contact_partner_mail");
        $contact_partner_msg = $this->input->post("contact_partner_msg");
        $contact_partner_mailto = $this->input->post("contact_partner_mailto");
        //$contact_partner_mailto = "manager@randevteam.com";

        $contact_partner_mailSubject = 'Contact partenaire - Sortez';

        $message_html = '
        <p>Bonjour,<br/>
        Ceci est une notification de contact de Sortez.org<br/>
        ci-dessous le contenu,</p>
        <p>Nom : ' . $contact_partner_nom . '<br/>
        T&eacute;l&eacute;phone : ' . $contact_partner_tel . '<br/>
        Email : ' . $contact_partner_mail . '<br/>
        Message : <br/>' . $contact_partner_msg . '
        ';

        $sending_mail_privicarte = false;

        $message_html = html_entity_decode(htmlentities($message_html));

        $zContactEmailCom_ = array();
        $zContactEmailCom_[] = array("Email" => $contact_partner_mailto, "Name" => $contact_partner_mailto);
        $sending_mail_privicarte = @envoi_notification($zContactEmailCom_, $contact_partner_mailSubject, $message_html, $contact_partner_mail);


        $zContactEmailCom_copy = array();
        $zContactEmailCom_copy[] = array("Email" => $contact_partner_mail, "Name" => $contact_partner_nom);
        $message_html_copy = '
        <p>Bonjour,<br/>
        Ceci est une copie de votre contact Sortez.org<br/>
        ci-dessous le contenu,</p>
        <p>Nom : ' . $contact_partner_nom . '<br/>
        T&eacute;l&eacute;phone : ' . $contact_partner_tel . '<br/>
        Email : ' . $contact_partner_mail . '<br/>
        Message : <br/>' . $contact_partner_msg . '
        ';
        $message_html_copy = html_entity_decode(htmlentities($message_html_copy));
        $sending_mail_privicarte_copy = @envoi_notification($zContactEmailCom_copy, $contact_partner_mailSubject, $message_html_copy);


        if ($sending_mail_privicarte) echo '<span style="color:#006600;">Votre message à bien été envoyé !</span>';
        else echo '<span style="color:#FF0000;">Une erreur est survenue, veuillez réessayer dans un instant !</span>';
    }

    //send recommandation
    function recommandation_partner_sendmail()
    {
        $contact_recommandation_nom = $this->input->post("contact_recommandation_nom");
        //$contact_recommandation_mail_ami = $this->input->post("contact_recommandation_mail_ami");
        $contact_recommandation_mail = $this->input->post("contact_recommandation_mail");
        $contact_recommandation_msg = $this->input->post("contact_recommandation_msg");
        $contact_recommandation_mailto = $this->input->post("contact_recommandation_mailto");
        //$contact_partner_mailto = "manager@randevteam.com";
        /*var_dump($contact_recommandation_nom);
        var_dump($contact_recommandation_mail);
        var_dump($contact_recommandation_msg);
        var_dump($contact_recommandation_mailto);;die(" donnée envoie post");*/


        $contact_partner_mailSubject = 'Contact partenaire - Sortez';

        $message_html = '
        <p>Bonjour,<br/>
        Ceci est une notification de contact de Sortez.org<br/>
        ci-dessous le contenu,</p>
        <p>Nom : ' . $contact_recommandation_nom . '<br/>
        Email : ' . $contact_recommandation_mail . '<br/>
        Message : <br/>' . $contact_recommandation_msg . '
        ';

        $sending_mail_privicarte = false;

        $message_html = html_entity_decode(htmlentities($message_html));

        $zContactEmailCom_ = array();
        $zContactEmailCom_[] = array("Email" => $contact_recommandation_mailto, "Name" => $contact_recommandation_mailto);
        $sending_mail_privicarte = envoi_notification($zContactEmailCom_, $contact_partner_mailSubject, $message_html, $contact_recommandation_mail);


        $zContactEmailCom_copy = array();
        $zContactEmailCom_copy[] = array("Email" => $contact_recommandation_mail, "Name" => $contact_recommandation_nom);
        $message_html_copy = '
        <p>Bonjour,<br/>
        Ceci est une copie de votre contact Sortez.org<br/>
        ci-dessous le contenu,</p>
        <p>Nom : ' . $contact_recommandation_nom . '<br/>
        Email : ' . $contact_recommandation_mail . '<br/>
        Message : <br/>' . $contact_recommandation_msg . '
        ';
        $message_html_copy = html_entity_decode(htmlentities($message_html_copy));
        $sending_mail_privicarte_copy = @envoi_notification($zContactEmailCom_copy, $contact_partner_mailSubject, $message_html_copy);


        if ($sending_mail_privicarte) echo '<span style="color:#006600;">Votre message à bien été envoyé !</span>';
        else echo '<span style="color:#FF0000;">Une erreur est survenue, veuillez réessayer dans un instant !</span>';
    }

    function verify_captcha($captcha)
    {
        // First, delete old captchas
        $expiration = time() - 3600; // one hour limit
        $this->db->query("DELETE FROM captcha WHERE captcha_time < " . $expiration);

        // Then see if a captcha exists:
        $sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
        $binds = array($captcha, $this->input->ip_address(), $expiration);
        $query = $this->db->query($sql, $binds);
        $row = $query->row();

        if ($row->count == 0) {
            echo "0";
        } else {
            echo "1";
        }
    }

    //William : captcha verification function on database

    function mon_inscription()
    {

        if (!$this->ion_auth->logged_in()) {
            redirect("front/professionnels/inscription");
        } else {
            $user_ion_auth = $this->ion_auth->user()->row();
            $idCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            $data['idCommercant'] = $idCommercant;
            $data['toCommercant'] = $this->Commercant->GetById($idCommercant);
            $data['toAbonnement'] = $this->Abonnement->GetCurrentCommercantAbonnement($idCommercant);
            $data['toCommertantAbonnement'] = $this->mdldemande_abonnement->getWhere('idCommercant = ' . $idCommercant . ' AND  idUser_ionauth = ' . $user_ion_auth->id);
        }

        $data["colAbonnements"] = $this->Abonnement->GetAll();

        $data['Abonnement'] = $this->Abonnement;
        $data["colDepartement"] = $this->mdldepartement->GetAll();

        $updated = $_REQUEST['updated'];
        if (isset($updated) && $updated == "1") $data['msg_abonnement'] = "Demande enregistr&eacute;e ! une notification est envoy&eacute; &agrave; l'administrateur.";

        $this->load->view("privicarte/mon_inscription", $data);


    }
    //compte basique vers premium affichage
    function BasicToPremium($id){

        $data["objCommercant"] =$this->Commercant->GetById($id);
        
        $data["colRubriques"] = $this->Rubrique->GetAll();
        $data["colSousRubriques"] = $this->SousRubrique->GetAll();//die("ok");
        $data["colVilles"] = $this->Ville->GetAll();
        $data["colVilles"] = $this->mdlville->GetAll();//var_dump($data["colVilles"]);
        $data["colDepartement"] = $this->mdldepartement->GetAll();
        $data['mdldepartement'] = $this->mdldepartement;
        $data["colCommune"] = $this->mdlcommune->GetAll();
        $data['mdlcommune'] = $this->mdlcommune;
        $data["colStatut"] = $this->mdlstatut->GetAll(); //getStatutById
        $data['new_fields'] = $this->Commercant->getAll_by_idcom($id);
        $data['assComRubrique']=$this->Rubrique->getIdRubrique($id);
        $data['assComSousRubrique']=$this->Rubrique->getSousRubrique($id);
        

        //die("ok");
        // $data["colAbonnements"] = $this->Abonnement->GetAll();//die("qfqsdf");

        // $data["objAssCommercantRubrique"] = $this->AssCommercantRubrique->GetWhere("ass_commercants_rubriques.IdCommercant = " . $commercant_UserId);

         // $data["objAssCommercantSousRubrique"] = $this->AssCommercantSousRubrique->GetWhere("ass_commercants_sousrubriques.IdCommercant = " . $commercant_UserId);
         // $data["objAssCommercantAbonnement"] = $this->AssCommercantAbonnement->GetWhere("ass_commercants_abonnements.IdCommercant = " . $commercant_UserId);        
        $this->load->view('privicarte/vwBasicToPremium', $data);  
    }




    function updateabonnement()
    {

        //save demande abonnement--------------------------------------
        $demande_abonnement = $this->input->post("demande_abonnement");
        if (!$demande_abonnement) {
            redirect("front/professionnels/mon_inscription");
        } else {
            $user_ion_auth = $this->ion_auth->user()->row();
            $idCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            $data['idCommercant'] = $idCommercant;
            $data['toCommercant'] = $this->Commercant->GetById($idCommercant);
            $data['toAbonnement'] = $this->Abonnement->GetCurrentCommercantAbonnement($idCommercant);
            $data['toCommertantAbonnement'] = $this->mdldemande_abonnement->getWhere('idCommercant = ' . $idCommercant . ' AND  idUser_ionauth = ' . $user_ion_auth->id);
        }
        $colAbonnements = $this->Abonnement->GetAll();
        $abonnement_asked = array();
        //var_dump($demande_abonnement);

        foreach ($demande_abonnement as $key_demande_abonnement => $item_demande_abonnement) {
            echo $key_demande_abonnement . "=>" . $item_demande_abonnement;
            $abonnement_asked[] = $key_demande_abonnement;
        }

        foreach ($colAbonnements as $objAbonnements) {
            $array_demande_abonnement = array('id' => NULL, 'idAbonnement' => $objAbonnements->IdAbonnement, 'idCommercant' => $idCommercant, 'idUser_ionauth' => $user_ion_auth->id);
            $IdAbonnementExist_on_database = $this->mdldemande_abonnement->IdAbonnementExist($objAbonnements->IdAbonnement, $idCommercant, $user_ion_auth->id);
            $IdAbonnementExist_on_demande = in_array(intval($objAbonnements->IdAbonnement), $abonnement_asked);
            //var_dump($IdAbonnementExist_on_database);
            //var_dump($IdAbonnementExist_on_demande);
            //if ($objAbonnements->IdAbonnement == '5') die("STOP");
            if ($IdAbonnementExist_on_database && $IdAbonnementExist_on_demande) {
            } elseif (!$IdAbonnementExist_on_database && $IdAbonnementExist_on_demande) {
                $insert_dmd_abonnement = $this->mdldemande_abonnement->insert($array_demande_abonnement);
            } elseif ((!$IdAbonnementExist_on_database && !$IdAbonnementExist_on_demande) || ($IdAbonnementExist_on_database && !$IdAbonnementExist_on_demande)) {
                $objAbonnementWhere = $this->mdldemande_abonnement->getWhere('idAbonnement = ' . $objAbonnements->IdAbonnement . ' AND idCommercant = ' . $idCommercant . ' AND  idUser_ionauth = ' . $user_ion_auth->id);
                $idAbonnementToDelete = $objAbonnementWhere[0]->id;
                //var_dump($will_tetttttt);
                $this->mdldemande_abonnement->delete($idAbonnementToDelete);
            }
            //if ($objAbonnements->IdAbonnement == '5') die("STOP");
        }


        //Envoi mail vers admin
        $objMessage = $this->input->post("Message");
        $colDestAdmin = array();
        $colDestAdmin[] = array("Email" => $this->config->item("mail_sender_adress"), "Name" => $this->config->item("mail_sender_name"));

        // Sujet
        $txtSujetAdmin = "Commerçants";

        $txtContenuAdmin = "
                    <p>Bonjour ,</p>
                    <p>Un commer&ccedil;ant vient de Modifier sa demande d\'abonnement sur Privicarte.fr.</p><p>";

        $txtContenuAdmin .= "Login : " . $this->Commercant->GetById($idCommercant)->Login . "<br/>";
        $txtContenuAdmin .= "Email : " . $this->Commercant->GetById($idCommercant)->Email . "<br/>";
        $txtContenuAdmin .= "Nom : " . $this->Commercant->GetById($idCommercant)->NomSociete . "<br/>";

        $txtContenuAdmin .= "Montant HT : " . $objMessage['montantht'] . "<br/>";
        $txtContenuAdmin .= "TVA 19.60% : " . $objMessage['montanttva'] . "<br/>";
        $txtContenuAdmin .= "Montant TTC : " . $objMessage['montantttc'] . "<br/>";
        if (isset($objMessage['devis'])) $txtContenuAdmin .= "Demande Devis<br/>";
        if (isset($objMessage['proforma'])) $txtContenuAdmin .= "Demande Facture proforma<br/>";
        if (isset($objMessage['facture'])) $txtContenuAdmin .= "Demande Facture<br/>";

        $txtContenuAdmin .= "</p><p>Cordialement,</p>";
        ////$this->firephp->log($txtContenuAdmin, 'txtContenuAdmin');

        @envoi_notification($colDestAdmin, $txtSujetAdmin, $txtContenuAdmin);


        redirect("front/professionnels/mon_inscription?updated=1");

    }

    /**
     * 06/06/2011
     * Liva :
     * Methode pour l'ajout d'un commercant
     * (A ce niveau, il s'agit d'une demande d'inscription seulemet)
     * Seuls les administrateurs qui peuvent valider l'inscription
     * et envoient les codes d'acces vers le commercants
     */
    function ajouter()
    {

        $objCommercant = $this->input->post("Societe");


        // ne pas cocher les checkbox dans admin
        $objCommercant1 = $this->input->post("Societe1");



        $objCommercant["IdVille_localisation"]=$objCommercant["IdVille"];
        ////$this->firephp->log($objCommercant, 'objCommercant');
        

        $new_fields = $this->input->post('new_fields');
        // $photo1 = $this->input->post('photo1');
        


        if (!$objCommercant) {
            redirect("front/professionnels/inscription");
        }

        $objMessage = $this->input->post("Message");
        ////$this->firephp->log($objMessage, 'objMessage');

        //var_dump($objMessage);

        $societe_Password = $this->input->post("Societe_Password");
        //$objCommercant["Email"] = $objCommercant["Login"];


        // start create nom_url *******************************************************************************
        $this->load->model("mdlville");
        $commercant_url_ville = $this->mdlville->getVilleById($objCommercant["IdVille"]);
        $commercant_url_nom_com = RemoveExtendedChars2($objCommercant["NomSociete"]);
        $commercant_url_nom_ville = RemoveExtendedChars2($commercant_url_ville->Nom);
        $commercant_url_nom = $commercant_url_nom_com . "_" . $commercant_url_nom_ville;
        $commercant_url_nom = $this->Commercant->setUrlFormat($commercant_url_nom, 0);
        $objCommercant["nom_url"] = $commercant_url_nom;
        $objCommercant["nom_url_vsv"] = $commercant_url_nom;
        // end create nom_url *********************************************************************************


        //record datecreation
        $objCommercant["datecreation"] = time();


        if ($objCommercant["IdVille"] == 0) {
            show_error('message' . $ville);
            $this->inscription();
            return;
        }
        $objAssCommercantRubrique = $this->input->post("AssCommercantRubrique");
        $objAssCommercantSousRubrique = $this->input->post("AssCommercantSousRubrique");
        $objAssCommercantAbonnement = $this->input->post("AssAbonnementCommercant");


        //ion_auth register *****************
        $username_ion_auth = $objCommercant['Login'];
        $password_ion_auth = $societe_Password;
        $email_ion_auth = $objCommercant['Email'];
        $additional_data_ion_auth = array('first_name' => $objCommercant['NomSociete'], 'company' => $objCommercant['NomSociete'], 'phone' => $objCommercant['TelFixe']);
        $abonnement_verify_ionauth = $this->Abonnement->GetById($objAssCommercantAbonnement["IdAbonnement"]);
        $value_abonnement_ionauth = '3';//sets default group to basic
        if ($abonnement_verify_ionauth->type == "gratuit"){
            $value_abonnement_ionauth = '3';
            $type_abonnement = "basique";

            $objCommercant['IsActif'] = 1;
            $objAssCommercantAbonnement["DateDebut"] = date("Y-m-d");
            $year = date("Y");
            $month = date("m");
            $day = date("d");
            $objAssCommercantAbonnement["DateFin"] = intval($year+10)."-".$month."-".$day;

        } 
        if ($abonnement_verify_ionauth->type == "premium") {            $value_abonnement_ionauth = '4';
            $type_abonnement = "premium";

            $objCommercant['IsActif'] = 0;
        }
        if ($abonnement_verify_ionauth->type == "platinum") {$value_abonnement_ionauth = '5';
            $type_abonnement = "platinium";
             $objCommercant['IsActif'] = 0;
        }
        $group_ion_auth = array($value_abonnement_ionauth);

/******Insertion du login et mdp en claire dans la table lis_login_import***********/
        $log_mdp = array('login' => $objCommercant['Login'], 'password' => $objCommercant['password']);
        $insert=$this->mdlcommercant->insertion_liste_login_import($log_mdp);
/***********************************************************************************/

        $willll_test = $this->ion_auth->register($username_ion_auth, $password_ion_auth, $email_ion_auth, $additional_data_ion_auth, $group_ion_auth);
        $ion_ath_error = $this->ion_auth->errors();

        //var_dump($ion_ath_error);

        //var_dump($objCommercant);

        ////$this->firephp->log($ion_ath_error, 'ion_ath_error');
        $user_lastid_ionauth = $this->ion_auth_used_by_club->GetUserlast_id($objCommercant['NomSociete'], $username_ion_auth, $email_ion_auth);
        $objCommercant["user_ionauth_id"] = $user_lastid_ionauth;
        //var_dump($user_lastid_ionauth);
        //die("STOP");
        //ion_auth register ******************


        if (isset($ion_ath_error) && $ion_ath_error != '' && $ion_ath_error != NULL && !$user_lastid_ionauth) {

            $data["txtContenu"] = "Une erreur s'est produite durant l'enregistrement de vos données. Veuillez recommencer s'il vous plait." . $ion_ath_error;
            $this->load->view("front/vwErreurInscriptionPro", $data);

        } else {

            $this->load->Model("Commercant");
            $objCommercant["blog"] = 1;
            $objCommercant["agenda"] = 1;
            $objCommercant["bonplan"] = 0;
            $objCommercant["annonce"] = 1;
            $objCommercant["limit_bonplan"] = 5;
            $objCommercant['referencement_annuaire'] = 1;
            $objCommercant['annonce'] = 1;
            $objCommercant['agenda'] = 1;
            $objCommercant['blog'] = 1;
            $objCommercant['bonplan'] = 1;
            // if(isset($objCommercant['referencement_resto']) && $objCommercant['referencement_resto'] != "0"){
            //     $objCommercant['referencement_resto'] = 1;
            // } else {
                $objCommercant['referencement_resto'] = 0;
            // }
            
            // if(isset($objCommercant['agenda_export']) && $objCommercant['agenda_export'] != "0"){
                $objCommercant["article_export"] = 0;
                $objCommercant["festival_export"] = 0;
                $objCommercant["annonce_export"] = 0;
                $objCommercant["bonplan_export"] = 0;
            // }


            // Enregister photo


            if(!empty($_FILES['photo1']['name'])){ 
                $user_dir = "application/resources/front/photoCommercant/imagesbank/".$user_lastid_ionauth."/";
                if (!file_exists($user_dir)) {
                    mkdir($user_dir, 0777);
                }
                
                // Set preference 
                $config['upload_path'] = $user_dir; 
                $config['allowed_types'] = 'jpg|jpeg|png'; 
                $config['max_size'] = '15000000'; // max_size in kb 
                $config['file_name'] = $_FILES['photo1']['name']; 
       
                // Load upload library 
                $this->load->library('upload',$config); 
          
                // File upload
                if($this->upload->do_upload('photo1')){ 
                   // Get data about the file
                   $uploadData = $this->upload->data(); 
                   $filename = $uploadData['file_name'];
                   $objCommercant["Photo1"] = $filename;
                }
            }


            //            var_dump($objCommercant["bonplan"]);die();
            $IdInsertedCommercant = $this->Commercant->Insert($objCommercant);

            // insert the new register field
            $new_fields['IdCommercant'] = $IdInsertedCommercant;
            $this->Commercant->insert_new_fields($new_fields);
            ///////////////////////////////

            $this->load->Model("AssCommercantRubrique");
            $objAssCommercantRubrique["IdCommercant"] = $IdInsertedCommercant;
            $this->AssCommercantRubrique->Insert($objAssCommercantRubrique);

            $this->load->Model("AssCommercantSousRubrique");
            $objAssCommercantSousRubrique["IdCommercant"] = $IdInsertedCommercant;
            $this->AssCommercantSousRubrique->Insert($objAssCommercantSousRubrique);

            $this->load->Model("AssCommercantAbonnement");
            $objAssCommercantAbonnement["IdCommercant"] = $IdInsertedCommercant;
            $this->AssCommercantAbonnement->Insert($objAssCommercantAbonnement);

            if (!empty($IdInsertedCommercant)) {


                //save demande abonnement--------------------------------------
                $demande_abonnement = $this->input->post("demande_abonnement");
                $colAbonnements = $this->Abonnement->GetAll();
                $abonnement_asked = array();
                //var_dump($demande_abonnement);

                foreach ($colAbonnements as $objAbonnements) {
                    $array_demande_abonnement = array('id' => NULL, 'idAbonnement' => $objAbonnements->IdAbonnement, 'idCommercant' => $IdInsertedCommercant, 'idUser_ionauth' => $user_lastid_ionauth);
                    $IdAbonnementExist_on_database = $this->mdldemande_abonnement->IdAbonnementExist($objAbonnements->IdAbonnement, $IdInsertedCommercant, $user_lastid_ionauth);
                    $IdAbonnementExist_on_demande = in_array($objAbonnements->IdAbonnement, $abonnement_asked);
                    if ($IdAbonnementExist_on_database && $IdAbonnementExist_on_demande) {
                    } elseif (!$IdAbonnementExist_on_database && $IdAbonnementExist_on_demande) {
                        $insert_dmd_abonnement = $this->mdldemande_abonnement->insert($array_demande_abonnement);
                    } elseif (!$IdAbonnementExist_on_database && !$IdAbonnementExist_on_demande) {
                        $this->mdldemande_abonnement->delete($objAbonnements->IdAbonnement);
                    }
                }
                //die("STOP");


                //Envoi mail vers admin
                $colDestAdmin = array();
                $colDestAdmin[] = array("Email" => $this->config->item("mail_sender_adress"), "Name" => $this->config->item("mail_sender_name"));
                //   $colDestAdmin[] = array("Email" => "alphadev@randevteam.com", "Name" => $this->config->item("mail_sender_name"));

                // Sujet
                $txtSujetAdmin = "Notification inscription nouveau Commercant Sortez";
                if(!isset($type_abonnement)){
                    $type_abonnement = '"Autre demande"';
                }

                $txtContenuAdmin = "
                    <p>Bonjour ,</p>
                    <p>Un commer&ccedil;ant vient de cr&eacute;er un compte <span style='Color:#000000; font-weight: bold;'>". $type_abonnement. "</span> gratuit chez Sortez.org</p><p>";

                $txtContenuAdmin .= "Login : " . $objCommercant['Login'] . "<br/>";
                $txtContenuAdmin .= "Password : " . $objCommercant['password'] . "<br/>";
                
                $txtContenuAdmin .= "Email : " . $objCommercant['Email'] . "<br/>";
                $txtContenuAdmin .= "Nom : " . $objCommercant['NomSociete'] . "<br/><h3 style='Color:#000000; font-weight: bold;'>DEVIS : </h3><br/>";

                if (isset($objMessage['basic'])) $txtContenuAdmin .= "Demande Abonnement Basic Gratuit<br/>";
                if (isset($objMessage['premium'])) $txtContenuAdmin .= "Demande Abonnement Premium : 250€<br/>";
                if (isset($objMessage['platinium'])) $txtContenuAdmin .= "Demande Abonnement Platinium : 400€<br/>";
                if (isset($objMessage['agenda_gratuit'])) $txtContenuAdmin .= "Demande Abonnement Agenda Gratuit<br/>";
                if (isset($objMessage['agenda_plus'])) $txtContenuAdmin .= "Demande Abonnement Agenda Plus : 250€<br/>";
                if (isset($objMessage['web_ref1'])) $txtContenuAdmin .= "Demande Abonnement Module web & référencement 1 an : 250€<br/>";
                if (isset($objMessage['web_ref_n'])) $txtContenuAdmin .= "Demande Abonnement Module web autres années : 100€<br/>";
                if (isset($objMessage['restauration'])) $txtContenuAdmin .= "Demande Abonnement Réstauration : 100€<br/>";

                // Pack services envoi par email
                if (isset($objCommercant1['referencement_agenda'])) $txtContenuAdmin .= "- Pack infos (actualité et agenda) : Quota de 50 articles & 50 événements<br/>";
                if (isset($objCommercant1['referencement_annonce'])) $txtContenuAdmin .= "- Les boutiques en ligne : Quota de 50 annonces<br/>";
                if (isset($objCommercant1['referencement_bonplan'])) $txtContenuAdmin .= "- Pack Promos : 5 bons plans et une condition de fidélisation<br/>";
                if (isset($objCommercant1['referencement_resto'])) $txtContenuAdmin .= "- Pack Restaurant : Réservations de tables, plats du jour et menu digital<br/>";
                if (isset($objCommercant1['referencement_gite'])) $txtContenuAdmin .= "- Pack Gîtes et chambres d'hôtes : Réservations et boutique en ligne<br/>";
                if (isset($new_fields['module_graphique'])) $txtContenuAdmin .= "- Module graphique Platinium<br/>";
                if (isset($objCommercant1['IsActifCommentGoogle'])) $txtContenuAdmin .= "- Module de référencement Google : uniquement avec l'abonnement Platinium<br/>";
                if (isset($objCommercant1['agenda_export'])) $txtContenuAdmin .= "- Pack export (sur devis) : agenda, actualité, boutique, bons plans, pack restaurant<br/>";

                if (isset($new_fields['aide_creation_page'])) $txtContenuAdmin .= "- Aide à la création de vos pages (sur devis)<br/>";
                if (isset($new_fields['gestion_reseaux_sociaux'])) $txtContenuAdmin .= "Création et gestion de vos réseaux sociaux<br/>";
                if (isset($new_fields['realisation_video'])) $txtContenuAdmin .= "- Réalisation d'une vidéo de présentation <br/>";
                if (isset($new_fields['publicite_magazine_sortez'])) $txtContenuAdmin .= "- Publicité sur le Magazine Sortez<br/>";
                if (isset($new_fields['impression_toutes_forme'])) $txtContenuAdmin .= "- L'impression sur toutes ses formes ! Sortez département Print<br/>";
                if (isset($new_fields['vivresaville'])) $txtContenuAdmin .= "vivresaville.fr<br/>";




                $txtContenuAdmin .= "</p><p>Lien validation Commercant : ";
                $txtContenuAdmin .= '<a href="' . site_url('admin/commercants/fiche/' . $IdInsertedCommercant) . '">' . $objCommercant['NomSociete'] . '</a>';
                $txtContenuAdmin .= "</p>";

                $txtContenuAdmin .= "<p>Cordialement,</p>";
                ////$this->firephp->log($txtContenuAdmin, 'txtContenuAdmin');

                $admin_check_mail_confirm = envoi_notification($colDestAdmin, $txtSujetAdmin, $txtContenuAdmin);


                //Envoi mail vers commercant

                $colDest = array();
                $email = $objCommercant["Login"];
                //echo $email;exit();
                //$colDest[] = array("Email"=>$this->config->item("mail_sender_adress"),"Name"=>$this->config->item("mail_sender_name"));
                $colDest[] = array("Email" => $email, "Name" => $objCommercant['NomSociete']);
                // Sujet
                $txtSujet = "Confirmation Inscription";


                $objParametreContenuMail = $this->parametre->getByCode("ContenuMailConfirmationInscriptionPro");
                // print_r($objParametreContenuMail);exit();
                $objParametreContenuPage = $this->parametre->getByCode("ContenuPageConfirmationInscriptionUser");

                $txtContenu = $objParametreContenuMail[0]->Contenu;
                $user_check_mail_confirm = envoi_notification($colDest,$txtSujet,$txtContenu);//replaced with activation code email

                /*var_dump($colDest);
                var_dump($txtSujet);
                var_dump($txtContenu);
                die();*/

                $data["txtContenu"] = $objParametreContenuPage[0]->Contenu;
                //$this->load->view("front/vwConfirmationInscriptionProfessionnels", $data);
                //$this->load->view("front2013/vwConfirmationInscriptionProfessionnels", $data);
                $this->load->view("privicarte/vwConfirmationInscriptionProfessionnels", $data);
            } else {
                $data["txtContenu"] = "<p>Une erreur s'est produite durant l'enregistrement de vos données.<br/>Veuillez recommencer s'il vous plait.</p>";
                $this->load->view("front/vwErreurInscriptionPro", $data);
            }

        }

    }


// function test_mdp(){
//         $objCommercant = $this->input->post("Societe");        
//         // $log_mdp = array('login' => $objCommercant['Login'], 'password' => $objCommercant['password']);
//         // $insert=$this->mdlcommercant->insertion_liste_login_import($log_mdp);
//         // var_dump($objCommercant['password']);
// }

    function inscription($type = "platinum")
    {
        $data = null;
        $data["colRubriques"] = $this->Rubrique->GetAll();
        $data["colSousRubriques"] = $this->SousRubrique->GetAll();
        $data["colVilles"] = $this->mdlville->GetAll();
        $data["colAbonnements"] = $this->Abonnement->GetAll();
        $data["colStatut"] = $this->mdlstatut->GetAll();
        $data['objModule']=$this->Abonnement->GetAll();
        $base_path_system = str_replace('system/', '', BASEPATH);
        $font_path_captcha = $base_path_system . "application/resources/fonts/AdobeGothicStd-Bold.otf";

        //create captcha and save into database
        $vals = array(

            'word' => '',
            'img_path' => './captcha/',
            'img_url' => base_url() . 'captcha/',
            'font_path' => $font_path_captcha,
            'img_width' => 300,
            'img_height' => 50,
            'expiration' => 7200

        );

        //end create captcha and save into database


        $data['current_page'] = "subscription_pro";

        if (!isset($type) || $type == "") $type = "basic";
        $data['type'] = $type;
        ////////////////google captcha////////////////////


////////////////////////////////////////////////////////////////

        //$this->load->view("front/vwInscriptionProfessionnels",$data);
        //$this->load->view("front2013/vwInscriptionProfessionnels",$data);
        /*if (CURRENT_SITE == "agenda"){
            $this->load->view("agenda/vwInscriptionProfessionnels",$data);
        } else {
            $this->load->view("front2013/vwInscriptionProfessionnels",$data);
        }*/
        //$this->load->view("frontAout2013/vwInscriptionProfessionnels",$data);

        $data['mdlville'] = $this->mdlville;
        $data["colDepartement"] = $this->mdldepartement->GetAll();

        //$this->load->view("privicarte/vwInscriptionProfessionnels", $data);
        //$this->load->view("privicarte/vwInscriptionBasic", $data);
        //$this->load->view("privicarte/vwInscriptionBasicmick", $data);
        if(isset($_GET['abonnement']) && $_GET['abonnement'] == 'premium'){
            $this->load->view("privicarte/vwInscriptionPremium", $data);
        } else {
            $this->load->view("privicarte/vwInscriptionProfessionnelsoriginal", $data);
        }
        
    }

    function ficheError($prmIdCommercant = 0)
    {
        $data = null;

        $this->load->view("front/vwFicheProfessionnelError", $data);
    }

    function verify_privicarte_subdomain()
    {

        $subdomain_url = $this->input->post("subdomain_url");
        $IdCommercant = $this->input->post("IdCommercant");

        $obj_verification = $this->Commercant->GetWhere(" nom_url='" . $subdomain_url . "' AND IdCommercant <> '" . $IdCommercant . "' ");

        if (count($obj_verification) > 0) echo '<span style="color:#FF0000;">URL existe d&eacute;j&agrave;</span>';
        else echo '<span style="color:#339900;">URL disponible</span>';

    }

    function verify_privicarte_subdomain_vsv()
    {

        $subdomain_url = $this->input->post("subdomain_url");
        $IdCommercant = $this->input->post("IdCommercant");

        $obj_verification = $this->Commercant->GetWhere(" nom_url_vsv='" . $subdomain_url . "' AND IdCommercant <> '" . $IdCommercant . "' ");

        if (count($obj_verification) > 0) echo '<span style="color:#FF0000;">URL existe d&eacute;j&agrave;</span>';
        else echo '<span style="color:#339900;">URL disponible</span>';

    }

    function verify_privicarte_subdomain_code()
    {

        $subdomain_url = $this->input->post("subdomain_url");
        $IdCommercant = $this->input->post("IdCommercant");

        $obj_verification = $this->Commercant->GetWhere(" nom_url='" . $subdomain_url . "' AND IdCommercant <> '" . $IdCommercant . "' ");

        if (count($obj_verification) > 0) echo '1';
        else echo '0';

    }

    function verify_privicarte_subdomain_code_vsv()
    {

        $subdomain_url = $this->input->post("subdomain_url");
        $IdCommercant = $this->input->post("IdCommercant");

        $obj_verification = $this->Commercant->GetWhere(" nom_url_vsv='" . $subdomain_url . "' AND IdCommercant <> '" . $IdCommercant . "' ");

        if (count($obj_verification) > 0) echo '1';
        else echo '0';

    }


    function glissiere($prmIdCommercant = 0, $mssg = 0)
    {

        if ($this->ion_auth->logged_in() && ($this->ion_auth->in_group(2))) {
            redirect("front/particuliers/inscription/");
        } else if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
            redirect('admin/home', 'refresh');
        } else if (!$this->ion_auth->logged_in()) {
            $this->session->set_userdata("cur_uri", $this->uri->uri_string());
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }

        if ($this->ion_auth->logged_in() && ($prmIdCommercant == 0 || $prmIdCommercant == "")) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $prmIdCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        }
        $data = null;

        $user_ion_auth = $this->ion_auth->user()->row();
        $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $data["userId"] = $commercant_UserId;

        $objAbonnementCommercant = $this->Abonnement->GetLastAbonnementCommercant($prmIdCommercant);
        $data['fiche_pro_platinum_detect'] = 1;
        $data['objAbonnementCommercant'] = $objAbonnementCommercant;

        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($prmIdCommercant);
        if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
        $data["user_groups"] = $user_groups[0];


        $objCommercant = $this->mdlglissiere->GetByIdCommercant($prmIdCommercant);
        //var_dump($objCommercant);
        $data["objCommercant"] = $objCommercant;

        $data['mssg'] = $mssg;


        $this->load->helper('ckeditor');


        //Ckeditor's configuration
        $data['ckeditor_presentation_1'] = array(

            //ID of the textarea that will be replaced
            'id' => 'presentation_1_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );


        $data['ckeditor_presentation_2'] = array(

            //ID of the textarea that will be replaced
            'id' => 'presentation_2_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor_presentation_3'] = array(

            //ID of the textarea that will be replaced
            'id' => 'presentation_3_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor_presentation_4'] = array(

            //ID of the textarea that will be replaced
            'id' => 'presentation_4_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor_page1_1'] = array(

            //ID of the textarea that will be replaced
            'id' => 'page1_1_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor_page1_2'] = array(

            //ID of the textarea that will be replaced
            'id' => 'page1_2_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor_page1_3'] = array(

            //ID of the textarea that will be replaced
            'id' => 'page1_3_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor_page1_4'] = array(

            //ID of the textarea that will be replaced
            'id' => 'page1_4_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor_page2_1'] = array(

            //ID of the textarea that will be replaced
            'id' => 'page2_1_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );


        $data['ckeditor_page2_2'] = array(

            //ID of the textarea that will be replaced
            'id' => 'page2_2_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor_page2_3'] = array(

            //ID of the textarea that will be replaced
            'id' => 'page2_3_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor_page2_4'] = array(

            //ID of the textarea that will be replaced
            'id' => 'page2_4_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $this->load->view("privicarte/glissiere_adminpro", $data);

    }


    function fiche($prmIdCommercant = 0, $mssg = 0,$current_page='')
    {
        if ($this->ion_auth->logged_in() && ($this->ion_auth->in_group(2))) {
            redirect("front/particuliers/inscription/");
        } else if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
            redirect('admin/home', 'refresh');
        } else if (!$this->ion_auth->logged_in()) {
            $this->session->set_userdata("cur_uri", $this->uri->uri_string());
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }

        if ($this->ion_auth->logged_in() && ($prmIdCommercant == 0 || $prmIdCommercant == "")) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $prmIdCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        }
        $data = null;
        // var_dump($prmIdCommercant);die();
        //$data['objbloc_info']= $this->mdlcommercant->get_bloc_by_id_com($prmIdCommercant);
        $data['objbloc_info']= $this->mdlcommercant->get_bloc_by_id_com($prmIdCommercant);
        //start verify if merchant subscription contain annonce WILLIAM
        $_iDCommercant = $prmIdCommercant;
        $current_date = date("Y-m-d");
        $query_abcom_cond = " IdCommercant = " . $_iDCommercant . " AND ('" . $current_date . "' BETWEEN DateDebut AND DateFin)";
        $toAbonnementCommercant = $this->AssCommercantAbonnement->GetWhere($query_abcom_cond, 0, 1);
        if (sizeof($toAbonnementCommercant)) {
            foreach ($toAbonnementCommercant as $oAbonnementCommercant) {
                if (isset($oAbonnementCommercant) && ($oAbonnementCommercant->IdAbonnement == 1 || $oAbonnementCommercant->IdAbonnement == 2)) {//IdAbonnement doesn't contain annonce
                    //redirect ("front/professionnels/fiche/$_iDCommercant") ;
                    $data['show_annonce_btn'] = 0;
                } else $data['show_annonce_btn'] = 1;

                if (isset($oAbonnementCommercant) && ($oAbonnementCommercant->IdAbonnement == 1 || $oAbonnementCommercant->IdAbonnement == 3)) {//IdAbonnement doesn't contain bonplan
                    //redirect ("front/professionnels/fiche/$_iDCommercant") ;
                    $data['show_bonplan_btn'] = 0;
                } else $data['show_bonplan_btn'] = 1;
            }
        } else {
            $data['show_annonce_btn'] = 0;
            $data['show_bonplan_btn'] = 0;
            //$this->load->view("front/vwFicheProfessionnelError",$data);
            redirect("front/professionnels/ficheError/");
            //exit();
        }
        //end verify if merchant subscription contain annonce WILLIAM


        $user_ion_auth = $this->ion_auth->user()->row();
        $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $data["userId"] = $commercant_UserId;

        $this->load->Model("Commercant");
        $this->load->Model("Rubrique");
        $this->load->Model("SousRubrique");
        $this->load->Model("Ville");
        $this->load->Model("Abonnement");
        $this->load->Model("AssCommercantRubrique");
        $this->load->Model("AssCommercantSousRubrique");
        $this->load->Model("AssCommercantAbonnement");
        $this->load->Model("Mdl_soutenons");

        $infocommand = $this->Mdl_soutenons->get_com_data_by_idcom($_iDCommercant);
        if (isset($infocommand) AND !empty($infocommand) AND $infocommand !=null){
            $data['is_exist_command'] ="1";
        }else{
            $data['is_exist_command'] ="0";
        }
        // var_dump($data["objbloc_info"]);die();


        $data["no_main_menu_home"] = "1";
        $data["no_left_menu_home"] = "1";

        $data["colRubriques"] = $this->Rubrique->GetAll();
        $data["colSousRubriques"] = $this->SousRubrique->GetAll();//die("ok");
        //$data["colVilles"] = $this->Ville->GetAll();
        $data["colVilles"] = $this->mdlville->GetAll();//var_dump($data["colVilles"]);
        $data["colDepartement"] = $this->mdldepartement->GetAll();
        $data['mdldepartement'] = $this->mdldepartement;
        $data["colCommune"] = $this->mdlcommune->GetAll();
        $data['mdlcommune'] = $this->mdlcommune;
        $data["colStatut"] = $this->mdlstatut->GetAll();
        $data['new_fields'] = $this->Commercant->getAll_by_idcom($_iDCommercant);

        //die("ok");
        $data["colAbonnements"] = $this->Abonnement->GetAll();//die("qfqsdf");

        $data["objCommercant"] = $this->Commercant->GetById($commercant_UserId);
        $data["objAssCommercantRubrique"] = $this->AssCommercantRubrique->GetWhere("ass_commercants_rubriques.IdCommercant = " . $commercant_UserId);

        $data["objAssCommercantSousRubrique"] = $this->AssCommercantSousRubrique->GetWhere("ass_commercants_sousrubriques.IdCommercant = " . $commercant_UserId);
        $data["objAssCommercantAbonnement"] = $this->AssCommercantAbonnement->GetWhere("ass_commercants_abonnements.IdCommercant = " . $commercant_UserId);
        // Ajouté par OP 27/11/2011
        $this->load->helper('ckeditor');


        //Ckeditor's configuration
        $data['ckeditor0'] = array(

            //ID of the textarea that will be replaced
            'id' => 'Societe[Caracteristiques]',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );
//// OP END


        $data['ckeditor1'] = array(

            //ID of the textarea that will be replaced
            'id' => 'activite1Societe',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor2'] = array(

            //ID of the textarea that will be replaced
            'id' => 'Societe[activite2]',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor3'] = array(

            //ID of the textarea that will be replaced
            'id' => 'HorairesSociete',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '150px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );


        $data['home_link_club'] = 1;
        $data['mssg'] = $mssg;

        $data['mdlville'] = $this->mdlville;

        $data['mdlglissiere'] = $this->mdlglissiere;

        $data['jquery_to_use'] = "1.4.2";

        $objAbonnementCommercant = $this->Abonnement->GetLastAbonnementCommercant($prmIdCommercant);
        $data['fiche_pro_platinum_detect'] = 1;
        $data['objAbonnementCommercant'] = $objAbonnementCommercant;

        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($prmIdCommercant);
        if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
        $data["user_groups"] = $user_groups[0];

        $page_data = $_REQUEST['page_data'];
        if (isset($page_data)) $data["page_data"] = $page_data; else $data["page_data"] = "coordonnees";


        $objGlissiere = $this->mdlglissiere->GetByIdCommercant($prmIdCommercant);
       //var_dump($objGlissiere);die();
        $data["objGlissiere"] = $objGlissiere;

        //$objbloc_info = $this->mdlglissiere->GetById_bloc_info($objGlissiere->id_glissiere);
        //var_dump($objCommercant);
        //$data["objbloc_info"] = $objbloc_info;



        //Ckeditor's configuration
        $data['ckeditor_presentation_1'] = array(

            //ID of the textarea that will be replaced
            'id' => 'presentation_1_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );


        $data['ckeditor_presentation_2'] = array(

            //ID of the textarea that will be replaced
            'id' => 'presentation_2_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor_presentation_3'] = array(

            //ID of the textarea that will be replaced
            'id' => 'presentation_3_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor_presentation_4'] = array(

            //ID of the textarea that will be replaced
            'id' => 'presentation_4_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor_page1_1'] = array(

            //ID of the textarea that will be replaced
            'id' => 'page1_1_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor_page1_2'] = array(

            //ID of the textarea that will be replaced
            'id' => 'page1_2_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor_page1_3'] = array(

            //ID of the textarea that will be replaced
            'id' => 'page1_3_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor_page1_4'] = array(

            //ID of the textarea that will be replaced
            'id' => 'page1_4_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor_page2_1'] = array(

            //ID of the textarea that will be replaced
            'id' => 'page2_1_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );


        $data['ckeditor_page2_2'] = array(

            //ID of the textarea that will be replaced
            'id' => 'page2_2_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor_page2_3'] = array(

            //ID of the textarea that will be replaced
            'id' => 'page2_3_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor_page2_4'] = array(

            //ID of the textarea that will be replaced
            'id' => 'page2_4_contenu',
            'path' => APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width' => '100%',    //Setting a custom width
                'height' => '350px',    //Setting a custom height
                'toolbar' => array(    //Setting a custom toolbar
                    array('Source'),
                    array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Outdent', 'Indent'),
                    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link'),
                    array('Font', 'FontSize', 'TextColor', 'Table', 'BGColor'),
                    array('Image', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['current_page'] = $current_page;
        $data['pagecategory'] = "admin_commercant";


        //$this->load->view("front2013/vwFicheProfessionnel_platinum",$data);
        //$this->load->view("frontAout2013/vwFicheProfessionnel_platinum",$data);
        // var_dump($data);die("tapitra");
        if (isset($oAbonnementCommercant->IdAbonnement) && $oAbonnementCommercant->IdAbonnement == 3){
    
        $this->load->view('privicarte/content_pro_basic', $data);
    }
        else{
                $this->load->view("privicarte/vwFicheProfessionnel_platinum", $data);
                
        }



    }


    function save_glissiere()
    {

        $objCommercant = $this->input->post("Societe");


        $user_ion_auth = $this->ion_auth->user()->row();
        $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $objCommercant["IdCommercant"] = $commercant_UserId;
        $objCommercant["id_ionauth"] = $user_ion_auth->id;

        //var_dump($objCommercant);

        if ($objCommercant["id_glissiere"] != '0') $IdUpdatedCommercant = $this->mdlglissiere->Update($objCommercant);
        else $IdUpdatedCommercant = $this->mdlglissiere->Insert($objCommercant);


        redirect("front/professionnels/glissiere/" . $objCommercant["IdCommercant"] . "/1");


    }


    function get_qrcode_img()
    {
        $IdCommercant = $this->input->post("IdCommercant");
        echo $this->Commercant->getById($IdCommercant)->qrcode_img;
    }

    function get_qrcode_text()
    {
        $IdCommercant = $this->input->post("IdCommercant");
        echo $this->Commercant->getById($IdCommercant)->qrcode_text;
    }

    function generate_qrcode()
    {

        $qrcode_text = $this->input->post("qrcode_text");


        $user_ion_auth = $this->ion_auth->user()->row();
        $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

        //var_dump($objCommercant);

        //if ($objCommercant["id_glissiere"] != '0') $IdUpdatedCommercant = $this->mdlglissiere->Update($objCommercant);
        //else $IdUpdatedCommercant = $this->mdlglissiere->Insert($objCommercant);


        //redirect("front/professionnels/glissiere/".$objCommercant["IdCommercant"]."/1");

        $base_path_system = str_replace('system/', '', BASEPATH);
        $qrcode_lib_path = $base_path_system . "application/resources/phpqrcode/";
        $qrcode_lib_images_url = base_url() . "application/resources/phpqrcode/images/";


        include($qrcode_lib_path . 'qrlib.php');

        //include('config.php');

        // how to save PNG codes to server

        $tempDir = $qrcode_lib_path . 'images/';
        $tempURL = $qrcode_lib_images_url;

        $codeContents = $qrcode_text;

        // we need to generate filename somehow,
        // with md5 or with database ID used to obtains $codeContents...
        $fileName = '005_file_' . md5($codeContents) . '.png';

        $pngAbsoluteFilePath = $tempDir . $fileName;
        $urlRelativeFilePath = $tempURL . $fileName;

        // generating
        if (!file_exists($pngAbsoluteFilePath)) {
            QRcode::png($codeContents, $pngAbsoluteFilePath, QR_ECLEVEL_L, 4);
            //echo 'File generated!';
            //echo '<hr />';
            $this->Commercant->UpdateQrcodeByIdCommercant($commercant_UserId, $qrcode_text, $fileName);
        } else {
            //echo 'File already generated! We can use this cached file to speed up site on common codes!';
            //echo '<hr />';
        }

        //echo 'Server PNG File: '.$pngAbsoluteFilePath;
        //echo '<hr />';

        // displaying
        echo '<img src="' . $urlRelativeFilePath . '" />';


    }


    function modifier(){
        //var_dump($_POST);
        //die();
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_userdata("cur_uri", $this->uri->uri_string());
            redirect();
        }
        $objCommercant_brute = $this->input->post("Societe");
        //var_dump($objCommercant_brute);die();
        $objCommercant=remove_2500($objCommercant_brute);
        $current=$this->input->post('current_page');
        $user_ion_auth = $this->ion_auth->user()->row();
        $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $objCommercant["IdCommercant"] = $commercant_UserId;
//        var_dump($objCommercant["IdCommercant"]); die();
        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($commercant_UserId);
        if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
        $data["user_groups"] = $user_groups[0];
        $user_groups=$user_groups[0]->id;
        //var_dump($user_groups[0]->id); die();

        $objGlissiere_brute = $this->input->post("Glissiere");
        $objGlissiere=remove_2500($objGlissiere_brute);

        $objbloc_info_brute = $this->input->post("bloc_info");
        $objbloc_info=remove_2500($objbloc_info_brute);
        //$objbloc_info_brute = $this->input->post("bloc_info");
        //$objbloc_info=remove_2500($objbloc_info_brute);
        //var_dump($objbloc_info);die();
        //var_dump($this->input->post("bloc_info[type_bloc4_page1]"));die();
        $objGlissiere["IdCommercant"] = $commercant_UserId;
        $objGlissiere["id_ionauth"] = $user_ion_auth->id;
        //var_dump($objCommercant);

        if(!isset($objGlissiere["bonplan_id"]) || $objGlissiere["bonplan_id"]=="") $objGlissiere["bonplan_id"] = 0;
        if(!isset($objGlissiere["bonplan_id_2"]) || $objGlissiere["bonplan_id_2"]=="") $objGlissiere["bonplan_id_2"] = 0;
        if(!isset($objGlissiere["bonplan_id_3"]) || $objGlissiere["bonplan_id_3"]=="") $objGlissiere["bonplan_id_3"] = 0;
        if(!isset($objGlissiere["bonplan_id_4"]) || $objGlissiere["bonplan_id_4"]=="") $objGlissiere["bonplan_id_4"] = 0;
        if(!isset($objGlissiere["fidelity_id"]) || $objGlissiere["fidelity_id"]=="") $objGlissiere["fidelity_id"] = 0;
        if(!isset($objGlissiere["fidelity_2_id"]) || $objGlissiere["fidelity_2_id"]=="") $objGlissiere["fidelity_2_id"] = 0;
        if(!isset($objGlissiere["fidelity_3_id"]) || $objGlissiere["fidelity_4_id"]=="") $objGlissiere["fidelity_3_id"] = 0;
        if(!isset($objGlissiere["fidelity_4_id"]) || $objGlissiere["fidelity_2_id"]=="") $objGlissiere["fidelity_4_id"] = 0;

        unset($objGlissiere['affichageNomCommercant']);
        //echo $objCommercant["id_glissiere"];
            /*
        if ($objCommercant["id_glissiere"] != '0' AND $objbloc_info["id_glissiere"] != '0'){
            var_dump($objbloc_info["id_glissiere"]);die();
            $IdUpdatedCommercant = $this->mdlglissiere->Update($objGlissiere);
            //$IdUpdatedCommercantBlocInfo = $this->mdlglissiere->Insert_bloc_info($objbloc_info);
            $IdUpdatedCommercantBlocInfo = $this->mdlglissiere->Update_bloc_info($objbloc_info);

        }else{
            $IdUpdatedCommercant = $this->mdlglissiere->Insert($objGlissiere);
            //$IdUpdatedCommercantBlocInfo = $this->mdlglissiere->Insert_bloc_info($objbloc_info);
        }*/

        //$bloc_info = $this->mdlglissiere->getById_bloc_info($objCommercant["IdCommercant"]);
        // var_dump($objbloc_info);die();
        $glissiere = $this->mdlglissiere->getById_glissiere($objCommercant["IdCommercant"]);
//        var_dump($glissiere); die();
       $bloc_info = $this->mdlglissiere->getById_bloc_info($objCommercant["IdCommercant"]);
        // var_dump($objbloc_info);die();
        if(isset($user_groups) AND $user_groups == '5'){
            if ($objCommercant["IdCommercant"] != '0' AND $bloc_info != NULL AND $glissiere != NULL){
                $IdUpdatedCommercant = $this->mdlglissiere->Update($objGlissiere);
                $IdUpdatedCommercantBlocInfo = $this->mdlglissiere->Update_bloc_info($objbloc_info);
                // var_dump( $IdUpdatedCommercantBlocInfo);die('1');

            }elseif ($objCommercant["IdCommercant"] != '0' AND $bloc_info ==NULL AND $glissiere != NULL){
                //var_dump($objbloc_info["id_glissiere"]);die();
                $IdUpdatedCommercant = $this->mdlglissiere->Update($objGlissiere);
                $IdUpdatedCommercantBlocInfo = $this->mdlglissiere->Insert_bloc_info($objbloc_info);
                // var_dump( $IdUpdatedCommercantBlocInfo);die('2');
            }elseif ($objCommercant["IdCommercant"] != '0' AND $bloc_info !=NULL AND $glissiere == NULL){
                //var_dump($objbloc_info["id_glissiere"]);die();
                $IdUpdatedCommercant = $this->mdlglissiere->Insert($objGlissiere);
                $IdUpdatedCommercantBlocInfo = $this->mdlglissiere->Update_bloc_info($objbloc_info);
                // var_dump( $IdUpdatedCommercantBlocInfo);die('2');
            }else{

                $IdUpdatedCommercantBlocInfo = $this->mdlglissiere->Insert($objGlissiere);
                $IdUpdatedCommercantBlocInfo = $this->mdlglissiere->Insert_bloc_info($objbloc_info);
                // var_dump( $IdUpdatedCommercantBlocInfo);die('3');
            }
        }
        if(isset($user_groups) AND $user_groups == '4'){
            if ($objCommercant["IdCommercant"] != NULL AND $glissiere != NULL){
                $IdUpdatedCommercant = $this->mdlglissiere->Update($objGlissiere);
            }
            else{
                $IdUpdatedCommercant = $this->mdlglissiere->Insert($objGlissiere);
            }
        }
        // start create nom_url & nom_url_vsv *******************************************************************************
        $this->load->model("mdlville");
        $commercant_url_ville = $this->mdlville->getVilleById($objCommercant["IdVille"]);
        $commercant_url_nom_com = RemoveExtendedChars2($objCommercant["NomSociete"]);
        $commercant_url_nom_ville = RemoveExtendedChars2($commercant_url_ville->Nom);
        $commercant_url_nom = $commercant_url_nom_com . "-" . $commercant_url_nom_ville;
        $commercant_url_nom = $this->Commercant->setUrlFormat($commercant_url_nom, $objCommercant["IdCommercant"]);

        $nom_url_updated_pro = $this->input->post("nom_url_updated_pro");
        if (isset($nom_url_updated_pro) && $nom_url_updated_pro != "") {
            $nom_url_updated_pro = RemoveExtendedChars2($nom_url_updated_pro);
            $objCommercant["nom_url"] = $nom_url_updated_pro;
        } else {
            $objCommercant["nom_url"] = $commercant_url_nom;
        }

        $nom_url_updated_pro_vsv = $this->input->post("nom_url_updated_pro_vsv");
        if (isset($nom_url_updated_pro_vsv) && $nom_url_updated_pro_vsv != "") {
            $nom_url_updated_pro_vsv = RemoveExtendedChars2($nom_url_updated_pro_vsv);
            $objCommercant["nom_url_vsv"] = $nom_url_updated_pro_vsv;
        } else {
            $objCommercant["nom_url_vsv"] = $commercant_url_nom;
        }
        // end create nom_url_vsv *********************************************************************************


        $objAssCommercantRubrique = $this->input->post("AssCommercantRubrique");
        $objAssCommercantSousRubrique = $this->input->post("AssCommercantSousRubrique");
        $objAssCommercantAbonnement = $this->input->post("AssAbonnementCommercant");


        $base_path_system = str_replace('system/', '', BASEPATH);
        $this->load->library('image_moo');

        //create background picture from color selected *************************************
        if (isset($objCommercant["bandeau_color"]) && $objCommercant["bandeau_color"] != "") {
            $file_to_resize_xxx = $base_path_system . "application/resources/front/images/wpimages2013/mini_pix_1.png";
            $bandeau_color_image = md5(rand() * time()) . '.png';
            $file_to_resize_yyy = $base_path_system . "application/resources/front/images/wpimages2013/" . $bandeau_color_image;
            if (file_exists($file_to_resize_xxx)) {
                $this->image_moo
                    ->load($file_to_resize_xxx)
                    ->set_background_colour($objCommercant["bandeau_color"])
                    ->resize(900, 455, true)
                    ->save($file_to_resize_yyy, true);
                $error_image_imoo_xxx = $this->image_moo->display_errors();
                //$qdfsdqsdfqg='qsdfqsd';
                if (file_exists($file_to_resize_yyy)) {
                    $objCommercant["bandeau_color_image"] = $bandeau_color_image;
                }
            }
        }


        $image_error_width_heigth_verify = '';


        if (isset($_FILES["SocietePdf"]["name"])) $pdf = $_FILES["SocietePdf"]["name"];
        if (isset($pdf) && $pdf != "") {
            $pdfAssocie = $pdf;
            $pdfCom = doUpload("SocietePdf", "application/resources/front/photoCommercant/images/", $pdfAssocie);
        } else {
            $pdfAssocie = $this->input->post("pdfAssocie");
            $pdfCom = $pdfAssocie;
        }


        $objCommercant["Pdf"] = $pdfCom;
        $objCommercant["Logo"] = $this->input->post("photologoAssocie");
        $objCommercant["bandeau_top"] = $this->input->post("bandeau_topAssocie");
        $objCommercant["background_image"] = $this->input->post("background_imageAssocie");
        $objCommercant["photogauche"] = $this->input->post("photogaucheAssocie");
        $objCommercant["photodroite"] = $this->input->post("photodroiteAssocie");
        $objCommercant["Photo1"] = $this->input->post("photo1Associe");
        $objCommercant["Photo2"] = $this->input->post("photo2Associe");
        $objCommercant["Photo3"] = $this->input->post("photo3Associe");
        $objCommercant["Photo4"] = $this->input->post("photo4Associe");
        $objCommercant["Photo5"] = $this->input->post("photo5Associe");
        $objCommercant["activite1_image1"] = $this->input->post("activite1_image1Associe");
        $objCommercant["activite1_image2"] = $this->input->post("activite1_image2Associe");
        $objCommercant["activite1_image3"] = $this->input->post("activite1_image3Associe");
        $objCommercant["activite1_image4"] = $this->input->post("activite1_image4Associe");
        $objCommercant["activite1_image5"] = $this->input->post("activite1_image5Associe");
        $objCommercant["activite2_image1"] = $this->input->post("activite2_image1Associe");
        $objCommercant["activite2_image2"] = $this->input->post("activite2_image2Associe");
        $objCommercant["activite2_image3"] = $this->input->post("activite2_image3Associe");
        $objCommercant["activite2_image4"] = $this->input->post("activite2_image4Associe");
        $objCommercant["activite2_image5"] = $this->input->post("activite2_image5Associe");
        $new_fields = $this->input->post("new_field");


        $this->load->Model("Commercant");
        $IdUpdatedCommercant = $this->Commercant->Update($objCommercant);
        $this->Commercant->Update_news($objCommercant["IdCommercant"], $new_fields);


        //ion_auth_update
        $user_ion_auth = $this->ion_auth->user()->row();
        $data_ion_auth_update = array(
            'email' => $objCommercant['Email'],
            'first_name' => $objCommercant['NomSociete'],
            'company' => $objCommercant['NomSociete']
        );
        $this->ion_auth->update($user_ion_auth->id, $data_ion_auth_update);
        //ion_auth_update

        $this->load->Model("AssCommercantRubrique");
        $this->AssCommercantRubrique->DeleteByIdCommercant($IdUpdatedCommercant);
        $objAssCommercantRubrique["IdCommercant"] = $IdUpdatedCommercant;
        $this->AssCommercantRubrique->Insert($objAssCommercantRubrique);

        $this->load->Model("AssCommercantSousRubrique");
        $this->AssCommercantSousRubrique->DeleteByIdCommercant($IdUpdatedCommercant);
        $objAssCommercantSousRubrique["IdCommercant"] = $IdUpdatedCommercant;
        $this->AssCommercantSousRubrique->Insert($objAssCommercantSousRubrique);

        $this->load->Model("AssCommercantAbonnement");
        /*$this->AssCommercantAbonnement->DeleteByIdCommercant($IdUpdatedCommercant);
        $objAssCommercantAbonnement["IdCommercant"] = $IdUpdatedCommercant;*/

        $query_verif_abonnement = ' IdCommercant=' . $IdUpdatedCommercant;
        $verif_abonnement_comm = $this->AssCommercantAbonnement->GetWhere($query_verif_abonnement);

        if (count($verif_abonnement_comm) == 0) $this->AssCommercantAbonnement->Insert($objAssCommercantAbonnement);

        if ($image_error_width_heigth_verify == '') $image_error_width_heigth_verify = 1;
        else {
            $image_error_width_heigth_verify = substr($image_error_width_heigth_verify, 0, -1);
        }

        //redirect("front/menuprofessionnels");
        redirect("front/professionnels/fiche/" . $commercant_UserId ."/". $image_error_width_heigth_verify . "/".$current. "?page_data=" . $_REQUEST['page_data']);
    }



    function send_nom_url_activation(){

        $user_ion_auth = $this->ion_auth->user()->row();
        $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

        // Liste des destinataires
        $colDest = array();
        $colDest[] = array("Email" => $this->config->item("mail_sender_adress"), "Name" => $this->config->item("mail_sender_name"));
        //$colDest[] = array("Email"=>"william.arthur@weburba.com","Name"=>$this->config->item("mail_sender_name"));

        // Sujet
        $txtSujet = "Demande d'Activation de Sous-domaine";

        $obj_Commercant_details = $this->Commercant->GetById($commercant_UserId);

        $txtContenu = "
                <p>Bonjour ,</p>
                <p>Un commer&ccedil;ant vient de modifier ses informations, et demande &agrave; activer son URL de sous-domaine.</p>
                <p>
                Nom : " . $obj_Commercant_details->NomSociete. "<br/>
                Login : " .$obj_Commercant_details->Login. "<br/>
                Email : " .$obj_Commercant_details->Email. "<br/>
                
                ";
        if (isset($obj_Commercant_details->nom_url_check)&&$obj_Commercant_details->nom_url_check=="0") {
            $txtContenu .= "
                URL à activer : http://www." .$obj_Commercant_details->nom_url . '.sortez.org'. "<br/>";
        } else if (isset($obj_Commercant_details->nom_url_check)&&$obj_Commercant_details->nom_url_check=="1") {
            $txtContenu .= "
                URL à activer : http://www." .$obj_Commercant_details->nom_url_vsv . '.vivresaville.fr'. "<br/>";
        }

        $txtContenu .= "
                </p>
                <p>Cordialement,</p>
            ";
        return @envoi_notification($colDest, $txtSujet, $txtContenu);
    }


    /*function activationBonPlan($iNumUnique=0){
        $ObjAssocclentBonPlan = $this->mdlcadeau->isValideNum($iNumUnique);
            if($ObjAssocclentBonPlan){
            $id =$ObjAssocclentBonPlan->id;
            $id_client =$ObjAssocclentBonPlan->id_client;
            $oClient = $this->user->getById($id_client);
            $NbrPoints = $oClient->NbrPoints+10;
            $this->mdlcadeau->ValiderNum($id);
            $this->mdlcadeau->AjouterPointsClients($id_client,$NbrPoints);
            $this->envoiMailNousContacter($NbrPoints,$oClient->Email);
            redirect("front/cadeau");
       }
   }*/


    function activationBonPlan($idComm)
    {
        $iNumUnique = $this->input->post("bonPlan");
        $oCom = $this->Commercant->GetById($idComm);
        $comNom = $oCom->Nom;
        if ($iNumUnique != "") {
            $ObjAssocclentBonPlan = $this->mdlcadeau->isValideNum($iNumUnique);
            if ($ObjAssocclentBonPlan) {
                $id = $ObjAssocclentBonPlan->id;
                $id_client = $ObjAssocclentBonPlan->id_client;
                $oClient = $this->user->getById($id_client);
                $NbrPoints = $oClient->NbrPoints + 10;
                $this->mdlcadeau->ValiderNum($id);
                $this->mdlcadeau->AjouterPointsClients($id_client, $NbrPoints);

                $this->envoiMailNousContacter($NbrPoints, $oClient->Email, $iNumUnique, $comNom);
                //redirect("front/cadeau");
                //$this->load->view("front/vwBonPlanValide", $data);
                //redirect("front/professionnels/fiche/".$idComm);

            }
        } else {

            redirect("front/professionnels/fiche/" . $idComm);
        }
    }

    function envoiMailNousContacter($NbrPoints, $mail, $iNumUnique, $comNom)
    {

        $zMessage = "";
        /* $zMessage = "Votre bon plan avec le code ".$iNumUnique." a Ã©tÃ© validÃ© par le commerÃ§ant  ".$comNom."\n";*/

        //$zMessage.= "vous avez maintenant:".$NbrPoints."points cadeaux\n";
        $zMessage .= $this->getContenuMailActivationBonPlan($iNumUnique, $comNom);
        $zTitre = "[Privicarte] Ajout de points cadeau";
        //echo $zMessage;exit();

        $zContactEmail = $mail;

        $headers = "From: " . "Privicarte" . "< Privicarte >\n";
        $headers .= "Reply-To: Privicarte \n";
        $headers .= "Bcc:randawilly@gmail.com \n"; // Copies cachÃ©es
        //$headers .='Content-Type: text/plain; charset="iso-8859-1"'."\n";
        $headers .= 'Content-Type: text/plain; charset=utf-8' . "\n";
        $headers .= 'Content-Transfer-Encoding: 8bit';
        //redirect("front/professionnels/load_bon_plan_valide");    exit();
        if (mail($zContactEmail, $zTitre, $zMessage, $headers)) {

            //redirect("front/menuprofessionnels");
            redirect("front/professionnels/load_bon_plan_valide");
        } else {
            echo "mail non envoye";//exit();

        }
    }

    function getContenuMailActivationBonPlan($iNumUnique, $comNom)
    {
        $ocontenumail = $this->mdl_mail_activation_bon_plan->get();
        $contenu = $ocontenumail->contenu;
        $contenu1 = $ocontenumail->contenu1;
        return ($contenu . " " . $iNumUnique . " " . $contenu1 . " " . $comNom);
    }

    function load_bon_plan_valide()
    {
        $this->load->view("front/vwBonPlanValide");
    }

    function getPostalCode($idVille = 0)
    {
        $oville = $this->mdlville->getVilleById($idVille);
        $data["cp"] = $oville->CodePostal;

        $data['idVille'] = $idVille;
        //$this->load->view("front/vwReponseVille", $data);
        echo $oville->CodePostal;
    }


    function getPostalCode_localisation($idVille = 0)
    {
        $oville = $this->mdlville->getVilleById($idVille);
        if (count($oville) != 0) $data["cp"] = $oville->CodePostal; else $data["cp"] = "";

        $data['idVille'] = $idVille;
        $this->load->view("front/vwReponseVille_localisation", $data);

    }

    function delete_files($prmIdCommercant = "0", $prmFiles)
    {
        //echo $prmFiles;die();
        //$oInfoCommercant = $this->Commercant->getById($prmIdCommercant);
       // $oglissiere = $this->Commercant->getByIdglissiere($prmIdCommercant);
        $filetodelete = "";
        if ($prmFiles == "Logo" || $prmFiles == "logo") {
           // $filetodelete = $oInfoCommercant->Logo;
            $this->Commercant->effacerPhotoLogoCommercant($prmIdCommercant);
        }
        if ($prmFiles == "iconePhone" || $prmFiles == "iconeLocalisation" || $prmFiles == "iconeSejour" || $prmFiles == "iconeDispo" ||$prmFiles == "iconeContact" ||$prmFiles == "iconeTable" || $prmFiles == "iconePlaquette" || $prmFiles == "iconeFacebook" || $prmFiles == "iconeVideo" || $prmFiles == "iconeTwitter" || $prmFiles == "iconeActualite" || $prmFiles == "iconeBoutique" || $prmFiles == "iconeCommentaire" || $prmFiles == "iconeMobilite") {
           // $filetodelete = $oInfoCommercant->Logo;
            $this->mdlglissiere->file_manager_delete_Icone($prmIdCommercant, $prmFiles);
        }
        if ($prmFiles == "bandeau_top") {
            //$filetodelete = $oInfoCommercant->bandeau_top;
            $this->Commercant->effacerbandeau_top($prmIdCommercant);
        }
        if ($prmFiles == "background_image") {
            //$filetodelete = $oInfoCommercant->background_image;
            //var_dump($filetodelete); die("stop");
            $this->Commercant->effacerbackground_image($prmIdCommercant);
        }
        if ($prmFiles == "Photo1") {
            //$filetodelete = $oInfoCommercant->Photo1;
            $this->Commercant->effacerPhoto1($prmIdCommercant);
        }
        if ($prmFiles == "Photo2") {
            //$filetodelete = $oInfoCommercant->Photo2;
            $this->Commercant->effacerPhoto2($prmIdCommercant);
        }
        if ($prmFiles == "Photo3") {
            //$filetodelete = $oInfoCommercant->Photo3;
            $this->Commercant->effacerPhoto3($prmIdCommercant);
        }
        if ($prmFiles == "Photo4") {
           // $filetodelete = $oInfoCommercant->Photo4;
            $this->Commercant->effacerPhoto4($prmIdCommercant);
        }
        if ($prmFiles == "Photo5") {
            //$filetodelete = $oInfoCommercant->Photo5;
            $this->Commercant->effacerPhoto5($prmIdCommercant);
        }
        if ($prmFiles == "photogauche") {
            //$filetodelete = $oInfoCommercant->Photo5;
            $this->Commercant->effacerphotogauche($prmIdCommercant);
        }
        if ($prmFiles == "photodroite") {
            //$filetodelete = $oInfoCommercant->Photo5;
            $this->Commercant->effacerphotodroite($prmIdCommercant);
        }
        if ($prmFiles == "PhotoAccueil") {
            //$filetodelete = $oInfoCommercant->PhotoAccueil;
            $this->Commercant->effacerPhotoCommercant($prmIdCommercant);
        }
        if ($prmFiles == "Pdf") {
           // $filetodelete = $oInfoCommercant->Pdf;
            $this->Commercant->effacerPdf($prmIdCommercant);
        }

        if ($prmFiles == "activite1_image1") {
           // $filetodelete = $oInfoCommercant->activite1_image1;
            $this->Commercant->effacer_activite1_image1($prmIdCommercant);
        }
        if ($prmFiles == "activite1_image2") {
            //$filetodelete = $oInfoCommercant->activite1_image2;
            $this->Commercant->effacer_activite1_image2($prmIdCommercant);
        }
        if ($prmFiles == "activite1_image3") {
           // $filetodelete = $oInfoCommercant->activite1_image3;
            $this->Commercant->effacer_activite1_image3($prmIdCommercant);
        }
        if ($prmFiles == "activite1_image4") {
          //  $filetodelete = $oInfoCommercant->activite1_image4;
            $this->Commercant->effacer_activite1_image4($prmIdCommercant);
        }
        if ($prmFiles == "activite1_image5") {
           // $filetodelete = $oInfoCommercant->activite1_image5;
            $this->Commercant->effacer_activite1_image5($prmIdCommercant);
        }
        if ($prmFiles == "activite2_image1") {
          //  $filetodelete = $oInfoCommercant->activite2_image1;
            $this->Commercant->effacer_activite2_image1($prmIdCommercant);
        }
        if ($prmFiles == "activite2_image2") {
          //  $filetodelete = $oInfoCommercant->activite2_image2;
            $this->Commercant->effacer_activite2_image2($prmIdCommercant);
        }
        if ($prmFiles == "activite2_image3") {
         //   $filetodelete = $oInfoCommercant->activite2_image3;
            $this->Commercant->effacer_activite2_image3($prmIdCommercant);
        }
        if ($prmFiles == "activite2_image4") {
        //    $filetodelete = $oInfoCommercant->activite2_image4;
            $this->Commercant->effacer_activite2_image4($prmIdCommercant);
        }
        if ($prmFiles == "activite2_image5") {
         //   $filetodelete = $oInfoCommercant->activite2_image5;
            $this->Commercant->effacer_activite2_image5($prmIdCommercant);
        }else {
            $this->Commercant->effacer_presentation_1_image_1($prmIdCommercant,$prmFiles);
        }


    }


    function verifier_email()
    {
        $txtEmail = $this->input->post("txtEmail_var");
        $oEmail = $this->user->verifier_mailUserPro($txtEmail);
        $emailExist = count($oEmail);
        $reponse = "0";
        $data['reponse'] = "0";
        if ($emailExist != "0") {
            $reponse = "1";
            //$data['reponse']="1";
            echo $reponse;
        } else {
            $reponse = "0";
            //$data['reponse']="0";
            echo $reponse;
        }

    }

    function verifier_email_ionauth()
    {
        $txtEmail = $this->input->post("txtEmail_var_ionauth");
        $oEmail = $this->user->verifier_mailUser_ionauth($txtEmail);
        $emailExist = count($oEmail);
        $reponse = "0";
        $data['reponse'] = "0";
        if ($emailExist != "0") {
            $reponse = "1";
            //$data['reponse']="1";
            echo $reponse;
        } else {
            $reponse = "0";
            //$data['reponse']="0";
            echo $reponse;
        }

    }

    function publightbox_identity()
    {
        $txtEmail = $this->input->post("publightbox_identity");
        $oEmail = $this->user->verifier_mailUser_ionauth($txtEmail);
        //var_dump($oEmail);
        $this->session->set_userdata('publightbox_email', $oEmail[0]->email);
        $this->session->set_userdata('publightbox_name', $oEmail[0]->first_name);
        if (isset($oEmail)) echo $oEmail[0]->first_name;
    }


    function verifier_login()
    {
        $txtLogin = $this->input->post("txtLogin_var");
        $oEmail = $this->user->verifier_loginUserPro($txtLogin);
        $emailExist = count($oEmail);
        $reponse = "0";
        $data['reponse'] = "0";
        if ($emailExist != "0") {
            $reponse = "1";
            //$data['reponse']="1";
            echo $reponse;
        } else {
            $reponse = "0";
            //$data['reponse']="0";
            echo $reponse;
        }

    }


    function verifier_login_ionauth()
    {
        $txtLogin = $this->input->post("txtLogin_var_ionauth");
        $oEmail = $this->user->verifier_loginUser_ionauth($txtLogin);
        $emailExist = count($oEmail);
        $reponse = "0";
        $data['reponse'] = "0";
        if ($emailExist != "0") {
            $reponse = "1";
            //$data['reponse']="1";
            echo $reponse;
        } else {
            $reponse = "0";
            //$data['reponse']="0";
            echo $reponse;
        }

    }

    function send_validation_pro()
    {
        $contact_to = $this->input->post("contact_to");
        $contact_from = $this->input->post("contact_from");
        $contact_object = $this->input->post("contact_object");
        $contact_message = $this->input->post("contact_message");
        $contact_message_content = $this->load->view('mail/' . $contact_message, '', true);

        $user_ion_auth = $this->ion_auth->user()->row();
        $idCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $objCommercant = $this->Commercant->GetById($idCommercant);

        $contact_message_content = str_replace('{name_pro}', $objCommercant->NomSociete . " " . $objCommercant->Nom . " " . $objCommercant->Prenom, $contact_message_content);
        $contact_message_content = str_replace('{email_pro}', $objCommercant->Email, $contact_message_content);
        $contact_message_content = str_replace('{id_pro}', $idCommercant, $contact_message_content);

        $colDestAdmin = array();
        $colDestAdmin[] = array("Email" => $contact_to, "Name" => 'Contact');

        $id_pack_test = '1';
        if ($contact_message == "demande_validation_referencement_annuaire") $id_pack_test = '4';
        if ($contact_message == "demande_validation_pack_promo") $id_pack_test = '1';
        if ($contact_message == "demande_validation_pack_info") $id_pack_test = '2';
        if ($contact_message == "demande_validation_pack_platinium") $id_pack_test = '3';

        $obj_validation["id"] = NULL;
        $obj_validation["id_pack_test"] = $id_pack_test;
        $obj_validation["idUser"] = $user_ion_auth->id;
        $obj_validation["idCommercant"] = $idCommercant;
        $obj_validation["isActive"] = '2';//pending activation
        $obj_validation["date_debut"] = NULL;
        $obj_validation["date_fin"] = NULL;

        $query_validation = " idUser=" . $user_ion_auth->id . " AND id_pack_test=" . $id_pack_test;
        $validation_pro = $this->pack_test_validation->getWhere($query_validation);
        if (isset($validation_pro) && count($validation_pro) > 0) {
            echo '0';
        } else {
            if (envoi_notification($colDestAdmin, $contact_object, $contact_message_content, $contact_from)) {
                $result_validation = $this->pack_test_validation->insert($obj_validation);
                echo '1';
            } else echo '0';
        }

    }
    public function test_captcha(){
        $resp=$this->input->post("g-recaptcha-response");
        if (isset($resp) AND $resp != null && $resp !="") {
            $data["captcha"]="OK";
        }
        else {
            $data["captcha"]="NO";
        }
        $http_code = 200;
        header('Content-type: json');
        header('HTTP/1.1: ' . $http_code);
        header('Status: ' . $http_code);
        exit(json_encode((array)$data));
    }
public function testtest_mail(){
    $objCommercant = $this->input->post("Societe");
    
    
    $AssCommercantRubrique = $_POST['rubriqueRecup'];
    $AssCommercantSousRubrique = $_POST['SousrubriqueRecup'];

    $statutRecup = $_POST['statusRecup'];
    $VilleRecup = $_POST['VilleRecup'];

    $DepartementRecup = $_POST['DepartementRecup'];
    // $AssCommercantSousRubrique = $_POST['SousrubriqueRecup'];

    
    $ChoixPack = $this->input->post("Societe1");
    $AutrePack = $this->input->post("new_fields");

    

    if (isset($ChoixPack['pack_info'])) $txtContenuAdmin .= "<li>Pack infos (actualité et agenda) : Quota de 50 articles & 50 événements</li>";
    if (isset($ChoixPack['referencement_annonce'])) $txtContenuAdmin .= "<li>Les boutiques en ligne : Quota de 50 annonces</li>";
    if (isset($ChoixPack['referencement_bonplan'])) $txtContenuAdmin .= "<li>Pack Promos : 5 bons plans et une condition de fidélisation</li>";
    if (isset($ChoixPack['pack_restau'])) $txtContenuAdmin .= "<li>Pack Restaurant : Réservations de tables, plats du jour et menu digital</li>";
    if (isset($ChoixPack['pack_chambres'])) $txtContenuAdmin .= "<li>Pack Gîtes et chambres d'hôtes : Réservations et boutique en ligne</li>";
    if (isset($ChoixPack['module_graph'])) $txtContenuAdmin .= "<li>Module graphique Platinium<br/>";
    if (isset($ChoixPack['module_ref'])) $txtContenuAdmin .= "<li>Module de référencement Google : uniquement avec l'abonnement Platinium</li>";


    if (isset($AutrePack['agenda_export'])) $txtContenuAdmin1 .= "<li>Pack export (sur devis) : agenda, actualité, boutique, bons plans, pack restaurant</li>";
    if (isset($AutrePack['aide_creation_page'])) $txtContenuAdmin1 .= "<li>Aide à la création de vos pages (sur devis)</li>";
    if (isset($AutrePack['gestion_reseaux_sociaux'])) $txtContenuAdmin1 .= "<li>Création et gestion de vos réseaux sociaux</li>";
    if (isset($AutrePack['realisation_video'])) $txtContenuAdmin1 .= "<li>Réalisation d'une vidéo de présentation </li>";
    if (isset($AutrePack['publicite_magazine_sortez'])) $txtContenuAdmin1 .= "<li>Publicité sur le Magazine Sortez</li>";
    if (isset($AutrePack['impression_toutes_forme'])) $txtContenuAdmin1 .= "<li>L'impression sur toutes ses formes ! Sortez département Print</li>";
    if (isset($AutrePack['vivresaville'])) $txtContenuAdmin1 .= "<li>vivresaville.fr</li>";        
                // var_dump($txtContenuAdmin);
    if (isset($objCommercant['Civilite']) && $objCommercant['Civilite'] == "0"){
        $civilite="Monsieur";
    }
    if (isset($objCommercant['Civilite']) && $objCommercant['Civilite'] == "1"){
        $civilite="Madame";
    }
    if (isset($objCommercant['Civilite']) && $objCommercant['Civilite'] == ""){
        $civilite="Mademoiselle";
    }    

        $headers = 'From: Sortez.org' . "\r\n" .
            'Reply-To: webmaster@example.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n"; 
        $subject = "Demande de souscription à un abonnement basique";
        $message = 
            "<p>Bonjour,<br/>
                Ceci est une demande de souscription à un abonnement basique<br/>
                ci-dessous les d&eacute;tails,
            </p>
            <hr/>                        
            <div>
                <ul>
                    <li><strong>Type abonnement</strong>t</li>
                        <ul>
                            <li>".$objCommercant['radio']."</li>                            
                        </ul>                    
                    <li><strong>Liste des packs choisis</strong>
                        <ul>
                            ".$txtContenuAdmin."
                        </ul>
                    </li>
                    <li><strong>Liste des autres packs choisis</strong>
                        <ul>
                            ".$txtContenuAdmin1."
                        </ul>
                    </li>                    
                </ul>
            </div>
            <div>    
                <ul>
                    <li style=''><strong>Les coordonnées du décideur</strong></li>
                        <ul>
                            <li>Nom: ".$objCommercant['Nom']."</li>
                            <li>Prenom: ".$objCommercant['Prenom']."</li>
                            <li>Civilité: ".$civilite."</li>                            
                            <li>Email: ".$objCommercant['Email_decidResp']."</li>
                            <li>Tél Direct: ".$objCommercant['TelDirectResp']."</li>
                            <li>Fonction: ".$objCommercant['Fonction']."</li>
                                                        
                        </ul>                    
                    <li><strong>Les coordonnées de votre établissement</strong>
                        <ul>
                            <li>Catégorie: ".$AssCommercantRubrique."</li>
                            <li>Sous catégorie: ".$AssCommercantSousRubrique."</li>
                            <li>Nom ou enseigne: ".$objCommercant['NomSociete']."</li>
                            <li>Status: ".$statutRecup."</li>
                            <li>Adresse: ".$objCommercant['Adresse1']."</li>
                            <li>Code postal: ".$objCommercant['CodePostal']."</li>
                            <li>Ville: ".$VilleRecup."</li>
                            <li>Département: ".$DepartementRecup."</li>
                            <li>Tél fixe: ".$objCommercant['TelFixe']."</li>
                            <li>Email Societe: ".$objCommercant['Email_decidResp']."</li>
                            <li>Site web: ".$objCommercant['SiteWeb']."</li>
                            <li>Siret: ".$objCommercant['Siret']."</li>
                            <li>Nombre employer: ".$objCommercant['nbr_employe']."</li>
                        </ul>
                    </li>                    
                </ul>                            
            </div>

            Cordialement,
            <br/>Sortez.org,";

        $envoyeur =$objCommercant['Nom'].' '.$objCommercant['Prenom'];
        $mailEnvoyeur = $objCommercant['Email_decidResp'];
        // var_dump($envoyeur);
        // var_dump($mailEnvoyeur);
            // $envoyeur="RAKOTO";
            // $mailEnvoyeur = "rakoto@gmail.com"; 


        // $to      = 'linamiranto@gmail.com';
        // $to2      = 'izheritiana@gmail.com';
        // $to2='randev@randev.ovh';
        // $to2='william.arthur.harilantoniaina@gmail.com';

        $to= 'webmaster@sortez.org';
        $to2= 'contact@sortez.org';

        $webMasterSortez[] = array("Email" => $to, "Name" => 'Sortez');
        $contactSortez[] = array("Email" => $to2, "Name" => 'Sortez');
        // $prmDestinataires2[] = array("Email" => $to2, "Name" => 'Sortez');
        // envoi_notification($webMasterSortez, $subject, $message, $prmEnvoyeur = $mail_societe, $prmEnvoyeurName = $nom_societe);
        // envoi_notification($contactSortez, $subject, $message, $prmEnvoyeur = $mail_societe, $prmEnvoyeurName = $nom_societe);


        if(@envoi_notification($webMasterSortez, $subject, $message,$mailEnvoyeur,$envoyeur) AND @envoi_notification($contactSortez, $subject, $message,$mailEnvoyeur,$envoyeur)){
            // echo 'ok';
            $this->load->view('privicarte/vwBasicToPremium');
        }

}
    public function sendEmail()
    {
        $this->load->library('email');
        // $data["objCommercant"] =$this->Commercant->GetById($id);
        $IdCommercant = $_POST['IdCommercant'];   
        $RubriqueSociete = $_POST['RubriqueSociete'];
        $SousRubriqueSociete = $_POST['SousRubriqueSociete'];
        $inputZip1 = $_POST['inputZip1'];
        $status = $_POST['status'];
        $Adresse1Societe = $_POST['Adresse1Societe'];
        $CodePostalSociete = $_POST['CodePostalSociete'];
        $IdVille_Nom_text = $_POST['IdVille_Nom_text'];
        $departement_id = $_POST['departement_id'];
        $EmailSociete = $_POST['EmailSociete'];
        $TelFixeSociete = $_POST['TelFixeSociete'];
        $SiteWebSociete = $_POST['SiteWebSociete'];
        $nbr_employe = $_POST['nbr_employe'];
        $siret = $_POST['siret'];
        $NomResponsableSociete = $_POST['NomResponsableSociete'];
        $Prenom = $_POST['Prenom'];
        $FoncRespSociete = $_POST['FoncRespSociete'];
        $Email_decidRespSociete = $_POST['Email_decidRespSociete'];
        $TelDirectRespSociete = $_POST['TelDirectRespSociete'];
        $CiviliteResponsableSociete = $_POST['CiviliteResponsableSociete'];

        
        // Type Abonnement
        // $choixAbonnement=$_POST['radio'];
        // $choix_packs=$_POST['liste_select'];
        // var_dump($choixAbonnement);
        // var_dump($choix_packs);


        if(isset($_POST["radio_value"])){  
          $result_radio = $_POST["radio_value"];  
          echo  '<strong>Abonnement :</strong><br>'.$result_radio.'<br />';

        }


        if(isset($_POST["checkboxes_value"])){  
          $result =array($_POST["checkboxes_value"]);

          foreach ($result as $choix) {
                $choix2 = str_replace(',', '<br/>', $choix);
               echo  '<strong>Packs choisies :</strong><br/>'.$choix2.'<br/>';
          }
         
        } 

        if(isset($_POST["checkBox_autre"])){   
          // echo  '<strong>Autre pack :</strong>'.$checkBox_autre.'<br />';
          $checkBox_autre =array($_POST["checkBox_autre"]);

          foreach ($checkBox_autre as $choix_autre) {
                $choix_autre2 = str_replace(',', '<br/>', $choix_autre);
               echo  '<strong>Autre Packs :</strong><br/>'.$choix_autre2.'<br/>';
          }  

        } 

            

        

        $headers = 'From: Sortez.org' . "\r\n" .
            'Reply-To: webmaster@example.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n"; 
        $subject = "Demande de souscription à un abonnement basique";
        $message = 
            "<p>Bonjour,<br/>
                Ceci est une demande de souscription à un abonnement basique<br/>
                ci-dessous les d&eacute;tails,
            </p>
            <hr/>                        
            <div>
                <ul>
                    <li><strong>Type abonnement</strong>t</li>
                        <ul>
                            <li>".$result_radio."</li>                            
                        </ul>                    
                    <li><strong>Liste des packs choisis</strong>
                        <ul>
                            <li>".$choix2."</li>
                        </ul>
                    </li>
                    <li><strong>Liste des autres packs choisis</strong>
                        <ul>
                            <li>".$choix_autre2."</li>
                        </ul>
                    </li>                    
                </ul>
            </div>
            <div>    
                <ul>
                    <li style=''><strong>Les coordonnées du décideur</strong></li>
                        <ul>
                            <li>Nom: ".$NomResponsableSociete."</li>
                            <li>Prenom: ".$Prenom."</li>
                            <li>Civilité: ".$CiviliteResponsableSociete."</li>                            
                            <li>Email: ".$Email_decidRespSociete."</li>
                            <li>Tél Direct: ".$TelDirectRespSociete."</li>
                            <li>Fonction: ".$FoncRespSociete."</li>
                                                        
                        </ul>                    
                    <li><strong>Les coordonnées de votre établissement</strong>
                        <ul>
                            <li>Catégorie: ".$RubriqueSociete."</li>
                            <li>Sous catégorie: ".$SousRubriqueSociete."</li>
                            <li>Nom ou enseigne: ".$inputZip1."</li>
                            <li>Status: ".$status."</li>
                            <li>Adresse: ".$Adresse1Societe."</li>
                            <li>Code postal: ".$CodePostalSociete."</li>
                            <li>Ville: ".$IdVille_Nom_text."</li>
                            <li>Département: ".$departement_id."</li>
                            <li>Tél fixe: ".$TelFixeSociete."</li>
                            <li>Email Societe: ".$EmailSociete."</li>
                            <li>Site web: ".$SiteWebSociete."</li>
                            <li>Siret: ".$siret."</li>
                            <li>Nombre employer: ".$nbr_employe."</li>
                        </ul>
                    </li>                    
                </ul>                            
            </div>

            Cordialement,
            <br/>Sortez.org,";

        $envoyeur =$NomResponsableSociete.' '.$Prenom;
        $mailEnvoyeur = $Email_decidRespSociete;
        // var_dump($envoyeur);
        // var_dump($mailEnvoyeur);
            // $envoyeur="RAKOTO";
            // $mailEnvoyeur = "rakoto@gmail.com"; 


        // $to      = 'linamiranto@gmail.com';
        // // $to2      = 'izheritiana@gmail.com';
        // $to2='william.arthur.harilantoniaina@gmail.com';

        $to= 'webmaster@sortez.org';
        $to2= 'contact@sortez.org';

        $webMasterSortez[] = array("Email" => $to, "Name" => 'Sortez');
        $contactSortez[] = array("Email" => $to2, "Name" => 'Sortez');
        // $prmDestinataires2[] = array("Email" => $to2, "Name" => 'Sortez');
        // envoi_notification($webMasterSortez, $subject, $message, $prmEnvoyeur = $mail_societe, $prmEnvoyeurName = $nom_societe);
        // envoi_notification($contactSortez, $subject, $message, $prmEnvoyeur = $mail_societe, $prmEnvoyeurName = $nom_societe);


        if(@envoi_notification($webMasterSortez, $subject, $message,$mailEnvoyeur,$envoyeur) AND @envoi_notification($contactSortez, $subject, $message,$mailEnvoyeur,$envoyeur)){
            echo 'ok';
        }
        //     echo json_encode();
            // $this->load->view('privicarte/vwBasicToPremium');

            // $this->load->view('privicarte/vwBasicToPremium');    
    }
    public function get_bonplan_selected($id=0){
        $this->load->model('commercant');
       $all= $this->commercant->get_bonplan_by_id($id);
       $data['bonplan']=$all;
       $data['ion_auth']=$this->commercant->get_ionauth_by_id_com($all->bonplan_commercant_id);
        $this->load->view('privicarte/includes/vwbonplan_professionnel',$data);

    }
    public function get_fidelity_selected($id=0,$idcom=0){
        $this->load->model('commercant');
        $all= $this->commercant->get_annonce_by_id($id,$idcom);
        $data['bonplan']=$all;
        $data['ion_auth']=$this->commercant->get_ionauth_by_id_com($all->id_commercant);
        $this->load->view('privicarte/includes/vwbonplan_professionnel',$data);
    }


    public function get_bonplan_selected2($id=0){
        $this->load->model('commercant');
        $all= $this->commercant->get_bonplan_by_id($id);
        if(isset($all)){
            $data['bonplan']=$all;
            $data['ion_auth']=$this->commercant->get_ionauth_by_id_com($all->bonplan_commercant_id);
            $this->load->view('privicarte/includes/vwbonplan_professionnel',$data);
        }


    }

    public function get_fidelity_selected2($id=0){

        $this->load->model('commercant');
        $all= $this->commercant->get_annonce_by_id($id);
        $data['bonplan']=$all;
        $data['ion_auth']=$this->commercant->get_ionauth_by_id_com($all->annonce_commercant_id);
        $this->load->view('privicarte/includes/vwbonplan_professionnel',$data);
    }

    public function get_commercant_banner_image(){
        //$IdCommercant = $id_annonce = $id_bonplan (selon le post)
        $id_ion_auth=$this->input->post('Ion_auth');
        $IdCommercant=$this->input->post('id_com');
        $rubrique=$this->input->post('rubrique');
        $data['ion_auth']=$id_ion_auth;
        $data['oInfoCommercant'] = $this->Commercant->getById($IdCommercant);
        $oDetailAnnonce = $this->mdlannonce->detailAnnonce($IdCommercant);
        $data['oDetailAnnonce'] = $oDetailAnnonce;

        if ($IdCommercant == 0) {
            $oBonPlan2 = $this->mdlbonplan->lastBonplanCom2($IdCommercant);
        } else {
            $oBonPlan2 = $this->mdlbonplan->getById($IdCommercant);
        }
        $data['toBonPlan2'] = $oBonPlan2;
        //$data['toBonPlan2'] = $this;
        if (isset($id_ion_auth) AND isset($IdCommercant) AND isset($rubrique) AND $rubrique=='home' ){

            $this->load->view('privicarte/includes/banner_iframe_commercant_home',$data);

        }elseif(isset($id_ion_auth) AND isset($IdCommercant) AND isset($rubrique) AND $rubrique=='p2' ){

            $this->load->view('privicarte/includes/banner_iframe_commercant_p2',$data);

        }elseif(isset($id_ion_auth) AND isset($IdCommercant) AND isset($rubrique) AND $rubrique=='p1' ){

            $this->load->view('privicarte/includes/banner_iframe_commercant_p1',$data);

        }elseif(isset($id_ion_auth) AND isset($IdCommercant) AND isset($rubrique) AND $rubrique=='bp' ){

            $this->load->view('privicarte/includes/banner_iframe_commercant_bp',$data);

        }elseif(isset($id_ion_auth) AND isset($IdCommercant) AND isset($rubrique) AND $rubrique=='annonce' ){

            $this->load->view('privicarte/includes/banner_iframe_commercant_annonce',$data);

        }elseif(isset($id_ion_auth) AND isset($IdCommercant) AND isset($rubrique) AND $rubrique=='ag' ){
            $data['oDetailAgenda']=$this->Mdl_agenda->getById($IdCommercant);
            $this->load->view('privicarte/includes/banner_iframe_commercant_ag',$data);

        }elseif(isset($id_ion_auth) AND isset($IdCommercant) AND isset($rubrique) AND $rubrique=='art' ){

            $data['oDetailAgenda']=$this->Mdlarticle->getById($IdCommercant);
            $this->load->view('privicarte/includes/banner_iframe_commercant_art',$data);

        }elseif(isset($id_ion_auth) AND isset($IdCommercant) AND isset($rubrique) AND $rubrique=='partner' ){

            $this->load->view('privicarte/includes/banner_iframe_commercant_partner',$data);

        }

    }

    public function valid_demande_module_platinum(){
        if (!$this->ion_auth->logged_in()) {
            redirect("connexion");
        }else{
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            if ($iduser !=null AND $iduser!=""){
                $aboutcom = $this->Commercant->GetById($iduser);
                $obj=$this->input->post('object');
                $fields=json_decode($obj);
                $data = new stdClass ;
                foreach($fields as $key => $val)
                {
                    if (preg_match('/on/',$val)){
                        $data->$key=$val;
                    }
                }
                $field= (array)$data;
                $nom=new stdClass;
                foreach ($field   as $key2 => $val2){
                    $id=  preg_replace('/on_/','',$val2);
                    $name= $this->Abonnement->get_name_by_id($id);
                    $nom->$key2 =$name;

                }
                $list = (array)$nom;
                $txtContenuAdmin="Bonjour,<br>";
                $txtContenuAdmin .="<br>";
                $txtContenuAdmin .="Le commercant : '".$fields->Nom_complet."' a demandé l'activation des modules payants suivants:<br>";
                $txtContenuAdmin .="<br>";
                $txtSujetAdmin="Demande d' activation des modules payants";
                foreach ($list as $lists){
                    $txtContenuAdmin .= '-'.$lists.'<br>';
                };
                $txtContenuAdmin .="<br>";
                $txtContenuAdmin .="Le coût total s'élève à ".$fields->price.' Euro TTC'.'<br>';
                $txtContenuAdmin .="Type de demande: ".$fields->val_type_demande.'<br>';
                $txtContenuAdmin .="Type de paiement: ".$fields->val_type_paiement.'<br>';
                $txtContenuAdmin .="<br>";
                $colDestAdmin1[] = array("Email" => "contact@sortez.org", "Name" => "contact sortez" . " " . "contact sortez");
                $colDestAdmin1_client[] = array("Email" => $aboutcom->Email, "Name" => "contact sortez" . " " . "contact sortez");

                //$contact_message_content =utf8_decode($txtContenu);
                $txtContenuAdmin_client="Bonjour,<br>";
                $txtContenuAdmin_client .="<br>";
                $txtContenuAdmin_client .="Ceci est un mail de confirmation de votre demande d'activation  des modules payants suivants:<br>";
                foreach ($list as $lists){
                    $txtContenuAdmin_client .= '-'.$lists.'<br>';
                };
                $txtContenuAdmin_client .="<br>";
                $txtContenuAdmin_client .="Le coût total s'élève à ".$fields->price.' Euro TTC'.'<br>';
                $txtContenuAdmin_client .="Type de demande: ".$fields->val_type_demande.'<br>';
                $txtContenuAdmin_client .="Type de paiement: ".$fields->val_type_paiement.'<br>';
                $txtContenuAdmin_client .="<br>";
                if (@envoi_notification($colDestAdmin1, $txtSujetAdmin, $txtContenuAdmin)){
                    if (@envoi_notification($colDestAdmin1_client, $txtSujetAdmin, $txtContenuAdmin_client)){
                        echo 'ok';
                    }
                }
            }

        }
    }


    public function get_abonnement_plat(){
        $info=$this->input->post('commercant');
        $infocom=$this->mdlcommercant->infoCommercant($info);
        $data['toCommercant']=$infocom;
        $data['objModule']=$this->Abonnement->GetAll();
        $this->load->view('privicarte/includes/vw_module_abonnement',$data);
    }
    function deleteimageblocinfo($prmIdCommercant,$prmFiles){
        $user_ion_auth = $this->ion_auth->user()->row();
        $idCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $this->mdlcommercant->get_bloc_by_id_com($idCommercant);
        $this->Commercant->effacer_image($prmIdCommercant,$prmFiles);
        if (file_exists("application/resources/front/images/article/photoCommercant/" . $user_ion_auth->id."/".$prmFiles) == true) {
            unlink("application/resources/front/images/article/photoCommercant/" . $user_ion_auth->id."/".$prmFiles);
        }
    }

    function commande_domaine(){
        $domain = $this->input->post('nom_domain');
        $nom_societe = $this->input->post('nom_societe');
        $message_com = $this->input->post('message');
        $mail_societe = $this->input->post('mail_societe');
        $IdCommercant = $this->input->post('IdCommercant');
        $TelCom = $this->input->post('TelCom');

        $to      = 'webmaster@sortez.org';
        $to2      = 'contact@sortez.org';
        $subject = "Commande nom de domaine";
        $message = "<p>Bonjour,
Ceci est un Email automatique de commande d'un nom de domaine vennant d'un Commercant inscrit sur Sortez.org <br>
Nom de domaine demandé : ".$domain." <br>
Détails du commercant : <br>
Nom : ".$nom_societe." <br>
Id : ".$IdCommercant."<br>
Email : ".$mail_societe."<br>
Tél : ".$TelCom."<br>
Message du Commercant : <br>
".$message_com. "</p>";
        if(isset($domain) && $domain!= null){                         
            $prmDestinataires[] = array("Email" => $to, "Name" => 'Sortez');
            $prmDestinataires2[] = array("Email" => $to2, "Name" => 'Sortez');
            envoi_notification($prmDestinataires, $subject, $message, $prmEnvoyeur = $mail_societe, $prmEnvoyeurName = $nom_societe);
            envoi_notification($prmDestinataires2, $subject, $message, $prmEnvoyeur = $mail_societe, $prmEnvoyeurName = $nom_societe);
            echo 'ok';
        }else{
            echo "ERROR";
        }

        $this->save_domain_name($IdCommercant,$domain,$mail_societe,$message_com,$etat='0');
    }


    function registre_recap(){
        $domain = $this->input->post('nom_domain');
        $nom_societe = $this->input->post('nom_societe');
        $message_com = $this->input->post('message');
        $mail_societe = $this->input->post('mail_societe');
        $IdCommercant = $this->input->post('IdCommercant');
        $TelCom = $this->input->post('TelCom');

        $to      = 'webmaster@sortez.org';
        $to2      = 'contact@sortez.org';
        $subject = "Commande nom de domaine";
        $message = "<p>Bonjour,
Ceci est un Email automatique de commande d'un nom de domaine vennant d'un Commercant inscrit sur Sortez.org <br>
Nom de domaine demandé : ".$domain." <br>
Détails du commercant : <br>
Nom : ".$nom_societe." <br>
Id : ".$IdCommercant."<br>
Email : ".$mail_societe."<br>
Tél : ".$TelCom."<br>
Message du Commercant : <br>
".$message_com. "</p>";
        if(isset($domain) && $domain!= null){                         
            $prmDestinataires[] = array("Email" => $to, "Name" => 'Sortez');
            $prmDestinataires2[] = array("Email" => $to2, "Name" => 'Sortez');
            envoi_notification($prmDestinataires, $subject, $message, $prmEnvoyeur = $mail_societe, $prmEnvoyeurName = $nom_societe);
            envoi_notification($prmDestinataires2, $subject, $message, $prmEnvoyeur = $mail_societe, $prmEnvoyeurName = $nom_societe);
            echo 'ok';
        }else{
            echo "ERROR";
        }

        $this->save_domain_name($IdCommercant,$domain,$mail_societe,$message_com,$etat='0');
    }


   function recap_validation(){
        $idCom = $this->input->post('idCom');
        $nom_domain = $this->input->post('nom_domain');
        $sitekey = $this->input->post('sitekey');
        $secretkey = $this->input->post('secretkey');

        if(isset($nom_domain) && $nom_domain!= null && isset($sitekey) && $sitekey!= null && isset($secretkey) && $secretkey!= null){
            echo 'ok';
        }else{
            echo "ERROR";
        }
        $this->save_recap_validation($idCom ,$nom_domain ,$sitekey,$secretkey);
    }

    function save_recap_validation($idCom ,$nom_domain ,$sitekey,$secretkey){
        $this->mdlcommercant->save_recap_validation($idCom ,$nom_domain ,$sitekey,$secretkey);
        $this->mdldemande->valid_demande($idCom);
        //redirect('../admin/liste_demande/index');
    }


    //function check domain name
    function check_domain_name(){
        $domain = $this->input->post('nom_domain');
        $all = explode('.',$domain);
        // "fgcfg gcfgfcg fcgcf gfcgfc gcf fcgfcgfgfcg cg".$hj."hghjhhhkj".$$n;
        if($all[1] == 'org'){
             if (gethostbyname($domain) != $domain && gethostbyname($domain) != '185.82.212.199' ) {
              echo "DNS Record found";
             }
             else {
              echo "NO DNS Record found";
             }
        }elseif($all[1] == 'fr'){
            if (gethostbyname($domain) != $domain && gethostbyname($domain) != '185.82.212.199' ) {
                   echo "DNS Record found";
            }
            else {
             echo "NO DNS Record found";
            }
        }elseif($all[1] == 'com'){
               if (gethostbyname($domain) != $domain && gethostbyname($domain) != '104.28.9.137' ) {
                      echo "DNS Record found";
               }
               else {
                echo "NO DNS Record found";
               }
        }
        if(gethostbyname($domain) == $domain){
            print_r("1");
        }else{
            print_r("0");
        }
        
    }

    //save domaine name
    function save_domain_name($IdCommercant,$domain,$mail_societe,$message_com,$etat){
        
        $idCom = $IdCommercant;
        $message = $message_com;
        $mailCom = $mail_societe;
        $etatDemande = $etat;
        $nomDomaine = $domain;
        $data = array("idCom"=>$idCom,
                      "message"=>$message,
                    "mailCom"=> $mailCom,
                    "etatDemande"=> $etatDemande,
                    "nomDomaine"=> $nomDomaine
                );
        $this->mdldemande->saveDemandeDomaine($data);
    }

    function testStatistique(){
        // echo "function test statistique";
        getBrowser();
    }
    public function media_video_send($argument=0){

        $all=explode("-",$argument);

        $user_ion_auth = $this->ion_auth->user()->row();

        $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

        if ($iduser == null || $iduser == 0 || $iduser == "") {

            $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

        }

        $data['IdUser'] = $iduser;

        $data['user_ionauth_id'] = $user_ion_auth->id;

        $data['cat'] =$all[1];

        $data['icat'] =$all[0]; // idcommercant

        $data['img'] = $all[2];

        $resultat = $this->mdlcommercant->get_video_bg_by_idcom($all[0]);
        $data['get_data'] = $resultat;

        $this->load->view('privicarte/vwfiche_pro/form_video',$data);
    }
    public function add_video_cover(){

        $idcom = $this->input->post('IdCommercant');

        $user_ionauth_id = $this->input->post('user_ionauth_id');

        $cat = $this->input->post('cat');

        $path = $_FILES['video']['name'];

        $ext = pathinfo($path, PATHINFO_EXTENSION);

        $filename = md5(uniqid(rand(), true)).'.'.$ext;
        //var_dump($filename);die('test');
        $type_global = $_FILES['video']['type'];
        $type_array = explode('/',$type_global);
        $type = $type_array[0];

        $data['user_ionauth_id'] = $user_ionauth_id;

        $data['cat'] = $cat;

        $data['icat'] = $idcom;

//        var_dump($type);die('type');

        if ($_FILES["video"]["size"] > 20971520) {
            $data['message_error'] = 'Votre fichier est trop volumineux, max size 20Mo';
            $data['error'] = 1;
        }else{
            if($type != 'video'){

                $data['message_error'] = 'Seul les vidéos sont prise en charge';
                $data['error'] = 1;

            }else{

                $user_dir_root = "application/resources/front/photoCommercant/imagesbank/$user_ionauth_id/";

                if (!file_exists($user_dir_root)) {

                    mkdir($user_dir_root, 0777);

                }

                $user_dir = "application/resources/front/photoCommercant/imagesbank/$user_ionauth_id/$cat/";

                if (!file_exists($user_dir)) {

                    mkdir($user_dir, 0777);

                }

                $path_dir = $user_dir.$filename;

//            var_dump($path_dir);die('link');

                if(move_uploaded_file($_FILES['video']['tmp_name'], $path_dir)){
                    $insert['background_video'] = $filename;
                    $insert['IdCommercant'] = $idcom;
                    $insert['Type'] = $type_global;
                    $resultat = $this->mdlcommercant->insert_video_bg($insert);
                    if($resultat != null){
                        $data['get_data'] = $resultat;
                        $data['message_success'] = 'Votre image a été téléchargé avec succés';
                    }
                }else{
                    $data['message_error'] = 'une erreur c\'est produite, veuillez réessayez plus tard!';
                }
            }
        }

        $this->load->view('privicarte/vwfiche_pro/form_video',$data);

    }
    public function suppression_video(){

        $Idcom = $this->input->post('idcom');
        $filename = $this->input->post('filename');

        $user_ion_auth = $this->ion_auth->user()->row();

        if (file_exists("application/resources/front/photoCommercant/imagesbank/" . $user_ion_auth->id."/bgvideo/".$filename) == true) {
            unlink("application/resources/front/photoCommercant/imagesbank/" . $user_ion_auth->id."/bgvideo/".$filename);
            $this->mdlcommercant->delete_video_bg_by_idcom($Idcom);
            echo 1;
        }else{
            echo 0;
        }


    }
}

?>