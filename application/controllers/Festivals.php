<?php

    class festivals extends CI_controller
    {

        function __construct()
        {
            parent::__construct();
            $this->load->model("mdlfestival");
            $this->load->model("mdlannonce");
            $this->load->model("mdlcommercant");
            $this->load->model("mdlbonplan");
            $this->load->model("mdlcategorie");
            $this->load->model("mdlville");
            $this->load->model("mdldepartement");
            $this->load->model("mdlcommercantpagination");
            $this->load->model("sousRubrique");
            $this->load->model("AssCommercantAbonnement");
            $this->load->model("AssCommercantSousRubrique");
            $this->load->model("Commercant");
            $this->load->model("user");
            $this->load->model("mdl_agenda");
            $this->load->model("mdl_categories_agenda");
            $this->load->model("mdl_categories_festival");
            $this->load->model("mdlimagespub");
            $this->load->model("Abonnement");
            $this->load->Model("mdlarticle_perso");


            $this->load->Model("mdl_localisation");
            $this->load->Model("mdl_festival_organiser");

            $this->load->library('user_agent');
            $this->load->library("pagination");
            $this->load->library('image_moo');
            $this->load->library('session');

            $this->load->library('ion_auth');
            $this->load->model("ion_auth_used_by_club");


            check_vivresaville_id_ville();
        }

        function index()
        {
            $this->liste();
        }

        function index_alaune()
        {
            /*$this->config->load('config');
             echo $this->config->item('base_url');
             echo "<br/>".$_SERVER['HTTP_HOST'];*/

            $is_mobile = $this->agent->is_mobile();
            //test ipad user agent
            $is_mobile_ipad = $this->agent->is_mobile('ipad');
            $data['is_mobile_ipad'] = $is_mobile_ipad;
            $is_robot = $this->agent->is_robot();
            $is_browser = $this->agent->is_browser();
            $is_platform = $this->agent->platform();
            $data['is_mobile'] = $is_mobile;
            $data['is_robot'] = $is_robot;
            $data['is_browser'] = $is_browser;
            $data['is_platform'] = $is_platform;

            if (isset($_POST["from_mobile_search_page_partner"])) {
                $data['from_mobile_search_page_partner'] = $_POST["from_mobile_search_page_partner"];
                $this->session->set_userdata('from_mobile_search_page_partner', $_POST["from_mobile_search_page_partner"]);
            }
            $from_mobile_search_page_partner = $this->session->userdata('from_mobile_search_page_partner');
            if (isset($from_mobile_search_page_partner)) $data['from_mobile_search_page_partner'] = $from_mobile_search_page_partner;

            if ($this->ion_auth->logged_in()) {
                $user_ion_auth = $this->ion_auth->user()->row();
                $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                if ($iduser == null || $iduser == 0 || $iduser == "") {
                    $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                }
                $data['IdUser'] = $iduser;
            }

            $data['oAgenda_alaune'] = $this->mdl_agenda->GetAllalaune();


            $toVille = $this->mdlville->GetAgendaVilles();//get ville list of agenda
            $data['toVille'] = $toVille;
            $toCategorie_principale = $this->mdl_agenda->GetAgendaCategorie();
            $data['toCategorie_principale'] = $toCategorie_principale;


            //echo "agenda";
            $this->load->view('agenda/home_main', $data);

        }

        function test_remove_all_old_data()
        {
            //$this->mdl_agenda->get_null_article_datetime();
            //$this->mdl_agenda->delete_null_agenda_datetime();
            echo "ok";
        }

        function liste()
        {
            //var_dump($_POST);die();
            $data['infos'] = null;

            /*if ($this->ion_auth->is_admin()) {
                $this->session->set_flashdata('domain_from', '1');
                redirect("admin/home");
            }*/

            ////////////////DELETE OLD AGENDA date_fin past 8 days
            $this->mdl_agenda->deleteOldAgenda_fin8jours();
            $nom_url_commercant = $this->uri->rsegment(2);
            //////$this->firephp->log($_POST, 'POST');
            if (isset($nom_url_commercant) && $nom_url_commercant != "" && !is_numeric($nom_url_commercant)) {
                $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);
                $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);
                $data['oInfoCommercant'] = $oInfoCommercant;
                //////$this->firephp->log($oInfoCommercant, 'oInfoCommercant');

                //$data['mdlannonce'] = $this->mdlannonce ;
                $data['mdlbonplan'] = $this->mdlbonplan;

                $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
                $oBonPlan = $this->mdlbonplan->bonPlanParCommercant($_iCommercantId);
                $data['nbBonPlan'] = sizeof($oBonPlan);
                //		$data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_titre : "";		// OP 27/11/2011
                $data['sTitreBonPlan'] = (sizeof($oBonPlan) > 0) ? $oBonPlan->bonplan_texte : "";
                $data['oImagespub'] = $this->mdlimagespub->GetByImagespubActiv();
                $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

                $oLastbonplanCom = $this->mdlbonplan->lastBonplanCom($_iCommercantId);
                if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

            }
            $argOffset = (isset($_SESSION["argOffset"])) ? $_SESSION["argOffset"] : 0;
            //$argOffset = $_iPage ;
            if (preg_match("/admin/", $_SERVER["PHP_SELF"])) {
                redirect("admin/home");
            } else {

                //if the link doesn't come from page navigation, clear category session
                /*$current_URI_club = $_SERVER['REQUEST_URI'];
                $current_URI_club_array = explode("accueil/index", $current_URI_club);
                ////$this->firephp->log(count($current_URI_club_array), 'nb_array');
                if (count($current_URI_club_array)==1) {
                    $this->session->unset_userdata('iCategorieId_x');
                    $this->session->unset_userdata('iVilleId_x');
                    $this->session->unset_userdata('zMotCle_x');
                    $this->session->unset_userdata('iOrderBy_x');
                    $this->session->unset_userdata('inputStringQuandHidden_x');
                    $this->session->unset_userdata('inputStringDatedebutHidden_x');
                    $this->session->unset_userdata('inputStringDatefinHidden_x');
                    $this->session->unset_userdata('inputGeoLongitude_x');
                }*/
                //if the link doesn't come from page navigation, clear catgory session


                if ($this->input->post("inputStringHidden") == "0") unset($_SESSION['iCategorieId']);
                if ($this->input->post("inputStringHidden_sub") == "0") unset($_SESSION['iSousCategorieId']);
                if ($this->input->post("inputStringVilleHidden") == "0") unset($_SESSION['iVilleId']);
                if ($this->input->post("inputStringDepartementHidden") == "") unset($_SESSION['iDepartementId']);
                if ($this->input->post("zMotCle") == "") unset($_SESSION['zMotCle']);
                if ($this->input->post("inputStringOrderByHidden") == "0") unset($_SESSION['iOrderBy']);
                if ($this->input->post("inputStringQuandHidden")) unset($_SESSION['inputStringQuandHidden']);
                if ($this->input->post("inputStringDatedebutHidden")) unset($_SESSION['inputStringDatedebutHidden']);
                if ($this->input->post("inputStringDatefinHidden")) unset($_SESSION['inputStringDatefinHidden']);
                if ($this->input->post("inputIdCommercant")) unset($_SESSION['inputIdCommercant']);


                //$TotalRows = $this->mdlcommercantpagination->Compter();
                $PerPage = 12;
                $data["iFavoris"] = "";


                if (isset($_POST["inputStringHidden"])) {
                    unset($_SESSION['iCategorieId']);
                    unset($_SESSION['iSousCategorieId']);
                    unset($_SESSION['iVilleId']);
                    unset($_SESSION['iDepartementId']);
                    unset($_SESSION['zMotCle']);
                    unset($_SESSION['iOrderBy']);
                    unset($_SESSION['inputStringQuandHidden']);
                    unset($_SESSION['inputStringDatedebutHidden']);
                    unset($_SESSION['inputStringDatefinHidden']);
                    unset($_SESSION['inputIdCommercant']);

                    //$iCategorieId = $_POST["inputStringHidden"] ;
                    $iCategorieId_all0 = $this->input->post("inputStringHidden");
                    if (isset($iCategorieId_all0) && $iCategorieId_all0 != "" && $iCategorieId_all0 != NULL && $iCategorieId_all0 != '0') {
                        $iCategorieId_all = substr($iCategorieId_all0, 1);
                        $iCategorieId = explode(',', $iCategorieId_all);
                    } else {
                        $iCategorieId = '0';
                    }
                    //////$this->firephp->log($iCategorieId_all0, 'iCategorieId_all0');
                    //////$this->firephp->log($iCategorieId_all, 'iCategorieId_all');
                    //////$this->firephp->log($iCategorieId, 'iCategorieId');


                    //$iCategorieId = $_POST["inputStringHidden_sub"] ;
                    $iSousCategorieId_all0 = $this->input->post("inputStringHidden_sub");
                    if (isset($iSousCategorieId_all0) && $iSousCategorieId_all0 != "" && $iSousCategorieId_all0 != NULL && $iSousCategorieId_all0 != '0') {
                        $iSousCategorieId_all = substr($iSousCategorieId_all0, 1);
                        $iSousCategorieId = explode(',', $iSousCategorieId_all);
                    } else {
                        $iSousCategorieId = '0';
                    }

                    if (isset($_POST["inputStringVilleHidden"])) $iVilleId = $_POST["inputStringVilleHidden"]; else $iVilleId = "0";
                    if (isset($_POST["inputStringVilleHidden_sub"])) $iOrderBy = $_POST["inputStringVilleHidden_sub"]; else $iOrderBy = "";
                    if (isset($_POST["inputStringDepartementHidden"])) $iDepartementId = $_POST["inputStringDepartementHidden"]; else $iDepartementId = 0;
                    if (isset($_POST["inputStringOrderByHidden"])) $iOrderBy = $_POST["inputStringOrderByHidden"]; else $iOrderBy = "";
                    if (isset($_POST["zMotCle"])) $zMotCle = $_POST["zMotCle"]; else $zMotCle = '';
                    if (isset($_POST["inputStringQuandHidden"])) $inputStringQuandHidden = $_POST["inputStringQuandHidden"]; else $inputStringQuandHidden = "0";
                    if (isset($_POST["inputStringDatedebutHidden"])) $inputStringDatedebutHidden = $_POST["inputStringDatedebutHidden"]; else $inputStringDatedebutHidden = "0000-00-00";
                    if (isset($_POST["inputStringDatefinHidden"])) $inputStringDatefinHidden = $_POST["inputStringDatefinHidden"]; else $inputStringDatefinHidden = "0000-00-00";
                    //if (isset($_POST["inputIdCommercant"])) $inputIdCommercant = $_POST["inputIdCommercant"] ; else $inputIdCommercant = "0";
                    $rsegment3 = $this->uri->rsegment(3);
                    if (isset($rsegment3) && $rsegment3 != "" && !is_numeric($rsegment3)) {
                        $inputIdCommercant = $_iCommercantId;
                    } else {
                        if (isset($_POST["inputIdCommercant"])) $inputIdCommercant = $_POST["inputIdCommercant"]; else $inputIdCommercant = "0";
                    }

                    $_SESSION['iCategorieId'] = $iCategorieId;
                    $this->session->set_userdata('iCategorieId_x', $iCategorieId);
                    $_SESSION['iSousCategorieId'] = $iSousCategorieId;
                    $this->session->set_userdata('iSousCategorieId_x', $iSousCategorieId);
                    $_SESSION['iVilleId'] = $iVilleId;
                    $this->session->set_userdata('iVilleId_x', $iVilleId);
                    $_SESSION['iDepartementId'] = $iDepartementId;
                    $this->session->set_userdata('iDepartementId_x', $iDepartementId);
                    $_SESSION['zMotCle'] = $zMotCle;
                    $this->session->set_userdata('zMotCle_x', $zMotCle);
                    $_SESSION['iOrderBy'] = $iOrderBy;
                    $this->session->set_userdata('iOrderBy_x', $iOrderBy);
                    $_SESSION['inputStringQuandHidden'] = $inputStringQuandHidden;
                    $this->session->set_userdata('inputStringQuandHidden_x', $inputStringQuandHidden);
                    $_SESSION['inputStringDatedebutHidden'] = $inputStringDatedebutHidden;
                    $this->session->set_userdata('inputStringDatedebutHidden_x', $inputStringDatedebutHidden);
                    $_SESSION['inputStringDatefinHidden'] = $inputStringDatefinHidden;
                    $this->session->set_userdata('inputStringDatefinHidden_x', $inputStringDatefinHidden);
                    $_SESSION['inputIdCommercant'] = $inputIdCommercant;
                    $this->session->set_userdata('inputIdCommercant_x', $inputIdCommercant);

                    $data['iCategorieId'] = $iCategorieId;
                    $data['iSousCategorieId'] = $iSousCategorieId;
                    $data['iVilleId'] = $iVilleId;
                    $data['iDepartementId'] = $iDepartementId;
                    $data['zMotCle'] = $zMotCle;
                    $data['iOrderBy'] = $iOrderBy;
                    $data['inputStringQuandHidden'] = $inputStringQuandHidden;
                    $data['inputStringDatedebutHidden'] = $inputStringDatedebutHidden;
                    $data['inputStringDatefinHidden'] = $inputStringDatefinHidden;
                    $data['IdCommercant'] = $inputIdCommercant;

                    $session_iCategorieId = $this->session->userdata('iCategorieId_x');
                    $session_iSousCategorieId = $this->session->userdata('iSousCategorieId_x');
                    $session_iVilleId = $this->session->userdata('iVilleId_x');
                    $session_iDepartementId = $this->session->userdata('iDepartementId_x');
                    $session_zMotCle = $this->session->userdata('zMotCle_x');
                    $session_iOrderBy = $this->session->userdata('iOrderBy_x');
                    $session_inputStringQuandHidden = $this->session->userdata('inputStringQuandHidden_x');
                    $session_inputStringDatedebutHidden = $this->session->userdata('inputStringDatedebutHidden_x');
                    $session_inputStringDatefinHidden = $this->session->userdata('inputStringDatefinHidden_x');
                    $session_inputIdCommercant = $this->session->userdata('inputIdCommercant_x');

                    ////$this->firephp->log($inputStringQuandHidden, 'inputStringQuandHidden');
                    ////$this->firephp->log($session_inputStringQuandHidden, 'session_inputStringQuandHidden');

                    //$toAgenda = $this->mdlarticle->listeArticleRecherche($_SESSION['iCategorieId'], $_SESSION['iVilleId'], $_SESSION['zMotCle'], $data["iFavoris"], $argOffset, $PerPage, $_SESSION['iOrderBy'], $_SESSION['inputStringQuandHidden'], $_SESSION['inputStringDatedebutHidden'], $_SESSION['inputStringDatefinHidden']) ;
                    //log_message('error', 'william TotalRows 1 : ');
                    $TotalRows = count($this->mdlfestival->listeFestivalRecherche($session_iCategorieId, $session_iVilleId, $session_iDepartementId, $session_zMotCle, $session_iSousCategorieId, 0, 10000, $session_iOrderBy, $session_inputStringQuandHidden, $session_inputStringDatedebutHidden, $session_inputStringDatefinHidden, $session_inputIdCommercant));

                    $config_pagination = array();
                    $rsegment3 = $this->uri->rsegment(3);

                    if (strpos($rsegment3, '&content_only_list') !== false) {
                        $pieces_rseg = explode("&content_only_list", $rsegment3);
                        $rsegment3 = $pieces_rseg[0];
                    }

                    if (isset($rsegment3) && $rsegment3 != "" && is_numeric($rsegment3)) {
                        $config_pagination["base_url"] = base_url() . "article/liste/" . $rsegment3;
                    } else {
                        $config_pagination["base_url"] = base_url() . "article/liste/";
                    }
                    $config_pagination["total_rows"] = $TotalRows;
                    $config_pagination["per_page"] = $PerPage;
                    $config_pagination["uri_segment"] = 3;
                    $config_pagination['first_link'] = 'Première page';
                    $config_pagination['last_link'] = 'Dernière page';
                    $config_pagination['prev_link'] = 'Précédent';
                    $config_pagination['next_link'] = 'Suivant';
                    $this->pagination->initialize($config_pagination);
                    if (isset($rsegment3) && $rsegment3 != "" && is_numeric($rsegment3)) {
                        $page_pagination = ($rsegment3) ? $rsegment3 : 0;
                    } else {
                        $page_pagination = ($rsegment3) ? $rsegment3 : 0;
                    }
                    //$toCommercant = $this->mdlarticle->listeArticleRecherche($session_iCategorieId, $_SESSION['iVilleId'], $_SESSION['zMotCle'], $data["iFavoris"], $page_pagination, $config_pagination["per_page"], $_SESSION['iOrderBy'], $_SESSION['inputStringQuandHidden'], $_SESSION['inputStringDatedebutHidden'], $_SESSION['inputStringDatefinHidden']) ;
                    $toAgenda = $this->mdlfestival->listeFestivalRecherche($session_iCategorieId, $session_iVilleId, $session_iDepartementId, $session_zMotCle, $session_iSousCategorieId, $page_pagination, $config_pagination["per_page"], $session_iOrderBy, $session_inputStringQuandHidden, $session_inputStringDatedebutHidden, $session_inputStringDatefinHidden, $session_inputIdCommercant);
                    //log_message('error', 'william toAgenda 1 : ');
                    $data["links_pagination"] = $this->pagination->create_links();

                } else {
                    $data["iFavoris"] = "";
                    $session_iCategorieId = $this->session->userdata('iCategorieId_x');
                    $session_iSousCategorieId = $this->session->userdata('iSousCategorieId_x');
                    $session_iVilleId = $this->session->userdata('iVilleId_x');
                    $session_iDepartementId = $this->session->userdata('iDepartementId_x');
                    $session_zMotCle = $this->session->userdata('zMotCle_x');
                    $session_iOrderBy = $this->session->userdata('iOrderBy_x');
                    $session_inputStringQuandHidden = $this->session->userdata('inputStringQuandHidden_x');
                    $session_inputStringDatedebutHidden = $this->session->userdata('inputStringDatedebutHidden_x');
                    $session_inputStringDatefinHidden = $this->session->userdata('inputStringDatefinHidden_x');
                    $session_inputIdCommercant = $this->session->userdata('inputIdCommercant_x');

                    $iCategorieId = (isset($session_iCategorieId)) ? $session_iCategorieId : 0;
                    $iSousCategorieId = (isset($session_iSousCategorieId)) ? $session_iSousCategorieId : 0;
                    $iVilleId = (isset($session_iVilleId)) ? $session_iVilleId : "0";
                    $iDepartementId = (isset($session_iDepartementId)) ? $session_iDepartementId : 0;
                    $zMotCle = (isset($session_zMotCle)) ? $session_zMotCle : "";
                    $iOrderBy = (isset($session_iOrderBy)) ? $session_iOrderBy : "0";
                    $inputStringQuandHidden = (isset($session_inputStringQuandHidden)) ? $session_inputStringQuandHidden : "0";
                    $inputStringDatedebutHidden = (isset($session_inputStringDatedebutHidden)) ? $session_inputStringDatedebutHidden : "0000-00-00";
                    $inputStringDatefinHidden = (isset($session_inputStringDatefinHidden)) ? $session_inputStringDatefinHidden : "0000-00-00";
                    //$inputIdCommercant = (isset($session_inputIdCommercant)) ? $session_inputIdCommercant : "0" ;
                    $rsegment3 = $this->uri->rsegment(3);
                    if (isset($rsegment3) && $rsegment3 != "" && !is_numeric($rsegment3)) {
                        $inputIdCommercant = $_iCommercantId;
                    } else {
                        $inputIdCommercant = (isset($session_inputIdCommercant)) ? $session_inputIdCommercant : "0";
                    }
                    //$toAgenda = $this->mdlarticle->listeArticleRecherche($iCategorieId, $iVilleId, $zMotCle, $data["iFavoris"], $argOffset, $PerPage, $iOrderBy, $inputStringQuandHidden, $inputStringDatedebutHidden, $inputStringDatefinHidden) ;
                    //log_message('error', 'william TotalRows 2 : ');
                    $TotalRows = count($this->mdlfestival->listeFestivalRecherche($iCategorieId, $iVilleId, $iDepartementId, $zMotCle, $iSousCategorieId, 0, 10000, $iOrderBy, $inputStringQuandHidden, $inputStringDatedebutHidden, $inputStringDatefinHidden, $inputIdCommercant));

                    $config_pagination = array();
                    $rsegment3 = $this->uri->rsegment(3);

                    if (strpos($rsegment3, '&content_only_list') !== false) {
                        $pieces_rseg = explode("&content_only_list", $rsegment3);
                        $rsegment3 = $pieces_rseg[0];
                    }
                    //die($rsegment3);

                    if (isset($rsegment3) && $rsegment3 != "" && is_numeric($rsegment3)) {
                        $config_pagination["base_url"] = base_url() . "article/liste/" . $rsegment3;
                    } else {
                        $config_pagination["base_url"] = base_url() . "article/liste/";
                    }
                    $config_pagination["total_rows"] = $TotalRows;
                    $config_pagination["per_page"] = $PerPage;
                    $config_pagination["uri_segment"] = 3;
                    $config_pagination['first_link'] = 'Première page';
                    $config_pagination['last_link'] = 'Dernière page';
                    $config_pagination['prev_link'] = 'Précédent';
                    $config_pagination['next_link'] = 'Suivant';
                    $this->pagination->initialize($config_pagination);

                    if (isset($rsegment3) && $rsegment3 != "" && is_numeric($rsegment3)) {
                        $page_pagination = ($rsegment3) ? $rsegment3 : 0;
                    } else {
                        $page_pagination = ($rsegment3) ? $rsegment3 : 0;
                    }
                    $toAgenda = $this->mdlfestival->listeFestivalRecherche($iCategorieId, $iVilleId, $iDepartementId, $zMotCle, $iSousCategorieId, $page_pagination, $config_pagination["per_page"], $iOrderBy, $inputStringQuandHidden, $inputStringDatedebutHidden, $inputStringDatefinHidden, $inputIdCommercant);
                    //log_message('error', 'william toAgenda 2 : ');
                    $data["links_pagination"] = $this->pagination->create_links();
                }

                ////$this->firephp->log($_SERVER['REQUEST_URI'], 'PATH_INFO');

                $iNombreLiens = $TotalRows / $PerPage;
                if ($iNombreLiens > round($iNombreLiens)) {
                    $iNombreLiens = round($iNombreLiens) + 1;
                } else {
                    $iNombreLiens = round($iNombreLiens);
                }
                //////////////////////////////////

                $data["iNombreLiens"] = $iNombreLiens;

                $data["PerPage"] = $PerPage;
                $data["TotalRows"] = $TotalRows;
                $data["argOffset"] = $argOffset;
                //$toCommercant= $this->mdlcommercantpagination->GetListeCommercantPagination($PerPage, $argOffset);
                $data['toAgenda'] = $toAgenda;//william hack
                // print_r($data['toCommercant']);exit();
                //$this->load->view('front/vwAccueil', $data) ;
                $data['pagecategory'] = 'article';


                $departement_check = $this->session->userdata('iDepartementId_x');

                //get ville list of article***********************************************************************************************************
                if (isset($_iCommercantId) && $_iCommercantId != "0" && $_iCommercantId != null && $_iCommercantId != "") {
                    if (isset($departement_check) && $departement_check != "" && $departement_check != null) {
                        $toVille = $this->mdlville->GetArticleVillesByIdCommercant_by_departement($_iCommercantId, $this->session->userdata('iDepartementId_x'));
                        $data['toVille'] = $toVille;
                    } else {
                        $toVille = $this->mdlville->GetArticleVillesByIdCommercant($_iCommercantId);
                        $data['toVille'] = $toVille;
                    }
                    $toCategorie_principale = $this->mdlfestival->GetFestivalCategorie_ByIdCommercant($_iCommercantId);
                    $data['toCategorie_principale'] = $toCategorie_principale;
                } else {
                    if (isset($departement_check) && $departement_check != "" && $departement_check != null) {
                        $toVille = $this->mdlville->GetArticleVilles_pvc_by_departement($this->session->userdata('iDepartementId_x'));
                        $data['toVille'] = $toVille;
                    } else {
                        $toVille = $this->mdlville->GetArticleVilles_pvc();
                        $data['toVille'] = $toVille;
                    }
                    $toCategorie_principale = $this->mdlfestival->GetFestivalCategorie();
                    $data['toCategorie_principale'] = $toCategorie_principale;
                }
                //get ville list of article************************************************************************************************************

                $toDepartement = $this->mdldepartement->GetArticleDepartements_pvc();
                $data['toDepartement'] = $toDepartement;

                $data["mdl_localisation"] = $this->mdl_localisation;
                $data["mdlville"] = $this->mdlville;


                $is_mobile = $this->agent->is_mobile();
                //test ipad user agent
                $is_mobile_ipad = $this->agent->is_mobile('ipad');
                $data['is_mobile_ipad'] = $is_mobile_ipad;
                $is_robot = $this->agent->is_robot();
                $is_browser = $this->agent->is_browser();
                $is_platform = $this->agent->platform();
                $data['is_mobile'] = $is_mobile;
                $data['is_robot'] = $is_robot;
                $data['is_browser'] = $is_browser;
                $data['is_platform'] = $is_platform;


                if ($this->ion_auth->logged_in()) {
                    $user_ion_auth = $this->ion_auth->user()->row();
                    $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                    if ($iduser == null || $iduser == 0 || $iduser == "") {
                        $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                    }
                    $data['IdUser'] = $iduser;
                }

                ////$this->firephp->log($_REQUEST, '_REQUEST');

                if (!isset($session_iCategorieId)) $session_iCategorieId_to_count = 0; else $session_iCategorieId_to_count = $session_iCategorieId;
                if (!isset($session_iVilleId)) $session_iVilleId_to_count = 0; else $session_iVilleId_to_count = $session_iVilleId;
                if (!isset($session_iDepartementId)) $session_iDepartementId_to_count = 0; else $session_iDepartementId_to_count = $session_iDepartementId;
                if (!isset($session_zMotCle)) $session_zMotCle_to_count = ""; else $session_zMotCle_to_count = $session_zMotCle;
                if (!isset($session_iSousCategorieId)) $session_iSousCategorieId_to_count = 0; else $session_iSousCategorieId_to_count = $session_iSousCategorieId;
                if (!isset($session_iOrderBy)) $session_iOrderBy_to_count = ""; else $session_iOrderBy_to_count = $session_iOrderBy;
                $session_inputStringDatedebutHidden_to_count = "0000-00-00";
                $session_inputStringDatefinHidden_to_count = "0000-00-00";
                $session_inputIdCommercant_to_count = "0";

                $data['toArticleTout_global'] = count($this->mdlfestival->listeFestivalRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "0", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
                $data['toArticleAujourdhui_global'] = count($this->mdlfestival->listeFestivalRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "101", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
                $data['toArticleWeekend_global'] = count($this->mdlfestival->listeFestivalRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "202", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
                $data['toArticleSemaine_global'] = count($this->mdlfestival->listeFestivalRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "303", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
                $data['toArticleSemproch_global'] = count($this->mdlfestival->listeFestivalRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "404", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
                $data['toArticleMois_global'] = count($this->mdlfestival->listeFestivalRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "505", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));

                $data['toArticleJanvier_global'] = count($this->mdlfestival->listeFestivalRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "01", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
                $data['toArticleFevrier_global'] = count($this->mdlfestival->listeFestivalRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "02", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
                $data['toArticleMars_global'] = count($this->mdlfestival->listeFestivalRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "03", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
                $data['toArticleAvril_global'] = count($this->mdlfestival->listeFestivalRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "04", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
                $data['toArticleMai_global'] = count($this->mdlfestival->listeFestivalRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "05", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
                $data['toArticleJuin_global'] = count($this->mdlfestival->listeFestivalRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "06", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
                $data['toArticleJuillet_global'] = count($this->mdlfestival->listeFestivalRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "07", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
                $data['toArticleAout_global'] = count($this->mdlfestival->listeFestivalRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "08", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
                $data['toArticleSept_global'] = count($this->mdlfestival->listeFestivalRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "09", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
                $data['toArticleOct_global'] = count($this->mdlfestival->listeFestivalRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "10", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
                $data['toArticleNov_global'] = count($this->mdlfestival->listeFestivalRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "11", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
                $data['toArticleDec_global'] = count($this->mdlfestival->listeFestivalRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "12", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));


                $data['mdlbonplan'] = $this->mdlbonplan;
                $data['mdlannonce'] = $this->mdlannonce;
                $data['Commercant'] = $this->Commercant;

                //TO NOT REMOVE - for details agenda commercant
                if (isset($_iCommercantId) && isset($oInfoCommercant)) {

                    $data['toCategorie_principale'] = $this->mdlfestival->GetFestivalCategorie_ByIdCommercant($_iCommercantId);

                    $data['toArticleJanvier'] = $this->mdlfestival->GetFestivalNbByMonth("01", $_iCommercantId);
                    $data['toArticleFevrier'] = $this->mdlfestival->GetFestivalNbByMonth("02", $_iCommercantId);
                    $data['toArticleMars'] = $this->mdlfestival->GetFestivalNbByMonth("03", $_iCommercantId);
                    $data['toArticleAvril'] = $this->mdlfestival->GetFestivalNbByMonth("04", $_iCommercantId);
                    $data['toArticleMai'] = $this->mdlfestival->GetFestivalNbByMonth("05", $_iCommercantId);
                    $data['toArticleJuin'] = $this->mdlfestival->GetFestivalNbByMonth("06", $_iCommercantId);
                    $data['toArticleJuillet'] = $this->mdlfestival->GetFestivalNbByMonth("07", $_iCommercantId);
                    $data['toArticleAout'] = $this->mdlfestival->GetFestivalNbByMonth("08", $_iCommercantId);
                    $data['toArticleSept'] = $this->mdlfestival->GetFestivalNbByMonth("09", $_iCommercantId);
                    $data['toArticleOct'] = $this->mdlfestival->GetFestivalNbByMonth("10", $_iCommercantId);
                    $data['toArticleNov'] = $this->mdlfestival->GetFestivalNbByMonth("11", $_iCommercantId);
                    $data['toArticleDec'] = $this->mdlfestival->GetFestivalNbByMonth("12", $_iCommercantId);

                    $data['nombre_article_com'] = $this->mdlfestival->GetByIdCommercant($_iCommercantId);

                    $data['pagecategory_partner'] = "list_agenda";

                    //$this->load->view('agenda/partner_agenda_list', $data) ;
                    //$this->load->view('privicarte/partner_agenda_list', $data);
                    $this->load->view('sortez/partner_article_list', $data);


                } else {


                    $this->session->set_userdata('nohome', '1');

                    $data["main_menu_content"] = "festival";

                    if ($_SERVER['SERVER_NAME'] == DOMAIN_SORTEZ_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_SORTEZ_GLOBAL) {
                        if ((isset($is_mobile_ipad) == false && isset($is_mobile) == true) || isset($is_robot) == true) {
                            //$this->load->view('sortez_mobile/liste_article', $data) ;
                            $this->load->view('sortez_vsv/festival_index', $data);
                        } else {
                            //$this->load->view('sortez/article', $data) ;
                            $this->load->view('sortez_vsv/festival_index', $data);
                        }
                    } elseif ($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL) {
                        $this->load->view("vivresaville/festival_index", $data);
                    }


                }


            }


            function check_Idcategory_of_subCategory()
            {
                $session_iCategorieId = $this->session->userdata('iSousCategorieId_x');
                if (isset($session_iCategorieId)) {
                    $iCategorieId_sess = $session_iCategorieId;
                    if (isset($iCategorieId_sess) && is_array($iCategorieId_sess)) {
                        $all_subcategory = "";
                        for ($i = 0; $i < count($iCategorieId_sess); $i++) {
                            $sousRubrique = $this->mdl_categories_agenda->getByIdSousCateg($iCategorieId_sess[$i]);
                            $all_subcategory .= "-" . $sousRubrique->agenda_categid;
                        }
                        echo substr($all_subcategory, 1);
                        ////$this->firephp->log($all_subcategory, 'all_subcategory');
                    } else {
                        echo "0";
                    }
                } else echo "0";
            }

            function check_Idcategory_of_Category()
            {
                $session_iCategorieId = $this->session->userdata('iSousCategorieId_x');
                if (isset($session_iCategorieId)) {
                    $iCategorieId_sess = $session_iCategorieId;
                    if (isset($iCategorieId_sess) && is_array($iCategorieId_sess)) {
                        $all_subcategory = "";
                        for ($i = 0; $i < count($iCategorieId_sess); $i++) {
                            $sousRubrique = $this->mdl_categories_agenda->getByIdSousCateg($iCategorieId_sess[$i]);
                            $all_subcategory .= "-" . $sousRubrique->agenda_categid;
                        }
                        echo substr($all_subcategory, 1);
                        ////$this->firephp->log($all_subcategory, 'all_subcategory');
                    } else {
                        echo "0";
                    }
                } else echo "0";
            }

            //this function is not the truth mon_agenda for particulier, this is a global search agenda
            function mon_agenda()
            {
                $data['infos'] = null;

                /*if ($this->ion_auth->is_admin()) {
                    $this->session->set_flashdata('domain_from', '1');
                    redirect("admin/home");
                }*/

                if (!$this->ion_auth->logged_in()) {
                    redirect("auth/login");
                } else {
                    $user_ion_auth = $this->ion_auth->user()->row();
                    $user_ionauth_id = $user_ion_auth->id;
                }

                $argOffset = (isset($_SESSION["argOffset"])) ? $_SESSION["argOffset"] : 0;
                //$argOffset = $_iPage ;
                if (preg_match("/admin/", $_SERVER["PHP_SELF"])) {
                    redirect("admin/home");
                } else {


                    //if the link doesn't come from page navigation, clear catgory session

                    if ($this->input->post("inputStringHidden") == "0") unset($_SESSION['iCategorieId']);
                    if ($this->input->post("inputStringHidden_sub") == "0") unset($_SESSION['iSousCategorieId']);
                    if ($this->input->post("inputStringVilleHidden") == "0") unset($_SESSION['iVilleId']);
                    if ($this->input->post("zMotCle") == "") unset($_SESSION['zMotCle']);
                    if ($this->input->post("inputStringOrderByHidden") == "0") unset($_SESSION['iOrderBy']);
                    if ($this->input->post("inputStringQuandHidden")) unset($_SESSION['inputStringQuandHidden']);
                    if ($this->input->post("inputStringDatedebutHidden")) unset($_SESSION['inputStringDatedebutHidden']);
                    if ($this->input->post("inputStringDatefinHidden")) unset($_SESSION['inputStringDatefinHidden']);
                    if ($this->input->post("inputIdCommercant")) unset($_SESSION['inputIdCommercant']);


                    //$TotalRows = $this->mdlcommercantpagination->Compter();
                    $PerPage = 10;
                    $data["iFavoris"] = "";


                    $toVille = $this->mdlville->GetAgendaVillesByIdUsers_ionauth($user_ionauth_id);//get ville list of agenda
                    $data['toVille'] = $toVille;
                    $toCategorie_principale = $this->mdl_agenda->GetAgendaCategorie_ByIdUsers_ionauth($user_ionauth_id);
                    $data['toCategorie_principale'] = $toCategorie_principale;


                    if (isset($_POST["inputStringHidden"])) {
                        unset($_SESSION['iCategorieId']);
                        unset($_SESSION['iSousCategorieId']);
                        unset($_SESSION['iVilleId']);
                        unset($_SESSION['zMotCle']);
                        unset($_SESSION['iOrderBy']);
                        unset($_SESSION['inputStringQuandHidden']);
                        unset($_SESSION['inputStringDatedebutHidden']);
                        unset($_SESSION['inputStringDatefinHidden']);
                        unset($_SESSION['inputIdCommercant']);

                        //$iCategorieId = $_POST["inputStringHidden"] ;
                        $iCategorieId_all0 = $this->input->post("inputStringHidden");
                        if (isset($iCategorieId_all0) && $iCategorieId_all0 != "" && $iCategorieId_all0 != NULL && $iCategorieId_all0 != '0') {
                            $iCategorieId_all = substr($iCategorieId_all0, 1);
                            $iCategorieId = explode(',', $iCategorieId_all);
                        } else {
                            $iCategorieId = '0';
                        }
                        //////$this->firephp->log($iCategorieId_all0, 'iCategorieId_all0');
                        //////$this->firephp->log($iCategorieId_all, 'iCategorieId_all');
                        //////$this->firephp->log($iCategorieId, 'iCategorieId');


                        //$iCategorieId = $_POST["inputStringHidden_sub"] ;
                        $iSousCategorieId_all0 = $this->input->post("inputStringHidden_sub");
                        if (isset($iSousCategorieId_all0) && $iSousCategorieId_all0 != "" && $iSousCategorieId_all0 != NULL && $iSousCategorieId_all0 != '0') {
                            $iSousCategorieId_all = substr($iSousCategorieId_all0, 1);
                            $iSousCategorieId = explode(',', $iSousCategorieId_all);
                        } else {
                            $iSousCategorieId = '0';
                        }

                        if (isset($_POST["inputStringVilleHidden"])) $iVilleId = $_POST["inputStringVilleHidden"]; else $iVilleId = "0";
                        if (isset($_POST["inputStringVilleHidden_sub"])) $iOrderBy = $_POST["inputStringVilleHidden_sub"]; else $iOrderBy = "";
                        if (isset($_POST["inputStringOrderByHidden"])) $iOrderBy = $_POST["inputStringOrderByHidden"]; else $iOrderBy = "";
                        if (isset($_POST["zMotCle"])) $zMotCle = $_POST["zMotCle"]; else $zMotCle = '';
                        if (isset($_POST["inputStringQuandHidden"])) $inputStringQuandHidden = $_POST["inputStringQuandHidden"]; else $inputStringQuandHidden = "0";
                        if (isset($_POST["inputStringDatedebutHidden"])) $inputStringDatedebutHidden = $_POST["inputStringDatedebutHidden"]; else $inputStringDatedebutHidden = "0000-00-00";
                        if (isset($_POST["inputStringDatefinHidden"])) $inputStringDatefinHidden = $_POST["inputStringDatefinHidden"]; else $inputStringDatefinHidden = "0000-00-00";
                        if (isset($_POST["inputIdCommercant"])) $inputIdCommercant = $_POST["inputIdCommercant"]; else $inputIdCommercant = "0";


                        $_SESSION['iCategorieId'] = $iCategorieId;
                        $this->session->set_userdata('iCategorieId_x', $iCategorieId);
                        $_SESSION['iSousCategorieId'] = $iSousCategorieId;
                        $this->session->set_userdata('iSousCategorieId_x', $iSousCategorieId);
                        $_SESSION['iVilleId'] = $iVilleId;
                        $this->session->set_userdata('iVilleId_x', $iVilleId);
                        $_SESSION['zMotCle'] = $zMotCle;
                        $this->session->set_userdata('zMotCle_x', $zMotCle);
                        $_SESSION['iOrderBy'] = $iOrderBy;
                        $this->session->set_userdata('iOrderBy_x', $iOrderBy);
                        $_SESSION['inputStringQuandHidden'] = $inputStringQuandHidden;
                        $this->session->set_userdata('inputStringQuandHidden_x', $inputStringQuandHidden);
                        $_SESSION['inputStringDatedebutHidden'] = $inputStringDatedebutHidden;
                        $this->session->set_userdata('inputStringDatedebutHidden_x', $inputStringDatedebutHidden);
                        $_SESSION['inputStringDatefinHidden'] = $inputStringDatefinHidden;
                        $this->session->set_userdata('inputStringDatefinHidden_x', $inputStringDatefinHidden);
                        $_SESSION['inputIdCommercant'] = $inputIdCommercant;
                        $this->session->set_userdata('inputIdCommercant_x', $inputIdCommercant);

                        $data['iCategorieId'] = $iCategorieId;
                        $data['iSousCategorieId'] = $iSousCategorieId;
                        $data['iVilleId'] = $iVilleId;
                        $data['zMotCle'] = $zMotCle;
                        $data['iOrderBy'] = $iOrderBy;
                        $data['inputStringQuandHidden'] = $inputStringQuandHidden;
                        $data['inputStringDatedebutHidden'] = $inputStringDatedebutHidden;
                        $data['inputStringDatefinHidden'] = $inputStringDatefinHidden;
                        $data['IdCommercant'] = $inputIdCommercant;

                        $session_iCategorieId = $this->session->userdata('iCategorieId_x');
                        $session_iSousCategorieId = $this->session->userdata('iSousCategorieId_x');
                        $session_iVilleId = $this->session->userdata('iVilleId_x');
                        $session_zMotCle = $this->session->userdata('zMotCle_x');
                        $session_iOrderBy = $this->session->userdata('iOrderBy_x');
                        $session_inputStringQuandHidden = $this->session->userdata('inputStringQuandHidden_x');
                        $session_inputStringDatedebutHidden = $this->session->userdata('inputStringDatedebutHidden_x');
                        $session_inputStringDatefinHidden = $this->session->userdata('inputStringDatefinHidden_x');
                        $session_inputIdCommercant = $this->session->userdata('inputIdCommercant_x');

                        ////$this->firephp->log($inputStringQuandHidden, 'inputStringQuandHidden');
                        ////$this->firephp->log($session_inputStringQuandHidden, 'session_inputStringQuandHidden');

                        //$toAgenda = $this->mdl_agenda->listeAgendaRecherche($_SESSION['iCategorieId'], $_SESSION['iVilleId'], $_SESSION['zMotCle'], $data["iFavoris"], $argOffset, $PerPage, $_SESSION['iOrderBy'], $_SESSION['inputStringQuandHidden'], $_SESSION['inputStringDatedebutHidden'], $_SESSION['inputStringDatefinHidden']) ;
                        $TotalRows = count($this->mdl_agenda->listeAgendaRecherche($session_iCategorieId, $session_iVilleId, $session_zMotCle, $session_iSousCategorieId, 0, 10000, $session_iOrderBy, $session_inputStringQuandHidden, $session_inputStringDatedebutHidden, $session_inputStringDatefinHidden, $session_inputIdCommercant, $user_ionauth_id));

                        $config_pagination = array();
                        $config_pagination["base_url"] = base_url() . "article/liste/" . $user_ionauth_id;
                        $config_pagination["total_rows"] = $TotalRows;
                        $config_pagination["per_page"] = $PerPage;
                        $config_pagination["uri_segment"] = 4;
                        $config_pagination['first_link'] = '<<<';
                        $config_pagination['last_link'] = '>>>';
                        $this->pagination->initialize($config_pagination);
                        $page_pagination = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                        //$toCommercant = $this->mdl_agenda->listeAgendaRecherche($session_iCategorieId, $_SESSION['iVilleId'], $_SESSION['zMotCle'], $data["iFavoris"], $page_pagination, $config_pagination["per_page"], $_SESSION['iOrderBy'], $_SESSION['inputStringQuandHidden'], $_SESSION['inputStringDatedebutHidden'], $_SESSION['inputStringDatefinHidden']) ;
                        $toAgenda = $this->mdl_agenda->listeAgendaRecherche($session_iCategorieId, $session_iVilleId, $session_zMotCle, $session_iSousCategorieId, $page_pagination, $config_pagination["per_page"], $session_iOrderBy, $session_inputStringQuandHidden, $session_inputStringDatedebutHidden, $session_inputStringDatefinHidden, $session_inputIdCommercant, $user_ionauth_id);
                        $data["links_pagination"] = $this->pagination->create_links();

                    } else {
                        $data["iFavoris"] = "";
                        $session_iCategorieId = $this->session->userdata('iCategorieId_x');
                        $session_iSousCategorieId = $this->session->userdata('iSousCategorieId_x');
                        $session_iVilleId = $this->session->userdata('iVilleId_x');
                        $session_zMotCle = $this->session->userdata('zMotCle_x');
                        $session_iOrderBy = $this->session->userdata('iOrderBy_x');
                        $session_inputStringQuandHidden = $this->session->userdata('inputStringQuandHidden_x');
                        $session_inputStringDatedebutHidden = $this->session->userdata('inputStringDatedebutHidden_x');
                        $session_inputStringDatefinHidden = $this->session->userdata('inputStringDatefinHidden_x');
                        $session_inputIdCommercant = $this->session->userdata('inputIdCommercant_x');

                        $iCategorieId = (isset($session_iCategorieId)) ? $session_iCategorieId : 0;
                        $iSousCategorieId = (isset($session_iSousCategorieId)) ? $session_iSousCategorieId : 0;
                        $iVilleId = (isset($session_iVilleId)) ? $session_iVilleId : "0";
                        $zMotCle = (isset($session_zMotCle)) ? $session_zMotCle : "";
                        $iOrderBy = (isset($session_iOrderBy)) ? $session_iOrderBy : "0";
                        $inputStringQuandHidden = (isset($session_inputStringQuandHidden)) ? $session_inputStringQuandHidden : "0";
                        $inputStringDatedebutHidden = (isset($session_inputStringDatedebutHidden)) ? $session_inputStringDatedebutHidden : "0000-00-00";
                        $inputStringDatefinHidden = (isset($session_inputStringDatefinHidden)) ? $session_inputStringDatefinHidden : "0000-00-00";
                        $inputIdCommercant = (isset($session_inputIdCommercant)) ? $session_inputIdCommercant : "0";

                        //$toAgenda = $this->mdl_agenda->listeAgendaRecherche($iCategorieId, $iVilleId, $zMotCle, $data["iFavoris"], $argOffset, $PerPage, $iOrderBy, $inputStringQuandHidden, $inputStringDatedebutHidden, $inputStringDatefinHidden) ;
                        $TotalRows = count($this->mdl_agenda->listeAgendaRecherche($iCategorieId, $iVilleId, $zMotCle, $iSousCategorieId, 0, 10000, $iOrderBy, $inputStringQuandHidden, $inputStringDatedebutHidden, $inputStringDatefinHidden, $inputIdCommercant, $user_ionauth_id));

                        $config_pagination = array();
                        $config_pagination["base_url"] = base_url() . "article/liste/" . $user_ionauth_id;
                        $config_pagination["total_rows"] = $TotalRows;
                        $config_pagination["per_page"] = $PerPage;
                        $config_pagination["uri_segment"] = 4;
                        $config_pagination['first_link'] = '<<<';
                        $config_pagination['last_link'] = '>>>';
                        $this->pagination->initialize($config_pagination);
                        $page_pagination = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                        $toAgenda = $this->mdl_agenda->listeAgendaRecherche($iCategorieId, $iVilleId, $zMotCle, $iSousCategorieId, $page_pagination, $config_pagination["per_page"], $iOrderBy, $inputStringQuandHidden, $inputStringDatedebutHidden, $inputStringDatefinHidden, $inputIdCommercant, $user_ionauth_id);
                        $data["links_pagination"] = $this->pagination->create_links();
                    }

                    ////$this->firephp->log($_SERVER['REQUEST_URI'], 'PATH_INFO');

                    $iNombreLiens = $TotalRows / $PerPage;
                    if ($iNombreLiens > round($iNombreLiens)) {
                        $iNombreLiens = round($iNombreLiens) + 1;
                    } else {
                        $iNombreLiens = round($iNombreLiens);
                    }
                    //////////////////////////////////

                    $data["iNombreLiens"] = $iNombreLiens;

                    $data["PerPage"] = $PerPage;
                    $data["TotalRows"] = $TotalRows;
                    $data["argOffset"] = $argOffset;
                    //$toCommercant= $this->mdlcommercantpagination->GetListeCommercantPagination($PerPage, $argOffset);
                    $data['toAgenda'] = $toAgenda;
                    // print_r($data['toCommercant']);exit();
                    //$this->load->view('front/vwAccueil', $data) ;
                    $data['pagecategory'] = 'article';


                    $is_mobile = $this->agent->is_mobile();
                    //test ipad user agent
                    $is_mobile_ipad = $this->agent->is_mobile('ipad');
                    $data['is_mobile_ipad'] = $is_mobile_ipad;
                    $is_robot = $this->agent->is_robot();
                    $is_browser = $this->agent->is_browser();
                    $is_platform = $this->agent->platform();
                    $data['is_mobile'] = $is_mobile;
                    $data['is_robot'] = $is_robot;
                    $data['is_browser'] = $is_browser;
                    $data['is_platform'] = $is_platform;


                    if ($this->ion_auth->logged_in()) {
                        $user_ion_auth = $this->ion_auth->user()->row();
                        $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                        if ($iduser == null || $iduser == 0 || $iduser == "") {
                            $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                        }
                        $data['IdUser'] = $iduser;
                    }


                    $this->load->view('agendaAout2013/mon_agenda', $data);

                }

            }

            function preview_festival($id_agenda)
            {

                if (!$this->ion_auth->logged_in()) {
                    redirect('front/utilisateur/no_permission');
                }

                $data['current_page'] = "details_festival";
                $toVille = $this->mdlville->GetArticleVilles_pvc();//get article list of agenda
                $data['toVille'] = $toVille;
                $toCategorie_principale = $this->mdlfestival->GetFestivalCategorie();
                $data['toCategorie_principale'] = $toCategorie_principale;
                //var_dump($toCategorie_principale);

                $id_agenda = $this->uri->rsegment(3);

                $oDetailAgenda = $this->mdlfestival->GetById_preview($id_agenda);
                $data['oDetailAgenda'] = $oDetailAgenda;

                //increment "accesscount" on agenda table
                $this->mdlfestival->Increment_accesscount($oDetailAgenda->id);

                //send info commercant to view
                $oInfoCommercant = $this->mdlcommercant->infoCommercant($oDetailAgenda->IdCommercant);
                $data['oInfoCommercant'] = $oInfoCommercant;

                $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);
                if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
                if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;
                $data['group_id_commercant_user'] = $group_id_commercant_user;


                //sending mail to event organiser **************************
                if (isset($_POST['text_mail_form_module_detailbonnplan'])) {
                    $text_mail_form_module_detailbonnplan = $this->input->post("text_mail_form_module_detailbonnplan");
                    $nom_mail_form_module_detailbonnplan = $this->input->post("nom_mail_form_module_detailbonnplan");
                    $tel_mail_form_module_detailbonnplan = $this->input->post("tel_mail_form_module_detailbonnplan");
                    $email_mail_form_module_detailbonnplan = $this->input->post("email_mail_form_module_detailbonnplan");

                    $colDestAdmin = array();
                    $colDestAdmin[] = array("Email" => $oDetailAgenda->email, "Name" => $oDetailAgenda->nom_manifestation . " " . $oDetailAgenda->nom_societe);

                    // Sujet
                    $txtSujetAdmin = "Demande d'information sur un évennement sur Agenda/Club";

                    $txtContenuAdmin = "
            <p>Bonjour ,</p>
            <p>Une demande d'information vous est adressé suite à un évennement que vous avez déposé sur le site Agenda.</p>
            <p>Détails :<br/>
            Evennement : " . $oDetailAgenda->nom_manifestation . "
            <br/>
            Date : " . translate_date_to_fr($oDetailAgenda->date_debut) . "<br/>
            Lieu : " . $oDetailAgenda->ville . " " . $oDetailAgenda->adresse_localisation . " " . $oDetailAgenda->codepostal_localisation . "<br/>
            Organisateur : " . $oDetailAgenda->organisateur . "<br/><br/>
            Demande Client :<br/>
            " . $text_mail_form_module_detailbonnplan . "<br/><br/>
            Nom client : " . $nom_mail_form_module_detailbonnplan . "<br/>
            Tel client : " . $tel_mail_form_module_detailbonnplan . "<br/>
            Email client : " . $email_mail_form_module_detailbonnplan . "<br/>
            </p>";

                    @envoi_notification($colDestAdmin, $txtSujetAdmin, $txtContenuAdmin);
                    $data['mssg_envoi_module_detail_bonplan'] = '<font color="#00CC00">Votre demande est envoyée</font>';
                    //$data['mssg_envoi_module_detail_bonplan'] = $txtContenuAdmin;

                } else $data['mssg_envoi_module_detail_bonplan'] = '';
                //sending mail to event organiser *******************************

                $is_mobile = $this->agent->is_mobile();
                //test ipad user agent
                $is_mobile_ipad = $this->agent->is_mobile('ipad');
                $data['is_mobile_ipad'] = $is_mobile_ipad;
                $is_robot = $this->agent->is_robot();
                $is_browser = $this->agent->is_browser();
                $is_platform = $this->agent->platform();
                $data['is_mobile'] = $is_mobile;
                $data['is_robot'] = $is_robot;
                $data['is_browser'] = $is_browser;
                $data['is_platform'] = $is_platform;

                $toDepartement = $this->mdldepartement->GetArticleDepartements_pvc();
                $data['toDepartement'] = $toDepartement;

                $data["pagecategory"] = "festival";
                $data["main_menu_content"] = "festival";
                $data["mdlbonplan"] = $this->mdlbonplan;

                $data["pagecategory_partner"] = "article_partner";


                if (isset($oDetailAgenda->id)) {
                    if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
                        //$this->load->view('privicarte_mobile/agenda_details_desk', $data);
                        $this->load->view('sortez_mobile/article_details_desk', $data);
                    } else {
                        //$this->load->view('privicarte/details_event', $data);
                        $this->load->view('sortez/details_article', $data);
                    }
                } else {
                    redirect('front/utilisateur/no_permission');
                }

            }


        }
        function details_festivals($id_festival)
        {

            $data['current_page'] = "details_festivals";
            $toVille = $this->mdlville->GetAgendaVilles_pvc();//get ville list of agenda
            $data['toVille'] = $toVille;
            $toCategorie_principale = $this->mdlfestival->GetFestivalCategorie();
            $data['toCategorie_principale'] = $toCategorie_principale;
            //var_dump($toCategorie_principale);

            $id_festival = $this->uri->rsegment(3); //die($id_agenda);
            if (isset($id_festival)) $id_festival = (intval($id_festival));
            else $id_festival = 0;

            $oDetailfestival = $this->mdlfestival->GetById_IsActif($id_festival);
            $data['oDetailAgenda'] = $oDetailfestival;//var_dump($oDetailAgenda); die();

            if (isset($oDetailfestival->id)) {

                //increment "accesscount" on agenda table
                $this->mdlfestival->Increment_accesscount($oDetailfestival->id);

                //send info commercant to view
                $oInfoCommercant = $this->mdlcommercant->infoCommercant($oDetailfestival->IdCommercant);
                $data['oInfoCommercant'] = $oInfoCommercant;


                //sending mail to event organiser **************************
                if (isset($_POST['text_mail_form_module_detailbonnplan'])) {
                    $text_mail_form_module_detailbonnplan = $this->input->post("text_mail_form_module_detailbonnplan");
                    $nom_mail_form_module_detailbonnplan = $this->input->post("nom_mail_form_module_detailbonnplan");
                    $tel_mail_form_module_detailbonnplan = $this->input->post("tel_mail_form_module_detailbonnplan");
                    $email_mail_form_module_detailbonnplan = $this->input->post("email_mail_form_module_detailbonnplan");

                    $colDestAdmin = array();
                    $colDestAdmin[] = array("Email" => $oDetailfestival->email, "Name" => $oDetailfestival->nom_manifestation . " " . $oDetailfestival->nom_societe);

                    // Sujet
                    $txtSujetAdmin = "Demande d'information sur un évennement sur Agenda/Club";

                    $txtContenuAdmin = "
            <p>Bonjour ,</p>
            <p>Une demande d'information vous est adressé suite à un évennement que vous avez déposé sur le site Agenda.</p>
            <p>Détails :<br/>
            Evennement : " . $oDetailfestival->nom_manifestation . "
            <br/>
            Date : " . translate_date_to_fr($oDetailfestival->date_debut) . "<br/>
            Lieu : " . $oDetailfestival->ville . " " . $oDetailfestival->adresse_localisation . " " . $oDetailfestival->codepostal_localisation . "<br/>
            Organisateur : " . $oDetailfestival->organisateur . "<br/><br/>
            Demande Client :<br/>
            " . $text_mail_form_module_detailbonnplan . "<br/><br/>
            Nom client : " . $nom_mail_form_module_detailbonnplan . "<br/>
            Tel client : " . $tel_mail_form_module_detailbonnplan . "<br/>
            Email client : " . $email_mail_form_module_detailbonnplan . "<br/>
            </p>";

                    @envoi_notification($colDestAdmin, $txtSujetAdmin, $txtContenuAdmin);
                    $data['mssg_envoi_module_detail_bonplan'] = '<font color="#00CC00">Votre demande est envoyée</font>';
                    //$data['mssg_envoi_module_detail_bonplan'] = $txtContenuAdmin;

                } else $data['mssg_envoi_module_detail_bonplan'] = '';
                //sending mail to event organiser *******************************

                $is_mobile = $this->agent->is_mobile();
                //test ipad user agent
                $is_mobile_ipad = $this->agent->is_mobile('ipad');
                $data['is_mobile_ipad'] = $is_mobile_ipad;
                $is_robot = $this->agent->is_robot();
                $is_browser = $this->agent->is_browser();
                $is_platform = $this->agent->platform();
                $data['is_mobile'] = $is_mobile;
                $data['is_robot'] = $is_robot;
                $data['is_browser'] = $is_browser;
                $data['is_platform'] = $is_platform;

                $toDepartement = $this->mdldepartement->GetAgendaDepartements_pvc();
                $data['toDepartement'] = $toDepartement;

                $data["pagecategory"] = "agenda";
                $data["main_menu_content"] = "agenda";
                $data["mdlbonplan"] = $this->mdlbonplan;

                $data["mdl_localisation"] = $this->mdl_localisation;
                $data["mdlville"] = $this->mdlville;
                $data["mdl_article_organiser"] = $this->mdl_festival_organiser;

                $data["pagecategory_partner"] = "agenda_partner";

                $data["oAgenda_associated"] = "";
                $data["agenda"]=$this->mdlfestival->get_agenda($id_festival);

                if($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL) {
                    $this->load->view('vivresaville/festival_details', $data);
                } else {
                    if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
                        //$this->load->view('mobile2013/details_event', $data);
                        //$this->load->view('privicarte_mobile/agenda_details_desk', $data);
                        $this->load->view('sortez_vsv/festival_details', $data);
                    } else {
                        //$this->load->view('privicarte/details_event', $data);
                        $this->load->view('sortez_vsv/festival_details', $data);
                    }
                }

            } else {
                redirect('front/utilisateur/no_permission');
            }

        }
        function check_category_list()
        {
            $inputStringQuandHidden_partenaires = $this->input->post("iQuand_sess");////$this->firephp->log($inputStringQuandHidden_partenaires, 'iQuand_sess');
            $inputStringDatedebutHidden_partenaires = $this->input->post("iDatedebut_sess");////$this->firephp->log($inputStringDatedebutHidden_partenaires, 'iDatedebut_sess');
            $inputStringDatefinHidden_partenaires = $this->input->post("iDatefin_sess");////$this->firephp->log($inputStringDatefinHidden_partenaires, 'iDatefin_sess');
            $inputStringVilleHidden_partenaires = $this->input->post("iVilleId_sess");////$this->firephp->log($inputStringVilleHidden_partenaires, 'iVilleId_sess');
            $inputStringDepartementHidden_partenaires = $this->input->post("iDepartementId_sess");////$this->firephp->log($inputStringDepartementHidden_partenaires, 'iDepartementId_sess');

            $toCategorie_principale = $this->mdlfestival->GetFestivalCategorie_by_params($inputStringQuandHidden_partenaires, $inputStringDatedebutHidden_partenaires, $inputStringDatefinHidden_partenaires, $inputStringDepartementHidden_partenaires, $inputStringVilleHidden_partenaires);
            //var_dump($toCategorie_principale);

            $result_to_show = '';

            $session_iCategorieId = $this->session->userdata('iCategorieId_x');
            if (isset($session_iCategorieId)) {
                $iCategorieId_sess = $session_iCategorieId;
            }

            $ii_rand = 0;

            if (isset($toCategorie_principale)) {
                foreach ($toCategorie_principale as $oCategorie_principale) {

                    if($_SERVER['SERVER_NAME'] == DOMAIN_SORTEZ_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_SORTEZ_GLOBAL)
                    {
                        /*if (isset($oCategorie_principale->nb_agenda) && $oCategorie_principale->nb_agenda != 0) {

                            $result_to_show .= '<div class="leftcontener2013title btn leftcontener2013title_vs" onClick="javascript:show_current_categ_subcateg(' . $oCategorie_principale->agenda_categid . ')">';
                            $result_to_show .= '
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="22">
                            <span id="span_check_main_part"><input  onClick="javascript:submit_search_agenda();" name="check_main_part[' . $ii_rand . ']" id="check_main_part_' . $oCategorie_principale->agenda_categid . '" type="checkbox" value="' . $oCategorie_principale->agenda_categid . '"';
                            if (isset($iCategorieId_sess) && is_array($iCategorieId_sess)) {
                                if (in_array($oCategorie_principale->agenda_categid, $iCategorieId_sess)) $result_to_show .= 'checked';
                            }
                            $result_to_show .= ' style="margin-top:2px;"></span>
                                </td>
                                <td>
                            ';
                            $result_to_show .= ucfirst(strtolower($oCategorie_principale->category)) . " (" . $oCategorie_principale->nb_agenda . ")";
                            $result_to_show .= '
                                </tr>
                            </table>
                            </div>';

                            $result_to_show .= '<div class="leftcontener2013content" id="leftcontener2013content_' . $oCategorie_principale->agenda_categid . '" style="margin-bottom:5px;">';

                            $OCommercantSousRubrique = $this->mdl_categories_agenda->GetAgendaSouscategorieByRubrique($oCategorie_principale->agenda_categid, $inputStringQuandHidden_partenaires, $inputStringDatedebutHidden_partenaires, $inputStringDatefinHidden_partenaires, $inputStringDepartementHidden_partenaires, $inputStringVilleHidden_partenaires);

                            //var_dump($this->mdl_categories_agenda->GetAgendaSouscategorieByRubrique($oCategorie_principale->agenda_categid, $inputStringQuandHidden_partenaires, $inputStringDatedebutHidden_partenaires, $inputStringDatefinHidden_partenaires, $inputStringDepartementHidden_partenaires, $inputStringVilleHidden_partenaires)); die();

                            //$result_to_show .= '<br/>';

                            $session_iSousCategorieId = $this->session->userdata('iSousCategorieId_x');
                            if (isset($session_iSousCategorieId)) {
                                $iSousCategorieId_sess = $session_iSousCategorieId;
                            }


                            if (count($OCommercantSousRubrique) > 0) {
                                $i_rand = 0;
                                foreach ($OCommercantSousRubrique as $ObjCommercantSousRubrique) {

                                    if (isset($ObjCommercantSousRubrique->nb_agenda) && $ObjCommercantSousRubrique->nb_agenda != 0) {
                                        $result_to_show .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>';
                                        $result_to_show .= '<td width="22"><span id="span_check_part"><input  onClick="javascript:submit_search_agenda();" name="check_part[' . $i_rand . ']" id="check_part_' . $ObjCommercantSousRubrique->agenda_subcategid . '" type="checkbox" value="' . $ObjCommercantSousRubrique->agenda_subcategid . '"';
                                        if (isset($iSousCategorieId_sess) && is_array($iSousCategorieId_sess)) {
                                            if (in_array($ObjCommercantSousRubrique->agenda_subcategid, $iSousCategorieId_sess)) $result_to_show .= 'checked';
                                        }
                                        $result_to_show .= '></span></td><td>';
                                        $result_to_show .= ucfirst(strtolower($ObjCommercantSousRubrique->subcateg)) . ' (' . $ObjCommercantSousRubrique->nb_agenda . ')</td>';
                                        $result_to_show .= '</tr></table>';
                                    }
                                    $i_rand++;
                                }
                            }
                            $result_to_show .= '</div>';
                        }*/
                        if (isset($oCategorie_principale->nb_festival) && $oCategorie_principale->nb_festival != 0) {
                            $data['empty'] = null;
                            //$data['oInfoCommercant'] = $oInfoCommercant ;
                            $data['oCategorie_principale'] = $oCategorie_principale;
                            $data['ii_rand'] = $ii_rand;
                            $data['iCategorieId_sess'] = $iCategorieId_sess;
                            $data['inputStringQuandHidden_partenaires'] = $inputStringQuandHidden_partenaires;
                            $data['inputStringDatedebutHidden_partenaires'] = $inputStringDatedebutHidden_partenaires;
                            $data['inputStringDatefinHidden_partenaires'] = $inputStringDatefinHidden_partenaires;
                            $data['inputStringDepartementHidden_partenaires'] = $inputStringDepartementHidden_partenaires;
                            $data['inputStringVilleHidden_partenaires'] = $inputStringVilleHidden_partenaires;
                            $data['mdl_categories_agenda'] = $this->mdl_categories_festival;
                            $data['session'] = $this->session;
                            $result_to_show .= $this->load->view('sortez_vsv/festival_check_category', $data, TRUE);
                        }
                    }
                    elseif($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL)
                    {
                        if (isset($oCategorie_principale->nb_festival) && $oCategorie_principale->nb_festival != 0) {
                            $data['empty'] = null;
                            //$data['oInfoCommercant'] = $oInfoCommercant ;
                            $data['oCategorie_principale'] = $oCategorie_principale;
                            $data['ii_rand'] = $ii_rand;
                            $data['iCategorieId_sess'] = $iCategorieId_sess;
                            $data['inputStringQuandHidden_partenaires'] = $inputStringQuandHidden_partenaires;
                            $data['inputStringDatedebutHidden_partenaires'] = $inputStringDatedebutHidden_partenaires;
                            $data['inputStringDatefinHidden_partenaires'] = $inputStringDatefinHidden_partenaires;
                            $data['inputStringDepartementHidden_partenaires'] = $inputStringDepartementHidden_partenaires;
                            $data['inputStringVilleHidden_partenaires'] = $inputStringVilleHidden_partenaires;
                            $data['mdl_categories_agenda'] = $this->mdl_categories_festival;
                            $data['session'] = $this->session;
                            $result_to_show .= $this->load->view('vivresaville/festival_check_category', $data, TRUE);
                        }
                    }
                    $ii_rand++;
                }
            }

            ////$this->firephp->log($result_to_show, 'result_to_show');
            echo $result_to_show;
            //echo mb_convert_encoding($result_to_show, "UTF-8");

        }

        function festival_perso()
        {

            ////$this->firephp->log($_REQUEST, '_REQUEST');

            $zCouleur = $this->input->get("zCouleur");
            if (!isset($zCouleur)) $zCouleur = $this->input->post("zCouleur");
            if (isset($zCouleur) && $zCouleur != "") $data['zCouleur'] = $zCouleur;
            $zCouleurTitre = $this->input->get("zCouleurTitre");
            if (!isset($zCouleurTitre)) $zCouleurTitre = $this->input->post("zCouleurTitre");
            if (isset($zCouleurTitre) && $zCouleurTitre != "") $data['zCouleurTitre'] = $zCouleurTitre;
            $zCouleurTextBouton = $this->input->get("zCouleurTextBouton");
            if (!isset($zCouleurTextBouton)) $zCouleurTextBouton = $this->input->post("zCouleurTextBouton");
            if (isset($zCouleurTextBouton) && $zCouleurTextBouton != "") $data['zCouleurTextBouton'] = $zCouleurTextBouton;
            $zCouleurBgBouton = $this->input->get("zCouleurBgBouton");
            if (!isset($zCouleurBgBouton)) $zCouleurBgBouton = $this->input->post("zCouleurBgBouton");
            if (isset($zCouleurBgBouton) && $zCouleurBgBouton != "") $data['zCouleurBgBouton'] = $zCouleurBgBouton;
            $zCouleurNbBtn = $this->input->get("zCouleurNbBtn");
            if (!isset($zCouleurNbBtn)) $zCouleurNbBtn = $this->input->post("zCouleurNbBtn");
            if (isset($zCouleurNbBtn) && $zCouleurNbBtn != "") $data['zCouleurNbBtn'] = $zCouleurNbBtn;

            $tiDepartement = $this->input->get("tiDepartement");
            if (!isset($tiDepartement)) $tiDepartement = $this->input->post("tiDepartement");
            if (isset($tiDepartement) && $tiDepartement != "") {
                $tiDepartement = substr_replace($tiDepartement, "", -1);
                $tiDepartement_array = explode("_", $tiDepartement);
                $data['tiDepartement_array'] = $tiDepartement;
            } else {
                $tiDepartement_array = "0";
            }
            $tiDeposant = $this->input->get("tiDeposant");
            if (!isset($tiDeposant)) $tiDeposant = $this->input->post("tiDeposant");
            if (isset($tiDeposant) && $tiDeposant != "") {
                $tiDeposant = substr_replace($tiDeposant, "", -1);
                $tiDeposant_array = explode("_", $tiDeposant);
                $data['tiDeposant_array'] = $tiDeposant;
            }
            $tiCategorie = $this->input->get("tiCategorie");
            if (!isset($tiCategorie)) $tiCategorie = $this->input->post("tiCategorie");
            if (isset($tiCategorie) && $tiCategorie != "") {
                $tiCategorie = substr_replace($tiCategorie, "", -1);
                $data["tiCategorie_for_init"] = $tiCategorie;
                $tiCategorie_array = explode("_", $tiCategorie);
                //$data['tiCategorie_array'] = $tiCategorie_array;
            } else {
                $tiCategorie_array = "0";
            }
            $tiDossperso = $this->input->get("tiDossperso");
            if (!isset($tiDossperso)) $tiDossperso = $this->input->post("tiDossperso");
            $data["tiDossperso"] = $tiDossperso;
            if (isset($tiDossperso) && $tiDossperso != "0") {
                $idCommercant_to_search = $tiDossperso;
            } else if (isset($tiDeposant_array)) {
                $idCommercant_to_search = $tiDeposant_array;
            } else {
                $idCommercant_to_search = "0";
            }

            $contentonly = $this->input->get("contentonly");
            if (!isset($contentonly)) $contentonly = $this->input->post("contentonly");
            $data["contentonly"] = $contentonly;

            $proximite_tiDatefilter = $this->input->get("proximite_tiDatefilter");
            if (!isset($proximite_tiDatefilter)) $proximite_tiDatefilter = $this->input->post("proximite_tiDatefilter");
            if (!isset($proximite_tiDatefilter)) $proximite_tiDatefilter = "0";
            $data["proximite_tiDatefilter"] = $proximite_tiDatefilter;

            $keyword_input_export = $this->input->get("keyword_input_export");
            if (!isset($keyword_input_export)) $keyword_input_export = $this->input->post("keyword_input_export");
            $data["keyword_input_export"] = $keyword_input_export;

            //var_dump($data); die();


            $toAgenda = $this->mdlfestival->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", $proximite_tiDatefilter, "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0);
            $data['toAgenda'] = $toAgenda;
//var_dump($toAgenda);die();

            //$data['toAgndaTout_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
            $data['toAgndaAujourdhui_global'] = count($this->mdlfestival->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "101", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
            $data['toAgndaWeekend_global'] = count($this->mdlfestival->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "202", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
            $data['toAgndaSemaine_global'] = count($this->mdlfestival->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "303", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
            $data['toAgndaSemproch_global'] = count($this->mdlfestival->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "404", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
            $data['toAgndaMois_global'] = count($this->mdlfestival->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "505", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
            /*
                    $data['toAgndaJanvier_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "01", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                    $data['toAgndaFevrier_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "02", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                    $data['toAgndaMars_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "03", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                    $data['toAgndaAvril_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "04", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                    $data['toAgndaMai_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "05", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                    $data['toAgndaJuin_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "06", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                    $data['toAgndaJuillet_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "07", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                    $data['toAgndaAout_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "08", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                    $data['toAgndaSept_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "09", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                    $data['toAgndaOct_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "10", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                    $data['toAgndaNov_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "11", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                    $data['toAgndaDec_global'] = count($this->mdl_agenda->listeAgendaRecherche_filtre_agenda_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "12", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));
                    */

            $tiCategorie_list_array = $this->mdlfestival->listeAgendaRecherche_agenda_perso_liste_categ($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0);
            //var_dump($tiCategorie_list_array);die();
            $data['tiCategorie_list_array'] = $tiCategorie_list_array;

            $data['defaul_thumb_width'] = $this->config->item('defaul_thumb_width');
            $data['defaul_thumb_height'] = $this->config->item('defaul_thumb_height');
            $data['currentpage'] = "export_festival";
            //$this->load->view('agendaAout2013/agenda_perso', $data);
            //$this->load->view('privicarte/agenda_perso', $data);
            $this->load->view('export/festival_perso_respo', $data);

        }



    }