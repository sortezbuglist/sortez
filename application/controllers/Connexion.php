<?php
class connexion extends CI_Controller{
    
    function __construct() {
        parent::__construct();
		$this->load->model("user") ;
                
        $this->load->library('session');
        $this->load->library('user_agent');

        $this->load->library('ion_auth');

		check_vivresaville_id_ville();

    }
    
    function index(){
        redirect("auth/login");
    }

    public function entrer() {
        redirect("auth/login");
    }

    
    public function sortir() {

        $this->ion_auth->logout();

        redirect("auth/login");
    }

    public function motdepasseoublie() {
        redirect("auth/forgot_password");

        $data["no_main_menu_home"] = "1";
        $this->load->view("vwMotDePasseOublie_2013", $data);
    }
	public function verificationemail(){
		 $zUtilisateurEmail = $_POST['txtEmail'] ;
		 $toUtilisateur = $this->user->GetWhere("users.Email = '" . $zUtilisateurEmail . "'") ;
		 if(sizeof($toUtilisateur)>0){
			$zMotDePasse = $toUtilisateur[0]->Password ;
			$config = array(); 
			$config[ 'protocol' ] = 'mail' ;
			$config[ 'mailtype' ] = 'html' ;
			$config[ 'charset' ] = 'utf-8' ;
			$this->load->library('email', $config);
			
			$errorMail    = "";
			
			$mail_to      = $toUtilisateur[0]->Email ;
                        //$mail_cc = "volatafita@gmail.com"  ;
                        $mail_cc = ""  ;
			$mail_to_name = $toUtilisateur[0]->Nom . " " . $toUtilisateur[0]->Prenom;
			$mail_subject = "[Club-proximite] Votre mot de passe" ;
			$mail_expediteur = $this->config->item("mail_sender_adress") ;
			$data = array() ;
			$data["zLogin"] = $toUtilisateur[0]->Login ;
			$data["zMotDePasse"] = $zMotDePasse ;
			   
			$mail_body = $this->load->view("front/vwContenuMailMotDePasseOublie", $data, true) ; 
			//$mail_body = "" ; 

			$this->email->from($mail_expediteur, "Club proximité");
			$this->email->to($mail_to);
			//$this->email->cc($mail_cc);
			$this->email->subject($mail_subject);
			$this->email->message($mail_body);
			
			if ($this->email->send()) {
				$data["ErrorMessage"] = "Votre mot de passe a &eacute;t&eacute; envoy&eacute;" ;
				$this->load->view("vwMotDePasseOublie", $data);
			} else {
				echo "tsy poinsa" ;
			}
		 }
		 else{
			$data["ErrorMessage"] = "Cet utilisateur n&rsquo;existe pas!" ;
			$this->load->view("vwMotDePasseOublie", $data);
		 }
	}
}