<?php
class mon_compte extends CI_Controller{
    
    function __construct() {
        parent::__construct();
		$this->load->model("user") ;
                
        $this->load->library('session');
        $this->load->library('user_agent');

        $this->load->library('ion_auth');

        check_vivresaville_id_ville();

    }
    
    function index(){
        $data["ztitle"] = "Mon compte";
        $this->load->view("privicarte/mon_compte", $data);
    }

    
}