<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Livreor extends CI_Controller
{
	const NB_COMMENTAIRE_PAR_PAGE = 100;
	
	public function __construct()
	{
		parent:: __Construct();
		
		//	Chargement des ressources pour tout le contrôleur
		$this->load->database();
		$this->load->helper(array('url', 'assets'));
		$this->load->model('livreor_model', 'livreorManager');
		$this->load->model("mdlcommercant") ;
		
        $this->load->model("mdlville") ;
        $this->load->model("mdlbonplan") ;
        $this->load->model("mdlbonplans") ;
        $this->load->model("mdlcadeau") ;

        $this->load->model("mdlannonce") ;
        $this->load->model("rubrique") ;
        $this->load->model("mdlimagespub") ;

        $this->load->model("Abonnement");
        $this->load->library('user_agent');

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");

        $this->load->model("Mdl_plat_du_jour");

        $this->load->library('session');
        $this->load->helper('clubproximite');


        check_vivresaville_id_ville();
	}
	
// ------------------------------------------------------------------------
	
	public function index($Idcommercant,$g_nb_commentaire = 1,$livre = 1)
	{
		$this->voir($Idcommercant,$g_nb_commentaire,$livre);
	}
	
// ------------------------------------------------------------------------
	
	public function voir($Idcommercant,$g_nb_commentaire = 1)
	{
		$nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);
        
		$this->load->library('pagination');
		
		$data = array();
		
		//	Récupération du nombre total de messages sauvegardés dans la base de données
		$nb_commentaire = $this->livreorManager->count($Idcommercant);

		$nb_commentaire_total = count($nb_commentaire);
		//	On vérifie la cohérence de la variable $_GET
		if($g_nb_commentaire > 1)
		{
			//	La variable $_GET semblent être correcte. On doit maintenant
			//	vérifier s'il y a bien assez de commentaires dans la base de données.
			if($g_nb_commentaire <= $nb_commentaire_total)
			{
				//	Il y a assez de commentaires dans la base de données.
				//	La variable $_GET est donc cohérente.
				
				$nb_commentaire = intval($g_nb_commentaire);
			}
			else
			{
				//	Il n'y pas assez de messages dans la base de données.
				
				$nb_commentaire = 1;
			}
		}
		else
		{
			//	La variable $_GET "nb_commentaire" est erronée. On lui donne une
			//	valeur par défaut.
			
			$nb_commentaire = 1;
		}
		
		//	Mise en place de la pagination
		$this->pagination->initialize(array('base_url' => base_url() . 'index.php/livreor/voir/',
						    'total_rows' => $nb_commentaire_total,
						    'per_page' => self::NB_COMMENTAIRE_PAR_PAGE)); 
		
		$data['pagination'] = $this->pagination->create_links();
		$data['nb_commentaires'] = $nb_commentaire_total;
		
		//	Maintenant que l'on connaît le numéro du commentaire, on peut lancer
		//	la requête récupérant les commentaires dans la base de données.
		$Avoir = $this->mdlcommercant->infoCommercant($Idcommercant);
		$data['messages'] = $this->livreorManager->get_commentaires(self::NB_COMMENTAIRE_PAR_PAGE, $nb_commentaire-1,$Idcommercant);
		$data['oInfoCommercant']=$Avoir;
		$data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($Idcommercant);
		$this->load->model("user") ;
        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser=0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));
		//	On charge la vue
		$this->load->view('afficher_commentaires', $data);
	}
	
// ------------------------------------------------------------------------
	
	public function ecrire($Idcommercant)
	{
		// $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        // $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);
        $Avoir = $this->mdlcommercant->infoCommercant($Idcommercant);
        $data['oInfoCommercant']=$Avoir;
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($Idcommercant);
   		$this->load->model("user") ;
        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser=0;
	
		//	Cette méthode permet de changer les délimiteurs par défaut des messages d'erreur (<p></p>).
		
		//	Mise en place des règles de validation du formulaire
		//	Nombre de caractères : [3,25] pour le pseudo et [3,3000] pour le commentaire
		//	Uniquement des caractères alphanumériques, des tirets et des underscores pour le pseudo
			//	Nous disposons d'un pseudo et d'un commentaire sous une bonne forme
			
			//	Sauvegarde du commentaire dans la base de données
			
			
			//	Affichage de la confirmation
			$this->load->view('ecrire_commentaire',$data);
	}
	public function save(){

		$Id_commercant=$this->input->post('Id_commercant');
		$Nom=$this->input->post('Nom');
		$mail=$this->input->post('mail');
		$mobile=$this->input->post('mobile');
		$message=$this->input->post('message');
		$date=$this->input->post('date');
		$vote=$this->input->post('vote');
		$data =array(
			'idcommercant'=>$Id_commercant,
			'nom' =>$Nom , 
			'mail'=>$mail ,
			'mobile'=>$mobile,
			'message'=>$message,
			'date'=>$date,
			'vote'=>$vote);
			
		$tout=$this->livreorManager->ajouter_commentaire($data);
		if ($tout==1){
        echo 'ok';
    	}else{
        echo 'ko';
    	}		
	}

	
}


/* End of file livreor.php */
/* Location: ./application/controllers/livreor.php */