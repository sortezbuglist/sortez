<?php class Commander extends CI_Controller

{

function __construct()

{

    parent::__construct();

    $this->load->model("mdlannonce");

    $this->load->model("User");

    $this->load->model("mdlcommercant");

    $this->load->model("mdlbonplan");

    $this->load->model("rubrique");

    $this->load->model("Sousrubrique");

    $this->load->model("mdlimagespub");

    $this->load->model("AssCommercantAbonnement");

    $this->load->model("mdlville");

    $this->load->model("mdldepartement");

    $this->load->model("mdlfidelity");

    $this->load->model("Mdl_card");

    $this->load->library('user_agent');

    $this->load->model("Abonnement");

    $this->load->model("commercant");

    $this->load->Model("mdlannonce_perso");

    $this->load->model("mdlarticle");

    $this->load->model("Mdlcommercant");

    $this->load->model("mdl_agenda");

    $this->load->model("mdlcommune");

    $this->load->model("Mdl_soutenons");

    $this->load->model("mdl_categories_annonce");

    $this->load->model("Mdl_plat_du_jour");

    $this->load->library('session');

    $this->load->Model("mdl_localisation");

    $this->load->library("pagination");

    $this->load->library("ion_auth");

    $this->load->library('image_moo');

    $this->load->model("mdlagenda_perso");
    $this->load->model("mdl_categories_agenda");
    $this->load->model("ion_auth_used_by_club");
    $this->load->helper('clubproximite');
}

    public function index($idcom ="301299",$getdata_comm ="soutenons"){

        $infocom = $this->mdlcommercant->infoCommercant($idcom);
        $data_commande = $this->Mdl_soutenons->get_com_data_by_idcom($idcom);
        $data['infocom'] = $infocom;
        $data['data_gli1'] = $this->Mdl_soutenons->getdatagli1($idcom);
        $data['data_gli2'] = $this->Mdl_soutenons->getdatagli2($idcom);
        $data['data_gli3'] = $this->Mdl_soutenons->getdatagli3($idcom);
        $data['data_gli4'] = $this->Mdl_soutenons->getdatagli4($idcom);
        $data['data_gli6'] = $this->Mdl_soutenons->getdatagli6($idcom);
        $data['data_gli7'] = $this->Mdl_soutenons->getdatagli7($idcom);
        $data['datacommande'] = $data_commande;
        $data['title_gli1'] = $this->Mdl_soutenons->gettitlegli1($idcom);
        $data['title_gli2'] = $this->Mdl_soutenons->gettitlegli2($idcom);
        $data['title_gli3'] = $this->Mdl_soutenons->gettitlegli3($idcom);
        $data['title_gli4'] = $this->Mdl_soutenons->gettitlegli4($idcom);
        $data['title_gli6'] = $this->Mdl_soutenons->gettitlegli6($idcom);
        $data['title_gli7'] = $this->Mdl_soutenons->gettitlegli7($idcom);
        $data['input'] =  $getdata_comm;

        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            //var_dump($user_ion_auth);die('data users');
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            $data['IdUser'] = $iduser;
            $card = $this->Mdl_card->getByIdUser($iduser);
            $data['card'] = $card;
            $res = $this->Mdl_soutenons->getuser_by_id_card($card->num_id_card_virtual);
            $data['res'] = $res;
        }

        $this->load->view('sortez_soutenons/front/commander_index',$data);

    }

    public function valid_command(){
        $allprod_brut = $this->input->post('allprod');
        $Idcom = $this->input->post('idCom');
        $idclient = $this->input->post('idclient');
        $comment = $this->input->post('comment');
        $type_payement = $this->input->post('type_payement');

        $type_livraison = $this->input->post('type');
        $type_facture_differe = $this->input->post('type_facture_differe');
        $jour_enlev = $this->input->post('jour_enlev');
        $heure_enlev = $this->input->post('heure_emporter');
        $infoclient = $this->Mdl_soutenons->getuser_by_id($idclient);
        $oInfoCommercant = $this->mdlcommercant->infoCommercant($Idcom) ;
        $data['oInfoCommercant'] = $oInfoCommercant ;
        $datacommande_com = $this->Mdl_soutenons->get_com_data_by_idcom($Idcom);
        $data['infoclient'] = $infoclient;
        $allprod_brut = json_decode($allprod_brut);
        $data['to_view'] = [];
        $data['categs'] = [];
        $data['comment'] = $comment;
        $data['type_livraison'] = $type_livraison;
        $data['jour_enlev'] = $jour_enlev;
        $data['heure_enlev'] = $heure_enlev;
        $data['type_payement'] = $type_payement;
        $data['type_facture_differe'] = $type_facture_differe;
        $i=0;
        $data['total_prix'] = 0;
        foreach ($allprod_brut as $posted_pro) {
            $info_prod = $this->Mdl_soutenons->get_product_by_id($posted_pro->id_produit, $posted_pro->id_glissiere);
            $nb = $posted_pro->nbre;
            $data['total_prix'] = $data['total_prix']+($nb*$info_prod->prix);
        }
        if (isset($data['total_prix']) AND $data['total_prix'] !=0){
            if (isset($datacommande_com->is_activ_prom) AND $datacommande_com->is_activ_prom == '1' AND $datacommande_com->price_to_win !=null AND $datacommande_com->price_to_reach !=null  ){
                if (floatval($data['total_prix']) > floatval($datacommande_com->price_to_reach)){
                    $data['total_prix'] = floatval($data['total_prix']) - floatval($datacommande_com->price_to_win);
                    $data['remise'] = $datacommande_com->price_to_win;
                }
            }
        }
        $to_list_command = array(
            "idCom" => $Idcom,
            "comment" => $comment,
            "id_client" => $idclient,
            "total_price" => $data['total_prix'],
            "type_livraison" => $type_livraison,
            "jour_enlev" => $jour_enlev,
            "heure_enleve" => $heure_enlev,
            "type_payement" => $type_payement,
            "type_command" => "Commande avec ".$type_livraison,
            "remise_promotion_price" => floatval($data['remise']) ?? 0,
        );
        $id_command_list = $this->Mdl_soutenons->save_command_list($to_list_command);
        foreach ($allprod_brut as $posted_pro){
            $info_prod = $this->Mdl_soutenons->get_product_by_id($posted_pro->id_produit,$posted_pro->id_glissiere);
//            var_dump($info_prod);
            if (count($info_prod) !=0){
                $to_push = array(
                    "id_product" => $info_prod->id,
                    "product_name" => $info_prod->true_title,
                    "product_price" => $info_prod->prix,
                    "categ_name" => $info_prod->titre_glissiere,
                    "idclient" => $idclient,
                    "idCom" => $Idcom,
                    'nbre' =>$posted_pro->nbre,
                );

                if (!in_array($info_prod->titre_glissiere,$data['categs'])){
                    array_push($data['categs'],$info_prod->titre_glissiere);
                }

                $data['to_view'][$i] = $to_push;
                $to_details_command = array(
                    "id_commande" => $id_command_list,
                    "id_produit" => $info_prod->id,
                    "nbre" => $posted_pro->nbre,
                    "type_product_id" => $info_prod->glissiere_ids,
                    "idcom" => $Idcom,
                    "idclient" =>$idclient
                );
                $this->Mdl_soutenons->save_detail_command($to_details_command);
            }
            $i++;
        }
        $infcom = $this->mdlcommercant->infoCommercant($Idcom);
        $infoclient = $this->User->getById_user($idclient);
        $txtSujet = "« Commande livraison à domicile ou à emporter »";
        $arrDest = array();
        $data['infoclient'] = $infoclient;
        $arrDest[] = array("Email" => $infcom->Email ?? '', "Name" => $infcom->NomSociete ?? '');
        $arrDest_cli[] = array("Email" => $infoclient->Login ?? '', "Name" => $infoclient->Nom ?? '');
        $data['com'] = '1';
        $card = $this->Mdl_card->getByIdUser($idclient);
        $data['card'] = $card;
        @envoi_notification($arrDest_cli,"Validation de votre commande",$this->load->view("sortez_soutenons/front/commander_mail",$data,TRUE));
        $data['com'] = '0';
        @envoi_notification($arrDest,$txtSujet,$this->load->view("sortez_soutenons/front/commander_mail",$data,TRUE));
        echo 'ok';
    }

    function ajouter_compte_part() {

        $nom_client = $this->input->post("nom_client");
        $prenom_client = $this->input->post("nom_client");
        $code_postal_client = $this->input->post('code_postal_client');
        $adresse_client = $this->input->post('adresse_client');
        $ville_client = $this->input->post('ville_client');
        $telephone_client = $this->input->post('telephone_client');
        $date_naissance_client = $this->input->post('date_naissance_client');
        $mail_client = $this->input->post('mail_client');
        $passwords = $this->input->post('password');
        $ville_data = $this->Mdl_soutenons->getVilleByNomSimple($ville_client);

        $objUser['Nom'] = $nom_client  ;
        $objUser['Prenom'] = $prenom_client ;
        $objUser['CodePostal'] = $code_postal_client ;
        $objUser['Adresse'] = $adresse_client ;
        $objUser['Portable'] = $telephone_client ;
        $objUser['DateNaissance'] =$date_naissance_client  ;
        $objUser['Email'] = $mail_client ;
        $objUser['Login'] = $mail_client ;
        $objUser['IdVille'] = $ville_data ?? "Nice" ;

        if (!$objUser) {
            redirect("front/particuliers/inscription");
        }

        $particulier_Password = $passwords;
        $from_menuconsommateurs = '0';


        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;


        $Rappeldevosinformations = "";
        $Rappeldevosinformations .= "<p>Rappel de vos informations</p>";
        $Rappeldevosinformations .= "<p>";
        $Rappeldevosinformations .= "Nom : ".$nom_client."<br/>";
        $Rappeldevosinformations .= "DateNaissance : ".$date_naissance_client."<br/>";
        $Rappeldevosinformations .= "Civilite : Mr/Mme <br/>";
        $Rappeldevosinformations .= "Adresse : ".$adresse_client."<br/>";
        $Rappeldevosinformations .= "Ville : ".$ville_client."<br/>";
        $Rappeldevosinformations .= "CodePostal : ".$code_postal_client."<br/>";
        $Rappeldevosinformations .= "Telephone : ".$telephone_client."<br/>";
        $Rappeldevosinformations .= "Mobile : ".$telephone_client."<br/>";
        $Rappeldevosinformations .= "Email : ".$mail_client."<br/>";
        $Rappeldevosinformations .= "Login : ".$mail_client."<br/>";
        $Rappeldevosinformations .= "</p>";

        $this->load->Model("user");
        $objUser["IsActif"] = "1";

        if($date_naissance_client!="") $date_naissance_client = convert_Frenchdate_to_Sqldate($date_naissance_client);
        else $date_naissance_client = null;
        if ($date_naissance_client == "--") $date_naissance_client = date("Y-m-d");


            $objUser["DateCreation"] = date("Y-m-d");

            //ion_auth register
            $username_ion_auth = $mail_client;
            ////$this->firephp->log($username_ion_auth, 'username_ion_auth');
            $password_ion_auth = $particulier_Password;
            ////$this->firephp->log($particulier_Password, 'particulier_Password');
            $email_ion_auth = $mail_client;
            ////$this->firephp->log($email_ion_auth, 'email_ion_auth');
            $additional_data_ion_auth = array('first_name' => $nom_client,'last_name' => $prenom_client, 'phone' => $telephone_client);
            $group_ion_auth = array('2'); // Sets user to particulier.
            $this->load->library('ion_auth');
            $user_part_ion = $this->ion_auth->register($username_ion_auth, $password_ion_auth, $email_ion_auth, $additional_data_ion_auth, $group_ion_auth);
            $ion_ath_error = $this->ion_auth->errors();
            $user_lastid_ionauth = $this->ion_auth_used_by_club->GetUserlast_id($nom_client, $username_ion_auth, $email_ion_auth);
            $objUser["user_ionauth_id"] = $user_lastid_ionauth;
            ////$this->firephp->log($user_lastid_ionauth, 'user_lastid_ionauth');
            //echo "qsf : ".$user_lastid_ionauth;
            //ion_auth register

            //var_dump($ion_ath_error); die();


            if (!$user_part_ion && isset($ion_ath_error) && $ion_ath_error!="") {
                $verifuserprofil = 2;
            } else {
                $objUser_id = $this->user->Insert($objUser);
                $objUser=$this->user->GetById($objUser_id);
                $id_ion_auth=$this->user->getbyids($objUser_id);
                $verifuserprofil = 0;
                if (isset($id_ion_auth->user_ionauth_id) AND $id_ion_auth->user_ionauth_id !=''  AND $id_ion_auth->user_ionauth_id!=null){
                    //var_dump($id_ion_auth->user_ionauth_id);die();
                    generate_client_card($id_ion_auth->user_ionauth_id);
                }
            }

        if (!is_object($objUser)) $objUser = (object)$objUser;


        $this->load->model("parametre");

        $objParametreContenuMail = $this->parametre->getByCode("ContenuMailConfirmationInscriptionUser");
        $objParametreContenuPage = $this->parametre->getByCode("ContenuPageConfirmationInscriptionUser");
        $arrDest = array();
        $arrDest[] = array("Email" => $mail_client ?? '', "Name" => $prenom_client ?? '' . " " .  $nom_client ?? '');

        $colDest[] = array("Email"=>$this->config->item("mail_sender_adress"),"Name"=>$this->config->item("mail_sender_name"));
        $colSujet = "Notification inscription Consommateur";
        $nom__ = $objUser->Nom;
        $Prenom__ = $objUser->Prenom;
        $Email__ = $objUser->Email;
        $colContenu = '<div>
            <p>Bonjour,<br/>Ceci est une notification d\'inscription de nouveau consommateur sur Sortez.org</p>
            <p>Nom: '.$nom__.'<br/>Prenom: '.$Prenom__.'<br/>Email: '.$Email__.'</p>
        </div>';

        $txtSujet = "Confirmation inscription";
        $txtContenu = $objParametreContenuMail[0]->Contenu;
        $txtContenu .= $Rappeldevosinformations;


        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
        } else $iduser = 0;
        if ($verifuserprofil == 0) {

            @envoi_notification($arrDest,$txtSujet,$txtContenu);
            @envoi_notification($colDest,$colSujet,$colContenu);
            $data["txtContenu"] = $objParametreContenuPage[0]->Contenu;
            echo $objUser_id;
        } else if($verifuserprofil == 2){
            echo "already";
        } else {
           echo $iduser;
        }

    }

    function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    public function check_user(){
        $num_card = $this->input->post("nom_card");
        $res = $this->Mdl_soutenons->getuser_by_id_card($num_card);
        $iduser_io_auth = $res->id_ionauth_user;
        $users_ionauth = $this->Mdl_soutenons->get_data_ion_users($iduser_io_auth);
        $username = $users_ionauth->username;
        $password = $users_ionauth->password;
        //var_dump($username);
        //var_dump($password);die('pass');
        if($this->ion_auth->login_commande($username, $password)){
            if(count($res) !=0){
                echo json_encode($res);
            }
        }else{
            echo "ko";
        }

    }
    public function save_command_specific_file(){
        $id_client = $this->input->post("id_client");
        $infoclient = $this->Mdl_soutenons->getuser_by_id($id_client);
        $card = $this->Mdl_card->getByIdUser($id_client);
        $comment_livr_enlev = $this->input->post("comment_livr_enlev");
        $type_livraison = $this->input->post("type_livraison");
        $jour_souhaite = $this->input->post("jour_souhaite");
        $heure_souhaite = $this->input->post("heure_souhaite");
        $type_paiement = $this->input->post("type_paiement");
        $id_com = $this->input->post("id_com");
        $infocom = $this->mdlcommercant->infoCommercant($id_com);

        if (dir("application/resources/front/command_clients/") == false){
            mkdir("application/resources/front/command_clients/");
        }
        if (dir("application/resources/front/command_clients/".$id_client."/") == false){
            mkdir("application/resources/front/command_clients/".$id_client."/");
        }
        $config['upload_path'] = "application/resources/front/command_clients/".$id_client."/";
        $config['allowed_types'] = 'pdf|docx|doc|txt|csv|docx|xlsx';
        $config['max_size']      = 5000;
        $this->load->library('upload',$config);
        if(isset($_FILES['command_file']['name']) && $_FILES['command_file']['name'] !=""){
            if ( ! $this->upload->do_upload('command_file'))
            {
                die($this->upload->display_errors());
            }
            else
            {
                $img_name = $this->upload->data()['file_name'];
                $field = array(
                    "idCom" =>$id_com,
                    "comment" => $comment_livr_enlev,
                    "id_client" => $id_client,
                    "type_livraison" =>$type_livraison,
                    "jour_enlev" => $jour_souhaite,
                    "heure_enleve" => $heure_souhaite,
                    "type_payement" => $type_paiement,
                    "command_files" => $img_name,
                    "type_command" => "TELECHARGEMENT D'UNE COMMANDE OU DOCUMENT ET ENVOI",
                );
                $data['com'] = '0';
                $data['img_file'] = $img_name;
                $data['comment_clinet'] = $comment_livr_enlev;
                $data['infoclient'] = $infoclient;
                $data['card'] = $card;
                $data['jour_enlev'] = $jour_souhaite;
                $data['type_payement'] = $type_paiement;
                $data['heure_enlev'] = $heure_souhaite;
                $data['type_livraison'] = $type_livraison;
                $data['type_commande'] = 'spec';
                $this->Mdl_soutenons->save_command_list($field);
                $colSujet = "« soutenonslecommercelocal.fr »Notification de commande";
                $arrDest[] = array("Email" => $infocom->Email ?? '', "Name" => $infocom->NomSociete ?? '');
                $arrDest2[] = array("Email" => $infoclient->Login ?? '', "Name" => $infoclient->Nom." ".$infoclient->Prenom ?? '');
//                $this->load->view('sortez_soutenons/front/commander_mail',$data);
               @envoi_notification($arrDest,$colSujet,$this->load->view('sortez_soutenons/front/commander_mail',$data,TRUE));
                $data['com'] = '1';
               @envoi_notification($arrDest2,$colSujet,$this->load->view('sortez_soutenons/front/commander_mail',$data,TRUE));
                echo "ok";
            }
        }
    }
}
