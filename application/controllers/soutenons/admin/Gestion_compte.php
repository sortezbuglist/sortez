<?php

    class Gestion_compte extends CI_Controller

    {

    function __construct()

    {

    parent::__construct();

    $this->load->model("mdlannonce");

    $this->load->model("mdlcommercant");

    $this->load->model("mdlbonplan");

    $this->load->model("rubrique");

    $this->load->model("Sousrubrique");

    $this->load->model("mdlimagespub");

    $this->load->model("AssCommercantAbonnement");

    $this->load->model("mdlville");

    $this->load->model("mdldepartement");

    $this->load->model("mdlfidelity");

    $this->load->library('user_agent');

    $this->load->model("Abonnement");

    $this->load->model("commercant");

    $this->load->Model("mdlannonce_perso");

    $this->load->model("mdlarticle");

    $this->load->model("Mdlcommercant");

    $this->load->model("mdl_agenda");

    $this->load->model("mdlcommune");

    $this->load->model("Mdl_soutenons");

    $this->load->model("mdl_categories_annonce");

    $this->load->model("Mdl_plat_du_jour");

    $this->load->library('session');

    $this->load->Model("mdl_localisation");

    $this->load->library("pagination");

    $this->load->library('image_moo');

    $this->load->library('ion_auth');

    $this->load->model("ion_auth_used_by_club");
        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");
        $group_proo_club = array(3, 4, 5,7);
        if ($this->ion_auth->is_admin()) {
            redirect('admin/home', 'refresh');
        } else if ($this->ion_auth->in_group(2)) {
            redirect('front/utilisateur/menuconsommateurs', 'refresh');
        } else if ($this->ion_auth->in_group($group_proo_club)) {
    //        redirect('front/utilisateur/contenupro', 'refresh');
        } else {
            redirect('/', 'refresh');
        }
    }

    function index(){
        $user_ion_auth = $this->ion_auth->user()->row();
        $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $data['infocom'] = $this->mdlcommercant->infocommercant($iduser);
        $data_commande = $this->Mdl_soutenons->get_com_data_by_idcom($iduser);
        $data['data_gli1'] = $this->Mdl_soutenons->getdatagli1($iduser);
        $data['data_gli2'] = $this->Mdl_soutenons->getdatagli2($iduser);
        $data['data_gli3'] = $this->Mdl_soutenons->getdatagli3($iduser);
        $data['data_gli4'] = $this->Mdl_soutenons->getdatagli4($iduser);
        $data['data_gli6'] = $this->Mdl_soutenons->getdatagli6($iduser);
        $data['data_gli7'] = $this->Mdl_soutenons->getdatagli7($iduser);

        $data['title_gli1'] = $this->Mdl_soutenons->gettitlegli1($iduser);
        $data['title_gli2'] = $this->Mdl_soutenons->gettitlegli2($iduser);
        $data['title_gli3'] = $this->Mdl_soutenons->gettitlegli3($iduser);
        $data['title_gli4'] = $this->Mdl_soutenons->gettitlegli4($iduser);
        $data['title_gli6'] = $this->Mdl_soutenons->gettitlegli6($iduser);
        $data['title_gli7'] = $this->Mdl_soutenons->gettitlegli7($iduser);

        $data['user_ion_auth'] = $user_ion_auth->id;
        $data['data_commande'] = $data_commande;
        $data['idcom'] = $iduser;
        $this->load->view('sortez_soutenons/admin/vwfiche_commande',$data);
    }

    public function save_command_data(){
        $user_ion_auth = $this->ion_auth->user()->row();
        $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $alldata = $this->input->post("data");
        $alldata['condition_content'] = html_entity_decode($alldata['condition_content']);
        //print_r($alldata);die();
        $alldata['idCommercant'] = $iduser;
        $this->Mdl_soutenons->save_command_data($alldata);
        redirect('soutenons/admin/Gestion_compte');
    }
    public function add_line(){
        $current = $this->input->post('current');
        $nbgliss = $this->input->post('nbgliss');
        $data['nbgliss'] = $nbgliss;
        $data['current'] = (int)$current+1;
        $this->load->view('sortez_soutenons/includes/line_form',$data);
    }
    public function save_gli(){
        $title = $this->input->post('title');
        $activ = $this->input->post('activ');
        $nbgli = $this->input->post('nbgli');
        $idcom = $this->input->post('idcom');

        $field = array(
            'titre_glissiere' => $title,
            'is_activ_glissiere' =>$activ,
            'idCom' =>$idcom,
        );
        echo "dfsdfs".$nbgli;
        $this->Mdl_soutenons->save_glissiere1($field,$nbgli);
        echo '1';
    }
    public function save_art(){
        $title = $this->input->post('titre');
        $true_title = $this->input->post('true_title');
        $prix = $this->input->post('prix');
        $idcom = $this->input->post('idCom');
        $idgli = $this->input->post('idgli');
        $indexs = $this->input->post('indexs');
        $id = $this->input->post('id');

        $fields = array(
            "titre" => $title,
            "prix" => $prix,
            "idCom" => $idcom,
            "id_glissiere" => $idgli,
            "id" => $id,
            "true_title" =>$true_title,
        );

        $idart = $this->Mdl_soutenons->save_article($fields);
        $sites = "'".site_url("media/index/".$idart."-soutgli1-photo".$idgli.$indexs)."','','width=1045, height=675, scrollbars=yes'";
        $return =array(
            "data"=>'<div id="Articlephoto'.$idgli.$indexs.'_container" onclick="javascript:window.open('.$sites.');" href="javascript:void(0);" class="w-100 img_add "><img class="w-100 h-100" src="'.base_url().'assets/images/download-icon-png.webp"></div>',
            "id" =>$idart,
            );
        echo json_encode($return);

    }
    public function save_paypal_data(){
        $is_active = $this->input->post('is_activ');
        $txt_paypal = $this->input->post('txt_paypal');
        $idcom = $this->input->post('idcom');
//        $pay_dom = $this->input->post("pay_dom");
//        $comment_livr_enlev = $this->input->post("comment_livr_enlev");
        $new_paypal_comment = $this->input->post("new_paypal_comment");

        $is_activ_comm_enlev = $this->input->post("is_activ_comm_enlev");
        $is_activ_com_spec = $this->input->post("is_activ_com_spec");
        $is_activ_comm_livr_dom = $this->input->post("is_activ_comm_livr_dom");
        $is_activ_comment_bottom = $this->input->post("is_activ_comment_bottom");
        $is_activ_card_bank_bottom = $this->input->post("is_activ_card_bank_bottom");
        $is_activ_cheque_bottom = $this->input->post("is_activ_cheque_bottom");
        $comment_comm_spec_bottom_txt = $this->input->post("comment_comm_spec_bottom_txt");


        $field = array(
            "is_activ_paypal" => $is_active,
            "paypal_content" => $txt_paypal,
            "idCommercant" => $idcom,
//            "pay_domicile" => $pay_dom,
//            "comment_livr_enlev" => $comment_livr_enlev,
            "new_paypal_comment" => $new_paypal_comment,
            "is_activ_comm_livr_dom" => $is_activ_comm_livr_dom,
            "is_activ_comm_enlev" => $is_activ_comm_enlev,
            "is_activ_com_spec" => $is_activ_com_spec,
            "is_activ_comment_bottom" => $is_activ_comment_bottom,
            "is_activ_card_bank_bottom" => $is_activ_card_bank_bottom,
            "is_activ_cheque_bottom" => $is_activ_cheque_bottom,
            "comment_comm_spec_bottom_txt" => $comment_comm_spec_bottom_txt,
        );
        $this->Mdl_soutenons->save_paypal_data($field);
    }
    public function delete_image(){
        $id_article = $this->input->post('id_art');
        $data_art = $this->Mdl_soutenons->get_art_by_id($id_article);
        if (isset($data_art) AND count($data_art) !=0){
            $image = $data_art->image;
            $idcom = $data_art->idCom;
            $infocom = $this->mdlcommercant->infoCommercant($idcom);
            $id_ionauth = $infocom->user_ionauth_id;
        }
        if (isset($image) AND isset($id_ionauth)){
                $this->Mdl_soutenons->delete_image_by_id($id_article);
                echo '1';
        }else{
            return '0';
        }
    }
    public function delete_article(){
        $id_art = $this->input->post('id_art');
        $this->Mdl_soutenons->delete_art_by_id($id_art);
        echo '1';
    }
    public function delete_cgv(){
        $user_ion_auth = $this->ion_auth->user()->row();
        $cgv = $this->input->post('cgv');
        $id_com = $this->input->post('id_com');
        $this->Mdl_soutenons->delete_cgv($cgv,$id_com);
        if(unlink("application/resources/front/photoCommercant/cgv/".$user_ion_auth->id."/".$cgv)){
            echo '1';
        }else{
            echo "0";
        }

    }
    public function delete_cgv_livraison(){
        $user_ion_auth = $this->ion_auth->user()->row();
        $cgv = $this->input->post('cgv');
        $id_com = $this->input->post('id_com');
        $this->Mdl_soutenons->delete_cgv_livraison($cgv,$id_com);
        if(unlink("application/resources/front/photoCommercant/cgv/".$user_ion_auth->id."/".$cgv)){
            echo '1';
        }else{
            echo "0";
        }

    }
    public function delete_cgv_differe(){
        $user_ion_auth = $this->ion_auth->user()->row();
        $cgv = $this->input->post('cgv');
        $id_com = $this->input->post('id_com');
        $this->Mdl_soutenons->delete_cgv_differe($cgv,$id_com);
        if(unlink("application/resources/front/photoCommercant/cgv/".$user_ion_auth->id."/".$cgv)){
            echo '1';
        }else{
            echo "0";
        }

    }
    public function delete_cgv_specif(){
        $user_ion_auth = $this->ion_auth->user()->row();
        $cgv = $this->input->post('cgv');
        $id_com = $this->input->post('id_com');
        $this->Mdl_soutenons->delete_cgv_specif($cgv,$id_com);
        if(unlink("application/resources/front/photoCommercant/cgv/".$user_ion_auth->id."/".$cgv)){
            echo '1';
        }else{
            echo "0";
        }

    }
    public function delete_doc(){
        $user_ion_auth = $this->ion_auth->user()->row();
        $doc = $this->input->post('doc');
        $id_com = $this->input->post('id_com');
        $this->Mdl_soutenons->delete_doc($doc,$id_com);
        if(unlink("application/resources/front/photoCommercant/doc/".$user_ion_auth->id."/".$doc)){
            echo '1';
        }else{
            echo "0";
        }

    }
    public function get_ville_by_codepostal(){
        $codepostal = $this->input->post('code_postal_coms');
        //var_dump($codepostal);
        $resultat = $this->mdlville->GetVilleByCodePostal_localisation_res($codepostal);
         echo json_encode($resultat[0]);
    }
    public function get_nom_by_id(){
        $idville = $this->input->post('idville');
        $result = $this->mdlville->getVilleById($idville);
        echo json_encode($result);
    }
    public function get_all_data_coms(){
        $data['IdCommercant'] = $this->input->post('idcommercant');
        $data['NomSociete'] = $this->input->post('nomsociete');
        $data['adresse_localisation'] = $this->input->post('adresse');
        $data['CodePostal'] = $this->input->post('codepostal');
        $data['IdVille'] = $this->input->post('ville_id');
        $data['Email'] = $this->input->post('courriel_coms');
        $data['TelMobile'] = $this->input->post('numero_telephone');
        $data['Siret'] = $this->input->post('siret');
        $data['code_ape'] = $this->input->post('code_ape');
        $data['Nom'] = $this->input->post('nom_responsable');
        $data['Prenom'] = $this->input->post('prenom_responsable');
        $data['Responsabilite'] = $this->input->post('titre_responsable');
        //var_dump($data);die();
        if($this->commercant->Update($data)){
            echo "ok";
        }else{
            echo "ko";
        }

    }
    //kappa 22.02.22
    function save_click_menu($idcom){

        $data = $this->input->post('data');
        $this->mdlcommercant->save_click($idcom, $data);
        redirect('soutenons/admin/Gestion_compte');
    }

 public function delete_click_menu($idcom){

    $infocom = $this->mdlcommercant->infoCommercant($idcom);
    if (is_file("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/click_menu/".$infocom->click_icon)){
            unlink("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/click_menu/".$infocom->click_icon);
    }
    $this->mdlcommercant->delete_click_menu($idcom);
    echo '1';
}
    
}