<?php

class vivresaville_mobile extends CI_Controller{

	function __construct(){
        parent::__construct();
        $this->load->model ( "mdl_card" );
        $this->load->model ( "mdl_card_bonplan_used" );
        $this->load->model ( "mdl_card_capital" );
        $this->load->model ( "mdl_card_tampon" );
        $this->load->model ( "mdl_card_remise" );
        $this->load->model ( "mdl_card_user_link" );
        $this->load->model ( "mdl_card_fiche_client_tampon" );
        $this->load->model ( "mdl_card_fiche_client_capital" );
        $this->load->model ( "mdl_card_fiche_client_remise" );
        $this->load->model ( 'assoc_client_bonplan_model' );
        $this->load->model ( 'assoc_client_commercant_model' );
        $this->load->Model ( "Ville" );
        $this->load->Model ( "Assoc_client_table_model" );
        $this->load->model ( "user" );
        $this->load->model ( "mdlbonplan" );
        $this->load->model ( "commercant" );
        $this->load->model ( "Mdlcommercant" );
        $this->load->model ( "mdlannonce" );
        $this->load->model ( "mdl_card_tampon" );
        $this->load->model ( "mdl_card_capital" );
        $this->load->model ( "mdl_card_remise" );
        $this->load->model("mdlarticle");
        $this->load->model("mdlcategorie");
        $this->load->model("mdlcommercantpagination");
        $this->load->library ( 'session' );
        $this->load->model("mdl_agenda_datetime");
        $this->load->library ( 'ion_auth' );
        $this->load->model ( "ion_auth_used_by_club" );
        $this->load->library('ion_auth');
        $this->load->model("api_model");
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->model("Vivresaville_model");
        $this->load->model("Api_model");
        $this->load->model("mdl_agenda");
        $this->load->model("Vivresaville_villes");
        $this->output->set_header('Access-Control-Allow-Origin: null');  header('Access-Control-Allow-Credentials: omit');
        header('Access-Control-Allow-Method:PUT,GET,POST,DELETE,OPTIONS');
        header('AccessControl-Allow-Headers:Content-Type;x-xsrf-token');
    }

    public function tester(){
        echo "Controller Vivresaville mande tsara ";
    }

    //recupération nom ville by iotadev
    public function get_all_nomville(){       
        $res = $this->Vivresaville_villes->getAl1active();
        $data["toville"] = $res;
     // var_dump($res);die("debug");  
        echo json_encode($data);        
    }

    //récuperation ville by idVille 
    public function get_ville($idVille){
        $res = $this->Vivresaville_model->get_name_ville($idVille);
        echo json_encode($res);
    }

    //recupération rubrique ville by iotadev
    public function get_list_rubriqueVille($idVille){

        // $rest_json=file_get_contents("php://input");
        // $value_code = explode('=',$rest_json)[1];
        // $value_code="Nice";
        $value_code = $idVille;
        // $res["agenda"]= $this->api_model->get_3_annuaire($value_code);
        $agenda= $this->Vivresaville_model->get_3_agenda($value_code);
        //echo json_encode($agenda);die("fin");

        $res["agenda"]= [];
        if(is_array($agenda) && !empty($agenda)){
        foreach($agenda as $key => $val){
            // $path_img1="application/resources/front/photoCommercant/imagesbank/".$val->user_ionauth_id."/".$val->photo1;
            $path_img2="application/resources/front/photoCommercant/imagesbank/scrapped_image/".$val->photo1;
            if(file_exists($path_img2) && $val->photo1 != null){
                $val->photo1 = base_url()."application/resources/front/photoCommercant/imagesbank/scrapped_image/".$val->photo1;
            }else{
                $val->photo1 = base_url()."application/resources/front/images/no_image_annuaire.png";
            }
            array_push($res["agenda"], $val);
        }
        }else {
            array_push($res["agenda"], "");
        }    

        $annuaire= $this->api_model->get_3_annuaire($value_code);
        $res["annuaire"] = [] ;
        if(is_array($annuaire) && !empty($annuaire)){
        foreach($annuaire as $key => $val){
            $path_img1="application/resources/front/photoCommercant/imagesbank/".$val->user_ionauth_id."/".$val->photo1;
            $path_img2="application/resources/front/photoCommercant/images/".$val->photo1;
            if(file_exists($path_img1) && $val->photo1 != null && $val->user_ionauth_id != null){
                $val->photo1 = base_url()."application/resources/front/photoCommercant/imagesbank/".$val->user_ionauth_id."/".$val->photo1;
            }elseif(file_exists($path_img2) && $val->photo1 != null){
                $val->photo1 = base_url()."application/resources/front/photoCommercant/images/".$val->photo1;
            }else{
                $val->photo1 = base_url()."application/resources/front/images/no_image_annuaire.png";
            }
            array_push($res["annuaire"], $val);
        }
        }else {
            array_push($res["annuaire"], "");
        }        


       $annonce= $this->api_model->get_3_annonce($value_code);
       $res['annonce'] = [] ;
       if(is_array($annonce) && !empty($annonce))
       {
           foreach($annonce as $key => $val){
                   $path_img="application/resources/front/photoCommercant/imagesbank/".$val->user_ionauth_id."/".$val->annonce_photo1;
                   if(!file_exists($path_img) && $val->annonce_photo1 == null && $val->user_ionauth_id == null){
                       $val->annonce_photo1 = "https://www.testpriviconcept.ovh/boutique_logo.jpg";
                   }else{
                       $val->annonce_photo1 = base_url()."application/resources/front/photoCommercant/imagesbank/".$val->user_ionauth_id."/".$val->annonce_photo1;
                   }
                   array_push($res['annonce'], $val);
           }
       }else{
        array_push($res['annonce'],"");
       }       

        $article= $this->api_model->get_3_article($value_code);
        $res["article"] = [] ;
        if(is_array($article) && !empty($article))
        {
            foreach($article as $key => $val){
                $path_img1="application/resources/front/photoCommercant/imagesbank/".$val->IdUsers_ionauth."/".$val->photo1;
                $path_img2="application/resources/front/photoCommercant/images/".$val->photo1;
                if(file_exists($path_img1) && $val->photo1 != null && $val->IdUsers_ionauth != null){
                    $val->photo1 = base_url()."application/resources/front/photoCommercant/imagesbank/".$val->IdUsers_ionauth."/".$val->photo1;
                }elseif(file_exists($path_img2) && $val->photo1 != null){
                    $val->photo1 = base_url()."application/resources/front/photoCommercant/images/".$val->photo1;
                }else{
                    $val->photo1 = base_url()."boutique_logo.jpg";
                }
                array_push($res["article"], $val);
            }
        }else {
            array_push($res["article"], "");
        }
        echo json_encode($res);
    }

    // get all annuaire by idville - lambdadev
    public function get_all_annuaire_by_idville($id_ville, $categ = 0){
        $value_code = $id_ville;
        $value_categ = $categ;
        $annuaire= $this->Vivresaville_model->get_all_annuaire_by_idville($value_code, $value_categ);
        $res["annuaire"] = [] ;
        if(is_array($annuaire) && !empty($annuaire)){
            foreach($annuaire as $key => $val){
                $path_img1="application/resources/front/photoCommercant/imagesbank/".$val->user_ionauth_id."/".$val->photo1;
                $path_img2="application/resources/front/photoCommercant/images/".$val->photo1;
                if(file_exists($path_img1) && $val->photo1 != null && $val->user_ionauth_id != null){
                    $val->photo1 = base_url()."application/resources/front/photoCommercant/imagesbank/".$val->user_ionauth_id."/".$val->photo1;
                }elseif(file_exists($path_img2) && $val->photo1 != null){
                    $val->photo1 = base_url()."application/resources/front/photoCommercant/images/".$val->photo1;
                }else{
                    $val->photo1 = base_url()."application/resources/front/images/no_image_annuaire.png";
                }
                array_push($res["annuaire"], $val);
            }
        }else {
            array_push($res["annuaire"], "");
        }
        echo json_encode($res);
    }

    // get all annonce by idville - lambdadev
    public function get_all_annonce_by_idville($id_ville, $categ = 0){
        $value_code = $id_ville;
        $value_categ = $categ;
        $annonce= $this->Vivresaville_model->get_all_annonce_by_idville($value_code, $value_categ);
        $res['annonce'] = [] ;
        if(is_array($annonce) && !empty($annonce)){
            foreach($annonce as $key => $val){
                $path_img="application/resources/front/photoCommercant/imagesbank/".$val->user_ionauth_id."/".$val->annonce_photo1;
                if(!file_exists($path_img) && $val->annonce_photo1 == null && $val->user_ionauth_id == null){
                    $val->annonce_photo1 = "https://www.testpriviconcept.ovh/boutique_logo.jpg";
                }else{
                    $val->annonce_photo1 = base_url()."application/resources/front/photoCommercant/imagesbank/".$val->user_ionauth_id."/".$val->annonce_photo1;
                }
                array_push($res['annonce'], $val);
            }
        }else{
            array_push($res['annonce'],"");
        } 
        echo json_encode($res);      
    }

    // get all agenda by idville and category - lambdadev
    public function get_all_agenda_by_idville($id_ville, $categ = 0){
        $value_code = $id_ville;
        $value_categ = $categ;
        $agenda= $this->Vivresaville_model->get_all_agenda_by_idville($value_code, $value_categ);
        $res["agenda"]= [];
        if(is_array($agenda) && !empty($agenda)){
            foreach($agenda as $key => $val){
                $path_img2="application/resources/front/photoCommercant/imagesbank/scrapped_image/".$val->photo1;
                if(file_exists($path_img2) && $val->photo1 != null){
                    $val->photo1 = base_url()."application/resources/front/photoCommercant/imagesbank/scrapped_image/".$val->photo1;
                }else{
                    $val->photo1 = base_url()."application/resources/front/images/no_image_annuaire.png";
                }
                array_push($res["agenda"], $val);
            }
        }else {
            array_push($res["agenda"], "");
        }
        echo json_encode($res); 
    }

    // get all article by idville and category - lambdadev
    public function get_all_article_by_idville($id_ville, $categ = 0){
        $value_code = $id_ville;
        $value_categ = $categ;
        $article= $this->Vivresaville_model->get_all_article_by_idville($value_code, $value_categ);
        $res["article"] = [] ;
        if(is_array($article) && !empty($article))
        {
            foreach($article as $key => $val){
                $path_img1="application/resources/front/photoCommercant/imagesbank/".$val->IdUsers_ionauth."/".$val->photo1;
                $path_img2="application/resources/front/photoCommercant/images/".$val->photo1;
                if(file_exists($path_img1) && $val->photo1 != null && $val->IdUsers_ionauth != null){
                    $val->photo1 = base_url()."application/resources/front/photoCommercant/imagesbank/".$val->IdUsers_ionauth."/".$val->photo1;
                }elseif(file_exists($path_img2) && $val->photo1 != null){
                    $val->photo1 = base_url()."application/resources/front/photoCommercant/images/".$val->photo1;
                }else{
                    $val->photo1 = base_url()."boutique_logo.jpg";
                }
                array_push($res["article"], $val);
                // var_dump($res["article"]);
                // die('misy');
            }
        }else {
            array_push($res["article"], "");
            // var_dump($res["article"]);
            // die('tsy misy');
        }
        echo json_encode($res);
    }

    // get all category for agenda - lambda
    public function get_all_category_agenda($id_ville){
        $category = $this->Vivresaville_model->get_all_categorie_agenda($id_ville);
        echo json_encode($category);
    }

    // get all category for article - lambda
    public function get_all_category_article($id_ville){
        $category = $this->Vivresaville_model->get_all_categorie_article($id_ville);
        echo json_encode($category);
    }

    //get all category for annuaire - lambda
    public function get_all_category_annuaire($id_ville){
        $category = $this->Vivresaville_model->get_all_categorie_annuaire($id_ville);
        echo json_encode($category);
    }

    //get all category for annonce - lambda
    public function get_all_category_annonce($id_ville){
        $category = $this->Vivresaville_model->get_all_categorie_annonces($id_ville);
        echo json_encode($category);
    }

    public function get_count_topic_data($id_ville){
        $count['article'] = $this->Vivresaville_model->get_count_article($id_ville);
        if($count['article'] == []){
            $count['article'][0] = array(
                'IdVille' => $id_ville,
                'article_count' => '0'
            );            
        }
        $count['annonce'] = $this->Vivresaville_model->get_count_annonce($id_ville);
        if($count['annonce'] == []){
            $count['annonce'][0] = array(
                'IdVille' => $id_ville,
                'annonce_count' => '0'
            );
        }
        $count['agenda'] = $this->Vivresaville_model->get_count_agenda($id_ville);
        if($count['agenda'] == []){
            $count['agenda'][0] = array(
                'IdVille' => $id_ville,
                'agenda_count' => '0'
            );
        }
        $count['annuaire'] = $this->Vivresaville_model->get_count_annuaire($id_ville);
        if($count['annuaire'] == []){
            $count['annuaire'][0] = array(
                'IdVille' => $id_ville,
                'annuaire_count' => '0'
            );
        }
        echo json_encode($count);
    }
    public function getAgendasListe($nbPage = 1){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);

        $commune=$_POST["commune"];

        $_iCategorieId = 0;
        $_iVilleId = $commune;
        $_iDepartementId = 0;
        $_zMotCle = "";
        $_iSousCategorieId = 0;
        $_limitstart = 0;
        $_limitend = $nbPage*20;
        $iOrderBy = "";
        $inputQuand = "0";
        $inputDatedebut = "0000-00-00";
        $inputDatefin = "0000-00-00";
        $inputIdCommercant = "0";
        $allag = [];

        $allagenda = $this->mdl_agenda->listeAgendaRecherche($_iCategorieId, $_iVilleId, $_iDepartementId, $_zMotCle, $_iSousCategorieId, $_limitstart, $_limitend, $iOrderBy, $inputQuand, $inputDatedebut, $inputDatefin, $inputIdCommercant);
        foreach($allagenda as $ag){
            $oInfoCommercant = $this->Mdlcommercant->infoCommercant($ag->IdCommercant);
            $image = "application/resources/front/photoCommercant/imagesbank/".$ag->IdUsers_ionauth."/".$ag->photo1;
            $image_thumbs = "application/resources/front/photoCommercant/imagesbank/".$ag->IdUsers_ionauth."/thumbs/thumb_".$ag->photo1;
            if(is_file($image_thumbs)){
                $imageag = base_url().$image_thumbs;
            }elseif(is_file($image)){
                $imageag = base_url().$image;
            }elseif(is_file("application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/".$oInfoCommercant->Photo1)){
                $imageag = base_url()."/application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/".$oInfoCommercant->Photo1;
            }else{
                $imageag = GetImagePath("front/") . "/wp71b211d2_06.png";
            }
            $ag->photo1 = $imageag;
            array_push($allag,$ag);
        }
        $data['toAgenda']= $allag;
        echo json_encode($data);
        
    }

    public function get_ag_details(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id = $_POST["id"];
        $dataAgenda = $this->mdl_agenda->GetById_IsActif($id);
        $oInfoCommercant = $this->Mdlcommercant->infoCommercant($dataAgenda->IdCommercant);
        $image = "application/resources/front/photoCommercant/imagesbank/".$dataAgenda->IdUsers_ionauth."/".$dataAgenda->photo1;
        $image_thumbs = "application/resources/front/photoCommercant/imagesbank/".$dataAgenda->IdUsers_ionauth."/"."thumbs/thumb_".$dataAgenda->photo1;
        if(is_file($image)){
            $imageag = base_url().$image;
        }elseif(is_file("application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/".$oInfoCommercant->Photo1)){
            $imageag = base_url()."/application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/".$oInfoCommercant->Photo1;
        }else{
            $imageag = GetImagePath("front/") . "/wp71b211d2_06.png";
        }
        $dataAgenda->photo1 = $imageag;
        $data['article'] = $dataAgenda;
        $date = "";
        $toArticle_datetime = $this->mdl_agenda_datetime->getByAgendaId($id);
        
         if (isset($toArticle_datetime) && count($toArticle_datetime) > 0) { 
            foreach ($toArticle_datetime as $objArticle_datetime) { 
                if (isset($objArticle_datetime->date_debut) && $objArticle_datetime->date_debut != "0000-00-00" && ($objArticle_datetime->date_debut == $objArticle_datetime->date_fin)) {
                    $date .=  "Le " . translate_date_to_fr($objArticle_datetime->date_debut);
                } else {
                    if (isset($objArticle_datetime->date_debut) && $objArticle_datetime->date_debut != "0000-00-00") $date = "Du " . translate_date_to_fr($objArticle_datetime->date_debut);
                    if (isset($objArticle_datetime->date_fin) && $objArticle_datetime->date_fin != "0000-00-00") {
                        if (isset($objArticle_datetime->date_debut) && $objArticle_datetime->date_debut != "0000-00-00") $date .= " au " . translate_date_to_fr($objArticle_datetime->date_fin);
                        else $date .= " Jusqu'au " . translate_date_to_fr($objArticle_datetime->date_fin);
                    }
                    if (isset($objArticle_datetime->heure_debut) && $objArticle_datetime->heure_debut != "0:00"   && $objArticle_datetime->heure_debut != "" && $objArticle_datetime->heure_debut != null) $date .= " à " . str_replace(":", "h", $objArticle_datetime->heure_debut);
                }
                
            } 
         }

         if (isset($dataAgenda->organiser_id) && $dataAgenda->organiser_id != "0") {
            $obj_organiser_article_details = $this->mdl_article_organiser->getById($dataAgenda->organiser_id);
            if (isset($obj_organiser_article_details) && is_object($obj_organiser_article_details)) {
                
                $dataAgenda->organiser_name = $obj_organiser_article_details->name;
                $dataAgenda->organiser_codePostal = $obj_organiser_article_details->postal_code;
                    if (isset($obj_organiser_article_details->address1) && $obj_organiser_article_details->address1 != "") $dataAgenda->organiser_adress =  $obj_organiser_article_details->address1;
                    if (isset($obj_organiser_article_details->ville_id) && $obj_organiser_article_details->ville_id != "0") $dataAgenda->organiser_ville =  $this->mdlville->getVilleById($obj_organiser_article_details->ville_id)->Nom;
                    if (isset($obj_organiser_article_details->tel) && $obj_organiser_article_details->tel != "") $dataAgenda->organiser_telephone = $obj_organiser_article_details->tel;
                    if (isset($obj_organiser_article_details->website) && $obj_organiser_article_details->website != "") $dataAgenda->organiser_website = $obj_organiser_article_details->website;//echo "<br/>Site Web : " . $obj_organiser_article_details->website;
                    if (isset($obj_organiser_article_details->facebook) && $obj_organiser_article_details->facebook != "") $dataAgenda->organiser_facebook = $obj_organiser_article_details->facebook; //echo "<br/>Facebook : " . $obj_organiser_article_details->facebook;
                    if (isset($obj_organiser_article_details->twitter) && $obj_organiser_article_details->twitter != "") $dataAgenda->organiser_twitter = $obj_organiser_article_details->twitter; //echo "<br/>Twitter : " . $obj_organiser_article_details->twitter;
                    if (isset($obj_organiser_article_details->googleplus) && $obj_organiser_article_details->googleplus != "") $dataAgenda->organiser_gplus = $obj_organiser_article_details->googleplus;//echo "<br/>Google+ : " . $obj_organiser_article_details->googleplus;
            
            }
        }


        $dataAgenda->description = htmlspecialchars_decode(strip_tags($dataAgenda->description),ENT_QUOTES);
        $dataAgenda->NomSociete = $oInfoCommercant->NomSociete;
        $dataAgenda->date_debut = $date;
        $data['article'] = $dataAgenda;

        echo json_encode($data);

    }
    public function get_art_details(){

        // $rest_json=file_get_contents("php://input");
        // $_POST=json_decode($rest_json,true);
        // $id = $_POST["id"];
        $id=151;
        $dataAgenda = $this->mdlarticle->GetById_IsActif($id);
        $oInfoCommercant = $this->Mdlcommercant->infoCommercant($dataAgenda->IdCommercant);
        $image = "application/resources/front/photoCommercant/imagesbank/".$dataAgenda->IdUsers_ionauth."/".$dataAgenda->photo1;
        $image2 = "application/resources/front/photoCommercant/imagesbank/rss_image/".$dataAgenda->photo1;
        if(is_file($image)){
            $imageag = "https://www.sortez.org/".$image;
        }elseif(is_file($image2)){
            $imageag = "https://www.sortez.org/".$image2;
        }else{
            $imageag = GetImagePath("front/") . "/wp71b211d2_06.png";
        }
        $dataAgenda->photo1 = $imageag;
        $data['article'] = $dataAgenda;
        $date = "";
        $toArticle_datetime = $this->mdl_article_datetime->getByArticleId($id);
         if (isset($toArticle_datetime) && count($toArticle_datetime) > 0) { 
            foreach ($toArticle_datetime as $objArticle_datetime) { 
                if (isset($objArticle_datetime->date_debut) && $objArticle_datetime->date_debut != "0000-00-00" && ($objArticle_datetime->date_debut == $objArticle_datetime->date_fin)) {
                    $date .=  "Le " . translate_date_to_fr($objArticle_datetime->date_debut);
                } else {
                    if (isset($objArticle_datetime->date_debut) && $objArticle_datetime->date_debut != "0000-00-00") $date = "Du " . translate_date_to_fr($objArticle_datetime->date_debut);
                    if (isset($objArticle_datetime->date_fin) && $objArticle_datetime->date_fin != "0000-00-00") {
                        if (isset($objArticle_datetime->date_debut) && $objArticle_datetime->date_debut != "0000-00-00") $date .= " au " . translate_date_to_fr($objArticle_datetime->date_fin);
                        else $date .= " Jusqu'au " . translate_date_to_fr($objArticle_datetime->date_fin);
                    }
                    if (isset($objArticle_datetime->heure_debut) && $objArticle_datetime->heure_debut != "0:00"   && $objArticle_datetime->heure_debut != "" && $objArticle_datetime->heure_debut != null) $date .= " à " . str_replace(":", "h", $objArticle_datetime->heure_debut);
                }
                
            } 
         }

         if (isset($dataAgenda->organiser_id) && $dataAgenda->organiser_id != "0") {
            $obj_organiser_article_details = $this->mdl_article_organiser->getById($dataAgenda->organiser_id);
            if (isset($obj_organiser_article_details) && is_object($obj_organiser_article_details)) {
                
                $dataAgenda->organiser_name = $obj_organiser_article_details->name;
                $dataAgenda->organiser_codePostal = $obj_organiser_article_details->postal_code;
                    if (isset($obj_organiser_article_details->address1) && $obj_organiser_article_details->address1 != "") $dataAgenda->organiser_adress =  $obj_organiser_article_details->address1;
                    if (isset($obj_organiser_article_details->ville_id) && $obj_organiser_article_details->ville_id != "0") $dataAgenda->organiser_ville =  $this->mdlville->getVilleById($obj_organiser_article_details->ville_id)->Nom;
                    if (isset($obj_organiser_article_details->tel) && $obj_organiser_article_details->tel != "") $dataAgenda->organiser_telephone = $obj_organiser_article_details->tel;
                    if (isset($obj_organiser_article_details->website) && $obj_organiser_article_details->website != "") $dataAgenda->organiser_website = $obj_organiser_article_details->website;//echo "<br/>Site Web : " . $obj_organiser_article_details->website;
                    if (isset($obj_organiser_article_details->facebook) && $obj_organiser_article_details->facebook != "") $dataAgenda->organiser_facebook = $obj_organiser_article_details->facebook; //echo "<br/>Facebook : " . $obj_organiser_article_details->facebook;
                    if (isset($obj_organiser_article_details->twitter) && $obj_organiser_article_details->twitter != "") $dataAgenda->organiser_twitter = $obj_organiser_article_details->twitter; //echo "<br/>Twitter : " . $obj_organiser_article_details->twitter;
                    if (isset($obj_organiser_article_details->googleplus) && $obj_organiser_article_details->googleplus != "") $dataAgenda->organiser_gplus = $obj_organiser_article_details->googleplus;//echo "<br/>Google+ : " . $obj_organiser_article_details->googleplus;
            
            }
        }


        $dataAgenda->description = htmlspecialchars_decode(strip_tags($dataAgenda->description),ENT_QUOTES);
        $dataAgenda->NomSociete = $oInfoCommercant->NomSociete;
        $dataAgenda->date_debut = $date;
        $data['article'] = $dataAgenda;
        echo json_encode($data);

    }

    function translatetext(){
        $text="bonjour mon teste translation";
        $curlSession = curl_init();

        curl_setopt($curlSession, CURLOPT_URL, 'https://translate.googleapis.com/translate_a/single?client=gtx&sl=en&tl=en&dt=t&q='.urlencode($text));
        curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curlSession);
        var_dump('valeur response'.$response);
        $jsonData = json_decode($response);
        var_dump('valeur json'.$jsonData);
        curl_close($curlSession);
    
        if(isset($jsonData[0][0][0])){
            return $jsonData[0][0][0];
        }else{
            return false;
        }
    }

    public function get_Annuaire($nbPage = 1){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        //$_POST["commune"]
        $commune=$_POST["commune"];
        $subcateg=0;
        $categorie=0;
        $motcle="";
        $data["iFavoris"] = "";
        $page_pagination = 0;
        $config_pagination["per_page"] = $nbPage*20;
        $iOrderBy = 0;
        $inputFromGeolocalisation = "0";
        $inputGeolocalisationValue = "0";
        $inputGeoLatitude = "0";
        $inputGeoLongitude = "0";
        $inputAvantagePartenaire = "0";
        $input_is_actif_coronna = 0;
        $input_is_actif_command = 0;
        $to_send = [];
        $toCommercant = $this->mdlcommercantpagination->listePartenaireRecherche(
            $categorie,
            $commune,
            $iDepartementId =0,
            $motcle,
            $data["iFavoris"],
            $page_pagination,
            $config_pagination["per_page"],
            $iOrderBy,
            $inputFromGeolocalisation,
            $inputGeolocalisationValue,
            $inputGeoLatitude,
            $inputGeoLongitude,
            $inputAvantagePartenaire,
            $subcateg,
            $input_is_actif_coronna,
            $input_is_actif_command
        );
        
        foreach($toCommercant as $com){
            $categs = $this->mdlcategorie->GetById($com->IdRubrique);
            $subcategs = $this->mdlcategorie->GetByIdSousCateg($com->IdSousRubrique);
            $image_thumbs = "application/resources/front/photoCommercant/imagesbank/".$com->user_ionauth_id."/thumbs/thumb_".$com->Photo1;
            if(is_file($image_thumbs)){
                $imagecom = base_url().$image_thumbs;
            }elseif(is_file("application/resources/front/photoCommercant/imagesbank/".$com->user_ionauth_id."/".$com->Photo1)){
                $imagecom = base_url()."/application/resources/front/photoCommercant/imagesbank/".$com->user_ionauth_id."/".$com->Photo1;
            }elseif(is_file("application/resources/front/photoCommercant/images/".$com->Photo1)){
                $imagecom = base_url()."/application/resources/front/photoCommercant/images/".$com->Photo1;
            }else{
                $imagecom = GetImagePath("front/") . "/wp71b211d2_06.png";
            }
            $array = array(
                "categorie" =>$categs->Nom,
                "categid" => $categs->IdRubrique,
                "titre" =>$com->NomSociete,
                "ville_nom" =>$com->ville,
                "ville_id" => $com->IdVille,
                "subctaeg" => $subcategs->Nom,
                "subcategid" =>$subcategs->IdSousRubrique,
                //"description" =>$com->metadescription,
                "description" => strip_tags($com->Caracteristiques),
                "id" => $com->IdCommercant,
                "user_ionauth_id" => $com->user_ionauth_id, 
                "Photo1" => $imagecom,
                "nom_url" => $com->nom_url
            );
            array_push($to_send,$array);
            // var_dump($array);
        }
        $data['toCommercant'] = $to_send ;
        echo json_encode($data);

    }
    public function get_bonplan_by_idville($id){

        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        // $id=$_POST['commune'];
        $to_get=[];
        $get = $this->Vivresaville_model->get_bonplan_byidville($id);
        

        foreach($get as $bnp){
            $image_thumbs = "application/resources/front/photoCommercant/imagesbank/".$bnp->ionauth_id."/thumbs/thumbnail_";
            if (isset($bnp->bonplan_photo1) and $bnp->bonplan_photo1!=null) {

                $photo= site_url().$image_thumbs.$bnp->bonplan_photo1;
            }
            elseif (isset($bnp->bonplan->photo2) and $bnp->bonplan->photo2 != null) {
                $photo= site_url().$image_thumbs.$bnp->bonplan_photo2;
            }
            elseif (isset($bnp->bonplan->photo3) and $bnp->bonplan->photo3 != null) {
                $photo= site_url().$image_thumbs.$bnp->bonplan_photo3;
            }
            elseif (isset($bnp->bonplan->photo4) and $bnp->bonplan->photo4 != null) {
                $photo= site_url().$image_thumbs.$bnp->bonplan_photo4;
            }
            elseif (isset($bnp->bonplan->photo5) and $bnp->bonplan->photo5 != null) {
                $photo= site_url().$image_thumbs.$bnp->bonplan_photo5;
            }
            else{
                $photo= site_url()."application/resources/front/images/no_image_boutique.png";
            }

            $array = array(
                "titre" =>$bnp->bonplan_titre,
                "description" => htmlspecialchars_decode(strip_tags($bnp->bonplan_texte),ENT_QUOTES),
                "photo"=>$photo,
                "date_debut" =>$bnp->bonplan_date_debut,
                "date_fin" =>$bnp->bonplan_date_fin,
                "condition" => $bnp->bonplan_condition_vente,
                "quantite" => $bnp->bonplan_quantite,
                "prix_uniq" =>$bnp->bp_unique_prix,
                "unique_value" =>$bnp->bp_unique_value,
                "unique_economie" => $bnp->bp_unique_eco,
                "multiple_date_debut" => $bnp->bp_multiple_date_debut,
                "multiple_date_fin" => $bnp->bp_multiple_date_fin, 
                "multiple_value" =>$bnp->bp_multiple_value,
                "multiple_economie" => $bnp->bp_multiple_eco,
                "nom_commercant" => $bnp->NomSociete,
                "ville"=>$bnp->ville,
                "adresse"=>$bnp->quartier,
             
            );
            array_push($to_get,$array);


        }
        echo json_encode($to_get);
    }
    public function get_fidelite_by_idville($id){

        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        
        $to_get=[];

        $tampon = $this->Vivresaville_model->get_fidelite_byidville($id,'tampon');
        $remise = $this->Vivresaville_model->get_fidelite_byidville($id,'remise');
        $capt = $this->Vivresaville_model->get_fidelite_byidville($id,'capital');

        // var_dump($capt);
        
        $allarrays = [];
        
        $no_src = site_url()."application/resources/front/images/no_image_boutique.png";
         $i = 0;

        if ($tampon !=[]){
            foreach ($tampon as $list_tampon){

                $src = base_url()."application/resources/front/photoCommercant/imagesbank/".$list_tampon->user_ionauth_id."/thumbs/thumbnail_";
                $allarrays[$i]['id'] = $list_tampon->id;
                $allarrays[$i]['titre'] = $list_tampon->titre;
                $allarrays[$i]['description'] = $list_tampon->description;
                $allarrays[$i]['montant'] = $list_tampon->montant;
                $allarrays[$i]['date_fin'] = $list_tampon->date_fin;
                $allarrays[$i]['date_debut'] =  $list_tampon->date_debut;
                $allarrays[$i]['ville_nom'] = $list_tampon->ville;
                $allarrays[$i]['ville_id'] = $list_tampon->IdVille;
                $allarrays[$i]['categorie'] = 'tampon';
                $allarrays[$i]['idcom'] = $list_tampon->IdCommercant;
                $allarrays[$i]['partenaire'] = $list_tampon->NomSociete;
                $allarrays[$i]['partenaire_url'] = $list_tampon->nom_url;
                if(isset($list_tampon->image1)&& $list_tampon->image1 !=null){
                $allarrays[$i]['image'] = $src.$list_tampon->image1;
            }else{
                $allarrays[$i]['image'] = $no_src;
            }

                $i ++;
                // array_push($to_get,$allarrays);
            }
        }

        if ($remise !=[]) {
            foreach ($remise as $list_remise){

                 $src = base_url()."application/resources/front/photoCommercant/imagesbank/".$list_remise->user_ionauth_id."/thumbs/thumbnail_";
                $allarrays[$i]['id'] = $list_remise->id;
                $allarrays[$i]['titre'] = $list_remise->titre;
                $allarrays[$i]['description'] = $list_remise->description;
                $allarrays[$i]['montant'] = $list_remise->montant;
                $allarrays[$i]['date_fin'] = $list_remise->date_fin;
                $allarrays[$i]['date_debut'] =  $list_remise->date_debut;
                $allarrays[$i]['ville_nom'] = $list_remise->ville;
                $allarrays[$i]['ville_id'] = $list_remise->IdVille;
                $allarrays[$i]['categorie'] = 'remise';
                $allarrays[$i]['idcom'] = $list_remise->IdCommercant;
                $allarrays[$i]['partenaire'] = $list_remise->NomSociete;
                $allarrays[$i]['partenaire_url'] = $list_remise->nom_url;
                if(isset($list_remise->image1)&& $list_remise->image1 !=null){
                $allarrays[$i]['image'] = $src.$list_remise->image1;
            }else{
                $allarrays[$i]['image'] = $no_src;
            }

                $i ++;
                // array_push($to_get,$allarrays);


            }
        }

        if ($capt !=[]) {
            foreach ($capt as $list_capt){

                 $src = base_url()."application/resources/front/photoCommercant/imagesbank/".$list_capt->user_ionauth_id."/thumbs/thumbnail_";
                $allarrays[$i]['id'] = $list_capt->id;
                $allarrays[$i]['titre'] = $list_capt->titre;
                $allarrays[$i]['description'] = $list_capt->description;
                $allarrays[$i]['montant'] = $list_capt->montant;
                $allarrays[$i]['date_fin'] = $list_capt->date_fin;
                $allarrays[$i]['date_debut'] =  $list_capt->date_debut;
                $allarrays[$i]['ville_nom'] = $list_capt->ville;
                $allarrays[$i]['ville_id'] = $list_capt->IdVille;
                $allarrays[$i]['categorie'] = 'capital';                
                $allarrays[$i]['idcom'] = $list_capt->IdCommercant;
                $allarrays[$i]['partenaire'] = $list_capt->NomSociete;
                $allarrays[$i]['partenaire_url'] = $list_capt->nom_url;
                if(isset($list_capt->image1)&& $list_capt->image1 !=null){
                $allarrays[$i]['image'] = $src.$list_capt->image1;
            }else{
                $allarrays[$i]['image'] = $no_src;
            }

                $i ++;
                // array_push($to_get,$allarrays);


            }
        }

         echo json_encode($allarrays);

    }
}

?>