<?php

Class ListVille extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("vivresaville_villes");
        $this->load->model("vsv_ville_other");
        $this->load->model("vsv_ville_reference");
        $this->load->library('session');
    }

    function index()
    {
        //check if there is ville_id referenced with ip address
        $ip_adress = $this->input->ip_address();
        $objVilleReference = $this->vsv_ville_reference->getWhere(' ip="'.$ip_adress.'" and id_ville!=0 and id_ville IS NOT NULL ');
        if (isset($objVilleReference) && count($objVilleReference)>0) {
            $vsv_object_checking = $this->vivresaville_villes->getWhere(" id_ville=".$objVilleReference[0]->id_ville);
            if (isset($vsv_object_checking) && count($vsv_object_checking)>0)
                redirect('vivresaville/ListVille/CheckVille/'.$vsv_object_checking[0]->id);
        }

        $localdata_IdVille = $this->session->userdata('localdata_IdVille');
        if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != false) {
            redirect("vivresaville/Accueil");
        }

        $id_ville_selected = $this->input->post("id_ville_selected");
        if (isset($id_ville_selected) && $id_ville_selected != "0" && $id_ville_selected != "") {
            $vsv_object = $this->vivresaville_villes->getById($id_ville_selected);
            if (isset($vsv_object) && isset($vsv_object->id_ville)) {
                $localdata_IdVille_all = array();
                $localdata_IdVille_all[] = $vsv_object->id_ville;
                $this->session->set_userdata('localdata_IdVille', $vsv_object->id_ville);
                $this->session->set_userdata('localdata_IdVille_parent', $vsv_object->id_ville);
                $this->session->set_userdata('localdata_IdVsv', $id_ville_selected);
                $vsv_ville_other_data = $this->vsv_ville_other->getByIdVsv($id_ville_selected);
                if (isset($vsv_ville_other_data) && count($vsv_ville_other_data) > 0) {
                    foreach ($vsv_ville_other_data as $vsv_ville_other_datum) {
                        $localdata_IdVille_all[] = $vsv_ville_other_datum->id_ville;
                    }
                }
                $this->session->set_userdata('localdata_IdVille_all', $localdata_IdVille_all);
                redirect("vivresaville/Accueil");
            }
        }
        $villes = $this->vivresaville_villes->getAl1active();
        $data['villes'] = $villes;
        $data['vivresaville_villes'] = $this->vivresaville_villes;
        $data['vsv_ville_other'] = $this->vsv_ville_other;
        $data['pagecategory'] = "list_vivresaville";
        //$this->load->view('vivresavilleView/ChoixVille', $data);
        $this->load->view('new_vsv/accueil_select_index', $data);
    }

    function CheckVille($id_ville='0') {
        //echo $id_ville;
        $id_ville_selected = $id_ville;
        if (isset($id_ville_selected) && $id_ville_selected != "0" && $id_ville_selected != "") {
            $vsv_object = $this->vivresaville_villes->getById($id_ville_selected);
            if (isset($vsv_object) && isset($vsv_object->id_ville)) {
                $localdata_IdVille_all = array();
                $localdata_IdVille_all[] = $vsv_object->id_ville;
                $this->session->set_userdata('localdata_IdVille', $vsv_object->id_ville);
                $this->session->set_userdata('localdata_IdVille_parent', $vsv_object->id_ville);
                $this->session->set_userdata('localdata_IdVsv', $id_ville_selected);
                $vsv_ville_other_data = $this->vsv_ville_other->getByIdVsv($id_ville_selected);
                if (isset($vsv_ville_other_data) && count($vsv_ville_other_data) > 0) {
                    foreach ($vsv_ville_other_data as $vsv_ville_other_datum) {
                        $localdata_IdVille_all[] = $vsv_ville_other_datum->id_ville;
                    }
                }
                $this->session->set_userdata('localdata_IdVille_all', $localdata_IdVille_all);
                redirect("vivresaville/Accueil");
            } else {
                redirect('vivresaville/ListVille');
            }
        } else {
            redirect('vivresaville/ListVille');
        }
    }


    function infos($info_type = "")
    {
        $localdata_IdVille = $this->session->userdata('localdata_IdVille');
        if (isset($localdata_IdVille) && $localdata_IdVille != "") {
            redirect("vivresaville/Accueil");
        }

        $id_ville_selected = $this->input->post("id_ville_selected");
        if (isset($id_ville_selected) && $id_ville_selected != "0" && $id_ville_selected != "") {
            $vsv_object = $this->vivresaville_villes->getById($id_ville_selected);
            if (isset($vsv_object) && isset($vsv_object->id_ville)) {
                $this->session->set_userdata('localdata_IdVille', $vsv_object->id_ville);
                redirect("vivresaville/Accueil");
            }
        }
        $villes = $this->vivresaville_villes->getAl1active();
        $data['villes'] = $villes;
        $data['pagecategory'] = "list_vivresaville";
        $data['info_type'] = $info_type;
        //$this->load->view('vivresavilleView/ChoixVille', $data);
        $this->load->view('vivresaville/accueil_select_index', $data);
    }
}

?>