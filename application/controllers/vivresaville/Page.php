<?php 
	Class page extends CI_Controller
	{
       function __construct()
        {
            parent::__construct();
            $this->load->model("vivresaville_villes");
            $this->load->model("vsv_ville_other");
            $this->load->model("mdl_ville");
            $this->load->model("mdlville");
            $this->load->library('session');
        }

        function index($link="")
        {
            $localdata_IdVille = $this->session->userdata('localdata_IdVille');
            if (isset($localdata_IdVille) && $localdata_IdVille!="0" && $localdata_IdVille!="") {
                $vsv_object = $this->vivresaville_villes->getByIdVille($localdata_IdVille);
                if (isset($vsv_object) && isset($vsv_object->id_ville)) {
                    if (isset($link) && $link != "") {
                        $data['link'] = $link;
                        $this->load->view('vivresaville/page_index', $data);
                    } else {
                        redirect("vivresaville/accueil");
                    }
                } else {
                    redirect("vivresaville/ListVille");
                }
            } else redirect("vivresaville/ListVille");

        }

        function liste_autre_ville($id_vivresaville=0) {
            $post_id_vivresaville = $this->input->post('id_vivresaville');
            if (isset($post_id_vivresaville)) $id_vivresaville = $post_id_vivresaville;
            if (isset($id_vivresaville) && is_numeric($id_vivresaville) && $id_vivresaville!=0 && $id_vivresaville!="") {
                $ville_data = $this->vsv_ville_other->getByIdVsv($id_vivresaville);
                $data['ville_data'] = $ville_data;
                $data['pagecategory'] = 'vsv_home';
                $this->load->view('vivresaville/list_ville_other', $data);
            }
        }
        function delete_autre_ville($id_vivresaville=0, $id_autre_ville=0) {
            if (isset($id_vivresaville) && is_numeric($id_vivresaville) && $id_vivresaville!=0 && $id_vivresaville!="") {
                if (isset($id_autre_ville) && is_numeric($id_autre_ville) && $id_autre_ville!=0 && $id_autre_ville!="") {
                    $data['id_vivresaville'] = $id_vivresaville;
                    $data['id_autre_ville'] = $id_autre_ville;
                    $this->load->view('vivresaville/delete_ville_other', $data);
                }
            }
        }
        function delete_autre_ville_confirmed($id_vivresaville=0, $id_autre_ville=0) {
            if (isset($id_vivresaville) && is_numeric($id_vivresaville) && $id_vivresaville!=0 && $id_vivresaville!="") {
                if (isset($id_autre_ville) && is_numeric($id_autre_ville) && $id_autre_ville!=0 && $id_autre_ville!="") {
                    $data['id_vivresaville'] = $id_vivresaville;
                    $data['id_autre_ville'] = $id_autre_ville;
                    $this->vsv_ville_other->deleteByIdVsv($id_vivresaville,$id_autre_ville);
                    $this->load->view('vivresaville/delete_ville_other_confirmed', $data);
                }
            }
        }
        function edit_autre_ville($id_vivresaville=0, $id_autre_ville=0) {
            $ville_other = $this->input->post("ville_other");
            if (isset($ville_other) && $ville_other!=false) {
                if ($ville_other['id']=="0" || $ville_other['id']=="") {
                    $ville_other['id'] = null;
                    $IdResult = $this->vsv_ville_other->insert($ville_other);
                } else {
                    $IdResult = $this->vsv_ville_other->update($ville_other);
                }
                $data['IdResult'] = $IdResult;
                $data['ville_other'] = $ville_other;
                if (isset($IdResult) && is_numeric($IdResult))
                    $this->load->view('vivresaville/result_ville_other', $data);
            } else {
                if (isset($id_vivresaville) && is_numeric($id_vivresaville) && $id_vivresaville!=0 && $id_vivresaville!="") {
                    $vsv_data = $this->vivresaville_villes->getById($id_vivresaville);
                    $data['vsv_data'] = $vsv_data;
                    $ville_data = $this->vsv_ville_other->getById($id_autre_ville);
                    $data['ville_data'] = $ville_data;
                    $data['pagecategory'] = 'vsv_home';
                    $data['mdl_ville'] = $this->mdl_ville;
                    $this->load->view('vivresaville/edit_ville_other', $data);
                }
            }
        }

        function getVIlleList()
        {
            $CodePostal = $this->input->post("vsv_postalcode_main");
            $departement_id = $this->input->post("vsv_id_department");
            $departements = $this->mdlville->GetVilleByCodePostal($CodePostal, $departement_id);
            $result_to_show = '<select name="ville_other[id_ville]" id="formGroupVilleId" class="form-control p-1">';
            if (isset($departements)) {
                foreach ($departements as $item) {
                    $result_to_show .= '<option value="' . $item->IdVille . '">' . $item->Nom . '</option>';
                }
            }
            $result_to_show .= '</select>';
            echo $result_to_show;
        }
	}

 ?>