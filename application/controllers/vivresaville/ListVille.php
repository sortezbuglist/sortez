<?php

Class ListVille extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("vivresaville_villes");
        $this->load->model("vsv_ville_other");
        $this->load->model("vsv_ville_reference");
        $this->load->library('session');
        $this->load->Model("mdlstatut");
        $this->load->model("user");
        $this->load->helper('captcha');
        $this->load->model("mdlville");
        $this->load->Model("mdldepartement");
        $this->load->model("SousRubrique");
        $this->load->Model("Rubrique");
        $this->load->model('Mdl_abonnementInscription');
    }

    function index($type_page ="")
    {
        //check if there is ville_id referenced with ip address
        $ip_adress = $this->input->ip_address();
        $objVilleReference = $this->vsv_ville_reference->getWhere(' ip="'.$ip_adress.'" and id_ville!=0 and id_ville IS NOT NULL ');
        if (isset($objVilleReference) && count($objVilleReference)>0) {
            $vsv_object_checking = $this->vivresaville_villes->getWhere(" id_ville=".$objVilleReference[0]->id_ville);
            if (isset($vsv_object_checking) && count($vsv_object_checking)>0)
                redirect('vivresaville/ListVille/CheckVille/'.$vsv_object_checking[0]->id);
        }

        $localdata_IdVille = $this->session->userdata('localdata_IdVille');
        if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != false) {
            redirect("vivresaville/Accueil");
        }

        $id_ville_selected = $this->input->post("id_ville_selected");
        if (isset($id_ville_selected) && $id_ville_selected != "0" && $id_ville_selected != "") {
            $vsv_object = $this->vivresaville_villes->getById($id_ville_selected);
            if (isset($vsv_object) && isset($vsv_object->id_ville)) {
                $localdata_IdVille_all = array();
                $localdata_IdVille_all[] = $vsv_object->id_ville;
                $this->session->set_userdata('localdata_IdVille', $vsv_object->id_ville);
                $this->session->set_userdata('localdata_IdVille_parent', $vsv_object->id_ville);
                $this->session->set_userdata('localdata_IdVsv', $id_ville_selected);
                $vsv_ville_other_data = $this->vsv_ville_other->getByIdVsv($id_ville_selected);
                if (isset($vsv_ville_other_data) && count($vsv_ville_other_data) > 0) {
                    foreach ($vsv_ville_other_data as $vsv_ville_other_datum) {
                        $localdata_IdVille_all[] = $vsv_ville_other_datum->id_ville;
                    }
                }
                $this->session->set_userdata('localdata_IdVille_all', $localdata_IdVille_all);
                redirect("vivresaville/Accueil");
            }
        }
        $villes = $this->vivresaville_villes->getAl1active();
        $data['villes'] = $villes;
        $data['vivresaville_villes'] = $this->vivresaville_villes;
        $data['vsv_ville_other'] = $this->vsv_ville_other;
        $data['pagecategory'] = "list_vivresaville";
        $data['type_page'] = $type_page;
        //$this->load->view('vivresavilleView/ChoixVille', $data);
        $this->load->view('new_vsv/accueil_select_index', $data);
    }

    function CheckVille($id_ville='0') {
        //echo $id_ville;
        $id_ville_selected = $id_ville;
        if (isset($id_ville_selected) && $id_ville_selected != "0" && $id_ville_selected != "") {
            $vsv_object = $this->vivresaville_villes->getById($id_ville_selected);
            if (isset($vsv_object) && isset($vsv_object->id_ville)) {
                $localdata_IdVille_all = array();
                $localdata_IdVille_all[] = $vsv_object->id_ville;
                $this->session->set_userdata('localdata_IdVille', $vsv_object->id_ville);
                $this->session->set_userdata('localdata_IdVille_parent', $vsv_object->id_ville);
                $this->session->set_userdata('localdata_IdVsv', $id_ville_selected);
                $vsv_ville_other_data = $this->vsv_ville_other->getByIdVsv($id_ville_selected);
                if (isset($vsv_ville_other_data) && count($vsv_ville_other_data) > 0) {
                    foreach ($vsv_ville_other_data as $vsv_ville_other_datum) {
                        $localdata_IdVille_all[] = $vsv_ville_other_datum->id_ville;
                    }
                }
                $this->session->set_userdata('localdata_IdVille_all', $localdata_IdVille_all);
                redirect("vivresaville/Accueil");
            } else {
                redirect('vivresaville/ListVille');
            }
        } else {
            redirect('vivresaville/ListVille');
        }
    }


    function infos($info_type = "")
    {
        $localdata_IdVille = $this->session->userdata('localdata_IdVille');
        if (isset($localdata_IdVille) && $localdata_IdVille != "") {
            redirect("vivresaville/Accueil");
        }

        $id_ville_selected = $this->input->post("id_ville_selected");
        if (isset($id_ville_selected) && $id_ville_selected != "0" && $id_ville_selected != "") {
            $vsv_object = $this->vivresaville_villes->getById($id_ville_selected);
            if (isset($vsv_object) && isset($vsv_object->id_ville)) {
                $this->session->set_userdata('localdata_IdVille', $vsv_object->id_ville);
                redirect("vivresaville/Accueil");
            }
        }
        $villes = $this->vivresaville_villes->getAl1active();
        $data['villes'] = $villes;
        $data['pagecategory'] = "list_vivresaville";
        $data['info_type'] = $info_type;
        //$this->load->view('vivresavilleView/ChoixVille', $data);
        $this->load->view('vivresaville/accueil_select_index', $data);
    }
    public function save_vivresaville_account(){
        $data = $this->input->post("Societe");

            $data['mdp']= $this->input->post('mdp');
            $data['confirmmdp']= $this->input->post('confirmmdp');
           
    $save = $this->Mdl_abonnementInscription->create($data);

// var_dump($save);

    if (!empty($save)) {
        // code...
    

                $colDestAdmin = array();
                $colDestAdmin[] = array("Email" => $this->config->item("mail_sender_adress"), "Name" => $this->config->item("mail_sender_name"));
                // $colDestAdmin[] = array("Email" => "alphadev@randevteam.com", "Name" => $this->config->item("mail_sender_name"));
                $txtSujetAdmin = "Notification inscription nouveau Compte vivresaville";
                 $message_html_admin = '
            <p>Bonjour ,</p>
                    <p>Quelqu\'un vient de cr&eacute;er un compte Vivresaville gratuit chez Sortez.org</p>
                    <p>ci-dessous les details : <br/>
        ';
           $message_html_admin .= "Nom : " . $data['NomSociete'] . "<br/>";
           $message_html_admin .= "Login : " . $data['Login'] . "<br/>";
           $message_html_admin .= "Mot de passe : " . $data['mdp'] . "<br/></p>";
           


        
        $send_mail_admin = @envoi_notification($colDestAdmin,$txtSujetAdmin,$message_html_admin);

        $txtSujet = "Notification inscription à un compte Vivresaville";

        $colDest[] = array("Email" => $data['Email'], "Name" => $data['NomSociete']);
        // $colDest[] = array("Email" => "kappadev@randevteam.com", "Name" => $this->config->item("mail_sender_name"));
                $txtSujet = "Notification inscription nouveau Compte vivresaville";
                 $message = '
            <p>Bonjour ,</p>
                    <p>Vous venez de cr&eacute;er un compte Vivresaville gratuit chez Sortez.org</p>
                    <p>ci-dessous les accès : 
        ';
        $message .= "Nom : " . $data['NomSociete'] . "<br/>";
        $message .= "Votre Login : " . $data['Login'] . "<br/>";
        $message .= "Mot de passe : " . $data['mdp'] . "<br/></p>";
         
        $send_mail = @envoi_notification($colDest,$txtSujet,$message);
        // var_dump($message_html_admin);



        }
        $this->load->view('privicarte/remerciment', $data);

    } 

// ****************************************************************/
/***********************************DEMANDE DE DEVIS VSV****************************/
    // *******************************************************************
    function formulaireSouscriptionvsv(){
        $data["colStatut"] = $this->mdlstatut->GetAll();
        $data["colVilles"] = $this->mdlville->GetAll();
        $data["colDepartement"] = $this->mdldepartement->GetAll();
        // $departements = $this->mdldepartement->GetDepByCodePostal($CodePostal);        

        // var_dump($data['colDepartement']);

        $this->load->view("privicarte/vwDemandeDevis",$data);    
    }
    function souscriptionAbonnementPremiumVsv(){


        // $data["colStatut"] = $this->mdlstatut->GetAll();
        // $data["colVilles"] = $this->mdlville->GetAll();
        
                           
        $data = $this->input->post("Societe");
        $data['mdp']= $this->input->post('mdp');
        $data['confirmmdp']= $this->input->post('confirmmdp');

        $mode=$_REQUEST['mode'];
        if($mode!=null){
           
           

            $this->Mdl_abonnementInscription->create($data);
        }

        $Choix1 = $this->input->post("choix1");

        $Choix2 = $this->input->post("choix2");
        
        // $data['IdVille_Nom_text']= $this->input->post("IdVille_Nom_text");
        // $VilleRecup = $_POST['VilleRecup'];
        // var_dump($data['IdVille_Nom_text']);
        // var_dump($data['IdVille']);
        // $departementHidden=$this->input->post('departementHidden');
        
        
        if (isset($Choix1['pack_infos'])) $txtContenuAdmin .= "<li>01. Pack infos (actualité et agenda) : Quota de 50 articles & 50 événements";
        if (isset($Choix1['boutiques'])) $txtContenuAdmin .= "<li>02. Les boutiques en ligne : Quota de 50 annonces</li>";
        if (isset($Choix1['pack_promos'])) $txtContenuAdmin .= "<li>03. Pack &#34;Promos&#34; : 5 bons plans et une condition de fidélisation</li>";
        if (isset($Choix1['pack_restaurant'])) $txtContenuAdmin .= "<li>04. Pack Restaurant : Réservations de tables, plats du jour et menu digital</li>";
        if (isset($Choix1['pack_gites'])) $txtContenuAdmin .= "<li>05. Pack &#34;Gîtes et chambres d&#39;hôtes&#34; : Réservations et boutique en ligne</li>";
        if (isset($Choix1['module_graphiques'])) $txtContenuAdmin .= "<li>06. Module graphique &#34;Platinium&#34;</li>";
        if (isset($Choix1['module_referencement'])) $txtContenuAdmin .= "<li>07. Module de référencement &#34;Google : uniquement avec l&#39;abonnement &#34;Platinium&#34;;</li>";



        if (isset($Choix2['pack_exports'])) $txtContenuAdmin1 .= "<li>08. Pack export (sur devis) : agenda, actualité, boutique, bons plans, pack restaurant</li>";
        if (isset($Choix2['aide_creation'])) $txtContenuAdmin1 .= "<li>09. Aide à la création de vos pages (sur devis)</li>";
        if (isset($Choix2['creation_gestion'])) $txtContenuAdmin1 .= "<li>10. Création et gestion de vos réseaux sociaux</li>";
        if (isset($Choix2['realisation_video'])) $txtContenuAdmin1 .= "<li>11. Réalisation d&#39;une vidéo de présentation</li>";
        if (isset($Choix2['pub_magazine'])) $txtContenuAdmin1 .= "<li>12. Publicité sur le Magazine Sortez</li>";
        if (isset($Choix2['impression'])) $txtContenuAdmin1 .= "<li>13. L&#39;impression sur toutes ses formes ! Sortez département Print<br/>";
        if (isset($Choix2['vsv'])) $txtContenuAdmin1 .= "<li>14. vivresaville.fr</li>";
        // if (isset($Choix2['vsv'])) $txtContenuAdmin .= "<li>15. vivresaville.fr</li>";
    

        $headers = 'From: Sortez.org' . "\r\n" .
            'Reply-To: webmaster@example.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n"; 
        $subject = "Demande de souscription à un abonnement Premium Vivresaville.fr";
        $message = 
            "<p>Bonjour,<br/>
                Demande de souscription à un abonnement Premium Vivresaville.fr<br/>
                ci-dessous les d&eacute;tails,
            </p>
            <hr/>                        
            <div>
                <ul>
                    <li><strong>Type abonnement</strong>
                        <ul>
                            Abonnement Premium Vivresaville
                        </ul>
                    </li>    
                    <li><strong>Liste des packs choisis</strong>
                        <ul>
                            ".$txtContenuAdmin."
                        </ul>
                    </li>
                    <li><strong>Les autres services optionels</strong>
                        <ul>
                            ".$txtContenuAdmin1."
                        </ul>
                    </li>                                   
                </ul>
            </div>
            <div>    
                <ul>
                    <li style=''><strong>Les coordonnées de votre correspondant</strong></li>
                        <ul>
                            <li>Nom et Prenom: ".$data['nom']."</li>                                                     
                            <li>Email: ".$data['Email_decideur']."</li>
                            <li>Tél Direct: ".$data['TelDirect']."</li>
                            <li>Fonction: ".$data['Responsabilite']."</li>
                                                        
                        </ul>                    
                    <li><strong>Les coordonnées de votre établissement</strong>
                        <ul>
                            <li>Nom ou Enseigne: ".$data['NomSociete']."</li>
                            
                            <li>Votre activité: ".$data['IdRubrique']."</li>
                            <li>Adresse: ".$data['Adresse']."</li>
                            <li>Code postal: ".$data['CodePostal']."</li>
                            
                            <li>Tél fixe: ".$data['TelFixe']."</li>
                            <li>Email Societe: ".$data['Email']."</li>
                            
                        </ul>
                    </li>                    
                </ul>                            
            </div>

            Cordialement,
            <br/>Sortez.org,";

        // $envoyeur =$data['Nom'].' '.$data['Prenom'];
        // $mailEnvoyeur = $data['Email_decidResp'];
        // var_dump($envoyeur);
        // var_dump($mailEnvoyeur);
            // $envoyeur="RAKOTO";
            // $mailEnvoyeur = "rakoto@gmail.com"; 


        // $to= 'alphadev@randevteam.com';
        // $to2      = 'izheritiana@gmail.com';
        // $to2='randev@randev.ovh';
        // $to2='william.arthur.harilantoniaina@gmail.com';

        $to= 'webmaster@sortez.org';
        $to2= $data['Email'];

        $webMasterSortez[] = array("Email" => $to, "Name" => 'Sortez');
        $contactSortez[] = array("Email" => $to2, "Name" => 'Sortez');
        // $prmDestinataires2[] = array("Email" => $to2, "Name" => 'Sortez');
        // envoi_notification($webMasterSortez, $subject, $message, $prmEnvoyeur = $mail_societe, $prmEnvoyeurName = $nom_societe);
        // envoi_notification($contactSortez, $subject, $message, $prmEnvoyeur = $mail_societe, $prmEnvoyeurName = $nom_societe);


         $send= @envoi_notification($webMasterSortez, $subject, $message);

            $send2= @envoi_notification($contactSortez, $subject, $message);

             if ($send2 && $send) {
                // var_dump($mode);
                $data['mode']=$mode;
                 $this->load->view('privicarte/remerciment', $data);
             }
             
        

        
    }    
    // ****************************************************************/
/***********************************FIN DEMANDE DEVIS VSV****************************/
    // *******************************************************************









    function createAbonnementInscription(){

        $data["colStatut"] = $this->mdlstatut->GetAll();
        $data["colVilles"] = $this->mdlville->GetAll();
        $data["colDepartement"] = $this->mdldepartement->GetAll();
        $data["colRubriques"] = $this->Rubrique->GetAll();
        $data["colSousRubriques"] = $this->SousRubrique->GetAll();
        $data['mode']=$_REQUEST['mode'];

        // if ($data['mode']!=null) {

        // $data['view']= $this->load->view('vivresaville/pack_vivresaville');
        // $data['Societe']= $this->input->post('Societe');
        // }
    
       

        $this->load->view('vivresaville/vwFormulaireinscription', $data);
        
            
             

    }
    function departementcp()
    {
        $CodePostal = $this->input->post("CodePostalSociete");
        // var_dump($CodePostal);
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdldepartement->GetDepByCodePostal($CodePostal);
        //echo $CodePostal;
        $result_to_show = '<select name="Societe[departement_id]" id="departement_id" class="form-control stl_long_input_platinum" onchange="javascript:CP_getVille_D_CP();" style="height: 35px;">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .= '<option value="' . $item->departement_id . '">' . $item->departement_nom . '</option>';
            }
        }
        $result_to_show .= '</select>';
        echo $result_to_show;
    }
     function villecp()
    {
        $CodePostal = $this->input->post("CodePostalSociete");
        $departement_id = $this->input->post("departement_id");
        if (isset($departement_id) && $departement_id!="" && $departement_id!="0") $departement_id = $this->mdldepartement->getById($departement_id)->departement_code;
        $departements = $this->mdlville->GetVilleByCodePostal($CodePostal, $departement_id);
        //echo $CodePostal;
        $result_to_show = '<select name="Societe[IdVille]" id="VilleSociete" class="stl_long_input_platinum form-control"  onchange="getLatitudeLongitudeAdresse();" style="height: 35px;">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .= '<option value="' . $item->IdVille . '">' . $item->Nom . '</option>';
            }
        }
        $result_to_show .= '</select>';
        echo $result_to_show;

    }
     public function test_captcha(){
        $resp=$this->input->post("g-recaptcha-response");
        // if (isset($resp) AND $resp != null && $resp !="") {
            $data["captcha"]="OK";
        // }
        // else {
        //     $data["captcha"]="NO";
        // }
        $http_code = 200;
        header('Content-type: json');
        header('HTTP/1.1: ' . $http_code);
        header('Status: ' . $http_code);
        exit(json_encode((array)$data));
    }
       function verifier_email()
    {
        $txtEmail = $this->input->post("txtEmail_var");
        $oEmail = $this->user->verifier_mailUserPro($txtEmail);
        $emailExist = count($oEmail);
        $reponse = "0";
        $data['reponse'] = "0";
        if ($emailExist != "0") {
            $reponse = "1";
            //$data['reponse']="1";
            echo $reponse;
        } else {
            $reponse = "0";
            //$data['reponse']="0";
            echo $reponse;
        }

    }
     function verifier_login_ionauth()
    {
        $txtLogin = $this->input->post("txtLogin_var_ionauth");
        $oEmail = $this->user->verifier_loginUser_ionauth($txtLogin);
        $emailExist = count($oEmail);
        $reponse = "0";
        $data['reponse'] = "0";
        if ($emailExist != "0") {
            $reponse = "1";
            //$data['reponse']="1";
            echo $reponse;
        } else {
            $reponse = "0";
            //$data['reponse']="0";
            echo $reponse;
        }

    }
      function verifier_email_ionauth()
    {
        $txtEmail = $this->input->post("txtEmail_var_ionauth");
        $oEmail = $this->user->verifier_mailUser_ionauth($txtEmail);
        $emailExist = count($oEmail);
        $reponse = "0";
        $data['reponse'] = "0";
        if ($emailExist != "0") {
            $reponse = "1";
            //$data['reponse']="1";
            echo $reponse;
        } else {
            $reponse = "0";
            //$data['reponse']="0";
            echo $reponse;
        }

    }

    function verifier_login()
    {
        $txtLogin = $this->input->post("txtLogin_var");
        $oEmail = $this->user->verifier_loginUserPro($txtLogin);
        $emailExist = count($oEmail);
        $reponse = "0";
        $data['reponse'] = "0";
        if ($emailExist != "0") {
            $reponse = "1";
            //$data['reponse']="1";
            echo $reponse;
        } else {
            $reponse = "0";
            //$data['reponse']="0";
            echo $reponse;
        }

    }

    function getPostalCode($idVille = 0)
    {
        $oville = $this->mdlville->getVilleById($idVille);
        $data["cp"] = $oville->CodePostal;

        $data['idVille'] = $idVille;
        //$this->load->view("front/vwReponseVille", $data);
        echo $oville->CodePostal;
    }
    function testAjax($id = "")
    {
        if ($id != ""){
            $data["colSousRubriques"] = $this->SousRubrique->GetByRubrique($id);
            $data['id'] = $id;
            $this->load->view("vivresaville/vwReponseRub", $data);
        }
    }

}


?>