<?php
class home extends CI_Controller{
    
    function __construct() {
        parent::__construct();

		$this->load->library('session');
        $this->load->library('ion_auth');

        $this->load->model("mdl_agenda");

        if (!$this->ion_auth->in_group(6)) {
			$this->session->set_flashdata('domain_from', '1');
			redirect("/");
		}

        $this->load->model("mdlbonplan") ;
        $this->load->model("mdlcategorie") ;
        $this->load->model("mdlville") ;
        $this->load->model("mdldepartement") ;
        $this->load->model("mdlbonplanpagination") ;
        $this->load->model("mdlcommercant") ;
        $this->load->model("Commercant") ;
        $this->load->library('pagination');
        $this->load->model("AssCommercantAbonnement") ;
        $this->load->model("mdlannonce") ;
        $this->load->model("Abonnement");
        $this->load->model("rubrique") ;
        $this->load->model("notification") ;
        
        $this->load->library("pagination");

        $this->load->model("ion_auth_used_by_club");

        check_vivresaville_id_ville();
    }
    
    function index(){

        
        $data["no_main_menu_home"] = "1";
        $data["no_left_menu_home"] = "1";

        //this function delete storage cache directory
        //$base_path_system_rand = str_replace('system/', '', BASEPATH);
        //$validation_delete_path_rand = delete_directory_rand($base_path_system_rand."/pagecaracteristics");
        //this function delete storage cache directory

        ////////////////DELETE OLD AGENDA date_fin past 8 days
        $this->mdl_agenda->deleteOldAgenda_fin8jours();

        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $idAssociation = $this->ion_auth_used_by_club->get_association_id_from_ion_id($user_ion_auth->id);
            $data['idAssociation'] = $idAssociation;
        }

        $data["mdldepartement"] = $this->mdldepartement;
        $data["mdlville"] = $this->mdlville;

        $data['pagename'] = 'associationhome';

        $this->load->view("association/vwHome", $data);
      
    }
}