<?php
class professionnels extends CI_Controller {

    function __construct() {
        parent::__construct();
		$this->load->model("SousRubrique") ;
		$this->load->model("mdlcadeau") ;
		$this->load->model("user") ;
		$this->load->Model("Commercant");
		$this->load->Model("mdl_mail_activation_bon_plan");
		$this->load->model("mdlville") ;
		$this->load->model("mdlglissiere") ;
		$this->load->model("AssCommercantAbonnement") ;
        $this->load->Model("Rubrique");
        $this->load->Model("SousRubrique");
        $this->load->Model("Ville");
        $this->load->Model("Abonnement");
        $this->load->Model("mdlstatut");
        $this->load->helper('captcha');
        $this->load->library('image_moo');
        $this->load->Model("mdldepartement");
        $this->load->Model("mdlcommune");
        $this->load->Model("mdldemande_abonnement");
        $this->load->model("parametre");
		
		$this->load->library('session');

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");


        check_vivresaville_id_ville();

        if (!$this->ion_auth->in_group(6)) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("/");
        }
		
    }
    
    function index(){
        $group_proo_club = array(3,4,5);
        if (!$this->ion_auth->in_group($group_proo_club)) {
            $this->session->set_userdata("cur_uri", $this->uri->uri_string());
			$this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }

    function allcommercant(){
        $allcommercant = $this->Commercant->GetAll();
        if (isset($allcommercant)) echo count($allcommercant); else "0";
    }

    function villecp() {
    	$CodePostal = $this->input->post("CodePostalSociete");
    	$departement_id = $this->input->post("departement_id");
        if (isset($departement_id) && $departement_id!="" && $departement_id!="0") $departement_id = $this->mdldepartement->getById($departement_id)->departement_code;
        $departements = $this->mdlville->GetVilleByCodePostal($CodePostal, $departement_id);
        //echo $CodePostal;
        $result_to_show = '<select name="Societe[IdVille]" id="VilleSociete" class="stl_long_input_platinum"  onchange="getLatitudeLongitudeAdresse();">';
        if (isset($departements)) {
        	foreach ($departements as $item) {
        		$result_to_show .=  '<option value="'.$item->IdVille.'">'.$item->Nom.'</option>';
        	}
        }
        $result_to_show .=  '</select>';
        echo $result_to_show;

    }

    function villecp_agenda() {
        $CodePostal = $this->input->post("CodePostalSociete");
        $departement_id = $this->input->post("departement_id");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdlville->GetVilleByCodePostal($CodePostal, $departement_id);
        //echo $CodePostal;
        $result_to_show = '<select name="Agenda[IdVille]" id="IdVille" class="stl_long_input_platinum">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .=  '<option value="'.$item->IdVille.'">'.$item->Nom.'</option>';
            }
        }
        $result_to_show .=  '</select>';
        echo $result_to_show;

    }

    function villecp_agenda_localisation() {
        $CodePostal = $this->input->post("CodePostalSociete");
        $departement_id = $this->input->post("departement_id");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdlville->GetVilleByCodePostal($CodePostal, $departement_id);
        //echo $CodePostal;
        $result_to_show = '<select name="Agenda[IdVille_localisation]" id="IdVille_localisation" class="stl_long_input_platinum">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .=  '<option value="'.$item->IdVille.'">'.$item->Nom.'</option>';
            }
        }
        $result_to_show .=  '</select>';
        echo $result_to_show;

    }



    function villecp_particulier() {
        $CodePostal = $this->input->post("CodePostalSociete");
        $departement_id = $this->input->post("departement_id");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdlville->GetVilleByCodePostal($CodePostal, $departement_id);
        //echo $CodePostal;
        $result_to_show = '<select name="Particulier[IdVille]" id="txtVille" class="stl_long_input_platinum" style="width:273px;">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .=  '<option value="'.$item->IdVille.'">'.$item->Nom.'</option>';
            }
        }
        $result_to_show .=  '</select>';
        echo $result_to_show;

    }

    function villecp_localisation() {
    	$CodePostal = $this->input->post("CodePostalSociete");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdlville->GetVilleByCodePostal_localisation($CodePostal);
        //echo $CodePostal;
        $result_to_show = '<select name="Societe[IdVille_localisation]" id="IdVille_localisationSociete" onchange="getLatitudeLongitudeLocalisation();" style="width:413px;">';
        if (isset($departements)) {
        	foreach ($departements as $item) {
        		$result_to_show .=  '<option value="'.$item->IdVille.'">'.$item->Nom.'</option>';
        	}
        }
        $result_to_show .=  '</select>';
        echo $result_to_show;

    }

    function departementcp () {
        $CodePostal = $this->input->post("CodePostalSociete");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdldepartement->GetDepByCodePostal($CodePostal);
        //echo $CodePostal;
        $result_to_show = '<select name="Societe[departement_id]" id="departement_id" class="stl_long_input_platinum" onchange="javascript:CP_getVille_D_CP();">';
        if (isset($departements)) {
        	foreach ($departements as $item) {
        		$result_to_show .=  '<option value="'.$item->departement_id.'">'.$item->departement_nom.'</option>';
        	}
        }
        $result_to_show .=  '</select>';
        echo $result_to_show;
    }

    function departementcp_agenda () {
        $CodePostal = $this->input->post("CodePostalSociete");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdldepartement->GetDepByCodePostal($CodePostal);
        //echo $CodePostal;
        $result_to_show = '<select name="Agenda[departement_id]" id="DepartementAgenda" class="stl_long_input_platinum" onchange="javascript:CP_getVille_D_CP();">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .=  '<option value="'.$item->departement_id.'">'.$item->departement_nom.'</option>';
            }
        }
        $result_to_show .=  '</select>';
        echo $result_to_show;
    }


    function departementcp_agenda_localisation () {
        $CodePostal = $this->input->post("CodePostalSociete");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdldepartement->GetDepByCodePostal($CodePostal);
        //echo $CodePostal;
        $result_to_show = '<select name="Agenda[departement_id_localisation]" id="DepartementAgendaLocalisation" class="stl_long_input_platinum" onchange="javascript:CP_getVille_D_CP_localisation();">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .=  '<option value="'.$item->departement_id.'">'.$item->departement_nom.'</option>';
            }
        }
        $result_to_show .=  '</select>';
        echo $result_to_show;
    }


    function departementcp_particulier () {
        $CodePostal = $this->input->post("CodePostalSociete");
        //if (isset($CodePostal)) $CodePostal = $this->input->post("CodePostal"); else $CodePostal = 0;
        $departements = $this->mdldepartement->GetDepByCodePostal($CodePostal);
        //echo $CodePostal;
        $result_to_show = '<select name="Societe[departement_id]" id="departement_id" class="stl_long_input_platinum" onchange="javascript:CP_getVille_D_CP();"  style="width:273px;">';
        if (isset($departements)) {
            foreach ($departements as $item) {
                $result_to_show .=  '<option value="'.$item->departement_id.'">'.$item->departement_nom.'</option>';
            }
        }
        $result_to_show .=  '</select>';
        echo $result_to_show;
    }
    
	function ajaxRubrique(){
	 // GetByRubrique
	}
	 function testAjax($id = ""){
        $data["colSousRubriques"] = $this->SousRubrique->GetByRubrique($id);
        $data['id']=$id;
            $this->load->view("front/vwReponseRub", $data);
     }  

     function TwitterPrivicarteForm() {
     	 $data['data'] = "";
         $this->load->view("privicarte/TwitterPrivicarteForm", $data);
     }

     function TwitterProForm($IdCommercant) {
         $data['IdCommercant'] = $IdCommercant;
         $data['oInfoCommercant'] = $this->Commercant->getById($IdCommercant);
         $this->load->view("privicarte/TwitterProForm", $data);
     }

     function FacebookPrivicarteForm() {
     	 $data['data'] = "";
         $this->load->view("privicarte/FacebookPrivicarteForm", $data);
     }

     function FacebookProForm($IdCommercant) {
     	 $data['IdCommercant'] = $IdCommercant;
         $data['oInfoCommercant'] = $this->Commercant->getById($IdCommercant);
         $this->load->view("privicarte/FacebookProForm", $data);
     }

     function GoogleplusPrivicarteForm() {
     	 $data['data'] = "";
         $this->load->view("privicarte/GoogleplusPrivicarteForm", $data);
     }

     function GoogleplusProForm($IdCommercant) {
         $data['IdCommercant'] = $IdCommercant;
         $data['oInfoCommercant'] = $this->Commercant->getById($IdCommercant);
         $this->load->view("privicarte/GoogleplusProForm", $data);
     }

     function FacebookPrivicarteForm_share() {
     	 $data['data'] = "";
         $this->load->view("privicarte/FacebookPrivicarteForm_share", $data);
     }

     function TwitterPrivicarteForm_share() {
     	 $data['data'] = "";
         $this->load->view("privicarte/TwitterPrivicarteForm_share", $data);
     }

     function GoogleplusPrivicarteForm_share() {
     	 $data['data'] = "";
         $this->load->view("privicarte/GoogleplusPrivicarteForm_share", $data);
     }


     function contact_partner_sendmail(){

        $contact_partner_nom = $this->input->post("contact_partner_nom");
        $contact_partner_tel = $this->input->post("contact_partner_tel");
        $contact_partner_mail = $this->input->post("contact_partner_mail");
        $contact_partner_msg = $this->input->post("contact_partner_msg");
        $contact_partner_mailto = $this->input->post("contact_partner_mailto");
        //$contact_partner_mailto = "william.arthur.harilantoniaina@gmail.com";

        $contact_partner_mailSubject = 'Contact partenaire - Sortez';

        $message_html = '
        <p>Bonjour,<br/>
        Ceci est une notification de contact de Sortez.org<br/>
        ci-dessous le contenu,</p>
        <p>Nom : '.$contact_partner_nom.'<br/>
        T&eacute;l&eacute;phone : '.$contact_partner_tel.'<br/>
        Email : '.$contact_partner_mail.'<br/>
        Message : <br/>'.$contact_partner_msg.'
        ';

        $sending_mail_privicarte = false;

        $message_html = html_entity_decode(htmlentities($message_html));

        $zContactEmailCom_ = array();
        $zContactEmailCom_[] = array("Email"=>$contact_partner_mailto,"Name"=>$contact_partner_mailto);
        $sending_mail_privicarte = @envoi_notification($zContactEmailCom_, $contact_partner_mailSubject, $message_html, $contact_partner_mail);


        $zContactEmailCom_copy = array();
        $zContactEmailCom_copy[] = array("Email"=>$contact_partner_mail,"Name"=>$contact_partner_nom);
        $message_html_copy = '
        <p>Bonjour,<br/>
        Ceci est une copie de votre contact Sortez.org<br/>
        ci-dessous le contenu,</p>
        <p>Nom : '.$contact_partner_nom.'<br/>
        T&eacute;l&eacute;phone : '.$contact_partner_tel.'<br/>
        Email : '.$contact_partner_mail.'<br/>
        Message : <br/>'.$contact_partner_msg.'
        ';
        $message_html_copy = html_entity_decode(htmlentities($message_html_copy));
        $sending_mail_privicarte_copy = @envoi_notification($zContactEmailCom_copy, $contact_partner_mailSubject, $message_html_copy);
        

        if ($sending_mail_privicarte) echo '<span style="color:#006600;">Votre message à bien été envoyé !</span>';
        else echo '<span style="color:#FF0000;">Une erreur est survenue, veuillez réessayer dans un instant !</span>'; 
     }




    function inscription($type="") {
        $data = null;
        
        
        $data["colRubriques"] = $this->Rubrique->GetAll();
        $data["colSousRubriques"] = $this->SousRubrique->GetAll();
        $data["colVilles"] = $this->mdlville->GetAll();
        $data["colAbonnements"] = $this->Abonnement->GetAll();
        $data["colStatut"] = $this->mdlstatut->GetAll();

        $base_path_system = str_replace('system/', '', BASEPATH);
        $font_path_captcha = $base_path_system."application/resources/fonts/AdobeGothicStd-Bold.otf";
        
        //create captcha and save into database
        $vals = array(
                'word'           => '',
                'img_path'	 => './captcha/',
                'img_url'	 => base_url().'captcha/',
                'font_path'	 => $font_path_captcha,
                'img_width'	 => 300,
                'img_height' => 50,
                'expiration' => 7200
                );
        $cap = create_captcha($vals);
        $data_captcha = array(
            'captcha_time'	=> $cap['time'],
            'ip_address'	=> $this->input->ip_address(),
            'word'              => $cap['word']
            );
        
        $query_captcha = $this->db->insert_string('captcha', $data_captcha);
        $this->db->query($query_captcha);
        //end create captcha and save into database
        
        $data['captcha'] = $cap;

        $data['current_page'] = "subscription_pro";

        if (!isset($type) || $type=="") $type = "basic";
        $data['type'] = $type;
        
        //$this->load->view("front/vwInscriptionProfessionnels",$data);
        //$this->load->view("front2013/vwInscriptionProfessionnels",$data);
        /*if (CURRENT_SITE == "agenda"){
            $this->load->view("agenda/vwInscriptionProfessionnels",$data);
        } else {
            $this->load->view("front2013/vwInscriptionProfessionnels",$data);
        }*/
        //$this->load->view("frontAout2013/vwInscriptionProfessionnels",$data);

        $data['mdlville'] = $this->mdlville;
        $data["colDepartement"] = $this->mdldepartement->GetAll();

        $this->load->view("association/vwInscriptionProfessionnels",$data);
    }
    
    //William : captcha verification function on database
    function verify_captcha($captcha) {
        // First, delete old captchas
        $expiration = time()-3600; // one hour limit
        $this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);	

        // Then see if a captcha exists:
        $sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
        $binds = array($captcha, $this->input->ip_address(), $expiration);
        $query = $this->db->query($sql, $binds);
        $row = $query->row();

        if ($row->count == 0)
        {
            echo "0";
        } else 
        {
            echo "1";
        }
    }



    function mon_inscription(){
        
        if (!$this->ion_auth->logged_in()) {
            redirect("front/professionnels/inscription");
        } else {
            $user_ion_auth = $this->ion_auth->user()->row();
            $idCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            $data['idCommercant'] = $idCommercant;
            $data['toCommercant'] = $this->Commercant->GetById($idCommercant);
            $data['toAbonnement'] = $this->Abonnement->GetCurrentCommercantAbonnement($idCommercant);
            $data['toCommertantAbonnement'] = $this->mdldemande_abonnement->getWhere('idCommercant = '.$idCommercant.' AND  idUser_ionauth = '.$user_ion_auth->id);
        }

        $data["colAbonnements"] = $this->Abonnement->GetAll();
        
        $data['Abonnement'] = $this->Abonnement;
        $data["colDepartement"] = $this->mdldepartement->GetAll();

        $updated = $_REQUEST['updated'];
        if (isset($updated) && $updated == "1")  $data['msg_abonnement'] = "Demande enregistr&eacute;e ! une notification est envoy&eacute; &agrave; l'administrateur.";

        $this->load->view("privicarte/mon_inscription",$data);

        


    }


    function updateabonnement(){

        //save demande abonnement--------------------------------------
        $demande_abonnement = $this->input->post("demande_abonnement");
        if (!$demande_abonnement) {
            redirect("front/professionnels/mon_inscription");
        } else {
            $user_ion_auth = $this->ion_auth->user()->row();
            $idCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            $data['idCommercant'] = $idCommercant;
            $data['toCommercant'] = $this->Commercant->GetById($idCommercant);
            $data['toAbonnement'] = $this->Abonnement->GetCurrentCommercantAbonnement($idCommercant);
            $data['toCommertantAbonnement'] = $this->mdldemande_abonnement->getWhere('idCommercant = '.$idCommercant.' AND  idUser_ionauth = '.$user_ion_auth->id);
        }
        $colAbonnements = $this->Abonnement->GetAll();
        $abonnement_asked = array();
        //var_dump($demande_abonnement);

        foreach ($demande_abonnement as $key_demande_abonnement => $item_demande_abonnement) {
            echo $key_demande_abonnement."=>".$item_demande_abonnement;
            $abonnement_asked[] = $key_demande_abonnement;
        }
        
        foreach ($colAbonnements as $objAbonnements) {
            $array_demande_abonnement = array('id' => NULL, 'idAbonnement' => $objAbonnements->IdAbonnement, 'idCommercant' => $idCommercant, 'idUser_ionauth' => $user_ion_auth->id);
            $IdAbonnementExist_on_database = $this->mdldemande_abonnement->IdAbonnementExist($objAbonnements->IdAbonnement, $idCommercant, $user_ion_auth->id);
            $IdAbonnementExist_on_demande = in_array(intval($objAbonnements->IdAbonnement), $abonnement_asked);
            //var_dump($IdAbonnementExist_on_database);
            //var_dump($IdAbonnementExist_on_demande);
            //if ($objAbonnements->IdAbonnement == '5') die("STOP");
            if ($IdAbonnementExist_on_database && $IdAbonnementExist_on_demande) {}
            elseif (!$IdAbonnementExist_on_database && $IdAbonnementExist_on_demande) {
                $insert_dmd_abonnement = $this->mdldemande_abonnement->insert($array_demande_abonnement);
            } elseif ((!$IdAbonnementExist_on_database && !$IdAbonnementExist_on_demande) || ($IdAbonnementExist_on_database && !$IdAbonnementExist_on_demande)) {
                $objAbonnementWhere = $this->mdldemande_abonnement->getWhere('idAbonnement = '.$objAbonnements->IdAbonnement.' AND idCommercant = '.$idCommercant.' AND  idUser_ionauth = '.$user_ion_auth->id);
                $idAbonnementToDelete = $objAbonnementWhere[0]->id;
                //var_dump($will_tetttttt);
                $this->mdldemande_abonnement->delete($idAbonnementToDelete);
            }
            //if ($objAbonnements->IdAbonnement == '5') die("STOP");
        }
        


         //Envoi mail vers admin
                $objMessage = $this->input->post("Message");
                $colDestAdmin = array();
                $colDestAdmin[] = array("Email"=>$this->config->item("mail_sender_adress"),"Name"=>$this->config->item("mail_sender_name"));

                // Sujet
                $txtSujetAdmin = "Commerçants";

                $txtContenuAdmin = "
                    <p>Bonjour ,</p>
                    <p>Un commer&ccedil;ant vient de Modifier sa demande d\'abonnement sur Privicarte.fr.</p><p>";

                $txtContenuAdmin .= "Login : ".$this->Commercant->GetById($idCommercant)->Login."<br/>";
                $txtContenuAdmin .= "Email : ".$this->Commercant->GetById($idCommercant)->Email."<br/>";
                $txtContenuAdmin .= "Nom : ".$this->Commercant->GetById($idCommercant)->NomSociete."<br/>";
                
                $txtContenuAdmin .= "Montant HT : ".$objMessage['montantht']."<br/>";
                $txtContenuAdmin .= "TVA 19.60% : ".$objMessage['montanttva']."<br/>";
                $txtContenuAdmin .= "Montant TTC : ".$objMessage['montantttc']."<br/>";
                if (isset($objMessage['devis']))  $txtContenuAdmin .= "Demande Devis<br/>";
                if (isset($objMessage['proforma']))  $txtContenuAdmin .= "Demande Facture proforma<br/>";
                if (isset($objMessage['facture']))  $txtContenuAdmin .= "Demande Facture<br/>";

                $txtContenuAdmin .= "</p><p>Cordialement,</p>";
                ////$this->firephp->log($txtContenuAdmin, 'txtContenuAdmin');

                @envoi_notification($colDestAdmin,$txtSujetAdmin,$txtContenuAdmin);


        redirect("front/professionnels/mon_inscription?updated=1");

    }

    
    /**
     * 06/06/2011
     * Liva :
     * Methode pour l'ajout d'un commercant 
     * (A ce niveau, il s'agit d'une demande d'inscription seulemet)
     * Seuls les administrateurs qui peuvent valider l'inscription
     * et envoient les codes d'acces vers le commercants
     */
    function ajouter() {

        $objCommercant = $this->input->post("Societe");
        ////$this->firephp->log($objCommercant, 'objCommercant');

        if (!$objCommercant) {
            redirect("front/professionnels/inscription");
        }

        $objMessage = $this->input->post("Message");
        ////$this->firephp->log($objMessage, 'objMessage');

        //var_dump($objMessage);

        $societe_Password = $this->input->post("Societe_Password");
        //$objCommercant["Email"] = $objCommercant["Login"];


        // start create nom_url *******************************************************************************
        $this->load->model("mdlville") ;
        $commercant_url_ville = $this->mdlville->getVilleById($objCommercant["IdVille"]);
        $commercant_url_nom_com = RemoveExtendedChars2($objCommercant["NomSociete"]);
        $commercant_url_nom_ville = RemoveExtendedChars2($commercant_url_ville->Nom);
        $commercant_url_nom = $commercant_url_nom_com."_".$commercant_url_nom_ville;
        $commercant_url_nom = $this->Commercant->setUrlFormat($commercant_url_nom, 0);
        $objCommercant["nom_url"] = $commercant_url_nom;
        // end create nom_url *********************************************************************************
        

        //record datecreation
        $objCommercant["datecreation"] = time();


        if ($objCommercant["IdVille"] == 0) {
            show_error('message'.$ville );
            $this->inscription();
            return;
        }
        $objAssCommercantRubrique = $this->input->post("AssCommercantRubrique");
        $objAssCommercantSousRubrique = $this->input->post("AssCommercantSousRubrique");
        $objAssCommercantAbonnement = $this->input->post("AssAbonnementCommercant");


        //ion_auth register *****************
        $username_ion_auth = $objCommercant['Login'];
        $password_ion_auth = $societe_Password;
        $email_ion_auth = $objCommercant['Email'];
        $additional_data_ion_auth = array('first_name' => $objCommercant['NomSociete'], 'company' => $objCommercant['NomSociete'], 'phone' => $objCommercant['TelFixe']);
        $abonnement_verify_ionauth = $this->Abonnement->GetById($objAssCommercantAbonnement["IdAbonnement"]);
        $value_abonnement_ionauth = '3';//sets default group to basic
        if($abonnement_verify_ionauth->type == "gratuit") $value_abonnement_ionauth = '3';
        if($abonnement_verify_ionauth->type == "premium") $value_abonnement_ionauth = '4';
        if($abonnement_verify_ionauth->type == "platinum") $value_abonnement_ionauth = '5';
        $group_ion_auth = array($value_abonnement_ionauth);

        

        $willll_test = $this->ion_auth->register($username_ion_auth, $password_ion_auth, $email_ion_auth, $additional_data_ion_auth, $group_ion_auth);
        $ion_ath_error = $this->ion_auth->errors();

        //var_dump($ion_ath_error);

        //var_dump($objCommercant);

        ////$this->firephp->log($ion_ath_error, 'ion_ath_error');
        $user_lastid_ionauth = $this->ion_auth_used_by_club->GetUserlast_id($objCommercant['NomSociete'], $username_ion_auth, $email_ion_auth);
        $objCommercant["user_ionauth_id"] = $user_lastid_ionauth;
        //var_dump($user_lastid_ionauth);
        //die("STOP");
        //ion_auth register ******************




        

        if (isset($ion_ath_error) && $ion_ath_error != '' && $ion_ath_error != NULL && !$user_lastid_ionauth) {

            $data["txtContenu"] = "Une erreur s'est produite durant l'enregistrement de vos données. Veuillez recommencer s'il vous plait.".$ion_ath_error;
            $this->load->view("front/vwErreurInscriptionPro", $data);

        } else {

            $this->load->Model("Commercant");
            $IdInsertedCommercant = $this->Commercant->Insert($objCommercant);

            $this->load->Model("AssCommercantRubrique");
            $objAssCommercantRubrique["IdCommercant"] = $IdInsertedCommercant;
            $this->AssCommercantRubrique->Insert($objAssCommercantRubrique);

            $this->load->Model("AssCommercantSousRubrique");
            $objAssCommercantSousRubrique["IdCommercant"] = $IdInsertedCommercant;
            $this->AssCommercantSousRubrique->Insert($objAssCommercantSousRubrique);

            $this->load->Model("AssCommercantAbonnement");
            $objAssCommercantAbonnement["IdCommercant"] = $IdInsertedCommercant;
            $this->AssCommercantAbonnement->Insert($objAssCommercantAbonnement);

            if(!empty($IdInsertedCommercant)) {


                //save demande abonnement--------------------------------------
                $demande_abonnement = $this->input->post("demande_abonnement");
                $colAbonnements = $this->Abonnement->GetAll();
                $abonnement_asked = array();
                //var_dump($demande_abonnement);

                foreach ($demande_abonnement as $key_demande_abonnement=>$item_demande_abonnement) {
                    //echo $key_demande_abonnement."=>".$item_demande_abonnement;
                    $abonnement_asked[] = $key_demande_abonnement;
                }
                foreach ($colAbonnements as $objAbonnements) {
                    $array_demande_abonnement = array('id' => NULL, 'idAbonnement' => $objAbonnements->IdAbonnement, 'idCommercant' => $IdInsertedCommercant, 'idUser_ionauth' => $user_lastid_ionauth);
                    $IdAbonnementExist_on_database = $this->mdldemande_abonnement->IdAbonnementExist($objAbonnements->IdAbonnement, $IdInsertedCommercant, $user_lastid_ionauth);
                    $IdAbonnementExist_on_demande = in_array($objAbonnements->IdAbonnement, $abonnement_asked);
                    if ($IdAbonnementExist_on_database && $IdAbonnementExist_on_demande) {}
                    elseif (!$IdAbonnementExist_on_database && $IdAbonnementExist_on_demande) {
                        $insert_dmd_abonnement = $this->mdldemande_abonnement->insert($array_demande_abonnement);
                    } elseif (!$IdAbonnementExist_on_database && !$IdAbonnementExist_on_demande) {
                        $this->mdldemande_abonnement->delete($objAbonnements->IdAbonnement);
                    }
                }
                //die("STOP");


                //Envoi mail vers admin
                $colDestAdmin = array();
                $colDestAdmin[] = array("Email"=>$this->config->item("mail_sender_adress"),"Name"=>$this->config->item("mail_sender_name"));

                // Sujet
                $txtSujetAdmin = "Notification inscription nouveau Commercant Privicarte";

                $txtContenuAdmin = "
                    <p>Bonjour ,</p>
                    <p>Un commer&ccedil;ant vient de d&eacute;poser sa demande d'inscription Privicarte.</p><p>";

                $txtContenuAdmin .= "Login : ".$objCommercant['Login']."<br/>";
                $txtContenuAdmin .= "Email : ".$objCommercant['Email']."<br/>";
                $txtContenuAdmin .= "Nom : ".$objCommercant['NomSociete']."<br/>";

                if (isset($objMessage['basic']))  $txtContenuAdmin .= "Demande Abonnement Basic Gratuit<br/>";
                if (isset($objMessage['premium']))  $txtContenuAdmin .= "Demande Abonnement Premium : 250€<br/>";
                if (isset($objMessage['platinium']))  $txtContenuAdmin .= "Demande Abonnement Platinium : 400€<br/>";
                if (isset($objMessage['agenda_gratuit']))  $txtContenuAdmin .= "Demande Abonnement Agenda Gratuit<br/>";
                if (isset($objMessage['agenda_plus']))  $txtContenuAdmin .= "Demande Abonnement Agenda Plus : 250€<br/>";
                if (isset($objMessage['web_ref1']))  $txtContenuAdmin .= "Demande Abonnement Module web & référencement 1 an : 250€<br/>";
                if (isset($objMessage['web_ref_n']))  $txtContenuAdmin .= "Demande Abonnement Module web autres années : 100€<br/>";
                if (isset($objMessage['restauration']))  $txtContenuAdmin .= "Demande Abonnement Réstauration : 100€<br/>";

                $txtContenuAdmin .= "Montant HT : ".$objMessage['montantht']."<br/>";
                $txtContenuAdmin .= "TVA 19.60% : ".$objMessage['montanttva']."<br/>";
                $txtContenuAdmin .= "Montant TTC : ".$objMessage['montantttc']."<br/>";
                if (isset($objMessage['devis']))  $txtContenuAdmin .= "Demande Devis<br/>";
                if (isset($objMessage['proforma']))  $txtContenuAdmin .= "Demande Facture proforma<br/>";
                if (isset($objMessage['facture']))  $txtContenuAdmin .= "Demande Facture<br/>";

                
                $txtContenuAdmin .= "</p><p>Lien validation Commercant : ";
                $txtContenuAdmin .= '<a href="'.site_url('admin/commercants/fiche/'.$IdInsertedCommercant).'">'.$objCommercant['NomSociete'].'</a>';
                $txtContenuAdmin .= "</p>";

                $txtContenuAdmin .= "<p>Cordialement,</p>";
                ////$this->firephp->log($txtContenuAdmin, 'txtContenuAdmin');

                @envoi_notification($colDestAdmin,$txtSujetAdmin,$txtContenuAdmin);


                 //Envoi mail vers commercant

                $colDest = array();
                $email = $objCommercant["Login"];
                //echo $email;exit();
                //$colDest[] = array("Email"=>$this->config->item("mail_sender_adress"),"Name"=>$this->config->item("mail_sender_name"));
                $colDest[] = array("Email"=>$email,"Name"=>$objCommercant['NomSociete']);
                // Sujet
                $txtSujet = "Confirmation Inscription";

                
                $objParametreContenuMail = $this->parametre->getByCode("ContenuMailConfirmationInscriptionPro");
                // print_r($objParametreContenuMail);exit();
                $objParametreContenuPage = $this->parametre->getByCode("ContenuPageConfirmationInscriptionPro");

                $txtContenu = $objParametreContenuMail[0]->Contenu;
                @envoi_notification($colDest,$txtSujet,$txtContenu);

                var_dump($colDest);
                var_dump($txtSujet);
                var_dump($txtContenu);
                die();

                $data["txtContenu"] = $objParametreContenuPage[0]->Contenu;
                //$this->load->view("front/vwConfirmationInscriptionProfessionnels", $data);
                //$this->load->view("front2013/vwConfirmationInscriptionProfessionnels", $data);
                $this->load->view("privicarte/vwConfirmationInscriptionProfessionnels", $data);
            }
            else {
                $data["txtContenu"] = "Une erreur s'est produite durant l\'enregistrement de vos données. Veuillez recommencer s\'il vous plait.";
                $this->load->view("front/vwErreurInscriptionPro", $data);
            }

        }
        
    }
    
    function ficheError($prmIdCommercant = 0) {
        $data = null;
        
        $this->load->view("front/vwFicheProfessionnelError",$data);
    }

    function verify_privicarte_subdomain() {
    	
    	$subdomain_url = $this->input->post("subdomain_url");
    	$IdCommercant = $this->input->post("IdCommercant");

    	$obj_verification = $this->Commercant->GetWhere(" nom_url='".$subdomain_url."' AND IdCommercant <> '".$IdCommercant."' ");

    	if (count($obj_verification)>0) echo '<span style="color:#FF0000;">URL existe d&eacute;j&agrave;</span>';
    	else echo '<span style="color:#339900;">URL disponible</span>';

    }

    function verify_privicarte_subdomain_code() {
    	
    	$subdomain_url = $this->input->post("subdomain_url");
    	$IdCommercant = $this->input->post("IdCommercant");

    	$obj_verification = $this->Commercant->GetWhere(" nom_url='".$subdomain_url."' AND IdCommercant <> '".$IdCommercant."' ");

    	if (count($obj_verification)>0) echo '1';
    	else echo '0';

    }


     function glissiere($prmIdCommercant = 0, $mssg = 0) {
	
        if ($this->ion_auth->logged_in() && ($this->ion_auth->in_group(2))) {
            redirect("front/particuliers/inscription/");
        } else if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
            redirect('admin/home', 'refresh');
        } else if (!$this->ion_auth->logged_in()){
            $this->session->set_userdata("cur_uri", $this->uri->uri_string());
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
        
        if($this->ion_auth->logged_in() && ($prmIdCommercant == 0 || $prmIdCommercant =="")) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $prmIdCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        } 
        $data = null;

        $user_ion_auth = $this->ion_auth->user()->row();
        $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $data["userId"] = $commercant_UserId;

        $objAbonnementCommercant = $this->Abonnement->GetLastAbonnementCommercant($prmIdCommercant) ;
        $data['fiche_pro_platinum_detect'] = 1;
        $data['objAbonnementCommercant'] = $objAbonnementCommercant;

        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($prmIdCommercant);
        if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
        $data["user_groups"] = $user_groups[0];




        $objCommercant = $this->mdlglissiere->GetByIdCommercant($prmIdCommercant);
        //var_dump($objCommercant);
        $data["objCommercant"] = $objCommercant;

        $data['mssg'] = $mssg;


        $this->load->helper('ckeditor');


		//Ckeditor's configuration
        $data['ckeditor_presentation_1'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'presentation_1_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );
		
        
        $data['ckeditor_presentation_2'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'presentation_2_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );
        
        $data['ckeditor_presentation_3'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'presentation_3_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

		$data['ckeditor_presentation_4'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'presentation_4_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

		$data['ckeditor_page1_1'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'page1_1_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

		$data['ckeditor_page1_2'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'page1_2_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

		$data['ckeditor_page1_3'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'page1_3_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

		$data['ckeditor_page1_4'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'page1_4_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

		$data['ckeditor_page2_1'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'page2_1_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );


		$data['ckeditor_page2_2'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'page2_2_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

		$data['ckeditor_page2_3'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'page2_3_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

		$data['ckeditor_page2_4'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'page2_4_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

		$this->load->view("privicarte/glissiere_adminpro",$data);

	}


    
    function fiche($prmIdCommercant = 0, $mssg = 0) {
	
        if ($this->ion_auth->logged_in() && ($this->ion_auth->in_group(2))) {
            redirect("front/particuliers/inscription/");
        } else if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
            redirect('admin/home', 'refresh');
        } else if (!$this->ion_auth->logged_in()){
            $this->session->set_userdata("cur_uri", $this->uri->uri_string());
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
        
        if($this->ion_auth->logged_in() && ($prmIdCommercant == 0 || $prmIdCommercant =="")) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $prmIdCommercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        } 
        $data = null;
		
        
        //start verify if merchant subscription contain annonce WILLIAM
        $_iDCommercant = $prmIdCommercant;
        $current_date = date("Y-m-d");
        $query_abcom_cond = " IdCommercant = ".$_iDCommercant." AND ('".$current_date."' BETWEEN DateDebut AND DateFin)";
        $toAbonnementCommercant = $this->AssCommercantAbonnement->GetWhere($query_abcom_cond, 0, 1) ;
        if(sizeof($toAbonnementCommercant)) { 
            foreach ($toAbonnementCommercant as $oAbonnementCommercant) {
                        if(isset($oAbonnementCommercant) && ($oAbonnementCommercant->IdAbonnement==1 || $oAbonnementCommercant->IdAbonnement==2)) {//IdAbonnement doesn't contain annonce
                                //redirect ("front/professionnels/fiche/$_iDCommercant") ;
                                $data['show_annonce_btn']=0; 
                                } else $data['show_annonce_btn']=1;
                                
                        if(isset($oAbonnementCommercant) && ($oAbonnementCommercant->IdAbonnement==1 || $oAbonnementCommercant->IdAbonnement==3)) {//IdAbonnement doesn't contain bonplan
                                //redirect ("front/professionnels/fiche/$_iDCommercant") ;
                                $data['show_bonplan_btn']=0; 
                                } else $data['show_bonplan_btn']=1;
            }
        } else {
            $data['show_annonce_btn']=0;
            $data['show_bonplan_btn']=0;
            //$this->load->view("front/vwFicheProfessionnelError",$data);
            redirect("front/professionnels/ficheError/");
            //exit();
        }
        //end verify if merchant subscription contain annonce WILLIAM


		

        $user_ion_auth = $this->ion_auth->user()->row();
        $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $data["userId"] = $commercant_UserId;
		
        $this->load->Model("Commercant");
        $this->load->Model("Rubrique");
        $this->load->Model("SousRubrique");
        $this->load->Model("Ville");
        $this->load->Model("Abonnement");
        $this->load->Model("AssCommercantRubrique");
        $this->load->Model("AssCommercantSousRubrique");
        $this->load->Model("AssCommercantAbonnement");
		
        
        $data["no_main_menu_home"] = "1";
        $data["no_left_menu_home"] = "1";
        
        $data["colRubriques"] = $this->Rubrique->GetAll();
        $data["colSousRubriques"] = $this->SousRubrique->GetAll();//die("ok");
       	//$data["colVilles"] = $this->Ville->GetAll();
	   	$data["colVilles"] = $this->mdlville->GetAll();//var_dump($data["colVilles"]);
        $data["colDepartement"] = $this->mdldepartement->GetAll();
        $data['mdldepartement'] = $this->mdldepartement ;
        $data["colCommune"] = $this->mdlcommune->GetAll();
        $data['mdlcommune'] = $this->mdlcommune ;
		
		//die("ok");
        $data["colAbonnements"] = $this->Abonnement->GetAll();//die("qfqsdf");
        
        $data["objCommercant"] = $this->Commercant->GetById($commercant_UserId);
        $data["objAssCommercantRubrique"] = $this->AssCommercantRubrique->GetWhere("ass_commercants_rubriques.IdCommercant = " . $commercant_UserId);
       
		$data["objAssCommercantSousRubrique"] = $this->AssCommercantSousRubrique->GetWhere("ass_commercants_sousrubriques.IdCommercant = " . $commercant_UserId);
        $data["objAssCommercantAbonnement"] = $this->AssCommercantAbonnement->GetWhere("ass_commercants_abonnements.IdCommercant = " . $commercant_UserId);
		// Ajouté par OP 27/11/2011
        $this->load->helper('ckeditor');



        //Ckeditor's configuration
        $data['ckeditor0'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'Societe[Caracteristiques]',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );
//// OP END
        
        
        $data['ckeditor1'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'activite1Societe',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );
        
        $data['ckeditor2'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'Societe[activite2]',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        $data['ckeditor3'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'HorairesSociete',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '150px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );


        
        $data['home_link_club'] = 1;
        $data['mssg'] = $mssg;

        $data['mdlville'] = $this->mdlville;

        $data['mdlglissiere'] = $this->mdlglissiere;

        $data['jquery_to_use'] = "1.4.2";

        $objAbonnementCommercant = $this->Abonnement->GetLastAbonnementCommercant($prmIdCommercant) ;
        $data['fiche_pro_platinum_detect'] = 1;
        $data['objAbonnementCommercant'] = $objAbonnementCommercant;

        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($prmIdCommercant);
        if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
        $data["user_groups"] = $user_groups[0];

        $page_data = $_REQUEST['page_data'];
        if (isset($page_data)) $data["page_data"] = $page_data; else $data["page_data"] = "coordonnees";




        $objGlissiere = $this->mdlglissiere->GetByIdCommercant($prmIdCommercant);
        //var_dump($objCommercant);
        $data["objGlissiere"] = $objGlissiere;

       
		//Ckeditor's configuration
        $data['ckeditor_presentation_1'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'presentation_1_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );
		
        
        $data['ckeditor_presentation_2'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'presentation_2_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );
        
        $data['ckeditor_presentation_3'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'presentation_3_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

		$data['ckeditor_presentation_4'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'presentation_4_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

		$data['ckeditor_page1_1'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'page1_1_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

		$data['ckeditor_page1_2'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'page1_2_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

		$data['ckeditor_page1_3'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'page1_3_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

		$data['ckeditor_page1_4'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'page1_4_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

		$data['ckeditor_page2_1'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'page2_1_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );


		$data['ckeditor_page2_2'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'page2_2_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

		$data['ckeditor_page2_3'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'page2_3_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

		$data['ckeditor_page2_4'] = array(

            //ID of the textarea that will be replaced
            'id'     =>     'page2_4_contenu',
            'path'    =>    APPPATH . 'resources/ckeditor',

            //Optionnal values
            'config' => array(
                'width'     =>     '100%',    //Setting a custom width
                'height'     =>     '350px',    //Setting a custom height
                'toolbar'     =>     array(    //Setting a custom toolbar
                    array( 'Source' ),
                    array( 'Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent'),
                    array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link'),
                    array( 'Font','FontSize','TextColor','Table','BGColor' ),
                    array( 'Image','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe')
                ),
                'filebrowserUploadUrl' => site_url('uploadfile/index/')
            )
        );

        




        //$this->load->view("front2013/vwFicheProfessionnel_platinum",$data);
        //$this->load->view("frontAout2013/vwFicheProfessionnel_platinum",$data);
        $this->load->view("privicarte/vwFicheProfessionnel_platinum",$data);


    }


    function save_glissiere() {
       
        $objCommercant = $this->input->post("Societe");


        $user_ion_auth = $this->ion_auth->user()->row();
        $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $objCommercant["IdCommercant"] = $commercant_UserId;
        $objCommercant["id_ionauth"] = $user_ion_auth->id;

        //var_dump($objCommercant);

        if ($objCommercant["id_glissiere"] != '0') $IdUpdatedCommercant = $this->mdlglissiere->Update($objCommercant);
        else $IdUpdatedCommercant = $this->mdlglissiere->Insert($objCommercant);


		redirect("front/professionnels/glissiere/".$objCommercant["IdCommercant"]."/1");


    }


    function get_qrcode_img() {
    	$IdCommercant = $this->input->post("IdCommercant");
    	echo $this->Commercant->getById($IdCommercant)->qrcode_img;
    }

    function get_qrcode_text() {
    	$IdCommercant = $this->input->post("IdCommercant");
    	echo $this->Commercant->getById($IdCommercant)->qrcode_text;
    }

    function generate_qrcode() {
       
        $qrcode_text = $this->input->post("qrcode_text");


        $user_ion_auth = $this->ion_auth->user()->row();
        $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

        //var_dump($objCommercant);

        //if ($objCommercant["id_glissiere"] != '0') $IdUpdatedCommercant = $this->mdlglissiere->Update($objCommercant);
        //else $IdUpdatedCommercant = $this->mdlglissiere->Insert($objCommercant);


		//redirect("front/professionnels/glissiere/".$objCommercant["IdCommercant"]."/1");

		$base_path_system = str_replace('system/', '', BASEPATH);
        $qrcode_lib_path = $base_path_system."application/resources/phpqrcode/";
        $qrcode_lib_images_url = base_url()."application/resources/phpqrcode/images/";


		include($qrcode_lib_path.'qrlib.php'); 
         
		//include('config.php'); 

		// how to save PNG codes to server 

		$tempDir = $qrcode_lib_path.'images/'; 
		$tempURL = $qrcode_lib_images_url;

		$codeContents = $qrcode_text; 

		// we need to generate filename somehow,  
		// with md5 or with database ID used to obtains $codeContents... 
		$fileName = '005_file_'.md5($codeContents).'.png'; 

		$pngAbsoluteFilePath = $tempDir.$fileName; 
		$urlRelativeFilePath = $tempURL.$fileName; 

		// generating 
		if (!file_exists($pngAbsoluteFilePath)) { 
			QRcode::png($codeContents, $pngAbsoluteFilePath, QR_ECLEVEL_L, 4); 
			//echo 'File generated!'; 
			//echo '<hr />'; 
			$this->Commercant->UpdateQrcodeByIdCommercant($commercant_UserId, $qrcode_text, $fileName);
		} else { 
			//echo 'File already generated! We can use this cached file to speed up site on common codes!'; 
			//echo '<hr />'; 
		} 

		//echo 'Server PNG File: '.$pngAbsoluteFilePath; 
		//echo '<hr />'; 

		// displaying 
		echo '<img src="'.$urlRelativeFilePath.'" />'; 



    }
    
	
    function modifier() {
        if(!$this->ion_auth->logged_in()) {
            $this->session->set_userdata("cur_uri", $this->uri->uri_string());
            redirect();
        }
        $objCommercant = $this->input->post("Societe");

        $user_ion_auth = $this->ion_auth->user()->row();
        $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $objCommercant["IdCommercant"] = $commercant_UserId;



		$objGlissiere = $this->input->post("Glissiere");
        $objGlissiere["IdCommercant"] = $commercant_UserId;
        $objGlissiere["id_ionauth"] = $user_ion_auth->id;
       //var_dump($objCommercant);
        if ($objCommercant["id_glissiere"] != '0') $IdUpdatedCommercant = $this->mdlglissiere->Update($objGlissiere);
        else $IdUpdatedCommercant = $this->mdlglissiere->Insert($objGlissiere);

        
        
        // start create nom_url *******************************************************************************
		$nom_url_updated_pro = $this->input->post("nom_url_updated_pro");
		if (isset($nom_url_updated_pro) && $nom_url_updated_pro != "") {
			$nom_url_updated_pro = RemoveExtendedChars2($nom_url_updated_pro);
			$objCommercant["nom_url"] = $nom_url_updated_pro;
		} else {
			$this->load->model("mdlville") ;
	        $commercant_url_ville = $this->mdlville->getVilleById($objCommercant["IdVille"]);
	        $commercant_url_nom_com = RemoveExtendedChars2($objCommercant["NomSociete"]);
	        $commercant_url_nom_ville = RemoveExtendedChars2($commercant_url_ville->Nom);
	        $commercant_url_nom = $commercant_url_nom_com."_".$commercant_url_nom_ville;
	        $commercant_url_nom = $this->Commercant->setUrlFormat($commercant_url_nom, $objCommercant["IdCommercant"]);
	        $objCommercant["nom_url"] = $commercant_url_nom;
		}
        // end create nom_url *********************************************************************************
        
        
        $objAssCommercantRubrique = $this->input->post("AssCommercantRubrique");
        $objAssCommercantSousRubrique = $this->input->post("AssCommercantSousRubrique");
        $objAssCommercantAbonnement = $this->input->post("AssAbonnementCommercant");
		
        
		if (isset($_FILES["Societelogo"]["name"])) $photologo =  $_FILES["Societelogo"]["name"];
		if(isset($photologo) && $photologo !=""){
		    $photologoAssocie = $photologo;
			$Societelogo = doUpload("Societelogo","application/resources/front/photoCommercant/images/",$photologoAssocie);
		}else {
		    $photologoAssocie = $this->input->post("photologoAssocie");
			$Societelogo = $photologoAssocie;
		}
                
                $base_path_system = str_replace('system/', '', BASEPATH);
                $this->load->library('image_moo');
                
                //create background picture from color selected *************************************
                if (isset($objCommercant["bandeau_color"]) && $objCommercant["bandeau_color"]!="") {
                        $file_to_resize_xxx = $base_path_system."application/resources/front/images/wpimages2013/mini_pix_1.png";
                        $bandeau_color_image = md5(rand() * time()).'.png';
                        $file_to_resize_yyy = $base_path_system."application/resources/front/images/wpimages2013/".$bandeau_color_image;
                        if(file_exists($file_to_resize_xxx)) {
                            $this->image_moo
                                    ->load($file_to_resize_xxx)
                                    ->set_background_colour($objCommercant["bandeau_color"])
                                    ->resize(900,455,true)
                                    ->save($file_to_resize_yyy,true);
                            $error_image_imoo_xxx = $this->image_moo->display_errors();
                            //$qdfsdqsdfqg='qsdfqsd';
                            if(file_exists($file_to_resize_yyy)) {
                                $objCommercant["bandeau_color_image"] = $bandeau_color_image;
                            }
                        }
                }
                
        
        //Upload bandeau du haut image *************************************************
        if (isset($_FILES["Societebandeau_top"]["name"]))  $bandeau_top =  $_FILES["Societebandeau_top"]["name"];
        if(isset($bandeau_top) && $bandeau_top !=""){
            $bandeau_topAssocie = $bandeau_top;
            $Societebandeau_top = doUploadResize("Societebandeau_top","application/resources/front/photoCommercant/images/",$bandeau_topAssocie);
                        //$base_path_system = str_replace('system/', '', BASEPATH);
                        


                        /*$file_to_resize_x = $base_path_system."application/resources/front/photoCommercant/images/".$Societebandeau_top;
                        $file_to_resize_y = base_url()."application/resources/front/photoCommercant/images/".$Societebandeau_top;
                        if(file_exists($file_to_resize_x)) {
                            //$this->load->library('image_moo');
                            $this->image_moo
                                    ->load($file_to_resize_x)
                                    ->set_background_colour($objCommercant["bandeau_color"])
                                    ->resize(900,455,true)
                                    ->save($file_to_resize_x,true);
                            $error_image_imoo = $this->image_moo->display_errors();
                            //$qdfsdqsdfq='qsdfqsd';
                        }
                        if(file_exists($file_to_resize_x)) {
                            $bg_nv_mobile = generate_mobile_bg($file_to_resize_x, 150, 320, '', '', '', false);
                        }*/
                        

                        /*////$this->firephp->log($Societebandeau_top, 'Societebandeau_top');
                        ////$this->firephp->log($file_to_resize_x, 'file_to_resize_x');
                        ////$this->firephp->log($file_to_resize_y, 'file_to_resize_y');
                        ////$this->firephp->log($bg_nv_mobile, 'bg_nv_mobile');*/
        }else {
            $bandeau_topAssocie = $this->input->post("bandeau_topAssocie");
            $Societebandeau_top = $bandeau_topAssocie;
        }

        // FIN Upload bandeau du haut image *************************************************





        //Upload Background image *************************************************
        if (isset($_FILES["Societebackground_image"]["name"]))  $background_image =  $_FILES["Societebackground_image"]["name"];
        if(isset($background_image) && $background_image !=""){
            $background_imageAssocie = $background_image;
            $Societebackground_image = doUploadResize("Societebackground_image","application/resources/front/photoCommercant/images/",$background_imageAssocie);
                        
        }else {
            $background_imageAssocie = $this->input->post("background_imageAssocie");
            $Societebackground_image = $background_imageAssocie;
        }
        // FIN Upload Background image *************************************************




        $image_error_width_heigth_verify = '';

        if (isset($_FILES["SocietePhoto1"]["name"])) $photo1 =  $_FILES["SocietePhoto1"]["name"];
        if(isset($photo1) && $photo1 !=""){
            $photo1Associe = $photo1;
            $SocietePhoto1 = doUploadResize("SocietePhoto1","application/resources/front/photoCommercant/images/",$photo1Associe);
            //verify if image width=1 and heigth=3/4
            if (file_exists("application/resources/front/photoCommercant/images/".$SocietePhoto1)== true) {
                if (verify_proportion_largeur_hauteur("application/resources/front/photoCommercant/images/".$SocietePhoto1)==false){
                    unlink("application/resources/front/photoCommercant/images/".$SocietePhoto1);
                    $SocietePhoto1 = '';
                    $image_error_width_heigth_verify .= 'Photo1-';
                } else {
                    $base_path_system = str_replace('system/', '', BASEPATH);
                    $image_path_resize_home = $base_path_system."/application/resources/front/photoCommercant/images/".$SocietePhoto1;
                    $image_path_resize_home_final = $base_path_system."/application/resources/front/photoCommercant/images/resized_home_".$SocietePhoto1;
                    $this->image_moo
                            ->load($image_path_resize_home)
                            ->resize_crop(200,150)
                            ->save($image_path_resize_home_final,true);
                }
            }
            //end verify
        }else {
            $photo1Associe = $this->input->post("photo1Associe");
            $SocietePhoto1 = $photo1Associe;
        }


        if (isset($_FILES["SocietePhoto2"]["name"])) $photo2 =  $_FILES["SocietePhoto2"]["name"];
        if(isset($photo2) && $photo2 !=""){
            $photo2Associe = $photo2;
            $SocietePhoto2 = doUploadResize("SocietePhoto2","application/resources/front/photoCommercant/images/",$photo2Associe);
            //verify if image width=1 and heigth=3/4
            if (file_exists("application/resources/front/photoCommercant/images/".$SocietePhoto2)== true) {
                if (verify_proportion_largeur_hauteur("application/resources/front/photoCommercant/images/".$SocietePhoto2)==false){
                    unlink("application/resources/front/photoCommercant/images/".$SocietePhoto2);
                    $SocietePhoto2 = '';
                    $image_error_width_heigth_verify .= 'Photo2-';
                } else {
                    $base_path_system = str_replace('system/', '', BASEPATH);
                    $image_path_resize_home = $base_path_system."/application/resources/front/photoCommercant/images/".$SocietePhoto2;
                    $image_path_resize_home_final = $base_path_system."/application/resources/front/photoCommercant/images/resized_home_".$SocietePhoto2;
                    $this->image_moo
                        ->load($image_path_resize_home)
                        ->resize_crop(200,150)
                        ->save($image_path_resize_home_final,true);
                }
            }
            //end verify
        }else {
            $photo2Associe = $this->input->post("photo2Associe");
            $SocietePhoto2 = $photo2Associe;
        }

		
		if (isset($_FILES["SocietePhoto3"]["name"])) $photo3 =  $_FILES["SocietePhoto3"]["name"];
        if(isset($photo3) && $photo3 !=""){
            $photo3Associe = $photo3;
            $SocietePhoto3 = doUploadResize("SocietePhoto3","application/resources/front/photoCommercant/images/",$photo3Associe);
            //verify if image width=1 and heigth=3/4
            if (file_exists("application/resources/front/photoCommercant/images/".$SocietePhoto3)== true) {
                if (verify_proportion_largeur_hauteur("application/resources/front/photoCommercant/images/".$SocietePhoto3)==false){
                    unlink("application/resources/front/photoCommercant/images/".$SocietePhoto3);
                    $SocietePhoto3 = '';
                    $image_error_width_heigth_verify .= 'Photo3-';
                } else {
                    $base_path_system = str_replace('system/', '', BASEPATH);
                    $image_path_resize_home = $base_path_system."/application/resources/front/photoCommercant/images/".$SocietePhoto3;
                    $image_path_resize_home_final = $base_path_system."/application/resources/front/photoCommercant/images/resized_home_".$SocietePhoto3;
                    $this->image_moo
                        ->load($image_path_resize_home)
                        ->resize_crop(200,150)
                        ->save($image_path_resize_home_final,true);
                }
            }
            //end verify
        }else {
            $photo3Associe = $this->input->post("photo3Associe");
            $SocietePhoto3 = $photo3Associe;
        }


		if (isset($_FILES["SocietePhoto4"]["name"]))  $photo4 =  $_FILES["SocietePhoto4"]["name"];
        if(isset($photo4) && $photo4 !=""){
            $photo4Associe = $photo4;
            $SocietePhoto4 = doUploadResize("SocietePhoto4","application/resources/front/photoCommercant/images/",$photo4Associe);
            //verify if image width=1 and heigth=3/4
            if (file_exists("application/resources/front/photoCommercant/images/".$SocietePhoto4)== true) {
                if (verify_proportion_largeur_hauteur("application/resources/front/photoCommercant/images/".$SocietePhoto4)==false){
                    unlink("application/resources/front/photoCommercant/images/".$SocietePhoto4);
                    $SocietePhoto4 = '';
                    $image_error_width_heigth_verify .= 'Photo4-';
                } else {
                    $base_path_system = str_replace('system/', '', BASEPATH);
                    $image_path_resize_home = $base_path_system."/application/resources/front/photoCommercant/images/".$SocietePhoto4;
                    $image_path_resize_home_final = $base_path_system."/application/resources/front/photoCommercant/images/resized_home_".$SocietePhoto4;
                    $this->image_moo
                        ->load($image_path_resize_home)
                        ->resize_crop(200,150)
                        ->save($image_path_resize_home_final,true);
                }
            }
            //end verify
        }else {
            $photo4Associe = $this->input->post("photo4Associe");
            $SocietePhoto4 = $photo4Associe;
        }

		
		if (isset($_FILES["SocietePhoto5"]["name"])) $photo5 =  $_FILES["SocietePhoto5"]["name"];
        if(isset($photo5) && $photo5 !=""){
            $photo5Associe = $photo5;
            $SocietePhoto5 = doUploadResize("SocietePhoto5","application/resources/front/photoCommercant/images/",$photo5Associe);
            //verify if image width=1 and heigth=3/4
            if (file_exists("application/resources/front/photoCommercant/images/".$SocietePhoto5)== true) {
                if (verify_proportion_largeur_hauteur("application/resources/front/photoCommercant/images/".$SocietePhoto5)==false){
                    unlink("application/resources/front/photoCommercant/images/".$SocietePhoto5);
                    $SocietePhoto5 = '';
                    $image_error_width_heigth_verify .= 'Photo5-';
                } else {
                    $base_path_system = str_replace('system/', '', BASEPATH);
                    $image_path_resize_home = $base_path_system."/application/resources/front/photoCommercant/images/".$SocietePhoto5;
                    $image_path_resize_home_final = $base_path_system."/application/resources/front/photoCommercant/images/resized_home_".$SocietePhoto5;
                    $this->image_moo
                        ->load($image_path_resize_home)
                        ->resize_crop(200,150)
                        ->save($image_path_resize_home_final,true);
                }
            }
            //end verify
        }else {
            $photo5Associe = $this->input->post("photo5Associe");
            $SocietePhoto5 = $photo5Associe;
        }


        //upload image of activite 1
        if (isset($_FILES["activite1_image1"]["name"])) $activite1_image1 =  $_FILES["activite1_image1"]["name"];
        if(isset($activite1_image1) && $activite1_image1 !=""){
            $activite1_image1Associe = $activite1_image1;
            $Societeactivite1_image1 = doUploadResize("activite1_image1","application/resources/front/photoCommercant/images/",$activite1_image1Associe);
            //verify if image width=1 and heigth=3/4
            if (file_exists("application/resources/front/photoCommercant/images/".$Societeactivite1_image1)== true) {
                if (verify_proportion_largeur_hauteur("application/resources/front/photoCommercant/images/".$Societeactivite1_image1)==false){
                    unlink("application/resources/front/photoCommercant/images/".$Societeactivite1_image1);
                    $Societeactivite1_image1 = '';
                    $image_error_width_heigth_verify .= 'activite1_image1-';
                }
            }
            //end verify
        }else {
            $activite1_image1Associe = $this->input->post("activite1_image1Associe");
            $Societeactivite1_image1 = $activite1_image1Associe;
        }


        if (isset($_FILES["activite1_image2"]["name"]))
        $activite1_image2 =  $_FILES["activite1_image2"]["name"];
        if(isset($activite1_image2) && $activite1_image2 !=""){
            $activite1_image2Associe = $activite1_image2;
            $Societeactivite1_image2 = doUploadResize("activite1_image2","application/resources/front/photoCommercant/images/",$activite1_image2Associe);
            //verify if image width=1 and heigth=3/4
            if (file_exists("application/resources/front/photoCommercant/images/".$Societeactivite1_image2)== true) {
                if (verify_proportion_largeur_hauteur("application/resources/front/photoCommercant/images/".$Societeactivite1_image2)==false){
                    unlink("application/resources/front/photoCommercant/images/".$Societeactivite1_image2);
                    $Societeactivite1_image2 = '';
                    $image_error_width_heigth_verify .= 'activite1_image2-';
                }
            }
            //end verify
        }else {
            $activite1_image2Associe = $this->input->post("activite1_image2Associe");
            $Societeactivite1_image2 = $activite1_image2Associe;
        }


        if (isset($_FILES["activite1_image3"]["name"])) $activite1_image3 =  $_FILES["activite1_image3"]["name"];
        if(isset($activite1_image3) && $activite1_image3 !=""){
            $activite1_image3Associe = $activite1_image3;
            $Societeactivite1_image3 = doUploadResize("activite1_image3","application/resources/front/photoCommercant/images/",$activite1_image3Associe);
            //verify if image width=1 and heigth=3/4
            if (file_exists("application/resources/front/photoCommercant/images/".$Societeactivite1_image3)== true) {
                if (verify_proportion_largeur_hauteur("application/resources/front/photoCommercant/images/".$Societeactivite1_image3)==false){
                    unlink("application/resources/front/photoCommercant/images/".$Societeactivite1_image3);
                    $Societeactivite1_image3 = '';
                    $image_error_width_heigth_verify .= 'activite1_image3-';
                }
            }
            //end verify
        }else {
            $activite1_image3Associe = $this->input->post("activite1_image3Associe");
            $Societeactivite1_image3 = $activite1_image3Associe;
        }


        if (isset($_FILES["activite1_image4"]["name"])) $activite1_image4 =  $_FILES["activite1_image4"]["name"];
        if(isset($activite1_image4) && $activite1_image4 !=""){
            $activite1_image4Associe = $activite1_image4;
            $Societeactivite1_image4 = doUploadResize("activite1_image4","application/resources/front/photoCommercant/images/",$activite1_image4Associe);
            //verify if image width=1 and heigth=3/4
            if (file_exists("application/resources/front/photoCommercant/images/".$Societeactivite1_image4)== true) {
                if (verify_proportion_largeur_hauteur("application/resources/front/photoCommercant/images/".$Societeactivite1_image4)==false){
                    unlink("application/resources/front/photoCommercant/images/".$Societeactivite1_image4);
                    $Societeactivite1_image4 = '';
                    $image_error_width_heigth_verify .= 'activite1_image4-';
                }
            }
            //end verify
        }else {
            $activite1_image4Associe = $this->input->post("activite1_image4Associe");
            $Societeactivite1_image4 = $activite1_image4Associe;
        }


        if (isset($_FILES["activite1_image5"]["name"])) $activite1_image5 =  $_FILES["activite1_image5"]["name"];
        if(isset($activite1_image5) && $activite1_image5 !=""){
            $activite1_image5Associe = $activite1_image5;
            $Societeactivite1_image5 = doUploadResize("activite1_image5","application/resources/front/photoCommercant/images/",$activite1_image5Associe);
            //verify if image width=1 and heigth=3/4
            if (file_exists("application/resources/front/photoCommercant/images/".$Societeactivite1_image5)== true) {
                if (verify_proportion_largeur_hauteur("application/resources/front/photoCommercant/images/".$Societeactivite1_image5)==false){
                    unlink("application/resources/front/photoCommercant/images/".$Societeactivite1_image5);
                    $Societeactivite1_image5 = '';
                    $image_error_width_heigth_verify .= 'activite1_image5-';
                }
            }
            //end verify
        }else {
            $activite1_image5Associe = $this->input->post("activite1_image5Associe");
            $Societeactivite1_image5 = $activite1_image5Associe;
        }


        //upload image of activite 2
        if (isset($_FILES["activite2_image1"]["name"])) $activite2_image1 =  $_FILES["activite2_image1"]["name"];
        if(isset($activite2_image1) && $activite2_image1 !=""){
            $activite2_image1Associe = $activite2_image1;
            $Societeactivite2_image1 = doUploadResize("activite2_image1","application/resources/front/photoCommercant/images/",$activite2_image1Associe);
            //verify if image width=1 and heigth=3/4
            if (file_exists("application/resources/front/photoCommercant/images/".$Societeactivite2_image1)== true) {
                if (verify_proportion_largeur_hauteur("application/resources/front/photoCommercant/images/".$Societeactivite2_image1)==false){
                    unlink("application/resources/front/photoCommercant/images/".$Societeactivite2_image1);
                    $Societeactivite2_image1 = '';
                    $image_error_width_heigth_verify .= 'activite2_image1-';
                }
            }
            //end verify
        }else {
            $activite2_image1Associe = $this->input->post("activite2_image1Associe");
            $Societeactivite2_image1 = $activite2_image1Associe;
        }


        if (isset($_FILES["activite2_image2"]["name"])) $activite2_image2 =  $_FILES["activite2_image2"]["name"];
        if(isset($activite2_image2) && $activite2_image2 !=""){
            $activite2_image2Associe = $activite2_image2;
            $Societeactivite2_image2 = doUploadResize("activite2_image2","application/resources/front/photoCommercant/images/",$activite2_image2Associe);
            //verify if image width=1 and heigth=3/4
            if (file_exists("application/resources/front/photoCommercant/images/".$Societeactivite2_image2)== true) {
                if (verify_proportion_largeur_hauteur("application/resources/front/photoCommercant/images/".$Societeactivite2_image2)==false){
                    unlink("application/resources/front/photoCommercant/images/".$Societeactivite2_image2);
                    $Societeactivite2_image2 = '';
                    $image_error_width_heigth_verify .= 'activite2_image2-';
                }
            }
            //end verify
        }else {
            $activite2_image2Associe = $this->input->post("activite2_image2Associe");
            $Societeactivite2_image2 = $activite2_image2Associe;
        }


        if(isset($_FILES["activite2_image3"]["name"])) $activite2_image3 =  $_FILES["activite2_image3"]["name"];
        if(isset($activite2_image3) && $activite2_image3 !=""){
            $activite2_image3Associe = $activite2_image3;
            $Societeactivite2_image3 = doUploadResize("activite2_image3","application/resources/front/photoCommercant/images/",$activite2_image3Associe);
            //verify if image width=1 and heigth=3/4
            if (file_exists("application/resources/front/photoCommercant/images/".$Societeactivite2_image3)== true) {
                if (verify_proportion_largeur_hauteur("application/resources/front/photoCommercant/images/".$Societeactivite2_image3)==false){
                    unlink("application/resources/front/photoCommercant/images/".$Societeactivite2_image3);
                    $Societeactivite2_image3 = '';
                    $image_error_width_heigth_verify .= 'activite2_image3-';
                }
            }
            //end verify
        }else {
            $activite2_image3Associe = $this->input->post("activite2_image3Associe");
            $Societeactivite2_image3 = $activite2_image3Associe;
        }


        if(isset($_FILES["activite2_image4"]["name"])) $activite2_image4 =  $_FILES["activite2_image4"]["name"];
        if(isset($activite2_image4) && $activite2_image4 !=""){
            $activite2_image4Associe = $activite2_image4;
            $Societeactivite2_image4 = doUploadResize("activite2_image4","application/resources/front/photoCommercant/images/",$activite2_image4Associe);
            //verify if image width=1 and heigth=3/4
            if (file_exists("application/resources/front/photoCommercant/images/".$Societeactivite2_image4)== true) {
                if (verify_proportion_largeur_hauteur("application/resources/front/photoCommercant/images/".$Societeactivite2_image4)==false){
                    unlink("application/resources/front/photoCommercant/images/".$Societeactivite2_image4);
                    $Societeactivite2_image4 = '';
                    $image_error_width_heigth_verify .= 'activite2_image4-';
                }
            }
            //end verify
        }else {
            $activite2_image4Associe = $this->input->post("activite2_image4Associe");
            $Societeactivite2_image4 = $activite2_image4Associe;
        }


        if (isset($_FILES["activite2_image5"]["name"])) $activite2_image5 =  $_FILES["activite2_image5"]["name"];
        if(isset($activite2_image5) && $activite2_image5 !=""){
            $activite2_image5Associe = $activite2_image5;
            $Societeactivite2_image5 = doUploadResize("activite2_image5","application/resources/front/photoCommercant/images/",$activite2_image5Associe);
            //verify if image width=1 and heigth=3/4
            if (file_exists("application/resources/front/photoCommercant/images/".$Societeactivite2_image5)== true) {
                if (verify_proportion_largeur_hauteur("application/resources/front/photoCommercant/images/".$Societeactivite2_image5)==false){
                    unlink("application/resources/front/photoCommercant/images/".$Societeactivite2_image5);
                    $Societeactivite2_image5 = '';
                    $image_error_width_heigth_verify .= 'activite2_image5-';
                }
            }
            //end verify
        }else {
            $activite2_image5Associe = $this->input->post("activite2_image5Associe");
            $Societeactivite2_image5 = $activite2_image5Associe;
        }





        if(isset($_FILES["SocietePdf"]["name"])) $pdf =  $_FILES["SocietePdf"]["name"];
        if(isset($pdf) && $pdf !=""){
            $pdfAssocie = $pdf;
            $pdfCom = doUpload("SocietePdf","application/resources/front/photoCommercant/images/",$pdfAssocie);
        }else {
            $pdfAssocie = $this->input->post("pdfAssocie");
            $pdfCom = $pdfAssocie;
        }

		
		/*$pdfAssocie = $this->input->post("pdfAssocie");
		$pdfCom = doUpload("SocietePdf","application/resources/front/photoCommercant/images/",$pdfAssocie);s
        	
		*/

		/*$SocietePhoto2 = doUpload("SocietePhoto2","application/resources/front/photoCommercant/images/",$photo2Associe);

		$SocietePhoto3 = doUpload("SocietePhoto3","application/resources/front/photoCommercant/images/",$photo3Associe);
        $SocietePhoto4 = doUpload("SocietePhoto4","application/resources/front/photoCommercant/images/",$photo4Associe);
        $SocietePhoto5 = doUpload("SocietePhoto5","application/resources/front/photoCommercant/images/",$photo5Associe);*/
        
		$objCommercant["Pdf"] = $pdfCom;
        $objCommercant["Logo"] = $Societelogo;
		$objCommercant["bandeau_top"] = $Societebandeau_top;
		$objCommercant["background_image"] = $Societebackground_image;
        $objCommercant["Photo1"] = $SocietePhoto1;
		$objCommercant["Photo2"] = $SocietePhoto2;
		$objCommercant["Photo3"] = $SocietePhoto3;
		$objCommercant["Photo4"] = $SocietePhoto4;
		$objCommercant["Photo5"] = $SocietePhoto5;
        $objCommercant["activite1_image1"] = $Societeactivite1_image1;
        $objCommercant["activite1_image2"] = $Societeactivite1_image2;
        $objCommercant["activite1_image3"] = $Societeactivite1_image3;
        $objCommercant["activite1_image4"] = $Societeactivite1_image4;
        $objCommercant["activite1_image5"] = $Societeactivite1_image5;
        $objCommercant["activite2_image1"] = $Societeactivite2_image1;
        $objCommercant["activite2_image2"] = $Societeactivite2_image2;
        $objCommercant["activite2_image3"] = $Societeactivite2_image3;
        $objCommercant["activite2_image4"] = $Societeactivite2_image4;
        $objCommercant["activite2_image5"] = $Societeactivite2_image5;


		$this->load->Model("Commercant");
        $IdUpdatedCommercant = $this->Commercant->Update($objCommercant);

        //ion_auth_update
        $user_ion_auth = $this->ion_auth->user()->row();
        $data_ion_auth_update = array(
            'username' => $objCommercant['Login'],
            'email' => $objCommercant['Email'],
            'first_name' => $objCommercant['NomSociete'],
            'company' => $objCommercant['NomSociete']
        );
        $this->ion_auth->update($user_ion_auth->id, $data_ion_auth_update);
        //ion_auth_update
        
        $this->load->Model("AssCommercantRubrique");
        $this->AssCommercantRubrique->DeleteByIdCommercant($IdUpdatedCommercant);
        $objAssCommercantRubrique["IdCommercant"] = $IdUpdatedCommercant;
        $this->AssCommercantRubrique->Insert($objAssCommercantRubrique);
        
        $this->load->Model("AssCommercantSousRubrique");
        $this->AssCommercantSousRubrique->DeleteByIdCommercant($IdUpdatedCommercant);
        $objAssCommercantSousRubrique["IdCommercant"] = $IdUpdatedCommercant;
        $this->AssCommercantSousRubrique->Insert($objAssCommercantSousRubrique);
        
        $this->load->Model("AssCommercantAbonnement");
        /*$this->AssCommercantAbonnement->DeleteByIdCommercant($IdUpdatedCommercant);
        $objAssCommercantAbonnement["IdCommercant"] = $IdUpdatedCommercant;*/
        
        $query_verif_abonnement = ' IdCommercant='.$IdUpdatedCommercant;
        $verif_abonnement_comm = $this->AssCommercantAbonnement->GetWhere($query_verif_abonnement);
        
        if (count($verif_abonnement_comm)==0) $this->AssCommercantAbonnement->Insert($objAssCommercantAbonnement);
        
		//activation bon plan
		 // $bonPlan = $this->input->post("bonPlan");
		 // if($bonPlan!=""){
		  // $this->activationBonPlan($bonPlan);
		 // }
		//
        if(!empty($IdUpdatedCommercant)) {
            // Liste des destinataires
            $colDest = array();
            $colDest[] = array("Email"=>$this->config->item("mail_sender_adress"),"Name"=>$this->config->item("mail_sender_name"));
            //$colDest[] = array("Email"=>"william.arthur@weburba.com","Name"=>$this->config->item("mail_sender_name"));
            
            // Sujet
            $txtSujet = "Modification de donn&eacute;es partenaires";

            $obj_Commercant_details = $this->Commercant->GetById($commercant_UserId);
            
            $txtContenu = "
                <p>Bonjour ,</p>
                <p>Un commer&ccedil;ant vient de modifier ses informations.</p>
                <p>
                Nom : ".$objCommercant['NomSociete']."<br/>
                Login : ".$objCommercant['Login']."<br/>
                Email : ".$objCommercant['Email']."<br/>
                Lien : ".site_url($obj_Commercant_details->nom_url.'/presentation')."
                </p>
                <p>Cordialement,</p>
            ";
            @envoi_notification($colDest,$txtSujet,$txtContenu);
        }

        if ($image_error_width_heigth_verify=='') $image_error_width_heigth_verify=1;
        else {
            $image_error_width_heigth_verify = substr($image_error_width_heigth_verify,0,-1);
        }
        
         //redirect("front/menuprofessionnels");
         redirect("front/professionnels/fiche/".$commercant_UserId."/".$image_error_width_heigth_verify."?page_data=".$_REQUEST['page_data']);
    }




	/*function activationBonPlan($iNumUnique=0){	
        $ObjAssocclentBonPlan = $this->mdlcadeau->isValideNum($iNumUnique);
            if($ObjAssocclentBonPlan){
    	    $id =$ObjAssocclentBonPlan->id;
    		$id_client =$ObjAssocclentBonPlan->id_client;
    		$oClient = $this->user->getById($id_client);
    		$NbrPoints = $oClient->NbrPoints+10;
    	    $this->mdlcadeau->ValiderNum($id);
    		$this->mdlcadeau->AjouterPointsClients($id_client,$NbrPoints);
            $this->envoiMailNousContacter($NbrPoints,$oClient->Email);		
    		redirect("front/cadeau");
	   }
   }*/



   function activationBonPlan($idComm){
	  $iNumUnique = $this->input->post("bonPlan");
	  $oCom = $this->Commercant->GetById($idComm);
	  $comNom = $oCom->Nom;
	  if($iNumUnique!=""){
		   $ObjAssocclentBonPlan = $this->mdlcadeau->isValideNum($iNumUnique);
		   if($ObjAssocclentBonPlan){
			$id = $ObjAssocclentBonPlan->id;
			$id_client = $ObjAssocclentBonPlan->id_client;
			$oClient = $this->user->getById($id_client);
			$NbrPoints = $oClient->NbrPoints+10;
			$this->mdlcadeau->ValiderNum($id);
			$this->mdlcadeau->AjouterPointsClients($id_client,$NbrPoints);
			
			$this->envoiMailNousContacter($NbrPoints,$oClient->Email,$iNumUnique,$comNom);		
			//redirect("front/cadeau");
			//$this->load->view("front/vwBonPlanValide", $data);
			//redirect("front/professionnels/fiche/".$idComm);
			
		   }
	 } else {
             
             redirect ("front/professionnels/fiche/".$idComm);
         }
   }
   
   function getContenuMailActivationBonPlan($iNumUnique,$comNom){
       $ocontenumail = $this->mdl_mail_activation_bon_plan->get();
	   $contenu  =  $ocontenumail->contenu;
	   $contenu1 =  $ocontenumail->contenu1;
	   return ($contenu. " ".$iNumUnique." ".$contenu1. " " .$comNom);
   }
   function envoiMailNousContacter($NbrPoints,$mail,$iNumUnique,$comNom){ 
			
            $zMessage ="";
		   /* $zMessage = "Votre bon plan avec le code ".$iNumUnique." a Ã©tÃ© validÃ© par le commerÃ§ant  ".$comNom."\n";*/
			
			//$zMessage.= "vous avez maintenant:".$NbrPoints."points cadeaux\n";
			$zMessage.= $this->getContenuMailActivationBonPlan($iNumUnique,$comNom) ;
			$zTitre = "[Privicarte] Ajout de points cadeau" ;
			//echo $zMessage;exit();
						
			$zContactEmail = $mail ;
			
			$headers ="From: " . "Privicarte" . "< Privicarte >\n" ;
			$headers .="Reply-To: Privicarte \n";
			$headers .= "Bcc:randawilly@gmail.com \n"; // Copies cachÃ©es			
			//$headers .='Content-Type: text/plain; charset="iso-8859-1"'."\n";
			$headers .='Content-Type: text/plain; charset=utf-8'."\n";
			$headers .='Content-Transfer-Encoding: 8bit';
			//redirect("front/professionnels/load_bon_plan_valide");	exit();		
			if(mail($zContactEmail, $zTitre, $zMessage, $headers)){     
              
                //redirect("front/menuprofessionnels");	
                redirect("front/professionnels/load_bon_plan_valide");						
			}else {
					  echo "mail non envoye";//exit();
					 
			} 
	}
	function load_bon_plan_valide(){
	 $this->load->view("front/vwBonPlanValide");
	}
	function getPostalCode($idVille=0){
	    $oville = $this->mdlville->getVilleById($idVille);
		$data["cp"] =$oville->CodePostal;
		 
        $data['idVille']=$idVille;
        //$this->load->view("front/vwReponseVille", $data);
	    echo $oville->CodePostal;
	}


    function getPostalCode_localisation($idVille=0){
        $oville = $this->mdlville->getVilleById($idVille);
        if (count($oville)!=0) $data["cp"] =$oville->CodePostal; else $data["cp"] = "";

        $data['idVille']=$idVille;
        $this->load->view("front/vwReponseVille_localisation", $data);

    }
    function delete_files($prmIdCommercant = "0", $prmFiles) {
        $oInfoCommercant = $this->Commercant->getById($prmIdCommercant);
        $filetodelete = "";
        if ($prmFiles == "Logo" || $prmFiles == "logo") {
            $filetodelete = $oInfoCommercant->Logo;
            $this->Commercant->effacerPhotoLogoCommercant($prmIdCommercant);
        }
        if ($prmFiles == "bandeau_top") {
            $filetodelete = $oInfoCommercant->bandeau_top;
            $this->Commercant->effacerbandeau_top($prmIdCommercant);
        }
        if ($prmFiles == "background_image") {
            $filetodelete = $oInfoCommercant->background_image;
            //var_dump($filetodelete); die("stop");
            $this->Commercant->effacerbackground_image($prmIdCommercant);
        }
        if ($prmFiles == "Photo1") {
            $filetodelete = $oInfoCommercant->Photo1;
            $this->Commercant->effacerPhoto1($prmIdCommercant);
        }
        if ($prmFiles == "Photo2") {
            $filetodelete = $oInfoCommercant->Photo2;
            $this->Commercant->effacerPhoto2($prmIdCommercant);
        }    
        if ($prmFiles == "Photo3") {
            $filetodelete = $oInfoCommercant->Photo3;
            $this->Commercant->effacerPhoto3($prmIdCommercant);
        }
        if ($prmFiles == "Photo4") {
            $filetodelete = $oInfoCommercant->Photo4;
            $this->Commercant->effacerPhoto4($prmIdCommercant);
        }
        if ($prmFiles == "Photo5") {
            $filetodelete = $oInfoCommercant->Photo5;
            $this->Commercant->effacerPhoto5($prmIdCommercant);
        }
        if ($prmFiles == "PhotoAccueil") {
            $filetodelete = $oInfoCommercant->PhotoAccueil;
            $this->Commercant->effacerPhotoCommercant($prmIdCommercant);
        }
        if ($prmFiles == "Pdf") {
            $filetodelete = $oInfoCommercant->Pdf;
            $this->Commercant->effacerPdf($prmIdCommercant);
        }
        if (file_exists("application/resources/front/photoCommercant/images/".$filetodelete)== true) {
            unlink("application/resources/front/photoCommercant/images/".$filetodelete);
        }
        if ($prmFiles == "activite1_image1") {
            $filetodelete = $oInfoCommercant->activite1_image1;
            $this->Commercant->effacer_activite1_image1($prmIdCommercant);
            unlink("application/resources/front/photoCommercant/images/".$filetodelete);
        }
        if ($prmFiles == "activite1_image2") {
            $filetodelete = $oInfoCommercant->activite1_image2;
            $this->Commercant->effacer_activite1_image2($prmIdCommercant);
            unlink("application/resources/front/photoCommercant/images/".$filetodelete);
        }
        if ($prmFiles == "activite1_image3") {
            $filetodelete = $oInfoCommercant->activite1_image3;
            $this->Commercant->effacer_activite1_image3($prmIdCommercant);
            unlink("application/resources/front/photoCommercant/images/".$filetodelete);
        }
        if ($prmFiles == "activite1_image4") {
            $filetodelete = $oInfoCommercant->activite1_image4;
            $this->Commercant->effacer_activite1_image4($prmIdCommercant);
            unlink("application/resources/front/photoCommercant/images/".$filetodelete);
        }
        if ($prmFiles == "activite1_image5") {
            $filetodelete = $oInfoCommercant->activite1_image5;
            $this->Commercant->effacer_activite1_image5($prmIdCommercant);
            unlink("application/resources/front/photoCommercant/images/".$filetodelete);
        }
        if ($prmFiles == "activite2_image1") {
            $filetodelete = $oInfoCommercant->activite2_image1;
            $this->Commercant->effacer_activite2_image1($prmIdCommercant);
            unlink("application/resources/front/photoCommercant/images/".$filetodelete);
        }
        if ($prmFiles == "activite2_image2") {
            $filetodelete = $oInfoCommercant->activite2_image2;
            $this->Commercant->effacer_activite2_image2($prmIdCommercant);
            unlink("application/resources/front/photoCommercant/images/".$filetodelete);
        }
        if ($prmFiles == "activite2_image3") {
            $filetodelete = $oInfoCommercant->activite2_image3;
            $this->Commercant->effacer_activite2_image3($prmIdCommercant);
            unlink("application/resources/front/photoCommercant/images/".$filetodelete);
        }
        if ($prmFiles == "activite2_image4") {
            $filetodelete = $oInfoCommercant->activite2_image4;
            $this->Commercant->effacer_activite2_image4($prmIdCommercant);
            unlink("application/resources/front/photoCommercant/images/".$filetodelete);
        }
        if ($prmFiles == "activite2_image5") {
            $filetodelete = $oInfoCommercant->activite2_image5;
            $this->Commercant->effacer_activite2_image5($prmIdCommercant);
            unlink("application/resources/front/photoCommercant/images/".$filetodelete);
        }

        
    }
    
    
    function verifier_email(){
            $txtEmail = $this->input->post("txtEmail_var");
            $oEmail = $this->user->verifier_mailUserPro($txtEmail);
            $emailExist = count($oEmail);
            $reponse = "0";
            $data['reponse']="0";
            if ($emailExist != "0") {
                $reponse = "1";
                //$data['reponse']="1";
                echo $reponse;
            } else {
                $reponse = "0";
                //$data['reponse']="0";
                echo $reponse;
            }

        }

    function verifier_email_ionauth(){
        $txtEmail = $this->input->post("txtEmail_var_ionauth");
        $oEmail = $this->user->verifier_mailUser_ionauth($txtEmail);
        $emailExist = count($oEmail);
        $reponse = "0";
        $data['reponse']="0";
        if ($emailExist != "0") {
            $reponse = "1";
            //$data['reponse']="1";
            echo $reponse;
        } else {
            $reponse = "0";
            //$data['reponse']="0";
            echo $reponse;
        }

    }

    function publightbox_identity(){
        $txtEmail = $this->input->post("publightbox_identity");
        $oEmail = $this->user->verifier_mailUser_ionauth($txtEmail);
        //var_dump($oEmail);
        $this->session->set_userdata('publightbox_email', $oEmail[0]->email);
        $this->session->set_userdata('publightbox_name', $oEmail[0]->first_name);
        if (isset($oEmail)) echo $oEmail[0]->first_name;
    }
        
        
        function verifier_login(){
            $txtLogin = $this->input->post("txtLogin_var");
            $oEmail = $this->user->verifier_loginUserPro($txtLogin);
            $emailExist = count($oEmail);
            $reponse = "0";
            $data['reponse']="0";
            if ($emailExist != "0") {
                $reponse = "1";
                //$data['reponse']="1";
                echo $reponse;
            } else {
                $reponse = "0";
                //$data['reponse']="0";
                echo $reponse;
            }

        }


    function verifier_login_ionauth(){
        $txtLogin = $this->input->post("txtLogin_var_ionauth");
        $oEmail = $this->user->verifier_loginUser_ionauth($txtLogin);
        $emailExist = count($oEmail);
        $reponse = "0";
        $data['reponse']="0";
        if ($emailExist != "0") {
            $reponse = "1";
            //$data['reponse']="1";
            echo $reponse;
        } else {
            $reponse = "0";
            //$data['reponse']="0";
            echo $reponse;
        }

    }
    
}

?>