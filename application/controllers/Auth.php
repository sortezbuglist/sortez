<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model ( "mdl_card" );
        $this->load->model ( "mdl_card_bonplan_used" );
        $this->load->model ( "mdl_card_capital" );
        $this->load->model ( "mdl_card_tampon" );
        $this->load->model ( "mdl_card_remise" );
        $this->load->model ( "mdl_card_user_link" );
        $this->load->model ( "mdl_card_fiche_client_tampon" );
        $this->load->model ( "mdl_card_fiche_client_capital" );
        $this->load->model ( "mdl_card_fiche_client_remise" );
        $this->load->model ( 'assoc_client_bonplan_model' );
        $this->load->model ( 'assoc_client_commercant_model' );
        $this->load->Model ( "Ville" );
        $this->load->model ( "user" );
        $this->load->model ( "mdlbonplan" );
        $this->load->model ( "commercant" );

        $this->load->library ( 'session' );

        $this->load->library ( 'ion_auth' );
        $this->load->model ( "ion_auth_used_by_club" );
        $this->load->library('ion_auth');
        $this->load->model("api_model");
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->helper('clubproximite');


        // Load MongoDB library instead of native db driver if required
        $this->config->item('use_mongodb', 'ion_auth') ?
            $this->load->library('mongo_db') :

            $this->load->database();

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->load->model("ion_auth_used_by_club");
        $this->load->model("Commercant");
        $this->load->model("AssCommercantAbonnement");
        $this->load->library('user_agent');


        check_vivresaville_id_ville();

    }

    //redirect if needed, otherwise display the user list
    function index($isFromMail=0)
    {
        if(isset($isFromMail) AND $isFromMail == 1){
            $isFromMail = 1;
        }else{
            $isFromMail =0;
        }
        if ($this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("admin/home");
        }

        if (!$this->ion_auth->logged_in()) {
            //redirect them to the login page
            $message_confirmation = $this->session->flashdata('message');
            if (isset($message_confirmation) && $message_confirmation != "") $this->session->set_flashdata('message_confirmation', $message_confirmation);
            if (isset($isFromMail) AND $isFromMail ==1){
                redirect('auth/login/1', 'refresh');
            }else{
                redirect('auth/login', 'refresh');
            }

        } elseif (!$this->ion_auth->is_admin()) {
            //redirect them to the home page because they must be an administrator to view this
            //redirect('/', 'refresh');

            $user_ion_auth = $this->ion_auth->user()->row();
            $user_groups = $this->ion_auth->get_users_groups($user_ion_auth->id)->result();
            $group_id_commercant_user = $user_groups[0]->id;
            if ($group_id_commercant_user != 2) redirect('front/utilisateur/contenupro', 'refresh');
            else  redirect('front/utilisateur/menuconsommateurs', 'refresh');

        } else {
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            //list the users
            $this->data['users'] = $this->ion_auth->users()->result();
            foreach ($this->data['users'] as $k => $user) {
                $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
            }
            $this->_render_page('auth/index', $this->data);
        }
    }

    //log the user in
    function login($isFromMail = 0)
    {
        if (isset($isFromMail) AND $isFromMail == 1){
            $isFromMail = 1;
            $this->data['isFromMail'] = "1";
        }else{
            $isFromMail = 0;
            $this->data['isFromMail'] = "0";
        }
        statistiques();
        if ($this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("admin/home");
        }

        if ($this->ion_auth->in_group(6)) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("association/home");
        }
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $user_groups = $this->ion_auth->get_users_groups($user_ion_auth->id)->result();
            $group_id_commercant_user = $user_groups[0]->id;
            if ($group_id_commercant_user != 2) redirect('front/utilisateur/contenupro', 'refresh');
            else  redirect('front/utilisateur/menuconsommateurs', 'refresh');
        }
        $this->data['title'] = "Login";
        $last_url_from_form = $this->input->post('last_url');
        if (isset($last_url_from_form) && $last_url_from_form != "") $this->session->set_userdata('last_url', $last_url_from_form);

        //Verify if there is an redirection
        $last_url = $this->session->flashdata('last_url');
        $last_url_session = $this->session->userdata('last_url');
        if (isset($last_url) && $last_url != "") {
            $this->session->set_userdata('last_url', $last_url);
        } else if (isset($last_url_session) && $last_url_session != "") {
            $this->session->set_userdata('last_url', $last_url_session);
        }
        ////$this->firephp->log($last_url, 'last_url_OKOK');
        ////$this->firephp->log($last_url_session, '$last_url_session_OKOK');
        //validate form input
        $this->form_validation->set_rules('identity', 'Identity', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $form_page_user_from = $this->input->post('form_page_user_from');
        //////$this->firephp->log($form_page_user_from, 'form_page_user_from');

        //$pop_view = $this->input->get('pop_view');
        $pop_view = '0';
        //if (isset($pop_view)) $this->session->set_userdata('pop_view', $pop_view);
        if (isset($_GET['pop_view'])) $_SESSION['pop_view'] = $_GET['pop_view'];
        if (isset($_GET['account'])) $_SESSION['account'] = $_GET['account'];

        $from_publightbox = $this->input->post('from_publightbox');

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;
        $message_confirmation = $this->session->flashdata('message_confirmation');
        if (isset($message_confirmation) && $message_confirmation != "") $data['message_confirmation'] = $message_confirmation;
        if ($this->form_validation->run() == true) {
            //check to see if the user is logging in
            //check for "remember me"
            // go here
            $remember = (bool)$this->input->post('remember');
            if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {

              //not here

                //if the login is successful
                //redirect them back to the home page
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                $group_proo_club = array(3, 4, 5);

                $last_url = $this->session->userdata('last_url');

                if (isset($last_url) && $last_url != "") {
                    $this->session->unset_userdata('last_url');
                    redirect($last_url);
                } else {
                    if ($this->ion_auth->is_admin()) {
                        redirect('admin/home', 'refresh');
                    } else if (isset($from_publightbox) && $from_publightbox == "1") {
                        redirect('/', 'refresh');
                    }
                    if ($this->ion_auth->in_group(6)) {// admin association group
                        redirect('association/home', 'refresh');
                    } else if ($this->ion_auth->in_group(2)) {
                        if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
                            redirect('front/fidelity/menuconsommateurs', 'refresh');
                            //redirect('front/utilisateur/menuconsommateurs', 'refresh');
                        } else {
                            redirect('front/utilisateur/menuconsommateurs', 'refresh');
                        }

                    } else if ($this->ion_auth->in_group($group_proo_club)) {
                        //verify if user is active on "commercants" table
                        $user_ion_auth = $this->ion_auth->user()->row();
                        $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                        $verify_commercant_is_active = $this->ion_auth_used_by_club->verify_commercant_is_active($iduser);
                        if ($verify_commercant_is_active == true OR $verify_commercant_is_active == false) {
                            if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
                                redirect('front/fidelity/contenupro', 'refresh');
                                //redirect('front/utilisateur/contenupro', 'refresh');
                            } else {
                                redirect('front/utilisateur/contenupro', 'refresh');
                            }
                        } else {
                            $this->ion_auth->logout();
                            redirect('front/utilisateur/expired_account', 'refresh');
                        }
                    } else {
                        redirect('/', 'refresh');
                    }

                }

            }
            else {

               // not here too

                //if the login was un-successful
                //redirect them back to the login page

                $this->session->set_flashdata('message', $this->ion_auth->errors());
                $data_club['message'] = $this->ion_auth->errors();
                //redirect('auth/login', 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
                if (isset($form_page_user_from) && $form_page_user_from == '1') {
                    if (CURRENT_SITE == "agenda") {
                        $data_club['current_page'] = "espaceconsommateurs";
                        $this->_render_page('agendaAout2013/espaceconsommateurs', $data_club);
                    } else {
                        $this->_render_page('frontAout2013/espaceconsommateurs', $data_club);
                    }
                } else if (isset($form_page_user_from) && $form_page_user_from == '2') {
                    if (CURRENT_SITE == "agenda") {
                        $data_club['current_page'] = "avantagespro";
                        $this->_render_page('agendaAout2013/avantagespro', $data_club);
                    } else {
                        $this->_render_page('frontAout2013/avantagespro', $data_club);
                    }
                } else {
                    if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
                        //$this->_render_page('vwConnexion_mobile', $data_club) ;
                        //$this->_render_page('mobile2013/login', $data_club) ;
                        //$this->_render_page('privicarte/login', $this->data);
                        $this->_render_page('mobile2014/auth/login', $data_club);
                    } else {
                        redirect('auth/login', 'refresh');
                    }
                }
            }
            //not here
        }
        else {
            //the user is not logging in so display the login page
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $this->data['identity'] = array('name' => 'identity',
                'id' => 'identity',
                'type' => 'text',
                'value' => $this->form_validation->set_value('identity'),
            );
            $this->data['password'] = array('name' => 'password',
                'id' => 'password',
                'type' => 'password',
            );
            if (isset($form_page_user_from) && $form_page_user_from == '1') {
                $this->_render_page('frontAout2013/espaceconsommateurs', $this->data);
            } else if (isset($form_page_user_from) && $form_page_user_from == '2') {
                $this->_render_page('frontAout2013/avantagespro', $this->data);
            } else {
                if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
                    //$this->_render_page('vwConnexion_mobile', $this->data) ;
                    //$this->_render_page('mobile2013/login', $this->data) ;
                    //$this->_render_page('privicarte/login', $this->data);
                    $this->data['current_page'] = "espaceconsommateurs";
                    $this->_render_page('mobile2014/auth/login', $this->data);
                } else {
                    //$this->_render_page('auth/login', $this->data);
                    $this->_render_page('privicarte/login', $this->data);
                }
            }
        }
    }

    //log the user out
    function logout()
    {
        $this->data['title'] = "Logout";

        //log the user out
        $logout = $this->ion_auth->logout();

        //redirect them to the login page
        $this->session->set_flashdata('message', $this->ion_auth->messages());
        //redirect('auth/login', 'refresh');
        redirect(base_url(), 'refresh');
    }

    //change password
    function change_password()
    {
        $this->form_validation->set_rules('old', 'Old password', 'required');
        $this->form_validation->set_rules('new', 'New Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
        $this->form_validation->set_rules('new_confirm', 'Confirm New Password', 'required');

        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }

        $user = $this->ion_auth->user()->row();

        if ($this->form_validation->run() == false) {
            //display the form
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
            $this->data['old_password'] = array(
                'name' => 'old',
                'id' => 'old',
                'type' => 'password',
            );
            $this->data['new_password'] = array(
                'name' => 'new',
                'id' => 'new',
                'type' => 'password',
                'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
            );
            $this->data['new_password_confirm'] = array(
                'name' => 'new_confirm',
                'id' => 'new_confirm',
                'type' => 'password',
                'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
            );
            $this->data['user_id'] = array(
                'name' => 'user_id',
                'id' => 'user_id',
                'type' => 'hidden',
                'value' => $user->id,
            );

            //render
            $this->_render_page('auth/change_password', $this->data);
        } else {
            $identity = $this->session->userdata($this->config->item('identity', 'ion_auth'));

            $change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

            if ($change) {
                //if the password was successfully changed
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                $this->logout();
            } else {
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect('auth/change_password', 'refresh');
            }
        }
    }

    //forgot password
    function forgot_password()
    {

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->data['titre'] = "Mot de passe oubli&eacute;";
        $this->form_validation->set_rules('email', 'Email Address', 'required');
        if ($this->form_validation->run() == false) {
            //setup the input
            $this->data['email'] = array('name' => 'email',
                'id' => 'email', 'class' => 'form-control'
            );

            if ($this->config->item('identity', 'ion_auth') == 'username') {
                $this->data['identity_label'] = 'Username';
            } else {
                $this->data['identity_label'] = 'Email';
            }

            //set any errors and display the form
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
                $this->_render_page('mobile2014/auth/forgot_password_mobile', $this->data);
            } else {
                $this->_render_page('auth/forgot_password', $this->data);
            }
        } else {
            // get identity for that email
            $config_tables = $this->config->item('tables', 'ion_auth');
            $identity = $this->db->where('email', $this->input->post('email'))->limit('1')->get($config_tables['users'])->row();

            ////$this->firephp->log($identity, 'identity');

            //run the forgotten password method to email an activation code to the user
            $forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

            if ($forgotten) {
                //if there were no errors
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect("auth/login", 'refresh'); //we should display a confirmation page here instead of the login page
            } else {
                $this->session->set_flashdata('message', $this->ion_auth->errors());

                if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
                    redirect("auth/forgot_password_mobile", 'refresh');
                } else {
                    redirect("auth/forgot_password", 'refresh');
                }
            }
        }
    }


    //reset password - final step for forgotten password
    public function reset_password($code = NULL)
    {
        if (!$code) {
            show_404();
        }

        $user = $this->ion_auth->forgotten_password_check($code);

        if ($user) {
            //if the code is valid then display the password reset form

            $this->form_validation->set_rules('new', 'Nouveau mot de passe', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
            $this->form_validation->set_rules('new_confirm', 'Confirmation Nouveau mot de passe', 'required');

            if ($this->form_validation->run() == false) {
                //display the form

                //set the flash data error message if there is one
                $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

                $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
                $this->data['new_password'] = array(
                    'name' => 'new',
                    'id' => 'new',
                    'type' => 'password',
                    'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
                    'class' => 'form-control'
                );
                $this->data['new_password_confirm'] = array(
                    'name' => 'new_confirm',
                    'id' => 'new_confirm',
                    'type' => 'password',
                    'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
                    'class' => 'form-control'
                );
                $this->data['user_id'] = array(
                    'name' => 'user_id',
                    'id' => 'user_id',
                    'type' => 'hidden',
                    'value' => $user->id,
                );
                $this->data['csrf'] = $this->_get_csrf_nonce();
                $this->data['code'] = $code;

                //render
                $this->_render_page('auth/reset_password', $this->data);
            } else {
                // do we have a valid request?
                if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id')) {

                    //something fishy might be up
                    $this->ion_auth->clear_forgotten_password_code($code);

                    show_error('This form post did not pass our security checks.');

                } else {
                    // finally change the password
                    $identity = $user->{$this->config->item('identity', 'ion_auth')};

                    $change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

                    if ($change) {
                        //if the password was successfully changed
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                        $this->logout();
                    } else {
                        $this->session->set_flashdata('message', $this->ion_auth->errors());
                        redirect('auth/reset_password/' . $code, 'refresh');
                    }
                }
            }
        } else {
            //if the code is invalid then send them back to the forgot password page
            $this->session->set_flashdata('message', $this->ion_auth->errors());
            redirect("auth/forgot_password", 'refresh');
        }
    }


    //activate the user
    function activate($id, $code = false,$isFromMail = 0)
    {
        if ($code !== false) {
            $activation = $this->ion_auth->activate($id, $code);
        } else if ($this->ion_auth->is_admin()) {
            $activation = $this->ion_auth->activate($id);
        }

        if ($activation) {

            if (!$this->ion_auth->in_group(2, $id) && !$this->ion_auth->in_group(6, $id)) {
                //activate account at commercant table
                $IdCommercant_ion_auth_id = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($id);
                $objCommercant = $this->Commercant->GetById($IdCommercant_ion_auth_id);
                $objCommercant->IsActif = 1;
                $objCommercant->datecreation = time();
                $objCommercantUpdate = $this->Commercant->Update($objCommercant);

                //changing group from basic to premium
                $user_groups = $this->ion_auth->get_users_groups($id)->result();
                if (isset($user_groups[0]->id)) $this->ion_auth->remove_from_group($user_groups[0]->id, $id);
                $this->ion_auth->add_to_group(4, $id);

                //set subscription for one year
                $objAssCommercantAbonnement["IdCommercant"] = $IdCommercant_ion_auth_id;
                $objAssCommercantAbonnement["IdAbonnement"] = 3;//ABONNEMENT PREMIUM 1ER ABONNEMENT GRATUIT at abonnements table
                $objAssCommercantAbonnement["DateDebut"] = date("Y-m-d");
                $objAssCommercantAbonnement["DateFin"] = date('Y-m-d', strtotime(date("Y-m-d", mktime()) . " + 365 day"));//date added one year
                $this->AssCommercantAbonnement->Insert($objAssCommercantAbonnement);
            }

            //redirect them to the auth page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("auth/index/1", 'refresh');
        } else {
            //redirect them to the forgot password page
            $this->session->set_flashdata('message', $this->ion_auth->errors());
            redirect("auth/forgot_password", 'refresh');
        }
    }

    //deactivate the user
    function deactivate($id = NULL)
    {
        $id = $this->config->item('use_mongodb', 'ion_auth') ? (string)$id : (int)$id;

        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirm', 'confirmation', 'required');
        $this->form_validation->set_rules('id', 'user ID', 'required|alpha_numeric');

        if ($this->form_validation->run() == FALSE) {
            // insert csrf check
            $this->data['csrf'] = $this->_get_csrf_nonce();
            $this->data['user'] = $this->ion_auth->user($id)->row();

            $this->_render_page('auth/deactivate_user', $this->data);
        } else {
            // do we really want to deactivate?
            if ($this->input->post('confirm') == 'yes') {
                // do we have a valid request?
                if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id')) {
                    show_error('This form post did not pass our security checks.');
                }

                // do we have the right userlevel?
                if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
                    $this->ion_auth->deactivate($id);
                }
            }

            //redirect them back to the auth page
            redirect('auth', 'refresh');
        }
    }

    //create a new user
    function create_user()
    {
        $this->data['title'] = "Create User";

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth', 'refresh');
        }

        //validate form input
        $this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|xss_clean');
        $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
        $this->form_validation->set_rules('phone1', 'First Part of Phone', 'required|xss_clean|min_length[3]|max_length[3]');
        $this->form_validation->set_rules('phone2', 'Second Part of Phone', 'required|xss_clean|min_length[3]|max_length[3]');
        $this->form_validation->set_rules('phone3', 'Third Part of Phone', 'required|xss_clean|min_length[4]|max_length[4]');
        $this->form_validation->set_rules('company', 'Company Name', 'required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'required');

        if ($this->form_validation->run() == true) {
            $username = strtolower($this->input->post('first_name')) . ' ' . strtolower($this->input->post('last_name'));
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'company' => $this->input->post('company'),
                'phone' => $this->input->post('phone1') . '-' . $this->input->post('phone2') . '-' . $this->input->post('phone3'),
            );
        }
        if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data)) {
            //check to see if we are creating the user
            //redirect them back to the admin page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("auth", 'refresh');
        } else {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['first_name'] = array(
                'name' => 'first_name',
                'id' => 'first_name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('first_name'),
            );
            $this->data['last_name'] = array(
                'name' => 'last_name',
                'id' => 'last_name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('last_name'),
            );
            $this->data['email'] = array(
                'name' => 'email',
                'id' => 'email',
                'type' => 'text',
                'value' => $this->form_validation->set_value('email'),
            );
            $this->data['company'] = array(
                'name' => 'company',
                'id' => 'company',
                'type' => 'text',
                'value' => $this->form_validation->set_value('company'),
            );
            $this->data['phone1'] = array(
                'name' => 'phone1',
                'id' => 'phone1',
                'type' => 'text',
                'value' => $this->form_validation->set_value('phone1'),
            );
            $this->data['phone2'] = array(
                'name' => 'phone2',
                'id' => 'phone2',
                'type' => 'text',
                'value' => $this->form_validation->set_value('phone2'),
            );
            $this->data['phone3'] = array(
                'name' => 'phone3',
                'id' => 'phone3',
                'type' => 'text',
                'value' => $this->form_validation->set_value('phone3'),
            );
            $this->data['password'] = array(
                'name' => 'password',
                'id' => 'password',
                'type' => 'password',
                'value' => $this->form_validation->set_value('password'),
            );
            $this->data['password_confirm'] = array(
                'name' => 'password_confirm',
                'id' => 'password_confirm',
                'type' => 'password',
                'value' => $this->form_validation->set_value('password_confirm'),
            );

            $this->_render_page('auth/create_user', $this->data);
        }
    }

    //edit a user
    function edit_user($id)
    {
        $this->data['title'] = "Edit User";

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth', 'refresh');
        }

        $user = $this->ion_auth->user($id)->row();
        $groups = $this->ion_auth->groups()->result_array();
        $currentGroups = $this->ion_auth->get_users_groups($id)->result();

        //process the phone number
        if (isset($user->phone) && !empty($user->phone)) {
            $user->phone = explode('-', $user->phone);
        }

        //validate form input
        $this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|xss_clean');
        $this->form_validation->set_rules('phone1', 'First Part of Phone', 'required|xss_clean|min_length[3]|max_length[3]');
        $this->form_validation->set_rules('phone2', 'Second Part of Phone', 'required|xss_clean|min_length[3]|max_length[3]');
        $this->form_validation->set_rules('phone3', 'Third Part of Phone', 'required|xss_clean|min_length[4]|max_length[4]');
        $this->form_validation->set_rules('company', 'Company Name', 'required|xss_clean');
        $this->form_validation->set_rules('groups', 'Groups', 'xss_clean');

        if (isset($_POST) && !empty($_POST)) {
            // do we have a valid request?
            if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id')) {
                show_error('This form post did not pass our security checks.');
            }

            $data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'company' => $this->input->post('company'),
                'phone' => $this->input->post('phone1') . '-' . $this->input->post('phone2') . '-' . $this->input->post('phone3'),
            );

            //Update the groups user belongs to
            $groupData = $this->input->post('groups');

            if (isset($groupData) && !empty($groupData)) {

                $this->ion_auth->remove_from_group('', $id);

                foreach ($groupData as $grp) {
                    $this->ion_auth->add_to_group($grp, $id);
                }

            }

            //update the password if it was posted
            if ($this->input->post('password')) {
                $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
                $this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'required');

                $data['password'] = $this->input->post('password');
            }

            if ($this->form_validation->run() === TRUE) {
                $this->ion_auth->update($user->id, $data);

                //check to see if we are creating the user
                //redirect them back to the admin page
                $this->session->set_flashdata('message', "User Saved");
                redirect("auth", 'refresh');
            }
        }

        //display the edit user form
        $this->data['csrf'] = $this->_get_csrf_nonce();

        //set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

        //pass the user to the view
        $this->data['user'] = $user;
        $this->data['groups'] = $groups;
        $this->data['currentGroups'] = $currentGroups;

        $this->data['first_name'] = array(
            'name' => 'first_name',
            'id' => 'first_name',
            'type' => 'text',
            'value' => $this->form_validation->set_value('first_name', $user->first_name),
        );
        $this->data['last_name'] = array(
            'name' => 'last_name',
            'id' => 'last_name',
            'type' => 'text',
            'value' => $this->form_validation->set_value('last_name', $user->last_name),
        );
        $this->data['company'] = array(
            'name' => 'company',
            'id' => 'company',
            'type' => 'text',
            'value' => $this->form_validation->set_value('company', $user->company),
        );
        $this->data['phone1'] = array(
            'name' => 'phone1',
            'id' => 'phone1',
            'type' => 'text',
            'value' => $this->form_validation->set_value('phone1', $user->phone[0]),
        );
        $this->data['phone2'] = array(
            'name' => 'phone2',
            'id' => 'phone2',
            'type' => 'text',
            'value' => $this->form_validation->set_value('phone2', $user->phone[1]),
        );
        $this->data['phone3'] = array(
            'name' => 'phone3',
            'id' => 'phone3',
            'type' => 'text',
            'value' => $this->form_validation->set_value('phone3', $user->phone[2]),
        );
        $this->data['password'] = array(
            'name' => 'password',
            'id' => 'password',
            'type' => 'password'
        );
        $this->data['password_confirm'] = array(
            'name' => 'password_confirm',
            'id' => 'password_confirm',
            'type' => 'password'
        );

        $this->_render_page('auth/edit_user', $this->data);
    }

    // create a new group
    function create_group()
    {
        $this->data['title'] = "Create Group";

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth', 'refresh');
        }

        //validate form input
        $this->form_validation->set_rules('group_name', 'Group name', 'required|alpha_dash|xss_clean');
        $this->form_validation->set_rules('description', 'Description', 'xss_clean');

        if ($this->form_validation->run() == TRUE) {
            $new_group_id = $this->ion_auth->create_group($this->input->post('group_name'), $this->input->post('description'));
            if ($new_group_id) {
                // check to see if we are creating the group
                // redirect them back to the admin page
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect("auth", 'refresh');
            }
        } else {
            //display the create group form
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['group_name'] = array(
                'name' => 'group_name',
                'id' => 'group_name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('group_name'),
            );
            $this->data['description'] = array(
                'name' => 'description',
                'id' => 'description',
                'type' => 'text',
                'value' => $this->form_validation->set_value('description'),
            );

            $this->_render_page('auth/create_group', $this->data);
        }
    }

    //edit a group
    function edit_group($id)
    {
        // bail if no group id given
        if (!$id || empty($id)) {
            redirect('auth', 'refresh');
        }

        $this->data['title'] = "Edit Group";

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth', 'refresh');
        }

        $group = $this->ion_auth->group($id)->row();

        //validate form input
        $this->form_validation->set_rules('group_name', 'Group name', 'required|alpha_dash|xss_clean');
        $this->form_validation->set_rules('group_description', 'Group Description', 'xss_clean');

        if (isset($_POST) && !empty($_POST)) {
            if ($this->form_validation->run() === TRUE) {
                $group_update = $this->ion_auth->update_group($id, $_POST['group_name'], $_POST['group_description']);

                if ($group_update) {
                    $this->session->set_flashdata('message', "Group Saved");
                } else {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
                }
                redirect("auth", 'refresh');
            }
        }

        //set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

        //pass the user to the view
        $this->data['group'] = $group;

        $this->data['group_name'] = array(
            'name' => 'group_name',
            'id' => 'group_name',
            'type' => 'text',
            'value' => $this->form_validation->set_value('group_name', $group->name),
        );
        $this->data['group_description'] = array(
            'name' => 'group_description',
            'id' => 'group_description',
            'type' => 'text',
            'value' => $this->form_validation->set_value('group_description', $group->description),
        );

        $this->_render_page('auth/edit_group', $this->data);
    }


    function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    function _valid_csrf_nonce()
    {
        if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
            $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue')
        ) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function _render_page($view, $data = null, $render = false)
    {

        $this->viewdata = (empty($data)) ? $this->data : $data;

        $view_html = $this->load->view($view, $this->viewdata, $render);

        if (!$render) return $view_html;
    }

    //fonction login dans ionic//
    function logionic($identity,$password)
    {

        if ($this->ion_auth->login($identity,$password)) {
            //if the login is successful
            //redirect them back to the home page
            $this->session->set_flashdata('message', $this->ion_auth->messages());

            $group_proo_club = array(3, 4, 5);
            $group_part_club = array(2,2);
            if ($this->ion_auth->in_group($group_proo_club)) {
                //verify if user is active on "commercants" table
                $user_ion_auth = $this->ion_auth->user()->row();
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                $verify_commercant_is_active = $this->ion_auth_used_by_club->verify_commercant_is_active($iduser);
                if ($verify_commercant_is_active == true) {

                    $last_url = $this->session->userdata('last_url');

                    if (isset($last_url) && $last_url != "") {
                        $this->session->unset_userdata('last_url');
                        redirect($last_url);
                    }

                        //redirect('front/utilisateur/contenupro', 'refresh');
                } else {
                    $this->ion_auth->logout();
                    redirect('front/utilisateur/expired_account', 'refresh');
                }
            }elseif($this->ion_auth->in_group($group_part_club)){
                $user_ion_auth = $this->ion_auth->user()->row();
                $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                if(isset($iduser) AND $iduser != null AND $iduser != ""){
                    $last_url = $this->session->userdata('last_url_part');
                    if (isset($last_url) && $last_url != "") {
                        $this->session->unset_userdata('last_url_part');
                        redirect($last_url);
                    }
                }else{
                    echo 'error';
                }            
            }

            $last_url = $this->session->userdata('last_url');

            if (isset($last_url) && $last_url != "") {
                $this->session->unset_userdata('last_url');
            }

            }

        }


    //}

    ////////si logé renvoie un donne json vers ionic////////
    public function login_sortez_pro(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $identity=$_POST["identity"];
        $password=$_POST["password"];
        $last_url =site_url("front/login/auth_or_not");
        $last_url_part =site_url("front/login/auth_or_not_part");
        $this->session->set_userdata('last_url_part',$last_url_part);
        $this->session->set_userdata('last_url',$last_url);
        $this->logionic($identity,$password);
    }

    function forgot_password_mobile()
    {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $email=$_POST['login'];
        $this->form_validation->set_rules('email', 'Email Address', 'required');
            // get identity for that email
            $config_tables = $this->config->item('tables', 'ion_auth');
            $identity = $this->db->where('email', $email)->limit('1')->get($config_tables['users'])->row();
            $forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});
            if ($forgotten) {
                //if there were no errors
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                 $data['json']="ok";
                 echo json_encode($data);
            } else {

            }
        }

}







