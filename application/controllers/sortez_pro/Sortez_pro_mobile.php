<?php
class sortez_pro_mobile extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model ( "mdl_card" );
        $this->load->model ( "mdl_card_bonplan_used" );
        $this->load->model ( "mdl_card_capital" );
        $this->load->model ( "mdl_card_tampon" );
        $this->load->model ( "mdl_card_remise" );
        $this->load->model ( "mdl_card_user_link" );
        $this->load->model ( "mdl_card_fiche_client_tampon" );
        $this->load->model ( "mdl_card_fiche_client_capital" );
        $this->load->model ( "mdl_card_fiche_client_remise" );
        $this->load->model ( 'assoc_client_bonplan_model' );
        $this->load->model ( 'assoc_client_commercant_model' );
        $this->load->Model ( "Ville" );
        $this->load->Model ( "Assoc_client_table_model" );
        $this->load->model ( "user" );
        $this->load->model ( "mdlville" );
        $this->load->model ( "mdlbonplan" );
        $this->load->model ( "mdldepartement" );
        $this->load->model ( "mdldemande_abonnement" );
        $this->load->model ( "parametre" );
        $this->load->model ( "mdlcategorie" );
        $this->load->model ( "commercant" );
        $this->load->model ( "Abonnement" );
        $this->load->model ( "Mdlcommercant" );
        $this->load->model ( "mdlstatut" );
        $this->load->model("Mdl_prospect");

        $this->load->model ( "mdl_card_tampon" );
        $this->load->model ( "mdl_card_capital" );
        $this->load->model ( "mdl_card_remise" );

        $this->load->library ( 'session' );

        $this->load->library ( 'ion_auth' );
        $this->load->model ( "ion_auth_used_by_club" );
        $this->load->library('ion_auth');
        $this->load->model("api_model");
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper('url');
    }
    public function tester(){
        header("Access-Control-Allow-Origin: *");

        $data[0] = [ "Id" => '1', "Nom" => 'Rakoto', "Adresse" => 'Lot 192 AI ter', "Telephone" => '0347856425' ];
        $data[1] = [ "Id" => '2', "Nom" => 'Rasoa', "Adresse" => 'Lot 193 AI ter', "Telephone" => '0337856425' ];
        $data[2] = [ "Id" => '3', "Nom" => 'Rangita', "Adresse" => 'Lot 194 AI ter', "Telephone" => '0327856425' ];
//        var_dump($data);die('test');

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));

    }

    public function get_tester(){
        header("Access-Control-Allow-Origin: *");

        $clientId = $this->input->post('productId');

//        $product = array("Id" => '1', "Nom" => 'Rakoto', "Adresse" => 'Lot 192 AI ter', "Telephone" => '0347856425') ;

        $data[0] = [ "Id" => '1', "Nom" => 'Rakoto', "Adresse" => 'Lot 192 AI ter', "Telephone" => '0347856425' ];
        $data[1] = [ "Id" => '2', "Nom" => 'Rasoa', "Adresse" => 'Lot 193 AI ter', "Telephone" => '0337856425' ];
        $data[2] = [ "Id" => '3', "Nom" => 'Rangita', "Adresse" => 'Lot 194 AI ter', "Telephone" => '0327856425' ];

        foreach ($data as $datas){
            if($datas['Id'] == $clientId){
                $product = array($datas);
            }
        }

        $productData = array(
            'Id' => $product[0]['Id'],
            'Nom' => $product[0]['Nom'],
            'Adresse' => $product[0]['Adresse'],
            'Telephone' => $product[0]['Telephone']
        );
        //var_dump($productData);die('test');
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($productData));
    }

    public function createProduct()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Request-Headers: GET,POST,OPTIONS,DELETE,PUT");

        $formdata = json_decode(file_get_contents('php://input'), true);

        if( ! empty($formdata)) {

            $productName = $formdata['productName'];
            $sku = $formdata['sku'];
            $price = $formdata['price'];

            $productData = array(
                'product_name' => $productName,
                'price' => $price,
                'sku' => $sku,
                'is_active' => 1,
                'created_at' => date('Y-m-d H:i:s', time())
            );

            //$id = $this->reactapi_model->insert_product($productData);

            $response = array(
                'status' => 'success',
                'message' => 'Product added successfully'
            );
        }
        else {
            $response = array(
                'status' => 'error'
            );
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

    public function editProduct()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Request-Headers: GET,POST,OPTIONS,DELETE,PUT");

        $formdata = json_decode(file_get_contents('php://input'), true);

        if( ! empty($formdata)) {

            $id = $formdata['id'];
            $productName = $formdata['productName'];
            $sku = $formdata['sku'];
            $price = $formdata['price'];

            $productData = array(
                'product_name' => $productName,
                'price' => $price,
                'sku' => $sku
            );

            //$id = $this->reactapi_model->update_product($id, $productData);

            $response = array(
                'status' => 'success',
                'message' => 'Product updated successfully.'
            );
        }
        else {
            $response = array(
                'status' => 'error'
            );
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }
    public function deleteProduct()
    {
        header("Access-Control-Allow-Origin: *");

        $clientsid = $this->input->post('clientsid');

//        $product = $this->reactapi_model->delete_product($productId);

        $response = array(
            'message' => 'Product deleted successfully.'
        );

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

    public function get_login_by_id(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $login=$_POST["login"];
        $request=$this->api_model->get_login_by_id($login);
        $json['json']=$request;
        // var_dump($request->IdCommercant);die("fin");
        if(is_object($request)){
        $objTampon = $this->mdl_card_tampon->getByIdCommercant($request->IdCommercant);
        $objCapital = $this->mdl_card_capital->getByIdCommercant($request->IdCommercant);
        $objRemise = $this->mdl_card_remise->getByIdCommercant($request->IdCommercant);
        if (!empty($objTampon) OR !empty($objCapital) OR !empty($objRemise)){     
            
            if (isset($objTampon->is_activ) AND $objTampon->is_activ=='1'){
                // $_iTypeFidelity='tampon';
                $json['TypeFidelity']='tampon';
                $json['fidelityid']=$objTampon->id;
            }
            if (isset($objCapital->id) AND $objCapital->is_activ=='1'){
                // $_iTypeFidelity='capital';
                $json['TypeFidelity']='capital';
                $json['fidelityid']=$objCapital->id;
            }
            if (isset($objRemise->id) AND $objRemise->is_activ=='1'){
                // $_iTypeFidelity='remise';
                $json['TypeFidelity']='remise';
                $json['fidelityid']=$objRemise->id;
            }
        }   
        } 
        echo json_encode($json);
    }
    public function get_client_list(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $login=$_POST["login"];
        //$login="randawilly8@gmail.com";
        $data=$this->api_model->get_id_commercant($login);
        $clientlist=$this->api_model->getall($data);
        foreach ($clientlist as $data){
            $result=$this->api_model->get_name($data);
            $json['json']=$result;
            echo json_encode($json);
        }

        //$json['json']=$clientlist;
        //echo json_encode($json);

    }
    public function liste_clients($action = false, $nb = null) {

        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $login=$_POST["login"];
        $datas=$this->api_model->get_login_by_id2($login);
        $id_commercant =$datas->IdCommercant;
        $mail = "";
        $list_client = $this->api_model->get_client_fidelity_bonplan_by_commercant_id ($id_commercant);
        $email_clients = array ();
        if (count( $list_client ) !=0) {
            foreach ( $list_client as $cleint ) {
                $mail = "mailto:contact@sortez.org?subject=Message";
                $mail .= "&bcc=$cleint->Email";
                $email_clients [] = $cleint->Email;
            }
            $clients = array ();
            foreach ( $list_client as $client ) {
                $fidelity = get_fidelity ( $client->id_commercant );
                $offre = get_offre_active ( $client->id_commercant );
                $fiche = get_fiche_client ( $offre, $client->id_client, $client->id_commercant );
                $user_bonplan_brut = get_user_bonplan_mobile ( $client->id_client, $client->id_commercant );
                $user_bonplan = json_decode(json_encode($user_bonplan_brut), FALSE);
                $plat=$this->api_model->get_plat_list_by_login($client->id_commercant,$client->id_client);
                $infocom=$this->api_model->infoCommercant($id_commercant);
                $client->offre = $offre;
                $client->solde = ! empty ( $fiche ) ? $fiche->solde : 0;

                switch ($offre) {
                    case "capital" :
                        $client->objectif = (is_object ( $fidelity ) && is_object ( $fidelity->capital )) ? $fidelity->capital->montant : 0;
                        $client->description_offre = (is_object ( $fidelity ) && is_object ( $fidelity->capital )) ? $fidelity->capital->description : 0;
                        break;
                    case "tampon" :
                        $client->objectif = (is_object ( $fidelity ) && is_object ( $fidelity->tampon )) ? $fidelity->tampon->tampon_value : 0;
                        $client->description_offre = (is_object ( $fidelity ) && is_object ( $fidelity->tampon )) ? $fidelity->tampon->description : 0;
                        break;
                    case "remise" :
                        $client->objectif = $client->remise->solde;
                        $client->description_offre = (is_object ( $fidelity ) && is_object ( $fidelity->remise )) ? $fidelity->remise->description : 0;
                        break;
                }

                $client->bonplan = $user_bonplan;
                $client->commercant=$infocom;
                $client->plat = $plat;
                $clients [] = $client;
            }

            if ($action == "solde_desc")
                usort ( $clients, "solde_desc" );
            if ($action == "solde_asc")
                usort ( $clients, "solde_asc" );
            echo json_encode($clients);die();
        }


    }
    public function get_detail_client(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_client=$_POST["id_client"];
        $id_commercant=$_POST["id_commercant"];
        //$id_client=340;
        //$id_commercant=301327;
        $result=$this->api_model->get_detail_client($id_client,$id_commercant);
        $json['json']=$result;
        echo json_encode($json);
    }
    function liste_reservation() {

        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $filtre = null;
        $ordre = null;
        $criteres ['valide'] = 0;
        $id_commercant=$_POST["id_commercant"];
        $criteres ['id_commercant'] = $id_commercant;
        $order_by ['assoc_client_bonplan.date_validation'] = ($ordre == "desc") ? "desc" : "asc";
        $data ['list_client'] = $this->api_model->get_list_by_criteria ( $criteres, $order_by );

        // echo $this->db->last_query();

        $data ['mdlbonplan'] = $this->mdlbonplan;
        $data ['assoc_client_bonplan_model'] = $this->api_model;

        var_dump($data);

    }

    function edittampon() {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_ionauth=$_POST["id_ionauth"];
        $id_commercant=$_POST["id_commercant"];
        $tampon_value=$_POST["tampon_value"];
        $is_activ=$_POST["is_activ"];
        if (isset($_POST["id"])){$id=$_POST["id"];} else{$id="";}
        //var_dump($is_activ);die();
        try {
            $objTampon =$_POST;



            $objTampon ["tampon_value"] =$tampon_value;

            // $user_ion_auth_id = $this->ion_auth->user ()->row ();
            //var_dump($user_ion_auth_id);
            $objTampon ["id_ionauth"] =$id_ionauth;
            $objTampon ["id_commercant"] =$id_commercant;

            $objTampon ["is_activ"] = (isset ( $is_activ )) ? 1 : 0;
            //var_dump($objTampon);die();
            if ($id == "" || $id == null || $id == "0") {
                $this->mdl_card_tampon->insert ( $objTampon );
            } else {
                $this->mdl_card_tampon->update ( $objTampon );
                $this->mdl_card_tampon->delete_where ( array (
                    'id_commercant' => $id_commercant,
                    'id !=' => $id
                ) );
            }

            if ($is_activ) {
                $objCapital = $this->mdl_card_capital->getByIdCommercant ($id_commercant );
                if (is_object ( $objCapital )) {
                    $arrayCapital ['id'] = $objCapital->id;
                    $arrayCapital ['is_activ'] = 0;
                    $this->mdl_card_capital->update ( $arrayCapital );
                }
                $objRemise = $this->mdl_card_remise->getByIdCommercant ( $id_commercant );
                if (is_object ( $objRemise )) {
                    $arrayRemise ['id'] = $objRemise->id;
                    $arrayRemise ['is_activ'] = 0;
                    $this->mdl_card_capital->update2 ( $arrayRemise );
                }
            }
            $this->session->set_flashdata ( 'message_success', 'Vos paramètres ont été mis à jour' );
            $json['json']="ok";
            echo json_encode($json);
        } catch ( Exception $e ) {
            redirect ( "front/fidelity_pro/tampon", 'refresh' );
        } }
    function editcapital() {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_ionauth=$_POST["id_ionauth"];
        $id_commercant=$_POST["id_commercant"];
        $montant=$_POST["montant"];
        $is_activ=$_POST["is_activ"];
        if (isset($_POST["id"])){$id=$_POST["id"];} else{$id="";}
        try {
            $objCapital = $_POST;
            // $objCapital["remise_value"] = trim($objCapital["remise_value"]);
            $objCapital ["montant"] = $montant;

            $objCapital ["id_ionauth"] = $id_ionauth;
            $objCapital ["id_commercant"] = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $id_ionauth );

            $objCapital ["is_activ"] = (isset ( $is_activ )) ? 1 : 0;

            if ($id == "" || $id == null || $id == "0") {
                $this->mdl_card_capital->insert ( $objCapital );
            } else {
                $this->mdl_card_capital->update ( $objCapital );
            }


            if ($is_activ) {
                $objTampon = $this->mdl_card_tampon->getByIdCommercant ( $id_commercant );
                if (is_object ( $objTampon )) {
                    $arrayTampon ['id'] = $objTampon->id;
                    $arrayTampon ['is_activ'] =0;
                    $this->mdl_card_tampon->update ( $arrayTampon );
                }
                $objRemise = $this->mdl_card_remise->getByIdCommercant ( $id_commercant );
                if (is_object ( $objRemise )) {
                    $arrayRemise ['id'] = $objRemise->id;
                    $arrayRemise ['is_activ'] = 0;
                    $this->mdl_card_remise->update ( $arrayRemise );
                }
            }
            $json['json']="ok";
            echo json_encode($json);
        } catch ( Exception $e ) {
            return "error";
        }
    }
    function editremise() {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_ionauth=$_POST["id_ionauth"];
        $id_commercant=$_POST["id_commercant"];
        $tampon_value=$_POST["montant"];
        $is_activ=$_POST["is_activ"];
        if (isset($_POST["id"])){$id=$_POST["id"];} else{$id="";}
        //var_dump($is_activ);die();

            $objRemise = $_POST;

            if (! empty ( $objRemise )) {

                $objRemise ["id_ionauth"] = $id_ionauth;
                $objRemise ["id_commercant"] = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $id_ionauth );

                $objRemise ["is_activ"] = (isset ( $is_activ )) ? 1 : 0;

                if ($id == "" || $id == null || $id == "0") {
                    $this->mdl_card_remise->insert ( $objRemise );
                } else {
                    $this->mdl_card_remise->update ( $objRemise );
                }
                if ($objRemise ["is_activ"]) {
                    $objCapital = $this->mdl_card_capital->getByIdCommercant ( $id_commercant );
                    if (is_object ( $objCapital )) {
                        $arrayCapital ['id'] = $objCapital->id;
                        $arrayCapital ['is_activ'] = 0;
                        $this->mdl_card_capital->update ( $arrayCapital );
                    }
                    $objTampon = $this->mdl_card_tampon->getByIdCommercant ( $id_commercant );
                    if (is_object ( $objTampon )) {
                        $arrayTampon ['id'] = $objTampon->id;
                        $arrayTampon ['is_activ'] = 0;
                        $this->mdl_card_tampon->update($arrayTampon);
                    }
                }
                $json['json']="ok";
                echo json_encode($json);
            }
    }
    function creerBonplan(){
        //$_iDCommercant = 0;
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $oBonplan = $_POST;
        $bp_unique_qttprop=$_POST["bp_unique_qttprop"];
        $bp_multiple_qttprop=$_POST["bp_multiple_qttprop"];
        $oBonplan['bonplan_quantite_depart_unique'] =$bp_unique_qttprop;
        $oBonplan['bonplan_quantite_depart_multiple'] = $bp_multiple_qttprop;
        $bonplan_date_debut=$_POST["bonplan_date_debut"];
        $bonplan_date_fin=$_POST["bonplan_date_fin"];
        $bp_unique_date_debut=$_POST["bp_unique_date_debut"];
        $bp_unique_date_fin=$_POST["bp_unique_date_fin"];
        //$bp_unique_date_visit=$_POST["bp_unique_date_visit"];
        $bp_multiple_date_debut=$_POST["bp_multiple_date_debut"];
        $bp_multiple_date_fin=$_POST["bp_multiple_date_fin"];
        //$bp_multiple_date_visit=$_POST["bp_multiple_date_visit"];

        /*$oBonplan["bonplan_photo1"] = $_POST["photo1Associe");
        $oBonplan["bonplan_photo2"] = $_POST["photo2Associe");
        $oBonplan["bonplan_photo3"] = $_POST["photo3Associe");
        $oBonplan["bonplan_photo4"] = $_POST["photo4Associe");*/

        if ($bonplan_date_debut=="--" || $bonplan_date_debut=="0000-00-00") $bonplan_date_debut = null;
        if ($bonplan_date_fin=="--" || $bonplan_date_fin=="0000-00-00") $bonplan_date_fin = null;

        if ($bp_unique_date_debut=="--" || $bp_unique_date_debut=="0000-00-00") $bp_unique_date_debut = null;
        if ($bp_unique_date_fin=="--" || $bp_unique_date_fin=="0000-00-00") $bp_unique_date_fin = null;
        //if ($bp_unique_date_visit=="--" || $bp_unique_date_visit=="0000-00-00") $bp_unique_date_visit = null;

        if ($bp_multiple_date_debut=="--" || $bp_multiple_date_debut=="0000-00-00") $bp_multiple_date_debut = null;
        if ($bp_multiple_date_fin=="--" || $bp_multiple_date_fin=="0000-00-00") $bp_multiple_date_fin = null;
        //if ($bp_multiple_date_visit=="--" || $bp_multiple_date_visit=="0000-00-00") $bp_multiple_date_visit = null;

        $IdInsertedBonplan = $this->api_model->insertBonplan($oBonplan);

        //$this->load->view('front/vwListeBonPlan', $data) ;
        // $this->load->view('front/vwFicheBonplan', $data) ;
        if ($IdInsertedBonplan){$json['json']="ok";
            echo json_encode($json);}

    }
    function modifBonplan(){
        //$_iDCommercant = 0;
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $oBonplan = $_POST;
        $bp_unique_qttprop=$_POST["bp_unique_qttprop"];
        $bp_multiple_qttprop=$_POST["bp_multiple_qttprop"];
        $oBonplan['bonplan_quantite_depart_unique'] =$bp_unique_qttprop;
        $oBonplan['bonplan_quantite_depart_multiple'] = $bp_multiple_qttprop;
        $bonplan_date_debut=$_POST["bonplan_date_debut"];
        $bonplan_date_fin=$_POST["bonplan_date_fin"];
        $bp_unique_date_debut=$_POST["bp_unique_date_debut"];
        $bp_unique_date_fin=$_POST["bp_unique_date_fin"];
        //$bp_unique_date_visit=$_POST["bp_unique_date_visit"];
        $bp_multiple_date_debut=$_POST["bp_multiple_date_debut"];
        $bp_multiple_date_fin=$_POST["bp_multiple_date_fin"];
        //$bp_multiple_date_visit=$_POST["bp_multiple_date_visit"];

        /*$oBonplan["bonplan_photo1"] = $_POST["photo1Associe");
        $oBonplan["bonplan_photo2"] = $_POST["photo2Associe");
        $oBonplan["bonplan_photo3"] = $_POST["photo3Associe");
        $oBonplan["bonplan_photo4"] = $_POST["photo4Associe");*/

        if ($bonplan_date_debut=="--" || $bonplan_date_debut=="0000-00-00") $bonplan_date_debut = null;
        if ($bonplan_date_fin=="--" || $bonplan_date_fin=="0000-00-00") $bonplan_date_fin = null;

        if ($bp_unique_date_debut=="--" || $bp_unique_date_debut=="0000-00-00") $bp_unique_date_debut = null;
        if ($bp_unique_date_fin=="--" || $bp_unique_date_fin=="0000-00-00") $bp_unique_date_fin = null;
        //if ($bp_unique_date_visit=="--" || $bp_unique_date_visit=="0000-00-00") $bp_unique_date_visit = null;

        if ($bp_multiple_date_debut=="--" || $bp_multiple_date_debut=="0000-00-00") $bp_multiple_date_debut = null;
        if ($bp_multiple_date_fin=="--" || $bp_multiple_date_fin=="0000-00-00") $bp_multiple_date_fin = null;
        //if ($bp_multiple_date_visit=="--" || $bp_multiple_date_visit=="0000-00-00") $bp_multiple_date_visit = null;

        $IdInsertedBonplan = $this->api_model->modifBonplan($oBonplan);

        //$this->load->view('front/vwListeBonPlan', $data) ;
        // $this->load->view('front/vwFicheBonplan', $data) ;
        if ($IdInsertedBonplan){$json['json']="ok";
            echo json_encode($json);}

    }
    public function showsolde(){
        $action = false;
        $nb = null;
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_client=$_POST["id_client"];
        $id_commercants=$_POST["id_commercant"];
        //$id_client=340;
        //$id_commercants=301327;

        $id_commercant = $id_commercants;

        $data ['titre'] = 'Liste de mes clients';
        $data ['print'] = 0;
        $data ['email_clients'] = array ();
        $data ['mail_to_clients'] = '';
        $data ['show_bonplan'] = 1;
        $data ['show_offre'] = 1;
        $criteres = array ();
        if ($action == "mois" && $nb != null) {
            $criteres ['month'] = $nb;
        }

        $mail = "";
        $list_client = $this->assoc_client_commercant_model->get_by_commercant_idsolde( $id_commercant, $criteres,$id_client );
        //var_dump($list_client);die();
        $email_clients = array ();
        if (! empty ( $list_client )) {
            foreach ( $list_client as $cleint ) {
                $mail = "mailto:contact@sortez.org?subject=Message";
                $mail .= "&bcc=$cleint->Email";
                $email_clients [] = $cleint->Email;
            }

            $data ['email_clients'] = $mail;

            $clients = array ();
            foreach ( $list_client as $client ) {
                $fidelity = get_fidelity ( $client->id_commercant );
                $offre = get_offre_active ( $client->id_commercant );
                $fiche = get_fiche_client ( $offre, $client->id_client, $client->id_commercant );
                $user_bonplan = get_user_bonplan ( $client->id_client, $client->id_commercant );

                $client->offre = $offre;
                $client->solde = ! empty ( $fiche ) ? $fiche->solde : 0;

                switch ($offre) {
                    case "capital" :
                        $client->objectif = (is_object ( $fidelity ) && is_object ( $fidelity->capital )) ? $fidelity->capital->montant : 0;
                        break;
                    case "tampon" :
                        $client->objectif = (is_object ( $fidelity ) && is_object ( $fidelity->tampon )) ? $fidelity->tampon->tampon_value : 0;
                        break;
                    case "remise" :
                        $client->objectif = $client->solde;
                        break;
                }

                $client->bonplan = $user_bonplan;
                $clients [] = $client;
                //var_dump($clients);die();
                $json['json']=$client;
                echo json_encode($json);
                // $client->cumul = ($offre == "remise" && is_object($fidelity)) ? $fidelity->remise : 0;
            }
        }
    }
    public function ModifAccount(){
        $rest_json=file_get_contents("php://input");
        $_POSTe=json_decode($rest_json,true);
        //$id_commercant=$_POST["id_commercant"];
        $request=$this->api_model->modifaccount($_POSTe);
        if ($request){
            $json['json']="ok";
            echo json_encode($json);
        }else{
            $json['json']="error";
            echo json_encode($json);
        }
    }
    public function getremisedirect(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $IdCommercant=$_POST["IdCommercant"];
        $request=$this->api_model->get_remisedirect($IdCommercant);
        $object = (object) $request;
        if ($request){
            $json['json']=$object;
            echo json_encode($json);
        }
    }
    public function getcoupdetampon(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $IdCommercant=$_POST["IdCommercant"];
        $request=$this->api_model->getcoupdetampon($IdCommercant);
        $object = (object) $request;
        if ($request){
            $json['json']=$object;
            echo json_encode($json);
        }
    }
    public function getcapital(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $IdCommercant=$_POST["IdCommercant"];
        $request=$this->api_model->getcapital($IdCommercant);
        $object = (object) $request;
        if ($request){
            $json['json']=$object;
            echo json_encode($json);
        }
    }
    public function getbonplan(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $IdCommercant=$_POST["IdCommercant"];
        //$IdCommercant=301327;
        $request=$this->api_model->getbonplan($IdCommercant);
        if ($request){
            $json['json']=$request;
            echo json_encode($json);
        }
    }
    public function get_login_by_id2(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $login=$_POST["login"];
        $request=$this->api_model->get_login_by_id($login);

    }
    public function get_client($login){
        $ordre = null;

        $request=$this->api_model->get_login_by_id2($login);
        $IdCommercant=$request->IdCommercant;
        //var_dump($IdCommercant);
        $criteres ['valide'] = 0;

        //$user_ion_auth_id =$user_ionauth_id;
        $id_commercant = $IdCommercant;
        $criteres ['id_commercant'] = $id_commercant;
        $order_by ['assoc_client_bonplan.date_validation'] = ($ordre == "desc") ? "desc" : "asc";
        $data = $this->api_model->get_list_by_criteria3 ( $criteres, $order_by );
        $clients = array ();
        foreach ($data as $datas){
            $d = $this->api_model->getById ($datas->id);
            $clients[]=$d;
            //var_dump($clients);

        }
        return $clients;

        //var_dump($clients);
    }
    function liste_reservationionic() {
        $ordre = null;
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $login=$_POST["login"];
        $request=$this->api_model->get_login_by_id2($login);
        $IdCommercant=$request->IdCommercant;
        //var_dump($IdCommercant);
        $criteres ['valide'] = 0;

        //$user_ion_auth_id =$user_ionauth_id;
        $id_commercant = $IdCommercant;
        $criteres ['id_commercant'] = $id_commercant;
        $order_by ['assoc_client_bonplan.date_validation'] = ($ordre == "desc") ? "desc" : "asc";
        $data['infoclient']=$this->api_model->get_list_by_criteria2 ($criteres, $order_by);
        $data['infocom']=$this->Mdlcommercant->infoCommercant($id_commercant);
        //$date_visite_client_check = $this->assoc_client_bonplan_model->getById($data->id)->date_visit;
        //$data["client"]=$this->get_client($login);
        //var_dump($data["client"]);die();
       // $object = (object) $data;
        if ($data){
            //$json['json']=$data;
            echo json_encode($data);
        }

    }
    function fidelity_card_operations() {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $IdUser=$_POST["IdUser"];
        $Id=$_POST["id"];
        $bonplanshow = "bonplan";
        $id_client_bonplan=$_POST["id"];
        $id_commercant=$_POST["bonplan_commercant_id"];
        //$data ['pagetitle'] = "Offre de fidélité !";
        // $IdUser = $this->uri->rsegment(3);
        // $bonplanshow = $this->uri->rsegment(4);

        $data ['oUser'] = $oUser = $this->user->GetById ((int)$IdUser);

        $data ['oCard'] = $oCard = $this->mdl_card->getByIdUser ($IdUser);

        $user_ion_auth_id = $this->api_model->get_ion_auth_by_commercant($id_commercant);
        $id_ionauth = $user_ion_auth_id->user_ionauth_id;
        $data ["id_ionauth"] = $id_ionauth;
        $data ["id_commercant"] = $id_commercant;

        $objTampon = $this->mdl_card_tampon->getByIdIonauth ( $id_ionauth );
        $data ["oTampon"] = $objTampon;
        $objCapital = $this->mdl_card_capital->getByIdIonauth ( $id_ionauth );
        $data ["oCapital"] = $objCapital;
        $objRemise = $this->mdl_card_remise->getByIdIonauth ( $id_ionauth );
        $data ["objRemise"] = $objRemise;

        if (isset($id_client_bonplan) && $id_client_bonplan!=0 && $id_client_bonplan!="0") $res_client_bonplan_obj = $this->assoc_client_bonplan_model->getById ( $id_client_bonplan );

        if (isset($res_client_bonplan_obj->id_bonplan) && is_object($res_client_bonplan_obj)) {
            $objBonplan = $this->mdlbonplan->getById($res_client_bonplan_obj->id_bonplan);
            $data ["oBonplan"] = $objBonplan;
        }

        $data ["objReservationNonValideCom"] = $this->assoc_client_bonplan_model->get_where(array('assoc_client_bonplan.id_bonplan'=>$res_client_bonplan_obj->id_bonplan,'assoc_client_bonplan.valide'=>0));

        if (is_object ( $oCard ) && is_object ( $oUser ) && $id_commercant) {
            $oFicheclient = $this->mdl_card_fiche_client_capital->getWhereOne ( " id_card='" . $oCard->id . "' AND id_user='" . $oUser->IdUser . "' AND id_commercant='" . $id_commercant . "' " );

            $data ['oFicheclient'] = $oFicheclient;
            if (isset ( $oFicheclient ) && count ( $oFicheclient ) > 0) {
                $capital_client_value = $oFicheclient->solde_capital;
            } else {
                $capital_client_value = 0;
            }

            $objCommercant_card_tampon = $this->mdl_card_tampon->getByIdCommercant ( $id_commercant );

            //echo $id_commercant;
            //var_dump($objCommercant_card_tampon); die();

            $tampon_commercant_value = (is_object ( $objCommercant_card_tampon )) ? $objCommercant_card_tampon->tampon_value : 0;
            $oFicheclient_tampon = $this->mdl_card_fiche_client_tampon->getWhereOne ( " id_card='" . $oCard->id . "' AND id_user='" . $oUser->IdUser . "' AND id_commercant='" . $id_commercant . "' " );



            if (isset ( $oFicheclient_tampon ) && count ( $oFicheclient_tampon ) > 0) {
                $tampon_client_value = $oFicheclient_tampon->solde_tampon;
                if ($tampon_client_value == $tampon_commercant_value) {
                    $tampon_status = "0";
                }
            } else {
                $tampon_client_value = "0";
            }

            $data ['capital_client_value'] = $capital_client_value;
            $data ['tampon_client_value'] = $tampon_client_value;
            $data ['tampon_commercant_value'] = $tampon_commercant_value;
            $data ['bonplanshow'] = $bonplanshow;
            $data ['visit']=$this->api_model->getById($Id);
            //var_dump($datas);die()
            //var_dump($bonplanshow);die();
            $data ['id_client_bonplan'] = $id_client_bonplan;

            // $criteres['id_commercant'] = $id_commercant;
            // $criteres['id_user'] = $IdUser;

            $criteres ['id'] = $id_client_bonplan;
            $assoc_client_array = $this->assoc_client_bonplan_model->get_list_by_criteria ( $criteres );
            // echo $this->db->last_query();
            // if(!is_array($assoc_client_array)){
            // redirect('auth/login');
            // }
            $data ['assoc_client'] = (! empty ( $assoc_client_array )) ? current ( $assoc_client_array ) : array ();

            $data ['assoc_client_remise'] = $this->mdl_card_fiche_client_remise->getWhereOne(" id_card='" . $oCard->id . "' AND id_user='" . $oUser->IdUser . "' AND id_commercant='" . $id_commercant . "' ");

            $is_mobile = TRUE;
            $data ['titre'] = 'Client: ' . ucfirst ( $oUser->Nom ) . ' ' . ucfirst ( $oUser->Prenom );
            $json['json']=$data;
            echo json_encode($json);
        } else {
            //redirect ( 'auth/login' );
            $data['error_msg'] = "Ce client ne dispose pas encore d'une carte Privilege !";
            $this->load->view ( "mobile2014/error", $data );
        }
    }

    function card_bonplan_validate() {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        if ($_POST["id_card"]) {

            $id_card = $_POST["id_card"];
            $id_bonplan = $_POST["id_bonplan"];
            $id_commercant = $_POST["id_commercant"];
            $id_client_bonplan = $_POST["id_client_bonplan"];

            // $this->assoc_client_bonplan_model->validate_bonplan($id_bonplan,$id_user);

            // confirmation by commercant
            $this->assoc_client_bonplan_model->validate_bonplan_by_id ( $id_client_bonplan );

            //decrementation nb bonplan
            //$this->mdlbonplan->decremente_quantite_bonplan($id_bonplan, $nb_decrement);



            $data ['id_user'] = $id_user = $_POST ['id_user'];


            $objBonplan = $this->mdlbonplan->getById ( $id_bonplan );
            $data ["oBonplan"] = $objBonplan;

            $data ["objReservationNonValideCom"] = $this->assoc_client_bonplan_model->get_where(array('assoc_client_bonplan.id_bonplan'=>$id_bonplan,'assoc_client_bonplan.valide'=>0));

            $data ['user'] = $user = $this->user->GetById ( $id_user );
            $data ['titre'] = "Client: " . ucfirst ( $user->Prenom ) . ' ' . ucfirst ( $user->Nom );


            $assoc_client_commercant = $this->assoc_client_commercant_model->get_by_id ( $id_user, $id_commercant );
            if (! $assoc_client_commercant) {
                $this->assoc_client_commercant_model->insert ( array (
                    'id_client' => $id_user,
                    'id_commercant' => $id_commercant
                ) );
            }

            $commercant = $this->commercant->GetById($id_commercant);
            $data['client'] = ucfirst ( $user->Prenom ) . ' ' . ucfirst ( $user->Nom );
            $data['commercant'] = $commercant->NomSociete;



            //$mail = $this->load->view('mobile2014/email/validation_bonplan',$data,TRUE);
            $mail = $this->load->view('mobile2014/email/validation_bonplan',$data,TRUE);

            /*$this->load->library('email');
            $this->email->from('contact@privicarte.fr', 'PRIVICARTE');
            $this->email->to($user->Email);
            $this->email->subject('[Privicarte] Validation de bon plan');
            $this->email->message($mail);
            $this->email->send();*/


            $zContactEmailCom_ = array();
            $zContactEmailCom_[] = array("Email"=>$user->Email,"Name"=>$user->Nom." ".$user->Prenom);
            @envoi_notification($zContactEmailCom_,'[Privicarte] Validation de bon plan',html_entity_decode(htmlentities($mail)), 'contact@privicarte.fr');


            $json['json']="ok";
            echo json_encode($json);
        }
    }
    function add_card_tampon_mobile() {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_card =$_POST["id_card"];
        if ($_POST["id_bonplan"]!==null){}
        $id_bonplan = $_POST["id_bonplan"];
        $id_commercant =$_POST["id_commercant"];
        $id_user =$_POST["id_user"];
        $new_tampon_value = $_POST["unite_tampon_value"];

        if ($id_card && $id_commercant && $id_user && $new_tampon_value) {

            $assoc_client_commercant = $this->assoc_client_commercant_model->get_by_id ( $id_user, $id_commercant );
            if (! $assoc_client_commercant) {
                $this->assoc_client_commercant_model->insert ( array (
                    'id_client' => $id_user,
                    'id_commercant' => $id_commercant
                ) );
            }

            // get the max tampon value into commercant
            $oFichecommercant = $this->mdl_card_tampon->getByIdCommercant ( $id_commercant );

            // $this->db->trans_start();
            // checking on card_fiche_client_tampon and save in it
            $oFicheclient = $this->mdl_card_fiche_client_tampon->getWhereOne ( " id_card='" . $id_card . "' AND id_commercant='" . $id_commercant . "' AND id_user='" . $id_user . "'" );
            $oCard_fiche_client_tampon ['id_card'] = $id_card;
            $oCard_fiche_client_tampon ['id_commercant'] = $id_commercant;
            $oCard_fiche_client_tampon ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
            $oCard_fiche_client_tampon ['id_user'] = $id_user;

            if (isset ( $oFicheclient ) && count ( $oFicheclient ) > 0) {
                $tampon_client_value = intval ( $oFicheclient->solde_tampon );
                $tampon_client_value = intval ( $tampon_client_value ) + $new_tampon_value;

                if ($tampon_client_value >= $oFichecommercant->tampon_value) {
                    redirect ( '/front/fidelity/actualise_compte/' . $id_user );
                }

                $oCard_fiche_client_tampon ['id'] = $oFicheclient->id;
                $solde_tampon_client = $tampon_client_value;

                $oCard_fiche_client_tampon ['solde_tampon'] = $solde_tampon_client;
                $this->mdl_card_fiche_client_tampon->update ( $oCard_fiche_client_tampon );
                $this->mdl_card_fiche_client_tampon->delete_where ( array (
                    'id !=' => $oCard_fiche_client_tampon ['id'],
                    'id_user' => $oCard_fiche_client_tampon ['id_user'],
                    'id_commercant' => $oCard_fiche_client_tampon ['id_commercant']
                ) );
            } else {
                $solde_tampon_client = $new_tampon_value;
                $oCard_fiche_client_tampon ['solde_tampon'] = $solde_tampon_client;

                $this->mdl_card_fiche_client_tampon->insert ( $oCard_fiche_client_tampon );
            }

            // saving into card_bonplan_used
            $card_bonplan_used ['id_card'] = $id_card;
            $card_bonplan_used ['id_commercant'] = $id_commercant;
            $card_bonplan_used ['id_user'] = $id_user;
            $card_bonplan_used ['id_bonplan'] = $id_bonplan;
            $card_bonplan_used ['scan_status'] = "OK";
            $card_bonplan_used ['tampon_value_added'] = $new_tampon_value;
            $card_bonplan_used ['date_use'] = date ( "Y-m-d h:m:s" );
            $card_bonplan_used ['description'] = "Ajout tampon";
            $card_bonplan_used ['objectif_tampon'] = $oFichecommercant->tampon_value;
            $card_bonplan_used ['cumul_tampon'] = $solde_tampon_client;
            $card_bonplan_used ['datefin_tampon'] = $oFichecommercant->date_fin;
            $this->mdl_card_bonplan_used->insert ( $card_bonplan_used );
            // $this->db->trans_complete();
            $data ['user'] = $user = $this->user->GetById ( $id_user );
            $data ['titre'] = $user->Prenom . ' ' . $user->Nom;
            $data ['solde_tampon_client'] = $solde_tampon_client;
            $data ['objectif_tampon'] = $oFichecommercant->tampon_value;


            $commercant = $this->commercant->GetById($id_commercant);
            $data['client'] = ucfirst ( $user->Prenom ) . ' ' . ucfirst ( $user->Nom );
            $data['commercant'] = $commercant->NomSociete;
            $data['value_added'] = $new_tampon_value;
            $data['solde'] = $solde_tampon_client;
            $data['objectif'] = $oFichecommercant->tampon_value;

            mail_fidelisation($data, $user->Email);


            $json['json']="ok";
            echo json_encode($json);
        } else {
            redirect ( 'front/fidelity/contenupro' );
        }
    }
    function add_card_remise_mobile() {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_card =$_POST["id_card"];
        $id_bonplan = $_POST["id_bonplan"];
        $id_commercant =$_POST["id_commercant"];
        $id_user =$_POST["id_user"];
        $card_remise_value = $_POST["unite_remise_value"];
        //$percent_remise = $_POST["percent_remise"];

        if ($id_card && $id_commercant && $id_user && $card_remise_value) {

            $assoc_client_commercant = $this->assoc_client_commercant_model->get_by_id ( $id_user, $id_commercant );
            if (! $assoc_client_commercant) {
                $this->assoc_client_commercant_model->insert ( array (
                    'id_client' => $id_user,
                    'id_commercant' => $id_commercant
                ) );
            }

            // $this->db->trans_start();
            $oCard_fiche_client_tampon ['id_card'] = $id_card;
            $oCard_fiche_client_tampon ['id_commercant'] = $id_commercant;
            $oCard_fiche_client_tampon ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
            $oCard_fiche_client_tampon ['id_user'] = $id_user;

            // checking on card_fiche_client_remise and save in it
            $oFicheclient = $this->mdl_card_fiche_client_remise->getWhereOne ( " id_card='" . $id_card . "' AND id_commercant='" . $id_commercant . "' AND id_user='" . $id_user . "'" );
            $oCard_fiche_client_remise ['id_card'] = $id_card;
            $oCard_fiche_client_remise ['id_commercant'] = $id_commercant;
            $oCard_fiche_client_remise ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
            $oCard_fiche_client_remise ['id_user'] = $id_user;

            if (isset ( $oFicheclient ) && count ( $oFicheclient ) > 0) {
                $remise_client_value = intval ( $oFicheclient->solde_remise );

                // get all card_remise_value but not percent value

                //$card_remise_value = (intval ( $card_remise_value ) * ($percent_remise / 100));

                $remise_client_value = intval ( $remise_client_value ) + intval ( $card_remise_value );
                $oCard_fiche_client_remise ['id'] = $oFicheclient->id;
                $oCard_fiche_client_remise ['solde_remise'] = $remise_client_value;
                $solde_remise_client = $remise_client_value;

                $this->mdl_card_fiche_client_remise->update ( $oCard_fiche_client_remise );
                $this->mdl_card_fiche_client_remise->delete_where ( array (
                    'id !=' => $oCard_fiche_client_remise ['id'],
                    'id_user' => $oCard_fiche_client_remise ['id_user'],
                    'id_commercant' => $oCard_fiche_client_remise ['id_commercant']
                ) );
            } else {
                $oCard_fiche_client_remise ['solde_remise'] = $card_remise_value;
                $solde_remise_client = $card_remise_value;

                $this->mdl_card_fiche_client_remise->insert ( $oCard_fiche_client_remise );
            }

            // get the max remise value into commercant
            $oFichecommercant = $this->mdl_card_remise->getByIdCommercant ( $id_commercant );

            // saving into card_bonplan_used
            $card_bonplan_used ['id_card'] = $id_card;
            $card_bonplan_used ['id_commercant'] = $id_commercant;
            $card_bonplan_used ['id_user'] = $id_user;
            $card_bonplan_used ['id_bonplan'] = $id_bonplan;
            $card_bonplan_used ['scan_status'] = "OK";
            $card_bonplan_used ['remise_value_added'] = $card_remise_value;
            $card_bonplan_used ['date_use'] = date ( "Y-m-d h:m:s" );
            $card_bonplan_used ['description'] = "Ajout remise";
            $card_bonplan_used ['cumul_remise'] = $solde_remise_client;
            $card_bonplan_used ['datefin_remise'] = $oFichecommercant->date_fin;

            $this->mdl_card_bonplan_used->insert ( $card_bonplan_used );
            // $this->db->trans_complete();
            $data ['user'] = $user = $this->user->GetById ( $id_user );
            $data ['titre'] = $user->Prenom . ' ' . $user->Nom;
            $data ['solde_remise_client'] = $solde_remise_client;
            $data ['objectif_remise'] = $oFichecommercant->montant;
            $criteres_assoc ['valide'] = 1;
            $criteres_assoc ['id_commercant'] = $id_commercant;
            $criteres_assoc ['id_user'] = $id_user;
            $assoc_client_bonplan_array = $this->assoc_client_bonplan_model->get_list_by_criteria ( $criteres_assoc );


            $commercant = $this->commercant->GetById($id_commercant);
            $data['client'] = ucfirst ( $user->Prenom ) . ' ' . ucfirst ( $user->Nom );
            $data['commercant'] = $commercant->NomSociete;
            $data['value_added'] = $card_remise_value;
            $data['solde'] = $solde_remise_client;
            //$data['objectif'] = $oFichecommercant->montant;

            mail_fidelisation($data, $user->Email);

            // $data['assoc_client_bon_plan'] = (!empty($assoc_client_bonplan_array)) ? current($assoc_client_bonplan_array) : null;
            $data ['assoc_client_bon_plan'] = get_fiche_client ( "remise", $id_user, $id_commercant );
            $json['json']="ok";
            echo json_encode($json);
        } else {
            echo "ko";
        }
    }
    function add_card_capital_mobile() {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_card =$_POST["id_card"];
        $id_bonplan = $_POST["id_bonplan"];
        $id_commercant =$_POST["id_commercant"];
        $id_user =$_POST["id_user"];
        $card_capital_value = $_POST["unite_capital_value"];

        if ($id_card && $id_commercant && $id_user && $card_capital_value) {

            $assoc_client_commercant = $this->assoc_client_commercant_model->get_by_id ( $id_user, $id_commercant );
            if (! $assoc_client_commercant) {
                $this->assoc_client_commercant_model->insert ( array (
                    'id_client' => $id_user,
                    'id_commercant' => $id_commercant
                ) );
            }

            // $this->db->trans_start();
            $oCard_fiche_client_tampon ['id_card'] = $id_card;
            $oCard_fiche_client_tampon ['id_commercant'] = $id_commercant;
            $oCard_fiche_client_tampon ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
            $oCard_fiche_client_tampon ['id_user'] = $id_user;

            // checking on card_fiche_client_capital and save in it
            $oFicheclient = $this->mdl_card_fiche_client_capital->getWhereOne ( " id_card='" . $id_card . "' AND id_commercant='" . $id_commercant . "' AND id_user='" . $id_user . "'" );
            $oCard_fiche_client_capital ['id_card'] = $id_card;
            $oCard_fiche_client_capital ['id_commercant'] = $id_commercant;
            $oCard_fiche_client_capital ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
            $oCard_fiche_client_capital ['id_user'] = $id_user;

            // get the max capital value into commercant
            $oFichecommercant = $this->mdl_card_capital->getByIdCommercant ( $id_commercant );

            if (isset ( $oFicheclient ) && count ( $oFicheclient ) > 0) {
                $capital_client_value = intval ( $oFicheclient->solde_capital );
                $capital_client_value = intval ( $capital_client_value ) + intval ( $card_capital_value );

                if (intval ( $capital_client_value ) >= intval ( $oFichecommercant->montant )) {
                    redirect ( '/front/fidelity/actualise_compte/' . $id_user );
                }
                $oCard_fiche_client_capital ['id'] = $oFicheclient->id;
                $oCard_fiche_client_capital ['solde_capital'] = $capital_client_value;
                $solde_capital_client = $capital_client_value;

                $this->mdl_card_fiche_client_capital->update ( $oCard_fiche_client_capital );
                $this->mdl_card_fiche_client_capital->delete_where ( array (
                    'id !=' => $oCard_fiche_client_capital ['id'],
                    'id_user' => $oCard_fiche_client_capital ['id_user'],
                    'id_commercant' => $oCard_fiche_client_capital ['id_commercant']
                ) );
            } else {
                $oCard_fiche_client_capital ['solde_capital'] = $card_capital_value;
                $solde_capital_client = $card_capital_value;

                $this->mdl_card_fiche_client_capital->insert ( $oCard_fiche_client_capital );
            }

            // saving into card_bonplan_used
            $card_bonplan_used ['id_card'] = $id_card;
            $card_bonplan_used ['id_commercant'] = $id_commercant;
            $card_bonplan_used ['id_user'] = $id_user;
            $card_bonplan_used ['id_bonplan'] = $id_bonplan;
            $card_bonplan_used ['scan_status'] = "OK";
            $card_bonplan_used ['capital_value_added'] = $card_capital_value;
            $card_bonplan_used ['date_use'] = date ( "Y-m-d h:m:s" );
            $card_bonplan_used ['description'] = "Ajout capital";
            $card_bonplan_used ['objectif_capital'] = $oFichecommercant->montant;
            $card_bonplan_used ['cumul_capital'] = $solde_capital_client;
            $card_bonplan_used ['datefin_capital'] = $oFichecommercant->date_fin;

            $this->mdl_card_bonplan_used->insert ( $card_bonplan_used );
            // $this->db->trans_complete();
            $data ['user'] = $user = $this->user->GetById ( $id_user );
            $data ['titre'] = "Client: " . ucfirst ( $user->Prenom ) . ' ' . ucfirst ( $user->Nom );
            $data ['solde_capital_client'] = $solde_capital_client;
            $data ['objectif_capital'] = $oFichecommercant->montant;

            $commercant = $this->commercant->GetById($id_commercant);
            $data['client'] = ucfirst ( $user->Prenom ) . ' ' . ucfirst ( $user->Nom );
            $data['commercant'] = $commercant->NomSociete;
            $data['value_added'] = $card_capital_value;
            $data['solde'] = $solde_capital_client;
            $data['objectif'] = $oFichecommercant->montant;

            mail_fidelisation($data, $user->Email);

            $json['json']="ok";
            echo json_encode($json);
        } else {
            redirect ( 'auth/login' );//aut/login
        }
    }
    public function getbonplannbre(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $IdCommercant=$_POST["IdCommercant"];
        $request=$this->api_model->getbonplannbre($IdCommercant);
        $result=$request->limit_bonplan;
        $nbre=$this->api_model->getnbrebonplan($IdCommercant);
        if ($result){
            $data=array("nbre"=>$nbre,"limit"=>$result);
            $json['json']=[$data];
            echo json_encode($json);
        }
    }
    public function deletebonplan(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $IdCommercant=$_POST["IdCommercant"];
        $request=$this->api_model->deletebonplan($IdCommercant);
        if ($request){
            $json['json']="ok";
            echo json_encode($json);
        }
    }
    function detail_client() {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idUser=$_POST["idUser"];


        $user_ion_auth_id = $this->ion_auth->user ()->row ();
        $id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
        $data ['titre'] = "Historique consommation";
        $critere = array (
            'card_bonplan_used.id_commercant' => $id_commercant,
            'card_bonplan_used.id_user' => $idUser
        );
        // $critere['card_bonplan_used.capital_value_added !='] ='';
        $order_by = array (
            'champ' => 'card_bonplan_used.id',
            'ordre' => 'ASC'
        );
        $data ['list_activity'] = $this->mdl_card_bonplan_used->get_by_critere ( $critere );
        $criteres_assoc ['valide'] = 1;
        $criteres_assoc ['id_commercant'] = $id_commercant;
        $criteres_assoc ['id_user'] = $idUser;

        if ($assoc_client_bon_plan = $this->assoc_client_bonplan_model->get_list_by_criteria ( $criteres_assoc )) {
            $data ['assoc_client_bonplan'] = current ( $assoc_client_bon_plan );
            $json['json']=[$data];
            echo json_encode($json);
        } else {
            $json['json']=[$data];
            echo json_encode($json);
        }

    }
    function check_scan_card() {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);

        $login=$_POST["login"];
        $num_card_scan=$_POST["num_card_scan"];
        //var_dump($num_card_scan);
        $this->load->model ( 'assoc_client_commercant_model' );
        $ocheck_num_card = $this->mdl_card->check_scan_card ( $num_card_scan );
        //var_dump($ocheck_num_card);die();
        $reponse  = "Cette carte est invalide !";
        if (count ( $ocheck_num_card ) > 0) {
            //var_dump($ocheck_num_card->is_activ);die();
            if (isset ( $ocheck_num_card ) && $ocheck_num_card->is_activ == "1") {
                $oCard = $this->mdl_card->getByCard ( $num_card_scan );
               // var_dump($oCard);die();
                $ocheck_card = $this->mdl_card_user_link->check_card ( $oCard->id );
                //var_dump($ocheck_card);die();
                if (count ( $ocheck_card ) > 0) {
                    $request=$this->api_model->get_login_by_id2($login);
                    //var_dump($request);die();
                    $id_ionauth=$request->user_ionauth_id;
                    $id_commercant=$request->IdCommercant;
                    $id_user = $ocheck_card->id_user;

                    $assoc_client_commercant = $this->assoc_client_commercant_model->get_by_id ( $id_user, $id_commercant );
                    //var_dump($assoc_client_commercant);die();
                    if (! $assoc_client_commercant) {
                        $this->assoc_client_commercant_model->insert ( array (
                            'id_client' => $id_user,
                            'id_commercant' => $id_commercant
                        ) );
                    }
                    $reponse  =$this->getuser_card_scan($num_card_scan);
                    //var_dump($reponse);die();
                } else {
                    $reponse  ="Votre carte n'est pas encore liée à un compte Client !";
                }
            } else {
                $reponse  = "Votre carte est répertorié, mais n'est pas activé !";

            }
        }
        //var_dump($reponse);die();
        echo json_encode ( $reponse );
    }
    function check_scan_card2($login,$num_card_scan) {
        //var_dump($request);die();
        //$num_card_scan=$_POST["num_card_scan"];
        $this->load->model ( 'assoc_client_commercant_model' );
        //$num_card_scan = $_POST ( 'num_card_scan' );
        $ocheck_num_card = $this->mdl_card->check_scan_card ( $num_card_scan );
        //var_dump($ocheck_num_card);die();
        $reponse  = "Cette carte est invalide !";
        if (count ( $ocheck_num_card ) > 0) {
            // echo "error";
            if (isset ( $ocheck_num_card ) && $ocheck_num_card->is_activ == "1") {
                $oCard = $this->mdl_card->getByCard ( $num_card_scan );
                $ocheck_card = $this->mdl_card_user_link->check_card ( $oCard->id );
                //var_dump($ocheck_card);die();
                if (count ( $ocheck_card ) > 0) {
                    //$user_ion_auth_id = $this->ion_auth->user ()->row ();
                    $request=$this->api_model->get_login_by_id2($login);
                    $id_ionauth=$request->user_ionauth_id;
                    $id_commercant=$request->IdCommercant;
                    //$id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
                    $id_user = $ocheck_card->id_user;

                    $assoc_client_commercant = $this->assoc_client_commercant_model->get_by_id ( $id_user, $id_commercant );
                    //var_dump($assoc_client_commercant);die();
                    if (! $assoc_client_commercant) {
                        $this->assoc_client_commercant_model->insert ( array (
                            'id_client' => $id_user,
                            'id_commercant' => $id_commercant
                        ) );
                    }
                    $reponse  =$this->getuser_card_scan2($num_card_scan);
//var_dump($reponse);die();
                } else {
                    $reponse  ="Votre carte n'est pas encore liée à un compte Client !";
                }
            } else {
                $reponse  = "Votre carte est répertorié, mais n'est pas activé !";

            }
        }
        return $reponse;
    }
    function getuser_card_scan($num_card_scan) {
       // $num_card_scan = $_POST ( 'num_card_scan' );
        $objnum_card_scan = $this->mdl_card->getByCard ( $num_card_scan );
        //var_dump($objnum_card_scan);
        if (count ( $objnum_card_scan ) > 0 && is_object ( $objnum_card_scan ))
           // echo $objnum_card_scan->id_user;
        $data="Client n°: $objnum_card_scan->id_user";
        else
            $data= "0";
        return $data;
    }
    function getuser_card_scan2($num_card_scan) {
        // $num_card_scan = $_POST ( 'num_card_scan' );
        $objnum_card_scan = $this->mdl_card->getByCard ( $num_card_scan );
        //var_dump($objnum_card_scan);
        if (count ( $objnum_card_scan ) > 0 && is_object ( $objnum_card_scan ))
            // echo $objnum_card_scan->id_user;
            $data=$objnum_card_scan->id_user;
        else
            $data= "0";
        return $data;
    }

    public function appremise(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $login=$_POST["login"];
        $num_card_scan=$_POST["num_card_scan"];
        $request=$this->api_model->get_login_by_id2($login);
        $id_ionauth=$request->user_ionauth_id;
        $id_commercant=$request->IdCommercant;
        $data['villenom']=$request->ville_nom;
        $data['telcom']=$request->TelMobile;
        $data['nomcom']=$request->NomSociete;

        $IdUser= $this->check_scan_card2($login,$num_card_scan);
        //var_dump($IdUser);
        $bonplanshow = "bonplan";
        $data ['oUser']=$oUser  = $this->user->GetById ( $IdUser );
        //var_dump($data ['oUser']);die();
        //var_dump($oUser);die();
        $data ['oCard'] = $oCard = $this->mdl_card->getByIdUser ( $IdUser );
        $user_ion_auth_id = $this->api_model->get_ion_auth_by_commercant($id_commercant);
        $id_ionauth = $user_ion_auth_id->user_ionauth_id;
        $data ["id_ionauth"] = $id_ionauth;
        $data ["id_commercant"] = $id_commercant;
        //var_dump($id_ionauth);die();
        $objTampon = $this->mdl_card_tampon->getByIdIonauth ( $id_ionauth );
        $data ["oTampon"] = $objTampon;
        $objCapital = $this->mdl_card_capital->getByIdIonauth ( $id_ionauth );
        $data ["oCapital"] = $objCapital;
        $objRemise = $this->mdl_card_remise->getByIdIonauth ( $id_ionauth );
        $data ["objRemise"] = $objRemise;

        $command = $this->api_model->get_command_data($IdUser,$id_commercant);
        $data ["command"] = $command;
        $command_detail = array();
        if(!empty($command)){
            foreach($command as $com){
                $detail_command_to_push = $this->api_model->get_command_details_data($com->id,$IdUser);
                array_push($command_detail,$detail_command_to_push);
            }
        }
        $data ["detail_command"] = $command_detail;
        if (is_object ( $oCard ) && is_object ( $oUser ) && $id_commercant) {
            $oFicheclient = $this->mdl_card_fiche_client_capital->getWhereOne ( " id_card='" . $oCard->id . "' AND id_user='" . $oUser->IdUser . "' AND id_commercant='" . $id_commercant . "' " );

            $data ['oFicheclient'] = $oFicheclient;
            if (isset ( $oFicheclient ) && count ( $oFicheclient ) > 0) {
                $capital_client_value = $oFicheclient->solde_capital;
            } else {
                $capital_client_value = 0;
            }
            $objCommercant_card_tampon = $this->mdl_card_tampon->getByIdCommercant ( $id_commercant );
            $tampon_commercant_value = (is_object ( $objCommercant_card_tampon )) ? $objCommercant_card_tampon->tampon_value : 0;
            $oFicheclient_tampon = $this->mdl_card_fiche_client_tampon->getWhereOne ( " id_card='" . $oCard->id . "' AND id_user='" . $oUser->IdUser . "' AND id_commercant='" . $id_commercant . "' " );
            if (isset ( $oFicheclient_tampon ) && count ( $oFicheclient_tampon ) > 0) {
                $tampon_client_value = $oFicheclient_tampon->solde_tampon;
                if ($tampon_client_value == $tampon_commercant_value) {
                    $tampon_status = "0";
                }
            } else {
                $tampon_client_value = "0";
            }

            $data ['capital_client_value'] = $capital_client_value;
            $data ['tampon_client_value'] = $tampon_client_value;
            $data ['tampon_commercant_value'] = $tampon_commercant_value;
            $data ['bonplanshow'] = $bonplanshow;
             $criteres['id_commercant'] = $id_commercant;
             $criteres['id_user'] = $IdUser;
            $criteres ['valide'] = 0;
            $assoc_client_array = $this->api_model->get_list_by_criteria2( $criteres,$order_by=array() );
            $data ['assoc_client'] =$assoc_client_array ;
            $data ['assoc_client_remise'] = $this->mdl_card_fiche_client_remise->getWhereOne(" id_card='" . $oCard->id . "' AND id_user='" . $oUser->IdUser . "' AND id_commercant='" . $id_commercant . "' ");
            $is_mobile = TRUE;
            $data ['titre'] = 'Client: ' . ucfirst ( $oUser->Nom ) . ' ' . ucfirst ( $oUser->Prenom );
            //$json['json']=[$data];
            $object = (object) $data;
            if ($request){
                $json['json']=$object;
                echo json_encode($json);
            }
        }else{echo "ko";}
    }
    
    function add_card_remise_mobile2() {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_card =$_POST["id_card"];
        $id_bonplan = 0;
        $id_commercant =$_POST["id_commercant"];
        $id_user =$_POST["id_user"];
        $card_remise_value = $_POST["unite_remise_value"];
        //$percent_remise = $_POST["percent_remise"];

        if ($id_card && $id_commercant && $id_user && $card_remise_value) {

            $assoc_client_commercant = $this->assoc_client_commercant_model->get_by_id ( $id_user, $id_commercant );
            if (! $assoc_client_commercant) {
                $this->assoc_client_commercant_model->insert ( array (
                    'id_client' => $id_user,
                    'id_commercant' => $id_commercant
                ) );
            }

            // $this->db->trans_start();
            $oCard_fiche_client_tampon ['id_card'] = $id_card;
            $oCard_fiche_client_tampon ['id_commercant'] = $id_commercant;
            $oCard_fiche_client_tampon ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
            $oCard_fiche_client_tampon ['id_user'] = $id_user;

            // checking on card_fiche_client_remise and save in it
            $oFicheclient = $this->mdl_card_fiche_client_remise->getWhereOne ( " id_card='" . $id_card . "' AND id_commercant='" . $id_commercant . "' AND id_user='" . $id_user . "'" );
            $oCard_fiche_client_remise ['id_card'] = $id_card;
            $oCard_fiche_client_remise ['id_commercant'] = $id_commercant;
            $oCard_fiche_client_remise ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
            $oCard_fiche_client_remise ['id_user'] = $id_user;

            if (isset ( $oFicheclient ) && count ( $oFicheclient ) > 0) {
                $remise_client_value = intval ( $oFicheclient->solde_remise );

                // get all card_remise_value but not percent value

                //$card_remise_value = (intval ( $card_remise_value ) * ($percent_remise / 100));

                $remise_client_value = intval ( $remise_client_value ) + intval ( $card_remise_value );
                $oCard_fiche_client_remise ['id'] = $oFicheclient->id;
                $oCard_fiche_client_remise ['solde_remise'] = $remise_client_value;
                $oCard_fiche_client_remise['date_dernier_validation']=date('Y-m-d');
                $oCard_fiche_client_remise['dernier_value_added']=$card_remise_value;
                $solde_remise_client = $remise_client_value;

                $this->mdl_card_fiche_client_remise->update ( $oCard_fiche_client_remise );
                $this->mdl_card_fiche_client_remise->delete_where ( array (
                    'id !=' => $oCard_fiche_client_remise ['id'],
                    'id_user' => $oCard_fiche_client_remise ['id_user'],
                    'id_commercant' => $oCard_fiche_client_remise ['id_commercant']
                ) );
            } else {
                $oCard_fiche_client_remise ['solde_remise'] = $card_remise_value;
                $solde_remise_client = $card_remise_value;

                $this->mdl_card_fiche_client_remise->insert ( $oCard_fiche_client_remise );
            }

            // get the max remise value into commercant
            $oFichecommercant = $this->mdl_card_remise->getByIdCommercant ( $id_commercant );

            // saving into card_bonplan_used
            $card_bonplan_used ['id_card'] = $id_card;
            $card_bonplan_used ['id_commercant'] = $id_commercant;
            $card_bonplan_used ['id_user'] = $id_user;
            $card_bonplan_used ['id_bonplan'] = $id_bonplan;
            $card_bonplan_used ['scan_status'] = "OK";
            $card_bonplan_used ['remise_value_added'] = $card_remise_value;
            $card_bonplan_used ['date_use'] = date ( "Y-m-d h:m:s" );
            $card_bonplan_used ['description'] = "Ajout remise";
            $card_bonplan_used ['cumul_remise'] = $solde_remise_client;
            $card_bonplan_used ['datefin_remise'] = $oFichecommercant->date_fin;

            $this->mdl_card_bonplan_used->insert ( $card_bonplan_used );
            // $this->db->trans_complete();
            $data ['user'] = $user = $this->user->GetById ( $id_user );
            $data ['titre'] = $user->Prenom . ' ' . $user->Nom;
            $data ['solde_remise_client'] = $solde_remise_client;
            $data ['objectif_remise'] = $oFichecommercant->montant;
            $criteres_assoc ['valide'] = 1;
            $criteres_assoc ['id_commercant'] = $id_commercant;
            $criteres_assoc ['id_user'] = $id_user;
            $assoc_client_bonplan_array = $this->assoc_client_bonplan_model->get_list_by_criteria ( $criteres_assoc );


            $commercant = $this->commercant->GetById($id_commercant);
            $data['client'] = ucfirst ( $user->Prenom ) . ' ' . ucfirst ( $user->Nom );
            $data['commercant'] = $commercant->NomSociete;
            $data['value_added'] = $card_remise_value;
            $data['solde'] = $solde_remise_client;
            //$data['objectif'] = $oFichecommercant->montant;

            mail_fidelisation($data, $user->Email);

            // $data['assoc_client_bon_plan'] = (!empty($assoc_client_bonplan_array)) ? current($assoc_client_bonplan_array) : null;
            $data ['assoc_client_bon_plan'] = get_fiche_client ( "remise", $id_user, $id_commercant );
            $json['json']="ok";
            echo json_encode($json);
        } else {
            echo "ko";
        }
    }
    function add_card_tampon_mobile2() {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_card =$_POST["id_card"];
        $id_bonplan =0;
        $id_commercant =$_POST["id_commercant"];
        $id_user =$_POST["id_user"];
        $new_tampon_value = $_POST["unite_tampon_value"];

        if ($id_card && $id_commercant && $id_user && $new_tampon_value) {

            $assoc_client_commercant = $this->assoc_client_commercant_model->get_by_id ( $id_user, $id_commercant );
            if (! $assoc_client_commercant) {
                $this->assoc_client_commercant_model->insert ( array (
                    'id_client' => $id_user,
                    'id_commercant' => $id_commercant
                ) );
            }

            // get the max tampon value into commercant
            $oFichecommercant = $this->mdl_card_tampon->getByIdCommercant ( $id_commercant );

            // $this->db->trans_start();
            // checking on card_fiche_client_tampon and save in it
            $oFicheclient = $this->mdl_card_fiche_client_tampon->getWhereOne ( " id_card='" . $id_card . "' AND id_commercant='" . $id_commercant . "' AND id_user='" . $id_user . "'" );
            $oCard_fiche_client_tampon ['id_card'] = $id_card;
            $oCard_fiche_client_tampon ['id_commercant'] = $id_commercant;
            $oCard_fiche_client_tampon ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
            $oCard_fiche_client_tampon ['id_user'] = $id_user;

            if (isset ( $oFicheclient ) && count ( $oFicheclient ) > 0) {
                $tampon_client_value = intval ( $oFicheclient->solde_tampon );
                $tampon_client_value = intval ( $tampon_client_value ) + $new_tampon_value;

                if ($tampon_client_value >= $oFichecommercant->tampon_value) {
                    redirect ( '/front/fidelity/actualise_compte/' . $id_user );
                }

                $oCard_fiche_client_tampon ['id'] = $oFicheclient->id;
                $oCard_fiche_client_tampon['date_dernier_validation']=date('Y-m-d');
                $oCard_fiche_client_tampon['dernier_value_added']=$new_tampon_value;
                $solde_tampon_client = $tampon_client_value;

                $oCard_fiche_client_tampon ['solde_tampon'] = $solde_tampon_client;
                $this->mdl_card_fiche_client_tampon->update ( $oCard_fiche_client_tampon );
                $this->mdl_card_fiche_client_tampon->delete_where ( array (
                    'id !=' => $oCard_fiche_client_tampon ['id'],
                    'id_user' => $oCard_fiche_client_tampon ['id_user'],
                    'id_commercant' => $oCard_fiche_client_tampon ['id_commercant']
                ) );
            } else {
                $solde_tampon_client = $new_tampon_value;
                $oCard_fiche_client_tampon ['solde_tampon'] = $solde_tampon_client;

                $this->mdl_card_fiche_client_tampon->insert ( $oCard_fiche_client_tampon );
            }

            // saving into card_bonplan_used
            $card_bonplan_used ['id_card'] = $id_card;
            $card_bonplan_used ['id_commercant'] = $id_commercant;
            $card_bonplan_used ['id_user'] = $id_user;
            $card_bonplan_used ['id_bonplan'] = $id_bonplan;
            $card_bonplan_used ['scan_status'] = "OK";
            $card_bonplan_used ['tampon_value_added'] = $new_tampon_value;
            $card_bonplan_used ['date_use'] = date ( "Y-m-d h:m:s" );
            $card_bonplan_used ['description'] = "Ajout tampon";
            $card_bonplan_used ['objectif_tampon'] = $oFichecommercant->tampon_value;
            $card_bonplan_used ['cumul_tampon'] = $solde_tampon_client;
            $card_bonplan_used ['datefin_tampon'] = $oFichecommercant->date_fin;
            $this->mdl_card_bonplan_used->insert ( $card_bonplan_used );
            // $this->db->trans_complete();
            $data ['user'] = $user = $this->user->GetById ( $id_user );
            $data ['titre'] = $user->Prenom . ' ' . $user->Nom;
            $data ['solde_tampon_client'] = $solde_tampon_client;
            $data ['objectif_tampon'] = $oFichecommercant->tampon_value;


            $commercant = $this->commercant->GetById($id_commercant);
            $data['client'] = ucfirst ( $user->Prenom ) . ' ' . ucfirst ( $user->Nom );
            $data['commercant'] = $commercant->NomSociete;
            $data['value_added'] = $new_tampon_value;
            $data['solde'] = $solde_tampon_client;
            $data['objectif'] = $oFichecommercant->tampon_value;

            mail_fidelisation($data, $user->Email);


            $json['json']="ok";
            echo json_encode($json);
        } else {
            redirect ( 'front/fidelity/contenupro' );
        }
    }
    function add_card_capital_mobile2() {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_card =$_POST["id_card"];
        $id_bonplan = 0;
        $id_commercant =$_POST["id_commercant"];
        $id_user =$_POST["id_user"];
        $card_capital_value = $_POST["unite_capital_value"];

        if ($id_card && $id_commercant && $id_user && $card_capital_value) {

            $assoc_client_commercant = $this->assoc_client_commercant_model->get_by_id ( $id_user, $id_commercant );
            if (! $assoc_client_commercant) {
                $this->assoc_client_commercant_model->insert ( array (
                    'id_client' => $id_user,
                    'id_commercant' => $id_commercant
                ) );
            }

            // $this->db->trans_start();
            $oCard_fiche_client_tampon ['id_card'] = $id_card;
            $oCard_fiche_client_tampon ['id_commercant'] = $id_commercant;
            $oCard_fiche_client_tampon ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
            $oCard_fiche_client_tampon ['id_user'] = $id_user;

            // checking on card_fiche_client_capital and save in it
            $oFicheclient = $this->mdl_card_fiche_client_capital->getWhereOne ( " id_card='" . $id_card . "' AND id_commercant='" . $id_commercant . "' AND id_user='" . $id_user . "'" );
            $oCard_fiche_client_capital ['id_card'] = $id_card;
            $oCard_fiche_client_capital ['id_commercant'] = $id_commercant;
            $oCard_fiche_client_capital ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
            $oCard_fiche_client_capital ['id_user'] = $id_user;

            // get the max capital value into commercant
            $oFichecommercant = $this->mdl_card_capital->getByIdCommercant ( $id_commercant );

            if (isset ( $oFicheclient ) && count ( $oFicheclient ) > 0) {
                $capital_client_value = intval ( $oFicheclient->solde_capital );
                $capital_client_value = intval ( $capital_client_value ) + intval ( $card_capital_value );

                if (intval ( $capital_client_value ) >= intval ( $oFichecommercant->montant )) {
                    redirect ( '/front/fidelity/actualise_compte/' . $id_user );
                }
                $oCard_fiche_client_capital ['id'] = $oFicheclient->id;
                $oCard_fiche_client_capital ['solde_capital'] = $capital_client_value;
                $oCard_fiche_client_capital['date_dernier_validation']=date('Y-m-d');
                $oCard_fiche_client_capital['dernier_value_added']=$card_capital_value;
                $solde_capital_client = $capital_client_value;

                $this->mdl_card_fiche_client_capital->update ( $oCard_fiche_client_capital );
                $this->mdl_card_fiche_client_capital->delete_where ( array (
                    'id !=' => $oCard_fiche_client_capital ['id'],
                    'id_user' => $oCard_fiche_client_capital ['id_user'],
                    'id_commercant' => $oCard_fiche_client_capital ['id_commercant']
                ) );
            } else {
                $oCard_fiche_client_capital ['solde_capital'] = $card_capital_value;
                $solde_capital_client = $card_capital_value;

                $this->mdl_card_fiche_client_capital->insert ( $oCard_fiche_client_capital );
            }

            // saving into card_bonplan_used
            $card_bonplan_used ['id_card'] = $id_card;
            $card_bonplan_used ['id_commercant'] = $id_commercant;
            $card_bonplan_used ['id_user'] = $id_user;
            $card_bonplan_used ['id_bonplan'] = $id_bonplan;
            $card_bonplan_used ['scan_status'] = "OK";
            $card_bonplan_used ['capital_value_added'] = $card_capital_value;
            $card_bonplan_used ['date_use'] = date ( "Y-m-d h:m:s" );
            $card_bonplan_used ['description'] = "Ajout capital";
            $card_bonplan_used ['objectif_capital'] = $oFichecommercant->montant;
            $card_bonplan_used ['cumul_capital'] = $solde_capital_client;
            $card_bonplan_used ['datefin_capital'] = $oFichecommercant->date_fin;

            $this->mdl_card_bonplan_used->insert ( $card_bonplan_used );
            // $this->db->trans_complete();
            $data ['user'] = $user = $this->user->GetById ( $id_user );
            $data ['titre'] = "Client: " . ucfirst ( $user->Prenom ) . ' ' . ucfirst ( $user->Nom );
            $data ['solde_capital_client'] = $solde_capital_client;
            $data ['objectif_capital'] = $oFichecommercant->montant;

            $commercant = $this->commercant->GetById($id_commercant);
            $data['client'] = ucfirst ( $user->Prenom ) . ' ' . ucfirst ( $user->Nom );
            $data['commercant'] = $commercant->NomSociete;
            $data['value_added'] = $card_capital_value;
            $data['solde'] = $solde_capital_client;
            $data['objectif'] = $oFichecommercant->montant;

            mail_fidelisation($data, $user->Email);

            $json['json']="ok";
            echo json_encode($json);
        } else {
            redirect ( 'auth/login' );//aut/login
        }
    }
    public function liste_clients2($login,$action = false, $nb = null) {
        //$login=$_POST["login"];
        //$login="randawilly8@gmail.com";
        $datas=$this->api_model->get_login_by_id2($login);
        $id_commercant =$datas->IdCommercant;
        // $data ['titre'] = 'Liste de mes clients';
        //$data ['print'] = 0;
        // $data ['email_clients'] = array ();
        // $data ['mail_to_clients'] = '';
        //$data ['show_bonplan'] = 1;
        // $data ['show_offre'] = 1;
        $criteres = array ();
        if ($action == "mois" && $nb != null) {
            $criteres ['month'] = $nb;
        }

        $mail = "";
        $list_client = $this->assoc_client_commercant_model->get_by_commercant_id ( $id_commercant, $criteres );
        $email_clients = array ();
        if ( count( $list_client ) !=0) {
            foreach ( $list_client as $cleint ) {
                $mail = "mailto:contact@sortez.org?subject=Message";
                $mail .= "&bcc=$cleint->Email";
                $email_clients [] = $cleint->Email;
            }

            //$data ['email_clients'] = $mail;

            $clients = array ();
            foreach ( $list_client as $client ) {
                $fidelity = get_fidelity ( $client->id_commercant );
                $offre = get_offre_active ( $client->id_commercant );
                $fiche = get_fiche_client ( $offre, $client->id_client, $client->id_commercant );
                $user_bonplan = get_user_bonplan ( $client->id_client, $client->id_commercant );

                $client->offre = $offre;
                $client->solde = ! empty ( $fiche ) ? $fiche->solde : 0;

                switch ($offre) {
                    case "capital" :
                        $client->objectif = (is_object ( $fidelity ) && is_object ( $fidelity->capital )) ? $fidelity->capital->montant : 0;
                        break;
                    case "tampon" :
                        $client->objectif = (is_object ( $fidelity ) && is_object ( $fidelity->tampon )) ? $fidelity->tampon->tampon_value : 0;
                        break;
                    case "remise" :
                        $client->objectif = $client->solde;
                        break;
                }

                $client->bonplan = $user_bonplan;
                $clients [] = $client;
                // $client->cumul = ($offre == "remise" && is_object($fidelity)) ? $fidelity->remise : 0;
            }

            if ($action == "solde_desc")
                usort ( $clients, "solde_desc" );
            if ($action == "solde_asc")
                usort ( $clients, "solde_asc" );

            return $clients;die();
        }

        if ($action == 'print') {
            //$data ['print'] = 1;
            //$list_client = $this->assoc_client_commercant_model->get_by_commercant_id ( $id_commercant, $criteres );
            //$data ['clients'] = $list_client;
            //echo $this->load->view ( 'mobile2014/pro/liste-clients-print', $data, true );
        } else if ($action == 'csv') {
            $this->load->dbutil ();
            $criteres ['query'] = 1;
            $query = $this->assoc_client_commercant_model->get_by_commercant_id_csv ( $id_commercant );
            $csv = $this->dbutil->csv_from_result ( $query );
            $this->load->helper ( 'download' );
            force_download ( 'liste_contact.csv', $csv );
        } else {


            if ($action == 'bonplan') {
                $data ['show_offre'] = 0;
            }
            if ($action == 'fidelisation') {
                $data ['show_bonplan'] = 0;
                $data ['show_offre'] = 1;
            }
            return $data;

        }

    }
    public function envoiMailClient(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $login=$_POST["login"];
        //var_dump($login);
        $message=$_POST["message"];
        $objet=$_POST["objet"];
        $infocom=$this->api_model->infoCommercant($login);
        //var_dump($infocom);
        if($objet == 'bonplan'){
            $link_image=base_url('application/resources/front/images/bonplan_logo.jpg');
        }elseif($objet =='bon cadeau joyeux anniversaire'){
            $link_image=base_url('application/resources/front/images/annif_logo.png');
        }elseif($objet =='Reserver votre table'){
            $link_image=base_url('application/resources/front/images/table_logo.jpg');
        }elseif($objet =='other'){
            $objet=$infocom->titre_entete;
            $link_image=base_url('application/resources/front/images/logo_mobile/'.$infocom->image_mobile);
        }
        $client=$_POST["datas"];
        if (isset($_POST['custom_link'])){
            $custom_link=$_POST['custom_link'];
        }
        $existed_link=$_POST['existed_link'];
        if (isset($custom_link) AND $custom_link != null AND $custom_link !=''){
            $link=$custom_link;
        }elseif(isset($existed_link) AND $existed_link !=null AND $existed_link !=''){
            if ($existed_link =='art'){
                $link = site_url().$infocom->nom_url.'/article';
            }elseif($existed_link =='ag'){
                $link = site_url().$infocom->nom_url.'/agenda';
            }elseif($existed_link =='bp_list'){
                $link = site_url().$infocom->nom_url.'/notre_bonplan';
            }elseif($existed_link =='plat'){
                $link = site_url().$infocom->nom_url.'/reservation';
            }elseif($existed_link =='bout'){
                $link = site_url().$infocom->nom_url.'/annonces';
            }elseif($existed_link =='home'){
                $link = site_url().$infocom->nom_url.'/presentation';
            }elseif ($existed_link == 'fd'){

            $objTampon = $this->mdl_card_tampon->getByIdCommercant($infocom->IdCommercant);
            $objCapital = $this->mdl_card_capital->getByIdCommercant($infocom->IdCommercant);
            $objRemise = $this->mdl_card_remise->getByIdCommercant($infocom->IdCommercant);

    if (!empty($objTampon) OR !empty($objCapital) OR !empty($objRemise)){

    if (isset($objTampon->is_activ) AND $objTampon->is_activ=='1'){
        $fidelityid=$objTampon->id;
    }
    if (isset($objCapital->id) AND $objCapital->is_activ=='1'){
        $fidelityid=$objCapital->id;
    }
    if (isset($objRemise->id) AND $objRemise->is_activ=='1'){
        $fidelityid=$objRemise->id;
    }
    if (isset($objTampon->is_activ) AND $objTampon->is_activ=='1'){
        $_iTypeFidelity='tampon';
    }
    if (isset($objCapital->id) AND $objCapital->is_activ=='1'){
        $_iTypeFidelity='capital';
    }
    if (isset($objRemise->id) AND $objRemise->is_activ=='1'){
        $_iTypeFidelity='remise';
    }
        $link= site_url($infocom->nom_url).'/notre_fidelisation/'.$_iTypeFidelity.'/'.$fidelityid;

    }
        }
    }
    //var_dump($link);die();
        foreach ($client as $data){
            //var_dump($data['mail']);
            if ($this->sendall($data['idcom'],$data['mail'],$message,$objet,$link,$link_image)){
                $ok=1;
            }
        }
        //die();
        $json['json']="ok";
        echo json_encode($json);
    }
   public function sendall($login,$logins,$message,$objet,$link,$link_image){

        $infocom=$this->api_model->infoCommercant($login);
        //$from_email=$_POST["login"];
        $contact_privicarte_nom = $infocom->Nom.' '.$infocom->Prenom;
        $contact_privicarte_mail =$infocom->Email;
        $contact_privicarte_mailto = $logins;
        $contact_privicarte_mailSubject = $objet;
        $envoyeur = $infocom->Email;
       $nomenvoyeur = $infocom->NomSociete;

        if (!isset($contact_privicarte_mailto) || $contact_privicarte_mailto == "" || $contact_privicarte_mailto == NULL) $contact_privicarte_mailto = "srova76@gmail.com";

        if (!isset($contact_privicarte_mail) || $contact_privicarte_mail == "" || $contact_privicarte_mail == NULL) $contact_privicarte_mail = "srova76@gmail.com";

        if (!isset($contact_privicarte_mailSubject) || $contact_privicarte_mailSubject == "" || $contact_privicarte_mailSubject == NULL) $contact_privicarte_mailSubject = 'Contact - Sortez';
        if (isset($infocom->logo_mobile) AND $infocom->logo_mobile !=null AND $infocom->logo_mobile !=''){
            $img="<img style='width:200px' src=".base_url('application/resources/front/images/logo_mobile/'.$infocom->logo_mobile.'').">";
        }else{
            $img="";
        }
       $txtContenuAdmin = '

      <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
	<table width="500px" style="text-align: center;">
		<tr>
			<td>'.$img.'</td>
		</tr>
		<tr>
			<td>'.$infocom->titre_entete.'</td>
		</tr>
        <tr>
        	<td>
        		<img width="200px"  src="'.$link_image.'">
        	</td>
        </tr>
 		<tr>
 			<td>
 				<p>'.$message.'</p>
 			</td>
 		</tr>
        <tr>
        	<td>
<a href="'.$link.'"><div style="height: 50px;width: 300px;background-color: #008000;text-align: center;margin: auto;color: white!important;padding-top: 20px;font-size: 20px">Plus de details</div></a>        </td>
    </tr>
</table>
        </div>
        ';
       $colDestAdmin[] = array("Email" =>$contact_privicarte_mailto, "Name" => $contact_privicarte_nom);
       if (envoi_notification($colDestAdmin, $contact_privicarte_mailSubject, $txtContenuAdmin,$envoyeur, $nomenvoyeur)){
           return true;
       }else{
           return false;
       }
    }

    public function  flut_post(){
            $rest_json=file_get_contents("php://input");
            $_POST=json_decode($rest_json,true);
            if ($_POST !=null){
                $data=array("id"=>"2","nom_manifestation"=>$_POST["identity"]);
                header('Content-Type: application/json');
                echo json_encode($data);
            }
    }

    public function liste_reservation_plat(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $login=$_POST["login"];
        $infocom=$this->api_model->get_login_by_id($login);
        $data['commercant']=$this->api_model->get_login_by_id2($login);
        $data['plat']=$this->api_model->get_plat_list_by_login($infocom->IdCommercant,'0');
        $data['plat_date_demande']=$this->api_model->get_plat_list_by_login_date_demande($infocom->IdCommercant,'0');
        $data['table']=$this->api_model->get_table_list_by_login($infocom->IdCommercant);
        $data['table_date_demande']=$this->api_model->get_table_list_by_login_date_demande($infocom->IdCommercant);
        $data['table_date_debut']=$this->api_model->get_table_list_by_login_date_debut($infocom->IdCommercant);
        $data['sejour']=$this->api_model->get_sejour_list_by_login($infocom->IdCommercant);
        $data['sejour_date_demande']=$this->api_model->get_sejour_list_by_login_date_demande($infocom->IdCommercant);
        $data['sejour_date_debut']=$this->api_model->get_sejour_list_by_login_date_debut($infocom->IdCommercant);
        echo json_encode($data);
    }
    public function get_client_detail(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idclient=$_POST["id_client"];
        //echo $idclient;die();
        $res= $this->user->GetById((int)$idclient);
        echo json_encode($res);
    }
    public function get_infoconso(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idclient=$_POST["id_client"];
        $idclient=(int)$idclient;
        $login=$_POST["login"];
        $datas=$this->api_model->get_login_by_id2($login);
        //var_dump($datas->IdCommercant);die();
        $id_commercant =$datas->IdCommercant;
            $data ['titre'] = "Historique consommation";
            $critere = array (
                'card_bonplan_used.id_commercant' => $id_commercant,
                'card_bonplan_used.id_user' => $idclient
            );
            $data ['list_activity'] = $this->mdl_card_bonplan_used->get_by_critere ( $critere );
            $data ['offre'] = $offre = get_offre_active ( $id_commercant );
            $data ['client'] = $this->api_model->Get_users_ById($idclient,$id_commercant);
            $data ['commercant'] = $datas;
            $data ['fidelity'] = get_fiche_client ( $offre, $idclient, $id_commercant );
            $data ['idcom'] = $idclient;

        $data ['titre'] = "Historique consommation";
        $data['user_bonplan'] = $this->api_model->get_list_bonplan_by_criteria($id_commercant,$idclient);
        $data['plat']= $this->api_model->get_historic_plat_by_criteria($id_commercant,$idclient);
        echo json_encode($data);
    }

    public function reinistialisation() {
        
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_commercant=$_POST['id_commercant'];
        $id_user = $_POST['id_user'];
        $bonplan_id = $_POST['bonplan_id'] ?? null;
        $fiche_id = $_POST['fiche_id'];
        $montant_init = $_POST['montant_init'];
        $montant_init = ! empty($montant_init ) ? $montant_init : 0;

        $assoc_client_commercant = $this->assoc_client_commercant_model->get_by_id ( $id_user, $id_commercant );
        $oCard = $this->mdl_card->getByIdUser ( $id_user );

        $data_fiche ['id_user'] = $oCard->id_user;
        $data_fiche ['id_card'] = $oCard->id;
        $data_fiche ['id_ionauth_client'] = $oCard->id_ionauth_user;
        $data_fiche ['id_commercant'] = $id_commercant;

        if ($assoc_client_commercant) {
            if ($_POST ['type_offre']) {
                $offre = $_POST ['type_offre'];

                if ($offre == "tampon") {
                    $card_bonplan_used ['cumul_tampon'] = $montant_init;
                    $card_bonplan_used ['tampon_value_added'] = $montant_init;
                    $card_bonplan_used ['description'] = "Reinitialisation tampon";
                    if ($fiche_id) {
                        $this->mdl_card_fiche_client_tampon->update_where ( array (
                            'solde_tampon' => $montant_init
                        ), array (
                            'id' => $fiche_id
                        ) );
                    } else {
                        $data_fiche ['solde_capital'] = $montant_init;
                        $this->mdl_card_fiche_client_tampon->insert ( $data_fiche );
                    }
                }
                if ($offre == "remise") {
                    $card_bonplan_used ['cumul_remise'] = $montant_init;
                    $card_bonplan_used ['remise_value_added'] = $montant_init;
                    $card_bonplan_used ['description'] = "Reinitialisation remise";
                    if ($fiche_id)
                        $this->mdl_card_fiche_client_remise->update_where ( array (
                            'solde_remise' => $montant_init
                        ), array (
                            'id' => $fiche_id
                        ) );
                    else {
                        $data_fiche ['solde_remise'] = $montant_init;
                        $this->mdl_card_fiche_client_remise->insert ( $data_fiche );
                    }
                }
                if ($offre == "capital") {
                    $card_bonplan_used ['cumul_capital'] = $montant_init;
                    $card_bonplan_used ['capital_value_added'] = $montant_init;
                    $card_bonplan_used ['description'] = "Reinitialisation capital";
                    if ($fiche_id)
                        $this->mdl_card_fiche_client_capital->update_where ( array (
                            'solde_capital' => $montant_init
                        ), array (
                            'id' => $fiche_id
                        ) );
                    else {
                        $data_fiche ['solde_remise'] = $montant_init;
                        $this->mdl_card_fiche_client_remise->insert ( $data_fiche );
                    }
                }

                // saving into card_bonplan_used
                $card_bonplan_used ['id_commercant'] = $id_commercant;
                $card_bonplan_used ['id_user'] = $id_user;
                $card_bonplan_used['id_bonplan'] = $bonplan_id;
                $card_bonplan_used ['scan_status'] = "OK";
                $card_bonplan_used ['date_use'] = date ( "Y-m-d h:m:s" );
                //var_dump($card_bonplan_used);die();
                $this->mdl_card_bonplan_used->deleteByUCid ( $id_user, $id_commercant );
                $this->mdl_card_bonplan_used->insert ( $card_bonplan_used );
                if ($offre == "bonplan"){
                    $assoc_client_bonplan_id = $_POST [ 'assoc_client_bonplan_id' ];
                    $this->assoc_client_bonplan_model->deleteById ( $assoc_client_bonplan_id );
                }
            } else if ($_POST ['assoc_client_bonplan_id' ]) {
                $assoc_client_bonplan_id = $_POST [ 'assoc_client_bonplan_id' ];
                $this->assoc_client_bonplan_model->deleteById ( $assoc_client_bonplan_id );
            }
        }
       echo json_encode('ok');
    }

    public function qrcodeGenerator ($qrtext,$SERVERFILEPATH)
    {



        if(isset($qrtext))
        {

            //file path for store images
            $text = $qrtext;
            $text1= substr($text, 0,9);

            $folder = $SERVERFILEPATH;
            $file_name1 = $text1."-Qrcode" . rand(2,200) . ".png";
            $file_name = $folder.$file_name1;
            QRcode::png($text,$file_name);
        }
        else
        {
            echo 'No Text Entered';
        }
    }
    public function get_table_detail(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idres=$_POST['idres'];
        $data=$this->api_model->get_res_table_by_id($idres);
        echo json_encode($data);
    }
    public function get_sejour_detail(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idres=$_POST['idres'];
        $data=$this->api_model->get_res_sejour_by_id($idres);
        echo json_encode($data);
    }
    public function delete_res_sejour(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idres=$_POST['idres'];
        $this->api_model->validate_sejour($idres);
        echo json_encode('ok');
    }
    public function delete_res_table(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idres=$_POST['idres'];
        $this->api_model->validate_table($idres);
        echo json_encode('ok');
    }
    public function get_plat_detail(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idres=$_POST['idplat'];
        $data=$this->api_model->get_detail_plat_by_idres($idres);
        echo json_encode($data);
    }
    public function validate_plat(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idres=$_POST['idres'];
        $data=$this->api_model->valid_res_plat($idres);
        echo json_encode('ok');
    }
    public function details_bonplan(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idcom=$_POST['id'];

    }
    public function save_image_name(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idcom=$_POST['idcom'];
        $img_name=$_POST['nom_photo'];
        $array=array(
            'IdCommercant'=>$idcom,
            'logo_mobile'=>$img_name
        );
        $this->api_model->save_name_com_photo($idcom,$array);
        echo json_encode('ok');
    }
    public function save_logo_name(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idcom=$_POST['idcom'];
        $img_name=$_POST['nom_photo'];
        $array=array(
            'IdCommercant'=>$idcom,
            'logo_mobile'=>$img_name
        );
        $this->api_model->save_name_com_photo($idcom,$array);
        echo json_encode('ok');
    }
    function upload_logo(){
        $target_path = "application/resources/front/images/logo_mobile/";

        $target_path = $target_path . basename( $_FILES['file']['name']);

        if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
          echo  json_encode('okphoto');
        } else {
            echo $target_path;
            echo "There was an error uploading the file, please try again!";
        }

    }
    public function save_image_name_mobile(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idcom=$_POST['idcom'];
        $img_name=$_POST['nom_photo'];
        $array=array(
            'IdCommercant'=>$idcom,
            'image_mobile'=>$img_name
        );
        $this->api_model->save_name_com_photo($idcom,$array);
        echo json_encode('ok');
    }
    public function upload_image(){
        $target_path = "application/resources/front/images/logo_mobile/";
        $target_path = $target_path . basename( $_FILES['file']['name']);
        if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
            echo  json_encode('okphoto');
        } else {
            echo "There was an error uploading the file, please try again!";
        }
    }
    public function liste_plat(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idcom=$_POST['idcommercant'];
        $data=$this->api_model->liste_plat($idcom);
        echo json_encode($data);
    }
    public function get_list_bonplan(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idcom=$_POST['idcom'];
        $list=$this->api_model->get_all_bonplan_by_idcom($idcom);
        echo json_encode($list);
    }
    public function delete_bonplan_by_id(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idbonplan=$_POST['id'];
        $this->api_model->delete_bonplan_by_id($idbonplan);
        echo json_encode('ok');
    }
    public function delete_plat_by_id(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idbonplan=$_POST['id'];
        $this->api_model->delete_plat_by_id($idbonplan);
        echo json_encode('ok');
    }
    function create_plat(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idcom =$_POST['idcommercant'];
        $infocom=$this->api_model->infoCommercant($idcom);
        $_POST['IdUsers_ionauth'] = $infocom->user_ionauth_id;
        //var_dump($_POST);
        $IdInsertedBonplan = $this->api_model->insert_plat($_POST);
        if ($IdInsertedBonplan){$json['json']="ok";
            echo json_encode($json);}

    }

    function update_by_idplat(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idcom=$_POST['idplat'];
        //var_dump($idcom);die("info post");
        $IdInsertedBonplan = $this->api_model->update_by_idplat($idcom);
       echo json_encode($IdInsertedBonplan);
    }

    function update_plat(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        //var_dump($_POST);die("info post");
        //var_dump($idcom);die("info post");
        $IdInsertedBonplan = $this->api_model->update_plat($_POST);
        echo json_encode($IdInsertedBonplan);
    }
    public function save_bonplan_image(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idcom_b=$_FILES['file']['name'];
        //var_dump($idcom_b);die();
        $idcom_b_b=explode('-',$idcom_b);
        $idcom=$idcom_b_b[1];
        $infocom=$this->api_model->infoCommercant2($idcom);
        $auth = $infocom->user_ionauth_id;
        $target_path = "application/resources/front/photoCommercant/imagesbank/".$auth.'/';
      if (is_dir($target_path) != true){
          mkdir ($target_path, 0777);
        }
        $target_path = $target_path . basename( $_FILES['file']['name']);

        if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
            echo json_encode($target_path);
        } else {
            echo $target_path;
            echo "There was an error uploading the file, please try again!";
        }
    }
    //save plat image
    public function save_plat_image(){
//        header('Access-Control-Allow-Origin: *');
//        $rest_json=file_get_contents("php://input");
//        $_POST=json_decode($rest_json,true);
//        $idcom=$_POST['idcom'];
//        $infocom=$this->api_model->infoCommercant2($idcom);
//        $auth = $infocom->user_ionauth_id;
//        $target_path = "application/resources/front/photoCommercant/imagesbank/".$auth.'/';
//        if (is_dir($target_path) != true){
//            mkdir ($target_path, 0777);
//        }
//        $target_path = $target_path . basename( $_FILES['file']['name']);
//        if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
//            echo json_encode($target_path);
//        } else {
//            echo $target_path;
//            echo "There was an error uploading the file, please try again!";
//        }
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idcom_b=$_FILES['file']['name'];
        //var_dump($idcom_b);die();
        $idcom_b_b=explode('-',$idcom_b);
        $idcom=$idcom_b_b[1];
        $infocom=$this->api_model->infoCommercant2($idcom);
        $auth = $infocom->user_ionauth_id;
        $target_path = "application/resources/front/photoCommercant/imagesbank/".$auth.'/';
        if (is_dir($target_path) != true){
            mkdir ($target_path, 0777);
        }
        $target_path = $target_path . basename( $_FILES['file']['name']);

        if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
            echo json_encode($target_path);
        } else {
            echo $target_path;
            echo "There was an error uploading the file, please try again!";
        }
    }
    public function save_bonplan_data_image(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idbonplan=$_POST['id_bonplan'];
        $idcom=$_POST['idcom'];
        $nom_photo=$_POST['nom_photo'];
        $field= array(
            'bonplan_photo1'=>$nom_photo
        );
        $this->api_model->save_bonplan_image($idcom,$idbonplan,$field);
        echo json_encode('ok');
    }

    //save data plat image
    public function save_plat_data_image(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_plat=$_POST['id_plat'];
        $idcom=$_POST['idcom'];
        $nom_photo=$_POST['nom_photo'];
        $field= array(
            'photo'=>$nom_photo
        );
        $this->api_model->save_plat_image($idcom,$id_plat,$field);
        echo json_encode('ok');
    }

    public function delete_bonplan_data_image(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idbonplan=$_POST['id_bonplan'];
        $idcom=$_POST['idcom'];
        $infocom=$this->api_model->infoCommercant2($idcom);
        $auth = $infocom->user_ionauth_id;
        $nom_photo= $this->api_model->get_nomPhotobp_by_id($idbonplan);
        //var_dump($nom_photo); die('dsfd');
        //var_dump($nom_photo[0]->photo);die("reslt photo slect");
//        var_dump($nom_photo);
//        var_dump($nom_photo->bonplan_photo1);
//        var_dump("application/resources/front/photoCommercant/imagesbank/".$auth.'/'.$nom_photo->photo);
        //var_dump($nom_photo->bonplan_photo1);die("glhlhghkg");
        if(isset($nom_photo->bonplan_photo1) AND $nom_photo->bonplan_photo1 != null){
            //var_dump('elie');die('dfdfdfd');
            unlink("application/resources/front/photoCommercant/imagesbank/".$auth.'/'.$nom_photo->bonplan_photo1);
        }else{
            json_encode("photo=null");
            //var_dump('eto');die('fgdgdgf');
        }     
        $photo_to_unlink= $this->api_model->delete_bp_image($idbonplan);
        //var_dump($photo_to_unlink);die("reslt");
        echo json_encode('ok');
    }
    public function get_list_bonplan_by_id(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idbonplan=$_POST['id'];
        $res= $this->api_model->get_bonplan_by_id($idbonplan);
        echo json_encode($res);
    }

    //delete plat data image
    public function delete_plat_data_image(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_plat=$_POST['id_plat'];
        $idcom=$_POST['idcom'];
        $infocom=$this->api_model->infoCommercant2($idcom);
        $auth = $infocom->user_ionauth_id;
        $nom_photo= $this->api_model->get_nomPhoto_by_id($id_plat);
        //var_dump($nom_photo[0]->photo);die("reslt photo slect");
        // var_dump($nom_photo);
        // var_dump($nom_photo->photo);
        // var_dump("application/resources/front/photoCommercant/imagesbank/".$auth.'/'.$nom_photo->photo);
        if($nom_photo->photo != null){
            // echo "path image physique";
            // unlink("application/resources/front/photoCommercant/imagesbank/".$auth.'/'.$nom_photo->photo);
            if(unlink("application/resources/front/photoCommercant/imagesbank/".$auth.'/'.$nom_photo->photo)){
                $photo_to_unlink= $this->api_model->delete_plat_image($id_plat);               
                echo json_encode('ok');
            }
        }else{
            echo "photo=null";
        }     
        //$photo_to_unlink= $this->api_model->delete_plat_image($id_plat);
        //var_dump($photo_to_unlink);die("reslt");        
        //echo json_encode('ok');
    }

    public function write_codebarre(){
        $rest_json=file_get_contents("php://input");
        $value_code = explode('=',$rest_json)[1];
        $monfichier = fopen('codebarred'.$this->getip().'.txt', 'a+');
        fputs($monfichier,$value_code);
        fclose($monfichier);
    }
    public function reset_codebarre(){
        $rest_json=file_get_contents("php://input");
        $monfichier = fopen('codebarred'.$this->getip().'.txt', 'a+');
        ftruncate($monfichier,0);
        fclose($monfichier);
    }
    public function get_codebarre(){
        $rest_json=file_get_contents("php://input");
        if(file_exists('codebarred'.$this->getip().'.txt')){
        $file = file_get_contents('codebarred'.$this->getip().'.txt', true);
        echo $file;
    }else{
        echo "";
    }
    }
    public function getip(){
       $ip =$_SERVER['REMOTE_ADDR'];
       $ip =str_replace('.', '',$ip);
       return $ip;
    }

    public function get_list_agendaMobile(){
        $rest_json=file_get_contents("php://input");
        $value_code = explode('=',$rest_json)[1];
        $res["agenda"]= $this->api_model->get_list_agendaMobile($value_code);
        $annuaire= $this->api_model->get_3_annuaire($value_code);
        $res["annuaire"] = [] ;
        if(is_array($annuaire) && !empty($annuaire))
       {
        foreach($annuaire as $key => $val){
            $path_img1="application/resources/front/photoCommercant/imagesbank/".$val->user_ionauth_id."/".$val->photo1;
            $path_img2="application/resources/front/photoCommercant/images/".$val->photo1;
            if(file_exists($path_img1) && $val->photo1 != null && $val->user_ionauth_id != null){
                $val->photo1 = base_url()."application/resources/front/photoCommercant/imagesbank/".$val->user_ionauth_id."/".$val->photo1;
            }elseif(file_exists($path_img2) && $val->photo1 != null){
                $val->photo1 = base_url()."application/resources/front/photoCommercant/images/".$val->photo1;
            }else{
                $val->photo1 = "https://www.testpriviconcept.ovh/application/resources/front/images/no_image_annuaire.png";
            }
            array_push($res["annuaire"], $val);
        }
        }else {
            array_push($res["annuaire"], "");
        }
       $annonce= $this->api_model->get_3_annonce($value_code);
       $res['annonce'] = [] ;
       if(is_array($annonce) && !empty($annonce))
       {
           foreach($annonce as $key => $val){
                   $path_img="application/resources/front/photoCommercant/imagesbank/".$val->user_ionauth_id."/".$val->annonce_photo1;
                   if(!file_exists($path_img) && $val->annonce_photo1 == null && $val->user_ionauth_id == null){
                       $val->annonce_photo1 = "https://www.testpriviconcept.ovh/boutique_logo.jpg";
                   }else{
                       $val->annonce_photo1 = base_url()."application/resources/front/photoCommercant/imagesbank/".$val->user_ionauth_id."/".$val->annonce_photo1;
                   }
                   array_push($res['annonce'], $val);
           }
       }else{
        array_push($res['annonce'],"");
       }
        $article= $this->api_model->get_3_article($value_code);
        $res["article"] = [] ;
        if(is_array($article) && !empty($article))
        {
            foreach($article as $key => $val){
                $path_img1="application/resources/front/photoCommercant/imagesbank/".$val->IdUsers_ionauth."/".$val->photo1;
                $path_img2="application/resources/front/photoCommercant/images/".$val->photo1;
                if(file_exists($path_img1) && $val->photo1 != null && $val->IdUsers_ionauth != null){
                    $val->photo1 = base_url()."application/resources/front/photoCommercant/imagesbank/".$val->IdUsers_ionauth."/".$val->photo1;
                }elseif(file_exists($path_img2) && $val->photo1 != null){
                    $val->photo1 = base_url()."application/resources/front/photoCommercant/images/".$val->photo1;
                }else{
                    $val->photo1 = "https://www.testpriviconcept.ovh/application/resources/front/images/no_image_annuaire.png";
                }
                array_push($res["article"], $val);
            }
        }else {
            array_push($res["article"], "");
        }
        echo json_encode($res);
    }
    public function get_agenda_by_id(){
        $rest_json=file_get_contents("php://input");
        $value_code = explode('=',$rest_json)[1];

        // var_dump($value_code);die("test");
        // //$_POST=json_decode($rest_json,true);
        // $idville=$_POST['idville'];

        $res= $this->api_model->get_agenda_by_id($value_code);

        echo json_encode($res);
    }
    public function get_list_annuaireMobile(){
        $rest_json=file_get_contents("php://input");
        $value_code = explode('=',$rest_json)[1];
        $res= $this->api_model->get_list_annuaireMobile($value_code);
        $resultat = [] ;
        if(is_array($res) && !empty($res))
        {
        foreach($res as $key => $val){
                $path_img1="application/resources/front/photoCommercant/imagesbank/".$val->user_ionauth_id."/".$val->photo1;
                $path_img2="application/resources/front/photoCommercant/images/".$val->photo1;
                if(file_exists($path_img1) && $val->user_ionauth_id != null && $val->photo1 != null){
                    $val->photo1 = base_url()."application/resources/front/photoCommercant/imagesbank/".$val->user_ionauth_id."/".$val->photo1;
                }elseif(file_exists($path_img2) && $val->photo1 != null){
                    $val->photo1 = base_url()."application/resources/front/photoCommercant/images/".$val->photo1;
                }else{
                    $val->photo1 = "https://www.testpriviconcept.ovh/application/resources/front/images/no_image_annuaire.png";
                }
                array_push($resultat, $val);
        }
    }
    echo json_encode($resultat);
    }
    public function get_all_annonce_by_cod_postal(){
    // $rest_json=file_get_contents("php://input");
    // $value_code = explode('=',$rest_json)[1];
    $value_code=0;
    $res= $this->api_model->get_annonce_by_codepostal($value_code);
    //var_dump(json_encode($res));die();
    $resultat = [] ;
    if(is_array($res) && !empty($res))
    {
        foreach($res as $key => $val){
                $path_img="application/resources/front/photoCommercant/imagesbank/".$val->user_ionauth_id."/".$val->annonce_photo1;
                if(!file_exists($path_img)){
                    $val->annonce_photo1 = "https://www.testpriviconcept.ovh/boutique_logo.jpg";
                }else{
                    $val->annonce_photo1 = base_url()."application/resources/front/photoCommercant/imagesbank/".$val->user_ionauth_id."/".$val->annonce_photo1;
                }
                array_push($resultat, $val);
        }
    }
    echo json_encode($resultat);
    }
     public function get_all_list_agendaMobile(){
        $rest_json=file_get_contents("php://input");
        $value_code = explode('=',$rest_json)[1];
        $res= $this->api_model->get_all_list_agendaMobile($value_code);
        echo json_encode($res);
    }
    public function get_annuaire_by_id(){
        $rest_json=file_get_contents("php://input");
        $value_code = explode('=',$rest_json)[1];
        $res= $this->api_model->get_annuaire_by_id($value_code);
        $path_img1="application/resources/front/photoCommercant/imagesbank/".$res->user_ionauth_id."/".$res->Photo1;
        $path_img2="application/resources/front/photoCommercant/images/".$res->Photo1;
        if(file_exists($path_img1) && $res->user_ionauth_id != null && $res->Photo1 != null){
            $res->Photo1 = base_url()."application/resources/front/photoCommercant/imagesbank/".$res->user_ionauth_id."/".$res->Photo1;
        }elseif(file_exists($path_img2) && $res->Photo1 != null){
            $res->Photo1 = base_url()."application/resources/front/photoCommercant/images/".$res->Photo1;
        }else{
            $res->Photo1 = "https://www.testpriviconcept.ovh/application/resources/front/images/no_image_annuaire.png";
        }
        echo json_encode($res);
    }
    public function get_by_id_annonce_by_cod_postal(){
        $rest_json=file_get_contents("php://input");
        $value_code = explode('=',$rest_json)[1];
        $res= $this->api_model->get_by_id_annonce_by_cod_postal($value_code);
        $path_img="application/resources/front/photoCommercant/imagesbank/".$res->user_ionauth_id."/".$res->annonce_photo1;
        if(!file_exists($path_img)){
            $res->annonce_photo1 = "https://www.testpriviconcept.ovh/boutique_logo.jpg";
        }else{
            $res->annonce_photo1 = base_url()."application/resources/front/photoCommercant/imagesbank/".$res->user_ionauth_id."/".$res->annonce_photo1;
        }
        echo json_encode($res);
    }
    public function get_agendaMobile_byId(){
        $rest_json=file_get_contents("php://input");
        $value_code = explode('=',$rest_json)[1];
        $res= $this->api_model->get_agendaMobile_byId($value_code);
        echo json_encode($res);
    }
    public function get_list_articleMobile(){
        $rest_json=file_get_contents("php://input");
        $value_code = explode('=',$rest_json)[1];
        //$value_code = '06000';
        $res= $this->api_model->get_list_articleMobile($value_code);
        //var_dump($res);die('koko');
        $path_img="application/resources/front/photoCommercant/imagesbank/".$res->IdUsers_ionauth."/".$res->photo1;
        $path_img2="application/resources/front/photoCommercant/imagesbank/rss_image/".$res->photo1;
        if(!file_exists($path_img) && !file_exists($path_img2)){
            $res->photo1 = "https://www.testpriviconcept.ovh/boutique_logo.jpg";
        }else if(file_exists($path_img)){
            $res->photo1 = base_url()."application/resources/front/photoCommercant/imagesbank/".$res->IdUsers_ionauth."/".$res->photo1;
        }else{
            $res->photo1 = base_url()."application/resources/front/photoCommercant/imagesbank/rss_image/".$res->photo1;
        }
        echo json_encode($res);
    }
    public function get_article_byId(){
        $rest_json=file_get_contents("php://input");
        $value_code = explode('=',$rest_json)[1];
        $res= $this->api_model->get_article_by_id($value_code);
        $path_img="application/resources/front/photoCommercant/imagesbank/".$res->user_ionauth_id."/".$res->photo1;
        $path_img2="application/resources/front/photoCommercant/imagesbank/rss_image/".$res->photo1;
        if(!file_exists($path_img) && !file_exists($path_img2)){
            $res->photo1 = "https://www.testpriviconcept.ovh/boutique_logo.jpg";
        }else if(file_exists($path_img)){
            $res->photo1 = base_url()."application/resources/front/photoCommercant/imagesbank/".$res->user_ionauth_id."/".$res->photo1;
        }else{
            $res->photo1 = base_url()."application/resources/front/photoCommercant/imagesbank/rss_image/".$res->photo1;
        }
        echo json_encode($res);
    }
    public function get_test(){
        $annonce= $this->api_model->get_3_annonce($value_code = '06000');
        $res['annonce'] = [] ;
        if(is_array($annonce) && !empty($annonce))
        {
            foreach($annonce as $key => $val){
                $path_img="application/resources/front/photoCommercant/imagesbank/".$val->user_ionauth_id."/".$val->annonce_photo1;
                if(!file_exists($path_img) && $val->annonce_photo1 == null && $val->user_ionauth_id == null){
                    $val->annonce_photo1 = "https://www.testpriviconcept.ovh/boutique_logo.jpg";
                }else{
                    $val->annonce_photo1 = base_url()."application/resources/front/photoCommercant/imagesbank/".$val->user_ionauth_id."/".$val->annonce_photo1;
                }
                array_push($res['annonce'], $val);
            }
        }else{
            array_push($res['annonce'],"");
        }
        var_dump($res);die('donnée');
    }
    public function commercant_idville_localisation(){
        $res = $this->commercant->commercant_idville_localisation();     
        //var_dump($res);die("fin test"); 
        foreach($res as $ores){
            $idcom=$ores->IdCommercant;
            if($ores->IdVille_localisation==null OR $ores->IdVille_localisation=="" OR $ores->IdVille_localisation=="0"){
                $IdVille_localisation=$ores->IdVille;
                $this->commercant->update_commercant_idville_localisation($idcom,$IdVille_localisation);
            }
            //echo $ores->IdVille." / ".$ores->IdVille_localisation."<br>";
        }       

    }

    public function liste_commande(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $login=$_POST["login"];
        $infocom=$this->api_model->get_login_by_id($login);
        $data['commercant']=$this->api_model->get_login_by_id2($login);
        $data['commande']=$this->api_model->get_commande_list_by_idcom($infocom->IdCommercant);
        echo json_encode($data);
    }

    public function get_detail_commandeby_id_commande(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $login=$_POST["login"];
        $idcommande = $_POST['idcommande'];
        $idclient = $_POST['idclient'];
        $data['details_commande'] = $this->api_model->get_command_details($idcommande);
        $data['commercant'] = $this->api_model->get_login_by_id2($login);
        $data['client'] = $this->api_model->getuser_by_id($idclient);
        $data['commande'] = $this->api_model->get_commande_list_by_id($idcommande);
        echo json_encode($data);
    }
    public function get_product_name_by_id(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $product_id=$_POST["id_product"];
        $data['product_name'] = $this->api_model->get_product_name_by_id($product_id);
        echo $data;
    }
    public function get_all_prospect_by_id_com(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $login=$_POST["login"];
        $infocom=$this->api_model->get_login_by_id($login);
        $data['commercant']=$this->api_model->get_login_by_id2($login);
        $data['prospect']=$this->api_model->get_prospect_list_by_idcom($infocom->IdCommercant);
        echo json_encode($data);
    }
    public function save_prospect(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idcom=$_POST["IdCommercant"];
        $Nom=$_POST["Nom"];
        $Prenom=$_POST["Prenom"];
        $Telephone=$_POST["Telephone"];
        $Email=$_POST["Email"];
        $fields = array(
            'Nom' =>$Nom,
            'Prenom'=>$Prenom,
            'Email'=>$Email,
            'Telephone'=>$Telephone,
            'IdComprospect'=>$idcom,
        );
        $this->api_model->save_prospect_by_id_com($fields);
        $data['status'] ='ok';
        echo json_encode($data);
}
public function upload_and_parse_xlsx_prospect(){
    $this->load->library('SimpleXLSX');
    $rest_json=file_get_contents("php://input");
    $_POST=json_decode($rest_json,true);
    $idcom_b=$_FILES['file']['name'];
    //var_dump($idcom_b);die();
    $idcom_b_b=explode('.',$idcom_b);
    $idcom=$idcom_b_b[0];
    $infocom=$this->api_model->infoCommercant2($idcom);
    $auth = $infocom->user_ionauth_id;
    $target_path = "application/resources/front/photoCommercant/imagesbank/".$auth.'/';
    if (is_dir($target_path) != true){
        mkdir ($target_path, 0777);
    }
    $target_path = "application/resources/front/photoCommercant/imagesbank/".$auth.'/uploaded_xls_mobile/';
    if (is_dir($target_path) != true){
        mkdir ($target_path, 0777);
    }
    
    $target_path = $target_path . basename( $_FILES['file']['name']);

    if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
        if($xlsx = SimpleXLSX::parse($target_path)){
            $allparsed = $xlsx->rows();
            $row =0;
            //var_dump($allparsed);die('en reste ici');
            foreach ($allparsed as $path) {
                if ($row > 0){
                $fields = array("Nom" =>$path[0],
                    "Prenom"=>$path[1],
                    "Telephone" => $path[2],
                    "Email" => $path[3]
                );
                $fields["IdComprospect"] = $infocom->IdCommercant;
                $this->Mdl_prospect->save_all($fields);
                }
                $row++;
            }
            $data['status'] = '1';
        }else{
            echo "failed to parse";
        }
        
        echo json_encode($data);
    } else {
        $data['status'] = '0';
    }
}

public function change_status_command(){
    $rest_json=file_get_contents("php://input");
    $_POST=json_decode($rest_json,true);
    $idcomande=$_POST['idcommande'];
    $status=$_POST['status'];
    $this->api_model->change_commande_status($idcomande,$status);
    $data['status'] = '1';
    echo json_encode($data);
}

public function liste_commande_menu(){
    $rest_json=file_get_contents("php://input");
    $_POST=json_decode($rest_json,true);
    $login=$_POST["login"];
    $infocom=$this->api_model->get_login_by_id($login);
    $data['commercant']=$this->api_model->get_login_by_id2($login);
    $data['commande_menu']=$this->api_model->get_commande_list_menu_by_idcom($infocom->IdCommercant);
    echo json_encode($data);
}

public function get_detail_commande_menuby_id_commande(){
    $rest_json=file_get_contents("php://input");
    $_POST=json_decode($rest_json,true);
    $idcommande = $_POST['idcommande'];
    $data['commande'] = $this->api_model->get_commande_list_menu_by_id($idcommande);
    echo json_encode($data);
}

public function convert_to_html($id)
{
    $html = $this->api_model->get_commande_list_menu_by_id($id);
    $data['html_returned'] = html_entity_decode(htmlspecialchars_decode($html->contenue_commande));
    echo $data['html_returned'];
    echo "<style>
    .titre_gli_up {
        font-size: 20px!important;
        text-align: center!important;
    }
    .text_label_mont {
        font-size: 9px!important;
        margin-bottom: 0!important;
    }
    .prix_tot {
        font-size: 12px!important;
    }
    .title_gli_sm_height {
        display: block !important;
        height: 60px!important;
        padding:0!important;
    }

    .textarea_style_input {
        position: absolute;
        right:20px;
        z-index:-1;
        top:-5px;
    }
    
        </style>";
}

public function change_status_command_menu(){
    $rest_json=file_get_contents("php://input");
    $_POST=json_decode($rest_json,true);
    $idcomande=$_POST['idcommande'];
    $status=$_POST['status'];
    $this->api_model->change_commande_status_menu($idcomande,$status);
    $data['status'] = '1';
    echo json_encode($data);
}

public function delete_all_selected_prospect(){
    $rest_json = file_get_contents("php://input");
    $_POST = json_decode($rest_json,true);
    $all_to_delete = $_POST['to_delete'];
    foreach($all_to_delete as $delete){
        $this->api_model->delete_prospect($delete['id']);
    }
    $data['status']='ok';
    echo json_encode($data);
}

public function ajouterParticulier() {
    $rest_json = file_get_contents("php://input");
    $objUser = json_decode($rest_json,true);
    
    if (!$objUser) {
        $data['status'] = "ko";
    }
    $objUser['Email'] = $objUser['Login'];
    $particulier_Password = $objUser['Password'];
    $idVille = $this->mdlville->GetVilleByCodePostal_localisation($objUser['CodePostal']);
    
    if(!empty($idVille)){
        $Rappeldevosinformations = "";
        $Rappeldevosinformations .= "<p>Rappel de vos informations</p>";
        $Rappeldevosinformations .= "<p>";
        $Rappeldevosinformations .= "Nom : ".$objUser['Nom']."<br/>";
        $Rappeldevosinformations .= "DateNaissance : ".$objUser['DateNaissance']."<br/>";
        if ($objUser['Civilite'] == "0") $Rappeldevosinformations .= "Civilite : Monsieur<br/>";
        if ($objUser['Civilite'] == "1") $Rappeldevosinformations .= "Civilite : Madame<br/>";
        if ($objUser['Civilite'] == "2") $Rappeldevosinformations .= "Civilite : Mademoiselle<br/>";
        //$Rappeldevosinformations .= "Profession : ".$objUser['Profession']."<br/>";
        $Rappeldevosinformations .= "Adresse : ".$objUser['Adresse']."<br/>";
        $obj_ville_ = $this->mdlville->getVilleById($idVille[0]->IdVille);
        $objUser['IdVille'] = $idVille[0]->IdVille;
        if ($obj_ville_) $Rappeldevosinformations .= "Ville : ".$obj_ville_->Nom."<br/>";
        $Rappeldevosinformations .= "CodePostal : ".$objUser['CodePostal']."<br/>";
        $Rappeldevosinformations .= "Telephone : ".$objUser['Telephone']."<br/>";
        $Rappeldevosinformations .= "Mobile : ".$objUser['Portable']."<br/>";
        //$Rappeldevosinformations .= "Fax : ".$objUser['Fax']."<br/>";
        $Rappeldevosinformations .= "Login : ".$objUser['Login']."<br/>";
        $Rappeldevosinformations .= "</p>";
        
        $objUser["IsActif"] = "1";
    
        if($objUser['DateNaissance']!="") $objUser['DateNaissance'] = convert_Frenchdate_to_Sqldate($objUser['DateNaissance']);
        else $objUser['DateNaissance'] = null;
        if ($objUser['DateNaissance'] == "--") $objUser['DateNaissance'] = date("Y-m-d");
    
    
        if ($objUser["IdUser"] == 0){
            $objUser["DateCreation"] = date("Y-m-d");
    
            //ion_auth register
            $username_ion_auth = $objUser['Login'];
            ////$this->firephp->log($username_ion_auth, 'username_ion_auth');
            $password_ion_auth = $particulier_Password;
            ////$this->firephp->log($particulier_Password, 'particulier_Password');
            $email_ion_auth = $objUser['Login'];
            ////$this->firephp->log($email_ion_auth, 'email_ion_auth');
            $additional_data_ion_auth = array('first_name' => $objUser['Nom'],'last_name' => $objUser['Prenom'], 'phone' => $objUser['Telephone']);
            $group_ion_auth = array('2'); // Sets user to particulier.
            $this->load->library('ion_auth');
            $user_part_ion = $this->ion_auth->register($username_ion_auth, $password_ion_auth, $email_ion_auth, $additional_data_ion_auth, $group_ion_auth);
            $ion_ath_error = $this->ion_auth->errors();
            $user_lastid_ionauth = $this->ion_auth_used_by_club->GetUserlast_id($objUser['Nom'], $username_ion_auth, $email_ion_auth);
            $objUser["user_ionauth_id"] = $user_lastid_ionauth;
            ////$this->firephp->log($user_lastid_ionauth, 'user_lastid_ionauth');
            //echo "qsf : ".$user_lastid_ionauth;
            //ion_auth register
    
            //var_dump($ion_ath_error); die();
    
    
            if (!$user_part_ion && isset($ion_ath_error) && $ion_ath_error!="") {
                $verifuserprofil = 2;
            } else {
                $objUser_id = $this->user->Insert($objUser);
                $objUser=$this->user->GetById($objUser_id);
                $id_ion_auth=$this->user->getbyids($objUser_id);
                $verifuserprofil = 0;
                if (isset($id_ion_auth->user_ionauth_id) AND $id_ion_auth->user_ionauth_id !=''  AND $id_ion_auth->user_ionauth_id!=null){
                    //var_dump($id_ion_auth->user_ionauth_id);die();
                    generate_client_card($id_ion_auth->user_ionauth_id);
                        }
            }
        } else {
    
            //ion_auth_update
            $user_ion_auth = $this->ion_auth->user()->row();
            $data_ion_auth_update = array(
                'first_name' => $objUser['Nom'],
                'last_name' => $objUser['Prenom']
            );
            $this->ion_auth->update($user_ion_auth->id, $data_ion_auth_update);
            $ion_ath_error = $this->ion_auth->errors();
            //ion_auth_update
            //var_dump($ion_ath_error);die();
            if (isset($ion_ath_error) && $ion_ath_error!="") {
                $verifuserprofil = 2;
            } else {
                $objUser = $this->user->Update($objUser);
                $verifuserprofil = 1;
            }
    
        }
    
            
        if (!is_object($objUser)) $objUser = (object)$objUser;
    
    
        
        $objParametreContenuMail = $this->parametre->getByCode("ContenuMailConfirmationInscriptionUser");
        $objParametreContenuPage = $this->parametre->getByCode("ContenuPageConfirmationInscriptionUser");
        $arrDest = array();
        $arrDest[] = array("Email" => $objUser->Email ?? '', "Name" => $objUser->Prenom ?? '' . " " .  $objUser->Nom ?? '');
    
        $colDest[] = array("Email"=>$this->config->item("mail_sender_adress"),"Name"=>$this->config->item("mail_sender_name"));
        $colSujet = "Notification inscription Consommateur";
        $nom__ = $objUser->Nom;
        $Prenom__ = $objUser->Prenom;
        $Email__ = $objUser->Email;
        $colContenu = '<div>
            <p>Bonjour,<br/>Ceci est une notification d\'inscription de nouveau consommateur sur Sortez.org</p>
            <p>Nom: '.$nom__.'<br/>Prenom: '.$Prenom__.'<br/>Email: '.$Email__.'</p>
        </div>';
        
        $txtSujet = "Confirmation inscription";
        $txtContenu = $objParametreContenuMail[0]->Contenu;
        $txtContenu .= $Rappeldevosinformations;
    
        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
        } else $iduser = 0;
    
        if ($verifuserprofil == 0) {
    
            @envoi_notification($arrDest,$txtSujet,$txtContenu);
            @envoi_notification($colDest,$colSujet,$colContenu);
            $data["status"] = "ok";
    
        } else if($verifuserprofil == 2){
    
            $data['status'] = "ko1";
    
        } else {
            $data["status"] = "ok1";
        }    
    }else{
        $data['status'] = "ko2";
    }
    echo json_encode($data);
}

function forgot_password()
    {
            $rest_json = file_get_contents("php://input");
            $email = json_decode($rest_json,true);
            $config_tables = $this->config->item('tables', 'ion_auth');
            $identity = $this->db->where('email', $email['email'])->limit('1')->get($config_tables['users'])->row();
            $forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});
            if ($forgotten) {
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                $data['status'] = "ok";
            } else {
                $data['status'] = "ko";
            }
            echo json_encode($data);
    }

    function getAllRubriques(){

        $allRub = $this->mdlcategorie->GetAll();
        $data['rubriues'] = $allRub;
        echo json_encode($data);

    }

    function getSubRubriqueByIdRubrique(){
        $rest_json = file_get_contents("php://input");
        $post = json_decode($rest_json,true);
        $idRubrique = $post['idRubrique'];
        $allSubRub = $this->mdlcategorie->GetCommercantSouscategorieByRubrique($idRubrique);
        $data['subRubrique'] = $allSubRub;
        echo json_encode($data);
    }

    function getStatus(){
        $data["status"] = $this->mdlstatut->GetAll();
        echo json_encode($data);
    }

    function getDepartement(){
        $allDep = $this->mdldepartement->GetAll();
        $data['department'] = $allDep;
        echo json_encode($data);
    }

    function getDepartByPostalCode(){
        $rest_json = file_get_contents("php://input");
        $post = json_decode($rest_json,true);
        $postalCode = $post['codePostal'];
        $allDep = $this->mdldepartement->GetDepByCodePostal($postalCode);
        $data['department'] = $allDep[0];
        echo json_encode($data);
    }

    function ajouterCommercant()
    {
        $rest_json = file_get_contents("php://input");
        $post = json_decode($rest_json,true);

        $objCommercant = $post["Societe"];
        $societe_Password = $post["Password"]['Societe_Password'];
        $idVille = $this->mdlville->GetVilleByCodePostal_localisation($objCommercant['CodePostal']);
        if(!empty($idVille)){
        $objCommercant['IdVille'] = $idVille[0]->IdVille;
        $objCommercant["IdVille_localisation"]=$objCommercant["IdVille"];
        
        if (!$objCommercant) {
            $data['status'] = "ko";
        }

        $objMessage = "premium";

        $commercant_url_ville = $this->mdlville->getVilleById($objCommercant["IdVille"]);
        $commercant_url_nom_com = RemoveExtendedChars2($objCommercant["NomSociete"]);
        $commercant_url_nom_ville = RemoveExtendedChars2($commercant_url_ville->Nom);
        $commercant_url_nom = $commercant_url_nom_com . "_" . $commercant_url_nom_ville;
        $commercant_url_nom = $this->commercant->setUrlFormat($commercant_url_nom, 0);
        $objCommercant["nom_url"] = $commercant_url_nom;
        $objCommercant["nom_url_vsv"] = $commercant_url_nom;


        $objCommercant["datecreation"] = time();


        if ($objCommercant["IdVille"] == 0) {
            $data['status'] = "koville1";
        }
        $objAssCommercantRubrique = $post["AssCommercantRubrique"];
        $objAssCommercantSousRubrique = $post["AssCommercantSousRubrique"];
        $objAssCommercantAbonnement = 60;


        $username_ion_auth = $objCommercant['Login'];
        $password_ion_auth = $societe_Password;
        $email_ion_auth = $objCommercant['Email'];
        $additional_data_ion_auth = array('first_name' => $objCommercant['NomSociete'], 'company' => $objCommercant['NomSociete'], 'phone' => $objCommercant['TelFixe']);
        $abonnement_verify_ionauth = $this->Abonnement->GetById($objAssCommercantAbonnement["IdAbonnement"]);
        $value_abonnement_ionauth = '4';//sets default group to basic
        if ($abonnement_verify_ionauth->type == "gratuit") $value_abonnement_ionauth = '3';
        if ($abonnement_verify_ionauth->type == "premium") $value_abonnement_ionauth = '4';
        if ($abonnement_verify_ionauth->type == "platinum") $value_abonnement_ionauth = '5';
        $group_ion_auth = array($value_abonnement_ionauth);


        $willll_test = $this->ion_auth->register($username_ion_auth, $password_ion_auth, $email_ion_auth, $additional_data_ion_auth, $group_ion_auth);
        $ion_ath_error = $this->ion_auth->errors();

        $user_lastid_ionauth = $this->ion_auth_used_by_club->GetUserlast_id($objCommercant['NomSociete'], $username_ion_auth, $email_ion_auth);
        $objCommercant["user_ionauth_id"] = $user_lastid_ionauth;
        if (isset($ion_ath_error) && $ion_ath_error != '' && $ion_ath_error != NULL && !$user_lastid_ionauth) {

            $data["status"] = "koIonauth";

        } else {

            $objCommercant["blog"] = 1;
            $objCommercant["agenda"] = 1;
            $objCommercant["bonplan"] = 0;
            $objCommercant["annonce"] = 1;
            $objCommercant["limit_bonplan"] = 5;
            $objCommercant['referencement_annuaire'] = 1;
            $objCommercant['annonce'] = 1;
            $objCommercant['agenda'] = 1;
            $objCommercant['blog'] = 1;
            $objCommercant['bonplan'] = 1;
            $objCommercant['referencement_resto'] = 1;
            $objCommercant['referencement_gite'] = 0;
            $objCommercant['mail_reservation'] = $objCommercant['Email_decideur'];
            $objCommercant['referencement_news'] = 0;
            $objCommercant['IsActifCommentGoogle'] = 0;
            $objCommercant['is_actif_affichBoutique_Linkicon'] = 1;

            $IdInsertedCommercant = $this->commercant->Insert($objCommercant);

            // $data['status'] = $societe_Password ;
            // echo json_encode($data);
            // die();

            $this->load->Model("AssCommercantRubrique");
            $objAssCommercantRubrique["IdCommercant"] = $IdInsertedCommercant;
            $this->AssCommercantRubrique->Insert($objAssCommercantRubrique);

            $this->load->Model("AssCommercantSousRubrique");
            $objAssCommercantSousRubrique["IdCommercant"] = $IdInsertedCommercant;
            $this->AssCommercantSousRubrique->Insert($objAssCommercantSousRubrique);

            $this->load->Model("AssCommercantAbonnement");
            $objAssCommercantAbonnement["IdCommercant"] = $IdInsertedCommercant;
            $this->AssCommercantAbonnement->Insert($objAssCommercantAbonnement);
            
            if (!empty($IdInsertedCommercant)) {

                $demande_abonnement = 60;
                $colAbonnements = $this->Abonnement->GetAll();
                $abonnement_asked = array();

                foreach ($colAbonnements as $objAbonnements) {
                    $array_demande_abonnement = array('id' => NULL, 'idAbonnement' => $objAbonnements->IdAbonnement, 'idCommercant' => $IdInsertedCommercant, 'idUser_ionauth' => $user_lastid_ionauth);
                    $IdAbonnementExist_on_database = $this->mdldemande_abonnement->IdAbonnementExist($objAbonnements->IdAbonnement, $IdInsertedCommercant, $user_lastid_ionauth);
                    $IdAbonnementExist_on_demande = in_array($objAbonnements->IdAbonnement, $abonnement_asked);
                    if ($IdAbonnementExist_on_database && $IdAbonnementExist_on_demande) {
                    } elseif (!$IdAbonnementExist_on_database && $IdAbonnementExist_on_demande) {
                        $insert_dmd_abonnement = $this->mdldemande_abonnement->insert($array_demande_abonnement);
                    } elseif (!$IdAbonnementExist_on_database && !$IdAbonnementExist_on_demande) {
                        $this->mdldemande_abonnement->delete($objAbonnements->IdAbonnement);
                    }
                }


                $colDestAdmin = array();
                $colDestAdmin[] = array("Email" => $this->config->item("mail_sender_adress"), "Name" => $this->config->item("mail_sender_name"));

                $txtSujetAdmin = "Notification inscription nouveau Commercant Sortez";

                $txtContenuAdmin = "
                    <p>Bonjour ,</p>
                    <p>Un commer&ccedil;ant vient de cr&eacute;er un compte premium gratuit chez Sortez.org</p><p>";

                $txtContenuAdmin .= "Login : " . $objCommercant['Login'] . "<br/>";
                $txtContenuAdmin .= "Email : " . $objCommercant['Email'] . "<br/>";
                $txtContenuAdmin .= "Nom : " . $objCommercant['NomSociete'] . "<br/>";

                if (isset($objMessage['basic'])) $txtContenuAdmin .= "Demande Abonnement Basic Gratuit<br/>";
                if (isset($objMessage['premium'])) $txtContenuAdmin .= "Demande Abonnement Premium : 250€<br/>";
                if (isset($objMessage['platinium'])) $txtContenuAdmin .= "Demande Abonnement Platinium : 400€<br/>";
                if (isset($objMessage['agenda_gratuit'])) $txtContenuAdmin .= "Demande Abonnement Agenda Gratuit<br/>";
                if (isset($objMessage['agenda_plus'])) $txtContenuAdmin .= "Demande Abonnement Agenda Plus : 250€<br/>";
                if (isset($objMessage['web_ref1'])) $txtContenuAdmin .= "Demande Abonnement Module web & référencement 1 an : 250€<br/>";
                if (isset($objMessage['web_ref_n'])) $txtContenuAdmin .= "Demande Abonnement Module web autres années : 100€<br/>";
                if (isset($objMessage['restauration'])) $txtContenuAdmin .= "Demande Abonnement Réstauration : 100€<br/>";




                $txtContenuAdmin .= "</p><p>Lien validation Commercant : ";
                $txtContenuAdmin .= '<a href="' . site_url('admin/commercants/fiche/' . $IdInsertedCommercant) . '">' . $objCommercant['NomSociete'] . '</a>';
                $txtContenuAdmin .= "</p>";

                $txtContenuAdmin .= "<p>Cordialement,</p>";

                $admin_check_mail_confirm = envoi_notification($colDestAdmin, $txtSujetAdmin, $txtContenuAdmin);



                $colDest = array();
                $email = $objCommercant["Login"];
                $colDest[] = array("Email" => $email, "Name" => $objCommercant['NomSociete']);
                $txtSujet = "Confirmation Inscription";


                $objParametreContenuMail = $this->parametre->getByCode("ContenuMailConfirmationInscriptionPro");
                $objParametreContenuPage = $this->parametre->getByCode("ContenuPageConfirmationInscriptionUser");

                $txtContenu = $objParametreContenuMail[0]->Contenu;
                $data["status"] = "ok";
            } else {
                $data["status"] = "ko";
            }
        }
        }else{
            $data['status'] = "koville";
        }

        echo json_encode($data);
    }
}