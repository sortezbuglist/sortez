<?php 

class Auto_Rss_Cron extends CI_Controller{

    public $success = 0;
    public $error = 0;

	function __construct(){
		parent::__construct();

		$this->output->set_header('Access-Control-Allow-Origin: null');  header('Access-Control-Allow-Credentials: omit');
        header('Access-Control-Allow-Method: PUT, GET, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');

        $this->load->model('xml_models');
        $this->load->model('Mdlarticle');
        $this->load->model('mdlville');
        $this->load->model('mdl_agenda');
        $this->load->model('mdl_agenda_datetime');
        $this->load->model('mdl_categories_agenda');
        $this->load->model('Mdl_rss_cache');
	}

    function index(){
        $this->rss_recovery();
    }

    // to get the last link done and the last data done
    function get_rss_cache(){
        $cache = $this->Mdl_rss_cache->getCache();
        if(count($cache) < 1){
            return null;
        }else{
            $state['lastLink'] = (int)$cache->lastLink;
            $state['lastDataId'] = (int)$cache->lastDataId;
            $state['cache'] = (int)$cache->cache;
            return $state;
        }
        
    }

    // to launch the rss recovery
    function rss_recovery(){
        $state = $this->get_rss_cache();
        if($state == null){
            $state['lastLink'] = 0;
            $state['lastDataId'] = 0;
            $state['cache'] = 0;
        }else{
            $state['lastLink'] = $state['lastLink'];
            $state['lastDataId'] = $state['lastDataId'];
            $state['cache'] = $state['cache'];
        }
        $state['counter'] = 0;
        $this->Import_RSS_By_Cron($state);
    }

    // to import all rss link
    function Import_RSS_By_Cron($state){
        $allink = $this->get_all_link();
        $state = $state;
        $state['allink'] = $allink;
        $this->Import_auto($state);
    }

    // to import each link
    function Import_auto($state){
        $lastLink = $state['lastLink'];
        $allink = $state['allink'];
        $data = $state;
        $data['ids'] = $allink[$lastLink]->id;
        $data['idVille'] = $allink[$lastLink]->IdVille;
        $data['source'] = $allink[$lastLink]->source;
        $data['lastLink'] = $lastLink;
        $data['lastDataId'] = $state['lastDataId'];
        $data['linkLength'] = count($allink);
        $this->get_all($data);
    }

    // to gel all data in one link
    function get_all($state){
        try{
            $datas = $this->count_data($state['ids']);
            if($datas != 'ko'){
                $end = (int)$datas - 1;
                if($state['lastDataId'] < $end){
                    if($state['counter'] == 500){
                        $this->Mdl_rss_cache->deleteCache();
                        $cache = array(
                            'lastLink' => $state['lastLink'],
                            'lastDataId' => $state['lastDataId'],
                            'cache' => $state['cache']
                        );
                        $this->Mdl_rss_cache->setCache($cache);
                        // stop script when counter get 10
                        $msg = 'Importation partielle';

                        $this->notify($msg, $this->success, $this->error);
                        exit('Importation partielle terminée pour link: ('.$state['lastLink'].'), data: ('.$state['lastDataId'].')');
                    }else{
                        try{
                            $this->save_data($state);
                            $state['lastDataId'] += 1;
                            $state['counter'] += 1;
                            if($state['cache'] == 0){
                                $state['cache'] = 1;
                            }
                            $this->get_all($state);
                        }catch(Exception $e){
                            $this->error += 1;
                            $state['lastDataId'] += 1;
                            $state['counter'] += 1;
                            if($state['cache'] == 0){
                                $state['cache'] = 1;
                            }
                            $this->get_all($state);
                        }
                    }
                }else{
                    $state['lastLink'] += 1;
                    if($state['lastLink'] < count($state['allink'])){
                        $state['lastDataId'] = 0;
                        $this->Import_auto($state);
                    }else{
                        $cache = array(
                            'lastLink' => $state['lastLink'],
                            'lastDataId' => $state['lastDataId'],
                            'cache' => $state['cache']
                        );
                        $this->Mdl_rss_cache->deleteCache();
                        $count_data = $this->Mdl_rss_cache->count_article_data();
                        $msg = 'Importation totale';
                        $this->notify($msg, $this->success, $this->error);
                        exit('Importation de tous les données terminée, '.$count_data.' articles dans la base');
                    }
                }
            }
        }catch(Exception $e){
            $this->get_all($state);
        }
    }

    // to save into table article
    function save_data($state){
        $final="";
        $image="";
        $file="";
        $contents='';
        $normal='';
        $id=(int)$state['lastDataId'];
        $idlink = $state['ids'];
        $lien = $state['source'];
        $villeid=$state['idVille'];
        $cache=$state['cache'];
        $aboutlink = $this->xml_models->getlinkbyid($idlink);
        // start update link object on database
        $link_current_date_update = (array)$aboutlink;
        $link_current_date_update['getted'] = 1;
        $link_current_date_update['current_date_getted'] = date("Y-m-d");
        $link_current_date_update_result = $this->xml_models->update_current_date($link_current_date_update);
        // end update link object on database
        $categ = $aboutlink->agenda_categid;
        if ($lien != "" AND $categ != "" AND $lien != null  AND $categ != null) {
            $this->load->model('xml_models');
            $this->load->helper('clubproximite');
            if ($cache == 0 ){
                $this->delete_cache();
            }

            $xml = simplexml_load_file($lien);
            if (isset($xml->channel->item)) {
                $is = (string)$xml->channel->item[$id]->description;
                $exploded = explode(';', htmlspecialchars($is));

                $results = "";

                foreach ($exploded as $val) {

                    if (preg_match("/\jpg\b/i", $val)) {
                        $results = $val;
                    }
                }
                //var_dump($results);
                if ($results != "") {
                    $finals = htmlentities($results, ENT_QUOTES);
                    $normal = str_replace("&amp;quot", '', $finals);
                } else {
                    if (isset($xml->channel->item[$id]->enclosure)) {
                        $enclosure = $xml->channel->item[$id]->enclosure['url'];
                        // var_dump($enclosure);
                    }

                }
                if (isset($normal) AND $normal != null AND $normal != "") {
                    $final = $normal;
                } else {
                    if (isset($enclosure) AND $enclosure != null AND $enclosure != "") {
                        //$final=$enclosure;
                        $test = explode("?", $enclosure);
                        $utile = $test[0];
                        $finals = str_replace(" ", '', $utile);
                        $final = $finals;
                    }
                }


                if (isset($final) && $final != null AND $final !='') {
                    $info = pathinfo($final);
                    $contents = file_get_contents($final);
                    $_zExtension = strrchr($info['basename'], '.');
                    $_zFilename = random_string('unique', 10) . $_zExtension;
                    $file = 'application/resources/front/photoCommercant/imagesbank/rss_image/' . $_zFilename;
                    $dir='application/resources/front/photoCommercant/imagesbank/rss_image/';
                    if (is_dir($dir) == true){
                        // var_dump($file);die();
                        @file_put_contents($file, $contents);
                        //$uploaded_file = new UploadedFile($file, $_zFilename);
                        if (isset($_zFilename)) $photo = $_zFilename;// save image name on agenda table
                        if ($file != "") {
                            if (is_file($file)) {

                                $base_path_system = str_replace('system/', '', BASEPATH);
                                $image_path_resize_home = $base_path_system . "/" . $file;
                                $image_path_resize_home_final = $base_path_system . "/" . $file;
                                $this_imgmoo =& get_instance();
                                $this_imgmoo->load->library('image_moo');
                                $this_imgmoo->image_moo
                                    ->load($image_path_resize_home)
                                    ->resize_crop(640, 480, false)
                                    ->save($image_path_resize_home_final, true)
                                    ->clear_temp()
                                    ->clear();
                            }
                        }
                    }else{
                        mkdir ($dir, 0777);
                        @file_put_contents($file, $contents);
                        //$uploaded_file = new UploadedFile($file, $_zFilename);
                        if (isset($_zFilename)) $photo = $_zFilename;// save image name on agenda table
                        if ($file != "") {
                            if (is_file($file)) {
                                $base_path_system = str_replace('system/', '', BASEPATH);
                                $image_path_resize_home = $base_path_system . "/" . $file;
                                $image_path_resize_home_final = $base_path_system . "/" . $file;
                                $this_imgmoo =& get_instance();
                                $this_imgmoo->load->library('image_moo');
                                $this_imgmoo->image_moo
                                    ->load($image_path_resize_home)
                                    ->resize_crop(640, 480, false)
                                    ->save($image_path_resize_home_final, true)
                                    ->clear_temp()
                                    ->clear();
                            }
                        }
                    }
                }else{
                    $photo=null;
                }

                if (!isset($photo) OR $photo == "") {
                    $photo = null;
                }
/////////////////////////formater date debut et date fin/////////////////////////////
                if (isset($xml->channel->item[$id]->pubDate)) {
                    $pubdate = $xml->channel->item[$id]->pubDate;
                    $testdateformat = explode("-", $pubdate);
                    if (isset($testdateformat[2])) {
                        $day = explode(" ", $testdateformat[2]);
                        $date_pub = $testdateformat[0] . "-" . $testdateformat[1] . "-" . $day[0];
                    } else {
                        $this->load->helper("clubproximite_helper");
                        $datebrutr = $xml->channel->item[$id]->pubDate;
                        $date = convert_xml_date_to_sql_date($datebrutr);
                        $date_pub = $date;
                    }
                } else {
                    $date_pub = null;
                }
                if (isset($villeid) AND $villeid !='' AND $villeid!=null AND $villeid!=null){
                    $ville_info=$this->mdlville->getVilleById($villeid);
                }
////////////////////////////////////////////////////////////////////////////////////
                $date_depot = date("y-m-d");
                $fieldc = array('nom_manifestation' => (string)$xml->channel->item[$id]->title,
                    'siteweb' => (string)$xml->channel->item[$id]->link,
                    'description' => (string)$xml->channel->item[$id]->description,
                    'date_depot' => $date_depot,
                    "article_categid" => $categ,
                    "IsActif" => 1,
                    "IdCommercant" => 301299,
                    "IdUsers_ionauth" => 1992,
                    "photo1" => $photo,
                    "date_debut" => $date_pub,
                    "date_fin" => $date_pub,
                    "IdVille_localisation"=>$ville_info->IdVille ?? 0,
                    "codepostal_localisation" =>$ville_info->CodePostal,
                );
                $article['nom_manifestation'] = (string)$xml->channel->item[$id]->title;
                $test = $this->Mdl_rss_cache->checkData($article);
                if($test == 1 AND (string)$xml->channel->item[$id]->title != ""){
                    $this->success += 1;
                    $state['success'] = $this->success;
                    $this->xml_models->save_to_true_sortez($fieldc);
                }              
            }
        }
    }

    // to get all rss link
    function get_all_link(){
        $allink = $this->xml_models->getlink();
        return $allink;
    }
    
    // delete cache about rss link
    public function delete_cache(){
        $this->Mdl_rss_cache->deleteCache();
    }

    // to count data in one rss link
    public function count_data($ids){
        $lien_id=$ids;
        $lien=$this->xml_models->getlink_by_id($lien_id)->source;
        $xml = simplexml_load_file($lien);
        if (isset($xml->channel->item)) {
            return count($xml->channel->item);
        }else{
            return 'ko';
        }
    } 

    public function notify($msg, $success, $error){
        echo $success.'</br>';
        echo $error.'</br>';
        $this->load->helper('clubproximite');
        $ip = $this->getUserIpAddr();
        $timezone  = +2; //(GMT -5:00) EST (U.S. & Canada) 
        $date = gmdate("Y/m/j H:i:s", time() + 3600*($timezone+date("I")));
        if($msg == 'Importation partielle'){
            $mailtosend = '
                The rss was done by this ip: '.$ip.
                '</br>
                At: '.$date.
                '</br>
                This recovery is just a part of all data, Please relaunch the script </br>
                Data recovered from this part is: '.$success.
                '</br>
                Data error from this part is: '.$error.
                '</br>
                Thanks
            ';
        }else{
            $mailtosend = '
                <p>The rss was done by this ip: '.$ip.'</p>
                <p>At: '.$date.'</p>
                <p>All recovery are done </p>
                <p>Data recovered from this part is: '.$success.'</p>
                <p>Data error from this part is: '.$error.'</p>
                <p>Thanks</p>
            ';
        }
        $destinataire[0]['Email'] = 'seirankurimi@gmail.com';
        $destinataire[0]['Name'] = 'Lambda Dev';
        $destinataire2[0]['Email'] = 'buglist@sortez.org';
        $destinataire2[0]['Name'] = 'Buglist Sortez';
        $prmSujet = "Notification Rss Recovery";
        $prmEnvoyeur = "contact@sortez.org";
        $prmEnvoyeurName = "Sortez";
        try{
            envoi_notification($destinataire, $prmSujet, $mailtosend, $prmEnvoyeur, $prmEnvoyeurName);
            envoi_notification($destinataire2, $prmSujet, $mailtosend, $prmEnvoyeur, $prmEnvoyeurName);
            echo 'Notification envoyé à '.$destinataire[0]['Email'];
        }catch(Exception $e){
            echo $e;
        }
    }

    function getUserIpAddr(){
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

}