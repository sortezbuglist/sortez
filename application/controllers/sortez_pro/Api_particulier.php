<?php
class Api_particulier extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model ( "mdl_card" );
        $this->load->model ( "mdl_card_bonplan_used" );
        $this->load->model ( "mdl_card_capital" );
        $this->load->model ( "mdl_card_tampon" );
        $this->load->model ( "mdl_card_remise" );
        $this->load->model ( "mdl_card_user_link" );
        $this->load->model ( "mdl_card_fiche_client_tampon" );
        $this->load->model ( "mdl_card_fiche_client_capital" );
        $this->load->model ( "mdl_card_fiche_client_remise" );
        $this->load->model ( 'assoc_client_bonplan_model' );
        $this->load->model ( 'assoc_client_plat_model' );
        $this->load->model ( 'assoc_client_commercant_model' );
        $this->load->Model ( "mdlville" );
        $this->load->Model ( "Assoc_client_table_model" );
        $this->load->model ( "user" );
        $this->load->model ( "mdlbonplan" );
        $this->load->model ( "commercant" );
        $this->load->model ( "Mdlcommercant" );
        $this->load->model("Mdl_prospect");

        $this->load->model ( "mdl_card_tampon" );
        $this->load->model ( "mdl_card_capital" );
        $this->load->model ( "mdl_card_remise" );

        $this->load->library ( 'session' );

        $this->load->library ( 'ion_auth' );
        $this->load->model ( "ion_auth_used_by_club" );
        $this->load->library('ion_auth');
        $this->load->model("Api_model_particulier");
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper('url');
    }

    public function tester(){
        echo "mande";
    }

    public function get_user_info_by_username(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $login=$_POST["login"];
        $user_info = $this->Api_model_particulier->get_user_info_by_username($login);
        $data['user'] = $user_info;
        echo json_encode($data);
    }

    public function get_user_info_by_username_direct($login){
        $user_info = $this->Api_model_particulier->get_user_info_by_username($login);
        return $user_info;
    }

    public function get_fidelity_user(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $login=$_POST["login"];
        $infouser = $this->get_user_info_by_username_direct($login);
        $id_user = $this->ion_auth_used_by_club->get_user_id_from_ion_id($infouser->id_ionauth_user);
        $data['titre'] = "Ma consommation fidelité";
        $data['fiche_capital'] = $this->mdl_card_fiche_client_capital->getByCritere(array('id_user'=>$id_user));
        $data['fiche_tampon'] = $this->mdl_card_fiche_client_tampon->getByCritere(array('id_user'=>$id_user));
        $data['fiche_remise'] = $this->mdl_card_fiche_client_remise->getByCritere(array('id_user'=>$id_user));
        $data['id_user'] = $id_user;
        echo json_encode($data);
    }

    public function get_command_user(){

        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $login=$_POST["login"];
        $infouser = $this->get_user_info_by_username_direct($login);
        $id_user = $this->ion_auth_used_by_club->get_user_id_from_ion_id($infouser->id_ionauth_user);
        $commande = $this->Api_model_particulier->get_commande_list_by_iduser($id_user);
        if(empty($commande)){
            $data['commande'] = "null";
        }else{
            $data['commande'] = $commande;
        }
        echo json_encode($data);

    }
    public function get_bonplan_user(){
    	$rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $login=$_POST["login"];
        $infouser = $this->get_user_info_by_username_direct($login);
        $id_user = $this->ion_auth_used_by_club->get_user_id_from_ion_id($infouser->id_ionauth_user);
        // $id_user = '953''957';
 
        $criteres['valide'] = 0;
        $criteres['id_user'] = $id_user;
        
        $resa = $this->assoc_client_bonplan_model->get_where(array('assoc_client_bonplan.id_client'=>$id_user,'assoc_client_bonplan.valide'=>0));
        $resactif = $this->assoc_client_bonplan_model->get_where(array('assoc_client_bonplan.id_client'=>$id_user,'assoc_client_bonplan.valide'=>1));
       // echo $this->db->last_query();
        //print_r($resa);
       // die();
       // $data['reservations_attente_validation'] = $this->assoc_client_bonplan_model->get_list_by_criteria($criteres);
       // $data['reservations_attente_validation'] = array();
		//$data['reservations_attente_validation'] = $this->mdl_card->get_reservation_by_user_id($id_user,1);
		//echo $this->db->last_query();
        // $list_res_plat=$this->assoc_client_plat_model->get_res_plat_user_by_iduser($id_user);
        // $data['list_plat']=$list_res_plat;
		$data['titre'] = 'Mes reservations en cours';
		$data['bonplan'] = $resa;
		$data['archives'] = $resactif;
        $data['id_user'] = $id_user;
        echo json_encode($data);
    }

    public function get_detail_command(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_commande=$_POST["id_commande"];
        $commande = $this->Api_model_particulier->get_commande_details($id_commande);
        if(empty($commande)){
            $data['commande'] = "null";
        }else{
            $data['commande'] = $commande;
        }
        echo json_encode($data);
    }
    public function update_user_info(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_user = $_POST['id_user'];
        $nom=$_POST["nom"];
        $prenom=$_POST["prenom"];
        $date=$_POST["date"];
        $adresse=$_POST["adresse"];
        $postal=$_POST["postal"];
        $tel=$_POST["tel"];
        $email=$_POST["email"];

        $ville = $this->mdlville->GetVilleByCodePostal_localisation_res($postal);

        $ville_id = $ville[0]->IdVille;

        $field = array(
            'Nom' => $nom,
            "Prenom" => $prenom,
            "DateNaissance" => $date,
            "Adresse" => $adresse,
            "CodePostal" => $postal,
            "Portable" => $tel,
            "Email" => $email,
            "IdVille" => $ville_id
        );

        $this->Api_model_particulier->update_user($field,$id_user);
        $data['status'] = "ok";
        echo json_encode($data);
        
    }

}