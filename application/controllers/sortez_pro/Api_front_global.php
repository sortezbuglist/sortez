<?php
class Api_front_global extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model ( "mdl_card" );
        $this->load->model ( "mdl_card_bonplan_used" );
        $this->load->model ( "mdl_card_capital" );
        $this->load->model ( "mdl_card_tampon" );
        $this->load->model ( "mdl_card_remise" );
        $this->load->model ( "mdl_card_user_link" );
        $this->load->model ( "mdl_card_fiche_client_tampon" );
        $this->load->model ( "mdl_card_fiche_client_capital" );
        $this->load->model ( "mdl_card_fiche_client_remise" );
        $this->load->model ( 'assoc_client_bonplan_model' );
        $this->load->model ( 'assoc_client_plat_model' );
        $this->load->model ( 'assoc_client_commercant_model' );
        $this->load->Model ( "mdlville" );
        $this->load->Model ( "Assoc_client_table_model" );
        $this->load->model ( "user" );
        $this->load->model ( "mdlbonplan" );
        $this->load->model ( "commercant" );
        $this->load->model ( "Mdlcommercant" );
        $this->load->model ( "mdlcommercant" );
        $this->load->model ( "mdlcadeau" );
        $this->load->model ( "mdlannonce" );
        $this->load->model("Mdl_prospect");
        $this->load->model("mdl_agenda");
        $this->load->model("mdlarticle");
        $this->load->model("mdlcommercantpagination");
        $this->load->model("mdlcategorie");
        $this->load->model("mdlfidelity");
        $this->load->model("mdl_article_datetime");
        $this->load->model("mdl_article_organiser");
        $this->load->model("mdl_agenda_datetime");

        $this->load->model ( "mdl_card_tampon" );
        $this->load->model ( "mdl_card_capital" );
        $this->load->model ( "mdl_card_remise" );
        $this->load->model ( "mdl_api_front_global" );

        $this->load->library ( 'session' );

        $this->load->library ( 'ion_auth' );
        $this->load->model ( "ion_auth_used_by_club" );
        $this->load->library('ion_auth');
        $this->load->model("Api_model_particulier");
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper('url');
    }

    public function get_communes(){
        $toVille = $this->mdlville->GetAgendaVilles_pvc_by_varest();
        $data['toVille'] = $toVille;
        echo json_encode($data);
    }

    public function get_communesArticle(){
        $toVille = $this->mdlville->GetArticleVillesByIdCommercant_by_departement();
        $data['toVille'] = $toVille;
        echo json_encode($data);
    }

    public function getCommercantsAgenda(){
        $_iCategorieId = 0;
        $_iVilleId = 0;
        $_iDepartementId = 0;
        $_zMotCle = "";
        $_iSousCategorieId = 0;
        $iOrderBy = "";
        $inputQuand = "0";
        $inputDatedebut = "0000-00-00";
        $inputDatefin = "0000-00-00";
        $inputIdCommercant = "0";
        $data['tocommercant'] = $this->mdl_agenda->listeAgendaRecherche_commercant_list($_iCategorieId, $_iVilleId, $_iDepartementId, $_zMotCle, $_iSousCategorieId, 0, 10000, $iOrderBy, $inputQuand, $inputDatedebut, $inputDatefin, $inputIdCommercant);
        echo json_encode($data);
    }

    public function getCommercantsArticle(){
        $data['tocommercant'] = $this->mdlarticle->listeArticleRecherche_commercant_list();
        echo json_encode($data);
    }
    

    public function getCategorieAgenda(){
        $toCategorie_principale = $this->mdl_agenda->GetAgendaCategorie();
        $data['toCategorie_principale'] = $toCategorie_principale;
        echo json_encode($data);
    }

    public function getCategorieArticle(){
        $toCategorie_principale = $this->mdlarticle->GetArticleCategorie();
        $data['toCategorie_principale'] = $toCategorie_principale;
        echo json_encode($data);
    }

    public function getAgendasListe($nbPage = 1){
        $_iCategorieId = 0;
        $_iVilleId = 0;
        $_iDepartementId = 0;
        $_zMotCle = "";
        $_iSousCategorieId = 0;
        $_limitstart = 0;
        $_limitend = $nbPage*20;
        $iOrderBy = "";
        $inputQuand = "0";
        $inputDatedebut = "0000-00-00";
        $inputDatefin = "0000-00-00";
        $inputIdCommercant = "0";
        $allag = [];
        $allagenda = $this->mdl_agenda->listeAgendaRecherche($_iCategorieId, $_iVilleId, $_iDepartementId, $_zMotCle, $_iSousCategorieId, $_limitstart, $_limitend, $iOrderBy, $inputQuand, $inputDatedebut, $inputDatefin, $inputIdCommercant);
        foreach($allagenda as $ag){
            $oInfoCommercant = $this->Mdlcommercant->infoCommercant($ag->IdCommercant);
            $image = "application/resources/front/photoCommercant/imagesbank/".$ag->IdUsers_ionauth."/".$ag->photo1;
            $image_thumbs = "application/resources/front/photoCommercant/imagesbank/".$ag->IdUsers_ionauth."/thumbs/thumb_".$ag->photo1;
            if(is_file($image_thumbs)){
                $imageag = "https://www.sortez.org/".$image_thumbs;
            }elseif(is_file($image)){
                $imageag = "https://www.sortez.org/".$image;
            }elseif(is_file("application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/".$oInfoCommercant->Photo1)){
                $imageag = "https://www.sortez.org/application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/".$oInfoCommercant->Photo1;
            }else{
                $imageag = GetImagePath("front/") . "/wp71b211d2_06.png";
            }
            $ag->photo1 = $imageag;
            array_push($allag,$ag);
        }
        $data['toAgenda']= $allag;
        echo json_encode($data);
    }
    public function getArticlesListe($page=1){
        $_iCategorieId = 0;
        $_iVilleId = 0;
        $_iDepartementId = 0;
        $_zMotCle = "";
        $_iSousCategorieId = 0;
        $_limitstart = 0;
        $_limitend = ($page*20);
        $iOrderBy = "";
        $inputQuand = "0";
        $inputDatedebut = "0000-00-00";
        $inputDatefin = "0000-00-00";
        $inputIdCommercant = "0";
        $agenda_article_type_id=0;
        $allag = [];
        $allagenda = $this->mdlarticle->listeArticleRecherche($_iCategorieId, $_iVilleId, $_iDepartementId, $_zMotCle, $_iSousCategorieId, $_limitstart, $_limitend, $iOrderBy, $inputQuand, $inputDatedebut, $inputDatefin, $inputIdCommercant,$agenda_article_type_id);
        foreach($allagenda as $ag){
            $image = "application/resources/front/photoCommercant/imagesbank/".$ag->IdUsers_ionauth."/".$ag->photo1;
            $image2 = "application/resources/front/photoCommercant/imagesbank/rss_image/".$ag->photo1;
            $image_thumbs = "application/resources/front/photoCommercant/imagesbank/".$ag->IdUsers_ionauth."/thumbs/thumb_".$ag->photo1;
            if(is_file($image_thumbs)){
                $imageag = "https://www.sortez.org/".$image_thumbs;
            }elseif(is_file($image)){
                $imageag = "https://www.sortez.org/".$image;
            }elseif(is_file($image2)){
                $imageag = "https://www.sortez.org/".$image2;
            }else{
                $imageag = GetImagePath("front/") . "/wp71b211d2_06.png";
            }
            $ag->photo1 = $imageag;
            array_push($allag,$ag);
        }
        $data['toAgenda'] = $allag;
        echo json_encode($data);
    }

    public function filterAgenda($nbPage = 1){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);

        $commune=$_POST["commune"];
        $commercant=$_POST["commercant"];
        $categorie=$_POST["categorie"];
        $date_debut=$_POST["date_debut"];
        $date_fin=$_POST["date_fin"];
        $motcle=$_POST["motcles"];

        $_iCategorieId['0'] = $categorie;
        $_iVilleId = $commune;
        $_iDepartementId = 0;
        $_zMotCle = $motcle;
        $_iSousCategorieId = 0;
        $_limitstart = 0;
        $_limitend = $nbPage*20;
        $iOrderBy = "";
        $inputQuand = "0";
        $inputDatedebut = $date_debut;
        $inputDatefin = $date_fin;
        $inputIdCommercant = $commercant;
        $allag = [];
        $allagenda=$this->mdl_agenda->listeAgendaRecherche($_iCategorieId, $_iVilleId, $_iDepartementId, $_zMotCle, $_iSousCategorieId, $_limitstart, $_limitend, $iOrderBy, $inputQuand, $inputDatedebut, $inputDatefin, $inputIdCommercant);
        
        foreach($allagenda as $ag){
            $oInfoCommercant = $this->Mdlcommercant->infoCommercant($ag->IdCommercant);
            $image = "application/resources/front/photoCommercant/imagesbank/".$ag->IdUsers_ionauth."/".$ag->photo1;
            $image_thumbs = "application/resources/front/photoCommercant/imagesbank/".$ag->IdUsers_ionauth."/thumbs/thumb_".$ag->photo1;
            if(is_file($image_thumbs)){
                $imageag = "https://www.sortez.org/".$image_thumbs;
            }elseif(is_file($image)){
                $imageag = "https://www.sortez.org/".$image;
            }elseif(is_file("application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/".$oInfoCommercant->Photo1)){
                $imageag = "https://www.sortez.org/application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/".$oInfoCommercant->Photo1;
            }else{
                $imageag = GetImagePath("front/") . "/wp71b211d2_06.png";
            }
            $ag->photo1 = $imageag;
            array_push($allag,$ag);
        }
        $data['toAgenda'] = $allag;
        echo json_encode($data);

    }
    public function filterArticle($nbPage = 1){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);

        $commune=$_POST["commune"];
        $commercant=$_POST["commercant"];
        $categorie=$_POST["categorie"];
        $date_debut=$_POST["date_debut"];
        $date_fin=$_POST["date_fin"];
        $motcle=$_POST["motcles"];

        $_iCategorieId = $categorie;
        $_iVilleId = $commune;
        $_iDepartementId = 0;
        $_zMotCle = $motcle;
        $_iSousCategorieId = 0;
        $_limitstart = 0;
        $_limitend = ($nbPage*20);
        $iOrderBy = "";
        $inputQuand = "0";
        $inputDatedebut = $date_debut;
        $inputDatefin = $date_fin;
        $inputIdCommercant = $commercant;
        $agenda_article_type_id=0;
        $allag = [];
        $allagenda = $this->mdlarticle->listeArticleRecherche($_iCategorieId, $_iVilleId, $_iDepartementId, $_zMotCle, $_iSousCategorieId, $_limitstart, $_limitend, $iOrderBy, $inputQuand, $inputDatedebut, $inputDatefin, $inputIdCommercant,$agenda_article_type_id);
        foreach($allagenda as $ag){
            $image = "application/resources/front/photoCommercant/imagesbank/".$ag->IdUsers_ionauth."/".$ag->photo1;
            $image2 = "application/resources/front/photoCommercant/imagesbank/rss_image/".$ag->photo1;
            $image_thumbs = "application/resources/front/photoCommercant/imagesbank/".$ag->IdUsers_ionauth."/thumbs/thumb_".$ag->photo1;
            if(is_file($image_thumbs)){
                $imageag = "https://www.sortez.org/".$image_thumbs;
            }elseif(is_file($image)){
                $imageag = "https://www.sortez.org/".$image;
            }elseif(is_file($image2)){
                $imageag = "https://www.sortez.org/".$image2;
            }else{
                $imageag = GetImagePath("front/") . "/wp71b211d2_06.png";
            }
            $ag->photo1 = $imageag;
            array_push($allag,$ag);
        }
        $data['toAgenda'] = $allag;
        echo json_encode($data);
    }
    public function getAnnonceListe($nbPage=1){
        $_iCategorieId = 0;
        $_iVilleId = 0;
        $_iDepartementId = 0;
        $_zMotCle = "";
        $_Etat = "";
        $_limitstart = 0;
        $_limitend = $nbPage*20;
        $iOrderBy = "";
        $inputIdCommercant = "0";
        $inputfromgeolocalisation = "0";
        $inputfromgeolocalisationvalue = "0";
        $inputfromgeolocalisationlatitude = "0";
        $inputfromgeolocalisationlongitude = "0";

        $data_annonce = [];

        $count = count($this->mdlannonce->listeAnnonceRecherche($_iCategorieId, $inputIdCommercant, $_zMotCle, $_Etat, $_limitstart, $_limitend, $_iVilleId, $_iDepartementId, $iOrderBy, $inputfromgeolocalisation, $inputfromgeolocalisationvalue, $inputfromgeolocalisationlatitude, $inputfromgeolocalisationlongitude));
        $get_all_datas = $this->mdlannonce->listeAnnonceRecherche($_iCategorieId, $inputIdCommercant, $_zMotCle, $_Etat, $_limitstart, $_limitend, $_iVilleId, $_iDepartementId, $iOrderBy, $inputfromgeolocalisation, $inputfromgeolocalisationvalue, $inputfromgeolocalisationlatitude, $inputfromgeolocalisationlongitude);
        foreach ($get_all_datas as $get_all_data){
            $oInfoCommercant = $this->Mdlcommercant->infoCommercant($get_all_data->IdCommercant);
            if ($get_all_data->etat == 1) $get_all_data->etat = "Neuf";
            else if ($get_all_data->etat == 0) $get_all_data->etat = "Revente";
            else if ($get_all_data->etat == 2) $get_all_data->etat = "Service";
            else $get_all_data->etat = "";

            if($get_all_data->prix_vente){
                if (strpos($get_all_data->prix_vente, '€') == false) {
                    $get_all_data->prixtoview =  $get_all_data->prix_vente.'€';
                }else{
                    $get_all_data->prixtoview =  $get_all_data->prix_vente;
                }
            }elseif($get_all_data->prix_ancien){
                if (strpos($get_all_data->prix_ancien, '€') == false) {
                    $get_all_data->prixtoview =  $get_all_data->prix_ancien.'€';
                }else{
                    $get_all_data->prixtoview =  $get_all_data->prix_ancien;
                }
            }

            $image_thumbs = "application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/thumbs/thumb_".$get_all_data->annonce_photo1->photo1;
            if(is_file($image_thumbs)){
                $get_all_data->photo_link = "https://www.sortez.org/".$image_thumbs;
            }elseif(is_file('application/resources/front/photoCommercant/imagesbank/'.$oInfoCommercant->user_ionauth_id.'/'.$get_all_data->annonce_photo1)){
                $get_all_data->photo_link = 'application/resources/front/photoCommercant/imagesbank/'.$oInfoCommercant->user_ionauth_id.'/'.$get_all_data->annonce_photo1;
            }else{
                $get_all_data->photo_link = 'application/resources/front/images/no_image_boutique.png';
            }
            $get_all_data->user_ion_auth = $oInfoCommercant->user_ionauth_id;
            $get_all_data->nom_url = $oInfoCommercant->nom_url;
            $get_all_data->count = $count.' annonces déposées';
            array_push($data_annonce,$get_all_data);
        }
        $data['toAnnonce']= $data_annonce;
        echo json_encode($data);
    }
    public function filterAnnonce($nbPage = 1){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);

        $commune=$_POST["commune"];
        $commercant=$_POST["commercant"];
        $categorie=$_POST["categorie"];
        $souscategorie=$_POST["souscategorie"];
        $motcle=$_POST["motcles"];

        $_iCategorieId[0] = $categorie;
        $_iVilleId = $commune;
        $_iDepartementId = 0;
        $_zMotCle = $motcle;
        $_iSousCategorieId =$souscategorie;
        $_limitstart = 0;
        $_limitend = $nbPage*20;
        $iOrderBy = "";
        $inputIdCommercant = $commercant;
        $_Etat = "";
        $inputfromgeolocalisation = "0";
        $inputfromgeolocalisationvalue = "0";
        $inputfromgeolocalisationlatitude = "0";
        $inputfromgeolocalisationlongitude = "0";
        $data_annonce = [];
        $get_all_datas = $this->mdlannonce->listeAnnonceRecherche($_iCategorieId, $inputIdCommercant, $_zMotCle, $_Etat, $_limitstart, $_limitend, $_iVilleId, $_iDepartementId, $iOrderBy, $inputfromgeolocalisation, $inputfromgeolocalisationvalue, $inputfromgeolocalisationlatitude, $inputfromgeolocalisationlongitude);
        foreach ($get_all_datas as $get_all_data){
            $oInfoCommercant = $this->Mdlcommercant->infoCommercant($get_all_data->IdCommercant);
            if ($get_all_data->etat == 1) $get_all_data->etat = "Neuf";
            else if ($get_all_data->etat == 0) $get_all_data->etat = "Revente";
            else if ($get_all_data->etat == 2) $get_all_data->etat = "Service";
            else $get_all_data->etat = "";

            if($get_all_data->prix_vente){
                if (strpos($get_all_data->prix_vente, '€') == false) {
                    $get_all_data->prixtoview =  $get_all_data->prix_vente.'€';
                }else{
                    $get_all_data->prixtoview =  $get_all_data->prix_vente;
                }
            }elseif($get_all_data->prix_ancien){
                if (strpos($get_all_data->prix_ancien, '€') == false) {
                    $get_all_data->prixtoview =  $get_all_data->prix_ancien.'€';
                }else{
                    $get_all_data->prixtoview =  $get_all_data->prix_ancien;
                }
            }

            $image_thumbs = "application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/thumbs/thumb_".$get_all_data->annonce_photo1->photo1;
            if(is_file($image_thumbs)){
                $get_all_data->photo_link = "https://www.sortez.org/".$image_thumbs;
            }elseif(is_file('application/resources/front/photoCommercant/imagesbank/'.$oInfoCommercant->user_ionauth_id.'/'.$get_all_data->annonce_photo1)){
                $get_all_data->photo_link = 'application/resources/front/photoCommercant/imagesbank/'.$oInfoCommercant->user_ionauth_id.'/'.$get_all_data->annonce_photo1;
            }else{
                $get_all_data->photo_link = 'application/resources/front/images/no_image_boutique.png';
            }
            $get_all_data->user_ion_auth = $oInfoCommercant->user_ionauth_id;
            $get_all_data->count = $count.' annonces déposées';
            array_push($data_annonce,$get_all_data);
        }
        $data['toAnnonce']= $data_annonce;
        echo json_encode($data);
    }
    function getcategorieAnnonce(){

        $toRubriquePrincipale = $this->mdlannonce->GetAnnonceCategoriePrincipale();

        $category_get =[];
        foreach ($toRubriquePrincipale as $toRubriquePrincipales){

            $toRubriquePrincipales->agenda_categid = $toRubriquePrincipales->IdSousRubrique;
            $toRubriquePrincipales->category = $toRubriquePrincipales->Nom;
            array_push($category_get,$toRubriquePrincipales);
        }

        $data['toCategorie_principale'] = $category_get;
        echo json_encode($data);

    }
    function getsouscategorieAnnonce(){

        $toSousRubrique = $this->mdlannonce->GetAnnonceSouscategorie();
        $souscategory_get =[];
        foreach ($toSousRubrique as $toSousRubriques){
            $toSousRubriques->agenda_categid = $toSousRubriques->IdSousRubrique;
            $toSousRubriques->category = $toSousRubriques->Nom;
            array_push($souscategory_get,$toSousRubriques);
        }

        $data['toSousRubrique'] = $toSousRubrique;

        echo json_encode($data);

    }
    function getpartenaireAnnonce(){

        $toCommercant = $this->Mdlcommercant->GetAllCommercant_with_annonce();

        $data['tocommercant'] = $toCommercant;

        echo json_encode($data);

    }
    function getcommunesAnnonce(){
        $toVille = $this->mdlville->GetAnnonceVilles_pvc_varest();

        $data['toVille'] = $toVille;

        echo json_encode($data);

    }

    public function getAnnuaireListe($nbPage = 1){
        $commune=0;
        $subcateg=0;
        $categorie=0;
        $motcle="";
        $data["iFavoris"] = "";
        $page_pagination = 0;
        $config_pagination = $nbPage*20;
        $iOrderBy = 0;
        $inputFromGeolocalisation = "0";
        $inputGeolocalisationValue = "0";
        $inputGeoLatitude = "0";
        $inputGeoLongitude = "0";
        $inputAvantagePartenaire = "0";
        $input_is_actif_coronna = 0;
        $input_is_actif_command = 0;

        $to_send = array();
        $toCommercant = $this->mdlcommercantpagination->listePartenaireRecherche(
            $categorie,
            $commune,
            $iDepartementId =0,
            $motcle,
            $data["iFavoris"],
            $page_pagination,
            $config_pagination,
            $iOrderBy,
            $inputFromGeolocalisation,
            $inputGeolocalisationValue,
            $inputGeoLatitude,
            $inputGeoLongitude,
            $inputAvantagePartenaire,
            $subcateg,
            $input_is_actif_coronna,
            $input_is_actif_command
        );
        foreach($toCommercant as $com){
            $categs = $this->mdlcategorie->GetById($com->IdRubrique);
            $subcategs = $this->mdlcategorie->GetByIdSousCateg($com->IdSousRubrique);
            $image_thumbs = "application/resources/front/photoCommercant/imagesbank/".$com->user_ionauth_id."/thumbs/thumb_".$com->Photo1;
            if(is_file($image_thumbs)){
                $imagecom = "https://www.sortez.org/".$image_thumbs;
            }elseif(is_file("application/resources/front/photoCommercant/imagesbank/".$com->user_ionauth_id."/".$com->Photo1)){
                $imagecom = "https://www.sortez.org/application/resources/front/photoCommercant/imagesbank/".$com->user_ionauth_id."/".$com->Photo1;
            }elseif(is_file("application/resources/front/photoCommercant/images/".$com->Photo1)){
                $imagecom = "https://www.sortez.org/application/resources/front/photoCommercant/images/".$com->Photo1;
            }else{
                $imagecom = GetImagePath("front/") . "/wp71b211d2_06.png";
            }
            
            $array = array(
                "categorie" =>$categs->Nom,
                "categid" => $categs->IdRubrique,
                "titre" =>$com->NomSociete,
                "ville_nom" =>$com->ville,
                "ville_id" => $com->IdVille,
                "subctaeg" => $subcategs->Nom,
                "subcategid" =>$subcategs->IdSousRubrique,
                "description" =>htmlspecialchars_decode(strip_tags($com->Caracteristiques),ENT_QUOTES),
                "id" => $com->IdCommercant,
                "user_ionauth_id" => $com->user_ionauth_id, 
                "Photo1" => $imagecom,
                "nom_url" => $com->nom_url
            );
            array_push($to_send,$array);
        }
        $data['toCommercant'] = $to_send;
        echo json_encode($data);
    }

    public function get_communesAnnonce(){
        $toVille = $this->mdlville->GetAgendaVilles_pvc_by_varest_annuaire_soutenons();
        $data['toVille'] = $toVille;
        echo json_encode($data);
    }

    public function getCategorieAnnuaire(){
        $toCategorie_principale = $this->mdlcategorie->GetCommercantCategorie();
        $data['toCategorie_principale'] = $toCategorie_principale;
        echo json_encode($data);
    }

    public function  getsubcategby_categid(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_rubrique=$_POST["id_categ"];
        $sousrubriques = $this->mdlcategorie->GetCommercantSouscategorieByRubrique($id_rubrique);
        $data['toSubcateg'] = $sousrubriques;
        echo json_encode($data);
    }

    public function filterAnnuaire($nbPage = 1){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);

        $commune=$_POST["commune"];
        $subcateg=$_POST["souscategorie"];
        $categorie=$_POST["categorie"];
        $motcle=$_POST["motcles"];
        $data["iFavoris"] = "";
        $page_pagination = 0;
        $config_pagination["per_page"] = $nbPage*20;
        $iOrderBy = 0;
        $inputFromGeolocalisation = "0";
        $inputGeolocalisationValue = "0";
        $inputGeoLatitude = "0";
        $inputGeoLongitude = "0";
        $inputAvantagePartenaire = "0";
        $input_is_actif_coronna = 0;
        $input_is_actif_command = 0;
        $to_send = [];
        $toCommercant = $this->mdlcommercantpagination->listePartenaireRecherche(
            $categorie,
            $commune,
            $iDepartementId =0,
            $motcle,
            $data["iFavoris"],
            $page_pagination,
            $config_pagination["per_page"],
            $iOrderBy,
            $inputFromGeolocalisation,
            $inputGeolocalisationValue,
            $inputGeoLatitude,
            $inputGeoLongitude,
            $inputAvantagePartenaire,
            $subcateg,
            $input_is_actif_coronna,
            $input_is_actif_command
        );
        foreach($toCommercant as $com){
            $categs = $this->mdlcategorie->GetById($com->IdRubrique);
            $subcategs = $this->mdlcategorie->GetByIdSousCateg($com->IdSousRubrique);
            $image_thumbs = "application/resources/front/photoCommercant/imagesbank/".$com->user_ionauth_id."/thumbs/thumb_".$com->Photo1;
            if(is_file($image_thumbs)){
                $imagecom = "https://www.sortez.org/".$image_thumbs;
            }elseif(is_file("application/resources/front/photoCommercant/imagesbank/".$com->user_ionauth_id."/".$com->Photo1)){
                $imagecom = "https://www.sortez.org/application/resources/front/photoCommercant/imagesbank/".$com->user_ionauth_id."/".$com->Photo1;
            }elseif(is_file("application/resources/front/photoCommercant/images/".$com->Photo1)){
                $imagecom = "https://www.sortez.org/application/resources/front/photoCommercant/images/".$com->Photo1;
            }else{
                $imagecom = GetImagePath("front/") . "/wp71b211d2_06.png";
            }
            $array = array(
                "categorie" =>$categs->Nom,
                "categid" => $categs->IdRubrique,
                "titre" =>$com->NomSociete,
                "ville_nom" =>$com->ville,
                "ville_id" => $com->IdVille,
                "subctaeg" => $subcategs->Nom,
                "subcategid" =>$subcategs->IdSousRubrique,
                "description" =>htmlspecialchars_decode(strip_tags($com->Caracteristiques),ENT_QUOTES),
                "id" => $com->IdCommercant,
                "user_ionauth_id" => $com->user_ionauth_id, 
                "Photo1" => $imagecom,
                "nom_url" => $com->nom_url
            );
            array_push($to_send,$array);
        }
        $data['toCommercant'] = $to_send;
    echo json_encode($data);
    }

    public function getcommunesDeals(){
        $communesbonplans = $this->mdl_api_front_global->getBonplancommunes();
        $communesFidelity = $this->mdl_api_front_global->getFidelitycommunes();
        $allidviles = [];
        $allville_nom = [];
        $allvilles = [];
        foreach($communesbonplans as $villebp){
            $to_push = array(
                "IdVille" => $villebp->IdVille,
                "ville_nom" => $villebp->ville_nom
            );

            if(in_array($villebp->IdVille,$allidviles,true) == false && in_array($villebp->ville_nom,$allville_nom,true)== false ){
                array_push($allidviles,$villebp->IdVille);
                array_push($allville_nom,$villebp->ville_nom);
                array_push($allvilles,$to_push);
            }

        }
        foreach($communesFidelity as $villefd){
            $to_push = array(
                "IdVille" => $villefd->IdVille,
                "ville_nom" => $villefd->ville_nom
            );
            if(!in_array($villefd->IdVille,$allidviles,true) && !in_array($villefd->ville_nom,$allville_nom,true) ){
                array_push($allvilles,$to_push);
                array_push($allidviles,$villefd->IdVille);
                array_push($allville_nom,$villefd->ville_nom);
            }
        }
        $data['toVille'] = $allvilles;
        echo json_encode($data);
    }

    public function getpartenaireDealsFidelity(){
        $arrayAll = [];
        $id_comBp = [];
        $commercantBonplan = $this->mdl_api_front_global->getBonplanPartenaire();
        $commercantFidelity = $this->mdl_api_front_global->getFidelityPartenaire();
        foreach($commercantBonplan as $combp){
            $to_push = array(
                "NomSociete" => $combp->NomSociete,
                "IdCommercant" =>  $combp->IdCommercant
            );
            array_push($id_comBp,$combp->IdCommercant);
            array_push($arrayAll,$to_push);
        }
        foreach($commercantFidelity as $comfd){
            $to_push = array(
                "NomSociete" => $comfd->NomSociete,
                "IdCommercant" =>  $comfd->IdCommercant
            );
            if(!in_array($comfd->IdCommercant,$id_comBp)){
                array_push($arrayAll,$to_push);
                array_push($id_comBp,$comfd->IdCommercant);
            }
        }
        $data['tocommercant'] = $arrayAll;
        echo json_encode($data);
    }

    public function getcategorieDealsFidelity(){

        $bonplancateg = $this->mdl_api_front_global->getBonplancateg();
        $fidelity_categ = $this->mdl_api_front_global->getFidelitycateg();
        $allcateg_id = [];
        $allcateg = [];
        foreach($bonplancateg as $bpcateg){
            if(!in_array($bpcateg->IdRubrique,$allcateg_id)){
                array_push($allcateg_id,$bpcateg->IdRubrique);
                $to_push = array(
                    "categorie" => $bpcateg->Nom,
                    "categid" => $bpcateg->IdSousRubrique
                );
                array_push($allcateg,$to_push);
            }
        }

        foreach($fidelity_categ as $fd_categ){
            if(!in_array($fd_categ->IdRubrique,$allcateg_id)){
                array_push($allcateg_id,$fd_categ->IdRubrique);
                $to_push = array(
                    "categorie" => $fd_categ->Nom,
                    "categid" => $fd_categ->IdSousRubrique
                );
                array_push($allcateg,$to_push);
            }
        }
        $data['toCategorie_principale'] = $allcateg;
        echo json_encode($data);
    }

    public function getBonplanFidelityListe($nbPage = 1){
        $_iCategorieId = 0;
        $i_CommercantId = 0;
        $_zMotCle = "";
        $_iFavoris = "";
        $_limitstart = 0;
        $_limitend = $nbPage*20;
        $_iIdVille = 0;
        $_iIdDepartement = 0;
        $iOrderBy = "";
        $inputFromGeo = "0";
        $inputGeoValue = "10";
        $inputGeoLatitude = "0";
        $inputGeoLongitude = "0";
        $session_inputFidelityType = 0;
        $table_fidelity_usedC = "capital";
        $table_fidelity_usedR = "remise";
        $table_fidelity_usedT = "tampon";

        $llfields = [];
        $bonplanListe = $this->mdl_api_front_global->listeBonPlanRecherche($_iCategorieId = 0, $i_CommercantId = 0, $_zMotCle = "", $_iFavoris = "", $_limitstart = 0, $_limitend = 10000, $_iIdVille = 0, $_iIdDepartement = 0, $iOrderBy="", $inputFromGeo = "0", $inputGeoValue = "10", $inputGeoLatitude="0", $inputGeoLongitude="0", $session_iWhereMultiple = "",$inputString_bonplan_type="");
        $fidelityListecapital = $this->mdlfidelity->listeFidelityRecherche($_iCategorieId, $i_CommercantId, $_zMotCle, $_iFavoris, $_limitstart, $_limitend, $_iIdVille, $_iIdDepartement, $iOrderBy, $inputFromGeo, $inputGeoValue, $inputGeoLatitude, $inputGeoLongitude, $session_inputFidelityType, $table_fidelity_usedC);
        $fidelityListeremise = $this->mdlfidelity->listeFidelityRecherche($_iCategorieId, $i_CommercantId, $_zMotCle, $_iFavoris, $_limitstart, $_limitend, $_iIdVille, $_iIdDepartement, $iOrderBy, $inputFromGeo, $inputGeoValue, $inputGeoLatitude, $inputGeoLongitude, $session_inputFidelityType, $table_fidelity_usedR);
        $fidelityListetampon = $this->mdlfidelity->listeFidelityRecherche($_iCategorieId, $i_CommercantId, $_zMotCle, $_iFavoris, $_limitstart, $_limitend, $_iIdVille, $_iIdDepartement, $iOrderBy, $inputFromGeo, $inputGeoValue, $inputGeoLatitude, $inputGeoLongitude, $session_inputFidelityType, $table_fidelity_usedT);
        
        if(!empty($fidelityListecapital)){
            foreach($fidelityListecapital as $cap){
                $image = "application/resources/front/photoCommercant/imagesbank/".$cap->user_ionauth_id."/".$cap->image1;
                $image_thumbs = "application/resources/front/photoCommercant/imagesbank/".$cap->user_ionauth_id."/thumbs/thumb_".$cap->image1;
                if(is_file($image_thumbs)){
                    $image_path = "https://www.sortez.org/".$image_thumbs;
                }elseif(is_file($image)){
                    $image_path = "https://www.sortez.org".$image;
                }else{
                    $image_path = "https://www.sortez.org/application/resources/privicarte/images/ico_sign_tampon_nv.png";
                }
                $array = array(
                    "id" => $cap->id,
                    "NomSociete" => $cap->NomSociete,
                    "description" => $cap->description,
                    "prix_normal" => $cap->tampon_value,
                    "ville_nom" => $cap->ville,
                    'remise' =>0,
                    "categorie" => $cap->Nom,
                    "date_debut" => $cap->date_debut,
                    "date_fin" => $cap->date_fin,
                    "image" => $image_path,
                    "type" => "capitalisation"
                );
                array_push($llfields,$array);
            }
        }
        if(!empty($fidelityListeremise)){
            foreach($fidelityListeremise as $rem){
                $imagere = "application/resources/front/photoCommercant/imagesbank/".$rem->user_ionauth_id."/".$rem->image1;
                $image_thumbs = "application/resources/front/photoCommercant/imagesbank/".$rem->user_ionauth_id."/thumbs/thumb_".$rem->image1;
                if(is_file($image_thumbs)){
                    $image_pathre = "https://www.sortez.org/".$image_thumbs;
                }elseif(is_file($imagere)){
                    $image_pathre = "https://www.sortez.org".$imagere;
                }else{
                    $image_pathre = "https://www.sortez.org/application/resources/privicarte/images/ico_sign_tampon_nv.png";
                }
                $arrayre = array(
                    "id" => $rem->id,
                    "NomSociete" => $rem->NomSociete,
                    "description" => $rem->description,
                    "prix_normal" => $rem->tampon_value,
                    "ville_nom" => $rem->ville,
                    "categorie" => $rem->Nom,
                    'remise' =>0,
                    "date_debut" => $rem->date_debut,
                    "date_fin" => $rem->date_fin,
                    "image" => $image_pathre,
                    "type" => "remise"
                );
                array_push($llfields,$arrayre);
            }
        }
        if(!empty($fidelityListetampon)){
            foreach($fidelityListetampon as $tam){
                $imageta = "application/resources/front/photoCommercant/imagesbank/".$tam->user_ionauth_id."/".$tam->image1;
                $image_thumbs = "application/resources/front/photoCommercant/imagesbank/".$tam->user_ionauth_id."/thumbs/thumb_".$tam->image1;
                if(is_file($image_thumbs)){
                    $image_pathta = "https://www.sortez.org/".$image_thumbs;
                }elseif(is_file($imageta)){
                    $image_pathta = "https://www.sortez.org/".$imageta;
                }else{
                    $image_pathta = "https://www.sortez.org/application/resources/privicarte/images/ico_sign_tampon_nv.png";
                }
                $arrayta = array(
                    "id" => $tam->id,
                    "NomSociete" => $tam->NomSociete,
                    "description" => $tam->description,
                    "prix_normal" => $tam->tampon_value,
                    "ville_nom" => $tam->ville,
                    "categorie" => $tam->Nom,
                    'remise' =>0,
                    "date_debut" => $tam->date_debut,
                    "date_fin" => $tam->date_fin,
                    "image" => $image_pathta,
                    "type" => "tampon"
                );
                array_push($llfields,$arrayta);
            }
        }
        if(!empty($bonplanListe)){
            foreach($bonplanListe as $bon){
                if($bon->bonplan_id !=null && $bon->bonplan_commercant_id != null){
                $imagebon = "application/resources/front/photoCommercant/imagesbank/".$bon->user_ionauth_id."/".$bon->bonplan_photo1;
                $image_thumbs = "application/resources/front/photoCommercant/imagesbank/".$bon->user_ionauth_id."/thumbs/thumb_".$bon->bonplan_photo1;
                if(is_file($image_thumbs)){
                    $image_pathbon = "https://www.sortez.org/".$image_thumbs;
                }elseif(is_file($imagebon)){
                    $image_pathbon = "https://www.sortez.org/".$imagebon;
                }else{
                    $image_pathbon = "https://www.sortez.org/application/resources/front/images/no_image_bonplan.png";
                }
                if($bon->bp_simple_prix !=0 && $bon->bp_simple_prix !=null && $bon->bp_simple_prix != ""){
                    $prix_remisebp = $bon->bp_simple_value;
                    $prix_normbp = $bon->bp_simple_prix;
                    $date_debut = $bon->bonplan_date_debut;
                    $date_fin = $bon->bonplan_date_fin;
                }elseif($bon->bp_unique_prix !=0 && $bon->bp_unique_prix !=null && $bon->bp_unique_prix != ""){
                    $prix_remisebp = $bon->bp_unique_prix;
                    $prix_normbp = $bon->bp_unique_value;
                    $date_debut = $bon->bp_unique_date_debut;
                    $date_fin = $bon->bp_unique_date_debut;
                }else{
                    $prix_remisebp = $bon->bp_multiple_value;
                    $prix_normbp = $bon->bp_multiple_prix;
                    $date_debut = $bon->bp_multiple_date_debut;
                    $date_fin = $bon->bp_multiple_date_debut;
                }
                
                $categ = $this->mdlcategorie->GetBonplanCategoriePrincipaleBycateg($bon->bonplan_commercant_id);
                if($categ != null){
                    $arraybon = array(
                        "id" => $bon->bonplan_id,
                        "NomSociete" => $bon->NomSociete,
                        "description" => $bon->bonplan_titre,
                        "prix_normal" => $prix_normbp,
                        'remise' =>$prix_remisebp,
                        "ville_nom" => $bon->ville,
                        "categorie" => $bon->Nom,
                        "date_debut" => $date_debut,
                        "date_fin" => $date_fin,
                        "image" => $image_pathbon,
                        "type" => "bonplan"
                    );
                    array_push($llfields,$arraybon);
                } 
            }
            }
        }
        $data['toDealsFidelity'] = $llfields;
        echo json_encode($data);
    }

public function filterDealsFidelity($nbPage=1){

        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $commune=$_POST["commune"];
        $commercant=$_POST["commercant"];
        $categorie=$_POST["categorie"];
        $type=$_POST["type"];
        $motcles=$_POST["motcles"];

        $_iCategorieId[0] = $categorie;
        $i_CommercantId = $commercant;
        $_zMotCle = $motcles;
        $_iFavoris = "";
        $_limitstart = 0;
        $_limitend = $nbPage*20;
        $_iIdVille = $commune;
        $_iIdDepartement = 0;
        $iOrderBy = "";
        $inputFromGeo = "0";
        $inputGeoValue = "10";
        $inputGeoLatitude = "0";
        $inputGeoLongitude = "0";
        $session_inputFidelityType = 0;
        $table_fidelity_usedC = "capital";
        $table_fidelity_usedR = "remise";
        $table_fidelity_usedT = "tampon";

        $llfields = [];
        $bonplanListe = $this->mdl_api_front_global->listeBonPlanRecherche($_iCategorieId, $i_CommercantId , $_zMotCle , $_iFavoris = "", $_limitstart = 0, $_limitend = 10000, $_iIdVille , $_iIdDepartement = 0, $iOrderBy="", $inputFromGeo = "0", $inputGeoValue = "10", $inputGeoLatitude="0", $inputGeoLongitude="0", $session_iWhereMultiple = "",$inputString_bonplan_type="");
        $fidelityListecapital = $this->mdlfidelity->listeFidelityRecherche($_iCategorieId, $i_CommercantId, $_zMotCle, $_iFavoris, $_limitstart, $_limitend, $_iIdVille, $_iIdDepartement, $iOrderBy, $inputFromGeo, $inputGeoValue, $inputGeoLatitude, $inputGeoLongitude, $session_inputFidelityType, $table_fidelity_usedC);
        $fidelityListeremise = $this->mdlfidelity->listeFidelityRecherche($_iCategorieId, $i_CommercantId, $_zMotCle, $_iFavoris, $_limitstart, $_limitend, $_iIdVille, $_iIdDepartement, $iOrderBy, $inputFromGeo, $inputGeoValue, $inputGeoLatitude, $inputGeoLongitude, $session_inputFidelityType, $table_fidelity_usedR);
        $fidelityListetampon = $this->mdlfidelity->listeFidelityRecherche($_iCategorieId, $i_CommercantId, $_zMotCle, $_iFavoris, $_limitstart, $_limitend, $_iIdVille, $_iIdDepartement, $iOrderBy, $inputFromGeo, $inputGeoValue, $inputGeoLatitude, $inputGeoLongitude, $session_inputFidelityType, $table_fidelity_usedT);
        if((isset($type) && $type =="df") || (isset($type) && $type =="f") ){
        if(!empty($fidelityListecapital)){
            foreach($fidelityListecapital as $cap){
                $image = "application/resources/front/photoCommercant/imagesbank/".$cap->user_ionauth_id."/".$cap->image1;
                $image_thumbs = "application/resources/front/photoCommercant/imagesbank/".$cap->user_ionauth_id."/thumbs/thumb_".$cap->image1;
                if(is_file($image_thumbs)){
                    $image_path = "https://www.sortez.org/".$image_thumbs;
                }elseif(is_file($image)){
                    $image_path = "https://www.sortez.org/".$image;
                }else{
                    $image_path = "https://www.sortez.org/application/resources/privicarte/images/ico_sign_tampon_nv.png";
                }
                $array = array(
                    "id" => $cap->id,
                    "NomSociete" => $cap->NomSociete,
                    "description" => $cap->description,
                    "prix_normal" => $cap->tampon_value,
                    "ville_nom" => $cap->ville,
                    'remise' =>0,
                    "categorie" => $cap->Nom,
                    "date_debut" => $cap->date_debut,
                    "date_fin" => $cap->date_fin,
                    "image" => $image_path,
                    "type" => "fidelite"
                );
                array_push($llfields,$array);
            }
        }
        if(!empty($fidelityListeremise)){
            foreach($fidelityListeremise as $rem){
                $imagere = "application/resources/front/photoCommercant/imagesbank/".$rem->user_ionauth_id."/".$rem->image1;
                $image_thumbs = "application/resources/front/photoCommercant/imagesbank/".$rem->user_ionauth_id."/thumbs/thumb_".$rem->image1;
                if(is_file($image_thumbs)){
                    $image_path = "https://www.sortez.org/".$image_thumbs;
                }elseif(is_file($imagere)){
                    $image_pathre = "https://www.sortez.org/".$imagere;
                }else{
                    $image_pathre = "https://www.sortez.org/application/resources/privicarte/images/ico_sign_tampon_nv.png";
                }
                $arrayre = array(
                    "id" => $rem->id,
                    "NomSociete" => $rem->NomSociete,
                    "description" => $rem->description,
                    "prix_normal" => $rem->tampon_value,
                    "ville_nom" => $rem->ville,
                    "categorie" => $rem->Nom,
                    'remise' =>0,
                    "date_debut" => $rem->date_debut,
                    "date_fin" => $rem->date_fin,
                    "image" => $image_pathre,
                    "type" => "fidelite"
                );
                array_push($llfields,$arrayre);
            }
        }
        if(!empty($fidelityListetampon)){
            foreach($fidelityListetampon as $tam){
                $imageta = "application/resources/front/photoCommercant/imagesbank/".$tam->user_ionauth_id."/".$tam->image1;
                $image_thumbs = "application/resources/front/photoCommercant/imagesbank/".$tam->user_ionauth_id."/thumbs/thumb_".$tam->image1;
                if(is_file($image_thumbs)){
                    $image_path = "https://www.sortez.org/".$image_thumbs;
                }elseif(is_file($imageta)){
                    $image_pathta = "https://www.sortez.org/".$imageta;
                }else{
                    $image_pathta = "https://www.sortez.org/application/resources/privicarte/images/ico_sign_tampon_nv.png";
                }
                $arrayta = array(
                    "id" => $tam->id,
                    "NomSociete" => $tam->NomSociete,
                    "description" => $tam->description,
                    "prix_normal" => $tam->tampon_value,
                    "ville_nom" => $tam->ville,
                    "categorie" => $tam->Nom,
                    'remise' =>0,
                    "date_debut" => $tam->date_debut,
                    "date_fin" => $tam->date_fin,
                    "image" => $image_pathta,
                    "type" => "fidelite"
                );
                array_push($llfields,$arrayta);
            }
        }
    }
    if((isset($type) && $type =="df") || (isset($type) && $type =="d") ){
        if(!empty($bonplanListe)){
            foreach($bonplanListe as $bon){
                if($bon->bonplan_id !=null && $bon->bonplan_commercant_id != null){
                $imagebon = "application/resources/front/photoCommercant/imagesbank/".$bon->user_ionauth_id."/".$bon->bonplan_photo1;
                $image_thumbs = "application/resources/front/photoCommercant/imagesbank/".$bon->user_ionauth_id."/thumbs/thumb_".$bon->bonplan_photo1;
                if(is_file($image_thumbs)){
                    $image_path = "https://www.sortez.org/".$image_thumbs;
                }elseif(is_file($imagebon)){
                    $image_pathbon = "https://www.sortez.org/".$imagebon;
                }else{
                    $image_pathbon = "https://www.sortez.org/application/resources/front/images/no_image_bonplan.png";
                }
                if($bon->bp_simple_prix !=0 && $bon->bp_simple_prix !=null && $bon->bp_simple_prix != ""){
                    $prix_remisebp = $bon->bp_simple_value;
                    $prix_normbp = $bon->bp_simple_prix;
                    $date_debut = $bon->bonplan_date_debut;
                    $date_fin = $bon->bonplan_date_fin;
                }elseif($bon->bp_unique_prix !=0 && $bon->bp_unique_prix !=null && $bon->bp_unique_prix != ""){
                    $prix_remisebp = $bon->bp_unique_prix;
                    $prix_normbp = $bon->bp_unique_value;
                    $date_debut = $bon->bp_unique_date_debut;
                    $date_fin = $bon->bp_unique_date_debut;
                }else{
                    $prix_remisebp = $bon->bp_multiple_value;
                    $prix_normbp = $bon->bp_multiple_prix;
                    $date_debut = $bon->bp_multiple_date_debut;
                    $date_fin = $bon->bp_multiple_date_debut;
                }
                
                $categ = $this->mdlcategorie->GetBonplanCategoriePrincipaleBycateg($bon->bonplan_commercant_id);
                if($categ != null){
                    $arraybon = array(
                        "id" => $bon->bonplan_id,
                        "NomSociete" => $bon->NomSociete,
                        "description" => $bon->bonplan_titre,
                        "prix_normal" => $prix_normbp,
                        'remise' =>$prix_remisebp,
                        "ville_nom" => $bon->ville,
                        "categorie" => $bon->Nom,
                        "date_debut" => $date_debut,
                        "date_fin" => $date_fin,
                        "image" => $image_pathbon,
                        "type" => "bonplan"
                    );
                    array_push($llfields,$arraybon);
                } 
            }
            }
        }
        }
        $data['toDealsFidelity'] = $llfields;
        echo json_encode($data);
    }

    public function get_art_details(){

        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id = $_POST["id"];
        // $id = 148889;
        $dataAgenda = $this->mdlarticle->GetById_IsActif($id);
        $oInfoCommercant = $this->mdlcommercant->infoCommercant($dataAgenda->IdCommercant);
        $image = "application/resources/front/photoCommercant/imagesbank/".$dataAgenda->IdUsers_ionauth."/".$dataAgenda->photo1;
        $image2 = "application/resources/front/photoCommercant/imagesbank/rss_image/".$dataAgenda->photo1;
        if(is_file($image)){
            $imageag = "https://www.sortez.org/".$image;
        }elseif(is_file($image2)){
            $imageag = "https://www.sortez.org/".$image2;
        }else{
            $imageag = GetImagePath("front/") . "/wp71b211d2_06.png";
        }
        $dataAgenda->photo1 = $imageag;
        $data['article'] = $dataAgenda;
        $date = "";
        $toArticle_datetime = $this->mdl_article_datetime->getByArticleId($id);
         if (isset($toArticle_datetime) && count($toArticle_datetime) > 0) { 
            foreach ($toArticle_datetime as $objArticle_datetime) { 
                if (isset($objArticle_datetime->date_debut) && $objArticle_datetime->date_debut != "0000-00-00" && ($objArticle_datetime->date_debut == $objArticle_datetime->date_fin)) {
                    $date .=  "Le " . translate_date_to_fr($objArticle_datetime->date_debut);
                } else {
                    if (isset($objArticle_datetime->date_debut) && $objArticle_datetime->date_debut != "0000-00-00") $date = "Du " . translate_date_to_fr($objArticle_datetime->date_debut);
                    if (isset($objArticle_datetime->date_fin) && $objArticle_datetime->date_fin != "0000-00-00") {
                        if (isset($objArticle_datetime->date_debut) && $objArticle_datetime->date_debut != "0000-00-00") $date .= " au " . translate_date_to_fr($objArticle_datetime->date_fin);
                        else $date .= " Jusqu'au " . translate_date_to_fr($objArticle_datetime->date_fin);
                    }
                    if (isset($objArticle_datetime->heure_debut) && $objArticle_datetime->heure_debut != "0:00"   && $objArticle_datetime->heure_debut != "" && $objArticle_datetime->heure_debut != null) $date .= " à " . str_replace(":", "h", $objArticle_datetime->heure_debut);
                }
                
            } 
         }

         if (isset($dataAgenda->organiser_id) && $dataAgenda->organiser_id != "0") {
            $obj_organiser_article_details = $this->mdl_article_organiser->getById($dataAgenda->organiser_id);
            if (isset($obj_organiser_article_details) && is_object($obj_organiser_article_details)) {
                
                $dataAgenda->organiser_name = $obj_organiser_article_details->name;
                $dataAgenda->organiser_codePostal = $obj_organiser_article_details->postal_code;
                    if (isset($obj_organiser_article_details->address1) && $obj_organiser_article_details->address1 != "") $dataAgenda->organiser_adress =  $obj_organiser_article_details->address1;
                    if (isset($obj_organiser_article_details->ville_id) && $obj_organiser_article_details->ville_id != "0") $dataAgenda->organiser_ville =  $this->mdlville->getVilleById($obj_organiser_article_details->ville_id)->Nom;
                    if (isset($obj_organiser_article_details->tel) && $obj_organiser_article_details->tel != "") $dataAgenda->organiser_telephone = $obj_organiser_article_details->tel;
                    if (isset($obj_organiser_article_details->website) && $obj_organiser_article_details->website != "") $dataAgenda->organiser_website = $obj_organiser_article_details->website;//echo "<br/>Site Web : " . $obj_organiser_article_details->website;
                    if (isset($obj_organiser_article_details->facebook) && $obj_organiser_article_details->facebook != "") $dataAgenda->organiser_facebook = $obj_organiser_article_details->facebook; //echo "<br/>Facebook : " . $obj_organiser_article_details->facebook;
                    if (isset($obj_organiser_article_details->twitter) && $obj_organiser_article_details->twitter != "") $dataAgenda->organiser_twitter = $obj_organiser_article_details->twitter; //echo "<br/>Twitter : " . $obj_organiser_article_details->twitter;
                    if (isset($obj_organiser_article_details->googleplus) && $obj_organiser_article_details->googleplus != "") $dataAgenda->organiser_gplus = $obj_organiser_article_details->googleplus;//echo "<br/>Google+ : " . $obj_organiser_article_details->googleplus;
            
            }
        }


        $dataAgenda->description = htmlspecialchars_decode(strip_tags($dataAgenda->description),ENT_QUOTES);
        $dataAgenda->NomSociete = $oInfoCommercant->NomSociete;
        $dataAgenda->date_debut = $date;
        $data['article'] = $dataAgenda;
        echo json_encode($data);

    }

    public function get_ag_details(){

        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id = $_POST["id"];
        // $id = 149828;
        $dataAgenda = $this->mdl_agenda->GetById_IsActif($id);
        $oInfoCommercant = $this->mdlcommercant->infoCommercant($dataAgenda->IdCommercant);
        $image = "application/resources/front/photoCommercant/imagesbank/".$dataAgenda->IdUsers_ionauth."/".$dataAgenda->photo1;
        $image_thumbs = "application/resources/front/photoCommercant/imagesbank/".$dataAgenda->IdUsers_ionauth."/"."thumbs/thumb_".$dataAgenda->photo1;
        if(is_file($image)){
            $imageag = "https://www.sortez.org/".$image;
        }elseif(is_file("application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/".$oInfoCommercant->Photo1)){
            $imageag = "https://www.sortez.org/application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/".$oInfoCommercant->Photo1;
        }else{
            $imageag = GetImagePath("front/") . "/wp71b211d2_06.png";
        }
        $dataAgenda->photo1 = $imageag;
        $data['article'] = $dataAgenda;
        $date = "";
        $toArticle_datetime = $this->mdl_agenda_datetime->getByAgendaId($id);
         if (isset($toArticle_datetime) && count($toArticle_datetime) > 0) { 
            foreach ($toArticle_datetime as $objArticle_datetime) { 
                if (isset($objArticle_datetime->date_debut) && $objArticle_datetime->date_debut != "0000-00-00" && ($objArticle_datetime->date_debut == $objArticle_datetime->date_fin)) {
                    $date .=  "Le " . translate_date_to_fr($objArticle_datetime->date_debut);
                } else {
                    if (isset($objArticle_datetime->date_debut) && $objArticle_datetime->date_debut != "0000-00-00") $date = "Du " . translate_date_to_fr($objArticle_datetime->date_debut);
                    if (isset($objArticle_datetime->date_fin) && $objArticle_datetime->date_fin != "0000-00-00") {
                        if (isset($objArticle_datetime->date_debut) && $objArticle_datetime->date_debut != "0000-00-00") $date .= " au " . translate_date_to_fr($objArticle_datetime->date_fin);
                        else $date .= " Jusqu'au " . translate_date_to_fr($objArticle_datetime->date_fin);
                    }
                    if (isset($objArticle_datetime->heure_debut) && $objArticle_datetime->heure_debut != "0:00"   && $objArticle_datetime->heure_debut != "" && $objArticle_datetime->heure_debut != null) $date .= " à " . str_replace(":", "h", $objArticle_datetime->heure_debut);
                }
                
            } 
         }

         if (isset($dataAgenda->organiser_id) && $dataAgenda->organiser_id != "0") {
            $obj_organiser_article_details = $this->mdl_article_organiser->getById($dataAgenda->organiser_id);
            if (isset($obj_organiser_article_details) && is_object($obj_organiser_article_details)) {
                
                $dataAgenda->organiser_name = $obj_organiser_article_details->name;
                $dataAgenda->organiser_codePostal = $obj_organiser_article_details->postal_code;
                    if (isset($obj_organiser_article_details->address1) && $obj_organiser_article_details->address1 != "") $dataAgenda->organiser_adress =  $obj_organiser_article_details->address1;
                    if (isset($obj_organiser_article_details->ville_id) && $obj_organiser_article_details->ville_id != "0") $dataAgenda->organiser_ville =  $this->mdlville->getVilleById($obj_organiser_article_details->ville_id)->Nom;
                    if (isset($obj_organiser_article_details->tel) && $obj_organiser_article_details->tel != "") $dataAgenda->organiser_telephone = $obj_organiser_article_details->tel;
                    if (isset($obj_organiser_article_details->website) && $obj_organiser_article_details->website != "") $dataAgenda->organiser_website = $obj_organiser_article_details->website;//echo "<br/>Site Web : " . $obj_organiser_article_details->website;
                    if (isset($obj_organiser_article_details->facebook) && $obj_organiser_article_details->facebook != "") $dataAgenda->organiser_facebook = $obj_organiser_article_details->facebook; //echo "<br/>Facebook : " . $obj_organiser_article_details->facebook;
                    if (isset($obj_organiser_article_details->twitter) && $obj_organiser_article_details->twitter != "") $dataAgenda->organiser_twitter = $obj_organiser_article_details->twitter; //echo "<br/>Twitter : " . $obj_organiser_article_details->twitter;
                    if (isset($obj_organiser_article_details->googleplus) && $obj_organiser_article_details->googleplus != "") $dataAgenda->organiser_gplus = $obj_organiser_article_details->googleplus;//echo "<br/>Google+ : " . $obj_organiser_article_details->googleplus;
            
            }
        }


        $dataAgenda->description = htmlspecialchars_decode(strip_tags($dataAgenda->description),ENT_QUOTES);
        $dataAgenda->NomSociete = $oInfoCommercant->NomSociete;
        $dataAgenda->date_debut = $date;
        $data['article'] = $dataAgenda;
        echo json_encode($data);

    }

    public function get_boutique_details(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id = $_POST["id"];
        // $id = 261;
        $dataAnnonce = $this->mdlannonce->GetById_annonce_etat($id);
        $oInfoCommercant = $this->mdlcommercant->infoCommercant($dataAnnonce->annonce_commercant_id);
        $image1 = "application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/".$dataAnnonce->photo1;
        $image2 = "application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/".$dataAnnonce->photo2;
        $image3 = "application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/".$dataAnnonce->photo3;
        $image4 = "application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/".$dataAnnonce->photo4;
        if(is_file($image1)){
            $image1s = "https://www.sortez.org/".$image1;
        }else{
            $image1s = GetImagePath("front/") . "/wp71b211d2_06.png";
        }

        if(is_file($image2)){
            $image2s = "https://www.sortez.org/".$image2;
        }

        if(is_file($image3)){
            $image3s = "https://www.sortez.org/".$image3;
        }

        if(is_file($image4)){
            $image4s = "https://www.sortez.org/".$image4;
        }
        $dataAnnonce->texte_longue =  htmlspecialchars_decode(strip_tags($dataAnnonce->texte_longue),ENT_QUOTES);
        $dataAnnonce->texte_courte =  htmlspecialchars_decode(strip_tags($dataAnnonce->texte_courte),ENT_QUOTES);

        $dataAnnonce->photo1 =  $image1s;
        $dataAnnonce->photo2 =  $image2s ?? null;
        $dataAnnonce->photo3 =  $image3s ?? null;
        $dataAnnonce->photo4 =  $image4s ?? null;

        $data['boutique'] = $dataAnnonce;
        echo json_encode($data);
    }

    public function get_deals_details(){

        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id = $_POST["id"];
        $type = $_POST["type"];
        if($type == "capitalisation"){
            $detail = $this->mdl_card_capital->getById($id);
            $oInfoCommercant = $this->Mdlcommercant->infoCommercant($detail->id_commercant);
            $imagebon = "application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/".$detail->image1;
            if(is_file($imagebon)){
                $image_pathbon = "https://www.sortez.org/".$imagebon;
            }else{
                $image_pathbon = "https://www.sortez.org/application/resources/front/images/no_image_bonplan.png";
            }
            $detail->prix =$detail->remise_value;
            $detail->image = $image_pathbon;
            $detail->titre = htmlspecialchars_decode(strip_tags($detail->description),ENT_QUOTES);
        }
        if($type == "tampon"){
            $detail = $this->mdl_card_tampon->getById($id);
            $oInfoCommercant = $this->Mdlcommercant->infoCommercant($detail->id_commercant);
            $imagebon = "application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/".$detail->image1;
            if(is_file($imagebon)){
                $image_pathbon = "https://www.sortez.org/".$imagebon;
            }else{
                $image_pathbon = "https://www.sortez.org/application/resources/front/images/no_image_bonplan.png";
            }
            $detail->image = $image_pathbon;
            $detail->prix =$detail->tampon_value;
            $detail->titre = htmlspecialchars_decode(strip_tags($detail->description),ENT_QUOTES);
        }
        if($type == "remise"){
            $detail = $this->mdl_card_remise->getById($id);
            $oInfoCommercant = $this->Mdlcommercant->infoCommercant($detail->id_commercant);
            $imagebon = "application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/".$detail->image1;
            if(is_file($imagebon)){
                $image_pathbon = "https://www.sortez.org/".$imagebon;
            }else{
                $image_pathbon = "https://www.sortez.org/application/resources/front/images/no_image_bonplan.png";
            }
            $detail->prix =$detail->remise_value;
            $detail->image = $image_pathbon;
            $detail->titre = htmlspecialchars_decode(strip_tags($detail->description),ENT_QUOTES);
        }
        if($type == "bonplan"){
            $detail = $this->mdlbonplan->getById($id);
            if($detail->bonplan_type =="1"){
                $detail->prix_remisebp = $detail->bp_simple_value;
                $detail->prix_normbp = $detail->bp_simple_prix;
                $detail->date_debut = $detail->bonplan_date_debut;
                $detail->date_fin = $detail->bonplan_date_fin;
                $detail->nbMax = 5;
            }elseif($detail->bonplan_type == "2"){
                $detail->prix_remisebp = $detail->bp_unique_prix;
                $detail->prix_normbp = $detail->bp_unique_value;
                $detail->date_debut = $detail->bp_unique_date_debut;
                $detail->date_fin = $detail->bp_unique_date_debut;
                $detail->nbMax = $detail->bp_unique_nbmax;
            }else{
                $detail->prix_remisebp = $detail->bp_multiple_value;
                $detail->prix_normbp = $detail->bp_multiple_prix;
                $detail->date_debut = $detail->bp_multiple_date_debut;
                $detail->date_fin = $detail->bp_multiple_date_debut;
                $detail->nbMax = $detail->activ_bp_multiple_nbmax;
            }
            $oInfoCommercant = $this->Mdlcommercant->infoCommercant($detail->bonplan_commercant_id);
            $imagebon = "application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/".$detail->bonplan_photo1;
            if(is_file($imagebon)){
                $image_pathbon = "https://www.sortez.org/".$imagebon;
            }else{
                $image_pathbon = "https://www.sortez.org/application/resources/front/images/no_image_bonplan.png";
            }
            $detail->bonplan_photo1 = $image_pathbon;
            $detail->bonplan_titre = htmlspecialchars_decode(strip_tags($detail->bonplan_titre),ENT_QUOTES);
            $detail->bonplan_texte = htmlspecialchars_decode(strip_tags($detail->bonplan_texte),ENT_QUOTES);
        } 

        $data['details'] = $detail;
        $data['infocom'] = $oInfoCommercant;

        echo json_encode($data);

    }

    public function contact_us(){

        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $email = $_POST["Email"];
        $message = $_POST["Message"];

        $contact_privicarte_nom = "Visiteur sortez";
        $contact_privicarte_tel = "Non precisé";
        $contact_privicarte_mail = $email;
        $contact_privicarte_msg = $message;
        $contact_privicarte_mailto = "alphadev@randevteam.com";
        $contact_privicarte_mailSubject = "Contact";

        if (!isset($contact_privicarte_mailto) || $contact_privicarte_mailto == "" || $contact_privicarte_mailto == NULL) $contact_privicarte_mailto = "contact@sortez.org";
        if (!isset($contact_privicarte_mail) || $contact_privicarte_mail == "" || $contact_privicarte_mail == NULL) $contact_privicarte_mail = "noreply@sortez.org";
        if (!isset($contact_privicarte_mailSubject) || $contact_privicarte_mailSubject == "" || $contact_privicarte_mailSubject == NULL) $contact_privicarte_mailSubject = 'Contact - Sortez';

        $message_html = '
        <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
        <p>Bonjour,<br/>
        Ceci est une notification de contact de Sortez.org<br/>
        Ci-dessous le contenu,</p>
        <p>Nom : ' . $contact_privicarte_nom . '<br/>
        T&eacute;l&eacute;phone : ' . $contact_privicarte_tel . '<br/>
        Email : ' . $contact_privicarte_mail . '<br/>
        Message : <br/><br/>' . $contact_privicarte_msg . '</p>
        <p><hr><a href="http://Sortez.org/"><img alt="Sortez" src="' . base_url() . '/application/resources/privicarte/images/logo.png"></a></p>
        </div>
        ';

        //echo "<br/>Init<br/>";


        $base_path_system = str_replace('system/', '', BASEPATH);
        //echo $base_path_system; die();

        require_once($base_path_system . 'application/resources/phpmailer/PHPMailerAutoload.php');

        //include("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

        $mail = new PHPMailer();

        //$body             = file_get_contents('contents.html');
        //$body             = eregi_replace("[\]",'',$body);

        $mail->IsHTML(true);
        $mail->CharSet = "utf-8";

        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->Host = "mail.sortez.org"; // SMTP server
        $mail->SMTPDebug = 0;                     // enables SMTP debug information (for testing)
        // 1 = errors and messages
        // 2 = messages only
        $mail->SMTPAuth = true;                  // enable SMTP authentication
        $mail->Host = "ssl0.ovh.net"; // sets the SMTP server
        $mail->SMTPSecure = "ssl";
        $mail->Port = 465;                    // set the SMTP port for the GMAIL server
        $mail->Username = "mail@sortez.org"; // SMTP account username
        $mail->Password = 'J^ou~#|~[{`\'5((*_~#{[]@_(-)';        // SMTP account password
        $mail->SetFrom($contact_privicarte_mail, $contact_privicarte_nom);
        $mail->AddReplyTo($contact_privicarte_mail, $contact_privicarte_nom);
        $mail->Subject = $contact_privicarte_mailSubject;
        $mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

        //$mail->MsgHTML($body);
        $mail->MsgHTML($message_html);

        $address = $contact_privicarte_mailto;
        $mail->AddAddress($address, "Contact");

        //$mail->AddAttachment("images/phpmailer.gif");      // attachment
        //$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment

        if (!$mail->Send()) {

            $zContactEmailCom_copy = array();
            $zContactEmailCom_copy[] = array("Email" => $contact_privicarte_mail, "Name" => $contact_privicarte_nom);
            $message_html_copy = '
                <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
                <p>Bonjour,<br/>
                Ceci est une copie de votre contact Sortez.org<br/>
                Ci-dessous le contenu,</p>
                <p>Nom : ' . $contact_privicarte_nom . '<br/>
                T&eacute;l&eacute;phone : ' . $contact_privicarte_tel . '<br/>
                Email : ' . $contact_privicarte_mail . '<br/>
                Message : <br/><br/>' . $contact_privicarte_msg . '</p>
                <p><hr><a href="http://Sortez.org/"><img alt="Sortez" src="http://privicarte.fr/portail/application/resources/privicarte/images/logo.png"></a></p>
                </div>
                ';
            $message_html_copy = html_entity_decode(htmlentities($message_html_copy));
            $sending_mail_privicarte_copy = @envoi_notification($zContactEmailCom_copy, $contact_privicarte_mailSubject, $message_html_copy);


            $data['sent'] = "Non Envoyé";
            echo json_encode($data);
        } else {
            $data['sent'] = "Envoyé";
            echo json_encode($data);
        }
    }

    function demandeBonPlan(){
            $rest_json=file_get_contents("php://input");
            $_POST=json_decode($rest_json,true);
            $id_bonplan = $_POST["id_bonplan"];
            $bonplan_type = $_POST["bonplan_type"];
            $bp_multiple_nbmax_client = $_POST["bp_multiple_nbmax_client"];
            $bp_multiple_date_visit_client = $_POST["bp_multiple_date_visit_client"];
            $ionauth_id = $_POST["ion_auth_id"];

            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($ionauth_id);
            $user_groups = $this->ion_auth->get_users_groups($ionauth_id)->result();
            ////$this->firephp->log($user_groups, 'user_groups');
            if ($user_groups[0]->id==1) $typeuser = "1";
            else if ($user_groups[0]->id==2) $typeuser = "0";
            else if ($user_groups[0]->id==3 || $user_groups[0]->id==4 || $user_groups[0]->id==5) $typeuser = "COMMERCANT";

            $id_client = $iduser;
            $type_client = $typeuser;
                // assoc_client_bonplan
            $idMax = $this->mdlcadeau->getMaxNumMailBonPlan();
            $id = $idMax+1;
            //generate code_cadeaux_client
            $iNumUnique = $id.$id_client.$id_bonplan ;
            //generate code_cadeaux_commercant
            $iNumUnique_commercant = newAleatoireChaine(8);
            if (isset($bp_multiple_date_visit_client) && $bp_multiple_date_visit_client == "0000-00-00") $bp_multiple_date_visit_client = null;
            $data = array(        
            'id' => null,
            'id_client' => $id_client,
            'id_bonplan' => $id_bonplan ,
            'code_cadeaux_client' => $iNumUnique ,
            'code_cadeaux_commercant' => $iNumUnique_commercant ,
            'date_validation' => date('Y-m-d'),
            'valide' => "0",
            'date_visit' => $bp_multiple_date_visit_client ,
            'datetime_validation'=>date("Y-m-d H:i:s"),
            'nb_place' => $bp_multiple_nbmax_client ,
            'bonplan_type' => $bonplan_type 
            );

            if ($bp_multiple_date_visit_client!=null) $bp_multiple_date_visit_client = convert_Sqldate_to_Frenchdate($bp_multiple_date_visit_client);
            
            $this->mdlcadeau->demanderBonPlan($data);

            //decrementation nb bonplan 
            $this->mdlbonplan->decremente_quantite_bonplan($id_bonplan, $bp_multiple_nbmax_client);
        
             
            $oClient = $this->user->getById($id_client); 
              
            
            //echo $id_client.$UserType;exit();
            //print_r($oClient);exit();
            $zContactEmail = $oClient->Email;
            $LoginClient = $oClient->Login;
            $NomClient= $oClient->Nom;
            $obonPlan = $this->mdlbonplan->getById($id_bonplan);
            $commercant_id = $obonPlan->bonplan_commercant_id;
            $ionauth_user_pro = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($commercant_id);

            $obj_card_client_number = $this->mdl_card->getWhere(' id_ionauth_user = '.$oClient->user_ionauth_id.' LIMIT 1 ');
            log_message('error', 'num_id_card_virtual : '.$obj_card_client_number);
            if (!isset($obj_card_client_number) || count($obj_card_client_number)==0)
            $client_card_number = generate_client_card($ionauth_id);
            else if (isset($obj_card_client_number[0]->num_id_card_virtual)) $client_card_number = $obj_card_client_number[0]->num_id_card_virtual; //----------------------- client card number
            else $client_card_number = "";

            $commercant = $this->commercant->GetById($commercant_id);
            $commercant_categ = $this->commercant->GetCatById($commercant_id);
            $oVille_com = $this->mdlville->getVilleById($commercant->IdVille);
            $zContactCom = $commercant->Email;
            
            if  ($obonPlan->bonplan_type == '2') {
                $validite_offre_bp = 'Offre valable une seule fois';
            } else $validite_offre_bp = 'Offre valable plusieurs fois';
            
            // EMAIL FOR CLIENT  ****************************************************************
            $zMessage = '<div style="font-family:Arial, Helvetica, sans-serif;">';
            $zMessage .= "<strong><h1>Sortez</h1></strong>\n\n";
            $zMessage .= "<p>";
            $zMessage .= "Votre Numero de Carte : ".$client_card_number;
            $zMessage .= "</p>";
            $zMessage .= "<p>";
            $zMessage .= "Votre Pr&eacute;nom : ".$oClient->Prenom."<br/>";
            $zMessage .= "Votre Nom : ".$oClient->Nom." <br/>";
            $zMessage .= "Votre Adresse : ".$oClient->Adresse."<br/>";
            $zMessage .= "Votre Ville : ".$oClient->NomVille."<br/>";
            $zMessage .= "Votre T&eacute;l&eacute;phone : ".$oClient->Telephone." / ".$oClient->Portable."<br/>";
            $zMessage .= "Votre Email : <a href='mailto:".$oClient->Email."'>".$oClient->Email."</a>";
            $zMessage .= "</p>";
            $zMessage .= "<p><strong>N&deg; du Mail : ".$iNumUnique."</strong></p>";
            $zMessage .= "<p>";
            $zMessage .= "<strong>Votre demande de r&eacute;servation de Bonplan est bien enregistr&eacute;e,</strong><br/>";
            $zMessage .= "<br/><strong>Celui ci vous a &eacute;t&eacute; conc&eacute;d&eacute; par :</strong><br/>";
            $zMessage .= "Nom du Commercant : ".$commercant->Nom." ".$commercant->Prenom."<br/>";
            $zMessage .= "Adresse du Commercant : ".$commercant->Adresse1." ".$commercant->Adresse2."<br/>";
            $zMessage .= "Code Postal : ".$commercant->CodePostal."<br/>";
            $zMessage .= "Ville : ".$oVille_com->Nom."<br/>";
            $zMessage .= "</p>";
            $zMessage .= "<p>";
            $zMessage .= "Ouverture : Uniquement sur Rendez Vous<br/>";
            $zMessage .= "T&eacute;l&eacute;phone : ".$commercant->TelFixe." / ".$commercant->TelDirect." / ".$commercant->TelMobile."<br/>";
            $zMessage .= "Email : ".$commercant->Email."<br/>";
            $zMessage .= "Site internet du Commercant : <a href='".$commercant->SiteWeb."'>".$commercant->SiteWeb."</a><br/>";
            $zMessage .= "</p>";
            $zMessage .= "<p>";
            if (isset($commercant_categ)) $zMessage .= "Dans la cat&eacute;gorie : ".$commercant_categ->rubrique." - ".$commercant_categ->sous_rubrique."<br/>";
            $zMessage .= "<strong>Votre avantage : ".htmlentities($obonPlan->bonplan_titre, ENT_QUOTES, "UTF-8")."</strong><br/>";
            if  ($obonPlan->bonplan_type == '2') {
            	$zMessage .= "<strong>Condition d\'utilisation : Offre valable du ".translate_date_to_fr($obonPlan->bp_unique_date_debut)." au ".translate_date_to_fr($obonPlan->bp_unique_date_fin)." - ".$validite_offre_bp."</strong><br/>";
        	} else if  ($obonPlan->bonplan_type == '2')  {
        		$zMessage .= "<strong>Condition d\'utilisation : Offre valable du ".translate_date_to_fr($obonPlan->bp_multiple_date_debut)." au ".translate_date_to_fr($obonPlan->bp_multiple_date_fin)." - ".$validite_offre_bp."</strong><br/>";
        	}
            $zMessage .= "</p>";
            $zMessage .= "<p>";
            $zMessage .= "Nous vous conseillons de contacter notre diffuseur avant tout d&eacute;placement afin qu\'il vous r&eacute;serve le meilleur accueil.<br/>";
            $zMessage .= "Date pr&eacute;vue de votre visite : ".$bp_multiple_date_visit_client."<br/>";
            $zMessage .= "Nombre de place de votre r&eacute;servation : ".$bp_multiple_nbmax_client."<br/>";
            $zMessage .= "</p>";
            $zMessage .= "<p>";
            $zMessage .= "Pour information, un double de ce mail a &eacute;t&eacute; adress&eacute; &agrave; notre diffuseur.";
            $zMessage .= "</p>";
            $zMessage .= "<p>";
            $zMessage .= "<strong>Nous vous rappelons aussi que cette offre est valable 15 jours &agrave; partir de la date de reception de ce Mail.</strong><br/>";
            $zMessage .= "<strong>N\'oubliez pas d\'imprimer ce Mail et de le pr&eacute;senter &agrave; notre diffuseur.</strong><br/>";
            //$zMessage .= "<strong>Les points cadeaux : n'oubliez pas de récupérer le Mail du commercant, il vous sera utile pour ajouter 10 points sur votre capital-points.</strong><br/>";
            $zMessage .= "</p>";
            $zMessage .= "<p>L'&eacute;quipe Sortez vous souhaite une bonne utilisation de cet avantage </p>";
            $zMessage .= '</div>';
            $zTitre = "[Sortez] Votre code de validation Bon Plan" ;




            
            // EMAIL FOR COMMERCANT ********************************************************************
            $zMessage1 = '<div style="font-family:Arial, Helvetica, sans-serif;">';
            $zMessage1 .= "<strong><h1>Sortez</h1></strong>\n\n";
            $zMessage1 .= "<p>";
            $zMessage1 .= "Numero de la carte du client : ".$client_card_number;
            $zMessage1 .= "</p>";
            $zMessage1 .= "Nom : ".$commercant->Nom."<br/>";
            $zMessage1 .= "Pr&eacute;nom : ".$commercant->Prenom."<br/>";
            $zMessage1 .= "Soci&eacute;t&eacute; : ".$commercant->NomSociete."<br/>";
            $zMessage1 .= "<p><strong>N&deg; du Mail : ".$iNumUnique."</strong></p>";
            $zMessage1 .= "<p>";
            $zMessage1 .= "<strong>Ceci est une demande de r&eacute;servation &agrave; votre Bonplan par l\'un de nos adh&eacute;rents,</strong><br/>";
            $zMessage1 .= "<br/><strong>Nom du Client :</strong> ".$oClient->Nom." ".$oClient->Prenom."<br/>";
            $zMessage1 .= "<strong>Adresse du Client :</strong> ".$oClient->Adresse."<br/>";
            $zMessage1 .= "<strong>Code postal :</strong> ".$oClient->CodePostal."<br/>";
            $zMessage1 .= "<strong>Ville :</strong> ".$oClient->NomVille."<br/>";
            $zMessage1 .= "<strong>T&eacute;l&eacute;phone du Client :</strong> ".$oClient->Telephone." / ".$oClient->Portable."<br/>";
            $zMessage1 .= "<strong>Email du Client :</strong> <a href='mailto:".$oClient->Email."'>".$oClient->Email."</a>";
            $zMessage1 .= "</p>";

            $zMessage1 .= "<p>";
            $zMessage1 .= "<strong>D&eacute;tails de la r&eacute;servation du client : <br/></strong>";
            $zMessage1 .= "Date de visite pr&eacute;vue : ".$bp_multiple_date_visit_client."<br/>";
            $zMessage1 .= "Nombre de place r&eacute;serv&eacute;e : ".$bp_multiple_nbmax_client."<br/>";
            $zMessage1 .= "</p>";

            $zMessage1 .= "<p>";
            $zMessage1 .= "<strong>Nous vous rappelons vos engagements :</strong><br/><br/>";
            if (isset($commercant_categ)) $zMessage1 .= "<strong>Dans la cat&eacute;gorie :</strong> ".$commercant_categ->rubrique." - ".$commercant_categ->sous_rubrique."<br/><br/>";
            $zMessage1 .= "<strong>Votre Bon plan :</strong> ".htmlentities($obonPlan->bonplan_titre, ENT_QUOTES, "UTF-8")."<br/><br/>";
            $zMessage1 .= "<strong>Condition d\'utilisation :</strong> ".$commercant->Conditions."<br/><br/>";
            $zMessage1 .= "Lors de son arriv&eacute; dans votre &eacute;tablissement, le b&eacute;n&eacute;ficiaire vous pr&eacute;sente l\'email adress&eacute; par Sortez (impression ou pr&eacute;sent sur son smartphone).<br/>";
            //$zMessage1 .= "<strong>Les points cadeaux : lors de la concr&eacute;tisation de son achat, vous lui remettez votre propre email, ce qui lui permettra sur son compte personnaliser de rajouter 10 points sur son capital.</strong> <br/>";
            $zMessage1 .= "</p>";
            //$zMessage1 .= "<p>";
            //$zMessage1 .= "<strong>Code de confirmation pour le Consommateur : ". $iNumUnique_commercant."</strong>";
            //$zMessage1 .= "</p>";
            $zMessage1 .= "<p>L\'&eacute;quipe Sortez vous souhaite une bonne journ&eacute;e </p>";
            $zMessage1 .= '</div>';
            $zTitre1 = "[Sortez] Code de mail bon plan unique d\'un client pour acces bon plan" ;
            
            $zContactEmail_ = array();
            $zContactEmail_[] = array("Email"=>$zContactEmail,"Name"=>$NomClient);

            $zContactEmailCom_ = array();
            $zContactEmailCom_[] = array("Email"=>$zContactCom,"Name"=>$commercant->NomSociete);

            $zContactEmailBP_ = array();
            $zContactEmailBP_[] = array("Email"=>"bonplan@sortez.org","Name"=>"Sortez Bon Plan");

            
            $obj_bonplan = $this->mdlbonplan->getById($id_bonplan);
            $obj_commercant = $this->commercant->GetById($obj_bonplan->bonplan_commercant_id);


            if(@envoi_notification($zContactEmail_,$zTitre,html_entity_decode(htmlentities($zMessage)))) {
                @envoi_notification($zContactEmailCom_,$zTitre1,html_entity_decode(htmlentities($zMessage1)));
                @envoi_notification($zContactEmailBP_,$zTitre1,html_entity_decode(htmlentities($zMessage1)));
             
                $data['status'] = "ok";
                echo json_encode($data);
            }else {
                $data['status'] = "error";
                echo json_encode($data);
                     
            }

   }

   public function getcardinfo(){
    $rest_json=file_get_contents("php://input");
    $_POST=json_decode($rest_json,true);
    $iduser = $_POST["id_user"];
    $cardinfo = $this->mdl_card->getByIdUser($iduser);
    $data['card'] = $cardinfo;
    echo json_encode($data);
   }

   public function getFavoris(){
    $rest_json=file_get_contents("php://input");
    $_POST=json_decode($rest_json,true);
    $iduser = $_POST["id_user"];
    $favoris = $this->mdl_api_front_global->getfavoris_by_idUser($iduser);
    $favVerified = [];
    if(!empty($favoris)){
        foreach($favoris as $fav){
            $image_thumbs = "application/resources/front/photoCommercant/imagesbank/".$fav->user_ionauth_id."/"."thumbs/thumb_".$fav->Photo1;
            if(is_file($image_thumbs)){
                $imagecom = "https://www.sortez.org/".$image_thumbs;
            }elseif(is_file("application/resources/front/photoCommercant/imagesbank/".$fav->user_ionauth_id."/".$fav->Photo1)){
                $imagecom = "https://www.sortez.org/application/resources/front/photoCommercant/imagesbank/".$fav->user_ionauth_id."/".$fav->Photo1;
            }elseif(is_file("application/resources/front/photoCommercant/images/".$fav->Photo1)){
                $imagecom = "https://www.sortez.org/application/resources/front/photoCommercant/images/".$fav->Photo1;
            }else{
                $imagecom = GetImagePath("front/") . "/wp71b211d2_06.png";
            }
            $to_push = array(
                "Photo1"=> $imagecom,
                "titre" =>$fav->NomSociete,
                "ville_id" => $fav->IdVille,
                "description" =>htmlspecialchars_decode(strip_tags($fav->Caracteristiques),ENT_QUOTES),
                "id" => $fav->IdCommercant,
                "user_ionauth_id" => $fav->user_ionauth_id, 
                "Photo1" => $imagecom,
                "nom_url" => $fav->nom_url,
            );
            array_push($favVerified,$to_push);
        }
    }
    $data['favoris'] = $favVerified;
    echo json_encode($data);
   } 
}