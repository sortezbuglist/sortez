<?php

class contact extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("mdlannonce");
        $this->load->model("mdlcommercant");
        $this->load->model("mdlbonplan");
        $this->load->model("mdlcategorie");
        $this->load->model("mdlville");
        $this->load->model("mdldepartement");
        $this->load->model("mdlcommercantpagination");
        $this->load->model("sousRubrique");
        $this->load->model("AssCommercantAbonnement");
        $this->load->model("AssCommercantSousRubrique");
        $this->load->model("Commercant");
        $this->load->model("user");
        $this->load->model("mdl_agenda");
        $this->load->model("mdl_categories_agenda");
        $this->load->model("mdlimagespub");
        $this->load->model("Abonnement");
        $this->load->Model("mdlagenda_perso");

        $this->load->library('user_agent');
        $this->load->library("pagination");
        $this->load->library('image_moo');
        $this->load->library('session');

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");


        check_vivresaville_id_ville();


    }

    function index()
    {
        $data['zTitle'] = "Contact";
        $data['mail_sender_adress'] = $this->config->item("mail_sender_adress");
        $this->load->view('privicarte/contact', $data);
    }


    function send_default_mail(){
        $contact_to = $this->input->post("contact_to");
        $contact_from = $this->input->post("contact_from");
        $contact_object = $this->input->post("contact_object");
        $contact_message = $this->input->post("contact_message");
        $contact_message_content = $this->load->view('mail/'.$contact_message);

        $colDestAdmin = array();
        $colDestAdmin[] = array("Email"=>$contact_to,"Name"=>'Contact');
        if (envoi_notification($colDestAdmin,$contact_object,$contact_message_content,$contact_from)) echo '1';
        else echo '0';
    }


    function send_email()
    {

        $contact_privicarte_nom = $this->input->post("contact_privicarte_nom");
        $contact_privicarte_tel = $this->input->post("contact_privicarte_tel");
        $contact_privicarte_mail = $this->input->post("contact_privicarte_mail");
        $contact_privicarte_msg = $this->input->post("contact_privicarte_msg");
        $contact_privicarte_mailto = $this->input->post("contact_privicarte_mailto");
        $contact_privicarte_mailSubject = $this->input->post("contact_privicarte_mailSubject");

        if (!isset($contact_privicarte_mailto) || $contact_privicarte_mailto == "" || $contact_privicarte_mailto == NULL) $contact_privicarte_mailto = "contact@sortez.org";
        if (!isset($contact_privicarte_mail) || $contact_privicarte_mail == "" || $contact_privicarte_mail == NULL) $contact_privicarte_mail = "noreply@sortez.org";
        if (!isset($contact_privicarte_mailSubject) || $contact_privicarte_mailSubject == "" || $contact_privicarte_mailSubject == NULL) $contact_privicarte_mailSubject = 'Contact - Sortez';

        $message_html = '
        <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
        <p>Bonjour,<br/>
        Ceci est une notification de contact de Sortez.org<br/>
        Ci-dessous le contenu,</p>
        <p>Nom : ' . $contact_privicarte_nom . '<br/>
        T&eacute;l&eacute;phone : ' . $contact_privicarte_tel . '<br/>
        Email : ' . $contact_privicarte_mail . '<br/>
        Message : <br/><br/>' . $contact_privicarte_msg . '</p>
        <p><hr><a href="http://Sortez.org/"><img alt="Sortez" src="' . base_url() . '/application/resources/privicarte/images/logo.png"></a></p>
        </div>
        ';

        //echo "<br/>Init<br/>";


        $base_path_system = str_replace('system/', '', BASEPATH);
        //echo $base_path_system; die();

        require_once($base_path_system . 'application/resources/phpmailer/PHPMailerAutoload.php');

        //include("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

        $mail = new PHPMailer();

        //$body             = file_get_contents('contents.html');
        //$body             = eregi_replace("[\]",'',$body);

        $mail->IsHTML(true);
        $mail->CharSet = "text/html; charset=UTF-8;";

        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->Host = "mail.sortez.org"; // SMTP server
        $mail->SMTPDebug = 0;                     // enables SMTP debug information (for testing)
        // 1 = errors and messages
        // 2 = messages only
        $mail->SMTPAuth = true;                  // enable SMTP authentication
        $mail->Host = "ssl0.ovh.net"; // sets the SMTP server
        $mail->SMTPSecure = "ssl";
        $mail->Port = 465;                    // set the SMTP port for the GMAIL server
        $mail->Username = "webmaster@sortez.org"; // SMTP account username
        $mail->Password = 'LM1@*~$"015';        // SMTP account password

        $mail->SetFrom($contact_privicarte_mail, $contact_privicarte_nom);

        $mail->AddReplyTo($contact_privicarte_mail, $contact_privicarte_nom);

        $mail->Subject = $contact_privicarte_mailSubject;

        $mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

        //$mail->MsgHTML($body);
        $mail->MsgHTML($message_html);

        $address = $contact_privicarte_mailto;
        $mail->AddAddress($address, "Contact");

        //$mail->AddAttachment("images/phpmailer.gif");      // attachment
        //$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment

        if (!$mail->Send()) {

            $zContactEmailCom_copy = array();
            $zContactEmailCom_copy[] = array("Email" => $contact_privicarte_mail, "Name" => $contact_privicarte_nom);
            $message_html_copy = '
                <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
                <p>Bonjour,<br/>
                Ceci est une copie de votre contact Sortez.org<br/>
                Ci-dessous le contenu,</p>
                <p>Nom : ' . $contact_privicarte_nom . '<br/>
                T&eacute;l&eacute;phone : ' . $contact_privicarte_tel . '<br/>
                Email : ' . $contact_privicarte_mail . '<br/>
                Message : <br/><br/>' . $contact_privicarte_msg . '</p>
                <p><hr><a href="http://Sortez.org/"><img alt="Sortez" src="http://privicarte.fr/portail/application/resources/privicarte/images/logo.png"></a></p>
                </div>
                ';
            $message_html_copy = html_entity_decode(htmlentities($message_html_copy));
            $sending_mail_privicarte_copy = @envoi_notification($zContactEmailCom_copy, $contact_privicarte_mailSubject, $message_html_copy);


            echo '<span style="color:#FF0000">Une erreur est survenue, veuillez réessayer dans un instant !</span>';//$mail->ErrorInfo
        } else {
            echo '<span style="color:#009900">Votre message a bien été envoyé !</span>';
        }

        //echo "<br/>test phpmailer<br/>";

    }


}

?>