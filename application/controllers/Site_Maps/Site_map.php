<?php
class Site_map extends CI_Controller{
    function __construct() {
        parent::__construct();
        $this->load->model("mdlannonce") ;
        $this->load->model("mdlcommercant") ;
        $this->load->model("mdlbonplan") ;
        $this->load->model("mdlcategorie") ;
        $this->load->model("mdlville") ;
        $this->load->model("mdlcommercantpagination") ;
        $this->load->model("sousRubrique") ;
        $this->load->model("AssCommercantAbonnement") ;
        $this->load->model("AssCommercantSousRubrique") ;
        $this->load->model("Commercant") ;
        $this->load->model("mdl_sitemap") ;
        $this->load->model("mdl_agenda") ;
        $this->load->model("notification") ;
        $this->load->model("mdl_datatourisme_agenda") ;
        $this->load->model("mdl_sitemap");
        $this->load->Model("mdl_card_tampon");
        $this->load->Model("mdl_card_capital");
        $this->load->Model("mdl_card_remise");
        $this->load->library('user_agent');
        $this->load->library("pagination");
        $this->load->library('image_moo');
        $this->load->library('session');
        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");
        $this->load->helpers("clubproximite_helper");
        $this->load->model("Mdl_card");
    }
    function get_all_commercant_1_200(){
    $com_1_200= $this->mdl_sitemap->get_all_commercant_1_200();
        $monfichier = fopen('sitemap.xml', 'a+');
        ftruncate($monfichier,0);
        fputs($monfichier, '<?xml version="1.0" encoding="UTF-8"?>
        <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">');
    foreach ($com_1_200 as $commercant){
     fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/presentation </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
     fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/article </loc><changefreq>daily</changefreq><priority>1.00</priority></url>'."\r\n");
     fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/agenda </loc><changefreq>daily</changefreq><priority>1.00</priority></url>'."\r\n");
     fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/infos </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
     fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/autresinfos </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
     fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/notre_bonplan </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
     fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/reservation </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
     fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/annonces </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
//     $art_com_1_200= $this->mdl_sitemap->get_art_1_200_by_id($commercant->IdCommercant);
//     if (count($art_com_1_200) > 0){
//         foreach ($art_com_1_200 as $article){
//             fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/details_article/ '.$article->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
//         }
//     }
//        $ag_com_1_200= $this->mdl_sitemap->get_ag_1_200_by_id($commercant->IdCommercant);
//        if (count($art_com_1_200) > 0){
//            foreach ($ag_com_1_200 as $agenda){
//                fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/details_agenda/ '.$agenda->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
//            }
//        }
        $bp_com_1_200= $this->mdl_sitemap->get_bp_1_200_by_id($commercant->IdCommercant);
        if (count($bp_com_1_200) > 0){
            foreach ($bp_com_1_200 as $bonplan){
                fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/notre_bonplan/ '.$bonplan->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
            }
        }
        $bout_com_1_200= $this->mdl_sitemap->get_bout_1_200_by_id($commercant->IdCommercant);
        if (count($bout_com_1_200) > 0){
            foreach ($bout_com_1_200 as $boutique){
                fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/detail_annonce-'.$boutique->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
            }
        }
    }
        $this->get_all_commercant_742_1113($monfichier);
        $this->get_commercant_372_742($monfichier);
        $this->get_all_agenda_front($monfichier);
        fputs($monfichier, '</urlset>');
        fclose($monfichier);
    }
    function get_all_agenda_front($monfichier){
        $all_agenda=$this->mdl_sitemap->get_all_agenda();
        foreach ($all_agenda as $agenda){
            fputs($monfichier, '<url><loc>'.base_url().'agenda/details_event/'.$agenda->id.' </loc><changefreq>daily</changefreq><priority>1.00</priority></url>'."\r\n");
        }
    }
    function get_all_commercant_742_1113($monfichier)
    {
        $get_com_742 = $this->mdl_sitemap->get_commercants_742_1113();
        foreach ($get_com_742 as $commercant){
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/presentation </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/article </loc><changefreq>daily</changefreq><priority>1.00</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/agenda </loc><changefreq>daily</changefreq><priority>1.00</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/infos </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/autresinfos </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/notre_bonplan </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/reservation </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/annonces </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
//            $art_com_742_1113= $this->mdl_sitemap->get_art_742_1113_by_id($commercant->IdCommercant);
//            if (!empty($art_com_742_1113)){
//                foreach ($art_com_742_1113 as $article){
//                    fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/details_article/ '.$article->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
//                }
//            }
//            $ag_com_742_1113= $this->mdl_sitemap->get_ag_742_1113_by_id($commercant->IdCommercant);
//            if (!empty($art_com_742_1113)){
//                foreach ($ag_com_742_1113 as $agenda){
//                    fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/details_agenda/ '.$agenda->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
//                }
//            }
            $bp_com_742_1113= $this->mdl_sitemap->get_bp_742_1113_by_id($commercant->IdCommercant);
            if (!empty($bp_com_742_1113)){
                foreach ($bp_com_742_1113 as $bonplan){
                    fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/notre_bonplan/ '.$bonplan->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
                }
            }
            $bout_com_742_1113= $this->mdl_sitemap->get_bout_742_1113_by_id($commercant->IdCommercant);
            if (!empty($bout_com_742_1113)){
                foreach ($bout_com_742_1113 as $boutique){
                    fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/detail_annonce-'.$boutique->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
                }
            }
        }
        $this->get_all_fidelity($monfichier);
    }
    function get_all_fidelity($monfichier){
        $commercant = $this->mdl_sitemap->get_all_commercants();
//        var_dump($commercant);die('koko');
        foreach ($commercant as $icommercant){
            $objTampon = $this->mdl_card_tampon->getByIdCommercant($icommercant->IdCommercant);
            if (isset($objTampon) AND $objTampon->is_activ !=0 AND $objTampon->is_activ !=null){
                fputs($monfichier, '<url><loc>'.base_url().$icommercant->nom_url.'/tampon/'.$objTampon->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
            }
            $objCapital = $this->mdl_card_capital->getByIdCommercant($icommercant->IdCommercant);
            if (isset($objCapital) AND $objCapital->is_activ !=0 AND $objCapital->is_activ !=null){
                fputs($monfichier, '<url><loc>'.base_url().$icommercant->nom_url.'/capital/'.$objCapital->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
            }
            $objRemise = $this->mdl_card_remise->getByIdCommercant($icommercant->IdCommercant);
            if (isset($objRemise) AND $objRemise->is_activ !=0 AND $objRemise->is_activ !=null){
                fputs($monfichier, '<url><loc>'.base_url().$icommercant->nom_url.'/remise/'.$objRemise->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
            }
        }
    }
    function get_commercant_372_742($monfichier){
        $toInfoCommercant = $this->mdl_sitemap->get_commercant_372_742();
        foreach ($toInfoCommercant as $commercant){
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/presentation </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/article </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/agenda </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/notre_bonplan </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/annonces </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/infos </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/autresinfos </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/reservation </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
//            $art_com_372_742= $this->mdl_sitemap->get_art_372_742_by_idCom($commercant->IdCommercant);
//            if (!empty($art_com_372_742)){
//                foreach ($art_com_372_742 as $article){
//                    fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/details_article/ '.$article->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
//                }
//            }
//            $ag_com_1_200= $this->mdl_sitemap->get_ag_372_742_by_idCom($commercant->IdCommercant);
//            if (!empty($ag_com_1_200)){
//                foreach ($ag_com_1_200 as $agenda){
//                    fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/details_agenda/ '.$agenda->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
//                }
//            }
            $bp_com_1_200= $this->mdl_sitemap->get_bp_372_742_by_idCom($commercant->IdCommercant);
            if (!empty($bp_com_1_200)){
                foreach ($bp_com_1_200 as $bonplan){
                    fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/notre_bonplan/ '.$bonplan->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
                }
            }
            $bout_com_1_200= $this->mdl_sitemap->get_bout_372_742_by_idCom($commercant->IdCommercant);
            if (!empty($bout_com_1_200)){
                foreach ($bout_com_1_200 as $boutique){
                    fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/detail_annonce-'.$boutique->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
                }
            }
        }
        //$this->get_article($monfichier);
    }
    function get_article($monfichier){
        $toInfoArticle = $this->mdl_sitemap->get_all_article();
        if (!empty($toInfoArticle)){
        foreach ($toInfoArticle as $article){
            fputs($monfichier, '<url><loc>'.base_url()."article/details/".$article->id.'</loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
        }
        }
    }
}