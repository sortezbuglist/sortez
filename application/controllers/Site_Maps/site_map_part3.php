<?php

class site_map_part3 extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("mdl_sitemap_part3");
        $this->load->Model("mdl_card_tampon");
        $this->load->Model("mdl_card_capital");
        $this->load->Model("mdl_card_remise");

    }


    function get_all_commercant_742_1113()
    {
        $get_com_742 = $this->mdl_sitemap_part3->get_commercants_742_1113();
        $monfichier = fopen('sitemaps3.xml', 'a+');
        ftruncate($monfichier,0);
        foreach ($get_com_742 as $commercant){
            echo '<url><loc>'.base_url().$commercant->nom_url.'/presentation </loc><changefreq>daily</changefreq><priority>1.00</priority></url><br>';
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/presentation </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/article </loc><changefreq>daily</changefreq><priority>1.00</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/agenda </loc><changefreq>daily</changefreq><priority>1.00</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/infos </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/autresinfos </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/notre_bonplan </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/reservation </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/annonces </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            $art_com_742_1113= $this->mdl_sitemap_part3->get_art_742_1113_by_id($commercant->IdCommercant);
            if (!empty($art_com_742_1113)){
                foreach ($art_com_742_1113 as $article){
                    fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/details_article/ '.$article->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
                }
            }

            $ag_com_742_1113= $this->mdl_sitemap_part3->get_ag_742_1113_by_id($commercant->IdCommercant);
            if (!empty($art_com_742_1113)){
                foreach ($ag_com_742_1113 as $agenda){
                    fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/details_agenda/ '.$agenda->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
                }
            }

            $bp_com_742_1113= $this->mdl_sitemap_part3->get_bp_742_1113_by_id($commercant->IdCommercant);
            if (!empty($bp_com_742_1113)){
                foreach ($bp_com_742_1113 as $bonplan){
                    fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/notre_bonplan/ '.$bonplan->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
                }
            }

            $bout_com_742_1113= $this->mdl_sitemap_part3->get_bout_742_1113_by_id($commercant->IdCommercant);
            if (!empty($bout_com_742_1113)){
                foreach ($bout_com_742_1113 as $boutique){
                    fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/detail_annonce-'.$boutique->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
                }
            }
        }
        $this->get_all_fidelity($monfichier);
        fclose($monfichier);
    }
    function get_all_fidelity($monfichier){

        $commercant = $this->mdl_sitemap_part3->get_all_commercants();
//        var_dump($commercant);die('koko');
        foreach ($commercant as $icommercant){
            $objTampon = $this->mdl_card_tampon->getByIdCommercant($icommercant->IdCommercant);
            if (isset($objTampon) AND $objTampon->is_activ !=0 AND $objTampon->is_activ !=null){
                //var_dump($objTampon);
                //echo base_url().$icommercant->nom_url.'/tampon/'.$objTampon->id.'<br>';
                fputs($monfichier, '<url><loc>'.base_url().$icommercant->nom_url.'/tampon/'.$objTampon->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
            }
            $objCapital = $this->mdl_card_capital->getByIdCommercant($icommercant->IdCommercant);
            if (isset($objCapital) AND $objCapital->is_activ !=0 AND $objCapital->is_activ !=null){
                //var_dump($objCapital);
                //echo base_url().$icommercant->nom_url.'/capital/'.$objCapital->id.'<br>';
                fputs($monfichier, '<url><loc>'.base_url().$icommercant->nom_url.'/capital/'.$objCapital->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
            }
            $objRemise = $this->mdl_card_remise->getByIdCommercant($icommercant->IdCommercant);
            if (isset($objRemise) AND $objRemise->is_activ !=0 AND $objRemise->is_activ !=null){
                //var_dump($objRemise);
                //echo base_url().$icommercant->nom_url.'/remise/'.$objRemise->id.'<br>';
                fputs($monfichier, '<url><loc>'.base_url().$icommercant->nom_url.'/remise/'.$objRemise->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
            }
        }
    }

}