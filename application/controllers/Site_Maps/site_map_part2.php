<?php
class Site_map_part2 extends CI_Controller{

    function __construct() {
        parent::__construct();
        $this->load->model("mdlannonce") ;
        $this->load->model("mdlcommercant") ;
        $this->load->model("mdlbonplan") ;
        $this->load->model("mdlcategorie") ;
        $this->load->model("mdlville") ;
        $this->load->model("mdlcommercantpagination") ;
        $this->load->model("sousRubrique") ;
        $this->load->model("AssCommercantAbonnement") ;
        $this->load->model("AssCommercantSousRubrique") ;
        $this->load->model("Commercant") ;
        $this->load->model("user") ;
        $this->load->model("mdl_agenda") ;
        $this->load->model("notification") ;
        $this->load->model("mdl_datatourisme_agenda") ;

        $this->load->library('user_agent');

        $this->load->library("pagination");

        $this->load->library('image_moo');

        $this->load->library('session');

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");
        $this->load->helpers("clubproximite_helper");
        $this->load->model("Mdl_card");
        $this->load->model("Mdl_sitemap");
        $this->load->model("Mdl_siteMap_part2");


        check_vivresaville_id_ville();


    }

    function get_commercant_372_742(){
        $toInfoCommercant = $this->Mdl_sitemap->get_commercant_372_742();
        foreach ($toInfoCommercant as $commercant){
            //echo base_url().$commercant->nom_url.'/presentation<br>';
            $monfichier = fopen('get_commercant_372_742.xml', 'a+');
            ftruncate($monfichier,0);
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/presentation </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/article </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/agenda </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/notre_bonplan </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/annonces </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/infos </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/autresinfos </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
            fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/reservation </loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");

            $art_com_372_742= $this->Mdl_sitemap->get_art_372_742_by_idCom($commercant->IdCommercant);
            if (!empty($art_com_372_742)){
                foreach ($art_com_372_742 as $article){
                    fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/details_article/ '.$article->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
                }
            }

            $ag_com_1_200= $this->Mdl_sitemap->get_ag_372_742_by_idCom($commercant->IdCommercant);
            if (!empty($ag_com_1_200)){
                foreach ($ag_com_1_200 as $agenda){
                    fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/details_agenda/ '.$agenda->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
                }
            }

            $bp_com_1_200= $this->Mdl_sitemap->get_bp_372_742_by_idCom($commercant->IdCommercant);
            if (!empty($bp_com_1_200)){
                foreach ($bp_com_1_200 as $bonplan){
                    fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/notre_bonplan/ '.$bonplan->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
                }
            }

            $bout_com_1_200= $this->Mdl_sitemap->get_bout_372_742_by_idCom($commercant->IdCommercant);
            if (!empty($bout_com_1_200)){
                foreach ($bout_com_1_200 as $boutique){
                    fputs($monfichier, '<url><loc>'.base_url().$commercant->nom_url.'/detail_annonce-'.$boutique->id."</loc><changefreq>daily</changefreq><priority>1.00</priority></url>\r\n");
                }
            }
        }
        $this->get_article($monfichier);
        fclose($monfichier);
    }

    //article
    function get_article($monfichier){
        $toInfoArticle = $this->Mdl_siteMap_part2->get_all_article();
        foreach ($toInfoArticle as $article){
            //echo base_url()."article/details/".$article->id."<br>";
            fputs($monfichier, '<url><loc>'.base_url()."article/details/".$article->id.'</loc><changefreq>daily</changefreq><priority>0.50</priority></url>'."\r\n");
        }
    }
}