<?php class commercant extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("mdlcommercant");
        $this->load->model("Mdl_news_letter_commercant");
        $this->load->model("mdlville") ;
        $this->load->model("mdlbonplan") ;

        $this->load->model("mdlfidelity") ;

        $this->load->model("mdl_card_remise") ;
        $this->load->model("mdl_card_tampon") ;
        $this->load->model("mdl_card_capital") ;

        $this->load->model("mdlville") ;

        $this->load->model("mdlbonplans") ;
        $this->load->model("mdlcadeau") ;
        $this->load->model("Mdl_soutenons") ;

        $this->load->model("mdl_article_datetime") ;
        $this->load->model("mdl_agenda_datetime") ;

        $this->load->model("mdl_categories_agenda") ;

        $this->load->model("mdlannonce") ;
        $this->load->model("mdlarticle") ;
        $this->load->model("mdl_agenda") ;
        $this->load->model("rubrique") ;
        $this->load->model("mdlimagespub") ;

        $this->load->model("Abonnement");
        $this->load->library('user_agent');

        $this->load->library('ion_auth');
        
        $this->load->model("ion_auth_used_by_club");

        $this->load->model("Mdl_plat_du_jour");
        $this->load->model("Mdl_card");
        $this->load->model("User");

        $this->load->library('session');
        $this->load->helper('clubproximite');
        $this->load->model('Mdl_reservation');
        $this->load->library("pagination");

        check_vivresaville_id_ville();


    }
    function photoCommercant($_iCommercantId){

        $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
        $data['oInfoCommercant'] = $oInfoCommercant ;

        $data['active_link'] = "activite1";
        $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);


        //$this->load->view('front/vwPhotoCommercant', $data) ;
        $this->load->view('front2013/photoscommercant', $data) ;
    }


    function videoCommercant($_iCommercantId){

        $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
        $data['oInfoCommercant'] = $oInfoCommercant ;

        $data['active_link'] = "accueil";
        $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user") ;
        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser=0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId) ;
        $data['toBonPlan'] = $toBonPlan ;

        //$data['active_link2'] = '1';


        //$this->load->view('front/vwVideoCommercant', $data) ;
        $this->load->view('front2013/videocommercant', $data) ;
    }

    function plusInfoCommercant($_iCommercantId){

        $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
        $data['oInfoCommercant'] = $oInfoCommercant ;

        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;

        $data['active_link'] = "activite1";
        $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user") ;
        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser=0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $data['current_page'] = "page_infos";
        $data['pagecategory'] = "pro";

        $data['pageglissiere'] = "page1";
        $this->load->model("mdlglissiere") ;
        $data['mdlglissiere'] = $this->mdlglissiere;

        $data['link_partner_current_page'] = 'page1';

        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId) ;
        $data['toBonPlan'] = $toBonPlan ;

        //ajout bouton rond noire annonce/agenda
        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
        else $data['result_check_commercant_annonce'] = '0';

        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
        else $data['result_check_commercant_agenda'] = '0';

//        $is_reserved_on=$this->Mdl_plat_du_jour->verify_reserved($_iCommercantId);
//        $data['reservation']=$is_reserved_on;

        $get_data_all__content=$this->Mdl_plat_du_jour->get_all_plat_by_idcom($_iCommercantId);
        $data['reservation']=$get_data_all__content;

        //$this->load->view('front/vwPlusInfoCommercant', $data) ;
        //$this->load->view('front2013/plusinfoscommercant', $data) ;
        $this->load->view('front_soutenons/plusinfoscommercant', $data) ;
    }

    function plusInfo2Commercant($_iCommercantId){

        $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
        $data['oInfoCommercant'] = $oInfoCommercant ;

        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;

        $data['active_link'] = "activite2";
        $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;


        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user") ;
        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser=0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $data['current_page'] = "page_infos2";

        $data['pageglissiere'] = "page2";
        $this->load->model("mdlglissiere") ;
        $data['mdlglissiere'] = $this->mdlglissiere;

        $data['link_partner_current_page'] = 'page2';

        $data['pagecategory'] = "pro";

        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId) ;
        $data['toBonPlan'] = $toBonPlan ;

        //ajout bouton rond noire annonce/agenda
        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
        else $data['result_check_commercant_annonce'] = '0';

        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
        else $data['result_check_commercant_agenda'] = '0';

//        $is_reserved_on=$this->Mdl_plat_du_jour->verify_reserved($_iCommercantId);
//        $data['reservation']=$is_reserved_on;
        $get_data_all__content=$this->Mdl_plat_du_jour->get_all_plat_by_idcom($_iCommercantId);
        $data['reservation']=$get_data_all__content;

        //$this->load->view('front/vwPlusInfo2Commercant', $data) ;
        //$this->load->view('front2013/plusinfos2commercant', $data) ;
        $this->load->view('front_soutenons/plusinfos2commercant', $data) ;
    }

    function mentionslegalesmobile($_iCommercantId){

        $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
        $data['oInfoCommercant'] = $oInfoCommercant ;


        $data['active_link'] = "activite2";
        $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);

        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;


        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);


        $this->load->view('front/vwMentionslegalesmobile', $data) ;
    }

    function nousSituerCommercant($_iCommercantId){

        /*
        // Load the library
        $this->load->library('googlemaps');
        // Initialize our map. Here you can also pass in additional parameters for customising the map (see below)
        $this->googlemaps->initialize();
        // Create the map. This will return the Javascript to be included in our pages <head></head> section and the HTML code to be
        // placed where we want the map to appear.
        // Set the marker parameters as an empty array. Especially important if we are using multiple markers
        $marker = array();
        // Specify an address or lat/long for where the marker should appear.
        //$marker['position '] = 'Crescent Park, Palo Alto';
        $zExemple = $marker['position '] = 'Crescent Park, Palo Alto';
        print_r($zExemple) ;
        // $marker['position'] = 'France, France, France';
        // Once all the marker parameters have been specified lets add the marker to our map
        $this->googlemaps->add_marker($marker);
        $data['map'] = $this->googlemaps->create_map();
        // Load our view, passing the map data that has just been created
        //$this->load->view('my_view', $data);
        */

        $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
        $data['mdlville'] = $this->mdlville ;
        $data['oInfoCommercant'] = $oInfoCommercant ;

        $data['active_link'] = "situer";
        $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;


        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user") ;
        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser=0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId) ;
        $data['toBonPlan'] = $toBonPlan ;


        //$this->load->view('front/vwNousSituerCommercant', $data) ;
        $this->load->view('front2013/noussituercommercant', $data) ;
    }


    function coordonneeshoraires($_iCommercantId){

        $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
        $data['mdlville'] = $this->mdlville ;
        $data['oInfoCommercant'] = $oInfoCommercant ;

        $data['active_link'] = "accueil";
        $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $this->load->view('front2013/coordonneeshoraires', $data) ;
    }

    function invalid_account() {
        $data['title'] = "Compte invalide";
        $data["zTitle"] = 'Erreur, Compte invalide';
        $this->load->view('privicarte/invalid_account', $data) ;
    }


    function presentation($_iCommercantId) {
        statistiques();
        $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);
        // var_dump($_iCommercantId); die("STOP");
        if (!isset($_iCommercantId) || $_iCommercantId == NULL || $_iCommercantId == "") redirect("front/commercant/invalid_account/");

        $oInfoCommercant = $this->mdlcommercant->infoCommercant(intval($_iCommercantId)) ;
        $nbPhoto =0;
        if($oInfoCommercant->Photo1!="" && $oInfoCommercant->Photo1!=null){
            $nbPhoto+=1;
        }
        if($oInfoCommercant->Photo2!="" && $oInfoCommercant->Photo2!=null){
            $nbPhoto+=1;
        }
        if($oInfoCommercant->Photo3!="" && $oInfoCommercant->Photo3!=null){
            $nbPhoto+=1;
        }
        if($oInfoCommercant->Photo4!="" && $oInfoCommercant->Photo4!=null){
            $nbPhoto+=1;
        }
        if($oInfoCommercant->Photo5!="" && $oInfoCommercant->Photo5!=null){
            $nbPhoto+=1;
        }
        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;
        $data['oInfoCommercant'] = $oInfoCommercant ;
        $data['nbPhoto'] = $nbPhoto ;

        //$data['mdlannonce'] = $this->mdlannonce ;
        $data['mdlbonplan'] = $this->mdlbonplan ;

        $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oBonPlan = 	$this->mdlbonplans->bonPlanParCommercant($_iCommercantId);
        $data['nbBonPlan'] = sizeof($oBonPlan);
        //		$data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_titre : "";		// OP 27/11/2011
        $data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_texte : "";
        $data['oImagespub'] = $this->mdlimagespub->GetByImagespubActiv() ;
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $oLastbonplanCom = 	$this->mdlbonplans->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $data['active_link'] = "presentation";

        $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user") ;
        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser=0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));
        $data['pagecategory'] = "pro";
        $data['link_partner_current_page'] = 'presentation';


        $data['pageglissiere'] = "presentation";
        $this->load->model("mdlglissiere");
        $data['mdlglissiere'] = $this->mdlglissiere;

        $data['current_partner_menu'] = "presentation_commercants";

        $toBonPlan = $this->mdlbonplans->getListeBonPlan($_iCommercantId) ;
        $data['toBonPlan'] = $toBonPlan ;

        //ajout bouton rond noire annonce/agenda
        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
        else $data['result_check_commercant_annonce'] = '0';

        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
        else $data['result_check_commercant_agenda'] = '0';

//        $is_reserved_on=$this->Mdl_plat_du_jour->verify_reserved($_iCommercantId);
//        $data['reservation']=$is_reserved_on;
        $get_data_all__content=$this->Mdl_plat_du_jour->get_all_plat_by_idcom($_iCommercantId);
        $data['reservation']=$get_data_all__content;
        $toListeFidelity_remise = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "remise");
        $toListeFidelity_tampon = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "tampon");
        $toListeFidelity_capital = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "capital");
        $toListeBonPlan_all_pvc = $this->mdlbonplan->listeBonPlanRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "","");
        $toListePlat_du_jour = $this->Mdl_plat_du_jour->listeplatRecherche( "", "", "",  0, 10000, "", "", "", "", $_iCommercantId,"");

        $i = 0;
        if ($toListeFidelity_remise !=[]){
            foreach ($toListeFidelity_remise as $listremise){
                $allarrays[$i]['id'] = $listremise->id;
                $allarrays[$i]['titre'] = $listremise->titre;
                $allarrays[$i]['description'] = $listremise->description;
                $allarrays[$i]['image1'] = $listremise->image1;
                $allarrays[$i]['date_fin'] = $listremise->date_fin;
                $allarrays[$i]['type'] = "remise";
                $allarrays[$i]['ville_nom'] = $listremise->ville;
                $allarrays[$i]['ville_id'] = $listremise->IdVille;
                $allarrays[$i]['idcom'] = $listremise->IdCommercant;
                $allarrays[$i]['partenaire'] = $listremise->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listremise->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_tampon !=[]){
            foreach ($toListeFidelity_tampon as $listtampon){
                $allarrays[$i]['id'] = $listtampon->id;
                $allarrays[$i]['titre'] = $listtampon->titre;
                $allarrays[$i]['description'] = $listtampon->description;
                $allarrays[$i]['image1'] = $listtampon->image1;
                $allarrays[$i]['date_fin'] = $listtampon->date_fin;
                $allarrays[$i]['type'] = "tampon";
                $allarrays[$i]['ville_nom'] = $listtampon->ville;
                $allarrays[$i]['ville_id'] = $listtampon->IdVille;
                $allarrays[$i]['idcom'] = $listtampon->IdCommercant;
                $allarrays[$i]['partenaire'] = $listtampon->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listtampon->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_capital !=[]){
            foreach ($toListeFidelity_capital as $listcapitel){
                $allarrays[$i]['id'] = $listcapitel->id;
                $allarrays[$i]['titre'] = $listcapitel->titre;
                $allarrays[$i]['description'] = $listcapitel->description;
                $allarrays[$i]['image1'] = $listcapitel->image1;
                $allarrays[$i]['date_fin'] = $listcapitel->date_fin;
                $allarrays[$i]['type'] = "capital";
                $allarrays[$i]['ville_nom'] = $listcapitel->ville;
                $allarrays[$i]['ville_id'] = $listcapitel->IdVille;
                $allarrays[$i]['idcom'] = $listcapitel->IdCommercant;
                $allarrays[$i]['partenaire'] = $listcapitel->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listcapitel->IdCommercant)->nom_url;
                $i++;
            }
        }
        if ($toListeBonPlan_all_pvc !=[]){
            foreach ($toListeBonPlan_all_pvc as $bonplan){
                //var_dump($bonplan);die();
                if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "2"){
                    $date = $bonplan->bp_unique_date_fin;
                    //$type = "Bonplan unique";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "1"){
                    $date = $bonplan->bonplan_date_fin;
                    //$type = "Bonplan simple";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "3"){
                    $date = $bonplan->bp_multiple_date_fin;
                    //$type = "Bonplan multiples";
                }

                $allarrays[$i]['id'] = $bonplan->bonplan_id;
                $allarrays[$i]['titre'] = $bonplan->bonplan_titre;
                $allarrays[$i]['description'] = $bonplan->bonplan_texte;
                $allarrays[$i]['image1'] = $bonplan->bonplan_photo1;
                $allarrays[$i]['date_fin'] = $date;
                $allarrays[$i]['type'] = $bonplan->bonplan_type;
                $allarrays[$i]['ville_nom'] = $this->mdlville->getVilleById($this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation)->Nom;
                $allarrays[$i]['ville_id'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation;
                $allarrays[$i]['idcom'] = $bonplan->bonplan_commercant_id;
                $allarrays[$i]['partenaire'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->nom_url;
                $i++;
            }
        }
        if ($toListePlat_du_jour !=[]){
            foreach ($toListePlat_du_jour as $plat){
                $allarrays[$i]['id'] = $plat->id;
                $allarrays[$i]['titre'] = $plat->rubrique;
                $allarrays[$i]['description'] = $plat->description_plat;
                $allarrays[$i]['image1'] = $plat->photo;
                $allarrays[$i]['date_fin'] = $plat->date_fin_plat;
                $allarrays[$i]['inputDatedebut'] = $plat->date_fin_plat;
                $allarrays[$i]['type'] = "plat";
                $allarrays[$i]['nbre_plat_propose'] = $plat->nbre_plat_propose;
                $allarrays[$i]['ville_nom'] = $plat->ville;
                $allarrays[$i]['ville_id'] = $plat->IdVille;
                $allarrays[$i]['idcom'] = $plat->IdCommercant;
                $allarrays[$i]['partenaire'] = $plat->NomSociete;
                $allarrays[$i]['prix_plat'] = $plat->prix_plat;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($plat->IdCommercant)->nom_url;
                $i++;
            }
        }
        $data['alldata'] = $allarrays;
        //var_dump($data);

        //$this->load->view('front/vwPresentationCommercant', $data) ;
        //$this->load->view('front2013/presentationcommercant', $data) ;
        //var_dump($data);die("zehahaha debug");
        $this->load->view('front_soutenons/presentation_commercants', $data);

    }

    function listeBonPlanParCommercant($_iCommercantId, $bonplan_id = 0, $mssg = 0){

        $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            $data['id_client'] = $iduser ;
        }

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);
        $data['oInfoCommercant'] = $oInfoCommercant ;
        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;
        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId);
        $data['toBonPlan'] = $toBonPlan;



        if ($bonplan_id == 0) {
            $oBonPlan2 = $this->mdlbonplan->lastBonplanCom2($_iCommercantId);
        } else {
            $oBonPlan2 = $this->mdlbonplan->getById($bonplan_id);
        }
        $data['oBonPlan'] = $oBonPlan2;
        $data['toBonPlan2'] = $oBonPlan2;



        $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oBonPlan = 	$this->mdlbonplan->bonPlanParCommercant($_iCommercantId);
        $data['nbBonPlan'] = sizeof($oBonPlan);

        $data['active_link'] = "bonplans";
        $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user") ;
        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser=0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));

        //lien pour retour automatique
        $page_from = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $_SESSION['page_from'] = $page_from;
        //lien pour retour automatique

        $data['mssg'] = $mssg;
        $data['pagecategory_partner'] = 'bonplans_partner';


        if (isset($_POST['text_mail_form_module_detailbonnplan'])) {
            $text_mail_form_module_detailbonnplan = $this->input->post("text_mail_form_module_detailbonnplan") ;
            $nom_mail_form_module_detailbonnplan = $this->input->post("nom_mail_form_module_detailbonnplan") ;
            $tel_mail_form_module_detailbonnplan = $this->input->post("tel_mail_form_module_detailbonnplan") ;
            $email_mail_form_module_detailbonnplan = $this->input->post("email_mail_form_module_detailbonnplan") ;

            $colDestAdmin = array();
            $colDestAdmin[] = array("Email"=>$oInfoCommercant->Email,"Name"=>$oInfoCommercant->Nom." ".$oInfoCommercant->Prenom);

            // Sujet
            $txtSujetAdmin = "Demande d'information sur un Bonplan Privicarte";

            $txtContenuAdmin = "
            <p>Bonjour ,</p>
            <p>Une demande d'information vous est adressé suite au bon plan que vous avez déposé sur Privicarte.fr</p>
            <p>Détails du bonplan<br/>
            Désignation : ".$oBonPlan2->bonplan_titre."<br/>
            N° : ".ajoutZeroPourString($oBonPlan2->bonplan_id, 6)." du ".convertDateWithSlashes($oBonPlan2->bonplan_date_debut)."
            </p><p>
            Nom Client : ".$nom_mail_form_module_detailbonnplan."<br/>
            Téléphone Client : ".$tel_mail_form_module_detailbonnplan."<br/>
            Email Client : ".$email_mail_form_module_detailbonnplan."<br/><br/>
            Demande Client :<br/>
            ".$text_mail_form_module_detailbonnplan."<br/><br/>
            </p>";

            @envoi_notification($colDestAdmin,$txtSujetAdmin,$txtContenuAdmin);
            $data['mssg_envoi_module_detail_bonplan'] = '<font color="#00CC00">Votre demande est envoyée</font>';
            //$data['mssg_envoi_module_detail_bonplan'] = $txtContenuAdmin;

        } else $data['mssg_envoi_module_detail_bonplan'] = '';

        $data['pagecategory_partner'] = 'bonplans_partner';

        $data['pagecategory'] = "pro";

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $data['link_partner_current_page'] = 'bonplan';

        $data['current_partner_menu'] = "bonplan";


        //ajout bouton rond noire annonce/agenda
        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
        else $data['result_check_commercant_annonce'] = '0';

        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
        else $data['result_check_commercant_agenda'] = '0';


        if ($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $this->load->model("assoc_client_bonplan_model") ;
            $user_ion_auth_verif = $this->ion_auth->user()->row();
            $iduser_verif = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth_verif->id);
            $data['bonplan_unique_verification'] = $this->assoc_client_bonplan_model->get_where(array('assoc_client_bonplan.id_client'=>$iduser_verif,'assoc_client_bonplan.id_bonplan'=>$bonplan_id));
        }
        $toListeFidelity_remise = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "remise");
        $toListeFidelity_tampon = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "tampon");
        $toListeFidelity_capital = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "capital");
        $toListeBonPlan_all_pvc = $this->mdlbonplan->listeBonPlanRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "","");
        $toListePlat_du_jour = $this->Mdl_plat_du_jour->listeplatRecherche( "", "", "",  0, 10000, "", "", "", "", $_iCommercantId,"");

        $i = 0;
        if ($toListeFidelity_remise !=[]){
            foreach ($toListeFidelity_remise as $listremise){
                $allarrays[$i]['id'] = $listremise->id;
                $allarrays[$i]['titre'] = $listremise->titre;
                $allarrays[$i]['description'] = $listremise->description;
                $allarrays[$i]['image1'] = $listremise->image1;
                $allarrays[$i]['date_fin'] = $listremise->date_fin;
                $allarrays[$i]['type'] = "remise";
                $allarrays[$i]['ville_nom'] = $listremise->ville;
                $allarrays[$i]['ville_id'] = $listremise->IdVille;
                $allarrays[$i]['idcom'] = $listremise->IdCommercant;
                $allarrays[$i]['partenaire'] = $listremise->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listremise->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_tampon !=[]){
            foreach ($toListeFidelity_tampon as $listtampon){
                $allarrays[$i]['id'] = $listtampon->id;
                $allarrays[$i]['titre'] = $listtampon->titre;
                $allarrays[$i]['description'] = $listtampon->description;
                $allarrays[$i]['image1'] = $listtampon->image1;
                $allarrays[$i]['date_fin'] = $listtampon->date_fin;
                $allarrays[$i]['type'] = "tampon";
                $allarrays[$i]['ville_nom'] = $listtampon->ville;
                $allarrays[$i]['ville_id'] = $listtampon->IdVille;
                $allarrays[$i]['idcom'] = $listtampon->IdCommercant;
                $allarrays[$i]['partenaire'] = $listtampon->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listtampon->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_capital !=[]){
            foreach ($toListeFidelity_capital as $listcapitel){
                $allarrays[$i]['id'] = $listcapitel->id;
                $allarrays[$i]['titre'] = $listcapitel->titre;
                $allarrays[$i]['description'] = $listcapitel->description;
                $allarrays[$i]['image1'] = $listcapitel->image1;
                $allarrays[$i]['date_fin'] = $listcapitel->date_fin;
                $allarrays[$i]['type'] = "capital";
                $allarrays[$i]['ville_nom'] = $listcapitel->ville;
                $allarrays[$i]['ville_id'] = $listcapitel->IdVille;
                $allarrays[$i]['idcom'] = $listcapitel->IdCommercant;
                $allarrays[$i]['partenaire'] = $listcapitel->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listcapitel->IdCommercant)->nom_url;
                $i++;
            }
        }
        if ($toListeBonPlan_all_pvc !=[]){
            foreach ($toListeBonPlan_all_pvc as $bonplan){
                //var_dump($bonplan);die();
                if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "2"){
                    $date = $bonplan->bp_unique_date_fin;
                    //$type = "Bonplan unique";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "1"){
                    $date = $bonplan->bonplan_date_fin;
                    //$type = "Bonplan simple";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "3"){
                    $date = $bonplan->bp_multiple_date_fin;
                    //$type = "Bonplan multiples";
                }

                $allarrays[$i]['id'] = $bonplan->bonplan_id;
                $allarrays[$i]['titre'] = $bonplan->bonplan_titre;
                $allarrays[$i]['description'] = $bonplan->bonplan_texte;
                $allarrays[$i]['image1'] = $bonplan->bonplan_photo1;
                $allarrays[$i]['date_fin'] = $date;
                $allarrays[$i]['type'] = $bonplan->bonplan_type;
                $allarrays[$i]['ville_nom'] = $this->mdlville->getVilleById($this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation)->Nom;
                $allarrays[$i]['ville_id'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation;
                $allarrays[$i]['idcom'] = $bonplan->bonplan_commercant_id;
                $allarrays[$i]['partenaire'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->nom_url;
                $i++;
            }
        }
        if ($toListePlat_du_jour !=[]){
            foreach ($toListePlat_du_jour as $plat){
                $allarrays[$i]['id'] = $plat->id;
                $allarrays[$i]['titre'] = $plat->rubrique;
                $allarrays[$i]['description'] = $plat->description_plat;
                $allarrays[$i]['image1'] = $plat->photo;
                $allarrays[$i]['date_fin'] = $plat->date_fin_plat;
                $allarrays[$i]['inputDatedebut'] = $plat->date_fin_plat;
                $allarrays[$i]['type'] = "plat";
                $allarrays[$i]['nbre_plat_propose'] = $plat->nbre_plat_propose;
                $allarrays[$i]['ville_nom'] = $plat->ville;
                $allarrays[$i]['ville_id'] = $plat->IdVille;
                $allarrays[$i]['idcom'] = $plat->IdCommercant;
                $allarrays[$i]['partenaire'] = $plat->NomSociete;
                $allarrays[$i]['prix_plat'] = $plat->prix_plat;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($plat->IdCommercant)->nom_url;
                $i++;
            }
        }
        $data['alldata'] = $allarrays;


        //$this->load->view('front/vwBonPlanParCommercant', $data) ;
        //$this->load->view('front2013/page_bonplan_partenaire', $data) ;
        $this->load->view('front_soutenons/details_bonplan_front', $data) ;
    }
    function recommanderAmi($_iCommercantId){

        $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant

        redirect(site_url($nom_url_commercant));

        if ($nom_url_commercant!="agenda" && $nom_url_commercant!="article") {

            $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

            $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
            $data['oInfoCommercant'] = $oInfoCommercant ;


            $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId) ;
            $data['toBonPlan'] = $toBonPlan ;

            $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
            $oBonPlan = 	$this->mdlbonplan->bonPlanParCommercant($_iCommercantId);
            $data['nbBonPlan'] = sizeof($oBonPlan);

            $data['active_link'] = "accueil";
            $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
            if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
            $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
            $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
            if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

            $is_mobile = $this->agent->is_mobile();
            //test ipad user agent
            $is_mobile_ipad = $this->agent->is_mobile('ipad');
            $data['is_mobile_ipad'] = $is_mobile_ipad;
            $is_robot = $this->agent->is_robot();
            $is_browser = $this->agent->is_browser();
            $is_platform = $this->agent->platform();
            $data['is_mobile'] = $is_mobile;
            $data['is_robot'] = $is_robot;
            $data['is_browser'] = $is_browser;
            $data['is_platform'] = $is_platform;

            $this->load->model("user") ;
            if ($this->ion_auth->logged_in()){
                $user_ion_auth = $this->ion_auth->user()->row();
                $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                if ($iduser==null || $iduser==0 || $iduser==""){
                    $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                }
            } else $iduser=0;
            $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));

            $this->load->model("mdl_agenda");
            $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

            //$this->load->view('front/vwRecommanderAmi', $data) ;
            $this->load->view('front2013/recommanderami', $data) ;

        } else if ($nom_url_commercant=="agenda") {
            $idAgenda = $this->uri->rsegment(4);
            $this->load->model("mdl_agenda");
            $oAgenda = $this->mdl_agenda->GetById($idAgenda);
            if (isset($oAgenda)) $data['IdCommercant'] = $oAgenda->IdCommercant; else $data['IdCommercant'] = "0";
            $data['idAgenda'] = $idAgenda;
            $this->load->view('frontAout2013/recommanderami', $data) ;
        } else if ($nom_url_commercant=="article") {
            $idAgenda = $this->uri->rsegment(4);
            $this->load->model("mdlarticle");
            $oAgenda = $this->mdlarticle->GetById($idAgenda);
            if (isset($oAgenda)) $data['IdCommercant'] = $oAgenda->IdCommercant; else $data['IdCommercant'] = "0";
            $data['idAgenda'] = $idAgenda;
            $this->load->view('sortez_mobile/recommanderami', $data) ;
        }

    }
    function envoiMailRecommander($_iCommercantId){
        $_iCommercantId = $this->uri->rsegment(3);
        $_iAgenda = $this->uri->rsegment(4);
        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
        /*$config = array();
        $config[ 'protocol' ] = 'mail' ;
        $config[ 'mailtype' ] = 'html' ;
        $config[ 'charset' ] = 'utf-8' ;*/
        $this->load->library('email');

        $errorMail    = "";

        $mail_to      = $_POST['zEmail'] ;
        //$mail_cc = $_POST['zMailTo']  ;
        $mail_to_name = "Privicarte";
        $mail_subject = "[Privicarte] Recommander " . $oInfoCommercant->NomSociete ;
        $mail_expediteur = $_POST['zNom'] ;
        $data = array() ;
        $data["zNom"] = $_POST['zNom'] ;
        $data["zTelephone"] = $_POST['zTelephone'] ;
        $data["zCommentaire"] = $_POST['zCommentaire'] ;
        //$mail_body = "Ceci est un mail de Recommandation \n\n";
        //$mail_body = $this->load->view("front/vwContenuMailNousContacterAnnonce", $data, true) ;
        $mail_body = '
Bonjour,


Une demande de contact a été formmulée provenant de Privicarte.fr

Expediteur : '.$_POST['zNom'].'

Nom : '.$_POST['zTelephone'].'


'.$_POST['zCommentaire'].'
';

        $this->email->from($mail_expediteur, "Privicarte");
        $this->email->to($mail_to);
        //$this->email->cc($mail_cc);
        $this->email->bcc("randawilly@gmail.com");
        $this->email->subject($mail_subject);
        //$this->email->attach($mail_Document);
        $this->email->message($mail_body);



        $data['oInfoCommercant'] = $oInfoCommercant ;
        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId) ;
        $data['toBonPlan'] = $toBonPlan ;

        $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oBonPlan = 	$this->mdlbonplan->bonPlanParCommercant($_iCommercantId);
        $data['nbBonPlan'] = sizeof($oBonPlan);

        $data['active_link'] = "accueil";
        $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user") ;
        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser=0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));


        if ($this->email->send()) {
            //echo "Le mail est bien envoyé !" ;



            //$this->load->view('front/vwRecommanderAmiSucces', $data) ;
            if (isset($_iAgenda) && $_iAgenda=="agenda") {
                $this->load->view('frontAout2013/recommanderamisucces', $data) ;
            } else {
                $this->load->view('front2013/recommanderamisucces', $data) ;
            }

        } else {
            //echo "Un problème est survenu, veuillez renvoyer le mail !" ;
            //$this->load->view('front/vwRecommanderAmiErreur', $data) ;

            if (isset($_iAgenda) && $_iAgenda=="agenda") {
                $this->load->view('frontAout2013/recommanderamierreur', $data) ;
            } else {
                $this->load->view('front2013/recommanderamierreur', $data) ;
            }
        }
    }




    function manage_nbrevisites($_iCommercantId){

        //$_iCommercantId = $this->uri->rsegment(3);



        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;


        $nbvisite = intval($oInfoCommercant->nbrevisites);
        $nbvisite_nv = $nbvisite + 1;

        //$oInfoCommercant['nbrevisites'] = $nbvisite;


        $this->mdlcommercant->Update_nbrevisites_commercant($_iCommercantId, $nbvisite_nv);

        echo $nbvisite_nv;

    }
public function reservation(){

    $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
    $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

    $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
    $data['oInfoCommercant'] = $oInfoCommercant ;

    $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
    $data['commande'] = $infocommande;

    $data['active_link'] = "activite1";
    $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
    if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
    $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
    $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
    if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

    $is_mobile = $this->agent->is_mobile();
    //test ipad user agent
    $is_mobile_ipad = $this->agent->is_mobile('ipad');
    $data['is_mobile_ipad'] = $is_mobile_ipad;
    $is_robot = $this->agent->is_robot();
    $is_browser = $this->agent->is_browser();
    $is_platform = $this->agent->platform();
    $data['is_mobile'] = $is_mobile;
    $data['is_robot'] = $is_robot;
    $data['is_browser'] = $is_browser;
    $data['is_platform'] = $is_platform;

    $this->load->model("user") ;
    if ($this->ion_auth->logged_in()){
        $user_ion_auth = $this->ion_auth->user()->row();
        $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
        if ($iduser==null || $iduser==0 || $iduser==""){
            $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        }
    } else $iduser=0;
    $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));

    $this->load->model("mdl_agenda");
    $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

    if ($this->ion_auth->logged_in()) {
        $user_ion_auth = $this->ion_auth->user()->row();
        //var_dump($user_ion_auth);die('data users');
        $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
        if ($iduser == null || $iduser == 0 || $iduser == "") {
            $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        }
        $data['IdUser'] = $iduser;
        $card = $this->Mdl_card->getByIdUser($iduser);
        $data['num_card'] = $card;
        $res = $this->Mdl_soutenons->getuser_by_id_card($card->num_id_card_virtual);
        $data['res'] = $res;
        $data['generate_qrcode'] = $user_ion_auth;
    }

    $data['current_page'] = "page_infos";
    $data['pagecategory'] = "pro";

    $data['pageglissiere'] = "page1";
    $this->load->model("mdlglissiere") ;
    $data['mdlglissiere'] = $this->mdlglissiere;
    $data['Mdl_plat_du_jour'] = $this->Mdl_plat_du_jour;

    $data['link_partner_current_page'] = 'page1';

    $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId) ;
    $data['toBonPlan'] = $toBonPlan ;

    //ajout bouton rond noire annonce/agenda
    $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
    if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
    else $data['result_check_commercant_annonce'] = '0';

    $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
    if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
    else $data['result_check_commercant_agenda'] = '0';

    $get_data_menu=$this->Mdl_plat_du_jour->get_title_menu_by_idcom($_iCommercantId);
    $get_data_all__content=$this->Mdl_plat_du_jour->get_all_plat_by_idcom($_iCommercantId);
    $data['reservation']=$get_data_all__content;
    $data['data_menu']=$get_data_menu;
    $toListeFidelity_remise = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "remise");
    $toListeFidelity_tampon = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "tampon");
    $toListeFidelity_capital = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "capital");
    $toListeBonPlan_all_pvc = $this->mdlbonplan->listeBonPlanRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "","");
    //$toListePlat_du_jour = $this->Mdl_plat_du_jour->listeplatRecherche( "", "", "",  0, 10000, "", "", "", "", $_iCommercantId,"");

    $i = 0;
    if ($toListeFidelity_remise !=[]){
        foreach ($toListeFidelity_remise as $listremise){
            $allarrays[$i]['id'] = $listremise->id;
            $allarrays[$i]['titre'] = $listremise->titre;
            $allarrays[$i]['description'] = $listremise->description;
            $allarrays[$i]['image1'] = $listremise->image1;
            $allarrays[$i]['date_fin'] = $listremise->date_fin;
            $allarrays[$i]['type'] = "remise";
            $allarrays[$i]['ville_nom'] = $listremise->ville;
            $allarrays[$i]['ville_id'] = $listremise->IdVille;
            $allarrays[$i]['idcom'] = $listremise->IdCommercant;
            $allarrays[$i]['partenaire'] = $listremise->NomSociete;
            $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listremise->IdCommercant)->nom_url;
            $i ++;
        }
    }
    if ($toListeFidelity_tampon !=[]){
        foreach ($toListeFidelity_tampon as $listtampon){
            $allarrays[$i]['id'] = $listtampon->id;
            $allarrays[$i]['titre'] = $listtampon->titre;
            $allarrays[$i]['description'] = $listtampon->description;
            $allarrays[$i]['image1'] = $listtampon->image1;
            $allarrays[$i]['date_fin'] = $listtampon->date_fin;
            $allarrays[$i]['type'] = "tampon";
            $allarrays[$i]['ville_nom'] = $listtampon->ville;
            $allarrays[$i]['ville_id'] = $listtampon->IdVille;
            $allarrays[$i]['idcom'] = $listtampon->IdCommercant;
            $allarrays[$i]['partenaire'] = $listtampon->NomSociete;
            $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listtampon->IdCommercant)->nom_url;
            $i ++;
        }
    }
    if ($toListeFidelity_capital !=[]){
        foreach ($toListeFidelity_capital as $listcapitel){
            $allarrays[$i]['id'] = $listcapitel->id;
            $allarrays[$i]['titre'] = $listcapitel->titre;
            $allarrays[$i]['description'] = $listcapitel->description;
            $allarrays[$i]['image1'] = $listcapitel->image1;
            $allarrays[$i]['date_fin'] = $listcapitel->date_fin;
            $allarrays[$i]['type'] = "capital";
            $allarrays[$i]['ville_nom'] = $listcapitel->ville;
            $allarrays[$i]['ville_id'] = $listcapitel->IdVille;
            $allarrays[$i]['idcom'] = $listcapitel->IdCommercant;
            $allarrays[$i]['partenaire'] = $listcapitel->NomSociete;
            $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listcapitel->IdCommercant)->nom_url;
            $i++;
        }
    }
    if ($toListeBonPlan_all_pvc !=[]){
        foreach ($toListeBonPlan_all_pvc as $bonplan){
            //var_dump($bonplan);die();
            if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "2"){
                $date = $bonplan->bp_unique_date_fin;
                //$type = "Bonplan unique";
            }
            else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "1"){
                $date = $bonplan->bonplan_date_fin;
                //$type = "Bonplan simple";
            }
            else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "3"){
                $date = $bonplan->bp_multiple_date_fin;
                //$type = "Bonplan multiples";
            }

            $allarrays[$i]['id'] = $bonplan->bonplan_id;
            $allarrays[$i]['titre'] = $bonplan->bonplan_titre;
            $allarrays[$i]['description'] = $bonplan->bonplan_texte;
            $allarrays[$i]['image1'] = $bonplan->bonplan_photo1;
            $allarrays[$i]['date_fin'] = $date;
            $allarrays[$i]['type'] = $bonplan->bonplan_type;
            $allarrays[$i]['ville_nom'] = $this->mdlville->getVilleById($this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation)->Nom;
            $allarrays[$i]['ville_id'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation;
            $allarrays[$i]['idcom'] = $bonplan->bonplan_commercant_id;
            $allarrays[$i]['partenaire'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->NomSociete;
            $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->nom_url;
            $i++;
        }
    }
    $data['alldata'] = $allarrays;

    //$this->load->view('front/vwPlusInfoCommercant', $data) ;
    //$this->load->view('front2013/plusinfoscommercant', $data) ;
    $this->load->view('front_soutenons/reservation', $data) ;

}

public function valid_reservation_client(){
    $IdPlat=$this->input->post('IdPlat');
    $IdCommercant=$this->input->post('IdCommercant');
    $date_res = $this->input->post('date_res');
    $champ_selected = $this->input->post('champ_selected');
    $heure_reservation=$this->input->post('heure_reservation');
    $nbre_pers_reserved=$this->input->post('nbre_pers_reserved');
    $nbre_platDuJour=$this->input->post('nbre_platDuJour');
    $num_carte=$this->input->post('num_carte');
    $date_reservation=date('Y-m-d');
    $datacom=$this->Mdl_plat_du_jour->get_resinfocom_by_idcom($IdCommercant);
    $oInfoCommercant = $this->mdlcommercant->infoCommercant($IdCommercant) ;
    $format_date_set = 'l j F Y';

    $date_reservation_cli =  dateToFrench($date_res,$format_date_set);

    //var_dump($date_reservation_cli);die('test info com');

    if (isset($num_carte) AND $num_carte !="" AND $num_carte!=null){
    $is_valid_card= $this->verif_carte($num_carte);
    if ($is_valid_card =='0'){
        echo 'numero de carte invalide';
    }elseif(count($is_valid_card) !=0){
        date_default_timezone_set('Europe/Paris');
        $field=array(
           "IdPlat"=>$IdPlat,
           "IdCommercant"=>$IdCommercant,
           "heure_reservation"=>$heure_reservation,
           "nbre_pers_reserved"=>(int)$nbre_pers_reserved,
           "nbre_platDuJour"=>(int)$nbre_platDuJour,
           "num_carte"=>(int)$num_carte,
           "date_reservation"=>$date_reservation,
           "datetime_reservation"=>date('Y-m-d H:i:s'),
       );
        $champ_valeur = explode('-',$champ_selected);
        $champ = $champ_valeur[0];
        $valeur = $champ_valeur[1];
        $total_count = $valeur - $nbre_platDuJour;
        $fields['id_plat_gli'] = $IdPlat;
        $fields[$champ] = $total_count;
        //var_dump($aboutuser);die('user');
        $is_saved= $this->Mdl_plat_du_jour->save_reservation_client($field);

      if ($is_saved == 1){
            $about_plat=$this->Mdl_plat_du_jour->get_by_id_plat($IdPlat);
            $aboutuser=$this->User->getById_client($is_valid_card->id_user);
            if (isset($aboutuser->Telephone) AND $aboutuser->Telephone!='' AND $aboutuser->Telephone !=null){
                $tel=$aboutuser->Telephone;
            }elseif (isset($aboutuser->Portable) AND $aboutuser->Portable !='' AND $aboutuser->Portable !=null){
                $tel=$aboutuser->Portable;
            }
            if(count($aboutuser)!=0){
                if($aboutuser->Email != null && $aboutuser->Email != ""){
                    $envoyeur = $aboutuser->Email;
                }elseif($aboutuser->Login != null && $aboutuser->Login != ""){
                    $envoyeur = $aboutuser->Login;
                }

                $contact_message_content_fc2="
                <p>Bonjour,<br/>
    Ceci est une mail copie de votre réservation de plat dans Sortez.org<br/>
    ci-dessous le contenu,
</p>

<p>
    - <b>Votre Nom</b>  : ".$aboutuser->Nom." ".$aboutuser->Prenom."<br/>
    - <b>Date de votre dépôt</b>  : ".date_french()."<br/>
    - <b>La date de réservation</b> : ".$date_reservation_cli."  Heure : ".$heure_reservation."<br/>
    - <b>Téléphone</b> : ".$tel."<br/>
    -  <b>Nombre de personne</b> : ".$nbre_pers_reserved." personne(s)<br/>
    - <b>Nombre de plat réservé</b> : ".$nbre_platDuJour." plat(s)<br/>
    - <b>Nom du plat réservé</b> : ".$about_plat[0]->designation."<br>
</p>
Cordialement,
<br/>Sortez.org
                ";
                if(isset($datacom->mail_reservation_plat) && $datacom->mail_reservation_plat != null && $datacom->mail_reservation_plat != ""){
                    $contact_to_fc[0]['Email'] = $datacom->mail_reservation_plat;
                }elseif($oInfoCommercant->Email != null && $oInfoCommercant->Email != ""){
                    $contact_to_fc[0]['Email'] = $oInfoCommercant->Email;
                }else{
                    $contact_to_fc[0]['Email'] = $oInfoCommercant->Login;
                }
                $contact_to_fc[0]['Name'] = $oInfoCommercant->Nom;
                $contact_object_fc = 'Réservation plat';
                $contact_message_content_fc ="<p>Bonjour,<br/>
    Ceci est une demande de réservation de plat de votre client sur Sortez.org<br/>
    ci-dessous le contenu,
</p>

<p>
    - <b>Le Nom du client</b> : ".$aboutuser->Nom." ".$aboutuser->Prenom."<br/>
    - <b>Date dépôt</b>  : ".date_french()."<br/>
    - <b>La date de réservation</b> : ".$date_reservation_cli."  Heure:".$heure_reservation."<br/>
    - <b>Téléphone du client</b> : ".$tel."<br/>
    - <b>Email du client</b> : ".$envoyeur."<br>
    - <b>Nombre de personne</b> : ".$nbre_pers_reserved." personne(s)<br/>
    - <b>Nombre de plat réservé</b> : ".$nbre_platDuJour." plat(s)<br/>
    - <b>Nom du plat réservé</b> : ".$about_plat[0]->designation."<br>
</p>
Cordialement,
<br/>Sortez.org,";
                /*
                $headers_fc = 'From: '.$aboutuser->Email.'' . "\r\n" .
                    'Reply-To: '.$aboutuser->Email.'' . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();
                $headers_fc2 = 'From: sortez.org' . "\r\n" .
                    'Reply-To: '.$datacom->mail_reservation_plat.'' . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();
                $headers_fc2 .= "MIME-Version: 1.0\r\n";
                $headers_fc2 .= "Content-Type: text/html; charset=utf8\r\n";

                $headers_fc .= "MIME-Version: 1.0\r\n";
                $headers_fc .= "Content-Type: text/html; charset=utf8\r\n";
                */

//                var_dump($contact_message_content_fc2);
//                var_dump($contact_message_content_fc);die('getall');
                $envoyeur_name = $aboutuser->Nom;
                if(isset($datacom->mail_reservation_plat) && $datacom->mail_reservation_plat != null && $datacom->mail_reservation_plat != ""){
                    $envoyeur2 = $datacom->mail_reservation_plat;
                }elseif($oInfoCommercant->Email != null && $oInfoCommercant->Email != ""){
                    $envoyeur2 = $oInfoCommercant->Email;
                }else{
                    $envoyeur2 = $oInfoCommercant->Login;
                }
                $envoyeur_name2 = $oInfoCommercant->Nom;
                $contact_to_fc2[0]['Email'] = $envoyeur;
                $contact_to_fc2[0]['Name'] = $envoyeur_name;

                if (envoi_notification($contact_to_fc,$contact_object_fc,$contact_message_content_fc,$envoyeur,$envoyeur_name)){
                    if (envoi_notification($contact_to_fc2,$contact_object_fc,$contact_message_content_fc2,$envoyeur2,$envoyeur_name2)){
                        $plat_unset = $this->Mdl_plat_du_jour->update_plat_after_reservation($fields);
                        if($plat_unset == 1){
                            echo 'Plat reservé';
                        }
                    }else{
                        echo 'erreur1';
                    }

                }else{
                    echo 'erreur2';
                }
            }

      }else{
          echo 'erreur3';
      }
   }else{
       echo 'numero de carte invalide';
   }

}

}

public function verif_carte($id_card){
       $is_valid= $this->Mdl_card->getByCard($id_card);

       if (count($is_valid) >0){
           return $is_valid;
       }else{return 0;}
}
public function desactivate_plat(){
        $id_plat=$this->input->post('IdPlat');
        $unactive=$this->Mdl_plat_du_jour->desactive_plat($id_plat);
        if ($unactive=='ok'){
            echo 'ok';
        }
}

public function submit_res_sejour(){
    $Id_commercant=$this->input->post('Id_commercant');
    $id_client=$this->input->post('id_client');
    $Pays=$this->input->post('Pays');
    $Nom=$this->input->post('Nom');
    $prenom=$this->input->post('prenom');
    $Adresse=$this->input->post('Adresse');
    $code_postal=$this->input->post('code_postal');
    $mail=$this->input->post('mail');
    $id_ville=$this->input->post('id_ville');
    $tel=$this->input->post('tel');
    $date_debut_res=$this->input->post('date_debut_res');
    $date_fin_res=$this->input->post('date_fin_res');
    $nbre_adulte=$this->input->post('nbre_adulte');
    $nbre_enfant=$this->input->post('nbre_enfant');
    $message_client=$this->input->post('message_client');
    $date_de_reservation=date('Y-m-d');
    $num_card=$this->input->post('num_card');
    $etat='1';

    $field=array(
        "Id_commercant"=>$Id_commercant,
        "id_client"=>$id_client,
        "Pays"=>$Pays,
        "Nom"=>$Nom,
        "Adresse"=>$Adresse,
        "code_postal"=>$code_postal,
        "mail"=>$mail,
        "id_ville"=>$id_ville,
        "tel"=>$tel,
        "date_debut_res"=>$date_debut_res,
        "date_fin_res"=>$date_fin_res,
        "nbre_adulte"=>$nbre_adulte,
        "nbre_enfant"=>$nbre_enfant,
        "message_client"=>$message_client,
        "date_de_reservation"=>$date_de_reservation,
        "num_card"=>$num_card,
        "etat"=>$etat,
        "prenom"=>$prenom
    );
    $save=$this->Mdl_reservation->save_reservation_sejour($field);
    $infocm=$this->mdlcommercant->infoCommercant($Id_commercant);
    if ($infocm->mail_reservation !='' AND $infocm->mail_reservation!=null){
        $mail_com=$infocm->mail_reservation;
    }else{
        $mail_com=$infocm->Email;
    }


    $contact_to = $mail_com;
    $contact_object = "Reservation hébergement";
    $contact_message_content = "<p>Bonjour,<br/>
    Ceci est une demande de r&eacute;servation de s&eacute;jour de votre client sur Sortez.org<br/>
    ci-dessous le contenu,
</p>

<p>
    - Le Nom du client :".$Nom." ".$prenom."<br/>
    - La date de r&eacute;servation : ".$date_de_reservation."<br/>
    - La date d'arriv&eacute;e :  ".$date_debut_res."<br/>
    - La date de d&eacute;part :".$date_fin_res."<br/>
    - T&eacute;l&eacute;phone du client:".$tel."<br/>
    - Mail du client:".$mail."<br/>
    - Nombre de personne:".$nbre_adulte." Adulte(s) et ".$nbre_enfant." enfant(s)<br/>
</p>

<p>
    ci-dessous le message du client:<br>
   ".$message_client."
</p>

Cordialement,
<br/>Sortez.org,";


    $headers = 'From: Sortez.org' . "\r\n" .
        'Reply-To: webmaster@example.com' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


    $contact_to2 = $mail;
    $contact_object2 = "Reservation de hébergement";
    $contact_message_content2 = "<p>Bonjour,<br/>
    Ceci est un mail de confirmation de votre r&eacute;servation de s&eacute;jour chez le commercant: ".$infocm->NomSociete." de sortez.org<br/>
    ci-dessous le contenu,
</p>

<p>
    - Votre Nom  :".$Nom." ".$prenom."<br/>
    - La date de votre r&eacute;servation : ".$date_de_reservation."<br/>
    - La date d'arriv&eacute;e :  ".$date_debut_res."<br/>
    - La date de d&eacute;part:".$date_fin_res."<br/>
    - Votre t&eacute;l&eacute;phone;".$tel."<br/>
    - Nombre de personne:".$nbre_adulte." Adulte(s) et ".$nbre_enfant." enfant(s)<br/>
</p>
Cordialement,
<br/>Sortez.org,";


    $headers2 = 'From: Sortez.org' . "\r\n" .
        'Reply-To: sortez.org' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
    $headers2 .= "MIME-Version: 1.0\r\n";
    $headers2 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

    $contact_to3 = 'srova76@gmail.com';
    $contact_object3 = "Reservation de hébergement";
    $contact_message_content3 = "<p>Bonjour,<br/>
    Ceci est un mail de confirmation de  r&eacute;servation de s&eacute;jour d'un client sortez.org<br/>
    ci-dessous le contenu,
</p>

<p>
    - Le Nom du client :".$Nom." ".$prenom."<br/>
    - La date de r&eacute;servation : ".$date_de_reservation."<br/>
    - La date d'arriv&eacute;e :  ".$date_debut_res."<br/>
    - La date de d&eacute;part :".$date_fin_res."<br/>
    - T&eacute;l&eacute;phone du client:".$tel."<br/>
    - Nombre de personne:".$nbre_adulte." Adulte(s) et ".$nbre_enfant." enfant(s)<br/>
</p>
Cordialement,
<br/>Sortez.org,";


    $headers3 = 'From: Sortez.org' . "\r\n" .
        'Reply-To: sortez.org' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
    $headers3 .= "MIME-Version: 1.0\r\n";
    $headers3 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    if (mail($contact_to,$contact_object,$contact_message_content,$headers) AND mail($contact_to2,$contact_object2,$contact_message_content2,$headers2) AND mail($contact_to3,$contact_object3,$contact_message_content3,$headers3)){
    if ($save==1){
        echo 'ok';
    }else{
        echo 'ko';
    }
    }
}
public function get_ville_by_code_postal(){

        $codepostal=$this->input->post('code_postal');
        $res=$this->mdlville->GetVilleByCodePostal_localisation_res($codepostal);
        print_r($res[0]->IdVille);

}

public function get_abonner_news_letter(){

        $nom_abonner=$this->input->post('nom_abonner');
        $email_abonner=$this->input->post('email_abonner');
        $mobile_abonner=$this->input->post('mobile_abonner');
        $idcom=$this->input->post('idcom');
        $abonner=array("nom"=>$nom_abonner,
                        "email"=>$email_abonner,
                        "mobile"=>$mobile_abonner,
                        "id_commercant"=>$idcom);
        $verify_exist=$this->Mdl_news_letter_commercant->verify($email_abonner);
        if ($verify_exist== 0 ){
        $res=$this->Mdl_news_letter_commercant->save($abonner);
        if ($res=='1'){
            echo 'ok';
        }else{
            echo 'ko';
        }

        }else{
            echo 'exist';
        }


}

    public function submit_res_table(){
        $Id_commercant=$this->input->post('Id_commercant');
        $id_client=$this->input->post('id_client');
        $Pays=$this->input->post('Pays');
        $Nom=$this->input->post('Nom');
        $prenom=$this->input->post('prenom');
        $Adresse=$this->input->post('Adresse');
        $code_postal=$this->input->post('code_postal');
        $mail=$this->input->post('mail');
        $id_ville=$this->input->post('id_ville');
        $tel=$this->input->post('tel');
        $heure_midi=$this->input->post('heure_midi');
        $heure_soir=$this->input->post('heure_soir');
        $date_res=$this->input->post('date_res');
        $nbre_adulte=$this->input->post('nbre_adulte');
        $nbre_enfant=$this->input->post('nbre_enfant');
        $message_client=$this->input->post('message_client');
        $date_de_reservation=date('Y-m-d');
        $num_card=$this->input->post('num_card');
        $etat='1';
        if (isset($id_ville) AND $id_ville !="" AND $id_ville !=0){
        $ville_nom=$this->mdlville->getVilleById($id_ville)->NomSimple;
        }else{$ville_nom="Inconnu";}
        $field=array(
            "Id_commercant"=>$Id_commercant,
            "id_client"=>$id_client,
            "Pays"=>$Pays,
            "Nom"=>$Nom,
            "Adresse"=>$Adresse,
            "code_postal"=>$code_postal,
            "mail"=>$mail,
            "id_ville"=>$id_ville,
            "tel"=>$tel,
            "heure_midi"=>$heure_midi,
            "heure_soir"=>$heure_soir,
            "date_res"=>$date_res,
            "nbre_adulte"=>$nbre_adulte,
            "nbre_enfant"=>$nbre_enfant,
            "message_client"=>$message_client,
            "date_de_reservation"=>$date_de_reservation,
            "num_card"=>$num_card,
            "etat"=>$etat,
            "prenom"=>$prenom
        );
        $infocm=$this->mdlcommercant->infoCommercant($Id_commercant);
        if ($infocm->mail_reservation !='' AND $infocm->mail_reservation!=null){
            $mail_com=$infocm->mail_reservation;
        }else{
            $mail_com=$infocm->Email;
        }
        $save=$this->Mdl_reservation->save_reservation_table($field);

        $data['Nom']=$Nom;
        $data['prenom']=$prenom;
        $data['date_de_reservation']=$date_de_reservation;
        $data['date_res']=$date_res;
        $data['heure_midi']=$heure_midi;
        $data['heure_soir']=$heure_soir;
        $data['tel']=$tel;
        $data['nbre_adulte']=$nbre_adulte;
        $data['nbre_enfant']=$nbre_enfant;
        $data['message_client']=$message_client;

        $contact_to = $mail_com;
        $contact_object = "Reservation de Table";
        $contact_message_content = "<p>Bonjour,<br/>
    Ceci est une demande de r&eacute;servation de table de votre client sur Sortez.org<br/>
    ci-dessous le contenu,
</p>

<p>
    - Le Nom du client :".$Nom." ".$prenom."<br/>
    - La date de r&eacute;servation : ".$date_de_reservation."<br/>
    - La date prevue :  ".$date_res."<br/>
    - L'heure prevue :".$heure_midi."-".$heure_soir."<br/>
    - T&eacute;l&eacute;phone du client:".$tel."<br/>
    - Mail du client:".$mail."<br/>
    - Nombre de personne:".$nbre_adulte." Adulte(s) et ".$nbre_enfant." enfant(s)<br/>
</p>

<p>
    ci-dessous le message du client:<br>
   ".$message_client."
</p>

Cordialement,
<br/>Sortez.org,";


        $headers = 'From: Sortez.org' . "\r\n" .
        'Reply-To: webmaster@example.com' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


        $contact_to2 = $mail;
        $contact_object2 = "Reservation de Table";
        $contact_message_content2 = "<p>Bonjour,<br/>
    Ceci est un mail de confirmation de votre r&eacute;servation de table chez le commercant : ".$infocm->NomSociete." de  sortez.org<br/>
    ci-dessous le contenu,
</p>

<p>
    - Votre Nom  :".$Nom." ".$prenom."<br/>
    - La date de votre r&eacute;servation : ".$date_de_reservation."<br/>
    - La date prevue :  ".$date_res."<br/>
    - L'heure prevue :".$heure_midi."-".$heure_soir."<br/>
    - Votre t&eacute;l&eacute;phone;".$tel."<br/>
    - Nombre de personne:".$nbre_adulte." Adulte(s) et ".$nbre_enfant." enfant(s)<br/>
</p>
Cordialement,
<br/>Sortez.org,";


        $headers2 = 'From: Sortez.org' . "\r\n" .
            'Reply-To: sortez.org' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers2 .= "MIME-Version: 1.0\r\n";
        $headers2 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $contact_to3 = 'srova76@gmail.com';
        $contact_object3 = "Reservation de Table";
        $contact_message_content3 = "<p>Bonjour,<br/>
    Ceci est un mail de confirmation de  r&eacute;servation de table d'un client sortez.org<br/>
    ci-dessous le contenu,
</p>

<p>
    - Le Nom du client :".$Nom." ".$prenom."<br/>
    - La date de r&eacute;servation : ".$date_de_reservation."<br/>
    - La date prevue :  ".$date_res."<br/>
    - L'heure prevue :".$heure_midi."-".$heure_soir."<br/>
    - T&eacute;l&eacute;phone du client:".$tel."<br/>
    - Nombre de personne:".$nbre_adulte." Adulte(s) et ".$nbre_enfant." enfant(s)<br/>
</p>
Cordialement,
<br/>Sortez.org,";


        $headers3 = 'From: Sortez.org' . "\r\n" .
            'Reply-To: sortez.org' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers3 .= "MIME-Version: 1.0\r\n";
        $headers3 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


        if (mail($contact_to,$contact_object,$contact_message_content,$headers) AND mail($contact_to2,$contact_object2,$contact_message_content2,$headers2) AND mail($contact_to3,$contact_object3,$contact_message_content3,$headers3)){

           if ($save==1){
               echo 'ok';
           }else{
               echo 'ko1';
           }

       }else{
           echo 'ko2';
       }

    }
    public function get_users_by_id_card(){

        $num_card=$this->input->post('num_card');
        $client_fiche=$this->mdlcommercant->get_user_by_id_card($num_card);
        if ($client_fiche){
            echo json_encode($client_fiche);
        }else{
            echo 'no';
        }
    }
    public function details_plat($id,$id2){

        $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
        $data['oInfoCommercant'] = $oInfoCommercant ;


        $data['active_link'] = "activite1";
        $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user") ;
        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser=0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $data['current_page'] = "page_infos";
        $data['pagecategory'] = "pro";

        $data['pageglissiere'] = "page1";
        $this->load->model("mdlglissiere") ;
        $data['mdlglissiere'] = $this->mdlglissiere;
        $data['Mdl_plat_du_jour'] = $this->Mdl_plat_du_jour;

        $data['link_partner_current_page'] = 'page1';

        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId) ;
        $data['toBonPlan'] = $toBonPlan ;

        //ajout bouton rond noire annonce/agenda
        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
        else $data['result_check_commercant_annonce'] = '0';

        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
        else $data['result_check_commercant_agenda'] = '0';

        $is_reserved_on=$this->Mdl_plat_du_jour->verify_reserved_by_idplat($_iCommercantId,$id2);
        $data['reservation']=$is_reserved_on;

        $toListeFidelity_remise = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "remise");
        $toListeFidelity_tampon = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "tampon");
        $toListeFidelity_capital = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "capital");
        $toListeBonPlan_all_pvc = $this->mdlbonplan->listeBonPlanRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "","");
        $toListePlat_du_jour = $this->Mdl_plat_du_jour->listeplatRecherche( "", "", "",  0, 10000, "", "", "", "", $_iCommercantId,"");

        $i = 0;
        if ($toListeFidelity_remise !=[]){
            foreach ($toListeFidelity_remise as $listremise){
                $allarrays[$i]['id'] = $listremise->id;
                $allarrays[$i]['titre'] = $listremise->titre;
                $allarrays[$i]['description'] = $listremise->description;
                $allarrays[$i]['image1'] = $listremise->image1;
                $allarrays[$i]['date_fin'] = $listremise->date_fin;
                $allarrays[$i]['type'] = "remise";
                $allarrays[$i]['ville_nom'] = $listremise->ville;
                $allarrays[$i]['ville_id'] = $listremise->IdVille;
                $allarrays[$i]['idcom'] = $listremise->IdCommercant;
                $allarrays[$i]['partenaire'] = $listremise->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listremise->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_tampon !=[]){
            foreach ($toListeFidelity_tampon as $listtampon){
                $allarrays[$i]['id'] = $listtampon->id;
                $allarrays[$i]['titre'] = $listtampon->titre;
                $allarrays[$i]['description'] = $listtampon->description;
                $allarrays[$i]['image1'] = $listtampon->image1;
                $allarrays[$i]['date_fin'] = $listtampon->date_fin;
                $allarrays[$i]['type'] = "tampon";
                $allarrays[$i]['ville_nom'] = $listtampon->ville;
                $allarrays[$i]['ville_id'] = $listtampon->IdVille;
                $allarrays[$i]['idcom'] = $listtampon->IdCommercant;
                $allarrays[$i]['partenaire'] = $listtampon->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listtampon->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_capital !=[]){
            foreach ($toListeFidelity_capital as $listcapitel){
                $allarrays[$i]['id'] = $listcapitel->id;
                $allarrays[$i]['titre'] = $listcapitel->titre;
                $allarrays[$i]['description'] = $listcapitel->description;
                $allarrays[$i]['image1'] = $listcapitel->image1;
                $allarrays[$i]['date_fin'] = $listcapitel->date_fin;
                $allarrays[$i]['type'] = "capital";
                $allarrays[$i]['ville_nom'] = $listcapitel->ville;
                $allarrays[$i]['ville_id'] = $listcapitel->IdVille;
                $allarrays[$i]['idcom'] = $listcapitel->IdCommercant;
                $allarrays[$i]['partenaire'] = $listcapitel->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listcapitel->IdCommercant)->nom_url;
                $i++;
            }
        }
        if ($toListeBonPlan_all_pvc !=[]){
            foreach ($toListeBonPlan_all_pvc as $bonplan){
                //var_dump($bonplan);die();
                if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "2"){
                    $date = $bonplan->bp_unique_date_fin;
                    //$type = "Bonplan unique";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "1"){
                    $date = $bonplan->bonplan_date_fin;
                    //$type = "Bonplan simple";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "3"){
                    $date = $bonplan->bp_multiple_date_fin;
                    //$type = "Bonplan multiples";
                }

                $allarrays[$i]['id'] = $bonplan->bonplan_id;
                $allarrays[$i]['titre'] = $bonplan->bonplan_titre;
                $allarrays[$i]['description'] = $bonplan->bonplan_texte;
                $allarrays[$i]['image1'] = $bonplan->bonplan_photo1;
                $allarrays[$i]['date_fin'] = $date;
                $allarrays[$i]['type'] = $bonplan->bonplan_type;
                $allarrays[$i]['ville_nom'] = $this->mdlville->getVilleById($this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation)->Nom;
                $allarrays[$i]['ville_id'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation;
                $allarrays[$i]['idcom'] = $bonplan->bonplan_commercant_id;
                $allarrays[$i]['partenaire'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->nom_url;
                $i++;
            }
        }
        if ($toListePlat_du_jour !=[]){
            foreach ($toListePlat_du_jour as $plat){
                $allarrays[$i]['id'] = $plat->id;
                $allarrays[$i]['titre'] = $plat->rubrique;
                $allarrays[$i]['description'] = $plat->description_plat;
                $allarrays[$i]['image1'] = $plat->photo;
                $allarrays[$i]['date_fin'] = $plat->date_fin_plat;
                $allarrays[$i]['inputDatedebut'] = $plat->date_fin_plat;
                $allarrays[$i]['type'] = "plat";
                $allarrays[$i]['nbre_plat_propose'] = $plat->nbre_plat_propose;
                $allarrays[$i]['ville_nom'] = $plat->ville;
                $allarrays[$i]['ville_id'] = $plat->IdVille;
                $allarrays[$i]['idcom'] = $plat->IdCommercant;
                $allarrays[$i]['partenaire'] = $plat->NomSociete;
                $allarrays[$i]['prix_plat'] = $plat->prix_plat;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($plat->IdCommercant)->nom_url;
                $i++;
            }
        }
        $data['alldata'] = $allarrays;
        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;
        //$this->load->view('front/vwPlusInfoCommercant', $data) ;
        //$this->load->view('front2013/plusinfoscommercant', $data) ;
        $this->load->view('front_soutenons/reservation', $data) ;

    }
    public function reservation_plat($idplat,$nbre_pers_reserved,$nbre_platDuJour,$heure_reservation,$date_selected,$champ_selected){
        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $data['iduser'] = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            $data['iduser'] =$iduser;
            $data['num_card']=$this->Mdl_card->getByIdUser($iduser);
        } else $data['iduser']=0;

        $plat=$this->Mdl_plat_du_jour->get_by_id_plat($idplat);
        $data['plat']=$plat;
        $idcom_plat = $plat[0]->IdCommercant;
        $data['active_link'] = "activite1";
        $infocom = $this->mdlcommercant->infoCommercant($idcom_plat) ;
        $data['oInfoCommercant'] = $infocom;
        $data['nbre_pers_reserved'] = $nbre_pers_reserved;
        $data['nbre_platDuJour'] = $nbre_platDuJour;
        $data['heure_reservation'] = $heure_reservation;
        $data['date_fin_plat'] = $date_selected;
        $data['champ_selected'] = $champ_selected;
        $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($idcom_plat);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($idcom_plat);
        $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($idcom_plat);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;
        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user") ;
        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser=0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$idcom_plat));
        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($idcom_plat);
        $data['current_page'] = "page_infos";
        $data['pagecategory'] = "pro";
        $data['pageglissiere'] = "page1";
        $this->load->model("mdlglissiere") ;
        $data['mdlglissiere'] = $this->mdlglissiere;
        $data['link_partner_current_page'] = 'page1';
        $toBonPlan = $this->mdlbonplan->getListeBonPlan($idcom_plat) ;
        $data['toBonPlan'] = $toBonPlan ;
        $get_data_all__content=$this->Mdl_plat_du_jour->get_all_plat_by_idcom($idcom_plat);
        $data['reservation']=$get_data_all__content;
        $toListeFidelity_remise = $this->mdlfidelity->listeFidelityRecherche("", $idcom_plat, "", "", 0, 10000, "", "", "", "", "", "", "", "", "remise");
        $toListeFidelity_tampon = $this->mdlfidelity->listeFidelityRecherche("", $idcom_plat, "", "", 0, 10000, "", "", "", "", "", "", "", "", "tampon");
        $toListeFidelity_capital = $this->mdlfidelity->listeFidelityRecherche("", $idcom_plat, "", "", 0, 10000, "", "", "", "", "", "", "", "", "capital");
        $toListeBonPlan_all_pvc = $this->mdlbonplan->listeBonPlanRecherche("", $idcom_plat, "", "", 0, 10000, "", "", "", "", "", "", "", "","");
        $toListePlat_du_jour = $this->Mdl_plat_du_jour->listeplatRecherche( "", "", "",  0, 10000, "", "", "", "", $idcom_plat,"");

        $i = 0;
        if ($toListeFidelity_remise !=[]){
            foreach ($toListeFidelity_remise as $listremise){
                $allarrays[$i]['id'] = $listremise->id;
                $allarrays[$i]['titre'] = $listremise->titre;
                $allarrays[$i]['description'] = $listremise->description;
                $allarrays[$i]['image1'] = $listremise->image1;
                $allarrays[$i]['date_fin'] = $listremise->date_fin;
                $allarrays[$i]['type'] = "remise";
                $allarrays[$i]['ville_nom'] = $listremise->ville;
                $allarrays[$i]['ville_id'] = $listremise->IdVille;
                $allarrays[$i]['idcom'] = $listremise->IdCommercant;
                $allarrays[$i]['partenaire'] = $listremise->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listremise->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_tampon !=[]){
            foreach ($toListeFidelity_tampon as $listtampon){
                $allarrays[$i]['id'] = $listtampon->id;
                $allarrays[$i]['titre'] = $listtampon->titre;
                $allarrays[$i]['description'] = $listtampon->description;
                $allarrays[$i]['image1'] = $listtampon->image1;
                $allarrays[$i]['date_fin'] = $listtampon->date_fin;
                $allarrays[$i]['type'] = "tampon";
                $allarrays[$i]['ville_nom'] = $listtampon->ville;
                $allarrays[$i]['ville_id'] = $listtampon->IdVille;
                $allarrays[$i]['idcom'] = $listtampon->IdCommercant;
                $allarrays[$i]['partenaire'] = $listtampon->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listtampon->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_capital !=[]){
            foreach ($toListeFidelity_capital as $listcapitel){
                $allarrays[$i]['id'] = $listcapitel->id;
                $allarrays[$i]['titre'] = $listcapitel->titre;
                $allarrays[$i]['description'] = $listcapitel->description;
                $allarrays[$i]['image1'] = $listcapitel->image1;
                $allarrays[$i]['date_fin'] = $listcapitel->date_fin;
                $allarrays[$i]['type'] = "capital";
                $allarrays[$i]['ville_nom'] = $listcapitel->ville;
                $allarrays[$i]['ville_id'] = $listcapitel->IdVille;
                $allarrays[$i]['idcom'] = $listcapitel->IdCommercant;
                $allarrays[$i]['partenaire'] = $listcapitel->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listcapitel->IdCommercant)->nom_url;
                $i++;
            }
        }
        if ($toListeBonPlan_all_pvc !=[]){
            foreach ($toListeBonPlan_all_pvc as $bonplan){
                //var_dump($bonplan);die();
                if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "2"){
                    $date = $bonplan->bp_unique_date_fin;
                    //$type = "Bonplan unique";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "1"){
                    $date = $bonplan->bonplan_date_fin;
                    //$type = "Bonplan simple";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "3"){
                    $date = $bonplan->bp_multiple_date_fin;
                    //$type = "Bonplan multiples";
                }

                $allarrays[$i]['id'] = $bonplan->bonplan_id;
                $allarrays[$i]['titre'] = $bonplan->bonplan_titre;
                $allarrays[$i]['description'] = $bonplan->bonplan_texte;
                $allarrays[$i]['image1'] = $bonplan->bonplan_photo1;
                $allarrays[$i]['date_fin'] = $date;
                $allarrays[$i]['type'] = $bonplan->bonplan_type;
                $allarrays[$i]['ville_nom'] = $this->mdlville->getVilleById($this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation)->Nom;
                $allarrays[$i]['ville_id'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation;
                $allarrays[$i]['idcom'] = $bonplan->bonplan_commercant_id;
                $allarrays[$i]['partenaire'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->nom_url;
                $i++;
            }
        }
        if ($toListePlat_du_jour !=[]){
            foreach ($toListePlat_du_jour as $plat){
                $allarrays[$i]['id'] = $plat->id;
                $allarrays[$i]['titre'] = $plat->rubrique;
                $allarrays[$i]['description'] = $plat->description_plat;
                $allarrays[$i]['image1'] = $plat->photo;
                $allarrays[$i]['date_fin'] = $plat->date_fin_plat;
                $allarrays[$i]['inputDatedebut'] = $plat->date_fin_plat;
                $allarrays[$i]['type'] = "plat";
                $allarrays[$i]['nbre_plat_propose'] = $plat->nbre_plat_propose;
                $allarrays[$i]['ville_nom'] = $plat->ville;
                $allarrays[$i]['ville_id'] = $plat->IdVille;
                $allarrays[$i]['idcom'] = $idcom_plat;
                $allarrays[$i]['partenaire'] = $plat->NomSociete;
                $allarrays[$i]['prix_plat'] = $plat->prix_plat;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($idcom_plat)->nom_url;
                $i++;
            }
        }
        $data['alldata'] = $allarrays;
        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($idcom_plat));
        $data['commande'] = $infocommande;

        $this->load->view('front_soutenons/reservation_plat',$data);
    }
    public function commander($_iCommercantId){
        $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);
        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
        $data['oInfoCommercant'] = $oInfoCommercant ;

        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;

        $data['active_link'] = "activite1";
        $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user") ;
        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser=0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $data['current_page'] = "page_infos";
        $data['pagecategory'] = "pro";

        $data['pageglissiere'] = "page1";
        $this->load->model("mdlglissiere") ;
        $data['mdlglissiere'] = $this->mdlglissiere;
        $data['Mdl_plat_du_jour'] = $this->Mdl_plat_du_jour;

        $data['link_partner_current_page'] = 'page1';

        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId) ;
        $data['toBonPlan'] = $toBonPlan ;

        //ajout bouton rond noire annonce/agenda
        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
        else $data['result_check_commercant_annonce'] = '0';

        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
        else $data['result_check_commercant_agenda'] = '0';

//        $is_reserved_on=$this->Mdl_plat_du_jour->verify_reserved($_iCommercantId);
//        $data['reservation']=$is_reserved_on;
        $get_data_all__content=$this->Mdl_plat_du_jour->get_all_plat_by_idcom($_iCommercantId);
        $data['reservation']=$get_data_all__content;

        //$this->load->view('front/vwPlusInfoCommercant', $data) ;
        //$this->load->view('front2013/plusinfoscommercant', $data) ;
        $infocom = $this->mdlcommercant->infoCommercant($_iCommercantId);
        $data_commande = $this->Mdl_soutenons->get_com_data_by_idcom($_iCommercantId);
        $data['infocom'] = $infocom;
        $data['data_gli1'] = $this->Mdl_soutenons->getdatagli1($_iCommercantId);
        $data['data_gli2'] = $this->Mdl_soutenons->getdatagli2($_iCommercantId);
        $data['data_gli3'] = $this->Mdl_soutenons->getdatagli3($_iCommercantId);
        $data['data_gli4'] = $this->Mdl_soutenons->getdatagli4($_iCommercantId);
        $data['data_gli6'] = $this->Mdl_soutenons->getdatagli6($_iCommercantId);
        $data['data_gli7'] = $this->Mdl_soutenons->getdatagli7($_iCommercantId);
        $data['datacommande'] = $data_commande;
        $data['title_gli1'] = $this->Mdl_soutenons->gettitlegli1($_iCommercantId);
        $data['title_gli2'] = $this->Mdl_soutenons->gettitlegli2($_iCommercantId);
        $data['title_gli3'] = $this->Mdl_soutenons->gettitlegli3($_iCommercantId);
        $data['title_gli4'] = $this->Mdl_soutenons->gettitlegli4($_iCommercantId);
        $data['title_gli6'] = $this->Mdl_soutenons->gettitlegli6($_iCommercantId);
        $data['title_gli7'] = $this->Mdl_soutenons->gettitlegli7($_iCommercantId);

        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            //var_dump($user_ion_auth);die('data users');
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            $data['IdUser'] = $iduser;
            $card = $this->Mdl_card->getByIdUser($iduser);
            $data['num_card'] = $card;
            $res = $this->Mdl_soutenons->getuser_by_id_card($card->num_id_card_virtual);
            $data['res'] = $res;
            $data['generate_qrcode'] = $user_ion_auth;
        }
        $toListeFidelity_remise = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "remise");
        $toListeFidelity_tampon = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "tampon");
        $toListeFidelity_capital = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "capital");
        $toListeBonPlan_all_pvc = $this->mdlbonplan->listeBonPlanRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "","");
        //$toListePlat_du_jour = $this->Mdl_plat_du_jour->listeplatRecherche( "", "", "",  0, 10000, "", "", "", "", $_iCommercantId,"");

        $i = 0;
        if ($toListeFidelity_remise !=[]){
            foreach ($toListeFidelity_remise as $listremise){
                $allarrays[$i]['id'] = $listremise->id;
                $allarrays[$i]['titre'] = $listremise->titre;
                $allarrays[$i]['description'] = $listremise->description;
                $allarrays[$i]['image1'] = $listremise->image1;
                $allarrays[$i]['date_fin'] = $listremise->date_fin;
                $allarrays[$i]['type'] = "remise";
                $allarrays[$i]['ville_nom'] = $listremise->ville;
                $allarrays[$i]['ville_id'] = $listremise->IdVille;
                $allarrays[$i]['idcom'] = $listremise->IdCommercant;
                $allarrays[$i]['partenaire'] = $listremise->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listremise->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_tampon !=[]){
            foreach ($toListeFidelity_tampon as $listtampon){
                $allarrays[$i]['id'] = $listtampon->id;
                $allarrays[$i]['titre'] = $listtampon->titre;
                $allarrays[$i]['description'] = $listtampon->description;
                $allarrays[$i]['image1'] = $listtampon->image1;
                $allarrays[$i]['date_fin'] = $listtampon->date_fin;
                $allarrays[$i]['type'] = "tampon";
                $allarrays[$i]['ville_nom'] = $listtampon->ville;
                $allarrays[$i]['ville_id'] = $listtampon->IdVille;
                $allarrays[$i]['idcom'] = $listtampon->IdCommercant;
                $allarrays[$i]['partenaire'] = $listtampon->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listtampon->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_capital !=[]){
            foreach ($toListeFidelity_capital as $listcapitel){
                $allarrays[$i]['id'] = $listcapitel->id;
                $allarrays[$i]['titre'] = $listcapitel->titre;
                $allarrays[$i]['description'] = $listcapitel->description;
                $allarrays[$i]['image1'] = $listcapitel->image1;
                $allarrays[$i]['date_fin'] = $listcapitel->date_fin;
                $allarrays[$i]['type'] = "capital";
                $allarrays[$i]['ville_nom'] = $listcapitel->ville;
                $allarrays[$i]['ville_id'] = $listcapitel->IdVille;
                $allarrays[$i]['idcom'] = $listcapitel->IdCommercant;
                $allarrays[$i]['partenaire'] = $listcapitel->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listcapitel->IdCommercant)->nom_url;
                $i++;
            }
        }
        if ($toListeBonPlan_all_pvc !=[]){
            foreach ($toListeBonPlan_all_pvc as $bonplan){
                //var_dump($bonplan);die();
                if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "2"){
                    $date = $bonplan->bp_unique_date_fin;
                    //$type = "Bonplan unique";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "1"){
                    $date = $bonplan->bonplan_date_fin;
                    //$type = "Bonplan simple";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "3"){
                    $date = $bonplan->bp_multiple_date_fin;
                    //$type = "Bonplan multiples";
                }

                $allarrays[$i]['id'] = $bonplan->bonplan_id;
                $allarrays[$i]['titre'] = $bonplan->bonplan_titre;
                $allarrays[$i]['description'] = $bonplan->bonplan_texte;
                $allarrays[$i]['image1'] = $bonplan->bonplan_photo1;
                $allarrays[$i]['date_fin'] = $date;
                $allarrays[$i]['type'] = $bonplan->bonplan_type;
                $allarrays[$i]['ville_nom'] = $this->mdlville->getVilleById($this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation)->Nom;
                $allarrays[$i]['ville_id'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation;
                $allarrays[$i]['idcom'] = $bonplan->bonplan_commercant_id;
                $allarrays[$i]['partenaire'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->nom_url;
                $i++;
            }
        }
        $data['alldata'] = $allarrays;

        $this->load->view('front_soutenons/commande_front', $data) ;
    }


    function menuannonceCommercant($_iCommercantId)

    {

        $nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant

        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);



        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);

        $data['oInfoCommercant'] = $oInfoCommercant;

        $data['toListeAnnonceParCommercant'] = $this->mdlannonce->listeAnnonceParCommercant_soutenons($_iCommercantId);



        $data['active_link'] = "annonces";

        $oAssComRub = $this->mdlcommercant->GetRubriqueId($_iCommercantId);

        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);

        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

        $oLastbonplanCom = $this->mdlbonplan->lastBonplanCom($_iCommercantId);

        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;



        $is_mobile = $this->agent->is_mobile();

        //test ipad user agent

        $is_mobile_ipad = $this->agent->is_mobile('ipad');

        $data['is_mobile_ipad'] = $is_mobile_ipad;

        $is_robot = $this->agent->is_robot();

        $is_browser = $this->agent->is_browser();

        $is_platform = $this->agent->platform();

        $data['is_mobile'] = $is_mobile;

        $data['is_robot'] = $is_robot;

        $data['is_browser'] = $is_browser;

        $data['is_platform'] = $is_platform;



        $this->load->model("user");

        if ($this->ion_auth->logged_in()) {

            $user_ion_auth = $this->ion_auth->user()->row();

            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

            if ($iduser == null || $iduser == 0 || $iduser == "") {

                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

            }

        } else $iduser = 0;

        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser, $_iCommercantId));

        $data['pagecategory_partner'] = 'annonces_partner';



        $data['dontshowannoncecase'] = 1;



        $data['link_partner_current_page'] = 'boutique';



        $this->load->model("mdl_agenda");

        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $data['pagecategory'] = "pro";



        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId);

        $data['toBonPlan'] = $toBonPlan;





        //ajout bouton rond noire annonce/agenda

        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);

        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';

        else $data['result_check_commercant_annonce'] = '0';



        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);

        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';

        else $data['result_check_commercant_agenda'] = '0';



//        $is_reserved_on=$this->Mdl_plat_du_jour->verify_reserved($_iCommercantId);
//
//        $data['reservation']=$is_reserved_on;

        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
//        $is_reserved_on=$this->Mdl_plat_du_jour->verify_reserved($_iCommercantId);
//        $data['reservation']=$is_reserved_on;

        $get_data_all__content=$this->Mdl_plat_du_jour->get_all_plat_by_idcom($_iCommercantId);
        $data['reservation']=$get_data_all__content;

        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche_sans_referencement_bonplan($_iCategorieId = 0, $_iCommercantId, $_zMotCle = "", $_iFavoris = "", $_limitstart = 0, $_limitend = 10000, $_iIdVille = 0, $_iIdDepartement = 0, $iOrderBy="", $inputFromGeo = "0", $inputGeoValue = "10", $inputGeoLatitude="0", $inputGeoLongitude="0", $session_iWhereMultiple = "",$inputString_bonplan_type="");

        $nombre_bp = count($toListeBonPlan);
        $data['nombre_bp']=$nombre_bp;
        $toListeFidelity_remise = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "remise");
        $toListeFidelity_tampon = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "tampon");
        $toListeFidelity_capital = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "capital");
        $toListeBonPlan_all_pvc = $this->mdlbonplan->listeBonPlanRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "","");
        $toListePlat_du_jour = $this->Mdl_plat_du_jour->listeplatRecherche( "", "", "",  0, 10000, "", "", "", "", $_iCommercantId,"");

        $i = 0;
        if ($toListeFidelity_remise !=[]){
            foreach ($toListeFidelity_remise as $listremise){
                $allarrays[$i]['id'] = $listremise->id;
                $allarrays[$i]['titre'] = $listremise->titre;
                $allarrays[$i]['description'] = $listremise->description;
                $allarrays[$i]['image1'] = $listremise->image1;
                $allarrays[$i]['date_fin'] = $listremise->date_fin;
                $allarrays[$i]['type'] = "remise";
                $allarrays[$i]['ville_nom'] = $listremise->ville;
                $allarrays[$i]['ville_id'] = $listremise->IdVille;
                $allarrays[$i]['idcom'] = $listremise->IdCommercant;
                $allarrays[$i]['partenaire'] = $listremise->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listremise->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_tampon !=[]){
            foreach ($toListeFidelity_tampon as $listtampon){
                $allarrays[$i]['id'] = $listtampon->id;
                $allarrays[$i]['titre'] = $listtampon->titre;
                $allarrays[$i]['description'] = $listtampon->description;
                $allarrays[$i]['image1'] = $listtampon->image1;
                $allarrays[$i]['date_fin'] = $listtampon->date_fin;
                $allarrays[$i]['type'] = "tampon";
                $allarrays[$i]['ville_nom'] = $listtampon->ville;
                $allarrays[$i]['ville_id'] = $listtampon->IdVille;
                $allarrays[$i]['idcom'] = $listtampon->IdCommercant;
                $allarrays[$i]['partenaire'] = $listtampon->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listtampon->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_capital !=[]){
            foreach ($toListeFidelity_capital as $listcapitel){
                $allarrays[$i]['id'] = $listcapitel->id;
                $allarrays[$i]['titre'] = $listcapitel->titre;
                $allarrays[$i]['description'] = $listcapitel->description;
                $allarrays[$i]['image1'] = $listcapitel->image1;
                $allarrays[$i]['date_fin'] = $listcapitel->date_fin;
                $allarrays[$i]['type'] = "capital";
                $allarrays[$i]['ville_nom'] = $listcapitel->ville;
                $allarrays[$i]['ville_id'] = $listcapitel->IdVille;
                $allarrays[$i]['idcom'] = $listcapitel->IdCommercant;
                $allarrays[$i]['partenaire'] = $listcapitel->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listcapitel->IdCommercant)->nom_url;
                $i++;
            }
        }
        if ($toListeBonPlan_all_pvc !=[]){
            foreach ($toListeBonPlan_all_pvc as $bonplan){
                //var_dump($bonplan);die();
                if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "2"){
                    $date = $bonplan->bp_unique_date_fin;
                    //$type = "Bonplan unique";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "1"){
                    $date = $bonplan->bonplan_date_fin;
                    //$type = "Bonplan simple";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "3"){
                    $date = $bonplan->bp_multiple_date_fin;
                    //$type = "Bonplan multiples";
                }

                $allarrays[$i]['id'] = $bonplan->bonplan_id;
                $allarrays[$i]['titre'] = $bonplan->bonplan_titre;
                $allarrays[$i]['description'] = $bonplan->bonplan_texte;
                $allarrays[$i]['image1'] = $bonplan->bonplan_photo1;
                $allarrays[$i]['date_fin'] = $date;
                $allarrays[$i]['type'] = $bonplan->bonplan_type;
                $allarrays[$i]['ville_nom'] = $this->mdlville->getVilleById($this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation)->Nom;
                $allarrays[$i]['ville_id'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation;
                $allarrays[$i]['idcom'] = $bonplan->bonplan_commercant_id;
                $allarrays[$i]['partenaire'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->nom_url;
                $i++;
            }
        }
        if ($toListePlat_du_jour !=[]){
            foreach ($toListePlat_du_jour as $plat){
                $allarrays[$i]['id'] = $plat->id;
                $allarrays[$i]['titre'] = $plat->rubrique;
                $allarrays[$i]['description'] = $plat->description_plat;
                $allarrays[$i]['image1'] = $plat->photo;
                $allarrays[$i]['date_fin'] = $plat->date_fin_plat;
                $allarrays[$i]['inputDatedebut'] = $plat->date_fin_plat;
                $allarrays[$i]['type'] = "plat";
                $allarrays[$i]['nbre_plat_propose'] = $plat->nbre_plat_propose;
                $allarrays[$i]['ville_nom'] = $plat->ville;
                $allarrays[$i]['ville_id'] = $plat->IdVille;
                $allarrays[$i]['idcom'] = $plat->IdCommercant;
                $allarrays[$i]['partenaire'] = $plat->NomSociete;
                $allarrays[$i]['prix_plat'] = $plat->prix_plat;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($plat->IdCommercant)->nom_url;
                $i++;
            }
        }
        $data['alldata'] = $allarrays;

        //$this->load->view('front/vwMenuAnnonceCommercant', $data) ;

        //$this->load->view('front2013/menuannoncecommercant', $data) ;

        $this->load->view('front_soutenons/annonces_commercants', $data);

    }
    function detailAnnonce($_iAnnonceId)

    {



        $_iAnnonceId = $this->uri->rsegment(4); //die($_iAnnonceId." stop");//reecuperation valeur $_iAnnonceId



        $oDetailAnnonce = $this->mdlannonce->detailAnnonce($_iAnnonceId);

        $data['oDetailAnnonce'] = $oDetailAnnonce;



        $_iCommercantId = $oDetailAnnonce->id_commercant;



        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);

        $data['oInfoCommercant'] = $oInfoCommercant;

        $data['active_link'] = "annonces";

        $data['active_link2'] = "detailannonce";

        $oAssComRub = $this->mdlcommercant->GetRubriqueId($_iCommercantId);

        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);

        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

        $oLastbonplanCom = $this->mdlbonplan->lastBonplanCom($_iCommercantId);

        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;



        $is_mobile = $this->agent->is_mobile();

        //test ipad user agent

        $is_mobile_ipad = $this->agent->is_mobile('ipad');

        $data['is_mobile_ipad'] = $is_mobile_ipad;

        $is_robot = $this->agent->is_robot();

        $is_browser = $this->agent->is_browser();

        $is_platform = $this->agent->platform();

        $data['is_mobile'] = $is_mobile;

        $data['is_robot'] = $is_robot;

        $data['is_browser'] = $is_browser;

        $data['is_platform'] = $is_platform;



        $this->load->model("user");

        if ($this->ion_auth->logged_in()) {

            $user_ion_auth = $this->ion_auth->user()->row();

            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

            if ($iduser == null || $iduser == 0 || $iduser == "") {

                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

            }

        } else $iduser = 0;

        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser, $_iCommercantId));

        $data['pagecategory_partner'] = 'details_annonces_partner';



        $data['mdlville'] = $this->mdlville;



        $this->load->model("mdl_agenda");

        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);



        if (isset($_POST['text_mail_form_module_detailannnonce'])) {

            $text_mail_form_module_detailannnonce = $this->input->post("text_mail_form_module_detailannnonce");

            $nom_mail_form_module_detailannnonce = $this->input->post("nom_mail_form_module_detailannnonce");

            $tel_mail_form_module_detailannnonce = $this->input->post("tel_mail_form_module_detailannnonce");

            $email_mail_form_module_detailannnonce = $this->input->post("email_mail_form_module_detailannnonce");



            $colDestAdmin = array();

            $colDestAdmin[] = array("Email" => $oInfoCommercant->Email, "Name" => $oInfoCommercant->Nom . " " . $oInfoCommercant->Prenom);



            // Sujet

            $txtSujetAdmin = "Demande d'information sur une annonce Privicarte";



            $txtContenuAdmin = "

            <p>Bonjour ,</p>

            <p>Une demande de contact vient du site ClubProxmité à propos d'une annonce que vous avez posté :</p>

            <p>Détails de l'annonce :<br/>

            Désignation : " . $oDetailAnnonce->texte_courte . "<br/>

            N° : " . ajoutZeroPourString($oDetailAnnonce->annonce_id, 6) . " du " . convertDateWithSlashes($oDetailAnnonce->annonce_date_debut) . "

            Prix de vente : " . $oDetailAnnonce->ancien_prix . "<br/></p><p>

            Nom Client : " . $nom_mail_form_module_detailannnonce . "<br/>

            Téléphone Client : " . $tel_mail_form_module_detailannnonce . "<br/>

            Email Client : " . $email_mail_form_module_detailannnonce . "<br/>

            Demande Client :<br/>

            " . $text_mail_form_module_detailannnonce . "<br/><br/>

            </p>";



            @envoi_notification($colDestAdmin, $txtSujetAdmin, $txtContenuAdmin);

            $data['mssg_envoi_module_detail_annonce'] = '<font color="#00CC00">Votre demande est envoyée</font>';

            //$data['mssg_envoi_module_detail_annonce'] = $txtContenuAdmin;



        } else $data['mssg_envoi_module_detail_annonce'] = '';





        $data['link_partner_current_page'] = 'boutique';

        $data['pagecategory'] = "pro";



        //ajout bouton rond noire annonce/agenda

        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);

        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';

        else $data['result_check_commercant_annonce'] = '0';



        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);

        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';

        else $data['result_check_commercant_agenda'] = '0';



        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId);

        $data['toBonPlan'] = $toBonPlan;



        $is_reserved_on=$this->Mdl_plat_du_jour->verify_reserved($_iCommercantId);

        $data['reservation']=$is_reserved_on;

        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;

        $toListeFidelity_remise = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "remise");
        $toListeFidelity_tampon = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "tampon");
        $toListeFidelity_capital = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "capital");
        $toListeBonPlan_all_pvc = $this->mdlbonplan->listeBonPlanRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "","");
        $toListePlat_du_jour = $this->Mdl_plat_du_jour->listeplatRecherche( "", "", "",  0, 10000, "", "", "", "", $_iCommercantId,"");

        $i = 0;
        if ($toListeFidelity_remise !=[]){
            foreach ($toListeFidelity_remise as $listremise){
                $allarrays[$i]['id'] = $listremise->id;
                $allarrays[$i]['titre'] = $listremise->titre;
                $allarrays[$i]['description'] = $listremise->description;
                $allarrays[$i]['image1'] = $listremise->image1;
                $allarrays[$i]['date_fin'] = $listremise->date_fin;
                $allarrays[$i]['type'] = "remise";
                $allarrays[$i]['ville_nom'] = $listremise->ville;
                $allarrays[$i]['ville_id'] = $listremise->IdVille;
                $allarrays[$i]['idcom'] = $listremise->IdCommercant;
                $allarrays[$i]['partenaire'] = $listremise->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listremise->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_tampon !=[]){
            foreach ($toListeFidelity_tampon as $listtampon){
                $allarrays[$i]['id'] = $listtampon->id;
                $allarrays[$i]['titre'] = $listtampon->titre;
                $allarrays[$i]['description'] = $listtampon->description;
                $allarrays[$i]['image1'] = $listtampon->image1;
                $allarrays[$i]['date_fin'] = $listtampon->date_fin;
                $allarrays[$i]['type'] = "tampon";
                $allarrays[$i]['ville_nom'] = $listtampon->ville;
                $allarrays[$i]['ville_id'] = $listtampon->IdVille;
                $allarrays[$i]['idcom'] = $listtampon->IdCommercant;
                $allarrays[$i]['partenaire'] = $listtampon->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listtampon->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_capital !=[]){
            foreach ($toListeFidelity_capital as $listcapitel){
                $allarrays[$i]['id'] = $listcapitel->id;
                $allarrays[$i]['titre'] = $listcapitel->titre;
                $allarrays[$i]['description'] = $listcapitel->description;
                $allarrays[$i]['image1'] = $listcapitel->image1;
                $allarrays[$i]['date_fin'] = $listcapitel->date_fin;
                $allarrays[$i]['type'] = "capital";
                $allarrays[$i]['ville_nom'] = $listcapitel->ville;
                $allarrays[$i]['ville_id'] = $listcapitel->IdVille;
                $allarrays[$i]['idcom'] = $listcapitel->IdCommercant;
                $allarrays[$i]['partenaire'] = $listcapitel->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listcapitel->IdCommercant)->nom_url;
                $i++;
            }
        }
        if ($toListeBonPlan_all_pvc !=[]){
            foreach ($toListeBonPlan_all_pvc as $bonplan){
                //var_dump($bonplan);die();
                if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "2"){
                    $date = $bonplan->bp_unique_date_fin;
                    //$type = "Bonplan unique";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "1"){
                    $date = $bonplan->bonplan_date_fin;
                    //$type = "Bonplan simple";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "3"){
                    $date = $bonplan->bp_multiple_date_fin;
                    //$type = "Bonplan multiples";
                }

                $allarrays[$i]['id'] = $bonplan->bonplan_id;
                $allarrays[$i]['titre'] = $bonplan->bonplan_titre;
                $allarrays[$i]['description'] = $bonplan->bonplan_texte;
                $allarrays[$i]['image1'] = $bonplan->bonplan_photo1;
                $allarrays[$i]['date_fin'] = $date;
                $allarrays[$i]['type'] = $bonplan->bonplan_type;
                $allarrays[$i]['ville_nom'] = $this->mdlville->getVilleById($this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation)->Nom;
                $allarrays[$i]['ville_id'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation;
                $allarrays[$i]['idcom'] = $bonplan->bonplan_commercant_id;
                $allarrays[$i]['partenaire'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->nom_url;
                $i++;
            }
        }
        if ($toListePlat_du_jour !=[]){
            foreach ($toListePlat_du_jour as $plat){
                $allarrays[$i]['id'] = $plat->id;
                $allarrays[$i]['titre'] = $plat->rubrique;
                $allarrays[$i]['description'] = $plat->description_plat;
                $allarrays[$i]['image1'] = $plat->photo;
                $allarrays[$i]['date_fin'] = $plat->date_fin_plat;
                $allarrays[$i]['inputDatedebut'] = $plat->date_fin_plat;
                $allarrays[$i]['type'] = "plat";
                $allarrays[$i]['nbre_plat_propose'] = $plat->nbre_plat_propose;
                $allarrays[$i]['ville_nom'] = $plat->ville;
                $allarrays[$i]['ville_id'] = $plat->IdVille;
                $allarrays[$i]['idcom'] = $plat->IdCommercant;
                $allarrays[$i]['partenaire'] = $plat->NomSociete;
                $allarrays[$i]['prix_plat'] = $plat->prix_plat;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($plat->IdCommercant)->nom_url;
                $i++;
            }
        }
        $data['alldata'] = $allarrays;

        //$this->load->view('front/vwDetailAnnonce', $data) ;

        //$this->load->view('front2013/detailsannonce', $data) ;

        $this->load->view('front_soutenons/details_annonces', $data);

    }
    function listetousbonplan(){



        statistiques();

        $nom_url_commercant = $this->uri->rsegment(3);



        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);

        // var_dump($_iCommercantId);die();

        $data['oInfoCommercant'] = $oInfoCommercant;

        $toListeBonPlan = $this->mdlbonplan->listeBonPlanRecherche_sans_referencement_bonplan($_iCategorieId = 0, $_iCommercantId, $_zMotCle = "", $_iFavoris = "", $_limitstart = 0, $_limitend = 10000, $_iIdVille = 0, $_iIdDepartement = 0, $iOrderBy="", $inputFromGeo = "0", $inputGeoValue = "10", $inputGeoLatitude="0", $inputGeoLongitude="0", $session_iWhereMultiple = "",$inputString_bonplan_type="");

        $nombre_bp = count($toListeBonPlan);

        $data['nombre_bp']=$nombre_bp;

        $this->load->model("user") ;

        $data['toListeBonPlan']=$toListeBonPlan;

        $data['Idcommercant']=$_iCommercantId;

        $this->load->view('privicarte/menubonplancommercant', $data);

    }

    function agenda_partner_list($_iCommercantId)
    {

        //code standard utilisation page partenaire START **************************************************

        $nom_url_commercant = $this->uri->rsegment(3);

        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);

        $data['oInfoCommercant'] = $oInfoCommercant;

        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($oInfoCommercant));
        $data['commande'] = $infocommande;

        //a basic account is not permit to show agenda list
        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);
        if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
        if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;
        if ($group_id_commercant_user == 3) {
            redirect('front/utilisateur/no_permission');
        }

        //$data['mdlannonce'] = $this->mdlannonce ;
        $data['mdlbonplan'] = $this->mdlbonplan;

        $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oBonPlan = $this->mdlbonplan->bonPlanParCommercant($_iCommercantId);
        $data['nbBonPlan'] = sizeof($oBonPlan);
        //      $data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_titre : "";     // OP 27/11/2011
        $data['sTitreBonPlan'] = (sizeof($oBonPlan) > 0) ? $oBonPlan->bonplan_texte : "";
        $data['oImagespub'] = $this->mdlimagespub->GetByImagespubActiv();
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

        $oLastbonplanCom = $this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $data['active_link'] = "agenda";

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user");
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser = 0;

        //code standard utilisation page partenaire END **************************************************

        $PerPage = 15;
        $data['PerPage'] = $PerPage;

        $inputStringHidden = $this->input->post("inputStringHidden");
        //var_dump($inputStringHidden);
        $inputStringQuandHidden = $this->input->post("inputStringQuandHidden");
        //var_dump($inputStringQuandHidden);die();
        if (isset($inputStringHidden)||isset($inputStringQuandHidden)){
            if (isset($inputStringHidden)) {
                $this->session->unset_userdata('iCategorieId');
                $this->session->set_userdata('iCategorieId', $inputStringHidden);
            }
            if (isset($inputStringQuandHidden)) {
                $this->session->unset_userdata('inputStringQuandHidden');
                $this->session->set_userdata('inputStringQuandHidden', $inputStringQuandHidden);
            }
            $data['iCategorieId'] = $inputStringHidden;
            $data['inputStringQuandHidden'] = $inputStringQuandHidden;
            $TotalRows = count($this->mdl_agenda->listeAgendaRechercheTotalCount(array($inputStringHidden), 0, 0, "", 0, 0, 10000, "", $inputStringQuandHidden, "0000-00-00", "0000-00-00", $oInfoCommercant->IdCommercant));
            $data["TotalRows"] = $TotalRows ;
            $config_pagination = array();
            $config_pagination["base_url"] = base_url() . "/" . $oInfoCommercant->nom_url . "/agenda/";
            $config_pagination["total_rows"] = $TotalRows;
            $config_pagination["per_page"] = $PerPage;
            $config_pagination["uri_segment"] = 4;
            $config_pagination['first_link'] = '<<<';
            $config_pagination['last_link'] = '>>>';
            $this->pagination->initialize($config_pagination);
            $page_pagination = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['toAgenda'] = $this->mdl_agenda->listeAgendaRecherche(array($inputStringHidden), 0, 0, "", 0, $page_pagination, $config_pagination["per_page"], "", $inputStringQuandHidden, "0000-00-00", "0000-00-00", $oInfoCommercant->IdCommercant);
            $data["links_pagination"] = $this->pagination->create_links();
        } else {
            $inputStringHidden = $this->session->userdata('iCategorieId');
            $inputStringQuandHidden = $this->session->userdata('inputStringQuandHidden');
            $data['iCategorieId'] = $inputStringHidden;
            $data['inputStringQuandHidden'] = $inputStringQuandHidden;
            $TotalRows = count($this->mdl_agenda->listeAgendaRechercheTotalCount(array($inputStringHidden), 0, 0, "", 0, 0, 10000, "", $inputStringQuandHidden, "0000-00-00", "0000-00-00", $oInfoCommercant->IdCommercant));
            $data["TotalRows"] = $TotalRows ;
            $config_pagination = array();
            $config_pagination["base_url"] = base_url() . "/" . $oInfoCommercant->nom_url . "/agenda/";
            $config_pagination["total_rows"] = $TotalRows;
            $config_pagination["per_page"] = $PerPage;
            $config_pagination["uri_segment"] = 4;
            $config_pagination['first_link'] = '<<<';
            $config_pagination['last_link'] = '>>>';
            $this->pagination->initialize($config_pagination);
            $page_pagination = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['toAgenda'] = $this->mdl_agenda->listeAgendaRecherche(array($inputStringHidden), 0, 0, "", 0, $page_pagination, $config_pagination["per_page"], "", $inputStringQuandHidden, "0000-00-00", "0000-00-00", $oInfoCommercant->IdCommercant);
            $data["links_pagination"] = $this->pagination->create_links();
        }

        $data['toCategorie_principale'] = $this->mdl_agenda->GetAgendaCategorie_ByIdCommercant($_iCommercantId);

        $data['toAgndaJanvier'] = $this->mdl_agenda->GetAgendaNbByMonth("01", $_iCommercantId);
        $data['toAgndaFevrier'] = $this->mdl_agenda->GetAgendaNbByMonth("02", $_iCommercantId);
        $data['toAgndaMars'] = $this->mdl_agenda->GetAgendaNbByMonth("03", $_iCommercantId);
        $data['toAgndaAvril'] = $this->mdl_agenda->GetAgendaNbByMonth("04", $_iCommercantId);
        $data['toAgndaMai'] = $this->mdl_agenda->GetAgendaNbByMonth("05", $_iCommercantId);
        $data['toAgndaJuin'] = $this->mdl_agenda->GetAgendaNbByMonth("06", $_iCommercantId);
        $data['toAgndaJuillet'] = $this->mdl_agenda->GetAgendaNbByMonth("07", $_iCommercantId);
        $data['toAgndaAout'] = $this->mdl_agenda->GetAgendaNbByMonth("08", $_iCommercantId);
        $data['toAgndaSept'] = $this->mdl_agenda->GetAgendaNbByMonth("09", $_iCommercantId);
        $data['toAgndaOct'] = $this->mdl_agenda->GetAgendaNbByMonth("10", $_iCommercantId);
        $data['toAgndaNov'] = $this->mdl_agenda->GetAgendaNbByMonth("11", $_iCommercantId);
        $data['toAgndaDec'] = $this->mdl_agenda->GetAgendaNbByMonth("12", $_iCommercantId);


        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $data['pagecategory_partner'] = "list_agenda";

        $data['link_partner_current_page'] = 'agenda';
        $data['pagecategory'] = "pro";

        $data['current_partner_menu'] = "agenda";

        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId);
        $data['toBonPlan'] = $toBonPlan;


        //ajout bouton rond noire annonce/agenda
        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
        else $data['result_check_commercant_annonce'] = '0';

        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
        else $data['result_check_commercant_agenda'] = '0';

//        $is_reserved_on=$this->Mdl_plat_du_jour->verify_reserved($_iCommercantId);
//        $data['reservation']=$is_reserved_on;
        $get_data_all__content=$this->Mdl_plat_du_jour->get_all_plat_by_idcom($_iCommercantId);
        $data['reservation']=$get_data_all__content;

        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;
        $toListeFidelity_remise = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "remise");
        $toListeFidelity_tampon = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "tampon");
        $toListeFidelity_capital = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "capital");
        $toListeBonPlan_all_pvc = $this->mdlbonplan->listeBonPlanRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "","");
        $toListePlat_du_jour = $this->Mdl_plat_du_jour->listeplatRecherche( "", "", "",  0, 10000, "", "", "", "", $_iCommercantId,"");

        $i = 0;
        if ($toListeFidelity_remise !=[]){
            foreach ($toListeFidelity_remise as $listremise){
                $allarrays[$i]['id'] = $listremise->id;
                $allarrays[$i]['titre'] = $listremise->titre;
                $allarrays[$i]['description'] = $listremise->description;
                $allarrays[$i]['image1'] = $listremise->image1;
                $allarrays[$i]['date_fin'] = $listremise->date_fin;
                $allarrays[$i]['type'] = "remise";
                $allarrays[$i]['ville_nom'] = $listremise->ville;
                $allarrays[$i]['ville_id'] = $listremise->IdVille;
                $allarrays[$i]['idcom'] = $listremise->IdCommercant;
                $allarrays[$i]['partenaire'] = $listremise->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listremise->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_tampon !=[]){
            foreach ($toListeFidelity_tampon as $listtampon){
                $allarrays[$i]['id'] = $listtampon->id;
                $allarrays[$i]['titre'] = $listtampon->titre;
                $allarrays[$i]['description'] = $listtampon->description;
                $allarrays[$i]['image1'] = $listtampon->image1;
                $allarrays[$i]['date_fin'] = $listtampon->date_fin;
                $allarrays[$i]['type'] = "tampon";
                $allarrays[$i]['ville_nom'] = $listtampon->ville;
                $allarrays[$i]['ville_id'] = $listtampon->IdVille;
                $allarrays[$i]['idcom'] = $listtampon->IdCommercant;
                $allarrays[$i]['partenaire'] = $listtampon->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listtampon->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_capital !=[]){
            foreach ($toListeFidelity_capital as $listcapitel){
                $allarrays[$i]['id'] = $listcapitel->id;
                $allarrays[$i]['titre'] = $listcapitel->titre;
                $allarrays[$i]['description'] = $listcapitel->description;
                $allarrays[$i]['image1'] = $listcapitel->image1;
                $allarrays[$i]['date_fin'] = $listcapitel->date_fin;
                $allarrays[$i]['type'] = "capital";
                $allarrays[$i]['ville_nom'] = $listcapitel->ville;
                $allarrays[$i]['ville_id'] = $listcapitel->IdVille;
                $allarrays[$i]['idcom'] = $listcapitel->IdCommercant;
                $allarrays[$i]['partenaire'] = $listcapitel->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listcapitel->IdCommercant)->nom_url;
                $i++;
            }
        }
        if ($toListeBonPlan_all_pvc !=[]){
            foreach ($toListeBonPlan_all_pvc as $bonplan){
                //var_dump($bonplan);die();
                if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "2"){
                    $date = $bonplan->bp_unique_date_fin;
                    //$type = "Bonplan unique";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "1"){
                    $date = $bonplan->bonplan_date_fin;
                    //$type = "Bonplan simple";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "3"){
                    $date = $bonplan->bp_multiple_date_fin;
                    //$type = "Bonplan multiples";
                }

                $allarrays[$i]['id'] = $bonplan->bonplan_id;
                $allarrays[$i]['titre'] = $bonplan->bonplan_titre;
                $allarrays[$i]['description'] = $bonplan->bonplan_texte;
                $allarrays[$i]['image1'] = $bonplan->bonplan_photo1;
                $allarrays[$i]['date_fin'] = $date;
                $allarrays[$i]['type'] = $bonplan->bonplan_type;
                $allarrays[$i]['ville_nom'] = $this->mdlville->getVilleById($this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation)->Nom;
                $allarrays[$i]['ville_id'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation;
                $allarrays[$i]['idcom'] = $bonplan->bonplan_commercant_id;
                $allarrays[$i]['partenaire'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->nom_url;
                $i++;
            }
        }
        if ($toListePlat_du_jour !=[]){
            foreach ($toListePlat_du_jour as $plat){
                $allarrays[$i]['id'] = $plat->id;
                $allarrays[$i]['titre'] = $plat->rubrique;
                $allarrays[$i]['description'] = $plat->description_plat;
                $allarrays[$i]['image1'] = $plat->photo;
                $allarrays[$i]['date_fin'] = $plat->date_fin_plat;
                $allarrays[$i]['inputDatedebut'] = $plat->date_fin_plat;
                $allarrays[$i]['type'] = "plat";
                $allarrays[$i]['nbre_plat_propose'] = $plat->nbre_plat_propose;
                $allarrays[$i]['ville_nom'] = $plat->ville;
                $allarrays[$i]['ville_id'] = $plat->IdVille;
                $allarrays[$i]['idcom'] = $plat->IdCommercant;
                $allarrays[$i]['partenaire'] = $plat->NomSociete;
                $allarrays[$i]['prix_plat'] = $plat->prix_plat;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($plat->IdCommercant)->nom_url;
                $i++;
            }
        }
        $data['alldata'] = $allarrays;

        //$this->load->view('agenda/partner_agenda_list', $data) ;
        //var_dump($data);
        $this->load->view('front_soutenons/agendacommercant_desk', $data);

    }

    function article_partner_list($_iCommercantId)

    {



        //code standard utilisation page partenaire START **************************************************



        $nom_url_commercant = $this->uri->rsegment(3);



        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);



        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);



        $data['oInfoCommercant'] = $oInfoCommercant;


        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;


        //a basic account is not permit to show agenda list

        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);

        if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;

        if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;

        if ($group_id_commercant_user == 3) {

            redirect('front/utilisateur/no_permission');

        }



        //$data['mdlannonce'] = $this->mdlannonce ;

        $data['mdlbonplan'] = $this->mdlbonplan;



        $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

        $oBonPlan = $this->mdlbonplan->bonPlanParCommercant($_iCommercantId);

        $data['nbBonPlan'] = sizeof($oBonPlan);

        //		$data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_titre : "";		// OP 27/11/2011

        $data['sTitreBonPlan'] = (sizeof($oBonPlan) > 0) ? $oBonPlan->bonplan_texte : "";

        $data['oImagespub'] = $this->mdlimagespub->GetByImagespubActiv();

        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);



        $oLastbonplanCom = $this->mdlbonplan->lastBonplanCom($_iCommercantId);

        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;



        $data['active_link'] = "article";



        $is_mobile = $this->agent->is_mobile();

        //test ipad user agent

        $is_mobile_ipad = $this->agent->is_mobile('ipad');

        $data['is_mobile_ipad'] = $is_mobile_ipad;

        $is_robot = $this->agent->is_robot();

        $is_browser = $this->agent->is_browser();

        $is_platform = $this->agent->platform();

        $data['is_mobile'] = $is_mobile;

        $data['is_robot'] = $is_robot;

        $data['is_browser'] = $is_browser;

        $data['is_platform'] = $is_platform;



        $this->load->model("user");

        if ($this->ion_auth->logged_in()) {

            $user_ion_auth = $this->ion_auth->user()->row();

            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

            if ($iduser == null || $iduser == 0 || $iduser == "") {

                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

            }

        } else $iduser = 0;



        //code standard utilisation page partenaire END **************************************************



        if ($this->input->post("inputStringHidden") == "0") unset($_SESSION['iCategorieId']);

        if ($this->input->post("inputStringQuandHidden")) unset($_SESSION['inputStringQuandHidden']);



        $PerPage = 15;

        $data['PerPage'] = $PerPage;



        if (isset($_POST["inputStringHidden"])) {



            unset($_SESSION['iCategorieId']);

            unset($_SESSION['inputStringQuandHidden']);



            $iCategorieId = $this->input->post("inputStringHidden");

            if (isset($_POST["inputStringQuandHidden"]))

                $inputStringQuandHidden = $_POST["inputStringQuandHidden"];

            else $inputStringQuandHidden = "0";



            $_SESSION['iCategorieId'] = $iCategorieId;

            $this->session->set_userdata('iCategorieId_x', $iCategorieId);

            $_SESSION['inputStringQuandHidden'] = $inputStringQuandHidden;

            $this->session->set_userdata('inputStringQuandHidden_x', $inputStringQuandHidden);



            $data['iCategorieId'] = $iCategorieId;

            $data['inputStringQuandHidden'] = $inputStringQuandHidden;



            $session_iCategorieId = $this->session->userdata('iCategorieId_x');

            $session_inputStringQuandHidden = $this->session->userdata('inputStringQuandHidden_x');





            $TotalRows = count($this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 10000000, $session_iCategorieId, $session_inputStringQuandHidden));

            $data["TotalRows"] = $TotalRows;



            $config_pagination = array();

            $config_pagination["base_url"] = base_url() . "/" . $oInfoCommercant->nom_url . "/article/";

            $config_pagination["total_rows"] = $TotalRows;

            $config_pagination["per_page"] = $PerPage;

            $config_pagination["uri_segment"] = 4;

            $config_pagination['first_link'] = '<<<';

            $config_pagination['last_link'] = '>>>';

            $this->pagination->initialize($config_pagination);

            $page_pagination = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

            $data['toAgenda'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, $page_pagination, $config_pagination["per_page"], $session_iCategorieId, $session_inputStringQuandHidden);

            $data["links_pagination"] = $this->pagination->create_links();



        } else {



            $session_iCategorieId = $this->session->userdata('iCategorieId_x');

            $session_inputStringQuandHidden = $this->session->userdata('inputStringQuandHidden_x');



            $iCategorieId = (isset($session_iCategorieId)) ? $session_iCategorieId : 0;

            $inputStringQuandHidden = (isset($session_inputStringQuandHidden)) ? $session_inputStringQuandHidden : "0";



            $TotalRows = count($this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 10000000, $iCategorieId, $inputStringQuandHidden));

            $data["TotalRows"] = $TotalRows;



            $config_pagination = array();

            $config_pagination["base_url"] = base_url() . "/" . $oInfoCommercant->nom_url . "/article/";

            $config_pagination["total_rows"] = $TotalRows;

            $config_pagination["per_page"] = $PerPage;

            $config_pagination["uri_segment"] = 4;

            $config_pagination['first_link'] = '<<<';

            $config_pagination['last_link'] = '>>>';

            $this->pagination->initialize($config_pagination);

            $page_pagination = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

            $data['toAgenda'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, $page_pagination, $config_pagination["per_page"], $iCategorieId, $inputStringQuandHidden);

            $data["links_pagination"] = $this->pagination->create_links();



        }





        $data['toCategorie_principale'] = $this->mdlarticle->GetArticleCategorie_ByIdCommercant($_iCommercantId);



        /*$data['toAgndaJanvier'] = $this->mdlarticle->GetArticleNbByMonth("01",$_iCommercantId);

        $data['toAgndaFevrier'] = $this->mdlarticle->GetArticleNbByMonth("02",$_iCommercantId);

        $data['toAgndaMars'] = $this->mdlarticle->GetArticleNbByMonth("03",$_iCommercantId);

        $data['toAgndaAvril'] = $this->mdlarticle->GetArticleNbByMonth("04",$_iCommercantId);

        $data['toAgndaMai'] = $this->mdlarticle->GetArticleNbByMonth("05",$_iCommercantId);

        $data['toAgndaJuin'] = $this->mdlarticle->GetArticleNbByMonth("06",$_iCommercantId);

        $data['toAgndaJuillet'] = $this->mdlarticle->GetArticleNbByMonth("07",$_iCommercantId);

        $data['toAgndaAout'] = $this->mdlarticle->GetArticleNbByMonth("08",$_iCommercantId);

        $data['toAgndaSept'] = $this->mdlarticle->GetArticleNbByMonth("09",$_iCommercantId);

        $data['toAgndaOct'] = $this->mdlarticle->GetArticleNbByMonth("10",$_iCommercantId);

        $data['toAgndaNov'] = $this->mdlarticle->GetArticleNbByMonth("11",$_iCommercantId);

        $data['toAgndaDec'] = $this->mdlarticle->GetArticleNbByMonth("12",$_iCommercantId);*/





        $data['toAgndaJanvier'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "01");

        $data['toAgndaFevrier'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "02");

        $data['toAgndaMars'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "03");

        $data['toAgndaAvril'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "04");

        $data['toAgndaMai'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "05");

        $data['toAgndaJuin'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "06");

        $data['toAgndaJuillet'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "07");

        $data['toAgndaAout'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "08");

        $data['toAgndaSept'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "09");

        $data['toAgndaOct'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "10");

        $data['toAgndaNov'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "11");

        $data['toAgndaDec'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "12");





        $this->load->model("mdl_agenda");

        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);



        $data['pagecategory_partner'] = "list_article";



        $data['link_partner_current_page'] = 'article';

        $data['pagecategory'] = "pro";



        $data['current_partner_menu'] = "article";



        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId);

        $data['toBonPlan'] = $toBonPlan;





        //ajout bouton rond noire annonce/agenda

        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);

        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';

        else $data['result_check_commercant_annonce'] = '0';



        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);

        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';

        else $data['result_check_commercant_agenda'] = '0';



//        $is_reserved_on=$this->Mdl_plat_du_jour->verify_reserved($_iCommercantId);
//
//        $data['reservation']=$is_reserved_on;
        $get_data_all__content=$this->Mdl_plat_du_jour->get_all_plat_by_idcom($_iCommercantId);
        $data['reservation']=$get_data_all__content;

        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;
        $toListeFidelity_remise = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "remise");
        $toListeFidelity_tampon = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "tampon");
        $toListeFidelity_capital = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "capital");
        $toListeBonPlan_all_pvc = $this->mdlbonplan->listeBonPlanRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "","");
        $toListePlat_du_jour = $this->Mdl_plat_du_jour->listeplatRecherche( "", "", "",  0, 10000, "", "", "", "", $_iCommercantId,"");

        $i = 0;
        if ($toListeFidelity_remise !=[]){
            foreach ($toListeFidelity_remise as $listremise){
                $allarrays[$i]['id'] = $listremise->id;
                $allarrays[$i]['titre'] = $listremise->titre;
                $allarrays[$i]['description'] = $listremise->description;
                $allarrays[$i]['image1'] = $listremise->image1;
                $allarrays[$i]['date_fin'] = $listremise->date_fin;
                $allarrays[$i]['type'] = "remise";
                $allarrays[$i]['ville_nom'] = $listremise->ville;
                $allarrays[$i]['ville_id'] = $listremise->IdVille;
                $allarrays[$i]['idcom'] = $listremise->IdCommercant;
                $allarrays[$i]['partenaire'] = $listremise->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listremise->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_tampon !=[]){
            foreach ($toListeFidelity_tampon as $listtampon){
                $allarrays[$i]['id'] = $listtampon->id;
                $allarrays[$i]['titre'] = $listtampon->titre;
                $allarrays[$i]['description'] = $listtampon->description;
                $allarrays[$i]['image1'] = $listtampon->image1;
                $allarrays[$i]['date_fin'] = $listtampon->date_fin;
                $allarrays[$i]['type'] = "tampon";
                $allarrays[$i]['ville_nom'] = $listtampon->ville;
                $allarrays[$i]['ville_id'] = $listtampon->IdVille;
                $allarrays[$i]['idcom'] = $listtampon->IdCommercant;
                $allarrays[$i]['partenaire'] = $listtampon->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listtampon->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_capital !=[]){
            foreach ($toListeFidelity_capital as $listcapitel){
                $allarrays[$i]['id'] = $listcapitel->id;
                $allarrays[$i]['titre'] = $listcapitel->titre;
                $allarrays[$i]['description'] = $listcapitel->description;
                $allarrays[$i]['image1'] = $listcapitel->image1;
                $allarrays[$i]['date_fin'] = $listcapitel->date_fin;
                $allarrays[$i]['type'] = "capital";
                $allarrays[$i]['ville_nom'] = $listcapitel->ville;
                $allarrays[$i]['ville_id'] = $listcapitel->IdVille;
                $allarrays[$i]['idcom'] = $listcapitel->IdCommercant;
                $allarrays[$i]['partenaire'] = $listcapitel->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listcapitel->IdCommercant)->nom_url;
                $i++;
            }
        }
        if ($toListeBonPlan_all_pvc !=[]){
            foreach ($toListeBonPlan_all_pvc as $bonplan){
                //var_dump($bonplan);die();
                if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "2"){
                    $date = $bonplan->bp_unique_date_fin;
                    //$type = "Bonplan unique";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "1"){
                    $date = $bonplan->bonplan_date_fin;
                    //$type = "Bonplan simple";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "3"){
                    $date = $bonplan->bp_multiple_date_fin;
                    //$type = "Bonplan multiples";
                }

                $allarrays[$i]['id'] = $bonplan->bonplan_id;
                $allarrays[$i]['titre'] = $bonplan->bonplan_titre;
                $allarrays[$i]['description'] = $bonplan->bonplan_texte;
                $allarrays[$i]['image1'] = $bonplan->bonplan_photo1;
                $allarrays[$i]['date_fin'] = $date;
                $allarrays[$i]['type'] = $bonplan->bonplan_type;
                $allarrays[$i]['ville_nom'] = $this->mdlville->getVilleById($this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation)->Nom;
                $allarrays[$i]['ville_id'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation;
                $allarrays[$i]['idcom'] = $bonplan->bonplan_commercant_id;
                $allarrays[$i]['partenaire'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->nom_url;
                $i++;
            }
        }
        if ($toListePlat_du_jour !=[]){
            foreach ($toListePlat_du_jour as $plat){
                $allarrays[$i]['id'] = $plat->id;
                $allarrays[$i]['titre'] = $plat->rubrique;
                $allarrays[$i]['description'] = $plat->description_plat;
                $allarrays[$i]['image1'] = $plat->photo;
                $allarrays[$i]['date_fin'] = $plat->date_fin_plat;
                $allarrays[$i]['inputDatedebut'] = $plat->date_fin_plat;
                $allarrays[$i]['type'] = "plat";
                $allarrays[$i]['nbre_plat_propose'] = $plat->nbre_plat_propose;
                $allarrays[$i]['ville_nom'] = $plat->ville;
                $allarrays[$i]['ville_id'] = $plat->IdVille;
                $allarrays[$i]['idcom'] = $plat->IdCommercant;
                $allarrays[$i]['partenaire'] = $plat->NomSociete;
                $allarrays[$i]['prix_plat'] = $plat->prix_plat;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($plat->IdCommercant)->nom_url;
                $i++;
            }
        }
        $data['alldata'] = $allarrays;



        //$this->load->view('agenda/partner_agenda_list', $data) ;

        //$this->load->view('privicarte/partner_agenda_list', $data) ;

        $this->load->view('front_soutenons/articlecommercant_desk', $data);



    }
    function agenda_partner_details($_iCommercantId)
    {

        //code standard utilisation page partenaire START **************************************************

        $nom_url_commercant = $this->uri->rsegment(3);

        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);

        $data['oInfoCommercant'] = $oInfoCommercant;

        //$data['mdlannonce'] = $this->mdlannonce ;
        $data['mdlbonplan'] = $this->mdlbonplan;

        $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oBonPlan = $this->mdlbonplan->bonPlanParCommercant($_iCommercantId);
        $data['nbBonPlan'] = sizeof($oBonPlan);
        //      $data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_titre : "";     // OP 27/11/2011
        $data['sTitreBonPlan'] = (sizeof($oBonPlan) > 0) ? $oBonPlan->bonplan_texte : "";
        $data['oImagespub'] = $this->mdlimagespub->GetByImagespubActiv();
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

        $oLastbonplanCom = $this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $data['active_link'] = "agenda";

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user");
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser = 0;

        //code standard utilisation page partenaire END **************************************************


        $id_agenda = $this->uri->rsegment(4);

        $oDetailAgenda = $this->mdl_agenda->GetById_IsActif($id_agenda);
        $data['oDetailAgenda'] = $oDetailAgenda;

        $data['pagecategory_partner'] = "details_agenda";

        //sending mail to event organiser **************************
        if (isset($_POST['text_mail_form_module_detailbonnplan'])) {
            $text_mail_form_module_detailbonnplan = $this->input->post("text_mail_form_module_detailbonnplan");
            $nom_mail_form_module_detailbonnplan = $this->input->post("nom_mail_form_module_detailbonnplan");
            $tel_mail_form_module_detailbonnplan = $this->input->post("tel_mail_form_module_detailbonnplan");
            $email_mail_form_module_detailbonnplan = $this->input->post("email_mail_form_module_detailbonnplan");

            $colDestAdmin = array();
            $colDestAdmin[] = array("Email" => $oDetailAgenda->email, "Name" => $oDetailAgenda->nom_manifestation . " " . $oDetailAgenda->nom_societe);

            // Sujet
            $txtSujetAdmin = "Demande d'information sur un évennement sur Agenda/Club";

            $txtContenuAdmin = "
            <p>Bonjour ,</p>
            <p>Une demande d'information vous est adressé suite à un évennement que vous avez déposé sur le site Agenda.</p>
            <p>Détails :<br/>
            Evennement : " . $oDetailAgenda->nom_manifestation . "
            <br/>
            Date : " . translate_date_to_fr($oDetailAgenda->date_debut) . "<br/>
            Lieu : " . $oDetailAgenda->ville . " " . $oDetailAgenda->adresse_localisation . " " . $oDetailAgenda->codepostal_localisation . "<br/>
            Organisateur : " . $oDetailAgenda->organisateur . "<br/><br/>
            Demande Client :<br/>
            " . $text_mail_form_module_detailbonnplan . "<br/><br/>
            Nom client : " . $nom_mail_form_module_detailbonnplan . "<br/>
            Tel client : " . $tel_mail_form_module_detailbonnplan . "<br/>
            Email client : " . $email_mail_form_module_detailbonnplan . "<br/>
            </p>";

            @envoi_notification($colDestAdmin, $txtSujetAdmin, $txtContenuAdmin);
            $data['mssg_envoi_module_detail_bonplan'] = '<font color="#00CC00">Votre demande est envoyée</font>';
            //$data['mssg_envoi_module_detail_bonplan'] = $txtContenuAdmin;

        } else $data['mssg_envoi_module_detail_bonplan'] = '';
        //sending mail to event organiser *******************************

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $data['pagecategory'] = 'pro';
        $data['pagecategory_partner'] = 'agenda_partner';

        $data['link_partner_current_page'] = 'agenda';

        $data['current_partner_menu'] = "agenda";

        $data['toArticle_datetime'] = $this->mdl_agenda_datetime->getByAgendaId($id_agenda);

        $data['cacher_slide'] = "1";

        //ajout bouton rond noire annonce/agenda
        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
        else $data['result_check_commercant_annonce'] = '0';

        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
        else $data['result_check_commercant_agenda'] = '0';

//        $is_reserved_on=$this->Mdl_plat_du_jour->verify_reserved($_iCommercantId);
//        $data['reservation']=$is_reserved_on;
        $get_data_all__content=$this->Mdl_plat_du_jour->get_all_plat_by_idcom($_iCommercantId);
        $data['reservation']=$get_data_all__content;

        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;
        $toListeFidelity_remise = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "remise");
        $toListeFidelity_tampon = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "tampon");
        $toListeFidelity_capital = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "capital");
        $toListeBonPlan_all_pvc = $this->mdlbonplan->listeBonPlanRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "","");
        $toListePlat_du_jour = $this->Mdl_plat_du_jour->listeplatRecherche( "", "", "",  0, 10000, "", "", "", "", $_iCommercantId,"");

        $i = 0;
        if ($toListeFidelity_remise !=[]){
            foreach ($toListeFidelity_remise as $listremise){
                $allarrays[$i]['id'] = $listremise->id;
                $allarrays[$i]['titre'] = $listremise->titre;
                $allarrays[$i]['description'] = $listremise->description;
                $allarrays[$i]['image1'] = $listremise->image1;
                $allarrays[$i]['date_fin'] = $listremise->date_fin;
                $allarrays[$i]['type'] = "remise";
                $allarrays[$i]['ville_nom'] = $listremise->ville;
                $allarrays[$i]['ville_id'] = $listremise->IdVille;
                $allarrays[$i]['idcom'] = $listremise->IdCommercant;
                $allarrays[$i]['partenaire'] = $listremise->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listremise->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_tampon !=[]){
            foreach ($toListeFidelity_tampon as $listtampon){
                $allarrays[$i]['id'] = $listtampon->id;
                $allarrays[$i]['titre'] = $listtampon->titre;
                $allarrays[$i]['description'] = $listtampon->description;
                $allarrays[$i]['image1'] = $listtampon->image1;
                $allarrays[$i]['date_fin'] = $listtampon->date_fin;
                $allarrays[$i]['type'] = "tampon";
                $allarrays[$i]['ville_nom'] = $listtampon->ville;
                $allarrays[$i]['ville_id'] = $listtampon->IdVille;
                $allarrays[$i]['idcom'] = $listtampon->IdCommercant;
                $allarrays[$i]['partenaire'] = $listtampon->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listtampon->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_capital !=[]){
            foreach ($toListeFidelity_capital as $listcapitel){
                $allarrays[$i]['id'] = $listcapitel->id;
                $allarrays[$i]['titre'] = $listcapitel->titre;
                $allarrays[$i]['description'] = $listcapitel->description;
                $allarrays[$i]['image1'] = $listcapitel->image1;
                $allarrays[$i]['date_fin'] = $listcapitel->date_fin;
                $allarrays[$i]['type'] = "capital";
                $allarrays[$i]['ville_nom'] = $listcapitel->ville;
                $allarrays[$i]['ville_id'] = $listcapitel->IdVille;
                $allarrays[$i]['idcom'] = $listcapitel->IdCommercant;
                $allarrays[$i]['partenaire'] = $listcapitel->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listcapitel->IdCommercant)->nom_url;
                $i++;
            }
        }
        if ($toListeBonPlan_all_pvc !=[]){
            foreach ($toListeBonPlan_all_pvc as $bonplan){
                //var_dump($bonplan);die();
                if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "2"){
                    $date = $bonplan->bp_unique_date_fin;
                    //$type = "Bonplan unique";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "1"){
                    $date = $bonplan->bonplan_date_fin;
                    //$type = "Bonplan simple";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "3"){
                    $date = $bonplan->bp_multiple_date_fin;
                    //$type = "Bonplan multiples";
                }

                $allarrays[$i]['id'] = $bonplan->bonplan_id;
                $allarrays[$i]['titre'] = $bonplan->bonplan_titre;
                $allarrays[$i]['description'] = $bonplan->bonplan_texte;
                $allarrays[$i]['image1'] = $bonplan->bonplan_photo1;
                $allarrays[$i]['date_fin'] = $date;
                $allarrays[$i]['type'] = $bonplan->bonplan_type;
                $allarrays[$i]['ville_nom'] = $this->mdlville->getVilleById($this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation)->Nom;
                $allarrays[$i]['ville_id'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation;
                $allarrays[$i]['idcom'] = $bonplan->bonplan_commercant_id;
                $allarrays[$i]['partenaire'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->nom_url;
                $i++;
            }
        }
        if ($toListePlat_du_jour !=[]){
            foreach ($toListePlat_du_jour as $plat){
                $allarrays[$i]['id'] = $plat->id;
                $allarrays[$i]['titre'] = $plat->rubrique;
                $allarrays[$i]['description'] = $plat->description_plat;
                $allarrays[$i]['image1'] = $plat->photo;
                $allarrays[$i]['date_fin'] = $plat->date_fin_plat;
                $allarrays[$i]['inputDatedebut'] = $plat->date_fin_plat;
                $allarrays[$i]['type'] = "plat";
                $allarrays[$i]['nbre_plat_propose'] = $plat->nbre_plat_propose;
                $allarrays[$i]['ville_nom'] = $plat->ville;
                $allarrays[$i]['ville_id'] = $plat->IdVille;
                $allarrays[$i]['idcom'] = $plat->IdCommercant;
                $allarrays[$i]['partenaire'] = $plat->NomSociete;
                $allarrays[$i]['prix_plat'] = $plat->prix_plat;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($plat->IdCommercant)->nom_url;
                $i++;
            }
        }
        $data['alldata'] = $allarrays;

        //$this->load->view('agenda/partner_agenda_details', $data) ;
        //var_dump($data);
        $this->load->view('front_soutenons/partner_agenda_details_desk', $data);

    }

    function article_partner_details($_iCommercantId)

    {



        //code standard utilisation page partenaire START **************************************************



        $nom_url_commercant = $this->uri->rsegment(3);



        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);



        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);



        $data['oInfoCommercant'] = $oInfoCommercant;



        //$data['mdlannonce'] = $this->mdlannonce ;

        $data['mdlbonplan'] = $this->mdlbonplan;



        $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

        $oBonPlan = $this->mdlbonplan->bonPlanParCommercant($_iCommercantId);

        $data['nbBonPlan'] = sizeof($oBonPlan);

        //		$data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_titre : "";		// OP 27/11/2011

        $data['sTitreBonPlan'] = (sizeof($oBonPlan) > 0) ? $oBonPlan->bonplan_texte : "";

        $data['oImagespub'] = $this->mdlimagespub->GetByImagespubActiv();

        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);



        $oLastbonplanCom = $this->mdlbonplan->lastBonplanCom($_iCommercantId);

        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;



        $data['active_link'] = "article";



        $is_mobile = $this->agent->is_mobile();

        //test ipad user agent

        $is_mobile_ipad = $this->agent->is_mobile('ipad');

        $data['is_mobile_ipad'] = $is_mobile_ipad;

        $is_robot = $this->agent->is_robot();

        $is_browser = $this->agent->is_browser();

        $is_platform = $this->agent->platform();

        $data['is_mobile'] = $is_mobile;

        $data['is_robot'] = $is_robot;

        $data['is_browser'] = $is_browser;

        $data['is_platform'] = $is_platform;



        $this->load->model("user");

        if ($this->ion_auth->logged_in()) {

            $user_ion_auth = $this->ion_auth->user()->row();

            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

            if ($iduser == null || $iduser == 0 || $iduser == "") {

                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

            }

        } else $iduser = 0;



        //code standard utilisation page partenaire END **************************************************





        $id_agenda = $this->uri->rsegment(4);



        $oDetailAgenda = $this->mdlarticle->GetById_IsActif($id_agenda);

        $data['oDetailAgenda'] = $oDetailAgenda;



        $data['pagecategory_partner'] = "details_article";



        //sending mail to event organiser **************************

        if (isset($_POST['text_mail_form_module_detailbonnplan'])) {

            $text_mail_form_module_detailbonnplan = $this->input->post("text_mail_form_module_detailbonnplan");

            $nom_mail_form_module_detailbonnplan = $this->input->post("nom_mail_form_module_detailbonnplan");

            $tel_mail_form_module_detailbonnplan = $this->input->post("tel_mail_form_module_detailbonnplan");

            $email_mail_form_module_detailbonnplan = $this->input->post("email_mail_form_module_detailbonnplan");



            $colDestAdmin = array();

            $colDestAdmin[] = array("Email" => $oDetailAgenda->email, "Name" => $oDetailAgenda->nom_manifestation . " " . $oDetailAgenda->nom_societe);



            // Sujet

            $txtSujetAdmin = "Demande d'information sur un article sur Sortez";



            $txtContenuAdmin = "

            <p>Bonjour ,</p>

            <p>Une demande d'information vous est adressé suite à un évennement que vous avez déposé sur le site Agenda.</p>

            <p>Détails :<br/>

            Evennement : " . $oDetailAgenda->nom_manifestation . "

            <br/>

            Date : " . translate_date_to_fr($oDetailAgenda->date_debut) . "<br/>

            Lieu : " . $oDetailAgenda->ville . " " . $oDetailAgenda->adresse_localisation . " " . $oDetailAgenda->codepostal_localisation . "<br/>

            Organisateur : " . $oDetailAgenda->organisateur . "<br/><br/>

            Demande Client :<br/>

            " . $text_mail_form_module_detailbonnplan . "<br/><br/>

            Nom client : " . $nom_mail_form_module_detailbonnplan . "<br/>

            Tel client : " . $tel_mail_form_module_detailbonnplan . "<br/>

            Email client : " . $email_mail_form_module_detailbonnplan . "<br/>

            </p>";



            @envoi_notification($colDestAdmin, $txtSujetAdmin, $txtContenuAdmin);

            $data['mssg_envoi_module_detail_bonplan'] = '<font color="#00CC00">Votre demande est envoyée</font>';

            //$data['mssg_envoi_module_detail_bonplan'] = $txtContenuAdmin;



        } else $data['mssg_envoi_module_detail_bonplan'] = '';

        //sending mail to event organiser *******************************



        $this->load->model("mdl_agenda");

        $data['nombre_agenda_com'] = $this->mdlarticle->GetByIdCommercant($_iCommercantId);



        $data['pagecategory'] = 'pro';

        $data['pagecategory_partner'] = 'article_partner';



        $data['link_partner_current_page'] = 'article';



        $data['current_partner_menu'] = "article";



        $data['toArticle_datetime'] = $this->mdl_article_datetime->getByArticleId($id_agenda);





        $data['cacher_slide'] = "1";



        //ajout bouton rond noire annonce/agenda

        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);

        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';

        else $data['result_check_commercant_annonce'] = '0';



        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);

        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';

        else $data['result_check_commercant_agenda'] = '0';



//        $is_reserved_on=$this->Mdl_plat_du_jour->verify_reserved($_iCommercantId);
//
//        $data['reservation']=$is_reserved_on;
        $get_data_all__content=$this->Mdl_plat_du_jour->get_all_plat_by_idcom($_iCommercantId);
        $data['reservation']=$get_data_all__content;

        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;
        $toListeFidelity_remise = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "remise");
        $toListeFidelity_tampon = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "tampon");
        $toListeFidelity_capital = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "capital");
        $toListeBonPlan_all_pvc = $this->mdlbonplan->listeBonPlanRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "","");
        $toListePlat_du_jour = $this->Mdl_plat_du_jour->listeplatRecherche( "", "", "",  0, 10000, "", "", "", "", $_iCommercantId,"");

        $i = 0;
        if ($toListeFidelity_remise !=[]){
            foreach ($toListeFidelity_remise as $listremise){
                $allarrays[$i]['id'] = $listremise->id;
                $allarrays[$i]['titre'] = $listremise->titre;
                $allarrays[$i]['description'] = $listremise->description;
                $allarrays[$i]['image1'] = $listremise->image1;
                $allarrays[$i]['date_fin'] = $listremise->date_fin;
                $allarrays[$i]['type'] = "remise";
                $allarrays[$i]['ville_nom'] = $listremise->ville;
                $allarrays[$i]['ville_id'] = $listremise->IdVille;
                $allarrays[$i]['idcom'] = $listremise->IdCommercant;
                $allarrays[$i]['partenaire'] = $listremise->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listremise->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_tampon !=[]){
            foreach ($toListeFidelity_tampon as $listtampon){
                $allarrays[$i]['id'] = $listtampon->id;
                $allarrays[$i]['titre'] = $listtampon->titre;
                $allarrays[$i]['description'] = $listtampon->description;
                $allarrays[$i]['image1'] = $listtampon->image1;
                $allarrays[$i]['date_fin'] = $listtampon->date_fin;
                $allarrays[$i]['type'] = "tampon";
                $allarrays[$i]['ville_nom'] = $listtampon->ville;
                $allarrays[$i]['ville_id'] = $listtampon->IdVille;
                $allarrays[$i]['idcom'] = $listtampon->IdCommercant;
                $allarrays[$i]['partenaire'] = $listtampon->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listtampon->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_capital !=[]){
            foreach ($toListeFidelity_capital as $listcapitel){
                $allarrays[$i]['id'] = $listcapitel->id;
                $allarrays[$i]['titre'] = $listcapitel->titre;
                $allarrays[$i]['description'] = $listcapitel->description;
                $allarrays[$i]['image1'] = $listcapitel->image1;
                $allarrays[$i]['date_fin'] = $listcapitel->date_fin;
                $allarrays[$i]['type'] = "capital";
                $allarrays[$i]['ville_nom'] = $listcapitel->ville;
                $allarrays[$i]['ville_id'] = $listcapitel->IdVille;
                $allarrays[$i]['idcom'] = $listcapitel->IdCommercant;
                $allarrays[$i]['partenaire'] = $listcapitel->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listcapitel->IdCommercant)->nom_url;
                $i++;
            }
        }
        if ($toListeBonPlan_all_pvc !=[]){
            foreach ($toListeBonPlan_all_pvc as $bonplan){
                //var_dump($bonplan);die();
                if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "2"){
                    $date = $bonplan->bp_unique_date_fin;
                    //$type = "Bonplan unique";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "1"){
                    $date = $bonplan->bonplan_date_fin;
                    //$type = "Bonplan simple";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "3"){
                    $date = $bonplan->bp_multiple_date_fin;
                    //$type = "Bonplan multiples";
                }

                $allarrays[$i]['id'] = $bonplan->bonplan_id;
                $allarrays[$i]['titre'] = $bonplan->bonplan_titre;
                $allarrays[$i]['description'] = $bonplan->bonplan_texte;
                $allarrays[$i]['image1'] = $bonplan->bonplan_photo1;
                $allarrays[$i]['date_fin'] = $date;
                $allarrays[$i]['type'] = $bonplan->bonplan_type;
                $allarrays[$i]['ville_nom'] = $this->mdlville->getVilleById($this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation)->Nom;
                $allarrays[$i]['ville_id'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation;
                $allarrays[$i]['idcom'] = $bonplan->bonplan_commercant_id;
                $allarrays[$i]['partenaire'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->nom_url;
                $i++;
            }
        }
        if ($toListePlat_du_jour !=[]){
            foreach ($toListePlat_du_jour as $plat){
                $allarrays[$i]['id'] = $plat->id;
                $allarrays[$i]['titre'] = $plat->rubrique;
                $allarrays[$i]['description'] = $plat->description_plat;
                $allarrays[$i]['image1'] = $plat->photo;
                $allarrays[$i]['date_fin'] = $plat->date_fin_plat;
                $allarrays[$i]['inputDatedebut'] = $plat->date_fin_plat;
                $allarrays[$i]['type'] = "plat";
                $allarrays[$i]['nbre_plat_propose'] = $plat->nbre_plat_propose;
                $allarrays[$i]['ville_nom'] = $plat->ville;
                $allarrays[$i]['ville_id'] = $plat->IdVille;
                $allarrays[$i]['idcom'] = $plat->IdCommercant;
                $allarrays[$i]['partenaire'] = $plat->NomSociete;
                $allarrays[$i]['prix_plat'] = $plat->prix_plat;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($plat->IdCommercant)->nom_url;
                $i++;
            }
        }
        $data['alldata'] = $allarrays;



        //$this->load->view('agenda/partner_agenda_details', $data);

        //$this->load->view('privicarte/partner_agenda_details', $data);

        //var_dump($data);

        $this->load->view('front_soutenons/partner_article_details_desk', $data);



    }
    public function condition_general($_iCommercantId=0){
        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
        $data['oInfoCommercant'] = $oInfoCommercant ;
        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commandes'] = $infocommande;
        $this->load->view('front_soutenons/includes_commande/conditions_generales',$data);

    }
    public function deal_fidelity_commercants(){
        $nom_url_commercant = $this->uri->rsegment(3);
        $session_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        //var_dump($session_iCommercantId);die();
        $oInfoCommercant = $this->mdlcommercant->infoCommercant($session_iCommercantId) ;
        $data['oInfoCommercant'] = $oInfoCommercant ;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user") ;
        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser=0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$session_iCommercantId));
        $data['pagecategory'] = "pro";

        $get_data_all__content=$this->Mdl_plat_du_jour->get_all_plat_by_idcom($session_iCommercantId);
        $data['reservation']=$get_data_all__content;

        $toListeFidelity_remise = $this->mdlfidelity->listeFidelityRecherche("", $session_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "remise");
        $toListeFidelity_tampon = $this->mdlfidelity->listeFidelityRecherche("", $session_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "tampon");
        $toListeFidelity_capital = $this->mdlfidelity->listeFidelityRecherche("", $session_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "capital");
        $toListeBonPlan_all_pvc = $this->mdlbonplan->listeBonPlanRecherche("", $session_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "","");
        //$toListePlat_du_jour = $this->Mdl_plat_du_jour->listeplatRecherche( "", "", "",  0, 10000, "", "", "", "", $session_iCommercantId,"");
        $i = 0;
        if ($toListeFidelity_remise !=[]){
            foreach ($toListeFidelity_remise as $listremise){
                $allarrays[$i]['id'] = $listremise->id;
                $allarrays[$i]['titre'] = $listremise->titre;
                $allarrays[$i]['description'] = $listremise->description;
                $allarrays[$i]['image1'] = $listremise->image1;
                $allarrays[$i]['date_fin'] = $listremise->date_fin;
                $allarrays[$i]['type'] = "remise";
                $allarrays[$i]['ville_nom'] = $listremise->ville;
                $allarrays[$i]['ville_id'] = $listremise->IdVille;
                $allarrays[$i]['idcom'] = $listremise->IdCommercant;
                $allarrays[$i]['partenaire'] = $listremise->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listremise->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_tampon !=[]){
            foreach ($toListeFidelity_tampon as $listtampon){
                $allarrays[$i]['id'] = $listtampon->id;
                $allarrays[$i]['titre'] = $listtampon->titre;
                $allarrays[$i]['description'] = $listtampon->description;
                $allarrays[$i]['image1'] = $listtampon->image1;
                $allarrays[$i]['date_fin'] = $listtampon->date_fin;
                $allarrays[$i]['type'] = "tampon";
                $allarrays[$i]['ville_nom'] = $listtampon->ville;
                $allarrays[$i]['ville_id'] = $listtampon->IdVille;
                $allarrays[$i]['idcom'] = $listtampon->IdCommercant;
                $allarrays[$i]['partenaire'] = $listtampon->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listtampon->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_capital !=[]){
            foreach ($toListeFidelity_capital as $listcapitel){
                $allarrays[$i]['id'] = $listcapitel->id;
                $allarrays[$i]['titre'] = $listcapitel->titre;
                $allarrays[$i]['description'] = $listcapitel->description;
                $allarrays[$i]['image1'] = $listcapitel->image1;
                $allarrays[$i]['date_fin'] = $listcapitel->date_fin;
                $allarrays[$i]['type'] = "capital";
                $allarrays[$i]['ville_nom'] = $listcapitel->ville;
                $allarrays[$i]['ville_id'] = $listcapitel->IdVille;
                $allarrays[$i]['idcom'] = $listcapitel->IdCommercant;
                $allarrays[$i]['partenaire'] = $listcapitel->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listcapitel->IdCommercant)->nom_url;
                $i++;
            }
        }
        if ($toListeBonPlan_all_pvc !=[]){
            foreach ($toListeBonPlan_all_pvc as $bonplan){
                //var_dump($bonplan);die();
                if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "2"){
                    $date = $bonplan->bp_unique_date_fin;
                    //$type = "Bonplan unique";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "1"){
                    $date = $bonplan->bonplan_date_fin;
                    //$type = "Bonplan simple";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "3"){
                    $date = $bonplan->bp_multiple_date_fin;
                    //$type = "Bonplan multiples";
                }

                $allarrays[$i]['id'] = $bonplan->bonplan_id;
                $allarrays[$i]['titre'] = $bonplan->bonplan_titre;
                $allarrays[$i]['description'] = $bonplan->bonplan_texte;
                $allarrays[$i]['image1'] = $bonplan->bonplan_photo1;
                $allarrays[$i]['date_fin'] = $date;
                $allarrays[$i]['type'] = $bonplan->bonplan_type;
                $allarrays[$i]['ville_nom'] = $this->mdlville->getVilleById($this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation)->Nom;
                $allarrays[$i]['ville_id'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation;
                $allarrays[$i]['idcom'] = $bonplan->bonplan_commercant_id;
                $allarrays[$i]['partenaire'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->nom_url;
                $i++;
            }
        }
        $data['alldata'] = $allarrays;
        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($session_iCommercantId));
        $data['commande'] = $infocommande;
        $this->load->view('front_soutenons/deal_fidelity_front',$data);
    }
    function detailFidelity($_iCommercantId = 0, $_iTypeFidelity = "capital", $_IdFidelity = 0, $mssg = 0)

    {

        $nom_url_commercant = $this->uri->rsegment(3);

        $_iTypeFidelity = $this->uri->rsegment(4);

        $_IdFidelity = $this->uri->rsegment(5);



        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;


        if (isset($_iCommercantId) && $_iCommercantId != NULL && $_iCommercantId != "0" && isset($_iTypeFidelity) && $_iTypeFidelity != NULL && $_iTypeFidelity != "" && isset($_IdFidelity) && $_IdFidelity != NULL && $_IdFidelity != "" && $_IdFidelity != "0") {

            if ($this->ion_auth->logged_in()) {

                $user_ion_auth = $this->ion_auth->user()->row();

                $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

                if ($iduser == null || $iduser == 0 || $iduser == "") {

                    $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

                }

                $data['id_client'] = $iduser;

            }



            $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);

            $data['oInfoCommercant'] = $oInfoCommercant;



            //$this->load->model("mdl_card_remise") ;

            //$this->load->model("mdl_card_tampon") ;

            //$this->load->model("mdl_card_capital") ;



            if ($_iTypeFidelity == "remise") {

                $oFidelity = $this->mdl_card_remise->getById($_IdFidelity);

                $data['toFidelity'] = $oFidelity;

            } elseif ($_iTypeFidelity == "tampon") {

                $oFidelity = $this->mdl_card_tampon->getById($_IdFidelity);

                $data['toFidelity'] = $oFidelity;

            } else { // $_iTypeFidelity == "capital"

                $oFidelity = $this->mdl_card_capital->getById($_IdFidelity);

                $data['toFidelity'] = $oFidelity;

            }



            $data['TypeFidelity'] = $_iTypeFidelity;





            $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId);

            $data['toBonPlan'] = $toBonPlan;



            $oBonPlan2 = $this->mdlbonplan->lastBonplanCom2($_iCommercantId);

            $data['toBonPlan2'] = $oBonPlan2;



            $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

            $oBonPlan = $this->mdlbonplan->bonPlanParCommercant($_iCommercantId);

            $data['nbBonPlan'] = sizeof($oBonPlan);





            $data['active_link'] = "fidelity";

            $oAssComRub = $this->mdlcommercant->GetRubriqueId($_iCommercantId);

            if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);

            $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

            $oLastbonplanCom = $this->mdlbonplan->lastBonplanCom($_iCommercantId);

            if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;



            $is_mobile = $this->agent->is_mobile();

            //test ipad user agent

            $is_mobile_ipad = $this->agent->is_mobile('ipad');

            $data['is_mobile_ipad'] = $is_mobile_ipad;

            $is_robot = $this->agent->is_robot();

            $is_browser = $this->agent->is_browser();

            $is_platform = $this->agent->platform();

            $data['is_mobile'] = $is_mobile;

            $data['is_robot'] = $is_robot;

            $data['is_browser'] = $is_browser;

            $data['is_platform'] = $is_platform;



            $this->load->model("user") ;
            if ($this->ion_auth->logged_in()) {

                $user_ion_auth = $this->ion_auth->user()->row();

                $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

                if ($iduser == null || $iduser == 0 || $iduser == "") {

                    $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

                }

            } else $iduser = 0;

            $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser, $_iCommercantId));



            //lien pour retour automatique

            $page_from = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

            $_SESSION['page_from'] = $page_from;

            //lien pour retour automatique



            $data['mssg'] = $mssg;

            $data['pagecategory_partner'] = 'bonplans_partner';



            $data['pagecategory'] = "pro";



            $this->load->model("mdl_agenda");

            $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);



            $data['link_partner_current_page'] = 'bonplan';



            $data['current_partner_menu'] = "bonplan";





            //ajout bouton rond noire annonce/agenda

            $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);

            if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';

            else $data['result_check_commercant_annonce'] = '0';



            $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);

            if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';

            else $data['result_check_commercant_agenda'] = '0';
            $toListeFidelity_remise = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "remise");
            $toListeFidelity_tampon = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "tampon");
            $toListeFidelity_capital = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "capital");
            $toListeBonPlan_all_pvc = $this->mdlbonplan->listeBonPlanRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "","");
            $toListePlat_du_jour = $this->Mdl_plat_du_jour->listeplatRecherche( "", "", "",  0, 10000, "", "", "", "", $_iCommercantId,"");
            $i = 0;
            if ($toListeFidelity_remise !=[]){
                foreach ($toListeFidelity_remise as $listremise){
                    $allarrays[$i]['id'] = $listremise->id;
                    $allarrays[$i]['titre'] = $listremise->titre;
                    $allarrays[$i]['description'] = $listremise->description;
                    $allarrays[$i]['image1'] = $listremise->image1;
                    $allarrays[$i]['date_fin'] = $listremise->date_fin;
                    $allarrays[$i]['type'] = "remise";
                    $allarrays[$i]['ville_nom'] = $listremise->ville;
                    $allarrays[$i]['ville_id'] = $listremise->IdVille;
                    $allarrays[$i]['idcom'] = $listremise->IdCommercant;
                    $allarrays[$i]['partenaire'] = $listremise->NomSociete;
                    $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listremise->IdCommercant)->nom_url;
                    $i ++;
                }
            }
            if ($toListeFidelity_tampon !=[]){
                foreach ($toListeFidelity_tampon as $listtampon){
                    $allarrays[$i]['id'] = $listtampon->id;
                    $allarrays[$i]['titre'] = $listtampon->titre;
                    $allarrays[$i]['description'] = $listtampon->description;
                    $allarrays[$i]['image1'] = $listtampon->image1;
                    $allarrays[$i]['date_fin'] = $listtampon->date_fin;
                    $allarrays[$i]['type'] = "tampon";
                    $allarrays[$i]['ville_nom'] = $listtampon->ville;
                    $allarrays[$i]['ville_id'] = $listtampon->IdVille;
                    $allarrays[$i]['idcom'] = $listtampon->IdCommercant;
                    $allarrays[$i]['partenaire'] = $listtampon->NomSociete;
                    $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listtampon->IdCommercant)->nom_url;
                    $i ++;
                }
            }
            if ($toListeFidelity_capital !=[]){
                foreach ($toListeFidelity_capital as $listcapitel){
                    $allarrays[$i]['id'] = $listcapitel->id;
                    $allarrays[$i]['titre'] = $listcapitel->titre;
                    $allarrays[$i]['description'] = $listcapitel->description;
                    $allarrays[$i]['image1'] = $listcapitel->image1;
                    $allarrays[$i]['date_fin'] = $listcapitel->date_fin;
                    $allarrays[$i]['type'] = "capital";
                    $allarrays[$i]['ville_nom'] = $listcapitel->ville;
                    $allarrays[$i]['ville_id'] = $listcapitel->IdVille;
                    $allarrays[$i]['idcom'] = $listcapitel->IdCommercant;
                    $allarrays[$i]['partenaire'] = $listcapitel->NomSociete;
                    $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listcapitel->IdCommercant)->nom_url;
                    $i++;
                }
            }
            if ($toListeBonPlan_all_pvc !=[]){
                foreach ($toListeBonPlan_all_pvc as $bonplan){
                    //var_dump($bonplan);die();
                    if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "2"){
                        $date = $bonplan->bp_unique_date_fin;
                        //$type = "Bonplan unique";
                    }
                    else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "1"){
                        $date = $bonplan->bonplan_date_fin;
                        //$type = "Bonplan simple";
                    }
                    else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "3"){
                        $date = $bonplan->bp_multiple_date_fin;
                        //$type = "Bonplan multiples";
                    }

                    $allarrays[$i]['id'] = $bonplan->bonplan_id;
                    $allarrays[$i]['titre'] = $bonplan->bonplan_titre;
                    $allarrays[$i]['description'] = $bonplan->bonplan_texte;
                    $allarrays[$i]['image1'] = $bonplan->bonplan_photo1;
                    $allarrays[$i]['date_fin'] = $date;
                    $allarrays[$i]['type'] = $bonplan->bonplan_type;
                    $allarrays[$i]['ville_nom'] = $this->mdlville->getVilleById($this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation)->Nom;
                    $allarrays[$i]['ville_id'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation;
                    $allarrays[$i]['idcom'] = $bonplan->bonplan_commercant_id;
                    $allarrays[$i]['partenaire'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->NomSociete;
                    $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->nom_url;
                    $i++;
                }
            }
            if ($toListePlat_du_jour !=[]){
                foreach ($toListePlat_du_jour as $plat){
                    $allarrays[$i]['id'] = $plat->id;
                    $allarrays[$i]['titre'] = $plat->rubrique;
                    $allarrays[$i]['description'] = $plat->description_plat;
                    $allarrays[$i]['image1'] = $plat->photo;
                    $allarrays[$i]['date_fin'] = $plat->date_fin_plat;
                    $allarrays[$i]['inputDatedebut'] = $plat->date_fin_plat;
                    $allarrays[$i]['type'] = "plat";
                    $allarrays[$i]['nbre_plat_propose'] = $plat->nbre_plat_propose;
                    $allarrays[$i]['ville_nom'] = $plat->ville;
                    $allarrays[$i]['ville_id'] = $plat->IdVille;
                    $allarrays[$i]['idcom'] = $plat->IdCommercant;
                    $allarrays[$i]['partenaire'] = $plat->NomSociete;
                    $allarrays[$i]['prix_plat'] = $plat->prix_plat;
                    $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($plat->IdCommercant)->nom_url;
                    $i++;
                }
            }
            $data['alldata'] = $allarrays;
            $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
            $data['commande'] = $infocommande;



            //$this->load->view('front/vwBonPlanParCommercant', $data) ;

            //$this->load->view('front2013/page_bonplan_partenaire', $data) ;

            $this->load->view('front_soutenons/details_fidelity_front', $data);



        } else {

            redirect("front/fidelity");

        }





    }
    function get_filter_plat(){

        //$nom_url_commercant = $this->uri->rsegment(3);//die($nom_url_commercant." stop");//reecuperation valeur $nom_url_commercant
        //$_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $_iCommercantId = $this->input->post('idcom');
        $_date_now = $this->input->post('date_now');
        $_date_tomorrow = $this->input->post('date_tomorrow');
        $_date_specific = $this->input->post('date_specific');



        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId) ;
        $data['oInfoCommercant'] = $oInfoCommercant ;

        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;

        $data['active_link'] = "activite1";
        $oAssComRub = 	$this->mdlcommercant->GetRubriqueId($_iCommercantId);
        if ($oAssComRub) $data['oRubCom'] = $this->rubrique->GetId($oAssComRub->IdRubrique);
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oLastbonplanCom = 	$this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user") ;
        if ($this->ion_auth->logged_in()){
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser=0;
        $data['UserComNewsletter'] = count($this->user->verifUserComNewsletter($iduser ,$_iCommercantId));

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            //var_dump($user_ion_auth);die('data users');
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            $data['IdUser'] = $iduser;
            $card = $this->Mdl_card->getByIdUser($iduser);
            $data['num_card'] = $card;
            $res = $this->Mdl_soutenons->getuser_by_id_card($card->num_id_card_virtual);
            $data['res'] = $res;
            $data['generate_qrcode'] = $user_ion_auth;
        }

        $data['current_page'] = "page_infos";
        $data['pagecategory'] = "pro";

        $data['pageglissiere'] = "page1";
        $this->load->model("mdlglissiere") ;
        $data['mdlglissiere'] = $this->mdlglissiere;
        $data['Mdl_plat_du_jour'] = $this->Mdl_plat_du_jour;

        $data['link_partner_current_page'] = 'page1';

        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId) ;
        $data['toBonPlan'] = $toBonPlan ;

        //ajout bouton rond noire annonce/agenda
        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
        else $data['result_check_commercant_annonce'] = '0';

        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
        else $data['result_check_commercant_agenda'] = '0';

        $get_data_menu=$this->Mdl_plat_du_jour->get_title_menu_by_idcom($_iCommercantId);

        $get_data_all__content=$this->Mdl_plat_du_jour->get_all_plat_by_idcom_filter($_iCommercantId,$_date_now,$_date_tomorrow,$_date_specific);

        $data['reservation']=$get_data_all__content;
        $data['data_menu']=$get_data_menu;
        $toListeFidelity_remise = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "remise");
        $toListeFidelity_tampon = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "tampon");
        $toListeFidelity_capital = $this->mdlfidelity->listeFidelityRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "", "capital");
        $toListeBonPlan_all_pvc = $this->mdlbonplan->listeBonPlanRecherche("", $_iCommercantId, "", "", 0, 10000, "", "", "", "", "", "", "", "","");
        //$toListePlat_du_jour = $this->Mdl_plat_du_jour->listeplatRecherche( "", "", "",  0, 10000, "", "", "", "", $_iCommercantId,"");

        $i = 0;
        if ($toListeFidelity_remise !=[]){
            foreach ($toListeFidelity_remise as $listremise){
                $allarrays[$i]['id'] = $listremise->id;
                $allarrays[$i]['titre'] = $listremise->titre;
                $allarrays[$i]['description'] = $listremise->description;
                $allarrays[$i]['image1'] = $listremise->image1;
                $allarrays[$i]['date_fin'] = $listremise->date_fin;
                $allarrays[$i]['type'] = "remise";
                $allarrays[$i]['ville_nom'] = $listremise->ville;
                $allarrays[$i]['ville_id'] = $listremise->IdVille;
                $allarrays[$i]['idcom'] = $listremise->IdCommercant;
                $allarrays[$i]['partenaire'] = $listremise->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listremise->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_tampon !=[]){
            foreach ($toListeFidelity_tampon as $listtampon){
                $allarrays[$i]['id'] = $listtampon->id;
                $allarrays[$i]['titre'] = $listtampon->titre;
                $allarrays[$i]['description'] = $listtampon->description;
                $allarrays[$i]['image1'] = $listtampon->image1;
                $allarrays[$i]['date_fin'] = $listtampon->date_fin;
                $allarrays[$i]['type'] = "tampon";
                $allarrays[$i]['ville_nom'] = $listtampon->ville;
                $allarrays[$i]['ville_id'] = $listtampon->IdVille;
                $allarrays[$i]['idcom'] = $listtampon->IdCommercant;
                $allarrays[$i]['partenaire'] = $listtampon->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listtampon->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_capital !=[]){
            foreach ($toListeFidelity_capital as $listcapitel){
                $allarrays[$i]['id'] = $listcapitel->id;
                $allarrays[$i]['titre'] = $listcapitel->titre;
                $allarrays[$i]['description'] = $listcapitel->description;
                $allarrays[$i]['image1'] = $listcapitel->image1;
                $allarrays[$i]['date_fin'] = $listcapitel->date_fin;
                $allarrays[$i]['type'] = "capital";
                $allarrays[$i]['ville_nom'] = $listcapitel->ville;
                $allarrays[$i]['ville_id'] = $listcapitel->IdVille;
                $allarrays[$i]['idcom'] = $listcapitel->IdCommercant;
                $allarrays[$i]['partenaire'] = $listcapitel->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($listcapitel->IdCommercant)->nom_url;
                $i++;
            }
        }
        if ($toListeBonPlan_all_pvc !=[]){
            foreach ($toListeBonPlan_all_pvc as $bonplan){
                //var_dump($bonplan);die();
                if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "2"){
                    $date = $bonplan->bp_unique_date_fin;
                    //$type = "Bonplan unique";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "1"){
                    $date = $bonplan->bonplan_date_fin;
                    //$type = "Bonplan simple";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "3"){
                    $date = $bonplan->bp_multiple_date_fin;
                    //$type = "Bonplan multiples";
                }

                $allarrays[$i]['id'] = $bonplan->bonplan_id;
                $allarrays[$i]['titre'] = $bonplan->bonplan_titre;
                $allarrays[$i]['description'] = $bonplan->bonplan_texte;
                $allarrays[$i]['image1'] = $bonplan->bonplan_photo1;
                $allarrays[$i]['date_fin'] = $date;
                $allarrays[$i]['type'] = $bonplan->bonplan_type;
                $allarrays[$i]['ville_nom'] = $this->mdlville->getVilleById($this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation)->Nom;
                $allarrays[$i]['ville_id'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->IdVille_localisation;
                $allarrays[$i]['idcom'] = $bonplan->bonplan_commercant_id;
                $allarrays[$i]['partenaire'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->Mdl_soutenons->GetById_commercants($bonplan->bonplan_commercant_id)->nom_url;
                $i++;
            }
        }
        $data['alldata'] = $allarrays;

        //$this->load->view('front/vwPlusInfoCommercant', $data) ;
        //$this->load->view('front2013/plusinfoscommercant', $data) ;
        $this->load->view('front_soutenons/includes/liste_reservation', $data) ;
    }

}