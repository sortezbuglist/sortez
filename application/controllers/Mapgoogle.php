<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mapgoogle extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model("mdlmapgoogle") ;
        $this->load->model("mdlville") ;
        $this->load->model("mdlbonplan") ;
        
		
		$this->load->library('session');

		check_vivresaville_id_ville();
    }


    public function getBonplanbyId(){
    	if(isset($_POST['IdBonplan'])) {
  			$oMapGoogle = $this->mdlmapgoogle->getIdVilleByIdBonplan($_POST['IdBonplan']);
  			$data['oMapGoogle'] = $oMapGoogle; 
  			$this->load->view('privicarte/mapgoogle', $data) ;
    	}
    }



	public function index()
	{
		//$this->load->view('welcome_message');

		if(isset($_POST['save_google_map_details'])) {


			$IdVille = 

			//Markers Parameters
			$address 			= $_POST['address'];
			$textfordirlink		= $_POST['textfordirectionslink'];
			$icon				= $_POST['icon'];
			$animation			= $_POST['animation'];
			$infowindow			= $_POST['infowindow'];
			$content			= $_POST['content'];
			
			$markers = array();
			
			$total_markers = count($address);
			for($i = 0; $i < $total_markers; $i++) {
				
				list($latitude,$longitude) = sscanf("$address[$i]", "%f,%f");
				if($latitude == '' || $longitude == '') {
					$location = json_decode(sb_get_lat_lng_from_address($address[$i]));		//Get latitude, longitude from geocode api
					if(strtoupper($location->status) == 'OK') {
						$latitude = $location->results[0]->geometry->location->lat;
						$longitude = $location->results[0]->geometry->location->lng;
					} else {
						$latitude = 0;
						$longitude = 0;
					}
				}
				
				$markers[] = array(
					'address'					=>	sanitize_text_field($address[$i]),
					'latitude'					=>	$latitude,
					'longitude'					=>	$longitude,
					'textfordirectionslink'		=>	sanitize_text_field($textfordirlink[$i]),
					'icon'						=>	sanitize_text_field($icon[$i]),
					'animation'					=>	sanitize_text_field($animation[$i]),
					'infowindow'				=>	sanitize_text_field($infowindow[$i]),
					'content'					=>	$content[$i]
				);
			}

		}


	}



}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */