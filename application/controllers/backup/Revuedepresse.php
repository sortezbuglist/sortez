<?php

class Revuedepresse extends CI_Controller
{

    //////////////////////////parsehub parameter///////////////////////////////


    /////////////////////////////////////////////////////////////////////////////

    function __construct()
    {
        parent::__construct();
        $this->output->set_header('Access-Control-Allow-Origin: null');  header('Access-Control-Allow-Credentials: omit');
        header('Access-Control-Allow-Method: PUT, GET, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');
        $this->load->model("mdlrevue");
        $this->load->model("mdlannonce");
        $this->load->model("mdlcommercant");
        $this->load->model("mdlbonplan");
        $this->load->model("mdlcategorie");
        $this->load->model("mdlville");
        $this->load->model("mdldepartement");
        $this->load->model("mdlcommercantpagination");
        $this->load->model("sousRubrique");
        $this->load->model("AssCommercantAbonnement");
        $this->load->model("AssCommercantSousRubrique");
        $this->load->model("Commercant");
        $this->load->model("user");
        $this->load->model("mdl_agenda");
        $this->load->model("mdl_categories_agenda");
        $this->load->model("mdl_categories_article");
        $this->load->model("mdlimagespub");
        $this->load->model("Abonnement");
        $this->load->Model("mdlrevue_perso");
        $this->load->Model("mdl_article_datetime");
        $this->load->Model("mdl_agenda_datetime");

        $this->load->Model("mdl_localisation");
        $this->load->Model("mdl_article_organiser");

        $this->load->library('user_agent');
        $this->load->library("pagination");
        $this->load->library('image_moo');
        $this->load->library('session');

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");


        //check_vivresaville_id_ville();


    }

    function index()
    {
        $this->liste();
    }

    function index_alaune()
    {

        /*$this->config->load('config');
        echo $this->config->item('base_url');
        echo "<br/>".$_SERVER['HTTP_HOST'];*/

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        if (isset($_POST["from_mobile_search_page_partner"])) {
            $data['from_mobile_search_page_partner'] = $_POST["from_mobile_search_page_partner"];
            $this->session->set_userdata('from_mobile_search_page_partner', $_POST["from_mobile_search_page_partner"]);
        }
        $from_mobile_search_page_partner = $this->session->userdata('from_mobile_search_page_partner');
        if (isset($from_mobile_search_page_partner)) $data['from_mobile_search_page_partner'] = $from_mobile_search_page_partner;

        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
            $data['IdUser'] = $iduser;
        }

        $data['oAgenda_alaune'] = $this->mdl_agenda->GetAllalaune();


        $toVille = $this->mdlville->GetAgendaVilles();//get ville list of agenda
        $data['toVille'] = $toVille;
        $toCategorie_principale = $this->mdl_agenda->GetAgendaCategorie();
        $data['toCategorie_principale'] = $toCategorie_principale;


        //echo "agenda";
        $this->load->view('agenda/home_main', $data);

    }

    function test_remove_all_old_data()
    {
        //$this->mdl_agenda->get_null_article_datetime();
        //$this->mdl_agenda->delete_null_agenda_datetime();
        echo "ok";
    }

    /*function test_datetime_completion() {
        $toresult = $this->mdl_agenda->GetAll();
        foreach ($toresult as $item) {
            echo $item->id."<br/>";
            $datetime = array();
            if ((isset($item->date_debut) && $item->date_debut != "" && $item->date_debut != "0000-00-00" && $item->date_debut != null) || (isset($item->date_fin) && $item->date_fin != "" && $item->date_fin != "0000-00-00" && $item->date_fin != null)) {
                $datetime['agenda_id'] = $item->id;
				if (isset($item->date_debut) && $item->date_debut == "0000-00-00") $item->date_debut = null;
                $datetime['date_debut'] = $item->date_debut;
                if (isset($item->date_fin) && $item->date_fin == "0000-00-00") $item->date_fin = null;
                $datetime['date_fin'] = $item->date_fin;
                $datetime['heure_debut'] = $item->heure_debut;
                $inserted_id_datetime = $this->mdl_agenda_datetime->insert($datetime);
            }
            $datetime_1 = array();
            if ((isset($item->date_debut_1) && $item->date_debut_1 != "" && $item->date_debut_1 != "0000-00-00" && $item->date_debut_1 != null) || (isset($item->date_fin_1) && $item->date_fin_1 != "" && $item->date_fin_1 != "0000-00-00" && $item->date_fin_1 != null)) {
                $datetime_1['agenda_id'] = $item->id;
				if (isset($item->date_debut_1) && $item->date_debut_1 == "0000-00-00") $item->date_debut_1 = null;
                $datetime_1['date_debut'] = $item->date_debut_1;
                if (isset($item->date_fin_1) && $item->date_fin_1 == "0000-00-00") $item->date_fin_1 = null;
                $datetime_1['date_fin'] = $item->date_fin_1;
                $datetime_1['heure_debut'] = $item->heure_debut_1;
                $inserted_id_datetime_1 = $this->mdl_agenda_datetime->insert($datetime_1);
            }
            $datetime_2 = array();
            if ((isset($item->date_debut_2) && $item->date_debut_2 != "" && $item->date_debut_2 != "0000-00-00" && $item->date_debut_2 != null) || (isset($item->date_fin_2) && $item->date_fin_2 != "" && $item->date_fin_2 != "0000-00-00" && $item->date_fin_2 != null)) {
                $datetime_2['agenda_id'] = $item->id;
				if (isset($item->date_debut_2) && $item->date_debut_2 == "0000-00-00") $item->date_debut_2 = null;
                $datetime_2['date_debut'] = $item->date_debut_2;
                if (isset($item->date_fin_2) && $item->date_fin_2 == "0000-00-00") $item->date_fin_2 = null;
                $datetime_2['date_fin'] = $item->date_fin_2;
                $datetime_2['heure_debut'] = $item->heure_debut_2;
                $inserted_id_datetime_2 = $this->mdl_agenda_datetime->insert($datetime_2);
            }
			$datetime_3 = array();
            if ((isset($item->date_debut_3) && $item->date_debut_3 != "" && $item->date_debut_3 != "0000-00-00" && $item->date_debut_3 != null) || (isset($item->date_fin_3) && $item->date_fin_3 != "" && $item->date_fin_3 != "0000-00-00" && $item->date_fin_3 != null)) {
                $datetime_3['agenda_id'] = $item->id;
				if (isset($item->date_debut_3) && $item->date_debut_3 == "0000-00-00") $item->date_debut_3 = null;
                $datetime_3['date_debut'] = $item->date_debut_3;
                if (isset($item->date_fin_3) && $item->date_fin_3 == "0000-00-00") $item->date_fin_3 = null;
                $datetime_3['date_fin'] = $item->date_fin_3;
                $datetime_3['heure_debut'] = $item->heure_debut_3;
                $inserted_id_datetime_3 = $this->mdl_agenda_datetime->insert($datetime_3);
            }
			$datetime_4 = array();
            if ((isset($item->date_debut_4) && $item->date_debut_4 != "" && $item->date_debut_4 != "0000-00-00" && $item->date_debut_4 != null)||(isset($item->date_fin_4) && $item->date_fin_4 != "" && $item->date_fin_4 != "0000-00-00" && $item->date_fin_4 != null))  {
                $datetime_4['agenda_id'] = $item->id;
				if (isset($item->date_debut_4) && $item->date_debut_4 == "0000-00-00") $item->date_debut_4 = null;
                $datetime_4['date_debut'] = $item->date_debut_4;
                if (isset($item->date_fin_4) && $item->date_fin_4 == "0000-00-00") $item->date_fin_4 = null;
                $datetime_4['date_fin'] = $item->date_fin_4;
                $datetime_4['heure_debut'] = $item->heure_debut_4;
                $inserted_id_datetime_4 = $this->mdl_agenda_datetime->insert($datetime_4);
            }
			$datetime_5 = array();
            if ((isset($item->date_debut_5) && $item->date_debut_5 != "" && $item->date_debut_5 != "0000-00-00" && $item->date_debut_5 != null)||(isset($item->date_fin_5) && $item->date_fin_5 != "" && $item->date_fin_5 != "0000-00-00" && $item->date_fin_5 != null)) {
                $datetime_5['agenda_id'] = $item->id;
				if (isset($item->date_debut_5) && $item->date_debut_5 == "0000-00-00") $item->date_debut_5 = null;
                $datetime_5['date_debut'] = $item->date_debut_5;
                if (isset($item->date_fin_5) && $item->date_fin_5 == "0000-00-00") $item->date_fin_5 = null;
                $datetime_5['date_fin'] = $item->date_fin_5;
                $datetime_5['heure_debut'] = $item->heure_debut_5;
                $inserted_id_datetime_5 = $this->mdl_agenda_datetime->insert($datetime_5);
            }
			$datetime_6 = array();
            if ((isset($item->date_debut_6) && $item->date_debut_6 != "" && $item->date_debut_6 != "0000-00-00" && $item->date_debut_6 != null)||(isset($item->date_fin_6) && $item->date_fin_6 != "" && $item->date_fin_6 != "0000-00-00" && $item->date_fin_6 != null)) {
                $datetime_6['agenda_id'] = $item->id;
				if (isset($item->date_debut_6) && $item->date_debut_6 == "0000-00-00") $item->date_debut_6 = null;
                $datetime_6['date_debut'] = $item->date_debut_6;
                if (isset($item->date_fin_6) && $item->date_fin_6 == "0000-00-00") $item->date_fin_6 = null;
                $datetime_6['date_fin'] = $item->date_fin_6;
                $datetime_6['heure_debut'] = $item->heure_debut_6;
                $inserted_id_datetime_6 = $this->mdl_agenda_datetime->insert($datetime_6);
            }
			$datetime_7 = array();
            if ((isset($item->date_debut_7) && $item->date_debut_7 != "" && $item->date_debut_7 != "0000-00-00" && $item->date_debut_7 != null)||(isset($item->date_fin_7) && $item->date_fin_7 != "" && $item->date_fin_7 != "0000-00-00" && $item->date_fin_7 != null)) {
                $datetime_7['agenda_id'] = $item->id;
				if (isset($item->date_debut_7) && $item->date_debut_7 == "0000-00-00") $item->date_debut_7 = null;
                $datetime_7['date_debut'] = $item->date_debut_7;
                if (isset($item->date_fin_7) && $item->date_fin_7 == "0000-00-00") $item->date_fin_7 = null;
                $datetime_7['date_fin'] = $item->date_fin_7;
                $datetime_7['heure_debut'] = $item->heure_debut_7;
                $inserted_id_datetime_7 = $this->mdl_agenda_datetime->insert($datetime_7);
            }
			$datetime_8 = array();
            if ((isset($item->date_debut_8) && $item->date_debut_8 != "" && $item->date_debut_8 != "0000-00-00" && $item->date_debut_8 != null)||(isset($item->date_fin_8) && $item->date_fin_8 != "" && $item->date_fin_8 != "0000-00-00" && $item->date_fin_8 != null)) {
                $datetime_8['agenda_id'] = $item->id;
				if (isset($item->date_debut_8) && $item->date_debut_8 == "0000-00-00") $item->date_debut_8 = null;
                $datetime_8['date_debut'] = $item->date_debut_8;
                if (isset($item->date_fin_8) && $item->date_fin_8 == "0000-00-00") $item->date_fin_8 = null;
                $datetime_8['date_fin'] = $item->date_fin_8;
                $datetime_8['heure_debut'] = $item->heure_debut_8;
                $inserted_id_datetime_8 = $this->mdl_agenda_datetime->insert($datetime_8);
            }
			$datetime_9 = array();
            if ((isset($item->date_debut_9) && $item->date_debut_9 != "" && $item->date_debut_9 != "0000-00-00" && $item->date_debut_9 != null)||(isset($item->date_fin_9) && $item->date_fin_9 != "" && $item->date_fin_9 != "0000-00-00" && $item->date_fin_9 != null)) {
                $datetime_9['agenda_id'] = $item->id;
				if (isset($item->date_debut_9) && $item->date_debut_9 == "0000-00-00") $item->date_debut_9 = null;
                $datetime_9['date_debut'] = $item->date_debut_9;
                if (isset($item->date_fin_9) && $item->date_fin_9 == "0000-00-00") $item->date_fin_9 = null;
                $datetime_9['date_fin'] = $item->date_fin_9;
                $datetime_9['heure_debut'] = $item->heure_debut_9;
                $inserted_id_datetime_9 = $this->mdl_agenda_datetime->insert($datetime_9);
            }
			$datetime_10 = array();
            if ((isset($item->date_debut_10) && $item->date_debut_10 != "" && $item->date_debut_10 != "0000-00-00" && $item->date_debut_10 != null)||(isset($item->date_fin_10) && $item->date_fin_10 != "" && $item->date_fin_10 != "0000-00-00" && $item->date_fin_10 != null)) {
                $datetime_10['agenda_id'] = $item->id;
				if (isset($item->date_debut_10) && $item->date_debut_10 == "0000-00-00") $item->date_debut_10 = null;
                $datetime_10['date_debut'] = $item->date_debut_10;
                if (isset($item->date_fin_10) && $item->date_fin_10 == "0000-00-00") $item->date_fin_10 = null;
                $datetime_10['date_fin'] = $item->date_fin_10;
                $datetime_10['heure_debut'] = $item->heure_debut_10;
                $inserted_id_datetime_10 = $this->mdl_agenda_datetime->insert($datetime_10);
            }
        }
    }*/

    function liste()
    {
        //var_dump($_POST);die();
        $data['infos'] = null;

        /*if ($this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("admin/home");
        }*/

        ////////////////DELETE OLD AGENDA date_fin past 8 days
        $this->mdl_agenda->deleteOldAgenda_fin8jours();


        $nom_url_commercant = $this->uri->rsegment(2);
        //////$this->firephp->log($_POST, 'POST');
        if (isset($nom_url_commercant) && $nom_url_commercant != "" && !is_numeric($nom_url_commercant)) {
            $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);
            $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);
            $data['oInfoCommercant'] = $oInfoCommercant;
            //////$this->firephp->log($oInfoCommercant, 'oInfoCommercant');

            //$data['mdlannonce'] = $this->mdlannonce ;
            $data['mdlbonplan'] = $this->mdlbonplan;

            $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
            $oBonPlan = $this->mdlbonplan->bonPlanParCommercant($_iCommercantId);
            $data['nbBonPlan'] = sizeof($oBonPlan);
            //		$data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_titre : "";		// OP 27/11/2011
            $data['sTitreBonPlan'] = (sizeof($oBonPlan) > 0) ? $oBonPlan->bonplan_texte : "";
            $data['oImagespub'] = $this->mdlimagespub->GetByImagespubActiv();
            $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

            $oLastbonplanCom = $this->mdlbonplan->lastBonplanCom($_iCommercantId);
            if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        }

        $argOffset = (isset($_SESSION["argOffset"])) ? $_SESSION["argOffset"] : 0;
        //$argOffset = $_iPage ;
        if (preg_match("/admin/", $_SERVER["PHP_SELF"])) {
            redirect("admin/home");
        } else {

            //if the link doesn't come from page navigation, clear category session
            /*$current_URI_club = $_SERVER['REQUEST_URI'];
            $current_URI_club_array = explode("accueil/index", $current_URI_club);
            ////$this->firephp->log(count($current_URI_club_array), 'nb_array');
            if (count($current_URI_club_array)==1) {
                $this->session->unset_userdata('iCategorieId_x');
                $this->session->unset_userdata('iVilleId_x');
                $this->session->unset_userdata('zMotCle_x');
                $this->session->unset_userdata('iOrderBy_x');
                $this->session->unset_userdata('inputStringQuandHidden_x');
                $this->session->unset_userdata('inputStringDatedebutHidden_x');
                $this->session->unset_userdata('inputStringDatefinHidden_x');
                $this->session->unset_userdata('inputGeoLongitude_x');
            }*/
            //if the link doesn't come from page navigation, clear catgory session


            if ($this->input->post("inputStringHidden") == "0") unset($_SESSION['iCategorieId']);
            if ($this->input->post("inputStringHidden_sub") == "0") unset($_SESSION['iSousCategorieId']);
            if ($this->input->post("inputStringVilleHidden") == "0") unset($_SESSION['iVilleId']);
            if ($this->input->post("inputStringDepartementHidden") == "") unset($_SESSION['iDepartementId']);
            if ($this->input->post("zMotCle") == "") unset($_SESSION['zMotCle']);
            if ($this->input->post("inputStringOrderByHidden") == "0") unset($_SESSION['iOrderBy']);
            if ($this->input->post("inputStringQuandHidden")) unset($_SESSION['inputStringQuandHidden']);
            if ($this->input->post("inputStringDatedebutHidden")) unset($_SESSION['inputStringDatedebutHidden']);
            if ($this->input->post("inputStringDatefinHidden")) unset($_SESSION['inputStringDatefinHidden']);
            if ($this->input->post("inputIdCommercant")) unset($_SESSION['inputIdCommercant']);


            //$TotalRows = $this->mdlcommercantpagination->Compter();
            $PerPage = 12;
            $data["iFavoris"] = "";


            if (isset($_POST["inputStringHidden"])) {
                unset($_SESSION['iCategorieId']);
                unset($_SESSION['iSousCategorieId']);
                unset($_SESSION['iVilleId']);
                unset($_SESSION['iDepartementId']);
                unset($_SESSION['zMotCle']);
                unset($_SESSION['iOrderBy']);
                unset($_SESSION['inputStringQuandHidden']);
                unset($_SESSION['inputStringDatedebutHidden']);
                unset($_SESSION['inputStringDatefinHidden']);
                unset($_SESSION['inputIdCommercant']);

                //$iCategorieId = $_POST["inputStringHidden"] ;
                $iCategorieId_all0 = $this->input->post("inputStringHidden");
                if (isset($iCategorieId_all0) && $iCategorieId_all0 != "" && $iCategorieId_all0 != NULL && $iCategorieId_all0 != '0') {
                    $iCategorieId_all = substr($iCategorieId_all0, 1);
                    $iCategorieId = explode(',', $iCategorieId_all);
                } else {
                    $iCategorieId = '0';
                }
                //////$this->firephp->log($iCategorieId_all0, 'iCategorieId_all0');
                //////$this->firephp->log($iCategorieId_all, 'iCategorieId_all');
                //////$this->firephp->log($iCategorieId, 'iCategorieId');


                //$iCategorieId = $_POST["inputStringHidden_sub"] ;
                $iSousCategorieId_all0 = $this->input->post("inputStringHidden_sub");
                if (isset($iSousCategorieId_all0) && $iSousCategorieId_all0 != "" && $iSousCategorieId_all0 != NULL && $iSousCategorieId_all0 != '0') {
                    $iSousCategorieId_all = substr($iSousCategorieId_all0, 1);
                    $iSousCategorieId = explode(',', $iSousCategorieId_all);
                } else {
                    $iSousCategorieId = '0';
                }

                if (isset($_POST["inputStringVilleHidden"])) $iVilleId = $_POST["inputStringVilleHidden"]; else $iVilleId = "0";
                if (isset($_POST["inputStringVilleHidden_sub"])) $iOrderBy = $_POST["inputStringVilleHidden_sub"]; else $iOrderBy = "";
                if (isset($_POST["inputStringDepartementHidden"])) $iDepartementId = $_POST["inputStringDepartementHidden"]; else $iDepartementId = 0;
                if (isset($_POST["inputStringOrderByHidden"])) $iOrderBy = $_POST["inputStringOrderByHidden"]; else $iOrderBy = "";
                if (isset($_POST["zMotCle"])) $zMotCle = $_POST["zMotCle"]; else $zMotCle = '';
                if (isset($_POST["inputStringQuandHidden"])) $inputStringQuandHidden = $_POST["inputStringQuandHidden"]; else $inputStringQuandHidden = "0";
                if (isset($_POST["inputStringDatedebutHidden"])) $inputStringDatedebutHidden = $_POST["inputStringDatedebutHidden"]; else $inputStringDatedebutHidden = "0000-00-00";
                if (isset($_POST["inputStringDatefinHidden"])) $inputStringDatefinHidden = $_POST["inputStringDatefinHidden"]; else $inputStringDatefinHidden = "0000-00-00";
                //if (isset($_POST["inputIdCommercant"])) $inputIdCommercant = $_POST["inputIdCommercant"] ; else $inputIdCommercant = "0";
                $rsegment3 = $this->uri->rsegment(3);
                if (isset($rsegment3) && $rsegment3 != "" && !is_numeric($rsegment3)) {
                    $inputIdCommercant = $_iCommercantId;
                } else {
                    if (isset($_POST["inputIdCommercant"])) $inputIdCommercant = $_POST["inputIdCommercant"]; else $inputIdCommercant = "0";
                }

                $_SESSION['iCategorieId'] = $iCategorieId;
                $this->session->set_userdata('iCategorieId_x', $iCategorieId);
                $_SESSION['iSousCategorieId'] = $iSousCategorieId;
                $this->session->set_userdata('iSousCategorieId_x', $iSousCategorieId);
                $_SESSION['iVilleId'] = $iVilleId;
                $this->session->set_userdata('iVilleId_x', $iVilleId);
                $_SESSION['iDepartementId'] = $iDepartementId;
                $this->session->set_userdata('iDepartementId_x', $iDepartementId);
                $_SESSION['zMotCle'] = $zMotCle;
                $this->session->set_userdata('zMotCle_x', $zMotCle);
                $_SESSION['iOrderBy'] = $iOrderBy;
                $this->session->set_userdata('iOrderBy_x', $iOrderBy);
                $_SESSION['inputStringQuandHidden'] = $inputStringQuandHidden;
                $this->session->set_userdata('inputStringQuandHidden_x', $inputStringQuandHidden);
                $_SESSION['inputStringDatedebutHidden'] = $inputStringDatedebutHidden;
                $this->session->set_userdata('inputStringDatedebutHidden_x', $inputStringDatedebutHidden);
                $_SESSION['inputStringDatefinHidden'] = $inputStringDatefinHidden;
                $this->session->set_userdata('inputStringDatefinHidden_x', $inputStringDatefinHidden);
                $_SESSION['inputIdCommercant'] = $inputIdCommercant;
                $this->session->set_userdata('inputIdCommercant_x', $inputIdCommercant);

                $data['iCategorieId'] = $iCategorieId;
                $data['iSousCategorieId'] = $iSousCategorieId;
                $data['iVilleId'] = $iVilleId;
                $data['iDepartementId'] = $iDepartementId;
                $data['zMotCle'] = $zMotCle;
                $data['iOrderBy'] = $iOrderBy;
                $data['inputStringQuandHidden'] = $inputStringQuandHidden;
                $data['inputStringDatedebutHidden'] = $inputStringDatedebutHidden;
                $data['inputStringDatefinHidden'] = $inputStringDatefinHidden;
                $data['IdCommercant'] = $inputIdCommercant;

                $session_iCategorieId = $this->session->userdata('iCategorieId_x');
                $session_iSousCategorieId = $this->session->userdata('iSousCategorieId_x');
                $session_iVilleId = $this->session->userdata('iVilleId_x');
                $session_iDepartementId = $this->session->userdata('iDepartementId_x');
                $session_zMotCle = $this->session->userdata('zMotCle_x');
                $session_iOrderBy = $this->session->userdata('iOrderBy_x');
                $session_inputStringQuandHidden = $this->session->userdata('inputStringQuandHidden_x');
                $session_inputStringDatedebutHidden = $this->session->userdata('inputStringDatedebutHidden_x');
                $session_inputStringDatefinHidden = $this->session->userdata('inputStringDatefinHidden_x');
                $session_inputIdCommercant = $this->session->userdata('inputIdCommercant_x');

                ////$this->firephp->log($inputStringQuandHidden, 'inputStringQuandHidden');
                ////$this->firephp->log($session_inputStringQuandHidden, 'session_inputStringQuandHidden');

                //$toAgenda = $this->mdlrevue->listeArticleRecherche($_SESSION['iCategorieId'], $_SESSION['iVilleId'], $_SESSION['zMotCle'], $data["iFavoris"], $argOffset, $PerPage, $_SESSION['iOrderBy'], $_SESSION['inputStringQuandHidden'], $_SESSION['inputStringDatedebutHidden'], $_SESSION['inputStringDatefinHidden']) ;
                //log_message('error', 'william TotalRows 1 : ');
                $TotalRows = count($this->mdlrevue->listeArticleRecherche($session_iCategorieId, $session_iVilleId, $session_iDepartementId, $session_zMotCle, $session_iSousCategorieId, 0, 10000, $session_iOrderBy, $session_inputStringQuandHidden, $session_inputStringDatedebutHidden, $session_inputStringDatefinHidden, $session_inputIdCommercant,"0",3));

                $config_pagination = array();
                $rsegment3 = $this->uri->rsegment(3);

                if (strpos($rsegment3, '&content_only_list') !== false) {
                    $pieces_rseg = explode("&content_only_list", $rsegment3);
                    $rsegment3 = $pieces_rseg[0];
                }

                if (isset($rsegment3) && $rsegment3 != "" && is_numeric($rsegment3)) {
                    $config_pagination["base_url"] = base_url() . "revuedepresse/liste/" . $rsegment3;
                } else {
                    $config_pagination["base_url"] = base_url() . "revuedepresse/liste/";
                }
                $config_pagination["total_rows"] = $TotalRows;
                $config_pagination["per_page"] = $PerPage;
                $config_pagination["uri_segment"] = 3;
                $config_pagination['first_link'] = 'Première page';
                $config_pagination['last_link'] = 'Dernière page';
                $config_pagination['prev_link'] = 'Précédent';
                $config_pagination['next_link'] = 'Suivant';
                $this->pagination->initialize($config_pagination);
                if (isset($rsegment3) && $rsegment3 != "" && is_numeric($rsegment3)) {
                    $page_pagination = ($rsegment3) ? $rsegment3 : 0;
                } else {
                    $page_pagination = ($rsegment3) ? $rsegment3 : 0;
                }
                //$toCommercant = $this->mdlrevue->listeArticleRecherche($session_iCategorieId, $_SESSION['iVilleId'], $_SESSION['zMotCle'], $data["iFavoris"], $page_pagination, $config_pagination["per_page"], $_SESSION['iOrderBy'], $_SESSION['inputStringQuandHidden'], $_SESSION['inputStringDatedebutHidden'], $_SESSION['inputStringDatefinHidden']) ;
                $toAgenda = $this->mdlrevue->listeArticleRecherche($session_iCategorieId, $session_iVilleId, $session_iDepartementId, $session_zMotCle, $session_iSousCategorieId, $page_pagination, $config_pagination["per_page"], $session_iOrderBy, $session_inputStringQuandHidden, $session_inputStringDatedebutHidden, $session_inputStringDatefinHidden, $session_inputIdCommercant,"0",3);
                //log_message('error', 'william toAgenda 1 : ');
                $data["links_pagination"] = $this->pagination->create_links();

            } else {
                $data["iFavoris"] = "";
                $session_iCategorieId = $this->session->userdata('iCategorieId_x');
                $session_iSousCategorieId = $this->session->userdata('iSousCategorieId_x');
                $session_iVilleId = $this->session->userdata('iVilleId_x');
                $session_iDepartementId = $this->session->userdata('iDepartementId_x');
                $session_zMotCle = $this->session->userdata('zMotCle_x');
                $session_iOrderBy = $this->session->userdata('iOrderBy_x');
                $session_inputStringQuandHidden = $this->session->userdata('inputStringQuandHidden_x');
                $session_inputStringDatedebutHidden = $this->session->userdata('inputStringDatedebutHidden_x');
                $session_inputStringDatefinHidden = $this->session->userdata('inputStringDatefinHidden_x');
                $session_inputIdCommercant = $this->session->userdata('inputIdCommercant_x');

                $iCategorieId = (isset($session_iCategorieId)) ? $session_iCategorieId : 0;
                $iSousCategorieId = (isset($session_iSousCategorieId)) ? $session_iSousCategorieId : 0;
                $iVilleId = (isset($session_iVilleId)) ? $session_iVilleId : "0";
                $iDepartementId = (isset($session_iDepartementId)) ? $session_iDepartementId : 0;
                $zMotCle = (isset($session_zMotCle)) ? $session_zMotCle : "";
                $iOrderBy = (isset($session_iOrderBy)) ? $session_iOrderBy : "0";
                $inputStringQuandHidden = (isset($session_inputStringQuandHidden)) ? $session_inputStringQuandHidden : "0";
                $inputStringDatedebutHidden = (isset($session_inputStringDatedebutHidden)) ? $session_inputStringDatedebutHidden : "0000-00-00";
                $inputStringDatefinHidden = (isset($session_inputStringDatefinHidden)) ? $session_inputStringDatefinHidden : "0000-00-00";
                //$inputIdCommercant = (isset($session_inputIdCommercant)) ? $session_inputIdCommercant : "0" ;
                $rsegment3 = $this->uri->rsegment(3);
                if (isset($rsegment3) && $rsegment3 != "" && !is_numeric($rsegment3)) {
                    $inputIdCommercant = $_iCommercantId;
                } else {
                    $inputIdCommercant = (isset($session_inputIdCommercant)) ? $session_inputIdCommercant : "0";
                }
                //$toAgenda = $this->mdlrevue->listeArticleRecherche($iCategorieId, $iVilleId, $zMotCle, $data["iFavoris"], $argOffset, $PerPage, $iOrderBy, $inputStringQuandHidden, $inputStringDatedebutHidden, $inputStringDatefinHidden) ;
                //log_message('error', 'william TotalRows 2 : ');
                $TotalRows = count($this->mdlrevue->listeArticleRecherche($iCategorieId, $iVilleId, $iDepartementId, $zMotCle, $iSousCategorieId, 0, 10000, $iOrderBy, $inputStringQuandHidden, $inputStringDatedebutHidden, $inputStringDatefinHidden, $inputIdCommercant,"0",3));

                $config_pagination = array();
                $rsegment3 = $this->uri->rsegment(3);

                if (strpos($rsegment3, '&content_only_list') !== false) {
                    $pieces_rseg = explode("&content_only_list", $rsegment3);
                    $rsegment3 = $pieces_rseg[0];
                }
                //die($rsegment3);

                if (isset($rsegment3) && $rsegment3 != "" && is_numeric($rsegment3)) {
                    $config_pagination["base_url"] = base_url() . "revuedepresse/liste/" . $rsegment3;
                } else {
                    $config_pagination["base_url"] = base_url() . "revuedepresse/liste/";
                }
                $config_pagination["total_rows"] = $TotalRows;
                $config_pagination["per_page"] = $PerPage;
                $config_pagination["uri_segment"] = 3;
                $config_pagination['first_link'] = 'Première page';
                $config_pagination['last_link'] = 'Dernière page';
                $config_pagination['prev_link'] = 'Précédent';
                $config_pagination['next_link'] = 'Suivant';
                $this->pagination->initialize($config_pagination);

                if (isset($rsegment3) && $rsegment3 != "" && is_numeric($rsegment3)) {
                    $page_pagination = ($rsegment3) ? $rsegment3 : 0;
                } else {
                    $page_pagination = ($rsegment3) ? $rsegment3 : 0;
                }
                $toAgenda = $this->mdlrevue->listeArticleRecherche($iCategorieId, $iVilleId, $iDepartementId, $zMotCle, $iSousCategorieId, $page_pagination, $config_pagination["per_page"], $iOrderBy, $inputStringQuandHidden, $inputStringDatedebutHidden, $inputStringDatefinHidden, $inputIdCommercant,"0",3);
                //log_message('error', 'william toAgenda 2 : ');
                $data["links_pagination"] = $this->pagination->create_links();
            }

            ////$this->firephp->log($_SERVER['REQUEST_URI'], 'PATH_INFO');

            $iNombreLiens = $TotalRows / $PerPage;
            if ($iNombreLiens > round($iNombreLiens)) {
                $iNombreLiens = round($iNombreLiens) + 1;
            } else {
                $iNombreLiens = round($iNombreLiens);
            }
            //////////////////////////////////

            $data["iNombreLiens"] = $iNombreLiens;

            $data["PerPage"] = $PerPage;
            $data["TotalRows"] = $TotalRows;
            $data["argOffset"] = $argOffset;
            //$toCommercant= $this->mdlcommercantpagination->GetListeCommercantPagination($PerPage, $argOffset);
            $data['toAgenda'] = $toAgenda;//william hack
            // print_r($data['toCommercant']);exit();
            //$this->load->view('front/vwAccueil', $data) ;
            $data['pagecategory'] = 'article';


            $departement_check = $this->session->userdata('iDepartementId_x');

            //get ville list of article***********************************************************************************************************
            if (isset($_iCommercantId) && $_iCommercantId != "0" && $_iCommercantId != null && $_iCommercantId != "") {
                if (isset($departement_check) && $departement_check != "" && $departement_check != null) {
                    $toVille = $this->mdlville->GetArticleVillesByIdCommercant_by_departement_revue($_iCommercantId, $this->session->userdata('iDepartementId_x'));
                    $data['toVille'] = $toVille;
                } else {
                    $toVille = $this->mdlville->GetArticleVillesByIdCommercant_revue($_iCommercantId);
                    $data['toVille'] = $toVille;
                }
                $toCategorie_principale = $this->mdlrevue->GetArticleCategorie_ByIdCommercant($_iCommercantId);
                $data['toCategorie_principale'] = $toCategorie_principale;
            } else {
                if (isset($departement_check) && $departement_check != "" && $departement_check != null) {
                    //$toVille= $this->mdlville->GetArticleVilles_pvc_by_departement($this->session->userdata('iDepartementId_x'));
                    $toVille = $this->mdlville->GetAgendaVilles_pvc_by_varest_article_revue($this->session->userdata('iDepartementId_x'));
                    $data['toVille'] = $toVille;
                } else {
                    $toVille = $this->mdlville->GetAgendaVilles_pvc_by_varest_article_revue();
                    $data['toVille'] = $toVille;
                }
                $toCategorie_principale = $this->mdlrevue->GetArticleCategorie();
                $data['toCategorie_principale'] = $toCategorie_principale;
            }
            //get ville list of article************************************************************************************************************

            $toDepartement = $this->mdldepartement->GetArticleDepartements_pvc_revue();
            $data['toDepartement'] = $toDepartement;

            $data["mdl_localisation"] = $this->mdl_localisation;
            $data["mdlville"] = $this->mdlville;


            $is_mobile = $this->agent->is_mobile();
            //test ipad user agent
            $is_mobile_ipad = $this->agent->is_mobile('ipad');
            $data['is_mobile_ipad'] = $is_mobile_ipad;
            $is_robot = $this->agent->is_robot();
            $is_browser = $this->agent->is_browser();
            $is_platform = $this->agent->platform();
            $data['is_mobile'] = $is_mobile;
            $data['is_robot'] = $is_robot;
            $data['is_browser'] = $is_browser;
            $data['is_platform'] = $is_platform;


            if ($this->ion_auth->logged_in()) {
                $user_ion_auth = $this->ion_auth->user()->row();
                $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                if ($iduser == null || $iduser == 0 || $iduser == "") {
                    $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                }
                $data['IdUser'] = $iduser;
            }

            ////$this->firephp->log($_REQUEST, '_REQUEST');

            if (!isset($session_iCategorieId)) $session_iCategorieId_to_count = 0; else $session_iCategorieId_to_count = $session_iCategorieId;
            if (!isset($session_iVilleId)) $session_iVilleId_to_count = 0; else $session_iVilleId_to_count = $session_iVilleId;
            if (!isset($session_iDepartementId)) $session_iDepartementId_to_count = 0; else $session_iDepartementId_to_count = $session_iDepartementId;
            if (!isset($session_zMotCle)) $session_zMotCle_to_count = ""; else $session_zMotCle_to_count = $session_zMotCle;
            if (!isset($session_iSousCategorieId)) $session_iSousCategorieId_to_count = 0; else $session_iSousCategorieId_to_count = $session_iSousCategorieId;
            if (!isset($session_iOrderBy)) $session_iOrderBy_to_count = ""; else $session_iOrderBy_to_count = $session_iOrderBy;
            $session_inputStringDatedebutHidden_to_count = "0000-00-00";
            $session_inputStringDatefinHidden_to_count = "0000-00-00";
            $session_inputIdCommercant_to_count = "0";

            $data['toArticleTout_global'] = count($this->mdlrevue->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "0", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,"0",3));
            $data['toArticleAujourdhui_global'] = count($this->mdlrevue->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "101", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,"0",3));
            $data['toArticleWeekend_global'] = count($this->mdlrevue->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "202", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,"0",3));
            $data['toArticleSemaine_global'] = count($this->mdlrevue->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "303", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,"0",3));
            $data['toArticleSemproch_global'] = count($this->mdlrevue->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "404", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,"0",3));
            $data['toArticleMois_global'] = count($this->mdlrevue->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "505", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,"0",3));

            $data['toArticleJanvier_global'] = count($this->mdlrevue->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "01", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,"0",3));
            $data['toArticleFevrier_global'] = count($this->mdlrevue->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "02", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,"0",3));
            $data['toArticleMars_global'] = count($this->mdlrevue->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "03", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,"0",3));
            $data['toArticleAvril_global'] = count($this->mdlrevue->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "04", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,"0",3));
            $data['toArticleMai_global'] = count($this->mdlrevue->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "05", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,"0",3));
            $data['toArticleJuin_global'] = count($this->mdlrevue->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "06", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,"0",3));
            $data['toArticleJuillet_global'] = count($this->mdlrevue->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "07", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,"0",3));
            $data['toArticleAout_global'] = count($this->mdlrevue->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "08", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,"0",3));
            $data['toArticleSept_global'] = count($this->mdlrevue->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "09", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,"0",3));
            $data['toArticleOct_global'] = count($this->mdlrevue->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "10", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,"0",3));
            $data['toArticleNov_global'] = count($this->mdlrevue->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "11", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,"0",3));
            $data['toArticleDec_global'] = count($this->mdlrevue->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "12", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count,"0",3));

            $data['mdlbonplan'] = $this->mdlbonplan;
            $data['mdlannonce'] = $this->mdlannonce;
            $data['Commercant'] = $this->Commercant;
            $data['mdl_article_datetime'] = $this->mdl_article_datetime;

            //TO NOT REMOVE - for details agenda commercant
            if (isset($_iCommercantId) && isset($oInfoCommercant)) {

                $data['toCategorie_principale'] = $this->mdlrevue->GetArticleCategorie_ByIdCommercant($_iCommercantId);

                $data['toArticleJanvier'] = $this->mdlrevue->GetArticleNbByMonth("01", $_iCommercantId);
                $data['toArticleFevrier'] = $this->mdlrevue->GetArticleNbByMonth("02", $_iCommercantId);
                $data['toArticleMars'] = $this->mdlrevue->GetArticleNbByMonth("03", $_iCommercantId);
                $data['toArticleAvril'] = $this->mdlrevue->GetArticleNbByMonth("04", $_iCommercantId);
                $data['toArticleMai'] = $this->mdlrevue->GetArticleNbByMonth("05", $_iCommercantId);
                $data['toArticleJuin'] = $this->mdlrevue->GetArticleNbByMonth("06", $_iCommercantId);
                $data['toArticleJuillet'] = $this->mdlrevue->GetArticleNbByMonth("07", $_iCommercantId);
                $data['toArticleAout'] = $this->mdlrevue->GetArticleNbByMonth("08", $_iCommercantId);
                $data['toArticleSept'] = $this->mdlrevue->GetArticleNbByMonth("09", $_iCommercantId);
                $data['toArticleOct'] = $this->mdlrevue->GetArticleNbByMonth("10", $_iCommercantId);
                $data['toArticleNov'] = $this->mdlrevue->GetArticleNbByMonth("11", $_iCommercantId);
                $data['toArticleDec'] = $this->mdlrevue->GetArticleNbByMonth("12", $_iCommercantId);

                $data['nombre_article_com'] = $this->mdlrevue->GetByIdCommercant($_iCommercantId);

                $data['pagecategory_partner'] = "list_agenda";

                //$this->load->view('agenda/partner_agenda_list', $data) ;
                //$this->load->view('privicarte/partner_agenda_list', $data);
                $this->load->view('sortez/partner_article_list', $data);


            } else {

                $this->session->set_userdata('nohome', '1');

                $data["main_menu_content"] = "article";

                if ($_SERVER['SERVER_NAME'] == DOMAIN_SORTEZ_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_SORTEZ_GLOBAL) {
                    if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
                        //$this->load->view('sortez_mobile/liste_article', $data) ;
                        $this->load->view('revuedepresse/article_index', $data);
                    } else {
                        //$this->load->view('sortez/article', $data) ;
                        $this->load->view('revuedepresse/article_index', $data);
                    }
                } elseif ($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL) {
                    $this->load->view("vivresaville/article_index", $data);
                }


            }

        }
    }


    function check_Idcategory_of_subCategory()
    {
        $session_iCategorieId = $this->session->userdata('iSousCategorieId_x');
        if (isset($session_iCategorieId)) {
            $iCategorieId_sess = $session_iCategorieId;
            if (isset($iCategorieId_sess) && is_array($iCategorieId_sess)) {
                $all_subcategory = "";
                for ($i = 0; $i < count($iCategorieId_sess); $i++) {
                    $sousRubrique = $this->mdl_categories_agenda->getByIdSousCateg($iCategorieId_sess[$i]);
                    $all_subcategory .= "-" . $sousRubrique->agenda_categid;
                }
                echo substr($all_subcategory, 1);
                ////$this->firephp->log($all_subcategory, 'all_subcategory');
            } else {
                echo "0";
            }
        } else echo "0";
    }

    function check_Idcategory_of_Category()
    {
        $session_iCategorieId = $this->session->userdata('iSousCategorieId_x');
        if (isset($session_iCategorieId)) {
            $iCategorieId_sess = $session_iCategorieId;
            if (isset($iCategorieId_sess) && is_array($iCategorieId_sess)) {
                $all_subcategory = "";
                for ($i = 0; $i < count($iCategorieId_sess); $i++) {
                    $sousRubrique = $this->mdl_categories_agenda->getByIdSousCateg($iCategorieId_sess[$i]);
                    $all_subcategory .= "-" . $sousRubrique->agenda_categid;
                }
                echo substr($all_subcategory, 1);
                ////$this->firephp->log($all_subcategory, 'all_subcategory');
            } else {
                echo "0";
            }
        } else echo "0";
    }

    //this function is not the truth mon_agenda for particulier, this is a global search agenda

    function mon_agenda()
    {
        $data['infos'] = null;

        /*if ($this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("admin/home");
        }*/

        if (!$this->ion_auth->logged_in()) {
            redirect("auth/login");
        } else {
            $user_ion_auth = $this->ion_auth->user()->row();
            $user_ionauth_id = $user_ion_auth->id;
        }

        $argOffset = (isset($_SESSION["argOffset"])) ? $_SESSION["argOffset"] : 0;
        //$argOffset = $_iPage ;
        if (preg_match("/admin/", $_SERVER["PHP_SELF"])) {
            redirect("admin/home");
        } else {


            //if the link doesn't come from page navigation, clear catgory session

            if ($this->input->post("inputStringHidden") == "0") unset($_SESSION['iCategorieId']);
            if ($this->input->post("inputStringHidden_sub") == "0") unset($_SESSION['iSousCategorieId']);
            if ($this->input->post("inputStringVilleHidden") == "0") unset($_SESSION['iVilleId']);
            if ($this->input->post("zMotCle") == "") unset($_SESSION['zMotCle']);
            if ($this->input->post("inputStringOrderByHidden") == "0") unset($_SESSION['iOrderBy']);
            if ($this->input->post("inputStringQuandHidden")) unset($_SESSION['inputStringQuandHidden']);
            if ($this->input->post("inputStringDatedebutHidden")) unset($_SESSION['inputStringDatedebutHidden']);
            if ($this->input->post("inputStringDatefinHidden")) unset($_SESSION['inputStringDatefinHidden']);
            if ($this->input->post("inputIdCommercant")) unset($_SESSION['inputIdCommercant']);


            //$TotalRows = $this->mdlcommercantpagination->Compter();
            $PerPage = 10;
            $data["iFavoris"] = "";


            $toVille = $this->mdlville->GetAgendaVillesByIdUsers_ionauth($user_ionauth_id);//get ville list of agenda
            $data['toVille'] = $toVille;
            $toCategorie_principale = $this->mdl_agenda->GetAgendaCategorie_ByIdUsers_ionauth($user_ionauth_id);
            $data['toCategorie_principale'] = $toCategorie_principale;


            if (isset($_POST["inputStringHidden"])) {
                unset($_SESSION['iCategorieId']);
                unset($_SESSION['iSousCategorieId']);
                unset($_SESSION['iVilleId']);
                unset($_SESSION['zMotCle']);
                unset($_SESSION['iOrderBy']);
                unset($_SESSION['inputStringQuandHidden']);
                unset($_SESSION['inputStringDatedebutHidden']);
                unset($_SESSION['inputStringDatefinHidden']);
                unset($_SESSION['inputIdCommercant']);

                //$iCategorieId = $_POST["inputStringHidden"] ;
                $iCategorieId_all0 = $this->input->post("inputStringHidden");
                if (isset($iCategorieId_all0) && $iCategorieId_all0 != "" && $iCategorieId_all0 != NULL && $iCategorieId_all0 != '0') {
                    $iCategorieId_all = substr($iCategorieId_all0, 1);
                    $iCategorieId = explode(',', $iCategorieId_all);
                } else {
                    $iCategorieId = '0';
                }
                //////$this->firephp->log($iCategorieId_all0, 'iCategorieId_all0');
                //////$this->firephp->log($iCategorieId_all, 'iCategorieId_all');
                //////$this->firephp->log($iCategorieId, 'iCategorieId');


                //$iCategorieId = $_POST["inputStringHidden_sub"] ;
                $iSousCategorieId_all0 = $this->input->post("inputStringHidden_sub");
                if (isset($iSousCategorieId_all0) && $iSousCategorieId_all0 != "" && $iSousCategorieId_all0 != NULL && $iSousCategorieId_all0 != '0') {
                    $iSousCategorieId_all = substr($iSousCategorieId_all0, 1);
                    $iSousCategorieId = explode(',', $iSousCategorieId_all);
                } else {
                    $iSousCategorieId = '0';
                }

                if (isset($_POST["inputStringVilleHidden"])) $iVilleId = $_POST["inputStringVilleHidden"]; else $iVilleId = "0";
                if (isset($_POST["inputStringVilleHidden_sub"])) $iOrderBy = $_POST["inputStringVilleHidden_sub"]; else $iOrderBy = "";
                if (isset($_POST["inputStringOrderByHidden"])) $iOrderBy = $_POST["inputStringOrderByHidden"]; else $iOrderBy = "";
                if (isset($_POST["zMotCle"])) $zMotCle = $_POST["zMotCle"]; else $zMotCle = '';
                if (isset($_POST["inputStringQuandHidden"])) $inputStringQuandHidden = $_POST["inputStringQuandHidden"]; else $inputStringQuandHidden = "0";
                if (isset($_POST["inputStringDatedebutHidden"])) $inputStringDatedebutHidden = $_POST["inputStringDatedebutHidden"]; else $inputStringDatedebutHidden = "0000-00-00";
                if (isset($_POST["inputStringDatefinHidden"])) $inputStringDatefinHidden = $_POST["inputStringDatefinHidden"]; else $inputStringDatefinHidden = "0000-00-00";
                if (isset($_POST["inputIdCommercant"])) $inputIdCommercant = $_POST["inputIdCommercant"]; else $inputIdCommercant = "0";


                $_SESSION['iCategorieId'] = $iCategorieId;
                $this->session->set_userdata('iCategorieId_x', $iCategorieId);
                $_SESSION['iSousCategorieId'] = $iSousCategorieId;
                $this->session->set_userdata('iSousCategorieId_x', $iSousCategorieId);
                $_SESSION['iVilleId'] = $iVilleId;
                $this->session->set_userdata('iVilleId_x', $iVilleId);
                $_SESSION['zMotCle'] = $zMotCle;
                $this->session->set_userdata('zMotCle_x', $zMotCle);
                $_SESSION['iOrderBy'] = $iOrderBy;
                $this->session->set_userdata('iOrderBy_x', $iOrderBy);
                $_SESSION['inputStringQuandHidden'] = $inputStringQuandHidden;
                $this->session->set_userdata('inputStringQuandHidden_x', $inputStringQuandHidden);
                $_SESSION['inputStringDatedebutHidden'] = $inputStringDatedebutHidden;
                $this->session->set_userdata('inputStringDatedebutHidden_x', $inputStringDatedebutHidden);
                $_SESSION['inputStringDatefinHidden'] = $inputStringDatefinHidden;
                $this->session->set_userdata('inputStringDatefinHidden_x', $inputStringDatefinHidden);
                $_SESSION['inputIdCommercant'] = $inputIdCommercant;
                $this->session->set_userdata('inputIdCommercant_x', $inputIdCommercant);

                $data['iCategorieId'] = $iCategorieId;
                $data['iSousCategorieId'] = $iSousCategorieId;
                $data['iVilleId'] = $iVilleId;
                $data['zMotCle'] = $zMotCle;
                $data['iOrderBy'] = $iOrderBy;
                $data['inputStringQuandHidden'] = $inputStringQuandHidden;
                $data['inputStringDatedebutHidden'] = $inputStringDatedebutHidden;
                $data['inputStringDatefinHidden'] = $inputStringDatefinHidden;
                $data['IdCommercant'] = $inputIdCommercant;

                $session_iCategorieId = $this->session->userdata('iCategorieId_x');
                $session_iSousCategorieId = $this->session->userdata('iSousCategorieId_x');
                $session_iVilleId = $this->session->userdata('iVilleId_x');
                $session_zMotCle = $this->session->userdata('zMotCle_x');
                $session_iOrderBy = $this->session->userdata('iOrderBy_x');
                $session_inputStringQuandHidden = $this->session->userdata('inputStringQuandHidden_x');
                $session_inputStringDatedebutHidden = $this->session->userdata('inputStringDatedebutHidden_x');
                $session_inputStringDatefinHidden = $this->session->userdata('inputStringDatefinHidden_x');
                $session_inputIdCommercant = $this->session->userdata('inputIdCommercant_x');

                ////$this->firephp->log($inputStringQuandHidden, 'inputStringQuandHidden');
                ////$this->firephp->log($session_inputStringQuandHidden, 'session_inputStringQuandHidden');

                //$toAgenda = $this->mdl_agenda->listeAgendaRecherche($_SESSION['iCategorieId'], $_SESSION['iVilleId'], $_SESSION['zMotCle'], $data["iFavoris"], $argOffset, $PerPage, $_SESSION['iOrderBy'], $_SESSION['inputStringQuandHidden'], $_SESSION['inputStringDatedebutHidden'], $_SESSION['inputStringDatefinHidden']) ;
                $TotalRows = count($this->mdl_agenda->listeAgendaRecherche($session_iCategorieId, $session_iVilleId, $session_zMotCle, $session_iSousCategorieId, 0, 10000, $session_iOrderBy, $session_inputStringQuandHidden, $session_inputStringDatedebutHidden, $session_inputStringDatefinHidden, $session_inputIdCommercant, $user_ionauth_id));

                $config_pagination = array();
                $config_pagination["base_url"] = base_url() . "revuedepresse/liste/" . $user_ionauth_id;
                $config_pagination["total_rows"] = $TotalRows;
                $config_pagination["per_page"] = $PerPage;
                $config_pagination["uri_segment"] = 4;
                $config_pagination['first_link'] = '<<<';
                $config_pagination['last_link'] = '>>>';
                $this->pagination->initialize($config_pagination);
                $page_pagination = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                //$toCommercant = $this->mdl_agenda->listeAgendaRecherche($session_iCategorieId, $_SESSION['iVilleId'], $_SESSION['zMotCle'], $data["iFavoris"], $page_pagination, $config_pagination["per_page"], $_SESSION['iOrderBy'], $_SESSION['inputStringQuandHidden'], $_SESSION['inputStringDatedebutHidden'], $_SESSION['inputStringDatefinHidden']) ;
                $toAgenda = $this->mdl_agenda->listeAgendaRecherche($session_iCategorieId, $session_iVilleId, $session_zMotCle, $session_iSousCategorieId, $page_pagination, $config_pagination["per_page"], $session_iOrderBy, $session_inputStringQuandHidden, $session_inputStringDatedebutHidden, $session_inputStringDatefinHidden, $session_inputIdCommercant, $user_ionauth_id);
                $data["links_pagination"] = $this->pagination->create_links();

            } else {
                $data["iFavoris"] = "";
                $session_iCategorieId = $this->session->userdata('iCategorieId_x');
                $session_iSousCategorieId = $this->session->userdata('iSousCategorieId_x');
                $session_iVilleId = $this->session->userdata('iVilleId_x');
                $session_zMotCle = $this->session->userdata('zMotCle_x');
                $session_iOrderBy = $this->session->userdata('iOrderBy_x');
                $session_inputStringQuandHidden = $this->session->userdata('inputStringQuandHidden_x');
                $session_inputStringDatedebutHidden = $this->session->userdata('inputStringDatedebutHidden_x');
                $session_inputStringDatefinHidden = $this->session->userdata('inputStringDatefinHidden_x');
                $session_inputIdCommercant = $this->session->userdata('inputIdCommercant_x');

                $iCategorieId = (isset($session_iCategorieId)) ? $session_iCategorieId : 0;
                $iSousCategorieId = (isset($session_iSousCategorieId)) ? $session_iSousCategorieId : 0;
                $iVilleId = (isset($session_iVilleId)) ? $session_iVilleId : "0";
                $zMotCle = (isset($session_zMotCle)) ? $session_zMotCle : "";
                $iOrderBy = (isset($session_iOrderBy)) ? $session_iOrderBy : "0";
                $inputStringQuandHidden = (isset($session_inputStringQuandHidden)) ? $session_inputStringQuandHidden : "0";
                $inputStringDatedebutHidden = (isset($session_inputStringDatedebutHidden)) ? $session_inputStringDatedebutHidden : "0000-00-00";
                $inputStringDatefinHidden = (isset($session_inputStringDatefinHidden)) ? $session_inputStringDatefinHidden : "0000-00-00";
                $inputIdCommercant = (isset($session_inputIdCommercant)) ? $session_inputIdCommercant : "0";

                //$toAgenda = $this->mdl_agenda->listeAgendaRecherche($iCategorieId, $iVilleId, $zMotCle, $data["iFavoris"], $argOffset, $PerPage, $iOrderBy, $inputStringQuandHidden, $inputStringDatedebutHidden, $inputStringDatefinHidden) ;
                $TotalRows = count($this->mdl_agenda->listeAgendaRecherche($iCategorieId, $iVilleId, $zMotCle, $iSousCategorieId, 0, 10000, $iOrderBy, $inputStringQuandHidden, $inputStringDatedebutHidden, $inputStringDatefinHidden, $inputIdCommercant, $user_ionauth_id));

                $config_pagination = array();
                $config_pagination["base_url"] = base_url() . "revuedepresse/liste/" . $user_ionauth_id;
                $config_pagination["total_rows"] = $TotalRows;
                $config_pagination["per_page"] = $PerPage;
                $config_pagination["uri_segment"] = 4;
                $config_pagination['first_link'] = '<<<';
                $config_pagination['last_link'] = '>>>';
                $this->pagination->initialize($config_pagination);
                $page_pagination = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $toAgenda = $this->mdl_agenda->listeAgendaRecherche($iCategorieId, $iVilleId, $zMotCle, $iSousCategorieId, $page_pagination, $config_pagination["per_page"], $iOrderBy, $inputStringQuandHidden, $inputStringDatedebutHidden, $inputStringDatefinHidden, $inputIdCommercant, $user_ionauth_id);
                $data["links_pagination"] = $this->pagination->create_links();
            }

            ////$this->firephp->log($_SERVER['REQUEST_URI'], 'PATH_INFO');

            $iNombreLiens = $TotalRows / $PerPage;
            if ($iNombreLiens > round($iNombreLiens)) {
                $iNombreLiens = round($iNombreLiens) + 1;
            } else {
                $iNombreLiens = round($iNombreLiens);
            }
            //////////////////////////////////

            $data["iNombreLiens"] = $iNombreLiens;

            $data["PerPage"] = $PerPage;
            $data["TotalRows"] = $TotalRows;
            $data["argOffset"] = $argOffset;
            //$toCommercant= $this->mdlcommercantpagination->GetListeCommercantPagination($PerPage, $argOffset);
            $data['toAgenda'] = $toAgenda;
            // print_r($data['toCommercant']);exit();
            //$this->load->view('front/vwAccueil', $data) ;
            $data['pagecategory'] = 'article';


            $is_mobile = $this->agent->is_mobile();
            //test ipad user agent
            $is_mobile_ipad = $this->agent->is_mobile('ipad');
            $data['is_mobile_ipad'] = $is_mobile_ipad;
            $is_robot = $this->agent->is_robot();
            $is_browser = $this->agent->is_browser();
            $is_platform = $this->agent->platform();
            $data['is_mobile'] = $is_mobile;
            $data['is_robot'] = $is_robot;
            $data['is_browser'] = $is_browser;
            $data['is_platform'] = $is_platform;


            if ($this->ion_auth->logged_in()) {
                $user_ion_auth = $this->ion_auth->user()->row();
                $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                if ($iduser == null || $iduser == 0 || $iduser == "") {
                    $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                }
                $data['IdUser'] = $iduser;
            }


            $this->load->view('agendaAout2013/mon_agenda', $data);

        }

    }


    function preview_article($id_agenda)
    {

        if (!$this->ion_auth->logged_in()) {
            redirect('front/utilisateur/no_permission');
        }

        $data['current_page'] = "details_article";
        $toVille = $this->mdlville->GetArticleVilles_pvc();//get article list of agenda
        $data['toVille'] = $toVille;
        $toCategorie_principale = $this->mdlrevue->GetArticleCategorie();
        $data['toCategorie_principale'] = $toCategorie_principale;
        //var_dump($toCategorie_principale);

        $id_agenda = $this->uri->rsegment(3);

        $oDetailAgenda = $this->mdlrevue->GetById_preview($id_agenda);
        $data['oDetailAgenda'] = $oDetailAgenda;

        //increment "accesscount" on agenda table
        $this->mdlrevue->Increment_accesscount($oDetailAgenda->id);

        //send info commercant to view
        $oInfoCommercant = $this->mdlcommercant->infoCommercant($oDetailAgenda->IdCommercant);
        $data['oInfoCommercant'] = $oInfoCommercant;

        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);
        if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
        if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;
        $data['group_id_commercant_user'] = $group_id_commercant_user;


        //sending mail to event organiser **************************
        if (isset($_POST['text_mail_form_module_detailbonnplan'])) {
            $text_mail_form_module_detailbonnplan = $this->input->post("text_mail_form_module_detailbonnplan");
            $nom_mail_form_module_detailbonnplan = $this->input->post("nom_mail_form_module_detailbonnplan");
            $tel_mail_form_module_detailbonnplan = $this->input->post("tel_mail_form_module_detailbonnplan");
            $email_mail_form_module_detailbonnplan = $this->input->post("email_mail_form_module_detailbonnplan");

            $colDestAdmin = array();
            $colDestAdmin[] = array("Email" => $oDetailAgenda->email, "Name" => $oDetailAgenda->nom_manifestation . " " . $oDetailAgenda->nom_societe);

            // Sujet
            $txtSujetAdmin = "Demande d'information sur un évennement sur Agenda/Club";

            $txtContenuAdmin = "
            <p>Bonjour ,</p>
            <p>Une demande d'information vous est adressé suite à un évennement que vous avez déposé sur le site Agenda.</p>
            <p>Détails :<br/>
            Evennement : " . $oDetailAgenda->nom_manifestation . "
            <br/>
            Date : " . translate_date_to_fr($oDetailAgenda->date_debut) . "<br/>
            Lieu : " . $oDetailAgenda->ville . " " . $oDetailAgenda->adresse_localisation . " " . $oDetailAgenda->codepostal_localisation . "<br/>
            Organisateur : " . $oDetailAgenda->organisateur . "<br/><br/>
            Demande Client :<br/>
            " . $text_mail_form_module_detailbonnplan . "<br/><br/>
            Nom client : " . $nom_mail_form_module_detailbonnplan . "<br/>
            Tel client : " . $tel_mail_form_module_detailbonnplan . "<br/>
            Email client : " . $email_mail_form_module_detailbonnplan . "<br/>
            </p>";

            @envoi_notification($colDestAdmin, $txtSujetAdmin, $txtContenuAdmin);
            $data['mssg_envoi_module_detail_bonplan'] = '<font color="#00CC00">Votre demande est envoyée</font>';
            //$data['mssg_envoi_module_detail_bonplan'] = $txtContenuAdmin;

        } else $data['mssg_envoi_module_detail_bonplan'] = '';
        //sending mail to event organiser *******************************

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $toDepartement = $this->mdldepartement->GetArticleDepartements_pvc();
        $data['toDepartement'] = $toDepartement;

        $data["pagecategory"] = "article";
        $data["main_menu_content"] = "article";
        $data["mdlbonplan"] = $this->mdlbonplan;

        $data["pagecategory_partner"] = "article_partner";


        if (isset($oDetailAgenda->id)) {
            if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
                //$this->load->view('privicarte_mobile/agenda_details_desk', $data);
                $this->load->view('sortez_mobile/article_details_desk', $data);
            } else {
                //$this->load->view('privicarte/details_event', $data);
                $this->load->view('sortez/details_article', $data);
            }
        } else {
            redirect('front/utilisateur/no_permission');
        }

    }

    function details($id_agenda,$link="")
    {
        $data['parent_page']=$link;
        $data['current_page'] = "details_article";
        $toVille = $this->mdlville->GetArticleVilles_pvc();//get article list of agenda
        $data['toVille'] = $toVille;
        $toCategorie_principale = $this->mdlrevue->GetArticleCategorie();
        $data['toCategorie_principale'] = $toCategorie_principale;
        //var_dump($toCategorie_principale);

        $id_agenda = $this->uri->rsegment(3);

        $oDetailAgenda = $this->mdlrevue->GetById_IsActif($id_agenda);
        $data['oDetailAgenda'] = $oDetailAgenda;

        /*send metadata for opengraph fb & twitter*/
        if (!empty($oDetailAgenda)) {
            $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oDetailAgenda->IdCommercant);
            if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
            if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;
            $photoCommercant_path = "application/resources/front/photoCommercant/imagesbank/" . $user_ion_auth_id . "/";
            $photoCommercant_path_old = "application/resources/front/images/revuedepresse/photoCommercant/";

            $image_home_vignette = "";
            if (isset($oDetailAgenda->photo1) && $oDetailAgenda->photo1 != "" && is_file($photoCommercant_path . $oDetailAgenda->photo1) == true) {
                $image_home_vignette = $oDetailAgenda->photo1;
            } else if ($image_home_vignette == "" && isset($oDetailAgenda->photo2) && $oDetailAgenda->photo2 != "" && is_file($photoCommercant_path . $oDetailAgenda->photo2) == true) {
                $image_home_vignette = $oDetailAgenda->photo2;
            } else if ($image_home_vignette == "" && isset($oDetailAgenda->photo3) && $oDetailAgenda->photo3 != "" && is_file($photoCommercant_path . $oDetailAgenda->photo3) == true) {
                $image_home_vignette = $oDetailAgenda->photo3;
            } else if ($image_home_vignette == "" && isset($oDetailAgenda->photo4) && $oDetailAgenda->photo4 != "" && is_file($photoCommercant_path . $oDetailAgenda->photo4) == true) {
                $image_home_vignette = $oDetailAgenda->photo4;
            } else if ($image_home_vignette == "" && isset($oDetailAgenda->photo5) && $oDetailAgenda->photo5 != "" && is_file($photoCommercant_path . $oDetailAgenda->photo5) == true) {
                $image_home_vignette = $oDetailAgenda->photo5;
            } else if ($image_home_vignette == "" && isset($oDetailAgenda->photo1) && $oDetailAgenda->photo1 != "" && is_file($photoCommercant_path_old . $oDetailAgenda->photo1) == true) {
                $image_home_vignette = $oDetailAgenda->photo1;
            } else if ($image_home_vignette == "" && isset($oDetailAgenda->photo2) && $oDetailAgenda->photo2 != "" && is_file($photoCommercant_path_old . $oDetailAgenda->photo2) == true) {
                $image_home_vignette = $oDetailAgenda->photo2;
            } else if ($image_home_vignette == "" && isset($oDetailAgenda->photo3) && $oDetailAgenda->photo3 != "" && is_file($photoCommercant_path_old . $oDetailAgenda->photo3) == true) {
                $image_home_vignette = $oDetailAgenda->photo3;
            } else if ($image_home_vignette == "" && isset($oDetailAgenda->photo4) && $oDetailAgenda->photo4 != "" && is_file($photoCommercant_path_old . $oDetailAgenda->photo4) == true) {
                $image_home_vignette = $oDetailAgenda->photo4;
            } else if ($image_home_vignette == "" && isset($oDetailAgenda->photo5) && $oDetailAgenda->photo5 != "" && is_file($photoCommercant_path_old . $oDetailAgenda->photo5) == true) {
                $image_home_vignette = $oDetailAgenda->photo5;
            }
            $this->load->model("mdl_categories_agenda");
            $toCateg_for_agenda = $this->mdl_categories_agenda->getById($oDetailAgenda->article_categid);
            if ($image_home_vignette == "" && isset($toCateg_for_agenda->images) && $toCateg_for_agenda->images != "" && is_file("application/resources/front/images/agenda/category/" . $toCateg_for_agenda->images) == true) {
                $image_home_vignette = GetImagePath("front/") . '/agenda/category/' . $toCateg_for_agenda->images;
            } else {
                if ($image_home_vignette != "") {
                    if (isset($image_home_vignette) && $image_home_vignette != "" && is_file($photoCommercant_path . $image_home_vignette) == true)
                        $image_home_vignette = base_url() . $photoCommercant_path . $image_home_vignette;
                    else $image_home_vignette = base_url() . $photoCommercant_path_old . $image_home_vignette;
                }
            }
            $data['zMetaImage'] = $image_home_vignette;
            //increment "accesscount" on agenda table
            $this->mdlrevue->Increment_accesscount($oDetailAgenda->id);
            //send info commercant to view
            $oInfoCommercant = $this->mdlcommercant->infoCommercant($oDetailAgenda->IdCommercant);
            $data['oInfoCommercant'] = $oInfoCommercant;
            $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);
            if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
            if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;
            $data['group_id_commercant_user'] = $group_id_commercant_user;


            //sending mail to event organiser **************************
            if (isset($_POST['text_mail_form_module_detailbonnplan'])) {
                $text_mail_form_module_detailbonnplan = $this->input->post("text_mail_form_module_detailbonnplan");
                $nom_mail_form_module_detailbonnplan = $this->input->post("nom_mail_form_module_detailbonnplan");
                $tel_mail_form_module_detailbonnplan = $this->input->post("tel_mail_form_module_detailbonnplan");
                $email_mail_form_module_detailbonnplan = $this->input->post("email_mail_form_module_detailbonnplan");

                $colDestAdmin = array();
                $colDestAdmin[] = array("Email" => $oDetailAgenda->email, "Name" => $oDetailAgenda->nom_manifestation . " " . $oDetailAgenda->nom_societe);

                // Sujet
                $txtSujetAdmin = "Demande d'information sur un évennement sur Agenda/Club";

                $txtContenuAdmin = "
            <p>Bonjour ,</p>
            <p>Une demande d'information vous est adressé suite à un évennement que vous avez déposé sur le site Agenda.</p>
            <p>Détails :<br/>
            Evennement : " . $oDetailAgenda->nom_manifestation . "
            <br/>
            Date : " . translate_date_to_fr($oDetailAgenda->date_debut) . "<br/>
            Lieu : " . $oDetailAgenda->ville . " " . $oDetailAgenda->adresse_localisation . " " . $oDetailAgenda->codepostal_localisation . "<br/>
            Organisateur : " . $oDetailAgenda->organisateur . "<br/><br/>
            Demande Client :<br/>
            " . $text_mail_form_module_detailbonnplan . "<br/><br/>
            Nom client : " . $nom_mail_form_module_detailbonnplan . "<br/>
            Tel client : " . $tel_mail_form_module_detailbonnplan . "<br/>
            Email client : " . $email_mail_form_module_detailbonnplan . "<br/>
            </p>";

                @envoi_notification($colDestAdmin, $txtSujetAdmin, $txtContenuAdmin);
                $data['mssg_envoi_module_detail_bonplan'] = '<font color="#00CC00">Votre demande est envoyée</font>';
                //$data['mssg_envoi_module_detail_bonplan'] = $txtContenuAdmin;

            } else $data['mssg_envoi_module_detail_bonplan'] = '';
            //sending mail to event organiser *******************************

            $is_mobile = $this->agent->is_mobile();
            //test ipad user agent
            $is_mobile_ipad = $this->agent->is_mobile('ipad');
            $data['is_mobile_ipad'] = $is_mobile_ipad;
            $is_robot = $this->agent->is_robot();
            $is_browser = $this->agent->is_browser();
            $is_platform = $this->agent->platform();
            $data['is_mobile'] = $is_mobile;
            $data['is_robot'] = $is_robot;
            $data['is_browser'] = $is_browser;
            $data['is_platform'] = $is_platform;

            $toDepartement = $this->mdldepartement->GetArticleDepartements_pvc();
            $data['toDepartement'] = $toDepartement;

            $data["pagecategory"] = "article";
            $data["main_menu_content"] = "article";
            $data["mdlbonplan"] = $this->mdlbonplan;

            $data["mdl_localisation"] = $this->mdl_localisation;
            $data["mdlville"] = $this->mdlville;
            $data["mdl_article_organiser"] = $this->mdl_article_organiser;

            $data["pagecategory_partner"] = "article_partner";

            $data['toArticle_datetime'] = $this->mdl_article_datetime->getByArticleId($id_agenda);


            if (isset($oDetailAgenda->id)) {
                if ($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL) {
                    $this->load->view('vivresaville/article_details', $data);
                } else {
                    if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
                        //$this->load->view('privicarte_mobile/agenda_details_desk', $data);
                        //$this->load->view('sortez_mobile/article_details_desk', $data);
                        $this->load->view('revuedepresse/article_details', $data);
                    } else {
                        //$this->load->view('privicarte/details_event', $data);
                        //$this->load->view('sortez/details_article', $data);
                        $this->load->view('revuedepresse/article_details', $data);
                    }
                }

            } else {
                redirect('front/utilisateur/no_permission');
            }
        }
    }

    function article_perso_details($id_agenda)
    {

        $data['current_page'] = "details_event";
        $toVille = $this->mdlville->GetArticleVilles_pvc();//get ville list of article
        $data['toVille'] = $toVille;
        $toCategorie_principale = $this->mdlrevue->GetArticleCategorie();
        $data['toCategorie_principale'] = $toCategorie_principale;
        //var_dump($toCategorie_principale);

        $data['zCouleurBgBouton'] = $this->input->get("zCouleurBgBouton");
        $data['zCouleurNbBtn'] = $this->input->get("zCouleurNbBtn");
        $data['zCouleurTextBouton'] = $this->input->get("zCouleurTextBouton");
        $data['zCouleur'] = $this->input->get("zCouleur");
        $data['zCouleurTitre'] = $this->input->get("zCouleurTitre");

        $id_agenda = $this->uri->rsegment(3); //die($id_agenda);
        if (isset($id_agenda)) $id_agenda = (intval($id_agenda));
        else $id_agenda = 0;

        $oDetailAgenda = $this->mdlrevue->GetById_IsActif($id_agenda);
        $data['oDetailAgenda'] = $oDetailAgenda;//var_dump($oDetailAgenda); die();

        if (isset($oDetailAgenda->id)) {

            //increment "accesscount" on agenda table
            $this->mdlrevue->Increment_accesscount($oDetailAgenda->id);

            //send info commercant to view
            $oInfoCommercant = $this->mdlcommercant->infoCommercant($oDetailAgenda->IdCommercant);
            $data['oInfoCommercant'] = $oInfoCommercant;

            $toDepartement = $this->mdldepartement->GetAgendaDepartements_pvc();
            $data['toDepartement'] = $toDepartement;

            $data["pagecategory"] = "agenda";
            $data["main_menu_content"] = "agenda";
            $data["mdlbonplan"] = $this->mdlbonplan;

            $data["mdl_localisation"] = $this->mdl_localisation;
            $data["mdlville"] = $this->mdlville;
            $data["mdl_article_organiser"] = $this->mdl_article_organiser;

            $data["pagecategory_partner"] = "agenda_partner";

            $data['toArticle_datetime'] = $this->mdl_agenda_datetime->getByAgendaId($id_agenda);

            $data['currentpage'] = "export_article";

            //$this->load->view('privicarte/details_event', $data);
            $this->load->view('export/article_perso_details', $data);

        } else {
            redirect('revuedepresse/article_perso');
        }

    }

    function details_event_contact($id_agenda, $contact_display = "contact")
    {

        $data['current_page'] = "details_event";
        $toVille = $this->mdlville->GetAgendaVilles_pvc();//get ville list of agenda
        $data['toVille'] = $toVille;
        $toCategorie_principale = $this->mdl_agenda->GetAgendaCategorie();
        $data['toCategorie_principale'] = $toCategorie_principale;
        //var_dump($toCategorie_principale);

        $id_agenda = $this->uri->rsegment(3);

        $oDetailAgenda = $this->mdl_agenda->GetById_IsActif($id_agenda);
        $data['oDetailAgenda'] = $oDetailAgenda;

        //increment "accesscount" on agenda table
        $this->mdl_agenda->Increment_accesscount($oDetailAgenda->id);

        //send info commercant to view
        $oInfoCommercant = $this->mdlcommercant->infoCommercant($oDetailAgenda->IdCommercant);
        $data['oInfoCommercant'] = $oInfoCommercant;

        $toDepartement = $this->mdldepartement->GetAgendaDepartements_pvc();
        $data['toDepartement'] = $toDepartement;

        $data["pagecategory"] = "article";
        $data["main_menu_content"] = "article";
        $data["mdlbonplan"] = $this->mdlbonplan;
        $data['contact_display'] = $contact_display;

        $this->load->view('privicarte/details_event_contact', $data);

    }


    function avantages_internautes()
    {
        $data['current_page'] = "avantages_internautes";

        $this->load->view('agenda/avantages_internautes', $data);
    }


    function check_category_list()
    {

        $inputStringQuandHidden_partenaires = $this->input->post("iQuand_sess");////$this->firephp->log($inputStringQuandHidden_partenaires, 'iQuand_sess');
        $inputStringDatedebutHidden_partenaires = $this->input->post("iDatedebut_sess");////$this->firephp->log($inputStringDatedebutHidden_partenaires, 'iDatedebut_sess');
        $inputStringDatefinHidden_partenaires = $this->input->post("iDatefin_sess");////$this->firephp->log($inputStringDatefinHidden_partenaires, 'iDatefin_sess');
        $inputStringVilleHidden_partenaires = $this->input->post("iVilleId_sess");////$this->firephp->log($inputStringVilleHidden_partenaires, 'iVilleId_sess');
        $inputStringDepartementHidden_partenaires = $this->input->post("iDepartementId_sess");////$this->firephp->log($inputStringDepartementHidden_partenaires, 'iDepartementId_sess');

        $toCategorie_principale = $this->mdlrevue->GetArticleCategorie_by_params($inputStringQuandHidden_partenaires, $inputStringDatedebutHidden_partenaires, $inputStringDatefinHidden_partenaires, $inputStringDepartementHidden_partenaires, $inputStringVilleHidden_partenaires);

        $result_to_show = '';

        $session_iCategorieId = $this->session->userdata('iCategorieId_x');
        if (isset($session_iCategorieId)) {
            $iCategorieId_sess = $session_iCategorieId;
        }

        $ii_rand = 0;

        if (isset($toCategorie_principale)) {
            foreach ($toCategorie_principale as $oCategorie_principale) {

                if ($_SERVER['SERVER_NAME'] == DOMAIN_WWW_SORTEZ_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_SORTEZ_GLOBAL) {
                    if (isset($oCategorie_principale->nb_article) && $oCategorie_principale->nb_article != 0) {
                        $data['empty'] = null;
                        //$data['oInfoCommercant'] = $oInfoCommercant;
                        $data['oCategorie_principale'] = $oCategorie_principale;
                        $data['ii_rand'] = $ii_rand;
                        if (isset($iCategorieId_sess)) $data['iCategorieId_sess'] = $iCategorieId_sess;
                        $data['inputStringQuandHidden_partenaires'] = $inputStringQuandHidden_partenaires;
                        $data['inputStringDatedebutHidden_partenaires'] = $inputStringDatedebutHidden_partenaires;
                        $data['inputStringDatefinHidden_partenaires'] = $inputStringDatefinHidden_partenaires;
                        $data['inputStringDepartementHidden_partenaires'] = $inputStringDepartementHidden_partenaires;
                        $data['inputStringVilleHidden_partenaires'] = $inputStringVilleHidden_partenaires;
                        $data['mdl_categories_article'] = $this->mdl_categories_article;
                        $data['session'] = $this->session;
                        $result_to_show .= $this->load->view('revuedepresse/article_check_category', $data, TRUE);
                    }
                } elseif ($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL) {
                    if (isset($oCategorie_principale->nb_article) && $oCategorie_principale->nb_article != 0) {
                        $data['empty'] = null;
                        //$data['oInfoCommercant'] = $oInfoCommercant ;
                        $data['oCategorie_principale'] = $oCategorie_principale;
                        $data['ii_rand'] = $ii_rand;
                        $data['iCategorieId_sess'] = $iCategorieId_sess;
                        $data['inputStringQuandHidden_partenaires'] = $inputStringQuandHidden_partenaires;
                        $data['inputStringDatedebutHidden_partenaires'] = $inputStringDatedebutHidden_partenaires;
                        $data['inputStringDatefinHidden_partenaires'] = $inputStringDatefinHidden_partenaires;
                        $data['inputStringDepartementHidden_partenaires'] = $inputStringDepartementHidden_partenaires;
                        $data['inputStringVilleHidden_partenaires'] = $inputStringVilleHidden_partenaires;
                        $data['mdl_categories_article'] = $this->mdl_categories_article;
                        $data['session'] = $this->session;
                        $result_to_show .= $this->load->view('vivresaville/article_check_category', $data, TRUE);
                    }
                }
                $ii_rand++;
            }

        }

        $result_to_show .= '<a href="javascript:void(0);" class="vsv_subcateg_filter col-3 btn btn-default" onclick="btn_re_init_annuaire_list();" style="color:#ffffff; background-color:#DA1181;padding: .7rem 1rem; height: 40px; font-size: 12px;;">Réinitialiser</a>';

        ////$this->firephp->log($result_to_show, 'result_to_show');
        echo $result_to_show;
        //echo mb_convert_encoding($result_to_show, "UTF-8");

    }


    function article_partner_list($_iCommercantId)
    {

        //code standard utilisation page partenaire START **************************************************

        $nom_url_commercant = $this->uri->rsegment(3);

        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);

        $data['oInfoCommercant'] = $oInfoCommercant;


        //a basic account is not permit to show agenda list
        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);
        if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
        if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;
        if ($group_id_commercant_user == 3) {
            redirect('front/utilisateur/no_permission');
        }

        //$data['mdlannonce'] = $this->mdlannonce ;
        $data['mdlbonplan'] = $this->mdlbonplan;

        $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oBonPlan = $this->mdlbonplan->bonPlanParCommercant($_iCommercantId);
        $data['nbBonPlan'] = sizeof($oBonPlan);
        //		$data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_titre : "";		// OP 27/11/2011
        $data['sTitreBonPlan'] = (sizeof($oBonPlan) > 0) ? $oBonPlan->bonplan_texte : "";
        $data['oImagespub'] = $this->mdlimagespub->GetByImagespubActiv();
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

        $oLastbonplanCom = $this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $data['active_link'] = "article";

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user");
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser = 0;

        //code standard utilisation page partenaire END **************************************************

        if ($this->input->post("inputStringHidden") == "0") unset($_SESSION['iCategorieId']);
        if ($this->input->post("inputStringQuandHidden")) unset($_SESSION['inputStringQuandHidden']);

        $PerPage = 15;
        $data['PerPage'] = $PerPage;

        if (isset($_POST["inputStringHidden"])) {

            unset($_SESSION['iCategorieId']);
            unset($_SESSION['inputStringQuandHidden']);

            $iCategorieId = $this->input->post("inputStringHidden");
            if (isset($_POST["inputStringQuandHidden"]))
                $inputStringQuandHidden = $_POST["inputStringQuandHidden"];
            else $inputStringQuandHidden = "0";

            $_SESSION['iCategorieId'] = $iCategorieId;
            $this->session->set_userdata('iCategorieId_x', $iCategorieId);
            $_SESSION['inputStringQuandHidden'] = $inputStringQuandHidden;
            $this->session->set_userdata('inputStringQuandHidden_x', $inputStringQuandHidden);

            $data['iCategorieId'] = $iCategorieId;
            $data['inputStringQuandHidden'] = $inputStringQuandHidden;

            $session_iCategorieId = $this->session->userdata('iCategorieId_x');
            $session_inputStringQuandHidden = $this->session->userdata('inputStringQuandHidden_x');


            $TotalRows = count($this->mdlrevue->GetByIdCommercantLimit($_iCommercantId, 0, 10000000, $session_iCategorieId, $session_inputStringQuandHidden));
            $data["TotalRows"] = $TotalRows;

            $config_pagination = array();
            $config_pagination["base_url"] = base_url() . "/" . $oInfoCommercant->nom_url . "/revuedepresse/";
            $config_pagination["total_rows"] = $TotalRows;
            $config_pagination["per_page"] = $PerPage;
            $config_pagination["uri_segment"] = 4;
            $config_pagination['first_link'] = '<<<';
            $config_pagination['last_link'] = '>>>';
            $this->pagination->initialize($config_pagination);
            $page_pagination = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['toAgenda'] = $this->mdlrevue->GetByIdCommercantLimit($_iCommercantId, $page_pagination, $config_pagination["per_page"], $session_iCategorieId, $session_inputStringQuandHidden);
            $data["links_pagination"] = $this->pagination->create_links();

        } else {

            $session_iCategorieId = $this->session->userdata('iCategorieId_x');
            $session_inputStringQuandHidden = $this->session->userdata('inputStringQuandHidden_x');

            $iCategorieId = (isset($session_iCategorieId)) ? $session_iCategorieId : 0;
            $inputStringQuandHidden = (isset($session_inputStringQuandHidden)) ? $session_inputStringQuandHidden : "0";

            $TotalRows = count($this->mdlrevue->GetByIdCommercantLimit($_iCommercantId, 0, 10000000, $iCategorieId, $inputStringQuandHidden));
            $data["TotalRows"] = $TotalRows;

            $config_pagination = array();
            $config_pagination["base_url"] = base_url() . "/" . $oInfoCommercant->nom_url . "/revuedepresse/";
            $config_pagination["total_rows"] = $TotalRows;
            $config_pagination["per_page"] = $PerPage;
            $config_pagination["uri_segment"] = 4;
            $config_pagination['first_link'] = '<<<';
            $config_pagination['last_link'] = '>>>';
            $this->pagination->initialize($config_pagination);
            $page_pagination = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['toAgenda'] = $this->mdlrevue->GetByIdCommercantLimit($_iCommercantId, $page_pagination, $config_pagination["per_page"], $iCategorieId, $inputStringQuandHidden);
            $data["links_pagination"] = $this->pagination->create_links();

        }


        $data['toCategorie_principale'] = $this->mdlrevue->GetArticleCategorie_ByIdCommercant($_iCommercantId);

        /*$data['toAgndaJanvier'] = $this->mdlrevue->GetArticleNbByMonth("01",$_iCommercantId);
        $data['toAgndaFevrier'] = $this->mdlrevue->GetArticleNbByMonth("02",$_iCommercantId);
        $data['toAgndaMars'] = $this->mdlrevue->GetArticleNbByMonth("03",$_iCommercantId);
        $data['toAgndaAvril'] = $this->mdlrevue->GetArticleNbByMonth("04",$_iCommercantId);
        $data['toAgndaMai'] = $this->mdlrevue->GetArticleNbByMonth("05",$_iCommercantId);
        $data['toAgndaJuin'] = $this->mdlrevue->GetArticleNbByMonth("06",$_iCommercantId);
        $data['toAgndaJuillet'] = $this->mdlrevue->GetArticleNbByMonth("07",$_iCommercantId);
        $data['toAgndaAout'] = $this->mdlrevue->GetArticleNbByMonth("08",$_iCommercantId);
        $data['toAgndaSept'] = $this->mdlrevue->GetArticleNbByMonth("09",$_iCommercantId);
        $data['toAgndaOct'] = $this->mdlrevue->GetArticleNbByMonth("10",$_iCommercantId);
        $data['toAgndaNov'] = $this->mdlrevue->GetArticleNbByMonth("11",$_iCommercantId);
        $data['toAgndaDec'] = $this->mdlrevue->GetArticleNbByMonth("12",$_iCommercantId);*/


        $data['toAgndaJanvier'] = $this->mdlrevue->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "01");
        $data['toAgndaFevrier'] = $this->mdlrevue->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "02");
        $data['toAgndaMars'] = $this->mdlrevue->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "03");
        $data['toAgndaAvril'] = $this->mdlrevue->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "04");
        $data['toAgndaMai'] = $this->mdlrevue->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "05");
        $data['toAgndaJuin'] = $this->mdlrevue->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "06");
        $data['toAgndaJuillet'] = $this->mdlrevue->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "07");
        $data['toAgndaAout'] = $this->mdlrevue->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "08");
        $data['toAgndaSept'] = $this->mdlrevue->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "09");
        $data['toAgndaOct'] = $this->mdlrevue->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "10");
        $data['toAgndaNov'] = $this->mdlrevue->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "11");
        $data['toAgndaDec'] = $this->mdlrevue->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "12");


        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);

        $data['pagecategory_partner'] = "list_article";

        $data['link_partner_current_page'] = 'article';
        $data['pagecategory'] = "pro";

        $data['current_partner_menu'] = "article";

        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId);
        $data['toBonPlan'] = $toBonPlan;


        //ajout bouton rond noire annonce/agenda
        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
        else $data['result_check_commercant_annonce'] = '0';

        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
        else $data['result_check_commercant_agenda'] = '0';


        //$this->load->view('agenda/partner_agenda_list', $data) ;
        //$this->load->view('privicarte/partner_agenda_list', $data) ;
        $this->load->view('sortez/partner_article_list', $data);

    }

    function article_partner_details($_iCommercantId)
    {

        //code standard utilisation page partenaire START **************************************************

        $nom_url_commercant = $this->uri->rsegment(3);

        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);

        $data['oInfoCommercant'] = $oInfoCommercant;

        //$data['mdlannonce'] = $this->mdlannonce ;
        $data['mdlbonplan'] = $this->mdlbonplan;

        $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);
        $oBonPlan = $this->mdlbonplan->bonPlanParCommercant($_iCommercantId);
        $data['nbBonPlan'] = sizeof($oBonPlan);
        //		$data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_titre : "";		// OP 27/11/2011
        $data['sTitreBonPlan'] = (sizeof($oBonPlan) > 0) ? $oBonPlan->bonplan_texte : "";
        $data['oImagespub'] = $this->mdlimagespub->GetByImagespubActiv();
        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

        $oLastbonplanCom = $this->mdlbonplan->lastBonplanCom($_iCommercantId);
        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        $data['active_link'] = "article";

        $is_mobile = $this->agent->is_mobile();
        //test ipad user agent
        $is_mobile_ipad = $this->agent->is_mobile('ipad');
        $data['is_mobile_ipad'] = $is_mobile_ipad;
        $is_robot = $this->agent->is_robot();
        $is_browser = $this->agent->is_browser();
        $is_platform = $this->agent->platform();
        $data['is_mobile'] = $is_mobile;
        $data['is_robot'] = $is_robot;
        $data['is_browser'] = $is_browser;
        $data['is_platform'] = $is_platform;

        $this->load->model("user");
        if ($this->ion_auth->logged_in()) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser = 0;

        //code standard utilisation page partenaire END **************************************************


        $id_agenda = $this->uri->rsegment(4);

        $oDetailAgenda = $this->mdlrevue->GetById_IsActif($id_agenda);
        $data['oDetailAgenda'] = $oDetailAgenda;

        $data['pagecategory_partner'] = "details_article";

        //sending mail to event organiser **************************
        if (isset($_POST['text_mail_form_module_detailbonnplan'])) {
            $text_mail_form_module_detailbonnplan = $this->input->post("text_mail_form_module_detailbonnplan");
            $nom_mail_form_module_detailbonnplan = $this->input->post("nom_mail_form_module_detailbonnplan");
            $tel_mail_form_module_detailbonnplan = $this->input->post("tel_mail_form_module_detailbonnplan");
            $email_mail_form_module_detailbonnplan = $this->input->post("email_mail_form_module_detailbonnplan");

            $colDestAdmin = array();
            $colDestAdmin[] = array("Email" => $oDetailAgenda->email, "Name" => $oDetailAgenda->nom_manifestation . " " . $oDetailAgenda->nom_societe);

            // Sujet
            $txtSujetAdmin = "Demande d'information sur un article sur Sortez";

            $txtContenuAdmin = "
            <p>Bonjour ,</p>
            <p>Une demande d'information vous est adressé suite à un évennement que vous avez déposé sur le site Agenda.</p>
            <p>Détails :<br/>
            Evennement : " . $oDetailAgenda->nom_manifestation . "
            <br/>
            Date : " . translate_date_to_fr($oDetailAgenda->date_debut) . "<br/>
            Lieu : " . $oDetailAgenda->ville . " " . $oDetailAgenda->adresse_localisation . " " . $oDetailAgenda->codepostal_localisation . "<br/>
            Organisateur : " . $oDetailAgenda->organisateur . "<br/><br/>
            Demande Client :<br/>
            " . $text_mail_form_module_detailbonnplan . "<br/><br/>
            Nom client : " . $nom_mail_form_module_detailbonnplan . "<br/>
            Tel client : " . $tel_mail_form_module_detailbonnplan . "<br/>
            Email client : " . $email_mail_form_module_detailbonnplan . "<br/>
            </p>";

            @envoi_notification($colDestAdmin, $txtSujetAdmin, $txtContenuAdmin);
            $data['mssg_envoi_module_detail_bonplan'] = '<font color="#00CC00">Votre demande est envoyée</font>';
            //$data['mssg_envoi_module_detail_bonplan'] = $txtContenuAdmin;

        } else $data['mssg_envoi_module_detail_bonplan'] = '';
        //sending mail to event organiser *******************************

        $this->load->model("mdl_agenda");
        $data['nombre_agenda_com'] = $this->mdlrevue->GetByIdCommercant($_iCommercantId);

        $data['pagecategory'] = 'pro';
        $data['pagecategory_partner'] = 'article_partner';

        $data['link_partner_current_page'] = 'article';

        $data['current_partner_menu'] = "article";

        $data['toArticle_datetime'] = $this->mdl_article_datetime->getByArticleId($id_agenda);


        $data['cacher_slide'] = "1";

        //ajout bouton rond noire annonce/agenda
        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);
        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';
        else $data['result_check_commercant_annonce'] = '0';

        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);
        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';
        else $data['result_check_commercant_agenda'] = '0';


        //$this->load->view('agenda/partner_agenda_details', $data);
        //$this->load->view('privicarte/partner_agenda_details', $data);
        $this->load->view('sortez/partner_article_details', $data);

    }


    function check_commercant()
    {
        $IdCommercant = $this->input->post("IdCommercant");
        //////$this->firephp->log($IdCommercant, 'IdCommercant');

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($IdCommercant);

        $result_to_show = "";
        $result_to_show .= "NomSociete!!!==>" . $oInfoCommercant->NomSociete . "==!!!==";
        $result_to_show .= "Adresse1!!!==>" . $oInfoCommercant->Adresse1 . "==!!!==";
        $result_to_show .= "Adresse2!!!==>" . $oInfoCommercant->Adresse2 . "==!!!==";
        $result_to_show .= "IdVille!!!==>" . $oInfoCommercant->IdVille . "==!!!==";
        $result_to_show .= "CodePostal!!!==>" . $oInfoCommercant->CodePostal . "==!!!==";
        $result_to_show .= "TelFixe!!!==>" . $oInfoCommercant->TelFixe . "==!!!==";
        $result_to_show .= "TelMobile!!!==>" . $oInfoCommercant->TelMobile . "==!!!==";
        $result_to_show .= "fax!!!==>" . $oInfoCommercant->fax . "==!!!==";
        $result_to_show .= "Email!!!==>" . $oInfoCommercant->Email . "==!!!==";
        $result_to_show .= "SiteWeb!!!==>" . $oInfoCommercant->SiteWeb . "==!!!==";
        $result_to_show .= "Facebook!!!==>" . $oInfoCommercant->Facebook . "==!!!==";
        $result_to_show .= "Twitter!!!==>" . $oInfoCommercant->Twitter . "==!!!==";
        $result_to_show .= "google_plus!!!==>" . $oInfoCommercant->google_plus . "==!!!==";

        echo $result_to_show;
    }


    function check_bonplan()
    {
        $IdCommercant = $this->input->post("IdCommercant");
        //////$this->firephp->log($IdCommercant, 'IdCommercant');

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($IdCommercant);

        $oBonplanCommercant = $this->mdlbonplan->bonPlanParCommercant($IdCommercant);

        if (isset($oBonplanCommercant->bonplan_id)) {
            $result_to_show = "";
            $result_to_show .= $oBonplanCommercant->bonplan_titre . "
            ";
            $result_to_show .= "Valable du " . translate_date_to_fr($oBonplanCommercant->bonplan_date_debut) . " au " . translate_date_to_fr($oBonplanCommercant->bonplan_date_fin) . "
            ";
            if ($oBonplanCommercant->bon_plan_utilise_plusieurs == "1") $result_to_show .= "BonPlan Utilisable plusieurs fois
            ";
            else $result_to_show .= "BonPlan Utilisable une seule fois
            ";
            $result_to_show .= strip_tags($oBonplanCommercant->bonplan_texte) . "
            ";

            echo $result_to_show;
        } else echo "error";
    }


    function delete_files_category($prmIdcateg = 0, $prmFiles)
    {
        $oInfocateg = $this->mdl_categories_agenda->getById($prmIdcateg);
        $filetodelete = "";
        if ($prmFiles == "images") {
            $filetodelete = $oInfocateg->images;
            $this->mdl_categories_agenda->effacerimages_categ($prmIdcateg);
        }
        if (file_exists("application/resources/front/images/agenda/category/" . $filetodelete) == true) {
            unlink("application/resources/front/images/agenda/category/" . $filetodelete);
            $img_photo2_split_array = explode('.', $filetodelete);
            $img_photo2_path = "application/resources/front/images/agenda/category/" . $img_photo2_split_array[0] . "_thumb_100_100." . $img_photo2_split_array[1];
            if (file_exists($img_photo2_path) == true) unlink($img_photo2_path);
        }

    }


    function delete_files($prmIdAgenda = 0, $prmFiles)
    {
        $oInfoAgenda = $this->mdl_agenda->GetById($prmIdAgenda);
        $filetodelete = "";
        if ($prmFiles == "doc_affiche") {
            $filetodelete = $oInfoAgenda->doc_affiche;
            $this->mdl_agenda->effacerdoc_affiche($prmIdAgenda);
        }
        if ($prmFiles == "photo1") {
            $filetodelete = $oInfoAgenda->photo1;
            $this->mdl_agenda->effacerphoto1($prmIdAgenda);
        }
        if ($prmFiles == "photo2") {
            $filetodelete = $oInfoAgenda->photo2;
            $this->mdl_agenda->effacerphoto2($prmIdAgenda);
        }
        if ($prmFiles == "photo3") {
            $filetodelete = $oInfoAgenda->photo3;
            $this->mdl_agenda->effacerphoto3($prmIdAgenda);
        }
        if ($prmFiles == "photo4") {
            $filetodelete = $oInfoAgenda->photo4;
            $this->mdl_agenda->effacerphoto4($prmIdAgenda);
        }
        if ($prmFiles == "photo5") {
            $filetodelete = $oInfoAgenda->photo5;
            $this->mdl_agenda->effacerphoto5($prmIdAgenda);
        }
        if ($prmFiles == "pdf") {
            $filetodelete = $oInfoAgenda->pdf;
            $this->mdl_agenda->effacerpdf($prmIdAgenda);
        }
        if ($prmFiles == "autre_doc_1") {
            $filetodelete = $oInfoAgenda->autre_doc_1;
            $this->mdl_agenda->effacerautre_doc_1($prmIdAgenda);
        }
        if ($prmFiles == "autre_doc_2") {
            $filetodelete = $oInfoAgenda->autre_doc_2;
            $this->mdl_agenda->effacerautre_doc_2($prmIdAgenda);
        }
        if (file_exists("application/resources/front/images/agenda/photoCommercant/" . $filetodelete) == true) {
            unlink("application/resources/front/images/agenda/photoCommercant/" . $filetodelete);
            $img_photo2_split_array = explode('.', $filetodelete);
            $img_photo2_path = "application/resources/front/images/agenda/photoCommercant/" . $img_photo2_split_array[0] . "_thumb_100_100." . $img_photo2_split_array[1];
            if (file_exists($img_photo2_path) == true) unlink($img_photo2_path);
        }

    }


    function personnalisation()
    {

        $data["current_page"] = "subscription_pro";//this is to differentiate js to use

        if (!$this->ion_auth->logged_in()) {
            redirect("auth/login");
        } else {


            $choix_dossier_perso_check_session = $this->session->userdata('choix_dossier_perso_check_session');
            if (isset($choix_dossier_perso_check_session)) {
                $data['choix_dossier_perso_check_session'] = $choix_dossier_perso_check_session;
                ////$this->firephp->log($choix_dossier_perso_check_session, 'choix_dossier_perso_check_session');
            }

            $agenda_perso = $this->input->post("agenda_perso");
            $choix_dossier_perso_value = $agenda_perso['dossperso'];
            ////$this->firephp->log($choix_dossier_perso_value, 'choix_dossier_perso_value');

            if (isset($choix_dossier_perso_value) && $choix_dossier_perso_value == "1") {
                $data['choix_dossier_perso_check_session'] = "1";
                $this->session->set_userdata('choix_dossier_perso_check_session', "1");

                $user_ion_auth = $this->ion_auth->user()->row();
                $iduser_verif = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                if ($iduser_verif == null || $iduser_verif == 0 || $iduser_verif == "") {
                    $_iCommercantId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                } else {
                    $_iCommercantId = "0";
                }
            } else {
                $_iCommercantId = "0";
                $data['choix_dossier_perso_check_session'] = "0";
                $this->session->set_userdata('choix_dossier_perso_check_session', "0");
            }

            if (isset($_iCommercantId) && $_iCommercantId != "0" && $_iCommercantId != null && $_iCommercantId != "") {
                $toVille = $this->mdlville->GetArticleVillesByIdCommercant($_iCommercantId);//get ville list of agenda
                //var_dump($toVille);die();
                $data['toVille'] = $toVille;
                $toCategorie_principale = $this->mdlrevue->GetArticleCategorie_ByIdCommercant($_iCommercantId);
                $data['toCategorie_principale'] = $toCategorie_principale;
                $toDeposant = $this->mdlrevue->getAlldeposantArticle($_iCommercantId);
                $data['toDeposant'] = $toDeposant;
            } else {
                $toVille = $this->mdlville->GetArticleVilles();//get ville list of agenda
                $data['toVille'] = $toVille;
                // var_dump($toVille);die();
                $toCategorie_principale = $this->mdlrevue->GetArticleCategorie();
                $data['toCategorie_principale'] = $toCategorie_principale;
                $toDeposant = $this->mdlrevue->getAlldeposantArticle();
                $data['toDeposant'] = $toDeposant;
            }

            $current_user_ion_auth = $this->ion_auth->user()->row();
            $current_iCommercantId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($current_user_ion_auth->id);
            $data['current_iCommercantId'] = $current_iCommercantId;
            $oAgenda_perso = $this->mdlrevue_perso->GetArticle_article_persoByUser($current_iCommercantId, $current_user_ion_auth->id);
            ////$this->firephp->log($current_iCommercantId, 'current_iCommercantId');
            ////$this->firephp->log($current_user_ion_auth->id, 'current_user_ion_auth');
            ////$this->firephp->log($oAgenda_perso, 'oAgenda_perso');
            $data['oAgenda_perso'] = $oAgenda_perso;

            $group_user = $this->ion_auth->get_users_groups($current_user_ion_auth->id)->result();
            $data['group_user'] = $group_user[0]->id;

            $data['pagecategory'] = "admin_commercant";
            $data['currentpage'] = "export_article";
            $this->load->view('privicarte/article_personnalisation', $data);

        }
    }

    function personnalisation_save()
    {
        $data["current_page"] = "subscription_pro";//this is to differentiate js to use

        if (!$this->ion_auth->logged_in()) {
            redirect("auth/login");
        } else {

            /*if ($this->ion_auth->in_group(3)) {
                redirect('front/utilisateur/no_permission');
            }
            else
            {*/
            $objArticle_perso = $this->input->post("agenda_perso");

            $user_ion_auth = $this->ion_auth->user()->row();
            $_iCommercantId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            $objArticle_perso['IdUsers_ionauth'] = $user_ion_auth->id;
            $objArticle_perso['IdCommercant'] = $_iCommercantId;

            //$objArticle_perso['code'] = base64_encode($objArticle_perso['code']);
            /*$objArticle_perso['code'] = str_replace('<','&lt;',$objArticle_perso['code']);
            $objArticle_perso['code'] = str_replace('>','&gt;',$objArticle_perso['code']);*/

            //$this->firephp->log($objArticle_perso, 'objArticle_perso');

            if ($objArticle_perso['id'] == "0") {
                $IdArticle_perso = $this->mdlrevue_perso->insert_article_perso($objArticle_perso);
            } else {
                $IdArticle_perso = $this->mdlrevue_perso->update_article_perso($objArticle_perso);
            }

            redirect('revuedepresse/personnalisation');

            //}

        }

    }

    function article_perso()
    {

        ////$this->firephp->log($_REQUEST, '_REQUEST');

        $zCouleur = $this->input->get("zCouleur");
        if (!isset($zCouleur)) $zCouleur = $this->input->post("zCouleur");
        if (isset($zCouleur) && $zCouleur != "") $data['zCouleur'] = $zCouleur;
        $zCouleurTitre = $this->input->get("zCouleurTitre");
        if (!isset($zCouleurTitre)) $zCouleurTitre = $this->input->post("zCouleurTitre");
        if (isset($zCouleurTitre) && $zCouleurTitre != "") $data['zCouleurTitre'] = $zCouleurTitre;
        $zCouleurTextBouton = $this->input->get("zCouleurTextBouton");
        if (!isset($zCouleurTextBouton)) $zCouleurTextBouton = $this->input->post("zCouleurTextBouton");
        if (isset($zCouleurTextBouton) && $zCouleurTextBouton != "") $data['zCouleurTextBouton'] = $zCouleurTextBouton;
        $zCouleurBgBouton = $this->input->get("zCouleurBgBouton");
        if (!isset($zCouleurBgBouton)) $zCouleurBgBouton = $this->input->post("zCouleurBgBouton");
        if (isset($zCouleurBgBouton) && $zCouleurBgBouton != "") $data['zCouleurBgBouton'] = $zCouleurBgBouton;
        $zCouleurNbBtn = $this->input->get("zCouleurNbBtn");
        if (!isset($zCouleurNbBtn)) $zCouleurNbBtn = $this->input->post("zCouleurNbBtn");
        if (isset($zCouleurNbBtn) && $zCouleurNbBtn != "") $data['zCouleurNbBtn'] = $zCouleurNbBtn;

        $tiDepartement = $this->input->get("tiDepartement");
        if (!isset($tiDepartement)) $tiDepartement = $this->input->post("tiDepartement");
        if (isset($tiDepartement) && $tiDepartement != "") {
            $tiDepartement = substr_replace($tiDepartement, "", -1);
            $tiDepartement_array = explode("_", $tiDepartement);
            $data['tiDepartement_array'] = $tiDepartement;
        } else {
            $tiDepartement_array = "0";
        }
        $tiDeposant = $this->input->get("tiDeposant");
        if (!isset($tiDeposant)) $tiDeposant = $this->input->post("tiDeposant");
        if (isset($tiDeposant) && $tiDeposant != "") {
            $tiDeposant = substr_replace($tiDeposant, "", -1);
            $tiDeposant_array = explode("_", $tiDeposant);
            $data['tiDeposant_array'] = $tiDeposant;
        }
        $tiCategorie = $this->input->get("tiCategorie");
        if (!isset($tiCategorie)) $tiCategorie = $this->input->post("tiCategorie");
        if (isset($tiCategorie) && $tiCategorie != "") {
            $tiCategorie = substr_replace($tiCategorie, "", -1);
            $data["tiCategorie_for_init"] = $tiCategorie;
            $tiCategorie_array = explode("_", $tiCategorie);
            //$data['tiCategorie_array'] = $tiCategorie_array;
        } else {
            $tiCategorie_array = "0";
        }
        $tiDossperso = $this->input->get("tiDossperso");
        if (!isset($tiDossperso)) $tiDossperso = $this->input->post("tiDossperso");
        $data["tiDossperso"] = $tiDossperso;
        if (isset($tiDossperso) && $tiDossperso != "0") {
            $idCommercant_to_search = $tiDossperso;
        } else if (isset($tiDeposant_array)) {
            $idCommercant_to_search = $tiDeposant_array;
        } else {
            $idCommercant_to_search = "0";
        }

        $contentonly = $this->input->get("contentonly");
        if (!isset($contentonly)) $contentonly = $this->input->post("contentonly");
        $data["contentonly"] = $contentonly;

        $proximite_tiDatefilter = $this->input->get("proximite_tiDatefilter");
        if (!isset($proximite_tiDatefilter)) $proximite_tiDatefilter = $this->input->post("proximite_tiDatefilter");
        if (!isset($proximite_tiDatefilter)) $proximite_tiDatefilter = "0";
        $data["proximite_tiDatefilter"] = $proximite_tiDatefilter;

        $keyword_input_export = $this->input->get("keyword_input_export");
        if (!isset($keyword_input_export)) $keyword_input_export = $this->input->post("keyword_input_export");
        $data["keyword_input_export"] = $keyword_input_export;

        //var_dump($data); die();


        $toAgenda = $this->mdlrevue->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", $proximite_tiDatefilter, "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0);
        $data['toAgenda'] = $toAgenda;


        //$data['toAgndaTout_global'] = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso($tiCategorie_array, 0, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 3));
        $data['toAgndaAujourdhui_global'] = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso($tiCategorie_array, 0, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "101", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 3));
        $data['toAgndaWeekend_global'] = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso($tiCategorie_array, 0, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "202", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 3));
        $data['toAgndaSemaine_global'] = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso($tiCategorie_array, 0, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "303", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 3));
        $data['toAgndaSemproch_global'] = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso($tiCategorie_array, 0, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "404", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 3));
        $data['toAgndaMois_global'] = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso($tiCategorie_array, 0, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "505", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 3));
        /*
        $data['toAgndaJanvier_global'] = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso($tiCategorie_array, 0, $tiDepartement_array, 0, "", 0, 0, 10000, "", "01", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 3));
        $data['toAgndaFevrier_global'] = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso($tiCategorie_array, 0, $tiDepartement_array, 0, "", 0, 0, 10000, "", "02", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 3));
        $data['toAgndaMars_global'] = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso($tiCategorie_array, 0, $tiDepartement_array, 0, "", 0, 0, 10000, "", "03", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 3));
        $data['toAgndaAvril_global'] = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso($tiCategorie_array, 0, $tiDepartement_array, 0, "", 0, 0, 10000, "", "04", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 3));
        $data['toAgndaMai_global'] = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso($tiCategorie_array, 0, $tiDepartement_array, 0, "", 0, 0, 10000, "", "05", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 3));
        $data['toAgndaJuin_global'] = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso($tiCategorie_array, 0, $tiDepartement_array, 0, "", 0, 0, 10000, "", "06", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 3));
        $data['toAgndaJuillet_global'] = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso($tiCategorie_array, 0, $tiDepartement_array, 0, "", 0, 0, 10000, "", "07", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 3));
        $data['toAgndaAout_global'] = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso($tiCategorie_array, 0, $tiDepartement_array, 0, "", 0, 0, 10000, "", "08", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 3));
        $data['toAgndaSept_global'] = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso($tiCategorie_array, 0, $tiDepartement_array, 0, "", 0, 0, 10000, "", "09", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 3));
        $data['toAgndaOct_global'] = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso($tiCategorie_array, 0, $tiDepartement_array, 0, "", 0, 0, 10000, "", "10", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 3));
        $data['toAgndaNov_global'] = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso($tiCategorie_array, 0, $tiDepartement_array, 0, "", 0, 0, 10000, "", "11", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 3));
        $data['toAgndaDec_global'] = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso($tiCategorie_array, 0, $tiDepartement_array, 0, "", 0, 0, 10000, "", "12", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 3));
        */

        $tiCategorie_list_array = $this->mdlrevue->listeArticleRecherche_article_perso_liste_categ($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 3);
        $data['tiCategorie_list_array'] = $tiCategorie_list_array;

        $data['defaul_thumb_width'] = $this->config->item('defaul_thumb_width');
        $data['defaul_thumb_height'] = $this->config->item('defaul_thumb_height');
        $data['currentpage'] = "export_article";
        //$this->load->view('agendaAout2013/agenda_perso', $data);
        //$this->load->view('privicarte/agenda_perso', $data);
        $this->load->view('export/article_perso_respo', $data);

    }


    function article_perso_check_filterQuand()
    {
        $proximite_tiDepartement = $this->input->post("proximite_tiDepartement");
        $proximite_tiDepartement = explode("_", $proximite_tiDepartement);
        $proximite_tiDeposant = $this->input->post("proximite_tiDeposant");
        $proximite_tiDeposant = explode("_", $proximite_tiDeposant);
        $proximite_tiCategorie = $this->input->post("proximite_tiCategorie");
        $proximite_tiDossperso = $this->input->post("proximite_tiDossperso");

        if (isset($proximite_tiDossperso) && $proximite_tiDossperso != "0") {
            $proximite_tiDeposant = $proximite_tiDossperso;
        }

        $proximite_tiCategorie_array = array($proximite_tiCategorie);

        $toAgenda = $this->mdlrevue->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0");

        $toAgndaTout_global = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaAujourdhui_global = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "101", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaWeekend_global = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "202", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaSemaine_global = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "303", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaSemproch_global = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "404", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaMois_global = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "505", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));

        $toAgndaJanvier_global = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "01", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaFevrier_global = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "02", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaMars_global = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "03", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaAvril_global = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "04", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaMai_global = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "05", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaJuin_global = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "06", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaJuillet_global = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "07", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaAout_global = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "08", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaSept_global = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "09", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaOct_global = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "10", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaNov_global = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "11", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));
        $toAgndaDec_global = count($this->mdlrevue->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "12", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));

        ?>

        <select id="inputStringQuandHidden_to_check" size="1" name="inputStringQuandHidden_to_check"
                onChange="javascript:change_list_agenda_perso_filtreIdQuand();">
            <option value="0"
                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "0") { ?>selected<?php } ?>>
                Toutes les
                dates <?php if (isset($toAgndaTout_global)) echo "(" . $toAgndaTout_global . ")"; else echo "(0)"; ?></option>
            <option value="101"
                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "101") { ?>selected<?php } ?>>
                Aujourd'hui <?php if (isset($toAgndaAujourdhui_global)) echo "(" . $toAgndaAujourdhui_global . ")"; else echo "(0)"; ?></option>
            <option value="202"
                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "202") { ?>selected<?php } ?>>
                Ce
                Week-end <?php if (isset($toAgndaWeekend_global)) echo "(" . $toAgndaWeekend_global . ")"; else echo "(0)"; ?></option>
            <option value="303"
                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "303") { ?>selected<?php } ?>>
                Cette
                semaine <?php if (isset($toAgndaSemaine_global)) echo "(" . $toAgndaSemaine_global . ")"; else echo "(0)"; ?></option>
            <option value="404"
                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "404") { ?>selected<?php } ?>>
                Semaine
                prochaine <?php if (isset($toAgndaSemproch_global)) echo "(" . $toAgndaSemproch_global . ")"; else echo "(0)"; ?></option>
            <!--<option value="505" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="505") {
            ?>selected<?php // }
            ?>>Ce mois <?php // if (isset($toAgndaMois_global)) echo "(".$toAgndaMois_global.")"; else echo "(0)";
            ?></option>-->
            <option value="01"
                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "01") { ?>selected<?php } ?>>
                Janvier <?php if (isset($toAgndaJanvier_global)) echo "(" . $toAgndaJanvier_global . ")"; else echo "(0)"; ?></option>
            <option value="02"
                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "02") { ?>selected<?php } ?>>
                Février <?php if (isset($toAgndaFevrier_global)) echo "(" . $toAgndaFevrier_global . ")"; else echo "(0)"; ?></option>
            <option value="03"
                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "03") { ?>selected<?php } ?>>
                Mars <?php if (isset($toAgndaMars_global)) echo "(" . $toAgndaMars_global . ")"; else echo "(0)"; ?></option>
            <option value="04"
                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "04") { ?>selected<?php } ?>>
                Avril <?php if (isset($toAgndaAvril_global)) echo "(" . $toAgndaAvril_global . ")"; else echo "(0)"; ?></option>
            <option value="05"
                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "05") { ?>selected<?php } ?>>
                Mai <?php if (isset($toAgndaMai_global)) echo "(" . $toAgndaMai_global . ")"; else echo "(0)"; ?></option>
            <option value="06"
                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "06") { ?>selected<?php } ?>>
                Juin <?php if (isset($toAgndaJuin_global)) echo "(" . $toAgndaJuin_global . ")"; else echo "(0)"; ?></option>
            <option value="07"
                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "07") { ?>selected<?php } ?>>
                Juillet <?php if (isset($toAgndaJuillet_global)) echo "(" . $toAgndaJuillet_global . ")"; else echo "(0)"; ?></option>
            <option value="08"
                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "08") { ?>selected<?php } ?>>
                Août <?php if (isset($toAgndaAout_global)) echo "(" . $toAgndaAout_global . ")"; else echo "(0)"; ?></option>
            <option value="09"
                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "09") { ?>selected<?php } ?>>
                Septembre <?php if (isset($toAgndaSept_global)) echo "(" . $toAgndaSept_global . ")"; else echo "(0)"; ?></option>
            <option value="10"
                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "10") { ?>selected<?php } ?>>
                Octobre <?php if (isset($toAgndaOct_global)) echo "(" . $toAgndaOct_global . ")"; else echo "(0)"; ?></option>
            <option value="11"
                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "11") { ?>selected<?php } ?>>
                Novembre <?php if (isset($toAgndaNov_global)) echo "(" . $toAgndaNov_global . ")"; else echo "(0)"; ?></option>
            <option value="12"
                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "12") { ?>selected<?php } ?>>
                Décembre <?php if (isset($toAgndaDec_global)) echo "(" . $toAgndaDec_global . ")"; else echo "(0)"; ?></option>
        </select>

        <?php

    }


    function article_perso_check()
    {

        $proximite_tiDepartement = $this->input->post("proximite_tiDepartement");
        $proximite_tiDepartement = explode("_", $proximite_tiDepartement);
        $proximite_tiDeposant = $this->input->post("proximite_tiDeposant");
        $proximite_tiDeposant = explode("_", $proximite_tiDeposant);
        $proximite_tiCategorie = $this->input->post("proximite_tiCategorie");
        $proximite_tiDossperso = $this->input->post("proximite_tiDossperso");
        $inputStringQuandHidden_to_check = $this->input->post("inputStringQuandHidden_to_check");
        $defaul_thumb_width = $this->config->item('defaul_thumb_width');
        $defaul_thumb_height = $this->config->item('defaul_thumb_height');

        if (isset($proximite_tiDossperso) && $proximite_tiDossperso != "0") {
            $proximite_tiDeposant = $proximite_tiDossperso;
        }

        $proximite_tiCategorie_array = array($proximite_tiCategorie);

        if (isset($inputStringQuandHidden_to_check) && $inputStringQuandHidden_to_check != "0") {
            $idQuand = $inputStringQuandHidden_to_check;
        } else {
            $idQuand = "0";
        }

        $toAgenda = $this->mdlrevue->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", $idQuand, "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0");

        ?>

        <?php foreach ($toAgenda as $oAgenda) { ?>

        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin:3px">
            <tr>

                <td style="width:90px">
                    <a href="javascript:void(0);"
                       onClick="javascript:proximite_perso_view_details(<?php echo $oAgenda->id; ?>);"
                       style="color:#000000; text-decoration:none;">
                        <?php
                        $image_home_vignette = "";
                        if (isset($oAgenda->photo1) && $oAgenda->photo1 != "" && is_file("application/resources/front/images/agenda/photoCommercant/" . $oAgenda->photo1) == true) {
                            $image_home_vignette = $oAgenda->photo1;
                        } else if ($image_home_vignette == "" && isset($oAgenda->photo2) && $oAgenda->photo2 != "" && is_file("application/resources/front/images/agenda/photoCommercant/" . $oAgenda->photo2) == true) {
                            $image_home_vignette = $oAgenda->photo2;
                        } else if ($image_home_vignette == "" && isset($oAgenda->photo3) && $oAgenda->photo3 != "" && is_file("application/resources/front/images/agenda/photoCommercant/" . $oAgenda->photo3) == true) {
                            $image_home_vignette = $oAgenda->photo3;
                        } else if ($image_home_vignette == "" && isset($oAgenda->photo4) && $oAgenda->photo4 != "" && is_file("application/resources/front/images/agenda/photoCommercant/" . $oAgenda->photo4) == true) {
                            $image_home_vignette = $oAgenda->photo4;
                        } else if ($image_home_vignette == "" && isset($oAgenda->photo5) && $oAgenda->photo5 != "" && is_file("application/resources/front/images/agenda/photoCommercant/" . $oAgenda->photo5) == true) {
                            $image_home_vignette = $oAgenda->photo5;
                        }
                        ////$this->firephp->log($image_home_vignette, 'image_home_vignette');

                        //showing category img if all image of agenda is null
                        $this->load->model("mdl_categories_agenda");
                        $toCateg_for_agenda = $this->mdl_categories_agenda->getById($oAgenda->agenda_categid);
                        if ($image_home_vignette == "" && isset($toCateg_for_agenda->images) && $toCateg_for_agenda->images != "" && is_file("application/resources/front/images/agenda/category/" . $toCateg_for_agenda->images) == true) {
                            echo '<img src="' . GetImagePath("front/") . '/agenda/category/' . $toCateg_for_agenda->images . '" width="90"/>';
                        } else {

                            if ($image_home_vignette != "") {
                                $img_photo_split_array = explode('.', $image_home_vignette);
                                $img_photo_path = "application/resources/front/images/agenda/photoCommercant/" . $img_photo_split_array[0] . "_thumb_" . $defaul_thumb_width . "_" . $defaul_thumb_height . "." . $img_photo_split_array[1];
                                if (is_file($img_photo_path) == false) {
                                    echo image_thumb("application/resources/front/images/agenda/photoCommercant/" . $image_home_vignette, $defaul_thumb_width, $defaul_thumb_height, '', '');
                                } else echo '<img src="' . GetImagePath("front/") . '/agenda/photoCommercant/' . $img_photo_split_array[0] . "_thumb_" . $defaul_thumb_width . "_" . $defaul_thumb_height . "." . $img_photo_split_array[1] . '" width="90"/>';

                            } else {
                                $image_home_vignette_to_show = GetImagePath("front/") . "/wp71b211d2_06.png";
                                echo '<img src="' . $image_home_vignette_to_show . '" width="90"/>';
                            }

                        }
                        ?>
                    </a>

                </td>
                <td width="450" valign="top" style="padding-left:15px; padding-right:0px;">
                    <a href="javascript:void(0);"
                       onClick="javascript:proximite_perso_view_details(<?php echo $oAgenda->id; ?>);"
                       style="color:#000000; text-decoration:none;">
                <span class="titre_agenda_perso">
                <strong>
                    <?php echo $oAgenda->category; ?><br/>
                    <?php echo $oAgenda->nom_manifestation; ?><br/>
                </strong>
                </span>
                        <span style="font-size:12px;">
              <?php echo $oAgenda->ville; ?>, <?php echo $oAgenda->adresse_localisation; ?>
                            , <?php echo $oAgenda->codepostal_localisation; ?><br/>
                            <?php
                            if ($oAgenda->date_debut == $oAgenda->date_fin) echo "Le " . translate_date_to_fr($oAgenda->date_debut);
                            else echo "Du " . translate_date_to_fr($oAgenda->date_debut) . " au " . translate_date_to_fr($oAgenda->date_fin);
                            ?>
              </span>
                    </a>
                </td>

                <td>
                    <a href="javascript:void(0);"
                       onClick="javascript:proximite_perso_view_details(<?php echo $oAgenda->id; ?>);"
                       style="color:#000000; text-decoration:none;">
                        <img src="<?php echo GetImagePath("front/"); ?>/mobile2013/fleche_agenda_mobile.png" width="30"
                             height="62" alt="details"/>
                    </a>
                </td>


            </tr>
        </table>

        <div style="height:3px; width:100%; background-color:#000000"></div>
    <?php } ?>
        <?php

    }


    function set_iframe_session_navigation()
    {
        $this->session->set_userdata('iframe_session_navigation', "1");
        echo "1";
    }

    function set_view_agenda_part_session_navigation()
    {
        $this->session->set_userdata('view_agenda_part_session_navigation', "1");
        echo "1";
    }


    function FacebookProFormAgenda($IdAgenda)
    {
        $data['IdAgenda'] = $IdAgenda;
        $data['oDetailAgenda'] = $this->mdl_agenda->GetById($IdAgenda);
        $this->load->view("privicarte/FacebookProFormAgenda", $data);
    }

    function check_filter_session_value()
    {
        $data = array();
        $data['zMotCle_x'] = $this->session->userdata('zMotCle_x');
        $data['inputStringQuandHidden_x'] = $this->session->userdata('inputStringQuandHidden_x');
        $data['inputStringDatedebutHidden_x'] = $this->session->userdata('inputStringDatedebutHidden_x');
        $data['inputStringDatefinHidden_x'] = $this->session->userdata('inputStringDatefinHidden_x');
        echo json_encode($data);
    }

    public function revue_presse()
    {
        $group_proo_club = array(3, 4, 5);
        if (!$this->ion_auth->logged_in()) {
            redirect("connexion");
        } else if ($this->ion_auth->in_group($group_proo_club)) {
            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            $allville = $this->mdlville->grtall_ville_revue();
            $data['allville'] = $allville;
            $this->load->view('revuedepresse/revue_presse', $data);
        }
    }

    public function valid_url()
    {
        unset($_SESSION['api_key']);
        unset($_SESSION['token']);

        $group_proo_club = array(3, 4, 5);

        if (!$this->ion_auth->logged_in()) {
            redirect("connexion");

        } else if ($this->ion_auth->in_group($group_proo_club)) {


            $api_key = $this->input->post("api_key");
            $token = $this->input->post("token");
            $codepostal = $this->input->post('code');
            $type = $this->input->post('type');
            $allville = $this->mdlville->grtall_ville_revue();
            $aville_brut = $this->mdlville->getall_ville_revue($codepostal);
            $aville = $aville_brut->IdVille;

            $villes = $this->input->post('villes');
            $urls = $this->input->post('url');
            $url_api = $this->input->post('url_api');
            $acturl = $this->input->post('acturl');


            //var_dump($urls);die();

            if (isset($urls) AND $urls != "" AND $urls != null) {
                $url = $this->input->post('url');
                $data["url"] = $urls;
                $data['codepostal'] = $codepostal;
                $data['type'] = $type;
                $data['allville'] = $allville;
                $data['villes_selected'] = $villes;
                $data["token"] = $token;
                $data["api_key"] = $api_key;
                $data["acturl"] = $acturl;
                $data['aville'] = $aville;


            } elseif (!isset($acturl) OR $acturl == "") {
                $url = $url_api;
                $data["api_url"] = $url;
                $data['codepostal'] = $codepostal;
                $data['type'] = $type;
                $data['allville'] = $allville;
                $data['villes_selected'] = $villes;
                $data["token"] = $token;
                $data["api_key"] = $api_key;
                $data['aville'] = $aville;
            }

            if (isset($url) AND $url != "" AND $url != null AND $url == site_url('revuedepresse/get_api')) {
                $data['runned'] = "yes";
                $data["api_key"] = $api_key;
                $this->session->set_userdata('api_key', $api_key);
                $this->session->set_userdata('token', $token);
            }
            if (isset($urls) AND $urls != "") {
                $full_json = $urls;
                $json = file_get_contents($full_json, true);
            } else {
                $json = $this->get_api($api_key, $token);
            }
            if ($json) {
                $alldecoded = json_decode($json);
                if ($alldecoded->agenda != null) {
                    $result = "ok";
                    $data['nbdate'] = count($alldecoded->agenda);

                } else {
                    $result = "no";

                }
                if (isset($url) AND $url != "" AND $url != null) {
                    $data['result_url'] = $result;
                } else {
                    $data['result_api'] = $result;
                }
            } else {
                $data['result_url'] = "no";
            }
//var_dump($result);die();
            $this->load->view('revuedepresse/revue_presse', $data);
        }
    }


    public function get_data()
    {
        $this->datatourisme_imported = 0;
        $this->datatourisme_skipped = 0;
        $this->datatourisme_exists = 0;
        $api_key = $this->session->userdata("api_key");
        $token = $this->session->userdata("token");
        $group_proo_club = array(3, 4, 5);

        if (!$this->ion_auth->logged_in()) {
            redirect("connexion");

        } else if ($this->ion_auth->in_group($group_proo_club)) {

            $user_ion_auth = $this->ion_auth->user()->row();
            $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

            ///////////////// si json generé url= donné generé, sinon generer json ///////////////////
            $isrunned = $this->input->post("isrunned");
            //$isrunned="ok";
            $i = $this->input->post("toLastIdDatatourisme");
            //$i=2;
            if (isset($isrunned) AND $isrunned != "") {

                $json = $this->get_runned_data($api_key, $token);
            } else {
                $url = $this->input->post("url");
                $json = file_get_contents($url, true);
            }


///////////////////////////////////////////////////////////////////////////////////////////////
            //$i = 1;
            //$url="http://localhost/sortez7/assets/json/agenda.json";

//var_dump($url);die();
            ///////////////////////// parse json   //////////////////////////////////


            $alldecode = json_decode($json);

//var_dump($alldecode);die();
            /////////////////////////////////////////////////////////////////////////
            $chainess = $alldecode->agenda[$i]->date_debut;
            $motifss = '/\b\n\b/i';
            $date_long = $alldecode->agenda[$i]->date_debut;
            $chaine_long = $date_long;
            $motif_long = '/h/';

            ///////////////////////////////  formatage date ///////////////////////////////////////

            if (isset($alldecode->agenda[$i]->date_debut)) {
                $typedate = explode(" ", $alldecode->agenda[$i]->date_debut);
                //var_dump($typedate);die();
                if (count($typedate) > 3) {

                    $date_brute = explode(" ", $alldecode->agenda[$i]->date_debut);
                    if ($date_brute[2] == 'janvier' || $date_brute[2] == 'Jan') $date_brute[2] = "01";
                    if ($date_brute[2] == 'févier' || $date_brute[2] == 'Fevier' || $date_brute[2] == 'Feb') $date_brute[2] = "02";
                    if ($date_brute[2] == 'mars' || $date_brute[2] == 'Mar') $date_brute[2] = "03";
                    if ($date_brute[2] == 'avril' || $date_brute[2] == 'Apr') $date_brute[2] = "04";
                    if ($date_brute[2] == 'mai' || $date_brute[2] == 'May') $date_brute[2] = "05";
                    if ($date_brute[2] == 'juin' || $date_brute[2] == 'Jun') $date_brute[2] = "06";
                    if ($date_brute[2] == 'juillet' || $date_brute[2] == 'Jul') $date_brute[2] = "07";
                    if ($date_brute[2] == 'août' || $date_brute[2] == 'Aout' || $date_brute[2] == 'Aug') $date_brute[2] = "08";
                    if ($date_brute[2] == 'septembre' || $date_brute[2] == 'Sep') $date_brute[2] = "09";
                    if ($date_brute[2] == 'octobre' || $date_brute[2] == 'Oct') $date_brute[2] = "10";
                    if ($date_brute[2] == 'novembre' || $date_brute[2] == 'Nov') $date_brute[2] = "11";
                    if ($date_brute[2] == 'décembre' || $date_brute[2] == 'Decembre' || $date_brute[2] == 'Dec') $date_brute[2] = "12";

                    $date_debut = $date_brute[3] . "-" . $date_brute[2] . "-" . $date_brute[1];

                } elseif (preg_match($motifss, $chainess)) {
                    $exploded = explode("\n", $chainess);
                    if ($exploded) {
                        $jour1 = $exploded[0];
                        $mois2 = $exploded[2];
                        $mois = $exploded[1];
                        $mois1 = $mois[0] . $mois[1];
                        $jour2 = $mois[2] . $mois[3];
                        $date_debut = date("Y") . '-' . $mois1 . '-' . $jour1;
                        $date_fin = date("Y") . '-' . $mois2 . '-' . $jour2;
                    }
                } elseif (preg_match($motif_long, $chaine_long)) {
                    $exploded = explode(" ", $date_long);
                    if ($exploded) {
                        if ($exploded[2] == 'janvier' || $exploded[2] == 'Jan') $exploded[2] = "01";
                        if ($exploded[2] == 'févier' || $exploded[2] == 'Fevier' || $exploded[2] == 'Feb') $exploded[2] = "02";
                        if ($exploded[2] == 'mars' || $exploded[2] == 'Mar') $exploded[2] = "03";
                        if ($exploded[2] == 'avril' || $exploded[2] == 'Apr') $exploded[2] = "04";
                        if ($exploded[2] == 'mai' || $exploded[2] == 'May') $exploded[2] = "05";
                        if ($exploded[2] == 'juin' || $exploded[2] == 'Jun') $exploded[2] = "06";
                        if ($exploded[2] == 'juillet' || $exploded[2] == 'Jul') $exploded[2] = "07";
                        if ($exploded[2] == 'août' || $exploded[2] == 'Aout' || $exploded[2] == 'Aug') $exploded[2] = "08";
                        if ($exploded[2] == 'septembre' || $exploded[2] == 'Sep') $exploded[2] = "09";
                        if ($exploded[2] == 'octobre' || $exploded[2] == 'Oct') $exploded[2] = "10";
                        if ($exploded[2] == 'novembre' || $exploded[2] == 'Nov') $exploded[2] = "11";
                        if ($exploded[2] == 'décembre' || $exploded[2] == 'Decembre' || $exploded[2] == 'Dec') $exploded[2] = "12";

                        $date_debut = date('Y') . "-" . $exploded[2] . "-" . $exploded[1];
                        $heure_debut_brute = explode('h', $exploded[3]);
                        $heure_debut = $heure_debut_brute[0] . ':' . $heure_debut_brute[1];

                        if (isset($alldecode->agenda[$i]->date_fin)) {
                            $date_fin_long = $alldecode->agenda[$i]->date_fin;

                            $date_fin_brute = $exploded = explode(" ", $date_fin_long);
                            if ($date_fin_brute[2] == 'janvier' || $date_fin_brute[2] == 'Jan') $date_fin_brute[2] = "01";
                            if ($date_fin_brute[2] == 'févier' || $date_fin_brute[2] == 'Fevier' || $date_fin_brute[2] == 'Feb') $date_fin_brute[2] = "02";
                            if ($date_fin_brute[2] == 'mars' || $date_fin_brute[2] == 'Mar') $date_fin_brute[2] = "03";
                            if ($date_fin_brute[2] == 'avril' || $date_fin_brute[2] == 'Apr') $date_fin_brute[2] = "04";
                            if ($date_fin_brute[2] == 'mai' || $date_fin_brute[2] == 'May') $date_fin_brute[2] = "05";
                            if ($date_fin_brute[2] == 'juin' || $date_fin_brute[2] == 'Jun') $date_fin_brute[2] = "06";
                            if ($date_fin_brute[2] == 'juillet' || $date_fin_brute[2] == 'Jul') $date_fin_brute[2] = "07";
                            if ($date_fin_brute[2] == 'août' || $date_fin_brute[2] == 'Aout' || $date_fin_brute[2] == 'Aug') $date_fin_brute[2] = "08";
                            if ($date_fin_brute[2] == 'septembre' || $date_fin_brute[2] == 'Sep') $date_fin_brute[2] = "09";
                            if ($date_fin_brute[2] == 'octobre' || $date_fin_brute[2] == 'Oct') $date_fin_brute[2] = "10";
                            if ($date_fin_brute[2] == 'novembre' || $date_fin_brute[2] == 'Nov') $date_fin_brute[2] = "11";
                            if ($date_fin_brute[2] == 'décembre' || $date_fin_brute[2] == 'Decembre' || $date_fin_brute[2] == 'Dec') $date_fin_brute[2] = "12";

                            $date_fin = date('Y') . "-" . $date_fin_brute[2] . "-" . $date_fin_brute[1];
                        }
                    }
                } elseif (isset($alldecode->agenda[$i]->date_debut) And $alldecode->agenda[$i]->date_debut != null) {

                    $date_brute = explode(" ", $alldecode->agenda[$i]->date_debut);
                    if ($date_brute[1] == 'Janvier' || $date_brute[1] == 'Jan') $date_brute[1] = "01";
                    if ($date_brute[1] == 'Févier' || $date_brute[1] == 'Fevier' || $date_brute[1] == 'Feb') $date_brute[1] = "02";
                    if ($date_brute[1] == 'Mars' || $date_brute[1] == 'Mar') $date_brute[1] = "03";
                    if ($date_brute[1] == 'Avril' || $date_brute[1] == 'Apr') $date_brute[1] = "04";
                    if ($date_brute[1] == 'Mai' || $date_brute[1] == 'May') $date_brute[1] = "05";
                    if ($date_brute[1] == 'Juin' || $date_brute[1] == 'Jun') $date_brute[1] = "06";
                    if ($date_brute[1] == 'Juillet' || $date_brute[1] == 'Jul') $date_brute[1] = "07";
                    if ($date_brute[1] == 'Août' || $date_brute[1] == 'Aout' || $date_brute[1] == 'Aug') $date_brute[1] = "08";
                    if ($date_brute[1] == 'Septembre' || $date_brute[1] == 'Sep') $date_brute[1] = "09";
                    if ($date_brute[1] == 'Octobre' || $date_brute[1] == 'Oct') $date_brute[1] = "10";
                    if ($date_brute[1] == 'Novembre' || $date_brute[1] == 'Nov') $date_brute[1] = "11";
                    if ($date_brute[1] == 'Décembre' || $date_brute[1] == 'Decembre' || $date_brute[1] == 'Dec') $date_brute[1] = "12";
                    $date_debut = date('Y') . "-" . $date_brute[1] . "-" . $date_brute[0];
                    if (isset($alldecode->agenda[$i]->date_fin) And $alldecode->agenda[$i]->date_fin != null) {

                        $date_brute = explode(" ", $alldecode->agenda[$i]->date_fin);
                        if ($date_brute[1] == 'Janvier' || $date_brute[1] == 'Jan') $date_brute[1] = "01";
                        if ($date_brute[1] == 'Févier' || $date_brute[1] == 'Fevier' || $date_brute[1] == 'Feb') $date_brute[1] = "02";
                        if ($date_brute[1] == 'Mars' || $date_brute[1] == 'Mar') $date_brute[1] = "03";
                        if ($date_brute[1] == 'Avril' || $date_brute[1] == 'Apr') $date_brute[1] = "04";
                        if ($date_brute[1] == 'Mai' || $date_brute[1] == 'May') $date_brute[1] = "05";
                        if ($date_brute[1] == 'Juin' || $date_brute[1] == 'Jun') $date_brute[1] = "06";
                        if ($date_brute[1] == 'Juillet' || $date_brute[1] == 'Jul') $date_brute[1] = "07";
                        if ($date_brute[1] == 'Août' || $date_brute[1] == 'Aout' || $date_brute[1] == 'Aug') $date_brute[1] = "08";
                        if ($date_brute[1] == 'Septembre' || $date_brute[1] == 'Sep') $date_brute[1] = "09";
                        if ($date_brute[1] == 'Octobre' || $date_brute[1] == 'Oct') $date_brute[1] = "10";
                        if ($date_brute[1] == 'Novembre' || $date_brute[1] == 'Nov') $date_brute[1] = "11";
                        if ($date_brute[1] == 'Décembre' || $date_brute[1] == 'Decembre' || $date_brute[1] == 'Dec') $date_brute[1] = "12";
                        $date_fin = date('Y') . "-" . $date_brute[1] . "-" . $date_brute[0];


                    }


                }


            }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            if (isset($alldecode->agenda[$i]->url)) {
                $siteweb = $alldecode->agenda[$i]->url;
            } else {
                $siteweb = "";
            }

            /////////////////////////////////////si pas de date_fin  alors date_debut=date_fin//////////////////////////////////
            if (isset($alldecode->agenda[$i]) AND $alldecode->agenda[$i] != null) {
                if (isset($date_fin)) {

                } else {

                    $date_fin = $date_debut;
                }

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                //var_dump($date_debut);die();
                ////////////////////////////////////sauvegarde d'image dans application/resources/front/photocommercant//////////////

                $final = $alldecode->agenda[$i]->image;

                if (isset($final) && $final != null) {
                    $info = pathinfo($final);
                    $contents = file_get_contents($final);
                    $_zExtension = strrchr($info['basename'], '.');
                    $_zFilename = random_string('unique', 10) . $_zExtension;
                    $file = 'application/resources/front/photoCommercant/imagesbank/' . $user_ion_auth->id . '/' . $_zFilename;
                    // var_dump($file);die();
                    @file_put_contents($file, $contents);
                    //$uploaded_file = new UploadedFile($file, $_zFilename);
                    if (isset($_zFilename)) $photo = $_zFilename;// save image name on agenda table
                    if ($file != "") {
                        if (is_file($file)) {
                            $base_path_system = str_replace('system/', '', BASEPATH);
                            $image_path_resize_home = $base_path_system . "/" . $file;
                            $image_path_resize_home_final = $base_path_system . "/" . $file;
                            $this_imgmoo =& get_instance();
                            $this_imgmoo->load->library('image_moo');
                            $this_imgmoo->image_moo
                                ->load($image_path_resize_home)
                                ->resize_crop(640, 480, false)
                                ->save($image_path_resize_home_final, true);
                        }
                    }
                } else {
                    $photo = null;
                }

                if (!isset($photo) OR $photo == "") {
                    $photo = null;
                }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//var_dump($photo);die();
                /////////////////////////////// test si categorie existe . sinon eregistrer categorie
                $categ = $alldecode->agenda[$i]->categorie;
                $allcateg = $this->mdlcategorie->getcateg();

                foreach ($allcateg as $category) {

                    $chaine = $category->category;
                    $motif = '/\b' . $categ . '\b/i';
                    if (preg_match($motif, $chaine)) {

                        $agenda_categ = $category->agenda_categid;
                        $matchs = "ok";

                    }
                }
            } else {
                $matchs = false;
            }
            if ($matchs != "ok") {
                $save_categ = $this->mdlcategorie->save_categ($categ);
                $agenda_categ = $save_categ;
            }

            //////////////////////////////////////////////////////////////////////////////////////////////////////

//var_dump($categ);die();

            /////////////////////////////////si description existe assiger sinon laisser vide//////////////////////////

            if (isset($alldecode->agenda[$i]->description)) {
                $description = $alldecode->agenda[$i]->description;
            } elseif (isset($alldecode->agenda[$i]->url)) {
                $description = $alldecode->agenda[$i]->url;
            } else {
                $description = "";
            }
            ///////////////////////////////   VERIFICATION TYPE : REVUE,ARTICLE ,AGENDA.../////////////////////////
            $type = $this->input->post("type");

//var_dump($type);die();
            ///////////////////////////////////////////////////////////////////////////////////////////////////////

            ///////////////////////////// SI ARTICLE CATEG=ARTICLE_CATEGID   SINON AGENDA_CATEGID

            /////////////////////////////////////////////////////////////////////////////////////
//var_dump($category_name);die();

            //////////////////// ajouter valeur s'il y a agenda_article_type sinon laisser null/////////////////

            if (isset($type) AND $type == "article" OR $type == "Article") {
                $article_agenda_type = "1";
            } elseif (isset($type) AND $type == "agenda" OR $type == "Agenda") {
                $article_agenda_type = "2";
            } elseif (isset($type) AND $type == "revue" OR $type == "Revue") {
                $article_agenda_type = "3";
            } else {
                $this->datatourisme_skipped;
            }
//var_dump($article_agenda_type);die();
///////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (!isset($heure_debut)) {

                $heure_debut = "";

            }


            $field_article = array('nom_manifestation' => $alldecode->agenda[$i]->titre,

                'date_debut' => $date_debut,
                'date_fin' => $date_fin,
                'photo1' => $_zFilename,
                'IdCommercant' => $iduser,
                "IdUsers_ionauth" => $user_ion_auth->id,
                "IsActif" => "1",
                "codepostal" => $this->input->post('codepostal'),
                "IdVille" => $this->input->post('ville'),
                "article_categid" => $agenda_categ,
                "siteweb" => $siteweb,
                "description" => $description,
                "agenda_article_type_id" => $article_agenda_type,
                "date_depot" => date("y-m-d"),
                "IdVille_localisation" => $this->input->post('ville'),
            );
            $field_agenda = array('nom_manifestation' => $alldecode->agenda[$i]->titre,

                'date_debut' => $date_debut,
                'date_fin' => $date_fin,
                'photo1' => $_zFilename,
                'IdCommercant' => $iduser,
                "IdUsers_ionauth" => $user_ion_auth->id,
                "IsActif" => "1",
                "codepostal" => $this->input->post('codepostal'),
                "IdVille" => $this->input->post('ville'),
                "agenda_categid" => $agenda_categ,
                "siteweb" => $siteweb,
                "description" => $description,
                "agenda_article_type_id" => $article_agenda_type,
                "date_depot" => date("y-m-d"),
                "IdVille_localisation" => $this->input->post('ville'),

            );

        }
//var_dump($field);die();
        /////////////////////////////   ANTI-DOUBLONS   /////////////////////////////////
        if ($type == "article" OR $type == "Article") {
            $testexist = $this->mdlrevue->test_revue_exist($alldecode->agenda[$i]->titre);
        } elseif ($type == "agenda" OR $type == "Agenda") {
            $testexist = $this->mdl_agenda->test_revue_exist($alldecode->agenda[$i]->titre);
        } else {
            $testexist = $this->mdlrevue->test_revue_exist($alldecode->agenda[$i]->titre);
        }

//var_dump($testexist);die();
        ////////////////////////////////////////////////////////////////////////////////////


//ar_dump($type);die();
        ///////////////////////////// SI EXISTE IGNORER SINON SAUVEGARGER
        if ($testexist == "0") {

            if (isset($type) AND $type == "article" OR $type == "Article") {
                if (isset($alldecode->agenda[$i])) {
                    $objarticle = $this->mdlrevue->save_revue_data($field_article);
                }

                if ($objarticle != false) {
                    $field_datetime = array("article_id" => $objarticle,
                        "date_debut" => $date_debut,
                        "date_fin" => $date_fin,
                        "heure_debut" => $heure_debut
                    );
                    $dateime = $this->mdlrevue->save_datetime_revue($field_datetime);
                    if ($dateime == "ok") {
                        $this->datatourisme_imported++;
                    } elseif ($dateime == "no") {
                        $this->datatourisme_skipped++;
                    }

                }
            } elseif (isset($type) AND $type == "agenda" OR $type == 'Agenda') {

                if (isset($alldecode->agenda[$i])) {
                    $objarticle = $this->mdl_agenda->save_revue_data($field_agenda);
                }

                if ($objarticle != false) {

                    $field_datetime = array("agenda_id" => $objarticle,
                        "date_debut" => $date_debut,
                        "date_fin" => $date_fin,
                        "heure_debut" => $heure_debut

                    );
                    $dateime = $this->mdl_agenda->save_datetime_revue($field_datetime);
                    if ($dateime == "ok") {
                        $this->datatourisme_imported++;
                    } elseif ($dateime == "no") {
                        $this->datatourisme_skipped++;
                    }

                }


            }

        } elseif ($testexist == "1") {
            $this->datatourisme_exists++;
        } else {
            $this->datatourisme_skipped++;
        }
//var_dump($objarticle);die();
        if (!isset($this->datatourisme_imported)) {
            $this->datatourisme_imported = 0;
        }
        $objResult['datatourisme_imported'] = $this->datatourisme_imported;
        $objResult['datatourisme_exists'] = $this->datatourisme_exists;
        $objResult['datatourisme_skipped'] = $this->datatourisme_skipped;

        /////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////add organiser////////////////////////////////////////////////////////

        $http_code = 200;
        header('Content-type: json');
        header('HTTP/1.1: ' . $http_code);
        header('Status: ' . $http_code);
        exit(json_encode((array)$objResult));

    }


    public function get_api($api_key, $token)
    {

        $params = array(
            "api_key" => $api_key,
            "start_template" => "main_template",
            "send_email" => "1"
        );

        $options = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-Type: application/x-www-form-urlencoded; charset=utf-8',
                'content' => http_build_query($params)
            )
        );

        $context = stream_context_create($options);
        $resultat = file_get_contents('https://www.parsehub.com/api/v2/projects/' . $token . '/run', false, $context);

        if ($resultat) {

            $seconds = 400;
            sleep($seconds);
            $params = http_build_query(array(
                "api_key" => $api_key,
                "format" => "json"
            ));

            $result = file_get_contents(
                'https://www.parsehub.com/api/v2/projects/' . $token . '/last_ready_run/data?' . $params,
                false,
                stream_context_create(array(
                    'http' => array(
                        'method' => 'GET'
                    )
                ))
            );
            $is = gzdecode($result);

            return ($is);
        }

    }

    public
    function get_runned_data($api_key, $token)
    {


        $params = http_build_query(array(
            "api_key" => $api_key,
            "format" => "json"
        ));

        $result = file_get_contents(
            'https://www.parsehub.com/api/v2/projects/' . $token . '/last_ready_run/data?' . $params,
            false,
            stream_context_create(array(
                'http' => array(
                    'method' => 'GET'
                )
            ))
        );
        $is = gzdecode($result);

        return ($is);
    }

    public
    function get_count_article()
    {
        $TotalRows = count($this->mdlrevue->listeArticleRecherche(0, 0, 0, "", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", "0", "0"));
        echo 'actualité (' . $TotalRows . ')';
    }

    function remove_all_datatourisme_data()
    {
        $this->load->model('mdl_datatourisme_agenda');
        $this->load->model('mdl_agenda');
        $this->load->model('mdl_agenda_datetime');
        $toresult = $this->mdl_datatourisme_agenda->getAll();
        foreach ($toresult as $item) {
            echo $item->agenda_id . " -> " . $item->datatourisme_id . "<br/>";
            $toresult_updated = $this->mdl_agenda->delete_definitif($item->agenda_id);
            if ($toresult_updated) $this->mdl_datatourisme_agenda->delet_by_agenda($item->agenda_id);
            if ($toresult_updated) $this->mdl_agenda_datetime->deleteByAgendaId($item->agenda_id);
        }
    }

}