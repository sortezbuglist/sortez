<?php







class Annuaire extends CI_Controller



{







    function __construct()



    {



        parent::__construct();



        $this->load->model("mdlannonce");



        $this->load->model("mdlcommercant");



        $this->load->model("mdlbonplan");



        $this->load->model("mdlcategorie");



        $this->load->model("mdlville");



        $this->load->model("mdlcommercantpagination");



        $this->load->model("sousRubrique");



        $this->load->model("Rubrique");



        $this->load->model("AssCommercantAbonnement");



        $this->load->model("AssCommercantSousRubrique");



        $this->load->model("Commercant");



        $this->load->model("user");



        $this->load->model("mdl_agenda");



        $this->load->model("notification");



        $this->load->library('user_agent');



        $this->load->library("pagination");



        $this->load->library('image_moo');



        $this->load->library('session');



        $this->load->library('ion_auth');



        $this->load->model("ion_auth_used_by_club");



        delete_revue_fin_2journ();



        check_vivresaville_id_ville();







    }











    function index()
    {
        ////////////////DELETE OLD AGENDA date_fin past 8 days
        $this->mdl_agenda->deleteOldAgenda_fin8jours();

        ////START VERIFY SUBSCRIPTION
        //@verify_all_pro_subscription();

        $argOffset = (isset($_SESSION["argOffset"])) ? $_SESSION["argOffset"] : 0;
        //$argOffset = $_iPage ;

        if (preg_match("/admin/", $_SERVER["PHP_SELF"])) {
            redirect("admin/home");
        } else {
            //if the link doesn't come from page navigation, clear catgory session

            $PerPage = 20;

            $data["iFavoris"] = "";
            $toCategorie = $this->mdlcategorie->GetCommercantSouscategorie();
            $data['toCategorie'] = $toCategorie;
            $toCategorie_principale = $this->mdlcategorie->GetCommercantCategorie();
            $data['toCategorie_principale'] = $toCategorie_principale;
            $toCommunes = $this->mdlville->GetCommercantCommunes();//get Commune list of commercant
            $data['toCommunes'] = $toCommunes;


            $iCategorieId_all0 = $this->input->post("inputStringHidden");
            if (isset($iCategorieId_all0) && $iCategorieId_all0 != "" && $iCategorieId_all0 != NULL && $iCategorieId_all0 != '0') {
                $char_to_find_categ = substr($iCategorieId_all0, 0, 1);
                if ($char_to_find_categ == ",") $iCategorieId_all = substr($iCategorieId_all0, 1);
                if (isset($iCategorieId_all) && $iCategorieId_all != "" && strpos($iCategorieId_all, ',') !== false)
                    $iCategorieId = explode(',', $iCategorieId_all);
                else $iCategorieId = $iCategorieId_all0;
            } else {
                $iCategorieId = '0';
            }
            $iSubCategorieId = $this->input->post("inputStringHiddenSubCateg");
            if (!isset($iSubCategorieId)) $iSubCategorieId = 0;
            $iVilleId = $this->input->post("inputStringVilleHidden");
            if (!isset($iVilleId)) $iVilleId = 0;
            $iDepartementId = $this->input->post("inputStringDepartementHidden");
            if (!isset($iDepartementId)) $iDepartementId = 0;
            $iOrderBy = $this->input->post("inputStringOrderByHidden");
            if (!isset($iOrderBy)) $iOrderBy = 0;
            $zMotCle = $this->input->post("zMotCle");
            if (!isset($zMotCle)) $zMotCle = 0;
            $inputFromGeolocalisation = $this->input->post("inputFromGeolocalisation");
            if (!isset($inputFromGeolocalisation)) $inputFromGeolocalisation = "0";
            $inputGeolocalisationValue = $this->input->post("inputGeolocalisationValue");
            if (!isset($inputFromGeolocalisation)) $inputGeolocalisationValue = "0";
            $inputGeoLatitude = $this->input->post("inputGeoLatitude");
            if (!isset($inputGeoLatitude)) $inputGeoLatitude = "0";
            $inputGeoLongitude = $this->input->post("inputGeoLongitude");
            if (!isset($inputGeoLongitude)) $inputGeoLongitude = "0";
            $inputAvantagePartenaire = $this->input->post("inputAvantagePartenaire");
            if (!isset($inputAvantagePartenaire)) $inputAvantagePartenaire = "0";

            $data['iCategorieId'] = $iCategorieId;
            $data['iSubCategorieId'] = $iSubCategorieId;
            $data['iVilleId'] = $iVilleId;
            $data['iDepartementId'] = $iDepartementId;
            $data['zMotCle'] = $zMotCle;
            $data['iOrderBy'] = $iOrderBy;
            $data['inputAvantagePartenaire'] = $inputAvantagePartenaire;
            $input_is_actif_coronna = $this->input->post('input_is_actif_coronna');
            $input_is_actif_command = $this->input->post('input_is_actif_command');
            if (!isset($input_is_actif_coronna)){
                $input_is_actif_coronna =0;
            }
            if (!isset($input_is_actif_command)){
                $input_is_actif_command = 0;
            }
            $TotalRows = count(
                $this->mdlcommercantpagination->listePartenaireRecherche(
                    $iCategorieId,
                    $iVilleId,
                    $iDepartementId,
                    $zMotCle,
                    $data["iFavoris"],
                    0,
                    10000,
                    $iOrderBy,
                    $inputFromGeolocalisation,
                    $inputGeolocalisationValue,
                    $inputGeoLatitude,
                    $inputGeoLongitude,
                    $inputAvantagePartenaire,
                    $iSubCategorieId,
                    $input_is_actif_coronna,
                    $input_is_actif_command
                )
            );

            // pagination list configuration
            $config_pagination = array();
            $config_pagination["base_url"] = base_url() . "soutenons/Annuaire_Soutenons/index/";
            $config_pagination["total_rows"] = $TotalRows;
            $config_pagination["per_page"] = $PerPage;
            $config_pagination["uri_segment"] = 4;
            $config_pagination['first_link'] = 'Debut';
            $config_pagination['last_link'] = 'Fin';
            $config_pagination['prev_link'] = 'Precedent';
            $config_pagination['next_link'] = 'Suivant';
            $this->pagination->initialize($config_pagination);
            $rsegment3 = $this->uri->segment(4);
            if (strpos($rsegment3, '&content_only_list') !== false) {
                $pieces_rseg = explode("&content_only_list", $rsegment3);
                $rsegment3 = $pieces_rseg[0];
            } else {
                $rsegment3 = $this->uri->segment(4);
            }
            $page_pagination = ($rsegment3) ? $rsegment3 : 0;
            $toCommercant = $this->mdlcommercantpagination->listePartenaireRecherche(
                $iCategorieId,
                $iVilleId,
                $iDepartementId,
                $zMotCle,
                $data["iFavoris"],
                $page_pagination,
                $config_pagination["per_page"],
                $iOrderBy,
                $inputFromGeolocalisation,
                $inputGeolocalisationValue,
                $inputGeoLatitude,
                $inputGeoLongitude,
                $inputAvantagePartenaire,
                $iSubCategorieId,
                $input_is_actif_coronna,
                $input_is_actif_command
            );

            $data["links_pagination"] = $this->pagination->create_links();
            $data["ci_pagination"] = $this->pagination;

            $iNombreLiens = $TotalRows / $PerPage;
            if ($iNombreLiens > round($iNombreLiens)) {
                $iNombreLiens = round($iNombreLiens) + 1;
            } else {
                $iNombreLiens = round($iNombreLiens);
            }

            $data["iNombreLiens"] = $iNombreLiens;
            $data["PerPage"] = $PerPage;
            $data["TotalRows"] = $TotalRows;
            $data["argOffset"] = $argOffset;
            $data['toCommercant'] = $toCommercant;

            // testing mobile gaphism
            $is_mobile = $this->agent->is_mobile();
            //test ipad user agent
            $is_mobile_ipad = $this->agent->is_mobile('ipad');
            $data['is_mobile_ipad'] = $is_mobile_ipad;
            $is_robot = $this->agent->is_robot();
            $is_browser = $this->agent->is_browser();
            $is_platform = $this->agent->platform();
            $data['is_mobile'] = $is_mobile;
            $data['is_robot'] = $is_robot;
            $data['is_browser'] = $is_browser;
            $data['is_platform'] = $is_platform;

            if (isset($_POST["from_mobile_search_page_partner"])) {
                $data['from_mobile_search_page_partner'] = $_POST["from_mobile_search_page_partner"];
                $this->session->set_userdata('from_mobile_search_page_partner', $_POST["from_mobile_search_page_partner"]);
            }

            $data['from_mobile_search_page_partner'] = $this->session->userdata('from_mobile_search_page_partner');

            if ($this->ion_auth->logged_in()) {
                $user_ion_auth = $this->ion_auth->user()->row();
                $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                if ($iduser == null || $iduser == 0 || $iduser == "") {
                    $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                }
                $data['IdUser'] = $iduser;
            }

            //get ville/commune list
            $toVille = $this->mdlville->GetAgendaVilles_pvc_by_varest_annuaire_soutenons($iDepartementId);
            $data['toVille'] = $toVille;

            // get subcateg list
            $toSubCategorie = $this->sousRubrique->GetAll();
            $data['toSubCategorie'] = $toSubCategorie;

            //get Departements list of commercant
            $toDepartements = $this->mdlville->GetCommercantDepartement();
            $data['toDepartement'] = $toDepartements;

            // set main content page name
            $data["main_menu_content"] = "annuaire";
            $data['pagecategory'] = 'annuaire';// IMPORTANT

            $this->session->set_userdata('nohome', '1');

            if ($_SERVER['SERVER_NAME'] == DOMAIN_SORTEZ_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_SORTEZ_GLOBAL) {
                //$this->load->view('sortez/annuaire_main', $data);
                $this->load->view('sortez_vsv/annuaire_index', $data);
            } elseif ($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL) {
                //$this->load->view("vivresavilleView/ville-test/annuaire-index", $data);
                $this->load->view("vivresaville/annuaire_index", $data);
            }
        }
    }



    function localdata_IdVille($id_ville_selected = "0")



    {



        if (isset($id_ville_selected) && $id_ville_selected != "0" && $id_ville_selected != "" && is_numeric($id_ville_selected)) {



            $this->session->sess_destroy('localdata_IdVille');



            $this->session->set_userdata('localdata_IdVille', $id_ville_selected);



            redirect("front/annuaire");



        } else {



            redirect("vivresaville/ListVille");



        }



    }







    function localdata_IdDepartement($IdDepartement)



    {



        //echo "ok";



        //LOCALDATA FILTRE SET



        $this->session->set_userdata('localdata_IdDepartement', $IdDepartement);



        //LOCALDATA FILTRE SET



        redirect("front/annuaire");



    }











    function redirection($_iPage)



    {



        $_SESSION["argOffset"] = $_iPage;



        $this->session->set_userdata('from_mobile_search_page_partner', '1');



        redirect("/");



    }







    function redirect_home()



    {



        unset($_SESSION['iCategorieId']);



        unset($_SESSION['iVilleId']);



        unset($_SESSION['zMotCle']);



        redirect("/");



    }











    function check_category_list()

    {

        $inputStringDepartementHidden_partenaires = $this->input->post("inputStringDepartementHidden_partenaires");

        $inputStringVilleHidden_partenaires = $this->input->post("inputStringVilleHidden_partenaires");

        ////$this->firephp->log($inputStringVilleHidden_partenaires, 'inputStringVilleHidden_partenaires');

        //$toCategorie= $this->mdlcategorie->GetCommercantSouscategorie();

        //$toVille= $this->mdlville->GetCommercantVilles();//get ville list of commercant

        if ((isset($inputStringVilleHidden_partenaires) && $inputStringVilleHidden_partenaires != "" && $inputStringVilleHidden_partenaires != '0' && $inputStringVilleHidden_partenaires != NULL)

            || (isset($inputStringDepartementHidden_partenaires) && $inputStringDepartementHidden_partenaires != "" && $inputStringDepartementHidden_partenaires != '0' && $inputStringDepartementHidden_partenaires != NULL)) {

            $toCategorie_principale = $this->mdlcategorie->GetCommercantCategoriebyVille_x_soutenons($inputStringDepartementHidden_partenaires, $inputStringVilleHidden_partenaires);

        } else {

            $toCategorie_principale = $this->mdlcategorie->GetCommercantCategorie_x();

        }


        $result_to_show = '';


        if (isset($toCategorie_principale)) {

            foreach ($toCategorie_principale as $oCategorie_principale) {


                //vérification nbCommercant par catégorie pple******************

                //$OCommercantSousRubrique_verifnb = $this->mdlcategorie->GetCommercantSouscategorieByRubrique_x($oCategorie_principale->IdRubrique);

                //search by Subcategory Id > all comm should be associated with a subcateg

                $OCommercantSousRubrique_verifnb = $this->mdlcategorie->GetCommercantSouscategorieByRubriqueVilleDepartement_x($oCategorie_principale->IdRubrique, $inputStringVilleHidden_partenaires, $inputStringDepartementHidden_partenaires);

                $verifnb = 0;

                if (isset($OCommercantSousRubrique_verifnb)) {

                    foreach ($OCommercantSousRubrique_verifnb as $ObjCommercantSousRubrique_verifnb) {

                        if (isset($inputStringVilleHidden_partenaires)) $iVilleId_verif = $inputStringVilleHidden_partenaires; else $iVilleId_verif = 0;

                        $IdSousRubrique_verif[0] = $ObjCommercantSousRubrique_verifnb->IdSousRubrique;

                        $toCommercant_verif = $this->mdlcommercantpagination->listePartenaireRecherche($oCategorie_principale->IdRubrique, $iVilleId_verif, $inputStringDepartementHidden_partenaires, "", "", 0, 10000, "", "0", "10", "0", "0", "0", $IdSousRubrique_verif);

                        $verifnb = $verifnb + count($toCommercant_verif);

                    }

                }

                //vérification nbCommercant par catégorie pple******************

                //Bouton categorie

                /*if ($_SERVER['SERVER_NAME'] == DOMAIN_SORTEZ_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_SORTEZ_GLOBAL) {

                    if (isset($verifnb) && $verifnb != 0) {

                        $result_to_show .= '<div class="leftcontener2013title btn btn-default left_category_list_annuaire" onClick="javascript:show_current_categ_subcateg(' . $oCategorie_principale->IdRubrique . ')" style="margin-top:5px;">';

                        $result_to_show .= ucfirst(strtolower($oCategorie_principale->Nom)) . " (" . $verifnb . ")";

                        $result_to_show .= '</div>';

                    }

                    $result_to_show .= '<div class="leftcontener2013content" id="leftcontener2013content_' . $oCategorie_principale->IdRubrique . '" style="margin-bottom:5px; margin-top:10px;">';

                } elseif ($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL) {*/

                if (isset($verifnb) && $verifnb != 0) {

                    $result_to_show .= '<a class="dropdown-item" id="vsv_categmain_' . $oCategorie_principale->IdRubrique . '" href="javascript:void(0);" onClick="javascript:show_current_categ_subcateg(' . $oCategorie_principale->IdRubrique . ')">';

                    $result_to_show .= '' . ucfirst(strtolower($oCategorie_principale->Nom)) . " (" . $verifnb. ")"; // . " (" . $verifnb . ")"

                    $result_to_show .= '</a>';

                }

                $result_to_show .= '<div class="leftcontener2013content d-lg-none d-xl-none row m-0" id="leftcontener2013content_' . $oCategorie_principale->IdRubrique . '" style="margin-bottom:5px; margin-top:10px;">';

                //}


                //$OCommercantSousRubrique = $this->mdlcategorie->GetCommercantSouscategorieByRubrique_x($oCategorie_principale->IdRubrique);

                $OCommercantSousRubrique = $this->mdlcategorie->GetCommercantSouscategorieByRubriqueVilleDepartement_x($oCategorie_principale->IdRubrique, $inputStringVilleHidden_partenaires, $inputStringDepartementHidden_partenaires);


                //$result_to_show .= '<br/>';


                /*if (isset($_SESSION['iCategorieId'])) {

                    $iCategorieId_sess = $_SESSION['iCategorieId'];

                }*/


                $session_iCategorieId = $this->session->userdata('iCategorieId_x');

                if (isset($session_iCategorieId)) {

                    $iCategorieId_sess = $session_iCategorieId;

                }


                if (count($OCommercantSousRubrique) > 0) {


                    //adding 'toutes les sous-categories' button

                    //if ($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL) {


//                    $result_to_show .= '<a class="dropdown-item" id="vsv_sub_categmain_' . $oCategorie_principale->IdRubrique . '" onClick="javascript:all_subcategories_func(' . $oCategorie_principale->IdRubrique . ');">';
//
//                    $result_to_show .= '<span>Toutes</span>';
//
//                    $result_to_show .= '</a>';


                    //}

                    //end adding 'toutes les sous-categories' button


                    $i_rand = 0;

                    foreach ($OCommercantSousRubrique as $ObjCommercantSousRubrique) {


                        //Vérification nbCommercant par sous categorie **********************

                        $verifnb_souscat = 0;

                        if (isset($inputStringVilleHidden_partenaires)) $iVilleId_verif_souscat = $inputStringVilleHidden_partenaires; else $iVilleId_verif_souscat = 0;

                        $IdSousRubrique_verif_souscat[0] = $ObjCommercantSousRubrique->IdSousRubrique;

                        $toCommercant_verif_souscat = $this->mdlcommercantpagination->listePartenaireRecherche($oCategorie_principale->IdRubrique, $iVilleId_verif_souscat, 0, "", "", 0, 10000, "", "0", "10", "0", "0", "0", $IdSousRubrique_verif_souscat);

                        $verifnb_souscat = count($toCommercant_verif_souscat);

                        //Vérification nbCommercant par sous categorie **********************


                        if (isset($verifnb_souscat) && $verifnb_souscat != 0) {

                            // menu filtre subcateg -------


                            $result_to_show .= '<a class="dropdown-item" id="vsv_sub_categmain_' . $ObjCommercantSousRubrique->IdSousRubrique . '" href="javascript:void(0);" onclick="javascript:show_current_subcategcontent_soutenons(' . $ObjCommercantSousRubrique->IdSousRubrique . ')">';


                            $result_to_show .= '<span>' . ucfirst(strtolower($ObjCommercantSousRubrique->Nom)) . ' (' . $verifnb_souscat . ')</span>'; // $verifnb_souscat

                            $result_to_show .= '</a>';


                            //-------------------------------

                        }

                        $i_rand++;

                    }


                    //if ($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL)


                }

                $result_to_show .= '</div>';

            }

        }


        echo $result_to_show;


    }











    function check_Idcategory_of_subCategory()



    {



        $session_iCategorieId = $this->session->userdata('iCategorieId_x');



        //var_dump($session_iCategorieId);



        if (isset($session_iCategorieId)) {



            $iCategorieId_sess = $session_iCategorieId;



            if (isset($iCategorieId_sess) && is_array($iCategorieId_sess)) {



                $all_subcategory = "";



                for ($i = 0; $i < count($iCategorieId_sess); $i++) {



                    $sousRubrique = $this->sousRubrique->GetById($iCategorieId_sess[$i]);



                    $all_subcategory .= "-" . $sousRubrique->IdRubrique;



                }



                echo substr($all_subcategory, 1);



            } else {



                echo "0";



            }



        } else echo "0";



    }



    function main_commune_list(){



        $this->load->model("mdlville");



        $this->load->model("mdl_ville");







        $id_ville_selected = $this->input->post("id_ville_selected");



        if (isset($id_ville_selected) && $id_ville_selected != "0" && $id_ville_selected != "") {



            $vsv_object = $this->mdl_ville->getById($id_ville_selected);



            if (isset($vsv_object) && isset($vsv_object->IdVille)) {



                $this->session->set_userdata('iVilleId_x', $vsv_object->IdVille);



                redirect("front/annuaire");



            }



        }



        $toDepartement = $this->mdlville->GetAgendaVilles_pvc_by_varest_annuaire();



        $data['toDepartement'] = $toDepartement;



        $this->load->view("main_commune_list_vw", $data);



    }



    public function get_count_annuaire(){







        $TotalRows = count($this->mdlcommercantpagination->listePartenaireRecherche(0, 0, 0, "", "", 0, 10000, "", "0", "10", "0", "0",  "0", 0));



        echo '('.$TotalRows.')';



    }



}