<?php
class publightbox extends CI_Controller{
    
    function __construct() {
        parent::__construct();
		$this->load->model("mdlannonce") ;
		$this->load->model("mdlcommercant") ;
		$this->load->model("mdlbonplan") ;
		$this->load->model("mdlcategorie") ;
		$this->load->model("mdlville") ;
		$this->load->model("mdldepartement") ;
		$this->load->model("mdlcommercantpagination") ;
        $this->load->model("sousRubrique") ;
        $this->load->model("AssCommercantAbonnement") ;
        $this->load->model("AssCommercantSousRubrique") ;
        $this->load->model("Commercant") ;
        $this->load->model("user") ;
        $this->load->model("mdl_agenda");
        $this->load->model("mdl_categories_agenda") ;
        $this->load->model("mdlimagespub") ;
        $this->load->model("Abonnement");
        $this->load->Model("mdlagenda_perso");
        $this->load->Model("mdldepartement");

        $this->load->library('user_agent');
        $this->load->library("pagination");
        $this->load->library('image_moo');
        $this->load->library('session');

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");


        check_vivresaville_id_ville();


    }

    function index(){
        $data['zTitle'] = "Privicarte";
        $data['mail_sender_adress'] = $this->config->item("mail_sender_adress");
        $alldepartement = $this->mdldepartement->GetAll();
        //var_dump($alldepartement);
        $data['alldepartement'] = $alldepartement;
        $data['currentpage'] = "publightbox";
        $this->load->view('privicarte/includes/publightbox', $data);
    }

    function deconnexion(){
        $this->session->unset_userdata('publightbox_email');
        $this->session->unset_userdata('publightbox_name');
        redirect("/");
    }

    function carte() {
        $publightbox_email = $this->session->userdata('publightbox_email');
        $this->session->set_userdata('publightbox_email', $publightbox_email);
        $data['zTitle'] = "Carte Privicarte";
        $data['pagecategory'] = 'publightbox';
        $data['publightbox_email'] = $publightbox_email;
        $this->load->view('privicarte/publightbox_carte', $data);
    }



}

?>