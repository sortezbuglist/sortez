<?php



class DealAndFidelite extends CI_Controller

{



    function __construct()

    {

        parent::__construct();

        $this->load->model("mdlannonce");

        $this->load->model("mdlcommercant");

        $this->load->model("mdlbonplan");

        $this->load->model("mdlcategorie");


        $this->load->model("rubrique");

        $this->load->model("Sousrubrique");

        $this->load->model("mdlimagespub");

        $this->load->model("AssCommercantAbonnement");

        $this->load->model("mdlville");

        $this->load->model("mdldepartement");

        $this->load->model("mdlfidelity");

        $this->load->library('user_agent');

        $this->load->model("Abonnement");

        $this->load->model("commercant");

        $this->load->Model("mdlannonce_perso");

        $this->load->model("mdlarticle");

        $this->load->model("Mdlcommercant");

        $this->load->model("mdl_agenda");

        $this->load->model("mdlcommune");

        $this->load->model("mdl_categories_annonce");



        $this->load->model("mdl_plat_du_jour");



        $this->load->library('session');

        $this->load->Model("mdl_localisation");

        $this->load->library("pagination");

        $this->load->library('image_moo');

        $this->load->library('ion_auth');

        $this->load->model("ion_auth_used_by_club");



        delete_revue_fin_2journ();

        check_vivresaville_id_ville();

    }

    function index(){


        $rsegment3 = $this->uri->segment(4);
        if (strpos($rsegment3, '&content_only_list') !== false) {

            $pieces_rseg = explode("&content_only_list", $rsegment3);

            $rsegment3 = $pieces_rseg[0];

        } else {

            $rsegment3 = $this->uri->segment(4);

        }

        $zMotcle_value = $this->input->post('zMotcle_value');
        $type_vue_deal = $this->input->post('type_vue_deal');
        $id_categ_deal = $this->input->post('id_categ_deal');
        $id_depart_deal = $this->input->post('id_depart_deal');
        $id_communes_deal = $this->input->post('id_communes_deal');

        $per_page = 20;

        $data['PerPage'] = $per_page;

        $page_pagination = ($rsegment3) ? $rsegment3 : 0;

        //var_dump($page_pagination);

        $data["iFavoris"] = $this->input->post("hdnFavoris");

        $session_iCategorieId = "";
        $categ_get_all =[];
        if($id_categ_deal != null){
            $categ_get_all[] = $id_categ_deal;
            $session_iCategorieId = $categ_get_all;
        }


        $session_iVilleId = "";
        if($id_communes_deal!= null){
            $session_iVilleId = $id_communes_deal;
        }

        $session_iDepartementId = "";
        if($id_depart_deal!= null){
            $session_iDepartementId = $id_depart_deal;
        }

        $session_iCommercantId = "";

        $session_zMotCle = "";
        if($zMotcle_value != null){
            $session_zMotCle = $zMotcle_value;
        }

        $session_iOrderBy = "";

        $session_inputFromGeolocalisation = "";

        $session_inputGeolocalisationValue = "";

        $session_inputGeoLatitude = "";

        $session_inputGeoLongitude = "";

        $session_inputFidelityType = "";

        $allarrays = [];
        $session_iWhereMultiple = "";

        $session_inputStringQuandHidden = "";

        $session_inputStringDatedebutHidden = "";

        $session_inputStringDatefinHidden = "";

        $session_inputStringidsousrubriqueHidden = "";

        $inputString_bonplan_type = "";

        if($type_vue_deal == 1){
            $toListeBonPlan_all_pvc = $this->mdlbonplan->listeBonPlanRecherche_new($session_iCategorieId, $session_iCommercantId, $session_zMotCle, $data["iFavoris"], $page_pagination, $per_page, $session_iVilleId, $session_iDepartementId, $session_iOrderBy, $session_inputFromGeolocalisation, $session_inputGeolocalisationValue, $session_inputGeoLatitude, $session_inputGeoLongitude, $session_iWhereMultiple,$inputString_bonplan_type);
        }elseif($type_vue_deal == 2){
            $toListeFidelity_remise = $this->mdlfidelity->listeFidelityRecherche_new($session_iCategorieId, $session_iCommercantId, $session_zMotCle, $data["iFavoris"], $page_pagination, $per_page, $session_iVilleId, $session_iDepartementId, $session_iOrderBy, $session_inputFromGeolocalisation, $session_inputGeolocalisationValue, $session_inputGeoLatitude, $session_inputGeoLongitude, $session_inputFidelityType, "remise");
            $toListeFidelity_tampon = $this->mdlfidelity->listeFidelityRecherche_new($session_iCategorieId, $session_iCommercantId, $session_zMotCle, $data["iFavoris"], $page_pagination, $per_page, $session_iVilleId, $session_iDepartementId, $session_iOrderBy, $session_inputFromGeolocalisation, $session_inputGeolocalisationValue, $session_inputGeoLatitude, $session_inputGeoLongitude, $session_inputFidelityType, "tampon");
            $toListeFidelity_capital = $this->mdlfidelity->listeFidelityRecherche_new($session_iCategorieId, $session_iCommercantId, $session_zMotCle, $data["iFavoris"], $page_pagination, $per_page, $session_iVilleId, $session_iDepartementId, $session_iOrderBy, $session_inputFromGeolocalisation, $session_inputGeolocalisationValue, $session_inputGeoLatitude, $session_inputGeoLongitude, $session_inputFidelityType, "capital");
        }else{
            $toListeFidelity_remise = $this->mdlfidelity->listeFidelityRecherche_new($session_iCategorieId, $session_iCommercantId, $session_zMotCle, $data["iFavoris"], $page_pagination, $per_page, $session_iVilleId, $session_iDepartementId, $session_iOrderBy, $session_inputFromGeolocalisation, $session_inputGeolocalisationValue, $session_inputGeoLatitude, $session_inputGeoLongitude, $session_inputFidelityType, "remise");
            $toListeFidelity_tampon = $this->mdlfidelity->listeFidelityRecherche_new($session_iCategorieId, $session_iCommercantId, $session_zMotCle, $data["iFavoris"], $page_pagination, $per_page, $session_iVilleId, $session_iDepartementId, $session_iOrderBy, $session_inputFromGeolocalisation, $session_inputGeolocalisationValue, $session_inputGeoLatitude, $session_inputGeoLongitude, $session_inputFidelityType, "tampon");
            $toListeFidelity_capital = $this->mdlfidelity->listeFidelityRecherche_new($session_iCategorieId, $session_iCommercantId, $session_zMotCle, $data["iFavoris"], $page_pagination, $per_page, $session_iVilleId, $session_iDepartementId, $session_iOrderBy, $session_inputFromGeolocalisation, $session_inputGeolocalisationValue, $session_inputGeoLatitude, $session_inputGeoLongitude, $session_inputFidelityType, "capital");
            $toListeBonPlan_all_pvc = $this->mdlbonplan->listeBonPlanRecherche_new($session_iCategorieId, $session_iCommercantId, $session_zMotCle, $data["iFavoris"], $page_pagination, $per_page, $session_iVilleId, $session_iDepartementId, $session_iOrderBy, $session_inputFromGeolocalisation, $session_inputGeolocalisationValue, $session_inputGeoLatitude, $session_inputGeoLongitude, $session_iWhereMultiple,$inputString_bonplan_type);
        }
        //$toListePlat_du_jour = $this->mdl_plat_du_jour->listeplatRecherche( $session_iVilleId, $session_iDepartementId, $session_zMotCle,  0, 10000, $session_iOrderBy, $session_inputStringQuandHidden, $session_inputStringDatedebutHidden, $session_inputStringDatefinHidden, $session_iCommercantId,$session_inputStringidsousrubriqueHidden);
        //$toListeBonPlan_all_pvc1 = $this->mdlbonplan->listeBonPlanRecherche_sans_referencement_bonplan($session_iCategorieId, $session_iCommercantId, $session_zMotCle, $data["iFavoris"], $page_pagination, $per_page, $session_iVilleId, $session_iDepartementId, $session_iOrderBy, $session_inputFromGeolocalisation, $session_inputGeolocalisationValue, $session_inputGeoLatitude, $session_inputGeoLongitude, $session_inputFidelityType ,$inputString_bonplan_type);

//        var_dump(count($toListeBonPlan_all_pvc1));
//        var_dump(count($toListeBonPlan_all_pvc));
        $i = 0;
        if ($toListeFidelity_remise !=[]){
            foreach ($toListeFidelity_remise as $listremise){
                $allarrays[$i]['id'] = $listremise->id;
                $allarrays[$i]['titre'] = $listremise->titre;
                $allarrays[$i]['description'] = $listremise->description;
                $allarrays[$i]['image1'] = $listremise->image1;
                $allarrays[$i]['date_fin'] = $listremise->date_fin;
                $allarrays[$i]['type'] = "remise";
                $allarrays[$i]['ville_nom'] = $listremise->ville;
                $allarrays[$i]['ville_id'] = $listremise->IdVille;
                $allarrays[$i]['idcom'] = $listremise->IdCommercant;
                $allarrays[$i]['partenaire'] = $listremise->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->commercant->GetById($listremise->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_tampon !=[]){
            foreach ($toListeFidelity_tampon as $listtampon){
                $allarrays[$i]['id'] = $listtampon->id;
                $allarrays[$i]['titre'] = $listtampon->titre;
                $allarrays[$i]['description'] = $listtampon->description;
                $allarrays[$i]['image1'] = $listtampon->image1;
                $allarrays[$i]['date_fin'] = $listtampon->date_fin;
                $allarrays[$i]['type'] = "tampon";
                $allarrays[$i]['ville_nom'] = $listtampon->ville;
                $allarrays[$i]['ville_id'] = $listtampon->IdVille;
                $allarrays[$i]['idcom'] = $listtampon->IdCommercant;
                $allarrays[$i]['partenaire'] = $listtampon->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->commercant->GetById($listtampon->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_capital !=[]){
            foreach ($toListeFidelity_capital as $listcapitel){
                $allarrays[$i]['id'] = $listcapitel->id;
                $allarrays[$i]['titre'] = $listcapitel->titre;
                $allarrays[$i]['description'] = $listcapitel->description;
                $allarrays[$i]['image1'] = $listcapitel->image1;
                $allarrays[$i]['date_fin'] = $listcapitel->date_fin;
                $allarrays[$i]['type'] = "capital";
                $allarrays[$i]['ville_nom'] = $listcapitel->ville;
                $allarrays[$i]['ville_id'] = $listcapitel->IdVille;
                $allarrays[$i]['idcom'] = $listcapitel->IdCommercant;
                $allarrays[$i]['partenaire'] = $listcapitel->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->commercant->GetById($listcapitel->IdCommercant)->nom_url;
                $i++;
            }
        }
        if ($toListeBonPlan_all_pvc !=[]){
            foreach ($toListeBonPlan_all_pvc as $bonplan){
                if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "2"){
                    $date = $bonplan->bp_unique_date_fin;
                    //$type = "Bonplan unique";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "1"){
                    $date = $bonplan->bonplan_date_fin;
                    //$type = "Bonplan simple";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "3"){
                    $date = $bonplan->bp_multiple_date_fin;
                    //$type = "Bonplan multiples";
                }

                $allarrays[$i]['id'] = $bonplan->bonplan_id;
                $allarrays[$i]['titre'] = $bonplan->bonplan_titre;
                $allarrays[$i]['description'] = $bonplan->bonplan_texte;
                $allarrays[$i]['image1'] = $bonplan->bonplan_photo1;
                $allarrays[$i]['date_fin'] = $date;
                $allarrays[$i]['type'] = $bonplan->bonplan_type;
                $allarrays[$i]['ville_nom'] = $this->mdlville->getVilleById($this->commercant->GetById($bonplan->bonplan_commercant_id)->IdVille_localisation)->Nom;
                $allarrays[$i]['ville_id'] = $this->commercant->GetById($bonplan->bonplan_commercant_id)->IdVille_localisation;
                $allarrays[$i]['idcom'] = $bonplan->bonplan_commercant_id;
                $allarrays[$i]['partenaire'] = $this->commercant->GetById($bonplan->bonplan_commercant_id)->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->commercant->GetById($bonplan->bonplan_commercant_id)->nom_url;
                $i++;
            }
        }
        $alldepatements = [];
        $allvilles = [];
        $get_all_departss = [];
        $get_all_vilesss = [];
        $allrubriques = [];
        $tobonplanDepartement = $this->mdldepartement->GetBonplanDepartement_pvc_soutenons();
        $tofidelityDepartement = $this->mdldepartement->GetFidelityDepartements_soutenons();
        //$toplatDepartement = $this->mdldepartement->GetplatDepartements_pvc();
        array_push($alldepatements,$tobonplanDepartement,$tofidelityDepartement);
//        var_dump($alldepatements);die();
        $departement_check = $this->session->userdata('iDepartementId_x');
        $bonplanCateg = $this->mdlcategorie->GetBonplanCategoriePrincipaleBycateg_soutenons($_iCommercantId = "0");
        $fidelityCateg = $this->mdlcategorie->GetFidelityCategoriePrincipaleByVille_soutenons($_departement = "0",$session_iVilleId);
        //$categplat = $this->mdl_plat_du_jour->GetPlatCategorie_by_params($session_inputStringQuandHidden, '0000-00-00', '0000-00-00', $departement_check, $session_iVilleId, $inputIdCommercant=0);

        array_push($allrubriques,$bonplanCateg,$fidelityCateg);

        if (isset($departement_check) && $departement_check != "" && $departement_check != null) {

            //$toVille = $this->mdlville->GetAnnonceVilles_pvc_by_departement($this->session->userdata('iDepartementId_x'));

            $tobonplanVille = $this->mdlville->GetBonplanVilles_by_departement_soutenons($this->session->userdata('iDepartementId_x'));
            //$toplatVille = $this->mdlville->GetplatVillesByIdCommercant_by_departement($this->session->userdata('iCommercantId_x'),$this->session->userdata('iDepartementId_x'));
            $tofidelityVille = $this->mdlville->GetFidelityVilles_by_departement_soutenons($this->session->userdata('iDepartementId_x'));

            array_push($allvilles,$tobonplanVille,$tofidelityVille);

        } else {

            $tofidelityVille = $this->mdlville->GetFidelityVilles_varest();
            //$toplatVille = $this->mdlville->GetplatVilles_pvc();
            $tobonplanVille = $this->mdlville->GetBonplanVilles_pvc_soutenons();

            array_push($allvilles,$tobonplanVille,$tofidelityVille);

        }
        $id_check = "";
        $count_check = 0;
        $id_check_ville = "";
        $count_check_ville = 0;
        for($i = 0; $i < count($alldepatements);$i++){
            if($alldepatements[$i] != null){
                foreach ($alldepatements[$i] as $alldepatementss){
                    array_push($get_all_departss,$alldepatementss);
                }
            }

        }
        for($i = 0; $i < count($allvilles);$i++){
            if($allvilles[$i] != null) {
                foreach ($allvilles[$i] as $allvilless) {
                    array_push($get_all_vilesss, $allvilless);
                }
            }
        }
        $get_all_categs = [];
        for($i = 0; $i < count($allrubriques);$i++){
            if($allrubriques[$i] != null) {
                foreach ($allrubriques[$i] as $allrubriquess) {
                    array_push($get_all_categs, $allrubriquess);
                }
            }
        }
        $last_id= 0;
        $numb_categ = [];
        //var_dump($get_all_categs);
//        $numb_categ[] = new StdClass;
        foreach ($get_all_categs as $get_all_categ){
            if($get_all_categ->IdRubrique != $last_id){
                $numb_categ[$get_all_categ->IdRubrique] = array(
                    "IdRubrique" => $get_all_categ->IdRubrique,
                    "rubrique" => $get_all_categ->rubrique,
                    "nbCommercant" => $get_all_categ->nbCommercant,
                );
                $last_id = $get_all_categ->IdRubrique;
            }else{
                $numb_categ[$get_all_categ->IdRubrique]['nbCommercant'] += 1;
                $numb_categ[$get_all_categ->IdRubrique] = array(
                    "IdRubrique" => $get_all_categ->IdRubrique,
                    "rubrique" => $get_all_categ->rubrique,
                    "nbCommercant" => $numb_categ[$get_all_categ->IdRubrique]['nbCommercant'],
                );
                $last_id = $get_all_categ->IdRubrique;
            }
        }
        //var_dump($numb_categ);
        $last_id_ville= 0;
        $numb_ville = [];
        foreach ($get_all_vilesss as $get_all_viless){
            if($get_all_viless->IdVille != $last_id_ville){
                $numb_ville[$get_all_viless->IdVille] = array(
                    "IdVille" => $get_all_viless->IdVille,
                    "ville_nom" => $get_all_viless->ville_nom,
                    "nbCommercant" => $get_all_viless->nbCommercant,
                );
                $last_id_ville = $get_all_viless->IdVille;
            }else{
                $numb_ville[$get_all_viless->IdVille]['nbCommercant'] += 1;
                $numb_ville[$get_all_viless->IdVille] = array(
                    "IdVille" => $get_all_viless->IdVille,
                    "ville_nom" => $get_all_viless->ville_nom,
                    "nbCommercant" => $numb_ville[$get_all_viless->IdVille]['nbCommercant'],
                );
                $last_id_ville = $get_all_viless->IdVille;
            }
        }
//        var_dump($get_all_departss);
        $last_id_depart= 0;
        $numb_depart = [];
        foreach ($get_all_departss as $get_all_departs){
            if($get_all_departs->departement_id != $last_id_depart){
                $numb_depart[$get_all_departs->departement_id] = array(
                    "departement_id" => $get_all_departs->departement_id,
                    "departement_code" => $get_all_departs->departement_code,
                    "departement_nom" => $get_all_departs->departement_nom,
                    "nbCommercant" => $get_all_departs->nbCommercant,
                );
                $last_id_depart = $get_all_departs->departement_id;
            }else{
                $numb_depart[$get_all_departs->departement_id]['nbCommercant'] += 1;
                $numb_depart[$get_all_departs->departement_id] = array(
                    "departement_id" => $get_all_departs->departement_id,
                    "departement_code" => $get_all_departs->departement_code,
                    "departement_nom" => $get_all_departs->departement_nom,
                    "nbCommercant" => $numb_depart[$get_all_departs->departement_id]['nbCommercant'],
                );
                $last_id_depart = $get_all_departs->departement_id;
            }
        }
        //var_dump($numb_depart);
        $data['toallcateg'] = $numb_categ;
        $data['toDepartement'] = $numb_depart;
        $data['toVille'] = $numb_ville;
        $data['count_deal'] = count($allarrays);
        //var_dump(count($allarrays));
        $data['alldata'] = $allarrays;
        $data['pagecategory'] = 'dealandfidelity';
        $this->load->view('sortez_vsv/deal_fidelity_index.php', $data);

    }
    public function get_count_deal(){

        $toListeFidelity_remise = $this->mdlfidelity->listeFidelityRecherche("", "", "", "", 0, 10000, "", "", "", "", "", "", "", "", "remise");
        $toListeFidelity_tampon = $this->mdlfidelity->listeFidelityRecherche("", "", "", "", 0, 10000, "", "", "", "", "", "", "", "", "tampon");
        $toListeFidelity_capital = $this->mdlfidelity->listeFidelityRecherche("", "", "", "", 0, 10000, "", "", "", "", "", "", "", "", "capital");
        $toListeBonPlan_all_pvc = $this->mdlbonplan->listeBonPlanRecherche("", "", "", "", 0, 10000, "", "", "", "", "", "", "", "","");

        if ($toListeFidelity_remise !=[]){
            foreach ($toListeFidelity_remise as $listremise){
                $allarrays[$i]['id'] = $listremise->id;
                $allarrays[$i]['titre'] = $listremise->titre;
                $allarrays[$i]['description'] = $listremise->description;
                $allarrays[$i]['image1'] = $listremise->image1;
                $allarrays[$i]['date_fin'] = $listremise->date_fin;
                $allarrays[$i]['type'] = "remise";
                $allarrays[$i]['ville_nom'] = $listremise->ville;
                $allarrays[$i]['ville_id'] = $listremise->IdVille;
                $allarrays[$i]['idcom'] = $listremise->IdCommercant;
                $allarrays[$i]['partenaire'] = $listremise->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->commercant->GetById($listremise->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_tampon !=[]){
            foreach ($toListeFidelity_tampon as $listtampon){
                $allarrays[$i]['id'] = $listtampon->id;
                $allarrays[$i]['titre'] = $listtampon->titre;
                $allarrays[$i]['description'] = $listtampon->description;
                $allarrays[$i]['image1'] = $listtampon->image1;
                $allarrays[$i]['date_fin'] = $listtampon->date_fin;
                $allarrays[$i]['type'] = "tampon";
                $allarrays[$i]['ville_nom'] = $listtampon->ville;
                $allarrays[$i]['ville_id'] = $listtampon->IdVille;
                $allarrays[$i]['idcom'] = $listtampon->IdCommercant;
                $allarrays[$i]['partenaire'] = $listtampon->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->commercant->GetById($listtampon->IdCommercant)->nom_url;
                $i ++;
            }
        }
        if ($toListeFidelity_capital !=[]){
            foreach ($toListeFidelity_capital as $listcapitel){
                $allarrays[$i]['id'] = $listcapitel->id;
                $allarrays[$i]['titre'] = $listcapitel->titre;
                $allarrays[$i]['description'] = $listcapitel->description;
                $allarrays[$i]['image1'] = $listcapitel->image1;
                $allarrays[$i]['date_fin'] = $listcapitel->date_fin;
                $allarrays[$i]['type'] = "capital";
                $allarrays[$i]['ville_nom'] = $listcapitel->ville;
                $allarrays[$i]['ville_id'] = $listcapitel->IdVille;
                $allarrays[$i]['idcom'] = $listcapitel->IdCommercant;
                $allarrays[$i]['partenaire'] = $listcapitel->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->commercant->GetById($listcapitel->IdCommercant)->nom_url;
                $i++;
            }
        }
        if ($toListeBonPlan_all_pvc !=[]){
            foreach ($toListeBonPlan_all_pvc as $bonplan){
                if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "2"){
                    $date = $bonplan->bp_unique_date_fin;
                    //$type = "Bonplan unique";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "1"){
                    $date = $bonplan->bonplan_date_fin;
                    //$type = "Bonplan simple";
                }
                else if(isset($bonplan->bonplan_type) && $bonplan->bonplan_type == "3"){
                    $date = $bonplan->bp_multiple_date_fin;
                    //$type = "Bonplan multiples";
                }

                $allarrays[$i]['id'] = $bonplan->bonplan_id;
                $allarrays[$i]['titre'] = $bonplan->bonplan_titre;
                $allarrays[$i]['description'] = $bonplan->bonplan_texte;
                $allarrays[$i]['image1'] = $bonplan->bonplan_photo1;
                $allarrays[$i]['date_fin'] = $date;
                $allarrays[$i]['type'] = $bonplan->bonplan_type;
                $allarrays[$i]['ville_nom'] = $this->mdlville->getVilleById($this->commercant->GetById($bonplan->bonplan_commercant_id)->IdVille_localisation)->Nom;
                $allarrays[$i]['ville_id'] = $this->commercant->GetById($bonplan->bonplan_commercant_id)->IdVille_localisation;
                $allarrays[$i]['idcom'] = $bonplan->bonplan_commercant_id;
                $allarrays[$i]['partenaire'] = $this->commercant->GetById($bonplan->bonplan_commercant_id)->NomSociete;
                $allarrays[$i]['partenaire_url'] = $this->commercant->GetById($bonplan->bonplan_commercant_id)->nom_url;
                $i++;
            }
        }

        $TotalRows = count($allarrays);

        echo '('.$TotalRows.')';
    }

}

