<?php

class   article extends CI_Controller

{


    function __construct()

    {

        parent::__construct();

        $this->output->set_header('Access-Control-Allow-Origin: null');
        header('Access-Control-Allow-Credentials: omit');

        header('Access-Control-Allow-Method: PUT, GET, POST, DELETE, OPTIONS');

        header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');

        $this->load->model("mdlarticle");

        $this->load->model("mdlannonce");

        $this->load->model("mdlcommercant");

        $this->load->model("mdlbonplan");

        $this->load->model("Mdl_soutenons");

        $this->load->model("mdlcategorie");

        $this->load->model("mdlville");

        $this->load->model("mdldepartement");

        $this->load->model("mdlcommercantpagination");

        $this->load->model("sousRubrique");

        $this->load->model("AssCommercantAbonnement");

        $this->load->model("AssCommercantSousRubrique");

        $this->load->model("Commercant");

        $this->load->model("user");

        $this->load->model("mdl_agenda");

        $this->load->model("mdl_categories_agenda");

        $this->load->model("mdl_categories_article");

        $this->load->model("mdlimagespub");

        $this->load->model("Abonnement");

        $this->load->Model("mdlarticle_perso");

        $this->load->Model("mdl_article_datetime");

        $this->load->Model("mdl_agenda_datetime");


        $this->load->Model("mdl_localisation");

        $this->load->Model("mdl_article_organiser");


        $this->load->library('user_agent');

        $this->load->library("pagination");

        $this->load->library('image_moo');

        $this->load->library('session');


        $this->load->model("Mdl_plat_du_jour");


        $this->load->library('ion_auth');

        $this->load->model("ion_auth_used_by_club");

        $this->load->helper('webscrape_date');

        delete_revue_fin_2journ();

        check_vivresaville_id_ville();

        deleteOldAgenda_fin8jours();

    }


    function index()

    {

        $this->liste();

    }


    function index_alaune()

    {

        /*$this->config->load('config');

        echo $this->config->item('base_url');

        echo "<br/>".$_SERVER['HTTP_HOST'];*/


        $is_mobile = $this->agent->is_mobile();

        //test ipad user agent

        $is_mobile_ipad = $this->agent->is_mobile('ipad');

        $data['is_mobile_ipad'] = $is_mobile_ipad;

        $is_robot = $this->agent->is_robot();

        $is_browser = $this->agent->is_browser();

        $is_platform = $this->agent->platform();

        $data['is_mobile'] = $is_mobile;

        $data['is_robot'] = $is_robot;

        $data['is_browser'] = $is_browser;

        $data['is_platform'] = $is_platform;


        if (isset($_POST["from_mobile_search_page_partner"])) {

            $data['from_mobile_search_page_partner'] = $_POST["from_mobile_search_page_partner"];

            $this->session->set_userdata('from_mobile_search_page_partner', $_POST["from_mobile_search_page_partner"]);

        }

        $from_mobile_search_page_partner = $this->session->userdata('from_mobile_search_page_partner');

        if (isset($from_mobile_search_page_partner)) $data['from_mobile_search_page_partner'] = $from_mobile_search_page_partner;


        if ($this->ion_auth->logged_in()) {

            $user_ion_auth = $this->ion_auth->user()->row();

            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

            if ($iduser == null || $iduser == 0 || $iduser == "") {

                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

            }

            $data['IdUser'] = $iduser;

        }


        $data['oAgenda_alaune'] = $this->mdl_agenda->GetAllalaune();


        $toVille = $this->mdlville->GetAgendaVilles();//get ville list of agenda

        $data['toVille'] = $toVille;

        $toCategorie_principale = $this->mdl_agenda->GetAgendaCategorie();

        $data['toCategorie_principale'] = $toCategorie_principale;


        //echo "agenda";

        $this->load->view('agenda/home_main', $data);

    }


    function test_remove_all_old_data()

    {

        //$this->mdl_agenda->get_null_article_datetime();

        //$this->mdl_agenda->delete_null_agenda_datetime();

        echo "ok";

    }


    /*function test_datetime_completion() {

        $toresult = $this->mdl_agenda->GetAll();

        foreach ($toresult as $item) {

            echo $item->id."<br/>";

            $datetime = array();

            if ((isset($item->date_debut) && $item->date_debut != "" && $item->date_debut != "0000-00-00" && $item->date_debut != null) || (isset($item->date_fin) && $item->date_fin != "" && $item->date_fin != "0000-00-00" && $item->date_fin != null)) {

                $datetime['agenda_id'] = $item->id;

				if (isset($item->date_debut) && $item->date_debut == "0000-00-00") $item->date_debut = null;

                $datetime['date_debut'] = $item->date_debut;

                if (isset($item->date_fin) && $item->date_fin == "0000-00-00") $item->date_fin = null;

                $datetime['date_fin'] = $item->date_fin;

                $datetime['heure_debut'] = $item->heure_debut;

                $inserted_id_datetime = $this->mdl_agenda_datetime->insert($datetime);

            }

            $datetime_1 = array();

            if ((isset($item->date_debut_1) && $item->date_debut_1 != "" && $item->date_debut_1 != "0000-00-00" && $item->date_debut_1 != null) || (isset($item->date_fin_1) && $item->date_fin_1 != "" && $item->date_fin_1 != "0000-00-00" && $item->date_fin_1 != null)) {

                $datetime_1['agenda_id'] = $item->id;

				if (isset($item->date_debut_1) && $item->date_debut_1 == "0000-00-00") $item->date_debut_1 = null;

                $datetime_1['date_debut'] = $item->date_debut_1;

                if (isset($item->date_fin_1) && $item->date_fin_1 == "0000-00-00") $item->date_fin_1 = null;

                $datetime_1['date_fin'] = $item->date_fin_1;

                $datetime_1['heure_debut'] = $item->heure_debut_1;

                $inserted_id_datetime_1 = $this->mdl_agenda_datetime->insert($datetime_1);

            }

            $datetime_2 = array();

            if ((isset($item->date_debut_2) && $item->date_debut_2 != "" && $item->date_debut_2 != "0000-00-00" && $item->date_debut_2 != null) || (isset($item->date_fin_2) && $item->date_fin_2 != "" && $item->date_fin_2 != "0000-00-00" && $item->date_fin_2 != null)) {

                $datetime_2['agenda_id'] = $item->id;

				if (isset($item->date_debut_2) && $item->date_debut_2 == "0000-00-00") $item->date_debut_2 = null;

                $datetime_2['date_debut'] = $item->date_debut_2;

                if (isset($item->date_fin_2) && $item->date_fin_2 == "0000-00-00") $item->date_fin_2 = null;

                $datetime_2['date_fin'] = $item->date_fin_2;

                $datetime_2['heure_debut'] = $item->heure_debut_2;

                $inserted_id_datetime_2 = $this->mdl_agenda_datetime->insert($datetime_2);

            }

			$datetime_3 = array();

            if ((isset($item->date_debut_3) && $item->date_debut_3 != "" && $item->date_debut_3 != "0000-00-00" && $item->date_debut_3 != null) || (isset($item->date_fin_3) && $item->date_fin_3 != "" && $item->date_fin_3 != "0000-00-00" && $item->date_fin_3 != null)) {

                $datetime_3['agenda_id'] = $item->id;

				if (isset($item->date_debut_3) && $item->date_debut_3 == "0000-00-00") $item->date_debut_3 = null;

                $datetime_3['date_debut'] = $item->date_debut_3;

                if (isset($item->date_fin_3) && $item->date_fin_3 == "0000-00-00") $item->date_fin_3 = null;

                $datetime_3['date_fin'] = $item->date_fin_3;

                $datetime_3['heure_debut'] = $item->heure_debut_3;

                $inserted_id_datetime_3 = $this->mdl_agenda_datetime->insert($datetime_3);

            }

			$datetime_4 = array();

            if ((isset($item->date_debut_4) && $item->date_debut_4 != "" && $item->date_debut_4 != "0000-00-00" && $item->date_debut_4 != null)||(isset($item->date_fin_4) && $item->date_fin_4 != "" && $item->date_fin_4 != "0000-00-00" && $item->date_fin_4 != null))  {

                $datetime_4['agenda_id'] = $item->id;

				if (isset($item->date_debut_4) && $item->date_debut_4 == "0000-00-00") $item->date_debut_4 = null;

                $datetime_4['date_debut'] = $item->date_debut_4;

                if (isset($item->date_fin_4) && $item->date_fin_4 == "0000-00-00") $item->date_fin_4 = null;

                $datetime_4['date_fin'] = $item->date_fin_4;

                $datetime_4['heure_debut'] = $item->heure_debut_4;

                $inserted_id_datetime_4 = $this->mdl_agenda_datetime->insert($datetime_4);

            }

			$datetime_5 = array();

            if ((isset($item->date_debut_5) && $item->date_debut_5 != "" && $item->date_debut_5 != "0000-00-00" && $item->date_debut_5 != null)||(isset($item->date_fin_5) && $item->date_fin_5 != "" && $item->date_fin_5 != "0000-00-00" && $item->date_fin_5 != null)) {

                $datetime_5['agenda_id'] = $item->id;

				if (isset($item->date_debut_5) && $item->date_debut_5 == "0000-00-00") $item->date_debut_5 = null;

                $datetime_5['date_debut'] = $item->date_debut_5;

                if (isset($item->date_fin_5) && $item->date_fin_5 == "0000-00-00") $item->date_fin_5 = null;

                $datetime_5['date_fin'] = $item->date_fin_5;

                $datetime_5['heure_debut'] = $item->heure_debut_5;

                $inserted_id_datetime_5 = $this->mdl_agenda_datetime->insert($datetime_5);

            }

			$datetime_6 = array();

            if ((isset($item->date_debut_6) && $item->date_debut_6 != "" && $item->date_debut_6 != "0000-00-00" && $item->date_debut_6 != null)||(isset($item->date_fin_6) && $item->date_fin_6 != "" && $item->date_fin_6 != "0000-00-00" && $item->date_fin_6 != null)) {

                $datetime_6['agenda_id'] = $item->id;

				if (isset($item->date_debut_6) && $item->date_debut_6 == "0000-00-00") $item->date_debut_6 = null;

                $datetime_6['date_debut'] = $item->date_debut_6;

                if (isset($item->date_fin_6) && $item->date_fin_6 == "0000-00-00") $item->date_fin_6 = null;

                $datetime_6['date_fin'] = $item->date_fin_6;

                $datetime_6['heure_debut'] = $item->heure_debut_6;

                $inserted_id_datetime_6 = $this->mdl_agenda_datetime->insert($datetime_6);

            }

			$datetime_7 = array();

            if ((isset($item->date_debut_7) && $item->date_debut_7 != "" && $item->date_debut_7 != "0000-00-00" && $item->date_debut_7 != null)||(isset($item->date_fin_7) && $item->date_fin_7 != "" && $item->date_fin_7 != "0000-00-00" && $item->date_fin_7 != null)) {

                $datetime_7['agenda_id'] = $item->id;

				if (isset($item->date_debut_7) && $item->date_debut_7 == "0000-00-00") $item->date_debut_7 = null;

                $datetime_7['date_debut'] = $item->date_debut_7;

                if (isset($item->date_fin_7) && $item->date_fin_7 == "0000-00-00") $item->date_fin_7 = null;

                $datetime_7['date_fin'] = $item->date_fin_7;

                $datetime_7['heure_debut'] = $item->heure_debut_7;

                $inserted_id_datetime_7 = $this->mdl_agenda_datetime->insert($datetime_7);

            }

			$datetime_8 = array();

            if ((isset($item->date_debut_8) && $item->date_debut_8 != "" && $item->date_debut_8 != "0000-00-00" && $item->date_debut_8 != null)||(isset($item->date_fin_8) && $item->date_fin_8 != "" && $item->date_fin_8 != "0000-00-00" && $item->date_fin_8 != null)) {

                $datetime_8['agenda_id'] = $item->id;

				if (isset($item->date_debut_8) && $item->date_debut_8 == "0000-00-00") $item->date_debut_8 = null;

                $datetime_8['date_debut'] = $item->date_debut_8;

                if (isset($item->date_fin_8) && $item->date_fin_8 == "0000-00-00") $item->date_fin_8 = null;

                $datetime_8['date_fin'] = $item->date_fin_8;

                $datetime_8['heure_debut'] = $item->heure_debut_8;

                $inserted_id_datetime_8 = $this->mdl_agenda_datetime->insert($datetime_8);

            }

			$datetime_9 = array();

            if ((isset($item->date_debut_9) && $item->date_debut_9 != "" && $item->date_debut_9 != "0000-00-00" && $item->date_debut_9 != null)||(isset($item->date_fin_9) && $item->date_fin_9 != "" && $item->date_fin_9 != "0000-00-00" && $item->date_fin_9 != null)) {

                $datetime_9['agenda_id'] = $item->id;

				if (isset($item->date_debut_9) && $item->date_debut_9 == "0000-00-00") $item->date_debut_9 = null;

                $datetime_9['date_debut'] = $item->date_debut_9;

                if (isset($item->date_fin_9) && $item->date_fin_9 == "0000-00-00") $item->date_fin_9 = null;

                $datetime_9['date_fin'] = $item->date_fin_9;

                $datetime_9['heure_debut'] = $item->heure_debut_9;

                $inserted_id_datetime_9 = $this->mdl_agenda_datetime->insert($datetime_9);

            }

			$datetime_10 = array();

            if ((isset($item->date_debut_10) && $item->date_debut_10 != "" && $item->date_debut_10 != "0000-00-00" && $item->date_debut_10 != null)||(isset($item->date_fin_10) && $item->date_fin_10 != "" && $item->date_fin_10 != "0000-00-00" && $item->date_fin_10 != null)) {

                $datetime_10['agenda_id'] = $item->id;

				if (isset($item->date_debut_10) && $item->date_debut_10 == "0000-00-00") $item->date_debut_10 = null;

                $datetime_10['date_debut'] = $item->date_debut_10;

                if (isset($item->date_fin_10) && $item->date_fin_10 == "0000-00-00") $item->date_fin_10 = null;

                $datetime_10['date_fin'] = $item->date_fin_10;

                $datetime_10['heure_debut'] = $item->heure_debut_10;

                $inserted_id_datetime_10 = $this->mdl_agenda_datetime->insert($datetime_10);

            }

        }

    }*/


    function liste()

    {

        $data['infos'] = null;

        $this->mdl_agenda->deleteOldAgenda_fin8jours();

        $nom_url_commercant = $this->uri->rsegment(2);
        //var_dump($nom_url_commercant);

        if (isset($nom_url_commercant) && $nom_url_commercant != "" && $nom_url_commercant != "index" && $nom_url_commercant != "liste" && !is_numeric($nom_url_commercant)) {

            $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);

            $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);

            $data['oInfoCommercant'] = $oInfoCommercant;

            $data['mdlbonplan'] = $this->mdlbonplan;

            $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

            $oBonPlan = $this->mdlbonplan->bonPlanParCommercant($_iCommercantId);

            $data['nbBonPlan'] = sizeof($oBonPlan);

            $data['sTitreBonPlan'] = (sizeof($oBonPlan) > 0) ? $oBonPlan->bonplan_texte : "";

            $data['oImagespub'] = $this->mdlimagespub->GetByImagespubActiv();

            $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

            $data['photocom_default'] = $this->mdlarticle->get_photo1com($_iCommercantId);

            $oLastbonplanCom = $this->mdlbonplan->lastBonplanCom($_iCommercantId);

            if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;

        }


        $argOffset = (isset($_SESSION["argOffset"])) ? $_SESSION["argOffset"] : 0;


        //$argOffset = $_iPage ;

        if (preg_match("/admin/", $_SERVER["PHP_SELF"])) {

            redirect("admin/home");

        }
        else
            {


            $PerPage = 20;
            $data["iFavoris"] = "";

//            $iCategorieId_all0 = $this->input->post("inputStringHidden");
//            if (isset($iCategorieId_all0) && $iCategorieId_all0 != "" && $iCategorieId_all0 != NULL && $iCategorieId_all0 != '0') {
//                $char_to_find_categ = substr($iCategorieId_all0, 0, 1);
//                if ($char_to_find_categ == ",") $iCategorieId_all = substr($iCategorieId_all0, 1);
//                if (isset($iCategorieId_all) && $iCategorieId_all != "" && strpos($iCategorieId_all, ',') !== false)
//                    $iCategorieId = explode(',', $iCategorieId_all);
//                else $iCategorieId = $iCategorieId_all0;
//            } else {
//                $iCategorieId = '0';
//            }

//            $iSousCategorieId_all0 = $this->input->post("inputStringHidden_sub");
//            if (isset($iSousCategorieId_all0) && $iSousCategorieId_all0 != "" && $iSousCategorieId_all0 != NULL && $iSousCategorieId_all0 != '0') {
//                $char_to_find_categ = substr($iSousCategorieId_all0, 0, 1);
//                if ($char_to_find_categ == ",") $iSousCategorieId_all = substr($iSousCategorieId_all0, 1);
//                if (isset($iSousCategorieId_all) && $iSousCategorieId_all != "" && strpos($iSousCategorieId_all, ',') !== false)
//                    $iSousCategorieId = explode(',', $iSousCategorieId_all);
//                else $iSousCategorieId = $iSousCategorieId_all0;
//            } else {
//                $iSousCategorieId = '0';
//            }

            $iCategorieId = $this->input->post("inputagenda_categorie");
            if (!isset($iCategorieId)) $iCategorieId = 0;
            $iVilleId = $this->input->post("inputStringVilleHidden_partenaires");
            if (!isset($iVilleId)) $iVilleId = 0;

            $iDepartementId = $this->input->post("inputStringDepartementHidden_partenaires");
            if (!isset($iDepartementId)) $iDepartementId = 0;

            $iOrderBy = $this->input->post("inputStringOrderByHidden");
            if (!isset($iOrderBy)) $iOrderBy = 0;

            $zMotCle = $this->input->post("zMotCle");
            if (!isset($zMotCle)) $zMotCle = 0;

            $inputStringQuandHidden = $this->input->post("inputStringQuandHidden");
            if (!isset($inputStringQuandHidden)) $inputStringQuandHidden = 0;

            $inputStringDatedebutHidden = $this->input->post("inputStringDatedebutHidden");
            if (!isset($inputStringDatedebutHidden)) $inputStringDatedebutHidden = "0000-00-00";

            $inputStringDatefinHidden = $this->input->post("inputStringDatefinHidden");
            if (!isset($inputStringDatefinHidden)) $inputStringDatefinHidden = "0000-00-00";

            $inputIdCommercant = $this->input->post("inputIdCommercant");
            if (!isset($inputIdCommercant)) $inputIdCommercant = "0";

            $inputagenda_article_type_id = $this->input->post("inputagenda_article_type_id");


            $rsegment3 = $this->uri->rsegment(3);
            //var_dump($rsegment3);
            if (isset($rsegment3) && $rsegment3 != "" && !is_numeric($rsegment3)) {
                $inputIdCommercant = $rsegment3;
            }
            //var_dump($inputIdCommercant);die();

            $data['iCategorieId'] = $iCategorieId;
//            $data['iSousCategorieId'] = $iSousCategorieId;
            $data['iVilleId'] = $iVilleId;
            $data['iDepartementId'] = $iDepartementId;
            $data['zMotCle'] = $zMotCle;
            $data['iOrderBy'] = $iOrderBy;
            $data['inputStringQuandHidden'] = $inputStringQuandHidden;
            $data['inputStringDatedebutHidden'] = $inputStringDatedebutHidden;
            $data['inputStringDatefinHidden'] = $inputStringDatefinHidden;
            $data['IdCommercant'] = $inputIdCommercant;

            $TotalRows = count(
                $this->mdlarticle->listeArticleRecherche(
                    $iCategorieId,
                    $iVilleId,
                    $iDepartementId,
                    $zMotCle,
                    $data["iFavoris"],
                    0,
                    10000,
                    $iOrderBy,
                    $inputStringQuandHidden,
                    $inputStringDatedebutHidden,
                    $inputStringDatefinHidden,
                    $inputIdCommercant,
                    $inputagenda_article_type_id
                )
            );

            $config_pagination = array();
            $rsegment3 = $this->uri->rsegment(3);

            if (strpos($rsegment3, '&content_only_list') !== false) {
                $pieces_rseg = explode("&content_only_list", $rsegment3);
                $rsegment3 = $pieces_rseg[0];
            }

            if (isset($rsegment3) && $rsegment3 != "" && is_numeric($rsegment3)) {
                $config_pagination["base_url"] = base_url() . "article/liste/" . $rsegment3;
            } else {
                $config_pagination["base_url"] = base_url() . "article/liste/";
            }
            $config_pagination["total_rows"] = $TotalRows;
            $config_pagination["per_page"] = $PerPage;
            $config_pagination["uri_segment"] = 3;
            $config_pagination['first_link'] = 'Première page';
            $config_pagination['last_link'] = 'Dernière page';
            $config_pagination['prev_link'] = 'Précédent';
            $config_pagination['next_link'] = 'Suivant';

            $this->pagination->initialize($config_pagination);
            if (isset($rsegment3) && $rsegment3 != "" && is_numeric($rsegment3)) {
                $page_pagination = ($rsegment3) ? $rsegment3 : 0;
            } else {
                $page_pagination = ($rsegment3) ? $rsegment3 : 0;
            }

            $data['tocommercant'] = $this->mdlarticle->listeArticleRecherche_commercant_list(
                $iCategorieId,
                $iVilleId,
                $iDepartementId,
                $zMotCle,
                $data["iFavoris"],
                $page_pagination,
                $config_pagination["per_page"],
                $iOrderBy,
                $inputStringQuandHidden,
                $inputStringDatedebutHidden,
                $inputStringDatefinHidden,
                $inputIdCommercant,
                $inputagenda_article_type_id
            );

            $toAgenda = $this->mdlarticle->listeArticleRecherche(
                $iCategorieId,
                $iVilleId,
                $iDepartementId,
                $zMotCle,
                $data["iFavoris"],
                $page_pagination,
                $config_pagination["per_page"],
                $iOrderBy,
                $inputStringQuandHidden,
                $inputStringDatedebutHidden,
                $inputStringDatefinHidden,
                $inputIdCommercant,
                $inputagenda_article_type_id
            );

            $data["links_pagination"] = $this->pagination->create_links();

            $iNombreLiens = $TotalRows / $PerPage;
            if ($iNombreLiens > round($iNombreLiens)) {
                $iNombreLiens = round($iNombreLiens) + 1;
            } else {
                $iNombreLiens = round($iNombreLiens);
            }

            $data["iNombreLiens"] = $iNombreLiens;
            $data["PerPage"] = $PerPage;
            $data["TotalRows"] = $TotalRows;
            $data["argOffset"] = $argOffset;
            $data['toAgenda'] = $toAgenda;//william hack
            $data['pagecategory'] = 'article';

            //get ville list of article***********************************************************************************************************
            $toVille = $this->mdlville->GetArticleVillesByIdCommercant_by_departement($inputIdCommercant, $iDepartementId);
            
            $data['toVille'] = $toVille;

            $toCategorie_principale = $this->mdlarticle->GetArticleCategorie_ByIdCommercant($inputIdCommercant);
            $data['toCategorie_principale'] = $toCategorie_principale;

            $toArticleTypeListe = $this->mdlarticle->getAllarticletype();
            $data['toArticleTypeListe'] = $toArticleTypeListe;

            $toDepartement = $this->mdldepartement->GetArticleDepartements_pvc();
            $data['toDepartement'] = $toDepartement;

            $data["mdl_localisation"] = $this->mdl_localisation;
            $data["mdlville"] = $this->mdlville;

            $is_mobile = $this->agent->is_mobile();
            //test ipad user agent
            $is_mobile_ipad = $this->agent->is_mobile('ipad');
            $data['is_mobile_ipad'] = $is_mobile_ipad;
            $is_robot = $this->agent->is_robot();
            $is_browser = $this->agent->is_browser();
            $is_platform = $this->agent->platform();
            $data['is_mobile'] = $is_mobile;
            $data['is_robot'] = $is_robot;
            $data['is_browser'] = $is_browser;
            $data['is_platform'] = $is_platform;

            if ($this->ion_auth->logged_in()) {
                $user_ion_auth = $this->ion_auth->user()->row();
                $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                if ($iduser == null || $iduser == 0 || $iduser == "") {
                    $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                }
                $data['IdUser'] = $iduser;
            }


//            $data['toArticleTout_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "0", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
//            $data['toArticleAujourdhui_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "101", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
//            $data['toArticleWeekend_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "202", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
//            $data['toArticleSemaine_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "303", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
//            $data['toArticleSemproch_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "404", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
//            $data['toArticleMois_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "505", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
//            $data['toArticleJanvier_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "01", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
//            $data['toArticleFevrier_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "02", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
//            $data['toArticleMars_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "03", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
//            $data['toArticleAvril_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "04", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
//            $data['toArticleMai_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "05", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
//            $data['toArticleJuin_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "06", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
//            $data['toArticleJuillet_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "07", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
//            $data['toArticleAout_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "08", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
//            $data['toArticleSept_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "09", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
//            $data['toArticleOct_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "10", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
//            $data['toArticleNov_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "11", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));
//            $data['toArticleDec_global'] = count($this->mdlarticle->listeArticleRecherche($session_iCategorieId_to_count, $session_iVilleId_to_count, $session_iDepartementId_to_count, $session_zMotCle_to_count, $session_iSousCategorieId_to_count, 0, 10000, $session_iOrderBy_to_count, "12", $session_inputStringDatedebutHidden_to_count, $session_inputStringDatefinHidden_to_count, $session_inputIdCommercant_to_count));

            $data['mdlbonplan'] = $this->mdlbonplan;
            $data['mdlannonce'] = $this->mdlannonce;
            $data['Commercant'] = $this->Commercant;
            $data['mdl_article_datetime'] = $this->mdl_article_datetime;

            //TO NOT REMOVE - for details agenda commercant
            $_iCommercantId = $inputIdCommercant;
            if (isset($_iCommercantId) && isset($oInfoCommercant)) {
                $data['toCategorie_principale'] = $this->mdlarticle->GetArticleCategorie_ByIdCommercant($_iCommercantId);
                $data['toArticleJanvier'] = $this->mdlarticle->GetArticleNbByMonth("01", $_iCommercantId);
                $data['toArticleFevrier'] = $this->mdlarticle->GetArticleNbByMonth("02", $_iCommercantId);
                $data['toArticleMars'] = $this->mdlarticle->GetArticleNbByMonth("03", $_iCommercantId);
                $data['toArticleAvril'] = $this->mdlarticle->GetArticleNbByMonth("04", $_iCommercantId);
                $data['toArticleMai'] = $this->mdlarticle->GetArticleNbByMonth("05", $_iCommercantId);
                $data['toArticleJuin'] = $this->mdlarticle->GetArticleNbByMonth("06", $_iCommercantId);
                $data['toArticleJuillet'] = $this->mdlarticle->GetArticleNbByMonth("07", $_iCommercantId);
                $data['toArticleAout'] = $this->mdlarticle->GetArticleNbByMonth("08", $_iCommercantId);
                $data['toArticleSept'] = $this->mdlarticle->GetArticleNbByMonth("09", $_iCommercantId);
                $data['toArticleOct'] = $this->mdlarticle->GetArticleNbByMonth("10", $_iCommercantId);
                $data['toArticleNov'] = $this->mdlarticle->GetArticleNbByMonth("11", $_iCommercantId);
                $data['toArticleDec'] = $this->mdlarticle->GetArticleNbByMonth("12", $_iCommercantId);

                $data['nombre_article_com'] = $this->mdlarticle->GetByIdCommercant($_iCommercantId);
                $data['pagecategory_partner'] = "list_agenda";
                //$this->load->view('agenda/partner_agenda_list', $data) ;
                //$this->load->view('privicarte/partner_agenda_list', $data);
                $this->load->view('sortez/partner_article_list', $data);
            } else {
                $this->session->set_userdata('nohome', '1');
                $data["main_menu_content"] = "article";
                $toDepartements = $this->mdlville->GetCommercantDepartement();
                $data['toDepartement'] = $toDepartements;

                if ($_SERVER['SERVER_NAME'] == DOMAIN_SORTEZ_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_SORTEZ_GLOBAL) {
                    if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
                        //$this->load->view('sortez_mobile/liste_article', $data);
                        $this->load->view('sortez_vsv/article_index', $data);
                    } else {
                        //$this->load->view('sortez/article', $data) ;
                        $this->load->view('sortez_vsv/article_index', $data);
                    }
                } elseif ($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL) {
                    $this->load->view("vivresaville/article_index", $data);
                }
            }
        }
    }

    public function check_commune_by_department()
    {
        $departement_id = $this->input->post("departement_id");
        if (!isset($departement_id) || $departement_id == "" || $departement_id == null) $departement_id="0";
        $toVille = $this->mdlville->getVilleListByDepartmenInArticle($departement_id);
        $data['toVille'] = $toVille;
        $this->load->view('sortez_vsv/view_commune_by_department_in_article', $data);
    }

    public function check_commercant_by_department()
    {
        $departement_id = $this->input->post("departement_id");
        if (!isset($departement_id) || $departement_id == "" || $departement_id == null) $departement_id="0";
        $toCommercant = $this->mdlville->getCommercantListByDepartmenInArticle($departement_id);
        foreach ($toCommercant as $oCommercant){
            ?>
            <a class="dropdown-item <?= $oCommercant->nbarticle ?>" id="partenaire_select_id_<?= $oCommercant->IdCommercant ?>" href="javascript:void(0);" onclick="
                                                               javascript:change_department_select(<?= $oCommercant->IdCommercant ?>);
                                                               javascript:change_commune_list(<?= $oCommercant->IdCommercant ?>);
                                                               javascript:change_commercant_select(<?= $oCommercant->IdCommercant ?>);
                                                               javascript:chenge_category_list(<?= $oCommercant->IdCommercant ?>);
                                                               javascript:change_subcategories_by_department_soutenons(<?= $oCommercant->IdCommercant ?>);
                                                               "><?= $oCommercant->NomSociete ?></a>
            <?php
        }
    }


    function check_Idcategory_of_subCategory()

    {

        $session_iCategorieId = $this->session->userdata('iSousCategorieId_x');

        if (isset($session_iCategorieId)) {

            $iCategorieId_sess = $session_iCategorieId;

            if (isset($iCategorieId_sess) && is_array($iCategorieId_sess)) {

                $all_subcategory = "";

                for ($i = 0; $i < count($iCategorieId_sess); $i++) {

                    $sousRubrique = $this->mdl_categories_agenda->getByIdSousCateg($iCategorieId_sess[$i]);

                    $all_subcategory .= "-" . $sousRubrique->agenda_categid;

                }

                echo substr($all_subcategory, 1);

                ////$this->firephp->log($all_subcategory, 'all_subcategory');

            } else {

                echo "0";

            }

        } else echo "0";

    }


    function check_Idcategory_of_Category()

    {

        $session_iCategorieId = $this->session->userdata('iSousCategorieId_x');

        if (isset($session_iCategorieId)) {

            $iCategorieId_sess = $session_iCategorieId;

            if (isset($iCategorieId_sess) && is_array($iCategorieId_sess)) {

                $all_subcategory = "";

                for ($i = 0; $i < count($iCategorieId_sess); $i++) {

                    $sousRubrique = $this->mdl_categories_agenda->getByIdSousCateg($iCategorieId_sess[$i]);

                    $all_subcategory .= "-" . $sousRubrique->agenda_categid;

                }

                echo substr($all_subcategory, 1);

                ////$this->firephp->log($all_subcategory, 'all_subcategory');

            } else {

                echo "0";

            }

        } else echo "0";

    }


    //this function is not the truth mon_agenda for particulier, this is a global search agenda


    function mon_agenda()

    {

        $data['infos'] = null;


        /*if ($this->ion_auth->is_admin()) {

            $this->session->set_flashdata('domain_from', '1');

            redirect("admin/home");

        }*/


        if (!$this->ion_auth->logged_in()) {

            redirect("auth/login");

        } else {

            $user_ion_auth = $this->ion_auth->user()->row();

            $user_ionauth_id = $user_ion_auth->id;

        }


        $argOffset = (isset($_SESSION["argOffset"])) ? $_SESSION["argOffset"] : 0;

        //$argOffset = $_iPage ;

        if (preg_match("/admin/", $_SERVER["PHP_SELF"])) {

            redirect("admin/home");

        } else {


            //if the link doesn't come from page navigation, clear catgory session


            if ($this->input->post("inputStringHidden") == "0") unset($_SESSION['iCategorieId']);

            if ($this->input->post("inputStringHidden_sub") == "0") unset($_SESSION['iSousCategorieId']);

            if ($this->input->post("inputStringVilleHidden") == "0") unset($_SESSION['iVilleId']);

            if ($this->input->post("zMotCle") == "") unset($_SESSION['zMotCle']);

            if ($this->input->post("inputStringOrderByHidden") == "0") unset($_SESSION['iOrderBy']);

            if ($this->input->post("inputStringQuandHidden")) unset($_SESSION['inputStringQuandHidden']);

            if ($this->input->post("inputStringDatedebutHidden")) unset($_SESSION['inputStringDatedebutHidden']);

            if ($this->input->post("inputStringDatefinHidden")) unset($_SESSION['inputStringDatefinHidden']);

            if ($this->input->post("inputIdCommercant")) unset($_SESSION['inputIdCommercant']);


            //$TotalRows = $this->mdlcommercantpagination->Compter();

            $PerPage = 10;

            $data["iFavoris"] = "";


            $toVille = $this->mdlville->GetAgendaVillesByIdUsers_ionauth($user_ionauth_id);//get ville list of agenda

            $data['toVille'] = $toVille;

            $toCategorie_principale = $this->mdl_agenda->GetAgendaCategorie_ByIdUsers_ionauth($user_ionauth_id);

            $data['toCategorie_principale'] = $toCategorie_principale;


            if (isset($_POST["inputStringHidden"])) {

                unset($_SESSION['iCategorieId']);

                unset($_SESSION['iSousCategorieId']);

                unset($_SESSION['iVilleId']);

                unset($_SESSION['zMotCle']);

                unset($_SESSION['iOrderBy']);

                unset($_SESSION['inputStringQuandHidden']);

                unset($_SESSION['inputStringDatedebutHidden']);

                unset($_SESSION['inputStringDatefinHidden']);

                unset($_SESSION['inputIdCommercant']);


                //$iCategorieId = $_POST["inputStringHidden"] ;

                $iCategorieId_all0 = $this->input->post("inputStringHidden");

                if (isset($iCategorieId_all0) && $iCategorieId_all0 != "" && $iCategorieId_all0 != NULL && $iCategorieId_all0 != '0') {

                    $iCategorieId_all = substr($iCategorieId_all0, 1);

                    $iCategorieId = explode(',', $iCategorieId_all);

                } else {

                    $iCategorieId = '0';

                }

                //////$this->firephp->log($iCategorieId_all0, 'iCategorieId_all0');

                //////$this->firephp->log($iCategorieId_all, 'iCategorieId_all');

                //////$this->firephp->log($iCategorieId, 'iCategorieId');


                //$iCategorieId = $_POST["inputStringHidden_sub"] ;

                $iSousCategorieId_all0 = $this->input->post("inputStringHidden_sub");

                if (isset($iSousCategorieId_all0) && $iSousCategorieId_all0 != "" && $iSousCategorieId_all0 != NULL && $iSousCategorieId_all0 != '0') {

                    $iSousCategorieId_all = substr($iSousCategorieId_all0, 1);

                    $iSousCategorieId = explode(',', $iSousCategorieId_all);

                } else {

                    $iSousCategorieId = '0';

                }


                if (isset($_POST["inputStringVilleHidden"])) $iVilleId = $_POST["inputStringVilleHidden"]; else $iVilleId = "0";

                if (isset($_POST["inputStringVilleHidden_sub"])) $iOrderBy = $_POST["inputStringVilleHidden_sub"]; else $iOrderBy = "";

                if (isset($_POST["inputStringOrderByHidden"])) $iOrderBy = $_POST["inputStringOrderByHidden"]; else $iOrderBy = "";

                if (isset($_POST["zMotCle"])) $zMotCle = $_POST["zMotCle"]; else $zMotCle = '';

                if (isset($_POST["inputStringQuandHidden"])) $inputStringQuandHidden = $_POST["inputStringQuandHidden"]; else $inputStringQuandHidden = "0";

                if (isset($_POST["inputStringDatedebutHidden"])) $inputStringDatedebutHidden = $_POST["inputStringDatedebutHidden"]; else $inputStringDatedebutHidden = "0000-00-00";

                if (isset($_POST["inputStringDatefinHidden"])) $inputStringDatefinHidden = $_POST["inputStringDatefinHidden"]; else $inputStringDatefinHidden = "0000-00-00";

                if (isset($_POST["inputIdCommercant"])) $inputIdCommercant = $_POST["inputIdCommercant"]; else $inputIdCommercant = "0";


                $_SESSION['iCategorieId'] = $iCategorieId;

                $this->session->set_userdata('iCategorieId_x', $iCategorieId);

                $_SESSION['iSousCategorieId'] = $iSousCategorieId;

                $this->session->set_userdata('iSousCategorieId_x', $iSousCategorieId);

                $_SESSION['iVilleId'] = $iVilleId;

                $this->session->set_userdata('iVilleId_x', $iVilleId);

                $_SESSION['zMotCle'] = $zMotCle;

                $this->session->set_userdata('zMotCle_x', $zMotCle);

                $_SESSION['iOrderBy'] = $iOrderBy;

                $this->session->set_userdata('iOrderBy_x', $iOrderBy);

                $_SESSION['inputStringQuandHidden'] = $inputStringQuandHidden;

                $this->session->set_userdata('inputStringQuandHidden_x', $inputStringQuandHidden);

                $_SESSION['inputStringDatedebutHidden'] = $inputStringDatedebutHidden;

                $this->session->set_userdata('inputStringDatedebutHidden_x', $inputStringDatedebutHidden);

                $_SESSION['inputStringDatefinHidden'] = $inputStringDatefinHidden;

                $this->session->set_userdata('inputStringDatefinHidden_x', $inputStringDatefinHidden);

                $_SESSION['inputIdCommercant'] = $inputIdCommercant;

                $this->session->set_userdata('inputIdCommercant_x', $inputIdCommercant);


                $data['iCategorieId'] = $iCategorieId;

                $data['iSousCategorieId'] = $iSousCategorieId;

                $data['iVilleId'] = $iVilleId;

                $data['zMotCle'] = $zMotCle;

                $data['iOrderBy'] = $iOrderBy;

                $data['inputStringQuandHidden'] = $inputStringQuandHidden;

                $data['inputStringDatedebutHidden'] = $inputStringDatedebutHidden;

                $data['inputStringDatefinHidden'] = $inputStringDatefinHidden;

                $data['IdCommercant'] = $inputIdCommercant;


                $session_iCategorieId = $this->session->userdata('iCategorieId_x');

                $session_iSousCategorieId = $this->session->userdata('iSousCategorieId_x');

                $session_iVilleId = $this->session->userdata('iVilleId_x');

                $session_zMotCle = $this->session->userdata('zMotCle_x');

                $session_iOrderBy = $this->session->userdata('iOrderBy_x');

                $session_inputStringQuandHidden = $this->session->userdata('inputStringQuandHidden_x');

                $session_inputStringDatedebutHidden = $this->session->userdata('inputStringDatedebutHidden_x');

                $session_inputStringDatefinHidden = $this->session->userdata('inputStringDatefinHidden_x');

                $session_inputIdCommercant = $this->session->userdata('inputIdCommercant_x');


                ////$this->firephp->log($inputStringQuandHidden, 'inputStringQuandHidden');

                ////$this->firephp->log($session_inputStringQuandHidden, 'session_inputStringQuandHidden');


                //$toAgenda = $this->mdl_agenda->listeAgendaRecherche($_SESSION['iCategorieId'], $_SESSION['iVilleId'], $_SESSION['zMotCle'], $data["iFavoris"], $argOffset, $PerPage, $_SESSION['iOrderBy'], $_SESSION['inputStringQuandHidden'], $_SESSION['inputStringDatedebutHidden'], $_SESSION['inputStringDatefinHidden']) ;

                $TotalRows = count($this->mdl_agenda->listeAgendaRecherche($session_iCategorieId, $session_iVilleId, $session_zMotCle, $session_iSousCategorieId, 0, 10000, $session_iOrderBy, $session_inputStringQuandHidden, $session_inputStringDatedebutHidden, $session_inputStringDatefinHidden, $session_inputIdCommercant, $user_ionauth_id));


                $config_pagination = array();

                $config_pagination["base_url"] = base_url() . "article/liste/" . $user_ionauth_id;

                $config_pagination["total_rows"] = $TotalRows;

                $config_pagination["per_page"] = $PerPage;

                $config_pagination["uri_segment"] = 4;

                $config_pagination['first_link'] = '<<<';

                $config_pagination['last_link'] = '>>>';

                $this->pagination->initialize($config_pagination);

                $page_pagination = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

                //$toCommercant = $this->mdl_agenda->listeAgendaRecherche($session_iCategorieId, $_SESSION['iVilleId'], $_SESSION['zMotCle'], $data["iFavoris"], $page_pagination, $config_pagination["per_page"], $_SESSION['iOrderBy'], $_SESSION['inputStringQuandHidden'], $_SESSION['inputStringDatedebutHidden'], $_SESSION['inputStringDatefinHidden']) ;

                $toAgenda = $this->mdl_agenda->listeAgendaRecherche($session_iCategorieId, $session_iVilleId, $session_zMotCle, $session_iSousCategorieId, $page_pagination, $config_pagination["per_page"], $session_iOrderBy, $session_inputStringQuandHidden, $session_inputStringDatedebutHidden, $session_inputStringDatefinHidden, $session_inputIdCommercant, $user_ionauth_id);

                $data["links_pagination"] = $this->pagination->create_links();


            } else {

                $data["iFavoris"] = "";

                $session_iCategorieId = $this->session->userdata('iCategorieId_x');

                $session_iSousCategorieId = $this->session->userdata('iSousCategorieId_x');

                $session_iVilleId = $this->session->userdata('iVilleId_x');

                $session_zMotCle = $this->session->userdata('zMotCle_x');

                $session_iOrderBy = $this->session->userdata('iOrderBy_x');

                $session_inputStringQuandHidden = $this->session->userdata('inputStringQuandHidden_x');

                $session_inputStringDatedebutHidden = $this->session->userdata('inputStringDatedebutHidden_x');

                $session_inputStringDatefinHidden = $this->session->userdata('inputStringDatefinHidden_x');

                $session_inputIdCommercant = $this->session->userdata('inputIdCommercant_x');


                $iCategorieId = (isset($session_iCategorieId)) ? $session_iCategorieId : 0;

                $iSousCategorieId = (isset($session_iSousCategorieId)) ? $session_iSousCategorieId : 0;

                $iVilleId = (isset($session_iVilleId)) ? $session_iVilleId : "0";

                $zMotCle = (isset($session_zMotCle)) ? $session_zMotCle : "";

                $iOrderBy = (isset($session_iOrderBy)) ? $session_iOrderBy : "0";

                $inputStringQuandHidden = (isset($session_inputStringQuandHidden)) ? $session_inputStringQuandHidden : "0";

                $inputStringDatedebutHidden = (isset($session_inputStringDatedebutHidden)) ? $session_inputStringDatedebutHidden : "0000-00-00";

                $inputStringDatefinHidden = (isset($session_inputStringDatefinHidden)) ? $session_inputStringDatefinHidden : "0000-00-00";

                $inputIdCommercant = (isset($session_inputIdCommercant)) ? $session_inputIdCommercant : "0";


                //$toAgenda = $this->mdl_agenda->listeAgendaRecherche($iCategorieId, $iVilleId, $zMotCle, $data["iFavoris"], $argOffset, $PerPage, $iOrderBy, $inputStringQuandHidden, $inputStringDatedebutHidden, $inputStringDatefinHidden) ;

                $TotalRows = count($this->mdl_agenda->listeAgendaRecherche($iCategorieId, $iVilleId, $zMotCle, $iSousCategorieId, 0, 10000, $iOrderBy, $inputStringQuandHidden, $inputStringDatedebutHidden, $inputStringDatefinHidden, $inputIdCommercant, $user_ionauth_id));


                $config_pagination = array();

                $config_pagination["base_url"] = base_url() . "article/liste/" . $user_ionauth_id;

                $config_pagination["total_rows"] = $TotalRows;

                $config_pagination["per_page"] = $PerPage;

                $config_pagination["uri_segment"] = 4;

                $config_pagination['first_link'] = '<<<';

                $config_pagination['last_link'] = '>>>';

                $this->pagination->initialize($config_pagination);

                $page_pagination = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

                $toAgenda = $this->mdl_agenda->listeAgendaRecherche($iCategorieId, $iVilleId, $zMotCle, $iSousCategorieId, $page_pagination, $config_pagination["per_page"], $iOrderBy, $inputStringQuandHidden, $inputStringDatedebutHidden, $inputStringDatefinHidden, $inputIdCommercant, $user_ionauth_id);

                $data["links_pagination"] = $this->pagination->create_links();

            }


            ////$this->firephp->log($_SERVER['REQUEST_URI'], 'PATH_INFO');


            $iNombreLiens = $TotalRows / $PerPage;

            if ($iNombreLiens > round($iNombreLiens)) {

                $iNombreLiens = round($iNombreLiens) + 1;

            } else {

                $iNombreLiens = round($iNombreLiens);

            }

            //////////////////////////////////


            $data["iNombreLiens"] = $iNombreLiens;


            $data["PerPage"] = $PerPage;

            $data["TotalRows"] = $TotalRows;

            $data["argOffset"] = $argOffset;

            //$toCommercant= $this->mdlcommercantpagination->GetListeCommercantPagination($PerPage, $argOffset);

            $data['toAgenda'] = $toAgenda;

            // print_r($data['toCommercant']);exit();

            //$this->load->view('front/vwAccueil', $data) ;

            $data['pagecategory'] = 'article';


            $is_mobile = $this->agent->is_mobile();

            //test ipad user agent

            $is_mobile_ipad = $this->agent->is_mobile('ipad');

            $data['is_mobile_ipad'] = $is_mobile_ipad;

            $is_robot = $this->agent->is_robot();

            $is_browser = $this->agent->is_browser();

            $is_platform = $this->agent->platform();

            $data['is_mobile'] = $is_mobile;

            $data['is_robot'] = $is_robot;

            $data['is_browser'] = $is_browser;

            $data['is_platform'] = $is_platform;


            if ($this->ion_auth->logged_in()) {

                $user_ion_auth = $this->ion_auth->user()->row();

                $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

                if ($iduser == null || $iduser == 0 || $iduser == "") {

                    $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

                }

                $data['IdUser'] = $iduser;

            }


            $this->load->view('agendaAout2013/mon_agenda', $data);


        }


    }


    function preview_article($id_agenda)

    {


        if (!$this->ion_auth->logged_in()) {

            redirect('front/utilisateur/no_permission');

        }


        $data['current_page'] = "details_article";

        $toVille = $this->mdlville->GetArticleVilles_pvc();//get article list of agenda

        $data['toVille'] = $toVille;

        $toCategorie_principale = $this->mdlarticle->GetArticleCategorie();

        $data['toCategorie_principale'] = $toCategorie_principale;

        //var_dump($toCategorie_principale);


        $id_agenda = $this->uri->rsegment(3);


        $oDetailAgenda = $this->mdlarticle->GetById_preview($id_agenda);

        $data['oDetailAgenda'] = $oDetailAgenda;


        //increment "accesscount" on agenda table

        $this->mdlarticle->Increment_accesscount($oDetailAgenda->id);


        //send info commercant to view

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($oDetailAgenda->IdCommercant);

        $data['oInfoCommercant'] = $oInfoCommercant;


        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);

        if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;

        if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;

        $data['group_id_commercant_user'] = $group_id_commercant_user;


        //sending mail to event organiser **************************

        if (isset($_POST['text_mail_form_module_detailbonnplan'])) {

            $text_mail_form_module_detailbonnplan = $this->input->post("text_mail_form_module_detailbonnplan");

            $nom_mail_form_module_detailbonnplan = $this->input->post("nom_mail_form_module_detailbonnplan");

            $tel_mail_form_module_detailbonnplan = $this->input->post("tel_mail_form_module_detailbonnplan");

            $email_mail_form_module_detailbonnplan = $this->input->post("email_mail_form_module_detailbonnplan");


            $colDestAdmin = array();

            $colDestAdmin[] = array("Email" => $oDetailAgenda->email, "Name" => $oDetailAgenda->nom_manifestation . " " . $oDetailAgenda->nom_societe);


            // Sujet

            $txtSujetAdmin = "Demande d'information sur un évennement sur Agenda/Club";


            $txtContenuAdmin = "

            <p>Bonjour ,</p>

            <p>Une demande d'information vous est adressé suite à un évennement que vous avez déposé sur le site Agenda.</p>

            <p>Détails :<br/>

            Evennement : " . $oDetailAgenda->nom_manifestation . "

            <br/>

            Date : " . translate_date_to_fr($oDetailAgenda->date_debut) . "<br/>

            Lieu : " . $oDetailAgenda->ville . " " . $oDetailAgenda->adresse_localisation . " " . $oDetailAgenda->codepostal_localisation . "<br/>

            Organisateur : " . $oDetailAgenda->organisateur . "<br/><br/>

            Demande Client :<br/>

            " . $text_mail_form_module_detailbonnplan . "<br/><br/>

            Nom client : " . $nom_mail_form_module_detailbonnplan . "<br/>

            Tel client : " . $tel_mail_form_module_detailbonnplan . "<br/>

            Email client : " . $email_mail_form_module_detailbonnplan . "<br/>

            </p>";


            @envoi_notification($colDestAdmin, $txtSujetAdmin, $txtContenuAdmin);

            $data['mssg_envoi_module_detail_bonplan'] = '<font color="#00CC00">Votre demande est envoyée</font>';

            //$data['mssg_envoi_module_detail_bonplan'] = $txtContenuAdmin;


        } else $data['mssg_envoi_module_detail_bonplan'] = '';

        //sending mail to event organiser *******************************


        $is_mobile = $this->agent->is_mobile();

        //test ipad user agent

        $is_mobile_ipad = $this->agent->is_mobile('ipad');

        $data['is_mobile_ipad'] = $is_mobile_ipad;

        $is_robot = $this->agent->is_robot();

        $is_browser = $this->agent->is_browser();

        $is_platform = $this->agent->platform();

        $data['is_mobile'] = $is_mobile;

        $data['is_robot'] = $is_robot;

        $data['is_browser'] = $is_browser;

        $data['is_platform'] = $is_platform;


        $toDepartement = $this->mdldepartement->GetArticleDepartements_pvc();

        $data['toDepartement'] = $toDepartement;


        $data["pagecategory"] = "article";

        $data["main_menu_content"] = "article";

        $data["mdlbonplan"] = $this->mdlbonplan;


        $data["pagecategory_partner"] = "article_partner";


        if (isset($oDetailAgenda->id)) {

            if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {

                //$this->load->view('privicarte_mobile/agenda_details_desk', $data);

                $this->load->view('sortez_mobile/article_details_desk', $data);

            } else {

                //$this->load->view('privicarte/details_event', $data);

                $this->load->view('sortez/details_article', $data);

            }

        } else {

            redirect('front/utilisateur/no_permission');

        }


    }


    function details($id_agenda)

    {


        $data['current_page'] = "details_article";

        $toVille = $this->mdlville->GetArticleVilles_pvc();//get article list of agenda

        $data['toVille'] = $toVille;

        $toCategorie_principale = $this->mdlarticle->GetArticleCategorie();

        $data['toCategorie_principale'] = $toCategorie_principale;

        //var_dump($toCategorie_principale);


        $id_agenda = $this->uri->rsegment(3);


        $oDetailAgenda = $this->mdlarticle->GetById_IsActif($id_agenda);

        if (!empty($oDetailAgenda)) {

            $data['oDetailAgenda'] = $oDetailAgenda;


            /*send metadata for opengraph fb & twitter*/

            if (!empty($oDetailAgenda)) {

                $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oDetailAgenda->IdCommercant);

                if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;

                if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;

                $photoCommercant_path = "application/resources/front/photoCommercant/imagesbank/" . $user_ion_auth_id . "/";

                $photoCommercant_path_old = "application/resources/front/images/article/photoCommercant/";


                $image_home_vignette = "";

                if (isset($oDetailAgenda->photo1) && $oDetailAgenda->photo1 != "" && is_file($photoCommercant_path . $oDetailAgenda->photo1) == true) {

                    $image_home_vignette = $oDetailAgenda->photo1;

                } else if ($image_home_vignette == "" && isset($oDetailAgenda->photo2) && $oDetailAgenda->photo2 != "" && is_file($photoCommercant_path . $oDetailAgenda->photo2) == true) {

                    $image_home_vignette = $oDetailAgenda->photo2;

                } else if ($image_home_vignette == "" && isset($oDetailAgenda->photo3) && $oDetailAgenda->photo3 != "" && is_file($photoCommercant_path . $oDetailAgenda->photo3) == true) {

                    $image_home_vignette = $oDetailAgenda->photo3;

                } else if ($image_home_vignette == "" && isset($oDetailAgenda->photo4) && $oDetailAgenda->photo4 != "" && is_file($photoCommercant_path . $oDetailAgenda->photo4) == true) {

                    $image_home_vignette = $oDetailAgenda->photo4;

                } else if ($image_home_vignette == "" && isset($oDetailAgenda->photo5) && $oDetailAgenda->photo5 != "" && is_file($photoCommercant_path . $oDetailAgenda->photo5) == true) {

                    $image_home_vignette = $oDetailAgenda->photo5;

                } else if ($image_home_vignette == "" && isset($oDetailAgenda->photo1) && $oDetailAgenda->photo1 != "" && is_file($photoCommercant_path_old . $oDetailAgenda->photo1) == true) {

                    $image_home_vignette = $oDetailAgenda->photo1;

                } else if ($image_home_vignette == "" && isset($oDetailAgenda->photo2) && $oDetailAgenda->photo2 != "" && is_file($photoCommercant_path_old . $oDetailAgenda->photo2) == true) {

                    $image_home_vignette = $oDetailAgenda->photo2;

                } else if ($image_home_vignette == "" && isset($oDetailAgenda->photo3) && $oDetailAgenda->photo3 != "" && is_file($photoCommercant_path_old . $oDetailAgenda->photo3) == true) {

                    $image_home_vignette = $oDetailAgenda->photo3;

                } else if ($image_home_vignette == "" && isset($oDetailAgenda->photo4) && $oDetailAgenda->photo4 != "" && is_file($photoCommercant_path_old . $oDetailAgenda->photo4) == true) {

                    $image_home_vignette = $oDetailAgenda->photo4;

                } else if ($image_home_vignette == "" && isset($oDetailAgenda->photo5) && $oDetailAgenda->photo5 != "" && is_file($photoCommercant_path_old . $oDetailAgenda->photo5) == true) {

                    $image_home_vignette = $oDetailAgenda->photo5;

                }

                $this->load->model("mdl_categories_agenda");

                $toCateg_for_agenda = $this->mdl_categories_agenda->getById($oDetailAgenda->article_categid);

                if ($image_home_vignette == "" && isset($toCateg_for_agenda->images) && $toCateg_for_agenda->images != "" && is_file("application/resources/front/images/agenda/category/" . $toCateg_for_agenda->images) == true) {

                    $image_home_vignette = GetImagePath("front/") . '/agenda/category/' . $toCateg_for_agenda->images;

                } else {

                    if ($image_home_vignette != "") {

                        if (isset($image_home_vignette) && $image_home_vignette != "" && is_file($photoCommercant_path . $image_home_vignette) == true)

                            $image_home_vignette = base_url() . $photoCommercant_path . $image_home_vignette;

                        else $image_home_vignette = base_url() . $photoCommercant_path_old . $image_home_vignette;

                    }

                }

                $data['zMetaImage'] = $image_home_vignette;

            }


            //increment "accesscount" on agenda table

            $this->mdlarticle->Increment_accesscount($oDetailAgenda->id);


            //send info commercant to view

            $oInfoCommercant = $this->mdlcommercant->infoCommercant($oDetailAgenda->IdCommercant);

            $data['oInfoCommercant'] = $oInfoCommercant;


            $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);

            if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;

            if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;

            $data['group_id_commercant_user'] = $group_id_commercant_user;


            //sending mail to event organiser **************************

            if (isset($_POST['text_mail_form_module_detailbonnplan'])) {

                $text_mail_form_module_detailbonnplan = $this->input->post("text_mail_form_module_detailbonnplan");

                $nom_mail_form_module_detailbonnplan = $this->input->post("nom_mail_form_module_detailbonnplan");

                $tel_mail_form_module_detailbonnplan = $this->input->post("tel_mail_form_module_detailbonnplan");

                $email_mail_form_module_detailbonnplan = $this->input->post("email_mail_form_module_detailbonnplan");


                $colDestAdmin = array();

                $colDestAdmin[] = array("Email" => $oDetailAgenda->email, "Name" => $oDetailAgenda->nom_manifestation . " " . $oDetailAgenda->nom_societe);


                // Sujet

                $txtSujetAdmin = "Demande d'information sur un évennement sur Agenda/Club";


                $txtContenuAdmin = "

            <p>Bonjour ,</p>

            <p>Une demande d'information vous est adressé suite à un évennement que vous avez déposé sur le site Agenda.</p>

            <p>Détails :<br/>

            Evennement : " . $oDetailAgenda->nom_manifestation . "

            <br/>

            Date : " . translate_date_to_fr($oDetailAgenda->date_debut) . "<br/>

            Lieu : " . $oDetailAgenda->ville . " " . $oDetailAgenda->adresse_localisation . " " . $oDetailAgenda->codepostal_localisation . "<br/>

            Organisateur : " . $oDetailAgenda->organisateur . "<br/><br/>

            Demande Client :<br/>

            " . $text_mail_form_module_detailbonnplan . "<br/><br/>

            Nom client : " . $nom_mail_form_module_detailbonnplan . "<br/>

            Tel client : " . $tel_mail_form_module_detailbonnplan . "<br/>

            Email client : " . $email_mail_form_module_detailbonnplan . "<br/>

            </p>";


                @envoi_notification($colDestAdmin, $txtSujetAdmin, $txtContenuAdmin);

                $data['mssg_envoi_module_detail_bonplan'] = '<font color="#00CC00">Votre demande est envoyée</font>';

                //$data['mssg_envoi_module_detail_bonplan'] = $txtContenuAdmin;


            } else $data['mssg_envoi_module_detail_bonplan'] = '';

            //sending mail to event organiser *******************************


            $is_mobile = $this->agent->is_mobile();

            //test ipad user agent

            $is_mobile_ipad = $this->agent->is_mobile('ipad');

            $data['is_mobile_ipad'] = $is_mobile_ipad;

            $is_robot = $this->agent->is_robot();

            $is_browser = $this->agent->is_browser();

            $is_platform = $this->agent->platform();

            $data['is_mobile'] = $is_mobile;

            $data['is_robot'] = $is_robot;

            $data['is_browser'] = $is_browser;

            $data['is_platform'] = $is_platform;


            $toDepartement = $this->mdldepartement->GetArticleDepartements_pvc();

            $data['toDepartement'] = $toDepartement;


            $data["pagecategory"] = "article";

            $data["main_menu_content"] = "article";

            $data["mdlbonplan"] = $this->mdlbonplan;


            $data["mdl_localisation"] = $this->mdl_localisation;

            $data["mdlville"] = $this->mdlville;

            $data["mdl_article_organiser"] = $this->mdl_article_organiser;


            $data["pagecategory_partner"] = "article_partner";


            $data['toArticle_datetime'] = $this->mdl_article_datetime->getByArticleId($id_agenda);


            if (isset($oDetailAgenda->id)) {

                if ($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL) {

                    $this->load->view('vivresaville/article_details', $data);

                } else {

                    if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {

                        //$this->load->view('privicarte_mobile/agenda_details_desk', $data);

                        //$this->load->view('sortez_mobile/article_details_desk', $data);

                        $this->load->view('sortez_vsv/article_details', $data);

                    } else {

                        //$this->load->view('privicarte/details_event', $data);

                        //$this->load->view('sortez/details_article', $data);

                        $this->load->view('sortez_vsv/article_details', $data);

                    }

                }


            } else {

                redirect('front/utilisateur/no_permission');

            }

        }


    }


    function article_perso_details($id_agenda)

    {


        $data['current_page'] = "details_event";

        $toVille = $this->mdlville->GetArticleVilles_pvc();//get ville list of article

        $data['toVille'] = $toVille;

        $toCategorie_principale = $this->mdlarticle->GetArticleCategorie();

        $data['toCategorie_principale'] = $toCategorie_principale;

        //var_dump($toCategorie_principale);


        $data['zCouleurBgBouton'] = $this->input->get("zCouleurBgBouton");

        $data['zCouleurNbBtn'] = $this->input->get("zCouleurNbBtn");

        $data['zCouleurTextBouton'] = $this->input->get("zCouleurTextBouton");

        $data['zCouleur'] = $this->input->get("zCouleur");

        $data['zCouleurTitre'] = $this->input->get("zCouleurTitre");


        $id_agenda = $this->uri->rsegment(3); //die($id_agenda);

        if (isset($id_agenda)) $id_agenda = (intval($id_agenda));

        else $id_agenda = 0;


        $oDetailAgenda = $this->mdlarticle->GetById_IsActif($id_agenda);

        $data['oDetailAgenda'] = $oDetailAgenda;//var_dump($oDetailAgenda); die();


        if (isset($oDetailAgenda->id)) {


            //increment "accesscount" on agenda table

            $this->mdlarticle->Increment_accesscount($oDetailAgenda->id);


            //send info commercant to view

            $oInfoCommercant = $this->mdlcommercant->infoCommercant($oDetailAgenda->IdCommercant);

            $data['oInfoCommercant'] = $oInfoCommercant;


            $toDepartement = $this->mdldepartement->GetAgendaDepartements_pvc();

            $data['toDepartement'] = $toDepartement;


            $data["pagecategory"] = "agenda";

            $data["main_menu_content"] = "agenda";

            $data["mdlbonplan"] = $this->mdlbonplan;


            $data["mdl_localisation"] = $this->mdl_localisation;

            $data["mdlville"] = $this->mdlville;

            $data["mdl_article_organiser"] = $this->mdl_article_organiser;


            $data["pagecategory_partner"] = "agenda_partner";


            $data['toArticle_datetime'] = $this->mdl_agenda_datetime->getByAgendaId($id_agenda);


            $data['currentpage'] = "export_article";


            //$this->load->view('privicarte/details_event', $data);

            $this->load->view('export/article_perso_details', $data);


        } else {

            redirect('article/article_perso');

        }


    }


    function details_event_contact($id_agenda, $contact_display = "contact")

    {


        $data['current_page'] = "details_event";

        $toVille = $this->mdlville->GetAgendaVilles_pvc();//get ville list of agenda

        $data['toVille'] = $toVille;

        $toCategorie_principale = $this->mdl_agenda->GetAgendaCategorie();

        $data['toCategorie_principale'] = $toCategorie_principale;

        //var_dump($toCategorie_principale);


        $id_agenda = $this->uri->rsegment(3);


        $oDetailAgenda = $this->mdl_agenda->GetById_IsActif($id_agenda);

        $data['oDetailAgenda'] = $oDetailAgenda;


        //increment "accesscount" on agenda table

        $this->mdl_agenda->Increment_accesscount($oDetailAgenda->id);


        //send info commercant to view

        $oInfoCommercant = $this->mdlcommercant->infoCommercant($oDetailAgenda->IdCommercant);

        $data['oInfoCommercant'] = $oInfoCommercant;


        $toDepartement = $this->mdldepartement->GetAgendaDepartements_pvc();

        $data['toDepartement'] = $toDepartement;


        $data["pagecategory"] = "article";

        $data["main_menu_content"] = "article";

        $data["mdlbonplan"] = $this->mdlbonplan;

        $data['contact_display'] = $contact_display;


        $this->load->view('privicarte/details_event_contact', $data);


    }


    function avantages_internautes()

    {

        $data['current_page'] = "avantages_internautes";


        $this->load->view('agenda/avantages_internautes', $data);

    }


    function check_category_list()
    {
        $inputStringQuandHidden_partenaires = $this->input->post("iQuand_sess");
        if (!isset($inputStringQuandHidden_partenaires) || $inputStringQuandHidden_partenaires == "" || $inputStringQuandHidden_partenaires == '0') {
            $inputStringQuandHidden_partenaires = "0";
        }
        $inputStringDatedebutHidden_partenaires = $this->input->post("iDatedebut_sess");
        if (!isset($inputStringDatedebutHidden_partenaires) || $inputStringDatedebutHidden_partenaires == "" || $inputStringDatedebutHidden_partenaires == '0' || $inputStringDatedebutHidden_partenaires == '0000-00-00') {
            $inputStringDatedebutHidden_partenaires = "0";
        }
        $inputStringDatefinHidden_partenaires = $this->input->post("iDatefin_sess");
        if (!isset($inputStringDatefinHidden_partenaires) || $inputStringDatefinHidden_partenaires == "" || $inputStringDatefinHidden_partenaires == '0' || $inputStringDatefinHidden_partenaires == '0000-00-00') {
            $inputStringDatefinHidden_partenaires = "0";
        }
        $inputStringVilleHidden_partenaires = $this->input->post("iVilleId_sess");
        if (!isset($inputStringVilleHidden_partenaires) || $inputStringVilleHidden_partenaires == "" || $inputStringVilleHidden_partenaires == '0') {
            $inputStringVilleHidden_partenaires = "0";
        }
        $inputStringDepartementHidden_partenaires = $this->input->post("iDepartementId_sess");
        if (!isset($inputStringDepartementHidden_partenaires) || $inputStringDepartementHidden_partenaires == "" || $inputStringDepartementHidden_partenaires == '0') {
            $inputStringDepartementHidden_partenaires = "0";
        }
        $inputStringIdCommercant = $this->input->post("inputIdCommercant");
        if (!isset($inputStringIdCommercant) || $inputStringIdCommercant == "" || $inputStringIdCommercant == '0') {
            $inputStringIdCommercant = "0";
        }

        $toCategorie_principale = $this->mdlarticle->GetArticleCategorie_by_params($inputStringQuandHidden_partenaires, $inputStringDatedebutHidden_partenaires, $inputStringDatefinHidden_partenaires, $inputStringDepartementHidden_partenaires, $inputStringVilleHidden_partenaires, $inputStringIdCommercant);

        $result_to_show = '';

        $session_iCategorieId = $this->session->userdata('iCategorieId_x');
        if (isset($session_iCategorieId)) {
            $iCategorieId_sess = $session_iCategorieId;
        }

        $ii_rand = 0;
        if (isset($toCategorie_principale)) {
            foreach ($toCategorie_principale as $oCategorie_principale) {
                if ($_SERVER['SERVER_NAME'] == DOMAIN_WWW_SORTEZ_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_SORTEZ_GLOBAL) {
                    if (isset($oCategorie_principale->nb_article) && $oCategorie_principale->nb_article != 0) {
                        $data['empty'] = null;
                        $data['inputStringIdCommercant'] = $inputStringIdCommercant;
                        //$data['oInfoCommercant'] = $oInfoCommercant;
                        $data['oCategorie_principale'] = $oCategorie_principale;
                        $data['ii_rand'] = $ii_rand;
                        if (isset($iCategorieId_sess)) $data['iCategorieId_sess'] = $iCategorieId_sess;
                        $data['inputStringQuandHidden_partenaires'] = $inputStringQuandHidden_partenaires;
                        $data['inputStringDatedebutHidden_partenaires'] = $inputStringDatedebutHidden_partenaires;
                        $data['inputStringDatefinHidden_partenaires'] = $inputStringDatefinHidden_partenaires;
                        $data['inputStringDepartementHidden_partenaires'] = $inputStringDepartementHidden_partenaires;
                        $data['inputStringVilleHidden_partenaires'] = $inputStringVilleHidden_partenaires;
                        $data['mdl_categories_article'] = $this->mdl_categories_article;
                        $data['session'] = $this->session;
                        $result_to_show .= $this->load->view('sortez_vsv/article_check_category', $data, TRUE);
                    }
                } elseif ($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL) {
                    if (isset($oCategorie_principale->nb_article) && $oCategorie_principale->nb_article != 0) {
                        $data['empty'] = null;
                        //$data['oInfoCommercant'] = $oInfoCommercant ;
                        $data['oCategorie_principale'] = $oCategorie_principale;
                        $data['ii_rand'] = $ii_rand;
                        $data['iCategorieId_sess'] = $iCategorieId_sess;
                        $data['inputStringQuandHidden_partenaires'] = $inputStringQuandHidden_partenaires;
                        $data['inputStringDatedebutHidden_partenaires'] = $inputStringDatedebutHidden_partenaires;
                        $data['inputStringDatefinHidden_partenaires'] = $inputStringDatefinHidden_partenaires;
                        $data['inputStringDepartementHidden_partenaires'] = $inputStringDepartementHidden_partenaires;
                        $data['inputStringVilleHidden_partenaires'] = $inputStringVilleHidden_partenaires;
                        $data['mdl_categories_article'] = $this->mdl_categories_article;
                        $data['session'] = $this->session;
                        $result_to_show .= $this->load->view('vivresaville/article_check_category', $data, TRUE);
                    }
                }
                $ii_rand++;
            }
        }
        ////$this->firephp->log($result_to_show, 'result_to_show');
        echo $result_to_show;
        //echo mb_convert_encoding($result_to_show, "UTF-8");
    }

    function check_category_list_in_article()
    {
        $departement_id = $this->input->post("departement_id");
        if (!isset($departement_id) || $departement_id == "" || $departement_id == '0') {
            $departement_id = "0";
        }
        $idCommercant = $this->input->post("idCommercant:");
        if (!isset($idCommercant) || $idCommercant == "" || $idCommercant == '0') {
            $idCommercant = "0";
        }
        $idVille = $this->input->post("idVille");
        if (!isset($idVille) || $idVille == "" || $idVille == '0') {
            $idVille = "0";
        }
        $agenda_article_type_id = $this->input->post("agenda_article_type_id");
        if (!isset($agenda_article_type_id) || $agenda_article_type_id == "" || $agenda_article_type_id == '0') {
            $agenda_article_type_id = "0";
        }


        $toCategorie_principale = $this->mdlarticle->getCategoryListInArticle($departement_id, $idVille, $idCommercant, $agenda_article_type_id);

        if (isset($toCategorie_principale) && count($toCategorie_principale)>0) {
            foreach ($toCategorie_principale as $oCategorie_principale) {
                echo '
                <a class="dropdown-item '.$oCategorie_principale->nbarticle.'" href="javascript:void(0);" onclick="javascript:pcv_select_category(67);">Tourisme, visite, excursion</a>
                ';
            }
        }

    }


    function article_partner_list($_iCommercantId)

    {


        //code standard utilisation page partenaire START **************************************************


        $nom_url_commercant = $this->uri->rsegment(3);


        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);


        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);


        $data['oInfoCommercant'] = $oInfoCommercant;


        $infocommande = $this->Mdl_soutenons->get_com_data_by_idcom(intval($_iCommercantId));
        $data['commande'] = $infocommande;


        //a basic account is not permit to show agenda list

        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);

        if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;

        if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;

        if ($group_id_commercant_user == 3) {

            redirect('front/utilisateur/no_permission');

        }


        //$data['mdlannonce'] = $this->mdlannonce ;

        $data['mdlbonplan'] = $this->mdlbonplan;


        $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

        $oBonPlan = $this->mdlbonplan->bonPlanParCommercant($_iCommercantId);

        $data['nbBonPlan'] = sizeof($oBonPlan);

        //		$data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_titre : "";		// OP 27/11/2011

        $data['sTitreBonPlan'] = (sizeof($oBonPlan) > 0) ? $oBonPlan->bonplan_texte : "";

        $data['oImagespub'] = $this->mdlimagespub->GetByImagespubActiv();

        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);


        $oLastbonplanCom = $this->mdlbonplan->lastBonplanCom($_iCommercantId);

        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;


        $data['active_link'] = "article";


        $is_mobile = $this->agent->is_mobile();

        //test ipad user agent

        $is_mobile_ipad = $this->agent->is_mobile('ipad');

        $data['is_mobile_ipad'] = $is_mobile_ipad;

        $is_robot = $this->agent->is_robot();

        $is_browser = $this->agent->is_browser();

        $is_platform = $this->agent->platform();

        $data['is_mobile'] = $is_mobile;

        $data['is_robot'] = $is_robot;

        $data['is_browser'] = $is_browser;

        $data['is_platform'] = $is_platform;


        $this->load->model("user");

        if ($this->ion_auth->logged_in()) {

            $user_ion_auth = $this->ion_auth->user()->row();

            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

            if ($iduser == null || $iduser == 0 || $iduser == "") {

                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

            }

        } else $iduser = 0;


        //code standard utilisation page partenaire END **************************************************


        if ($this->input->post("inputStringHidden") == "0") unset($_SESSION['iCategorieId']);

        if ($this->input->post("inputStringQuandHidden")) unset($_SESSION['inputStringQuandHidden']);


        $PerPage = 15;

        $data['PerPage'] = $PerPage;


        if (isset($_POST["inputStringHidden"])) {


            unset($_SESSION['iCategorieId']);

            unset($_SESSION['inputStringQuandHidden']);


            $iCategorieId = $this->input->post("inputStringHidden");

            if (isset($_POST["inputStringQuandHidden"]))

                $inputStringQuandHidden = $_POST["inputStringQuandHidden"];

            else $inputStringQuandHidden = "0";


            $_SESSION['iCategorieId'] = $iCategorieId;

            $this->session->set_userdata('iCategorieId_x', $iCategorieId);

            $_SESSION['inputStringQuandHidden'] = $inputStringQuandHidden;

            $this->session->set_userdata('inputStringQuandHidden_x', $inputStringQuandHidden);


            $data['iCategorieId'] = $iCategorieId;

            $data['inputStringQuandHidden'] = $inputStringQuandHidden;


            $session_iCategorieId = $this->session->userdata('iCategorieId_x');

            $session_inputStringQuandHidden = $this->session->userdata('inputStringQuandHidden_x');


            $TotalRows = count($this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 10000000, $session_iCategorieId, $session_inputStringQuandHidden));

            $data["TotalRows"] = $TotalRows;


            $config_pagination = array();

            $config_pagination["base_url"] = base_url() . "/" . $oInfoCommercant->nom_url . "/article/";

            $config_pagination["total_rows"] = $TotalRows;

            $config_pagination["per_page"] = $PerPage;

            $config_pagination["uri_segment"] = 4;

            $config_pagination['first_link'] = '<<<';

            $config_pagination['last_link'] = '>>>';

            $this->pagination->initialize($config_pagination);

            $page_pagination = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

            $data['toAgenda'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, $page_pagination, $config_pagination["per_page"], $session_iCategorieId, $session_inputStringQuandHidden);

            $data["links_pagination"] = $this->pagination->create_links();


        } else {


            $session_iCategorieId = $this->session->userdata('iCategorieId_x');

            $session_inputStringQuandHidden = $this->session->userdata('inputStringQuandHidden_x');


            $iCategorieId = (isset($session_iCategorieId)) ? $session_iCategorieId : 0;

            $inputStringQuandHidden = (isset($session_inputStringQuandHidden)) ? $session_inputStringQuandHidden : "0";


            $TotalRows = count($this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 10000000, $iCategorieId, $inputStringQuandHidden));

            $data["TotalRows"] = $TotalRows;


            $config_pagination = array();

            $config_pagination["base_url"] = base_url() . "/" . $oInfoCommercant->nom_url . "/article/";

            $config_pagination["total_rows"] = $TotalRows;

            $config_pagination["per_page"] = $PerPage;

            $config_pagination["uri_segment"] = 4;

            $config_pagination['first_link'] = '<<<';

            $config_pagination['last_link'] = '>>>';

            $this->pagination->initialize($config_pagination);

            $page_pagination = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

            $data['toAgenda'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, $page_pagination, $config_pagination["per_page"], $iCategorieId, $inputStringQuandHidden);

            $data["links_pagination"] = $this->pagination->create_links();


        }


        $data['toCategorie_principale'] = $this->mdlarticle->GetArticleCategorie_ByIdCommercant($_iCommercantId);


        /*$data['toAgndaJanvier'] = $this->mdlarticle->GetArticleNbByMonth("01",$_iCommercantId);

        $data['toAgndaFevrier'] = $this->mdlarticle->GetArticleNbByMonth("02",$_iCommercantId);

        $data['toAgndaMars'] = $this->mdlarticle->GetArticleNbByMonth("03",$_iCommercantId);

        $data['toAgndaAvril'] = $this->mdlarticle->GetArticleNbByMonth("04",$_iCommercantId);

        $data['toAgndaMai'] = $this->mdlarticle->GetArticleNbByMonth("05",$_iCommercantId);

        $data['toAgndaJuin'] = $this->mdlarticle->GetArticleNbByMonth("06",$_iCommercantId);

        $data['toAgndaJuillet'] = $this->mdlarticle->GetArticleNbByMonth("07",$_iCommercantId);

        $data['toAgndaAout'] = $this->mdlarticle->GetArticleNbByMonth("08",$_iCommercantId);

        $data['toAgndaSept'] = $this->mdlarticle->GetArticleNbByMonth("09",$_iCommercantId);

        $data['toAgndaOct'] = $this->mdlarticle->GetArticleNbByMonth("10",$_iCommercantId);

        $data['toAgndaNov'] = $this->mdlarticle->GetArticleNbByMonth("11",$_iCommercantId);

        $data['toAgndaDec'] = $this->mdlarticle->GetArticleNbByMonth("12",$_iCommercantId);*/


        $data['toAgndaJanvier'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "01");

        $data['toAgndaFevrier'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "02");

        $data['toAgndaMars'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "03");

        $data['toAgndaAvril'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "04");

        $data['toAgndaMai'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "05");

        $data['toAgndaJuin'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "06");

        $data['toAgndaJuillet'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "07");

        $data['toAgndaAout'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "08");

        $data['toAgndaSept'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "09");

        $data['toAgndaOct'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "10");

        $data['toAgndaNov'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "11");

        $data['toAgndaDec'] = $this->mdlarticle->GetByIdCommercantLimit($_iCommercantId, 0, 1000000, 0, "12");


        $this->load->model("mdl_agenda");

        $data['nombre_agenda_com'] = $this->mdl_agenda->GetByIdCommercant($_iCommercantId);


        $data['pagecategory_partner'] = "list_article";


        $data['link_partner_current_page'] = 'article';

        $data['pagecategory'] = "pro";


        $data['current_partner_menu'] = "article";


        $toBonPlan = $this->mdlbonplan->getListeBonPlan($_iCommercantId);

        $data['toBonPlan'] = $toBonPlan;


        //ajout bouton rond noire annonce/agenda

        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);

        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';

        else $data['result_check_commercant_annonce'] = '0';


        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);

        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';

        else $data['result_check_commercant_agenda'] = '0';


        $is_reserved_on = $this->Mdl_plat_du_jour->verify_reserved($_iCommercantId);

        $data['reservation'] = $is_reserved_on;


        //$this->load->view('agenda/partner_agenda_list', $data) ;

        //$this->load->view('privicarte/partner_agenda_list', $data) ;

        $this->load->view('sortez/partner_article_list', $data);


    }


    function article_partner_details($_iCommercantId)

    {


        //code standard utilisation page partenaire START **************************************************


        $nom_url_commercant = $this->uri->rsegment(3);


        $_iCommercantId = $this->mdlcommercant->GetIdCommercantfromUrl($nom_url_commercant);


        $oInfoCommercant = $this->mdlcommercant->infoCommercant($_iCommercantId);


        $data['oInfoCommercant'] = $oInfoCommercant;


        //$data['mdlannonce'] = $this->mdlannonce ;

        $data['mdlbonplan'] = $this->mdlbonplan;


        $data['nbAnnonce'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);

        $oBonPlan = $this->mdlbonplan->bonPlanParCommercant($_iCommercantId);

        $data['nbBonPlan'] = sizeof($oBonPlan);

        //		$data['sTitreBonPlan'] =(sizeof($oBonPlan) >0 )? $oBonPlan->bonplan_titre : "";		// OP 27/11/2011

        $data['sTitreBonPlan'] = (sizeof($oBonPlan) > 0) ? $oBonPlan->bonplan_texte : "";

        $data['oImagespub'] = $this->mdlimagespub->GetByImagespubActiv();

        $data['nombre_annonce_com'] = $this->mdlannonce->nombreAnnonceParDiffuseur($_iCommercantId);


        $oLastbonplanCom = $this->mdlbonplan->lastBonplanCom($_iCommercantId);

        if ($oLastbonplanCom) $data['oLastbonplanCom'] = $oLastbonplanCom;


        $data['active_link'] = "article";


        $is_mobile = $this->agent->is_mobile();

        //test ipad user agent

        $is_mobile_ipad = $this->agent->is_mobile('ipad');

        $data['is_mobile_ipad'] = $is_mobile_ipad;

        $is_robot = $this->agent->is_robot();

        $is_browser = $this->agent->is_browser();

        $is_platform = $this->agent->platform();

        $data['is_mobile'] = $is_mobile;

        $data['is_robot'] = $is_robot;

        $data['is_browser'] = $is_browser;

        $data['is_platform'] = $is_platform;


        $this->load->model("user");

        if ($this->ion_auth->logged_in()) {

            $user_ion_auth = $this->ion_auth->user()->row();

            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

            if ($iduser == null || $iduser == 0 || $iduser == "") {

                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

            }

        } else $iduser = 0;


        //code standard utilisation page partenaire END **************************************************


        $id_agenda = $this->uri->rsegment(4);


        $oDetailAgenda = $this->mdlarticle->GetById_IsActif($id_agenda);

        $data['oDetailAgenda'] = $oDetailAgenda;


        $data['pagecategory_partner'] = "details_article";


        //sending mail to event organiser **************************

        if (isset($_POST['text_mail_form_module_detailbonnplan'])) {

            $text_mail_form_module_detailbonnplan = $this->input->post("text_mail_form_module_detailbonnplan");

            $nom_mail_form_module_detailbonnplan = $this->input->post("nom_mail_form_module_detailbonnplan");

            $tel_mail_form_module_detailbonnplan = $this->input->post("tel_mail_form_module_detailbonnplan");

            $email_mail_form_module_detailbonnplan = $this->input->post("email_mail_form_module_detailbonnplan");


            $colDestAdmin = array();

            $colDestAdmin[] = array("Email" => $oDetailAgenda->email, "Name" => $oDetailAgenda->nom_manifestation . " " . $oDetailAgenda->nom_societe);


            // Sujet

            $txtSujetAdmin = "Demande d'information sur un article sur Sortez";


            $txtContenuAdmin = "

            <p>Bonjour ,</p>

            <p>Une demande d'information vous est adressé suite à un évennement que vous avez déposé sur le site Agenda.</p>

            <p>Détails :<br/>

            Evennement : " . $oDetailAgenda->nom_manifestation . "

            <br/>

            Date : " . translate_date_to_fr($oDetailAgenda->date_debut) . "<br/>

            Lieu : " . $oDetailAgenda->ville . " " . $oDetailAgenda->adresse_localisation . " " . $oDetailAgenda->codepostal_localisation . "<br/>

            Organisateur : " . $oDetailAgenda->organisateur . "<br/><br/>

            Demande Client :<br/>

            " . $text_mail_form_module_detailbonnplan . "<br/><br/>

            Nom client : " . $nom_mail_form_module_detailbonnplan . "<br/>

            Tel client : " . $tel_mail_form_module_detailbonnplan . "<br/>

            Email client : " . $email_mail_form_module_detailbonnplan . "<br/>

            </p>";


            @envoi_notification($colDestAdmin, $txtSujetAdmin, $txtContenuAdmin);

            $data['mssg_envoi_module_detail_bonplan'] = '<font color="#00CC00">Votre demande est envoyée</font>';

            //$data['mssg_envoi_module_detail_bonplan'] = $txtContenuAdmin;


        } else $data['mssg_envoi_module_detail_bonplan'] = '';

        //sending mail to event organiser *******************************


        $this->load->model("mdl_agenda");

        $data['nombre_agenda_com'] = $this->mdlarticle->GetByIdCommercant($_iCommercantId);


        $data['pagecategory'] = 'pro';

        $data['pagecategory_partner'] = 'article_partner';


        $data['link_partner_current_page'] = 'article';


        $data['current_partner_menu'] = "article";


        $data['toArticle_datetime'] = $this->mdl_article_datetime->getByArticleId($id_agenda);


        $data['cacher_slide'] = "1";


        //ajout bouton rond noire annonce/agenda

        $result_check_commercant_annonce = $this->mdlannonce->check_commercant_annonce($_iCommercantId);

        if (count($result_check_commercant_annonce) > 0) $data['result_check_commercant_annonce'] = '1';

        else $data['result_check_commercant_annonce'] = '0';


        $result_check_commercant_agenda = $this->mdl_agenda->check_commercant_agenda($_iCommercantId);

        if (count($result_check_commercant_agenda) > 0) $data['result_check_commercant_agenda'] = '1';

        else $data['result_check_commercant_agenda'] = '0';


        $is_reserved_on = $this->Mdl_plat_du_jour->verify_reserved($_iCommercantId);

        $data['reservation'] = $is_reserved_on;


        //$this->load->view('agenda/partner_agenda_details', $data);

        //$this->load->view('privicarte/partner_agenda_details', $data);

        //var_dump($data);

        $this->load->view('sortez/partner_article_details', $data);


    }


    function check_commercant()

    {

        $IdCommercant = $this->input->post("IdCommercant");

        //////$this->firephp->log($IdCommercant, 'IdCommercant');


        $oInfoCommercant = $this->mdlcommercant->infoCommercant($IdCommercant);


        $result_to_show = "";

        $result_to_show .= "NomSociete!!!==>" . $oInfoCommercant->NomSociete . "==!!!==";

        $result_to_show .= "Adresse1!!!==>" . $oInfoCommercant->Adresse1 . "==!!!==";

        $result_to_show .= "Adresse2!!!==>" . $oInfoCommercant->Adresse2 . "==!!!==";

        $result_to_show .= "IdVille!!!==>" . $oInfoCommercant->IdVille . "==!!!==";

        $result_to_show .= "CodePostal!!!==>" . $oInfoCommercant->CodePostal . "==!!!==";

        $result_to_show .= "TelFixe!!!==>" . $oInfoCommercant->TelFixe . "==!!!==";

        $result_to_show .= "TelMobile!!!==>" . $oInfoCommercant->TelMobile . "==!!!==";

        $result_to_show .= "fax!!!==>" . $oInfoCommercant->fax . "==!!!==";

        $result_to_show .= "Email!!!==>" . $oInfoCommercant->Email . "==!!!==";

        $result_to_show .= "SiteWeb!!!==>" . $oInfoCommercant->SiteWeb . "==!!!==";

        $result_to_show .= "Facebook!!!==>" . $oInfoCommercant->Facebook . "==!!!==";

        $result_to_show .= "Twitter!!!==>" . $oInfoCommercant->Twitter . "==!!!==";

        $result_to_show .= "google_plus!!!==>" . $oInfoCommercant->google_plus . "==!!!==";


        echo $result_to_show;

    }


    function check_bonplan()

    {

        $IdCommercant = $this->input->post("IdCommercant");

        //////$this->firephp->log($IdCommercant, 'IdCommercant');


        $oInfoCommercant = $this->mdlcommercant->infoCommercant($IdCommercant);


        $oBonplanCommercant = $this->mdlbonplan->bonPlanParCommercant($IdCommercant);


        if (isset($oBonplanCommercant->bonplan_id)) {

            $result_to_show = "";

            $result_to_show .= $oBonplanCommercant->bonplan_titre . "

            ";

            $result_to_show .= "Valable du " . translate_date_to_fr($oBonplanCommercant->bonplan_date_debut) . " au " . translate_date_to_fr($oBonplanCommercant->bonplan_date_fin) . "

            ";

            if ($oBonplanCommercant->bon_plan_utilise_plusieurs == "1") $result_to_show .= "BonPlan Utilisable plusieurs fois

            ";

            else $result_to_show .= "BonPlan Utilisable une seule fois

            ";

            $result_to_show .= strip_tags($oBonplanCommercant->bonplan_texte) . "

            ";


            echo $result_to_show;

        } else echo "error";

    }


    function delete_files_category($prmIdcateg = 0, $prmFiles)

    {

        $oInfocateg = $this->mdl_categories_agenda->getById($prmIdcateg);

        $filetodelete = "";

        if ($prmFiles == "images") {

            $filetodelete = $oInfocateg->images;

            $this->mdl_categories_agenda->effacerimages_categ($prmIdcateg);

        }

        if (file_exists("application/resources/front/images/agenda/category/" . $filetodelete) == true) {

            unlink("application/resources/front/images/agenda/category/" . $filetodelete);

            $img_photo2_split_array = explode('.', $filetodelete);

            $img_photo2_path = "application/resources/front/images/agenda/category/" . $img_photo2_split_array[0] . "_thumb_100_100." . $img_photo2_split_array[1];

            if (file_exists($img_photo2_path) == true) unlink($img_photo2_path);

        }


    }


    function delete_files($prmIdAgenda = 0, $prmFiles)

    {

        $oInfoAgenda = $this->mdl_agenda->GetById($prmIdAgenda);

        $filetodelete = "";

        if ($prmFiles == "doc_affiche") {

            $filetodelete = $oInfoAgenda->doc_affiche;

            $this->mdl_agenda->effacerdoc_affiche($prmIdAgenda);

        }

        if ($prmFiles == "photo1") {

            $filetodelete = $oInfoAgenda->photo1;

            $this->mdl_agenda->effacerphoto1($prmIdAgenda);

        }

        if ($prmFiles == "photo2") {

            $filetodelete = $oInfoAgenda->photo2;

            $this->mdl_agenda->effacerphoto2($prmIdAgenda);

        }

        if ($prmFiles == "photo3") {

            $filetodelete = $oInfoAgenda->photo3;

            $this->mdl_agenda->effacerphoto3($prmIdAgenda);

        }

        if ($prmFiles == "photo4") {

            $filetodelete = $oInfoAgenda->photo4;

            $this->mdl_agenda->effacerphoto4($prmIdAgenda);

        }

        if ($prmFiles == "photo5") {

            $filetodelete = $oInfoAgenda->photo5;

            $this->mdl_agenda->effacerphoto5($prmIdAgenda);

        }

        if ($prmFiles == "pdf") {

            $filetodelete = $oInfoAgenda->pdf;

            $this->mdl_agenda->effacerpdf($prmIdAgenda);

        }

        if ($prmFiles == "autre_doc_1") {

            $filetodelete = $oInfoAgenda->autre_doc_1;

            $this->mdl_agenda->effacerautre_doc_1($prmIdAgenda);

        }

        if ($prmFiles == "autre_doc_2") {

            $filetodelete = $oInfoAgenda->autre_doc_2;

            $this->mdl_agenda->effacerautre_doc_2($prmIdAgenda);

        }

        if (file_exists("application/resources/front/images/agenda/photoCommercant/" . $filetodelete) == true) {

            unlink("application/resources/front/images/agenda/photoCommercant/" . $filetodelete);

            $img_photo2_split_array = explode('.', $filetodelete);

            $img_photo2_path = "application/resources/front/images/agenda/photoCommercant/" . $img_photo2_split_array[0] . "_thumb_100_100." . $img_photo2_split_array[1];

            if (file_exists($img_photo2_path) == true) unlink($img_photo2_path);

        }


    }


    function personnalisation()

    {


        $data["current_page"] = "subscription_pro";//this is to differentiate js to use


        if (!$this->ion_auth->logged_in()) {

            redirect("auth/login");

        } else {


            $choix_dossier_perso_check_session = $this->session->userdata('choix_dossier_perso_check_session');

            if (isset($choix_dossier_perso_check_session)) {

                $data['choix_dossier_perso_check_session'] = $choix_dossier_perso_check_session;

                ////$this->firephp->log($choix_dossier_perso_check_session, 'choix_dossier_perso_check_session');

            }


            $agenda_perso = $this->input->post("agenda_perso");

            $choix_dossier_perso_value = $agenda_perso['dossperso'];

            ////$this->firephp->log($choix_dossier_perso_value, 'choix_dossier_perso_value');


            if (isset($choix_dossier_perso_value) && $choix_dossier_perso_value == "1") {

                $data['choix_dossier_perso_check_session'] = "1";

                $this->session->set_userdata('choix_dossier_perso_check_session', "1");


                $user_ion_auth = $this->ion_auth->user()->row();

                $iduser_verif = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

                if ($iduser_verif == null || $iduser_verif == 0 || $iduser_verif == "") {

                    $_iCommercantId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

                } else {

                    $_iCommercantId = "0";

                }

            } else {

                $_iCommercantId = "0";

                $data['choix_dossier_perso_check_session'] = "0";

                $this->session->set_userdata('choix_dossier_perso_check_session', "0");

            }


            if (isset($_iCommercantId) && $_iCommercantId != "0" && $_iCommercantId != null && $_iCommercantId != "") {

                $toVille = $this->mdlville->GetArticleVillesByIdCommercant($_iCommercantId);//get ville list of agenda

                //var_dump($toVille);die();

                $data['toVille'] = $toVille;

                $toCategorie_principale = $this->mdlarticle->GetArticleCategorie_ByIdCommercant($_iCommercantId);

                $data['toCategorie_principale'] = $toCategorie_principale;

                $toDeposant = $this->mdlarticle->getAlldeposantArticle($_iCommercantId);

                $data['toDeposant'] = $toDeposant;

            } else {

                $toVille = $this->mdlville->GetArticleVilles();//get ville list of agenda

                $data['toVille'] = $toVille;

                // var_dump($toVille);die();

                $toCategorie_principale = $this->mdlarticle->GetArticleCategorie();

                $data['toCategorie_principale'] = $toCategorie_principale;

                $toDeposant = $this->mdlarticle->getAlldeposantArticle();

                $data['toDeposant'] = $toDeposant;

            }


            $current_user_ion_auth = $this->ion_auth->user()->row();

            $current_iCommercantId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($current_user_ion_auth->id);

            $data['current_iCommercantId'] = $current_iCommercantId;

            $oAgenda_perso = $this->mdlarticle_perso->GetArticle_article_persoByUser($current_iCommercantId, $current_user_ion_auth->id);

            ////$this->firephp->log($current_iCommercantId, 'current_iCommercantId');

            ////$this->firephp->log($current_user_ion_auth->id, 'current_user_ion_auth');

            ////$this->firephp->log($oAgenda_perso, 'oAgenda_perso');

            $data['oAgenda_perso'] = $oAgenda_perso;


            $group_user = $this->ion_auth->get_users_groups($current_user_ion_auth->id)->result();

            $data['group_user'] = $group_user[0]->id;


            $data['pagecategory'] = "admin_commercant";

            $data['currentpage'] = "export_article";

            $this->load->view('privicarte/article_personnalisation', $data);


        }

    }


    function personnalisation_save()

    {

        $data["current_page"] = "subscription_pro";//this is to differentiate js to use


        if (!$this->ion_auth->logged_in()) {

            redirect("auth/login");

        } else {


            /*if ($this->ion_auth->in_group(3)) {

                redirect('front/utilisateur/no_permission');

            }

            else

            {*/

            $objArticle_perso = $this->input->post("agenda_perso");


            $user_ion_auth = $this->ion_auth->user()->row();

            $_iCommercantId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

            $objArticle_perso['IdUsers_ionauth'] = $user_ion_auth->id;

            $objArticle_perso['IdCommercant'] = $_iCommercantId;


            //$objArticle_perso['code'] = base64_encode($objArticle_perso['code']);

            /*$objArticle_perso['code'] = str_replace('<','&lt;',$objArticle_perso['code']);

            $objArticle_perso['code'] = str_replace('>','&gt;',$objArticle_perso['code']);*/


            //$this->firephp->log($objArticle_perso, 'objArticle_perso');


            if ($objArticle_perso['id'] == "0") {

                $IdArticle_perso = $this->mdlarticle_perso->insert_article_perso($objArticle_perso);

            } else {

                $IdArticle_perso = $this->mdlarticle_perso->update_article_perso($objArticle_perso);

            }


            redirect('article/personnalisation');


            //}


        }


    }


    function article_perso()

    {


        ////$this->firephp->log($_REQUEST, '_REQUEST');


        $zCouleur = $this->input->get("zCouleur");

        if (!isset($zCouleur)) $zCouleur = $this->input->post("zCouleur");

        if (isset($zCouleur) && $zCouleur != "") $data['zCouleur'] = $zCouleur;

        $zCouleurTitre = $this->input->get("zCouleurTitre");

        if (!isset($zCouleurTitre)) $zCouleurTitre = $this->input->post("zCouleurTitre");

        if (isset($zCouleurTitre) && $zCouleurTitre != "") $data['zCouleurTitre'] = $zCouleurTitre;

        $zCouleurTextBouton = $this->input->get("zCouleurTextBouton");

        if (!isset($zCouleurTextBouton)) $zCouleurTextBouton = $this->input->post("zCouleurTextBouton");

        if (isset($zCouleurTextBouton) && $zCouleurTextBouton != "") $data['zCouleurTextBouton'] = $zCouleurTextBouton;

        $zCouleurBgBouton = $this->input->get("zCouleurBgBouton");

        if (!isset($zCouleurBgBouton)) $zCouleurBgBouton = $this->input->post("zCouleurBgBouton");

        if (isset($zCouleurBgBouton) && $zCouleurBgBouton != "") $data['zCouleurBgBouton'] = $zCouleurBgBouton;

        $zCouleurNbBtn = $this->input->get("zCouleurNbBtn");

        if (!isset($zCouleurNbBtn)) $zCouleurNbBtn = $this->input->post("zCouleurNbBtn");

        if (isset($zCouleurNbBtn) && $zCouleurNbBtn != "") $data['zCouleurNbBtn'] = $zCouleurNbBtn;


        $tiDepartement = $this->input->get("tiDepartement");

        if (!isset($tiDepartement)) $tiDepartement = $this->input->post("tiDepartement");

        if (isset($tiDepartement) && $tiDepartement != "") {

            $tiDepartement = substr_replace($tiDepartement, "", -1);

            $tiDepartement_array = explode("_", $tiDepartement);

            $data['tiDepartement_array'] = $tiDepartement;

        } else {

            $tiDepartement_array = "0";

        }

        $tiDeposant = $this->input->get("tiDeposant");

        if (!isset($tiDeposant)) $tiDeposant = $this->input->post("tiDeposant");

        if (isset($tiDeposant) && $tiDeposant != "") {

            $tiDeposant = substr_replace($tiDeposant, "", -1);

            $tiDeposant_array = explode("_", $tiDeposant);

            $data['tiDeposant_array'] = $tiDeposant;

        }

        $tiCategorie = $this->input->get("tiCategorie");

        if (!isset($tiCategorie)) $tiCategorie = $this->input->post("tiCategorie");

        if (isset($tiCategorie) && $tiCategorie != "") {

            $tiCategorie = substr_replace($tiCategorie, "", -1);

            $data["tiCategorie_for_init"] = $tiCategorie;

            $tiCategorie_array = explode("_", $tiCategorie);

            //$data['tiCategorie_array'] = $tiCategorie_array;

        } else {

            $tiCategorie_array = "0";

        }

        $tiDossperso = $this->input->get("tiDossperso");

        if (!isset($tiDossperso)) $tiDossperso = $this->input->post("tiDossperso");

        $data["tiDossperso"] = $tiDossperso;

        if (isset($tiDossperso) && $tiDossperso != "0") {

            $idCommercant_to_search = $tiDossperso;

        } else if (isset($tiDeposant_array)) {

            $idCommercant_to_search = $tiDeposant_array;

        } else {

            $idCommercant_to_search = "0";

        }


        $contentonly = $this->input->get("contentonly");

        if (!isset($contentonly)) $contentonly = $this->input->post("contentonly");

        $data["contentonly"] = $contentonly;


        $proximite_tiDatefilter = $this->input->get("proximite_tiDatefilter");

        if (!isset($proximite_tiDatefilter)) $proximite_tiDatefilter = $this->input->post("proximite_tiDatefilter");

        if (!isset($proximite_tiDatefilter)) $proximite_tiDatefilter = "0";

        $data["proximite_tiDatefilter"] = $proximite_tiDatefilter;


        $keyword_input_export = $this->input->get("keyword_input_export");

        if (!isset($keyword_input_export)) $keyword_input_export = $this->input->post("keyword_input_export");

        $data["keyword_input_export"] = $keyword_input_export;


        //var_dump($data); die();


        /////// filter by villes //////////////


        $tiville_posted = $this->input->get("ville_filter");

        if ($tiville_posted == "" || $tiville_posted == "0") {

            $tiville_posted = '0';

        }

        if (isset($tiville_posted) && $tiville_posted != null) {

            $this->session->set_userdata('ville_filtred', $tiville_posted);

        }

        $filter_ville = $this->session->userdata('ville_filtred');

        if (isset($filter_ville) and $filter_ville != null) {

            $data["tiville"] = $filter_ville;

            $tiville = $filter_ville;

        }

        if (!isset($tiville)) {

            $tiville = 0;

        }


        $data["allville"] = $this->mdlarticle->getarticle_join_ville($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", $proximite_tiDatefilter, "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 0, $tiville);

        $toAgenda = $this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", $proximite_tiDatefilter, "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville);

        $data['toAgenda'] = $toAgenda;


        //$data['toAgndaTout_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaAujourdhui_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "101", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaWeekend_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "202", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaSemaine_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "303", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaSemproch_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "404", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaMois_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "505", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        /*

        $data['toAgndaJanvier_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "01", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaFevrier_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "02", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaMars_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "03", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaAvril_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "04", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaMai_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "05", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaJuin_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "06", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaJuillet_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "07", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaAout_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "08", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaSept_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "09", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaOct_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "10", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaNov_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "11", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaDec_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "12", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        */


        $tiCategorie_list_array = $this->mdlarticle->listeArticleRecherche_article_perso_liste_categ($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0);

        $data['tiCategorie_list_array'] = $tiCategorie_list_array;


        $data['defaul_thumb_width'] = $this->config->item('defaul_thumb_width');

        $data['defaul_thumb_height'] = $this->config->item('defaul_thumb_height');

        $data['currentpage'] = "export_article";

        //$this->load->view('agendaAout2013/agenda_perso', $data);

        //$this->load->view('privicarte/agenda_perso', $data);

        $this->load->view('export/article_perso_respo', $data);


    }


    function article_perso_check_filterQuand()

    {

        $proximite_tiDepartement = $this->input->post("proximite_tiDepartement");

        $proximite_tiDepartement = explode("_", $proximite_tiDepartement);

        $proximite_tiDeposant = $this->input->post("proximite_tiDeposant");

        $proximite_tiDeposant = explode("_", $proximite_tiDeposant);

        $proximite_tiCategorie = $this->input->post("proximite_tiCategorie");

        $proximite_tiDossperso = $this->input->post("proximite_tiDossperso");


        if (isset($proximite_tiDossperso) && $proximite_tiDossperso != "0") {

            $proximite_tiDeposant = $proximite_tiDossperso;

        }


        $proximite_tiCategorie_array = array($proximite_tiCategorie);


        $toAgenda = $this->mdlarticle->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0");


        $toAgndaTout_global = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));

        $toAgndaAujourdhui_global = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "101", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));

        $toAgndaWeekend_global = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "202", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));

        $toAgndaSemaine_global = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "303", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));

        $toAgndaSemproch_global = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "404", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));

        $toAgndaMois_global = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "505", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));


        $toAgndaJanvier_global = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "01", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));

        $toAgndaFevrier_global = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "02", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));

        $toAgndaMars_global = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "03", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));

        $toAgndaAvril_global = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "04", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));

        $toAgndaMai_global = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "05", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));

        $toAgndaJuin_global = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "06", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));

        $toAgndaJuillet_global = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "07", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));

        $toAgndaAout_global = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "08", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));

        $toAgndaSept_global = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "09", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));

        $toAgndaOct_global = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "10", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));

        $toAgndaNov_global = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "11", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));

        $toAgndaDec_global = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", "12", "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0"));


        ?>


        <select id="inputStringQuandHidden_to_check"
                size="1"
                name="inputStringQuandHidden_to_check"

                onChange="javascript:change_list_agenda_perso_filtreIdQuand();">

            <option value="0"

                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "0") { ?>selected<?php } ?>>

                Toutes
                les

                dates <?php if (isset($toAgndaTout_global)) echo "(" . $toAgndaTout_global . ")"; else echo "(0)"; ?></option>

            <option value="101"

                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "101") { ?>selected<?php } ?>>

                Aujourd'hui <?php if (isset($toAgndaAujourdhui_global)) echo "(" . $toAgndaAujourdhui_global . ")"; else echo "(0)"; ?></option>

            <option value="202"

                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "202") { ?>selected<?php } ?>>

                Ce

                Week-end <?php if (isset($toAgndaWeekend_global)) echo "(" . $toAgndaWeekend_global . ")"; else echo "(0)"; ?></option>

            <option value="303"

                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "303") { ?>selected<?php } ?>>

                Cette

                semaine <?php if (isset($toAgndaSemaine_global)) echo "(" . $toAgndaSemaine_global . ")"; else echo "(0)"; ?></option>

            <option value="404"

                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "404") { ?>selected<?php } ?>>

                Semaine

                prochaine <?php if (isset($toAgndaSemproch_global)) echo "(" . $toAgndaSemproch_global . ")"; else echo "(0)"; ?></option>

            <!--<option value="505" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="505") {

            ?>selected<?php // }

            ?>>Ce mois <?php // if (isset($toAgndaMois_global)) echo "(".$toAgndaMois_global.")"; else echo "(0)";

            ?></option>-->

            <option value="01"

                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "01") { ?>selected<?php } ?>>

                Janvier <?php if (isset($toAgndaJanvier_global)) echo "(" . $toAgndaJanvier_global . ")"; else echo "(0)"; ?></option>

            <option value="02"

                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "02") { ?>selected<?php } ?>>

                Février <?php if (isset($toAgndaFevrier_global)) echo "(" . $toAgndaFevrier_global . ")"; else echo "(0)"; ?></option>

            <option value="03"

                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "03") { ?>selected<?php } ?>>

                Mars <?php if (isset($toAgndaMars_global)) echo "(" . $toAgndaMars_global . ")"; else echo "(0)"; ?></option>

            <option value="04"

                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "04") { ?>selected<?php } ?>>

                Avril <?php if (isset($toAgndaAvril_global)) echo "(" . $toAgndaAvril_global . ")"; else echo "(0)"; ?></option>

            <option value="05"

                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "05") { ?>selected<?php } ?>>

                Mai <?php if (isset($toAgndaMai_global)) echo "(" . $toAgndaMai_global . ")"; else echo "(0)"; ?></option>

            <option value="06"

                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "06") { ?>selected<?php } ?>>

                Juin <?php if (isset($toAgndaJuin_global)) echo "(" . $toAgndaJuin_global . ")"; else echo "(0)"; ?></option>

            <option value="07"

                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "07") { ?>selected<?php } ?>>

                Juillet <?php if (isset($toAgndaJuillet_global)) echo "(" . $toAgndaJuillet_global . ")"; else echo "(0)"; ?></option>

            <option value="08"

                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "08") { ?>selected<?php } ?>>

                Août <?php if (isset($toAgndaAout_global)) echo "(" . $toAgndaAout_global . ")"; else echo "(0)"; ?></option>

            <option value="09"

                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "09") { ?>selected<?php } ?>>

                Septembre <?php if (isset($toAgndaSept_global)) echo "(" . $toAgndaSept_global . ")"; else echo "(0)"; ?></option>

            <option value="10"

                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "10") { ?>selected<?php } ?>>

                Octobre <?php if (isset($toAgndaOct_global)) echo "(" . $toAgndaOct_global . ")"; else echo "(0)"; ?></option>

            <option value="11"

                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "11") { ?>selected<?php } ?>>

                Novembre <?php if (isset($toAgndaNov_global)) echo "(" . $toAgndaNov_global . ")"; else echo "(0)"; ?></option>

            <option value="12"

                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "12") { ?>selected<?php } ?>>

                Décembre <?php if (isset($toAgndaDec_global)) echo "(" . $toAgndaDec_global . ")"; else echo "(0)"; ?></option>

        </select>


        <?php


    }


    function article_perso_check()

    {


        $proximite_tiDepartement = $this->input->post("proximite_tiDepartement");

        $proximite_tiDepartement = explode("_", $proximite_tiDepartement);

        $proximite_tiDeposant = $this->input->post("proximite_tiDeposant");

        $proximite_tiDeposant = explode("_", $proximite_tiDeposant);

        $proximite_tiCategorie = $this->input->post("proximite_tiCategorie");

        $proximite_tiDossperso = $this->input->post("proximite_tiDossperso");

        $inputStringQuandHidden_to_check = $this->input->post("inputStringQuandHidden_to_check");

        $defaul_thumb_width = $this->config->item('defaul_thumb_width');

        $defaul_thumb_height = $this->config->item('defaul_thumb_height');


        if (isset($proximite_tiDossperso) && $proximite_tiDossperso != "0") {

            $proximite_tiDeposant = $proximite_tiDossperso;

        }


        $proximite_tiCategorie_array = array($proximite_tiCategorie);


        if (isset($inputStringQuandHidden_to_check) && $inputStringQuandHidden_to_check != "0") {

            $idQuand = $inputStringQuandHidden_to_check;

        } else {

            $idQuand = "0";

        }


        $toAgenda = $this->mdlarticle->listeArticleRecherche_filtre_article_perso_check_categ($proximite_tiCategorie_array, 0, $proximite_tiDepartement, "", 0, 0, 10000, "", $idQuand, "0000-00-00", "0000-00-00", $proximite_tiDeposant, "0");


        ?>


        <?php foreach ($toAgenda as $oAgenda) { ?>


        <table width="100%"
               border="0"
               cellpadding="0"
               cellspacing="0"
               style="margin:3px">

            <tr>


                <td style="width:90px">

                    <a href="javascript:void(0);"

                       onClick="javascript:proximite_perso_view_details(<?php echo $oAgenda->id; ?>);"

                       style="color:#000000; text-decoration:none;">

                        <?php

                        $image_home_vignette = "";

                        if (isset($oAgenda->photo1) && $oAgenda->photo1 != "" && is_file("application/resources/front/images/agenda/photoCommercant/" . $oAgenda->photo1) == true) {

                            $image_home_vignette = $oAgenda->photo1;

                        } else if ($image_home_vignette == "" && isset($oAgenda->photo2) && $oAgenda->photo2 != "" && is_file("application/resources/front/images/agenda/photoCommercant/" . $oAgenda->photo2) == true) {

                            $image_home_vignette = $oAgenda->photo2;

                        } else if ($image_home_vignette == "" && isset($oAgenda->photo3) && $oAgenda->photo3 != "" && is_file("application/resources/front/images/agenda/photoCommercant/" . $oAgenda->photo3) == true) {

                            $image_home_vignette = $oAgenda->photo3;

                        } else if ($image_home_vignette == "" && isset($oAgenda->photo4) && $oAgenda->photo4 != "" && is_file("application/resources/front/images/agenda/photoCommercant/" . $oAgenda->photo4) == true) {

                            $image_home_vignette = $oAgenda->photo4;

                        } else if ($image_home_vignette == "" && isset($oAgenda->photo5) && $oAgenda->photo5 != "" && is_file("application/resources/front/images/agenda/photoCommercant/" . $oAgenda->photo5) == true) {

                            $image_home_vignette = $oAgenda->photo5;

                        }

                        ////$this->firephp->log($image_home_vignette, 'image_home_vignette');


                        //showing category img if all image of agenda is null

                        $this->load->model("mdl_categories_agenda");

                        $toCateg_for_agenda = $this->mdl_categories_agenda->getById($oAgenda->agenda_categid);

                        if ($image_home_vignette == "" && isset($toCateg_for_agenda->images) && $toCateg_for_agenda->images != "" && is_file("application/resources/front/images/agenda/category/" . $toCateg_for_agenda->images) == true) {

                            echo '<img src="' . GetImagePath("front/") . '/agenda/category/' . $toCateg_for_agenda->images . '" width="90"/>';

                        } else {


                            if ($image_home_vignette != "") {

                                $img_photo_split_array = explode('.', $image_home_vignette);

                                $img_photo_path = "application/resources/front/images/agenda/photoCommercant/" . $img_photo_split_array[0] . "_thumb_" . $defaul_thumb_width . "_" . $defaul_thumb_height . "." . $img_photo_split_array[1];

                                if (is_file($img_photo_path) == false) {

                                    echo image_thumb("application/resources/front/images/agenda/photoCommercant/" . $image_home_vignette, $defaul_thumb_width, $defaul_thumb_height, '', '');

                                } else echo '<img src="' . GetImagePath("front/") . '/agenda/photoCommercant/' . $img_photo_split_array[0] . "_thumb_" . $defaul_thumb_width . "_" . $defaul_thumb_height . "." . $img_photo_split_array[1] . '" width="90"/>';


                            } else {

                                $image_home_vignette_to_show = GetImagePath("front/") . "/wp71b211d2_06.png";

                                echo '<img src="' . $image_home_vignette_to_show . '" width="90"/>';

                            }


                        }

                        ?>

                    </a>


                </td>

                <td width="450"
                    valign="top"
                    style="padding-left:15px; padding-right:0px;">

                    <a href="javascript:void(0);"

                       onClick="javascript:proximite_perso_view_details(<?php echo $oAgenda->id; ?>);"

                       style="color:#000000; text-decoration:none;">

                <span class="titre_agenda_perso">

                <strong>

                    <?php echo $oAgenda->category; ?><br/>

                    <?php echo $oAgenda->nom_manifestation; ?><br/>

                </strong>

                </span>

                        <span style="font-size:12px;">

              <?php echo $oAgenda->ville; ?>, <?php echo $oAgenda->adresse_localisation; ?>

                            , <?php echo $oAgenda->codepostal_localisation; ?><br/>

                            <?php

                            if ($oAgenda->date_debut == $oAgenda->date_fin) echo "Le " . translate_date_to_fr($oAgenda->date_debut);

                            else echo "Du " . translate_date_to_fr($oAgenda->date_debut) . " au " . translate_date_to_fr($oAgenda->date_fin);

                            ?>

              </span>

                    </a>

                </td>


                <td>

                    <a href="javascript:void(0);"

                       onClick="javascript:proximite_perso_view_details(<?php echo $oAgenda->id; ?>);"

                       style="color:#000000; text-decoration:none;">

                        <img src="<?php echo GetImagePath("front/"); ?>/mobile2013/fleche_agenda_mobile.png"
                             width="30"

                             height="62"
                             alt="details"/>

                    </a>

                </td>


            </tr>

        </table>


        <div style="height:3px; width:100%; background-color:#000000"></div>

    <?php } ?>

        <?php


    }

    function set_iframe_session_navigation()

    {

        $this->session->set_userdata('iframe_session_navigation', "1");

        echo "1";

    }


    function set_view_agenda_part_session_navigation()

    {

        $this->session->set_userdata('view_agenda_part_session_navigation', "1");

        echo "1";

    }


    function FacebookProFormAgenda($IdAgenda)

    {

        $data['IdAgenda'] = $IdAgenda;

        $data['oDetailAgenda'] = $this->mdl_agenda->GetById($IdAgenda);

        $this->load->view("privicarte/FacebookProFormAgenda", $data);

    }


    function check_filter_session_value()

    {

        $data = array();

        $data['zMotCle_x'] = $this->session->userdata('zMotCle_x');

        $data['inputStringQuandHidden_x'] = $this->session->userdata('inputStringQuandHidden_x');

        $data['inputStringDatedebutHidden_x'] = $this->session->userdata('inputStringDatedebutHidden_x');

        $data['inputStringDatefinHidden_x'] = $this->session->userdata('inputStringDatefinHidden_x');

        echo json_encode($data);

    }


    public function revue_presse()

    {

        $group_proo_club = array(3, 4, 5);

        if (!$this->ion_auth->logged_in()) {

            redirect("connexion");

        } else if ($this->ion_auth->in_group($group_proo_club)) {

            $user_ion_auth = $this->ion_auth->user()->row();

            $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);

            $allville = $this->mdlville->grtall_ville_revue();

            $data['allville'] = $allville;

            $this->load->view('sortez_vsv/revue_presse', $data);

        }

    }


    public function valid_url()

    {

        unset($_SESSION['api_key']);

        unset($_SESSION['token']);


        $group_proo_club = array(3, 4, 5);


        if (!$this->ion_auth->logged_in()) {

            redirect("connexion");


        } else if ($this->ion_auth->in_group($group_proo_club)) {


            $api_key = $this->input->post("api_key");

            $token = $this->input->post("token");

            $codepostal = $this->input->post('code');

            $type = $this->input->post('type');

            $allville = $this->mdlville->grtall_ville_revue();

            $aville_brut = $this->mdlville->getall_ville_revue($codepostal);

            $aville = $aville_brut->IdVille;


            $villes = $this->input->post('villes');

            $urls = $this->input->post('url');

            $url_api = $this->input->post('url_api');

            $acturl = $this->input->post('acturl');


            //var_dump($urls);die();


            if (isset($urls) and $urls != "" and $urls != null) {

                $url = $this->input->post('url');

                $data["url"] = $urls;

                $data['codepostal'] = $codepostal;

                $data['type'] = $type;

                $data['allville'] = $allville;

                $data['villes_selected'] = $villes;

                $data["token"] = $token;

                $data["api_key"] = $api_key;

                $data["acturl"] = $acturl;

                $data['aville'] = $aville;


            } elseif (!isset($acturl) or $acturl == "") {

                $url = $url_api;

                $data["api_url"] = $url;

                $data['codepostal'] = $codepostal;

                $data['type'] = $type;

                $data['allville'] = $allville;

                $data['villes_selected'] = $villes;

                $data["token"] = $token;

                $data["api_key"] = $api_key;

                $data['aville'] = $aville;

            }


            if (isset($url) and $url != "" and $url != null and $url == site_url('article/get_api')) {

                $data['runned'] = "yes";

                $data["api_key"] = $api_key;

                $this->session->set_userdata('api_key', $api_key);

                $this->session->set_userdata('token', $token);

            }

            if (isset($urls) and $urls != "") {

                $full_json = $urls;

                $json = file_get_contents($full_json, true);

            } else {

                $json = $this->get_api($api_key, $token);

            }

            if ($json != null and $json != "") {

                $alldecoded = json_decode($json);

                if ($alldecoded->agenda != null) {

                    $result = "ok";

                    $data['nbdate'] = count($alldecoded->agenda);


                } else {

                    $result = "no";


                }

                if (isset($url) and $url != "" and $url != null) {

                    $data['result_url'] = $result;

                } else {

                    $data['result_api'] = $result;

                }

            } else {

                $data['result_url'] = "no";

            }

//var_dump($result);die();

            $this->load->view('sortez_vsv/revue_presse', $data);

        }

    }


    public function get_data()

    {

        $this->datatourisme_imported = 0;

        $this->datatourisme_skipped = 0;

        $this->datatourisme_exists = 0;

        $api_key = $this->session->userdata("api_key");

        $token = $this->session->userdata("token");

        $group_proo_club = array(3, 4, 5);


        if (!$this->ion_auth->logged_in()) {

            redirect("connexion");


        } else if ($this->ion_auth->in_group($group_proo_club)) {


            $user_ion_auth = $this->ion_auth->user()->row();

            $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);


            ///////////////// si json generé url= donné generé, sinon generer json ///////////////////

            $isrunned = $this->input->post("isrunned");

            //$isrunned="ok";

            $i = $this->input->post("toLastIdDatatourisme");

            //$i=2;

            if (isset($isrunned) and $isrunned != "") {


                $json = $this->get_runned_data($api_key, $token);

            } else {

                $url = $this->input->post("url");

                $json = file_get_contents($url, true);

            }


///////////////////////////////////////////////////////////////////////////////////////////////

            //$i = 1;

            //$url="http://localhost/sortez7/assets/json/agenda.json";


//var_dump($url);die();

            ///////////////////////// parse json   //////////////////////////////////


            $alldecode = json_decode($json);


//var_dump($alldecode);die();

            /////////////////////////////////////////////////////////////////////////

            if (isset($alldecode->agenda[$i]->date_debut_sl)) {


                $date = $alldecode->agenda[$i]->date_debut_sl;

                $alldec = explode("/", $date);

                if ($alldec) {

                    $years = explode(' ', $alldec[2]);

                    $date_debut = $years[0] . '-' . $alldec[1] . '-' . $alldec[0];

                }

                $url = $alldecode->agenda[$i]->link;

                $content = file_get_contents($url);

                $first_step = explode('<div class="col-content">', $content);

                if (isset($first_step[1])) {

                    $second_step = explode("<div class=\"content-richtext-end\"><!--empty-->", $first_step[1]);

                    $description_brute = ($second_step[0]);

                    $all = str_replace("img src=\"", "<img src=\"http://www.cannes.com", $description_brute);


                    $description = $all;

                } elseif (isset($first_step[0])) {

                    $second_step = explode("<div class=\"content-richtext-end\"><!--empty-->", $first_step[0]);

                    $description_brute = ($second_step[0]);

                    $all = str_replace("img src=\"", "<img src=\"http://www.cannes.com", $description_brute);


                    $description = $all;

                } else {

                    $description = "";

                }


                $url = $alldecode->agenda[$i]->link;

                $sitewebs = $alldecode->agenda[$i]->link;

                $content = file_get_contents($url);

                $first_step = explode('<div class="col-content">', $content);

                if (isset($first_step[1])) {

                    $second_step = explode("<div class=\"content-richtext-end\"><!--empty-->", $first_step[1]);

                    $description_brute = ($second_step[0]);

                    $all = str_replace("img src=\"", "<img src=\"http://www.cannes.com", $description_brute);

                    $is = (string)$all;

                    $exploded = explode('>', ($is));

                    //var_dump($exploded);

                    foreach ($exploded as $val) {

                        if (preg_match('/^<<img/', $val)) {

                            $results = $val;


                            if (isset($results)) {

                                //var_dump($results);

                                if (preg_match('/src=/', $results)) {

                                    $as = explode('src="', $results);

                                    foreach ($as as $ass) {

                                        if (isset($ass) and preg_match('/^http/', $ass)) {


                                            $image_alt = str_replace('"', '', $ass);

                                            $img = explode(' ', $image_alt);

                                            foreach ($img as $resu) {

                                                if (preg_match('/^http/', $resu)) {

                                                    $image = $resu;

                                                }

                                            }

                                        }

                                    }

                                } elseif (preg_match('/href/', $results)) {

                                    $as = explode('href="', $results);

                                    foreach ($as as $ass) {

                                        if (isset($ass) and preg_match('/^http/', $ass)) {


                                            $image_alt = str_replace('"', '', $ass);

                                            $img = explode(' ', $image_alt);

                                            foreach ($img as $resu) {

                                                if (preg_match('/^http/', $resu)) {

                                                    $image = $resu;

                                                }

                                            }

                                        }

                                    }

                                }


                            }

                        }

                    }


                }


            }

            if (isset($alldecode->agenda[$i]->heure_debut)) {

                $heure_debut = $alldecode->agenda[$i]->heure_debut;

            }


            if (isset($alldecode->agenda[$i]->date_debut_tn)) {


                $exploded_date = explode(" ", strtolower($alldecode->agenda[$i]->date_debut_tn));

                if (count($exploded_date) == 4) {

                    $exploded = explode('/', $exploded_date[1]);

                    $date_debut = $exploded[2] . '-' . $exploded[1] . '-' . $exploded[0];


                } elseif (count($exploded_date) == 2) {


                    $exploded = explode('/', $exploded_date[1]);

                    $date_debut = $exploded[2] . '-' . $exploded[1] . '-' . $exploded[0];

                }

            }

            if (isset($alldecode->agenda[$i]->date_debut_ind)) {


                $exploded = explode(" ", strtolower($alldecode->agenda[$i]->date_debut_ind));

                if (count($exploded) == 7) {

                    $date_debut = convert_ind_to_sql_date_counted7($exploded);

                    $heure_debut = convert_ind_to_sql_heure_counted7($exploded);


                } elseif (count($exploded) == 13) {


                    $date_debut = convert_ind_to_sql_date_debut_counted13($exploded);

                    $heure_debut = convert_ind_to_sql_heure_debut_counted13($exploded);

                    $date_fin = convert_ind_to_sql_date_fin_counted13($exploded);


                }

            }


            if (isset($alldecode->agenda[$i]->date_debut_nthea)) {


                $date_de = $alldecode->agenda[$i]->date_debut_nthea;

                $exploded_date_debut = explode(' ', $date_de);

                if ($exploded_date_debut[2] == 'janvier') $exploded_date_debut[2] = "01";

                if ($exploded_date_debut[2] == 'février' || $exploded_date_debut[2] == 'Feb') $exploded_date_debut[2] = "02";

                if ($exploded_date_debut[2] == 'mars') $exploded_date_debut[2] = "03";

                if ($exploded_date_debut[2] == 'avril') $exploded_date_debut[2] = "04";

                if ($exploded_date_debut[2] == 'mai') $exploded_date_debut[2] = "05";

                if ($exploded_date_debut[2] == 'juin') $exploded_date_debut[2] = "06";

                if ($exploded_date_debut[2] == 'juillet') $exploded_date_debut[2] = "07";

                if ($exploded_date_debut[2] == 'Août' || $exploded_date_debut[2] == 'Aug') $exploded_date_debut[2] = "08";

                if ($exploded_date_debut[2] == 'séptembre') $exploded_date_debut[2] = "09";

                if ($exploded_date_debut[2] == 'octobre') $exploded_date_debut[2] = "10";

                if ($exploded_date_debut[2] == 'novembre') $exploded_date_debut[2] = "11";

                if ($exploded_date_debut[2] == 'décembre' || $exploded_date_debut[2] == 'Dec') $exploded_date_debut[2] = "12";


                $date_debut = $exploded_date_debut[3] . '-' . $exploded_date_debut[2] . '-' . $exploded_date_debut[1];

                $heure = $exploded_date_debut[5];

                $exploded_heure = explode('h', $heure);

                $heure_debut = $exploded_heure[0] . ':' . $telephone = preg_replace('/[^0-9]/', '', $exploded_heure[1]);


            }

            if (isset($alldecode->agenda[$i]->date_debut_fin)) {

                $all = explode("-", $alldecode->agenda[$i]->date_debut_fin);

                $date_debut = convert_date_debut_fin_to_sql_date_debut($all, $alldecode->agenda[$i]->date_debut_fin);

                $date_fin = convert_date_debut_fin_to_sql_date_fin($all, $alldecode->agenda[$i]->date_debut_fin);

                $heure_debut = convert_date_debut_fin_to_sql_heure_debut($all, $alldecode->agenda[$i]->date_debut_fin);


            }

            if (isset($alldecode->agenda[$i]->date_debut_nik) and !isset($alldecode->agenda[$i]->date_fin_nik)) {


                $exploded = explode(' ', $alldecode->agenda[$i]->date_debut_nik);


                $date_debut = convert_date_nik_to_sql_date($exploded);


            } elseif (isset($alldecode->agenda[$i]->date_debut_nik) and isset($alldecode->agenda[$i]->date_fin_nik)) {

                $date_de = $alldecode->agenda[$i]->date_debut_nik;

                $date_f = $alldecode->agenda[$i]->date_fin_nik;

                $exploded_date_debut = explode(' ', $date_de);

                $exploded_date_debut_date_fin = explode(' ', $date_f);


                $date_debut = convert_nik_date_with_date_fin_to_sql_date_debut($exploded_date_debut, $exploded_date_debut_date_fin);

                $date_fin = convert_nik_date_with_date_fin_to_sql_date_fin($exploded_date_debut, $exploded_date_debut_date_fin);

            }

            if (isset($alldecode->agenda[$i]->date_debut_m)) {

                $date_bruted = $alldecode->agenda[$i]->date_debut_m;

                $jour = $alldecode->agenda[$i]->daye_debut_j;


                $date_debut = convert_date_debut_m_to_sql_date($date_bruted, $jour);


            }


            if (isset($alldecode->agenda[$i]->date_debut_grassev)) {


                $date = $alldecode->agenda[$i]->date_debut_grassev;

                $exploded = explode(' ', strtoupper($date));


                if (count($exploded) == 3) {


                    $date_debut = convert_date_debut_grassev_counted_3_to_sql_date_debut($exploded);


                } elseif (count($exploded) == 6) {


                    $date_debut = convert_date_debut_grassev_counted_6_to_sql_date_debut($exploded);

                    $date_fin = convert_date_debut_grassev_counted_6_to_sql_date_fin($exploded);


                } elseif (count($exploded) == 8) {


                    $date_debut = convert_date_debut_grassev_counted_8_to_sql_date_debut($exploded);

                    $date_fin = convert_date_debut_grassev_counted_8_to_sql_date_fin($exploded);


                } elseif (count($exploded) == 7) {


                    $date_debut = convert_date_debut_grassev_counted_7_to_sql_date_debut($exploded);

                    $date_fin = convert_date_debut_grassev_counted_7_to_sql_date_fin($exploded);


                }

            }

            if (isset($alldecode->agenda[$i]->date_debut_carlo)) {

                $date = $alldecode->agenda[$i]->date_debut_carlo;

                $date_debut = convert_carlo_date_to_sql_date_debut($date);

            }


            if (isset($alldecode->agenda[$i]->heure_debut_grassev)) {


                $heure_debut = convert_heure_debut_grassev_to_sql_heure($alldecode, $alldecode->agenda[$i]->heure_debut_grassev);


            }


            if (isset($alldecode->agenda[$i]->date_debut_norm)) {


                $exploded = explode(" ", $alldecode->agenda[$i]->date_debut_norm);


                $date_debut = convert_date_debut_norm_to_sql_date($exploded);


            }

            ///////////////////////////////  formatage date ///////////////////////////////////////


            if (isset($alldecode->agenda[$i]->date_debut)) {

                $chainess = $alldecode->agenda[$i]->date_debut;

                $motifss = '/\b\n\b/i';

                $date_long = $alldecode->agenda[$i]->date_debut;

                $chaine_long = $date_long;

                $motif_long = '/h/';


                $date = explode(" ", $date_long);

                if (count($date) == "2") {


                    $date_brute = explode(" ", $alldecode->agenda[$i]->date_debut);


                    $date_debut = convert_date_debut_counted_2_to_sql_date_debut($date_brute, $alldecode->agenda[$i]->date_fin);

                    $date_fin = convert_date_debut_counted_2_to_sql_date_fin($date_brute, $alldecode->agenda[$i]->date_fin);

                }


                $typedate = explode(" ", $alldecode->agenda[$i]->date_debut);

                //var_dump($typedate);die();

                if (count($typedate) > 3) {


                    $date_debut = convert_date_debut_counted_bigger_than_3_to_sql_date_debut($date_brute);


                }

                if (preg_match($motifss, $chainess)) {

                    $exploded = explode("\n", $chainess);

                    $date_debut = convert_pregmatched_motifss_and_cainess_to_sql_date_debut($exploded);

                    $date_fin = convert_pregmatched_motifss_and_cainess_to_sql_date_fin($exploded);

                }

                if (preg_match($motif_long, $chaine_long)) {

                    $exploded = explode(" ", $date_long);

                    $date_debut = convert_pregmatched_motif_long_and_chaine_long_to_sql_date_debut($exploded, $alldecode->agenda[$i]->date_fin);

                    $date_fin = convert_pregmatched_motif_long_and_chaine_long_to_sql_date_fin($exploded, $alldecode->agenda[$i]->date_fin);

                    $heure_debut = convert_pregmatched_motif_long_and_chaine_long_to_sql_heure_debut($exploded, $alldecode->agenda[$i]->date_fin);

                }


            }

            if (isset($alldecode->agenda[$i]->date_debut_menton)) {


                $date = $alldecode->agenda[$i]->date_debut_menton;

                $exploded = explode(" ", strtolower($date));

                if (count($exploded) == 12) {


                    $date_debut = convert_date_debut_menton_counted_12_to_sql_date_debut($exploded);

                    $heure_debut = convert_date_debut_menton_counted_12_to_sql_heure_debut($exploded);


                } elseif (count($exploded) == 15) {


                    $date_debut = convert_date_debut_menton_counted_15_to_sql_date_debut($exploded);

                    $date_fin = convert_date_debut_menton_counted_15_to_sql_date_fin($exploded);

                    $heure_debut = convert_date_debut_menton_counted_15_to_sql_heure_debut($exploded);


                } elseif (count($exploded) == 7) {


                    $date_debut = convert_date_debut_menton_counted_7_to_sql_date_debut($exploded);

                    $heure_debut = convert_date_debut_menton_counted_7_to_sql_heure_debut($exploded);


                } elseif (count($exploded) == 4) {


                    $date_debut = convert_date_debut_menton_counted_4_to_sql_date_debut($exploded);


                }


            }

            if (isset($alldecode->agenda[$i]->date_debut) and $date_debut == null) {

                $date = $alldecode->agenda[$i]->date_debut;

                $exploded = explode(' ', $date);

                $date_debut = convert_standard_date_to_sql_date_debut($exploded);

            }


            if (isset($alldecode->agenda[$i]->date_raphael)) {


                $date = $alldecode->agenda[$i]->date_raphael;

                $date_decoded = explode(' ', $date);


                if (count($date_decoded) == 8) {

                    $mois = preg_replace('/,/', '', $date_decoded[2]);


                    $date_debut = convert_date_raphael_counted_8_to_sql_date_debut($mois, $date_decoded, $date);

                    $heure_debut = convert_date_raphael_counted_8_to_sql_heure_debut($mois, $date_decoded, $date);


                } elseif (count($date_decoded) == 13) {


                    $date_debut = convert_date_raphael_counted_13_to_sql_date_debut($date_decoded, $date);

                    $heure_debut = convert_date_raphael_counted_13_to_sql_heure_debut($date_decoded, $date);

                    $date_fin = convert_date_raphael_counted_13_to_sql_date_fin($date_decoded, $date);


                }

            }


            if (isset($alldecode->agenda[$i]->date_drag)) {


                if (preg_match('/[()]/', $alldecode->agenda[$i]->date_drag)) {

                    $date = preg_replace('/[()]/', '', $alldecode->agenda[$i]->date_drag);

                }

                $date_brute = explode(' ', $date);

                $date_debut = convert_date_drag_to_sql_date_debut($date_brute);

                $heure_debut = convert_date_drag_to_sql_heure_debut($date_brute);

            }

            if (isset($alldecode->agenda[$i]->date_debut_loubet)) {

                $date = $alldecode->agenda[$i]->date_debut_loubet;

                $date_exploded = explode('.', $date);

                $date_debut = convert_date_debut_loubet_to_sql_date_debut($date_exploded);

            }

            if (isset($alldecode->agenda[$i]->heure_debut_loubet)) {

                $heure = $alldecode->agenda[$i]->heure_debut_loubet;

                $heure_exploded = explode('h', $heure);

                $heure_debut = convert_heure_debut_loubet_to_sql_heure_debut($heure_exploded);

            }

            if (isset($alldecode->agenda[$i]->date_debut_loubet) and isset($alldecode->agenda[$i]->date_fin_loubet)) {


                $date_fin = convert_date_fin_loubet_to_sql_date_fin($date_exploded, $alldecode->agenda[$i]->date_fin_loubet);

            }

            if (isset($alldecode->agenda[$i]->date_debut_mandelieu)) {


                $date = $alldecode->agenda[$i]->date_debut_mandelieu;


                $date_debut = convert_mandelieu_date_to_sql_date_debut($date);


                $heure_debut = convert_mandelieu_date_to_sql_heure_debut($date);


            }

            if (isset($alldecode->agenda[$i]->date_loubet_art)) {

                $date_exploded = explode(' ', $alldecode->agenda[$i]->date_loubet_art);

                $date_debut = convert_loubet_art_date_to_sql_date_date_debut($date_exploded);

            }


            if (isset($alldecode->agenda[$i]->date_debut_fayence)) {

                $date_exploded = explode(' ', $alldecode->agenda[$i]->date_debut_fayence);

                $date_debut = convert_fayence_art_date_to_sql_date_date_debut($date_exploded);

            }

            if (isset($alldecode->agenda[$i]->date_dracenie)) {

                $date_exploded = explode(" ", $alldecode->agenda[$i]->date_dracenie);

                $date_debut = convert_dracenie_date_debut_to_sql_date_debut($date_exploded);

            }

            if (isset($alldecode->agenda[$i]->date_dracenie)) {

                $date_exploded = explode(" ", $alldecode->agenda[$i]->date_dracenie);

                $date_fin = convert_dracenie_date_debut_to_sql_date_fin($date_exploded);

            }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


            if (isset($alldecode->agenda[$i]->url)) {

                $siteweb = $alldecode->agenda[$i]->url;

            } elseif (isset($sitewebs)) {

                $siteweb = $sitewebs;

            } else {
                $siteweb = "";
            }

            if ($iduser == "300607") {

                $siteweb = "http://www.ville-grasse.fr/";

            }

            /////////////////////////////////////si pas de date_fin  alors date_debut=date_fin//////////////////////////////////

            if (isset($date_debut)) {

                if (isset($date_fin)) {


                } else {


                    $date_fin = $date_debut;

                }

            }


            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            if (preg_match('/itok/', $alldecode->agenda[$i]->image)) {

                $reformat = explode('?', $alldecode->agenda[$i]->image);

                $alldecode->agenda[$i]->image = $reformat[0];

            }

            //var_dump($date_debut);die();

            ////////////////////////////////////sauvegarde d'image dans application/resources/front/photocommercant//////////////

            if (isset($image) and $image != null) {

                $final = $image;

            } elseif (isset($alldecode->agenda[$i]->image)) {

                $final = $alldecode->agenda[$i]->image;

            } elseif (isset($alldecode->agenda[$i]->image_canne)) {

                $dec = explode('?objectId', $alldecode->agenda[$i]->image_canne);

                $final = $dec[0];

            }

            if ($iduser == "300221") {

                $titre_image = strtolower($alldecode->agenda[$i]->titre);

                $image_titre = str_replace(' ', '-', $titre_image);

                $final = 'https://www.theatredegrasse.com//images/18-19/355x355/355x355_' . $image_titre . '.jpg';

            }

            if (!preg_match('/.jpg/', $final) and !preg_match('/.png/', $final)) {

                $final = $final . '.jpg';

            }


            if (isset($final) && $final != null) {

                if ($filesize = @getimagesize($final)) {

                    if (isset($filesize[0])) {

                        $info = pathinfo($final);

                        $contents = file_get_contents($final);

                        $_zExtension = strrchr($info['basename'], '.');

                        $_zFilename = random_string('unique', 10) . $_zExtension;

                        $file = 'application/resources/front/photoCommercant/imagesbank/scrapped_image/' . $_zFilename;

                        $dir = 'application/resources/front/photoCommercant/imagesbank/scrapped_image/';

                        if (is_dir($dir) == true) {

                            // var_dump($file);die();

                            @file_put_contents($file, $contents);

                            //$uploaded_file = new UploadedFile($file, $_zFilename);

                            if (isset($_zFilename)) $photo = $_zFilename;// save image name on agenda table

                            if ($file != "") {

                                if (is_file($file)) {

                                    $base_path_system = str_replace('system/', '', BASEPATH);

                                    $image_path_resize_home = $base_path_system . "/" . $file;

                                    $image_path_resize_home_final = $base_path_system . "/" . $file;

                                    $this_imgmoo =& get_instance();

                                    $this_imgmoo->load->library('image_moo');

                                    $this_imgmoo->image_moo
                                        ->load($image_path_resize_home)
                                        ->resize_crop(640, 480, false)
                                        ->save($image_path_resize_home_final, true);

                                }

                            } else {
                                $_zFilename = "";
                            }

                        } else {

                            mkdir($dir, 0777);

                            // var_dump($file);die();

                            @file_put_contents($file, $contents);

                            //$uploaded_file = new UploadedFile($file, $_zFilename);

                            if (isset($_zFilename)) $photo = $_zFilename;// save image name on agenda table

                            if ($file != "") {

                                if (is_file($file)) {

                                    $base_path_system = str_replace('system/', '', BASEPATH);

                                    $image_path_resize_home = $base_path_system . "/" . $file;

                                    $image_path_resize_home_final = $base_path_system . "/" . $file;

                                    $this_imgmoo =& get_instance();

                                    $this_imgmoo->load->library('image_moo');

                                    $this_imgmoo->image_moo
                                        ->load($image_path_resize_home)
                                        ->resize_crop(640, 480, false)
                                        ->save($image_path_resize_home_final, true);

                                } else {
                                    $_zFilename = "";
                                }

                            }

                        }


                    }

                }

            } else {

                $_zFilename = "";

            }


            if (!isset($_zFilename) or $_zFilename == "") {

                $_zFilename = null;

            }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//var_dump($photo);die();

            /////////////////////////////// test si categorie existe . sinon eregistrer categorie

            if (!isset($alldecode->agenda[$i]->categorie) or $alldecode->agenda[$i]->categorie == "") {

                $agenda_categ = 70;

            } else {

                $categ = preg_replace('/\nà/', ' ', $alldecode->agenda[$i]->categorie);

                $allcateg = $this->mdlcategorie->getcateg();


                foreach ($allcateg as $category) {


                    $chaine = $category->category;

                    $motif = '/' . $categ . '/i';


                    $chaine2 = $categ;

                    $motif2 = '/' . $category->category . '/i';

                    if (preg_match($motif, $chaine)) {


                        $agenda_categ = $category->agenda_categid;

                        $matchs = "ok";


                    } elseif (preg_match($motif2, $chaine2)) {

                        $agenda_categ = $category->agenda_categid;

                        $matchs = "ok";

                    } elseif ($categ == "humour") {

                        $agenda_categ = "66";

                    } elseif ($categ == "Médiathèque") {

                        $agenda_categ = "112";

                    } elseif ($categ == "Musées") {

                        $agenda_categ = "88";

                    } elseif ($categ == "Caritatif") {

                        $categ = "100";

                    } elseif ($categ == "Vie associative") {

                        $categ = "114";

                    } elseif ($categ == "Foire & marché") {

                        $categ = "59";

                    } elseif ($categ == "Foire & marché") {

                        $categ = "62";

                    } elseif ($categ == "Apéritifs & repas") {

                        $categ = "99";

                    } else {

                        $matchs = null;

                    }

                }

                /*if ($matchs != "ok") {

                    $save_categ = $this->mdlcategorie->save_categ($categ);

                    $agenda_categ = $save_categ;

                }*/

            }

            //////////////////////////////////////////////////////////////////////////////////////////////////////


//var_dump($categ);die();


            /////////////////////////////////si description existe assiger sinon laisser vide//////////////////////////


            if (isset($alldecode->agenda[$i]->description)) {

                $description = $alldecode->agenda[$i]->description;

            } elseif (isset($alldecode->agenda[$i]->url)) {

                $description = $alldecode->agenda[$i]->url;

            }

            ///////////////////////////////   VERIFICATION TYPE : REVUE,ARTICLE ,AGENDA.../////////////////////////

            $type = $this->input->post("type");


//var_dump($type);die();

            ///////////////////////////////////////////////////////////////////////////////////////////////////////


            ///////////////////////////// SI ARTICLE CATEG=ARTICLE_CATEGID   SINON AGENDA_CATEGID


            /////////////////////////////////////////////////////////////////////////////////////

//var_dump($category_name);die();


            //////////////////// ajouter valeur s'il y a agenda_article_type sinon laisser null/////////////////


            if (isset($type) and $type == "article" or $type == "Article") {

                $article_agenda_type = "1";

            } elseif (isset($type) and $type == "agenda" or $type == "Agenda") {

                $article_agenda_type = "2";

            } elseif (isset($type) and $type == "revue" or $type == "Revue") {

                $article_agenda_type = "3";

            } else {

                $this->datatourisme_skipped;

            }

//var_dump($article_agenda_type);die();

///////////////////////////////////////////////////////////////////////////////////////////////////////////

            if (!isset($heure_debut)) {


                $heure_debut = "";


            }


            if (isset($alldecode->agenda[$i]->location)) {

                $exploded_location = explode(" ", $alldecode->agenda[$i]->location);

                foreach ($exploded_location as $is_postal) {

                    $o = explode("\n", $is_postal);

                    if (!empty($o)) {

                        foreach ($o as $s) {

                            if ($s != "" && is_numeric($s) && strlen($s) == 5) {

                                $codepostal_loc = $s;

                            }

                        }

                    }

                }

            }

            $infocom = $this->mdlarticle->get_aboutcom($iduser);


            if ($iduser == "300232") {

                $agenda_categ = '66';

            } elseif ($iduser == "301370") {

                $agenda_categ = '60';

            } elseif ($iduser == "300232" or $iduser == "300222") {

                $agenda_categ = "104";

            } elseif ($iduser == "300221") {


                if (preg_match('/Théâtre/i', $alldecode->agenda[$i]->categorie)) {

                    $agenda_categ = "66";

                } elseif (preg_match('/Danse/i', $alldecode->agenda[$i]->categorie)) {

                    $agenda_categ = "55";

                } elseif (preg_match('/Cirque/i', $alldecode->agenda[$i]->categorie)) {

                    $agenda_categ = "65";

                } elseif (preg_match('/Musique/i', $alldecode->agenda[$i]->categorie)) {

                    $agenda_categ = "61";

                } elseif (preg_match('/Humour/i', $alldecode->agenda[$i]->categorie)) {

                    $agenda_categ = "66";

                } elseif (preg_match('/Création/i', $alldecode->agenda[$i]->categorie)) {

                    $agenda_categ = "66";

                } elseif (preg_match('/Opéra/i', $alldecode->agenda[$i]->categorie)) {

                    $agenda_categ = "60";

                }

            } elseif ($iduser == "301398") {


                $agenda_categ = "117";

            } elseif ($iduser == "301445") {

                $agenda_categ = "88";

            }

            if (isset($alldecode->agenda[$i]->location)) {

                $location_filtred = preg_replace('/Grasse/', '', $alldecode->agenda[$i]->location);

            }

            $loc = $location_filtred ?? null;

            $location = $this->mdlarticle->get_location($loc);

            if (isset($location) and count($location) == 1 and is_object($location)) {

                //var_dump($location);die();

                $location_id = $location->location_id;

            } elseif (count($location) > 1) {

                $location_id = $location[0]->location_id;

            } else {


                $location_datas = array("location" => preg_replace('/Grasse/', '', $loc) ?? $infocom->adresse_localisation ?? '',

                    "location_address" => $loc ?? $infocom->adresse_localisation ?? '',

                    "location_postcode" => $codepostal_loc ?? $infocom->CodePostal ?? '',

                    "location_villeid" => $infocom->IdVille ?? '',

                    "location_departementid" => $infocom->departement_id ?? '',

                    "IdCommercant" => $infocom->IdCommercant ?? '');


                $location_id = $this->mdlarticle->save_location($location_datas);

            }


            $organiser = array(

                "IdCommercant" => $iduser,

                "name" => $infocom->NomSociete ?? '',

                "address1" => $alldecode->agenda[$i]->location ?? $infocom->adresse_localisation ?? '',

                "postal_code" => $codepostal_loc ?? $infocom->CodePostal,

                "department_id" => $infocom->departement_id ?? '0',

                "ville_id" => $infocom->IdVille,

                "tel" => $infocom->TelFixe ?? '',

                "mobile" => $infocom->TelMobile ?? '',

                "email" => $infocom->Email ?? '',

                "website" => $infocom->SiteWeb ?? '',

                "facebook" => $infocom->Facebook ?? '',

                "twitter" => $infocom->Twitter ?? '',

                "googleplus" => $infocom->google_plus ?? '');


            $save_organiser = $this->mdlarticle->save_organiser($organiser);

            $field_article = array('nom_manifestation' => $alldecode->agenda[$i]->titre,


                'date_debut' => $date_debut ?? null,

                'Adresse1' => $alldecode->agenda[$i]->location ?? $infocom->adresse_localisation ?? '',

                'codepostal_localisation' => $codepostal_loc ?? $infocom->CodePostal ?? '',

                'adresse_localisation' => $alldecode->agenda[$i]->location ?? $infocom->adresse_localisation ?? '',

                'telephone' => $infocom->TelFixe ?? '',

                'nom_societe' => $infocom->NomSociete ?? '',

                "organiser_id" => $save_organiser ?? '',

                'date_fin' => $date_fin ?? null,

                'photo1' => $_zFilename,

                'IdCommercant' => $iduser,

                "IdUsers_ionauth" => $user_ion_auth->id,

                "IsActif" => "1",

                "codepostal" => $codepostal_loc ?? $infocom->CodePostal ?? '',

                "IdVille" => $infocom->IdVille_localisation ?? $infocom->IdVille ?? $this->input->post('ville'),

                "article_categid" => $agenda_categ ?? '100',

                "siteweb" => $siteweb,

                "description" => $description,

                "agenda_article_type_id" => $article_agenda_type,

                "date_depot" => date("y-m-d"),

                "IdVille_localisation" => $infocom->IdVille_localisation ?? $infocom->IdVille ?? $this->input->post('ville'),

                "location_id" => $location_id ?? '0',


            );

            if ($iduser == "300221") {

                $codepostale = 06130;

            }

            if ($iduser == "301006") {

                $codepostale = 06400;

                $alldecode->agenda[$i]->adresse1 = "Palais des Festivals et des Congrès";

                $location_filtred = "Palais des Festivals et des Congrès";

            }

            if ($iduser == '301398') {

                $codepostale = 83700;

                $idvillefixed = '33688';

                $locationfixed = '1548';

            }

            if ($iduser == '301399') {

                $codepostale = 83370;

                $idvillefixed = '36941';

                $locationfixed = '1549';

            }

            if (isset($alldecode->agenda[$i]->location)) {

                $location_filtred = preg_replace('/Grasse/', '', $alldecode->agenda[$i]->location);

            }

            $field_agenda = array('nom_manifestation' => $alldecode->agenda[$i]->titre,


                'date_debut' => $date_debut ?? null,

                'Adresse1' => $alldecode->agenda[$i]->adresse1 ?? $location_filtred ?? $infocom->Adresse1 ?? $infocom->adresse_localisation ?? '',

                'codepostal_localisation' => $codepostal_loc ?? $codepostale ?? $infocom->CodePostal ?? '',

                'adresse_localisation' => $location_filtred ?? $infocom->Adresse1 ?? $infocom->adresse_localisation ?? '',

                'telephone' => $alldecode->agenda[$i]->tel ?? $infocom->TelFixe ?? '',

                'nom_societe' => $infocom->NomSociete ?? '',

                "organiser_id" => $save_organiser ?? '',

                "description_tarif" => $infocom->Horaires ?? '',

                "reservation_enligne" => $alldecode->agenda[$i]->reserver_url ?? '',

                'date_fin' => $date_fin ?? null,

                'photo1' => $_zFilename,

                'IdCommercant' => $iduser,

                "IdUsers_ionauth" => $user_ion_auth->id,

                "IsActif" => "1",

                "codepostal" => $codepostal_loc ?? $codepostale ?? $infocom->CodePostal,

                "IdVille" => $idvillefixed ?? $infocom->IdVille_localisation ?? $infocom->IdVille ?? $this->input->post('ville'),

                "agenda_categid" => $agenda_categ ?? '100',

                "siteweb" => $siteweb,

                "description" => $description,

                "agenda_article_type_id" => $article_agenda_type,

                "date_depot" => date("y-m-d"),

                "IdVille_localisation" => $idvillefixed ?? $infocom->IdVille ?? $infocom->IdVille_localisation ?? $this->input->post('ville'),

                "location_id" => $locationfixed ?? $location_id ?? '0',

                "facebook" => $alldecode->agenda[$i]->facebook_url ?? null,

                "twitter" => $alldecode->agenda[$i]->twitter_url ?? null,

            );


//var_dump($field);die();

            /////////////////////////////   ANTI-DOUBLONS   /////////////////////////////////

            if ($type == "article" or $type == "Article") {

                $testexist = $this->mdlarticle->test_revue_exist($alldecode->agenda[$i]->titre);

            } elseif ($type == "agenda" or $type == "Agenda") {

                $testexist = $this->mdl_agenda->test_revue_exist($alldecode->agenda[$i]->titre, $date_debut ?? null);

            } else {

                $testexist = $this->mdlarticle->test_revue_exist($alldecode->agenda[$i]->titre);

            }


//var_dump($testexist);die();

            ////////////////////////////////////////////////////////////////////////////////////


//ar_dump($type);die();

            ///////////////////////////// SI EXISTE IGNORER SINON SAUVEGARGER

            if ($testexist == "0") {


                if (isset($type) and $type == "article" or $type == "Article") {

                    if (isset($alldecode->agenda[$i])) {

                        $objarticle = $this->mdlarticle->save_revue_data($field_article);

                    }


                    if ($objarticle != false) {

                        $field_datetime = array("article_id" => $objarticle,

                            "date_debut" => $date_debut,

                            "date_fin" => $date_fin,

                            "heure_debut" => $heure_debut

                        );


                        $format = "Y-m-d";

                        if (DateTime::createFromFormat($format, $date_debut) and DateTime::createFromFormat($format, $date_fin)) {

                            $dateime = $this->mdlarticle->save_datetime_revue($field_datetime);

                            if ($dateime == "ok") {

                                $this->datatourisme_imported++;

                            } elseif ($dateime == "no") {

                                $this->datatourisme_skipped++;

                            }

                        } else {

                            $this->datatourisme_skipped++;

                        }


                    }

                } elseif (isset($type) and $type == "agenda" or $type == 'Agenda') {

                    $format = "Y-m-d";

                    if (DateTime::createFromFormat($format, $date_debut ?? null) and DateTime::createFromFormat($format, $date_fin ?? null)) {


                        if (isset($alldecode->agenda[$i])) {

                            $objarticle = $this->mdl_agenda->save_revue_data($field_agenda);

                        }


                        if ($objarticle != false) {


                            $field_datetime = array("agenda_id" => $objarticle,

                                "date_debut" => $date_debut,

                                "date_fin" => $date_fin,

                                "heure_debut" => $heure_debut ?? null

                            );

                            $dateime = $this->mdl_agenda->save_datetime_revue($field_datetime);

                            if ($dateime == "ok") {

                                $this->datatourisme_imported++;

                            } elseif ($dateime == "no") {

                                $this->datatourisme_skipped++;

                            }


                        }

                    } else {


                        $this->datatourisme_skipped++;

                    }


                }


            } elseif ($testexist == "1") {

                $this->datatourisme_exists++;

            } else {

                $this->datatourisme_skipped++;

            }

//var_dump($objarticle);die();

            if (!isset($this->datatourisme_imported)) {

                $this->datatourisme_imported = 0;

            }

            $objResult['datatourisme_imported'] = $this->datatourisme_imported;

            $objResult['datatourisme_exists'] = $this->datatourisme_exists;

            $objResult['datatourisme_skipped'] = $this->datatourisme_skipped;


            /////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////add organiser////////////////////////////////////////////////////////


            $http_code = 200;

            header('Content-type: json');

            header('HTTP/1.1: ' . $http_code);

            header('Status: ' . $http_code);

            exit(json_encode((array)$objResult));


        }

    }


    public function get_api($api_key, $token)

    {


        $params = array(

            "api_key" => $api_key,

            "start_template" => "main_template",

            "send_email" => "1"

        );


        $options = array(

            'http' => array(

                'method' => 'POST',

                'header' => 'Content-Type: application/x-www-form-urlencoded; charset=utf-8',

                'content' => http_build_query($params)

            )

        );


        $context = stream_context_create($options);

        $resultat = file_get_contents('https://www.parsehub.com/api/v2/projects/' . $token . '/run', false, $context);


        if ($resultat) {


            $seconds = 10;

            sleep($seconds);

            $params = http_build_query(array(

                "api_key" => $api_key,

                "format" => "json"

            ));


            $result = file_get_contents(

                'https://www.parsehub.com/api/v2/projects/' . $token . '/last_ready_run/data?' . $params,

                false,

                stream_context_create(array(

                    'http' => array(

                        'method' => 'GET'

                    )

                ))

            );

            $is = gzdecode($result);


            return ($is);

        }


    }


    public

    function get_runned_data($api_key, $token)

    {


        $params = http_build_query(array(

            "api_key" => $api_key,

            "format" => "json"

        ));


        $result = file_get_contents(

            'https://www.parsehub.com/api/v2/projects/' . $token . '/last_ready_run/data?' . $params,

            false,

            stream_context_create(array(

                'http' => array(

                    'method' => 'GET'

                )

            ))

        );

        $is = gzdecode($result);


        return ($is);

    }


    public

    function get_count_article()

    {

        $TotalRows = count($this->mdlarticle->listeArticleRecherche(0, 0, 0, "", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", "0", "0"));

        echo '(' . $TotalRows . ')';

    }


    function remove_all_datatourisme_data()

    {

        $this->load->model('mdl_datatourisme_agenda');

        $this->load->model('mdl_agenda');

        $this->load->model('mdl_agenda_datetime');

        $toresult = $this->mdl_datatourisme_agenda->getAll();

        foreach ($toresult as $item) {

            echo $item->agenda_id . " -> " . $item->datatourisme_id . "<br/>";

            $toresult_updated = $this->mdl_agenda->delete_definitif($item->agenda_id);

            if ($toresult_updated) $this->mdl_datatourisme_agenda->delet_by_agenda($item->agenda_id);

            if ($toresult_updated) $this->mdl_agenda_datetime->deleteByAgendaId($item->agenda_id);

        }

    }

    public function getnb_categ()
    {

        $tiville_posted = $this->input->get("filter_ville");

        $categ_filter = $this->input->get("categ_filter");

        if (!isset($tiville_posted)) {

            $tiville_posted = "0";

        }

        if (!isset($categ_filter)) {

            $categ_filter = "0";

        }


        $toAgenda = $this->mdlarticle->listeArticleRecherche_filtre_categ_perso($categ_filter, $tiDepartement_array = 0, 0, $keyword_input_export = "", 0, 0, 10000, "", $proximite_tiDatefilter = "0", "0000-00-00", "0000-00-00", $idCommercant_to_search = "0", "0", 0, $tiville_posted);


        echo count($toAgenda);

    }

    public function get_article_now()
    {

        ////$this->firephp->log($_REQUEST, '_REQUEST');


        $zCouleur = $this->input->get("zCouleur");

        if (!isset($zCouleur)) $zCouleur = $this->input->post("zCouleur");

        if (isset($zCouleur) && $zCouleur != "") $data['zCouleur'] = $zCouleur;

        $zCouleurTitre = $this->input->get("zCouleurTitre");

        if (!isset($zCouleurTitre)) $zCouleurTitre = $this->input->post("zCouleurTitre");

        if (isset($zCouleurTitre) && $zCouleurTitre != "") $data['zCouleurTitre'] = $zCouleurTitre;

        $zCouleurTextBouton = $this->input->get("zCouleurTextBouton");

        if (!isset($zCouleurTextBouton)) $zCouleurTextBouton = $this->input->post("zCouleurTextBouton");

        if (isset($zCouleurTextBouton) && $zCouleurTextBouton != "") $data['zCouleurTextBouton'] = $zCouleurTextBouton;

        $zCouleurBgBouton = $this->input->get("zCouleurBgBouton");

        if (!isset($zCouleurBgBouton)) $zCouleurBgBouton = $this->input->post("zCouleurBgBouton");

        if (isset($zCouleurBgBouton) && $zCouleurBgBouton != "") $data['zCouleurBgBouton'] = $zCouleurBgBouton;

        $zCouleurNbBtn = $this->input->get("zCouleurNbBtn");

        if (!isset($zCouleurNbBtn)) $zCouleurNbBtn = $this->input->post("zCouleurNbBtn");

        if (isset($zCouleurNbBtn) && $zCouleurNbBtn != "") $data['zCouleurNbBtn'] = $zCouleurNbBtn;


        $tiDepartement = $this->input->get("tiDepartement");

        if (!isset($tiDepartement)) $tiDepartement = $this->input->post("tiDepartement");

        if (isset($tiDepartement) && $tiDepartement != "") {

            $tiDepartement = substr_replace($tiDepartement, "", -1);

            $tiDepartement_array = explode("_", $tiDepartement);

            $data['tiDepartement_array'] = $tiDepartement;

        } else {

            $tiDepartement_array = "0";

        }

        $tiDeposant = $this->input->get("tiDeposant");

        if (!isset($tiDeposant)) $tiDeposant = $this->input->post("tiDeposant");

        if (isset($tiDeposant) && $tiDeposant != "") {

            $tiDeposant = substr_replace($tiDeposant, "", -1);

            $tiDeposant_array = explode("_", $tiDeposant);

            $data['tiDeposant_array'] = $tiDeposant;

        }

        $tiCategorie = $this->input->get("tiCategorie");

        if (!isset($tiCategorie)) $tiCategorie = $this->input->post("tiCategorie");

        if (isset($tiCategorie) && $tiCategorie != "") {

            $tiCategorie = substr_replace($tiCategorie, "", -1);

            $data["tiCategorie_for_init"] = $tiCategorie;

            $tiCategorie_array = explode("_", $tiCategorie);

            //$data['tiCategorie_array'] = $tiCategorie_array;

        } else {

            $tiCategorie_array = "0";

        }

        $tiDossperso = $this->input->get("tiDossperso");

        if (!isset($tiDossperso)) $tiDossperso = $this->input->post("tiDossperso");

        $data["tiDossperso"] = $tiDossperso;

        if (isset($tiDossperso) && $tiDossperso != "0") {

            $idCommercant_to_search = $tiDossperso;

        } else if (isset($tiDeposant_array)) {

            $idCommercant_to_search = $tiDeposant_array;

        } else {

            $idCommercant_to_search = "0";

        }


        $contentonly = $this->input->get("contentonly");

        if (!isset($contentonly)) $contentonly = $this->input->post("contentonly");

        $data["contentonly"] = $contentonly;


        $proximite_tiDatefilter = $this->input->get("proximite_tiDatefilter");

        if (!isset($proximite_tiDatefilter)) $proximite_tiDatefilter = $this->input->post("proximite_tiDatefilter");

        if (!isset($proximite_tiDatefilter)) $proximite_tiDatefilter = "0";

        $data["proximite_tiDatefilter"] = $proximite_tiDatefilter;


        $keyword_input_export = $this->input->get("keyword_input_export");

        if (!isset($keyword_input_export)) $keyword_input_export = $this->input->post("keyword_input_export");

        $data["keyword_input_export"] = $keyword_input_export;


        //var_dump($data); die();


        /////// filter by villes //////////////


        $tiville_posted = $this->input->get("ville_filter");

        if ($tiville_posted == "" || $tiville_posted == "0") {

            $tiville_posted = '0';

        }

        if (isset($tiville_posted) && $tiville_posted != null) {

            $this->session->set_userdata('ville_filtred', $tiville_posted);

        }

        $filter_ville = $this->session->userdata('ville_filtred');

        if (isset($filter_ville) and $filter_ville != null) {

            $data["tiville"] = $filter_ville;

            $tiville = $filter_ville;

        }

        if (!isset($tiville)) {

            $tiville = 0;

        }


        $data["allville"] = $this->mdlarticle->getarticle_join_ville($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", $proximite_tiDatefilter, "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 0, $tiville);

        $toAgenda = $this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", $proximite_tiDatefilter, "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville);

        $data['toAgenda'] = $toAgenda;


        //$data['toAgndaTout_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaAujourdhui_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "101", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaWeekend_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "202", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaSemaine_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "303", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaSemproch_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "404", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaMois_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "505", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        /*

        $data['toAgndaJanvier_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "01", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaFevrier_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "02", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaMars_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "03", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaAvril_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "04", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaMai_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "05", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaJuin_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "06", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaJuillet_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "07", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaAout_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "08", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaSept_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "09", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaOct_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "10", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaNov_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "11", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaDec_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "12", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        */


        $tiCategorie_list_array = $this->mdlarticle->listeArticleRecherche_article_perso_liste_categ($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0);

        $data['tiCategorie_list_array'] = $tiCategorie_list_array;


        $data['defaul_thumb_width'] = $this->config->item('defaul_thumb_width');

        $data['defaul_thumb_height'] = $this->config->item('defaul_thumb_height');

        $data['currentpage'] = "export_article";

        //$this->load->view('agendaAout2013/agenda_perso', $data);

        //$this->load->view('privicarte/agenda_perso', $data);

        echo "Aujourd'hui (" . $data['toAgndaAujourdhui_global'] . ")";

    }


    public function get_article_week()
    {

        ////$this->firephp->log($_REQUEST, '_REQUEST');


        $zCouleur = $this->input->get("zCouleur");

        if (!isset($zCouleur)) $zCouleur = $this->input->post("zCouleur");

        if (isset($zCouleur) && $zCouleur != "") $data['zCouleur'] = $zCouleur;

        $zCouleurTitre = $this->input->get("zCouleurTitre");

        if (!isset($zCouleurTitre)) $zCouleurTitre = $this->input->post("zCouleurTitre");

        if (isset($zCouleurTitre) && $zCouleurTitre != "") $data['zCouleurTitre'] = $zCouleurTitre;

        $zCouleurTextBouton = $this->input->get("zCouleurTextBouton");

        if (!isset($zCouleurTextBouton)) $zCouleurTextBouton = $this->input->post("zCouleurTextBouton");

        if (isset($zCouleurTextBouton) && $zCouleurTextBouton != "") $data['zCouleurTextBouton'] = $zCouleurTextBouton;

        $zCouleurBgBouton = $this->input->get("zCouleurBgBouton");

        if (!isset($zCouleurBgBouton)) $zCouleurBgBouton = $this->input->post("zCouleurBgBouton");

        if (isset($zCouleurBgBouton) && $zCouleurBgBouton != "") $data['zCouleurBgBouton'] = $zCouleurBgBouton;

        $zCouleurNbBtn = $this->input->get("zCouleurNbBtn");

        if (!isset($zCouleurNbBtn)) $zCouleurNbBtn = $this->input->post("zCouleurNbBtn");

        if (isset($zCouleurNbBtn) && $zCouleurNbBtn != "") $data['zCouleurNbBtn'] = $zCouleurNbBtn;


        $tiDepartement = $this->input->get("tiDepartement");

        if (!isset($tiDepartement)) $tiDepartement = $this->input->post("tiDepartement");

        if (isset($tiDepartement) && $tiDepartement != "") {

            $tiDepartement = substr_replace($tiDepartement, "", -1);

            $tiDepartement_array = explode("_", $tiDepartement);

            $data['tiDepartement_array'] = $tiDepartement;

        } else {

            $tiDepartement_array = "0";

        }

        $tiDeposant = $this->input->get("tiDeposant");

        if (!isset($tiDeposant)) $tiDeposant = $this->input->post("tiDeposant");

        if (isset($tiDeposant) && $tiDeposant != "") {

            $tiDeposant = substr_replace($tiDeposant, "", -1);

            $tiDeposant_array = explode("_", $tiDeposant);

            $data['tiDeposant_array'] = $tiDeposant;

        }

        $tiCategorie = $this->input->get("tiCategorie");

        if (!isset($tiCategorie)) $tiCategorie = $this->input->post("tiCategorie");

        if (isset($tiCategorie) && $tiCategorie != "") {

            $tiCategorie = substr_replace($tiCategorie, "", -1);

            $data["tiCategorie_for_init"] = $tiCategorie;

            $tiCategorie_array = explode("_", $tiCategorie);

            //$data['tiCategorie_array'] = $tiCategorie_array;

        } else {

            $tiCategorie_array = "0";

        }

        $tiDossperso = $this->input->get("tiDossperso");

        if (!isset($tiDossperso)) $tiDossperso = $this->input->post("tiDossperso");

        $data["tiDossperso"] = $tiDossperso;

        if (isset($tiDossperso) && $tiDossperso != "0") {

            $idCommercant_to_search = $tiDossperso;

        } else if (isset($tiDeposant_array)) {

            $idCommercant_to_search = $tiDeposant_array;

        } else {

            $idCommercant_to_search = "0";

        }


        $contentonly = $this->input->get("contentonly");

        if (!isset($contentonly)) $contentonly = $this->input->post("contentonly");

        $data["contentonly"] = $contentonly;


        $proximite_tiDatefilter = $this->input->get("proximite_tiDatefilter");

        if (!isset($proximite_tiDatefilter)) $proximite_tiDatefilter = $this->input->post("proximite_tiDatefilter");

        if (!isset($proximite_tiDatefilter)) $proximite_tiDatefilter = "0";

        $data["proximite_tiDatefilter"] = $proximite_tiDatefilter;


        $keyword_input_export = $this->input->get("keyword_input_export");

        if (!isset($keyword_input_export)) $keyword_input_export = $this->input->post("keyword_input_export");

        $data["keyword_input_export"] = $keyword_input_export;


        //var_dump($data); die();


        /////// filter by villes //////////////


        $tiville_posted = $this->input->get("ville_filter");

        if ($tiville_posted == "" || $tiville_posted == "0") {

            $tiville_posted = '0';

        }

        if (isset($tiville_posted) && $tiville_posted != null) {

            $this->session->set_userdata('ville_filtred', $tiville_posted);

        }

        $filter_ville = $this->session->userdata('ville_filtred');

        if (isset($filter_ville) and $filter_ville != null) {

            $data["tiville"] = $filter_ville;

            $tiville = $filter_ville;

        }

        if (!isset($tiville)) {

            $tiville = 0;

        }


        $data["allville"] = $this->mdlarticle->getarticle_join_ville($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", $proximite_tiDatefilter, "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 0, $tiville);

        $toAgenda = $this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", $proximite_tiDatefilter, "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville);

        $data['toAgenda'] = $toAgenda;


        //$data['toAgndaTout_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaAujourdhui_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "101", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaWeekend_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "202", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaSemaine_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "303", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaSemproch_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "404", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaMois_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "505", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        /*

        $data['toAgndaJanvier_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "01", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaFevrier_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "02", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaMars_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "03", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaAvril_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "04", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaMai_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "05", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaJuin_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "06", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaJuillet_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "07", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaAout_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "08", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaSept_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "09", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaOct_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "10", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaNov_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "11", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaDec_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "12", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        */


        $tiCategorie_list_array = $this->mdlarticle->listeArticleRecherche_article_perso_liste_categ($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0);

        $data['tiCategorie_list_array'] = $tiCategorie_list_array;


        $data['defaul_thumb_width'] = $this->config->item('defaul_thumb_width');

        $data['defaul_thumb_height'] = $this->config->item('defaul_thumb_height');

        $data['currentpage'] = "export_article";

        //$this->load->view('agendaAout2013/agenda_perso', $data);

        //$this->load->view('privicarte/agenda_perso', $data);

        echo "Ce Week-end (" . $data['toAgndaWeekend_global'] . ")";

    }


    public function get_article_full_week()
    {

        ////$this->firephp->log($_REQUEST, '_REQUEST');


        $zCouleur = $this->input->get("zCouleur");

        if (!isset($zCouleur)) $zCouleur = $this->input->post("zCouleur");

        if (isset($zCouleur) && $zCouleur != "") $data['zCouleur'] = $zCouleur;

        $zCouleurTitre = $this->input->get("zCouleurTitre");

        if (!isset($zCouleurTitre)) $zCouleurTitre = $this->input->post("zCouleurTitre");

        if (isset($zCouleurTitre) && $zCouleurTitre != "") $data['zCouleurTitre'] = $zCouleurTitre;

        $zCouleurTextBouton = $this->input->get("zCouleurTextBouton");

        if (!isset($zCouleurTextBouton)) $zCouleurTextBouton = $this->input->post("zCouleurTextBouton");

        if (isset($zCouleurTextBouton) && $zCouleurTextBouton != "") $data['zCouleurTextBouton'] = $zCouleurTextBouton;

        $zCouleurBgBouton = $this->input->get("zCouleurBgBouton");

        if (!isset($zCouleurBgBouton)) $zCouleurBgBouton = $this->input->post("zCouleurBgBouton");

        if (isset($zCouleurBgBouton) && $zCouleurBgBouton != "") $data['zCouleurBgBouton'] = $zCouleurBgBouton;

        $zCouleurNbBtn = $this->input->get("zCouleurNbBtn");

        if (!isset($zCouleurNbBtn)) $zCouleurNbBtn = $this->input->post("zCouleurNbBtn");

        if (isset($zCouleurNbBtn) && $zCouleurNbBtn != "") $data['zCouleurNbBtn'] = $zCouleurNbBtn;


        $tiDepartement = $this->input->get("tiDepartement");

        if (!isset($tiDepartement)) $tiDepartement = $this->input->post("tiDepartement");

        if (isset($tiDepartement) && $tiDepartement != "") {

            $tiDepartement = substr_replace($tiDepartement, "", -1);

            $tiDepartement_array = explode("_", $tiDepartement);

            $data['tiDepartement_array'] = $tiDepartement;

        } else {

            $tiDepartement_array = "0";

        }

        $tiDeposant = $this->input->get("tiDeposant");

        if (!isset($tiDeposant)) $tiDeposant = $this->input->post("tiDeposant");

        if (isset($tiDeposant) && $tiDeposant != "") {

            $tiDeposant = substr_replace($tiDeposant, "", -1);

            $tiDeposant_array = explode("_", $tiDeposant);

            $data['tiDeposant_array'] = $tiDeposant;

        }

        $tiCategorie = $this->input->get("tiCategorie");

        if (!isset($tiCategorie)) $tiCategorie = $this->input->post("tiCategorie");

        if (isset($tiCategorie) && $tiCategorie != "") {

            $tiCategorie = substr_replace($tiCategorie, "", -1);

            $data["tiCategorie_for_init"] = $tiCategorie;

            $tiCategorie_array = explode("_", $tiCategorie);

            //$data['tiCategorie_array'] = $tiCategorie_array;

        } else {

            $tiCategorie_array = "0";

        }

        $tiDossperso = $this->input->get("tiDossperso");

        if (!isset($tiDossperso)) $tiDossperso = $this->input->post("tiDossperso");

        $data["tiDossperso"] = $tiDossperso;

        if (isset($tiDossperso) && $tiDossperso != "0") {

            $idCommercant_to_search = $tiDossperso;

        } else if (isset($tiDeposant_array)) {

            $idCommercant_to_search = $tiDeposant_array;

        } else {

            $idCommercant_to_search = "0";

        }


        $contentonly = $this->input->get("contentonly");

        if (!isset($contentonly)) $contentonly = $this->input->post("contentonly");

        $data["contentonly"] = $contentonly;


        $proximite_tiDatefilter = $this->input->get("proximite_tiDatefilter");

        if (!isset($proximite_tiDatefilter)) $proximite_tiDatefilter = $this->input->post("proximite_tiDatefilter");

        if (!isset($proximite_tiDatefilter)) $proximite_tiDatefilter = "0";

        $data["proximite_tiDatefilter"] = $proximite_tiDatefilter;


        $keyword_input_export = $this->input->get("keyword_input_export");

        if (!isset($keyword_input_export)) $keyword_input_export = $this->input->post("keyword_input_export");

        $data["keyword_input_export"] = $keyword_input_export;


        //var_dump($data); die();


        /////// filter by villes //////////////


        $tiville_posted = $this->input->get("ville_filter");

        if ($tiville_posted == "" || $tiville_posted == "0") {

            $tiville_posted = '0';

        }

        if (isset($tiville_posted) && $tiville_posted != null) {

            $this->session->set_userdata('ville_filtred', $tiville_posted);

        }

        $filter_ville = $this->session->userdata('ville_filtred');

        if (isset($filter_ville) and $filter_ville != null) {

            $data["tiville"] = $filter_ville;

            $tiville = $filter_ville;

        }

        if (!isset($tiville)) {

            $tiville = 0;

        }


        $data["allville"] = $this->mdlarticle->getarticle_join_ville($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", $proximite_tiDatefilter, "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 0, $tiville);

        $toAgenda = $this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", $proximite_tiDatefilter, "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville);

        $data['toAgenda'] = $toAgenda;


        //$data['toAgndaTout_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaAujourdhui_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "101", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaWeekend_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "202", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaSemaine_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "303", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaSemproch_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "404", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaMois_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "505", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        /*

        $data['toAgndaJanvier_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "01", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaFevrier_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "02", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaMars_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "03", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaAvril_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "04", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaMai_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "05", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaJuin_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "06", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaJuillet_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "07", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaAout_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "08", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaSept_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "09", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaOct_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "10", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaNov_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "11", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaDec_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "12", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        */


        $tiCategorie_list_array = $this->mdlarticle->listeArticleRecherche_article_perso_liste_categ($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0);

        $data['tiCategorie_list_array'] = $tiCategorie_list_array;


        $data['defaul_thumb_width'] = $this->config->item('defaul_thumb_width');

        $data['defaul_thumb_height'] = $this->config->item('defaul_thumb_height');

        $data['currentpage'] = "export_article";

        //$this->load->view('agendaAout2013/agenda_perso', $data);

        //$this->load->view('privicarte/agenda_perso', $data);

        echo "Cette semaine (" . $data['toAgndaSemaine_global'] . ")";

    }


    public function get_article_next_week()
    {

        ////$this->firephp->log($_REQUEST, '_REQUEST');


        $zCouleur = $this->input->get("zCouleur");

        if (!isset($zCouleur)) $zCouleur = $this->input->post("zCouleur");

        if (isset($zCouleur) && $zCouleur != "") $data['zCouleur'] = $zCouleur;

        $zCouleurTitre = $this->input->get("zCouleurTitre");

        if (!isset($zCouleurTitre)) $zCouleurTitre = $this->input->post("zCouleurTitre");

        if (isset($zCouleurTitre) && $zCouleurTitre != "") $data['zCouleurTitre'] = $zCouleurTitre;

        $zCouleurTextBouton = $this->input->get("zCouleurTextBouton");

        if (!isset($zCouleurTextBouton)) $zCouleurTextBouton = $this->input->post("zCouleurTextBouton");

        if (isset($zCouleurTextBouton) && $zCouleurTextBouton != "") $data['zCouleurTextBouton'] = $zCouleurTextBouton;

        $zCouleurBgBouton = $this->input->get("zCouleurBgBouton");

        if (!isset($zCouleurBgBouton)) $zCouleurBgBouton = $this->input->post("zCouleurBgBouton");

        if (isset($zCouleurBgBouton) && $zCouleurBgBouton != "") $data['zCouleurBgBouton'] = $zCouleurBgBouton;

        $zCouleurNbBtn = $this->input->get("zCouleurNbBtn");

        if (!isset($zCouleurNbBtn)) $zCouleurNbBtn = $this->input->post("zCouleurNbBtn");

        if (isset($zCouleurNbBtn) && $zCouleurNbBtn != "") $data['zCouleurNbBtn'] = $zCouleurNbBtn;


        $tiDepartement = $this->input->get("tiDepartement");

        if (!isset($tiDepartement)) $tiDepartement = $this->input->post("tiDepartement");

        if (isset($tiDepartement) && $tiDepartement != "") {

            $tiDepartement = substr_replace($tiDepartement, "", -1);

            $tiDepartement_array = explode("_", $tiDepartement);

            $data['tiDepartement_array'] = $tiDepartement;

        } else {

            $tiDepartement_array = "0";

        }

        $tiDeposant = $this->input->get("tiDeposant");

        if (!isset($tiDeposant)) $tiDeposant = $this->input->post("tiDeposant");

        if (isset($tiDeposant) && $tiDeposant != "") {

            $tiDeposant = substr_replace($tiDeposant, "", -1);

            $tiDeposant_array = explode("_", $tiDeposant);

            $data['tiDeposant_array'] = $tiDeposant;

        }

        $tiCategorie = $this->input->get("tiCategorie");

        if (!isset($tiCategorie)) $tiCategorie = $this->input->post("tiCategorie");

        if (isset($tiCategorie) && $tiCategorie != "") {

            $tiCategorie = substr_replace($tiCategorie, "", -1);

            $data["tiCategorie_for_init"] = $tiCategorie;

            $tiCategorie_array = explode("_", $tiCategorie);

            //$data['tiCategorie_array'] = $tiCategorie_array;

        } else {

            $tiCategorie_array = "0";

        }

        $tiDossperso = $this->input->get("tiDossperso");

        if (!isset($tiDossperso)) $tiDossperso = $this->input->post("tiDossperso");

        $data["tiDossperso"] = $tiDossperso;

        if (isset($tiDossperso) && $tiDossperso != "0") {

            $idCommercant_to_search = $tiDossperso;

        } else if (isset($tiDeposant_array)) {

            $idCommercant_to_search = $tiDeposant_array;

        } else {

            $idCommercant_to_search = "0";

        }


        $contentonly = $this->input->get("contentonly");

        if (!isset($contentonly)) $contentonly = $this->input->post("contentonly");

        $data["contentonly"] = $contentonly;


        $proximite_tiDatefilter = $this->input->get("proximite_tiDatefilter");

        if (!isset($proximite_tiDatefilter)) $proximite_tiDatefilter = $this->input->post("proximite_tiDatefilter");

        if (!isset($proximite_tiDatefilter)) $proximite_tiDatefilter = "0";

        $data["proximite_tiDatefilter"] = $proximite_tiDatefilter;


        $keyword_input_export = $this->input->get("keyword_input_export");

        if (!isset($keyword_input_export)) $keyword_input_export = $this->input->post("keyword_input_export");

        $data["keyword_input_export"] = $keyword_input_export;


        //var_dump($data); die();


        /////// filter by villes //////////////


        $tiville_posted = $this->input->get("ville_filter");

        if ($tiville_posted == "" || $tiville_posted == "0") {

            $tiville_posted = '0';

        }

        if (isset($tiville_posted) && $tiville_posted != null) {

            $this->session->set_userdata('ville_filtred', $tiville_posted);

        }

        $filter_ville = $this->session->userdata('ville_filtred');

        if (isset($filter_ville) and $filter_ville != null) {

            $data["tiville"] = $filter_ville;

            $tiville = $filter_ville;

        }

        if (!isset($tiville)) {

            $tiville = 0;

        }


        $data["allville"] = $this->mdlarticle->getarticle_join_ville($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", $proximite_tiDatefilter, "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 0, $tiville);

        $toAgenda = $this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", $proximite_tiDatefilter, "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville);

        $data['toAgenda'] = $toAgenda;


        //$data['toAgndaTout_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaAujourdhui_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "101", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaWeekend_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "202", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaSemaine_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "303", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaSemproch_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "404", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaMois_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "505", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        /*

        $data['toAgndaJanvier_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "01", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaFevrier_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "02", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaMars_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "03", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaAvril_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "04", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaMai_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "05", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaJuin_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "06", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaJuillet_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "07", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaAout_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "08", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaSept_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "09", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaOct_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "10", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaNov_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "11", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaDec_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "12", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        */


        $tiCategorie_list_array = $this->mdlarticle->listeArticleRecherche_article_perso_liste_categ($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0);

        $data['tiCategorie_list_array'] = $tiCategorie_list_array;


        $data['defaul_thumb_width'] = $this->config->item('defaul_thumb_width');

        $data['defaul_thumb_height'] = $this->config->item('defaul_thumb_height');

        $data['currentpage'] = "export_article";

        //$this->load->view('agendaAout2013/agenda_perso', $data);

        //$this->load->view('privicarte/agenda_perso', $data);

        echo "Semaine prochaine (" . $data['toAgndaSemproch_global'] . ")";

    }


    public function get_article_next_months()
    {

        ////$this->firephp->log($_REQUEST, '_REQUEST');


        $zCouleur = $this->input->get("zCouleur");

        if (!isset($zCouleur)) $zCouleur = $this->input->post("zCouleur");

        if (isset($zCouleur) && $zCouleur != "") $data['zCouleur'] = $zCouleur;

        $zCouleurTitre = $this->input->get("zCouleurTitre");

        if (!isset($zCouleurTitre)) $zCouleurTitre = $this->input->post("zCouleurTitre");

        if (isset($zCouleurTitre) && $zCouleurTitre != "") $data['zCouleurTitre'] = $zCouleurTitre;

        $zCouleurTextBouton = $this->input->get("zCouleurTextBouton");

        if (!isset($zCouleurTextBouton)) $zCouleurTextBouton = $this->input->post("zCouleurTextBouton");

        if (isset($zCouleurTextBouton) && $zCouleurTextBouton != "") $data['zCouleurTextBouton'] = $zCouleurTextBouton;

        $zCouleurBgBouton = $this->input->get("zCouleurBgBouton");

        if (!isset($zCouleurBgBouton)) $zCouleurBgBouton = $this->input->post("zCouleurBgBouton");

        if (isset($zCouleurBgBouton) && $zCouleurBgBouton != "") $data['zCouleurBgBouton'] = $zCouleurBgBouton;

        $zCouleurNbBtn = $this->input->get("zCouleurNbBtn");

        if (!isset($zCouleurNbBtn)) $zCouleurNbBtn = $this->input->post("zCouleurNbBtn");

        if (isset($zCouleurNbBtn) && $zCouleurNbBtn != "") $data['zCouleurNbBtn'] = $zCouleurNbBtn;


        $tiDepartement = $this->input->get("tiDepartement");

        if (!isset($tiDepartement)) $tiDepartement = $this->input->post("tiDepartement");

        if (isset($tiDepartement) && $tiDepartement != "") {

            $tiDepartement = substr_replace($tiDepartement, "", -1);

            $tiDepartement_array = explode("_", $tiDepartement);

            $data['tiDepartement_array'] = $tiDepartement;

        } else {

            $tiDepartement_array = "0";

        }

        $tiDeposant = $this->input->get("tiDeposant");

        if (!isset($tiDeposant)) $tiDeposant = $this->input->post("tiDeposant");

        if (isset($tiDeposant) && $tiDeposant != "") {

            $tiDeposant = substr_replace($tiDeposant, "", -1);

            $tiDeposant_array = explode("_", $tiDeposant);

            $data['tiDeposant_array'] = $tiDeposant;

        }

        $tiCategorie = $this->input->get("tiCategorie");

        if (!isset($tiCategorie)) $tiCategorie = $this->input->post("tiCategorie");

        if (isset($tiCategorie) && $tiCategorie != "") {

            $tiCategorie = substr_replace($tiCategorie, "", -1);

            $data["tiCategorie_for_init"] = $tiCategorie;

            $tiCategorie_array = explode("_", $tiCategorie);

            //$data['tiCategorie_array'] = $tiCategorie_array;

        } else {

            $tiCategorie_array = "0";

        }

        $tiDossperso = $this->input->get("tiDossperso");

        if (!isset($tiDossperso)) $tiDossperso = $this->input->post("tiDossperso");

        $data["tiDossperso"] = $tiDossperso;

        if (isset($tiDossperso) && $tiDossperso != "0") {

            $idCommercant_to_search = $tiDossperso;

        } else if (isset($tiDeposant_array)) {

            $idCommercant_to_search = $tiDeposant_array;

        } else {

            $idCommercant_to_search = "0";

        }


        $contentonly = $this->input->get("contentonly");

        if (!isset($contentonly)) $contentonly = $this->input->post("contentonly");

        $data["contentonly"] = $contentonly;


        $proximite_tiDatefilter = $this->input->get("proximite_tiDatefilter");

        if (!isset($proximite_tiDatefilter)) $proximite_tiDatefilter = $this->input->post("proximite_tiDatefilter");

        if (!isset($proximite_tiDatefilter)) $proximite_tiDatefilter = "0";

        $data["proximite_tiDatefilter"] = $proximite_tiDatefilter;


        $keyword_input_export = $this->input->get("keyword_input_export");

        if (!isset($keyword_input_export)) $keyword_input_export = $this->input->post("keyword_input_export");

        $data["keyword_input_export"] = $keyword_input_export;


        //var_dump($data); die();


        /////// filter by villes //////////////


        $tiville_posted = $this->input->get("ville_filter");

        if ($tiville_posted == "" || $tiville_posted == "0") {

            $tiville_posted = '0';

        }

        if (isset($tiville_posted) && $tiville_posted != null) {

            $this->session->set_userdata('ville_filtred', $tiville_posted);

        }

        $filter_ville = $this->session->userdata('ville_filtred');

        if (isset($filter_ville) and $filter_ville != null) {

            $data["tiville"] = $filter_ville;

            $tiville = $filter_ville;

        }

        if (!isset($tiville)) {

            $tiville = 0;

        }


        $data["allville"] = $this->mdlarticle->getarticle_join_ville($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", $proximite_tiDatefilter, "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, 0, $tiville);

        $toAgenda = $this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", $proximite_tiDatefilter, "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville);

        $data['toAgenda'] = $toAgenda;


        //$data['toAgndaTout_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaAujourdhui_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "101", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaWeekend_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "202", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaSemaine_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "303", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaSemproch_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "404", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        $data['toAgndaMois_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, $keyword_input_export, 0, 0, 10000, "", "505", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0, $tiville));

        /*

        $data['toAgndaJanvier_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "01", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaFevrier_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "02", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaMars_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "03", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaAvril_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "04", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaMai_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "05", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaJuin_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "06", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaJuillet_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "07", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaAout_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "08", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaSept_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "09", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaOct_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "10", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaNov_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "11", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        $data['toAgndaDec_global'] = count($this->mdlarticle->listeArticleRecherche_filtre_article_perso($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "12", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0));

        */


        $tiCategorie_list_array = $this->mdlarticle->listeArticleRecherche_article_perso_liste_categ($tiCategorie_array, $tiDepartement_array, 0, "", 0, 0, 10000, "", "0", "0000-00-00", "0000-00-00", $idCommercant_to_search, "0", 0);

        $data['tiCategorie_list_array'] = $tiCategorie_list_array;


        $data['defaul_thumb_width'] = $this->config->item('defaul_thumb_width');

        $data['defaul_thumb_height'] = $this->config->item('defaul_thumb_height');

        $data['currentpage'] = "export_article";

        //$this->load->view('agendaAout2013/agenda_perso', $data);

        //$this->load->view('privicarte/agenda_perso', $data);

        echo "Ce mois (" . $data['toAgndaMois_global'] . ")";

    }

}