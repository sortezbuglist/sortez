<?php
class magazine extends CI_Controller{

    function __construct() {
        parent::__construct();
        $this->load->model("mdlannonce") ;
        $this->load->model("mdlcommercant") ;
        $this->load->model("mdlbonplan") ;
        $this->load->model("mdlcategorie") ;
        $this->load->model("mdlville") ;
        $this->load->model("mdlcommercantpagination") ;
        $this->load->model("sousRubrique") ;
        $this->load->model("AssCommercantAbonnement") ;
        $this->load->model("AssCommercantSousRubrique") ;
        $this->load->model("Commercant") ;
        $this->load->model("user") ;

        $this->load->library('user_agent');

        $this->load->library("pagination");

        $this->load->library('image_moo');

        $this->load->library('session');

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");


        check_vivresaville_id_ville();

    }


    function index() {
        $this->infoscarte();
    }


    function infoscarte() {
        $data ="";
        $this->load->view('frontAout2013/infoscarte', $data) ;
    }


    function infospro() {
        $data ="";
        $this->load->view('frontAout2013/infospro', $data) ;
    }

}