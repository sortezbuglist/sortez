<?php
class sortez_pro_mobile extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model ( "mdl_card" );
        $this->load->model ( "mdl_card_bonplan_used" );
        $this->load->model ( "mdl_card_capital" );
        $this->load->model ( "mdl_card_tampon" );
        $this->load->model ( "mdl_card_remise" );
        $this->load->model ( "mdl_card_user_link" );
        $this->load->model ( "mdl_card_fiche_client_tampon" );
        $this->load->model ( "mdl_card_fiche_client_capital" );
        $this->load->model ( "mdl_card_fiche_client_remise" );
        $this->load->model ( 'assoc_client_bonplan_model' );
        $this->load->model ( 'assoc_client_commercant_model' );
        $this->load->Model ( "Ville" );
        $this->load->model ( "user" );
        $this->load->model ( "mdlbonplan" );
        $this->load->model ( "commercant" );

        $this->load->library ( 'session' );

        $this->load->library ( 'ion_auth' );
        $this->load->model ( "ion_auth_used_by_club" );
        $this->load->library('ion_auth');
        $this->load->model("api_model");
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->output->set_header('Access-Control-Allow-Origin: null');  header('Access-Control-Allow-Credentials: omit');
        header('Access-Control-Allow-Method:PUT,GET,POST,DELETE,OPTIONS');
        header('AccessControl-Allow-Headers:Content-Type;x-xsrf-token');
    }
    public function tester(){
        echo "mande ";
    }
    public function get_login_by_id(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $login=$_POST["login"];
        $request=$this->api_model->get_login_by_id($login);
        $json['json']=$request;
        echo json_encode($json);
    }
    public function get_client_list(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $login=$_POST["login"];
        //$login="randawilly8@gmail.com";
        $data=$this->api_model->get_id_commercant($login);
        $clientlist=$this->api_model->getall($data);
        foreach ($clientlist as $data){
            $result=$this->api_model->get_name($data);
            $json['json']=$result;
            echo json_encode($json);
        }

        //$json['json']=$clientlist;
        //echo json_encode($json);

    }
    public function liste_clients($action = false, $nb = null) {

        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $login=$_POST["login"];
        //$login="randawilly8@gmail.com";
        $datas=$this->api_model->get_id_commercant($login);
        $id_commercant =$datas->IdCommercant;
        // $data ['titre'] = 'Liste de mes clients';
        //$data ['print'] = 0;
        // $data ['email_clients'] = array ();
        // $data ['mail_to_clients'] = '';
        //$data ['show_bonplan'] = 1;
        // $data ['show_offre'] = 1;
        $criteres = array ();
        if ($action == "mois" && $nb != null) {
            $criteres ['month'] = $nb;
        }

        $mail = "";
        $list_client = $this->assoc_client_commercant_model->get_by_commercant_id ( $id_commercant, $criteres );
        $email_clients = array ();
        if (! empty ( $list_client )) {
            foreach ( $list_client as $cleint ) {
                $mail = "mailto:contact@sortez.org?subject=Message";
                $mail .= "&bcc=$cleint->Email";
                $email_clients [] = $cleint->Email;
            }

            //$data ['email_clients'] = $mail;

            $clients = array ();
            foreach ( $list_client as $client ) {
                $fidelity = get_fidelity ( $client->id_commercant );
                $offre = get_offre_active ( $client->id_commercant );
                $fiche = get_fiche_client ( $offre, $client->id_client, $client->id_commercant );
                $user_bonplan = get_user_bonplan ( $client->id_client, $client->id_commercant );

                $client->offre = $offre;
                $client->solde = ! empty ( $fiche ) ? $fiche->solde : 0;

                switch ($offre) {
                    case "capital" :
                        $client->objectif = (is_object ( $fidelity ) && is_object ( $fidelity->capital )) ? $fidelity->capital->montant : 0;
                        break;
                    case "tampon" :
                        $client->objectif = (is_object ( $fidelity ) && is_object ( $fidelity->tampon )) ? $fidelity->tampon->tampon_value : 0;
                        break;
                    case "remise" :
                        $client->objectif = $client->solde;
                        break;
                }

                $client->bonplan = $user_bonplan;
                $clients [] = $client;
                // $client->cumul = ($offre == "remise" && is_object($fidelity)) ? $fidelity->remise : 0;
            }

            if ($action == "solde_desc")
                usort ( $clients, "solde_desc" );
            if ($action == "solde_asc")
                usort ( $clients, "solde_asc" );

            $data ['json'] = $clients;
            echo json_encode($data);die();
        }

        if ($action == 'print') {
            //$data ['print'] = 1;
            //$list_client = $this->assoc_client_commercant_model->get_by_commercant_id ( $id_commercant, $criteres );
            //$data ['clients'] = $list_client;
            echo $this->load->view ( 'mobile2014/pro/liste-clients-print', $data, true );
        } else if ($action == 'csv') {
            $this->load->dbutil ();
            $criteres ['query'] = 1;
            $query = $this->assoc_client_commercant_model->get_by_commercant_id_csv ( $id_commercant );
            $csv = $this->dbutil->csv_from_result ( $query );
            $this->load->helper ( 'download' );
            force_download ( 'liste_contact.csv', $csv );
        } else {


            if ($action == 'bonplan') {
                $data ['show_offre'] = 0;
            }
            if ($action == 'fidelisation') {
                $data ['show_bonplan'] = 0;
                $data ['show_offre'] = 1;
            }
            $json['json']=$data;
            echo json_encode($data);

        }

    }
    public function get_detail_client(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_client=$_POST["id_client"];
        $id_commercant=$_POST["id_commercant"];
        //$id_client=340;
        //$id_commercant=301327;
        $result=$this->api_model->get_detail_client($id_client,$id_commercant);
        $json['json']=$result;
        echo json_encode($json);
    }
    function liste_reservation() {

        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $filtre = null;
        $ordre = null;
        $criteres ['valide'] = 0;
        $id_commercant=$_POST["id_commercant"];
        $criteres ['id_commercant'] = $id_commercant;
        $order_by ['assoc_client_bonplan.date_validation'] = ($ordre == "desc") ? "desc" : "asc";
        $data ['list_client'] = $this->api_model->get_list_by_criteria ( $criteres, $order_by );

        // echo $this->db->last_query();

        $data ['mdlbonplan'] = $this->mdlbonplan;
        $data ['assoc_client_bonplan_model'] = $this->api_model;

        var_dump($data);

    }

    function edittampon() {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_ionauth=$_POST["id_ionauth"];
        $id_commercant=$_POST["id_commercant"];
        $tampon_value=$_POST["tampon_value"];
        $is_activ=$_POST["is_activ"];
        if (isset($_POST["id"])){$id=$_POST["id"];} else{$id="";}
        //var_dump($is_activ);die();
        try {
            $objTampon =$_POST;



            $objTampon ["tampon_value"] =$tampon_value;

            // $user_ion_auth_id = $this->ion_auth->user ()->row ();
            //var_dump($user_ion_auth_id);
            $objTampon ["id_ionauth"] =$id_ionauth;
            $objTampon ["id_commercant"] =$id_commercant;

            $objTampon ["is_activ"] = (isset ( $is_activ )) ? 1 : 0;
            //var_dump($objTampon);die();
            if ($id == "" || $id == null || $id == "0") {
                $this->mdl_card_tampon->insert ( $objTampon );
            } else {
                $this->mdl_card_tampon->update ( $objTampon );
                $this->mdl_card_tampon->delete_where ( array (
                    'id_commercant' => $id_commercant,
                    'id !=' => $id
                ) );
            }

            if ($is_activ) {
                $objCapital = $this->mdl_card_capital->getByIdCommercant ($id_commercant );
                if (is_object ( $objCapital )) {
                    $arrayCapital ['id'] = $objCapital->id;
                    $arrayCapital ['is_activ'] = 0;
                    $this->mdl_card_capital->update ( $arrayCapital );
                }
                $objRemise = $this->mdl_card_remise->getByIdCommercant ( $id_commercant );
                if (is_object ( $objRemise )) {
                    $arrayRemise ['id'] = $objRemise->id;
                    $arrayRemise ['is_activ'] = 0;
                    $this->mdl_card_capital->update2 ( $arrayRemise );
                }
            }
            $this->session->set_flashdata ( 'message_success', 'Vos paramètres ont été mis à jour' );
            $json['json']="ok";
            echo json_encode($json);
        } catch ( Exception $e ) {
            redirect ( "front/fidelity_pro/tampon", 'refresh' );
        } }
    function editcapital() {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_ionauth=$_POST["id_ionauth"];
        $id_commercant=$_POST["id_commercant"];
        $montant=$_POST["montant"];
        $is_activ=$_POST["is_activ"];
        if (isset($_POST["id"])){$id=$_POST["id"];} else{$id="";}
        try {
            $objCapital = $_POST;
            // $objCapital["remise_value"] = trim($objCapital["remise_value"]);
            $objCapital ["montant"] = $montant;

            $objCapital ["id_ionauth"] = $id_ionauth;
            $objCapital ["id_commercant"] = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $id_ionauth );

            $objCapital ["is_activ"] = (isset ( $is_activ )) ? 1 : 0;

            if ($id == "" || $id == null || $id == "0") {
                $this->mdl_card_capital->insert ( $objCapital );
            } else {
                $this->mdl_card_capital->update ( $objCapital );
            }


            if ($is_activ) {
                $objTampon = $this->mdl_card_tampon->getByIdCommercant ( $id_commercant );
                if (is_object ( $objTampon )) {
                    $arrayTampon ['id'] = $objTampon->id;
                    $arrayTampon ['is_activ'] =0;
                    $this->mdl_card_tampon->update ( $arrayTampon );
                }
                $objRemise = $this->mdl_card_remise->getByIdCommercant ( $id_commercant );
                if (is_object ( $objRemise )) {
                    $arrayRemise ['id'] = $objRemise->id;
                    $arrayRemise ['is_activ'] = 0;
                    $this->mdl_card_remise->update ( $arrayRemise );
                }
            }
            $json['json']="ok";
            echo json_encode($json);
        } catch ( Exception $e ) {
            return "error";
        }
    }
    function editremise() {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_ionauth=$_POST["id_ionauth"];
        $id_commercant=$_POST["id_commercant"];
        $tampon_value=$_POST["montant"];
        $is_activ=$_POST["is_activ"];
        if (isset($_POST["id"])){$id=$_POST["id"];} else{$id="";}
        //var_dump($is_activ);die();
        try {
            $objRemise = $_POST;

            if (! empty ( $objRemise )) {

                $objRemise ["id_ionauth"] = $id_ionauth;
                $objRemise ["id_commercant"] = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $id_ionauth );

                $objRemise ["is_activ"] = (isset ( $is_activ )) ? 1 : 0;

                if ($id == "" || $id == null || $id == "0") {
                    $this->mdl_card_remise->insert ( $objRemise );
                } else {
                    $this->mdl_card_remise->update ( $objRemise );
                }
                if ($objRemise ["is_activ"]) {
                    $objCapital = $this->mdl_card_capital->getByIdCommercant ( $id_commercant );
                    if (is_object ( $objCapital )) {
                        $arrayCapital ['id'] = $objCapital->id;
                        $arrayCapital ['is_activ'] = 0;
                        $this->mdl_card_capital->update ( $arrayCapital );
                    }
                    $objTampon = $this->mdl_card_tampon->getByIdCommercant ( $id_commercant );
                    if (is_object ( $objTampon )) {
                        $arrayTampon ['id'] = $objTampon->id;
                        $arrayTampon ['is_activ'] = 0;
                        $this->mdl_card_tampon->update ( $arrayTampon );
                    }
                }
                $json['json']="ok";
                echo json_encode($json);
            }
        } catch ( Exception $e ) {
            return "error";
        }
    }
    function creerBonplan()
    {
        //$_iDCommercant = 0;
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $oBonplan = $_POST;
        $bp_unique_qttprop=$_POST["bp_unique_qttprop"];
        $bp_multiple_qttprop=$_POST["bp_multiple_qttprop"];
        $oBonplan['bonplan_quantite_depart_unique'] =$bp_unique_qttprop;
        $oBonplan['bonplan_quantite_depart_multiple'] = $bp_multiple_qttprop;
        $bonplan_date_debut=$_POST["bonplan_date_debut"];
        $bonplan_date_fin=$_POST["bonplan_date_fin"];
        $bp_unique_date_debut=$_POST["bp_unique_date_debut"];
        $bp_unique_date_fin=$_POST["bp_unique_date_fin"];
        //$bp_unique_date_visit=$_POST["bp_unique_date_visit"];
        $bp_multiple_date_debut=$_POST["bp_multiple_date_debut"];
        $bp_multiple_date_fin=$_POST["bp_multiple_date_fin"];
        //$bp_multiple_date_visit=$_POST["bp_multiple_date_visit"];

        /*$oBonplan["bonplan_photo1"] = $this->input->post("photo1Associe");
        $oBonplan["bonplan_photo2"] = $this->input->post("photo2Associe");
        $oBonplan["bonplan_photo3"] = $this->input->post("photo3Associe");
        $oBonplan["bonplan_photo4"] = $this->input->post("photo4Associe");*/

        if ($bonplan_date_debut=="--" || $bonplan_date_debut=="0000-00-00") $bonplan_date_debut = null;
        if ($bonplan_date_fin=="--" || $bonplan_date_fin=="0000-00-00") $bonplan_date_fin = null;

        if ($bp_unique_date_debut=="--" || $bp_unique_date_debut=="0000-00-00") $bp_unique_date_debut = null;
        if ($bp_unique_date_fin=="--" || $bp_unique_date_fin=="0000-00-00") $bp_unique_date_fin = null;
        //if ($bp_unique_date_visit=="--" || $bp_unique_date_visit=="0000-00-00") $bp_unique_date_visit = null;

        if ($bp_multiple_date_debut=="--" || $bp_multiple_date_debut=="0000-00-00") $bp_multiple_date_debut = null;
        if ($bp_multiple_date_fin=="--" || $bp_multiple_date_fin=="0000-00-00") $bp_multiple_date_fin = null;
        //if ($bp_multiple_date_visit=="--" || $bp_multiple_date_visit=="0000-00-00") $bp_multiple_date_visit = null;

        $IdInsertedBonplan = $this->api_model->insertBonplan($oBonplan);

        //$this->load->view('front/vwListeBonPlan', $data) ;
        // $this->load->view('front/vwFicheBonplan', $data) ;
        if ($IdInsertedBonplan){$json['json']="ok";
            echo json_encode($json);}

    }
    public function showsolde(){
        $action = false;
        $nb = null;
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_client=$_POST["id_client"];
        $id_commercants=$_POST["id_commercant"];
        //$id_client=340;
        //$id_commercants=301327;

        $id_commercant = $id_commercants;

        $data ['titre'] = 'Liste de mes clients';
        $data ['print'] = 0;
        $data ['email_clients'] = array ();
        $data ['mail_to_clients'] = '';
        $data ['show_bonplan'] = 1;
        $data ['show_offre'] = 1;
        $criteres = array ();
        if ($action == "mois" && $nb != null) {
            $criteres ['month'] = $nb;
        }

        $mail = "";
        $list_client = $this->assoc_client_commercant_model->get_by_commercant_idsolde( $id_commercant, $criteres,$id_client );
        //var_dump($list_client);die();
        $email_clients = array ();
        if (! empty ( $list_client )) {
            foreach ( $list_client as $cleint ) {
                $mail = "mailto:contact@sortez.org?subject=Message";
                $mail .= "&bcc=$cleint->Email";
                $email_clients [] = $cleint->Email;
            }

            $data ['email_clients'] = $mail;

            $clients = array ();
            foreach ( $list_client as $client ) {
                $fidelity = get_fidelity ( $client->id_commercant );
                $offre = get_offre_active ( $client->id_commercant );
                $fiche = get_fiche_client ( $offre, $client->id_client, $client->id_commercant );
                $user_bonplan = get_user_bonplan ( $client->id_client, $client->id_commercant );

                $client->offre = $offre;
                $client->solde = ! empty ( $fiche ) ? $fiche->solde : 0;

                switch ($offre) {
                    case "capital" :
                        $client->objectif = (is_object ( $fidelity ) && is_object ( $fidelity->capital )) ? $fidelity->capital->montant : 0;
                        break;
                    case "tampon" :
                        $client->objectif = (is_object ( $fidelity ) && is_object ( $fidelity->tampon )) ? $fidelity->tampon->tampon_value : 0;
                        break;
                    case "remise" :
                        $client->objectif = $client->solde;
                        break;
                }

                $client->bonplan = $user_bonplan;
                $clients [] = $client;
                //var_dump($clients);die();
                $json['json']=[$client];
                echo json_encode($json);
                // $client->cumul = ($offre == "remise" && is_object($fidelity)) ? $fidelity->remise : 0;
            }
        }
    }
    public function ModifAccount(){
        $rest_json=file_get_contents("php://input");
        $_POSTe=json_decode($rest_json,true);
        //$id_commercant=$_POST["id_commercant"];
        $request=$this->api_model->modifaccount($_POSTe);
        if ($request){
            $json['json']="ok";
            echo json_encode($json);
        }
    }
    public function getremisedirect(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $IdCommercant=$_POST["IdCommercant"];
        $request=$this->api_model->get_remisedirect($IdCommercant);
        if ($request){
            $json['json']=[$request];
            echo json_encode($json);
        }
    }
    public function getcoupdetampon(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $IdCommercant=$_POST["IdCommercant"];
        $request=$this->api_model->getcoupdetampon($IdCommercant);
        if ($request){
            $json['json']=[$request];
            echo json_encode($json);
        }
    }
    public function getcapital(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $IdCommercant=$_POST["IdCommercant"];
        $request=$this->api_model->getcapital($IdCommercant);
        if ($request){
            $json['json']=[$request];
            echo json_encode($json);
        }
    }
    public function getbonplan(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $IdCommercant=$_POST["IdCommercant"];
        //$IdCommercant=301327;
        $request=$this->api_model->getbonplan($IdCommercant);
        if ($request){
            $json['json']=$request;
            echo json_encode($json);
        }
    }
    public function get_login_by_id2(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $login=$_POST["login"];
        $request=$this->api_model->get_login_by_id($login);

    }
    public function get_client($login){
        $ordre = null;

        $request=$this->api_model->get_login_by_id2($login);
        $IdCommercant=$request->IdCommercant;
        //var_dump($IdCommercant);
        $criteres ['valide'] = 0;

        //$user_ion_auth_id =$user_ionauth_id;
        $id_commercant = $IdCommercant;
        $criteres ['id_commercant'] = $id_commercant;
        $order_by ['assoc_client_bonplan.date_validation'] = ($ordre == "desc") ? "desc" : "asc";
        $data = $this->api_model->get_list_by_criteria3 ( $criteres, $order_by );
        $clients = array ();
        foreach ($data as $datas){
            $d = $this->api_model->getById ($datas->id);
            $clients[]=$d;
            //var_dump($clients);

        }
        return $clients;

        //var_dump($clients);
    }
    function liste_reservationionic() {
        $ordre = null;
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $login=$_POST["login"];
        $request=$this->api_model->get_login_by_id2($login);
        $IdCommercant=$request->IdCommercant;
        //var_dump($IdCommercant);
        $criteres ['valide'] = 0;

        //$user_ion_auth_id =$user_ionauth_id;
        $id_commercant = $IdCommercant;
        $criteres ['id_commercant'] = $id_commercant;
        $order_by ['assoc_client_bonplan.date_validation'] = ($ordre == "desc") ? "desc" : "asc";
        $data=$this->api_model->get_list_by_criteria2 ( $criteres, $order_by );
        //$data["client"]=$this->get_client($login);
        //var_dump($data["client"]);die();
        $json['json']=$data;
        echo json_encode($json);
        //var_dump($data);die();

    }
    function fidelity_card_operations() {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $IdUser=$_POST["IdUser"];
        $Id=$_POST["id"];
        $bonplanshow = "bonplan";
        $id_client_bonplan=$_POST["id"];
        $id_commercant=$_POST["bonplan_commercant_id"];
        //$data ['pagetitle'] = "Offre de fidélité !";
        // $IdUser = $this->uri->rsegment(3);
        // $bonplanshow = $this->uri->rsegment(4);

        $data ['oUser'] = $oUser = $this->user->GetById ( $IdUser );

        $data ['oCard'] = $oCard = $this->mdl_card->getByIdUser ( $IdUser );

        $user_ion_auth_id = $this->api_model->get_ion_auth_by_commercant($id_commercant);
        $id_ionauth = $user_ion_auth_id->user_ionauth_id;
        $data ["id_ionauth"] = $id_ionauth;
        $data ["id_commercant"] = $id_commercant;

        $objTampon = $this->mdl_card_tampon->getByIdIonauth ( $id_ionauth );
        $data ["oTampon"] = $objTampon;
        $objCapital = $this->mdl_card_capital->getByIdIonauth ( $id_ionauth );
        $data ["oCapital"] = $objCapital;
        $objRemise = $this->mdl_card_remise->getByIdIonauth ( $id_ionauth );
        $data ["objRemise"] = $objRemise;

        if (isset($id_client_bonplan) && $id_client_bonplan!=0 && $id_client_bonplan!="0") $res_client_bonplan_obj = $this->assoc_client_bonplan_model->getById ( $id_client_bonplan );

        if (isset($res_client_bonplan_obj->id_bonplan) && is_object($res_client_bonplan_obj)) {
            $objBonplan = $this->mdlbonplan->getById($res_client_bonplan_obj->id_bonplan);
            $data ["oBonplan"] = $objBonplan;
        }

        $data ["objReservationNonValideCom"] = $this->assoc_client_bonplan_model->get_where(array('assoc_client_bonplan.id_bonplan'=>$res_client_bonplan_obj->id_bonplan,'assoc_client_bonplan.valide'=>0));

        if (is_object ( $oCard ) && is_object ( $oUser ) && $id_commercant) {
            $oFicheclient = $this->mdl_card_fiche_client_capital->getWhereOne ( " id_card='" . $oCard->id . "' AND id_user='" . $oUser->IdUser . "' AND id_commercant='" . $id_commercant . "' " );

            $data ['oFicheclient'] = $oFicheclient;
            if (isset ( $oFicheclient ) && count ( $oFicheclient ) > 0) {
                $capital_client_value = $oFicheclient->solde_capital;
            } else {
                $capital_client_value = 0;
            }

            $objCommercant_card_tampon = $this->mdl_card_tampon->getByIdCommercant ( $id_commercant );

            //echo $id_commercant;
            //var_dump($objCommercant_card_tampon); die();

            $tampon_commercant_value = (is_object ( $objCommercant_card_tampon )) ? $objCommercant_card_tampon->tampon_value : 0;
            $oFicheclient_tampon = $this->mdl_card_fiche_client_tampon->getWhereOne ( " id_card='" . $oCard->id . "' AND id_user='" . $oUser->IdUser . "' AND id_commercant='" . $id_commercant . "' " );



            if (isset ( $oFicheclient_tampon ) && count ( $oFicheclient_tampon ) > 0) {
                $tampon_client_value = $oFicheclient_tampon->solde_tampon;
                if ($tampon_client_value == $tampon_commercant_value) {
                    $tampon_status = "0";
                }
            } else {
                $tampon_client_value = "0";
            }

            $data ['capital_client_value'] = $capital_client_value;
            $data ['tampon_client_value'] = $tampon_client_value;
            $data ['tampon_commercant_value'] = $tampon_commercant_value;
            $data ['bonplanshow'] = $bonplanshow;
            $data ['visit']=$this->api_model->getById($Id);
            //var_dump($datas);die()
            //var_dump($bonplanshow);die();
            $data ['id_client_bonplan'] = $id_client_bonplan;

            // $criteres['id_commercant'] = $id_commercant;
            // $criteres['id_user'] = $IdUser;

            $criteres ['id'] = $id_client_bonplan;
            $assoc_client_array = $this->assoc_client_bonplan_model->get_list_by_criteria ( $criteres );
            // echo $this->db->last_query();
            // if(!is_array($assoc_client_array)){
            // redirect('auth/login');
            // }
            $data ['assoc_client'] = (! empty ( $assoc_client_array )) ? current ( $assoc_client_array ) : array ();

            $data ['assoc_client_remise'] = $this->mdl_card_fiche_client_remise->getWhereOne(" id_card='" . $oCard->id . "' AND id_user='" . $oUser->IdUser . "' AND id_commercant='" . $id_commercant . "' ");

            $is_mobile = TRUE;
            $data ['titre'] = 'Client: ' . ucfirst ( $oUser->Nom ) . ' ' . ucfirst ( $oUser->Prenom );
            $json['json']=[$data];
            echo json_encode($json);
        } else {
            //redirect ( 'auth/login' );
            $data['error_msg'] = "Ce client ne dispose pas encore d'une carte Privilege !";
            $this->load->view ( "mobile2014/error", $data );
        }
    }
    function card_bonplan_validate() {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        if ($_POST["id_card"]) {

            $id_card = $_POST["id_card"];
            $id_bonplan = $_POST["id_bonplan"];
            $id_commercant = $_POST["id_commercant"];
            $id_client_bonplan = $_POST["id_client_bonplan"];

            // $this->assoc_client_bonplan_model->validate_bonplan($id_bonplan,$id_user);

            // confirmation by commercant
            $this->assoc_client_bonplan_model->validate_bonplan_by_id ( $id_client_bonplan );

            //decrementation nb bonplan
            //$this->mdlbonplan->decremente_quantite_bonplan($id_bonplan, $nb_decrement);



            $data ['id_user'] = $id_user = $this->input->post ( 'id_user' );


            $objBonplan = $this->mdlbonplan->getById ( $id_bonplan );
            $data ["oBonplan"] = $objBonplan;

            $data ["objReservationNonValideCom"] = $this->assoc_client_bonplan_model->get_where(array('assoc_client_bonplan.id_bonplan'=>$id_bonplan,'assoc_client_bonplan.valide'=>0));

            $data ['user'] = $user = $this->user->GetById ( $id_user );
            $data ['titre'] = "Client: " . ucfirst ( $user->Prenom ) . ' ' . ucfirst ( $user->Nom );


            $assoc_client_commercant = $this->assoc_client_commercant_model->get_by_id ( $id_user, $id_commercant );
            if (! $assoc_client_commercant) {
                $this->assoc_client_commercant_model->insert ( array (
                    'id_client' => $id_user,
                    'id_commercant' => $id_commercant
                ) );
            }

            $commercant = $this->commercant->GetById($id_commercant);
            $data['client'] = ucfirst ( $user->Prenom ) . ' ' . ucfirst ( $user->Nom );
            $data['commercant'] = $commercant->NomSociete;



            //$mail = $this->load->view('mobile2014/email/validation_bonplan',$data,TRUE);
            $mail = $this->load->view('mobile2014/email/validation_bonplan',$data,TRUE);

            /*$this->load->library('email');
            $this->email->from('contact@privicarte.fr', 'PRIVICARTE');
            $this->email->to($user->Email);
            $this->email->subject('[Privicarte] Validation de bon plan');
            $this->email->message($mail);
            $this->email->send();*/


            $zContactEmailCom_ = array();
            $zContactEmailCom_[] = array("Email"=>$user->Email,"Name"=>$user->Nom." ".$user->Prenom);
            @envoi_notification($zContactEmailCom_,'[Privicarte] Validation de bon plan',html_entity_decode(htmlentities($mail)), 'contact@privicarte.fr');


            $json['json']="ok";
            echo json_encode($json);
        }
    }
    function add_card_tampon_mobile() {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_card =$_POST["id_card"];
        $id_bonplan = $_POST["id_bonplan"];
        $id_commercant =$_POST["id_commercant"];
        $id_user =$_POST["id_user"];
        $new_tampon_value = $_POST["unite_tampon_value"];

        if ($id_card && $id_commercant && $id_user && $new_tampon_value) {

            $assoc_client_commercant = $this->assoc_client_commercant_model->get_by_id ( $id_user, $id_commercant );
            if (! $assoc_client_commercant) {
                $this->assoc_client_commercant_model->insert ( array (
                    'id_client' => $id_user,
                    'id_commercant' => $id_commercant
                ) );
            }

            // get the max tampon value into commercant
            $oFichecommercant = $this->mdl_card_tampon->getByIdCommercant ( $id_commercant );

            // $this->db->trans_start();
            // checking on card_fiche_client_tampon and save in it
            $oFicheclient = $this->mdl_card_fiche_client_tampon->getWhereOne ( " id_card='" . $id_card . "' AND id_commercant='" . $id_commercant . "' AND id_user='" . $id_user . "'" );
            $oCard_fiche_client_tampon ['id_card'] = $id_card;
            $oCard_fiche_client_tampon ['id_commercant'] = $id_commercant;
            $oCard_fiche_client_tampon ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
            $oCard_fiche_client_tampon ['id_user'] = $id_user;

            if (isset ( $oFicheclient ) && count ( $oFicheclient ) > 0) {
                $tampon_client_value = intval ( $oFicheclient->solde_tampon );
                $tampon_client_value = intval ( $tampon_client_value ) + $new_tampon_value;

                if ($tampon_client_value >= $oFichecommercant->tampon_value) {
                    redirect ( '/front/fidelity/actualise_compte/' . $id_user );
                }

                $oCard_fiche_client_tampon ['id'] = $oFicheclient->id;
                $solde_tampon_client = $tampon_client_value;

                $oCard_fiche_client_tampon ['solde_tampon'] = $solde_tampon_client;
                $this->mdl_card_fiche_client_tampon->update ( $oCard_fiche_client_tampon );
                $this->mdl_card_fiche_client_tampon->delete_where ( array (
                    'id !=' => $oCard_fiche_client_tampon ['id'],
                    'id_user' => $oCard_fiche_client_tampon ['id_user'],
                    'id_commercant' => $oCard_fiche_client_tampon ['id_commercant']
                ) );
            } else {
                $solde_tampon_client = $new_tampon_value;
                $oCard_fiche_client_tampon ['solde_tampon'] = $solde_tampon_client;

                $this->mdl_card_fiche_client_tampon->insert ( $oCard_fiche_client_tampon );
            }

            // saving into card_bonplan_used
            $card_bonplan_used ['id_card'] = $id_card;
            $card_bonplan_used ['id_commercant'] = $id_commercant;
            $card_bonplan_used ['id_user'] = $id_user;
            $card_bonplan_used ['id_bonplan'] = $id_bonplan;
            $card_bonplan_used ['scan_status'] = "OK";
            $card_bonplan_used ['tampon_value_added'] = $new_tampon_value;
            $card_bonplan_used ['date_use'] = date ( "Y-m-d h:m:s" );
            $card_bonplan_used ['description'] = "Ajout tampon";
            $card_bonplan_used ['objectif_tampon'] = $oFichecommercant->tampon_value;
            $card_bonplan_used ['cumul_tampon'] = $solde_tampon_client;
            $card_bonplan_used ['datefin_tampon'] = $oFichecommercant->date_fin;
            $this->mdl_card_bonplan_used->insert ( $card_bonplan_used );
            // $this->db->trans_complete();
            $data ['user'] = $user = $this->user->GetById ( $id_user );
            $data ['titre'] = $user->Prenom . ' ' . $user->Nom;
            $data ['solde_tampon_client'] = $solde_tampon_client;
            $data ['objectif_tampon'] = $oFichecommercant->tampon_value;


            $commercant = $this->commercant->GetById($id_commercant);
            $data['client'] = ucfirst ( $user->Prenom ) . ' ' . ucfirst ( $user->Nom );
            $data['commercant'] = $commercant->NomSociete;
            $data['value_added'] = $new_tampon_value;
            $data['solde'] = $solde_tampon_client;
            $data['objectif'] = $oFichecommercant->tampon_value;

            mail_fidelisation($data, $user->Email);


            $json['json']="ok";
            echo json_encode($json);
        } else {
            redirect ( 'front/fidelity/contenupro' );
        }
    }
    function add_card_remise_mobile() {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_card =$_POST["id_card"];
        $id_bonplan = $_POST["id_bonplan"];
        $id_commercant =$_POST["id_commercant"];
        $id_user =$_POST["id_user"];
        $card_remise_value = $_POST["unite_remise_value"];
        //$percent_remise = $_POST["percent_remise"];

        if ($id_card && $id_commercant && $id_user && $card_remise_value) {

            $assoc_client_commercant = $this->assoc_client_commercant_model->get_by_id ( $id_user, $id_commercant );
            if (! $assoc_client_commercant) {
                $this->assoc_client_commercant_model->insert ( array (
                    'id_client' => $id_user,
                    'id_commercant' => $id_commercant
                ) );
            }

            // $this->db->trans_start();
            $oCard_fiche_client_tampon ['id_card'] = $id_card;
            $oCard_fiche_client_tampon ['id_commercant'] = $id_commercant;
            $oCard_fiche_client_tampon ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
            $oCard_fiche_client_tampon ['id_user'] = $id_user;

            // checking on card_fiche_client_remise and save in it
            $oFicheclient = $this->mdl_card_fiche_client_remise->getWhereOne ( " id_card='" . $id_card . "' AND id_commercant='" . $id_commercant . "' AND id_user='" . $id_user . "'" );
            $oCard_fiche_client_remise ['id_card'] = $id_card;
            $oCard_fiche_client_remise ['id_commercant'] = $id_commercant;
            $oCard_fiche_client_remise ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
            $oCard_fiche_client_remise ['id_user'] = $id_user;

            if (isset ( $oFicheclient ) && count ( $oFicheclient ) > 0) {
                $remise_client_value = intval ( $oFicheclient->solde_remise );

                // get all card_remise_value but not percent value

                //$card_remise_value = (intval ( $card_remise_value ) * ($percent_remise / 100));

                $remise_client_value = intval ( $remise_client_value ) + intval ( $card_remise_value );
                $oCard_fiche_client_remise ['id'] = $oFicheclient->id;
                $oCard_fiche_client_remise ['solde_remise'] = $remise_client_value;
                $solde_remise_client = $remise_client_value;

                $this->mdl_card_fiche_client_remise->update ( $oCard_fiche_client_remise );
                $this->mdl_card_fiche_client_remise->delete_where ( array (
                    'id !=' => $oCard_fiche_client_remise ['id'],
                    'id_user' => $oCard_fiche_client_remise ['id_user'],
                    'id_commercant' => $oCard_fiche_client_remise ['id_commercant']
                ) );
            } else {
                $oCard_fiche_client_remise ['solde_remise'] = $card_remise_value;
                $solde_remise_client = $card_remise_value;

                $this->mdl_card_fiche_client_remise->insert ( $oCard_fiche_client_remise );
            }

            // get the max remise value into commercant
            $oFichecommercant = $this->mdl_card_remise->getByIdCommercant ( $id_commercant );

            // saving into card_bonplan_used
            $card_bonplan_used ['id_card'] = $id_card;
            $card_bonplan_used ['id_commercant'] = $id_commercant;
            $card_bonplan_used ['id_user'] = $id_user;
            $card_bonplan_used ['id_bonplan'] = $id_bonplan;
            $card_bonplan_used ['scan_status'] = "OK";
            $card_bonplan_used ['remise_value_added'] = $card_remise_value;
            $card_bonplan_used ['date_use'] = date ( "Y-m-d h:m:s" );
            $card_bonplan_used ['description'] = "Ajout remise";
            $card_bonplan_used ['cumul_remise'] = $solde_remise_client;
            $card_bonplan_used ['datefin_remise'] = $oFichecommercant->date_fin;

            $this->mdl_card_bonplan_used->insert ( $card_bonplan_used );
            // $this->db->trans_complete();
            $data ['user'] = $user = $this->user->GetById ( $id_user );
            $data ['titre'] = $user->Prenom . ' ' . $user->Nom;
            $data ['solde_remise_client'] = $solde_remise_client;
            $data ['objectif_remise'] = $oFichecommercant->montant;
            $criteres_assoc ['valide'] = 1;
            $criteres_assoc ['id_commercant'] = $id_commercant;
            $criteres_assoc ['id_user'] = $id_user;
            $assoc_client_bonplan_array = $this->assoc_client_bonplan_model->get_list_by_criteria ( $criteres_assoc );


            $commercant = $this->commercant->GetById($id_commercant);
            $data['client'] = ucfirst ( $user->Prenom ) . ' ' . ucfirst ( $user->Nom );
            $data['commercant'] = $commercant->NomSociete;
            $data['value_added'] = $card_remise_value;
            $data['solde'] = $solde_remise_client;
            //$data['objectif'] = $oFichecommercant->montant;

            mail_fidelisation($data, $user->Email);

            // $data['assoc_client_bon_plan'] = (!empty($assoc_client_bonplan_array)) ? current($assoc_client_bonplan_array) : null;
            $data ['assoc_client_bon_plan'] = get_fiche_client ( "remise", $id_user, $id_commercant );
            $json['json']="ok";
            echo json_encode($json);
        } else {
            echo "ko";
        }
    }
    function add_card_capital_mobile() {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $id_card =$_POST["id_card"];
        $id_bonplan = $_POST["id_bonplan"];
        $id_commercant =$_POST["id_commercant"];
        $id_user =$_POST["id_user"];
        $card_capital_value = $_POST["unite_capital_value"];

        if ($id_card && $id_commercant && $id_user && $card_capital_value) {

            $assoc_client_commercant = $this->assoc_client_commercant_model->get_by_id ( $id_user, $id_commercant );
            if (! $assoc_client_commercant) {
                $this->assoc_client_commercant_model->insert ( array (
                    'id_client' => $id_user,
                    'id_commercant' => $id_commercant
                ) );
            }

            // $this->db->trans_start();
            $oCard_fiche_client_tampon ['id_card'] = $id_card;
            $oCard_fiche_client_tampon ['id_commercant'] = $id_commercant;
            $oCard_fiche_client_tampon ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
            $oCard_fiche_client_tampon ['id_user'] = $id_user;

            // checking on card_fiche_client_capital and save in it
            $oFicheclient = $this->mdl_card_fiche_client_capital->getWhereOne ( " id_card='" . $id_card . "' AND id_commercant='" . $id_commercant . "' AND id_user='" . $id_user . "'" );
            $oCard_fiche_client_capital ['id_card'] = $id_card;
            $oCard_fiche_client_capital ['id_commercant'] = $id_commercant;
            $oCard_fiche_client_capital ['id_ionauth_client'] = $this->ion_auth_used_by_club->get_ion_id_from_user_id ( $id_user );
            $oCard_fiche_client_capital ['id_user'] = $id_user;

            // get the max capital value into commercant
            $oFichecommercant = $this->mdl_card_capital->getByIdCommercant ( $id_commercant );

            if (isset ( $oFicheclient ) && count ( $oFicheclient ) > 0) {
                $capital_client_value = intval ( $oFicheclient->solde_capital );
                $capital_client_value = intval ( $capital_client_value ) + intval ( $card_capital_value );

                if (intval ( $capital_client_value ) >= intval ( $oFichecommercant->montant )) {
                    redirect ( '/front/fidelity/actualise_compte/' . $id_user );
                }
                $oCard_fiche_client_capital ['id'] = $oFicheclient->id;
                $oCard_fiche_client_capital ['solde_capital'] = $capital_client_value;
                $solde_capital_client = $capital_client_value;

                $this->mdl_card_fiche_client_capital->update ( $oCard_fiche_client_capital );
                $this->mdl_card_fiche_client_capital->delete_where ( array (
                    'id !=' => $oCard_fiche_client_capital ['id'],
                    'id_user' => $oCard_fiche_client_capital ['id_user'],
                    'id_commercant' => $oCard_fiche_client_capital ['id_commercant']
                ) );
            } else {
                $oCard_fiche_client_capital ['solde_capital'] = $card_capital_value;
                $solde_capital_client = $card_capital_value;

                $this->mdl_card_fiche_client_capital->insert ( $oCard_fiche_client_capital );
            }

            // saving into card_bonplan_used
            $card_bonplan_used ['id_card'] = $id_card;
            $card_bonplan_used ['id_commercant'] = $id_commercant;
            $card_bonplan_used ['id_user'] = $id_user;
            $card_bonplan_used ['id_bonplan'] = $id_bonplan;
            $card_bonplan_used ['scan_status'] = "OK";
            $card_bonplan_used ['capital_value_added'] = $card_capital_value;
            $card_bonplan_used ['date_use'] = date ( "Y-m-d h:m:s" );
            $card_bonplan_used ['description'] = "Ajout capital";
            $card_bonplan_used ['objectif_capital'] = $oFichecommercant->montant;
            $card_bonplan_used ['cumul_capital'] = $solde_capital_client;
            $card_bonplan_used ['datefin_capital'] = $oFichecommercant->date_fin;

            $this->mdl_card_bonplan_used->insert ( $card_bonplan_used );
            // $this->db->trans_complete();
            $data ['user'] = $user = $this->user->GetById ( $id_user );
            $data ['titre'] = "Client: " . ucfirst ( $user->Prenom ) . ' ' . ucfirst ( $user->Nom );
            $data ['solde_capital_client'] = $solde_capital_client;
            $data ['objectif_capital'] = $oFichecommercant->montant;

            $commercant = $this->commercant->GetById($id_commercant);
            $data['client'] = ucfirst ( $user->Prenom ) . ' ' . ucfirst ( $user->Nom );
            $data['commercant'] = $commercant->NomSociete;
            $data['value_added'] = $card_capital_value;
            $data['solde'] = $solde_capital_client;
            $data['objectif'] = $oFichecommercant->montant;

            mail_fidelisation($data, $user->Email);

            $json['json']="ok";
            echo json_encode($json);
        } else {
            redirect ( 'auth/login' );//aut/login
        }
    }
    public function getbonplannbre(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $IdCommercant=$_POST["IdCommercant"];
        $request=$this->api_model->getbonplannbre($IdCommercant);
        $result=$request->limit_bonplan;
        $nbre=$this->api_model->getnbrebonplan($IdCommercant);
        if ($result){
            $data=array("nbre"=>$nbre,"limit"=>$result);
            $json['json']=[$data];
            echo json_encode($json);
        }
    }
    public function deletebonplan(){
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $IdCommercant=$_POST["IdCommercant"];
        $request=$this->api_model->deletebonplan($IdCommercant);
        if ($request){
            $json['json']="ok";
            echo json_encode($json);
        }
    }
    function detail_client() {
        $rest_json=file_get_contents("php://input");
        $_POST=json_decode($rest_json,true);
        $idUser=$_POST["idUser"];


        $user_ion_auth_id = $this->ion_auth->user ()->row ();
        $id_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id ( $user_ion_auth_id->id );
        $data ['titre'] = "Historique consommation";
        $critere = array (
            'card_bonplan_used.id_commercant' => $id_commercant,
            'card_bonplan_used.id_user' => $idUser
        );
        // $critere['card_bonplan_used.capital_value_added !='] ='';
        $order_by = array (
            'champ' => 'card_bonplan_used.id',
            'ordre' => 'ASC'
        );
        $data ['list_activity'] = $this->mdl_card_bonplan_used->get_by_critere ( $critere );
        $criteres_assoc ['valide'] = 1;
        $criteres_assoc ['id_commercant'] = $id_commercant;
        $criteres_assoc ['id_user'] = $idUser;

        if ($assoc_client_bon_plan = $this->assoc_client_bonplan_model->get_list_by_criteria ( $criteres_assoc )) {
            $data ['assoc_client_bonplan'] = current ( $assoc_client_bon_plan );
            $json['json']=[$data];
            echo json_encode($json);
        } else {
            $json['json']=[$data];
            echo json_encode($json);
        }

    }
}