<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic agenda interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Agendas extends REST_Controller
{
	/*function agenda_get()
    {
        if(!$this->get('id'))
        {
        	$this->response(NULL, 400);
        }

        // $agenda = $this->some_model->getSomething( $this->get('id') );
    	$agendas = array(
			1 => array('id' => 1, 'name' => 'Some Guy', 'email' => 'example1@example.com', 'fact' => 'Loves swimming'),
			2 => array('id' => 2, 'name' => 'Person Face', 'email' => 'example2@example.com', 'fact' => 'Has a huge face'),
			3 => array('id' => 3, 'name' => 'Scotty', 'email' => 'example3@example.com', 'fact' => 'Is a Scott!'),
		);
		
    	$agenda = @$agendas[$this->get('id')];
    	
        if($agenda)
        {
            $this->response($agenda, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'agenda could not be found'), 404);
        }
    }
    
    function agenda_post()
    {
        //$this->some_model->updateagenda( $this->get('id') );
        $message = array('id' => $this->get('id'), 'name' => $this->post('name'), 'email' => $this->post('email'), 'message' => 'ADDED!');
        
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
    function agenda_delete()
    {
    	//$this->some_model->deletesomething( $this->get('id') );
        $message = array('id' => $this->get('id'), 'message' => 'DELETED!');
        
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
    function agendas_get()
    {
        //$agendas = $this->cities_model->get();
        $agendas = array(
			array('id' => 1, 'name' => 'Some Guy', 'email' => 'example1@example.com'),
			array('id' => 2, 'name' => 'Person Face', 'email' => 'example2@example.com'),
			array('id' => 3, 'name' => 'Scotty', 'email' => 'example3@example.com'),
		);
        
        if($agendas)
        {
            $this->response($agendas, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Couldn\'t find any agendas!'), 404);
        }
    }*/
public function __construct()
{
    parent::__construct();
    $this->load->model('mdl_agenda');
    $this->load->model('mdlville');
    $this->load->model('mdldepartement');
}

    function agenda_get()
    {    
            $agenda = $this->mdl_agenda->GetAllAppsMobile($this->get('limit'));
            if($agenda)
            {
                $this->response(array('response' => $agenda), 200);
            }

            else
            {
                $this->response(array('error' => 'agenda could not be found'), 404);
            }
    }        

    function agenda_getById_get()
    {    
            $agenda = $this->mdl_agenda->GetById($this->get('id'));
            if($agenda)
            {
                $this->response(array('response' => $agenda), 200);
            }

            else
            {
                $this->response(array('error' => 'id could not be found'), 404);
            }  
    }

    function getById_IsActif_get()
    {    
            $agenda = $this->mdl_agenda->GetById_IsActif($this->get('id'));
            if($agenda)
            {
                $this->response(array('response' => $agenda), 200);
            }

            else
            {
                $this->response(array('error' => 'id could not be found'), 404);
            }  
    }
    
    function agenda_getByIdCommercant_get()
    {    
            $agenda = $this->mdl_agenda->GetByIdCommercant($this->get('IdCommercant'));
            if($agenda)
            {
                $this->response(array('response' => $agenda), 200);
            }

            else
            {
                $this->response(array('error' => 'IdCommercant could not be found'), 404);
            }

    }

    function getAgendaCategorie_get()
    {    
            $agenda = $this->mdl_agenda->GetAgendaCategorie();
            if($agenda)
            {
                $this->response(array('response' => $agenda), 200);
            }

            else
            {
                $this->response(array('error' => 'IdCommercant could not be found'), 404);
            }

    }

    function getAgendaVilles_pvc_get()
    {    
            $agenda = $this->mdlville->GetAgendaVilles_pvc();
            if($agenda)
            {
                $this->response(array('response' => $agenda), 200);
            }

            else
            {
                $this->response(array('error' => 'Agenda could not be found'), 404);
            }

    }

    function getAgendaDepartements_pvc_get()
    {    
            $agenda = $this->mdldepartement->GetAgendaDepartements_pvc();
            if($agenda)
            {
                $this->response(array('response' => $agenda), 200);
            }

            else
            {
                $this->response(array('error' => 'Agenda could not be found'), 404);
            }

    }

    function listeAgendaRecherche_get()
    {    
            $agenda = $this->mdl_agenda->listeAgendaRecherche($this->get('iCategorieId'), 
                                                              $this->get('iVilleId'), 
                                                              $this->get('iDepartementId'), 
                                                              $this->get('zMotCle'), 
                                                              $this->get('iSousCategorieId'), 
                                                              $this->get('limitstart'), 
                                                              $this->get('limitend'), 
                                                              $this->get('iOrderBy'), 
                                                              $this->get('inputQuand'), 
                                                              $this->get('inputDatedebut'), 
                                                              $this->get('inputDatefin'), 
                                                              $this->get('inputIdCommercant'), 
                                                              $this->get('IdUsers_ionauth'));
            if($agenda)
            {
                $this->response(array('response' => $agenda), 200);
            }

            else
            {
                $this->response(array('error' => 'IdCommercant could not be found'), 404);
            }

    }
    
    /*function agenda_post()
    {
        echo'test agenda_post';
    }
    
    function agenda_delete()
    {
        echo'test agenda_delete';
    }
    
    function agendas_get()
    {
        echo'test agendas_get';
    }*/
}