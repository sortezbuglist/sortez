<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic article interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Articles extends REST_Controller
{
	/*function article_get()
    {
        if(!$this->get('id'))
        {
        	$this->response(NULL, 400);
        }

        // $article = $this->some_model->getSomething( $this->get('id') );
    	$articles = array(
			1 => array('id' => 1, 'name' => 'Some Guy', 'email' => 'example1@example.com', 'fact' => 'Loves swimming'),
			2 => array('id' => 2, 'name' => 'Person Face', 'email' => 'example2@example.com', 'fact' => 'Has a huge face'),
			3 => array('id' => 3, 'name' => 'Scotty', 'email' => 'example3@example.com', 'fact' => 'Is a Scott!'),
		);
		
    	$article = @$articles[$this->get('id')];
    	
        if($article)
        {
            $this->response($article, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'article could not be found'), 404);
        }
    }
    
    function article_post()
    {
        //$this->some_model->updatearticle( $this->get('id') );
        $message = array('id' => $this->get('id'), 'name' => $this->post('name'), 'email' => $this->post('email'), 'message' => 'ADDED!');
        
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
    function article_delete()
    {
    	//$this->some_model->deletesomething( $this->get('id') );
        $message = array('id' => $this->get('id'), 'message' => 'DELETED!');
        
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
    function articles_get()
    {
        //$articles = $this->cities_model->get();
        $articles = array(
			array('id' => 1, 'name' => 'Some Guy', 'email' => 'example1@example.com'),
			array('id' => 2, 'name' => 'Person Face', 'email' => 'example2@example.com'),
			array('id' => 3, 'name' => 'Scotty', 'email' => 'example3@example.com'),
		);
        
        if($articles)
        {
            $this->response($articles, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Couldn\'t find any articles!'), 404);
        }
    }*/
public function __construct()
{
    parent::__construct();
    $this->load->model('mdlarticle');
    $this->load->model('mdlville');
    $this->load->model('mdldepartement');
}

    function article_get()
    {    
            $article = $this->mdlarticle->GetAllAppMobile($this->get('limit'));
            if($article)
            {
                $this->response(array('response' => $article), 200);
            }

            else
            {
                $this->response(array('error' => 'article could not be found'), 404);
            }
    }        

    function article_getById_get()
    {    
            $article = $this->mdlarticle->getById($this->get('id'));
            if($article)
            {
                $this->response(array('response' => $article), 200);
            }

            else
            {
                $this->response(array('error' => 'id could not be found'), 404);
            }  
    }

        function article_getById_IsActif_get()
    {    
            $article = $this->mdlarticle->GetById_IsActif($this->get('id'));
            if($article)
            {
                $this->response(array('response' => $article), 200);
            }

            else
            {
                $this->response(array('error' => 'id could not be found'), 404);
            }  
    }
    
    function article_getByIdCommercant_get()
    {    
            $article = $this->mdlarticle->GetByIdCommercant($this->get('IdCommercant'));
            if($article)
            {
                $this->response(array('response' => $article), 200);
            }

            else
            {
                $this->response(array('error' => 'IdCommercant could not be found'), 404);
            }

    }


    function getArticleCategorie_get()
    {    
            $article = $this->mdlarticle->GetArticleCategorie();
            if($article)
            {
                $this->response(array('response' => $article), 200);
            }

            else
            {
                $this->response(array('error' => 'could not be found'), 404);
            }

    }

    function getArticleVilles_pvc_get()
    {    
            $article = $this->mdlville->GetArticleVilles_pvc();
            if($article)
            {
                $this->response(array('response' => $article), 200);
            }

            else
            {
                $this->response(array('error' => 'could not be found'), 404);
            }

    }

    function getArticleDepartements_pvc_get()
    {    
            $article = $this->mdldepartement->GetArticleDepartements_pvc();
            if($article)
            {
                $this->response(array('response' => $article), 200);
            }

            else
            {
                $this->response(array('error' => 'could not be found'), 404);
            }

    }

    function listeArticleRecherches_get()
    {    
            $article = $this->mdlarticle->listeArticleRecherche($this->get('iCategorieId'), 
                                                                $this->get('iVilleId'), 
                                                                $this->get('iDepartementId'), 
                                                                $this->get('zMotCle'), 
                                                                $this->get('iSousCategorieId'), 
                                                                $this->get('limitstart'), 
                                                                $this->get('iOrderBy'), 
                                                                $this->get('inputQuand'), 
                                                                $this->get('inputDatedebut'), 
                                                                $this->get('inputDatefin'), 
                                                                $this->get('inputIdCommercant'), 
                                                                $this->get('IdUsers_ionauth'));
            if($article)
            {
                $this->response(array('response' => $article), 200);
            }

            else
            {
                $this->response(array('error' => 'IdCommercant could not be found'), 404);
            }

    }
    
    /*function article_post()
    {
        echo'test article_post';
    }
    
    function article_delete()
    {
        echo'test article_delete';
    }
    
    function articles_get()
    {
        echo'test articles_get';
    }*/
}