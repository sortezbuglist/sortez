<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic bonplan interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package     CodeIgniter
 * @subpackage  Rest Server
 * @category    Controller
 * @author      Phil Sturgeon
 * @link        http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Bonplans extends REST_Controller
{
    /*function bonplan_get()
    {
        if(!$this->get('id'))
        {
            $this->response(NULL, 400);
        }

        // $bonplan = $this->some_model->getSomething( $this->get('id') );
        $bonplans = array(
            1 => array('id' => 1, 'name' => 'Some Guy', 'email' => 'example1@example.com', 'fact' => 'Loves swimming'),
            2 => array('id' => 2, 'name' => 'Person Face', 'email' => 'example2@example.com', 'fact' => 'Has a huge face'),
            3 => array('id' => 3, 'name' => 'Scotty', 'email' => 'example3@example.com', 'fact' => 'Is a Scott!'),
        );
        
        $bonplan = @$bonplans[$this->get('id')];
        
        if($bonplan)
        {
            $this->response($bonplan, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'bonplan could not be found'), 404);
        }
    }
    
    function bonplan_post()
    {
        //$this->some_model->updatebonplan( $this->get('id') );
        $message = array('id' => $this->get('id'), 'name' => $this->post('name'), 'email' => $this->post('email'), 'message' => 'ADDED!');
        
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
    function bonplan_delete()
    {
        //$this->some_model->deletesomething( $this->get('id') );
        $message = array('id' => $this->get('id'), 'message' => 'DELETED!');
        
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
    function bonplans_get()
    {
        //$bonplans = $this->cities_model->get();
        $bonplans = array(
            array('id' => 1, 'name' => 'Some Guy', 'email' => 'example1@example.com'),
            array('id' => 2, 'name' => 'Person Face', 'email' => 'example2@example.com'),
            array('id' => 3, 'name' => 'Scotty', 'email' => 'example3@example.com'),
        );
        
        if($bonplans)
        {
            $this->response($bonplans, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Couldn\'t find any bonplans!'), 404);
        }
    }*/
public function __construct()
{
    parent::__construct();
    $this->load->model('mdlbonplan');
}

    function getById_get()
    {    
            $bonplan = $this->mdlbonplan->GetById($this->get('id'));
            if($bonplan)
            {
                $this->response(array('response' => $bonplan), 200);
            }

            else
            {
                $this->response(array('error' => 'bonplan could not be found'), 404);
            }
    } 

    function bonplanParCommercant_get()
    {    
            $bonplan = $this->mdlbonplan->bonplanParCommercant($this->get('iCommercantId'));
            if($bonplan)
            {
                $this->response(array('response' => $bonplan), 200);
            }

            else
            {
                $this->response(array('error' => 'bonplan could not be found'), 404);
            }
    }      

        function listeBonPlanRecherches_get()
    {    
            $bonplan = $this->mdlbonplan->listeBonPlanRecherche($this->get('iCategorieId'), 
                                                                $this->get('i_CommercantId'), 
                                                                $this->get('zMotCle'), 
                                                                $this->get('iFavoris'), 
                                                                $this->get('limitstart'), 
                                                                $this->get('limitend'), 
                                                                $this->get('iIdVille'), 
                                                                $this->get('iIdDepartement'), 
                                                                $this->get('iOrderBy'), 
                                                                $this->get('inputFromGeo'), 
                                                                $this->get('inputGeoValue'), 
                                                                $this->get('inputGeoLatitude'),
                                                                $this->get('inputGeoLongitude'),
                                                                $this->get('session_iWhereMultiple'));
            if($bonplan)
            {
                $this->response(array('response' => $bonplan), 200);
            }

            else
            {
                $this->response(array('error' => 'could not be found'), 404);
            }

    }

    /*function bonplan_getById_get()
    {    
            $bonplan = $this->mdl_card_bonplan_used->GetById($this->get('id'));
            if($bonplan)
            {
                $this->response(array('response' => $bonplan), 200);
            }

            else
            {
                $this->response(array('error' => 'id could not be found'), 404);
            }  
    }
    
    function bonplan_getByIdCommercant_get()
    {    
            $bonplan = $this->mdl_card_bonplan_used->GetByIdCommercant($this->get('IdCommercant'));
            if($bonplan)
            {
                $this->response(array('response' => $bonplan), 200);
            }

            else
            {
                $this->response(array('error' => 'IdCommercant could not be found'), 404);
            }

    }
    
    function bonplan_post()
    {
        echo'test bonplan_post';
    }
    
    function bonplan_delete()
    {
        echo'test bonplan_delete';
    }
    
    function bonplans_get()
    {
        echo'test bonplans_get';
    }*/
}