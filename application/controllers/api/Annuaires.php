<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic article interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Annuaires extends REST_Controller
{
	/*function article_get()
    {
        if(!$this->get('id'))
        {
        	$this->response(NULL, 400);
        }

        // $article = $this->some_model->getSomething( $this->get('id') );
    	$articles = array(
			1 => array('id' => 1, 'name' => 'Some Guy', 'email' => 'example1@example.com', 'fact' => 'Loves swimming'),
			2 => array('id' => 2, 'name' => 'Person Face', 'email' => 'example2@example.com', 'fact' => 'Has a huge face'),
			3 => array('id' => 3, 'name' => 'Scotty', 'email' => 'example3@example.com', 'fact' => 'Is a Scott!'),
		);
		
    	$article = @$articles[$this->get('id')];
    	
        if($article)
        {
            $this->response($article, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'article could not be found'), 404);
        }
    }
    
    function article_post()
    {
        //$this->some_model->updatearticle( $this->get('id') );
        $message = array('id' => $this->get('id'), 'name' => $this->post('name'), 'email' => $this->post('email'), 'message' => 'ADDED!');
        
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
    function article_delete()
    {
    	//$this->some_model->deletesomething( $this->get('id') );
        $message = array('id' => $this->get('id'), 'message' => 'DELETED!');
        
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
    function articles_get()
    {
        //$articles = $this->cities_model->get();
        $articles = array(
			array('id' => 1, 'name' => 'Some Guy', 'email' => 'example1@example.com'),
			array('id' => 2, 'name' => 'Person Face', 'email' => 'example2@example.com'),
			array('id' => 3, 'name' => 'Scotty', 'email' => 'example3@example.com'),
		);
        
        if($articles)
        {
            $this->response($articles, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Couldn\'t find any articles!'), 404);
        }
    }*/
public function __construct()
{
    parent::__construct();
    $this->load->model('mdlcommercantpagination');
    $this->load->model('mdlcommercant');
}
    function infoCommercant_get()
    {    
            $annuaire = $this->mdlcommercant->infoCommercant($this->get('iCommercantId'));
            if($annuaire)
            {
                $this->response(array('response' => $annuaire), 200);
            }

            else
            {
                $this->response(array('error' => 'IdCommercant could not be found'), 404);
            }

    }

    function getAll_get()
    {    
            $annuaire = $this->mdlcommercant->GetAll();
            if($annuaire)
            {
                $this->response(array('response' => $annuaire), 200);
            }

            else
            {
                $this->response(array('error' => ' could not be found'), 404);
            }

    }

    function listePartenaireRecherche_get()
    {    
            $annuaire = $this->mdlcommercantpagination->listePartenaireRecherche($this->get('iCategorieId'), 
                                                                $this->get('iVilleId'), 
                                                                $this->get('iDepartementId'), 
                                                                $this->get('zMotCle'), 
                                                                $this->get('iSousCategorieId'), 
                                                                $this->get('limitstart'), 
                                                                $this->get('iOrderBy'), 
                                                                $this->get('inputQuand'), 
                                                                $this->get('inputDatedebut'), 
                                                                $this->get('inputDatefin'), 
                                                                $this->get('inputIdCommercant'), 
                                                                $this->get('IdUsers_ionauth'));
            if($annuaire)
            {
                $this->response(array('response' => $annuaire), 200);
            }

            else
            {
                $this->response(array('error' => 'could not be found'), 404);
            }

    }
    
    /*function article_post()
    {
        echo'test article_post';
    }
    
    function article_delete()
    {
        echo'test article_delete';
    }
    
    function articles_get()
    {
        echo'test articles_get';
    }*/
}