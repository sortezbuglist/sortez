<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name: Ion Auth Lang - French
*
* Author: Stan
* tfspir@gmail.com
*
* Location: http://github.com/benedmunds/ion_auth/
*
* Created: 03.23.2010
*
* Description: French language file for Ion Auth messages and errors
*
*/
 
// Account Creation
$lang['account_creation_successful'] = 'Compte cr&eacute;&eacute; avec succ&egrave;s';
$lang['account_creation_unsuccessful'] = 'Impossible de cr&eacute;er le compte';
$lang['account_creation_duplicate_email'] = 'Email d&eacute;j&agrave; utilis&eacute; ou invalide';
$lang['account_creation_duplicate_username'] = 'Nom d\'utilisateur d&eacute;j&agrave; utilis&eacute; ou invalide';
 
 
// Password
$lang['password_change_successful'] = 'Le mot de passe a &eacute;t&eacute; chang&eacute; avec succ&egrave;s';
$lang['password_change_unsuccessful'] = 'Impossible de changer le mot de passe';
$lang['forgot_password_successful'] = 'Mail de r&eacute;initialisation du mot de passe envoy&eacute;';
$lang['forgot_password_unsuccessful'] = 'Impossible de r&eacute;initialiser le mot de passe';
 
// Activation
$lang['activate_successful'] = 'Compte activ&eacute;';
$lang['activate_unsuccessful'] = 'Impossible d\'activer le compte';
$lang['deactivate_successful'] = 'Compte d&eacute;sactiv&eacute;';
$lang['deactivate_unsuccessful'] = 'Impossible de d&eacute;sactiver le compte';
$lang['activation_email_successful'] = 'Email d\'activation envoy&eacute; avec succ&egrave;s';
$lang['activation_email_unsuccessful'] = 'Impossible d\'envoyer le mail d\'activation';
 
// Login / Logout
$lang['login_successful'] = 'Connect&eacute; avec succ&egrave;s';
$lang['login_unsuccessful'] = 'Erreur lors de la connexion';
$lang['login_unsuccessful_not_active'] = 'Ce compte est inactif';
$lang['logout_successful'] = 'D&eacute;connexion effectu&eacute;e avec succ&egrave;s';
  
// Account Changes
$lang['update_successful'] = 'Compte utilisateur mis &agrave; jour avec succ&egrave;s';
$lang['update_unsuccessful'] = 'Impossible de mettre &agrave; jour le compte utilisateur';
$lang['delete_successful'] = 'Utilisateur supprim&eacute;';
$lang['delete_unsuccessful'] = 'Impossible de supprimer l\'utilisateur';

// Email Subjects - TODO Please Translate
$lang['email_forgotten_password_subject']    = 'Forgotten Password Verification';
$lang['email_new_password_subject']          = 'New Password';
$lang['email_activation_subject']            = 'Account Activation';