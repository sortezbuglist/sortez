<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*

| -------------------------------------------------------------------------

| URI ROUTING

| -------------------------------------------------------------------------

| This file lets you re-map URI requests to specific controller functions.

|

| Typically there is a one-to-one relationship between a URL string

| and its corresponding controller class/method. The segments in a

| URL normally follow this pattern:

|

|	example.com/class/method/id/

|

| In some instances, however, you may want to remap this relationship

| so that a different class/function is called than the one

| corresponding to the URL.

|

| Please see the user guide for complete details:

|

|	http://codeigniter.com/user_guide/general/routing.html

|

| -------------------------------------------------------------------------

| RESERVED ROUTES

| -------------------------------------------------------------------------

|

| There area two reserved routes:

|

|	$route['default_controller'] = 'welcome';

|

| This route indicates which controller class should be loaded if the

| URI contains no data. In the above example, the "welcome" class

| would be loaded.

|

|	$route['404_override'] = 'errors/page_missing';

|

| This route will tell the Router what URI segments to use if those provided

| in the URL cannot be matched to a valid route.

|

*/



// $route['default_controller'] = "welcome";

/*if (CURRENT_SITE == "agenda"){

    $route['default_controller'] = "agenda";

} else if (CURRENT_SITE == "clubproximite"){

    $route['default_controller'] = "front/bonplan";

} else {

    $route['default_controller'] = "front/bonplan";

}*/



$route['default_controller'] = "front/bonplan";



$route['404_override'] = 'erreur';



$route['auth'] = "auth";

$route['connexion'] = "connexion";

$route['sommaire'] = "sommaire";

$route['annuaire'] = "annuaire";
$route['annuaire/index'] = "annuaire/index";

$route['agenda'] = "agenda";

$route['article'] = "article";
$route['article/liste'] = "article";

$route['revuedepresse'] = "revuedepresse";

$route['uploadfile'] = "uploadfile";

$route['contact'] = "contact";

$route['admin'] = "admin/home";

$route['publightbox'] = "publightbox";

$route['media'] = "media";

$route['soutenons'] = "soutenons/Annuaire_Soutenons";

$route['annuaire'] = "annuaire";

$route['annuaire/index'] = "annuaire/index";

//$route['listVille'] = "vivresaville/listVilleController";







//$route['cagnes-sur-mer'] = "front/bonplan?localdata_IdVille=2031";

//$route['villeneuve-loubet'] = "front/bonplan?localdata_IdVille=2004";



$route['cagnes-sur-mer/presentation'] = "front/annuaire/localdata_IdVille/2031";

$route['villeneuve-loubet/presentation'] = "front/annuaire/localdata_IdVille/2004";



/*$route['cagnes-sur-mer'] = "front/annuaire/localdata_IdVille/2031";

$route['villeneuve-loubet'] = "front/annuaire/localdata_IdVille/2004";



$route['ruederbach'] = "front/annuaire/localdata_IdVille/2031";

$route['guewenheim'] = "front/annuaire/localdata_IdVille/2050";*/







$route['ville\/([a-z0-9_-]+)'] = "front/annuaire/localdata_IdVille/$1";






$route['([a-z0-9_-]+)\/commander'] = "front/commercant/commander/$1";

$route['sortez06/presentation'] = "front/annuaire/localdata_IdDepartement/06";

$route['sortez06'] = "front/annuaire/localdata_IdDepartement/06";

$route['([a-z0-9_-]+)'] = "front/annonce/ficheCommercantAnnonce/$1";

$route['([a-z0-9_-]+)\/index'] = "front/annonce/ficheCommercantAnnonce/$1";

if (strpos($_SERVER[REQUEST_URI], 'presentation') !== false && strpos($_SERVER[REQUEST_URI], 'presentation_commercants') != true) {
//    $hostname = "localhost";
//    $database = "sortez1";
//    $username = "root";
//    $password = "";

    $hostname = "priviconfwsortez.mysql.db";
    $database = "priviconfwsortez";
    $username = "priviconfwsortez";
    $password = "jk98rezOPI";

    $uuuuuuuu = $_SERVER[REQUEST_URI];
    $uuuuuuuu = str_replace("/presentation", "", $uuuuuuuu);
    $uuuuuuuu = str_replace("/", "", $uuuuuuuu);

    try {
        $dbh = new PDO('mysql:host=' . $hostname . ';dbname=' . $database, $username, $password);
        $dbh_request = '
SELECT
commercants.IdCommercant,
commercants.user_ionauth_id,
users_groups_ionauth.group_ionauth_id,
commercants.nom_url
FROM
commercants
INNER JOIN users_groups_ionauth ON commercants.user_ionauth_id = users_groups_ionauth.user_ionauth_id
WHERE
commercants.nom_url = "'.$uuuuuuuu.'"
LIMIT 1
';
        $dbh_results = $dbh->query($dbh_request);
        $group_to_find = "";
        foreach ($dbh_results as $row) {
            $group_to_find = $row['group_ionauth_id'];
        }
        $dbh = null;
        if ($group_to_find == "4") {
            $route['([a-z0-9_-]+)\/presentation'] = "front_soutenons/commercant/presentation/$1";
        } else {
            $route['([a-z0-9_-]+)\/presentation'] = "front/commercant/presentation/$1";
        }
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage() . "<br/>";
        //die();
    }
}

$route['([a-z0-9_-]+)\/presentation_commercants'] = "front_soutenons/commercant/presentation/$1";

$route['([a-z0-9_-]+)\/commandes_commercants'] = "front_soutenons/commercant/commander/$1";

$route['([a-z0-9_-]+)\/annonces_commercants'] = "front_soutenons/commercant/menuannonceCommercant/$1";

$route['([a-z0-9_-]+)\/article_commercant'] = "front_soutenons/commercant/article_partner_list/$1";

$route['([a-z0-9_-]+)\/agenda_commercants'] = "front_soutenons/commercant/agenda_partner_list/$1";

$route['([a-z0-9_-]+)\/infos'] = "front/commercant/plusInfoCommercant/$1";

$route['([a-z0-9_-]+)\/infos_commercants'] = "front_soutenons/commercant/plusInfoCommercant/$1";

$route['([a-z0-9_-]+)\/reservation_commercants'] = "front_soutenons/commercant/reservation/$1";

$route['([a-z0-9_-]+)\/reservation'] = "front/commercant/reservation/$1";


$route['([a-z0-9_-]+)\/details_plat\/([a-z0-9_-]+)'] = "front/commercant/details_plat/$1/$2";

$route['([a-z0-9_-]+)\/autresinfos'] = "front/commercant/plusInfo2Commercant/$1";

$route['([a-z0-9_-]+)\/annonces'] = "front/annonce/menuannonceCommercant/$1";

$route['([a-z0-9_-]+)\/detail_annonce\-([0-9]+)'] = "front/annonce/detailAnnonce/$2";

$route['([a-z0-9_-]+)\/detail_annonce_commercants\/([0-9]+)'] = "front_soutenons/commercant/detailAnnonce/$1/$2";

$route['([a-z0-9_-]+)\/notre_bonplan'] = "front/bonplan/listetousbonplan/$1";

$route['([a-z0-9_-]+)\/notre_bonplan\/([a-z0-9_-]+)'] = "front/commercant/listeBonPlanParCommercant/$1/$2";

$route['([a-z0-9_-]+)\/notre_bonplan\/([a-z0-9_-]+)\/([a-z0-9_-]+)'] = "front/commercant/listeBonPlanParCommercant/$1/$2/$3";

$route['([a-z0-9_-]+)\/notre_fidelisation\/([a-z0-9_-]+)\/([a-z0-9_-]+)'] = "front/fidelity/detailFidelity/$1/$2/$3";

$route['([a-z0-9_-]+)\/nous_situer'] = "front/commercant/nousSituerCommercant/$1";

$route['([a-z0-9_-]+)\/nous_contacter'] = "front/annonce/nousContacterAnnonce/0/$1";

$route['([a-z0-9_-]+)\/video'] = "front/commercant/videoCommercant/$1";

$route['([a-z0-9_-]+)\/recommandation'] = "front/commercant/recommanderAmi/$1";

$route['([a-z0-9_-]+)\/photos'] = "front/commercant/photoCommercant/$1";

$route['([a-z0-9_-]+)\/menu_mobile'] = "front/annonce/menuMobile/$1";

$route['([a-z0-9_-]+)\/mentions_legales'] = "front/commercant/mentionslegalesmobile/$1";

$route['([a-z0-9_-]+)\/coordonnees_horaires'] = "front/commercant/coordonneeshoraires/$1";

$route['([a-z0-9_-]+)\/agenda'] = "agenda/agenda_partner_list/$1";

$route['([a-z0-9_-]+)\/agenda\/([0-9]+)'] = "agenda/agenda_partner_list/$1/$2";

$route['([a-z0-9_-]+)\/details_agenda\/([0-9]+)'] = "agenda/agenda_partner_details/$1/$2";

$route['([a-z0-9_-]+)\/details_agenda_commercants\/([0-9]+)'] = "front_soutenons/commercant/agenda_partner_details/$1/$2";

$route['([a-z0-9_-]+)\/agenda_liste'] = "agenda/liste/$1";

$route['([a-z0-9_-]+)\/article'] = "article/article_partner_list/$1";

$route['([a-z0-9_-]+)\/article\/([0-9]+)'] = "article/article_partner_list/$1/$2";

$route['([a-z0-9_-]+)\/details_article\/([0-9]+)'] = "article/article_partner_details/$1/$2";

$route['([a-z0-9_-]+)\/details_article_commercants\/([0-9]+)'] = "front_soutenons/commercant/article_partner_details/$1/$2";

$route['([a-z0-9_-]+)\/article_liste'] = "article/liste/$1";



/*

 * http://localhost/clubproximite/branches/front/annonce/ficheCommercantAnnonce/300077      ok

 * http://localhost/clubproximite/branches/front/commercant/presentation/300077             ok   

 * http://localhost/clubproximite/branches/front/commercant/plusInfoCommercant/300077       ok

 * http://localhost/clubproximite/branches/front/commercant/plusInfo2Commercant/300077      ok

 * http://localhost/clubproximite/branches/front/annonce/menuannonceCommercant/300077       ok

 * http://localhost/clubproximite/branches/front/annonce/detailAnnonce/5                        ok

 * http://localhost/clubproximite/branches/front/commercant/listeBonPlanParCommercant/300077    ok

 * http://localhost/clubproximite/branches/front/commercant/nousSituerCommercant/300077         ok

 * http://localhost/clubproximite/branches/front/annonce/nousContacterAnnonce/0/300077          ok

 * http://localhost/clubproximite/branches/front/commercant/videoCommercant/300077              ok

 * http://localhost/clubproximite/branches/front/commercant/recommanderAmi/300077               ok

 * mobile link

 * front/commercant/photoCommercant             ok

 * front/annonce/menuMobile                     ok

 * front/commercant/mentionslegalesmobile       ok

 * front/commercant/coordonneeshoraires

 * 

 * 

*/



