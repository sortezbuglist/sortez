<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/*
|--------------------------------------------------------------------------
| Autres
|--------------------------------------------------------------------------
|
*/
define("_USER_TYPE_COMMERCANT_", "COMMERCANT"); // Pour les commercants
define("_USER_TYPE_AUTRES_", "AUTRES"); // Autres que commercants (Client ou Admin)
define("_USER_TYPE_VISITOR_", 0);
define("_USER_TYPE_ADMIN_", 1);

/*
|--------------------------------------------------------------------------
| CURRENT_SITE
| Set a constant for whichever site you happen to be running, if its not here
| it will fatal error.
|--------------------------------------------------------------------------
*/
switch($_SERVER['HTTP_HOST'])
{
    case 'agenda-cotedazur.fr':
        define('CURRENT_SITE', 'agenda');
        break;
    case 'www.agenda-cotedazur.fr':
        define('CURRENT_SITE', 'agenda');
        break;
    case 'agenda-test.club-proximite.wuhost01.o2switch.net':
        define('CURRENT_SITE', 'agenda');
        break;

    case 'localhost':
        define('CURRENT_SITE', 'clubproximite');//clubproximite
        break;
    case 'club-proximite.wuhost01.o2switch.net':
        define('CURRENT_SITE', 'clubproximite');
        break;

    case 'club-proximite.com':
        define('CURRENT_SITE', 'clubproximite');
        break;

    case 'www.club-proximite.com':
        define('CURRENT_SITE', 'clubproximite');
        break;

    default:
        define('CURRENT_SITE', 'clubproximite');//clubproximite
        break;
}


if (isset($_GET['localdata']) && $_GET['localdata']=="cagnescommerces") {
    define('LOCALDATA',"cagnescommerces");
}


/*Filtre Domaine*/
define("DOMAIN_SORTEZ_GLOBAL", "randevprojectpriviconcept.ovh");
define("DOMAIN_WWW_SORTEZ_GLOBAL", "www.randevprojectpriviconcept.ovh");
if (
    $_SERVER['HTTP_HOST'] == "antibes-juan-les-pins.randawilly.ovh" || $_SERVER['HTTP_HOST'] == "www.antibes-juan-les-pins.randawilly.ovh" ||
    $_SERVER['HTTP_HOST'] == "cagnes-sur-mer.randawilly.ovh" || $_SERVER['HTTP_HOST'] == "www.cagnes-sur-mer.randawilly.ovh" ||
    $_SERVER['HTTP_HOST'] == "cannes.randawilly.ovh" || $_SERVER['HTTP_HOST'] == "www.cannes.randawilly.ovh" ||
    $_SERVER['HTTP_HOST'] == "les-vallees-de-la-roya-bevera.randawilly.ovh" || $_SERVER['HTTP_HOST'] == "www.les-vallees-de-la-roya-bevera.randawilly.ovh" ||
    $_SERVER['HTTP_HOST'] == "nice.randawilly.ovh" || $_SERVER['HTTP_HOST'] == "www.nice.randawilly.ovh" ||
    $_SERVER['HTTP_HOST'] == "saint-laurent-du-var.randawilly.ovh" || $_SERVER['HTTP_HOST'] == "www.saint-laurent-du-var.randawilly.ovh" ||
    $_SERVER['HTTP_HOST'] == "villefranche-sur-mer.randawilly.ovh" || $_SERVER['HTTP_HOST'] == "www.villefranche-sur-mer.randawilly.ovh"
) {
    define("DOMAIN_VIVRESAVILLE_GLOBAL", $_SERVER['HTTP_HOST']);
    define("DOMAIN_WWW_VIVRESAVILLE_GLOBAL", $_SERVER['HTTP_HOST']);
} else {
    define("DOMAIN_VIVRESAVILLE_GLOBAL", "randawilly.ovh");
    define("DOMAIN_WWW_VIVRESAVILLE_GLOBAL", "www.randawilly.ovh");
}


/* End of file constants.php */
/* Location: ./application/config/constants.php */


