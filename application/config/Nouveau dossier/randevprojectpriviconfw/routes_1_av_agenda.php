<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once("database.php");
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

// $route['default_controller'] = "welcome";
$route['default_controller'] = "accueil";
$route['404_override'] = '';

$route['connexion'] = "connexion";
$route['([a-z0-9_-]+)'] = "front/annonce/ficheCommercantAnnonce/$1";
$route['([a-z0-9_-]+)\/presentation'] = "front/commercant/presentation/$1";
$route['([a-z0-9_-]+)\/infos'] = "front/commercant/plusInfoCommercant/$1";
$route['([a-z0-9_-]+)\/autresinfos'] = "front/commercant/plusInfo2Commercant/$1";
$route['([a-z0-9_-]+)\/annonces'] = "front/annonce/menuannonceCommercant/$1";
$route['([a-z0-9_-]+)\/detail_annonce\-([0-9]+)'] = "front/annonce/detailAnnonce/$2";
$route['([a-z0-9_-]+)\/notre_bonplan'] = "front/commercant/listeBonPlanParCommercant/$1";
$route['([a-z0-9_-]+)\/notre_bonplan\/([a-z0-9_-]+)'] = "front/commercant/listeBonPlanParCommercant/$1/$2";
$route['([a-z0-9_-]+)\/nous_situer'] = "front/commercant/nousSituerCommercant/$1";
$route['([a-z0-9_-]+)\/nous_contacter'] = "front/annonce/nousContacterAnnonce/0/$1";
$route['([a-z0-9_-]+)\/video'] = "front/commercant/videoCommercant/$1";
$route['([a-z0-9_-]+)\/recommandation'] = "front/commercant/recommanderAmi/$1";
$route['([a-z0-9_-]+)\/photos'] = "front/commercant/photoCommercant/$1";
$route['([a-z0-9_-]+)\/menu_mobile'] = "front/annonce/menuMobile/$1";
$route['([a-z0-9_-]+)\/mentions_legales'] = "front/commercant/mentionslegalesmobile/$1";
$route['([a-z0-9_-]+)\/coordonnees_horaires'] = "front/commercant/coordonneeshoraires/$1";

/*
 * http://localhost/clubproximite/branches/front/annonce/ficheCommercantAnnonce/300077      ok
 * http://localhost/clubproximite/branches/front/commercant/presentation/300077             ok   
 * http://localhost/clubproximite/branches/front/commercant/plusInfoCommercant/300077       ok
 * http://localhost/clubproximite/branches/front/commercant/plusInfo2Commercant/300077      ok
 * http://localhost/clubproximite/branches/front/annonce/menuannonceCommercant/300077       ok
 * http://localhost/clubproximite/branches/front/annonce/detailAnnonce/5                        ok
 * http://localhost/clubproximite/branches/front/commercant/listeBonPlanParCommercant/300077    ok
 * http://localhost/clubproximite/branches/front/commercant/nousSituerCommercant/300077         ok
 * http://localhost/clubproximite/branches/front/annonce/nousContacterAnnonce/0/300077          ok
 * http://localhost/clubproximite/branches/front/commercant/videoCommercant/300077              ok
 * http://localhost/clubproximite/branches/front/commercant/recommanderAmi/300077               ok
 * mobile link
 * front/commercant/photoCommercant             ok
 * front/annonce/menuMobile                     ok
 * front/commercant/mentionslegalesmobile       ok
 * front/commercant/coordonneeshoraires
 * 
 * 
*/




/**
 * AUTRES
 */
$hostname = $db['default']['hostname'];
$database = $db['default']['database'];
$username = $db['default']['username'];
$password = $db['default']['password'];

$club = mysql_connect($hostname, $username, $password) or trigger_error(mysql_error(),E_USER_ERROR) ;
mysql_select_db($database, $club);

$sqlVilles = "
    SELECT
        *
    FROM
        villes
";
$qryVille = mysql_query($sqlVilles);
while($objVille = mysql_fetch_object($qryVille)) {
    $route[$objVille->NomSimple . '/(.*)'] = "front/annonce/fiche_commerce/" . $objVille->NomSimple . "/$1/";
}

/* End of file routes.php */
/* Location: ./application/config/routes.php */