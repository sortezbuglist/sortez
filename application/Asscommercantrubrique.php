<?php
class AssCommercantRubrique extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    function Insert($prmData) {
        $this->db->insert("ass_commercants_rubriques", $prmData);
        return $this->db->insert_id();
    }
    
    function GetWhere($prmWhere = "0 = 0", $prmOffset = 0, $prmLimit = 0, $prmOrder = " IdCommercant DESC ") {
        if (empty($prmOrder)) {
            $prmOrder = " IdCommercant DESC ";
        }
        
        
           $qryString = "SELECT   * FROM ass_commercants_rubriques WHERE " . $prmWhere . " ORDER BY " . $prmOrder . "";
        if (!(($prmOffset == 0) and ($prmLimit == 0))) {
            $qryString .= " LIMIT $prmOffset, $prmLimit";
        }
        return $this->db->query($qryString)->result();
    }
    
    function DeleteByIdCommercant($prmIdCommercant) {
        $this->db->where("IdCommercant", $prmIdCommercant);
        $this->db->delete("ass_commercants_rubriques");
    }
}