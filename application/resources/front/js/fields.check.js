/*Script pour v&eacute;rifier les champs d'un formulaire*/
function trim (myString) {
    if(myString!=null) {
        return myString.replace(/^\s+/g,'').replace(/\s+$/g,'');
    } else {
        return myString ;
    }
}

function isEmail(input) {
    return /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(input);
}

function isLettersOnly(input) {
    var reg = new RegExp("^[a-zA-Z]*$");
	if(reg.test(input)) {
	    return true;
	} else {
	    return false;
	}
}

function isLettersAndNumberOnly(input) {
    var reg = new RegExp("^[a-zA-Z0-9]*$");
	if(reg.test(input)) {
	    return true;
	} else {
	    return false;
	}
}

function isAlphaNumeric(input) {
    var reg = new RegExp("^\\w*$");
	if(reg.test(input)) {
	    return true;
	} else {
	    return false;
	}
}

function isInteger(input) {
    var reg = new RegExp("^-?\\d\\d*$");
	if(reg.test(input)) {
	    return true;
	} else {
	    return false;
	}
}

function isPositiveInteger(input) {
    var reg = new RegExp("^\\d\\d*$");
	if(reg.test(input)) {
	    return true;
	} else {
	    return false;
	}
}

function isPositiveReal(input) {
    var reg = new RegExp("^\\d[\\.]?\\d*$");
	if(reg.test(input)) {
	    return true;
	} else {
	    return false;
	}
}

function isNumber(input) {
    // var reg = new RegExp("^-?(\\d\\d*\\.\\d*$)|(^-?\\d\\d*$)|(^-?\\.\\d\\d*$)");
    var reg = new RegExp("^-?(\\d\\d*\\.\\d*$)|^-?(\\d\\d*\\,\\d*$)|(^-?\\d\\d*$)|(^-?\\.\\d\\d*$)");
	if(reg.test(input)) {
	    return true;
	} else {
	    return false;
	}
}

function isPositiveNumber(input) {
    var reg = new RegExp("^(\\d\\d*\\.\\d*$)|^(\\d\\d*\\,\\d*$)|(^\\d\\d*$)");
	if(reg.test(input)) {
	    return true;
	} else {
	    return false;
	}
}


function isEmpty(input) {
    if (input == "") {
        return true;
    } else {
        return false;
    }
}

function isURL(input) {
    return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(input);
}

function isValidDate(input) {
     return !/Invalid|NaN/.test(new Date(input));
}

// [Hari] . Controle que le champ est bien une heure bien formee:
// -----------------------------------------------------------
// Recoit une chaine (et non pas un objet de formulaire).
// Cette fonction permet de v�rifier la validit� d'une heure au format hh:mm.
//
function isValidTime(argTime) {
    var TimeSeparator = ":"

    var pos1 = argTime.indexOf(TimeSeparator)
    var strHour   = argTime.substring(0, pos1)
    var strMinute = argTime.substring(pos1 + 1)

    if (!isInteger(strHour)) {
        return false
    }

    if (!isInteger(strMinute)) {
        return false
    }

    var Hour = parseInt(strHour)
    if (Hour < 0 || Hour > 24) {
        return false;
    }

    var Minute = parseInt(strMinute)
    if (Minute < 0 || Minute > 59) {
        return false;
    }

    return true;
}