// JavaScript Document
function testFormImagespub (){
	var zTitle = jQuery("#IdTitrePub").val () ;
	var zImage = jQuery("#IdPhoto1").val () ;
	var zErreur = "" ;
	
	if (zTitle == ""){
		zErreur += "Veuillez entrer le titre\n" ;
	}
	
	if (zImage == ""){
		zErreur += "Veuillez entrer une image\n" ;
	}
	
	if (zErreur == ""){
		document.frmCreationimagespub.submit();
		
	}else{
		alert (zErreur);
	}
	
}

function testFormAnnonce (){
	var zSousRubrique = jQuery("#idSousRubrique").val () ;
	var zEtat = jQuery("#idEtat").val () ;
	var zDateDebut = jQuery("#IdDateDebut").val () ;
	var zDateFin = jQuery("#IdDateFin").val () ;
	var zDescriptionCourt = jQuery("#idDescriptionCourt").val () ;
	var zDescriptionLong = jQuery("#idDescriptionLong").val () ;
	var zPrixNeuf = jQuery("#idPrixNeuf").val () ;
	var zPrixSolde = jQuery("#IdPrixSolde").val () ;
	var zPhoto1 = jQuery("#IdPhoto1").val () ;
	var zPhoto2 = jQuery("#IdPhoto2").val () ;
	var zPhoto3 = jQuery("#IdPhoto3").val () ;
	var zPhoto4 = jQuery("#IdPhoto4").val () ;
	
	var zErreur = "" ;
	
	if (zSousRubrique == 0){
		zErreur += "Veuillez selectionner une sous rubrique\n" ;
	}
	if (zEtat == "c"){
		zErreur += "Veuillez selectionner l'etat\n" ;
	}
	if (zDateDebut == ""){
		zErreur += "Veuillez indiquer la date du debut\n" ;
	}
	if (zDateFin == ""){
		zErreur += "Veuillez indiquer la date fin\n" ;
	}
	if (zDescriptionCourt == ""){
		zErreur += "Veuillez entrer une description court\n" ;
	}
	if (zDescriptionLong == ""){
		zErreur += "Veuillez entrer une description longue\n" ;
	}
	if (zPrixNeuf == ""){
		zErreur += "Veuillez indiquer le prix neuf\n" ;
	}
	if (zPrixSolde == ""){
		zErreur += "Veuillez indiquer le prix solde\n" ;
	}
	// if (zPhoto1 == ""){
		// zErreur += "Veuillez entrer une photo1 \n" ;
	// }
	// if (zPhoto1 == ""){
		// zErreur += "Veuillez entrer une photo3\n" ;
	// }
	// if (zPhoto1 == ""){
		// zErreur += "Veuillez entrer une photo2\n" ;
	// }
	// if (zPhoto1 == ""){
		// zErreur += "Veuillez entrer une photo4\n" ;
	// }
	
	if (zErreur != ""){
		alert (zErreur) ;
	}else{
		document.frmCreationAnnonce.submit() ;
	}
	
	return false;
}

function supprimeAnnonce (){
	
	if (confirm('voulez-vous vraiment supprimer cette annoce?')){
		jQuery.post(_zBaseUrl,
				{},
			function success(data){ 
			
				alert (data);

		 });
	} 
	
	return false;
}

function testFormBonplan (){
	var zCategorie = jQuery("#idCategorie").val () ;
	var zVille = jQuery("#idVille").val () ;
	var zDateDebut = jQuery("#IdDateDebut").val () ;
	var zDateFin = jQuery("#IdDateFin").val () ;
	var zTitre = jQuery("#idTitre").val () ;
	var zDescription = jQuery("#idDescription").val () ;
	//var zNbrpris = jQuery("#IdNbrpris").val () ;
	var zPhoto1 = jQuery("#IdPhoto1").val () ;
	var zPhoto2 = jQuery("#IdPhoto2").val () ;
	var zPhoto3 = jQuery("#IdPhoto3").val () ;
	var zPhoto4 = jQuery("#IdPhoto4").val () ;
	
	var zErreur = ""
	
	/*if (zCategorie == 0){
		zErreur += "Veuillez selectionner une categorie \n" ;
	}*/
	/*if (zVille == 0){
		zErreur += "Veuillez selectionner une ville \n" ;
	}*/
	if (zTitre == ""){
		zErreur += "Veuillez entrer un titre\n" ;
	}
	if (zDescription == ""){
		zErreur += "Veuillez entrer la description\n" ;
	}
	// if (zNbrpris == ""){
		// zErreur += "Veuillez entrer le nombre pris \n" ;
	// }
	if (zDateDebut == ""){
		zErreur += "Veuillez indiquer la date du debut\n" ;
	}
	if (zDateFin == ""){
		zErreur += "Veuillez indiquer la date fin\n" ;
	}
	// if (zPhoto1 == ""){
		// zErreur += "Veuillez entrer une photo1 \n" ;
	// }
	// if (zPhoto1 == ""){
		// zErreur += "Veuillez entrer une photo3\n" ;
	// }
	// if (zPhoto1 == ""){
		// zErreur += "Veuillez entrer une photo2\n" ;
	// }
	// if (zPhoto1 == ""){
		// zErreur += "Veuillez entrer une photo4\n" ;
	// }
	
	if (zErreur != ""){
		alert (zErreur) ;
	}else{
		document.frmCreationBonplan.submit() ;
	}
	
	return false;
}
function annulationAjout (_zBaseUrl,_zRetourUrl, _zMode, _iDCommercant){
	var _zPhoto1 =  jQuery("#IdPhoto1").val () ;
	var _zPhoto2 =  jQuery("#IdPhoto2").val () ;
	var _zPhoto3 =  jQuery("#IdPhoto3").val () ;
	var _zPhoto4 =  jQuery("#IdPhoto4").val () ;
	var _zNameFile = "" ;
	var _zFileExist = false ;
	if (_zPhoto1 != ""){
		_zNameFile += _zPhoto1 + "-";
	}
	if (_zPhoto2 != ""){
		_zNameFile += _zPhoto2 + "-";
	}
	if (_zPhoto3 != ""){
		_zNameFile += _zPhoto3+ "-";
	}
	if (_zPhoto4 != ""){
		_zNameFile += _zPhoto4 + "-";
	}
	
	if (_zNameFile != "" && _zMode == 'ajout'){
		jQuery.post(_zBaseUrl,
				{zConcateNameFile : _zNameFile},
			function success(data){ 
				alert (data) ;
				window.location = _zRetourUrl + "/" + _iDCommercant ;
			
		});
	}else{
		window.location = _zRetourUrl + "/" + _iDCommercant ;
	}
	
	return false;
}
function supprimeAnnonce (_zBaseUrl){
	
	if (confirm('voulez-vous vraiment supprimer cette annoce?')){
		jQuery.post(_zBaseUrl,
				{},
			function success(data){ 
			
				alert (data) ;

		 });
	} 
	
	return false;
}

function hideLoad (_iNumId){

	if (_iNumId == 1){
		jQuery("#loading1").show();
		jQuery("#loading2").hide();
		jQuery("#loading3").hide();
		jQuery("#loading4").hide();
	}
	
	if (_iNumId == 2){
		jQuery("#loading2").show();
		jQuery("#loading1").hide();
		jQuery("#loading3").hide();
		jQuery("#loading4").hide();
	}
	
	if (_iNumId == 3){
		jQuery("#loading3").show();
		jQuery("#loading2").hide();
		jQuery("#loading1").hide();
		jQuery("#loading4").hide();
	}
	
	if (_iNumId == 4){
		jQuery("#loading4").show();
		jQuery("#loading2").hide();
		jQuery("#loading3").hide();
		jQuery("#loading1").hide();
	}
}

function UploadImage (_zUrl, _zNomchamp, _iNumId){
	var zCheminFichier = jQuery("#"+_zNomchamp).val () ;
	var tzFileName = zCheminFichier.split('.') ;
	var iPosExt = tzFileName.length - 1;
	var zExtension = tzFileName[iPosExt] ;
	var _zExistNomImage = ""
	if (jQuery("#IdPhoto"+_iNumId).val () != ""){
		_zExistNomImage = "/" + jQuery("#IdPhoto"+_iNumId).val () ;
	}
	jQuery("#loading"+_iNumId)
		.ajaxStart(function(){
			hideLoad (_iNumId)
		})
		.ajaxComplete(function(){
			jQuery(this).hide();  
		});
	jQuery.ajaxFileUpload ({
		url : _zUrl +"/"+ _zNomchamp +"/"+ zExtension + _zExistNomImage ,
		secureuri : false,
		fileElementId : _zNomchamp,
		dataType : 'json',
		async :false,
		success: function (data, status) {
			if (typeof(data.error) != 'undefined') {
				if(data.error != '') {
					jQuery("#").val("");
					alert(data.error);
				} else {
					if (data.msg!='') {
						var pPhotosAfficher = data.msg ;
						var pPhotosAfficherSplited = pPhotosAfficher.split("application/resources/front/images/") ;
						var pPhotosEnregistrer = pPhotosAfficherSplited[1] ;							
						jQuery("#IdPhoto"+_iNumId).val(pPhotosEnregistrer) ;
						
				   }
				}
			}
		},
		error: function (data, status, e) {
			alert(e);
		}
	});
}
function setbon_plan_utilise_plusieurs(){
 if(document.getElementById("chk_bon").checked) {
   document.getElementById("bon_plan_utilise_plusieurs").value= 1;
 } else {
    document.getElementById("bon_plan_utilise_plusieurs").value= 0;
 }
 
}
