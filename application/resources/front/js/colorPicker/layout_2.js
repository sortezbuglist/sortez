(function($){
	var initLayout = function() {
		var hash = window.location.hash.replace('#', '');
		var currentTab = $('ul.navigationTabs a')
							.bind('click', showTab)
							.filter('a[rel=' + hash + ']');
		if (currentTab.size() == 0) {
			currentTab = $('ul.navigationTabs a:first');
		}
		showTab.apply(currentTab.get(0));

        var widt = false;

        //colorpickerHolder2******************************************
        $('#colorpickerHolder2').ColorPicker({
			flat: true,
			color: '#00ff00',
			onSubmit: function(hsb, hex, rgb) {
				$('#colorSelector2 div').css('backgroundColor', '#' + hex);
                                $('#bandeau_colorSociete').val('#' + hex);
			}
		});
		$('#colorpickerHolder2>div').css('position', 'absolute');

		$('#colorSelector2').bind('click', function() {
			$('#colorpickerHolder2').stop().animate({height: widt ? 0 : 173}, 500);
			widt = !widt;
		});
        //colorpickerHolder_perso_couleur_cadre******************************************
        $('#colorpickerHolder_perso_couleur_cadre').ColorPicker({
            flat: true,
            color: '#00ff00',
            onSubmit: function(hsb, hex, rgb) {
                $('#colorSelector_perso_couleur_cadre div').css('backgroundColor', '#' + hex);
                $('#perso_couleur_cadre').val(hex);
            }
        });
        $('#colorpickerHolder_perso_couleur_cadre>div').css('position', 'absolute');

        $('#colorSelector_perso_couleur_cadre').bind('click', function() {
            $('#colorpickerHolder_perso_couleur_cadre').stop().animate({height: widt ? 0 : 173}, 500);
            widt = !widt;
        });
        //colorpickerHolder_perso_couleur_titre******************************************
        $('#colorpickerHolder_perso_couleur_titre').ColorPicker({
            flat: true,
            color: '#00ff00',
            onSubmit: function(hsb, hex, rgb) {
                $('#colorSelector_perso_couleur_titre div').css('backgroundColor', '#' + hex);
                $('#perso_couleur_titre').val(hex);
            }
        });
        $('#colorpickerHolder_perso_couleur_titre>div').css('position', 'absolute');

        $('#colorSelector_perso_couleur_titre').bind('click', function() {
            $('#colorpickerHolder_perso_couleur_titre').stop().animate({height: widt ? 0 : 173}, 500);
            widt = !widt;
        });
        //colorpickerHolder_perso_couleur_btn******************************************
        $('#colorpickerHolder_perso_couleur_btn').ColorPicker({
            flat: true,
            color: '#00ff00',
            onSubmit: function(hsb, hex, rgb) {
                $('#colorSelector_perso_couleur_btn div').css('backgroundColor', '#' + hex);
                $('#perso_couleur_btn').val(hex);
            }
        });
        $('#colorpickerHolder_perso_couleur_btn>div').css('position', 'absolute');

        $('#colorSelector_perso_couleur_btn').bind('click', function() {
            $('#colorpickerHolder_perso_couleur_btn').stop().animate({height: widt ? 0 : 173}, 500);
            widt = !widt;
        });
		
	};
	
	var showTab = function(e) {
		var tabIndex = $('ul.navigationTabs a')
							.removeClass('active')
							.index(this);
		$(this)
			.addClass('active')
			.blur();
		$('div.tab')
			.hide()
				.eq(tabIndex)
				.show();
	};
	
	EYE.register(initLayout, 'init');
})(jQuery)