/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.extraPlugins = 'uploadimage';
	config.uploadUrl = '/uploadfile/index/';
	config.extraPlugins = 'widget';
	config.extraPlugins = 'filetools';
	config.extraPlugins = 'notificationaggregator';
	config.extraPlugins = 'lineutils';
	config.extraPlugins = 'notification';
	config.extraPlugins = 'uploadwidget';
	config.extraPlugins = 'lineheight';
	config.line_height = "0;1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17;18;19;20";
};
