<?php require_once("phpmailer.php") ?>
<?php
/**
 * @author Hari
 * Class SendMail
 */
 if (!defined("BASEPATH")) exit("No direct script access allowed");

class IBMail extends PHPMailer {


    public function __construct() {

    }

    /**
     * Reinitialisation des paramètres d'envoi
     */
    public function __Empty() {
        $this->to = array();
        $this->cc = array();
        $this->bcc = array();
        $this->ReplyTo = array();
        $this->attachement = array();
        $this->CustomHeader = array();
    }

    public function SendMail() {
        require(RootPath() . "system/application/config/config.php");

        if ($config["version"] == "dev") {
            $this->IsSMTP();
            $this->Host = $config["mail_server"];
            $this->Mailer = "smtp";
        }

        $this->From = $config["mail_sender_adress"];
        $this->FromName = $config["mail_sender_name"];

        $this->IsHTML(true);

        $this->Subject = $this->Subject;

        return $this->Send();
    }

    public function AddAdress($address, $name = '') {
        $this->AddAddress($address, $name = '');
    }
}
?>