<?php
   /**
   * @desc Gestion des connexions des utilisateurs
   */

   require(APPPATH . "config/database.php");
   define("DB_HOST", $db['default']['hostname']);
   define("DB_USER", $db['default']['username']);
   define("DB_PASSWORD", $db['default']['password']);
   define("DB_NAME", $db['default']['database']);

    // Ouverture de session
    // echo "SESSION ID IS: " . session_id() . "";
    if (session_id() == "") {
        // echo "YES SESSION STARTED IN CURRENTUSER<br/>";
        session_start();
    } else {
        // echo "NO SESSION STARTED !!!!!!!!!!!!<br/>";
        // echo "SESSION ID IS: " . session_id() . "";
        session_destroy ();         // 2008-05-29. Speciale Linux ! (Pour simple pr�caution suite aux probl�mes rencontr�s auparavant dans LMB et LEC)
        session_start();
}

    class CurrentUser {

        // Object internal state, exposes a travers des getters et setters propres.

        private $_IdUser;
        private $_Nom;
        private $_Prenom;
        private $_Login;
        private $_Email;
        private $_UserType;
        private $_NbrPoints;
        private $_Civilite;
        private $_Adresse;
        private $_CodePostal;
        private $_IdVille;
        
        public function __construct() {
            //
        }

        public static function GetInstance() {
            // CLASSIC
            if (empty($_SESSION["clubproximite" . session_id()])) {
                $_SESSION["clubproximite" . session_id()] = new CurrentUser;
            }

            return $_SESSION["clubproximite" . session_id()];
        }

        private function QryExecute($prmQry) {
            $Link = @mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
            if (!$Link) {
                die ("Impossible de se connecter au serveur de base de données [" . DB_HOST . "]");
            }

            $DBSelect = @mysql_select_db(DB_NAME, $Link);
            if (!$DBSelect) {
                die ("Impossible de se connecter à la base de données [" . DB_NAME . "]");
            }

            $Res = @mysql_query($prmQry, $Link);

            mysql_close($Link);

            return $Res;
        }

        public function Login($argLogin, $argPassword, $prmTypeUser = _USER_TYPE_AUTRES_) {
            /**
            $UserType = strtoupper($prmTypeUser);
            switch($UserType) {
                case _USER_TYPE_AUTRES_:
                    // Rechercher dans users
                    $prmTableName = "users";
                    $prmTableId = "IdUser";
                    break;
                case _USER_TYPE_COMMERCANT_:
                    // Rechercher dans commercants
                    $prmTableName = "commercants";
                    $prmTableId = "IdCommercant";
                    break;
                default:
                    // Rechercher dans users
                    $prmTableName = "users";
                    $prmTableId = "IdUser";
                    break;
            }
            // Rechercher dans la table correspondante
            $sqlUtilisateurs = "
                SELECT
            ";
            $sqlUtilisateurs .= ($UserType == _USER_TYPE_AUTRES_) ? ("UserRole AS 'UserRole',") : ("");
            $sqlUtilisateurs .= "
                    $prmTableId AS 'Id',
                    Nom AS 'Nom',
                    Prenom AS 'Prenom',
                    Email AS 'Email',
                    Login AS 'Login',
                    Civilite AS 'Civilite',
                    NbrPoints AS 'NbrPoints',
                    Adresse  AS 'Adresse',
                    CodePostal  AS 'CodePostal',
                    IdVille AS 'IdVille'
                FROM " . $prmTableName . "
                WHERE
                    Login = '" . utf8_decode($argLogin) . "'
                    AND Password = '" . utf8_decode($argPassword) . "'
                    AND IsActif = true
            ";
            $qryUtilisateurs = $this->QryExecute($sqlUtilisateurs);
            */
            $sqlUtilisateurs = "
                SELECT
                    UserRole AS 'UserRole',
                    IdUser AS 'Id',
                    Nom AS 'Nom',
                    Prenom AS 'Prenom',
                    Email AS 'Email',
                    Login AS 'Login',
                    Civilite AS 'Civilite',
                    NbrPoints AS 'NbrPoints',
                    Adresse  AS 'Adresse',
                    CodePostal  AS 'CodePostal',
                    IdVille AS 'IdVille'
                FROM
                    users
                WHERE
                    Login = '" . utf8_decode($argLogin) . "'
                    AND Password = '" . utf8_decode($argPassword) . "'
                    AND IsActif = true
            ";
            $qryUtilisateurs = mysql_query($sqlUtilisateurs);
            // Si vous ajoutez des proprietes ici, n'oubliez pas la clause else !
            if (mysql_numrows($qryUtilisateurs) > 0) {
                $objUser = mysql_fetch_object($qryUtilisateurs);
            } else {
                $sqlUtilisateurs = "
                    SELECT
                        IdCommercant AS 'Id',
                        Nom AS 'Nom',
                        Prenom AS 'Prenom',
                        Email AS 'Email',
                        Login AS 'Login',
                        Civilite AS 'Civilite',
                        NbrPoints AS 'NbrPoints',
                        Adresse  AS 'Adresse',
                        CodePostal  AS 'CodePostal',
                        IdVille AS 'IdVille'
                    FROM
                        commercants
                    WHERE
                        Login = '" . utf8_decode($argLogin) . "'
                        AND Password = '" . utf8_decode($argPassword) . "'
                        AND IsActif = true
                ";
                $qryUtilisateurs = mysql_query($sqlUtilisateurs);
                if (mysql_numrows($qryUtilisateurs) > 0) {
                    $objUser = mysql_fetch_object($qryUtilisateurs);
                }
            }
            if(!empty($objUser)) {
                if (isset($objUser->UserRole)) {
                    $this->_UserType = $objUser->UserRole ;
                } else {
                    $this->_UserType = _USER_TYPE_COMMERCANT_;
                }
                
                $this->_IdUser = $objUser->Id;
                $this->_Nom = $objUser->Nom;
                $this->_Prenom = $objUser->Prenom;
                $this->_Login = $objUser->Login;
                $this->_Email = $objUser->Email;
                $this->_Civilite= $objUser->Civilite;
                $this->_NbrPoints= $objUser->NbrPoints;
                $this->_Adresse= $objUser->Adresse;
                $this->_CodePostal= $objUser->CodePostal;
                $this->_IdVille= $objUser->IdVille;
                
                // echo "<pre>";print_r($objUser);exit();
            } else {
                $this->_IdUser = 0;
                $this->_Nom = "";
                $this->_Prenom = "";
                $this->_Login = "";
                $this->_Email = "";
                $this->_Civilite= "";
                $this->_NbrPoints= 0;
                $this->_Adresse= "";
                $this->_CodePostal= "";
                $this->_IdVille= 0;
            }
        }

        /**
        * @desc D�truit la session de l'utilisateur et remet � 0 toutes ses propri�t�s
        */
        public function Logout() {
            if ($this->IsConnected()) {

                $this->_IdUser = 0;
                $this->_Nom = "";
                $this->_Prenom = "";
                $this->_Login = "";
                $this->_Email = "";
                $this->_Civilite= "";
                $this->_NbrPoints= 0;
                $this->_Adresse= "";
                $this->_CodePostal= "";
                $this->_IdVille= 0;
            }
        }

        public function IsConnected() {
            return ($this->_IdUser > 0) ? true : false ;
        }
        
        public function UserId() {
            return $this->_IdUser;
        }
        
        
        public function UserNom() {
            return $this->_Nom;
        }
        
        public function UserPrenom() {
            return $this->_Prenom;
        }
        
        public function UserLogin() {
            return $this->_Login;
        }
        
        public function UserEmail() {
            return $this->_Email;
        }
        
        public function UserType() {
            return $this->_UserType;
        }
        public function UserCivilite() {
            return $this->_Civilite;
        }
        public function NbrPoints() {
            return $this->_NbrPoints;
        }
        public function Adresse() {
            return $this->_Adresse;
        }
        public function CodePostal() {
            return $this->_CodePostal;
        }
        public function IdVille() {
            return $this->_IdVille;
        }
        
        

        public function __get($argPropertyName) {
            $MethodName = "_Get{$argPropertyName}";
            if (method_exists($this, $MethodName)) { return $this->$MethodName(); }
        }
        
        // Setters civilises avec la fonction magique __set()
        public function __set($argPropertyName, $argValue) {
            $MethodName = "_Set{$argPropertyName}";
            if (method_exists($this, $MethodName)) { return $this->$MethodName($argValue); }
        }

    }

    //$GLOBALS["gCurrentUser"] = CurrentUser::GetInstance();
	
	
	
	