<?php

class Generate_thumb{


    public function makeThumbnails($updir = '', $img='', $new_updir = '',$base_url = '')
    {
        //var_dump($updir.'__'.$img.'__'.$new_updir);die();
        $thumbnail_width = 350;
        $thumbnail_height = 200;
        $thumb_beforeword = "thumbnail";
        $way_to_image = "$updir" . "$img";
        $type_image_data = explode('.',$img);
        if($type_image_data[1] != "webp"){
            if($new_updir != ''){
                if (file_exists("application/resources/front/photoCommercant/imagesbank/") == false){
                    mkdir("application/resources/front/photoCommercant/imagesbank/");
                }
                if (file_exists("$new_updir") == false){
                    mkdir("$new_updir");
                }
                if (file_exists("$new_updir"."thumbs/") == false){
                    mkdir("$new_updir"."thumbs/");
                }
                $new_updir = "$new_updir"."thumbs/";
            }
            if($updir != '' && $img != ''){
                $arr_image_details = getimagesize("$updir"."$img"); // pass id to thumb name
                $original_width = $arr_image_details[0];
                $original_height = $arr_image_details[1];
                if ($original_width > $original_height) {
                    if($original_height <= $thumbnail_width){
                        $new_width = $thumbnail_width;
                        $new_height = $thumbnail_height;
                    }else{
                        $new_width = $thumbnail_width;
                        $new_height = intval($original_height * $new_width / $original_width);
                    }
                } else {
                    $new_height = intval($original_height);
                    $new_width = intval($original_width);
                    $thumbnail_height = $original_height;
                    if($original_width  <=  $thumbnail_width){
                        $new_width = intval($original_width * 9 / 9);
                        $thumbnail_width = intval($original_width * 9 / 9);
                    }else{
                        $new_width = intval($original_width * 8 / 9);
                        $thumbnail_width = intval($original_width * 8 / 9);
                    }

                }
                if($new_height < $thumbnail_height){
                    $new_height = $thumbnail_height;
                }
//                $dest_x = intval(($thumbnail_width - $new_width) / 2);
//                $dest_y = intval(($thumbnail_height - $new_height) / 2);
                $dest_y = intval(0);
                $dest_x = intval(0);




                if ($arr_image_details[2] == IMAGETYPE_GIF) {
                    $imgt = "ImageGIF";
                    $imgcreatefrom = "ImageCreateFromGIF";
                }
                if ($arr_image_details[2] == IMAGETYPE_JPEG) {
                    $imgt = "ImageJPEG";
                    $imgcreatefrom = "ImageCreateFromJPEG";
                }
                if ($arr_image_details[2] == IMAGETYPE_PNG) {
                    $imgt = "ImagePNG";
                    $imgcreatefrom = "ImageCreateFromPNG";
                }
                if ($imgt) {
                    $old_image = $imgcreatefrom($way_to_image);
                    $new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
//                    $white = imagecolorallocate($new_image, 255, 255, 255, 0);
                    imagealphablending( $new_image, false );
                    imagesavealpha( $new_image, true );
//                    imagefilledrectangle($new_image, 0, 0, $thumbnail_width, $thumbnail_height, $white);
                    imagecopyresized($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);
                    $imgt($new_image, "$new_updir". "$thumb_beforeword". '_' . "$img");

                    $link_reel_image = $new_updir.$thumb_beforeword. '_' .$img;

                    return $link_reel_image;
                }
            }
        }else{

            $link_reel_image = "$updir" . "$img";

            return $link_reel_image;
        }

    }

}