<?php
if (!defined("CURRENTUSER_SESSION_PREFIX"))         define("CURRENTUSER_SESSION_PREFIX",     "curruser-");
if (!defined("CURRENTUSER_DEBUG_MODE"))         define("CURRENTUSER_DEBUG_MODE",     "true");
//===================================================================================================================
// USERS
if (!defined("CURRENTUSER_USER_TABLENAME"))         define("CURRENTUSER_USER_TABLENAME",     "utilisateurs");

if (!defined("CURRENTUSER_USER_IDCOL"))         define("CURRENTUSER_USER_IDCOL",     "IdUtilisateur");

if (!defined("CURRENTUSER_USER_NAMECOL"))         define("CURRENTUSER_USER_NAMECOL",     "Nom");

if (!defined("CURRENTUSER_USER_FIRSTNAMECOL"))         define("CURRENTUSER_USER_FIRSTNAMECOL",     "Prenoms");

if (!defined("CURRENTUSER_USER_LOGINCOL"))         define("CURRENTUSER_USER_LOGINCOL",     "Login");

if (!defined("CURRENTUSER_USER_PASSWORDCOL"))         define("CURRENTUSER_USER_PASSWORDCOL",     "MotDePasse");

if (!defined("CURRENTUSER_USER_EMAILCOL"))         define("CURRENTUSER_USER_EMAILCOL",     "Email");


//======================================================================================================================
// PRIVILEGES
if (!defined("CURRENTUSER_PRIVILEGES_TABLENAME"))         define("CURRENTUSER_PRIVILEGES_TABLENAME",     "privileges");

if (!defined("CURRENTUSER_PRIVILEGES_IDCOL"))         define("CURRENTUSER_PRIVILEGES_IDCOL",     "IdPrivilege");

if (!defined("CURRENTUSER_PRIVILEGES_CODECOL"))         define("CURRENTUSER_PRIVILEGES_CODECOL",     "Code");


//======================================================================================================================
// ASSOCIATION PRIV USERS
if (!defined("CURRENTUSER_USERPRIVS_TABLENAME"))         define("CURRENTUSER_USERPRIVS_TABLENAME",     "assprivilegesutilisateurs");

if (!defined("CURRENTUSER_USERPRIVS_IDUSERCOL"))         define("CURRENTUSER_USERPRIVS_IDUSERCOL",     "IdUtilisateur");

if (!defined("CURRENTUSER_USERPRIVS_IDPRIVCOL"))         define("CURRENTUSER_USERPRIVS_IDPRIVCOL",     "IdPrivilege");


//======================================================================================================================
// ELEMENTS
if (!defined("CURRENTUSER_ELEMENTS_TABLENAME"))         define("CURRENTUSER_ELEMENTS_TABLENAME",     "elements");

if (!defined("CURRENTUSER_ELEMENTS_IDUSERCOL"))         define("CURRENTUSER_ELEMENTS_IDCOL",     "IdElement");

if (!defined("CURRENTUSER_ELEMENTS_IDPRIVCOL"))         define("CURRENTUSER_ELEMENTS_CODECOL",     "Code");

if (!defined("CURRENTUSER_ELEMENTS_IDPRIVCOL"))         define("CURRENTUSER_ELEMENTS_NOMCOL",     "Code");

if (!defined("CURRENTUSER_ELEMENTS_IDPRIVCOL"))         define("CURRENTUSER_ELEMENTS_DESCCOL",     "Description");

if (!defined("CURRENTUSER_ELEMENTS_ISACTIVATEDCOL"))         define("CURRENTUSER_ELEMENTS_ISACTIVATEDCOL",     "IsActif");


//=======================================================================================================================
// ASSOCIATION PRIV ELTS
if (!defined("CURRENTUSER_PRIVELTS_TABLENAME"))         define("CURRENTUSER_PRIVELTS_TABLENAME",     "assprivilegeselements");

if (!defined("CURRENTUSER_PRIVELTS_IDELEMENTCOL"))         define("CURRENTUSER_PRIVELTS_IDELEMENTCOL",     "IdElement");

if (!defined("CURRENTUSER_PRIVELTS_IDPRIVCOL"))         define("CURRENTUSER_PRIVELTS_IDPRIVCOL",     "IdPrivilege");

if (!defined("CURRENTUSER_PRIVELTS_DATEDEBUTCOL"))         define("CURRENTUSER_PRIVELTS_DATEDEBUTCOL",     "DateDebut");

if (!defined("CURRENTUSER_PRIVELTS_DATEFINCOL"))         define("CURRENTUSER_PRIVELTS_DATEFINCOL",     "DateFin");

if (!defined("CURRENTUSER_PRIVELTS_OBSERVATIONCOL"))         define("CURRENTUSER_PRIVELTS_OBSERVATIONCOL",     "Observation");

//==========================================================================================================================
// Connexion � la base de donn�es
if (!defined("CURRENTUSER_DB_SERVER"))         define("CURRENTUSER_DB_SERVER",     "localhost");

if (!defined("CURRENTUSER_DB_USER"))         define("CURRENTUSER_DB_USER",     "root");

if (!defined("CURRENTUSER_DB_PASSWORD"))         define("CURRENTUSER_DB_PASSWORD",     "agrotropic");

if (!defined("CURRENTUSER_DB_NAME"))         define("CURRENTUSER_DB_NAME",     "rgamp-coi08-v2");

