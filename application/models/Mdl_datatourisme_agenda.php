
<?php
class mdl_datatourisme_agenda extends CI_Model{

    //public $flux_datatourisme_table = 'flux-1403-201804091513';
    //public $flux_datatourisme_table = 'flux-1404-201806061514';
    //public $flux_datatourisme_table = 'flux-1404-201806101509';
    //public $flux_datatourisme_table = 'flux-1404-201806141515';
    public $flux_datatourisme_table = 'flux-1404-201806251520';

    function __construct() {
        parent::__construct();
    }
	function getByAgenda($id=0){
        if (isset($id)&&$id!=0&&$id!=''){
            $Sql = "select * from datatourisme_agenda where agenda_id =". $id  ;
            $Query = $this->db->query($Sql);
            return $Query->row();
        } else return null;
	}

    function getByDatatourisme($id=0){
        if (isset($id)&&$id!=0&&$id!=''){
            $Sql = "select * from datatourisme_agenda where datatourisme_id =". $id ." LIMIT 1 " ;
            $Query = $this->db->query($Sql);
            return $Query->row();
        } else return null;
    }

    function getByIdDatatourisme($id=0){
        if (isset($id)&&$id!=0&&$id!=''){
            $Sql = "select * from `".$this->flux_datatourisme_table."` where id =". $id ;
            $Query = $this->db->query($Sql);
            return $Query->row();
        } else return null;
    }

    function getAll(){
        $request = $this->db->query("
            SELECT
                *
            FROM
                datatourisme_agenda
            ORDER BY agenda_id DESC
        ");
        if($request->num_rows() > 0) {
            return $request->result();
        }
    }

    function getAllDatatourisme(){
        $request = $this->db->query("
            SELECT
                *
            FROM
                `".$this->flux_datatourisme_table."`
            ORDER BY id ASC
        ");
        if($request->num_rows() > 0) {
            return $request->result();
        }
    }

    function countAllDatatourisme(){
        $Sql = "SELECT COUNT(*) as nb FROM `". $this->flux_datatourisme_table."`";
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

	function getWhere($where=''){
        $sql = "
            SELECT
                *
            FROM
                datatourisme_agenda
            WHERE 
              0=0 
        ";
        if (isset($where) && $where != '') $sql .= " AND ".$where;
        $request = $this->db->query($sql);
        if($request->num_rows() > 0) {
            return $request->result();
        }
    }

    function insert($prmData) {
        $this->db->insert("datatourisme_agenda", $prmData);
        return $this->db->insert_id();
    }

    function update_by_agenda($prmData) {
        $this->db->where("agenda_id", $prmData["agenda_id"]);
        $this->db->update("datatourisme_agenda", $prmData);
        $objA = $this->getByAgenda($prmData["agenda_id"]);
        return $objA->agenda_id;
    }

    function update_by_datatourisme($prmData) {
        $this->db->where("datatourisme_id", $prmData["datatourisme_id"]);
        $this->db->update("datatourisme_agenda", $prmData);
        $objA = $this->getByAgenda($prmData["datatourisme_id"]);
        return $objA->datatourisme_id;
    }

    function delet_by_agenda($prmId=0){
        $qryBonplan = $this->db->query("DELETE FROM datatourisme_agenda WHERE agenda_id = ?", $prmId);
        return $qryBonplan ;
    }

    function delet_by_datatourisme($prmId=0){
        $qryBonplan = $this->db->query("DELETE FROM datatourisme_agenda WHERE datatourisme_id = ?", $prmId);
        return $qryBonplan ;
    }

    function getMinMax_flux()
    {
        $Sql = "SELECT MIN(id) as min_id, MAX(id) as max_id FROM `".$this->flux_datatourisme_table."`";
        $Query = $this->db->query($Sql);
        return $Query->row();
    }



}
