<?php

class PicturesModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function create($data)
    {
        return $this->db->insert('newadmin', $data);
    }

    public function getAllItems()
    {
        $query = $this->db->get('newadmin');
        
        return $query->result();
    }

    public function getAnItem(int $id)
    {
        $query = $this->db->get_where('newadmin', ['id' => $id]);
        return $query->result();
    }

    public function update(int $id, array $newValues)
    {
        $this->db->update('newadmin', $newValues, ['id' => $id]);
    }

    public function delete(int $id)
    {
        $this->db->delete('newadmin', ['id' => $id]);
    }

    /**
     * image by agenda
     */
    public function getImageAgenda()
    {
        $query = $this->db->get('agenda');
        
        return $query->result();
    }

}
