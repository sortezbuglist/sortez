<?php
class mdl_types_agenda extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    function getById($id=0){
        $Sql = "select * from agenda_type where agenda_typeid =". $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }
    function getByIdcodename($oListeCategorie){
        $this->db->select('codename');
        $this->db->where("agenda_categid", $oListeCategorie->agenda_categid);
        $query=$this->db->get("agenda_categ");
        $codename=$query->row();
        ;
        $this->db->select('nom');
        $this->db->where("id", $codename->codename);
        $name=$this->db->get("codename");
        $code=$name->row();
        return $code->nom;
    }

    function GetAll(){
        $qryagenda_type = $this->db->query("
            SELECT *
            FROM
                agenda_type
            ORDER BY agenda_type ASC
        ");
        if($qryagenda_type->num_rows() > 0) {
            return $qryagenda_type->result();
        }

    }
    /*function GetAllcodename(){
        $qryagenda_type = $this->db->query("
            SELECT *
            FROM
                codename
            ORDER BY nom ASC
        ");
        if($qryagenda_type->num_rows() > 0) {
            return $qryagenda_type->result();
        }

    }*/

    function supprimeagenda_type($prmId){

        $qryTypeagenda = $this->db->query("DELETE FROM agenda_type WHERE agenda_typeid = ?", $prmId) ;
        return $qryTypeagenda ;
    }

    function insertagenda_type($prmData) {
        $this->db->insert("agenda_type", $prmData);
        return $this->db->insert_id();
    }

    function updateagenda_type($prmData) {
        $this->db->where("agenda_typeid", $prmData["agenda_typeid"]);
        $this->db->update("agenda_type", $prmData);
        $objTypeagenda = $this->getById($prmData["agenda_typeid"]);
        $objcodename = $this->getByIdcodename($prmData["id"]);
        return $objTypeagenda->agenda_typeid;
    }

    function verifier_type_agenda($prmId){

        $querk = "SELECT
                    *
                    FROM
                    agenda_categ
                    WHERE
                    agenda_categ.agenda_typeid = '".$prmId."'";
        $qryagenda_type = $this->db->query($querk);
        if($qryagenda_type->num_rows() > 0) {
            return $qryagenda_type->result();
        }
    }

}