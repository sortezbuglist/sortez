<?php
class Ville extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    function GetAll_1(){
        $qryVilles = $this->db->query("
            SELECT
                IdVille,
                Nom,
                CodePostal
            FROM
                villes
            ORDER BY Nom ASC
        ");
        if($qryVilles->num_rows() > 0) {
            return $qryVilles->result();
        }
    }

    function GetAll(){
        $qryCategorie = $this->db->query("
                SELECT
                    IdVille,
                    Nom,
                    CodePostal,
                    NomSimple
                FROM
                    villes
                ORDER BY Nom ASC

        ");
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


}