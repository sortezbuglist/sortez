<?php
class mdlbonplans extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    function bonPlanParCommercant($_iCommercantId){

        $zSqlBonPlanParCommercant = "
        SELECT * , commercants.NomSociete AS NomSociete, villes.Nom AS ville,
        commercants.Adresse1 AS quartier, commercants.Adresse2 AS rue,
        commercants.IdCommercant
        FROM
        bonplan
        LEFT JOIN commercants ON commercants.IdCommercant=bonplan.bonplan_commercant_id
        LEFT JOIN villes ON villes.IdVille=commercants.IdVille
        WHERE 0=0
        ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlBonPlanParCommercant .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlBonPlanParCommercant .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlBonPlanParCommercant .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlBonPlanParCommercant .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlBonPlanParCommercant .= " OR ";
            }
            $zSqlBonPlanParCommercant .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlBonPlanParCommercant .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE



        if (isset($_iCommercantId) && $_iCommercantId!=0 && $_iCommercantId!="" && $_iCommercantId!=NULL && $_iCommercantId!="0")
            $zSqlBonPlanParCommercant .= "
		    AND
        commercants.IdCommercant=" . $_iCommercantId ;

        $zQueryBonPlanParCommercant = $this->db->query($zSqlBonPlanParCommercant) ;
        //$toBonPlanParCommercant = $zQueryBonPlanParCommercant->result() ;
        $toBonPlanParCommercant = $zQueryBonPlanParCommercant->row() ;
        //return $toBonPlanParCommercant[0] ;
        return $toBonPlanParCommercant ;
    }
    function lastBonplanCom($_iCommercantId){

        $zSqlBonPlan = "SELECT *  FROM bonplan ";
        if (isset($_iCommercantId) && $_iCommercantId!=0 && $_iCommercantId!="" && $_iCommercantId!=NULL && $_iCommercantId!="0")
            $zSqlBonPlan .= " WHERE bonplan_commercant_id =".$_iCommercantId;
        $zSqlBonPlan .= " order by bonplan_id desc LIMIT 1" ;

        $zQuery = $this->db->query($zSqlBonPlan) ;
        return $zQuery->row() ;
    }
    function getListeBonPlan($_iCommercantId){
        $zSqlBonPlanParCommercant = "SELECT bonplan.* , commercants.Photo5 as Photo5, commercants.NomSociete AS NomSociete, villes.Nom AS ville, commercants.Adresse1 AS quartier, commercants.Adresse2 AS rue, commercants.IdCommercant FROM bonplan LEFT JOIN commercants ON commercants.IdCommercant=bonplan.bonplan_commercant_id LEFT JOIN villes ON villes.IdVille=commercants.IdVille WHERE commercants.IdCommercant=" . $_iCommercantId ." order by bonplan_id desc ";
        $zQueryBonPlanParCommercant = $this->db->query($zSqlBonPlanParCommercant) ;
        $toBonPlanParCommercant = $zQueryBonPlanParCommercant->result() ;
        return $toBonPlanParCommercant;
    }

    function getAlldeposantBonplan($_iCommercantId = "0")
    {

        $zSqlAlldeposantAgenda = "SELECT
            commercants.NomSociete,
            commercants.IdCommercant
            FROM
            bonplan
            INNER JOIN commercants ON bonplan.bonplan_commercant_id = commercants.IdCommercant
            ";

        if ($_iCommercantId != "0" && $_iCommercantId != NULL && $_iCommercantId != "" && $_iCommercantId != 0) {
            $zSqlAlldeposantAgenda .= " AND bonplan.bonplan_commercant_id = '" . $_iCommercantId . "'";
        }

        $zSqlAlldeposantAgenda .= " GROUP BY
            bonplan.bonplan_commercant_id";

        $zQueryAlldeposantAgenda = $this->db->query($zSqlAlldeposantAgenda);
        return $zQueryAlldeposantAgenda->result();
    }
    function getAlldeposantAgenda($_iCommercantId = "0")
    {

        $zSqlAlldeposantAgenda = "SELECT
            commercants.NomSociete,
            commercants.IdCommercant
            FROM
            bonplan
            INNER JOIN commercants ON bonplan.bonplan_commercant_id = commercants.IdCommercant
            ";

        if ($_iCommercantId != "0" && $_iCommercantId != NULL && $_iCommercantId != "" && $_iCommercantId != 0) {
            $zSqlAlldeposantAgenda .= " AND bonplan.bonplan_commercant_id = '" . $_iCommercantId . "'";
        }

        $zSqlAlldeposantAgenda .= " GROUP BY
            bonplan.bonplan_commercant_id";

        $zQueryAlldeposantAgenda = $this->db->query($zSqlAlldeposantAgenda);
        return $zQueryAlldeposantAgenda->result();
    }

}