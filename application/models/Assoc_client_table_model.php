<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class Assoc_client_table_model extends CI_Model {

    public function __construct() {
        parent::__construct ();
    }

    public function get_list_reservation_table_by_idcom($idcom){
        $this->db->select('reservation_type_table.*,reservations.id as idRes,reservations.*');
        $this->db->from('reservations');
        $this->db->join('reservation_type_table','reservation_type_table.id=reservations.id_type_reservation');
        $this->db->where('reservations.IdCommercant',$idcom);
        $this->db->where('reservations.etat_reservation','1');
        $res=$this->db->get();
        return $res->result();
    }

    public function validate_table($idres){
        $array=array("etat_reservation"=>2);
        $this->db->where('id',$idres);
        $this->db->update('reservations',$array);
    }
}
