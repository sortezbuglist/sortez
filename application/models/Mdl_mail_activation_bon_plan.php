<?php
class mdl_mail_activation_bon_plan extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    function Get(){
        $qry = $this->db->query("
            SELECT
                *
            FROM
                mail_activation_bon_plan
            ORDER BY id ASC
        ");
        if($qry->num_rows() > 0) {
            return $qry->row();
        }
    }
	
	function insert($prmData) {
        $this->db->insert("mail_activation_bon_plan", $prmData);
        return $this->db->insert_id();
    }
	
	function update($prmData) {
		$prmData = (array)$prmData;
        $this->db->where("id", $prmData["id"]);
        $this->db->update("mail_activation_bon_plan", $prmData);
        $obj = $this->GetById($prmData["id"]);
		return $obj->id;
    }
	
	function GetById($prmId) {
        $qry =  $this->db->query("SELECT * FROM mail_activation_bon_plan WHERE id = ?", $prmId);
        if ($qry->num_rows() > 0) {
            $Res = $qry->result();
            return $Res[0];
		}
    }
	
	function liste($prmId){
		$zQuery = $this->db->query("SELECT * FROM mail_activation_bon_plan WHERE id = ?", $prmId) ;
		return $zQuery->result() ;
    }
}