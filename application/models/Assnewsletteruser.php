<?php
class AssNewsletterUser extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    function Insert($prmData) {
        $this->db->insert("ass_newsletters_users", $prmData);
        return $this->getById($this->db->insert_id());
    }
    
    function Update($prmData) {
		$prmData = (array)$prmData;
        $this->db->where("IdAssNewsletterUser", $prmData["IdAssNewsletterUser"]);
        $this->db->update("ass_newsletters_users", $prmData);
		return $this->getById($prmData["IdAssNewsletterUser"]);
    }
    
    function getById($prmId = 0){ 
        $SqlNewsletter = "SELECT * FROM ass_newsletters_users WHERE IdAssNewsletterUser = " . $prmId ;
        $QryNewsletter = $this->db->query($SqlNewsletter) ;
        if($QryNewsletter->num_rows() > 0) {
            $Res = $QryNewsletter->result();
            return $Res[0];
        }
    }
}