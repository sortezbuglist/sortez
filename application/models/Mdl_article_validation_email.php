<?php
class mdl_article_validation_email extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    function getAll(){
        $qryCategorie = $this->db->query("
                SELECT
                    *
                FROM
                    article_validation_email
                ORDER BY nom ASC

        ");

        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function getByIdCommercant($IdCommercant = 0)
    {
        $sql = "
                SELECT
                    *
                FROM
                    article_validation_email
                    WHERE IdCommercant = " . $IdCommercant . " 
                ORDER BY nom ASC
                ";
        $qryCategorie = $this->db->query($sql);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function getById($id=0){
        $Sql = " select * from article_validation_email where id = ". $id;      
        $Query = $this->db->query($Sql);
        return $Query->row();
    }
    

    function getByNom($email='', $idCommercant=0){
        $Sql = " select * from article_validation_email where email = '". $email ."' and IdCommercant = '". $idCommercant ."' ";
        $Query = $this->db->query($Sql);
        if($Query->num_rows() > 0) {
            return $Query->result();
        }
    }

    function getByEmail($email=''){
        $Sql = " select * from article_validation_email where email = '". $email ."' ";      
        $Query = $this->db->query($Sql);
        if($Query->num_rows() > 0) {
            return $Query->result();
        }
    }

    function insert($prmData) {
        $this->db->insert("article_validation_email", $prmData);
        return $this->getById($this->db->insert_id());
    }
    
    function update($prmData) {
        $prmData = (array)$prmData;
        $this->db->where("id", $prmData["id"]);
        $this->db->update("article_validation_email", $prmData);
        return $this->GetById($prmData["id"]);
    }

    function getWhere($prmWhere = "0 = 0", $prmOffset = 0, $prmLimit = 0, $prmOrder = " article_validation_email.nom ASC ") {
        if (empty($prmOrder)) {
            $prmOrder = " article_validation_email.nom ASC ";
        }
        
        $qryString = "
            SELECT
                *
            FROM
                article_validation_email
            WHERE " . $prmWhere . "
            ORDER BY " . $prmOrder . "";
        if (!(($prmOffset == 0) and ($prmLimit == 0))) {
            $qryString .= " LIMIT $prmOffset, $prmLimit";
        }
        return $this->db->query($qryString)->result();
    }


}