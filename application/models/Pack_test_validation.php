<?php

class pack_test_validation extends CI_Model
{

    private $_table = "pack_test_validation";

    function __construct()
    {
        parent::__construct();
    }

    function getById($id = 0)
    {
        $Sql = "select * from " . $this->_table . " where id = " . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getAll()
    {
//        $qry = $this->db->query("
//            SELECT
//                *
//            FROM
//                abonnements
//            ORDER BY idAbonnement DESC
//        ");
//        if ($qry->num_rows() > 0) {
//            return $qry->result();
//        }
        $this->db->order_by('idAbonnement','desc');
        $all = $this->db->get('abonnements');
        return $all->result();
    }

    function getByIdUser($idUser = 0)
    {
        $qry = "
                    SELECT
                        *
                    FROM
                        " . $this->_table . "
                    WHERE
                        idUser LIKE '" . $idUser . "'  
                    ORDER BY date_fin ASC
                    ";

        $qryResult = $this->db->query($qry);
        if ($qryResult->num_rows() > 0) {
            return $qryResult->result();
        }
    }

    function getByIdCommercant($idCommercant = 0)
    {
        $qry = "
                    SELECT
                        *
                    FROM
                        " . $this->_table . "
                    WHERE
                        idUser LIKE '" . $idCommercant . "'  
                    ORDER BY date_fin ASC
                    ";

        $qryResult = $this->db->query($qry);
        if ($qryResult->num_rows() > 0) {
            return $qryResult->result();
        }
    }

    function getWhere($where = " 0=0 ", $orderBy = " date_fin ASC ")
    {
        $qry = "
                    SELECT
                        *
                    FROM
                        " . $this->_table . "
                    WHERE
                         " . $where . "  
                    ORDER BY " . $orderBy . " 
                    ";

        $qryResult = $this->db->query($qry);
        if ($qryResult->num_rows() > 0) {
            return $qryResult->result();
        }
    }

    function delete($prmId)
    {
        $qry = $this->db->query("DELETE FROM " . $this->_table . " WHERE id = ?", $prmId);
        return $qry;
    }

    function insert($prmData)
    {
        $this->db->insert($this->_table, $prmData);
        return $this->db->insert_id();
    }

    function update($prmData)
    {
        $this->db->where("id", $prmData["IdVille"]);
        $this->db->update($this->_table, $prmData);
        $obj = $this->getById($prmData["IdVille"]);
        return $obj->id;
    }

}