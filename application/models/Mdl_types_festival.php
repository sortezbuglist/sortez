<?php
class mdl_types_festival extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    function getById($id=0){
        $Sql = "select * from festival_type where festival_typeid =". $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }
    function GetAll(){
        $qryfestival_type = $this->db->query("
            SELECT *
            FROM
                festival_type
            ORDER BY festival_type ASC
        ");
        if($qryfestival_type->num_rows() > 0) {
            return $qryfestival_type->result();
        }
    }

    function supprimefestival_type($prmId){

        $qryTypeFestival = $this->db->query("DELETE FROM festival_type WHERE festival_typeid = ?", $prmId) ;
        return $qryTypeFestival ;
    }

    function insertfestival_type($prmData) {
        $this->db->insert("festival_type", $prmData);
        return $this->db->insert_id();
    }

    function updatefestival_type($prmData) {
        $this->db->where("festival_typeid", $prmData["festival_typeid"]);
        $this->db->update("festival_type", $prmData);
        $objTypeFestival = $this->getById($prmData["festival_typeid"]);
        return $objTypeFestival->festival_typeid;
    }

    function verifier_type_festival($prmId){

        $querk = "SELECT
                    *
                    FROM
                    festival_categ
                    WHERE
                    festival_categ.festival_typeid = '".$prmId."'";
        $qryfestival_type = $this->db->query($querk);
        if($qryfestival_type->num_rows() > 0) {
            return $qryfestival_type->result();
        }
    }

}