<?php

class mdlannuaire_perso extends CI_Model
{
    function __construct() {
        parent::__construct();
    }

    function getannuaire_persoById($id=0){
        $Sql = "select * from annuaire_perso where id =". $id  ;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function GetAll(){
        $qryannuaire_perso = $this->db->query("
            SELECT *
            FROM
                annuaire_perso
            ORDER BY id DESC
        ");
        if($qryannuaire_perso->num_rows() > 0) {
            return $qryannuaire_perso->result();
        }
    }

    function GetAnnuaire_annuaire_persoByUser($IdCommercant){

        $qryCategorie_sql = "SELECT
                annuaire_perso.*
                FROM
                annuaire_perso
                WHERE
                0=0 ";
        if (isset($IdCommercant) && $IdCommercant!="0" && $IdCommercant!="" && $IdCommercant!=NULL && $IdCommercant!=0) {
            $qryCategorie_sql .= " AND annuaire_perso.IdCommercant = '".$IdCommercant."'";
        }

        $qryCategorie_sql .= " LIMIT 1";

        ////$this->firephp->log($qryCategorie_sql, 'qryCategorie_sql');

        $Query = $this->db->query($qryCategorie_sql);
        return $Query->row();
    }

    function GetCommercant_annuaire_perso(){
        $qryCategorie = $this->db->query("
            SELECT
            commercants.IdCommercant,
            annuaire_perso.Idagenda_perso,
            annuaire_perso.Nom,
            annuaire_perso.CodePostal,
            annuaire_perso.NomSimple,
            commercants.IsActif,
            COUNT(commercants.IdCommercant) as nbCommercant
            FROM
            commercants
            Inner Join annuaire_perso ON commercants.Idannuaire_perso = annuaire_perso.Idannuaire_perso
            Inner Join ass_commercants_abonnements ON commercants.IdCommercant = ass_commercants_abonnements.IdCommercant
            INNER JOIN ass_commercants_sousrubriques ON commercants.IdCommercant = ass_commercants_sousrubriques.IdCommercant
            WHERE
            commercants.IsActif =  '1' AND
            DATEDIFF(ass_commercants_abonnements.DateFin ,CURDATE()) >0
            GROUP BY
            annuaire_perso.Idannuaire_perso
            ORDER BY
            annuaire_perso.Nom ASC
        ");
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function delete_annuaire_perso($prmId){

        $qryBonplan = $this->db->query("DELETE FROM annuaire_perso WHERE id = ?", $prmId) ;
        return $qryBonplan ;
    }

    function insert_annuaire_perso($prmData) {
        $this->db->insert("annuaire_perso", $prmData);
        return $this->db->insert_id();
    }

    function update_annuaire_perso($prmData) {
        $this->db->where("id", $prmData["id"]);
        $this->db->update("annuaire_perso", $prmData);
        $objAnnonce = $this->getannuaire_persoById($prmData["id"]);
        return $objAnnonce->id;
    }
}
