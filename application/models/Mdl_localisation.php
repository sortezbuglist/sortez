<?php

class mdl_localisation extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function getById($id = 0)
    {
        $Sql = "select * from locations where location_id =" . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByName($nom = '')
    {
        $Sql = " select * from locations where location = '" . $nom . "' ";
        $Query = $this->db->query($Sql);
        if ($Query->num_rows() > 0) {
            return $Query->result();
        } else return null;
    }

    function getWhere($where = '')
    {
        if (isset($where) && $where!='') {
            $Sql = " select * from locations where 0=0 AND ".$where;
            $Query = $this->db->query($Sql);
        }
        if (isset($Query) && $Query->num_rows() > 0) {
            return $Query->result();
        } else return null;
    }

    function GetAll()
    {
        $qryLocalisation = $this->db->query("
            SELECT * 
            FROM
                locations
            ORDER BY location ASC
        ");
        if ($qryLocalisation->num_rows() > 0) {
            return $qryLocalisation->result();
        }
    }

    function getByIdCommercant($IdCommercant = 0)
    {
        $sql = "
            SELECT * 
            FROM
                locations
            WHERE 
                IdCommercant = ".$IdCommercant."
            ORDER BY location ASC
        ";
        $qryLocalisation = $this->db->query($sql);
        if ($qryLocalisation->num_rows() > 0) {
            return $qryLocalisation->result();
        }
    }

    function delete($prmId)
    {

        $qryBonplan = $this->db->query("DELETE FROM locations WHERE location_id = ?", $prmId);
        return $qryBonplan;
    }

    function insert($prmData)
    {
        $this->db->insert("locations", $prmData);
        return $this->db->insert_id();
    }

    function update($prmData)
    {
        $this->db->where("location_id", $prmData["location_id"]);
        $this->db->update("locations", $prmData);
        $objAnnonce = $this->getById($prmData["location_id"]);
        return $objAnnonce->location_id;
    }
    public function getlieuville($idville){
        $this->db->where("IdVille",$idville);
        $res=$this->db->get("villes");
        return $res->row();
    }
    public function getlieutrueville($idville){
        $this->db->where("location_villeid",$idville);
        $res=$this->db->get("locations");
        return $res->row();
    }
    function getLocationIdByAdresse($CodePostal,$IdVille,$adresse, $titre){
        $sql = "SELECT * FROM `locations` WHERE location_postcode = ".$CodePostal. " AND location_villeid = ".$IdVille." AND locations.location LIKE '%".$adresse."%';";
       
                $Query = $this->db->query($sql);
                $db_error = $this->db->error();
                if (!empty($db_error)) {
                    // $errorlog= "INSERT INTO `Webscrapping_error_log` (`id`, `Titre`) VALUES (NULL, '".$titre."');";
                    // $this->db->query($errorlog);
                    return true;
                } else {
                    return $Query->row();
                } 
    }
}