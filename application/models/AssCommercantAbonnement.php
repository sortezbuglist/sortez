<?php
class AssCommercantAbonnement extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    function Insert($prmData) {
        $this->db->insert("ass_commercants_abonnements", $prmData);
        return $this->db->insert_id();
    }
    
    function GetWhere($prmWhere = "0 = 0", $prmOffset = 0, $prmLimit = 0, $prmOrder = " IdAssCommercantAbonnement DESC ") {
		if (empty($prmOrder)) {
			$prmOrder = " IdAssCommercantAbonnement DESC ";
		}
		
		
       	$qryString = "SELECT   * FROM ass_commercants_abonnements WHERE " . $prmWhere . " ORDER BY " . $prmOrder . "";
		if (!(($prmOffset == 0) and ($prmLimit == 0))) {
			$qryString .= " LIMIT $prmOffset, $prmLimit";
		}

       //echo $qryString;
                
        return $this->db->query($qryString)->result();
    }
    
    function DeleteByIdCommercant($prmIdCommercant) {
        $this->db->where("IdCommercant", $prmIdCommercant);
        $this->db->delete("ass_commercants_abonnements");
    }
    
    function Expiration3mois (){
        $qryString = "
            SELECT * FROM ass_commercants_abonnements WHERE 
            DATEDIFF(DateFin ,CURDATE()) = 90;
            ";
        return $this->db->query($qryString)->result();
    }
    
    function Expiration2mois (){
        $qryString = "
            SELECT * FROM ass_commercants_abonnements WHERE 
            DATEDIFF(DateFin ,CURDATE()) = 60;
            ";
        return $this->db->query($qryString)->result();
    }
    
    function Expiration15jours (){
        $qryString = "
            SELECT * FROM ass_commercants_abonnements WHERE 
            DATEDIFF(DateFin ,CURDATE()) = 15;
            ";
        return $this->db->query($qryString)->result();
    }
    
    function Expiration1jours (){
        $qryString = "
            SELECT * FROM ass_commercants_abonnements WHERE 
            DATEDIFF(DateFin ,CURDATE()) = 1;
            ";
        return $this->db->query($qryString)->result();
    }
    function getIdAbonnement($IdCommercant){
        $qryString = "
        SELECT IdAbonnement FROM `ass_commercants_abonnements` WHERE IdCommercant =".$IdCommercant.";";
        return $this->db->query($qryString)->result();
    }
}