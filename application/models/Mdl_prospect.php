<?php

class Mdl_prospect extends CI_Model
{
    function save_all($data){
        $this->db->where('Email',$data['Email']);
        $testexist = $this->db->get('prospect_externe');
        if (empty($testexist->row()) || $testexist->row() == null ){
        $this->db->insert('prospect_externe',$data);
            }
    }
    function getall(){
        $res = $this->db->get('prospect_externe');
        return $res->result();
    }
    function getByIdcom_prospect($idcom = 0){
        $this->db->select('*');
        $this->db->from('prospect_externe');
        $this->db->where('IdComprospect',$idcom);
        $res = $this->db->get();
        return $res->result();
    }
    function delete_by_idcom($idcom){
        $this->db->where('IdComprospect',$idcom);
        $res = $this->db->delete('prospect_externe');
        return $res;
    }
}