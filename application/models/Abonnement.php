<?php
class Abonnement extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    function GetAll(){
        $qryAbonnements = $this->db->query("
            SELECT
                *
            FROM
                abonnements
            ORDER BY IdAbonnement ASC
        ");
        if($qryAbonnements->num_rows() > 0) {
            return $qryAbonnements->result();
        }
    }

    function GetWhere($prmWhere = "0 = 0") {

        $qryAnnonce =  $this->db->query("SELECT   * FROM abonnements WHERE " . $prmWhere . " ORDER BY IdAbonnement DESC LIMIT 1");

        if ($qryAnnonce->num_rows() > 0) {
            $Res = $qryAnnonce->result();
            return $Res[0];
        }
    }
    
    function GetById($prmId) {
        $qryAnnonce =  $this->db->query("SELECT * FROM abonnements WHERE IdAbonnement = ?", $prmId);
        if ($qryAnnonce->num_rows() > 0) {
            $Res = $qryAnnonce->result();
            return $Res[0];
		}
    }
    
    function Insert($prmData) {
        $this->db->insert("abonnements", $prmData);
        return $this->db->insert_id();
    }
	
	function Update($prmData) {
		$prmData = (array)$prmData;
        $this->db->where("IdAbonnement", $prmData["IdAbonnement"]);
        $this->db->update("abonnements", $prmData);
        $objAnnonce = $this->GetById($prmData["IdAbonnement"]);
		return $objAnnonce->IdAbonnement;
    }
    
    function Delete($prmId){
        $qryBonplan = $this->db->query("DELETE FROM abonnements WHERE IdAbonnement = ?", $prmId);
        return $qryBonplan ;
    }
    
    function verifier_abonnement_commercant($IdRubrique){
        $qryCategorie = $this->db->query("
              
            SELECT
            ass_commercants_abonnements.IdAssCommercantAbonnement,
            ass_commercants_abonnements.IdCommercant,
            ass_commercants_abonnements.IdAbonnement,
            ass_commercants_abonnements.DateDebut,
            ass_commercants_abonnements.DateFin
            FROM
            ass_commercants_abonnements
            WHERE
            ass_commercants_abonnements.IdAbonnement = '".$IdRubrique."' AND
            CURDATE() >= ass_commercants_abonnements.DateDebut AND
            CURDATE() <= ass_commercants_abonnements.DateFin

       ");
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }
    
    
    function GetCurrentCommercantAbonnement($prmId) {
        $squery = "
            SELECT
            ass_commercants_abonnements.*,
            abonnements.type
            FROM
            ass_commercants_abonnements
            INNER JOIN abonnements ON abonnements.IdAbonnement = ass_commercants_abonnements.IdAbonnement
            WHERE
            ass_commercants_abonnements.IdCommercant = '".$prmId."' AND
            CURDATE() >= ass_commercants_abonnements.DateDebut AND
            CURDATE() <= ass_commercants_abonnements.DateFin
            LIMIT 1
        ";
        $qryAnnonce =  $this->db->query($squery);
        if ($qryAnnonce->num_rows() > 0) {
            $Res = $qryAnnonce->result();
            return $Res[0];
		}
    }

    function GetLastAbonnementCommercant($prmId) {
        /*$squery = "
            SELECT
            ass_commercants_abonnements.IdAssCommercantAbonnement,
            ass_commercants_abonnements.IdCommercant,
            ass_commercants_abonnements.DateDebut,
            ass_commercants_abonnements.DateFin,
            abonnements.Nom,
            abonnements.Description,
            abonnements.tarif,
            abonnements.type
            FROM
            abonnements
            INNER JOIN ass_commercants_abonnements ON ass_commercants_abonnements.IdAbonnement = abonnements.IdAbonnement
            WHERE
            ass_commercants_abonnements.IdCommercant = '".$prmId."'
            ORDER BY
            ass_commercants_abonnements.IdAssCommercantAbonnement DESC
            LIMIT 1
            ";*/
        $squery = "
            SELECT
            ass_commercants_abonnements.IdAssCommercantAbonnement,
            ass_commercants_abonnements.IdCommercant,
            ass_commercants_abonnements.DateDebut,
            ass_commercants_abonnements.DateFin

            FROM
            ass_commercants_abonnements

            WHERE
            ass_commercants_abonnements.IdCommercant = '".$prmId."'
            ORDER BY
            ass_commercants_abonnements.IdAssCommercantAbonnement DESC
            LIMIT 1
            ";
        $qryAnnonce =  $this->db->query($squery);
        if ($qryAnnonce->num_rows() > 0) {
            $Res = $qryAnnonce->result();
            return $Res[0];
        }
    }
    public function get_name_by_id($id){
        $this->db->where('IdAbonnement',$id);
        $res=$this->db->get('abonnements');
        return $res->row()->Nom;
    }

    public function delete_image($field,$id){
        $this->db->where('IdAbonnement',$id);
       $delete= $this->db->update('abonnements',$field);
       if ($delete){
           return 1;
       }else{
           return 0;
       }
    }
}