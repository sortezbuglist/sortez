<?php
class mdl_agenda_datetime extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    function getAll(){
        $result = $this->db->query("
                SELECT
                    *
                FROM
                    agenda_datetime
                ORDER BY id DESC

        ");
        if($result->num_rows() > 0) {
            return $result->result();
        }
    }

    function getByAgendaId($agenda_id = 0){
        $sql = "
                SELECT
                    *
                FROM
                    agenda_datetime
                    WHERE agenda_id = ".$agenda_id." 
                ORDER BY id ASC
                ";
        $qryCategorie = $this->db->query($sql);
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function getById($id=0){
        if(isset($id) && $id!=""){
            $Sql = " select * from agenda_datetime where id = ". $id;
            $Query = $this->db->query($Sql);
            return $Query->row();
        } else return null;

    }


    function insert($prmData) {
        if ($prmData["id"]=='0' || $prmData["id"]==0) $prmData["id"]=null;
        if ($prmData["agenda_id"]!='0') {
            $this->db->insert("agenda_datetime", $prmData);
            return $this->getById($this->db->insert_id());
        } else {
            return 0;
        }
    }
    
    function update($prmData) {
        $prmData = (array)$prmData;
        $this->db->where("id", $prmData["id"]);
        $this->db->update("agenda_datetime", $prmData);
        return $this->getById($prmData["id"]);
    }

    function delete($prmId=0){
        $qryBonplan = $this->db->query("DELETE FROM agenda_datetime WHERE id = ?", $prmId);
        return $qryBonplan ;
    }

    function deleteByAgendaId($prmId=0){
        $qryBonplan = $this->db->query("DELETE FROM agenda_datetime WHERE agenda_id = ?", $prmId);
        return $qryBonplan ;
    }

    function getWhere($prmWhere = "0 = 0", $prmOffset = 0, $prmLimit = 0, $prmOrder = " agenda_datetime.id ASC ") {
        if (empty($prmOrder)) {
            $prmOrder = " agenda_datetime.id ASC ";
        }
        
        $qryString = "
            SELECT
                *
            FROM
                agenda_datetime
            WHERE " . $prmWhere . "
            ORDER BY " . $prmOrder . "";
        if (!(($prmOffset == 0) and ($prmLimit == 0))) {
            $qryString .= " LIMIT $prmOffset, $prmLimit";
        }
        return $this->db->query($qryString)->result();
    }


}