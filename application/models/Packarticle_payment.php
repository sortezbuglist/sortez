
<?php
class packarticle_payment extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
	function getById($id=0){
        if (isset($id)&&$id!=0&&$id!=''){
            $Sql = "select * from packarticle_payment where id =". $id  ;
            $Query = $this->db->query($Sql);
            return $Query->row();
        } else return null;
	}

    function getAll(){
        $request = $this->db->query("
            SELECT
                *
            FROM
                packarticle_payment
            ORDER BY id DESC
        ");
        if($request->num_rows() > 0) {
            return $request->result();
        }
    }

	function getWhere($where=''){
        $sql = "
            SELECT
                *
            FROM
                packarticle_payment
            WHERE 
              0=0 
        ";
        if (isset($where) && $where != '') $sql .= " AND ".$where;
        $request = $this->db->query($sql);
        if($request->num_rows() > 0) {
            return $request->result();
        }
    }

    function insert($prmData) {
        $this->db->insert("packarticle_payment", $prmData);
        return $this->db->insert_id();
    }

    function update($prmData) {
        $this->db->where("id", $prmData["id"]);
        $this->db->update("packarticle_payment", $prmData);
        $objAnnonce = $this->getById($prmData["id"]);
        return $objAnnonce->IdVille;
    }
}