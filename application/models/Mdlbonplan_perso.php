<?php
class mdlbonplan_perso extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    function getarticle_persoById($id=0){
        $Sql = "select * from bonplan_perso where id =". $id  ;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }
    function GetAll(){
        $qryarticle_perso = $this->db->query("
            SELECT *
            FROM
                bonplan_perso
            ORDER BY id DESC
        ");
        if($qryarticle_perso->num_rows() > 0) {
            return $qryarticle_perso->result();
        }
    }


    function GetBonplan_bonplan_persoByUser($IdCommercant="0", $IdUsers_ionauth="0"){

        $qryCategorie_sql = "SELECT
                bonplan_perso.*
                FROM
                bonplan_perso
                WHERE
                0=0 ";
        if (isset($IdCommercant) && $IdCommercant!="0" && $IdCommercant!="" && $IdCommercant!=NULL && $IdCommercant!=0) {
            $qryCategorie_sql .= " AND bonplan_perso.IdCommercant = '".$IdCommercant."'";
        }
        if (isset($IdUsers_ionauth) && $IdUsers_ionauth!="0" && $IdUsers_ionauth!="" && $IdUsers_ionauth!=NULL && $IdUsers_ionauth!=0) {
            $qryCategorie_sql .= " AND bonplan_perso.IdUsers_ionauth = '".$IdUsers_ionauth."'";
        }
        $qryCategorie_sql .= " LIMIT 1";

        ////$this->firephp->log($qryCategorie_sql, 'qryCategorie_sql');

        $Query = $this->db->query($qryCategorie_sql);
        return $Query->row();
    }

    function GetArticle_article_persoByIdUsers_ionauth($user_ionauth_id){
        $qryCategorie = $this->db->query("
                SELECT
                bonplan_perso.Nom,
                bonplan_perso.Idarticle_perso,
                COUNT(bonplan.id) as nb_agenda
                FROM
                bonplan
                INNER JOIN bonplan_perso ON bonplan.Idarticle_perso_localisation = bonplan_perso.Idarticle_perso
                WHERE
                (bonplan.IsActif = 1 OR
                bonplan.IsActif = 2) AND
                bonplan.IdUsers_ionauth = '".$user_ionauth_id."'
                GROUP BY
                bonplan_perso.Idarticle_perso
                ORDER BY
                bonplan_perso.Nom ASC

        ");
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetCommercant_article_perso(){
        $qryCategorie = $this->db->query("
            SELECT
            commercants.IdCommercant,
            bonplan_perso.Idarticle_perso,
            bonplan_perso.Nom,
            bonplan_perso.CodePostal,
            bonplan_perso.NomSimple,
            commercants.IsActif,
            COUNT(commercants.IdCommercant) as nbCommercant
            FROM
            commercants
            Inner Join bonplan_perso ON commercants.Idarticle_perso = bonplan_perso.Idarticle_perso
            Inner Join ass_commercants_abonnements ON commercants.IdCommercant = ass_commercants_abonnements.IdCommercant
            INNER JOIN ass_commercants_sousrubriques ON commercants.IdCommercant = ass_commercants_sousrubriques.IdCommercant
            WHERE
            commercants.IsActif =  '1' AND
            DATEDIFF(ass_commercants_abonnements.DateFin ,CURDATE()) >0
            GROUP BY
            bonplan_perso.Idarticle_perso
            ORDER BY
            bonplan_perso.Nom ASC
        ");
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function delete_article_perso($prmId){

        $qryBonplan = $this->db->query("DELETE FROM bonplan_perso WHERE id = ?", $prmId) ;
        return $qryBonplan ;
    }

    function insert_bonplan_perso($prmData) {
        $this->db->insert("bonplan_perso", $prmData);
        return $this->db->insert_id();
    }

    function update_bonplan_perso($prmData) {
        //var_dump($prmData);die();
        $this->db->where("id", $prmData["id"]);
        $this->db->update("bonplan_perso", $prmData);
        $objAnnonce = $this->getarticle_persoById($prmData["IdCommercant"]);
        return $objAnnonce->id;
    }
    function GetAgenda_agenda_persoByUser($IdCommercant="0", $IdUsers_ionauth="0"){

        $qryCategorie_sql = "SELECT
                bonplan_perso.*
                FROM
                bonplan_perso
                WHERE
                0=0 ";
        if (isset($IdCommercant) && $IdCommercant!="0" && $IdCommercant!="" && $IdCommercant!=NULL && $IdCommercant!=0) {
            $qryCategorie_sql .= " AND bonplan_perso.IdCommercant = '".$IdCommercant."'";
        }
        if (isset($IdUsers_ionauth) && $IdUsers_ionauth!="0" && $IdUsers_ionauth!="" && $IdUsers_ionauth!=NULL && $IdUsers_ionauth!=0) {
            $qryCategorie_sql .= " AND bonplan_perso.IdUsers_ionauth = '".$IdUsers_ionauth."'";
        }
        $qryCategorie_sql .= " LIMIT 1";

        ////$this->firephp->log($qryCategorie_sql, 'qryCategorie_sql');

        $Query = $this->db->query($qryCategorie_sql);
        return $Query->row();
    }
    function GetAgendaCategorie_ByIdCommercant($_iCommercantId)
    {

        $sqlcat = "
            SELECT
            agenda_categ.agenda_categid,
            agenda_categ.agenda_typeid,
            agenda_categ.category,
            COUNT(bonplan.bonplan_id) as nb_bonplan
            FROM
            agenda_categ
            INNER JOIN bonplan ON agenda_categ.agenda_categid = bonplan.bonplan_categorie_id
           
             agenda.IdCommercant = '" . $_iCommercantId . "' ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcat .= " AND bonplan.bonplan_ville_id = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcat .= " AND bonplan.bonplan_ville_id = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcat .= " bonplan.bonplan_ville_id = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
            }
            $sqlcat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcat .= " AND bonplan.bonplan_ville_id IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        $sqlcat .= "
            GROUP BY
            agenda_categ.agenda_categid
            ";

        $qryCategorie = $this->db->query($sqlcat);

        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }
    function getAlldeposantAgenda($_iCommercantId = "0")
    {

        $zSqlAlldeposantAgenda = "SELECT
            commercants.NomSociete,
            commercants.IdCommercant
            FROM
            bonplan
            INNER JOIN commercants ON bonplan.commercant_id = commercants.IdCommercant
            ";

        if ($_iCommercantId != "0" && $_iCommercantId != NULL && $_iCommercantId != "" && $_iCommercantId != 0) {
            $zSqlAlldeposantAgenda .= " AND bonplan.commercant_id = '" . $_iCommercantId . "'";
        }

        $zSqlAlldeposantAgenda .= " GROUP BY
           bonplan.commercant_id";

        $zQueryAlldeposantAgenda = $this->db->query($zSqlAlldeposantAgenda);
        return $zQueryAlldeposantAgenda->result();
    }


}