<?php

class mdlfidelity extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    function listeFidelityRecherche($_iCategorieId = 0, $i_CommercantId = 0, $_zMotCle = "", $_iFavoris = "", $_limitstart = 0, $_limitend = 10000, $_iIdVille = 0, $_iIdDepartement = 0, $iOrderBy = "", $inputFromGeo = "0", $inputGeoValue = "10", $inputGeoLatitude = "0", $inputGeoLongitude = "0", $session_inputFidelityType = 0, $table_fidelity_used = "capital")
    {
        $txtWhereFavoris = "";

        $thisss =& get_instance();
        $thisss->load->library('ion_auth');
        $thisss->load->model("ion_auth_used_by_club");

        if ($thisss->ion_auth->logged_in()) {
            $user_ion_auth = $thisss->ion_auth->user()->row();
            $iduser = $thisss->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $thisss->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser = 0;


        if ($_iFavoris == "1" && $iduser != "") {
            $sqlFavoris = "
                SELECT
                    CONCAT('(',GROUP_CONCAT(ass_commercants_users.IdCommercant SEPARATOR ','),')') AS IdCommercant
                FROM
                    ass_commercants_users
                WHERE
                    ass_commercants_users.IdUser = '" . $iduser . "'
                GROUP BY ass_commercants_users.IdUser
            ";
            $QueryFavoris = $this->db->query($sqlFavoris);
            if ($QueryFavoris->num_rows() > 0) {
                $colFavoris = $QueryFavoris->result();
                $txtWhereFavoris .= " AND commercants.IdCommercant IN " . $colFavoris[0]->IdCommercant;
            } else {
                $txtWhereFavoris .= " AND commercants.IdCommercant IN (0) ";
            }
        }


        $zSqlListeBonPlan = "
            SELECT
            ";
        if ($table_fidelity_used == "remise") {
            $zSqlListeBonPlan .= "
                card_remise.id,
                card_remise.date_debut, 
                card_remise.date_fin,
                card_remise.description,
                card_remise.montant,
                card_remise.value_type,
                card_remise.image1,
                ";
        } else if ($table_fidelity_used == "tampon") {
            $zSqlListeBonPlan .= "
                card_tampon.id,
                card_tampon.date_debut,
                card_tampon.date_fin,
                card_tampon.description,
                card_tampon.tampon_value,
                card_tampon.image1,
                ";
        } else {
            $zSqlListeBonPlan .= "
                card_capital.id,
                card_capital.date_debut, 
                card_capital.date_fin,
                card_capital.description,
    			card_capital.remise_value,
                card_capital.montant,
                card_capital.image1,
                ";
        }
        $zSqlListeBonPlan .= "
            commercants.Photo5 AS Photo5,
            commercants.NomSociete AS NomSociete,
            commercants.user_ionauth_id,
            villes.Nom AS ville,
            commercants.Adresse1 AS quartier,
            commercants.Adresse2 AS rue,
            commercants.IdCommercant,
            sous_rubriques.IdSousRubrique,
            sous_rubriques.IdRubrique,
            sous_rubriques.Nom,
            commercants.IdVille,
            commercants.nom_url
            ";

        if ($inputFromGeo == "1") {
            $zSqlListeBonPlan .= " , (6371 * acos(cos(radians($inputGeoLatitude)) * cos(radians(commercants.latitude)) * cos(radians(commercants.longitude) - radians($inputGeoLongitude)) + sin(radians($inputGeoLatitude)) * sin(radians(commercants.latitude)))) AS distance ";
        }

        $zSqlListeBonPlan .= "
            FROM
            ";
        if ($table_fidelity_used == "remise") {
            $zSqlListeBonPlan .= "
                card_remise
                Inner Join commercants ON commercants.IdCommercant = card_remise.id_commercant
                ";
        } else if ($table_fidelity_used == "tampon") {
            $zSqlListeBonPlan .= "
                card_tampon
                Inner Join commercants ON commercants.IdCommercant = card_tampon.id_commercant
                ";
        } else {
            $zSqlListeBonPlan .= "
                card_capital
                Inner Join commercants ON commercants.IdCommercant = card_capital.id_commercant
                ";
        }

        $zSqlListeBonPlan .= "
            Left Join villes ON villes.IdVille = commercants.IdVille
            Inner Join ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
            Inner Join sous_rubriques ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique
            ";

        if ($table_fidelity_used == "remise") $zSqlListeBonPlan .= "LEFT OUTER JOIN bonplan on bonplan.bonplan_commercant_id = card_remise.id_commercant";
        else if ($table_fidelity_used == "tampon") $zSqlListeBonPlan .= "LEFT OUTER JOIN bonplan on bonplan.bonplan_commercant_id = card_tampon.id_commercant";
        else $zSqlListeBonPlan .= "LEFT OUTER JOIN bonplan on bonplan.bonplan_commercant_id = card_capital.id_commercant";

        $zSqlListeBonPlan .= "
            WHERE
            commercants.IsActif = 1 AND commercants.referencement_fidelite = 1  
        ";

        if ($table_fidelity_used == "remise") {
            $zSqlListeBonPlan .= " AND card_remise.is_activ = 1 AND NOW() BETWEEN    card_remise.date_debut AND card_remise.date_fin";
        } else if ($table_fidelity_used == "tampon") {
            $zSqlListeBonPlan .= " AND card_tampon.is_activ = 1 AND NOW() BETWEEN    card_tampon.date_debut AND card_tampon.date_fin";
        } else {
            $zSqlListeBonPlan .= " AND card_capital.is_activ = 1 AND NOW() BETWEEN    card_capital.date_debut AND card_capital.date_fin";
        }

        if ($inputFromGeo == "1") {
            $zSqlListeBonPlan .= " AND commercants.latitude != '' AND commercants.longitude != '' ";
        }

        /*if($_iCategorieId != 0){
            $zSqlListeBonPlan .= " AND sous_rubriques.IdSousRubrique =" . $_iCategorieId ;
        }*/
        if (isset($_iCategorieId) && is_array($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSqlListeBonPlan .= " AND (";
            if (sizeof($_iCategorieId) == 1) {
                $zSqlListeBonPlan .= " sous_rubriques.IdSousRubrique = " . $_iCategorieId[0];
            } else {
                $zSqlListeBonPlan .= " sous_rubriques.IdSousRubrique = " . $_iCategorieId[0];
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSqlListeBonPlan .= " OR sous_rubriques.IdSousRubrique = " . $_iCategorieId[$ii_rand];
                }
            }
            $zSqlListeBonPlan .= " )";
        }
        if ($i_CommercantId != 0) {
            $zSqlListeBonPlan .= " AND commercants.IdCommercant = '$i_CommercantId' ";
        }
        /*
        if($_zMotCle != ""){
            $zSqlListeBonPlan .= " AND bonplan_titre LIKE '%" . $_zMotCle . "%'" ;
        }
        */
        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);

        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListeBonPlan .= "\n AND ";
                }
                if ($table_fidelity_used == "remise") {
                    $zSqlListeBonPlan .= " ( \n" .
                        " UPPER(card_remise.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(commercants.Adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(commercants.Adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " )
                    ";
                } else if ($table_fidelity_used == "tampon") {
                    $zSqlListeBonPlan .= " ( \n" .
                        " UPPER(card_tampon.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(commercants.Adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(commercants.Adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " )
                    ";
                } else {
                    $zSqlListeBonPlan .= " ( \n" .
                        " UPPER(card_capital.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(commercants.Adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(commercants.Adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " )
                    ";
                }

                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListeBonPlan .= "\n OR ";
                }
            }
        }
        if ($_iFavoris == "1" && $iduser != "") {
            $zSqlListeBonPlan .= $txtWhereFavoris;
        }

        //add ville id list on query
        //
        if ($_iIdVille != 0 && $_iIdVille != "" && $_iIdVille != null) {
            $zSqlListeBonPlan .= " AND commercants.IdVille=" . $_iIdVille;
        } elseif ($_iIdDepartement != 0 && $_iIdDepartement != "" && $_iIdDepartement != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iIdDepartement);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListeBonPlan .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListeBonPlan .= " commercants.IdVille=" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListeBonPlan .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListeBonPlan .= ")";
            }
        }


        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlListeBonPlan .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlListeBonPlan .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlListeBonPlan .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlListeBonPlan .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListeBonPlan .= " OR ";
            }
            $zSqlListeBonPlan .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlListeBonPlan .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        // demo account commercant should not appear on annuaire list
        $zSqlListeBonPlan .= " AND commercants.IdCommercant <> '301298' ";


        if ($inputFromGeo == "1") {
            $zSqlListeBonPlan .= "  HAVING distance < $inputGeoValue  ";
        }

        if ($table_fidelity_used == "remise") {
            if ($iOrderBy == '1') $iOrderBy_value = " order by card_remise.id desc ";
            else if ($iOrderBy == '2') $iOrderBy_value = " order by commercants.nbrevisites desc ";
            else if ($iOrderBy == '3') $iOrderBy_value = " order by card_remise.id asc ";
            else $iOrderBy_value = " order by card_remise.date_fin asc ";
        } else if ($table_fidelity_used == "tampon") {
            if ($iOrderBy == '1') $iOrderBy_value = " order by card_tampon.id desc ";
            else if ($iOrderBy == '2') $iOrderBy_value = " order by commercants.nbrevisites desc ";
            else if ($iOrderBy == '3') $iOrderBy_value = " order by card_tampon.id asc ";
            else $iOrderBy_value = " order by card_tampon.date_fin asc ";
        } else {
            if ($iOrderBy == '1') $iOrderBy_value = " order by card_capital.id desc ";
            else if ($iOrderBy == '2') $iOrderBy_value = " order by commercants.nbrevisites desc ";
            else if ($iOrderBy == '3') $iOrderBy_value = " order by card_capital.id asc ";
            else $iOrderBy_value = " order by card_capital.date_fin asc ";
        }

        //$zSqlListeBonPlan .= $iOrderBy_value;

        $zSqlListeBonPlan .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $zSqlListeBonPlan."<br/><br/>";
        //die("TEST 3");
        ////$this->firephp->log($iOrderBy, 'iOrderBy_3');

        $zQueryListeBonPlan = $this->db->query($zSqlListeBonPlan);
        return $zQueryListeBonPlan->result();
    }

    function listeFidelityRecherche_new($_iCategorieId = 0, $i_CommercantId = 0, $_zMotCle = "", $_iFavoris = "", $_limitstart = 0, $_limitend = 10000, $_iIdVille = 0, $_iIdDepartement = 0, $iOrderBy = "", $inputFromGeo = "0", $inputGeoValue = "10", $inputGeoLatitude = "0", $inputGeoLongitude = "0", $session_inputFidelityType = 0, $table_fidelity_used = "capital")
    {
        $txtWhereFavoris = "";

        $thisss =& get_instance();
        $thisss->load->library('ion_auth');
        $thisss->load->model("ion_auth_used_by_club");

        if ($thisss->ion_auth->logged_in()) {
            $user_ion_auth = $thisss->ion_auth->user()->row();
            $iduser = $thisss->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser == null || $iduser == 0 || $iduser == "") {
                $iduser = $thisss->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser = 0;


        if ($_iFavoris == "1" && $iduser != "") {
            $sqlFavoris = "
                SELECT
                    CONCAT('(',GROUP_CONCAT(ass_commercants_users.IdCommercant SEPARATOR ','),')') AS IdCommercant
                FROM
                    ass_commercants_users
                WHERE
                    ass_commercants_users.IdUser = '" . $iduser . "'
                GROUP BY ass_commercants_users.IdUser
            ";
            $QueryFavoris = $this->db->query($sqlFavoris);
            if ($QueryFavoris->num_rows() > 0) {
                $colFavoris = $QueryFavoris->result();
                $txtWhereFavoris .= " AND commercants.IdCommercant IN " . $colFavoris[0]->IdCommercant;
            } else {
                $txtWhereFavoris .= " AND commercants.IdCommercant IN (0) ";
            }
        }


        $zSqlListeBonPlan = "
            SELECT
            ";
        if ($table_fidelity_used == "remise") {
            $zSqlListeBonPlan .= "
                card_remise.id,
                card_remise.date_debut, 
                card_remise.date_fin,
                card_remise.description,
                card_remise.montant,
                card_remise.value_type,
                card_remise.image1,
                ";
        } else if ($table_fidelity_used == "tampon") {
            $zSqlListeBonPlan .= "
                card_tampon.id,
                card_tampon.date_debut,
                card_tampon.date_fin,
                card_tampon.description,
                card_tampon.tampon_value,
                card_tampon.image1,
                ";
        } else {
            $zSqlListeBonPlan .= "
                card_capital.id,
                card_capital.date_debut, 
                card_capital.date_fin,
                card_capital.description,
    			card_capital.remise_value,
                card_capital.montant,
                card_capital.image1,
                ";
        }
        $zSqlListeBonPlan .= "
            commercants.Photo5 AS Photo5,
            commercants.NomSociete AS NomSociete,
            commercants.user_ionauth_id,
            villes.Nom AS ville,
            commercants.Adresse1 AS quartier,
            commercants.Adresse2 AS rue,
            commercants.IdCommercant,
            rubriques.IdRubrique,
            rubriques.Nom,
            commercants.IdVille,
            commercants.nom_url
            ";

        if ($inputFromGeo == "1") {
            $zSqlListeBonPlan .= " , (6371 * acos(cos(radians($inputGeoLatitude)) * cos(radians(commercants.latitude)) * cos(radians(commercants.longitude) - radians($inputGeoLongitude)) + sin(radians($inputGeoLatitude)) * sin(radians(commercants.latitude)))) AS distance ";
        }

        $zSqlListeBonPlan .= "
            FROM
            ";
        if ($table_fidelity_used == "remise") {
            $zSqlListeBonPlan .= "
                card_remise
                Inner Join commercants ON commercants.IdCommercant = card_remise.id_commercant
                ";
        } else if ($table_fidelity_used == "tampon") {
            $zSqlListeBonPlan .= "
                card_tampon
                Inner Join commercants ON commercants.IdCommercant = card_tampon.id_commercant
                ";
        } else {
            $zSqlListeBonPlan .= "
                card_capital
                Inner Join commercants ON commercants.IdCommercant = card_capital.id_commercant
                ";
        }

        $zSqlListeBonPlan .= "
            Left Join villes ON villes.IdVille = commercants.IdVille
            Inner Join ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
            Inner Join rubriques ON rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique
            ";

        if ($table_fidelity_used == "remise") $zSqlListeBonPlan .= "LEFT OUTER JOIN bonplan on bonplan.bonplan_commercant_id = card_remise.id_commercant";
        else if ($table_fidelity_used == "tampon") $zSqlListeBonPlan .= "LEFT OUTER JOIN bonplan on bonplan.bonplan_commercant_id = card_tampon.id_commercant";
        else $zSqlListeBonPlan .= "LEFT OUTER JOIN bonplan on bonplan.bonplan_commercant_id = card_capital.id_commercant";

        $zSqlListeBonPlan .= "
            WHERE
            commercants.IsActif = 1 AND commercants.referencement_fidelite = 1  
        ";

        if ($table_fidelity_used == "remise") {
            $zSqlListeBonPlan .= " AND card_remise.is_activ = 1 AND NOW() BETWEEN    card_remise.date_debut AND card_remise.date_fin";
        } else if ($table_fidelity_used == "tampon") {
            $zSqlListeBonPlan .= " AND card_tampon.is_activ = 1 AND NOW() BETWEEN    card_tampon.date_debut AND card_tampon.date_fin";
        } else {
            $zSqlListeBonPlan .= " AND card_capital.is_activ = 1 AND NOW() BETWEEN    card_capital.date_debut AND card_capital.date_fin";
        }

        if ($inputFromGeo == "1") {
            $zSqlListeBonPlan .= " AND commercants.latitude != '' AND commercants.longitude != '' ";
        }

        /*if($_iCategorieId != 0){
            $zSqlListeBonPlan .= " AND sous_rubriques.IdSousRubrique =" . $_iCategorieId ;
        }*/
        if (isset($_iCategorieId) && is_array($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSqlListeBonPlan .= " AND (";
            if (sizeof($_iCategorieId) == 1) {
                $zSqlListeBonPlan .= " 	rubriques.IdRubrique = " . $_iCategorieId[0];
            } else {
                $zSqlListeBonPlan .= " 	rubriques.IdRubrique = " . $_iCategorieId[0];
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSqlListeBonPlan .= " OR 	rubriques.IdRubrique = " . $_iCategorieId[$ii_rand];
                }
            }
            $zSqlListeBonPlan .= " )";
        }
        if ($i_CommercantId != 0) {
            $zSqlListeBonPlan .= " AND commercants.IdCommercant = '$i_CommercantId' ";
        }
        /*
        if($_zMotCle != ""){
            $zSqlListeBonPlan .= " AND bonplan_titre LIKE '%" . $_zMotCle . "%'" ;
        }
        */
        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);

        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListeBonPlan .= "\n AND ";
                }
                if ($table_fidelity_used == "remise") {
                    $zSqlListeBonPlan .= " ( \n" .
                        " UPPER(card_remise.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(commercants.Adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(commercants.Adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " )
                    ";
                } else if ($table_fidelity_used == "tampon") {
                    $zSqlListeBonPlan .= " ( \n" .
                        " UPPER(card_tampon.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(commercants.Adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(commercants.Adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " )
                    ";
                } else {
                    $zSqlListeBonPlan .= " ( \n" .
                        " UPPER(card_capital.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(commercants.Adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(commercants.Adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " )
                    ";
                }

                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListeBonPlan .= "\n OR ";
                }
            }
        }
        if ($_iFavoris == "1" && $iduser != "") {
            $zSqlListeBonPlan .= $txtWhereFavoris;
        }

        //add ville id list on query
        //
        if ($_iIdVille != 0 && $_iIdVille != "" && $_iIdVille != null) {
            $zSqlListeBonPlan .= " AND commercants.IdVille=" . $_iIdVille;
        } elseif ($_iIdDepartement != 0 && $_iIdDepartement != "" && $_iIdDepartement != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iIdDepartement);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListeBonPlan .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListeBonPlan .= " commercants.IdVille=" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListeBonPlan .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListeBonPlan .= ")";
            }
        }


        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlListeBonPlan .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlListeBonPlan .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlListeBonPlan .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlListeBonPlan .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListeBonPlan .= " OR ";
            }
            $zSqlListeBonPlan .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlListeBonPlan .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        // demo account commercant should not appear on annuaire list
        $zSqlListeBonPlan .= " AND commercants.IdCommercant <> '301298' ";


        if ($inputFromGeo == "1") {
            $zSqlListeBonPlan .= "  HAVING distance < $inputGeoValue  ";
        }

        if ($table_fidelity_used == "remise") {
            if ($iOrderBy == '1') $iOrderBy_value = " order by card_remise.id desc ";
            else if ($iOrderBy == '2') $iOrderBy_value = " order by commercants.nbrevisites desc ";
            else if ($iOrderBy == '3') $iOrderBy_value = " order by card_remise.id asc ";
            else $iOrderBy_value = " order by card_remise.date_fin asc ";
        } else if ($table_fidelity_used == "tampon") {
            if ($iOrderBy == '1') $iOrderBy_value = " order by card_tampon.id desc ";
            else if ($iOrderBy == '2') $iOrderBy_value = " order by commercants.nbrevisites desc ";
            else if ($iOrderBy == '3') $iOrderBy_value = " order by card_tampon.id asc ";
            else $iOrderBy_value = " order by card_tampon.date_fin asc ";
        } else {
            if ($iOrderBy == '1') $iOrderBy_value = " order by card_capital.id desc ";
            else if ($iOrderBy == '2') $iOrderBy_value = " order by commercants.nbrevisites desc ";
            else if ($iOrderBy == '3') $iOrderBy_value = " order by card_capital.id asc ";
            else $iOrderBy_value = " order by card_capital.date_fin asc ";
        }

        //$zSqlListeBonPlan .= $iOrderBy_value;

        $zSqlListeBonPlan .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $zSqlListeBonPlan."<br/><br/>";
        //die("TEST 3");
        ////$this->firephp->log($iOrderBy, 'iOrderBy_3');

        $zQueryListeBonPlan = $this->db->query($zSqlListeBonPlan);
        return $zQueryListeBonPlan->result();
    }


    function bonPlanParCommercant($_iCommercantId)
    {

        $zSqlBonPlanParCommercant = "
        SELECT * , commercants.NomSociete AS NomSociete, villes.Nom AS ville,
        commercants.Adresse1 AS quartier, commercants.Adresse2 AS rue,
        commercants.IdCommercant
        FROM
        bonplan
        LEFT JOIN commercants ON commercants.IdCommercant=bonplan.bonplan_commercant_id
        LEFT JOIN villes ON villes.IdVille=commercants.IdVille
        WHERE 0=0
        ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlBonPlanParCommercant .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlBonPlanParCommercant .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlBonPlanParCommercant .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlBonPlanParCommercant .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlBonPlanParCommercant .= " OR ";
            }
            $zSqlBonPlanParCommercant .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlBonPlanParCommercant .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        if (isset($_iCommercantId) && $_iCommercantId != 0 && $_iCommercantId != "" && $_iCommercantId != NULL && $_iCommercantId != "0")
            $zSqlBonPlanParCommercant .= "
            AND
        commercants.IdCommercant=" . $_iCommercantId;

        $zQueryBonPlanParCommercant = $this->db->query($zSqlBonPlanParCommercant);
        //$toBonPlanParCommercant = $zQueryBonPlanParCommercant->result() ;
        $toBonPlanParCommercant = $zQueryBonPlanParCommercant->row();
        //return $toBonPlanParCommercant[0] ;
        return $toBonPlanParCommercant;
    }

    function lastBonplanCom($_iCommercantId)
    {

        $zSqlBonPlan = "SELECT *  FROM bonplan ";
        if (isset($_iCommercantId) && $_iCommercantId != 0 && $_iCommercantId != "" && $_iCommercantId != NULL && $_iCommercantId != "0")
            $zSqlBonPlan .= " WHERE bonplan_commercant_id =" . $_iCommercantId;
        $zSqlBonPlan .= " order by bonplan_id desc LIMIT 1";

        $zQuery = $this->db->query($zSqlBonPlan);
        return $zQuery->row();
    }

    function lastBonplanCom2($_iCommercantId)
    {

        $zRqt = "SELECT *  FROM bonplan ";
        if (isset($_iCommercantId) && $_iCommercantId != 0 && $_iCommercantId != "" && $_iCommercantId != NULL && $_iCommercantId != "0")
            $zRqt .= " WHERE bonplan_commercant_id =" . $_iCommercantId;
        $zRqt .= " order by bonplan_id desc LIMIT 1";

        $qryBonplan = $this->db->query($zRqt);
        if ($qryBonplan->num_rows() > 0) {
            $Res = $qryBonplan->result();
            return $Res[0];
        }
    }

    function getById($_id)
    {
        $zRqt = "SELECT *  FROM bonplan WHERE bonplan_id =" . $_id;
        $qryBonplan = $this->db->query($zRqt);
        if ($qryBonplan->num_rows() > 0) {
            $Res = $qryBonplan->result();
            return $Res[0];
        }
    }

    function decremente_quantite_bonplan($_id)
    {
        $obj_bonplan = $this->GetById($_id);
        $nb_bp = intval($obj_bonplan->bonplan_quantite);
        if ($nb_bp > 0) {
            $nb_bp = $nb_bp - 1;
        }
        $zSqldecrement = "UPDATE bonplan SET bonplan_quantite='" . $nb_bp . "' WHERE bonplan_id=" . $_id;
        $this->db->query($zSqldecrement);

    }

    function getListeBonPlan($_iCommercantId = '0')
    {
        $zSqlBonPlanParCommercant = "SELECT bonplan.* , commercants.Photo5 as Photo5, commercants.NomSociete AS NomSociete, villes.Nom AS ville, commercants.Adresse1 AS quartier, commercants.Adresse2 AS rue, commercants.IdCommercant FROM bonplan LEFT JOIN commercants ON commercants.IdCommercant=bonplan.bonplan_commercant_id LEFT JOIN villes ON villes.IdVille=commercants.IdVille WHERE commercants.IdCommercant=" . $_iCommercantId . " order by bonplan_id desc LIMIT 1"; //bon plan unique à revoir
        $zQueryBonPlanParCommercant = $this->db->query($zSqlBonPlanParCommercant);
        $toBonPlanParCommercant = $zQueryBonPlanParCommercant->result();
        return $toBonPlanParCommercant;
    }

    function insertBonplan($prmData)
    {
        $this->db->insert("bonplan", $prmData);
        return $this->db->insert_id();
    }

    function updateBonplan($prmData)
    {
        $prmData = (array)$prmData;
        $this->db->where("bonplan_id", $prmData["bonplan_id"]);
        $this->db->update("bonplan", $prmData);
        $objBonplan = $this->GetById($prmData["bonplan_id"]);
        return $objBonplan->bonplan_id;
    }

    // function GetById($prmId) {
    // $qryBonplan =  $this->db->query("SELECT * FROM bonplan WHERE bonplan_id = ?", $prmId);
    // if ($qryBonplan->num_rows() > 0) {
    // $Res = $qryBonplan->result();
    // return $Res[0];
    // }
    // }

    function listeMesBonplans($prmId)
    {
        /*$zRqt = "SELECT `bonplan_id` , `bonplan_commercant_id` , `bonplan_titre` , `bonplan_texte` , `bonplan_nombrepris` , `bonplan_date_debut` , `bonplan_date_fin` , `bonplan_photo` , `bonplan_categorie_id`  , `bonplan_ville_id` , `bonplan_photo1` , `bonplan_photo2` , `bonplan_photo3` , `bonplan_photo4` , commercants.Photo5 as Photo5, c.Nom as categorie_nom, v.Nom as Nom
                FROM bonplan
                LEFT OUTER JOIN commercants ON (commercants.IdCommercant = bonplan.bonplan_commercant_id),
                rubriques c, villes v
                WHERE bonplan.bonplan_categorie_id = c.IdRubrique
                AND bonplan.bonplan_ville_id = v.IdVille
                AND bonplan.bonplan_commercant_id = " . $prmId */

        $zRqt = "SELECT `bonplan_id` , `bonplan_commercant_id` , `bonplan_titre` , `bonplan_texte` , `bonplan_nombrepris` ,
            `bonplan_date_debut` , `bonplan_date_fin` , `bonplan_photo` , `bonplan_categorie_id`  , `bonplan_ville_id` , 
            `bonplan_photo1` , `bonplan_photo2` , `bonplan_photo3` , `bonplan_photo4` , commercants.Photo5 as Photo5
                FROM bonplan
                LEFT OUTER JOIN commercants ON (commercants.IdCommercant = bonplan.bonplan_commercant_id)
                WHERE bonplan.bonplan_commercant_id = " . $prmId . "

                ORDER BY bonplan_id DESC
                LIMIT 1
                ";
        $qryBonplan = $this->db->query($zRqt);
        if ($qryBonplan->num_rows() > 0) {
            $Res = $qryBonplan->result();
            return $Res[0];
        }
    }

    function GetBonplan($prmId)
    {
        $qryBonplan = $this->db->query("SELECT * FROM bonplan WHERE bonplan_id = ?", $prmId);
        if ($qryBonplan->num_rows() > 0) {
            $Res = $qryBonplan->result();
            return $Res[0];
        }
    }

    function supprimeBonplans($prmId)
    {

        /*$zRpsBonplan =  $this->db->query("SELECT * FROM bonplan WHERE bonplan_id = ?", $prmId);
        $toBonplan = $zRpsBonplan->result() ;
        $oBonplan = $toBonplan[0] ;*/
        /*if (file_exists("application/resources/front/images/".$oBonplan->Bonplan_photo1)==true){
            unlink ("application/resources/front/images/".$oBonplan->Bonplan_photo1) ;
        }
        if (file_exists("application/resources/front/images/".$oBonplan->Bonplan_photo2)==true){
            unlink ("application/resources/front/images/".$oBonplan->Bonplan_photo2) ;
        }
        if (file_exists("application/resources/front/images/".$oBonplan->Bonplan_photo3)==true){
            unlink ("application/resources/front/images/".$oBonplan->Bonplan_photo3) ;
        }
        if (file_exists("application/resources/front/images/".$oBonplan->Bonplan_photo4)==true){
            unlink ("application/resources/front/images/".$oBonplan->Bonplan_photo4) ;
        }*/
        $qryBonplan = $this->db->query("DELETE FROM bonplan WHERE bonplan_id = ?", $prmId);
        return $qryBonplan;
    }

    function getValideBonPlanByIdCommernats($prmListCommercants = "")
    {
        $qryBonplan = $this->db->query("
            SELECT
                bonplan.*,
                commercants.NomSociete
            FROM
                (bonplan
                LEFT JOIN    commercants ON commercants.IdCommercant = bonplan.bonplan_commercant_id)     
            WHERE
                bonplan.bonplan_commercant_id IN ($prmListCommercants)
                AND NOW() BETWEEN    bonplan.bonplan_date_debut AND bonplan.bonplan_date_fin
        ");
        return $qryBonplan->result();
    }
    public function get_annonce_by_id($id,$idcom){

        $zSql = "SELECT * FROM card_capital
 WHERE 
 card_capital.id_commercant=$idcom AND
 card_capital.is_activ =1 AND
    card_capital.id =". $id  ;
        $zQuery = $this->db->query($zSql);
        //var_dump($zQuery->num_rows());die();
        if ($zQuery->num_rows()>0){return $zQuery->row();}else{
            $zSql2 = "SELECT * FROM card_remise
         WHERE 
            card_remise.id_commercant=$idcom AND
            card_remise.is_activ =1 AND
            card_remise.id =". $id  ;
            $zQuery2 = $this->db->query($zSql2);
            if ($zQuery2->num_rows()>0){return $zQuery2->row();}else{
                $zSql3 = "SELECT * FROM card_tampon
         WHERE 
            card_tampon.id_commercant=$idcom AND
            card_tampon.is_activ =1 AND
            card_tampon.id =". $id  ;
                $zQuery3 = $this->db->query($zSql3);
                return $zQuery3->row();
            }
        }
    }
}