<?php
/**
 * Created by PhpStorm.
 * User: rand
 * Date: 14/11/2017
 * Time: 10:34
 */

class vsv_ville_session extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    function getById($id=0){
        $Sql = "select * from vsv_ville_session where id =" . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByIdVille($idVille=0){
        $Sql = "select * from vsv_ville_session where id_ville =" . $idVille . " ORDER BY id DESC";
        $Query = $this->db->query($Sql);
        if ($Query->num_rows() > 0) {
            return $Query->result();
        }
    }
    function getByIdSession($id_session_ci=0){
        $Sql = "select * from vsv_ville_session where id_session_ci =" . $id_session_ci . " ORDER BY id DESC";
        $Query = $this->db->query($Sql);
        if ($Query->num_rows() > 0) {
            return $Query->result();
        }
    }

    function getByIpAdress($ip_adress=0){
        $Sql = "select * from vsv_ville_session where id_vsv =" . $ip_adress . " ORDER BY id DESC";
        $Query = $this->db->query($Sql);
        if ($Query->num_rows() > 0) {
            return $Query->result();
        }
    }
    function getWhere($query=" id=0 "){
        $Sql = "select * from vsv_ville_session where " . $query ;
        $Query = $this->db->query($Sql);
        if ($Query->num_rows() > 0) {
            return $Query->result();
        }
    }

    function getAl1()
    {
        $getvivresaville = $this->db->query("
            SELECT *
            FROM
                vsv_ville_session
            ORDER BY id DESC
        ");
        if ($getvivresaville->num_rows() > 0) {
            return $getvivresaville->result();
        }
    }

    function delete($prmId)
    {
        $sql = $this->db->query("DELETE FROM vsv_ville_session WHERE id= ?", $prmId);
        return $sql;
    }
    function deleteWhere($query=" id=0 ")
    {
        $sql = $this->db->query("DELETE FROM vsv_ville_session WHERE  ".$query." ");
        return $sql;
    }

    function insert($prmData)
    {
        $this->db->insert("vsv_ville_session", $prmData);
        return $this->db->insert_id();
    }

    function update($prmData)
    {
        $this->db->where("id", $prmData["id"]);
        $this->db->update("vsv_ville_session", $prmData);
        $sql = $this->getById($prmData["id"]);
        return $sql->id;
    }

}