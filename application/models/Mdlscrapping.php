<?php

class Mdlscrapping extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function save_api_data($data){
        $this->db->insert('Api_scrapping_data',$data);
    }

   public function get_all($first =0){
        if ($first !=0){
            $this->db->where('idCount',$first);
            $res = $this->db->get("Api_scrapping_data");
            return $res->result();
        }
   }

   public function get_all_api(){
      $res = $this->db->get('Api_scrapping_data');
      return $res->result();
   }
   public function delete_api($id){
    $this->db->where('id',$id);
    $this->db->delete('Api_scrapping_data');
   }
   public function get_by_id($id){
    $this->db->where('id',$id);
    $res = $this->db->get('Api_scrapping_data');
    return $res->row();
   }
   public function update_status($data){
       $this->db->insert('status_scrapping',$data);
   }
   public function check(){
    $this->db->order_by("id", "desc");
    $res = $this->db->get('status_scrapping');
    $result = $res->row();
    if(isset($result) AND !empty($result) AND $result->status == '1'){
        return true;
    }else{
        return false;
    }
   }
   public function reset_status(){
       $this->db->empty_table('status_scrapping');
   }
   public function getNomcomByidCom($idcom){
        $this->db->where("IdCommercant",$idcom);
        $all = $this->db->get("commercants");
        return $all->row();
   }
}