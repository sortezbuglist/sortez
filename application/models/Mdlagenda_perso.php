<?php
class mdlagenda_perso extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    function getagenda_persoById($id=0){
        $Sql = "select * from agenda_perso where id =". $id  ;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }
    function GetAll(){
        $qryagenda_perso = $this->db->query("
            SELECT *
            FROM
                agenda_perso
            ORDER BY id DESC
        ");
        if($qryagenda_perso->num_rows() > 0) {
            return $qryagenda_perso->result();
        }
    }

   
    function GetAgenda_agenda_persoByUser($IdCommercant="0", $IdUsers_ionauth="0"){

        $qryCategorie_sql = "SELECT
                agenda_perso.*
                FROM
                agenda_perso
                WHERE
                0=0 ";
        if (isset($IdCommercant) && $IdCommercant!="0" && $IdCommercant!="" && $IdCommercant!=NULL && $IdCommercant!=0) {
            $qryCategorie_sql .= " AND agenda_perso.IdCommercant = '".$IdCommercant."'";
        }
        if (isset($IdUsers_ionauth) && $IdUsers_ionauth!="0" && $IdUsers_ionauth!="" && $IdUsers_ionauth!=NULL && $IdUsers_ionauth!=0) {
            $qryCategorie_sql .= " AND agenda_perso.IdUsers_ionauth = '".$IdUsers_ionauth."'";
        }
        $qryCategorie_sql .= " LIMIT 1";

        ////$this->firephp->log($qryCategorie_sql, 'qryCategorie_sql');

        $Query = $this->db->query($qryCategorie_sql);
        return $Query->row();
    }

    function GetAgenda_agenda_persoByIdUsers_ionauth($user_ionauth_id){
        $qryCategorie = $this->db->query("
                SELECT
                agenda_perso.Nom,
                agenda_perso.Idagenda_perso,
                COUNT(agenda.id) as nb_agenda
                FROM
                agenda
                INNER JOIN agenda_perso ON agenda.Idagenda_perso_localisation = agenda_perso.Idagenda_perso
                WHERE
                (agenda.IsActif = 1 OR
                agenda.IsActif = 2) AND
                agenda.IdUsers_ionauth = '".$user_ionauth_id."'
                GROUP BY
                agenda_perso.Idagenda_perso
                ORDER BY
                agenda_perso.Nom ASC

        ");
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetCommercant_agenda_perso(){
        $qryCategorie = $this->db->query("
            SELECT
            commercants.IdCommercant,
            agenda_perso.Idagenda_perso,
            agenda_perso.Nom,
            agenda_perso.CodePostal,
            agenda_perso.NomSimple,
            commercants.IsActif,
            COUNT(commercants.IdCommercant) as nbCommercant
            FROM
            commercants
            Inner Join agenda_perso ON commercants.Idagenda_perso = agenda_perso.Idagenda_perso
            Inner Join ass_commercants_abonnements ON commercants.IdCommercant = ass_commercants_abonnements.IdCommercant
            INNER JOIN ass_commercants_sousrubriques ON commercants.IdCommercant = ass_commercants_sousrubriques.IdCommercant
            WHERE
            commercants.IsActif =  '1' AND
            DATEDIFF(ass_commercants_abonnements.DateFin ,CURDATE()) >0
            GROUP BY
            agenda_perso.Idagenda_perso
            ORDER BY
            agenda_perso.Nom ASC
        ");
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function delete_agenda_perso($prmId){

        $qryBonplan = $this->db->query("DELETE FROM agenda_perso WHERE id = ?", $prmId) ;
        return $qryBonplan ;
    }

    function insert_agenda_perso($prmData) {
        $this->db->insert("agenda_perso", $prmData);
        return $this->db->insert_id();
    }

    function update_agenda_perso($prmData) {
        $this->db->where("id", $prmData["id"]);
        $this->db->update("agenda_perso", $prmData);
        $objAnnonce = $this->getagenda_persoById($prmData["id"]);
        return $objAnnonce->id;
    }
}