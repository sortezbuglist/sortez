<?php
class AssCommercantSousRubrique extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    function Insert($prmData) {
        $this->db->insert("ass_commercants_sousrubriques", $prmData);
        return $this->db->insert_id();
    }
    
    function GetWhere($prmWhere = "0 = 0", $prmOffset = 0, $prmLimit = 0, $prmOrder = " IdCommercant DESC ") {
		if (empty($prmOrder)) {
			$prmOrder = " IdCommercant DESC ";
		}
		
		
       	$qryString = "SELECT   * FROM ass_commercants_sousrubriques WHERE " . $prmWhere . " ORDER BY " . $prmOrder . "";
		if (!(($prmOffset == 0) and ($prmLimit == 0))) {
			$qryString .= " LIMIT $prmOffset, $prmLimit";
		}
        return $this->db->query($qryString)->result();
    }
    
    function DeleteByIdCommercant($prmIdCommercant) {
        $this->db->where("IdCommercant", $prmIdCommercant);
        $this->db->delete("ass_commercants_sousrubriques");
    }
    
    function commsousrubriqueByRubrique($idRubrique) {
        $qryString = "SELECT
                        sous_rubriques.IdSousRubrique,
                        sous_rubriques.IdRubrique,
                        sous_rubriques.Nom,
                        ass_commercants_sousrubriques.IdCommercant,
                        count(sous_rubriques.IdSousRubrique) as nbCommercant
                        FROM
                        ass_commercants_sousrubriques
                        INNER JOIN sous_rubriques ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique
                        WHERE
                        sous_rubriques.IdRubrique = '".$idRubrique."'
                        GROUP BY
                        sous_rubriques.IdSousRubrique                            
";
        return $this->db->query($qryString)->result();
    }
    
}