<?php
class Sousrubrique extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    function GetAll(){
        $qrySousRubriques = $this->db->query("
            SELECT
                IdSousRubrique,
                IdRubrique,
                Nom
            FROM
                sous_rubriques
            ORDER BY Nom ASC
        ");
        if($qrySousRubriques->num_rows() > 0) {
            return $qrySousRubriques->result();
        }
    }
	 function GetByRubrique($id=0){
        $qrySousRubriques = $this->db->query("
            SELECT
                IdSousRubrique,
                IdRubrique,
                Nom
            FROM
                sous_rubriques
				where IdRubrique =".$id. 
            " ORDER BY Nom ASC
        ");
        if($qrySousRubriques->num_rows() > 0) {
            return $qrySousRubriques->result();
        }
    }
    
    
    function GetById($prmId) {
        $qry_sous_rubriques =  $this->db->query("SELECT * FROM sous_rubriques WHERE IdSousRubrique = ?", $prmId);
        if ($qry_sous_rubriques->num_rows() > 0) {
            $Res = $qry_sous_rubriques->result();
            return $Res[0];
        }
    }
}