<?php
class mdl_card_fiche_client_capital extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }

	function getById($id=0){
		$Sql = "select * from card_fiche_client_capital where id =". $id ;
		$Query = $this->db->query($Sql);
		return $Query->row();
	}
	function GetAll(){
        $qry = $this->db->query("
            SELECT * 
            FROM
                card_fiche_client_capital
            ORDER BY id DESC
        ");
        if($qry->num_rows() > 0) {
            return $qry->result();
        }
    }

    function getWhereOne($where=""){
        $query = "
            SELECT *
            FROM
                card_fiche_client_capital
            WHERE
              0=0
        ";
        if (isset($where) && $where!="") $query .= " AND ".$where;

        $query .= " LIMIT 1 ";

        $Query = $this->db->query($query);
        return $Query->row();
    }

    function getWhere($where=""){
        $query = "
            SELECT *
            FROM
                card_fiche_client_capital
            WHERE
              0=0
        ";
        if (isset($where) && $where!="") $query .= " AND ".$where;

        $qry = $this->db->query($query);
        if($qry->num_rows() > 0) {
            return $qry->result();
        }
    }

    function delete($prmId){
    
        $qry = $this->db->query("DELETE FROM card_fiche_client_capital WHERE id = ?", $prmId) ;
        return $qry ;
    }

    function insert($prmData) {
        $this->db->insert("card_fiche_client_capital", $prmData);
        return $this->db->insert_id();
    }

    function update($prmData) {
        $this->db->where("id", $prmData["id"]);
        $this->db->update("card_fiche_client_capital", $prmData);
        $obj = $this->getById($prmData["id"]);
        return $obj->id;
    }
    function updateByUCId($prmData) {
    	$this->db->where("id_user", $prmData['id_user']);
    	$this->db->where("id_commercant", $prmData['id_commercant']);
    	unset($prmData['id_user']);
    	unset($prmData['id_commercant']);
    	return 	$this->db->update("card_fiche_client_capital", $prmData);
    }
    
    function update_where($data,$where){
    	return $this->db->update('card_fiche_client_capital', $data, $where);
    }
    
    function deleteByUCid($userId,$commercantId){
    	$this->db->delete('card_fiche_client_capital', array('id_user' => $userId,'id_commercant'=>$commercantId));
    }
    
    function getByCritere($where=array()){
    	$this->db->select('commercants.NomSociete,commercants.CodePostal,commercants.adresse1,commercants.Email,commercants.TelMobile,commercants.nom_url,card_fiche_client_capital.id as fiche_id,card_fiche_client_capital.solde_capital as solde,card_capital.montant as objectif,card_capital.*,card_fiche_client_capital.*,villes.ville_nom');
    	$this->db->join('card_capital', 'card_capital.id_commercant = card_fiche_client_capital.id_commercant');
        $this->db->join('commercants', 'commercants.IdCommercant = card_fiche_client_capital.id_commercant');
        $this->db->join('villes',"villes.IdVille = commercants.IdVille");
        $query = $this->db->get_where('card_fiche_client_capital', $where);
    	return  $query->result();
    }
    function delete_where($where=array()){
    	$this->db->where($where);
    	return   (!empty($where)) ? $this->db->delete("card_fiche_client_capital"): false;
    }
}