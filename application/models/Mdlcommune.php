<?php
class mdlcommune extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
	function GetAutocomplete($queryString=0){
		$Sql = "select * from com_cant where N_Com_min like '%$queryString%'  order by N_Com_min " ;		
		$Query = $this->db->query($Sql);
		return $Query->result();
	}
	function getById($id=0){
		$Sql = "select * from com_cant where id_commune =". $id ;		
		$Query = $this->db->query($Sql);
		return $Query->row();
	}

    function GetByIdVille($id=0){
        $Sql = "
            SELECT
            villes.IdVille,
            villes.Nom,
            com_cant.C_Com,
            com_cant.N_Com_min,
            com_cant.N_Cant_min,
            com_cant.id_commune
            FROM
            villes
            INNER JOIN com_cant ON com_cant.C_Com = villes.ville_code_commune
            WHERE
            villes.IdVille = '".$id."'
            LIMIT 1
        ";       
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

	function GetAll(){
        $qryVille = $this->db->query("
           SELECT id_commune, C_Com, N_Com_min
                FROM
                com_cant
                GROUP BY
                com_cant.C_Com, com_cant.id_commune
                ORDER BY N_Com_min ASC
        ");
        if($qryVille->num_rows() > 0) {
            return $qryVille->result();
        }
    }
    
    function delete($prmId){
    
        $qryBonplan = $this->db->query("DELETE FROM com_cant WHERE id_commune = ?", $prmId) ;
        return $qryBonplan ;
    }

    function insert($prmData) {
        $this->db->insert("com_cant", $prmData);
        return $this->db->insert_id();
    }

    function update($prmData) {
        $this->db->where("id_commune", $prmData["id_commune"]);
        $this->db->update("com_cant", $prmData);
        $objResult = $this->getById($prmData["id_commune"]);
        return $objResult->id_commune;
    }


    function GetBonplanCommunes(){
        $sqlcat = "
          SELECT
            com_cant.id_commune,
            com_cant.N_Com_min,
            com_cant.C_Com,
            commercants.NomSociete as commercant,
            COUNT(IdCommercant) as nb_commercant
            FROM
            bonplan
            Inner Join commercants ON bonplan.bonplan_commercant_id = commercants.IdCommercant
            Inner Join villes ON villes.IdVille = commercants.IdVille
            Inner Join com_cant ON com_cant.C_Com = villes.ville_code_commune
            where commercants.IsActif = 1 ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcat .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcat .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcat .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
            }
            $sqlcat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcat .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
            GROUP BY
            com_cant.C_Com ";

        $qryCategorie = $this->db->query($sqlcat);
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }
    




}