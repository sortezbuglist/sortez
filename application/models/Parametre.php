<?php
class Parametre extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    function getByCode($prmCode) {
        $this->db->where("Code", $prmCode);
        return $this->db->get("parametres")->result();
    }
    
    function getWhereCodeIn($prmWhere) {
        $this->db->where_in("Code", $prmWhere);
        return $this->db->get("parametres")->result();
    }
    
    function Update($prmData) {
        $prmData = (array)$prmData;
        $this->db->where("IdParametre", $prmData["IdParametre"]);
        $this->db->update("parametres", $prmData);
    }
}