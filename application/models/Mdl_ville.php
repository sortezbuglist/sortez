<?php
class mdl_ville extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
	function GetAutocompleteVille($queryString=0){
		$Sql = "select * from villes where Nom like '%$queryString%'  order by Nom " ;		
		$Query = $this->db->query($Sql);
		return $Query->result();
	}
	function getVilleById($id=0){
		$Sql = "select * from villes where IdVille =". $id  ;		
		$Query = $this->db->query($Sql);
		return $Query->row();
	}
    function GetAll(){
        $qryVilles = $this->db->query("
            SELECT
                IdVille,
                Nom,
                CodePostal
            FROM
                villes
            ORDER BY Nom ASC
        ");
        if($qryVilles->num_rows() > 0) {
            return $qryVilles->result();
        }
    }

    function updateVille($prmData) {
        $this->db->where("IdVille", $prmData["IdVille"]);
        $this->db->update("villes", $prmData);
        $objAnnonce = $this->getVilleById($prmData["IdVille"]);
        return $objAnnonce->IdVille;
    }
    function GetAgendaVilles_specific(){
        $sqlcat = "
          SELECT
            villes.IdVille,
            commercants.referencement_agenda,
            villes.ville_nom,
            COUNT(agenda.id) as nbCommercant
            FROM
            agenda
            Inner Join villes ON villes.IdVille = agenda.IdVille_localisation
            Inner Join commercants ON commercants.IdCommercant = agenda.IdCommercant
            where agenda.IsActif = 1  AND commercants.referencement_agenda=1 AND villes.IdVille = 33710 or villes.IdVille=33611 OR villes.IdVille =33629 OR villes.IdVille =33622 OR villes.IdVille =33684 OR villes.IdVille =33571
             OR villes.IdVille =33667 OR villes.IdVille =33644 OR villes.IdVille =33577 OR villes.IdVille =33608 OR villes.IdVille =26968 OR villes.IdVille =7971
              OR villes.IdVille =33712 OR villes.IdVille =33653 OR villes.IdVille =33618 OR villes.IdVille =33661 OR villes.IdVille =33601 OR villes.IdVille =33671
               OR villes.IdVille =33599 OR villes.IdVille =33615 OR villes.IdVille =33652 OR villes.IdVille =33641 OR villes.IdVille =33619 OR villes.IdVille =36941
                OR villes.IdVille =33592 OR villes.IdVille =33620 OR villes.IdVille =36944 OR villes.IdVille =33714 OR villes.IdVille =33602 OR villes.IdVille =33637
                 OR villes.IdVille =33666 OR villes.IdVille =33648 OR villes.IdVille =33649 OR villes.IdVille =33631 OR villes.IdVille =33612 OR villes.IdVille =33669 OR agenda.departement_id=6";

        $sqlcat .= "
            GROUP BY
            villes.IdVille ";

        $qryCategorie = $this->db->query($sqlcat);
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }

    }

    function GetAgendaDepartements_pvc(){
        $sqlcat = "
          SELECT
            villes.IdVille,
            commercants.referencement_agenda,
            villes.ville_nom,
            COUNT(villes.IdVille) as nbCommercant
            FROM
            agenda
            Inner Join villes ON villes.IdVille = agenda.IdVille_localisation
            Inner Join commercants ON commercants.IdCommercant = agenda.IdCommercant
            where agenda.IsActif = 1  AND commercants.referencement_agenda=1 AND villes.IdVille = 33710 or villes.IdVille=33611 OR villes.IdVille =33629 OR villes.IdVille =33622 OR villes.IdVille =33684 OR villes.IdVille =33571
             OR villes.IdVille =33667 OR villes.IdVille =33644 OR villes.IdVille =33577 OR villes.IdVille =33608 OR villes.IdVille =26968 OR villes.IdVille =7971
              OR villes.IdVille =33712 OR villes.IdVille =33653 OR villes.IdVille =33618 OR villes.IdVille =33661 OR villes.IdVille =33601 OR villes.IdVille =33671
               OR villes.IdVille =33599 OR villes.IdVille =33615 OR villes.IdVille =33652 OR villes.IdVille =33641 OR villes.IdVille =33619 OR villes.IdVille =36941
                OR villes.IdVille =33592 OR villes.IdVille =33620 OR villes.IdVille =36944 OR villes.IdVille =33714 OR villes.IdVille =33602 OR villes.IdVille =33637
                 OR villes.IdVille =33666 OR villes.IdVille =33648 OR villes.IdVille =33649 OR villes.IdVille =33631 OR villes.IdVille =33612 OR villes.IdVille =33669 OR agenda.departement_id=6";

        $sqlcat .= "
            GROUP BY
            villes.IdVille ";

        $qryCategorie = $this->db->query($sqlcat);
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }

    }
    function getById($id=0){
        $Sql = "select * from villes where IdVille =". $id ;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }
}