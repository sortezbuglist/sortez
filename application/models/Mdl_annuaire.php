<?php

class mdl_annuaire extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function GetById_IsActif($id = 0)
    {
        $Query_txt = "
            SELECT
            annuaire.*,
            villes.Nom AS ville,
            annuaire_categ.category,
            annuaire_categ.annuaire_categid,
            annuaire_subcateg.subcateg

            FROM
                annuaire
            LEFT OUTER JOIN villes ON villes.IdVille = annuaire.IdVille_localisation
            INNER JOIN annuaire_categ ON annuaire_categ.annuaire_categid = annuaire.annuaire_categid
            LEFT OUTER JOIN annuaire_subcateg ON annuaire_subcateg.annuaire_subcategid = annuaire.annuaire_subcategid

            WHERE id = '" . $id . "'
            AND (annuaire.IsActif = '1' OR annuaire.IsActif = '2')
            LIMIT 1
        "; //var_dump($Query_txt); die();
        $qry = $this->db->query($Query_txt);
        $result_main = $qry->row();
        return $result_main;
    }

    function Increment_accesscount($id = 0)
    {
        $current_annuaire = $this->GetById($id);
        $last_accesscount_nb = intval($current_annuaire->accesscount);
        $last_accesscount_nb_total = $last_accesscount_nb + 1;
        $zSql = "UPDATE annuaire SET accesscount='" . $last_accesscount_nb_total . "' WHERE id=" . $id;
        $this->db->query($zSql);
    }

    function GetAnnuaireCategorie_ByIdCommercant($_iCommercantId)
    {
        $sqlcat = "
            SELECT
            annuaire_categ.annuaire_categid,
            annuaire_categ.annuaire_typeid,
            annuaire_categ.category,
            COUNT(annuaire.id) as nb_annuaire
            FROM
            annuaire_categ
            INNER JOIN annuaire ON annuaire_categ.annuaire_categid = annuaire.annuaire_categid
            WHERE
            
            (annuaire.IsActif = '1' OR annuaire.IsActif = '2')
            AND annuaire.IdCommercant = '" . $_iCommercantId . "' ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $sqlcat .= " AND annuaire.IdVille_localisation = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "0" && $localdata_IdVille != "") {
            $sqlcat .= " AND annuaire.IdVille_localisation = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $sqlcat .= " AND annuaire.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE

        $sqlcat .= "
            GROUP BY
            annuaire_categ.annuaire_categid
            ";

        $qryCategorie = $this->db->query($sqlcat);

        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAnnuaireCategorie()
    {
        $queryCat = "
            SELECT
            annuaire_categ.annuaire_categid,
            annuaire_categ.annuaire_typeid,
            annuaire_categ.category,
            COUNT(annuaire.id) as nb_annuaire
            FROM
            annuaire_categ
            INNER JOIN annuaire ON annuaire_categ.annuaire_categid = annuaire.annuaire_categid
            WHERE
            
            (annuaire.IsActif = '1' OR
            annuaire.IsActif = '2')
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $queryCat .= " AND annuaire.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "0" && $localdata_IdVille != "") {
            $queryCat .= " AND annuaire.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $queryCat .= " AND annuaire.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE

        $queryCat .= "
            GROUP BY
            annuaire_categ.annuaire_categid
            ";

        $qryCategorie = $this->db->query($queryCat);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function listeAnnuaireRecherche_annuaire_perso_liste_categ($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0", $referencement_annuaire = 1)
    {

        $zSqlListePartenaire = "
            SELECT
                COUNT(annuaire_datetime.id) as nb_annuaire, 
                annuaire_datetime.date_debut as datetime_debut, 
                annuaire_datetime.date_fin as datetime_fin, 
                annuaire_datetime.heure_debut as datetime_heure_debut,
                annuaire.*,
                villes.Nom AS ville,
                annuaire_categ.category,
                annuaire_categ.annuaire_categid,
                annuaire_subcateg.subcateg,
                commercants.NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                annuaire
                LEFT OUTER JOIN villes ON villes.IdVille = annuaire.IdVille_localisation
                INNER JOIN annuaire_categ ON annuaire_categ.annuaire_categid = annuaire.annuaire_categid
                LEFT OUTER JOIN annuaire_subcateg ON annuaire_subcateg.annuaire_subcategid = annuaire.annuaire_subcategid
                INNER JOIN commercants ON commercants.IdCommercant = annuaire.IdCommercant
                LEFT OUTER JOIN annuaire_datetime ON annuaire_datetime.annuaire_id = annuaire.id

            WHERE
                ( 
                (annuaire.IsActif = '1' OR annuaire.IsActif = '2') 
                AND commercants.IsActif = 1 
                ";
        if ($referencement_annuaire == 1) $zSqlListePartenaire .= " AND commercants.referencement_annuaire = " . $referencement_annuaire . " ";

        $zSqlListePartenaire .= "
                AND (annuaire.id <> '0' OR annuaire.id <> NULL OR annuaire.id <> 0) 

        ";

        if (is_array($inputIdCommercant)) {
            $zSql_all_icommercant_request = "";
            $zSql_all_icommercant_request .= " (";
            if (sizeof($inputIdCommercant) == 1) {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
            } else {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                for ($i_iCommercantId = 1; $i_iCommercantId < count($inputIdCommercant); $i_iCommercantId++) {
                    $zSql_all_icommercant_request .= " OR commercants.IdCommercant = '" . $inputIdCommercant[$i_iCommercantId] . "' ";
                }
            }
            $zSql_all_icommercant_request .= " ) ";
            //$zSqlListePartenaire .= $zSql_all_icommercant_request;
        } else if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }

        if ($IdUsers_ionauth != "0" && $IdUsers_ionauth != "" && $IdUsers_ionauth != NULL && $IdUsers_ionauth != 0) {
            $zSqlListePartenaire .= " AND annuaire.IdUsers_ionauth = '" . $IdUsers_ionauth . "'";
        }

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);


            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (annuaire_datetime.date_debut >= '" . $date_of_day . "' AND annuaire_datetime.date_fin <= '" . $date_of_day . "' ) OR ( annuaire_datetime.date_debut IS NULL AND annuaire_datetime.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (annuaire_datetime.date_debut >= '" . $next_saturday_day . "' AND annuaire_datetime.date_fin <= '" . $next_sunday_day . "') OR (annuaire_datetime.date_fin >= '" . $next_saturday_day . "' AND annuaire_datetime.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (annuaire_datetime.date_debut >= '" . $last_sunday_day . "' AND annuaire_datetime.date_fin <= '" . $next_saturday_day . "') OR (annuaire_datetime.date_fin >= '" . $last_sunday_day . "' AND annuaire_datetime.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (annuaire_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND annuaire_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') OR (annuaire_datetime.date_fin >= '" . $last_sunday_of_next_monday . "' AND annuaire_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (annuaire_datetime.date_debut >= '" . $first_day_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_month . "') OR (annuaire_datetime.date_fin >= '" . $first_day_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_month . "' AND annuaire_datetime.date_fin >= '" . $date_of_day . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (annuaire_datetime.date_debut >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') OR (annuaire_datetime.date_fin >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (annuaire_datetime.date_debut >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') OR (annuaire_datetime.date_fin >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (annuaire_datetime.date_debut >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') OR (annuaire_datetime.date_fin >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (annuaire_datetime.date_debut >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') OR (annuaire_datetime.date_fin >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (annuaire_datetime.date_debut >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') OR (annuaire_datetime.date_fin >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (annuaire_datetime.date_debut >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') OR (annuaire_datetime.date_fin >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (annuaire_datetime.date_debut >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') OR (annuaire_datetime.date_fin >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (annuaire_datetime.date_debut >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') OR (annuaire_datetime.date_fin >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (annuaire_datetime.date_debut >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') OR (annuaire_datetime.date_fin >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (annuaire_datetime.date_debut >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') OR (annuaire_datetime.date_fin >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (annuaire_datetime.date_debut >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') OR (annuaire_datetime.date_fin >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (annuaire_datetime.date_debut >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') OR (annuaire_datetime.date_fin >= '" . $first_day_user_month . "' AND annuaire_datetime.date_fin <= '" . $last_day_user_month . "') )";

        }

        if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null && $_iVilleId != "0") {
            if (is_array($_iVilleId)) {
                $zSql_all_iville_request = "";
                $zSql_all_iville_request .= " (";
                if (sizeof($_iVilleId) == 1) {
                    if ($_iVilleId[0] != 0 && $_iVilleId[0] != "" && $_iVilleId[0] != null && $_iVilleId[0] != "0") {
                        $zSql_all_iville_request .= " annuaire.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    } else {
                        $zSql_all_iville_request .= " 0=0 ";
                    }
                } else {
                    $zSql_all_iville_request .= " annuaire.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    for ($i_iVilleId = 1; $i_iVilleId < count($_iVilleId); $i_iVilleId++) {
                        $zSql_all_iville_request .= " OR annuaire.IdVille_localisation = '" . $_iVilleId[$i_iVilleId] . "' ";
                    }
                }
                $zSql_all_iville_request .= " ) ";
                ////$this->firephp->log($zSql_all_iville_request, 'zSql_all_iville_request');
                //$zSqlListePartenaire .= $zSql_all_iville_request;
            }
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " annuaire.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            $zSqlListePartenaire .= " AND annuaire_datetime.date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            $zSqlListePartenaire .= " AND annuaire_datetime.date_fin = '" . $inputDatefin . "'";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " annuaire.annuaire_categid = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " annuaire.annuaire_categid = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR annuaire.annuaire_categid = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }

        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " annuaire.annuaire_categid = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " annuaire.annuaire_categid = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR annuaire.annuaire_categid = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }

        /*if($zSql_all_categ_request != "" && $zSql_all_subcateg_request == "") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;
        else if($zSql_all_categ_request == "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ".$zSql_all_subcateg_request;
        else if($zSql_all_categ_request != "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ( ".$zSql_all_categ_request." OR ".$zSql_all_subcateg_request." ) ";*/

        //if ($zSql_all_categ_request!="") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;


        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);

        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(annuaire.nom_societe ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(annuaire.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(annuaire.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(annuaire.category ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(annuaire_subcateg.subcateg ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(annuaire.adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(annuaire.adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(annuaire.organisateur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(annuaire.adresse_localisation_diffuseur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }


        $zSqlListePartenaire .= " ) ";

        if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") || (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) {
            $zSqlListePartenaire .= " AND ( ";

            if (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") $zSqlListePartenaire .= $zSql_all_icommercant_request;
            if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") && (isset($zSql_all_iville_request) && $zSql_all_iville_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_iville_request;
            else if (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") $zSqlListePartenaire .= $zSql_all_iville_request;
            if (((isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "")) && (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
            else if (isset($zSql_all_categ_request) && $zSql_all_categ_request != "") $zSqlListePartenaire .= $zSql_all_categ_request;

            $zSqlListePartenaire .= " ) ";
        }

        $zSqlListePartenaire .= " AND (annuaire.IsActif = '1' OR annuaire.IsActif = '2') ";

        $zSqlListePartenaire .= " group by annuaire.annuaire_categid ";

        if ($iOrderBy == '1') $iOrderBy_value = " order by annuaire.last_update desc ";
        else if ($iOrderBy == '2') $iOrderBy_value = " order by annuaire.accesscount desc ";
        else $iOrderBy_value = " order by annuaire_categ.category asc ";

        $zSqlListePartenaire .= $iOrderBy_value;

        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $_limitend;
        //echo $zSqlListePartenaire;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListePartenaire');
        //die("<br/>STOP");

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }


}