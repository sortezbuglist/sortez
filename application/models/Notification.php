<?php
class notification extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
	
    function getById($id=0){
		$Sql = "select * from notification where id =". $id ;		
		$Query = $this->db->query($Sql);
		return $Query->row();
	}
    function getByCode($code=""){
        $Sql = "select * from notification where code =". $code ;       
        $Query = $this->db->query($Sql);
        return $Query->row();
    }
    function isNotificationSent($id=0){
        $Sql = "select is_sent from notification where id =". $id ;       
        $Query = $this->db->query($Sql);
        return $Query->row();
    }
	function GetAll(){
        $qryVille = $this->db->query("
            SELECT *
            FROM
                notification 
        ");
        if($qryVille->num_rows() > 0) {
            return $qryVille->result();
        }
    }
    
    function delete($prmId){
    
        $qryBonplan = $this->db->query("DELETE FROM notification WHERE id = ?", $prmId) ;
        return $qryBonplan ;
    }

    function enable($prmId){
        $qryBonplan = $this->db->query("UPDATE `notification` SET `is_sent` = '1' WHERE `notification`.`id` = ?", $prmId) ;
        return $qryBonplan ;
    }

    function disable($prmId){
        $qryBonplan = $this->db->query("UPDATE `notification` SET `is_sent` = '0' WHERE `notification`.`id` = ?", $prmId) ;
        return $qryBonplan ;
    }
    
    function insert($prmData) {
        $this->db->insert("notification", $prmData);
        return $this->db->insert_id();
    }

    function update($prmData) {
        $this->db->where("id", $prmData["id"]);
        $this->db->update("notification", $prmData);
        $objResult = $this->getById($prmData["id"]);
        return $objResult->departement_id;
    }





}