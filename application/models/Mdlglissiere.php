<?php
class mdlglissiere extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
	
	function getById($id=0){
		$Sql = "select * from glissieres where id_glissiere =". $id  ;		
		$Query = $this->db->query($Sql);
		return $Query->row();
	}
    function GetAll(){
        $qryVilles = $this->db->query("
            SELECT
                *
            FROM
                glissieres

        ");
        if($qryVilles->num_rows() > 0) {
            return $qryVilles->result();
        }
    }

    function getall_bloc_info(){
        $qrybloc_info = $this->db->query("
            SELECT
                *
            FROM
                bloc_info

        ");
        if($qrybloc_info->num_rows() > 0) {
            return $qrybloc_info->result();
        }
    }

    function Update($prmData) {
        $prmData = (array)$prmData;
        $this->db->where("id_glissiere", $prmData["id_glissiere"]);
        $this->db->update("glissieres", $prmData);
        $objCommercant = $this->getById($prmData["id_glissiere"]);
        return $objCommercant->IdCommercant;
    }

    function Update_bloc_info($prmData) {
        $prmData = (array)$prmData;
        //var_dump($prmData);
        $this->db->where("IdCommercant", $prmData["IdCommercant"]);
        $this->db->update("bloc_info", $prmData);
        $objCommercant_bloc_info = $this->getById_bloc_info($prmData["IdCommercant"]);
        return $objCommercant_bloc_info->IdCommercant;
    }

    function Update_bloc_newsletter($prmData) {
        $prmData = (array)$prmData;
//var_dump($prmData);die("prmdata");
        $this->db->where("IdCommercant", $prmData["IdCommercant"]);
//var_dump($prmData["IdCommercant"]);die("id");
        $this->db->update("bloc_newsletter", $prmData);
    //var_dump($prmData);die();
        //$objCommercant_bloc_newsletter = $this->getById_bloc_newsletter($prmData["IdCommercant"]);
        $objCommercant_bloc_newsletter = $this->getId($prmData["IdCommercant"]);
    //var_dump($objCommercant_bloc_newsletter);die("id");
        return $objCommercant_bloc_newsletter[0]->IdCommercant;
        //var_dump($objCommercant_bloc_newsletter[0]->IdCommercant);
    }

    function getById_bloc_info($id=0){
        $qry = "
                    SELECT
                        *
                    FROM
                        bloc_info
                    WHERE
                        IdCommercant = '".$id."'
                    ";

        $resultat = $this->db->query($qry);
        if($resultat->num_rows() > 0) {
            $objresult = $resultat->result();
            return $objresult[0];
        }
    }
    function getById_bloc_newsletter($id=0){
        $qry = "
                    SELECT
                        *
                    FROM
                        bloc_newsletter
                    WHERE
                        IdCommercant = '".$id."'
                    ";

        $resultat = $this->db->query($qry);
        if($resultat->num_rows() > 0) {
            $objresult = $resultat->result();
            return $objresult[0];
            //var_dump($objresult);die();
        }
    }
    function getById_glissiere($id=0){
        $qry = "
                    SELECT
                        *
                    FROM
                        glissieres
                    WHERE
                        IdCommercant = '".$id."'
                    ";

        $resultat = $this->db->query($qry);
        if($resultat->num_rows() > 0) {
            $objresult = $resultat->result();
            return $objresult[0];
        }
    }

    function Insert($prmData) {
        $this->db->insert("glissieres", $prmData);
        return $this->db->insert_id();
    }

    function Insert_bloc_info($prmData) {
       // var_dump($prmData);
        $this->db->insert("bloc_info", $prmData);

        //return $this->db->insert_id();
    }
    function Insert_bloc_newsletter($prmData) {
      $this->db->insert("bloc_newsletter", $prmData);
        }
    function GetByIdCommercant($IdCommercant = "0"){
            $qry = "
                    SELECT
                        *
                    FROM
                        glissieres
                    WHERE
                        glissieres.IdCommercant = '".$IdCommercant."' 
                    ORDER BY id_glissiere desc
                    LIMIT 1

                    ";

            $qryVille = $this->db->query($qry);
            if($qryVille->num_rows() > 0) {
                $objresult = $qryVille->result();
                return $objresult[0];
            }
        
    }

    function GetByIdionauth($id_ionauth = "0"){

        
            $qry = "
                    SELECT
                        *
                    FROM
                        glissieres
                    WHERE
                        glissieres.id_ionauth = '".$id_ionauth."'
                    LIMIT 1

                    ";

            $qryVille = $this->db->query($qry);
            if($qryVille->num_rows() > 0) {
                return $qryVille->result();
            }
        
    }
    
    function get_by_idcom($idcom){
        $this->db->where('IdCommercant',$idcom);
       $res= $this->db->get('glissieres');
       return $res->row();
    }


        function getId($iduser){
        $this->db->select('IdCommercant');
        $this->db->where('IdCommercant',$iduser);
        $save=$this->db->get('bloc_newsletter');
        return $save->result();    

    }
    
    
}