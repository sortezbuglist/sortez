<?php
class mdl_card_fiche_client_tampon extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }

	function getById($id=0){
		$Sql = "select * from card_fiche_client_tampon where id =". $id ;
		$Query = $this->db->query($Sql);
		return $Query->row();
	}
	function GetAll(){
        $qry = $this->db->query("
            SELECT * 
            FROM
                card_fiche_client_tampon
            ORDER BY id DESC
        ");
        if($qry->num_rows() > 0) {
            return $qry->result();
        }
    }

    function getWhereOne($where=""){
        $query = "
            SELECT *
            FROM
                card_fiche_client_tampon
            WHERE
              0=0
        ";
        if (isset($where) && $where!="") $query .= " AND ".$where;

        $query .= " LIMIT 1 ";

        $Query = $this->db->query($query);
        return $Query->row();
    }

    function getWhere($where=""){
        $query = "
            SELECT *
            FROM
                card_fiche_client_tampon
            WHERE
              0=0
        ";
        if (isset($where) && $where!="") $query .= " AND ".$where;

        $qry = $this->db->query($query);
        if($qry->num_rows() > 0) {
            return $qry->result();
        }
    }


    function delete($prmId){
    
        $qry = $this->db->query("DELETE FROM card_fiche_client_tampon WHERE id = ?", $prmId) ;
        return $qry ;
    }

    function insert($prmData) {
        $this->db->insert("card_fiche_client_tampon", $prmData);
        return $this->db->insert_id();
    }

    function update($prmData) {
        $this->db->where("id", $prmData["id"]);
        $this->db->update("card_fiche_client_tampon", $prmData);
        $obj = $this->getById($prmData["id"]);
        return $obj->id;
    }
    function updateByUCId($prmData) {
    	$this->db->where("id_user", $prmData['id_user']);
    	$this->db->where("id_commercant", $prmData['id_commercant']);
    	unset($prmData['id_user']);
    	unset($prmData['id_commercant']);
    	return 	$this->db->update("card_fiche_client_tampon", $prmData);
    }
    
    function update_where($data,$where){
    	return $this->db->update('card_fiche_client_tampon', $data, $where);
    }
    
    function deleteByUCid($userId,$commercantId){
    	$this->db->delete('card_fiche_client_tampon', array('id_user' => $userId,'id_commercant'=>$commercantId));
    }
    
    function getByCritere($where=array()){
    	$this->db->select('commercants.NomSociete,commercants.Email,commercants.CodePostal,commercants.adresse1,commercants.TelMobile,commercants.nom_url,card_fiche_client_tampon.id as fiche_id,card_fiche_client_tampon.solde_tampon as solde,card_fiche_client_tampon.*,card_tampon.*,card_tampon.tampon_value as objectif,villes.ville_nom');
    	$this->db->join('card_tampon', 'card_tampon.id_commercant = card_fiche_client_tampon.id_commercant');
        $this->db->join('commercants', 'commercants.IdCommercant = card_fiche_client_tampon.id_commercant');
        $this->db->join('villes',"villes.IdVille = commercants.IdVille");
        $this->db->where($where);
    	$query = $this->db->get('card_fiche_client_tampon');
    	
    	return  $query->result();
    }
    
    function delete_where($where=array()){
    	$this->db->where($where);
    	return   (!empty($where)) ? $this->db->delete("card_fiche_client_tampon"): false;
    }
}