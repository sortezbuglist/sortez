<?php
class Mgenerernumcard extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    function GetAll(){
        $qryAbonnements = $this->db->query("
            SELECT
                *
            FROM
                abonnements
            ORDER BY IdAbonnement ASC
        ");
        if($qryAbonnements->num_rows() > 0) {
            return $qryAbonnements->result();
        }
    }

}