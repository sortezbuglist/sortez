<?php
class ion_auth_used_by_club extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    function GetUserlast_id($first_name, $username, $email){
    
        $qryuserid =  $this->db->select('id')
                        ->from('users_ionauth')
                        ->where('username', $username)
                        ->where('email', $email)
                        ->order_by('id','desc')
                        ->get()
                        ->result(); //->where('first_name', $first_name)

        if ($qryuserid) {
            return $qryuserid[0]->id;
        }

    }

    /*function GetUserlast_id($first_name, $username, $email){
        $qryuserid = $this->db->query("
            SELECT
            users_ionauth.id
            FROM
            users_ionauth
            WHERE
            users_ionauth.first_name = '".$first_name."' AND
            users_ionauth.username = '".$username."' AND
            users_ionauth.email = '".$email."'
            ORDER BY
            users_ionauth.id DESC
            LIMIT 1
            ");
        if ($qryuserid->num_rows() > 0) {
            $Res = $qryuserid->result();
            return $Res[0]->id;
        }
        
    }*/


    function get_user_id_from_ion_id($ion_id) {
        $qur = "
            SELECT
            users.IdUser
            FROM
            users_ionauth
            INNER JOIN users ON users.user_ionauth_id = users_ionauth.id
            WHERE
            users_ionauth.id = '".$ion_id."'
            LIMIT 1
            ";
        $qryuserid = $this->db->query($qur);
        if ($qryuserid->num_rows() > 0) {
            $Res = $qryuserid->result();
            return $Res[0]->IdUser;
        }
    }

    function get_ion_id_from_user_id($user_id) {
        $qryuserid = $this->db->query("
            SELECT
            users_ionauth.id
            FROM
            users_ionauth
            INNER JOIN users ON users.user_ionauth_id = users_ionauth.id
            WHERE
            users.IdUser = '".$user_id."'
            LIMIT 1
            ");
        if ($qryuserid->num_rows() > 0) {
            $Res = $qryuserid->result();
            return $Res[0]->id;
        }
    }

    function get_ion_id_from_association_id($assoc_id) {
        $qryuserid = $this->db->query("
            SELECT
            users_ionauth.id
            FROM
            users_ionauth
            INNER JOIN associations ON associations.user_ionauth_id = users_ionauth.id
            WHERE
            associations.IdAssociation = '".$assoc_id."'
            LIMIT 1
            ");
        if ($qryuserid->num_rows() > 0) {
            $Res = $qryuserid->result();
            return $Res[0]->id;
        }
    }

    function get_association_id_from_ion_id($ion_id) {
        $qryuserid = $this->db->query("
            SELECT
            associations.IdAssociation
            FROM
            associations
            INNER JOIN users_ionauth ON associations.user_ionauth_id = users_ionauth.id
            WHERE
            users_ionauth.id = '".$ion_id."'
            LIMIT 1
            ");
        if ($qryuserid->num_rows() > 0) {
            $Res = $qryuserid->result();
            return $Res[0]->IdAssociation;
        }
    }


    function get_commercant_id_from_ion_id($ion_id) {
        $qryuserid = $this->db->query("
            SELECT
            commercants.IdCommercant
            FROM
            commercants
            INNER JOIN users_ionauth ON commercants.user_ionauth_id = users_ionauth.id
            WHERE
            users_ionauth.id = '".$ion_id."'
            LIMIT 1
            ");
        if ($qryuserid->num_rows() > 0) {
            $Res = $qryuserid->result();
            return $Res[0]->IdCommercant;
        }
    }

    function get_ion_id_from_commercant_id($commercant_id) {
        $qryuserid = $this->db->query("
            SELECT
            users_ionauth.id
            FROM
            users_ionauth
            INNER JOIN commercants ON commercants.user_ionauth_id = users_ionauth.id
            WHERE
            commercants.IdCommercant = '".$commercant_id."'
            LIMIT 1
            ");
        if ($qryuserid->num_rows() > 0) {
            $Res = $qryuserid->result();
            return $Res[0]->id;
        }
    }


    function verify_commercant_is_active($commercant_id) {
        $qryuserid = $this->db->query("
            SELECT
            commercants.IdCommercant
            FROM
            ass_commercants_abonnements
            INNER JOIN commercants ON commercants.IdCommercant = ass_commercants_abonnements.IdCommercant
            WHERE
            commercants.IsActif = 1 AND
            ass_commercants_abonnements.DateDebut <= NOW() AND
            ass_commercants_abonnements.DateFin >= NOW() AND
            commercants.IdCommercant = '".$commercant_id."'
            LIMIT 1
            ");
        if ($qryuserid->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }


}