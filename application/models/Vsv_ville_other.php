<?php
class vsv_ville_other extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    function getById($id=0){
        $Sql = "select * from vsv_ville_other where id =" . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByIdVille($idVille=0){
        $Sql = "select * from vsv_ville_other where id_ville =" . $idVille . " ORDER BY id DESC";
        $Query = $this->db->query($Sql);
        if ($Query->num_rows() > 0) {
            return $Query->result();
        }
    }
    function getByIdVsv($id_vsv=0){
        $Sql = "select * from vsv_ville_other where id_vsv =" . $id_vsv . " ORDER BY id DESC";
        $Query = $this->db->query($Sql);
        if ($Query->num_rows() > 0) {
            return $Query->result();
        }
    }

    function getAl1()
    {
        $getvivresaville = $this->db->query("
            SELECT *
            FROM
                vsv_ville_other
            ORDER BY name ASC
        ");
        if ($getvivresaville->num_rows() > 0) {
            return $getvivresaville->result();
        }
    }

    function delete($prmId)
    {
        $sql = $this->db->query("DELETE FROM vsv_ville_other WHERE id= ?", $prmId);
        return $sql;
    }
    function deleteByIdVsv($Id_vsv=0, $Id_ville=0)
    {
        $sql = $this->db->query("DELETE FROM vsv_ville_other WHERE id_vsv = '".$Id_vsv."' AND id = '".$Id_ville."' ");
        return $sql;
    }

    function insert($prmData)
    {
        $this->db->insert("vsv_ville_other", $prmData);
        return $this->db->insert_id();
    }

    function update($prmData)
    {
        $this->db->where("id", $prmData["id"]);
        $this->db->update("vsv_ville_other", $prmData);
        $sql = $this->getById($prmData["id"]);
        return $sql->id;
    }

}