<?php
class mdl_card_capital extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    function getById($id=0){
        $Sql = "select * from card_capital where id =". $id ;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByIdIonauth($id_ionauth=0){
        $Sql = "select * from card_capital where id_ionauth ='". $id_ionauth ."' LIMIT 1;";
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByIdCommercant($id_comm=0){
        $Sql = "select * from card_capital where id_commercant =". $id_comm ." LIMIT 1;";
        $Query = $this->db->query($Sql);
        return $Query->row();
    }


    function getByIdRubrique($IdRubrique=0){
        $Sql = "
            SELECT
                card_capital.id,
                ass_commercants_rubriques.IdCommercant,
                ass_commercants_rubriques.IdRubrique
                FROM
                card_capital
                INNER JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = card_capital.id_commercant
                INNER JOIN commercants ON commercants.IdCommercant = card_capital.id_commercant
                WHERE
                card_capital.is_activ = 1 AND
                commercants.IsActif = 1 AND
                ass_commercants_rubriques.IdRubrique = '". $IdRubrique ."'
        ";
        $Query = $this->db->query($Sql);
        return $Query->row();
    }


    function getByIdSousRubrique($IdSousRubrique=0){
        $Sql = "
            SELECT
                card_capital.id,
                ass_commercants_sousrubriques.IdCommercant,
                ass_commercants_sousrubriques.IdSousRubrique
                FROM
                card_capital
                INNER JOIN ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = card_capital.id_commercant
                WHERE
                card_capital.is_activ = 1 AND
                ass_commercants_sousrubriques.IdSousRubrique = '". $IdSousRubrique ."'
        ";
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function GetAll(){
        $qry = $this->db->query("
            SELECT * 
            FROM
                card_capital
            ORDER BY id DESC
        ");
        if($qry->num_rows() > 0) {
            return $qry->result();
        }
    }

    function GetWhere($where='0=0'){
        $Sql = "
            SELECT * 
            FROM
                card_capital 
            ".$where." 
            ORDER BY id DESC
        ";
        $qry = $this->db->query($Sql);
        if($qry->num_rows() > 0) {
            return $qry->result();
        }
    }



    function delete($prmId){

        $qry = $this->db->query("DELETE FROM card_capital WHERE id = ?", $prmId) ;
        return $qry ;
    }

    function insert($prmData) {
        $this->db->insert("card_capital", $prmData);
        return $this->db->insert_id();
    }

    function update($prmData) {
        $this->db->where("id", $prmData["id"]);
        $this->db->update("card_capital", $prmData);
        $obj = $this->getById($prmData["id"]);
        return $obj->id;
    }
    function update2($prmData) {
        $this->db->where("id", $prmData["id"]);
        $this->db->update("card_remise", $prmData);
        $obj = $this->getById($prmData["id"]);
        //return $obj->id;
    }
    
    function delete_where($where=array()){
        $this->db->where($where);
        return   (!empty($where)) ? $this->db->delete("card_capital"): false;
    }
    
    function is_capital_active($id_commercant){
        $this->db->select('is_activ');
        $query = $this->db->get_where('card_capital', array('id_commercant'=>$id_commercant));
    
        return $query->first_row();
    }

    function id_capital_active($id_commercant){
        $this->db->select('id,description');
        $query = $this->db->get_where('card_capital', array('id_commercant'=>$id_commercant, 'is_activ'=>'1'));
    
        return $query->first_row();
    }

    function file_manager_update_capitalisation($iUser, $user_ion_auth_id, $image1, $img_file){
        // $this->db->where("id_commercant", $iUser);
        // $this->db->where("id_ionauth", $user_ion_auth_id);
        // $field=array($image1=>$img_file);
        // $this->db->update("card_capital",$field);
        $Sql = "
            UPDATE card_capital
            SET image1 = '".$img_file."'
            WHERE id_commercant = '".$iUser."' AND id_ionauth = '".$user_ion_auth_id."'
        ";
        $Query = $this->db->query($Sql);
    }
    function delete_manager_update_capitalisation($idcom){       
        $Sql = "
            UPDATE card_capital
            SET image1 = ''
            WHERE id_commercant = '".$idcom."'
        ";
        $Query = $this->db->query($Sql);
    }
}