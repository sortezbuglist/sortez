<?php
class Assoc_client_bonplan_model extends CI_Model {
	private $_table	  = 'assoc_client_bonplan';
	private $_users	  = 'users';
	private $_bonplan = 'bonplan';
	private $_commercants = 'commercants';
	
	public function get_list_by_criteria($criteres,$order_by=array()){
	   // var_dump($criteres);die();
		$this->db->select(
			'assoc_client_bonplan.id,
			assoc_client_bonplan.id_client,
			users.IdUser,
			users.Nom,
			users.Prenom,
			users.Email,
			users.Telephone,
			users.Portable,
			assoc_client_bonplan.date_validation,
			assoc_client_bonplan.datetime_validation,
			bonplan.*,
			villes.Nom as ville_client,
			users.Email as user_email,
			users.Portable as user_tel,
			commercants.NomSociete,
			commercants.nom_url,
			commercants.IdCommercant,
			assoc_client_bonplan.valide,
			assoc_client_bonplan.valide as bonplan_valide
			');
        $this->db->distinct($this->_users, $this->_table.'.id_client');
		$this->db->from($this->_table);
		$this->db->join($this->_users, $this->_table.'.id_client = '.$this->_users.'.IdUser');
		$this->db->join($this->_bonplan, $this->_table.'.id_bonplan = '.$this->_bonplan.'.bonplan_id');
		$this->db->join('villes','villes.idVille = users.idVille');
		$this->db->join($this->_commercants, $this->_commercants.'.IdCommercant = '.$this->_bonplan.'.bonplan_commercant_id');
		
		if(array_key_exists('valide', $criteres)){
			$this->db->where($this->_table.'.valide', $criteres['valide']);
			$this->db->where('assoc_client_bonplan.date_validation !=', '0000-00-00');
		}

		if(array_key_exists('id_commercant', $criteres)){
			$this->db->where($this->_bonplan.'.bonplan_commercant_id ', $criteres['id_commercant']);
		}

		if(array_key_exists('id_user', $criteres)){
			$this->db->where($this->_users.'.IdUser ', $criteres['id_user']);
		}

		if(array_key_exists('id', $criteres)){
			$this->db->where($this->_table.'.id ', $criteres['id']);
		}

		if(!empty($order_by) && (current($order_by)== "desc" || current($order_by)== "asc")){
			$this->db->order_by(key($order_by),current($order_by));
		}

		$query = $this->db->get();
		//var_dump($query);
		if(array_key_exists('num_rows', $criteres) && $criteres['num_rows']){
			return $query->num_rows();
		}
		//var_dump($query->result());die();
		if(array_key_exists('query', $criteres) && $criteres['query']){
			return $query;
		}
		
		return  ($query->num_rows() > 0) ? $query->result() : false;
	}
	
	/*public function get_list_by_criteria($criteres,$order_by=array()){
		$this->db->select(
		'users.IdUser,
		users.Nom,
		users.Prenom,
		card_capital.montant as objectif_capital,
		card_remise.montant as remise_montant,
		card_capital.description as description_capital,
		card_tampon.description as description_tampon,
		card_remise.description as description_remise,
		card_tampon.tampon_value,
		card_capital.is_activ as param_captital_active,
		card_tampon.is_activ as param_tampon_active,
		card_remise.is_activ as param_remise_active,				
		max(card_fiche_client_capital.solde_capital) as solde_capital,
		max(card_fiche_client_tampon.solde_tampon) as solde_tampon,
		max(card_fiche_client_remise.solde_remise) as solde_remise,
		assoc_client_bonplan.date_validation,
		bonplan.bonplan_commercant_id,
		bonplan.bonplan_id,
		bonplan.bonplan_texte,
		bonplan.bonplan_titre,
		bonplan.bonplan_date_fin,	
		villes.Nom as ville_client,
		users.Email as user_email,
		users.Portable as user_tel,
		commercants.NomSociete,
		commercants.nom_url,
		assoc_client_bonplan.valide,
		assoc_client_bonplan.valide as bonplan_valide				
		');
		
	   	$this->db->from($this->_table);
	   	$this->db->join($this->_users, $this->_table.'.id_client = '.$this->_users.'.IdUser');
	   	$this->db->join($this->_bonplan, $this->_table.'.id_bonplan = '.$this->_bonplan.'.bonplan_id');
	    //$this->db->join($this->_card_fiche_client_capital, $this->_users.'.IdUser = '.$this->_card_fiche_client_capital.'.id_user','left');
	   	//$this->db->join($this->_card_fiche_client_tampon, $this->_users.'.IdUser = '.$this->_card_fiche_client_tampon.'.id_user','left');
	   	$this->db->join($this->_card_fiche_client_capital, $this->_card_fiche_client_capital.'.id_commercant = '.$this->_bonplan.'.bonplan_commercant_id AND '.$this->_users.'.IdUser = '.$this->_card_fiche_client_capital.'.id_user','left');
	   	$this->db->join($this->_card_fiche_client_tampon, $this->_card_fiche_client_tampon.'.id_commercant = '.$this->_bonplan.'.bonplan_commercant_id AND '.$this->_users.'.IdUser = '.$this->_card_fiche_client_tampon.'.id_user','left');
	   	$this->db->join($this->_card_fiche_client_remise, $this->_card_fiche_client_remise.'.id_commercant = '.$this->_bonplan.'.bonplan_commercant_id AND '.$this->_users.'.IdUser = '.$this->_card_fiche_client_remise.'.id_user','left');
	   	$this->db->join($this->_card_capital, $this->_card_capital.'.id_commercant = '.$this->_bonplan.'.bonplan_commercant_id','left');
		$this->db->join($this->_card_tampon, $this->_card_tampon.'.id_commercant = '.$this->_bonplan.'.bonplan_commercant_id','left');
		$this->db->join($this->_card_remise, $this->_card_remise.'.id_commercant = '.$this->_bonplan.'.bonplan_commercant_id','left');
		$this->db->join('villes','villes.idVille = users.idVille');
	   	$this->db->join($this->_commercants, $this->_commercants.'.IdCommercant = '.$this->_bonplan.'.bonplan_commercant_id');
	   	
	   	
	   	if(array_key_exists('valide', $criteres)){
			$this->db->where($this->_table.'.valide', $criteres['valide']);
			$this->db->where('assoc_client_bonplan.date_validation !=', '0000-00-00');
		}
		if(array_key_exists('id_commercant', $criteres)){
			$this->db->where($this->_bonplan.'.bonplan_commercant_id ', $criteres['id_commercant']);
		}
		if(array_key_exists('id_user', $criteres)){
			$this->db->where($this->_users.'.IdUser ', $criteres['id_user']);
		}
		if(array_key_exists('month', $criteres)){
			$this->db->where('MONTH('.$this->_table.'.date_validation)', $criteres['month']);
		}
		
		
		//$this->db->where('assoc_client_bonplan.date_validation !=', '0000-00-00');
		$this->db->distinct();
		$this->db->group_by(array('users.IdUser'));
		if(!empty($order_by) && (current($order_by)== "desc" || current($order_by)== "asc")){
			 $this->db->order_by(key($order_by),current($order_by));
		}
		$query = $this->db->get();
	   	if(array_key_exists('num_rows', $criteres) && $criteres['num_rows']){
	   		return $query->num_rows();
	   	}
	   
	   	if(array_key_exists('query', $criteres) && $criteres['query']){
	   		return $query;
	   	}
		return  ($query->num_rows() > 0) ? $query->result() : false;
	}*/
	
	public function get_list_by_criteria_csv($criteres,$order_by=array()){
		$this->db->select(
			'users.IdUser,
			users.Nom,
			users.Prenom,
			card_capital.montant as objectif_capital,
			card_capital.description as description_capital,
			card_tampon.description as description_tampon,
			card_tampon.tampon_value,
			max(card_fiche_client_capital.solde_capital) as solde_capital,
			max(card_fiche_client_tampon.solde_tampon) as solde_tampon,
			assoc_client_bonplan.date_validation,
			bonplan.bonplan_commercant_id,
			bonplan.bonplan_id,
			bonplan.bonplan_texte,
			bonplan.bonplan_titre,
			bonplan.bonplan_date_fin,
			villes.Nom as ville_client,
			users.Email as user_email,
			users.Portable as user_tel,
			commercants.NomSociete,
			commercants.nom_url,
			assoc_client_bonplan.valide
			');
	
		$this->db->from($this->_table);
		$this->db->join($this->_users, $this->_table.'.id_client = '.$this->_users.'.IdUser');
		$this->db->join($this->_bonplan, $this->_table.'.id_bonplan = '.$this->_bonplan.'.bonplan_id');
		//$this->db->join($this->_card_fiche_client_capital, $this->_users.'.IdUser = '.$this->_card_fiche_client_capital.'.id_user','left');
		//$this->db->join($this->_card_fiche_client_tampon, $this->_users.'.IdUser = '.$this->_card_fiche_client_tampon.'.id_user','left');
		$this->db->join($this->_card_fiche_client_capital, $this->_card_fiche_client_capital.'.id_commercant = '.$this->_bonplan.'.bonplan_commercant_id','left');
		$this->db->join($this->_card_fiche_client_tampon, $this->_card_fiche_client_tampon.'.id_commercant = '.$this->_bonplan.'.bonplan_commercant_id','left');
		$this->db->join($this->_card_capital, $this->_card_capital.'.id_commercant = '.$this->_bonplan.'.bonplan_commercant_id','left');
		$this->db->join($this->_card_tampon, $this->_card_tampon.'.id_commercant = '.$this->_bonplan.'.bonplan_commercant_id','left');
		$this->db->join('villes','villes.idVille = users.idVille');
		$this->db->join($this->_commercants, $this->_commercants.'.IdCommercant = '.$this->_bonplan.'.bonplan_commercant_id');
		 
		if(array_key_exists('valide', $criteres)){
			$this->db->where($this->_table.'.valide', $criteres['valide']);
		}
		if(array_key_exists('id_commercant', $criteres)){
			$this->db->where($this->_bonplan.'.bonplan_commercant_id ', $criteres['id_commercant']);
		}
		if(array_key_exists('id_user', $criteres)){
			$this->db->where($this->_users.'.IdUser ', $criteres['id_user']);
		}
		
		//$this->db->where('assoc_client_bonplan.date_validation !=', '0000-00-00');
		$this->db->distinct();
		$this->db->group_by(array('users.IdUser'));
		if(!empty($order_by) && (current($order_by)== "desc" || current($order_by)== "asc")){
			$this->db->order_by(key($order_by),current($order_by));
		}
		$query = $this->db->get();
		 
		return  $query;
	}
	
	function validate_bonplan($id_bonplan,$id_user){
		$this->db->update($this->_table, array('valide'=>1,'date_validation'=>date('Y-m-d')), array('id_bonplan' => $id_bonplan,'id_client'=>$id_user));
	}
	
	function validate_bonplan_by_id($id){
		$this->db->update($this->_table, array('valide'=>1,'date_validation'=>date('Y-m-d')), array('id' => $id));
	}
	
	function deleteByUBid($userId,$bonplanId){
		$this->db->delete($this->_table, array('id_client' => $userId,'id_bonplan'=>$bonplanId));
	}
	
	function deleteById($id){
		return	$this->db->delete($this->_table, array('id' => $id));
	}
    function delete_bonplanById($id){
        return	$this->db->delete($this->_table, array('id' => $id));
    }
	
	function get_where($where=array()){
		$this->db->select($this->_table.'.id as assoc_client_bonplan_id,assoc_client_bonplan.*,commercants.*,villes.*,bonplan.*');
		$this->db->join($this->_bonplan, $this->_table.'.id_bonplan = '.$this->_bonplan.'.bonplan_id');
		$this->db->join($this->_commercants, $this->_commercants.'.IdCommercant = '.$this->_bonplan.'.bonplan_commercant_id');
		$this->db->join('villes','villes.idVille = commercants.idVille');
		$query = $this->db->get_where($this->_table, $where);
		return $query->result();
	}
    function get_where_mobile($where=array()){
        $this->db->select($this->_table.'.id as assoc_client_bonplan_id,assoc_client_bonplan.*,commercants.*,villes.*,bonplan.*,assoc_client_bonplan.date_validation as date_valid_bonplan');
        $this->db->join($this->_bonplan, $this->_table.'.id_bonplan = '.$this->_bonplan.'.bonplan_id');
        $this->db->join($this->_commercants, $this->_commercants.'.IdCommercant = '.$this->_bonplan.'.bonplan_commercant_id');
        $this->db->join('villes','villes.idVille = commercants.idVille');
        $query = $this->db->get_where($this->_table, $where);
        return $query->result();
    }
	function getById($_id){
        $zRqt = "SELECT *  FROM assoc_client_bonplan WHERE id =".$_id ;
        $qryBonplan = $this->db->query($zRqt) ;
        if ($qryBonplan->num_rows() > 0) {
            $Res = $qryBonplan->result();
            return $Res[0];
        }
    }
}