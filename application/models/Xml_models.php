<?php

class xml_models extends CI_model
{
    public $flux_datatourisme_table = 'rss_list';

    function __construct()
    {
        parent::__construct();
    }

    public function save($objAgenda)
    {

        $this->db->insert('article', $objAgenda);
        return $this->db->insert_id();
    }

    public function save_agenda($obj)
    {

        $this->db->insert('agenda', $obj);
        return $this->db->insert_id();
    }


    public function save2($field2)
    {
        $this->db->insert('article_datetime', $field2);


    }
    public function save_22($field2)
    {
        $this->db->insert('agenda_datetime', $field2);


    }
    public function save3($field3)
    {
        $this->db->insert('rss_datetime', $field3);
    }

    public function afficher_xml()
    {


        $query = $this->db->get('rss_list');
        return $query->result();


    }

    public function delete($ID)
    {
        $this->db->where('id', $ID);
        $this->db->delete('rss_list');
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_lis_sortezmine($ID)
    {
        $this->db->where('id', $ID);
        $this->db->delete('liste_data_saved_to_sortez_xml');
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function modifier($ID)
    {
        $this->db->where('id', $ID);
        $query = $this->db->get('rss_list');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function afficher_liens()
    {
        $query = $this->db->get('rss_source');
        return $query->result();
    }

    public function afficher_liens2($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('rss_source');
        return $query->result();
    }

    public function sauver_source($field)
    {

        $this->db->insert('rss_source', $field);
        return $this->db->insert_id();

    }


    public function supprimer_source($ID)
    {
        $this->db->where('id', $ID);
        $this->db->delete('rss_source');
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getliens_by_id($ID)
    {
        $this->db->where('id', $ID);
        $query = $this->db->get('rss_source');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getid_liens()
    {
        $query = $this->db->get('rss_source');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function save_to_sortez($fieldc)
    {
     $this->db->insert('rss_list', $fieldc);
    }

    public function get_all_saved_sortez($ID)
    {
        $this->db->where('id', $ID);
        $query = $this->db->get('rss_list');
        return $query->result();
    }

    public function save_to_true_sortez($field)
    {
        $this->db->insert('article', $field);
        return $this->db->insert_id();


    }

    public function afficher_categ_saved()
    {
        $query = "SELECT
liste_data_saved_to_sortez_xml.titre,
article.nom_manifestation,
liste_data_saved_to_sortez_xml.id,
article.IsActif
FROM
article ,
liste_data_saved_to_sortez_xml
WHERE
article.nom_manifestation = liste_data_saved_to_sortez_xml.titre";
        $result = $this->db->query($query);

        if ($result->num_rows() > 0) {
            return $result->result();
        }

    }

    public function save_to_true_sortez_mine($field2)
    {
        $this->db->insert('liste_data_saved_to_sortez_xml', $field2);
    }

    public function save_all_categ_checked($field)
    {
        $ID = $this->input->post('categ');
        if (!empty($field)) {
            $this->db->where_in('id', $ID);
            $this->db->update('rss_list', $field);
        }


    }

    public function save_all_categ_src($fieldz)
    {
        $this->db->where('article_categid', null);
        $this->db->update('rss_list', $fieldz);
    }

    public function delete_all()
    {
        $ID = $this->input->post('checked');
        //var_dump($ID);die();
        $this->db->where_in('titre', $ID);
        $this->db->delete('liste_data_saved_to_sortez_xml');
        $this->db->where_in('nom_manifestation', $ID);
        $this->db->delete('article');
        $this->db->where_in('nom_manifestation', $ID);
        $this->db->delete('article');


    }

    public function active_all($isactive)
    {
        $ID = $this->input->post('checked');
        $this->db->where_in('nom_manifestation', $ID);
        $this->db->update('article', $isactive);
    }

    public function active_all_and_save($isactive)
    {
        //var_dump($isactive);die();
        $ID = $this->input->post('categ');
        $subcategid = $this->input->post('subcategid');
        $categid = $this->input->post('categid');
        //var_dump($ID);die();
        $this->db->where_in('id', $ID);
        $this->db->update('rss_list', $isactive);

        $this->db->where_in('id', $ID);
        $checked = $this->db->get('rss_list', $isactive);
        $result = $checked->result();
        foreach ($result as $donnee) {
            $field = array(
                "IdUsers_ionauth" => "1992",
                "IdCommercant" => "301299",
                'nom_manifestation' => $donnee->nom_manifestation,
                'siteweb' => $donnee->siteweb,
                'description' => $donnee->description,
                'date_depot' => date("Y-m-d"),//$donnee->date_depot,
                'activ_fb_comment' => $donnee->activ_fb_comment,
                'isActif' => "1",
                "article_categid" => $donnee->article_categid,
                "article_subcategid" => $donnee->article_subcategid,
            );
            $IdUpdatedArticle = $this->db->insert('article', $field);
            $field2 = array(
                'titre' => $donnee->nom_manifestation,
            );
            $this->db->insert('liste_data_saved_to_sortez_xml', $field2);

        }


    }

    public function getcategbyliens($lien)
    {
        $categ = $this->db->get("agenda_categ");
        $result = $categ->result();
        //var_dump($categ);die();
        foreach ($result as $resulta) {
            //var_dump($resulta);die();
            $this->db->like("category", $lien, 'both');
            $this->db->limit(1);
            $resulte = $this->db->get("agenda_categ");
            //var_dump($resulte);die();
            if ($resulte->num_rows > 0) {
                return $resulta->agenda_categid;
            } else {
                return 0;
            }
        }

    }

    function getByAgenda($id = 0)
    {
        if (isset($id) && $id != 0 && $id != '') {
            $Sql = "select * from datatourisme_agenda where id =" . $id;
            $Query = $this->db->query($Sql);
            return $Query->row();
        } else return null;
    }

    function getByDatatourisme($id = 0)
    {
        if (isset($id) && $id != 0 && $id != '') {
            $Sql = "select * from datatourisme_agenda where article_id =" . $id;
            $Query = $this->db->query($Sql);
            return $Query->row();
        } else return null;
    }

    function getByIdDatatourisme($id = 0)
    {
        if (isset($id) && $id != 0 && $id != '') {
            $Sql = "select * from `" . $this->flux_datatourisme_table . "` where id =" . $id;
            $Query = $this->db->query($Sql);
            return $Query->row();
        } else return null;
    }

    function getAll()
    {
        $request = $this->db->query("
            SELECT
                *
            FROM
                datatourisme_agenda
            ORDER BY id DESC
        ");
        if ($request->num_rows() > 0) {
            return $request->result();
        }
    }

    function getAllDatatourisme()
    {
        $request = $this->db->query("
            SELECT
                *
            FROM
                `" . $this->flux_datatourisme_table . "`
            ORDER BY id ASC
        ");
        if ($request->num_rows() > 0) {
            return $request->result();
        }
    }

    function countAllDatatourisme()
    {
        $Sql = "SELECT COUNT(*) as nb FROM `" . $this->flux_datatourisme_table . "`";
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getWhere($where = '')
    {
        $sql = "
            SELECT
                *
            FROM
                datatourisme_agenda
            WHERE 
              0=0 
        ";
        if (isset($where) && $where != '') $sql .= " AND " . $where;
        $request = $this->db->query($sql);
        if ($request->num_rows() > 0) {
            return $request->result();
        }
    }

    function insert($prmData)
    {
        $this->db->insert("datatourisme_agenda", $prmData);
        return $this->db->insert_id();
    }

    function update_by_agenda($prmData)
    {
        $this->db->where("id", $prmData["id"]);
        $this->db->update("datatourisme_agenda", $prmData);
        $objA = $this->getByAgenda($prmData["id"]);
        return $objA->id;
    }

    public function countxml($lien)
    {
        $xml = simplexml_load_file($lien);
        $result = count($xml->channel->item);
        return $result;
    }

    public function testexist($test)
    {
        //var_dump($test);
        $this->db->where("nom_manifestation", $test);
        $result = $this->db->get("rss_list");
        $res = $result->num_rows();
        //var_dump($res);die();
        if ($res > 0) {
            return 0;
        } else {
            return 1;
        }
    }

    public function testexist2($test)
    {
        //var_dump($test);
        $this->db->where("nom_manifestation", $test);
        $result = $this->db->get("article");
        $res = $result->num_rows();
        //var_dump($res);die();
        if ($res > 0) {
            return 0;
        } else {
            return 1;
        }
    }

    function getMinMax_flux()
    {
        $Sql = "SELECT MIN(id) as min_id, MAX(id) as max_id FROM `" . $this->flux_datatourisme_table . "`";
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    public function getmin()
    {
        $request = $this->db->get("rss_list");
        $res = $request->num_rows();
        return $res;
    }

    public function delete_cache()
    {
        $sql="TRUNCATE TABLE `rss_list`";
        return $this->db->query($sql);
        }


    public function getdepartement()
    {
        $qryCategorie = $this->db->query("
            SELECT
            villes.IdVille,
            villes.ville_nom
            FROM
            villes
            where (villes.IdVille = 33710 OR villes.IdVille=33611 OR villes.IdVille =33629 OR villes.IdVille =33622 OR villes.IdVille =33684 OR villes.IdVille =33571
             OR villes.IdVille =33667 OR villes.IdVille =33644 OR villes.IdVille =33577 OR villes.IdVille =33608 OR villes.IdVille =26968 OR villes.IdVille =7971
              OR villes.IdVille =33712 OR villes.IdVille =33653 OR villes.IdVille =33618 OR villes.IdVille =33661 OR villes.IdVille =33601 OR villes.IdVille =33671
               OR villes.IdVille =33599 OR villes.IdVille =33615 OR villes.IdVille =33652 OR villes.IdVille =33641 OR villes.IdVille =33619 OR villes.IdVille =36941
                OR villes.IdVille =33592 OR villes.IdVille =33620 OR villes.IdVille =36944 OR villes.IdVille =33714 OR villes.IdVille =33602 OR villes.IdVille =33637
                 OR villes.IdVille =33666 OR villes.IdVille =33648 OR villes.IdVille =33649 OR villes.IdVille =33631 OR villes.IdVille =33612 OR villes.IdVille =33669 OR villes.ville_departement=6)

            GROUP BY
            villes.ville_nom
        ");
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    public function getallville()
    {
        $all = $this->db->get('villes');
        return $all->result();
    }

    public function getdepartementbypostalcode($postalcode)
    {
        // var_dump($postalcode);die();
        $this->db->where("CodePostal", $postalcode);
        $all = $this->db->get('villes');
        $ville = $all->row();
//var_dump($ville);die();
        $this->db->where("departement_code", $ville->ville_departement);
        $depart = $this->db->get('departement');
        $villes = $depart->row();
        //var_dump($villes);die();
        return $villes;

    }

    public function getcateg()
    {
        $categ = $this->db->get("agenda_categ");
        return $categ->result();
    }

    public function getsubcateg($categid)
    {
        $this->db->where("agenda_categid", $categid);
        $categ = $this->db->get("agenda_subcateg");
        return $categ->result();
    }

    public function getlink()
    {
        $this->db->select('*');
        $this->db->from('rss_source');
        $this->db->order_by('id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }
    public function getlink_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('rss_source');
        $this->db->where('id',$id);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get();
        return $query->row();
    }

    public function getcategbycategid($categid)
    {
        $this->db->where("agenda_categid", $categid);
        $result = $this->db->get("agenda_categ");
        return $result->row();
    }
    //linksave
    public function savelink($field)
    {
        $save = $this->db->insert("rss_source", $field);
        if ($save) {
            return true;
        } else {
            return false;
        }

    }

    public function getlinkbyid($idlink)
    {
        //var_dump($idlink);
        $this->db->where("id", $idlink);
        $all = $this->db->get("rss_source");
        return $all->row();
    }

    public function delete_link($id)
    {
        $this->db->where("id", $id);
        $request = $this->db->delete("rss_source");
        return $request;
    }

    public function getvillebyid($IdVille)
    {
        $this->db->where("IdVille", $IdVille);
        $save = $this->db->get("villes");
        return $save->row();
    }

    public function getlinkbylink($idlink)
    {
        $this->db->like("source", $idlink);
        $result = $this->db->get("rss_source");
        return $result->row();
    }
    public function get_all_rss(){
        $this->db->where("agenda_article_type_id",'3');
       $res= $this->db->get("article");
       return $res->result();
    }
    public function get_rss($id){
        $this->db->where("agenda_article_type_id",'3');
        $this->db->where('id',$id);
        $res= $this->db->get("article");
        return $res->row();
    }
    public function delete_rss_list(){
        $this->db->empty_table('rss_list');
    }
    public function delete_all_rss($id){
        $this->db->where('agenda_article_type_id',3);
        $this->db->delete('article');
    }
    public function get_last_id(){
            $this->db->where('agenda_article_type_id',3);
            $this->db->order_by('id',"desc");
            $this->db->limit(1);
            $last = $this->db->get('article');
            return $last->row();
    }
    public function get_link($id)  {

        $this->db->where('id',$id);
       $res=$this->db->get('rss_source');
       return $res->row();

    }
    public function get_alllink(){
        $res=$this->db->get('rss_source');
        return $res->result();
    }

    function update_current_date($prmData) {
        $this->db->where("id", $prmData["id"]);
        $this->db->update("rss_source", $prmData);
        return $prmData["id"];
    }

} ?>