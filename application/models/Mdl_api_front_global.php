<?php

class Mdl_api_front_global extends CI_Model
{
    public function getBonplancommunes(){

        $zSqlListePartenaire = "
            SELECT
                villes.IdVille,
                villes.ville_nom,
                commercants.referencement_bonplan,              
                COUNT(bonplan.bonplan_id) as nbCommercant
                ";

        $zSqlListePartenaire .= "
            FROM
                bonplan
                INNER JOIN commercants ON commercants.IdCommercant = bonplan.bonplan_commercant_id
                LEFT OUTER JOIN villes ON villes.IdVille = commercants.IdVille
                
            WHERE 
                
                 commercants.IsActif = 1 
                 AND commercants.referencement_bonplan = 1  
                 ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $zSqlListePartenaire .= " AND commercants.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0" && is_numeric($localdata_IdVille)) {
            $zSqlListePartenaire .= " AND commercants.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all) > 0) {
            $zSqlListePartenaire .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik++) {
                $zSqlListePartenaire .= " commercants.IdVille = '" . $localdata_IdVille_all[$iiik] . "' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListePartenaire .= " OR ";
            }
            $zSqlListePartenaire .= " ) ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0" && is_numeric($localdata_IdDepartement)) {
            $zSqlListePartenaire .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE

        $zSqlListePartenaire .= "
            GROUP BY
            villes.ville_nom asc ";

        $qryCategorie = $this->db->query($zSqlListePartenaire);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    public function getFidelitycommunes(){
        $departement_id = 0;
        $sqlcat = "
          SELECT
            villes.IdVille,
            villes.ville_nom
            FROM
            villes
            Inner Join commercants ON commercants.IdVille = villes.IdVille
            LEFT OUTER JOIN card_capital ON card_capital.id_commercant = commercants.IdCommercant
            LEFT OUTER JOIN card_remise ON card_remise.id_commercant = commercants.IdCommercant
            LEFT OUTER JOIN card_tampon ON card_tampon.id_commercant = commercants.IdCommercant
            ";

        if (isset($departement_id) && $departement_id != "" && $departement_id != "0" && $departement_id != null) {
            $sqlcat .= " Inner Join departement ON villes.ville_departement = departement.departement_code ";
        }

        $sqlcat .= " 
            where commercants.IsActif = 1
            AND (commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_remise) 
            OR commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_tampon) 
            or commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_capital))
        ";

        if (isset($departement_id) && $departement_id != "" && $departement_id != "0" && $departement_id != null) {
            $sqlcat .= " AND departement.departement_id = '" . $departement_id . "'";
        }

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $sqlcat .= " AND villes.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0" && is_numeric($localdata_IdVille)) {
            $sqlcat .= " AND villes.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all) > 0) {
            $sqlcat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik++) {
                $sqlcat .= " villes.IdVille = '" . $localdata_IdVille_all[$iiik] . "' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
            }
            $sqlcat .= " ) ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0" && is_numeric($localdata_IdDepartement)) {
            $sqlcat .= " AND villes.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
            GROUP BY
            villes.IdVille, commercants.NomSociete ";

        $qryCategorie = $this->db->query($sqlcat);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    public function getBonplanPartenaire(){

        $sqlcom = "
                SELECT
                commercants.NomSociete AS NomSociete,
                commercants.IdCommercant
                FROM commercants
                INNER JOIN bonplan ON commercants.IdCommercant = bonplan.bonplan_commercant_id
                WHERE commercants.IsActif = 1 ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcom .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcom .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcom .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcom .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcom .= " OR ";
            }
            $sqlcom .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcom .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE



        $sqlcom .= "
                group by IdCommercant
        ";

        $qryCommercant =  $this->db->query($sqlcom);
        return $qryCommercant->result();
    }

    public function getFidelityPartenaire(){
        $sqlcom = "
                SELECT
                commercants.NomSociete AS NomSociete,
                commercants.IdCommercant
                FROM commercants
                LEFT OUTER JOIN card_capital ON card_capital.id_commercant = commercants.IdCommercant
                LEFT OUTER JOIN card_remise ON card_remise.id_commercant = commercants.IdCommercant
                LEFT OUTER JOIN card_tampon ON card_tampon.id_commercant = commercants.IdCommercant
                WHERE commercants.IsActif = 1
                AND (commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_remise) 
                OR commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_tampon) 
                or commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_capital))
                ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcom .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcom .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcom .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcom .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcom .= " OR ";
            }
            $sqlcom .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcom .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE



        $sqlcom .= "
                group by IdCommercant
        ";

        $qryCommercant =  $this->db->query($sqlcom);
        return $qryCommercant->result();
    }

    public function getBonplancateg(){

        $sqlcateg = "
            SELECT
            sous_rubriques.IdRubrique,
            sous_rubriques.Nom,
            commercants.IdCommercant,
            sous_rubriques.IdSousRubrique,
            count(bonplan.bonplan_id) as nb_bonplan
            FROM
            sous_rubriques
            Inner Join ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdSousRubrique = sous_rubriques.IdSousRubrique
            Inner Join commercants ON commercants.IdCommercant = ass_commercants_sousrubriques.IdCommercant
            Inner Join bonplan ON bonplan.bonplan_commercant_id = commercants.IdCommercant
            WHERE commercants.IsActif = 1";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcateg .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcateg .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcateg .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcateg .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcateg .= " OR ";
            }
            $sqlcateg .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcateg .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $sqlcateg .= "
            GROUP BY
            sous_rubriques.IdSousRubrique, commercants.IdCommercant
        ";

        $qryCategorie = $this->db->query($sqlcateg);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    public function getFidelitycateg(){
        $sqlcateg = "
            SELECT
            sous_rubriques.IdRubrique,
            sous_rubriques.Nom,
            sous_rubriques.IdSousRubrique
            FROM
            sous_rubriques
            Inner Join ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdSousRubrique = sous_rubriques.IdSousRubrique
            Inner Join commercants ON commercants.IdCommercant = ass_commercants_sousrubriques.IdCommercant
            LEFT OUTER JOIN card_capital ON card_capital.id_commercant = commercants.IdCommercant
            LEFT OUTER JOIN card_remise ON card_remise.id_commercant = commercants.IdCommercant
            LEFT OUTER JOIN card_tampon ON card_tampon.id_commercant = commercants.IdCommercant
            WHERE 
            commercants.IsActif = 1 
            AND (commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_remise) 
            OR commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_tampon) 
            or commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_capital))
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcateg .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcateg .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcateg .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcateg .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcateg .= " OR ";
            }
            $sqlcateg .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcateg .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $sqlcateg .= "
            GROUP BY
            sous_rubriques.IdSousRubrique, commercants.IdCommercant
        ";

        $qryCategorie = $this->db->query($sqlcateg);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    public function getBonplanListe($_limitstart = 0, $_limitend = 10000){

        $zSqlListeBonPlan = "
            SELECT
                bonplan.* ,commercants.Photo5 as Photo5,
                commercants.NomSociete AS NomSociete,
                villes.Nom AS ville,
                commercants.Adresse1 AS quartier,
                commercants.Adresse2 AS rue,
                commercants.IdCommercant,
                commercants.user_ionauth_id
            FROM
                bonplan
                LEFT JOIN commercants ON commercants.IdCommercant = bonplan.bonplan_commercant_id
                LEFT JOIN villes ON villes.IdVille = commercants.IdVille
        ";
        
        $zSqlListeBonPlan .= " LIMIT ".$_limitstart.",".$_limitend ;
        
        //echo $zSqlListeBonPlan;
        
        $zQueryListeBonPlan = $this->db->query($zSqlListeBonPlan) ;
        return $zQueryListeBonPlan->result() ;
    }

    function listeBonPlanRecherche($_iCategorieId = 0, $i_CommercantId = 0, $_zMotCle = "", $_iFavoris = "", $_limitstart = 0, $_limitend = 10000, $_iIdVille = 0, $_iIdDepartement = 0, $iOrderBy="", $inputFromGeo = "0", $inputGeoValue = "10", $inputGeoLatitude="0", $inputGeoLongitude="0", $session_iWhereMultiple = "",$inputString_bonplan_type=""){
        $txtWhereFavoris = "";

        $thisss =& get_instance();
        $thisss->load->library('ion_auth');
        $thisss->load->model("ion_auth_used_by_club");

        if ($thisss->ion_auth->logged_in()){
            $user_ion_auth = $thisss->ion_auth->user()->row();
            $iduser = $thisss->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $thisss->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser=0;


        if($_iFavoris == "1" && $iduser != "") {
            $sqlFavoris = "
                SELECT
                    CONCAT('(',GROUP_CONCAT(ass_commercants_users.IdCommercant SEPARATOR ','),')') AS IdCommercant
                FROM
                    ass_commercants_users
                WHERE
                    ass_commercants_users.IdUser = '" . $iduser . "'
                GROUP BY ass_commercants_users.IdUser
            ";
            $QueryFavoris = $this->db->query($sqlFavoris) ;
            if($QueryFavoris->num_rows() > 0) {
                $colFavoris = $QueryFavoris->result();
                $txtWhereFavoris .= " AND commercants.IdCommercant IN " . $colFavoris[0]->IdCommercant;
            } else {
                $txtWhereFavoris .= " AND commercants.IdCommercant IN (0) ";
            }
        }

        $zSqlListeBonPlan = "
            SELECT
            bonplan.*,
            commercants.Photo5 AS Photo5,
            commercants.NomSociete AS NomSociete,
            villes.Nom AS ville,
            commercants.Adresse1 AS quartier,
            commercants.Adresse2 AS rue,
            commercants.user_ionauth_id,
            commercants.IdCommercant,
            sous_rubriques.IdSousRubrique,
            sous_rubriques.IdRubrique,
            sous_rubriques.Nom,
            commercants.IdVille,
            commercants.nom_url";

        if ($inputFromGeo == "1") {
            $zSqlListeBonPlan .= " , (6371 * acos(cos(radians($inputGeoLatitude)) * cos(radians(commercants.latitude)) * cos(radians(commercants.longitude) - radians($inputGeoLongitude)) + sin(radians($inputGeoLatitude)) * sin(radians(commercants.latitude)))) AS distance ";
        }

        $zSqlListeBonPlan .= "
            FROM
            bonplan
            Inner Join commercants ON commercants.IdCommercant = bonplan.bonplan_commercant_id
            Left Join villes ON villes.IdVille = commercants.IdVille
            Inner Join ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
            Inner Join sous_rubriques ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique
            WHERE
            commercants.IsActif = 1  AND ( (bp_unique_date_fin >= NOW()) OR (bp_multiple_date_fin >= NOW()) OR (bonplan_date_fin >= NOW()) )
        ";

        if ($inputFromGeo == "1") {
            $zSqlListeBonPlan .= " AND commercants.latitude != '' AND commercants.longitude != '' ";
        }
        
        /*if($_iCategorieId != 0){
            $zSqlListeBonPlan .= " AND sous_rubriques.IdSousRubrique =" . $_iCategorieId ;
        }*/
        if(isset($_iCategorieId) && is_array($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0){
            $zSqlListeBonPlan .= " AND (";
            if (sizeof($_iCategorieId)==1){
                $zSqlListeBonPlan .= " sous_rubriques.IdSousRubrique = " . $_iCategorieId[0];
            } else {
                $zSqlListeBonPlan .= " sous_rubriques.IdSousRubrique = " . $_iCategorieId[0];
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand ++) {
                    $zSqlListeBonPlan .= " OR sous_rubriques.IdSousRubrique = " . $_iCategorieId[$ii_rand];
                }
            }
            $zSqlListeBonPlan .= " )";
        }
        if($i_CommercantId != 0){
            $zSqlListeBonPlan .= " AND commercants.IdCommercant = '$i_CommercantId' " ;
        }
        /*
        if($_zMotCle != ""){
            $zSqlListeBonPlan .= " AND bonplan_titre LIKE '%" . $_zMotCle . "%'" ;
        }
        */
        $SearchedWordsString = str_replace("+", " ", $_zMotCle) ;
        $SearchedWordsString = str_replace("'", "\'", $SearchedWordsString) ;
        $SearchedWordsString = str_replace('"', '\"', $SearchedWordsString) ;
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString) ;
        
        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;
        
        for ($i = 0; $i < sizeof($SearchedWords); $i ++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search) ;
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListeBonPlan .= "\n AND " ;
                }
                $zSqlListeBonPlan .= " ( \n" .
                    " UPPER(bonplan.bonplan_titre ) LIKE '%" . strtoupper($Search) . "%'\n" . 
                    " OR UPPER(bonplan.bonplan_texte ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.Adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.Adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .    
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                " )
                ";
                if ($i != (sizeof($SearchedWords) - 1)){
                    $zSqlListeBonPlan .= "\n OR ";
                }
            }
        }
        if($_iFavoris == "1" && $iduser != "") {
            $zSqlListeBonPlan .= $txtWhereFavoris;
        }

        //add ville id list on query
        //
        if($_iIdVille != 0 && $_iIdVille != "" && $_iIdVille != null){
            $zSqlListeBonPlan .= " AND commercants.IdVille=" . $_iIdVille ;
        } elseif ($_iIdDepartement != 0 && $_iIdDepartement != "" && $_iIdDepartement != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iIdDepartement);
            if (count($villeByIdDepartement)>0) {
                $zSqlListeBonPlan .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListeBonPlan .= " commercants.IdVille=" . $key_villeDep->IdVille ;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListeBonPlan .= " OR ";
                     } 
                    $iii_villedep++;
                }
                $zSqlListeBonPlan .= ")";                    
            }
        }


        if ($session_iWhereMultiple != "") {
            $zSqlListeBonPlan .= " AND bonplan.bon_plan_utilise_plusieurs = '".$session_iWhereMultiple."' ";        
        }

        if ($inputString_bonplan_type != "" && $inputString_bonplan_type!="0" && $inputString_bonplan_type!=null && $inputString_bonplan_type!=0) {
            $zSqlListeBonPlan .= " AND bonplan.bonplan_type = '".$inputString_bonplan_type."' ";
        }

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlListeBonPlan .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlListeBonPlan .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlListeBonPlan .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlListeBonPlan .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListeBonPlan .= " OR ";
            }
            $zSqlListeBonPlan .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlListeBonPlan .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE



        //SORTEZ.ORG => NO BASIC PARTNER ACCOUT
            $zSqlListeBonPlan .= " AND commercants.user_ionauth_id IN (SELECT users_groups_ionauth.user_ionauth_id FROM users_groups_ionauth WHERE users_groups_ionauth.group_ionauth_id = 4 OR users_groups_ionauth.group_ionauth_id = 5) ";
        //SORTEZ.ORG => NO BASIC PARTNER ACCOUT

        // demo account commercant should not appear on annuaire list
        $zSqlListeBonPlan .= " AND commercants.IdCommercant <> '301298'  AND commercants.IdCommercant <> '301266' ";
        //$zSqlListeBonPlan .= " AND commercants.IdCommercant <> '301298' ";

        if ($inputFromGeo == "1") {
            $zSqlListeBonPlan .= "  HAVING distance < $inputGeoValue  ";
        }

        if ($iOrderBy=='1') $iOrderBy_value = " order by bonplan.bonplan_id desc ";
        else if ($iOrderBy=='2') $iOrderBy_value = " order by commercants.nbrevisites desc ";
        else if ($iOrderBy=='3') $iOrderBy_value = " order by bonplan.bonplan_id asc ";
        else $iOrderBy_value = " order by bonplan_date_fin asc ";

        $zSqlListeBonPlan .= $iOrderBy_value;

        $zSqlListeBonPlan .= " LIMIT ".$_limitstart.",".$_limitend ;
        
        //echo $zSqlListeBonPlan."<br/><br/>";
        ////$this->firephp->log($iOrderBy, 'iOrderBy_3');
        
        $zQueryListeBonPlan = $this->db->query($zSqlListeBonPlan) ;
        return $zQueryListeBonPlan->result() ;
    }

    public function getfavoris_by_idUser($idUser){
        $this->db->select("ass_commercants_users.*,commercants.*");
        $this->db->from('ass_commercants_users');
        $this->db->join('commercants','commercants.IdCommercant = ass_commercants_users.IdCommercant');
        $this->db->where('ass_commercants_users.Favoris',1);
        $this->db->where('ass_commercants_users.IdUser',$idUser);
        $res = $this->db->get();
        return $res->result();
    }
}