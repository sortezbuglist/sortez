<?php

class Mdl_menu extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    public function get_menu_data_by_idcom($idcom){
        $this->db->where('idCommercant',$idcom);
        $res = $this->db->get('info_menu_commercant');
        if($res->num_rows() !=0){
            return $res->row();
        }else{
            return false;
        }
    }
    public function get_menu_text_by_idcom($idcom){
        $this->db->where('idCommercant',$idcom);
        $res = $this->db->get('info_menu_commercant');
        if($res->num_rows() !=0){
            return $res->row()->menu_value;
        }else{
            return false;
        }
    }
    public function save_command_data($data){
        if(isset($data['id']) AND $data['id'] !=0){
            $this->db->where('id',$data['id']);
            $this->db->update('info_menu_commercant',$data);
        }else{
            $this->db->insert('info_menu_commercant',$data);
        }
        if(isset($data['idCommercant']) AND $data['idCommercant'] !=""){
            $this->db->where('IdCommercant',$data['idCommercant']);
            $this->db->update('commercants', array('activate_soutenons' => '1'));
        }else{
            $this->db->where('IdCommercant',$data['idCommercant']);
            $this->db->update('commercants', array('activate_soutenons' => '1'));
        }
    }
    public function save_glissiere1($data,$nbgli){
        if ($nbgli =="title_gli1"){
            $this->db->where('idCom',$data['idCom']);
            $res = $this->db->get('glissiere_menu1')->row();
            if (count($res) !=0 ){
                $this->db->where('id',$res->id);
                $this->db->delete('glissiere_menu1');
            }
            $this->db->insert("glissiere_menu1",$data);
        }elseif($nbgli == "title_gli2"){
            $res = $this->db->get('glissiere_menu2')->row();
            if (count($res) !=0 ){
                $this->db->where('id',$res->id);
                $this->db->delete('glissiere_menu2');
            }
            $this->db->insert("glissiere_menu2",$data);
        }elseif ($nbgli == "title_gli3"){
            $res = $this->db->get('glissiere_menu3')->row();
            if (count($res) !=0 ){
                $this->db->where('id',$res->id);
                $this->db->delete('glissiere_menu3');
            }
            $this->db->insert("glissiere_menu3",$data);
        }elseif ($nbgli =="title_gli4"){
            $res = $this->db->get('glissiere_menu4')->row();
            if (count($res) !=0 ){
                $this->db->where('id',$res->id);
                $this->db->delete('glissiere_menu4');
            }
            $this->db->insert("glissiere_menu4",$data);
        }elseif ($nbgli =="title_gli6"){
            $res = $this->db->get('glissiere_menu5')->row();
            if (count($res) !=0 ){
                $this->db->where('id',$res->id);
                $this->db->delete('glissiere_menu5');
            }
            $this->db->insert("glissiere_menu5",$data);
        }elseif ($nbgli =="title_gli7"){
            $res = $this->db->get('glissiere_menu6')->row();
            if (count($res) !=0 ){
                $this->db->where('id',$res->id);
                $this->db->delete('glissiere_menu6');
            }
            $this->db->insert("glissiere_menu6",$data);
        }

    }
    public function add_photo($iUser, $user_ion_auth_id, $image1, $img_file){
        echo $img_file;
        $Sql = "
            UPDATE article_menu
            SET image = '".$img_file."'
            WHERE id = '".$iUser."'
        ";
        $Query = $this->db->query($Sql);
    }
    public function add_photo_menu($iUser, $user_ion_auth_id, $image1, $img_file){
        echo $img_file;
        $Sql = "
            UPDATE info_menu_commercant
            SET image_menu_gen = '".$img_file."'
            WHERE idCommercant = '".$iUser."'
        ";
        $Query = $this->db->query($Sql);
    }
    public function save_article($fields){
        $this->db->where('id',$fields['id']);
        $res = $this->db->get('article_menu');
        if($res->num_rows() == 0){
            $this->db->insert('article_menu',$fields);
            return $this->db->insert_id();
        }else{
            $this->db->where('id',$fields['id']);
            $this->db->update('article_menu',$fields);
            return $res->row()->id;
        }
    }
    public function getdatagli1($idcom){
        $this->db->where('id_glissiere','1');
        $this->db->where('idCom',$idcom);
        $res = $this->db->get('article_menu');
        return $res->result();
    }
    public function getdatagli2($idcom){
        $this->db->where('id_glissiere','2');
        $this->db->where('idCom',$idcom);
        $res = $this->db->get('article_menu');
        return $res->result();
    }
    public function getdatagli3($idcom){
        $this->db->where('id_glissiere','3');
        $this->db->where('idCom',$idcom);
        $res = $this->db->get('article_menu');
        return $res->result();
    }
    public function getdatagli4($idcom){
        $this->db->where('id_glissiere','4');
        $this->db->where('idCom',$idcom);
        $res = $this->db->get('article_menu');
        return $res->result();
    }
    public function getdatagli6($idcom){
        $this->db->where('id_glissiere','6');
        $this->db->where('idCom',$idcom);
        $res = $this->db->get('article_menu');
        return $res->result();
    }
    public function getdatagli7($idcom){
        $this->db->where('id_glissiere','7');
        $this->db->where('idCom',$idcom);
        $res = $this->db->get('article_menu');
        return $res->result();
    }
    public function gettitlegli1($idcom){
        $this->db->where('idCom',$idcom);
        $res = $this->db->get("glissiere_menu1");
        return $res->row();
    }
    public function gettitlegli2($idcom){
        $this->db->where('idCom',$idcom);
        $res = $this->db->get("glissiere_menu2");
        return $res->row();
    }
    public function gettitlegli3($idcom){
        $this->db->where('idCom',$idcom);
        $res = $this->db->get("glissiere_menu3");
        return $res->row();
    }
    public function gettitlegli4($idcom){
        $this->db->where('idCom',$idcom);
        $res = $this->db->get("glissiere_menu4");
        return $res->row();
    }
    public function gettitlegli6($idcom){
        $this->db->where('idCom',$idcom);
        $res = $this->db->get("glissiere_menu5");
        return $res->row();
    }
    public function gettitlegli7($idcom){
        $this->db->where('idCom',$idcom);
        $res = $this->db->get("glissiere_menu6");
        return $res->row();
    }
    public function save_paypal_data($fields){
        $this->db->where('idCommercant',$fields['idCommercant']);
        $res = $this->db->get('info_menu_commercant');
        if($res->num_rows() == 0){
            $this->db->insert('info_menu_commercant',$fields);
        }else{
            $this->db->where('idCommercant',$fields['idCommercant']);
            $this->db->update('info_menu_commercant',$fields);
        }
    }
    public function get_product_by_id($id,$id_gli){
        $this->db->select('article_menu.id AS id,article_menu.titre,article_menu.prix,article_menu.image,article_menu.id_glissiere,article_menu.idCom,article_menu.true_title,glissiere_soutenons'.$id_gli.'.titre_glissiere,glissiere_soutenons'.$id_gli.'.idCom,glissiere_soutenons'.$id_gli.'.is_activ_glissiere,glissiere_soutenons'.$id_gli.'.id AS glissiere_ids');
        $this->db->from('article_menu');
        $this->db->join('glissiere_soutenons'.$id_gli, 'glissiere_soutenons'.$id_gli.'.idCom = article_menu.idCom');
        $this->db->where('article_menu.id',$id);
        return $this->db->get()->row();
    }
    public function save_command_list($fields){
        $this->db->insert('commande_menu_list',$fields);
        return $this->db->insert_id();
    }
    public function save_detail_command($fields){
        $this->db->insert('commande_menu_details',$fields);
        return $this->db->insert_id();
    }
    function getVilleByNomSimple($nom=""){
        $nom = strtolower($nom);
        $Sql = "SELECT IdVille FROM villes WHERE NomSimple ='".$nom."' ";
        $Query = $this->db->query($Sql);
        return $Query->row()->IdVille;
    }
    public function getuser_by_id_card($id_card){
        $this->db->select('card.*,users.*');
        $this->db->from('users');
        $this->db->join('card', 'card.id_user = users.IdUser');
        $this->db->where('card.num_id_card_virtual',$id_card);
        return $this->db->get()->row();
    }
    public function getuser_by_id($id){
        $this->db->where('users.IdUser',$id);
        return $this->db->get('users')->row();
    }
    public function get_art_by_id($id){
        $this->db->where('id',$id);
        $res =$this->db->get('article_menu');
        return $res->row();
    }
    public function delete_image_by_id($id){
        $field=array(
            "image"=>null
        );
        $this->db->where('id',$id);
        $this->db->update('article_menu',$field);
    }
    public function delete_image_menu_gen_by_idcom($idcom){
        $field=array(
            "image_menu_gen"=>null
        );
        $this->db->where('idCommercant',$idcom);
        $this->db->update('info_menu_commercant',$field);
    }
    public function delete_art_by_id($id){
        $this->db->where('id',$id);
        $this->db->delete('article_menu');
    }
    public function delete_cgv($cgv,$idcom){
        $array = array("cgv_file" => null,"cgv_link" => null);
        $this->db->where("idCommercant",$idcom);
        $this->db->update("info_menu_commercant",$array);
    }
    public function delete_cgv_livraison($cgv,$idcom){
        $array = array("cgv_file_livraison" => null,"cgv_link_livraison" => null);
        $this->db->where("idCommercant",$idcom);
        $this->db->update("info_menu_commercant",$array);
    }
    public function delete_cgv_differe($cgv,$idcom){
        $array = array("cgv_file_differe" => null,"cgv_link_differe" => null);
        $this->db->where("idCommercant",$idcom);
        $this->db->update("info_menu_commercant",$array);
    }
    public function delete_cgv_specif($cgv,$idcom){
        $array = array("cgv_file_specif" => null,"cgv_link_specif" => null);
        $this->db->where("idCommercant",$idcom);
        $this->db->update("info_menu_commercant",$array);
    }
    public function delete_doc($doc,$idcom){
        $array = array("doc_file" => null);
        $this->db->where("idCommercant",$idcom);
        $this->db->update("info_menu_commercant",$array);
    }
    public function get_data_ion_users($idion_auth){
        //var_dump($idion_auth);die('eto alou');
        $this->db->where("id",$idion_auth);
        $rest = $this->db->get("users_ionauth")->row();
        return $rest;
    }
    public function delete_commande($id){
        $this->db->where('id',$id);
        $this->db->delete('commande_menu_list');
        $this->db->where('id_commande',$id);
        $this->db->delete('commande_menu_details');
    }
    public function get_command_list($id){
        $this->db->where("id",$id);
        $rest = $this->db->get("commande_menu_list")->row();
        return $rest;
    }
    public function get_command_details($id){
        $this->db->where("id_commande",$id);
        $rest = $this->db->get("commande_menu_details")->result();
        return $rest;
    }
    public function get_product_by_id_comm($id){
        $this->db->where("id",$id);
        $rest = $this->db->get("article_menu")->row();
        return $rest;
    }
    public function get_all_commmand_by_idcom($idcom,$filter){
        $this->db->select('commande_menu_list.*,users.*,commercants.*');
        $this->db->from('commande_menu_list');
        $this->db->join('users', 'users.IdUser = commande_menu_list.id_client');
        $this->db->join('commercants', 'commercants.IdCommercant = commande_menu_list.idCom');
        if (isset($filter) AND $filter !="" && $filter !=null){
            $this->db->where("commande_menu_list.etat_commande",$filter);
        }
        $this->db->where("commande_menu_list.idCom",$idcom);
        $rest = $this->db->get()->result();
        return $rest;
    }
    public function save_etat_commande($id_commande,$field){
        $this->db->where('id',$id_commande);
        $this->db->update('commande_menu_list',$field);
    }
    function GetById_commercants($prmId) {
        $qryCommercant =  $this->db->query("SELECT * FROM commercants WHERE IdCommercant = ?", $prmId);
        if ($qryCommercant->num_rows() > 0) {
            $Res = $qryCommercant->result();
            return $Res[0];
        }
    }
    function get_menu_menu_by_id_com($id_com){
        $infocom_menu = $this->get_menu_data_by_idcom($id_com);
        if ($infocom_menu->menu_value != null && $infocom_menu->menu_value != '' ){
            return $infocom_menu->menu_value;
        }else{
            return "Nos cartes et MEnus";
        }
    }
    function is_menu_seted($id_com){
        $infocom_menu = $this->get_menu_data_by_idcom($id_com);
        if ($infocom_menu->menu_value != null && $infocom_menu->menu_value != '' ){
            return true;
        }else{
            return false;
        }
    }
    public function Insert_qr_code_menu($field,$id_menu){
        $this->db->where('id',$id_menu);
        $this->db->update('info_menu_commercant',$field);
    }
}