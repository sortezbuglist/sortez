<?php
class Rubrique extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    function GetAll(){
        $qryRubrique = $this->db->query("
            SELECT
                IdRubrique,
                Nom
            FROM
                rubriques
            ORDER BY Nom ASC
        ");
        if($qryRubrique->num_rows() > 0) {
            return $qryRubrique->result();
        }
    }
    
    function GetId($Id = 0) {
        
        $qryString = "SELECT   * FROM rubriques WHERE IdRubrique = " . $Id . " LIMIT 1;";
        return $this->db->query($qryString)->row();
    }
    function getIdRubrique($Id=0){
        $sql = "
        SELECT * FROM `ass_commercants_rubriques` WHERE IdCommercant =".$Id.";";
        $Query = $this->db->query($sql);
        return $Query->result();   
    }    
    function getSousRubrique($Id=0){
        $sql = "
        SELECT * FROM `ass_commercants_sousrubriques` WHERE IdCommercant =".$Id.";";
        $Query = $this->db->query($sql);
        return $Query->result();   
    }    

}