<?php
class mdl_article_agenda_ref extends CI_Model{ 
    
    function __construct() {
        parent::__construct();
    }
    
    function getAll(){
        $qryCategorie = $this->db->query("
                SELECT
                    *
                FROM
                    article_agenda_ref
                ORDER BY id DESC

        ");
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function getById($id=0){
        $Sql = " select * from article_agenda_ref where id = ". $id;      
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByArticleId($id=''){
        $Sql = " select * from article_agenda_ref where article_id = '". $id ."' LIMIT 1 ";      
        $Query = $this->db->query($Sql);
        return $Query->row();
    }
    
    function getByAgendaId($id=''){
        $Sql = " select * from article_agenda_ref where agenda_id = '". $id ."' LIMIT 1 ";      
        $Query = $this->db->query($Sql);
        return $Query->row();
    }
    
    function insert($prmData) {
        $this->db->insert("article_agenda_ref", $prmData);
        return $this->getById($this->db->insert_id());
    }
    
    function update($prmData) {
        $prmData = (array)$prmData;
        $this->db->where("id", $prmData["id"]);
        $this->db->update("article_agenda_ref", $prmData);
        return $this->getById($prmData["id"]);
    }

    function delete($prmId){
        $zSql = "
            DELETE
            FROM
            article_agenda_ref
            WHERE
            id = ". $prmId;

        $qry = $this->db->query($zSql) ;
        return $qry ;
    }

    function getWhere($prmWhere = "0 = 0", $prmOffset = 0, $prmLimit = 0, $prmOrder = " article_agenda_ref.id DESC ") {
        if (empty($prmOrder)) {
            $prmOrder = " article_agenda_ref.id DESC ";
        }
        
        $qryString = "
            SELECT
                *
            FROM
                article_agenda_ref
            WHERE " . $prmWhere . "
            ORDER BY " . $prmOrder . "";
        if (!(($prmOffset == 0) and ($prmLimit == 0))) {
            $qryString .= " LIMIT $prmOffset, $prmLimit";
        }
        return $this->db->query($qryString)->result();
    }


}