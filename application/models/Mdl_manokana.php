<?php

class mdl_manokana extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function GetById($id = 0)
    {
        $Sql = "select * from agenda where id =" . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function Increment_accesscount($id = 0)
    {
        $current_agenda = $this->GetById($id);
        $last_accesscount_nb = intval($current_agenda->accesscount);
        $last_accesscount_nb_total = $last_accesscount_nb + 1;
        $zSql = "UPDATE agenda SET accesscount='" . $last_accesscount_nb_total . "' WHERE id=" . $id;
        $this->db->query($zSql);
    }

    function GetById_IsActif($id = 0)
    {
        $Query_txt = "
            SELECT
            agenda.*,
            villes.Nom AS ville,
            agenda_categ.category,
            agenda_categ.agenda_categid,
            agenda_subcateg.subcateg

            FROM
                agenda
            LEFT OUTER JOIN villes ON villes.IdVille = agenda.IdVille_localisation
            INNER JOIN agenda_categ ON agenda_categ.agenda_categid = agenda.agenda_categid
            LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = agenda.agenda_subcategid

            WHERE id = '" . $id . "'
            AND (agenda.IsActif = '1' OR agenda.IsActif = '2')
            LIMIT 1
        "; //var_dump($Query_txt); die();
        $qry = $this->db->query($Query_txt);
        $result_main = $qry->row();
        return $result_main;
    }

    function GetById_preview($id = 0)
    {
        $qry = $this->db->query("
            SELECT
            agenda.*,
            villes.Nom AS ville,
            agenda_categ.category,
            agenda_subcateg.subcateg

            FROM
                agenda
            LEFT OUTER JOIN villes ON villes.IdVille = agenda.IdVille_localisation
            INNER JOIN agenda_categ ON agenda_categ.agenda_categid = agenda.agenda_categid
            LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = agenda.agenda_subcategid

            WHERE id = '" . $id . "'
            LIMIT 1
        ");
        return $qry->row();
    }

    function GetByIdCommercant($IdCommercant = 0)
    {
        $sql_qr = "
            SELECT *
            FROM
                agenda
            WHERE IdCommercant = '" . $IdCommercant . "'
            AND IsActif <> '3'
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille_value = $this_session_localdata->session->userdata('localdata_IdVille');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $sql_qr .= " AND agenda.IdVille_localisation = '2031' ";
        } else if (isset($localdata_IdVille_value) && $localdata_IdVille_value != "0" && $localdata_IdVille_value != "") {
            $sql_qr .= " AND agenda.IdVille_localisation = '" . $localdata_IdVille_value . "' ";
        }
        //LOCALDATA FILTRE

        $sql_qr .= "
            ORDER BY order_partner ASC
        ";

        $qry = $this->db->query($sql_qr);
        if ($qry->num_rows() > 0) {
            return $qry->result();
        }
    }

    function GetByIdCommercantLimit($IdCommercant = 0, $_limitstart = 0, $_limitend = 10000000)
    {
        $qry = $this->db->query("
            SELECT
                agenda_datetime.id as datetime_id, 
                agenda_datetime.date_debut as datetime_debut, 
                agenda_datetime.date_fin as datetime_fin, 
                agenda_datetime.heure_debut as datetime_heure_debut,
                agenda.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_subcateg.subcateg
            FROM
                agenda
                LEFT OUTER JOIN villes ON villes.IdVille = agenda.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = agenda.agenda_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = agenda.agenda_subcategid
                LEFT OUTER JOIN agenda_datetime ON agenda_datetime.agenda_id = agenda.id
            WHERE
                agenda.IdCommercant = '" . $IdCommercant . "'
            AND
                agenda.IsActif = '1' OR agenda.IsActif = '2'
            group by agenda_datetime.id, agenda.id
            ORDER BY agenda.order_partner, agenda_datetime.date_debut, agenda.date_debut ASC
            LIMIT " . $_limitstart . "," . $_limitend . "
        ");
        if ($qry->num_rows() > 0) {
            return $qry->result();
        }
    }

    function GetByIdUsers_ionauth($IdUsers_ionauth = 0)
    {
        $qry = $this->db->query("
            SELECT *
            FROM
                agenda
            WHERE IdCommercant = '" . $IdUsers_ionauth . "'
            AND IsActif <> '3'
            ORDER BY date_debut ASC
        ");
        if ($qry->num_rows() > 0) {
            return $qry->result();
        }
    }

    function GetAll()
    {
        $qry = $this->db->query("
            SELECT *
            FROM
                agenda
            ORDER BY date_debut ASC limit 10
        ");
        if ($qry->num_rows() > 0) {
            return $qry->result();
        }
    }

    function GetAllalaune()
    {
        $qry = $this->db->query("
            SELECT *
            FROM
                agenda
            WHERE alaune = '1'
            AND IsActif = '1'
            ORDER BY date_debut ASC
        ");
        if ($qry->num_rows() > 0) {
            return $qry->result();
        }
    }

    function delete($prmId)
    {
        $qur = "UPDATE `agenda` SET `IsActif` = '3' WHERE `agenda`.`id` =" . $prmId . ";";
        $qry = $this->db->query($qur);
        return $qry;
    }

    function delete_definitif($prmId)
    {
        $qry = $this->db->query("DELETE FROM agenda WHERE id = ?", $prmId);
        return $qry;
    }

    function insert($prmData)
    {
        $prmData = (array)$prmData;
        if ($prmData['id'] == '') $prmData['id'] = null;
        if ($prmData['IdVille_localisation'] == '') $prmData['IdVille_localisation'] = null;
        $this->db->insert("agenda", $prmData);
        return $this->db->insert_id();
    }

    function update($prmData)
    {
        if ($prmData['IdVille_localisation'] == '') $prmData['IdVille_localisation'] = null;
        $this->db->where("id", $prmData["id"]);
        $this->db->update("agenda", $prmData);
        $obj = $this->GetById($prmData["id"]);
        return $obj->id;
    }

    function GetAgendaCategorie()
    {
        $queryCat = "
            SELECT
            agenda_categ.agenda_categid,
            agenda_categ.agenda_typeid,
            agenda_categ.category,
            COUNT(agenda.id) as nb_agenda
            FROM
            agenda_categ
            INNER JOIN agenda ON agenda_categ.agenda_categid = agenda.agenda_categid
            WHERE
            (agenda.IsActif = '1' OR
            agenda.IsActif = '2')
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $queryCat .= " AND agenda.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "0" && $localdata_IdVille != "") {
            $queryCat .= " AND agenda.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $queryCat .= " AND agenda.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE

        $queryCat .= "
            GROUP BY
            agenda_categ.agenda_categid
            ";

        $qryCategorie = $this->db->query($queryCat);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetAgendaCategorie_ByIdCommercant($_iCommercantId)
    {

        $sqlcat = "
            SELECT
            agenda_categ.agenda_categid,
            agenda_categ.agenda_typeid,
            agenda_categ.category,
            COUNT(agenda.id) as nb_agenda
            FROM
            agenda_categ
            INNER JOIN agenda ON agenda_categ.agenda_categid = agenda.agenda_categid
            WHERE
            (agenda.IsActif = '1' OR agenda.IsActif = '2')
            AND agenda.IdCommercant = '" . $_iCommercantId . "' ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $sqlcat .= " AND agenda.IdVille_localisation = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "0" && $localdata_IdVille != "") {
            $sqlcat .= " AND agenda.IdVille_localisation = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $sqlcat .= " AND agenda.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE

        $sqlcat .= "
            GROUP BY
            agenda_categ.agenda_categid
            ";

        $qryCategorie = $this->db->query($sqlcat);

        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetAgendaCategorie_ByIdUsers_ionauth($user_ionauth_id)
    {

        $qryCategorie = $this->db->query("
            SELECT
            agenda_categ.agenda_categid,
            agenda_categ.agenda_typeid,
            agenda_categ.category,
            COUNT(agenda.id) as nb_agenda
            FROM
            agenda_categ
            INNER JOIN agenda ON agenda_categ.agenda_categid = agenda.agenda_categid
            WHERE
            (agenda.IsActif = '1' OR agenda.IsActif = '2')
            AND agenda.IdUsers_ionauth = '" . $user_ionauth_id . "'
            GROUP BY
            agenda_categ.agenda_categid
            ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAgendaSubCateg_by_IdCateg($_iCategorieId)
    {
        $qur = "SELECT
            agenda_subcateg.agenda_subcategid,
            agenda_subcateg.subcateg,
            COUNT(agenda.id) as nb_agenda
            FROM
            agenda
            INNER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = agenda.agenda_subcategid
            WHERE
            (agenda.IsActif = 1 OR agenda.IsActif = 2)
            ";
        if (isset($_iCategorieId) && $_iCategorieId != "" && $_iCategorieId != '0')
            $qur .= "
            AND agenda_subcateg.agenda_categid = '" . $_iCategorieId . "'
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $qur .= " AND agenda.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $qur .= " AND agenda.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $qur .= " AND agenda.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE

        $qur .= "
            GROUP BY
            agenda_subcateg.agenda_subcategid";

        $qryCategorie = $this->db->query($qur);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAgendaSubCateg_by_IdCateg_IdVille($_iCategorieId, $_IdVille)
    {
        $qur = "SELECT
            agenda_subcateg.agenda_subcategid,
            agenda_subcateg.subcateg,
            COUNT(agenda.id) as nb_agenda
            FROM
            agenda
            INNER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = agenda.agenda_subcategid
            WHERE
            (agenda.IsActif = 1 OR agenda.IsActif = 2)
            ";
        if (isset($_iCategorieId) && $_iCategorieId != "" && $_iCategorieId != '0')
            $qur .= "
            AND agenda_subcateg.agenda_categid = '" . $_iCategorieId . "'
            ";
        if (isset($_IdVille) && $_IdVille != "" && $_IdVille != '0')
            $qur .= "
            AND agenda.IdVille_localisation = '" . $_IdVille . "'
            ";
        $qur .= "
            GROUP BY
            agenda_subcateg.agenda_subcategid";

        ////$this->firephp->log($qur, 'qur');

        $qryCategorie = $this->db->query($qur);

        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function listeAgendaRecherche_y($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iFavoris = "", $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputFromGeo = "0", $inputGeoValue = "10", $inputGeoLatitude = "0", $inputGeoLongitude = "0")
    {

        $zSqlListePartenaire = "
            SELECT
                commercants.*,
                villes.Nom AS ville,
                bonplan.bonplan_titre AS bonplan_titre,
                bonplan.bonplan_texte AS bonplan_texte,
                annonce.annonce_description_courte AS annonce_description_courte,
                annonce.annonce_description_longue AS annonce_description_longue,
                ass_commercants_sousrubriques.IdSousRubrique AS IdSousRubrique
                ";
        if ($inputFromGeo == "1") {
            $zSqlListePartenaire .= " , (6371 * acos(cos(radians($inputGeoLatitude)) * cos(radians(commercants.latitude)) * cos(radians(commercants.longitude) - radians($inputGeoLongitude)) + sin(radians($inputGeoLatitude)) * sin(radians(commercants.latitude)))) AS distance ";
        }

        $zSqlListePartenaire .= "
            FROM
                commercants
                LEFT OUTER JOIN bonplan ON commercants.IdCommercant = bonplan.bonplan_commercant_id
                LEFT OUTER JOIN annonce ON commercants.IdCommercant = annonce.annonce_commercant_id
                INNER JOIN villes ON villes.IdVille = commercants.IdVille
                INNER JOIN ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
                LEFT OUTER JOIN sous_rubriques ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique
                LEFT OUTER JOIN rubriques ON rubriques.IdRubrique = sous_rubriques.IdRubrique
            WHERE
                commercants.IsActif = 1 AND commercants.referencement_agenda = 1 

        ";

        if ($inputFromGeo == "1") {
            $zSqlListePartenaire .= " AND commercants.latitude != '' AND commercants.longitude != '' ";
        }

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSqlListePartenaire .= " AND (";
            if (sizeof($_iCategorieId) == 1) {
                $zSqlListePartenaire .= " ass_commercants_sousrubriques.IdSousRubrique = " . $_iCategorieId[0];
            } else {
                $zSqlListePartenaire .= " ass_commercants_sousrubriques.IdSousRubrique = " . $_iCategorieId[0];
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSqlListePartenaire .= " OR ass_commercants_sousrubriques.IdSousRubrique = " . $_iCategorieId[$ii_rand];
                }
            }
            $zSqlListePartenaire .= " )";
        }

        if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null) {
            $zSqlListePartenaire .= " AND commercants.IdVille=" . $_iVilleId;
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " commercants.IdVille=" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        /*
        if($_zMotCle != ""){
            $zSqlListePartenaire .= " AND bonplan_titre LIKE '%" . $_zMotCle . "%'" ;
        }
        */
        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = str_replace("'", "\'", $SearchedWordsString);
        $SearchedWordsString = str_replace('"', '\"', $SearchedWordsString);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);

        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(commercants.Adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.Adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.Siret ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.Prenom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.SiteWeb ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.Facebook ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.Twitter ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.Caracteristiques ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.activite1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.activite2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.labelactivite1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.labelactivite2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.Email ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(bonplan.bonplan_titre ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(bonplan.bonplan_texte ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(annonce.annonce_description_courte ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(annonce.annonce_description_longue ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(sous_rubriques.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(rubriques.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }

        $zSqlListePartenaire .= " group by commercants.IdCommercant ";

        if ($inputFromGeo == "1") {
            $zSqlListePartenaire .= "  HAVING distance < $inputGeoValue  ";
        }

        if ($iOrderBy == '1') $iOrderBy_value = " order by commercants.IdCommercant desc ";
        else if ($iOrderBy == '2') $iOrderBy_value = " order by commercants.nbrevisites desc ";
        else if ($iOrderBy == '3') $iOrderBy_value = " order by commercants.IdCommercant asc ";
        else $iOrderBy_value = " order by commercants.IdCommercant desc ";

        $zSqlListePartenaire .= $iOrderBy_value;

        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $zSqlListePartenaire;
        log_message('DEBUG', $zSqlListePartenaire);
        //////$this->firephp->log($zSqlListePartenaire, 'zSqlListePartenaire');

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }


    function GetAgendaNbByMonth($month, $_iCommercantId)
    {
        $date_of_day = date("Y-m-d");
        $time_date = strtotime($date_of_day);
        $next_sunday = strtotime('next sunday, 12pm', $time_date);
        $last_sunday = strtotime('last sunday, 12pm', $time_date);
        $next_saturday = strtotime('next saturday, 11:59am', $time_date);
        $next_monday = strtotime('next monday, 11:59am', $time_date);
        $format_date = 'Y-m-d';
        $next_sunday_day = date($format_date, $next_sunday);
        $last_sunday_day = date($format_date, $last_sunday);
        $next_saturday_day = date($format_date, $next_saturday);
        $next_monday_day = date($format_date, $next_monday);
        $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
        $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
        $first_day_month = date('Y-m-01', strtotime($date_of_day));
        $last_day_month = date('Y-m-t', strtotime($date_of_day));
        $first_day_user_month = date('Y-' . $month . '-01', $time_date);
        $last_day_user_month = date('Y-' . $month . '-t', $time_date);

        $zSqlListePartenaire = "
            SELECT
                agenda_datetime.id as datetime_id, 
                agenda_datetime.date_debut as datetime_debut, 
                agenda_datetime.date_fin as datetime_fin, 
                agenda_datetime.heure_debut as datetime_heure_debut, 
                agenda.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_subcateg.subcateg,
                commercants.NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                agenda
                LEFT OUTER JOIN villes ON villes.IdVille = agenda.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = agenda.agenda_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = agenda.agenda_subcategid
                INNER JOIN commercants ON commercants.IdCommercant = agenda.IdCommercant
                LEFT OUTER JOIN agenda_datetime ON agenda_datetime.agenda_id = agenda.id

            WHERE
                (agenda.IsActif = '1' OR agenda.IsActif = '2') ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $zSqlListePartenaire .= " AND agenda.IdVille_localisation = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $zSqlListePartenaire .= " AND agenda.IdVille_localisation = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $zSqlListePartenaire .= " AND commercants.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE

        if ($_iCommercantId != "0" && $_iCommercantId != "" && $_iCommercantId != NULL) {
            $zSqlListePartenaire .= "
                AND agenda.IdCommercant = '" . $_iCommercantId . "'
                ";
        }

        if ($month != "0" && $month != "" && $month != NULL) {
            
            if ($month == "101") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $date_of_day . "' AND agenda_datetime.date_fin <= '" . $date_of_day . "' ) OR ( agenda_datetime.date_debut IS NULL AND agenda_datetime.date_fin >= '" . $date_of_day . "' ) )";
            if ($month == "202") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $next_saturday_day . "' AND agenda_datetime.date_fin <= '" . $next_sunday_day . "') OR (agenda_datetime.date_fin >= '" . $next_saturday_day . "' AND agenda_datetime.date_fin <= '" . $next_sunday_day . "') )";
            if ($month == "303") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $last_sunday_day . "' AND agenda_datetime.date_fin <= '" . $next_saturday_day . "') OR (agenda_datetime.date_fin >= '" . $last_sunday_day . "' AND agenda_datetime.date_fin <= '" . $next_saturday_day . "') )";
            if ($month == "404") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND agenda_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') OR (agenda_datetime.date_fin >= '" . $last_sunday_of_next_monday . "' AND agenda_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($month == "505") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_month . "' AND agenda_datetime.date_fin <= '" . $last_day_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_month . "' AND agenda_datetime.date_fin <= '" . $last_day_month . "' AND agenda_datetime.date_fin >= '" . $date_of_day . "') )";

            if ($month == "01") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($month == "02") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($month == "03") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($month == "04") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($month == "05") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($month == "06") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($month == "07") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($month == "08") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($month == "09") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($month == "10") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($month == "11") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($month == "12") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";


        }
        //////$this->firephp->log($zSqlListePartenaire, 'date_by_month_sql');
        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }


    function check_commercant_agenda($IdCommercant)
    {
        $zSqlListePartenaire = "
        SELECT
            commercants.IdCommercant,
            agenda.IdCommercant,
            commercants.IsActif,
            commercants.agenda,
            agenda.IsActif
            FROM
            commercants
            INNER JOIN agenda ON agenda.IdCommercant = commercants.IdCommercant
            WHERE
            commercants.IdCommercant = '" . $IdCommercant . "' AND
            commercants.IsActif = 1 AND
            agenda.IsActif = 1


        ";
        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        $toListePartenaire = $zQueryListeBonPlan->result();
        return $toListePartenaire;
    }


    function listeAgendaRecherche($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0")
    {

        $zSqlListePartenaire = "
            SELECT
                agenda_datetime.id as datetime_id, 
                agenda_datetime.date_debut as datetime_debut, 
                agenda_datetime.date_fin as datetime_fin, 
                agenda_datetime.heure_debut as datetime_heure_debut, 
                agenda.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_subcateg.subcateg,
                commercants.NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                agenda
                LEFT OUTER JOIN villes ON villes.IdVille = agenda.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = agenda.agenda_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = agenda.agenda_subcategid
                INNER JOIN commercants ON commercants.IdCommercant = agenda.IdCommercant
                LEFT OUTER JOIN agenda_datetime ON agenda_datetime.agenda_id = agenda.id

            WHERE
                (agenda.IsActif = '1' OR agenda.IsActif = '2') 
                AND commercants.IsActif = 1 
                AND commercants.referencement_agenda = 1  
                AND (agenda.id <> '0' OR agenda.id <> NULL OR agenda.id <> 0) 

        ";

        if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }

        if ($IdUsers_ionauth != "0" && $IdUsers_ionauth != "" && $IdUsers_ionauth != NULL && $IdUsers_ionauth != 0) {
            $zSqlListePartenaire .= " AND agenda.IdUsers_ionauth = '" . $IdUsers_ionauth . "'";
        }

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $date_of_day . "' AND agenda_datetime.date_fin <= '" . $date_of_day . "' ) OR ( agenda_datetime.date_debut IS NULL AND agenda_datetime.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $next_saturday_day . "' AND agenda_datetime.date_fin <= '" . $next_sunday_day . "') OR (agenda_datetime.date_fin >= '" . $next_saturday_day . "' AND agenda_datetime.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $last_sunday_day . "' AND agenda_datetime.date_fin <= '" . $next_saturday_day . "') OR (agenda_datetime.date_fin >= '" . $last_sunday_day . "' AND agenda_datetime.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND agenda_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') OR (agenda_datetime.date_fin >= '" . $last_sunday_of_next_monday . "' AND agenda_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_month . "' AND agenda_datetime.date_fin <= '" . $last_day_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_month . "' AND agenda_datetime.date_fin <= '" . $last_day_month . "' AND agenda_datetime.date_fin >= '" . $date_of_day . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";

        }

        if ($_iVilleId != 0) {
            $zSqlListePartenaire .= " AND agenda.IdVille_localisation = '" . $_iVilleId . "'";
        }
        if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null) {
            $zSqlListePartenaire .= " AND agenda.IdVille_localisation =" . $_iVilleId;
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " agenda.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlListePartenaire .= " AND agenda.IdVille_localisation = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0"){
            if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
                $zSqlListePartenaire .= " AND ( ";
                for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                    $zSqlListePartenaire .= " agenda.IdVille_localisation = '".$localdata_IdVille_all[$iiik]."' ";
                    if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListePartenaire .= " OR ";
                }
                $zSqlListePartenaire .= " ) ";
            } else if ((!isset($localdata_IdVille_all) || $localdata_IdVille_all==false) && isset($localdata_IdVille) && is_numeric($localdata_IdVille)) {
                $zSqlListePartenaire .= " AND agenda.IdVille_localisation = '".$localdata_IdVille."' ";
            }
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0"){
            $zSqlListePartenaire .= " AND agenda.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        //LOCALDATA FILTRE
        /*$this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $zSqlListePartenaire .= " AND agenda.IdVille_localisation = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $zSqlListePartenaire .= " AND agenda.IdVille_localisation = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $zSqlListePartenaire .= " AND agenda.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }*/
        //LOCALDATA FILTRE




        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            //$zSqlListePartenaire .= " AND agenda_datetime.date_debut = '" . $inputDatedebut . "'";
            $zSqlListePartenaire .= " AND ( agenda_datetime.date_debut >= '" . $inputDatedebut . "' OR agenda_datetime.date_debut IS NULL )";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            //$zSqlListePartenaire .= " AND agenda_datetime.date_fin = '" . $inputDatefin . "'";
            $zSqlListePartenaire .= " AND ( agenda_datetime.date_fin <= '" . $inputDatefin . "' )";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " agenda.agenda_categid = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " agenda.agenda_categid = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR agenda.agenda_categid = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }

        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " agenda.agenda_subcategid = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " agenda.agenda_subcategid = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR agenda.agenda_subcategid = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }

        if ($zSql_all_categ_request != "" && $zSql_all_subcateg_request == "") $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
        else if ($zSql_all_categ_request == "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND " . $zSql_all_subcateg_request;
        else if ($zSql_all_categ_request != "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ( " . $zSql_all_categ_request . " OR " . $zSql_all_subcateg_request . " ) ";


        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = str_replace("'", "\'", $SearchedWordsString);
        $SearchedWordsString = str_replace('"', '\"', $SearchedWordsString);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);

        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(agenda.nom_societe ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_categ.category ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_subcateg.subcateg ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.organisateur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.adresse_localisation_diffuseur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }

        //SORTEZ.ORG => NO BASIC PARTNER ACCOUT
        $zSqlListePartenaire .= " AND commercants.user_ionauth_id IN (SELECT users_groups_ionauth.user_ionauth_id FROM users_groups_ionauth WHERE users_groups_ionauth.group_ionauth_id = 4 OR users_groups_ionauth.group_ionauth_id = 5) ";
        //SORTEZ.ORG => NO BASIC PARTNER ACCOUT


        // demo account commercant should not appear on annuaire list
        //$zSqlListePartenaire .= " AND commercants.IdCommercant <> '301298' ";


        $zSqlListePartenaire .= " group by agenda_datetime.id, agenda.id ";

        if ($iOrderBy == '1') $iOrderBy_value = " order by agenda.last_update desc ";
        else if ($iOrderBy == '2') $iOrderBy_value = " order by agenda.accesscount desc ";
        else $iOrderBy_value = " order by agenda_datetime.date_debut,agenda_datetime.date_fin, agenda.date_debut asc ";

        $zSqlListePartenaire .= $iOrderBy_value;

        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $zSqlListePartenaire;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListeAgenda');

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }


    function listeAgendaRecherche_filtre_agenda_perso($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0", $referencement_agenda = 1)
    {

        $zSqlListePartenaire = "
            SELECT
                agenda_datetime.id as datetime_id, 
                agenda_datetime.date_debut as datetime_debut, 
                agenda_datetime.date_fin as datetime_fin, 
                agenda_datetime.heure_debut as datetime_heure_debut,
                agenda.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_subcateg.subcateg,
                commercants.NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                agenda
                LEFT OUTER JOIN villes ON villes.IdVille = agenda.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = agenda.agenda_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = agenda.agenda_subcategid
                INNER JOIN commercants ON commercants.IdCommercant = agenda.IdCommercant
                LEFT OUTER JOIN agenda_datetime ON agenda_datetime.agenda_id = agenda.id

            WHERE
                ( 
                (agenda.IsActif = '1' OR agenda.IsActif = '2') 
                AND commercants.IsActif = 1 
                ";
        if ($referencement_agenda==1)   $zSqlListePartenaire .= " AND commercants.referencement_agenda = ".$referencement_agenda." ";

        $zSqlListePartenaire .= "
                AND (agenda.id <> '0' OR agenda.id <> NULL OR agenda.id <> 0) 

        ";

        if (is_array($inputIdCommercant)) {
            $zSql_all_icommercant_request = "";
            $zSql_all_icommercant_request .= " (";
            if (sizeof($inputIdCommercant) == 1) {
                if ($inputIdCommercant[0] != "0" && $inputIdCommercant[0] != "" && $inputIdCommercant[0] != NULL && $inputIdCommercant[0] != 0) {
                    $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                } else {
                    $zSql_all_icommercant_request .= " 0=0 ";
                }
            } else {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                for ($i_iCommercantId = 1; $i_iCommercantId < count($inputIdCommercant); $i_iCommercantId++) {
                    $zSql_all_icommercant_request .= " OR commercants.IdCommercant = '" . $inputIdCommercant[$i_iCommercantId] . "' ";
                }
            }
            $zSql_all_icommercant_request .= " ) ";
            //$zSqlListePartenaire .= $zSql_all_icommercant_request;
        } else if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }

        if ($IdUsers_ionauth != "0" && $IdUsers_ionauth != "" && $IdUsers_ionauth != NULL && $IdUsers_ionauth != 0) {
            $zSqlListePartenaire .= " AND agenda.IdUsers_ionauth = '" . $IdUsers_ionauth . "'";
        }

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

            /*
            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $date_of_day . "' AND agenda_datetime.date_fin <= '" . $date_of_day . "') OR (agenda_datetime.date_debut IS NULL AND agenda_datetime.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $next_saturday_day . "' AND agenda_datetime.date_fin <= '" . $next_sunday_day . "') OR (agenda_datetime.date_debut IS NULL AND agenda_datetime.date_fin >= '" . $next_saturday_day . "' AND agenda_datetime.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $last_sunday_day . "' AND agenda_datetime.date_fin <= '" . $next_saturday_day . "') OR (agenda_datetime.date_debut IS NULL AND agenda_datetime.date_fin >= '" . $last_sunday_day . "' AND agenda_datetime.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND agenda_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') OR (agenda_datetime.date_debut IS NULL AND agenda_datetime.date_fin >= '" . $last_sunday_of_next_monday . "' AND agenda_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_month . "' AND agenda_datetime.date_fin <= '" . $last_day_month . "') OR (agenda_datetime.date_debut IS NULL AND agenda_datetime.date_fin >= '" . $first_day_month . "' AND agenda_datetime.date_fin <= '" . $last_day_month . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_debut IS NULL AND agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_debut IS NULL AND agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_debut IS NULL AND agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_debut IS NULL AND agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_debut IS NULL AND agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_debut IS NULL AND agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_debut IS NULL AND agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_debut IS NULL AND agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_debut IS NULL AND agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_debut IS NULL AND agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_debut IS NULL AND agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_debut IS NULL AND agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            */
            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $date_of_day . "' AND agenda_datetime.date_fin <= '" . $date_of_day . "' ) OR ( agenda_datetime.date_debut IS NULL AND agenda_datetime.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $next_saturday_day . "' AND agenda_datetime.date_fin <= '" . $next_sunday_day . "') OR (agenda_datetime.date_fin >= '" . $next_saturday_day . "' AND agenda_datetime.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $last_sunday_day . "' AND agenda_datetime.date_fin <= '" . $next_saturday_day . "') OR (agenda_datetime.date_fin >= '" . $last_sunday_day . "' AND agenda_datetime.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND agenda_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') OR (agenda_datetime.date_fin >= '" . $last_sunday_of_next_monday . "' AND agenda_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_month . "' AND agenda_datetime.date_fin <= '" . $last_day_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_month . "' AND agenda_datetime.date_fin <= '" . $last_day_month . "' AND agenda_datetime.date_fin >= '" . $date_of_day . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";

        }

        if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null && $_iVilleId != "0") {
            if (is_array($_iVilleId)) {
                $zSql_all_iville_request = "";
                $zSql_all_iville_request .= " (";
                if (sizeof($_iVilleId) == 1) {
                    if ($_iVilleId[0] != 0 && $_iVilleId[0] != "" && $_iVilleId[0] != null && $_iVilleId[0] != "0") {
                        $zSql_all_iville_request .= " agenda.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    } else {
                        $zSql_all_iville_request .= " 0=0 ";
                    }
                } else {
                    $zSql_all_iville_request .= " agenda.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    for ($i_iVilleId = 1; $i_iVilleId < count($_iVilleId); $i_iVilleId++) {
                        $zSql_all_iville_request .= " OR agenda.IdVille_localisation = '" . $_iVilleId[$i_iVilleId] . "' ";
                    }
                }
                $zSql_all_iville_request .= " ) ";
                ////$this->firephp->log($zSql_all_iville_request, 'zSql_all_iville_request');
                //$zSqlListePartenaire .= $zSql_all_iville_request;
            }
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " agenda.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            $zSqlListePartenaire .= " AND agenda_datetime.date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            $zSqlListePartenaire .= " AND agenda_datetime.date_fin = '" . $inputDatefin . "'";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " agenda.agenda_categid = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " agenda.agenda_categid = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR agenda.agenda_categid = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }

        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " agenda.agenda_subcategid = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " agenda.agenda_subcategid = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR agenda.agenda_subcategid = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }

        /*if($zSql_all_categ_request != "" && $zSql_all_subcateg_request == "") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;
        else if($zSql_all_categ_request == "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ".$zSql_all_subcateg_request;
        else if($zSql_all_categ_request != "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ( ".$zSql_all_categ_request." OR ".$zSql_all_subcateg_request." ) ";*/

        //if ($zSql_all_categ_request!="") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;


        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);

        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(agenda.nom_societe ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_categ.category ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_subcateg.subcateg ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.organisateur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.adresse_localisation_diffuseur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }


        $zSqlListePartenaire .= " ) ";

        if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") || (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) {
            $zSqlListePartenaire .= " AND ( ";

            if (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") $zSqlListePartenaire .= $zSql_all_icommercant_request;
            if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") && (isset($zSql_all_iville_request) && $zSql_all_iville_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_iville_request;
            else if (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") $zSqlListePartenaire .= $zSql_all_iville_request;
            if (((isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "")) && (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
            else if (isset($zSql_all_categ_request) && $zSql_all_categ_request != "") $zSqlListePartenaire .= $zSql_all_categ_request;

            $zSqlListePartenaire .= " ) ";
        }

        $zSqlListePartenaire .= " AND (agenda.IsActif = '1' OR agenda.IsActif = '2') ";

        $zSqlListePartenaire .= " group by agenda_datetime.id ";

        if ($iOrderBy == '1') $iOrderBy_value = " order by agenda.last_update desc ";
        else if ($iOrderBy == '2') $iOrderBy_value = " order by agenda.accesscount desc ";
        else $iOrderBy_value = " order by agenda_datetime.date_debut,agenda_datetime.date_fin, agenda.date_debut asc ";

        $zSqlListePartenaire .= $iOrderBy_value;

        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $_limitend;
        //echo $zSqlListePartenaire;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListePartenaire');
        //die("<br/>STOP");

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }

    function listeAgendaRecherche_agenda_perso_liste_categ($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0", $referencement_agenda = 1)
    {

        $zSqlListePartenaire = "
            SELECT
                COUNT(agenda_datetime.id) as nb_agenda, 
                agenda_datetime.date_debut as datetime_debut, 
                agenda_datetime.date_fin as datetime_fin, 
                agenda_datetime.heure_debut as datetime_heure_debut,
                agenda.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_categ.agenda_categid,
                agenda_subcateg.subcateg,
                commercants.NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                agenda
                LEFT OUTER JOIN villes ON villes.IdVille = agenda.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = agenda.agenda_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = agenda.agenda_subcategid
                INNER JOIN commercants ON commercants.IdCommercant = agenda.IdCommercant
                LEFT OUTER JOIN agenda_datetime ON agenda_datetime.agenda_id = agenda.id

            WHERE
                ( 
                (agenda.IsActif = '1' OR agenda.IsActif = '2') 
                AND commercants.IsActif = 1 
                ";
        if ($referencement_agenda==1)   $zSqlListePartenaire .= " AND commercants.referencement_agenda = ".$referencement_agenda." ";

        $zSqlListePartenaire .= "
                AND (agenda.id <> '0' OR agenda.id <> NULL OR agenda.id <> 0) 

        ";

        if (is_array($inputIdCommercant)) {
            $zSql_all_icommercant_request = "";
            $zSql_all_icommercant_request .= " (";
            if (sizeof($inputIdCommercant) == 1) {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
            } else {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                for ($i_iCommercantId = 1; $i_iCommercantId < count($inputIdCommercant); $i_iCommercantId++) {
                    $zSql_all_icommercant_request .= " OR commercants.IdCommercant = '" . $inputIdCommercant[$i_iCommercantId] . "' ";
                }
            }
            $zSql_all_icommercant_request .= " ) ";
            //$zSqlListePartenaire .= $zSql_all_icommercant_request;
        } else if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }

        if ($IdUsers_ionauth != "0" && $IdUsers_ionauth != "" && $IdUsers_ionauth != NULL && $IdUsers_ionauth != 0) {
            $zSqlListePartenaire .= " AND agenda.IdUsers_ionauth = '" . $IdUsers_ionauth . "'";
        }

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);


            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $date_of_day . "' AND agenda_datetime.date_fin <= '" . $date_of_day . "' ) OR ( agenda_datetime.date_debut IS NULL AND agenda_datetime.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $next_saturday_day . "' AND agenda_datetime.date_fin <= '" . $next_sunday_day . "') OR (agenda_datetime.date_fin >= '" . $next_saturday_day . "' AND agenda_datetime.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $last_sunday_day . "' AND agenda_datetime.date_fin <= '" . $next_saturday_day . "') OR (agenda_datetime.date_fin >= '" . $last_sunday_day . "' AND agenda_datetime.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND agenda_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') OR (agenda_datetime.date_fin >= '" . $last_sunday_of_next_monday . "' AND agenda_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_month . "' AND agenda_datetime.date_fin <= '" . $last_day_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_month . "' AND agenda_datetime.date_fin <= '" . $last_day_month . "' AND agenda_datetime.date_fin >= '" . $date_of_day . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') OR (agenda_datetime.date_fin >= '" . $first_day_user_month . "' AND agenda_datetime.date_fin <= '" . $last_day_user_month . "') )";

        }

        if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null && $_iVilleId != "0") {
            if (is_array($_iVilleId)) {
                $zSql_all_iville_request = "";
                $zSql_all_iville_request .= " (";
                if (sizeof($_iVilleId) == 1) {
                    if ($_iVilleId[0] != 0 && $_iVilleId[0] != "" && $_iVilleId[0] != null && $_iVilleId[0] != "0") {
                        $zSql_all_iville_request .= " agenda.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    } else {
                        $zSql_all_iville_request .= " 0=0 ";
                    }
                } else {
                    $zSql_all_iville_request .= " agenda.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    for ($i_iVilleId = 1; $i_iVilleId < count($_iVilleId); $i_iVilleId++) {
                        $zSql_all_iville_request .= " OR agenda.IdVille_localisation = '" . $_iVilleId[$i_iVilleId] . "' ";
                    }
                }
                $zSql_all_iville_request .= " ) ";
                ////$this->firephp->log($zSql_all_iville_request, 'zSql_all_iville_request');
                //$zSqlListePartenaire .= $zSql_all_iville_request;
            }
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " agenda.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            $zSqlListePartenaire .= " AND agenda_datetime.date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            $zSqlListePartenaire .= " AND agenda_datetime.date_fin = '" . $inputDatefin . "'";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " agenda.agenda_categid = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " agenda.agenda_categid = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR agenda.agenda_categid = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }

        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " agenda.agenda_subcategid = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " agenda.agenda_subcategid = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR agenda.agenda_subcategid = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }

        /*if($zSql_all_categ_request != "" && $zSql_all_subcateg_request == "") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;
        else if($zSql_all_categ_request == "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ".$zSql_all_subcateg_request;
        else if($zSql_all_categ_request != "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ( ".$zSql_all_categ_request." OR ".$zSql_all_subcateg_request." ) ";*/

        //if ($zSql_all_categ_request!="") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;


        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);

        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(agenda.nom_societe ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_categ.category ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_subcateg.subcateg ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.organisateur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.adresse_localisation_diffuseur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }


        $zSqlListePartenaire .= " ) ";

        if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") || (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) {
            $zSqlListePartenaire .= " AND ( ";

            if (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") $zSqlListePartenaire .= $zSql_all_icommercant_request;
            if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") && (isset($zSql_all_iville_request) && $zSql_all_iville_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_iville_request;
            else if (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") $zSqlListePartenaire .= $zSql_all_iville_request;
            if (((isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "")) && (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
            else if (isset($zSql_all_categ_request) && $zSql_all_categ_request != "") $zSqlListePartenaire .= $zSql_all_categ_request;

            $zSqlListePartenaire .= " ) ";
        }

        $zSqlListePartenaire .= " AND (agenda.IsActif = '1' OR agenda.IsActif = '2') ";

        $zSqlListePartenaire .= " group by agenda.agenda_categid ";

        if ($iOrderBy == '1') $iOrderBy_value = " order by agenda.last_update desc ";
        else if ($iOrderBy == '2') $iOrderBy_value = " order by agenda.accesscount desc ";
        else $iOrderBy_value = " order by agenda_categ.category asc ";

        $zSqlListePartenaire .= $iOrderBy_value;

        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $_limitend;
        //echo $zSqlListePartenaire;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListePartenaire');
        //die("<br/>STOP");

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }

    function listeAgendaRecherche_filtre_agenda_perso_check_categ($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0")
    {

        $zSqlListePartenaire = "
            SELECT
                agenda.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_subcateg.subcateg,
                commercants.NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                agenda
                LEFT OUTER JOIN villes ON villes.IdVille = agenda.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = agenda.agenda_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = agenda.agenda_subcategid
                INNER JOIN commercants ON commercants.IdCommercant = agenda.IdCommercant

            WHERE
                ( 0=0

        ";

        if (is_array($inputIdCommercant)) {
            $zSql_all_icommercant_request = "";
            $zSql_all_icommercant_request .= " (";
            if (sizeof($inputIdCommercant) == 1) {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
            } else {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                for ($i_iCommercantId = 1; $i_iCommercantId < count($inputIdCommercant); $i_iCommercantId++) {
                    $zSql_all_icommercant_request .= " OR commercants.IdCommercant = '" . $inputIdCommercant[$i_iCommercantId] . "' ";
                }
            }
            $zSql_all_icommercant_request .= " ) ";
            //$zSqlListePartenaire .= $zSql_all_icommercant_request;
        } else if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }

        if ($IdUsers_ionauth != "0" && $IdUsers_ionauth != "" && $IdUsers_ionauth != NULL && $IdUsers_ionauth != 0) {
            $zSqlListePartenaire .= " AND agenda.IdUsers_ionauth = '" . $IdUsers_ionauth . "'";
        }

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (agenda.date_debut >= '" . $date_of_day . "' AND agenda.date_fin <= '" . $date_of_day . "') OR (agenda.date_debut IS NULL AND agenda.date_fin >= '" . $date_of_day . "') )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( agenda.date_debut >= '" . $next_saturday_day . "' AND agenda.date_fin <= '" . $next_sunday_day . "' )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( agenda.date_debut >= '" . $last_sunday_day . "' AND agenda.date_fin <= '" . $next_saturday_day . "' )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( agenda.date_debut >= '" . $last_sunday_of_next_monday . "' AND agenda.date_fin <= '" . $next_saturday_of_next_monday . "' )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( agenda.date_debut >= '" . $first_day_month . "' AND agenda.date_fin <= '" . $last_day_month . "' )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( agenda.date_debut >= '" . $first_day_user_month . "' AND agenda.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( agenda.date_debut >= '" . $first_day_user_month . "' AND agenda.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( agenda.date_debut >= '" . $first_day_user_month . "' AND agenda.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( agenda.date_debut >= '" . $first_day_user_month . "' AND agenda.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( agenda.date_debut >= '" . $first_day_user_month . "' AND agenda.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( agenda.date_debut >= '" . $first_day_user_month . "' AND agenda.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( agenda.date_debut >= '" . $first_day_user_month . "' AND agenda.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( agenda.date_debut >= '" . $first_day_user_month . "' AND agenda.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( agenda.date_debut >= '" . $first_day_user_month . "' AND agenda.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( agenda.date_debut >= '" . $first_day_user_month . "' AND agenda.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( agenda.date_debut >= '" . $first_day_user_month . "' AND agenda.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( agenda.date_debut >= '" . $first_day_user_month . "' AND agenda.date_debut <= '" . $last_day_user_month . "' )";
        }

        if ($_iVilleId != 0) {
            if (is_array($_iVilleId)) {
                $zSql_all_iville_request = "";
                $zSql_all_iville_request .= " (";
                if (sizeof($_iVilleId) == 1) {
                    $zSql_all_iville_request .= " agenda.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                } else {
                    $zSql_all_iville_request .= " agenda.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    for ($i_iVilleId = 1; $i_iVilleId < count($_iVilleId); $i_iVilleId++) {
                        $zSql_all_iville_request .= " OR agenda.IdVille_localisation = '" . $_iVilleId[$i_iVilleId] . "' ";
                    }
                }
                $zSql_all_iville_request .= " ) ";
                ////$this->firephp->log($zSql_all_iville_request, 'zSql_all_iville_request');
                //$zSqlListePartenaire .= $zSql_all_iville_request;
            }
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " agenda.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            $zSqlListePartenaire .= " AND agenda.date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            $zSqlListePartenaire .= " AND agenda.date_fin = '" . $inputDatefin . "'";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " agenda.agenda_categid = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " agenda.agenda_categid = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR agenda.agenda_categid = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }

        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " agenda.agenda_subcategid = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " agenda.agenda_subcategid = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR agenda.agenda_subcategid = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }

        /*if($zSql_all_categ_request != "" && $zSql_all_subcateg_request == "") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;
        else if($zSql_all_categ_request == "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ".$zSql_all_subcateg_request;
        else if($zSql_all_categ_request != "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ( ".$zSql_all_categ_request." OR ".$zSql_all_subcateg_request." ) ";*/

        if ($zSql_all_categ_request != "") $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;


        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);

        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(agenda.nom_societe ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_categ.category ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_subcateg.subcateg ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.organisateur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda.adresse_localisation_diffuseur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }


        $zSqlListePartenaire .= " ) ";

        if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") || (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) {
            $zSqlListePartenaire .= " AND ( ";

            if (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") $zSqlListePartenaire .= $zSql_all_icommercant_request;
            if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") && (isset($zSql_all_iville_request) && $zSql_all_iville_request != "")) $zSqlListePartenaire .= " OR " . $zSql_all_iville_request;
            else if (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") $zSqlListePartenaire .= $zSql_all_iville_request;

            $zSqlListePartenaire .= " ) ";
        }

        $zSqlListePartenaire .= " AND (agenda.IsActif = '1' OR agenda.IsActif = '2') ";

        $zSqlListePartenaire .= " group by agenda.id ";

        if ($iOrderBy == '1') $iOrderBy_value = " order by agenda.last_update desc ";
        else if ($iOrderBy == '2') $iOrderBy_value = " order by agenda.accesscount desc ";
        else $iOrderBy_value = " order by agenda.date_debut asc ";

        $zSqlListePartenaire .= $iOrderBy_value;

        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $zSqlListePartenaire;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListePartenaire');

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }


    function GetAgendaSouscategorieByRubriqueVille_x($idRubrique, $IdVille)
    {
        $qry = "
            SELECT
               agenda.agenda_subcategid,
               COUNT(agenda.id) as nb_agenda
            FROM
                agenda
            WHERE
                (agenda.IsActif = '1' OR agenda.IsActif = '2') ";
        if ($idRubrique != "0" && $idRubrique != "" && $idRubrique != null) $qry .= " AND agenda.agenda_categid = '" . $idRubrique . "' ";
        if ($IdVille != "0" && $IdVille != "" && $IdVille != null) $qry .= " AND agenda.IdVille_localisation = '" . $IdVille . "' ";
        $qry .= "
                GROUP BY agenda.agenda_subcategid

            ";

        $qryCategorie = $this->db->query($qry);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetAgendaCategorie_by_params($inputQuand, $inputDatedebut, $inputDatefin, $_iDepartementId, $_iVilleId)
    {
        $zSqlListePartenaire = "
            SELECT
                agenda_categ.agenda_categid,
                agenda_categ.category,
                COUNT(agenda.id) AS nb_agenda
                ";

        $zSqlListePartenaire .= "
            FROM
                agenda
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = agenda.agenda_categid
                LEFT OUTER JOIN agenda_datetime ON agenda_datetime.agenda_id = agenda.id

            WHERE
                (agenda.IsActif = '1' OR agenda.IsActif = '2')

        ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $zSqlListePartenaire .= " AND agenda.IdVille_localisation = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $zSqlListePartenaire .= " AND agenda.IdVille_localisation = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $zSqlListePartenaire .= " AND agenda.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE

        if ($inputQuand != "0" && $inputQuand != "" && $inputQuand != null) {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (agenda_datetime.date_debut >= '" . $date_of_day . "' AND agenda_datetime.date_fin <= '" . $date_of_day . "') OR (agenda_datetime.date_debut IS NULL AND agenda_datetime.date_fin >= '" . $date_of_day . "') )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( agenda_datetime.date_debut >= '" . $next_saturday_day . "' AND agenda_datetime.date_fin <= '" . $next_sunday_day . "' )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( agenda_datetime.date_debut >= '" . $last_sunday_day . "' AND agenda_datetime.date_fin <= '" . $next_saturday_day . "' )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( agenda_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND agenda_datetime.date_fin <= '" . $next_saturday_of_next_monday . "' )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( agenda_datetime.date_debut >= '" . $first_day_month . "' AND agenda_datetime.date_fin <= '" . $last_day_month . "' )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( agenda_datetime.date_debut >= '" . $first_day_user_month . "' AND agenda_datetime.date_debut <= '" . $last_day_user_month . "' )";
        }

        if ($_iVilleId != "0" && $_iVilleId != "" && $_iVilleId != null) {
            $zSqlListePartenaire .= " AND agenda.IdVille_localisation = '" . $_iVilleId . "'";
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " agenda.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null && $inputDatedebut != "0") {
            $zSqlListePartenaire .= " AND agenda_datetime.date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null && $inputDatefin != "0") {
            $zSqlListePartenaire .= " AND agenda_datetime.date_fin = '" . $inputDatefin . "'";
        }

        $zSqlListePartenaire .= " group by agenda_categ.agenda_categid ";

        //$iOrderBy_value = " order by agenda_datetime.date_debut asc ";//
        $iOrderBy_value = " order by agenda_categ.category asc ";

        $zSqlListePartenaire .= $iOrderBy_value;

        //echo $zSqlListePartenaire;
        //log_message('debug', $zSqlListePartenaire);
        ////$this->firephp->log($zSqlListePartenaire, 'query_category');

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();


    }


    function effacerdoc_affiche($_iAgendaId)
    {
        $zSql = "UPDATE agenda SET doc_affiche='' WHERE id=" . $_iAgendaId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerphoto1($_iAgendaId)
    {
        $zSql = "UPDATE agenda SET photo1='' WHERE id=" . $_iAgendaId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerphoto2($_iAgendaId)
    {
        $zSql = "UPDATE agenda SET photo2='' WHERE id=" . $_iAgendaId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerphoto3($_iAgendaId)
    {
        $zSql = "UPDATE agenda SET photo3='' WHERE id=" . $_iAgendaId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerphoto4($_iAgendaId)
    {
        $zSql = "UPDATE agenda SET photo4='' WHERE id=" . $_iAgendaId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerphoto5($_iAgendaId)
    {
        $zSql = "UPDATE agenda SET photo5='' WHERE id=" . $_iAgendaId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerpdf($_iAgendaId)
    {
        $zSql = "UPDATE agenda SET pdf='' WHERE id=" . $_iAgendaId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerautre_doc_1($_iAgendaId)
    {
        $zSql = "UPDATE agenda SET autre_doc_1='' WHERE id=" . $_iAgendaId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerautre_doc_2($_iAgendaId)
    {
        $zSql = "UPDATE agenda SET autre_doc_2='' WHERE id=" . $_iAgendaId;
        $zQuery = $this->db->query($zSql);
    }


    function getAlldeposantAgenda($_iCommercantId = "0")
    {

        $zSqlAlldeposantAgenda = "SELECT
            commercants.NomSociete,
            commercants.IdCommercant
            FROM
            agenda
            INNER JOIN commercants ON agenda.IdCommercant = commercants.IdCommercant
            WHERE
            (agenda.IsActif = '1' OR agenda.IsActif = '2')";

        if ($_iCommercantId != "0" && $_iCommercantId != NULL && $_iCommercantId != "" && $_iCommercantId != 0) {
            $zSqlAlldeposantAgenda .= " AND agenda.IdCommercant = '" . $_iCommercantId . "'";
        }

        $zSqlAlldeposantAgenda .= " GROUP BY
            agenda.IdCommercant";

        $zQueryAlldeposantAgenda = $this->db->query($zSqlAlldeposantAgenda);
        return $zQueryAlldeposantAgenda->result();
    }

    function deleteOldAgenda_fin8jours()
    {
        $date_of_day = date("Y-m-d");
        $time_date = strtotime($date_of_day);
        $time_date_limit = $time_date - 581200;
        $time_date_limit_article = $time_date - 2901200;
        $date_limit = date("Y-m-d", $time_date_limit);
        $date_limit_article = date("Y-m-d", $time_date_limit_article);

        //REMOVING ALL WITH DATE_FIN < MORE 1 WEEK FOR AGENDA
        $zSqlAlldeposantAgendaDatetime = "
            SELECT agenda_id, id, COUNT(id) as nb_agenda_time FROM agenda_datetime 
            WHERE  agenda_datetime.date_fin <= '" . $date_limit . "'
            GROUP BY agenda_id 
             ;";
        $agenda_datetime_list_expire = $this->db->query($zSqlAlldeposantAgendaDatetime);
        $agenda_datetime_list_expire_result = $agenda_datetime_list_expire->result();
        if (count($agenda_datetime_list_expire_result>0)) {
            foreach ($agenda_datetime_list_expire_result as $item_n) {
                if (intval($item_n->nb_agenda_time)<=1) {
                    $this->db->query(" DELETE FROM agenda_datetime WHERE id= ".$item_n->id);
                    $this->db->query(" DELETE FROM agenda WHERE id= ".$item_n->agenda_id);
                } else {
                    $sql_date_time_agenda_id = "
                    DELETE
                    FROM
                    agenda_datetime
                    WHERE
                    agenda_id = '".$item_n->agenda_id."'
                    AND agenda_datetime.date_fin <= '" . $date_limit . "' ;";
                    $this->db->query($sql_date_time_agenda_id);
                }
            }
        }

        //REMOVING ALL WITH DATE_FIN < MORE 1 WEEK FOR ARTICLE
        $zSqlAlldeposantarticleDatetime_article = "
            SELECT article_id, id, COUNT(id) as nb_article_time FROM article_datetime 
            WHERE  article_datetime.date_fin <= '" . $date_limit_article . "'
            GROUP BY article_id 
             ;";
        $article_datetime_list_expire = $this->db->query($zSqlAlldeposantarticleDatetime_article);
        $article_datetime_list_expire_result = $article_datetime_list_expire->result();
        if (count($article_datetime_list_expire_result>0)) {
            foreach ($article_datetime_list_expire_result as $item_n) {
                if (intval($item_n->nb_article_time)<=1) {
                    $this->db->query(" DELETE FROM article_datetime WHERE id= ".$item_n->id);
                    $this->db->query(" DELETE FROM article WHERE id= ".$item_n->article_id);
                } else {
                    $sql_date_time_article_id = "
                    DELETE
                    FROM
                    article_datetime
                    WHERE
                    article_id = '".$item_n->article_id."'
                    AND article_datetime.date_fin <= '" . $date_limit_article . "' ;";
                    $this->db->query($sql_date_time_article_id);
                }
            }
        }

    }

    function get_null_article_datetime(){
        $zSqlAlldeposantAgendaDatetime_article = "
            SELECT * FROM article_datetime 
            WHERE  article_datetime.date_fin IS NULL 
             ;";
        $agenda_datetime_list_expire = $this->db->query($zSqlAlldeposantAgendaDatetime_article);
        $all_result = $agenda_datetime_list_expire->result();
        //var_dump($all_result);
        if (count($all_result)>0) {
            foreach ($all_result as $item) {
                $this->db->query(" DELETE FROM article WHERE id= ".$item->article_id);
                $this->db->query(" DELETE FROM article_datetime WHERE id= ".$item->id);
            }
        }
    }

    function delete_null_agenda_datetime(){
        $zSqlAlldeposantAgendaDatetime_article = "
            SELECT
                agenda.id as agenda_id,
                agenda_datetime.id,
                agenda_datetime.date_debut,
                agenda_datetime.date_fin
                FROM
                agenda
                LEFT OUTER JOIN agenda_datetime ON agenda_datetime.agenda_id = agenda.id
                WHERE 
                agenda_datetime.date_fin is NULL
                ORDER BY
                agenda_datetime.date_fin ASC
             ";
        $agenda_datetime_list_expire = $this->db->query($zSqlAlldeposantAgendaDatetime_article);
        $all_result = $agenda_datetime_list_expire->result();
        //var_dump($all_result);
        if (count($all_result)>0) {
            foreach ($all_result as $item) {
                $this->db->query(" DELETE FROM agenda WHERE id= ".$item->agenda_id);
            }
        }
    }

    function save_order_partner($idCommercant, $agenda_id, $inputOrderPartner)
    {
        $zSql = "UPDATE agenda SET `order_partner`='" . $inputOrderPartner . "' WHERE id='" . $agenda_id . "' AND IdCommercant='" . $idCommercant . "';";
        //var_dump($zSqleffacer_annonce_photo); die();
        $this->db->query($zSql);
    }


    function file_manager_update($_iArticleId = '0', $_iUserId = '0', $_iField = 'photo1', $_iValue = '')
    {
        $zSql = "UPDATE agenda SET `" . $_iField . "` = '" . $_iValue . "' WHERE id = " . $_iArticleId . " AND `IdUsers_ionauth` = '" . $_iUserId . "' ";
        $zQuery = $this->db->query($zSql);
    }

    function getByIdField($id = 0, $iField = 'photo1')
    {
        $Sql = "select " . $iField . " from agenda where id =" . $id;
        $Query = $this->db->query($Sql);
        $result = $Query->row();
        if ($iField == 'photo1') return $result->photo1;
        if ($iField == 'photo2') return $result->photo2;
        if ($iField == 'photo3') return $result->photo3;
        if ($iField == 'photo4') return $result->photo4;
        if ($iField == 'photo5') return $result->photo5;
    }


    public function search_actor($date_debut) {
        $this->db->select('*');
        $this->db->from('agenda_datetime');
        $this->db->like('date_debut',$date_debut);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
           return $query->result();
        }else
            //must use"return $query->result();" to avoid error when don't have data like yoour searched
            return $query->result();
    }



}