<?php

class vivresaville_villes extends CI_Model{



    function __construct() {

        parent::__construct();

    }



    function getById($id=0){

        $Sql = "select * from vivresaville_villes where id =" . $id;

        $Query = $this->db->query($Sql);

        return $Query->row();

    }

    function getBloc_Menu_by_id_vivresaville_villes($id=0){

        $ar_id = (int)$id;

        $Sql = "select * from bloc_menu where id_bloc_menu =" . $ar_id;

        $Query = $this->db->query($Sql);

        return $Query->row();

    }



    function getByIdVille($idVille=0){

        $Sql = "select * from vivresaville_villes where id_ville =" . $idVille . " ORDER BY id DESC limit 1";

        $Query = $this->db->query($Sql);

        return $Query->row();

    }



    function getAl1()

    {

        $getvivresaville = $this->db->query("

            SELECT *

            FROM

                vivresaville_villes

            ORDER BY name_ville ASC

        ");

        if ($getvivresaville->num_rows() > 0) {

            return $getvivresaville->result();

        }

    }



    function getWhere($where=''){

        $sql = "

            SELECT

                *

            FROM

                vivresaville_villes

            WHERE 

              0=0 

        ";

        if (isset($where) && $where != '') $sql .= " AND ".$where;

        $request = $this->db->query($sql);

        if($request->num_rows() > 0) {

            return $request->result();

        }

    }



    function getAl1active()

    {

        $getvivresaville = $this->db->query("

            SELECT *

            FROM

                vivresaville_villes

            WHERE isactive = '1'  

            ORDER BY name_ville ASC

        ");

        if ($getvivresaville->num_rows() > 0) {

            return $getvivresaville->result();

        }

    }



    function delete($prmId)

    {

        $sql = $this->db->query("DELETE FROM vivresaville_villes WHERE id= ?", $prmId);

        return $sql;

    }



    function insert($prmData)

    {

        $this->db->insert("vivresaville_villes", $prmData);

        return $this->db->insert_id();

    }



   function update($prmData)

    {

        $this->db->where("id", $prmData["id"]);

      

        // die();

        $this->db->update("vivresaville_villes", $prmData);

        $sql = $this->getById($prmData["id"]);

        return $sql->id;

    }

    function insert_bloc_menu($prmData)

    {

        $this->db->insert("bloc_menu", $prmData);

        return $this->db->insert_id();

    }



    function update_bloc_menu($prmData)

    {

        $this->db->where("id_bloc_menu", $prmData["id_bloc_menu"]);

        $this->db->update("bloc_menu", $prmData);

        $sql = $this->getBloc_Menu_by_id_vivresaville_villes($prmData["id_bloc_menu"]);

        return $sql->id;

    }



    function remove_logo($vsv_id=0){

        $vsv_object = $this->getById($vsv_id);

        $vsv_object = (array) $vsv_object;

        $vsv_object['logo'] = "";

        return $this->update($vsv_object);

    }

    function remove_logo_droite($vsv_id=0){

        $vsv_object = $this->getById($vsv_id);

        $vsv_object = (array) $vsv_object;

        $vsv_object['logo_droite'] = "";

        return $this->update($vsv_object);

    }

    function remove_vsv_home_photo_site_web($vsv_id=0){

        $vsv_object = $this->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

        $vsv_object = (array) $vsv_object;

        $vsv_object['home_photo_site_web'] = "";

        $this->update_bloc_menu($vsv_object);

        return $vsv_object['id_vivresaville_villes'];

    }

    /**Show list bloc */



    function show_list($idVille){

        $this->db->where('id_vivresaville_villes', $idVille);
        $this->db->order_by('content_order', 'ASC');
        $query=$this->db->get('bloc_menu');

        //select * from bloc_menu

        return $query->result();

    }

    //function model for drag and drop
    public function update_order($data = array(), $id){ 
        if(!array_key_exists('date', $data)){ 
            $data['date'] = date("Y-m-d H:i:s"); 
        } 
        $update = $this->db->update('bloc_menu', $data, array('id_bloc_menu' => $id)); 
        return $update?true:false; 
    } 

    /**Edit bloc list */

    function can_edit($edit_id){

        $query = $this->db->get_where('bloc_menu',['id_bloc_menu'=>$edit_id]);

        return $query->row();

    }

    /**delete bloc list */

    function delete_bloc($clear_id){

       
        $this->db->where('id_bloc_menu',$clear_id);
        return $this->db->delete('bloc_menu');

    }

    /**UPDATE */

    public function update_bloc($data)

    {

        $this->db->where('id_bloc_menu', $data['id_bloc_menu']);

        $query=$this->db->update('bloc_menu',$data);

       return $query;

     

        

    }









    ///new

    function remove_vsv_home_photo_articleb($vsv_id=0){

        $vsv_object = $this->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

        $vsv_object = (array) $vsv_object;

        $vsv_object['home_photo_articleb'] = "";

        //$this->update_bloc_menu($vsv_object);

        return $vsv_object['id_vivresaville_villes'];

    }

    function remove_vsv_home_photo_revue($vsv_id=0){

        $vsv_object = $this->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

        $vsv_object = (array) $vsv_object;

        $vsv_object['home_photo_revue'] = "";

      //  $this->update_bloc_menu($vsv_object);

        return $vsv_object['id_vivresaville_villes'];

    }

    function remove_vsv_home_photo_bonnes_adresses($vsv_id=0){

        $vsv_object = $this->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

        $vsv_object = (array) $vsv_object;

        $vsv_object['home_photo_bonnes_adresses'] = "";

      //  $this->update_bloc_menu($vsv_object);

        return $vsv_object['id_vivresaville_villes'];

    }

    function remove_vsv_home_photo_commerce($vsv_id=0){

        $vsv_object = $this->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

        $vsv_object = (array) $vsv_object;

        $vsv_object['home_photo_commerce'] = "";

      //  $this->update_bloc_menu($vsv_object);

        return $vsv_object['id_vivresaville_villes'];

    }

    function remove_vsv_home_photo_form($vsv_id=0){

        $vsv_object = $this->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

        $vsv_object = (array) $vsv_object;

        $vsv_object['home_photo_form'] = "";

       // $this->update_bloc_menu($vsv_object);

        return $vsv_object['id_vivresaville_villes'];

    }

    function remove_vsv_home_photo_deal($vsv_id=0){

        $vsv_object = $this->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

        $vsv_object = (array) $vsv_object;

        $vsv_object['home_photo_deal'] = "";

       // $this->update_bloc_menu($vsv_object);

        return $vsv_object['id_vivresaville_villes'];

    }

    function remove_vsv_home_photo_carte($vsv_id=0){

        $vsv_object = $this->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

        $vsv_object = (array) $vsv_object;

        $vsv_object['home_photo_carte'] = "";

      //  $this->update_bloc_menu($vsv_object);

        return $vsv_object['id_vivresaville_villes'];

    }



    function remove_background($vsv_id=0){

        $vsv_object = $this->getById($vsv_id);

        $vsv_object = (array) $vsv_object;

        $vsv_object['background'] = "";

        return $this->update($vsv_object);

    }



    function remove_vsv_home_photo_banner($vsv_id=0){

        $vsv_object = $this->getById($vsv_id);

        $vsv_object = (array) $vsv_object;

        $vsv_object['home_photo_banner'] = "";

        return $this->update($vsv_object);

    }



    function remove_vsv_home_photo_article($vsv_id=0){

        $vsv_object = $this->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

        $vsv_object = (array) $vsv_object;

        $vsv_object['home_photo_article'] = "";

        return $this->update($vsv_object);

    }



    function remove_vsv_home_photo_agenda($vsv_id=0){

        $vsv_object = $this->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

        $vsv_object = (array) $vsv_object;

        $vsv_object['home_photo_agendab'] = "";

      //  $this->update_bloc_menu($vsv_object);

        return $vsv_object['id_vivresaville_villes'];

    }



    function remove_vsv_home_photo_annonce($vsv_id=0){

        $vsv_object = $this->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

        $vsv_object = (array) $vsv_object;

        $vsv_object['home_photo_annonceb'] = "";

      //  $this->update_bloc_menu($vsv_object);

        return $vsv_object['id_vivresaville_villes'];

    }



    function remove_vsv_home_photo_annuaire($vsv_id=0){

        $vsv_object = $this->getById($vsv_id);

        $vsv_object = (array) $vsv_object;

        $vsv_object['home_photo_annuaire'] = "";

        return $this->update($vsv_object);

    }



    function remove_vsv_home_photo_fidelite($vsv_id=0){

        $vsv_object = $this->getById($vsv_id);

        $vsv_object = (array) $vsv_object;

        $vsv_object['home_photo_fidelite'] = "";

        return $this->update($vsv_object);

    }



    function remove_vsv_home_photo_bonplan($vsv_id=0){

        $vsv_object = $this->getById($vsv_id);

        $vsv_object = (array) $vsv_object;

        $vsv_object['home_photo_bonplan'] = "";

        return $this->update($vsv_object);

    }

    function remove_vsv_home_photo_newsletter($vsv_id=0){

        $vsv_object = $this->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

        $vsv_object = (array) $vsv_object;

        $vsv_object['home_photo_newsletter'] = "";

        $this->update_bloc_menu($vsv_object);

        return $vsv_object['id_vivresaville_villes'];

    }



    function remove_vsv_home_photo_gestion($vsv_id=0){

        $vsv_object = $this->getBloc_Menu_by_id_vivresaville_villes($vsv_id);

        $vsv_object = (array) $vsv_object;

        $vsv_object['home_photo_gestion'] = "";

       // $this->update_bloc_menu($vsv_object);

        return $vsv_object['id_vivresaville_villes'];

    }



}