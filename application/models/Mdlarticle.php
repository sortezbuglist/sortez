<?php

class mdlarticle extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getById($id = 0)
    {
        $Sql = "select * from article where id =" . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }


    function GetByIdCommercant($IdCommercant = 0)
    {
        $sql_qr = "
            SELECT *
            FROM
                article
            WHERE IdCommercant = '" . $IdCommercant . "'
            AND IsActif <> '3'
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sql_qr .= " AND article.IdVille_localisation = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sql_qr .= " AND article.IdVille_localisation = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sql_qr .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sql_qr .= " article.IdVille_localisation = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sql_qr .= " OR ";
            }
            $sql_qr .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sql_qr .= " AND article.IdVille_localisation IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        $sql_qr .= "
            ORDER BY order_partner ASC
        ";

        $qry = $this->db->query($sql_qr);
        if ($qry->num_rows() > 0) {
            return $qry->result();
        }
    }

    function GetByIdCommercant_admin($IdCommercant = 0)
    {
        $sql_qr = "
            SELECT *
            FROM
                article
            WHERE IdCommercant = '" . $IdCommercant . "'
            AND IsActif <> '3'
            ";

        $sql_qr .= "
            ORDER BY order_partner ASC
        ";

        $qry = $this->db->query($sql_qr);
        if ($qry->num_rows() > 0) {
            return $qry->result();
        }
    }

    function GetAll()
    {
        $qryarticle = $this->db->query("
            SELECT *
            FROM   
                article
            ORDER BY id desc
        ");
        if ($qryarticle->num_rows() > 0) {
            return $qryarticle->result();
        }
    }

    function delete($prmId)
    {

        $qryBonplan = $this->db->query("DELETE FROM article WHERE id = ?", $prmId);
        return $qryBonplan;
    }

    function insert($prmData)
    {
        if ($prmData['IdVille_localisation'] == '') $prmData['IdVille_localisation'] = null;
        if ($prmData['date_depot'] == '--' || $prmData['date_depot'] == '') $prmData['date_depot'] = null;
        $this->db->insert("article", $prmData);
        return $this->db->insert_id();
    }

    function update($prmData)
    {
        if ($prmData['IdVille_localisation'] == '') $prmData['IdVille_localisation'] = null;
        $this->db->where("id", $prmData["id"]);
        $this->db->update("article", $prmData);
        $objAnnonce = $this->getById($prmData["id"]);
        return $objAnnonce->id;
    }


    function effacerdoc_affiche($_iArticleId)
    {
        $zSql = "UPDATE article SET doc_affiche='' WHERE id=" . $_iArticleId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerphoto1($_iArticleId)
    {
        $zSql = "UPDATE article SET photo1='' WHERE id=" . $_iArticleId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerphoto2($_iArticleId)
    {
        $zSql = "UPDATE article SET photo2='' WHERE id=" . $_iArticleId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerphoto3($_iArticleId)
    {
        $zSql = "UPDATE article SET photo3='' WHERE id=" . $_iArticleId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerphoto4($_iArticleId)
    {
        $zSql = "UPDATE article SET photo4='' WHERE id=" . $_iArticleId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerphoto5($_iArticleId)
    {
        $zSql = "UPDATE article SET photo5='' WHERE id=" . $_iArticleId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerpdf($_iArticleId)
    {
        $zSql = "UPDATE article SET pdf='' WHERE id=" . $_iArticleId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerautre_doc_1($_iArticleId)
    {
        $zSql = "UPDATE article SET autre_doc_1='' WHERE id=" . $_iArticleId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerautre_doc_2($_iArticleId)
    {
        $zSql = "UPDATE article SET autre_doc_2='' WHERE id=" . $_iArticleId;
        $zQuery = $this->db->query($zSql);
    }


    function listeArticleRecherche(
        $_iCategorieId = 0,
        $_iVilleId = 0,
        $_iDepartementId = 0,
        $_zMotCle = "",
        $_iSousCategorieId = 0,
        $_limitstart = 0,
        $_limitend = 10000,
        $iOrderBy = "",
        $inputQuand = "0",
        $inputDatedebut = "0000-00-00",
        $inputDatefin = "0000-00-00",
        $inputIdCommercant = "0",
        $agenda_article_type_id
    )
    {

        $zSqlListePartenaire = "
            SELECT
                article_datetime.id as datetime_id, 
                article_datetime.date_debut as datetime_debut, 
                article_datetime.date_fin as datetime_fin, 
                article_datetime.heure_debut as datetime_heure_debut, 
                article.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_subcateg.subcateg,
                commercants.NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                article
                LEFT OUTER JOIN villes ON villes.IdVille = article.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = article.article_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = article.article_subcategid
                INNER JOIN commercants ON commercants.IdCommercant = article.IdCommercant
                LEFT OUTER JOIN article_datetime ON article_datetime.article_id = article.id

            WHERE
                (article.IsActif = '1' OR article.IsActif = '2') 
                AND commercants.IsActif = '1' 
                AND commercants.referencement_article = '1' 
                AND (article.id <> '0' OR article.id <> NULL OR article.id <> 0) 

        ";

        if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }

//        if ($IdUsers_ionauth != "0" && $IdUsers_ionauth != "" && $IdUsers_ionauth != NULL && $IdUsers_ionauth != 0) {
//            $zSqlListePartenaire .= " AND article.IdUsers_ionauth = '" . $IdUsers_ionauth . "'";
//        }

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $date_of_day . "' AND article_datetime.date_fin <= '" . $date_of_day . "' ) OR ( article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "') OR (article_datetime.date_fin >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "') OR (article_datetime.date_fin >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') OR (article_datetime.date_fin >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "') OR (article_datetime.date_fin >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "' AND article_datetime.date_fin >= '" . $date_of_day . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";

        }

        if ($_iVilleId != 0) {
            $zSqlListePartenaire .= " AND article.IdVille_localisation = '" . $_iVilleId . "'";
        }
        if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null) {
            $zSqlListePartenaire .= " AND article.IdVille_localisation =" . $_iVilleId;
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " article.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }

        //LOCALDATA FILTRE
        // $this_session_localdata =& get_instance();
        // $this_session_localdata->load->library('session');
        // $localdata_value = $this_session_localdata->session->userdata('localdata');
        // $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        // $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        // $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        // $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        // if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
        //     $zSqlListePartenaire .= " AND article.IdVille_localisation = '2031' ";
        // } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
        //     if (!isset($localdata_IdVille_all)){
        //         $zSqlListePartenaire .= " AND article.IdVille_localisation = '".$localdata_IdVille."' ";
        //     } else {
        //         $zSqlListePartenaire .= " AND ( ";
        //         for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
        //             $zSqlListePartenaire .= " article.IdVille_localisation = '".$localdata_IdVille_all[$iiik]."' ";
        //             if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListePartenaire .= " OR ";
        //         }
        //         $zSqlListePartenaire .= " ) ";
        //     }
            
        // } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
        //     $zSqlListePartenaire .= " AND article.IdVille_localisation IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        // }
        //LOCALDATA FILTRE


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            //$zSqlListePartenaire .= " AND article_datetime.date_debut = '" . $inputDatedebut . "'";
            $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $inputDatedebut . "' OR article_datetime.date_debut IS NULL )";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            //$zSqlListePartenaire .= " AND article_datetime.date_fin = '" . $inputDatefin . "'";
            $zSqlListePartenaire .= " AND ( article_datetime.date_fin <= '" . $inputDatefin . "' )";
        }
        if ($agenda_article_type_id != "" && $agenda_article_type_id !="0") {
            $zSqlListePartenaire .= " AND ( agenda_categ.category LIKE '%" . $agenda_article_type_id . "%' )";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

//        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
//            $zSql_all_categ_request .= " (";
//            if (sizeof($_iCategorieId) == 1) {
//                $zSql_all_categ_request .= " article.article_categid = '" . $_iCategorieId[0] . "' ";
//            } else {
//                $zSql_all_categ_request .= " article.article_categid = '" . $_iCategorieId[0] . "' ";
//                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
//                    $zSql_all_categ_request .= " OR article.article_categid = '" . $_iCategorieId[$ii_rand] . "' ";
//                }
//            }
//            $zSql_all_categ_request .= " ) ";
//        }
        if (isset($_iCategorieId) && $_iCategorieId != NULL && $_iCategorieId != "" && $_iCategorieId != 0) {

                $zSql_all_categ_request .= " article.article_categid = '" . $_iCategorieId . "' ";

        }

        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " article.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " article.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR article.article_subcategid = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }

        if ($zSql_all_categ_request != "" && $zSql_all_subcateg_request == "") $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
        else if ($zSql_all_categ_request == "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND " . $zSql_all_subcateg_request;
        else if ($zSql_all_categ_request != "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ( " . $zSql_all_categ_request . " OR " . $zSql_all_subcateg_request . " ) ";


        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = str_replace("'", "\'", $SearchedWordsString);
        $SearchedWordsString = str_replace('"', '\"', $SearchedWordsString);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);

        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(article.nom_societe ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_categ.category ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_subcateg.subcateg ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.organisateur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.adresse_localisation_diffuseur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }

        //SORTEZ.ORG => NO BASIC PARTNER ACCOUT
        // $zSqlListePartenaire .= " AND commercants.user_ionauth_id IN (SELECT users_groups_ionauth.user_ionauth_id FROM users_groups_ionauth WHERE users_groups_ionauth.group_ionauth_id = 4 OR users_groups_ionauth.group_ionauth_id = 5) ";
        //SORTEZ.ORG => NO BASIC PARTNER ACCOUT


        //DIFERENCIATE ARTICLE AND REVUE DE PRESSE  > article = 1 / revue = 3
        /*if(isset($agenda_article_type_id) && is_numeric($agenda_article_type_id))
        $zSqlListePartenaire .= " AND article.agenda_article_type_id = ".$agenda_article_type_id." ";*/


        // demo account commercant should not appear on annuaire list
        //$zSqlListePartenaire .= " AND commercants.IdCommercant <> '301298' ";


        $zSqlListePartenaire .= " group by article_datetime.id, article.id ";

        if ($iOrderBy == '1') $iOrderBy_value = " order by article.last_update desc ";
        else if ($iOrderBy == '2') $iOrderBy_value = " order by article.accesscount desc ";
        else $iOrderBy_value = " order by article_datetime.date_debut desc, article_datetime.date_fin desc, article.date_debut desc ";

        $zSqlListePartenaire .= $iOrderBy_value;

        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //die($zSqlListePartenaire);
        //echo "<br/>".$_limitstart." / ".$_limitend;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListeAgenda');
        if ($inputQuand == "09") log_message('error', 'william ARTICLE MAIN SQL : ' . $zSqlListePartenaire);


        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }

    function listeArticleRecherche_commercant_list($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0",$agenda_article_type_id = "0")
    {

        $zSqlListePartenaire = "
            SELECT
                commercants.IdCommercant,
                commercants.NomSociete,
                count(article.id) as nbcommercant
                ";

        $zSqlListePartenaire .= "
            FROM
                article
                LEFT OUTER JOIN villes ON villes.IdVille = article.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = article.article_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = article.article_subcategid
                INNER JOIN commercants ON commercants.IdCommercant = article.IdCommercant
                LEFT OUTER JOIN article_datetime ON article_datetime.article_id = article.id

            WHERE
                (article.IsActif = '1' OR article.IsActif = '2') 
                AND commercants.IsActif = '1' 
                AND commercants.referencement_article = '1' 
                AND (article.id <> '0' OR article.id <> NULL OR article.id <> 0) 

        ";

        if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }

        if ($IdUsers_ionauth != "0" && $IdUsers_ionauth != "" && $IdUsers_ionauth != NULL && $IdUsers_ionauth != 0) {
            $zSqlListePartenaire .= " AND article.IdUsers_ionauth = '" . $IdUsers_ionauth . "'";
        }

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $date_of_day . "' AND article_datetime.date_fin <= '" . $date_of_day . "' ) OR ( article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "') OR (article_datetime.date_fin >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "') OR (article_datetime.date_fin >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') OR (article_datetime.date_fin >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "') OR (article_datetime.date_fin >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "' AND article_datetime.date_fin >= '" . $date_of_day . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";

        }

        if ($_iVilleId != 0) {
            $zSqlListePartenaire .= " AND article.IdVille_localisation = '" . $_iVilleId . "'";
        }
        if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null) {
            $zSqlListePartenaire .= " AND article.IdVille_localisation =" . $_iVilleId;
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " article.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlListePartenaire .= " AND article.IdVille_localisation = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlListePartenaire .= " AND article.IdVille_localisation = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlListePartenaire .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlListePartenaire .= " article.IdVille_localisation = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListePartenaire .= " OR ";
            }
            $zSqlListePartenaire .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlListePartenaire .= " AND article.IdVille_localisation IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            //$zSqlListePartenaire .= " AND article_datetime.date_debut = '" . $inputDatedebut . "'";
            $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $inputDatedebut . "' OR article_datetime.date_debut IS NULL )";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            //$zSqlListePartenaire .= " AND article_datetime.date_fin = '" . $inputDatefin . "'";
            $zSqlListePartenaire .= " AND ( article_datetime.date_fin <= '" . $inputDatefin . "' )";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " article.article_categid = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " article.article_categid = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR article.article_categid = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }

        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " article.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " article.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR article.article_subcategid = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }

        if ($zSql_all_categ_request != "" && $zSql_all_subcateg_request == "") $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
        else if ($zSql_all_categ_request == "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND " . $zSql_all_subcateg_request;
        else if ($zSql_all_categ_request != "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ( " . $zSql_all_categ_request . " OR " . $zSql_all_subcateg_request . " ) ";


        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = str_replace("'", "\'", $SearchedWordsString);
        $SearchedWordsString = str_replace('"', '\"', $SearchedWordsString);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);

        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(article.nom_societe ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_categ.category ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_subcateg.subcateg ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.organisateur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.adresse_localisation_diffuseur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }

        //SORTEZ.ORG => NO BASIC PARTNER ACCOUT
        $zSqlListePartenaire .= " AND commercants.user_ionauth_id IN (SELECT users_groups_ionauth.user_ionauth_id FROM users_groups_ionauth WHERE users_groups_ionauth.group_ionauth_id = 4 OR users_groups_ionauth.group_ionauth_id = 5) ";
        //SORTEZ.ORG => NO BASIC PARTNER ACCOUT


        //DIFERENCIATE ARTICLE AND REVUE DE PRESSE  > article = 1 / revue = 3
        if(isset($agenda_article_type_id) && $agenda_article_type_id != "0" && $agenda_article_type_id != "")
            $zSqlListePartenaire .= " AND agenda_categ.category LIKE '%".$agenda_article_type_id."%' ";


        // demo account commercant should not appear on annuaire list
        //$zSqlListePartenaire .= " AND commercants.IdCommercant <> '301298' ";


        $zSqlListePartenaire .= " group by commercants.IdCommercant ";

        $zSqlListePartenaire .= " order by commercants.NomSociete desc ";

        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //die($zSqlListePartenaire);
        //echo "<br/>".$_limitstart." / ".$_limitend;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListeAgenda');
        if ($inputQuand == "09") log_message('error', 'william ARTICLE MAIN SQL : ' . $zSqlListePartenaire);


        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }

    function listeArticleRecherche_filtre_article_perso($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0", $referencement_article = 1,$tiville)
    {
        $agenda_article_type_id = 1;
        $zSqlListePartenaire = "
            SELECT
                article_datetime.id as datetime_id, 
                article_datetime.date_debut as datetime_debut, 
                article_datetime.date_fin as datetime_fin, 
                article_datetime.heure_debut as datetime_heure_debut,
                article.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_subcateg.subcateg,
                commercants.NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                article
                LEFT OUTER JOIN villes ON villes.IdVille = article.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = article.article_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = article.article_subcategid
                INNER JOIN commercants ON commercants.IdCommercant = article.IdCommercant
                LEFT OUTER JOIN article_datetime ON article_datetime.article_id = article.id

            WHERE
                ( 
                (article.IsActif = '1' OR article.IsActif = '2') 
                AND commercants.IsActif = 1 
                ";
        if ($referencement_article==1)   $zSqlListePartenaire .= " AND commercants.referencement_article = ".$referencement_article." ";

        $zSqlListePartenaire .= "
                AND (article.id <> '0' OR article.id <> NULL OR article.id <> 0) 

        ";

        if (is_array($inputIdCommercant)) {
            $zSql_all_icommercant_request = "";
            $zSql_all_icommercant_request .= " (";
            if (sizeof($inputIdCommercant) == 1) {
                if ($inputIdCommercant[0] != "0" && $inputIdCommercant[0] != "" && $inputIdCommercant[0] != NULL && $inputIdCommercant[0] != 0) {
                    $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                } else {
                    $zSql_all_icommercant_request .= " 0=0 ";
                }
            } else {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                for ($i_iCommercantId = 1; $i_iCommercantId < count($inputIdCommercant); $i_iCommercantId++) {
                    $zSql_all_icommercant_request .= " OR commercants.IdCommercant = '" . $inputIdCommercant[$i_iCommercantId] . "' ";
                }
            }
            $zSql_all_icommercant_request .= " ) ";
            //$zSqlListePartenaire .= $zSql_all_icommercant_request;
        } else if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }

        if ($IdUsers_ionauth != "0" && $IdUsers_ionauth != "" && $IdUsers_ionauth != NULL && $IdUsers_ionauth != 0) {
            $zSqlListePartenaire .= " AND article.IdUsers_ionauth = '" . $IdUsers_ionauth . "'";
        }
        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

            /*
            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $date_of_day . "' AND article_datetime.date_fin <= '" . $date_of_day . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            */
            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $date_of_day . "' AND article_datetime.date_fin <= '" . $date_of_day . "' ) OR ( article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "') OR (article_datetime.date_fin >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "') OR (article_datetime.date_fin >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') OR (article_datetime.date_fin >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "') OR (article_datetime.date_fin >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "' AND article_datetime.date_fin >= '" . $date_of_day . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";

        }

        if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null && $_iVilleId != "0") {
            if (is_array($_iVilleId)) {
                $zSql_all_iville_request = "";
                $zSql_all_iville_request .= " (";
                if (sizeof($_iVilleId) == 1) {
                    if ($_iVilleId[0] != 0 && $_iVilleId[0] != "" && $_iVilleId[0] != null && $_iVilleId[0] != "0") {
                        $zSql_all_iville_request .= " article.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    } else {
                        $zSql_all_iville_request .= " 0=0 ";
                    }
                } else {
                    $zSql_all_iville_request .= " article.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    for ($i_iVilleId = 1; $i_iVilleId < count($_iVilleId); $i_iVilleId++) {
                        $zSql_all_iville_request .= " OR article.IdVille_localisation = '" . $_iVilleId[$i_iVilleId] . "' ";
                    }
                }
                $zSql_all_iville_request .= " ) ";
                ////$this->firephp->log($zSql_all_iville_request, 'zSql_all_iville_request');
                //$zSqlListePartenaire .= $zSql_all_iville_request;
            }
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " article.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            $zSqlListePartenaire .= " AND article_datetime.date_debut = '" . $inputDatedebut . "'";
        }
        if ($tiville != "0" && $tiville != "" && $tiville != null) {
            $zSqlListePartenaire .= " AND article.IdVille_localisation= '" . $tiville . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            $zSqlListePartenaire .= " AND article_datetime.date_fin = '" . $inputDatefin . "'";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " article.article_categid = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " article.article_categid = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR article.article_categid = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }

        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " article.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " article.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR article.article_subcategid = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }

        /*if($zSql_all_categ_request != "" && $zSql_all_subcateg_request == "") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;
        else if($zSql_all_categ_request == "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ".$zSql_all_subcateg_request;
        else if($zSql_all_categ_request != "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ( ".$zSql_all_categ_request." OR ".$zSql_all_subcateg_request." ) ";*/

        //if ($zSql_all_categ_request!="") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;


        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);
        //var_dump($SearchedWordsString);
        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(article.nom_societe ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_categ.category ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_subcateg.subcateg ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.organisateur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.adresse_localisation_diffuseur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }


        $zSqlListePartenaire .= " ) ";

        if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") || (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) {
            $zSqlListePartenaire .= " AND ( ";

            if (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") $zSqlListePartenaire .= $zSql_all_icommercant_request;
            if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") && (isset($zSql_all_iville_request) && $zSql_all_iville_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_iville_request;
            else if (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") $zSqlListePartenaire .= $zSql_all_iville_request;
            if (((isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "")) && (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
            else if (isset($zSql_all_categ_request) && $zSql_all_categ_request != "") $zSqlListePartenaire .= $zSql_all_categ_request;

            $zSqlListePartenaire .= " ) ";
        }


        //DIFERENCIATE ARTICLE AND REVUE DE PRESSE  > article = 1 / revue = 3
//        if(isset($agenda_article_type_id) && is_numeric($agenda_article_type_id))
            //$zSqlListePartenaire .= " AND article.agenda_article_type_id = ".$agenda_article_type_id." ";

        $zSqlListePartenaire .= " AND (article.IsActif = '1' OR article.IsActif = '2') ";

        $zSqlListePartenaire .= " group by article_datetime.id ";

        if ($iOrderBy == '1') $iOrderBy_value = " order by article.last_update desc ";
        else if ($iOrderBy == '2') $iOrderBy_value = " order by article.accesscount desc ";
        else $iOrderBy_value = " order by article_datetime.date_debut,article_datetime.date_fin, article.date_debut asc ";

        $zSqlListePartenaire .= $iOrderBy_value;

        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $_limitend;
        //echo $zSqlListePartenaire;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListePartenaire');
        //die("<br/>STOP");
        //var_dump($zSqlListePartenaire);
        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }


    function GetArticleCategorie_ByIdCommercant($_iCommercantId="0")
    {

        $sqlcat = "
            SELECT
            agenda_categ.agenda_categid,
            agenda_categ.agenda_typeid,
            agenda_categ.category,
            COUNT(article.id) as nb_article
            FROM
            agenda_categ
            INNER JOIN article ON agenda_categ.agenda_categid = article.article_categid
            LEFT OUTER JOIN article_datetime ON article_datetime.article_id = article.id
            WHERE
            (article.IsActif = '1' OR article.IsActif = '2')
            ";

        if (isset($_iCommercantId) && $_iCommercantId != "" && $_iCommercantId != "0" && $_iCommercantId != null) {
            $sqlcat .= "
                AND
                article.IdCommercant = '".$_iCommercantId."'
                ";
        }

        //LOCALDATA FILTRE
        // $this_session_localdata =& get_instance();
        // $this_session_localdata->load->library('session');
        // $localdata_value = $this_session_localdata->session->userdata('localdata');
        // $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        // $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        // $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        // $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        // if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
        //     $sqlcat .= " AND article.IdVille_localisation = '2031' ";
        // } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
        //     $sqlcat .= " AND article.IdVille_localisation = '".$localdata_IdVille."' ";
        // } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
        //     $sqlcat .= " AND ( ";
        //     for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
        //         $sqlcat .= " article.IdVille_localisation = '".$localdata_IdVille_all[$iiik]."' ";
        //         if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
        //     }
        //     $sqlcat .= " ) ";
        // } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
        //     $sqlcat .= " AND article.IdVille_localisation IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        // }
        //LOCALDATA FILTRE

        $sqlcat .= "
            GROUP BY
            agenda_categ.agenda_categid
            ";

        $qryCategorie = $this->db->query($sqlcat);

        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetArticleCategorie()
    {
        $queryCat = "
            SELECT
            agenda_categ.agenda_categid,
            agenda_categ.agenda_typeid,
            agenda_categ.category,
            COUNT(article.id) as nb_article
            FROM
            agenda_categ
            INNER JOIN article ON agenda_categ.agenda_categid = article.article_categid
            WHERE
            (article.IsActif = '1' OR
            article.IsActif = '2')
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $queryCat .= " AND article.IdVille_localisation = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $queryCat .= " AND article.IdVille_localisation = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $queryCat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $queryCat .= " article.IdVille_localisation = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $queryCat .= " OR ";
            }
            $queryCat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $queryCat .= " AND article.IdVille_localisation IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        $queryCat .= "
            GROUP BY
            agenda_categ.agenda_categid
            ";

        $qryCategorie = $this->db->query($queryCat);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetArticleCategorie_by_params($inputQuand="0", $inputDatedebut="", $inputDatefin="", $_iDepartementId="0", $_iVilleId="0", $inputIdCommercant=0)
    {
        $zSqlListePartenaire = "
            SELECT
                agenda_categ.agenda_categid,
                agenda_categ.category,
                COUNT(article.id) AS nb_article
                ";

        $zSqlListePartenaire .= "
            FROM
                article
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = article.article_categid
                LEFT OUTER JOIN article_datetime ON article_datetime.article_id = article.id

            WHERE
                (article.IsActif = '1' OR article.IsActif = '2')

        ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlListePartenaire .= " AND article.IdVille_localisation = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlListePartenaire .= " AND article.IdVille_localisation = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlListePartenaire .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlListePartenaire .= " article.IdVille_localisation = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListePartenaire .= " OR ";
            }
            $zSqlListePartenaire .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlListePartenaire .= " AND article.IdVille_localisation IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        if ($inputQuand != "0" && $inputQuand != "" && $inputQuand != null) {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $date_of_day . "' AND article_datetime.date_fin <= '" . $date_of_day . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $date_of_day . "') )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "' )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "' )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "' )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "' )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
        }

        if ($_iVilleId != "0" && $_iVilleId != "" && $_iVilleId != null) {
            $zSqlListePartenaire .= " AND article.IdVille_localisation = '" . $_iVilleId . "'";
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " article.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null && $inputDatedebut != "0") {
            $zSqlListePartenaire .= " AND article_datetime.date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null && $inputDatefin != "0") {
            $zSqlListePartenaire .= " AND article_datetime.date_fin = '" . $inputDatefin . "'";
        }

        if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND article.IdCommercant = '" . $inputIdCommercant . "'";
        }

        //$zSqlListePartenaire .= " AND agenda_article_type_id = 1 ";// Differenciate article with Revue de presse

        $zSqlListePartenaire .= " group by agenda_categ.agenda_categid ";

        //$iOrderBy_value = " order by article_datetime.date_debut asc ";//
        $iOrderBy_value = " order by agenda_categ.category asc ";

        $zSqlListePartenaire .= $iOrderBy_value;

        //echo $zSqlListePartenaire;
        //log_message('debug', $zSqlListePartenaire);
        ////$this->firephp->log($zSqlListePartenaire, 'query_category');
        //log_message('error', 'CHECKING_BY_CATEG_NUMBER : '.$zSqlListePartenaire);

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();

    }

    function getCategoryListInArticle($_iDepartementId="0", $_iVilleId="0", $inputIdCommercant=0, $_iMediasType="") {
        $zSqlListePartenaire = '
            SELECT
            agenda_categ.agenda_categid,
            agenda_categ.category,
            Count(article.id) AS nbarticle,
            villes.ville_departement
            FROM
            agenda_categ
            INNER JOIN article ON article.article_categid = agenda_categ.agenda_categid
            INNER JOIN villes ON villes.IdVille = article.IdVille_localisation
            WHERE
            (article.IsActif = 1 OR
            article.IsActif = 2) 
            ';
        if (isset($_iDepartementId) && $_iDepartementId != "0" && $_iDepartementId != "") {
            $zSqlListePartenaire .= '
            AND villes.ville_departement = "'.$this->getDepartmentCodeById($_iDepartementId)->ville_departement.'" 
            ';
        }
        if (isset($_iVilleId) && $_iVilleId != "0" && $_iVilleId != "") {
            $zSqlListePartenaire .= '
            AND article.IdVille_localisation = "'.$_iVilleId.'" 
            ';
        }
        if (isset($inputIdCommercant) && $inputIdCommercant != "0" && $inputIdCommercant != "") {
            $zSqlListePartenaire .= '
            AND article.IdCommercant = "'.$inputIdCommercant.'" 
            ';
        }
        if (isset($_iMediasType) && $_iMediasType != "0" && $_iMediasType != "") {
            $zSqlListePartenaire .= '
            AND article.agenda_article_type_id = "'.$_iMediasType.'" 
            ';
        }
        $zSqlListePartenaire .= '
            GROUP BY
            agenda_categ.agenda_categid
            ';

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }

    function GetById_IsActif($id = 0)
    {
        $qry = $this->db->query("
            SELECT
            article.*,
            villes.Nom AS ville,
            agenda_categ.category,
            agenda_categ.agenda_categid,
            agenda_subcateg.subcateg

            FROM
                article
            LEFT OUTER JOIN villes ON villes.IdVille = article.IdVille_localisation
            INNER JOIN agenda_categ ON agenda_categ.agenda_categid = article.article_categid
            LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = article.article_subcategid

            WHERE id = '" . $id . "'
            AND (IsActif = '1' OR IsActif = '2')
            LIMIT 1
        ");
        return $qry->row();
    }

    function GetById_preview($id = 0)
    {
        $qry = $this->db->query("
            SELECT
            article.*,
            villes.Nom AS ville,
            agenda_categ.category,
            agenda_categ.agenda_categid,
            agenda_subcateg.subcateg

            FROM
                article
            LEFT OUTER JOIN villes ON villes.IdVille = article.IdVille_localisation
            INNER JOIN agenda_categ ON agenda_categ.agenda_categid = article.article_categid
            LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = article.article_subcategid

            WHERE id = '" . $id . "'
            LIMIT 1
        ");
        return $qry->row();
    }

    function Increment_accesscount($id = 0)
    {
        $current_agenda = $this->getById($id);
        $last_accesscount_nb = intval($current_agenda->accesscount);
        $last_accesscount_nb_total = $last_accesscount_nb + 1;
        $zSql = "UPDATE article SET accesscount='" . $last_accesscount_nb_total . "' WHERE id=" . $id;
        $this->db->query($zSql);
    }

    function GetByIdCommercantLimit($IdCommercant = 0, $_limitstart = 0, $_limitend = 10000000,$_iCategorieId = 0, $inputQuand = "0")
    {

        $query = "
            SELECT
                article_datetime.id as datetime_id, 
                article_datetime.date_debut as datetime_debut, 
                article_datetime.date_fin as datetime_fin, 
                article_datetime.heure_debut as datetime_heure_debut, 
                article.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_subcateg.subcateg
            FROM
                article
                LEFT OUTER JOIN villes ON villes.IdVille = article.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = article.article_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = article.article_subcategid
                LEFT OUTER JOIN article_datetime ON article_datetime.article_id = article.id
            WHERE
                article.IdCommercant = '" . $IdCommercant . "'
            AND
                article.IsActif = '1' OR article.IsActif = '2'
            ";

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

            if ($inputQuand == "01") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        }

        if (isset($_iCategorieId) && $_iCategorieId != 0) {
            if (is_array($_iCategorieId)) $_iCategorieId = $_iCategorieId[0];
            $query .= " AND article.article_categid = '" . $_iCategorieId . "' ";
        }

        $query .= "    
            group by article_datetime.id, article.id
            ORDER BY article.order_partner, article_datetime.date_debut, article.date_debut ASC
            LIMIT " . $_limitstart . "," . $_limitend . "
        ";
        log_message('error', 'partner_article_list_filter_00 : ' . $query);
        $qry = $this->db->query($query);
        if ($qry->num_rows() > 0) {
            return $qry->result();
        }
    }


    function GetArticleNbByMonth($month, $_iCommercantId)
    {
        $date_of_day = date("Y-m-d");
        $time_date = strtotime($date_of_day);
        $next_sunday = strtotime('next sunday, 12pm', $time_date);
        $last_sunday = strtotime('last sunday, 12pm', $time_date);
        $next_saturday = strtotime('next saturday, 11:59am', $time_date);
        $next_monday = strtotime('next monday, 11:59am', $time_date);
        $format_date = 'Y-m-d';
        $next_sunday_day = date($format_date, $next_sunday);
        $last_sunday_day = date($format_date, $last_sunday);
        $next_saturday_day = date($format_date, $next_saturday);
        $next_monday_day = date($format_date, $next_monday);
        $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
        $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
        $first_day_month = date('Y-m-01', strtotime($date_of_day));
        $last_day_month = date('Y-m-t', strtotime($date_of_day));
        $first_day_user_month = date('Y-' . $month . '-01', $time_date);
        $last_day_user_month = date('Y-' . $month . '-t', $time_date);

        $zSqlListePartenaire = "
            SELECT
                article.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_subcateg.subcateg,
                commercants.NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                article
                LEFT OUTER JOIN villes ON villes.IdVille = article.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = article.article_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = article.article_subcategid
                INNER JOIN commercants ON commercants.IdCommercant = article.IdCommercant
                LEFT OUTER JOIN article_datetime ON article_datetime.article_id = article.id
            WHERE
                (article.IsActif = '1' OR article.IsActif = '2') ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlListePartenaire .= " AND article.IdVille_localisation = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlListePartenaire .= " AND article.IdVille_localisation = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlListePartenaire .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlListePartenaire .= " article.IdVille_localisation = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListePartenaire .= " OR ";
            }
            $zSqlListePartenaire .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlListePartenaire .= " AND article.IdVille_localisation IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        if ($_iCommercantId != "0" && $_iCommercantId != "" && $_iCommercantId != NULL) {
            $zSqlListePartenaire .= "
                AND article.IdCommercant = '" . $_iCommercantId . "'
                ";
        }

        if ($month != "0" && $month != "" && $month != NULL) {
            if ($month == "101") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $date_of_day . "' AND article_datetime.date_fin <= '" . $date_of_day . "' )";
            if ($month == "202") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "' )";
            if ($month == "303") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "' )";
            if ($month == "404") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "' )";
            if ($month == "505") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "' )";

            if ($month == "01") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($month == "02") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($month == "03") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($month == "04") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($month == "05") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($month == "06") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($month == "07") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($month == "08") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($month == "09") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($month == "10") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($month == "11") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($month == "12") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
        }
        //////$this->firephp->log($zSqlListePartenaire, 'date_by_month_sql');
        //log_message('error', 'list_by_month_partner_article : '.$zSqlListePartenaire);
        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }


    function GetByIdCommercant_articleActif($IdCommercant = 0)
    {
        $sql_qr = "
            SELECT *
            FROM
                article
            WHERE IdCommercant = '" . $IdCommercant . "'
            AND IsActif = '1'
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sql_qr .= " AND article.IdVille_localisation = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sql_qr .= " AND article.IdVille_localisation = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sql_qr .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sql_qr .= " article.IdVille_localisation = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sql_qr .= " OR ";
            }
            $sql_qr .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sql_qr .= " AND article.IdVille_localisation IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        $sql_qr .= "
            ORDER BY order_partner ASC
        ";

        $qry = $this->db->query($sql_qr);
        if ($qry->num_rows() > 0) {
            return $qry->result();
        }
    }


    function GetArticleSubCateg_by_IdCateg($_iCategorieId)
    {
        $qur = "SELECT
            agenda_subcateg.agenda_subcategid,
            agenda_subcateg.subcateg,
            COUNT(article.id) as nb_article
            FROM
            article
            INNER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = article.article_subcategid
            WHERE
            (article.IsActif = 1 OR article.IsActif = 2)
            ";
        if (isset($_iCategorieId) && $_iCategorieId != "" && $_iCategorieId != '0')
            $qur .= "
            AND agenda_subcateg.agenda_categid = '" . $_iCategorieId . "'
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $qur .= " AND article.IdVille_localisation = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $qur .= " AND article.IdVille_localisation = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $qur .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $qur .= " article.IdVille_localisation = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $qur .= " OR ";
            }
            $qur .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $qur .= " AND article.IdVille_localisation IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        $qur .= "
            GROUP BY
            agenda_subcateg.agenda_subcategid";

        $qryCategorie = $this->db->query($qur);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetArticleSubCateg_by_IdCateg_IdVille($_iCategorieId, $_IdVille)
    {
        $qur = "SELECT
            agenda_subcateg.agenda_subcategid,
            agenda_subcateg.subcateg,
            COUNT(article.id) as nb_article
            FROM
            article
            INNER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = article.article_subcategid
            WHERE
            (article.IsActif = 1 OR article.IsActif = 2)
            ";
        if (isset($_iCategorieId) && $_iCategorieId != "" && $_iCategorieId != '0')
            $qur .= "
            AND agenda_subcateg.agenda_categid = '" . $_iCategorieId . "'
            ";
        if (isset($_IdVille) && $_IdVille != "" && $_IdVille != '0')
            $qur .= "
            AND article.IdVille_localisation = '" . $_IdVille . "'
            ";
        $qur .= "
            GROUP BY
            agenda_subcateg.agenda_subcategid";

        ////$this->firephp->log($qur, 'qur');

        $qryCategorie = $this->db->query($qur);

        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function file_manager_update($_iArticleId = '0', $_iUserId = '0', $_iField = 'photo1', $_iValue = '')
    {
        $zSql = "UPDATE article SET `" . $_iField . "` = '" . $_iValue . "' WHERE id = " . $_iArticleId . " AND `IdUsers_ionauth` = '" . $_iUserId . "' ";
        $zQuery = $this->db->query($zSql);
    }

    function getByIdField($id = 0, $iField = 'photo1')
    {
        $Sql = "select " . $iField . " from article where id =" . $id;
        $Query = $this->db->query($Sql);
        $result = $Query->row();
        if ($iField == 'photo1') return $result->photo1;
        if ($iField == 'photo2') return $result->photo2;
        if ($iField == 'photo3') return $result->photo3;
        if ($iField == 'photo4') return $result->photo4;
        if ($iField == 'photo5') return $result->photo5;
    }


    function getAlldeposantArticle($_iCommercantId = "0")
    {

        $zSqlAlldeposantAgenda = "SELECT
            commercants.NomSociete,
            commercants.IdCommercant
            FROM
            article
            INNER JOIN commercants ON article.IdCommercant = commercants.IdCommercant
            WHERE
            (article.IsActif = '1' OR article.IsActif = '2')";

        if ($_iCommercantId != "0" && $_iCommercantId != NULL && $_iCommercantId != "" && $_iCommercantId != 0) {
            $zSqlAlldeposantAgenda .= " AND article.IdCommercant = '" . $_iCommercantId . "'";
        }

        $zSqlAlldeposantAgenda .= " GROUP BY
            article.IdCommercant";

        $zQueryAlldeposantAgenda = $this->db->query($zSqlAlldeposantAgenda);
        return $zQueryAlldeposantAgenda->result();
    }
    function listeArticleRecherche_article_perso_liste_categ($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0", $referencement_article = 1, $agenda_article_type_id = 1)
    {

        $zSqlListePartenaire = "
            SELECT
                COUNT(article_datetime.id) as nb_agenda, 
                article_datetime.date_debut as datetime_debut, 
                article_datetime.date_fin as datetime_fin, 
                article_datetime.heure_debut as datetime_heure_debut,
                article.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_categ.agenda_categid,
                agenda_subcateg.subcateg,
                commercants.NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                article
                LEFT OUTER JOIN villes ON villes.IdVille = article.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = article.article_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = article.article_subcategid
                INNER JOIN commercants ON commercants.IdCommercant = article.IdCommercant
                LEFT OUTER JOIN article_datetime ON article_datetime.article_id = article.id

            WHERE
                ( 
                (article.IsActif = '1' OR article.IsActif = '2') 
                AND commercants.IsActif = 1 
                ";
        if ($referencement_article==1)   $zSqlListePartenaire .= " AND commercants.referencement_article = ".$referencement_article." ";

        $zSqlListePartenaire .= "
                AND (article.id <> '0' OR article.id <> NULL OR article.id <> 0) 

        ";

        if (is_array($inputIdCommercant)) {
            $zSql_all_icommercant_request = "";
            $zSql_all_icommercant_request .= " (";
            if (sizeof($inputIdCommercant) == 1) {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
            } else {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                for ($i_iCommercantId = 1; $i_iCommercantId < count($inputIdCommercant); $i_iCommercantId++) {
                    $zSql_all_icommercant_request .= " OR commercants.IdCommercant = '" . $inputIdCommercant[$i_iCommercantId] . "' ";
                }
            }
            $zSql_all_icommercant_request .= " ) ";
            //$zSqlListePartenaire .= $zSql_all_icommercant_request;
        } else if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }

        if ($IdUsers_ionauth != "0" && $IdUsers_ionauth != "" && $IdUsers_ionauth != NULL && $IdUsers_ionauth != 0) {
            $zSqlListePartenaire .= " AND article.IdUsers_ionauth = '" . $IdUsers_ionauth . "'";
        }

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);


            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $date_of_day . "' AND article_datetime.date_fin <= '" . $date_of_day . "' ) OR ( article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "') OR (article_datetime.date_fin >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "') OR (article_datetime.date_fin >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') OR (article_datetime.date_fin >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "') OR (article_datetime.date_fin >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "' AND article_datetime.date_fin >= '" . $date_of_day . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";

        }

        if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null && $_iVilleId != "0") {
            if (is_array($_iVilleId)) {
                $zSql_all_iville_request = "";
                $zSql_all_iville_request .= " (";
                if (sizeof($_iVilleId) == 1) {
                    if ($_iVilleId[0] != 0 && $_iVilleId[0] != "" && $_iVilleId[0] != null && $_iVilleId[0] != "0") {
                        $zSql_all_iville_request .= " article.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    } else {
                        $zSql_all_iville_request .= " 0=0 ";
                    }
                } else {
                    $zSql_all_iville_request .= " article.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    for ($i_iVilleId = 1; $i_iVilleId < count($_iVilleId); $i_iVilleId++) {
                        $zSql_all_iville_request .= " OR article.IdVille_localisation = '" . $_iVilleId[$i_iVilleId] . "' ";
                    }
                }
                $zSql_all_iville_request .= " ) ";
                ////$this->firephp->log($zSql_all_iville_request, 'zSql_all_iville_request');
                //$zSqlListePartenaire .= $zSql_all_iville_request;
            }
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " article.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            $zSqlListePartenaire .= " AND article_datetime.date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            $zSqlListePartenaire .= " AND article_datetime.date_fin = '" . $inputDatefin . "'";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " article.article_categid = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " article.article_categid = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR article.article_categid = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }

        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " article.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " article.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR article.article_subcategid = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }

        /*if($zSql_all_categ_request != "" && $zSql_all_subcateg_request == "") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;
        else if($zSql_all_categ_request == "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ".$zSql_all_subcateg_request;
        else if($zSql_all_categ_request != "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ( ".$zSql_all_categ_request." OR ".$zSql_all_subcateg_request." ) ";*/

        //if ($zSql_all_categ_request!="") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;


        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);

        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(article.nom_societe ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_categ.category ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_subcateg.subcateg ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.organisateur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.adresse_localisation_diffuseur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }


        $zSqlListePartenaire .= " ) ";

        if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") || (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) {
            $zSqlListePartenaire .= " AND ( ";

            if (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") $zSqlListePartenaire .= $zSql_all_icommercant_request;
            if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") && (isset($zSql_all_iville_request) && $zSql_all_iville_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_iville_request;
            else if (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") $zSqlListePartenaire .= $zSql_all_iville_request;
            if (((isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "")) && (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
            else if (isset($zSql_all_categ_request) && $zSql_all_categ_request != "") $zSqlListePartenaire .= $zSql_all_categ_request;

            $zSqlListePartenaire .= " ) ";
        }

        $zSqlListePartenaire .= " AND (article.IsActif = '1' OR article.IsActif = '2') ";


        //DIFERENCIATE ARTICLE AND REVUE DE PRESSE  > article = 1 / revue = 3
       // if(isset($agenda_article_type_id) && is_numeric($agenda_article_type_id))
           // $zSqlListePartenaire .= " AND article.agenda_article_type_id = ".$agenda_article_type_id." ";


        $zSqlListePartenaire .= " group by article.article_categid ";

        if ($iOrderBy == '1') $iOrderBy_value = " order by article.last_update desc ";
        else if ($iOrderBy == '2') $iOrderBy_value = " order by article.accesscount desc ";
        else $iOrderBy_value = " order by agenda_categ.category asc ";

        $zSqlListePartenaire .= $iOrderBy_value;

        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $_limitend;
        //echo $zSqlListePartenaire;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListePartenaire');
        //die("<br/>STOP");

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }

public function save_revue_data($field){

        $res= $this->db->insert("article",$field);
        if ($res){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

public function test_revue_exist($titre){

        $this->db->where('nom_manifestation',$titre);
        $test=$this->db->get('article');
        if ($test->num_rows()>0){
            return 1;
        }else{return 0;}
}
public function save_datetime_revue($field_datetime){
      $save_datetime= $this->db->insert("article_datetime",$field_datetime);
      if ($save_datetime){
          return 'ok';
      }else{return "no";}
}
public function get_photo1com($IdCommercant){
        $this->db->select('Photo1');
        $this->db->where('IdCommercant',$IdCommercant);
        $get=$this->db->get('commercants');
        return $get->row();
}
public function get_aboutcom($IdCommercant){
        $this->db->where('IdCommercant',$IdCommercant);
        $res=$this->db->get('commercants');
        return $res->row();
}
public function save_organiser($organiser){
       $save= $this->db->insert('article_organiser',$organiser);
       if ($save){
           return $this->db->insert_id();
       }else{
           return 0;
       }

}
public function get_location($location_adress){
        $this->db->where("location_address",$location_adress);
        $res=$this->db->get('locations');
        if ($res->num_rows()==1){
            return$res->row();
        }elseif ($res->num_rows() >1 ){
            return $res->result();
        }else{ return 0;}

}
public function save_location($location_datas){

       $save = $this->db->insert('locations',$location_datas);
        if ($save){
            return $this->db->insert_id();
        }else{
            return "1";
        }
}

    function getarticle_join_ville ($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0", $referencement_article = 1,$ville_folter)
    {
        $agenda_article_type_id = 1;
        $zSqlListePartenaire = "
            SELECT
                article_datetime.id as datetime_id, 
                article_datetime.date_debut as datetime_debut, 
                article_datetime.date_fin as datetime_fin, 
                article_datetime.heure_debut as datetime_heure_debut,
                article.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_subcateg.subcateg,
                commercants.NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                article
                LEFT OUTER JOIN villes ON villes.IdVille = article.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = article.article_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = article.article_subcategid
                INNER JOIN commercants ON commercants.IdCommercant = article.IdCommercant
                LEFT OUTER JOIN article_datetime ON article_datetime.article_id = article.id

            WHERE
                ( 
                (article.IsActif = '1' OR article.IsActif = '2') 
                AND commercants.IsActif = 1 
                ";
        if ($referencement_article==1)   $zSqlListePartenaire .= " AND commercants.referencement_article = ".$referencement_article." ";

        $zSqlListePartenaire .= "
                AND (article.id <> '0' OR article.id <> NULL OR article.id <> 0) 

        ";

        if (is_array($inputIdCommercant)) {
            $zSql_all_icommercant_request = "";
            $zSql_all_icommercant_request .= " (";
            if (sizeof($inputIdCommercant) == 1) {
                if ($inputIdCommercant[0] != "0" && $inputIdCommercant[0] != "" && $inputIdCommercant[0] != NULL && $inputIdCommercant[0] != 0) {
                    $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                } else {
                    $zSql_all_icommercant_request .= " 0=0 ";
                }
            } else {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                for ($i_iCommercantId = 1; $i_iCommercantId < count($inputIdCommercant); $i_iCommercantId++) {
                    $zSql_all_icommercant_request .= " OR commercants.IdCommercant = '" . $inputIdCommercant[$i_iCommercantId] . "' ";
                }
            }
            $zSql_all_icommercant_request .= " ) ";
            //$zSqlListePartenaire .= $zSql_all_icommercant_request;
        } else if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }

        if ($IdUsers_ionauth != "0" && $IdUsers_ionauth != "" && $IdUsers_ionauth != NULL && $IdUsers_ionauth != 0) {
            $zSqlListePartenaire .= " AND article.IdUsers_ionauth = '" . $IdUsers_ionauth . "'";
        }

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

            /*
            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $date_of_day . "' AND article_datetime.date_fin <= '" . $date_of_day . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            */
            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $date_of_day . "' AND article_datetime.date_fin <= '" . $date_of_day . "' ) OR ( article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "') OR (article_datetime.date_fin >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "') OR (article_datetime.date_fin >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') OR (article_datetime.date_fin >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "') OR (article_datetime.date_fin >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "' AND article_datetime.date_fin >= '" . $date_of_day . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";

        }

        if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null && $_iVilleId != "0") {
            if (is_array($_iVilleId)) {
                $zSql_all_iville_request = "";
                $zSql_all_iville_request .= " (";
                if (sizeof($_iVilleId) == 1) {
                    if ($_iVilleId[0] != 0 && $_iVilleId[0] != "" && $_iVilleId[0] != null && $_iVilleId[0] != "0") {
                        $zSql_all_iville_request .= " article.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    } else {
                        $zSql_all_iville_request .= " 0=0 ";
                    }
                } else {
                    $zSql_all_iville_request .= " article.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    for ($i_iVilleId = 1; $i_iVilleId < count($_iVilleId); $i_iVilleId++) {
                        $zSql_all_iville_request .= " OR article.IdVille_localisation = '" . $_iVilleId[$i_iVilleId] . "' ";
                    }
                }
                $zSql_all_iville_request .= " ) ";
                ////$this->firephp->log($zSql_all_iville_request, 'zSql_all_iville_request');
                //$zSqlListePartenaire .= $zSql_all_iville_request;
            }
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " article.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            $zSqlListePartenaire .= " AND article_datetime.date_debut = '" . $inputDatedebut . "'";
        }
        if ($ville_folter != "0" && $ville_folter != "" && $ville_folter != null) {
            $zSqlListePartenaire .= " AND article.IdVille_localisation = '" . $ville_folter . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            $zSqlListePartenaire .= " AND article_datetime.date_fin = '" . $inputDatefin . "'";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " article.article_categid = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " article.article_categid = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR article.article_categid = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }

        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " article.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " article.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR article.article_subcategid = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }

        /*if($zSql_all_categ_request != "" && $zSql_all_subcateg_request == "") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;
        else if($zSql_all_categ_request == "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ".$zSql_all_subcateg_request;
        else if($zSql_all_categ_request != "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ( ".$zSql_all_categ_request." OR ".$zSql_all_subcateg_request." ) ";*/

        //if ($zSql_all_categ_request!="") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;


        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);
        //var_dump($SearchedWordsString);
        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(article.nom_societe ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_categ.category ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_subcateg.subcateg ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.organisateur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.adresse_localisation_diffuseur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }


        $zSqlListePartenaire .= " ) ";

        if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") || (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) {
            $zSqlListePartenaire .= " AND ( ";

            if (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") $zSqlListePartenaire .= $zSql_all_icommercant_request;
            if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") && (isset($zSql_all_iville_request) && $zSql_all_iville_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_iville_request;
            else if (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") $zSqlListePartenaire .= $zSql_all_iville_request;
            if (((isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "")) && (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
            else if (isset($zSql_all_categ_request) && $zSql_all_categ_request != "") $zSqlListePartenaire .= $zSql_all_categ_request;

            $zSqlListePartenaire .= " ) ";
        }


        //DIFERENCIATE ARTICLE AND REVUE DE PRESSE  > article = 1 / revue = 3
        if(isset($agenda_article_type_id) && is_numeric($agenda_article_type_id))
            //$zSqlListePartenaire .= " AND article.agenda_article_type_id = ".$agenda_article_type_id." ";

            $zSqlListePartenaire .= " AND (article.IsActif = '1' OR article.IsActif = '2') ";

//        $zSqlListePartenaire .= " group by article_datetime.id ";
            $zSqlListePartenaire .= " group by villes.IdVille ";

            $zSqlListePartenaire .= " order by villes.Nom asc ";

//        if ($iOrderBy == '1') $iOrderBy_value = " order by article.last_update desc ";
//        else if ($iOrderBy == '2') $iOrderBy_value = " order by article.accesscount desc ";
//        else $iOrderBy_value = " order by article_datetime.date_debut,article_datetime.date_fin, article.date_debut asc ";
//
//        $zSqlListePartenaire .= $iOrderBy_value;

        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $_limitend;
        //echo $zSqlListePartenaire;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListePartenaire');
        //die("<br/>STOP");

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }
    public function get_count_filter_article($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0", $referencement_article = 1,$tiville)
    {
        $agenda_article_type_id = 1;
        $zSqlListePartenaire = "
            SELECT
                article_datetime.id as datetime_id, 
                article_datetime.date_debut as datetime_debut, 
                article_datetime.date_fin as datetime_fin, 
                article_datetime.heure_debut as datetime_heure_debut,
                article.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_subcateg.subcateg,
                commercants.NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                article
                LEFT OUTER JOIN villes ON villes.IdVille = article.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = article.article_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = article.article_subcategid
                INNER JOIN commercants ON commercants.IdCommercant = article.IdCommercant
                LEFT OUTER JOIN article_datetime ON article_datetime.article_id = article.id

            WHERE
                ( 
                (article.IsActif = '1' OR article.IsActif = '2') 
                AND commercants.IsActif = 1 
                ";
        if ($referencement_article==1)   $zSqlListePartenaire .= " AND commercants.referencement_article = ".$referencement_article." ";

        $zSqlListePartenaire .= "
                AND (article.id <> '0' OR article.id <> NULL OR article.id <> 0) 

        ";

        if (is_array($inputIdCommercant)) {
            $zSql_all_icommercant_request = "";
            $zSql_all_icommercant_request .= " (";
            if (sizeof($inputIdCommercant) == 1) {
                if ($inputIdCommercant[0] != "0" && $inputIdCommercant[0] != "" && $inputIdCommercant[0] != NULL && $inputIdCommercant[0] != 0) {
                    $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                } else {
                    $zSql_all_icommercant_request .= " 0=0 ";
                }
            } else {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                for ($i_iCommercantId = 1; $i_iCommercantId < count($inputIdCommercant); $i_iCommercantId++) {
                    $zSql_all_icommercant_request .= " OR commercants.IdCommercant = '" . $inputIdCommercant[$i_iCommercantId] . "' ";
                }
            }
            $zSql_all_icommercant_request .= " ) ";
            //$zSqlListePartenaire .= $zSql_all_icommercant_request;
        } else if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }

        if ($IdUsers_ionauth != "0" && $IdUsers_ionauth != "" && $IdUsers_ionauth != NULL && $IdUsers_ionauth != 0) {
            $zSqlListePartenaire .= " AND article.IdUsers_ionauth = '" . $IdUsers_ionauth . "'";
        }
        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

            /*
            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $date_of_day . "' AND article_datetime.date_fin <= '" . $date_of_day . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            */
            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $date_of_day . "' AND article_datetime.date_fin <= '" . $date_of_day . "' ) OR ( article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "') OR (article_datetime.date_fin >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "') OR (article_datetime.date_fin >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') OR (article_datetime.date_fin >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "') OR (article_datetime.date_fin >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "' AND article_datetime.date_fin >= '" . $date_of_day . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";

        }

        if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null && $_iVilleId != "0") {
            if (is_array($_iVilleId)) {
                $zSql_all_iville_request = "";
                $zSql_all_iville_request .= " (";
                if (sizeof($_iVilleId) == 1) {
                    if ($_iVilleId[0] != 0 && $_iVilleId[0] != "" && $_iVilleId[0] != null && $_iVilleId[0] != "0") {
                        $zSql_all_iville_request .= " article.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    } else {
                        $zSql_all_iville_request .= " 0=0 ";
                    }
                } else {
                    $zSql_all_iville_request .= " article.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    for ($i_iVilleId = 1; $i_iVilleId < count($_iVilleId); $i_iVilleId++) {
                        $zSql_all_iville_request .= " OR article.IdVille_localisation = '" . $_iVilleId[$i_iVilleId] . "' ";
                    }
                }
                $zSql_all_iville_request .= " ) ";
                ////$this->firephp->log($zSql_all_iville_request, 'zSql_all_iville_request');
                //$zSqlListePartenaire .= $zSql_all_iville_request;
            }
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " article.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            $zSqlListePartenaire .= " AND article_datetime.date_debut = '" . $inputDatedebut . "'";
        }
        if ($tiville != "0" && $tiville != "" && $tiville != null) {
            $zSqlListePartenaire .= " AND article.IdVille_localisation= '" . $tiville . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            $zSqlListePartenaire .= " AND article_datetime.date_fin = '" . $inputDatefin . "'";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " article.article_categid = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " article.article_categid = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR article.article_categid = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }

        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " article.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " article.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR article.article_subcategid = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }

        /*if($zSql_all_categ_request != "" && $zSql_all_subcateg_request == "") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;
        else if($zSql_all_categ_request == "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ".$zSql_all_subcateg_request;
        else if($zSql_all_categ_request != "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ( ".$zSql_all_categ_request." OR ".$zSql_all_subcateg_request." ) ";*/

        //if ($zSql_all_categ_request!="") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;


        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);
        //var_dump($SearchedWordsString);
        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(article.nom_societe ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_categ.category ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_subcateg.subcateg ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.organisateur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(article.adresse_localisation_diffuseur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }


        $zSqlListePartenaire .= " ) ";

        if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") || (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) {
            $zSqlListePartenaire .= " AND ( ";

            if (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") $zSqlListePartenaire .= $zSql_all_icommercant_request;
            if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") && (isset($zSql_all_iville_request) && $zSql_all_iville_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_iville_request;
            else if (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") $zSqlListePartenaire .= $zSql_all_iville_request;
            if (((isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "")) && (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
            else if (isset($zSql_all_categ_request) && $zSql_all_categ_request != "") $zSqlListePartenaire .= $zSql_all_categ_request;

            $zSqlListePartenaire .= " ) ";
        }


        //DIFERENCIATE ARTICLE AND REVUE DE PRESSE  > article = 1 / revue = 3
//        if(isset($agenda_article_type_id) && is_numeric($agenda_article_type_id))
        //$zSqlListePartenaire .= " AND article.agenda_article_type_id = ".$agenda_article_type_id." ";

        $zSqlListePartenaire .= " AND (article.IsActif = '1' OR article.IsActif = '2') ";

        $zSqlListePartenaire .= " group by article_datetime.id ";

        if ($iOrderBy == '1') $iOrderBy_value = " order by article.last_update desc ";
        else if ($iOrderBy == '2') $iOrderBy_value = " order by article.accesscount desc ";
        else $iOrderBy_value = " order by article_datetime.date_debut,article_datetime.date_fin, article.date_debut asc ";

        $zSqlListePartenaire .= $iOrderBy_value;

        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $_limitend;
        //echo $zSqlListePartenaire;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListePartenaire');
        //die("<br/>STOP");
        //var_dump($zSqlListePartenaire);
        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        echo count($zQueryListeBonPlan->result());
    }
public function listeArticleRecherche_filtre_categ_perso($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0", $referencement_article = 1,$tiville)
{
    $agenda_article_type_id = 1;
    $zSqlListePartenaire = "
            SELECT
                article_datetime.id as datetime_id, 
                article_datetime.date_debut as datetime_debut, 
                article_datetime.date_fin as datetime_fin, 
                article_datetime.heure_debut as datetime_heure_debut,
                article.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_subcateg.subcateg,
                commercants.NomSociete
                ";

    $zSqlListePartenaire .= "
            FROM
                article
                LEFT OUTER JOIN villes ON villes.IdVille = article.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = article.article_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = article.article_subcategid
                INNER JOIN commercants ON commercants.IdCommercant = article.IdCommercant
                LEFT OUTER JOIN article_datetime ON article_datetime.article_id = article.id

            WHERE
                ( 
                (article.IsActif = '1' OR article.IsActif = '2') 
                AND commercants.IsActif = 1 
                ";
    if ($referencement_article==1)   $zSqlListePartenaire .= " AND commercants.referencement_article = ".$referencement_article." ";

    $zSqlListePartenaire .= "
                AND (article.id <> '0' OR article.id <> NULL OR article.id <> 0) 

        ";

    if (is_array($inputIdCommercant)) {
        $zSql_all_icommercant_request = "";
        $zSql_all_icommercant_request .= " (";
        if (sizeof($inputIdCommercant) == 1) {
            if ($inputIdCommercant[0] != "0" && $inputIdCommercant[0] != "" && $inputIdCommercant[0] != NULL && $inputIdCommercant[0] != 0) {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
            } else {
                $zSql_all_icommercant_request .= " 0=0 ";
            }
        } else {
            $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
            for ($i_iCommercantId = 1; $i_iCommercantId < count($inputIdCommercant); $i_iCommercantId++) {
                $zSql_all_icommercant_request .= " OR commercants.IdCommercant = '" . $inputIdCommercant[$i_iCommercantId] . "' ";
            }
        }
        $zSql_all_icommercant_request .= " ) ";
        //$zSqlListePartenaire .= $zSql_all_icommercant_request;
    } else if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
        $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
    }

    if ($IdUsers_ionauth != "0" && $IdUsers_ionauth != "" && $IdUsers_ionauth != NULL && $IdUsers_ionauth != 0) {
        $zSqlListePartenaire .= " AND article.IdUsers_ionauth = '" . $IdUsers_ionauth . "'";
    }
    if ($inputQuand != "0") {
        $date_of_day = date("Y-m-d");
        $time_date = strtotime($date_of_day);
        $next_sunday = strtotime('next sunday, 12pm', $time_date);
        $last_sunday = strtotime('last sunday, 12pm', $time_date);
        $next_saturday = strtotime('next saturday, 11:59am', $time_date);
        $next_monday = strtotime('next monday, 11:59am', $time_date);
        $format_date = 'Y-m-d';
        $next_sunday_day = date($format_date, $next_sunday);
        $last_sunday_day = date($format_date, $last_sunday);
        $next_saturday_day = date($format_date, $next_saturday);
        $next_monday_day = date($format_date, $next_monday);
        $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
        $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
        $first_day_month = date('Y-m-01', strtotime($date_of_day));
        $last_day_month = date('Y-m-t', strtotime($date_of_day));
        $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
        $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

        /*
        if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $date_of_day . "' AND article_datetime.date_fin <= '" . $date_of_day . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $date_of_day . "' ) )";
        if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "') )";
        if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "') )";
        if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') )";
        if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "') )";

        if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        */
        if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $date_of_day . "' AND article_datetime.date_fin <= '" . $date_of_day . "' ) OR ( article_datetime.date_debut IS NULL AND article_datetime.date_fin >= '" . $date_of_day . "' ) )";
        if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "') OR (article_datetime.date_fin >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "') )";
        if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "') OR (article_datetime.date_fin >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "') )";
        if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') OR (article_datetime.date_fin >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') )";
        if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "') OR (article_datetime.date_fin >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "' AND article_datetime.date_fin >= '" . $date_of_day . "') )";

        if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
        if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";

    }

    if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null && $_iVilleId != "0") {
        if (is_array($_iVilleId)) {
            $zSql_all_iville_request = "";
            $zSql_all_iville_request .= " (";
            if (sizeof($_iVilleId) == 1) {
                if ($_iVilleId[0] != 0 && $_iVilleId[0] != "" && $_iVilleId[0] != null && $_iVilleId[0] != "0") {
                    $zSql_all_iville_request .= " article.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                } else {
                    $zSql_all_iville_request .= " 0=0 ";
                }
            } else {
                $zSql_all_iville_request .= " article.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                for ($i_iVilleId = 1; $i_iVilleId < count($_iVilleId); $i_iVilleId++) {
                    $zSql_all_iville_request .= " OR article.IdVille_localisation = '" . $_iVilleId[$i_iVilleId] . "' ";
                }
            }
            $zSql_all_iville_request .= " ) ";
            ////$this->firephp->log($zSql_all_iville_request, 'zSql_all_iville_request');
            //$zSqlListePartenaire .= $zSql_all_iville_request;
        }
    } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
        # code...
        model_load_model("mdldepartement");
        $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
        if (count($villeByIdDepartement) > 0) {
            $zSqlListePartenaire .= " AND (";
            $iii_villedep = 1;
            foreach ($villeByIdDepartement as $key_villeDep) {
                # code...
                $zSqlListePartenaire .= " article.IdVille_localisation =" . $key_villeDep->IdVille;
                if ($iii_villedep != count($villeByIdDepartement)) {
                    $zSqlListePartenaire .= " OR ";
                }
                $iii_villedep++;
            }
            $zSqlListePartenaire .= ")";
        }
    }


    if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
        $zSqlListePartenaire .= " AND article_datetime.date_debut = '" . $inputDatedebut . "'";
    }
    if ($tiville != "0" && $tiville != "" && $tiville != null) {
        $zSqlListePartenaire .= " AND article.IdVille_localisation= '" . $tiville . "'";
    }

    if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
        $zSqlListePartenaire .= " AND article_datetime.date_fin = '" . $inputDatefin . "'";
    }

    $zSql_all_categ_request = "";
    $zSql_all_subcateg_request = "";

    if (isset($_iCategorieId) && $_iCategorieId != "" && $_iCategorieId != 0) {
        $zSql_all_categ_request .= " (";

            $zSql_all_categ_request .= " article.article_categid = '" . $_iCategorieId . "' ";

        $zSql_all_categ_request .= " ) ";
    }

    if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
        $zSql_all_subcateg_request .= " (";
        if (sizeof($_iSousCategorieId) == 1) {
            $zSql_all_subcateg_request .= " article.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
        } else {
            $zSql_all_subcateg_request .= " article.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
            for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                $zSql_all_subcateg_request .= " OR article.article_subcategid = '" . $_iSousCategorieId[$ii_rand] . "' ";
            }
        }
        $zSql_all_subcateg_request .= " ) ";
    }


    $SearchedWordsString = str_replace("+", " ", $_zMotCle);
    $SearchedWordsString = trim($SearchedWordsString);
    $SearchedWords = explode(" ", $SearchedWordsString);


    for ($i = 0; $i < sizeof($SearchedWords); $i++) {
        $Search = $SearchedWords[$i];
        $Search = str_replace(" ", "", $Search);
        if ($Search != "") {
            if ($i == 0) {
                $zSqlListePartenaire .= "\n AND ";
            }
            $zSqlListePartenaire .= " ( \n" .
                " UPPER(article.nom_societe ) LIKE '%" . strtoupper($Search) . "%'\n" .
                " OR UPPER(article.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'\n" .
                " OR UPPER(article.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                " OR UPPER(agenda_categ.category ) LIKE '%" . strtoupper($Search) . "%'\n" .
                " OR UPPER(agenda_subcateg.subcateg ) LIKE '%" . strtoupper($Search) . "%'\n" .
                " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                " OR UPPER(article.adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                " OR UPPER(article.adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                " OR UPPER(article.organisateur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                " OR UPPER(article.adresse_localisation_diffuseur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                " )
                    ";
            if ($i != (sizeof($SearchedWords) - 1)) {
                $zSqlListePartenaire .= "\n OR ";
            }
        }
    }


    $zSqlListePartenaire .= " ) ";

    if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") || (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) {
        $zSqlListePartenaire .= " AND ( ";

        if (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") $zSqlListePartenaire .= $zSql_all_icommercant_request;
        if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") && (isset($zSql_all_iville_request) && $zSql_all_iville_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_iville_request;
        else if (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") $zSqlListePartenaire .= $zSql_all_iville_request;
        if (((isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "")) && (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
        else if (isset($zSql_all_categ_request) && $zSql_all_categ_request != "") $zSqlListePartenaire .= $zSql_all_categ_request;

        $zSqlListePartenaire .= " ) ";
    }


    //DIFERENCIATE ARTICLE AND REVUE DE PRESSE  > article = 1 / revue = 3
//        if(isset($agenda_article_type_id) && is_numeric($agenda_article_type_id))
    //$zSqlListePartenaire .= " AND article.agenda_article_type_id = ".$agenda_article_type_id." ";

    $zSqlListePartenaire .= " AND (article.IsActif = '1' OR article.IsActif = '2') ";

    $zSqlListePartenaire .= " group by article_datetime.id ";

    if ($iOrderBy == '1') $iOrderBy_value = " order by article.last_update desc ";
    else if ($iOrderBy == '2') $iOrderBy_value = " order by article.accesscount desc ";
    else $iOrderBy_value = " order by article_datetime.date_debut,article_datetime.date_fin, article.date_debut asc ";

    $zSqlListePartenaire .= $iOrderBy_value;

    $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;
    $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
    return $zQueryListeBonPlan->result();
}

public function getArticleTypeList($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $inputIdCommercant = "0"){

    $zSqlListePartenaire = "
        SELECT
            agenda_article_type.type,
            agenda_article_type.agenda_article_type_id,
            villes.IdVille,
            count(id) as nbarticle
            FROM
            article
            INNER JOIN villes ON villes.IdVille = article.IdVille_localisation
            INNER JOIN agenda_article_type ON article.agenda_article_type_id = agenda_article_type.agenda_article_type_id
            WHERE
            article.IsActif = 1
            
    ";

    if(isset($_iCategorieId) && $_iCategorieId != "" && $_iCategorieId != "0" && $_iCategorieId != 0 && $_iCategorieId != null){
        $zSqlListePartenaire .= " AND article.article_categid = '".$_iCategorieId."' ";
    }

    if(isset($_iVilleId) && $_iVilleId != "" && $_iVilleId != "0" && $_iVilleId != 0 && $_iVilleId != null){
        $zSqlListePartenaire .= " AND article.IdVille_localisation = '".$_iCategorieId."' ";
    }

    if(isset($_iDepartementId) && $_iDepartementId != "" && $_iDepartementId != "0" && $_iDepartementId != 0 && $_iDepartementId != null){
        $zSqlListePartenaire .= " AND article.ville_departement = '".$this->getDepartmentCodeById($_iDepartementId)->departement_code."' ";
    }

    if(isset($inputIdCommercant) && $inputIdCommercant != "" && $inputIdCommercant != "0" && $inputIdCommercant != 0 && $inputIdCommercant != null){
        $zSqlListePartenaire .= " AND article.IdCommercant = '".$inputIdCommercant."' ";
    }

        //LOCALDATA FILTRE
    $this_session_localdata =& get_instance();
    $this_session_localdata->load->library('session');
    $localdata_value = $this_session_localdata->session->userdata('localdata');
    $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
    $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
    $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
    $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
    if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
        $zSqlListePartenaire .= " AND article.IdVille_localisation = '2031' ";
    } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
        $zSqlListePartenaire .= " AND article.IdVille_localisation = '".$localdata_IdVille."' ";
    } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
        $zSqlListePartenaire .= " AND ( ";
        for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
            $zSqlListePartenaire .= " article.IdVille_localisation = '".$localdata_IdVille_all[$iiik]."' ";
            if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListePartenaire .= " OR ";
        }
        $zSqlListePartenaire .= " ) ";
    } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
        $zSqlListePartenaire .= " AND article.IdVille_localisation IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
    }
    //LOCALDATA FILTRE

        $zSqlListePartenaire .= "
            GROUP BY
            agenda_article_type.agenda_article_type_id
        ";

    $qryarticle = $this->db->query($zSqlListePartenaire);
    if ($qryarticle->num_rows() > 0) {
        return $qryarticle->result();
    }
}

function getDepartmentCodeById($_iDepartementId = 0){
    $Sql = "select * from departement where departement_id =" . $_iDepartementId . " LIMIT 1 ";
    $Query = $this->db->query($Sql);
    return $Query->row();
}
// function getAllarticletype(){
//     $Sql = "select * from article_type ";
//     $Query = $this->db->query($Sql);
//     return $Query->result();
// }
    function getAllarticletype(){
        $Sql = 'SELECT DISTINCT IF(POSITION(":" IN agenda_categ.category)!=0,SUBSTR(agenda_categ.category,1,(POSITION(":" IN agenda_categ.category)-2)),agenda_categ.category) AS category FROM `rss_source` INNER JOIN agenda_categ ON rss_source.agenda_categid = agenda_categ.agenda_categid;
        ';
        $Query = $this->db->query($Sql);
        return $Query->result();
    }
    function check_Isactif($IdCommercant=0){

        $sql = "
            SELECT *
            FROM
                article
            WHERE IdCommercant = '" . $IdCommercant . "'
            AND IsActif = '1'
            ";
        $Query = $this->db->query($sql);
        return $Query->result();
    }

}