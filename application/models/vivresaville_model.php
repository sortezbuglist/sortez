<?php

class vivresaville_model extends CI_Model{

    public function get_all_name_ville(){

        $qryAbonnements = $this->db->query("
        SELECT
        vivresaville_villes.id,
        vivresaville_villes.name_ville,
        vivresaville_villes.id_ville
        FROM
        vivresaville_villes
        
       ");
       if($qryAbonnements->num_rows() > 0) {
           return $qryAbonnements->result();
       }
    
    }

    // get ville by id
    public function get_name_ville($idVille){
        $qryAbonnements = $this->db->query("
            SELECT
                vivresaville_villes.id,
                vivresaville_villes.name_ville,
                vivresaville_villes.id_ville
            FROM
                vivresaville_villes
            WHERE
                vivresaville_villes.id_ville = ".$idVille."
        ");
       if($qryAbonnements->num_rows() > 0) {
           return $qryAbonnements->result();
       }
    }

    //liste 5 Agenda
    public function get_3_agenda($id_ville){
        $req = 'SELECT
            agenda.IdVille,
            agenda.id,
            agenda.photo1,
            agenda.adresse_localisation,
            agenda.description,
            agenda.telephone,
            agenda.nom_societe,
            agenda.nom_manifestation,
            agenda.date_debut,
            agenda.date_fin,
            villes.ville_nom,
            villes.ville_longitude_deg,
            villes.ville_latitude_deg,
            villes.CodePostal,
            villes.ville_canton,
            villes.Nom,
            villes.ville_arrondissement,
            villes.IdVille
            FROM
            agenda
            INNER JOIN villes ON agenda.IdVille = villes.IdVille
            WHERE villes.IdVille ="'.$id_ville.'" LIMIT 5';
        $Resreq = $this->db->query($req);
        return $Resreq->result();
    }
    //liste 5 annuaire
    public function get_3_annuaire($id_ville){
        $this->db->select('commercants.user_ionauth_id ,villes.Nom, commercants.NomSociete , commercants.photo1 , commercants.Caracteristiques , rubriques.Nom as rubriques , villes.ville_nom, villes.IdVille');
        $this->db->from('commercants');
        $this->db->join('villes', 'commercants.IdVille = villes.IdVille');
        $this->db->join('ass_commercants_rubriques', 'commercants.IdCommercant = ass_commercants_rubriques.IdCommercant');
        $this->db->join('rubriques', 'ass_commercants_rubriques.IdRubrique = rubriques.IdRubrique');
        $this->db->where('villes.IdVille', $id_ville);
        $this->db->where('commercants.IsActif = 1');
        $this->db->where('commercants.referencement_annuaire = 1');
        $this->db->LIMIT('5');
        $res = $this->db->get();
        return $res->result();
    }
    public function get_3_annonce($id_ville){
        $this->db->select('annonce.annonce_photo1 AS photo,
                    annonce.annonce_description_courte AS texte_courte,
                    annonce.annonce_prix_neuf AS prix_vente,
                    annonce.annonce_prix_solde AS prix_ancien,
                    annonce.annonce_etat AS etat,
                    commercants.NomSociete AS NomSociete,
                    villes.Nom AS ville,
                    villes.IdVille,
                    villes.CodePostal AS code,
                    commercants.Adresse1 AS quartier,
                    commercants.Adresse2 AS rue,
                    commercants.IdCommercant,
                    commercants.nom_url,
                    commercants.NomSociete,
                    commercants.user_ionauth_id,
                    sous_rubriques.Nom AS rubrique,
                    sous_rubriques.IdSousRubrique,
                    sous_rubriques.IdRubrique,
                    annonce.annonce_id,
                    annonce.annonce_description_longue,
                    annonce.annonce_photo1,
                    annonce.annonce_photo2,
                    annonce.annonce_photo3,
                    annonce.annonce_photo4,
                    annonce.module_paypal,
                    annonce.retrait_etablissement, 
                    annonce.livraison_domicile,
                    annonce.annonce_etat,
                    annonce.annonce_commercant_id');
        $this->db->from('annonce');
        $this->db->join('commercants', 'commercants.IdCommercant = annonce.annonce_commercant_id');
        $this->db->join('villes', 'villes.IdVille = commercants.IdVille');
        $this->db->join('ass_commercants_sousrubriques', 'ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant');
        $this->db->join('sous_rubriques', 'sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique');
        $this->db->where("villes.IdVille",$id_ville);
        $this->db->where("commercants.referencement_annonce = 1");
        $this->db->where("commercants.annonce", '1');
        $this->db->where("commercants.IsActif = 1");
        $this->db->LIMIT('5');
        $query = $this->db->get();
        return $query->result();
      }
      public function get_3_article($id_ville){
        $this->db->select('
                article_datetime.id as datetime_id, 
                article_datetime.date_debut as datetime_debut, 
                article_datetime.date_fin as datetime_fin, 
                article_datetime.heure_debut as datetime_heure_debut, 
                article.*,
                villes.Nom AS ville,
                villes.IdVille,
                agenda_categ.category,
                commercants.NomSociete');
        $this->db->from('article');
        $this->db->join('villes', 'article.IdVille_localisation = villes.IdVille');
        $this->db->join('agenda_categ', 'agenda_categ.agenda_categid = article.article_categid');
        $this->db->join('commercants', 'commercants.IdCommercant = article.IdCommercant');
        $this->db->join('article_datetime', 'article_datetime.article_id = article.id');
        $this->db->where('villes.IdVille', $id_ville);
        $this->db->where('article.IsActif = 1 OR article.IsActif = 2');
        $this->db->where('commercants.IsActif = 1');
        $this->db->where('commercants.referencement_article = 1');
        $this->db->LIMIT('5');
        $res = $this->db->get();
        return $res->result();
    }

    public function get_all_annonce_by_idville($id_ville, $categ= 0){
        $this->db->select('annonce.annonce_photo1 AS photo,
                    annonce.annonce_description_courte AS texte_courte,
                    annonce.annonce_prix_neuf AS prix_vente,
                    annonce.annonce_prix_solde AS prix_ancien,
                    annonce.annonce_etat AS etat,
                    annonce.annonce_date_debut,
                    annonce.annonce_date_fin,
                    commercants.NomSociete AS NomSociete,
                    villes.Nom AS ville,
                    villes.IdVille,
                    villes.CodePostal AS code,
                    commercants.Adresse1 AS quartier,
                    commercants.Adresse2 AS rue,
                    commercants.IdCommercant,
                    commercants.nom_url,
                    commercants.NomSociete,
                    commercants.user_ionauth_id,
                    sous_rubriques.Nom AS rubrique,
                    sous_rubriques.IdSousRubrique,
                    sous_rubriques.IdRubrique,
                    annonce.annonce_id,
                    annonce.annonce_description_longue,
                    annonce.annonce_photo1,
                    annonce.annonce_photo2,
                    annonce.annonce_photo3,
                    annonce.annonce_photo4,
                    annonce.module_paypal,
                    annonce.retrait_etablissement, 
                    annonce.livraison_domicile,
                    annonce.annonce_etat,
                    annonce.annonce_commercant_id');
        $this->db->from('annonce');
        $this->db->join('commercants', 'commercants.IdCommercant = annonce.annonce_commercant_id');
        $this->db->join('villes', 'villes.IdVille = commercants.IdVille');
        $this->db->join('ass_commercants_sousrubriques', 'ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant');
        $this->db->join('sous_rubriques', 'sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique');
        if($categ != 0){
            $this->db->where('sous_rubriques.IdSousRubrique', $categ);
        }
        $this->db->where("villes.IdVille",$id_ville);
        $this->db->where("commercants.referencement_annonce = 1");
        $this->db->where("commercants.annonce", '1');
        $this->db->where("commercants.IsActif = 1");
        $query = $this->db->get();
        return $query->result();
    }

    public function get_all_annuaire_by_idville($id_ville, $categ = 0){
        $this->db->select('
            commercants.user_ionauth_id ,
            villes.Nom, 
            commercants.NomSociete , 
            commercants.photo1 , 
            commercants.Caracteristiques , 
            commercants.IdCommercant ,
            commercants.TelFixe,
            commercants.Email,
            rubriques.Nom as rubriques , 
            villes.ville_nom, 
            villes.IdVille');
        $this->db->from('commercants');
        $this->db->join('villes', 'commercants.IdVille = villes.IdVille');
        $this->db->join('ass_commercants_rubriques', 'commercants.IdCommercant = ass_commercants_rubriques.IdCommercant');
        $this->db->join('rubriques', 'ass_commercants_rubriques.IdRubrique = rubriques.IdRubrique');
        if($categ != 0){
            $this->db->where('rubriques.IdRubrique', $categ);
        }
        $this->db->where('villes.IdVille', $id_ville);
        $this->db->where('commercants.IsActif = 1');
        $this->db->where('commercants.referencement_annuaire = 1');
        $res = $this->db->get();
        return $res->result();
    }


    public function get_all_agenda_by_idville($id_ville, $categ = 0){
        if($categ == 0){
            $req = 'SELECT
            agenda.IdVille,
            agenda.id,
            agenda.photo1,
            agenda.adresse_localisation,
            agenda.description,
            agenda.telephone,
            agenda.nom_societe,
            agenda.nom_manifestation,
            agenda.date_debut,
            agenda.date_fin,
            agenda.agenda_categid,
            villes.ville_nom,
            villes.ville_longitude_deg,
            villes.ville_latitude_deg,
            villes.CodePostal,
            villes.ville_canton,
            villes.Nom,
            villes.ville_arrondissement,
            villes.IdVille
            FROM
            agenda
            INNER JOIN villes ON agenda.IdVille = villes.IdVille
            WHERE villes.IdVille ="'.$id_ville.'"';
        }else{
            $req = 'SELECT
            agenda.IdVille,
            agenda.id,
            agenda.photo1,
            agenda.adresse_localisation,
            agenda.description,
            agenda.telephone,
            agenda.nom_societe,
            agenda.nom_manifestation,
            agenda.date_debut,
            agenda.date_fin,
            agenda.agenda_categid,
            villes.ville_nom,
            villes.ville_longitude_deg,
            villes.ville_latitude_deg,
            villes.CodePostal,
            villes.ville_canton,
            villes.Nom,
            villes.ville_arrondissement,
            villes.IdVille
            FROM
            agenda
            INNER JOIN villes ON agenda.IdVille = villes.IdVille
            WHERE villes.IdVille ="'.$id_ville.'"
            AND
            agenda_categid = "'.$categ.'"
            ';
        }
        
        $Resreq = $this->db->query($req);
        return $Resreq->result();
    }

    public function get_all_article_by_idville($idville, $categ = 0){
        $this->db->select('
                article_datetime.id as datetime_id, 
                article_datetime.date_debut as datetime_debut, 
                article_datetime.date_fin as datetime_fin, 
                article_datetime.heure_debut as datetime_heure_debut, 
                article.*,
                villes.Nom AS ville,
                villes.IdVille,
                agenda_categ.category,
                commercants.NomSociete');
        $this->db->from('article');
        $this->db->join('villes', 'article.IdVille_localisation = villes.IdVille');
        $this->db->join('agenda_categ', 'agenda_categ.agenda_categid = article.article_categid');
        $this->db->join('commercants', 'commercants.IdCommercant = article.IdCommercant');
        $this->db->join('article_datetime', 'article_datetime.article_id = article.id');
        $this->db->where('villes.IdVille', $idville);
        if($categ != 0){
            $this->db->where('article.article_categid', $categ);
        }
        $this->db->where('article.IsActif = 1 OR article.IsActif = 2');
        $this->db->where('commercants.IsActif = 1');
        $this->db->where('commercants.referencement_article = 1');
        $res = $this->db->get();
        return $res->result();

    }

    // get count categorie fiter by ville for agenda
    public function get_all_categorie_agenda($id_ville){
        $req = '
            SELECT agenda.agenda_categid,
                    agenda_categ.category,
                    agenda.IdVille_localisation,
            COUNT(*) agenda_count
            FROM agenda
            INNER JOIN agenda_categ 
            ON agenda_categ.agenda_categid = agenda.agenda_categid
            JOIN villes 
            ON agenda.IdVille_localisation = villes.IdVille
            JOIN commercants 
            ON commercants.IdCommercant = agenda.IdCommercant
            WHERE villes.IdVille = '.$id_ville.'
            AND commercants.IsActif = 1
            AND commercants.referencement_article = 1
            GROUP BY agenda_categid
        ';
        $Resreq = $this->db->query($req);
        return $Resreq->result();
    }

    // get count categorie fiter by ville for article
    public function get_all_categorie_article($id_ville){
        $req = '
            SELECT article.article_categid,
                    agenda_categ.category,
                    article.IdVille_localisation,
            COUNT(*) article_count
            FROM article
            INNER JOIN agenda_categ 
            ON agenda_categ.agenda_categid = article.article_categid
            JOIN villes 
            ON article.IdVille_localisation = villes.IdVille
            JOIN commercants 
            ON commercants.IdCommercant = article.IdCommercant
            JOIN article_datetime 
            ON article_datetime.article_id = article.id 
            WHERE villes.IdVille = '.$id_ville.'
            AND article.IsActif = 1 OR article.IsActif = 2
            AND commercants.IsActif = 1
            AND commercants.referencement_article = 1
            GROUP BY agenda_categid
        ';
        $Resreq = $this->db->query($req);
        return $Resreq->result();
    }

    public function get_all_categorie_annuaire($idVille){
        $req = '
            SELECT commercants.IdCommercant,
            rubriques.IdRubrique,
            rubriques.Nom,
            COUNT(*) commercant_count
            FROM commercants 
            INNER JOIN ass_commercants_rubriques as acr
            ON acr.IdCommercant = commercants.IdCommercant
            INNER JOIN rubriques
            ON rubriques.IdRubrique = acr.IdRubrique 
            WHERE commercants.IdVille = '.$idVille.'
            AND commercants.IsActif = 1
            AND commercants.referencement_article = 1
            GROUP BY IdRubrique
        ';
        $Resreq = $this->db->query($req);
        return $Resreq->result();
    }

    public function get_all_categorie_annonces($idVille){
        $req = '
            SELECT annonce.annonce_id,
                    sous_rubriques.Nom,
                    sous_rubriques.IdSousRubrique,
            COUNT(*) annonce_count 
            FROM annonce 
            INNER JOIN commercants
            ON commercants.IdCommercant = annonce.annonce_commercant_id
            INNER JOIN villes 
            ON villes.IdVille = commercants.IdVille 
            INNER JOIN ass_commercants_sousrubriques
            ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
            INNER JOIN sous_rubriques 
            ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique
            WHERE villes.IdVille = '.$idVille.'
            AND commercants.referencement_annonce = 1
            AND commercants.annonce = 1
            AND commercants.IsActif = 1
            GROUP BY IdSousRubrique
        ';
        $Resreq = $this->db->query($req);
        return $Resreq->result();
    }

    // count all data in topic - lambda
    public function get_count_article($id_ville){
        $req = 'SELECT
            article.IdVille_localisation as IdVille,
            COUNT(*) article_count
            FROM
            article
            INNER JOIN villes 
            ON article.IdVille_localisation = villes.IdVille
            INNER JOIN agenda_categ 
            ON agenda_categ.agenda_categid = article.article_categid
            INNER JOIN commercants 
            ON commercants.IdCommercant = article.IdCommercant
            INNER JOIN article_datetime 
            ON article_datetime.article_id = article.id
            WHERE villes.IdVille ='.$id_ville.'
            AND article.IsActif = 1 OR article.IsActif = 2
            AND commercants.IsActif = 1
            AND commercants.referencement_article = 1
            GROUP BY IdVille
        ';
        $Resreq = $this->db->query($req);
        return $Resreq->result();
    }

    // count all data in topic - lambda
    public function get_count_agenda($id_ville){
        $req = 'SELECT
            agenda.IdVille,
            COUNT(*) agenda_count
            FROM
            agenda
            INNER JOIN villes 
            ON agenda.IdVille = villes.IdVille
            WHERE villes.IdVille ='.$id_ville.'
            GROUP BY IdVille
        ';
        $Resreq = $this->db->query($req);
        return $Resreq->result();
    }

    // count all data in topic - lambda
    public function get_count_annonce($id_ville){
        $req = 'SELECT
            villes.IdVille,
            COUNT(*) annonce_count
            FROM
            annonce
            INNER JOIN commercants 
            ON commercants.IdCommercant = annonce.annonce_commercant_id
            INNER JOIN villes 
            ON villes.IdVille = commercants.IdVille
            INNER JOIN ass_commercants_sousrubriques 
            ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
            INNER JOIN sous_rubriques 
            ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique
            WHERE villes.IdVille ='.$id_ville.'
            AND commercants.referencement_annonce = 1
            AND commercants.annonce = 1
            AND commercants.IsActif = 1        
            GROUP BY IdVille
        ';
        $Resreq = $this->db->query($req);
        return $Resreq->result();
    }

    // count all data in topic - lambda
    public function get_count_annuaire($id_ville){
        $req = 'SELECT
            commercants.IdVille,
            COUNT(*) annuaire_count
            FROM
            commercants
            INNER JOIN villes 
            ON commercants.IdVille = villes.IdVille
            INNER JOIN ass_commercants_rubriques 
            ON commercants.IdCommercant = ass_commercants_rubriques.IdCommercant
            INNER JOIN rubriques 
            ON ass_commercants_rubriques.IdRubrique = rubriques.IdRubrique
            WHERE villes.IdVille ='.$id_ville.'
            AND commercants.referencement_annonce = 1
            AND commercants.IsActif = 1        
            GROUP BY IdVille
        ';
        $Resreq = $this->db->query($req);
        return $Resreq->result();
    }

public function get_bonplan_byidville($id){


    $zSqlListeBonPlan = "
            SELECT
                bonplan.* ,
                commercants.user_ionauth_id as ionauth_id,
                commercants.Photo5 as Photo5,
                commercants.NomSociete AS NomSociete,
                villes.Nom AS ville,
                commercants.Adresse1 AS quartier,
                commercants.Adresse2 AS rue,
                commercants.IdCommercant
            FROM
                bonplan
                LEFT JOIN commercants ON commercants.IdCommercant = bonplan.bonplan_commercant_id
                LEFT JOIN villes ON villes.IdVille = commercants.IdVille
            WHERE  commercants.IdVille  = ".$id." AND (bonplan.bp_unique_date_fin >= NOW()) OR (bonplan.bp_multiple_date_fin >= NOW()) OR (bonplan.bonplan_date_fin >= NOW())
        ";
        
        
        //echo $zSqlListeBonPlan;
        
        $zQueryListeBonPlan = $this->db->query($zSqlListeBonPlan) ;
        return $zQueryListeBonPlan->result() ;
}
    public function get_fidelite_byidville($id,$table_fidelity_used){


        $zSqlListeBonPlan = "
            SELECT
            ";
        if ($table_fidelity_used == "remise") {
            $zSqlListeBonPlan .= "
                card_remise.id,
                card_remise.date_debut, 
                card_remise.date_fin,
                card_remise.description,
                card_remise.montant,
                card_remise.value_type,
                card_remise.image1,
                ";
        } else if ($table_fidelity_used == "tampon") {
            $zSqlListeBonPlan .= "
                card_tampon.id,
                card_tampon.date_debut,
                card_tampon.date_fin,
                card_tampon.description,
                card_tampon.tampon_value,
                card_tampon.image1,
                ";
        } else {
            $zSqlListeBonPlan .= "
                card_capital.id,
                card_capital.date_debut, 
                card_capital.date_fin,
                card_capital.description,
                card_capital.remise_value,
                card_capital.montant,
                card_capital.image1,
                ";
        }
        $zSqlListeBonPlan .= "
            commercants.Photo5 AS Photo5,
            commercants.NomSociete,
            commercants.user_ionauth_id,
            villes.Nom AS ville,
            commercants.Adresse1 AS quartier,
            commercants.Adresse2 AS rue,
            commercants.IdCommercant,
            rubriques.IdRubrique,
            rubriques.Nom,
            commercants.IdVille,
            commercants.nom_url
            ";
            $zSqlListeBonPlan .= "
            FROM
            ";
        if ($table_fidelity_used == "remise") {
            $zSqlListeBonPlan .= "
                card_remise
                Inner Join commercants ON commercants.IdCommercant = card_remise.id_commercant
                ";
        } else if ($table_fidelity_used == "tampon") {
            $zSqlListeBonPlan .= "
                card_tampon
                Inner Join commercants ON commercants.IdCommercant = card_tampon.id_commercant
                ";
        } else {
            $zSqlListeBonPlan .= "
                card_capital
                Inner Join commercants ON commercants.IdCommercant = card_capital.id_commercant
                ";
        }

        $zSqlListeBonPlan .= "
            Left Join villes ON villes.IdVille = commercants.IdVille
            Inner Join ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
            Inner Join rubriques ON rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique
            ";
             if ($table_fidelity_used == "remise") $zSqlListeBonPlan .= "LEFT OUTER JOIN bonplan on bonplan.bonplan_commercant_id = card_remise.id_commercant";
        else if ($table_fidelity_used == "tampon") $zSqlListeBonPlan .= "LEFT OUTER JOIN bonplan on bonplan.bonplan_commercant_id = card_tampon.id_commercant";
        else $zSqlListeBonPlan .= "LEFT OUTER JOIN bonplan on bonplan.bonplan_commercant_id = card_capital.id_commercant";

        $zSqlListeBonPlan .= "
            WHERE 
            commercants.IdVille = $id
        ";

        if ($table_fidelity_used == "remise") {
            $zSqlListeBonPlan .= " AND card_remise.is_activ = 1 AND NOW() BETWEEN    card_remise.date_debut AND card_remise.date_fin";
        } else if ($table_fidelity_used == "tampon") {
            $zSqlListeBonPlan .= "  AND card_tampon.is_activ = 1 AND NOW() BETWEEN    card_tampon.date_debut AND card_tampon.date_fin";
        } else {
            $zSqlListeBonPlan .= " AND card_capital.is_activ = 1 AND NOW() BETWEEN    card_capital.date_debut AND card_capital.date_fin";
        }
        $zQueryListeBonPlan = $this->db->query($zSqlListeBonPlan);
        return $zQueryListeBonPlan->result();

    }
}
?>
