<?php
class mdldemande_abonnement extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
	
	public function getAll(){
    
        $list = $this->db->select('*')
                    ->from("demande_abonnement")
                    ->get()
                    ->result();
        return $list;   
    
    }

    function getWhere($prmWhere = "0 = 0") {

        $qry =  $this->db->query("SELECT * FROM demande_abonnement WHERE " . $prmWhere );

        if ($qry->num_rows() > 0) {
            $Res = $qry->result();
            return $Res;
        }
    }
    

    public function getById($id){
    
        $rep = $this->db->select('*')
                    ->from("demande_abonnement")
                    ->where('id',$id)
                    ->get()
                    ->result();
        return $rep;
    
    }

    public function getByIdCommercant($idCommercant){
    
        $rep = $this->db->select('*')
                    ->from("demande_abonnement")
                    ->where('idCommercant',$idCommercant)
                    ->get()
                    ->result();
        return $rep;
    
    }
    
    public function getByIdUser_ionauth($idUser_ionauth){
    
        $rep = $this->db->select('*')
                    ->from("demande_abonnement")
                    ->where('idUser_ionauth',$idUser_ionauth)
                    ->get()
                    ->result();
        return $rep;
    
    }


    public function getByIdAbonnement($idAbonnement){
    
        $rep = $this->db->select('*')
                    ->from("demande_abonnement")
                    ->where('idAbonnement',$idAbonnement)
                    ->get()
                    ->result();
        return $rep;
    
    }
    
    public function IdAbonnementExist($idAbonnement, $idCommercant, $idUser_ionauth){
    
        $rep = $this->db->select('*')
                    ->from("demande_abonnement")
                    ->where('idAbonnement',$idAbonnement)
                    ->where('idCommercant',$idCommercant)
                    ->where('idUser_ionauth',$idUser_ionauth)
                    ->get()
                    ->result();
        
        if (count($rep)>0) return true;
        else return false;
    
    }

    function insert($prmData) {
        $this->db->insert("demande_abonnement", $prmData);
        return $this->db->insert_id();
    }

    function update($prmData) {
        $prmData = (array)$prmData;
        $this->db->where("id", $prmData["id"]);
        $this->db->update("demande_abonnement", $prmData);
        return $prmData["id"];
    }

    function delete($prmId){
        $qry = $this->db->query("DELETE FROM demande_abonnement WHERE id = ?", $prmId);
        return $qry;
    }

    function getByIdCommercantIdUser($prmId=0, $secId=0) {

        $qryuserid = $this->db->query("
            SELECT
            abonnements.Nom,
            abonnements.Description,
            abonnements.tarif,
            demande_abonnement.idCommercant,
            demande_abonnement.idUser_ionauth,
            abonnements.IdAbonnement
            FROM
            abonnements 
            INNER JOIN demande_abonnement on demande_abonnement.idAbonnement = abonnements.IdAbonnement
            WHERE
            demande_abonnement.idCommercant = '".$prmId."' AND
            demande_abonnement.idUser_ionauth = '".$secId."'
            GROUP BY
            abonnements.IdAbonnement
            ");
        if ($qryuserid->num_rows() > 0) {
            $Res = $qryuserid->result();
            return $Res;
        } else return NULL;
   
    }
 
    
   
}