<?php
class mdl_lightbox_mail extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
	
	function getById($id=0){
		$Sql = "select * from lightbox_mail where id =". $id ;		
		$Query = $this->db->query($Sql);
		return $Query->row();
	}

    function getFirstId(){
        $Sql = " SELECT id from lightbox_mail ORDER BY id ASC LIMIT 1 ";
        $Query = $this->db->query($Sql);
        return $Query->row()->id;
    }

    function getLastId(){
        $Sql = " SELECT id from lightbox_mail ORDER BY id DESC LIMIT 1 ";
        $Query = $this->db->query($Sql);
        return $Query->row()->id;
    }

    // requette verification adress mail
    function getByMail($mail=""){
        $Sql = "select * from lightbox_mail where mail ='". $mail ."'";
        $Query = $this->db->query($Sql);
        if($Query->num_rows() > 0) {
            return $Query->result();
        }
    }

    function getByCode($departement_code="00"){
        $Sql = "select * from lightbox_mail where departement_code ='". $departement_code ."'";
        $Query = $this->db->query($Sql);
        if($Query->num_rows() > 0) {
            return $Query->result();
        }
    }

	function GetAll(){
        $qryVille = $this->db->query("
            SELECT * FROM `lightbox_mail` WHERE `mail` IS NOT null

        ");
        if($qryVille->num_rows() > 0) {
            return $qryVille->result();
        }
    }
        
    function delete($prmId){
    
        $qryBonplan = $this->db->query("DELETE FROM lightbox_mail WHERE id = ?", $prmId) ;
        return $qryBonplan ;
    }

    function insert($prmData) {
        $this->db->insert("lightbox_mail", $prmData);
        return $this->db->insert_id();
    }

    // function insert($prmData) {
    //     // $this->db->insert("lightbox_mail", $prmData);
    //     // return $this->db->insert_id();

    //     $prmData = array(
    //         'mail' => $param['upublightbox_mail'],
    //         'name' => $param['publightbox_name'],
    //     );
    //     $this->db->insert("lightbox_mail", $prmData);
    // }

    function update($prmData) {
        $this->db->where("id", $prmData["id"]);
        $this->db->update("lightbox_mail", $prmData);
        $objResult = $this->getById($prmData["id"]);
        return $objResult->id;
    }

    function GetWhere($prmWhere = "0 = 0", $prmOffset = 0, $prmLimit = 0, $prmOrder = " mail ASC ") {
        if (empty($prmOrder)) {
            $prmOrder = " mail ASC ";
        }

        $qryString = "
            SELECT
                lightbox_mail.id AS 'id',
                lightbox_mail.mail AS 'mail',
                lightbox_mail.departement_code AS 'departement_code',
                lightbox_mail.name AS 'name'
            FROM
                lightbox_mail
            WHERE " . $prmWhere . " AND mail IS NOT null
            ORDER BY " . $prmOrder . "";
        if (!(($prmOffset == 0) and ($prmLimit == 0))) {
            $qryString .= " LIMIT $prmOffset, $prmLimit";
        }
        return $this->db->query($qryString)->result();
    }

    function CountWhere($prmWhere = "0=0") {
        $Res = $this->db->query("SELECT COUNT(id) AS 'Decompte' FROM lightbox_mail WHERE " . $prmWhere)->result();
        return $Res[0]->Decompte;
    }

    function duplicateEmailList() {
        $qryString = "
        SELECT
        count(id)-1 as nb
        FROM
        lightbox_mail
        GROUP BY
        lightbox_mail.mail
        HAVING
        count(id) > 1
            ";
        return $this->db->query($qryString)->result();
    }

    function removeDuplicateEmail(){
        $qryString = "
        DELETE FROM lightbox_mail
         WHERE id NOT IN (SELECT * 
                            FROM (SELECT MIN(n.id)
                                    FROM lightbox_mail n
                                GROUP BY n.mail) x)
            ";
        return $this->db->query($qryString);
    }

   public function get_count_lightbox_mail(){
       $this->db->where('mail !=','');
       $res = $this->db->get('lightbox_mail');
       return $res->num_rows();
   }

public function test_exist($test_exist){
    $this->db->where('mail',$test_exist);
    $res = $this->db->get('lightbox_mail');
    if($res->num_rows() != 0 ){
        return true;
    }else{
        return false;
    }
}
 function get_by_idcom($id){


                $sql = "SELECT commercants.Email 
                FROM `ass_commercants_abonnements` 
                INNER JOIN commercants 
                ON ass_commercants_abonnements.IdCommercant = commercants.IdCommercant 
                WHERE ass_commercants_abonnements.IdAbonnement ='". $id ."' AND commercants.Email IS NOT NULL ";

                        $run = $this->db->query($sql);
                        return $run->result();
}

// function get_by_idcom_4(){


//                 $sql = "SELECT commercants.Email 
//                 FROM `ass_commercants_abonnements` 
//                 INNER JOIN commercants 
//                 ON ass_commercants_abonnements.IdCommercant = commercants.IdCommercant 
//                 WHERE ass_commercants_abonnements.IdAbonnement = 4";

//                         $run = $this->db->query($sql);
//                         return $run->result();
//     }

// function get_all_test(){


//                 $sql = "SELECT `Email` FROM `news_statut`";

//                         $run = $this->db->query($sql);
//                         return $run->result();
//     }

}