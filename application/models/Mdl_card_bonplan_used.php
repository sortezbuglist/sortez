<?php
class mdl_card_bonplan_used extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    function getById($id=0){
        $Sql = "select * from card_bonplan_used where id =". $id ;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }
    
	public function get_with_commercant_by_id($id){
		$this->db->select('card_bonplan_used.*,commercants.*,villes.nom as ville_commercant,bonplan.bonplan_titre as bonplan_titre, bonplan.bonplan_photo1 as bonplan_photo');
	   	$this->db->from('card_bonplan_used');
	   	$this->db->join('commercants','card_bonplan_used.id_commercant = commercants.IdCommercant');
	   	$this->db->join('villes','villes.idVille = commercants.idVille');
	   	$this->db->join('bonplan','bonplan.bonplan_id = card_bonplan_used.id_bonplan');
	   	$this->db->where('card_bonplan_used.id',$id);
		$query = $this->db->get();
		return ($query->num_rows == 1) ? $query->row() : false;
	}
	
	public function get_by_critere($critere=array(),$order_by=array('champ'=>'card_bonplan_used.id','ordre'=> 'DESC'),$group_by=array(),$list =0){
        //var_dump($critere);die();
		$this->db->select('card_bonplan_used.*,commercants.*,villes.nom as ville_commercant,bonplan.bonplan_titre as bonplan_titre, bonplan.bonplan_photo1 as bonplan_photo');
	   	$this->db->from('card_bonplan_used');
	   	$this->db->join('commercants','card_bonplan_used.id_commercant = commercants.IdCommercant');
	   	$this->db->join('villes','villes.idVille = commercants.idVille');
	   	$this->db->join('bonplan','bonplan.bonplan_id = card_bonplan_used.id_bonplan','left');
	   	$this->db->where($critere);
	   	if($list){
	   		$this->db->where('card_bonplan_used.id IN (SELECT MAX(id) FROM card_bonplan_used group by id_commercant, description,id_user)');
	   	}
	   	if(isset($order_by['champ']) && isset($order_by['ordre'])){
	   		$this->db->order_by($order_by['champ'],$order_by['ordre']);
	   	}
	    $this->db->group_by($group_by);  
		$query = $this->db->get();
		//var_dump($query);die();
		
		return ($query->num_rows > 0) ? $query->result() : false;
	}

    public function get_by_critere2($iduser,$idcom){
        $this->db->select('card_bonplan_used.*,commercants.*,bonplan.*');
        $this->db->from('card_bonplan_used');
        $this->db->join('commercants','card_bonplan_used.id_commercant = commercants.IdCommercant');
        $this->db->join('bonplan','card_bonplan_used.id_bonplan = bonplan.bonplan_id');
        $this->db->where('card_bonplan_used.id_user',$iduser);
        $this->db->where('card_bonplan_used.id_commercant',$idcom);
        $query = $this->db->get();
        return $query->result();
    }
    function GetAll(){
        $qry = $this->db->query("
            SELECT * 
            FROM
                card_bonplan_used
            ORDER BY id DESC
        ");
        if($qry->num_rows() > 0) {
            return $qry->result();
        }
    }

    function getWhereOne($where=""){
        $query = "
            SELECT *
            FROM
                card_bonplan_used
            WHERE
              0=0
        ";
        if (isset($where) && $where!="") $query .= " AND ".$where;

        $query .= " LIMIT 1 ";

        $Query = $this->db->query($query);
        return $Query->row();
    }

    function getWhere($where=""){
        $query = "
            SELECT *
            FROM
                card_bonplan_used
            WHERE
              0=0
        ";
        if (isset($where) && $where!="") $query .= " AND ".$where;

        //echo $query;

        $qry = $this->db->query($query);
        if($qry->num_rows() > 0) {
            return $qry->result();
        }
    }

    function getWhereLimit($where="",$_limitstart=0,$_limitend=100000){
        $query = "
            SELECT *
            FROM
                card_bonplan_used
            WHERE
              0=0
        ";
        if (isset($where) && $where!="") $query .= " AND ".$where;

        $query .= " LIMIT ".$_limitstart.",".$_limitend ;

        $qry = $this->db->query($query);
        if($qry->num_rows() > 0) {
            return $qry->result();
        }
    }

    function getAllTamponByIdUser($idUser="0",$_limitstart=0,$_limitend=100000){
        $query = "
            SELECT *
                        FROM
                            card_bonplan_used
                        WHERE
                          0=0
            AND
            id_user = '".$idUser."'
            AND tampon_value_added <> ''
            ORDER BY id DESC
        ";
        $query .= " LIMIT ".$_limitstart.",".$_limitend ;

        $qry = $this->db->query($query);
        if($qry->num_rows() > 0) {
            return $qry->result();
        }
    }

    function getAllCapitalByIdUser($idUser="0",$_limitstart=0,$_limitend=100000){
        $query = "
            SELECT *
                        FROM
                          card_bonplan_used
                        WHERE
                          0=0
            AND
            id_user = '".$idUser."'
            AND capital_value_added <> ''
            ORDER BY id DESC
        ";
        $query .= " LIMIT ".$_limitstart.",".$_limitend ;

        $qry = $this->db->query($query);
        if($qry->num_rows() > 0) {
            return $qry->result();
        }
    }

    function getByIdCommercant($id_commercant="0"){
        $query = "
            SELECT
            users.Nom,
            users.Prenom,
            card.num_id_card_virtual,
            card.num_id_card_physical
            FROM
            card_bonplan_used
            INNER JOIN users ON card_bonplan_used.id_user = users.IdUser
            INNER JOIN card ON card_bonplan_used.id_card = card.id
            WHERE
            card_bonplan_used.id_commercant = '".$id_commercant."'
            GROUP BY
            card_bonplan_used.id_user

        ";

        $qry = $this->db->query($query);
        if($qry->num_rows() > 0) {
            return $qry->result();
        }
    }

    function delete($prmId){

        $qry = $this->db->query("DELETE FROM card_bonplan_used WHERE id = ?", $prmId) ;
        return $qry ;
    }

    function deleteByUCid($userId,$commercantId){
        $this->db->where('id_user',$userId);
        $this->db->where('id_commercant',$commercantId);
    	$this->db->delete('card_bonplan_used');
    }
    
    function insert($prmData) {
        $this->db->insert("card_bonplan_used", $prmData);
        return $this->db->insert_id();
    }

    function update($prmData) {
        $this->db->where("id", $prmData["id"]);
        $this->db->update("card_bonplan_used", $prmData);
        $obj = $this->getById($prmData["id"]);
        return $obj->id;
    }
    
}