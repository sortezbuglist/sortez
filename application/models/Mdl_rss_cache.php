<?php

class Mdl_rss_cache extends CI_model{

	function __construct(){
		parent::__construct();
	}

	function getCache(){
		$sql = "SELECT * FROM rss_cache LIMIT 1";
		$query = $this->db->query($sql);
		return $query->row();
	}

	function deleteCache(){
		$sql = "TRUNCATE TABLE `rss_cache`";
		return $this->db->query($sql);
	}

	function setCache($cache){
		$this->db->insert("rss_cache", $cache);
		return $this->db->insert_id();
	}

	function resetArticle(){
		$sql = "TRUNCATE TABLE `article`";
		return $this->db->query($sql);
	}

	function checkData($data){
		$nom_manifestation = $data['nom_manifestation'];
		$this->db->select('*');
		$this->db->from('article');
		$this->db->where("nom_manifestation",$nom_manifestation);
		$query = $this->db->get();
		$res = $query->num_rows();
        if ($res > 0) {
            return 0;
        } else {
            return 1;
        }
	}

	function count_article_data(){
		$this->db->select('*');
		$this->db->from('article');
		$query = $this->db->get();
		$res = $query->num_rows();
		return $res;
	}

}