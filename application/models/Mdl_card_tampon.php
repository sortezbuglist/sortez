<?php
class mdl_card_tampon extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    function getById($id=0){
        $Sql = "select * from card_tampon where id =". $id ;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByIdIonauth($id_ionauth=0){
        $Sql = "select * from card_tampon where id_ionauth ='". $id_ionauth ."' LIMIT 1;";
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByIdCommercant($id_comm=0){
        $Sql = "select * from card_tampon where id_commercant =". $id_comm ." LIMIT 1;";
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByIdRubrique($IdRubrique=0){
        $Sql = "
            SELECT
                card_tampon.id,
                ass_commercants_rubriques.IdCommercant,
                ass_commercants_rubriques.IdRubrique
                FROM
                card_tampon
                INNER JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = card_tampon.id_commercant
                INNER JOIN commercants ON commercants.IdCommercant = card_tampon.id_commercant
                WHERE
                card_tampon.is_activ = 1 AND
                commercants.IsActif = 1 AND
                ass_commercants_rubriques.IdRubrique = '". $IdRubrique ."'
        ";
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByIdSousRubrique($IdSousRubrique=0){
        $Sql = "
            SELECT
                card_tampon.id,
                ass_commercants_sousrubriques.IdCommercant,
                ass_commercants_sousrubriques.IdSousRubrique
                FROM
                card_tampon
                INNER JOIN ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = card_tampon.id_commercant
                WHERE
                card_tampon.is_activ = 1 AND
                ass_commercants_sousrubriques.IdSousRubrique = '". $IdSousRubrique ."'
        ";
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function GetAll(){
        $qry = $this->db->query("
            SELECT * 
            FROM
                card_tampon
            ORDER BY id DESC
        ");
        if($qry->num_rows() > 0) {
            return $qry->result();
        }
    }

    function GetWhere($where='0=0'){
        $Sql = "
            SELECT * 
            FROM
                card_tampon 
            ".$where." 
            ORDER BY id DESC
        ";
        $qry = $this->db->query($Sql);
        if($qry->num_rows() > 0) {
            return $qry->result();
        }
    }



    function delete($prmId){

        $qry = $this->db->query("DELETE FROM card_tampon WHERE id = ?", $prmId) ;
        return $qry ;
    }

    function insert($prmData) {
        $this->db->insert("card_tampon", $prmData);
        return $this->db->insert_id();
    }

    function update($prmData) {
        $this->db->where("id", $prmData["id"]);
        $this->db->update("card_tampon", $prmData);
        $obj = $this->getById($prmData["id"]);
        return $obj->id;
    }
    
    function delete_where($where=array()){
    	$this->db->where($where);
    	return   (!empty($where)) ? $this->db->delete("card_tampon"): false;
    }
    
    function is_tampon_active($id_commercant){
    	$this->db->select('is_activ');
    	$query = $this->db->get_where('card_tampon', array('id_commercant'=>$id_commercant));

    	return $query->first_row();
    }

    function id_tampon_active($id_commercant){
        $this->db->select('id,description');
        $query = $this->db->get_where('card_tampon', array('id_commercant'=>$id_commercant, 'is_activ'=>'1'));
        
        return $query->first_row();
    }
    function file_manager_update_tampon($iUser, $user_ion_auth_id, $image1, $img_file){
        $this->db->where("id_commercant", $iUser);
        $this->db->where("id_ionauth", $user_ion_auth_id);
        $field=array($image1=>$img_file);
        $this->db->update("card_tampon",$field);
    }

    function file_manager_delete_tampon($iUser, $image1){
        $this->db->where("id_commercant", $iUser);
        $field=array($image1=>null);
        $this->db->update("card_tampon",$field);
    }
    function delete_manager_update_capitalisation($idcom){       
        $Sql = "
            UPDATE card_tampon
            SET image1 = ''
            WHERE id_commercant = '".$idcom."'
        ";
        $Query = $this->db->query($Sql);
    }
    
    
}