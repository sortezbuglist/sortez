<?php
class mdl_card_user_link extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    function getById($id=0){
        $Sql = "select * from card_user_link where id =". $id ;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function check_card($idCard=0){
        $Sql = "select * from card where id ='".$idCard."' LIMIT 1" ;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }
//    function check_card($idCard=0){
//        $Sql = "select * from card_user_link where id_card ='".$idCard."' LIMIT 1" ;
//        $Query = $this->db->query($Sql);
//        return $Query->row();
//    }

    function GetAll(){
        $qry = $this->db->query("
            SELECT * 
            FROM
                card_user_link
            ORDER BY id DESC
        ");
        if($qry->num_rows() > 0) {
            return $qry->result();
        }
    }


    function delete($prmId){

        $qry = $this->db->query("DELETE FROM card_user_link WHERE id = ?", $prmId) ;
        return $qry ;
    }

    function insert($prmData) {
        $this->db->insert("card_user_link", $prmData);
        return $this->db->insert_id();
    }

    function update($prmData) {
        $this->db->where("id", $prmData["id"]);
        $this->db->update("card_user_link", $prmData);
        $obj = $this->getById($prmData["id"]);
        return $obj->id;
    }
    public function check_card_command($idcard){
        $this->db->select("commande_soutenons_list.*,card.*");
        $this->db->from("commande_soutenons_list");
        $this->db->join("card", "card.id_user = commande_soutenons_list.id_client");
        $this->db->where("commande_soutenons_list.id_client", $idcard);
        $query = $this->db->get();
        return $query->row();
    }
}