<?php
class Seo_model extends CI_Model {
     
     
    public function saverecords($paramdata,$id_commercant=0)
    {
        if($id_commercant != 0 && $id_commercant == $paramdata['IdCommercant']){
            $this->db->where('IdCommercant',$id_commercant);
            $this->db->update('seo',$paramdata);
            return 1;
        }else{
            $this->db->insert('seo',$paramdata);
            return $this->db->insert_id();
        }
    }
    public function get_data_by_dommain_name($prmdata){
        $this->db->where('domaine_name',$prmdata);
        $resultat = $this->db->get('seo');
        return $resultat->result();
    }
    public function get_data_by_Idcommercant($idcom){
        $this->db->where('IdCommercant',$idcom);
        $resultat = $this->db->get('seo');
        return $resultat->row();
    }
    public function check_seo_by_Idcommercant($idcom){
        $this->db->where('IdCommercant',$idcom);
        $resultat = $this->db->get('commercants');
        return $resultat->row(); //active_seo
    }    
}