<?php

class user extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function Create()
    {
        $objUtilisateur = new stdClass();
        $objUtilisateur->IdUser = "0";
        $objUtilisateur->DateNaissance = "";
        $objUtilisateur->Civilite = "";
        $objUtilisateur->Nom = "";
        $objUtilisateur->Prenom = "";
        $objUtilisateur->Profession = "";
        $objUtilisateur->Adresse = "";
        $objUtilisateur->IdVille = "0";
        $objUtilisateur->CodePostal = "";
        $objUtilisateur->Telephone = "";
        $objUtilisateur->Portable = "";
        $objUtilisateur->Fax = "";
        $objUtilisateur->Email = "";
        $objUtilisateur->Login = "";
        $objUtilisateur->DateCreation = "";
        $objUtilisateur->IsActif = "0";
        $objUtilisateur->NbrPoints = "0";
        $objUtilisateur->UserRole = "0";
        return $objUtilisateur;
    }

    function GetWhere($prmWhere = "0 = 0", $prmOffset = 0, $prmLimit = 0, $prmOrder = " Nom ASC ")
    {
        if (empty($prmOrder)) {
            $prmOrder = " Nom ASC ";
        }


        $qryString = "
            SELECT
                users.IdUser AS 'IdUser',
                users.DateNaissance AS 'DateNaissance',
                users.Civilite AS 'Civilite',
                users.Nom AS 'Nom',
                users.Prenom AS 'Prenom',
                users.Profession AS 'Profession',
                users.Adresse AS 'Adresse',
                users.IdVille AS 'IdVille',
                users.CodePostal AS 'CodePostal',
                users.Telephone AS 'Telephone',
                users.Portable AS 'Portable',
                users.Fax AS 'Fax',
                users.Email AS 'Email',
                users.Login AS 'Login',
                users.DateCreation AS 'DateCreation',
                users.IsActif AS 'IsActif',
                users.UserRole AS 'UserRole',
                users.user_ionauth_id,
                users.NbrPoints AS 'NbrPoints',
                
                villes.Nom AS 'NomVille'
            FROM
                (users
                LEFT JOIN villes ON villes.IdVille = users.IdVille)
            WHERE " . $prmWhere . "
            ORDER BY " . $prmOrder . "";
        if (!(($prmOffset == 0) and ($prmLimit == 0))) {
            $qryString .= " LIMIT $prmOffset, $prmLimit";
        }
        return $this->db->query($qryString)->result();
    }

    function GetById($prmId=0)
    {
        $sqsqr = "
            SELECT
                users.*,
                villes.Nom AS 'NomVille'
            FROM
                (users
                LEFT JOIN villes ON villes.IdVille = users.IdVille)
                ";

        $sqsqr .= "
            WHERE users.IdUser = " . $prmId;
            $qryUtilisateur = $this->db->query($sqsqr);
            if ($qryUtilisateur->num_rows() > 0) {
                $Res = $qryUtilisateur->result();
                return $Res[0];
            }
    }

    function Insert($prmData)
    {
        $this->db->insert("users", $prmData);
        return $this->db->insert_id();
    }

    function Update($prmData)
    {
        $prmData = (array)$prmData;
        $this->db->where("IdUser", $prmData["IdUser"]);
        $this->db->update("users", $prmData);
        return $this->GetById($prmData["IdUser"]);
    }

    /**
     * Fonction pour la suppression d'un objet Eleve ? partir de son Identifiant
     *
     * @param unknown_type $prmId
     */
    function DeleteById($prmId = 0)
    {
        $qry = $this->db->query("DELETE FROM users WHERE IdUser = ?", $prmId);
        return $qry;
    }

    function CountWhere($prmWhere = "0=0")
    {
        $Res = $this->db->query("SELECT COUNT(IdUser) AS 'Decompte' FROM (users LEFT JOIN villes On villes.IdVille = users.IdVille) WHERE " . $prmWhere)->result();
        return $Res[0]->Decompte;
    }

    function GetLastOrdre()
    {
        $qryUtilisateur = $this->db->query("SELECT MAX(Ordre) AS 'Maxi' FROM users");
        if ($qryUtilisateur->num_rows() > 0) {
            $Res = $qryUtilisateur->result();
            return $Res[0]->Maxi;
        } else {
            return null;
        }
    }

    function MoveOrder($prmId, $prmDirection = "up")
    {
        if ($prmDirection == "up") {
            $MoveUnit = 1;
        } elseif ($prmDirection == "down") {
            $MoveUnit = -1;
        } else {
            throw new Exception("Le second param?tre de MoveOrder doit ?tre : 'up' ou 'down'");
        }
        //
        $objUtilisateur = $this->GetById($prmId);
        // Liste des Eleves
        $colUtilisateurToMove = $this->GetWhere('1 AND users.IdUser = ' . $objUtilisateur->IdUser . '');

        for ($i = 0; $i < sizeof($colUtilisateurToMove); $i++) {
            if ($objUtilisateur->Ordre == $colUtilisateurToMove[$i]->Ordre) {
                if (isset($colUtilisateurToMove[$i - $MoveUnit])) {
                    $objUtilisateur1 = $this->GetById($colUtilisateurToMove[$i]->Eleve_id);
                    $objUtilisateur1->Ordre = $colUtilisateurToMove[$i - $MoveUnit]->Ordre;
                    $this->Update($objUtilisateur1);

                    $objUtilisateur2 = $this->GetById($colUtilisateurToMove[$i - $MoveUnit]->Eleve_id);
                    $objUtilisateur2->Ordre = $objUtilisateur->Ordre;
                    $this->Update($objUtilisateur2);
                }

                unset($objUtilisateur);
                unset($objUtilisateur1);
                unset($objUtilisateur2);

                break;
            }
        }

        // Au cas o? aucune Eleve ne pr?c?de/suive la Eleve en cours
        // Nous ne faisons rien car nous atteignons dans ce cas le sommet/bout de la liste
        // dans le th?me concern? !
        // ...

    }

    /** End Of MoveOrder  Function */


    function GetAll()
    {
        $qryUtilisateur = $this->db->query("
            SELECT
                *
            FROM
                users
        ");
        if ($qryUtilisateur->num_rows() > 0) {
            return $qryUtilisateur->result();
        }
    }

    function GetAllOkNewsletter()
    {
        $qryUtilisateur = $this->db->query("
            SELECT
                users.IdUser,
                users.Nom,
                users.Prenom,
                users.Email,
                commercants.IdCommercant,
                commercants.NomSociete,
                GROUP_CONCAT(commercants.IdCommercant ORDER BY commercants.IdCommercant ASC) AS 'ListCommercants'
            FROM
                ((ass_commercants_users
                LEFT JOIN users ON users.IdUser = ass_commercants_users.IdUser)
                LEFT JOIN commercants ON commercants.IdCommercant = ass_commercants_users.IdCommercant)
            WHERE
                ass_commercants_users.Newsletter = '1'
            GROUP BY IdUser
        ");
        if ($qryUtilisateur->num_rows() > 0) {
            return $qryUtilisateur->result();
        }
    }

    function GetAllValidUserList_test()
    {
        $qryUtilisateur = $this->db->query("
            SELECT
            users.*
            FROM
            users
            WHERE
            users.Email <> ''
        ");
        if ($qryUtilisateur->num_rows() > 0) {
            return $qryUtilisateur->result();
        }
    }

    function GetAllValidUserList_test_1()
    {
        $qryUtilisateur = $this->db->query("
            SELECT
            users.*
            FROM
            users
            WHERE
            users.Email = 'randawilly@gmail.com' OR
            users.Email = 'william.arthur.harilantoniaina@gmail.com' OR
            users.Email = 'randawilly@hotmail.com' OR
            users.Email = 'randawillyrindra@yahoo.fr' OR
            users.Email = 'randawilly@aol.fr' OR
            users.Email = 'randawilly@gmail2.com' OR
            users.Email = 'randawilly@gmail3.com' OR
            users.Email = 'william.arthur@globalvision.mg'
        ");
        if ($qryUtilisateur->num_rows() > 0) {
            return $qryUtilisateur->result();
        }
    }

    function AddFavoris($prmData)
    {
        $this->db->insert("ass_commercants_users", $prmData);
        return $this->db->insert_id();
    }

    function UpdateFavoris($prmData)
    {
        $this->db->where("IdAssCommercantUser", $prmData["IdAssCommercantUser"]);
        $this->db->update("ass_commercants_users", $prmData);
        return $this->db->insert_id();
    }

    function getFavoris($prmData)
    {
        $qryCommercant = $this->db->query("
   
            SELECT
            ass_commercants_users.IdAssCommercantUser,
            ass_commercants_users.IdUser,
            ass_commercants_users.IdCommercant,
            ass_commercants_users.Favoris,
            ass_commercants_users.Newsletter,
            commercants.*,
            ass_commercants_sousrubriques.IdSousRubrique,
            villes.Nom as ville
            FROM
            ass_commercants_users
            INNER JOIN commercants ON commercants.IdCommercant = ass_commercants_users.IdCommercant
            INNER JOIN ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
            INNER JOIN villes ON villes.IdVille = commercants.IdVille
            WHERE
            ass_commercants_users.IdUser = '" . $prmData . "' AND
            ass_commercants_users.Favoris = 1


        ");
        return $qryCommercant->result();
    }


    function verify_favoris($IdUser, $IdCommercant)
    {
        $qryUtilisateur = $this->db->query("
            SELECT
            ass_commercants_users.IdAssCommercantUser,
            ass_commercants_users.IdUser,
            ass_commercants_users.IdCommercant,
            ass_commercants_users.Favoris,
            ass_commercants_users.Newsletter
            FROM
            ass_commercants_users
            WHERE
            ass_commercants_users.IdUser = '" . $IdUser . "' AND
            ass_commercants_users.IdCommercant = '" . $IdCommercant . "'
            LIMIT 1
            "
        );
        if ($qryUtilisateur->num_rows() > 0) {
            $Res = $qryUtilisateur->result();
            return $Res[0];
        } else {
            return 0;
        }
    }

    /*
    function Insert($prmData) {
        $this->db->insert("users", $prmData);
        return $this->db->insert_id();
    }
    */

    function getById_client($_id)
    {
        $zSqlBonPlan = "SELECT *  FROM users WHERE IdUser =" . $_id;
        $zQuery = $this->db->query($zSqlBonPlan);
        return $zQuery->row();
    }

    function getById_user($_id)
    {
        $zSqlBonPlan = "SELECT users.*,villes.ville_nom  FROM users 
        INNER JOIN villes ON villes.IdVIlle = users.IdVille
        WHERE IdUser =" . $_id;
        $zQuery = $this->db->query($zSqlBonPlan);
        return $zQuery->row();
    }

    function getById_user_idcart_nul()
    {
        $qryUtilisateur = $this->db->query("
            SELECT
            users.IdUser,
            users.user_ionauth_id,
            card.id
            FROM
            users
            LEFT OUTER JOIN card ON users.user_ionauth_id = card.id_ionauth_user
            WHERE card.id IS NULL
        ");
        if ($qryUtilisateur->num_rows() > 0) {
            return $qryUtilisateur->result();
        }
    }

    function getMailByIdUser($_id)
    {
        $zSqlBonPlan = "
            SELECT
            users_ionauth.username as mail,
            users_ionauth.email as mail2,
            users_ionauth.id
            FROM
            users_ionauth
            WHERE users_ionauth.username <> '' AND users_ionauth.email <> '' AND users_ionauth.active = '1' AND users_ionauth.id = $_id";
            
        $zQuery = $this->db->query($zSqlBonPlan);
        return $zQuery->row();
    }

    function getFirstId()
    {
        $zSqlBonPlan = "SELECT id FROM users_ionauth ORDER BY id ASC LIMIT 1 ";
        $zQuery = $this->db->query($zSqlBonPlan);
        return $zQuery->row()->id;
    }

    function getLastId()
    {
        $zSqlBonPlan = "SELECT id FROM users_ionauth ORDER BY id DESC LIMIT 1 ";
        $zQuery = $this->db->query($zSqlBonPlan);
        return $zQuery->row()->id;
    }

    function getAllId(){
        $requete = "SELECT id FROM users_ionauth WHERE users_ionauth.username <> '' AND users_ionauth.email <> '' AND users_ionauth.active = '1'";
        $res = $this->db->query($requete);
        return $res->result();
    }

    function verifUserComNewsletter($iduser, $idcom)
    {
        $zSqlquery = "SELECT
            ass_commercants_users.IdAssCommercantUser,
            ass_commercants_users.IdUser,
            ass_commercants_users.IdCommercant,
            ass_commercants_users.Favoris,
            ass_commercants_users.Newsletter
            FROM
            ass_commercants_users
            WHERE
            ass_commercants_users.IdUser =  '" . $iduser . "' AND
            ass_commercants_users.IdCommercant =  '" . $idcom . "' 
            LIMIT 1
            ";
        //echo $zSqlquery;
        $zQuery = $this->db->query($zSqlquery);

        return $zQuery->row();
    }

    function deleteUserComNewsletter($iditem)
    {
        $zSqlquery = "
            DELETE FROM `ass_commercants_users` WHERE `ass_commercants_users`.`IdAssCommercantUser` = " . $iditem . "
            ";
        //echo $zSqlquery;
        $zQuery = $this->db->query($zSqlquery);

        //return $zQuery->row() ;
    }

    function verifier_mailUser($mailUser)
    {
        $mailUser = trim($mailUser);
        $query = "SELECT
                *
                FROM
                users
                WHERE
                users.Email =  '" . $mailUser . "'
                ";

        $qryUtilisateur = $this->db->query($query);
        return $qryUtilisateur->result();

    }

    function verifier_mailUserPro($mailUser)
    {
        $mailUser = trim($mailUser);
        $query = "SELECT
                *
                FROM
                commercants
                WHERE
                commercants.Email =  '" . $mailUser . "'
                ";

        $qryUtilisateur = $this->db->query($query);
        return $qryUtilisateur->result();

    }

    function verifier_mailUser_ionauth($mailUser)
    {
        $mailUser = trim($mailUser);
        $query = "SELECT
                *
                FROM
                users_ionauth
                WHERE
                users_ionauth.email =  '" . $mailUser . "'
                ";

        $qryUtilisateur = $this->db->query($query);
        return $qryUtilisateur->result();

    }

    function verifier_loginUser($loginUser)
    {
        $loginUser = trim($loginUser);
        $query = "SELECT
                *
                FROM
                users
                WHERE
                users.Login =  '" . $loginUser . "'
                ";

        $qryUtilisateur = $this->db->query($query);
        return $qryUtilisateur->result();

    }

    function verifier_loginUserPro($loginUser)
    {
        $loginUser = trim($loginUser);
        $query = "SELECT
                *
                FROM
                commercants
                WHERE
                commercants.Login =  '" . $loginUser . "'
                ";

        $qryUtilisateur = $this->db->query($query);
        return $qryUtilisateur->result();

    }

    function verifier_loginUser_ionauth($loginUser)
    {
        $loginUser = trim($loginUser);
        $query = "SELECT
                *
                FROM
                users_ionauth
                WHERE
                users_ionauth.username =  '" . $loginUser . "'
                ";

        $qryUtilisateur = $this->db->query($query);
        return $qryUtilisateur->result();

    }


    function edit_geodistance($iduser, $value)
    {
        $zSql_geodistance = "UPDATE users SET geodistance='" . $value . "' WHERE IdUser=" . $iduser;
        $zresult_geodistance = $this->db->query($zSql_geodistance);
    }

    function getEmailActive(){
        $requete = "SELECT * FROM `users_ionauth` WHERE `email` <> '' AND `username` <> '' AND `active` = '1'";
        $res = $this->db->query($requete);
        return $res->result();
    }

    public function get_client_id($iduser){
        $this->db->where('UserRole',0);
        $this->db->where('user_ionauth_id',$iduser);
        $res=$this->db->get('users');
        return $res->row();
    }
    public function getbyids($id){
        $this->db->where('IdUser',$id);
        $res=$this->db->get('users');
        return $res->row();
    }
    public function get_all_statistiques(){
        $res=$this->db->get('statistiques');
        return $res->result();
    }
    public function get_all_statistiques_by_date($date){
        // $this->db->where('date',$date);
        // $res=$this->db->get('statistiques');
        // return $res->result();

        $requete = "SELECT * FROM `statistiques` WHERE `date`='".$date."'";
        // $requete = "SELECT * FROM `statistiques` WHERE `date`='2019-09-21'";
        $res = $this->db->query($requete);
        return $res->result();
    }
    public function ajout_nbre($prmData){
        $this->db->insert("statistiques", $prmData);
        return $this->db->insert_id();
    }
}