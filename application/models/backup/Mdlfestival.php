<?php

class mdlfestival extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function getById($id = 0)
    {
        $Sql = "select * from festival where id =" . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function GetByIdCommercant_no_filter_ville($IdCommercant = 0)
    {
        $sql_qr = "
            SELECT *
            FROM
                festival
            WHERE IdCommercant = '" . $IdCommercant . "'
            AND IsActif <> '3'
            ";


        $sql_qr .= "
            ORDER BY order_partner ASC
        ";

        $qry = $this->db->query($sql_qr);
        if ($qry->num_rows() > 0) {
            return $qry->result();
        }
    }

    function GetByIdCommercant($IdCommercant = 0)
    {
        $sql_qr = "
            SELECT *
            FROM
                festival
            WHERE IdCommercant = '" . $IdCommercant . "'
            AND IsActif <> '3'
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sql_qr .= " AND festival.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sql_qr .= " AND festival.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sql_qr .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sql_qr .= " festival.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sql_qr .= " OR ";
            }
            $sql_qr .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sql_qr .= " AND festival.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        $sql_qr .= "
            ORDER BY order_partner ASC
        ";

        $qry = $this->db->query($sql_qr);
        if ($qry->num_rows() > 0) {
            return $qry->result();
        }
    }

    function GetAll()
    {
        $qryfestival = $this->db->query("
            SELECT *
            FROM   
                festival
            ORDER BY id desc
        ");
        if ($qryfestival->num_rows() > 0) {
            return $qryfestival->result();
        }
    }

    function delete($prmId)
    {

        $qryBonplan = $this->db->query("DELETE FROM festival WHERE id = ?", $prmId);
        return $qryBonplan;
    }

    function insert($prmData)
    {
        if ($prmData['IdVille_localisation'] == '') $prmData['IdVille_localisation'] = null;
        if ($prmData['date_depot'] == '--' || $prmData['date_depot'] == '') $prmData['date_depot'] = null;
        $this->db->insert("festival", $prmData);
        return $this->db->insert_id();
    }

    function update($prmData)
    {
        if ($prmData['IdVille_localisation'] == '') $prmData['IdVille_localisation'] = null;
        $this->db->where("id", $prmData["id"]);
        $this->db->update("festival", $prmData);
        $objAnnonce = $this->getById($prmData["id"]);
        return $objAnnonce->id;
    }


    function effacerdoc_affiche($_iFestivalId)
    {
        $zSql = "UPDATE festival SET doc_affiche='' WHERE id=" . $_iFestivalId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerphoto1($_iFestivalId)
    {
        $zSql = "UPDATE festival SET photo1='' WHERE id=" . $_iFestivalId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerphoto2($_iFestivalId)
    {
        $zSql = "UPDATE festival SET photo2='' WHERE id=" . $_iFestivalId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerphoto3($_iFestivalId)
    {
        $zSql = "UPDATE festival SET photo3='' WHERE id=" . $_iFestivalId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerphoto4($_iFestivalId)
    {
        $zSql = "UPDATE festival SET photo4='' WHERE id=" . $_iFestivalId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerphoto5($_iFestivalId)
    {
        $zSql = "UPDATE festival SET photo5='' WHERE id=" . $_iFestivalId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerpdf($_iFestivalId)
    {
        $zSql = "UPDATE festival SET pdf='' WHERE id=" . $_iFestivalId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerautre_doc_1($_iFestivalId)
    {
        $zSql = "UPDATE festival SET autre_doc_1='' WHERE id=" . $_iFestivalId;
        $zQuery = $this->db->query($zSql);
    }

    function effacerautre_doc_2($_iFestivalId)
    {
        $zSql = "UPDATE festival SET autre_doc_2='' WHERE id=" . $_iFestivalId;
        $zQuery = $this->db->query($zSql);
    }


    function ListeFestivalRecherche($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0")
    {

        $zSqlListePartenaire = "
            SELECT
                festival.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_subcateg.subcateg,
                commercants.NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                festival
                LEFT OUTER JOIN villes ON villes.IdVille = festival.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = festival.article_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = festival.article_subcategid
                INNER JOIN commercants ON commercants.IdCommercant = festival.IdCommercant
                

            WHERE
                (festival.IsActif = '1' OR festival.IsActif = '2') 
                AND commercants.IsActif = '1' 
               
                AND (festival.id <> '0' OR festival.id <> NULL OR festival.id <> 0) 

        ";

        if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }

        if ($IdUsers_ionauth != "0" && $IdUsers_ionauth != "" && $IdUsers_ionauth != NULL && $IdUsers_ionauth != 0) {
            $zSqlListePartenaire .= " AND festival.IdUsers_ionauth = '" . $IdUsers_ionauth . "'";
        }

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $date_of_day . "' AND festival.date_fin <= '" . $date_of_day . "' ) OR ( festival.date_debut IS NULL AND festival.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $next_saturday_day . "' AND festival.date_fin <= '" . $next_sunday_day . "') OR (festival.date_fin >= '" . $next_saturday_day . "' AND festival.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $last_sunday_day . "' AND festival.date_fin <= '" . $next_saturday_day . "') OR (festival.date_fin >= '" . $last_sunday_day . "' AND festival.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $last_sunday_of_next_monday . "' AND festival.date_fin <= '" . $next_saturday_of_next_monday . "') OR (festival.date_fin >= '" . $last_sunday_of_next_monday . "' AND festival.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_month . "' AND festival.date_fin <= '" . $last_day_month . "') OR (festival.date_fin >= '" . $first_day_month . "' AND festival.date_fin <= '" . $last_day_month . "' AND festival.date_fin >= '" . $date_of_day . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin  <= '" . $last_day_user_month . "') OR (festival.date_fin  >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (festival.date_debut>= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin  >= '" . $first_day_user_month . "' AND festival.date_fin  <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (festival.date_debut>= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin  >= '" . $first_day_user_month . "' AND festival.date_fin  <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (festival.date_debut>= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin  <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (festival.date_debut>= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (festival.date_debut>= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin  >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (festival.date_debut>= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin  <= '" . $last_day_user_month . "') )";

        }

        if ($_iVilleId != 0) {
            $zSqlListePartenaire .= " AND festival.IdVille_localisation = '" . $_iVilleId . "'";
        }
        if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null) {
            $zSqlListePartenaire .= " AND festival.IdVille_localisation =" . $_iVilleId;
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " festival.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlListePartenaire .= " AND festival.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlListePartenaire .= " AND festival.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlListePartenaire .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlListePartenaire .= " festival.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListePartenaire .= " OR ";
            }
            $zSqlListePartenaire .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlListePartenaire .= " AND festival.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE



        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            //$zSqlListePartenaire .= " AND article_datetime.date_debut = '" . $inputDatedebut . "'";
            $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $inputDatedebut . "' OR festival.date_debut IS NULL )";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            //$zSqlListePartenaire .= " AND article_datetime.date_fin = '" . $inputDatefin . "'";
            $zSqlListePartenaire .= " AND ( festival.date_fin <= '" . $inputDatefin . "' )";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " festival.article_categid = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " festival.article_categid = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR festival.article_categid = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }

        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " festival.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " festival.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR festival.article_subcategid = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }

        if ($zSql_all_categ_request != "" && $zSql_all_subcateg_request == "") $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
        else if ($zSql_all_categ_request == "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND " . $zSql_all_subcateg_request;
        else if ($zSql_all_categ_request != "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ( " . $zSql_all_categ_request . " OR " . $zSql_all_subcateg_request . " ) ";


        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = str_replace("'", "\'", $SearchedWordsString);
        $SearchedWordsString = str_replace('"', '\"', $SearchedWordsString);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);

        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(festival.nom_societe ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_categ.category ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_subcateg.subcateg ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.organisateur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.adresse_localisation_diffuseur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }

        //SORTEZ.ORG => NO BASIC PARTNER ACCOUT
        $zSqlListePartenaire .= " AND commercants.user_ionauth_id IN (SELECT users_groups_ionauth.user_ionauth_id FROM users_groups_ionauth WHERE users_groups_ionauth.group_ionauth_id = 4 OR users_groups_ionauth.group_ionauth_id = 5) ";
        //SORTEZ.ORG => NO BASIC PARTNER ACCOUT


        // demo account commercant should not appear on annuaire list
        //$zSqlListePartenaire .= " AND commercants.IdCommercant <> '301298' ";


        $zSqlListePartenaire .= " group by festival.id, festival.id ";

        if ($iOrderBy == '1') $iOrderBy_value = " order by festival.last_update desc ";
        else if ($iOrderBy == '2') $iOrderBy_value = " order by festival.accesscount desc ";
        else $iOrderBy_value = " order by festival.date_debut,festival.date_fin, festival.date_debut asc ";

        $zSqlListePartenaire .= $iOrderBy_value;

        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //die($zSqlListePartenaire);
        //echo "<br/>".$_limitstart." / ".$_limitend;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListeAgenda');
        if ($inputQuand == "09") log_message('error', 'william festival MAIN SQL : ' . $zSqlListePartenaire);


        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }


    function listeFestivalRecherche_filtre_festival_perso($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0", $referencement_agenda = 1)
    {

        $zSqlListePartenaire = "
            SELECT
                festival_datetime.id as datetime_id, 
                festival_datetime.date_debut as datetime_debut, 
                festival_datetime.date_fin as datetime_fin, 
                festival_datetime.heure_debut as datetime_heure_debut,
                festival.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_subcateg.subcateg,
                commercants.NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                festival
                LEFT OUTER JOIN villes ON villes.IdVille = festival.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = festival.article_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = festival.article_subcategid
                INNER JOIN commercants ON commercants.IdCommercant = festival.IdCommercant
                LEFT OUTER JOIN article_datetime ON article_datetime.article_id = festival.id

            WHERE
                ( 
                (festival.IsActif = '1' OR festival.IsActif = '2') 
                AND commercants.IsActif = 1 
                ";
        if ($referencement_agenda==1)   $zSqlListePartenaire .= " AND commercants.referencement_festival = ".$referencement_festival." ";

        $zSqlListePartenaire .= "
                AND (festival.id <> '0' OR festival.id <> NULL OR festival.id <> 0) 

        ";

        if (is_array($inputIdCommercant)) {
            $zSql_all_icommercant_request = "";
            $zSql_all_icommercant_request .= " (";
            if (sizeof($inputIdCommercant) == 1) {
                if ($inputIdCommercant[0] != "0" && $inputIdCommercant[0] != "" && $inputIdCommercant[0] != NULL && $inputIdCommercant[0] != 0) {
                    $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                } else {
                    $zSql_all_icommercant_request .= " 0=0 ";
                }
            } else {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                for ($i_iCommercantId = 1; $i_iCommercantId < count($inputIdCommercant); $i_iCommercantId++) {
                    $zSql_all_icommercant_request .= " OR commercants.IdCommercant = '" . $inputIdCommercant[$i_iCommercantId] . "' ";
                }
            }
            $zSql_all_icommercant_request .= " ) ";
            //$zSqlListePartenaire .= $zSql_all_icommercant_request;
        } else if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }

        if ($IdUsers_ionauth != "0" && $IdUsers_ionauth != "" && $IdUsers_ionauth != NULL && $IdUsers_ionauth != 0) {
            $zSqlListePartenaire .= " AND festival.IdUsers_ionauth = '" . $IdUsers_ionauth . "'";
        }

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

            /*
            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $date_of_day . "' AND festival.date_fin <= '" . $date_of_day . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $next_saturday_day . "' AND festival.date_fin <= '" . $next_sunday_day . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $next_saturday_day . "' AND festival.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $last_sunday_day . "' AND festival.date_fin <= '" . $next_saturday_day . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $last_sunday_day . "' AND festival.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $last_sunday_of_next_monday . "' AND festival.date_fin <= '" . $next_saturday_of_next_monday . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $last_sunday_of_next_monday . "' AND festival.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_month . "' AND festival.date_fin <= '" . $last_day_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_month . "' AND festival.date_fin <= '" . $last_day_month . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            */
            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (festival_datetime.date_debut >= '" . $date_of_day . "' AND festival_datetime.date_fin <= '" . $date_of_day . "' ) OR ( festival_datetime.date_debut IS NULL AND festival_datetime.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (festival_datetime.date_debut >= '" . $next_saturday_day . "' AND festival_datetime.date_fin <= '" . $next_sunday_day . "') OR (festival_datetime.date_fin >= '" . $next_saturday_day . "' AND festival_datetime.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (festival_datetime.date_debut >= '" . $last_sunday_day . "' AND festival_datetime.date_fin <= '" . $next_saturday_day . "') OR (festival_datetime.date_fin >= '" . $last_sunday_day . "' AND festival_datetime.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (festival_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND festival_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') OR (festival_datetime.date_fin >= '" . $last_sunday_of_next_monday . "' AND festival_datetime.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (festival_datetime.date_debut >= '" . $first_day_month . "' AND festival_datetime.date_fin <= '" . $last_day_month . "') OR (festival_datetime.date_fin >= '" . $first_day_month . "' AND festival_datetime.date_fin <= '" . $last_day_month . "' AND festival_datetime.date_fin >= '" . $date_of_day . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (festival_datetime.date_debut >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') OR (festival_datetime.date_fin >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (festival_datetime.date_debut >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') OR (festival_datetime.date_fin >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (festival_datetime.date_debut >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') OR (festival_datetime.date_fin >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (festival_datetime.date_debut >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') OR (festival_datetime.date_fin >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (festival_datetime.date_debut >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') OR (festival_datetime.date_fin >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (festival_datetime.date_debut >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') OR (festival_datetime.date_fin >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (festival_datetime.date_debut >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') OR (festival_datetime.date_fin >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (festival_datetime.date_debut >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') OR (festival_datetime.date_fin >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (festival_datetime.date_debut >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') OR (festival_datetime.date_fin >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (festival_datetime.date_debut >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') OR (festival_datetime.date_fin >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (festival_datetime.date_debut >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') OR (festival_datetime.date_fin >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (festival_datetime.date_debut >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') OR (festival_datetime.date_fin >= '" . $first_day_user_month . "' AND festival_datetime.date_fin <= '" . $last_day_user_month . "') )";

        }

        if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null && $_iVilleId != "0") {
            if (is_array($_iVilleId)) {
                $zSql_all_iville_request = "";
                $zSql_all_iville_request .= " (";
                if (sizeof($_iVilleId) == 1) {
                    if ($_iVilleId[0] != 0 && $_iVilleId[0] != "" && $_iVilleId[0] != null && $_iVilleId[0] != "0") {
                        $zSql_all_iville_request .= " festival.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    } else {
                        $zSql_all_iville_request .= " 0=0 ";
                    }
                } else {
                    $zSql_all_iville_request .= " festival.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    for ($i_iVilleId = 1; $i_iVilleId < count($_iVilleId); $i_iVilleId++) {
                        $zSql_all_iville_request .= " OR festival.IdVille_localisation = '" . $_iVilleId[$i_iVilleId] . "' ";
                    }
                }
                $zSql_all_iville_request .= " ) ";
                ////$this->firephp->log($zSql_all_iville_request, 'zSql_all_iville_request');
                //$zSqlListePartenaire .= $zSql_all_iville_request;
            }
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " festival.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            $zSqlListePartenaire .= " AND festival_datetime.date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            $zSqlListePartenaire .= " AND festival_datetime.date_fin = '" . $inputDatefin . "'";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " festival.article_categid = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " festival.article_categid = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR festival.article_categid = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }

        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " festival.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " festival.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR festival.article_subcategid = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }

        /*if($zSql_all_categ_request != "" && $zSql_all_subcateg_request == "") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;
        else if($zSql_all_categ_request == "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ".$zSql_all_subcateg_request;
        else if($zSql_all_categ_request != "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ( ".$zSql_all_categ_request." OR ".$zSql_all_subcateg_request." ) ";*/

        //if ($zSql_all_categ_request!="") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;


        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);

        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(festival.nom_societe ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival_categ.category ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival_subcateg.subcateg ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.organisateur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.adresse_localisation_diffuseur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }


        $zSqlListePartenaire .= " ) ";

        if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") || (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) {
            $zSqlListePartenaire .= " AND ( ";

            if (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") $zSqlListePartenaire .= $zSql_all_icommercant_request;
            if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") && (isset($zSql_all_iville_request) && $zSql_all_iville_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_iville_request;
            else if (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") $zSqlListePartenaire .= $zSql_all_iville_request;
            if (((isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "")) && (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
            else if (isset($zSql_all_categ_request) && $zSql_all_categ_request != "") $zSqlListePartenaire .= $zSql_all_categ_request;

            $zSqlListePartenaire .= " ) ";
        }

        $zSqlListePartenaire .= " AND (festival.IsActif = '1' OR festival.IsActif = '2') ";

        $zSqlListePartenaire .= " group by festival_datetime.id ";

        if ($iOrderBy == '1') $iOrderBy_value = " order by festival.last_update desc ";
        else if ($iOrderBy == '2') $iOrderBy_value = " order by festival.accesscount desc ";
        else $iOrderBy_value = " order by article_datetime.date_debut,article_datetime.date_fin, festival.date_debut asc ";

        $zSqlListePartenaire .= $iOrderBy_value;

        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $_limitend;
        //echo $zSqlListePartenaire;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListePartenaire');
        //die("<br/>STOP");

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }


    function GetFestivalCategorie_ByIdCommercant($_iCommercantId)
    {

        $sqlcat = "
            SELECT
            agenda_categ.agenda_categid,
            agenda_categ.agenda_typeid,
            agenda_categ.category,
            COUNT(festival.id) as nb_festival
            FROM
            agenda_categ
            INNER JOIN festival ON agenda_categ.agenda_categid = festival.article_categid
            WHERE
            (festival.IsActif = '1' OR festival.IsActif = '2')
            AND festival.IdCommercant = '" . $_iCommercantId . "' ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcat .= " AND festival.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcat .= " AND festival.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcat .= " festival.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
            }
            $sqlcat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcat .= " AND festival.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        $sqlcat .= "
            GROUP BY
            agenda_categ.agenda_categid
            ";

        $qryCategorie = $this->db->query($sqlcat);

        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetFestivalCategorie()
    {
        $queryCat = "
            SELECT
            agenda_categ.agenda_categid,
            agenda_categ.agenda_typeid,
            agenda_categ.category,
            COUNT(festival.id) as nb_festival
            FROM
            agenda_categ
            INNER JOIN festival ON agenda_categ.agenda_categid = festival.article_categid
            WHERE
            (festival.IsActif = '1' OR
            festival.IsActif = '2')
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $queryCat .= " AND festival.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $queryCat .= " AND festival.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $queryCat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $queryCat .= " festival.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $queryCat .= " OR ";
            }
            $queryCat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $queryCat .= " AND festival.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        $queryCat .= "
            GROUP BY
            agenda_categ.agenda_categid
            ";

        $qryCategorie = $this->db->query($queryCat);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetFestivalCategorie_by_params($inputQuand, $inputDatedebut, $inputDatefin, $_iDepartementId, $_iVilleId)
    {
        $zSqlListePartenaire = "
            SELECT
                agenda_categ.agenda_categid,
                agenda_categ.category,
                COUNT(festival.id) AS nb_festival
                ";

        $zSqlListePartenaire .= "
            FROM
                festival
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = 34
                

            WHERE
                (festival.IsActif = '1' OR festival.IsActif = '2')

        ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlListePartenaire .= " AND festival.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlListePartenaire .= " AND festival.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlListePartenaire .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlListePartenaire .= " festival.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListePartenaire .= " OR ";
            }
            $zSqlListePartenaire .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlListePartenaire .= " AND festival.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        if ($inputQuand != "0" && $inputQuand != "" && $inputQuand != null) {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $date_of_day . "' AND festival.date_fin <= '" . $date_of_day . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $date_of_day . "') )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $next_saturday_day . "' AND festival.date_fin <= '" . $next_sunday_day . "' )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $last_sunday_day . "' AND festival.date_fin <= '" . $next_saturday_day . "' )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $last_sunday_of_next_monday . "' AND festival.date_fin <= '" . $next_saturday_of_next_monday . "' )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_month . "' AND festival.date_fin <= '" . $last_day_month . "' )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
        }

        if ($_iVilleId != "0" && $_iVilleId != "" && $_iVilleId != null) {
            $zSqlListePartenaire .= " AND festival.IdVille_localisation = '" . $_iVilleId . "'";
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " festival.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null && $inputDatedebut != "0") {
            $zSqlListePartenaire .= " AND festival.date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null && $inputDatefin != "0") {
            $zSqlListePartenaire .= " AND festival.date_fin = '" . $inputDatefin . "'";
        }

        $zSqlListePartenaire .= " group by agenda_categ.agenda_categid ";

        //$iOrderBy_value = " order by festival.date_debut asc ";//
        $iOrderBy_value = " order by agenda_categ.category asc ";

        $zSqlListePartenaire .= $iOrderBy_value;

        //echo $zSqlListePartenaire;
        //log_message('debug', $zSqlListePartenaire);
        ////$this->firephp->log($zSqlListePartenaire, 'query_category');

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();


    }


    function GetById_IsActif($id = 0)
    {
        $qry = $this->db->query("
            SELECT
            festival.*,
            villes.Nom AS ville,
            agenda_categ.category,
            agenda_categ.agenda_categid,
            agenda_subcateg.subcateg

            FROM
                festival
            LEFT OUTER JOIN villes ON villes.IdVille = festival.IdVille_localisation
            INNER JOIN agenda_categ ON agenda_categ.agenda_categid = festival.article_categid
            LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = festival.article_subcategid

            WHERE id = '" . $id . "'
            AND (IsActif = '1' OR IsActif = '2')
            LIMIT 1
        ");
        return $qry->row();
    }

    function GetById_preview($id = 0)
    {
        $qry = $this->db->query("
            SELECT
            festival.*,
            villes.Nom AS ville,
            agenda_categ.category,
            agenda_categ.agenda_categid,
            agenda_subcateg.subcateg

            FROM
                festival
            LEFT OUTER JOIN villes ON villes.IdVille = festival.IdVille_localisation
            INNER JOIN agenda_categ ON agenda_categ.agenda_categid = festival.article_categid
            LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = festival.article_subcategid

            WHERE id = '" . $id . "'
            LIMIT 1
        ");
        return $qry->row();
    }

    function Increment_accesscount($id = 0)
    {
        $current_agenda = $this->getById($id);
        $last_accesscount_nb = intval($current_agenda->accesscount);
        $last_accesscount_nb_total = $last_accesscount_nb + 1;
        $zSql = "UPDATE festival SET accesscount='" . $last_accesscount_nb_total . "' WHERE id=" . $id;
        $this->db->query($zSql);
    }

    function GetByIdCommercantLimit($IdCommercant = 0, $_limitstart = 0, $_limitend = 10000000,$_iCategorieId = 0, $inputQuand = "0")
    {

        $query = "
            SELECT
                festival.id as datetime_id, 
                festival.date_debut as datetime_debut, 
                festival.date_fin as datetime_fin, 
                festival.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_subcateg.subcateg
            FROM
                festival
                LEFT OUTER JOIN villes ON villes.IdVille = festival.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = festival.article_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = festival.article_subcategid
                LEFT OUTER JOIN festival ON festival.id = festival.id
            WHERE
                festival.IdCommercant = '" . $IdCommercant . "'
            AND
                festival.IsActif = '1' OR festival.IsActif = '2'
            ";

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

            /* if ($inputQuand == "01") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
             if ($inputQuand == "02") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
             if ($inputQuand == "03") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
             if ($inputQuand == "04") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
             if ($inputQuand == "05") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
             if ($inputQuand == "06") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
             if ($inputQuand == "07") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
             if ($inputQuand == "08") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
             if ($inputQuand == "09") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
             if ($inputQuand == "10") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
             if ($inputQuand == "11") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
             if ($inputQuand == "12") $query .= " AND ( (article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') OR (article_datetime.date_fin >= '" . $first_day_user_month . "' AND article_datetime.date_fin <= '" . $last_day_user_month . "') )";
         }*/

            if (isset($_iCategorieId) && $_iCategorieId != 0) {
                if (is_array($_iCategorieId)) $_iCategorieId = $_iCategorieId[0];
                $query .= " AND festival.article_categid = '" . $_iCategorieId . "' ";
            }

            $query .= "    
            group by festival.id, festival.id
            ORDER BY festival.order_partner, festival.date_debut, festival.date_debut ASC
            LIMIT " . $_limitstart . "," . $_limitend . "
        ";
            log_message('error', 'partner_festival_list_filter_00 : ' . $query);
            $qry = $this->db->query($query);
            if ($qry->num_rows() > 0) {
                return $qry->result();
            }
        }
    }


    /*function GetFestivalNbByMonth($month, $_iCommercantId)
    {
        $date_of_day = date("Y-m-d");
        $time_date = strtotime($date_of_day);
        $next_sunday = strtotime('next sunday, 12pm', $time_date);
        $last_sunday = strtotime('last sunday, 12pm', $time_date);
        $next_saturday = strtotime('next saturday, 11:59am', $time_date);
        $next_monday = strtotime('next monday, 11:59am', $time_date);
        $format_date = 'Y-m-d';
        $next_sunday_day = date($format_date, $next_sunday);
        $last_sunday_day = date($format_date, $last_sunday);
        $next_saturday_day = date($format_date, $next_saturday);
        $next_monday_day = date($format_date, $next_monday);
        $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
        $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
        $first_day_month = date('Y-m-01', strtotime($date_of_day));
        $last_day_month = date('Y-m-t', strtotime($date_of_day));
        $first_day_user_month = date('Y-' . $month . '-01', $time_date);
        $last_day_user_month = date('Y-' . $month . '-t', $time_date);

        $zSqlListePartenaire = "
            SELECT
                festival.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_subcateg.subcateg,
                commercants.NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                festival
                LEFT OUTER JOIN villes ON villes.IdVille = festival.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = festival.article_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = festival.article_subcategid
                INNER JOIN commercants ON commercants.IdCommercant = festival.IdCommercant
                LEFT OUTER JOIN festival ON festival.id = festival.id
            WHERE
                (festival.IsActif = '1' OR festival.IsActif = '2') ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $zSqlListePartenaire .= " AND article.IdVille_localisation = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $zSqlListePartenaire .= " AND article.IdVille_localisation = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $zSqlListePartenaire .= " AND commercants.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE

        if (  $_iCommercantId != "0" && $_iCommercantId != "" && $_iCommercantId != NULL) {
            $zSqlListePartenaire .= "
                AND festival.IdCommercant = '" . $_iCommercantId . "'
                ";
        }

        if ($month != "0" && $month != "" && $month != NULL) {
            if ($month == "101") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $date_of_day . "' AND festival.date_fin <= '" . $date_of_day . "' )";
            if ($month == "202") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $next_saturday_day . "' AND festival.date_fin <= '" . $next_sunday_day . "' )";
            if ($month == "303") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $last_sunday_day . "' AND festival.date_fin <= '" . $next_saturday_day . "' )";
            if ($month == "404") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "' )";
            if ($month == "505") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "' )";

            if ($month == "01") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($month == "02") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($month == "03") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($month == "04") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($month == "05") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($month == "06") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($month == "07") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($month == "08") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($month == "09") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($month == "10") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($month == "11") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
            if ($month == "12") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_user_month . "' AND article_datetime.date_debut <= '" . $last_day_user_month . "' )";
        }
        //////$this->firephp->log($zSqlListePartenaire, 'date_by_month_sql');
        //log_message('error', 'list_by_month_partner_article : '.$zSqlListePartenaire);
        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }*/


    function GetByIdCommercant_festivalActif($IdCommercant = 0)
    {
        $sql_qr = "
            SELECT *
            FROM
                festival
            WHERE IdCommercant = '" . $IdCommercant . "'
            AND IsActif = '1'
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sql_qr .= " AND festival.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sql_qr .= " AND festival.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sql_qr .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sql_qr .= " festival.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sql_qr .= " OR ";
            }
            $sql_qr .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sql_qr .= " AND festival.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        $sql_qr .= "
            ORDER BY order_partner ASC
        ";

        $qry = $this->db->query($sql_qr);
        if ($qry->num_rows() > 0) {
            return $qry->result();
        }
    }


    function GetFestivalSubCateg_by_IdCateg($_iCategorieId)
    {
        $qur = "SELECT
            agenda_subcateg.agenda_subcategid,
            agenda_subcateg.subcateg,
            COUNT(festival.id) as nb_festival
            FROM
            festival
            INNER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = festival.article_subcategid
            WHERE
            (festival.IsActif = 1 OR festival.IsActif = 2)
            ";
        if (isset($_iCategorieId) && $_iCategorieId != "" && $_iCategorieId != '0')
            $qur .= "
            AND agenda_subcateg.agenda_categid = '" . $_iCategorieId . "'
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $qur .= " AND festival.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $qur .= " AND festival.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $qur .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $qur .= " festival.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $qur .= " OR ";
            }
            $qur .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $qur .= " AND festival.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        $qur .= "
            GROUP BY
            agenda_subcateg.agenda_subcategid";

        $qryCategorie = $this->db->query($qur);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetFestivalSubCateg_by_IdCateg_IdVille($_iCategorieId, $_IdVille)
    {
        $qur = "SELECT
            agenda_subcateg.agenda_subcategid,
            agenda_subcateg.subcateg,
            COUNT(festival.id) as nb_festival
            FROM
            festival
            INNER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = festival.article_subcategid
            WHERE
            (festival.IsActif = 1 OR festival.IsActif = 2)
            ";
        if (isset($_iCategorieId) && $_iCategorieId != "" && $_iCategorieId != '0')
            $qur .= "
            AND agenda_subcateg.agenda_categid = '" . $_iCategorieId . "'
            ";
        if (isset($_IdVille) && $_IdVille != "" && $_IdVille != '0')
            $qur .= "
            AND festival.IdVille_localisation = '" . $_IdVille . "'
            ";
        $qur .= "
            GROUP BY
            agenda_subcateg.agenda_subcategid";

        ////$this->firephp->log($qur, 'qur');

        $qryCategorie = $this->db->query($qur);

        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function file_manager_update($_iFestivalId = '0', $_iUserId = '0', $_iField = 'photo1', $_iValue = '')
    {
        $zSql = "UPDATE festival SET `" . $_iField . "` = '" . $_iValue . "' WHERE id = " . $_iFestivalId . " AND `IdUsers_ionauth` = '" . $_iUserId . "' ";
        $zQuery = $this->db->query($zSql);
    }

    function getByIdField($id = 0, $iField = 'photo1')
    {
        $Sql = "select " . $iField . " from festival where id =" . $id;
        $Query = $this->db->query($Sql);
        $result = $Query->row();
        if ($iField == 'photo1') return $result->photo1;
        if ($iField == 'photo2') return $result->photo2;
        if ($iField == 'photo3') return $result->photo3;
        if ($iField == 'photo4') return $result->photo4;
        if ($iField == 'photo5') return $result->photo5;
    }


    function getAlldeposantArticle($_iCommercantId = "0")
    {

        $zSqlAlldeposantAgenda = "SELECT
            commercants.NomSociete,
            commercants.IdCommercant
            FROM
            festival
            INNER JOIN commercants ON festival.IdCommercant = commercants.IdCommercant
            WHERE
            (festival.IsActif = '1' OR festival.IsActif = '2')";

        if ($_iCommercantId != "0" && $_iCommercantId != NULL && $_iCommercantId != "" && $_iCommercantId != 0) {
            $zSqlAlldeposantAgenda .= " AND festival.IdCommercant = '" . $_iCommercantId . "'";
        }

        $zSqlAlldeposantAgenda .= " GROUP BY
            festival.IdCommercant";

        $zQueryAlldeposantAgenda = $this->db->query($zSqlAlldeposantAgenda);
        return $zQueryAlldeposantAgenda->result();
    }

    function getWhere($where=''){
        $sql = "
            SELECT
                *
            FROM
                festival
            WHERE 
              0=0 
        ";
        if (isset($where) && $where != '') $sql .= " AND ".$where;
        $request = $this->db->query($sql);
        if($request->num_rows() > 0) {
            return $request->result();
        }
    }

public function get_agenda($id_festival){
        $this->db->where("IdFestival",$id_festival);
        $query=$this->db->get("agenda");
        return $query->result();
}






    function listeAgendaRecherche($id_festival,$_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0")
    {

        $zSqlListePartenaire = "
            SELECT
                festival.id as datetime_id, 
                festival.date_debut as datetime_debut, 
                festival.date_fin as datetime_fin, 
               
                festival.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_subcateg.subcateg,
                commercants.NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                festival
                LEFT OUTER JOIN villes ON villes.IdVille = festival.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = festival.article_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = festival.article_subcategid
                INNER JOIN commercants ON commercants.IdCommercant = festival.IdCommercant

            WHERE
                
              festival.id = '$id_festival'
                

        ";

        if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }

        if ($IdUsers_ionauth != "0" && $IdUsers_ionauth != "" && $IdUsers_ionauth != NULL && $IdUsers_ionauth != 0) {
            $zSqlListePartenaire .= " AND festival.IdUsers_ionauth = '" . $IdUsers_ionauth . "'";
        }

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $date_of_day . "' AND festival.date_fin <= '" . $date_of_day . "' ) OR ( festival.date_debut IS NULL AND festival.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $next_saturday_day . "' AND festival.date_fin <= '" . $next_sunday_day . "') OR (festival.date_fin >= '" . $next_saturday_day . "' AND festival.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $last_sunday_day . "' AND festival.date_fin <= '" . $next_saturday_day . "') OR (festival.date_fin >= '" . $last_sunday_day . "' AND festival.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $last_sunday_of_next_monday . "' AND festival.date_fin <= '" . $next_saturday_of_next_monday . "') OR (festival.date_fin >= '" . $last_sunday_of_next_monday . "' AND festival.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_month . "' AND festival.date_fin <= '" . $last_day_month . "') OR (festival.date_fin >= '" . $first_day_month . "' AND festival.date_fin <= '" . $last_day_month . "' AND festival.date_fin >= '" . $date_of_day . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";

        }

        if ($_iVilleId != 0) {
            $zSqlListePartenaire .= " AND festival.IdVille_localisation = '" . $_iVilleId . "'";
        }
        if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null) {
            $zSqlListePartenaire .= " AND festival.IdVille_localisation =" . $_iVilleId;
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " festival.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlListePartenaire .= " AND festival.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlListePartenaire .= " AND festival.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlListePartenaire .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlListePartenaire .= " festival.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListePartenaire .= " OR ";
            }
            $zSqlListePartenaire .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlListePartenaire .= " AND festival.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            //$zSqlListePartenaire .= " AND festival.date_debut = '" . $inputDatedebut . "'";
            $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $inputDatedebut . "' OR festival.date_debut IS NULL )";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            //$zSqlListePartenaire .= " AND festival.date_fin = '" . $inputDatefin . "'";
            $zSqlListePartenaire .= " AND ( festival.date_fin <= '" . $inputDatefin . "' )";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " festival.article_categid = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " festival.article_categid = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR festival.article_categid = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }

        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " festival.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " festival.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR festival.article_subcategid = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }

        if ($zSql_all_categ_request != "" && $zSql_all_subcateg_request == "") $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
        else if ($zSql_all_categ_request == "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND " . $zSql_all_subcateg_request;
        else if ($zSql_all_categ_request != "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ( " . $zSql_all_categ_request . " OR " . $zSql_all_subcateg_request . " ) ";


        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = str_replace("'", "\'", $SearchedWordsString);
        $SearchedWordsString = str_replace('"', '\"', $SearchedWordsString);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);

        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(festival.nom_societe ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_categ.category ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_subcateg.subcateg ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.organisateur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.adresse_localisation_diffuseur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }

        //SORTEZ.ORG => NO BASIC PARTNER ACCOUT
        $zSqlListePartenaire .= " AND commercants.user_ionauth_id IN (SELECT users_groups_ionauth.user_ionauth_id FROM users_groups_ionauth WHERE users_groups_ionauth.group_ionauth_id = 4 OR users_groups_ionauth.group_ionauth_id = 5) ";
        //SORTEZ.ORG => NO BASIC PARTNER ACCOUT


        // demo account commercant should not appear on annuaire list
        //$zSqlListePartenaire .= " AND commercants.IdCommercant <> '301298' ";


        $zSqlListePartenaire .= " group by festival.id ";

        if ($iOrderBy == '1') $iOrderBy_value = " order by festival.last_update desc ";
        else if ($iOrderBy == '2') $iOrderBy_value = " order by festival.accesscount desc ";
        else $iOrderBy_value = " order by festival.date_debut,festival.date_fin asc ";

        $zSqlListePartenaire .= $iOrderBy_value;

        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $zSqlListePartenaire;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListeAgenda');

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }
    function GetArticleCategorie_ByIdCommercant($_iCommercantId)
    {

        $sqlcat = "
            SELECT
            agenda_categ.agenda_categid,
            agenda_categ.agenda_typeid,
            agenda_categ.category,
            COUNT(festival.id) as nb_article
            FROM
            agenda_categ
            INNER JOIN festival ON agenda_categ.agenda_categid = festival.article_categid
            LEFT OUTER JOIN festival ON festival.id = festival.id
            WHERE
            (festival.IsActif = '1' OR festival.IsActif = '2')
            AND festival.IdCommercant = '" . $_iCommercantId . "' ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcat .= " AND festival.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcat .= " AND festival.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcat .= " festival.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
            }
            $sqlcat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcat .= " AND festival.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        $sqlcat .= "
            GROUP BY
            agenda_categ.agenda_categid
            ";

        $qryCategorie = $this->db->query($sqlcat);

        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetArticleCategorie()
    {
        $queryCat = "
            SELECT
            agenda_categ.agenda_categid,
            agenda_categ.agenda_typeid,
            agenda_categ.category,
            COUNT(festival.id) as nb_article
            FROM
            agenda_categ
            INNER JOIN festival ON agenda_categ.agenda_categid = festival.article_categid
            WHERE
            (festival.IsActif = '1' OR
            festival.IsActif = '2')
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $queryCat .= " AND festival.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $queryCat .= " AND festival.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $queryCat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $queryCat .= " festival.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $queryCat .= " OR ";
            }
            $queryCat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $queryCat .= " AND festival.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        $queryCat .= "
            GROUP BY
            agenda_categ.agenda_categid
            ";

        $qryCategorie = $this->db->query($queryCat);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }
    function listeArticleRecherche_filtre_article_perso($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0", $referencement_agenda = 1)
    {

        $zSqlListePartenaire = "
            SELECT
                festival.id as datetime_id, 
                festival.date_debut as datetime_debut, 
                festival.date_fin as datetime_fin, 
                festival.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_subcateg.subcateg,
                commercants.NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                festival
                LEFT OUTER JOIN villes ON villes.IdVille = festival.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = festival.article_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = festival.article_subcategid
                INNER JOIN commercants ON commercants.IdCommercant = festival.IdCommercant
                

            WHERE
                ( 
                (festival.IsActif = '1' OR festival.IsActif = '2') 
                AND commercants.IsActif = 1 
                ";
        if ($referencement_agenda==1)   $zSqlListePartenaire .= " AND commercants.referencement_article = ".$referencement_festival." ";

        $zSqlListePartenaire .= "
                AND (festival.id <> '0' OR festival.id <> NULL OR festival.id <> 0) 

        ";

        if (is_array($inputIdCommercant)) {
            $zSql_all_icommercant_request = "";
            $zSql_all_icommercant_request .= " (";
            if (sizeof($inputIdCommercant) == 1) {
                if ($inputIdCommercant[0] != "0" && $inputIdCommercant[0] != "" && $inputIdCommercant[0] != NULL && $inputIdCommercant[0] != 0) {
                    $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                } else {
                    $zSql_all_icommercant_request .= " 0=0 ";
                }
            } else {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                for ($i_iCommercantId = 1; $i_iCommercantId < count($inputIdCommercant); $i_iCommercantId++) {
                    $zSql_all_icommercant_request .= " OR commercants.IdCommercant = '" . $inputIdCommercant[$i_iCommercantId] . "' ";
                }
            }
            $zSql_all_icommercant_request .= " ) ";
            //$zSqlListePartenaire .= $zSql_all_icommercant_request;
        } else if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }

        if ($IdUsers_ionauth != "0" && $IdUsers_ionauth != "" && $IdUsers_ionauth != NULL && $IdUsers_ionauth != 0) {
            $zSqlListePartenaire .= " AND festival.IdUsers_ionauth = '" . $IdUsers_ionauth . "'";
        }

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

            /*
            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $date_of_day . "' AND festival.date_fin <= '" . $date_of_day . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $next_saturday_day . "' AND festival.date_fin <= '" . $next_sunday_day . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $next_saturday_day . "' AND festival.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $last_sunday_day . "' AND festival.date_fin <= '" . $next_saturday_day . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $last_sunday_day . "' AND festival.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $last_sunday_of_next_monday . "' AND festival.date_fin <= '" . $next_saturday_of_next_monday . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $last_sunday_of_next_monday . "' AND festival.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_month . "' AND festival.date_fin <= '" . $last_day_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_month . "' AND festival.date_fin <= '" . $last_day_month . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            */
            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $date_of_day . "' AND festival.date_fin <= '" . $date_of_day . "' ) OR ( festival.date_debut IS NULL AND festival.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $next_saturday_day . "' AND festival.date_fin <= '" . $next_sunday_day . "') OR (festival.date_fin >= '" . $next_saturday_day . "' AND festival.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $last_sunday_day . "' AND festival.date_fin <= '" . $next_saturday_day . "') OR (festival.date_fin >= '" . $last_sunday_day . "' AND festival.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $last_sunday_of_next_monday . "' AND festival.date_fin <= '" . $next_saturday_of_next_monday . "') OR (festival.date_fin >= '" . $last_sunday_of_next_monday . "' AND festival.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_month . "' AND festival.date_fin <= '" . $last_day_month . "') OR (festival.date_fin >= '" . $first_day_month . "' AND festival.date_fin <= '" . $last_day_month . "' AND festival.date_fin >= '" . $date_of_day . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";

        }

        if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null && $_iVilleId != "0") {
            if (is_array($_iVilleId)) {
                $zSql_all_iville_request = "";
                $zSql_all_iville_request .= " (";
                if (sizeof($_iVilleId) == 1) {
                    if ($_iVilleId[0] != 0 && $_iVilleId[0] != "" && $_iVilleId[0] != null && $_iVilleId[0] != "0") {
                        $zSql_all_iville_request .= " festival.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    } else {
                        $zSql_all_iville_request .= " 0=0 ";
                    }
                } else {
                    $zSql_all_iville_request .= " festival.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    for ($i_iVilleId = 1; $i_iVilleId < count($_iVilleId); $i_iVilleId++) {
                        $zSql_all_iville_request .= " OR festival.IdVille_localisation = '" . $_iVilleId[$i_iVilleId] . "' ";
                    }
                }
                $zSql_all_iville_request .= " ) ";
                ////$this->firephp->log($zSql_all_iville_request, 'zSql_all_iville_request');
                //$zSqlListePartenaire .= $zSql_all_iville_request;
            }
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " festival.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            $zSqlListePartenaire .= " AND festival.date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            $zSqlListePartenaire .= " AND festival.date_fin = '" . $inputDatefin . "'";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " festival.article_categid = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " festival.article_categid = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR festival.article_categid = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }

        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " festival.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " festival.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR festival.article_subcategid = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }

        /*if($zSql_all_categ_request != "" && $zSql_all_subcateg_request == "") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;
        else if($zSql_all_categ_request == "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ".$zSql_all_subcateg_request;
        else if($zSql_all_categ_request != "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ( ".$zSql_all_categ_request." OR ".$zSql_all_subcateg_request." ) ";*/

        //if ($zSql_all_categ_request!="") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;


        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);

        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(festival.nom_societe ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.category ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.subcateg ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.organisateur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.adresse_localisation_diffuseur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }


        $zSqlListePartenaire .= " ) ";

        if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") || (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) {
            $zSqlListePartenaire .= " AND ( ";

            if (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") $zSqlListePartenaire .= $zSql_all_icommercant_request;
            if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") && (isset($zSql_all_iville_request) && $zSql_all_iville_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_iville_request;
            else if (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") $zSqlListePartenaire .= $zSql_all_iville_request;
            if (((isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "")) && (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
            else if (isset($zSql_all_categ_request) && $zSql_all_categ_request != "") $zSqlListePartenaire .= $zSql_all_categ_request;

            $zSqlListePartenaire .= " ) ";
        }

        $zSqlListePartenaire .= " AND (festival.IsActif = '1' OR festival.IsActif = '2') ";

        $zSqlListePartenaire .= " group by festival.id ";

        if ($iOrderBy == '1') $iOrderBy_value = " order by festival.last_update desc ";
        else if ($iOrderBy == '2') $iOrderBy_value = " order by festival.accesscount desc ";
        else $iOrderBy_value = " order by festival.date_debut,festival.date_fin, festival.date_debut asc ";

        $zSqlListePartenaire .= $iOrderBy_value;

        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $_limitend;
        //echo $zSqlListePartenaire;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListePartenaire');
        //die("<br/>STOP");

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }
    function getAlldeposantAgenda($_iCommercantId = "0")
    {

        $zSqlAlldeposantAgenda = "SELECT
            commercants.NomSociete,
            commercants.IdCommercant
            FROM
            festival
            INNER JOIN commercants ON festival.IdCommercant = commercants.IdCommercant
            WHERE
            (festival.IsActif = '1' OR festival.IsActif = '2')";

        if ($_iCommercantId != "0" && $_iCommercantId != NULL && $_iCommercantId != "" && $_iCommercantId != 0) {
            $zSqlAlldeposantAgenda .= " AND festival.IdCommercant = '" . $_iCommercantId . "'";
        }

        $zSqlAlldeposantAgenda .= " GROUP BY
            festival.IdCommercant";

        $zQueryAlldeposantAgenda = $this->db->query($zSqlAlldeposantAgenda);
        return $zQueryAlldeposantAgenda->result();
    }

    function listeAgendaRecherche_agenda_perso_liste_categ($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0", $referencement_agenda = 1)
    {

        $zSqlListePartenaire = "
            SELECT
                COUNT(festival.id) as nb_agenda, 
                festival.date_debut as datetime_debut, 
                festival.date_fin as datetime_fin, 
                festival.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_categ.agenda_categid,
                agenda_subcateg.subcateg,
                commercants.NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                festival
                LEFT OUTER JOIN villes ON villes.IdVille = festival.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = festival.article_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = festival.article_subcategid
                INNER JOIN commercants ON commercants.IdCommercant = festival.IdCommercant

            WHERE
                ( 
                (festival.IsActif = '1' OR festival.IsActif = '2') 
                AND commercants.IsActif = 1 
                ";
        if ($referencement_agenda==1)   $zSqlListePartenaire .= " AND commercants.referencement_agenda = ".$referencement_festival." ";

        $zSqlListePartenaire .= "
                AND (festival.id <> '0' OR festival.id <> NULL OR festival.id <> 0) 

        ";

        if (is_array($inputIdCommercant)) {
            $zSql_all_icommercant_request = "";
            $zSql_all_icommercant_request .= " (";
            if (sizeof($inputIdCommercant) == 1) {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
            } else {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                for ($i_iCommercantId = 1; $i_iCommercantId < count($inputIdCommercant); $i_iCommercantId++) {
                    $zSql_all_icommercant_request .= " OR commercants.IdCommercant = '" . $inputIdCommercant[$i_iCommercantId] . "' ";
                }
            }
            $zSql_all_icommercant_request .= " ) ";
            //$zSqlListePartenaire .= $zSql_all_icommercant_request;
        } else if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }

        if ($IdUsers_ionauth != "0" && $IdUsers_ionauth != "" && $IdUsers_ionauth != NULL && $IdUsers_ionauth != 0) {
            $zSqlListePartenaire .= " AND festival.IdUsers_ionauth = '" . $IdUsers_ionauth . "'";
        }

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);


            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $date_of_day . "' AND festival.date_fin <= '" . $date_of_day . "' ) OR ( festival.date_debut IS NULL AND festival.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $next_saturday_day . "' AND festival.date_fin <= '" . $next_sunday_day . "') OR (festival.date_fin >= '" . $next_saturday_day . "' AND festival.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $last_sunday_day . "' AND festival.date_fin <= '" . $next_saturday_day . "') OR (festival.date_fin >= '" . $last_sunday_day . "' AND festival.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $last_sunday_of_next_monday . "' AND festival.date_fin <= '" . $next_saturday_of_next_monday . "') OR (festival.date_fin >= '" . $last_sunday_of_next_monday . "' AND festival.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_month . "' AND festival.date_fin <= '" . $last_day_month . "') OR (festival.date_fin >= '" . $first_day_month . "' AND festival.date_fin <= '" . $last_day_month . "' AND festival.date_fin >= '" . $date_of_day . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";

        }

        if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null && $_iVilleId != "0") {
            if (is_array($_iVilleId)) {
                $zSql_all_iville_request = "";
                $zSql_all_iville_request .= " (";
                if (sizeof($_iVilleId) == 1) {
                    if ($_iVilleId[0] != 0 && $_iVilleId[0] != "" && $_iVilleId[0] != null && $_iVilleId[0] != "0") {
                        $zSql_all_iville_request .= " festival.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    } else {
                        $zSql_all_iville_request .= " 0=0 ";
                    }
                } else {
                    $zSql_all_iville_request .= " festival.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    for ($i_iVilleId = 1; $i_iVilleId < count($_iVilleId); $i_iVilleId++) {
                        $zSql_all_iville_request .= " OR festival.IdVille_localisation = '" . $_iVilleId[$i_iVilleId] . "' ";
                    }
                }
                $zSql_all_iville_request .= " ) ";
                ////$this->firephp->log($zSql_all_iville_request, 'zSql_all_iville_request');
                //$zSqlListePartenaire .= $zSql_all_iville_request;
            }
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " festival.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            $zSqlListePartenaire .= " AND festival.date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            $zSqlListePartenaire .= " AND festival.date_fin = '" . $inputDatefin . "'";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " festival.article_categid = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " festival.article_categid = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR festival.article_categid = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }

        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " festival.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " festival.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR festival.article_subcategid = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }

        /*if($zSql_all_categ_request != "" && $zSql_all_subcateg_request == "") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;
        else if($zSql_all_categ_request == "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ".$zSql_all_subcateg_request;
        else if($zSql_all_categ_request != "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ( ".$zSql_all_categ_request." OR ".$zSql_all_subcateg_request." ) ";*/

        //if ($zSql_all_categ_request!="") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;


        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);

        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(festival.nom_societe ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_categ.category ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_subcateg.subcateg ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.organisateur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.adresse_localisation_diffuseur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }


        $zSqlListePartenaire .= " ) ";

        if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") || (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) {
            $zSqlListePartenaire .= " AND ( ";

            if (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") $zSqlListePartenaire .= $zSql_all_icommercant_request;
            if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") && (isset($zSql_all_iville_request) && $zSql_all_iville_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_iville_request;
            else if (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") $zSqlListePartenaire .= $zSql_all_iville_request;
            if (((isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "")) && (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
            else if (isset($zSql_all_categ_request) && $zSql_all_categ_request != "") $zSqlListePartenaire .= $zSql_all_categ_request;

            $zSqlListePartenaire .= " ) ";
        }

        $zSqlListePartenaire .= " AND (festival.IsActif = '1' OR festival.IsActif = '2') ";

        $zSqlListePartenaire .= " group by festival.article_categid ";

        if ($iOrderBy == '1') $iOrderBy_value = " order by festival.last_update desc ";
        else if ($iOrderBy == '2') $iOrderBy_value = " order by festival.accesscount desc ";
        else $iOrderBy_value = " order by agenda_categ.category asc ";

        $zSqlListePartenaire .= $iOrderBy_value;

        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $_limitend;
        //echo $zSqlListePartenaire;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListePartenaire');
        //die("<br/>STOP");

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }

    function listeArticleRecherche_filtre_article_perso_check_categ($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0")
    {

        $zSqlListePartenaire = "
            SELECT
                festival.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_subcateg.subcateg,
                commercants.NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                festival
                LEFT OUTER JOIN villes ON villes.IdVille = festival.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = festival.article_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = festival.article_subcategid
                INNER JOIN commercants ON commercants.IdCommercant = festival.IdCommercant

            WHERE
                ( 0=0

        ";

        if (is_array($inputIdCommercant)) {
            $zSql_all_icommercant_request = "";
            $zSql_all_icommercant_request .= " (";
            if (sizeof($inputIdCommercant) == 1) {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
            } else {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                for ($i_iCommercantId = 1; $i_iCommercantId < count($inputIdCommercant); $i_iCommercantId++) {
                    $zSql_all_icommercant_request .= " OR commercants.IdCommercant = '" . $inputIdCommercant[$i_iCommercantId] . "' ";
                }
            }
            $zSql_all_icommercant_request .= " ) ";
            //$zSqlListePartenaire .= $zSql_all_icommercant_request;
        } else if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }

        if ($IdUsers_ionauth != "0" && $IdUsers_ionauth != "" && $IdUsers_ionauth != NULL && $IdUsers_ionauth != 0) {
            $zSqlListePartenaire .= " AND festival.IdUsers_ionauth = '" . $IdUsers_ionauth . "'";
        }

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $date_of_day . "' AND festival.date_fin <= '" . $date_of_day . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $date_of_day . "') )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $next_saturday_day . "' AND festival.date_fin <= '" . $next_sunday_day . "' )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $last_sunday_day . "' AND festival.date_fin <= '" . $next_saturday_day . "' )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $last_sunday_of_next_monday . "' AND festival.date_fin <= '" . $next_saturday_of_next_monday . "' )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_month . "' AND festival.date_fin <= '" . $last_day_month . "' )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
        }

        if ($_iVilleId != 0) {
            if (is_array($_iVilleId)) {
                $zSql_all_iville_request = "";
                $zSql_all_iville_request .= " (";
                if (sizeof($_iVilleId) == 1) {
                    $zSql_all_iville_request .= " festival.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                } else {
                    $zSql_all_iville_request .= " festival.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    for ($i_iVilleId = 1; $i_iVilleId < count($_iVilleId); $i_iVilleId++) {
                        $zSql_all_iville_request .= " OR festival.IdVille_localisation = '" . $_iVilleId[$i_iVilleId] . "' ";
                    }
                }
                $zSql_all_iville_request .= " ) ";
                ////$this->firephp->log($zSql_all_iville_request, 'zSql_all_iville_request');
                //$zSqlListePartenaire .= $zSql_all_iville_request;
            }
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " festival.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            $zSqlListePartenaire .= " AND festival.date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            $zSqlListePartenaire .= " AND festival.date_fin = '" . $inputDatefin . "'";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " festival.article_categid = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " festival.article_categid = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR festival.article_categid = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }

        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " festival.agenda_subcategid = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " festival.agenda_subcategid = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR festival.agenda_subcategid = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }

        /*if($zSql_all_categ_request != "" && $zSql_all_subcateg_request == "") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;
        else if($zSql_all_categ_request == "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ".$zSql_all_subcateg_request;
        else if($zSql_all_categ_request != "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ( ".$zSql_all_categ_request." OR ".$zSql_all_subcateg_request." ) ";*/

        if ($zSql_all_categ_request != "") $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;


        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);

        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(festival.nom_societe ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_categ.category ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_subcateg.subcateg ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.organisateur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.adresse_localisation_diffuseur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }


        $zSqlListePartenaire .= " ) ";

        if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") || (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) {
            $zSqlListePartenaire .= " AND ( ";

            if (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") $zSqlListePartenaire .= $zSql_all_icommercant_request;
            if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") && (isset($zSql_all_iville_request) && $zSql_all_iville_request != "")) $zSqlListePartenaire .= " OR " . $zSql_all_iville_request;
            else if (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") $zSqlListePartenaire .= $zSql_all_iville_request;

            $zSqlListePartenaire .= " ) ";
        }

        $zSqlListePartenaire .= " AND (festival.IsActif = '1' OR festival.IsActif = '2') ";

        $zSqlListePartenaire .= " group by festival.id ";

        if ($iOrderBy == '1') $iOrderBy_value = " order by festival.last_update desc ";
        else if ($iOrderBy == '2') $iOrderBy_value = " order by festival.accesscount desc ";
        else $iOrderBy_value = " order by festival.date_debut asc ";

        $zSqlListePartenaire .= $iOrderBy_value;

        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $zSqlListePartenaire;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListePartenaire');

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }

    function listeAgendaRecherche_filtre_agenda_perso($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0", $referencement_agenda = 1)
    {

        $zSqlListePartenaire = "
            SELECT
                festival.id as datetime_id, 
                festival.date_debut as datetime_debut, 
                festival.date_fin as datetime_fin, 
                festival.*,
                villes.Nom AS ville,
                agenda_categ.category,
                agenda_subcateg.subcateg,
                commercants.NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                festival
                LEFT OUTER JOIN villes ON villes.IdVille = festival.IdVille_localisation
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = festival.article_categid
                LEFT OUTER JOIN agenda_subcateg ON agenda_subcateg.agenda_subcategid = festival.article_subcategid
                INNER JOIN commercants ON commercants.IdCommercant = festival.IdCommercant
                

            WHERE
                ( 
                (festival.IsActif = '1' OR festival.IsActif = '2') 
                AND commercants.IsActif = 1 
                ";
        if ($referencement_agenda==1)   $zSqlListePartenaire .= " AND commercants.referencement_agenda = ".$referencement_festival." ";

        $zSqlListePartenaire .= "
                AND (festival.id <> '0' OR festival.id <> NULL OR festival.id <> 0) 

        ";

        if (is_array($inputIdCommercant)) {
            $zSql_all_icommercant_request = "";
            $zSql_all_icommercant_request .= " (";
            if (sizeof($inputIdCommercant) == 1) {
                if ($inputIdCommercant[0] != "0" && $inputIdCommercant[0] != "" && $inputIdCommercant[0] != NULL && $inputIdCommercant[0] != 0) {
                    $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                } else {
                    $zSql_all_icommercant_request .= " 0=0 ";
                }
            } else {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                for ($i_iCommercantId = 1; $i_iCommercantId < count($inputIdCommercant); $i_iCommercantId++) {
                    $zSql_all_icommercant_request .= " OR commercants.IdCommercant = '" . $inputIdCommercant[$i_iCommercantId] . "' ";
                }
            }
            $zSql_all_icommercant_request .= " ) ";
            //$zSqlListePartenaire .= $zSql_all_icommercant_request;
        } else if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }

        if ($IdUsers_ionauth != "0" && $IdUsers_ionauth != "" && $IdUsers_ionauth != NULL && $IdUsers_ionauth != 0) {
            $zSqlListePartenaire .= " AND festival.IdUsers_ionauth = '" . $IdUsers_ionauth . "'";
        }

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

            /*
            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $date_of_day . "' AND festival.date_fin <= '" . $date_of_day . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $next_saturday_day . "' AND festival.date_fin <= '" . $next_sunday_day . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $next_saturday_day . "' AND festival.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $last_sunday_day . "' AND festival.date_fin <= '" . $next_saturday_day . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $last_sunday_day . "' AND festival.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $last_sunday_of_next_monday . "' AND festival.date_fin <= '" . $next_saturday_of_next_monday . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $last_sunday_of_next_monday . "' AND festival.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_month . "' AND festival.date_fin <= '" . $last_day_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_month . "' AND festival.date_fin <= '" . $last_day_month . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_debut IS NULL AND festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            */
            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $date_of_day . "' AND festival.date_fin <= '" . $date_of_day . "' ) OR ( festival.date_debut IS NULL AND festival.date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $next_saturday_day . "' AND festival.date_fin <= '" . $next_sunday_day . "') OR (festival.date_fin >= '" . $next_saturday_day . "' AND festival.date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $last_sunday_day . "' AND festival.date_fin <= '" . $next_saturday_day . "') OR (festival.date_fin >= '" . $last_sunday_day . "' AND festival.date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $last_sunday_of_next_monday . "' AND festival.date_fin <= '" . $next_saturday_of_next_monday . "') OR (festival.date_fin >= '" . $last_sunday_of_next_monday . "' AND festival.date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_month . "' AND festival.date_fin <= '" . $last_day_month . "') OR (festival.date_fin >= '" . $first_day_month . "' AND festival.date_fin <= '" . $last_day_month . "' AND festival.date_fin >= '" . $date_of_day . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') OR (festival.date_fin >= '" . $first_day_user_month . "' AND festival.date_fin <= '" . $last_day_user_month . "') )";

        }

        if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null && $_iVilleId != "0") {
            if (is_array($_iVilleId)) {
                $zSql_all_iville_request = "";
                $zSql_all_iville_request .= " (";
                if (sizeof($_iVilleId) == 1) {
                    if ($_iVilleId[0] != 0 && $_iVilleId[0] != "" && $_iVilleId[0] != null && $_iVilleId[0] != "0") {
                        $zSql_all_iville_request .= " festival.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    } else {
                        $zSql_all_iville_request .= " 0=0 ";
                    }
                } else {
                    $zSql_all_iville_request .= " festival.IdVille_localisation = '" . $_iVilleId[0] . "' ";
                    for ($i_iVilleId = 1; $i_iVilleId < count($_iVilleId); $i_iVilleId++) {
                        $zSql_all_iville_request .= " OR festival.IdVille_localisation = '" . $_iVilleId[$i_iVilleId] . "' ";
                    }
                }
                $zSql_all_iville_request .= " ) ";
                ////$this->firephp->log($zSql_all_iville_request, 'zSql_all_iville_request');
                //$zSqlListePartenaire .= $zSql_all_iville_request;
            }
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " festival.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            $zSqlListePartenaire .= " AND festival.date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            $zSqlListePartenaire .= " AND festival.date_fin = '" . $inputDatefin . "'";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " festival.article_categid = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " festival.article_categid = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR festival.article_categid = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }

        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " festival.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " festival.article_subcategid = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR festival.article_subcategid = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }

        /*if($zSql_all_categ_request != "" && $zSql_all_subcateg_request == "") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;
        else if($zSql_all_categ_request == "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ".$zSql_all_subcateg_request;
        else if($zSql_all_categ_request != "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ( ".$zSql_all_categ_request." OR ".$zSql_all_subcateg_request." ) ";*/

        //if ($zSql_all_categ_request!="") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;


        $SearchedWordsString = str_replace("+", " ", $_zMotCle);

        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);
//var_dump($SearchedWords);
        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(festival.nom_societe ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.nom_manifestation ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.description ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_categ.category ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_subcateg.subcateg ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.organisateur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(festival.adresse_localisation_diffuseur ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }


        $zSqlListePartenaire .= " ) ";

        if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") || (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) {
            $zSqlListePartenaire .= " AND ( ";

            if (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") $zSqlListePartenaire .= $zSql_all_icommercant_request;
            if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") && (isset($zSql_all_iville_request) && $zSql_all_iville_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_iville_request;
            else if (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") $zSqlListePartenaire .= $zSql_all_iville_request;
            if (((isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "")) && (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
            else if (isset($zSql_all_categ_request) && $zSql_all_categ_request != "") $zSqlListePartenaire .= $zSql_all_categ_request;

            $zSqlListePartenaire .= " ) ";
        }

        $zSqlListePartenaire .= " AND (festival.IsActif = '1' OR festival.IsActif = '2') ";

        $zSqlListePartenaire .= " group by festival.id ";

        if ($iOrderBy == '1') $iOrderBy_value = " order by festival.last_update desc ";
        else if ($iOrderBy == '2') $iOrderBy_value = " order by festival.accesscount desc ";
        else $iOrderBy_value = " order by festival.date_debut,festival.date_fin, festival.date_debut asc ";

        $zSqlListePartenaire .= $iOrderBy_value;

        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $_limitend;
        //echo $zSqlListePartenaire;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListePartenaire');
        //die("<br/>STOP");

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }



}