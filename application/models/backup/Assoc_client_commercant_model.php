<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class Assoc_client_commercant_model extends CI_Model {
	
	private $_table = 'assoc_client_commercant';
	private $_users	  = 'users';
	private $_bonplan = 'bonplan';
	private $_commercants = 'commercants';
	private $_client_commercant='client_commercant';
	
	public function __construct() {
		parent::__construct ();
	}
	
	public function get_all($criteres = array()) {
		$this->db->select('*');
		$this->db->from($this->_table);
		if(!empty($criteres)){
			$this->db->where($criteres);
		}
		$query = $this->db->get();
		return $query->result();
	}
	
	
	
	public function insert($data) {
		$this->db->insert($this->_table, $data);
		return $this->db->insert_id();
	}
	
	public function get_by_id($id_client,$id_commercant){
		$this->db->select('*');
		$this->db->from($this->_table);
		$this->db->where(array('id_client'=>$id_client,'id_commercant'=>$id_commercant));
		
		$query = $this->db->get();
		return  ($query->num_rows() > 0) ? $query->first_row() : false;
	}
	
	public function get_by_client_id($id_client){
		$this->db->select('*');
		$this->db->from($this->_table);
		$this->db->where($this->_table.'.id_client', $id_client);
		$query = $this->db->get();
		return  ($query->num_rows() > 0) ? $query->first_row() : false;
	}
	
	public function get_by_commercant_id($id_commercant,$criteres=array()){
		$this->db->select('villes.Nom as Ville,'.$this->_table.'.*,'.$this->_users.'.*');
		$this->db->from($this->_table);
		$this->db->join($this->_users, $this->_table.'.id_client = '.$this->_users.'.IdUser');
		$this->db->join('villes','villes.idVille = users.idVille');
	//	$this->db->join($this->_commercants, $this->_commercants.'.IdCommercant = '.$this->_table.'.id_commercant');
		$where[$this->_table.'.id_commercant'] = $id_commercant;
		if(array_key_exists('month', $criteres)){
			$where['MONTH('.$this->_users.'.DateCreation)'] =  $criteres['month'];
		}
		$this->db->where($where);
		$query = $this->db->get();
		
		return  ($query->num_rows() > 0) ? $query->result() : false;
	}

    public function get_by_commercant_idsolde($id_commercant,$criteres=array(),$id_client){
        $this->db->select('villes.Nom as Ville,'.$this->_table.'.*,'.$this->_users.'.*');
        $this->db->from($this->_table);
        $this->db->join($this->_users, $this->_table.'.id_client = '.$this->_users.'.IdUser');
        $this->db->join('villes','villes.idVille = users.idVille');
        //	$this->db->join($this->_commercants, $this->_commercants.'.IdCommercant = '.$this->_table.'.id_commercant');
        $where[$this->_table.'.id_commercant'] = $id_commercant;
        if(array_key_exists('month', $criteres)){
            $where['MONTH('.$this->_users.'.DateCreation)'] =  $criteres['month'];
        }

        $this->db->where($where);
        $this->db->where("id_client",$id_client);
        $query = $this->db->get();

        return  ($query->num_rows() > 0) ? $query->result() : false;
    }

    public function get_by_commercant_id_csv($id_commercant){
		$this->db->select("users.Nom,users.Prenom,users.Adresse,villes.NomSimple as ville,users.Portable,users.Email, DATE_FORMAT(users.DateNaissance,'%d %b %Y') as DateNaissance",FALSE);
		$this->db->from($this->_table);
		$this->db->join($this->_users, $this->_table.'.id_client = '.$this->_users.'.IdUser');
		$this->db->join('villes','villes.idVille = users.idVille');
		//	$this->db->join($this->_commercants, $this->_commercants.'.IdCommercant = '.$this->_table.'.id_commercant');
	
		$this->db->where($this->_table.'.id_commercant', $id_commercant);
		$query = $this->db->get();
		return $query;
	
	}
	
	public function delete($where){
		return $this->db->delete($this->_table, $where);
	}

	public function get_newsletter_customer_only($idcom){
        $this->db->select('Email');
	    $this->db->where('id_commercant',$idcom);
	    $res=$this->db->get('client_commercant');
	    return $res->result();
    }
	public function get_false_client_by_commercant_id($idcom){
        $this->db->where('id_commercant',$idcom);
        $res= $this->db->get('client_commercant');
       return $res->result();
    }
    public function get_comment_client_by_commercant_id($idcom){
        $this->db->select('mail');
        $this->db->where('idcommercant',$idcom);
        $res= $this->db->get('livreor_commentaires');
        return $res->result();
    }
}
