<?php

class mdldemande extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    function getAllDemande() {
        $this->db->where('etatDemande',0);
        $all = $this->db->get("liste_demande");
        return $all->result();
    }
    public function saveDemandeDomaine($data){
        $this->db->insert("liste_demande", $data);
    }
    public function getComNameById($idcom){

        $this->db->where('IdCommercant',$idcom);
        $infocom = $this->db->get('commercants');
        if(isset($infocom) AND !empty($infocom)){
            return $infocom->row()->NomSociete;
        }
    }

    public function valid_demande($id){
        $data = array('etatDemande'=> 1);
        $this->db->where('id',$id);
        $this->db->update('liste_demande', $data);
    }
    public function getbyid($id){
        $this->db->where('id',$id);
        $res = $this->db->get('liste_demande');
        return $res->row();
    }
}
?>
