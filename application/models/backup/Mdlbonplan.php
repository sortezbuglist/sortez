<?php
class mdlbonplan extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    function listeBonPlan($_limitstart = 0, $_limitend = 10000){
        $zSqlListeBonPlan = "
            SELECT
                bonplan.* ,commercants.Photo5 as Photo5,
                commercants.NomSociete AS NomSociete,
                villes.Nom AS ville,
                commercants.Adresse1 AS quartier,
                commercants.Adresse2 AS rue,
                commercants.IdCommercant
            FROM
                bonplan
                LEFT JOIN commercants ON commercants.IdCommercant = bonplan.bonplan_commercant_id
                LEFT JOIN villes ON villes.IdVille = commercants.IdVille
        ";
        
        $zSqlListeBonPlan .= " LIMIT ".$_limitstart.",".$_limitend ;
        
        //echo $zSqlListeBonPlan;
        
        $zQueryListeBonPlan = $this->db->query($zSqlListeBonPlan) ;
        return $zQueryListeBonPlan->result() ;
    }
    
    
    function GetAllBonplanforNewsletter() {
        $qryCommercant =  $this->db->query("
            SELECT
            bonplan.*,
            commercants.IdCommercant,
            commercants.NomSociete,
            commercants.Adresse1,
            commercants.Adresse2,
            villes.Nom AS ville,
            sous_rubriques.Nom as sousrubrique
            FROM
            bonplan
            Inner Join commercants ON commercants.IdCommercant = bonplan.bonplan_commercant_id
            Inner Join villes ON villes.IdVille = commercants.IdVille
            Inner Join ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
            left outer join sous_rubriques ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique
            WHERE
                        commercants.IsActif =  '1'
            ORDER BY
                        bonplan.bonplan_id DESC
            LIMIT 4
        ");
        return $qryCommercant->result();
    }
    
    
    function listeBonPlanRecherche($_iCategorieId = 0, $i_CommercantId = 0, $_zMotCle = "", $_iFavoris = "", $_limitstart = 0, $_limitend = 10000, $_iIdVille = 0, $_iIdDepartement = 0, $iOrderBy="", $inputFromGeo = "0", $inputGeoValue = "10", $inputGeoLatitude="0", $inputGeoLongitude="0", $session_iWhereMultiple = "",$inputString_bonplan_type=""){
        $txtWhereFavoris = "";

        $thisss =& get_instance();
        $thisss->load->library('ion_auth');
        $thisss->load->model("ion_auth_used_by_club");

        if ($thisss->ion_auth->logged_in()){
            $user_ion_auth = $thisss->ion_auth->user()->row();
            $iduser = $thisss->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $thisss->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser=0;


        if($_iFavoris == "1" && $iduser != "") {
            $sqlFavoris = "
                SELECT
                    CONCAT('(',GROUP_CONCAT(ass_commercants_users.IdCommercant SEPARATOR ','),')') AS IdCommercant
                FROM
                    ass_commercants_users
                WHERE
                    ass_commercants_users.IdUser = '" . $iduser . "'
                GROUP BY ass_commercants_users.IdUser
            ";
            $QueryFavoris = $this->db->query($sqlFavoris) ;
            if($QueryFavoris->num_rows() > 0) {
                $colFavoris = $QueryFavoris->result();
                $txtWhereFavoris .= " AND commercants.IdCommercant IN " . $colFavoris[0]->IdCommercant;
            } else {
                $txtWhereFavoris .= " AND commercants.IdCommercant IN (0) ";
            }
        }

        $zSqlListeBonPlan = "
            SELECT
            bonplan.*,
            commercants.Photo5 AS Photo5,
            commercants.NomSociete AS NomSociete,
            villes.Nom AS ville,
            commercants.Adresse1 AS quartier,
            commercants.Adresse2 AS rue,
            commercants.IdCommercant,
            sous_rubriques.IdSousRubrique,
            sous_rubriques.IdRubrique,
            sous_rubriques.Nom,
            commercants.IdVille,
            commercants.nom_url";

        if ($inputFromGeo == "1") {
            $zSqlListeBonPlan .= " , (6371 * acos(cos(radians($inputGeoLatitude)) * cos(radians(commercants.latitude)) * cos(radians(commercants.longitude) - radians($inputGeoLongitude)) + sin(radians($inputGeoLatitude)) * sin(radians(commercants.latitude)))) AS distance ";
        }

        $zSqlListeBonPlan .= "
            FROM
            bonplan
            Inner Join commercants ON commercants.IdCommercant = bonplan.bonplan_commercant_id
            Left Join villes ON villes.IdVille = commercants.IdVille
            Inner Join ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
            Inner Join sous_rubriques ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique
            WHERE
            commercants.IsActif = 1 AND commercants.referencement_bonplan = 1  
        ";

        if ($inputFromGeo == "1") {
            $zSqlListeBonPlan .= " AND commercants.latitude != '' AND commercants.longitude != '' ";
        }
        
        /*if($_iCategorieId != 0){
            $zSqlListeBonPlan .= " AND sous_rubriques.IdSousRubrique =" . $_iCategorieId ;
        }*/
        if(isset($_iCategorieId) && is_array($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0){
            $zSqlListeBonPlan .= " AND (";
            if (sizeof($_iCategorieId)==1){
                $zSqlListeBonPlan .= " sous_rubriques.IdSousRubrique = " . $_iCategorieId[0];
            } else {
                $zSqlListeBonPlan .= " sous_rubriques.IdSousRubrique = " . $_iCategorieId[0];
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand ++) {
                    $zSqlListeBonPlan .= " OR sous_rubriques.IdSousRubrique = " . $_iCategorieId[$ii_rand];
                }
            }
            $zSqlListeBonPlan .= " )";
        }
        if($i_CommercantId != 0){
            $zSqlListeBonPlan .= " AND commercants.IdCommercant = '$i_CommercantId' " ;
        }
        /*
        if($_zMotCle != ""){
            $zSqlListeBonPlan .= " AND bonplan_titre LIKE '%" . $_zMotCle . "%'" ;
        }
        */
        $SearchedWordsString = str_replace("+", " ", $_zMotCle) ;
        $SearchedWordsString = str_replace("'", "\'", $SearchedWordsString) ;
        $SearchedWordsString = str_replace('"', '\"', $SearchedWordsString) ;
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString) ;
        
        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;
        
        for ($i = 0; $i < sizeof($SearchedWords); $i ++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search) ;
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListeBonPlan .= "\n AND " ;
                }
                $zSqlListeBonPlan .= " ( \n" .
                    " UPPER(bonplan.bonplan_titre ) LIKE '%" . strtoupper($Search) . "%'\n" . 
                    " OR UPPER(bonplan.bonplan_texte ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.Adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.Adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .    
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                " )
                ";
                if ($i != (sizeof($SearchedWords) - 1)){
                    $zSqlListeBonPlan .= "\n OR ";
                }
            }
        }
        if($_iFavoris == "1" && $iduser != "") {
            $zSqlListeBonPlan .= $txtWhereFavoris;
        }

        //add ville id list on query
        //
        if($_iIdVille != 0 && $_iIdVille != "" && $_iIdVille != null){
            $zSqlListeBonPlan .= " AND commercants.IdVille=" . $_iIdVille ;
        } elseif ($_iIdDepartement != 0 && $_iIdDepartement != "" && $_iIdDepartement != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iIdDepartement);
            if (count($villeByIdDepartement)>0) {
                $zSqlListeBonPlan .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListeBonPlan .= " commercants.IdVille=" . $key_villeDep->IdVille ;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListeBonPlan .= " OR ";
                     } 
                    $iii_villedep++;
                }
                $zSqlListeBonPlan .= ")";                    
            }
        }


        if ($session_iWhereMultiple != "") {
            $zSqlListeBonPlan .= " AND bonplan.bon_plan_utilise_plusieurs = '".$session_iWhereMultiple."' ";        
        }

        if ($inputString_bonplan_type != "" && $inputString_bonplan_type!="0" && $inputString_bonplan_type!=null && $inputString_bonplan_type!=0) {
            $zSqlListeBonPlan .= " AND bonplan.bonplan_type = '".$inputString_bonplan_type."' ";
        }

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlListeBonPlan .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlListeBonPlan .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlListeBonPlan .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlListeBonPlan .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListeBonPlan .= " OR ";
            }
            $zSqlListeBonPlan .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlListeBonPlan .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE



        //SORTEZ.ORG => NO BASIC PARTNER ACCOUT
            $zSqlListeBonPlan .= " AND commercants.user_ionauth_id IN (SELECT users_groups_ionauth.user_ionauth_id FROM users_groups_ionauth WHERE users_groups_ionauth.group_ionauth_id = 4 OR users_groups_ionauth.group_ionauth_id = 5) ";
        //SORTEZ.ORG => NO BASIC PARTNER ACCOUT

        // demo account commercant should not appear on annuaire list
        $zSqlListeBonPlan .= " AND commercants.IdCommercant <> '301298'  AND commercants.IdCommercant <> '301266' ";
        //$zSqlListeBonPlan .= " AND commercants.IdCommercant <> '301298' ";

        if ($inputFromGeo == "1") {
            $zSqlListeBonPlan .= "  HAVING distance < $inputGeoValue  ";
        }

        if ($iOrderBy=='1') $iOrderBy_value = " order by bonplan.bonplan_id desc ";
        else if ($iOrderBy=='2') $iOrderBy_value = " order by commercants.nbrevisites desc ";
        else if ($iOrderBy=='3') $iOrderBy_value = " order by bonplan.bonplan_id asc ";
        else $iOrderBy_value = " order by bonplan_date_fin asc ";

        $zSqlListeBonPlan .= $iOrderBy_value;

        $zSqlListeBonPlan .= " LIMIT ".$_limitstart.",".$_limitend ;
        
        //echo $zSqlListeBonPlan."<br/><br/>";
        ////$this->firephp->log($iOrderBy, 'iOrderBy_3');
        
        $zQueryListeBonPlan = $this->db->query($zSqlListeBonPlan) ;
        return $zQueryListeBonPlan->result() ;
    }


    function bonPlanParCommercant($_iCommercantId){

        $zSqlBonPlanParCommercant = "
        SELECT * , commercants.NomSociete AS NomSociete, villes.Nom AS ville,
        commercants.Adresse1 AS quartier, commercants.Adresse2 AS rue,
        commercants.IdCommercant
        FROM
        bonplan
        LEFT JOIN commercants ON commercants.IdCommercant=bonplan.bonplan_commercant_id
        LEFT JOIN villes ON villes.IdVille=commercants.IdVille
        WHERE 0=0
        ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlBonPlanParCommercant .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlBonPlanParCommercant .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlBonPlanParCommercant .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlBonPlanParCommercant .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlBonPlanParCommercant .= " OR ";
            }
            $zSqlBonPlanParCommercant .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlBonPlanParCommercant .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE



        if (isset($_iCommercantId) && $_iCommercantId!=0 && $_iCommercantId!="" && $_iCommercantId!=NULL && $_iCommercantId!="0")
            $zSqlBonPlanParCommercant .= "
		    AND
        commercants.IdCommercant=" . $_iCommercantId ;

        $zQueryBonPlanParCommercant = $this->db->query($zSqlBonPlanParCommercant) ;
        //$toBonPlanParCommercant = $zQueryBonPlanParCommercant->result() ;
        $toBonPlanParCommercant = $zQueryBonPlanParCommercant->row() ;
        //return $toBonPlanParCommercant[0] ;
        return $toBonPlanParCommercant ;
    }
    
    function lastBonplanCom($_iCommercantId){ 

        $zSqlBonPlan = "SELECT *  FROM bonplan ";
        if (isset($_iCommercantId) && $_iCommercantId!=0 && $_iCommercantId!="" && $_iCommercantId!=NULL && $_iCommercantId!="0")
        $zSqlBonPlan .= " WHERE bonplan_commercant_id =".$_iCommercantId;
        $zSqlBonPlan .= " order by bonplan_id desc LIMIT 1" ;

        $zQuery = $this->db->query($zSqlBonPlan) ;
        return $zQuery->row() ;
    }

    function lastBonplanCom2($_iCommercantId){

        $zRqt = "SELECT *  FROM bonplan ";
        if (isset($_iCommercantId) && $_iCommercantId!=0 && $_iCommercantId!="" && $_iCommercantId!=NULL && $_iCommercantId!="0")
        $zRqt .= " WHERE bonplan_commercant_id =".$_iCommercantId;
        $zRqt .= " order by bonplan_id desc LIMIT 1" ;

        $qryBonplan = $this->db->query($zRqt) ;
        if ($qryBonplan->num_rows() > 0) {
            $Res = $qryBonplan->result();
            return $Res[0];
        }
    }
    
    function getById($_id=0){
        $zRqt = "SELECT *  FROM bonplan WHERE bonplan_id =".$_id ;
        $qryBonplan = $this->db->query($zRqt) ;
        if ($qryBonplan->num_rows() > 0) {
            $Res = $qryBonplan->result();
            return $Res[0];
        }
    }

    function decremente_quantite_bonplan($_id, $_nb="1"){
        $obj_bonplan = $this->GetById($_id);
        if ($obj_bonplan->bonplan_type == "2") $nb_bp = intval($obj_bonplan->bp_unique_qttprop);
        else if ($obj_bonplan->bonplan_type == "3") $nb_bp = intval($obj_bonplan->bp_multiple_qttprop);
        else $nb_bp = 0;
        if ($nb_bp > 0) {
            $nb_bp = $nb_bp -   intval($_nb);
        }
        if ($obj_bonplan->bonplan_type == "2")  $zSqldecrement = "UPDATE bonplan SET bp_unique_qttprop='".$nb_bp."' WHERE bonplan_id=" . $_id ;
        else if ($obj_bonplan->bonplan_type == "3")  $zSqldecrement = "UPDATE bonplan SET bp_multiple_qttprop='".$nb_bp."' WHERE bonplan_id=" . $_id ;
        $this->db->query($zSqldecrement) ;

    }
    
    function getListeBonPlan($_iCommercantId){
        $zSqlBonPlanParCommercant = "SELECT bonplan.* , commercants.Photo5 as Photo5, commercants.NomSociete AS NomSociete, villes.Nom AS ville, commercants.Adresse1 AS quartier, commercants.Adresse2 AS rue, commercants.IdCommercant FROM bonplan LEFT JOIN commercants ON commercants.IdCommercant=bonplan.bonplan_commercant_id LEFT JOIN villes ON villes.IdVille=commercants.IdVille WHERE commercants.IdCommercant=" . $_iCommercantId ." order by bonplan_id desc "; 
        $zQueryBonPlanParCommercant = $this->db->query($zSqlBonPlanParCommercant) ;
        $toBonPlanParCommercant = $zQueryBonPlanParCommercant->result() ;
        return $toBonPlanParCommercant ;
    }
    
    function insertBonplan($prmData) {
        if ($prmData['bonplan_id']=='') $prmData['bonplan_id']=null;
        if ($prmData['bonplan_date_debut']==''||$prmData['bonplan_date_debut']=='--') $prmData['bonplan_date_debut']=null;
        if ($prmData['bonplan_date_fin']==''||$prmData['bonplan_date_fin']=='--') $prmData['bonplan_date_fin']=null;
        if ($prmData['bp_unique_date_debut']==''||$prmData['bp_unique_date_debut']=='--') $prmData['bp_unique_date_debut']=null;
        if ($prmData['bp_unique_date_fin']==''||$prmData['bp_unique_date_fin']=='--') $prmData['bp_unique_date_fin']=null;
        if ($prmData['bp_unique_date_visit']==''||$prmData['bp_unique_date_visit']=='--') $prmData['bp_unique_date_visit']=null;
        if ($prmData['bp_multiple_date_debut']==''||$prmData['bp_multiple_date_debut']=='--') $prmData['bp_multiple_date_debut']=null;
        if ($prmData['bp_multiple_date_fin']==''||$prmData['bp_multiple_date_fin']=='--') $prmData['bp_multiple_date_fin']=null;
        if ($prmData['bp_multiple_date_visit']==''||$prmData['bp_multiple_date_visit']=='--') $prmData['bp_multiple_date_visit']=null;
        if ($prmData['activ_bp_unique_prix']=='') $prmData['activ_bp_unique_prix']=0;
        if ($prmData['activ_bp_unique_qttprop']=='') $prmData['activ_bp_unique_qttprop']=0;
        if ($prmData['activ_bp_unique_nbmax']=='') $prmData['activ_bp_unique_nbmax']=0;
        if ($prmData['activ_bp_unique_date_visit']=='') $prmData['activ_bp_unique_date_visit']=0;
        if ($prmData['activ_bp_unique_reserv_regler']=='') $prmData['activ_bp_unique_reserv_regler']=0;
        if ($prmData['activ_bp_multiple_prix']=='') $prmData['activ_bp_multiple_prix']=0;
        if ($prmData['activ_bp_multiple_qttprop']=='') $prmData['activ_bp_multiple_qttprop']=0;
        if ($prmData['activ_bp_multiple_nbmax']=='') $prmData['activ_bp_multiple_nbmax']=0;
        if ($prmData['activ_bp_multiple_date_visit']=='') $prmData['activ_bp_multiple_date_visit']=0;
        if ($prmData['activ_bp_multiple_reserv_regler']=='') $prmData['activ_bp_multiple_reserv_regler']=0;
        if ($prmData['activ_bp_reserver_paypal']=='') $prmData['activ_bp_reserver_paypal']=0;

        $this->db->insert("bonplan", $prmData);
        return $this->db->insert_id();
    }
    
    function updateBonplan($prmData) {
        $prmData = (array)$prmData;
        $this->db->where("bonplan_id", $prmData["bonplan_id"]);
        $this->db->update("bonplan", $prmData);
        $objBonplan = $this->GetById($prmData["bonplan_id"]);
        return $objBonplan->bonplan_id;
    }
    
    // function GetById($prmId) {
        // $qryBonplan =  $this->db->query("SELECT * FROM bonplan WHERE bonplan_id = ?", $prmId);
        // if ($qryBonplan->num_rows() > 0) {
            // $Res = $qryBonplan->result();
            // return $Res[0];
        // }
    // }
    
    function listeMesBonplans($prmId){
        /*$zRqt = "SELECT `bonplan_id` , `bonplan_commercant_id` , `bonplan_titre` , `bonplan_texte` , `bonplan_nombrepris` , `bonplan_date_debut` , `bonplan_date_fin` , `bonplan_photo` , `bonplan_categorie_id`  , `bonplan_ville_id` , `bonplan_photo1` , `bonplan_photo2` , `bonplan_photo3` , `bonplan_photo4` , commercants.Photo5 as Photo5, c.Nom as categorie_nom, v.Nom as Nom
                FROM bonplan
                LEFT OUTER JOIN commercants ON (commercants.IdCommercant = bonplan.bonplan_commercant_id),
                rubriques c, villes v
                WHERE bonplan.bonplan_categorie_id = c.IdRubrique
                AND bonplan.bonplan_ville_id = v.IdVille
                AND bonplan.bonplan_commercant_id = " . $prmId */
        
        $zRqt = "SELECT bonplan.* , commercants.Photo5 as Photo5
                FROM bonplan
                LEFT OUTER JOIN commercants ON (commercants.IdCommercant = bonplan.bonplan_commercant_id)
                WHERE bonplan.bonplan_commercant_id = " . $prmId."

                ORDER BY bonplan_id DESC
                
                " ;
        $qryBonplan = $this->db->query($zRqt) ;
        if ($qryBonplan->num_rows() > 0) {
            return $qryBonplan->result();
        }
    }
    
    function GetBonplan($prmId){
        $qryBonplan = $this->db->query("SELECT * FROM bonplan WHERE bonplan_id = ?", $prmId) ;
        if ($qryBonplan->num_rows() > 0) {
            $Res = $qryBonplan->result();
            return $Res[0];
        }
    }
    
    function supprimeBonplans($prmId){
    
        /*$zRpsBonplan =  $this->db->query("SELECT * FROM bonplan WHERE bonplan_id = ?", $prmId);
        $toBonplan = $zRpsBonplan->result() ;
        $oBonplan = $toBonplan[0] ;*/
        /*if (file_exists("application/resources/front/images/".$oBonplan->Bonplan_photo1)==true){
            unlink ("application/resources/front/images/".$oBonplan->Bonplan_photo1) ;
        }
        if (file_exists("application/resources/front/images/".$oBonplan->Bonplan_photo2)==true){
            unlink ("application/resources/front/images/".$oBonplan->Bonplan_photo2) ;
        }
        if (file_exists("application/resources/front/images/".$oBonplan->Bonplan_photo3)==true){
            unlink ("application/resources/front/images/".$oBonplan->Bonplan_photo3) ;
        }
        if (file_exists("application/resources/front/images/".$oBonplan->Bonplan_photo4)==true){
            unlink ("application/resources/front/images/".$oBonplan->Bonplan_photo4) ;
        }*/
        $qryBonplan = $this->db->query("DELETE FROM bonplan WHERE bonplan_id = ?", $prmId) ;
        return $qryBonplan ;
    }
    function getValideBonPlanByIdCommernats($prmListCommercants = "") {
        $qryBonplan = $this->db->query("
            SELECT
                bonplan.*,
                commercants.NomSociete
            FROM
                (bonplan
                LEFT JOIN    commercants ON commercants.IdCommercant = bonplan.bonplan_commercant_id)     
            WHERE
                bonplan.bonplan_commercant_id IN ($prmListCommercants)
                AND NOW() BETWEEN    bonplan.bonplan_date_debut AND bonplan.bonplan_date_fin
        ") ;
        return $qryBonplan->result();
    }


    function file_manager_update($_iArticleId = '0', $_iUserId = '0', $_iField = 'bonplan_photo1', $_iValue = ''){
        $zSql = "UPDATE bonplan SET `".$_iField."` = '".$_iValue."' WHERE bonplan_id = ".$_iArticleId." AND `bonplan_commercant_id` = '".$_iUserId."' " ;
        $zQuery = $this->db->query($zSql);
    }
    function listeBonplanRecherche_filtre_bonplan_perso($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0", $referencement_bonplan = 1)
    {

        $zSqlListePartenaire = "
            
                SELECT
                bonplan.*,
                villes.Nom AS ville,
                commercants.NomSociete,
                rubriques.Nom,
                sous_rubriques.Nom 
                ";

        $zSqlListePartenaire .= "
            FROM
                bonplan
                LEFT OUTER JOIN villes ON villes.IdVille = bonplan.bonplan_ville_id
                INNER JOIN commercants ON commercants.IdCommercant = bonplan.bonplan_commercant_id
                INNER JOIN ass_commercants_rubriques ON commercants.IdCommercant = ass_commercants_rubriques.IdCommercant
                INNER JOIN rubriques ON ass_commercants_rubriques.IdRubrique = rubriques.IdRubrique
                INNER JOIN ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
                INNER JOIN sous_rubriques ON ass_commercants_sousrubriques.IdSousRubrique = sous_rubriques.IdSousRubrique
                WHERE
                commercants.IsActif = 1
                AND commercants.referencement_bonplan = 1  
                ";
        if ($referencement_bonplan==1)   $zSqlListePartenaire .= " AND commercants.referencement_bonplan = ".$referencement_bonplan." ";

        $zSqlListePartenaire .= "
                AND (bonplan.bonplan_id <> '0' OR bonplan.bonplan_id <> NULL OR bonplan.bonplan_id <> 0) 

        ";

        if (is_array($inputIdCommercant)) {
            $zSql_all_icommercant_request = "";
            $zSql_all_icommercant_request .= " (";
            if (sizeof($inputIdCommercant) == 1) {
                if ($inputIdCommercant[0] != "0" && $inputIdCommercant[0] != "" && $inputIdCommercant[0] != NULL && $inputIdCommercant[0] != 0) {
                    $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                } else {
                    $zSql_all_icommercant_request .= " 0=0 ";
                }
            } else {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                for ($i_iCommercantId = 1; $i_iCommercantId < count($inputIdCommercant); $i_iCommercantId++) {
                    $zSql_all_icommercant_request .= " OR commercants.IdCommercant = '" . $inputIdCommercant[$i_iCommercantId] . "' ";
                }
            }
            $zSql_all_icommercant_request .= " ) ";
            //$zSqlListePartenaire .= $zSql_all_icommercant_request;
        } else if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }

        if ($IdUsers_ionauth != "0" && $IdUsers_ionauth != "" && $IdUsers_ionauth != NULL && $IdUsers_ionauth != 0) {
            $zSqlListePartenaire .= " AND commercants.IdUsers_ionauth = '" . $IdUsers_ionauth . "'";
        }


        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

            /*
            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $date_of_day . "' AND bonplan.bonplan_date_fin <= '" . $date_of_day . "') OR (bonplan.bonplan_date_debut IS NULL AND bonplan.bonplan_date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $next_saturday_day . "' AND bonplan.bonplan_date_fin <= '" . $next_sunday_day . "') OR (bonplan.bonplan_date_debut IS NULL AND bonplan.bonplan_date_fin >= '" . $next_saturday_day . "' AND bonplan.bonplan_date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $last_sunday_day . "' AND bonplan.bonplan_date_fin <= '" . $next_saturday_day . "') OR (bonplan.bonplan_date_debut IS NULL AND bonplan.bonplan_date_fin >= '" . $last_sunday_day . "' AND bonplan.bonplan_date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $last_sunday_of_next_monday . "' AND bonplan.bonplan_date_fin <= '" . $next_saturday_of_next_monday . "') OR (bonplan.bonplan_date_debut IS NULL AND bonplan.bonplan_date_fin >= '" . $last_sunday_of_next_monday . "' AND bonplan.bonplan_date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_month . "') OR (bonplan.bonplan_date_debut IS NULL AND bonplan.bonplan_date_fin >= '" . $first_day_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_month . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_debut IS NULL AND bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_debut IS NULL AND bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_debut IS NULL AND bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_debut IS NULL AND bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_debut IS NULL AND bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_debut IS NULL AND bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_debut IS NULL AND bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_debut IS NULL AND bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_debut IS NULL AND bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_debut IS NULL AND bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_debut IS NULL AND bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_debut IS NULL AND bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            */
            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $date_of_day . "' AND bonplan.bonplan_date_fin <= '" . $date_of_day . "' ) OR ( bonplan.bonplan_date_debut IS NULL AND bonplan.bonplan_date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $next_saturday_day . "' AND bonplan.bonplan_date_fin <= '" . $next_sunday_day . "') OR (bonplan.bonplan_date_fin >= '" . $next_saturday_day . "' AND bonplan.bonplan_date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $last_sunday_day . "' AND bonplan.bonplan_date_fin <= '" . $next_saturday_day . "') OR (bonplan.bonplan_date_fin >= '" . $last_sunday_day . "' AND bonplan.bonplan_date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $last_sunday_of_next_monday . "' AND bonplan.bonplan_date_fin <= '" . $next_saturday_of_next_monday . "') OR (bonplan.bonplan_date_fin >= '" . $last_sunday_of_next_monday . "' AND bonplan.bonplan_date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_month . "' AND bonplan.bonplan_date_fin >= '" . $date_of_day . "') )";


            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";

        }

        if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null && $_iVilleId != "0") {
            if ($_iVilleId) {
                $zSql_all_iville_request = "";
                $zSql_all_iville_request .= " (";
                if (sizeof($_iVilleId) == 1) {
                    if ($_iVilleId[0] != 0 && $_iVilleId[0] != "" && $_iVilleId[0] != null && $_iVilleId[0] != "0") {
                        $zSql_all_iville_request .= " commercants.IdVille = '" . $_iVilleId[0] . "' ";
                    } else {
                        $zSql_all_iville_request .= " 0=0 ";
                    }
                } else {
                    $zSql_all_iville_request .= " commercants.IdVille = '" . $_iVilleId[0] . "' ";
                    for ($i_iVilleId = 1; $i_iVilleId < count($_iVilleId); $i_iVilleId++) {
                        $zSql_all_iville_request .= " OR commercants.IdVille = '" . $_iVilleId[$i_iVilleId] . "' ";
                    }
                }
                $zSql_all_iville_request .= " ) ";
                ////$this->firephp->log($zSql_all_iville_request, 'zSql_all_iville_request');
                //$zSqlListePartenaire .= $zSql_all_iville_request;
            }
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " commercants.IdVille =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            $zSqlListePartenaire .= " AND bonplan.bonplan_date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            $zSqlListePartenaire .= " AND bonplan.bonplan_date_fin = '" . $inputDatefin . "'";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " rubriques.IdRubrique = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " rubriques.IdRubrique  = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR rubriques.IdRubrique  = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }

        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " sous_rubriques.IdSousRubrique = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " sous_rubriques.IdSousRubrique = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR sous_rubriques.IdSousRubrique = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }


        /*if($zSql_all_categ_request != "" && $zSql_all_subcateg_request == "") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;
        else if($zSql_all_categ_request == "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ".$zSql_all_subcateg_request;
        else if($zSql_all_categ_request != "" && $zSql_all_subcateg_request != "") $zSqlListePartenaire .= " AND ( ".$zSql_all_categ_request." OR ".$zSql_all_subcateg_request." ) ";*/

        //if ($zSql_all_categ_request!="") $zSqlListePartenaire .= " AND ".$zSql_all_categ_request;


        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = str_replace("'", "\'", $SearchedWordsString) ;
        $SearchedWordsString = str_replace('"', '\"', $SearchedWordsString) ;
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);

        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(bonplan.bonplan_titre ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(bonplan.bonplan_texte ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.Adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.Adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }

        //$zSqlListePartenaire .= " ) ";

        if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") || (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) {
            $zSqlListePartenaire .= " AND ( ";

            if (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") $zSqlListePartenaire .= $zSql_all_icommercant_request;
            if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") && (isset($zSql_all_iville_request) && $zSql_all_iville_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_iville_request;
            else if (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") $zSqlListePartenaire .= $zSql_all_iville_request;
            if (((isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "")) && (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
            else if (isset($zSql_all_categ_request) && $zSql_all_categ_request != "") $zSqlListePartenaire .= $zSql_all_categ_request;

            $zSqlListePartenaire .= " ) ";
        }

        $zSqlListePartenaire .= " AND (commercants.IsActif = '1' OR commercants.IsActif = '2') ";

        $zSqlListePartenaire .= " group by bonplan.bonplan_id ";

        if ($iOrderBy == '1') $iOrderBy_value = " order by bonplan.last_update desc ";
        else if ($iOrderBy == '2') $iOrderBy_value = " order by bonplan.accesscount desc ";
        else $iOrderBy_value = " order by bonplan.bonplan_date_debut, bonplan.bonplan_date_fin ";

        $zSqlListePartenaire .= $iOrderBy_value;

        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $_limitend;
        //echo $zSqlListePartenaire;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListePartenaire');
        //die("<br/>STOP");

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }
    function listeBonplanRecherche_Bonplan_perso_liste_categ($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0", $referencement_bonplan = 1)
    {


        $zSqlListePartenaire = "
            SELECT
                 COUNT(bonplan.bonplan_id) as nb_bonplan,
                 bonplan.*,
                 sous_rubriques.Nom AS rubrique,
                 sous_rubriques.IdSousRubrique,
                 sous_rubriques.IdRubrique,
                 villes.Nom AS ville,
                 commercants.NomSociete AS NomSociete,
                 rubriques.Nom,
                 rubriques.IdRubrique
                ";

        $zSqlListePartenaire .= "
            FROM
                bonplan
                INNER JOIN commercants ON bonplan.bonplan_commercant_id = commercants.IdCommercant
                INNER JOIN ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
                INNER JOIN sous_rubriques ON ass_commercants_sousrubriques.IdSousRubrique = sous_rubriques.IdSousRubrique
                INNER JOIN ass_commercants_rubriques ON commercants.IdCommercant = ass_commercants_rubriques.IdCommercant
                INNER JOIN rubriques ON ass_commercants_rubriques.IdRubrique = rubriques.IdRubrique
                INNER JOIN villes ON commercants.IdVille = villes.IdVille            
             WHERE
                commercants.IsActif = 1
                AND commercants.referencement_bonplan = 1 
                
            
            
                ";
        if ($referencement_bonplan == 1)
            $zSqlListePartenaire .= " AND commercants.referencement_bonplan = " . $referencement_bonplan . " ";

        $zSqlListePartenaire .= "
                AND (bonplan.bonplan_id <> '0' OR bonplan.bonplan_id <> NULL OR bonplan.bonplan_id <> 0) 

        ";

        if (is_array($inputIdCommercant)) {
            $zSql_all_icommercant_request = "";
            $zSql_all_icommercant_request .= " (";
            if (sizeof($inputIdCommercant) == 1) {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
            } else {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                for ($i_iCommercantId = 1; $i_iCommercantId < count($inputIdCommercant); $i_iCommercantId++) {
                    $zSql_all_icommercant_request .= " OR commercants.IdCommercant = '" . $inputIdCommercant[$i_iCommercantId] . "' ";
                }
            }
            $zSql_all_icommercant_request .= " ) ";

            //$zSqlListePartenaire .= $zSql_all_icommercant_request;
        } else if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }



        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);


            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $date_of_day . "' AND bonplan.bonplan_date_fin <= '" . $date_of_day . "' ) OR ( bonplan.bonplan_date_debut IS NULL AND bonplan.bonplan_date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $next_saturday_day . "' AND bonplan.bonplan_date_fin <= '" . $next_sunday_day . "') OR (bonplan.bonplan_date_fin >= '" . $next_saturday_day . "' AND bonplan.bonplan_date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $last_sunday_day . "' AND bonplan.bonplan_date_fin <= '" . $next_saturday_day . "') OR (bonplan.bonplan_date_fin >= '" . $last_sunday_day . "' AND bonplan.bonplan_date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $last_sunday_of_next_monday . "' AND bonplan.bonplan_date_fin <= '" . $next_saturday_of_next_monday . "') OR (bonplan.bonplan_date_fin >= '" . $last_sunday_of_next_monday . "' AND bonplan.bonplan_date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_month . "' AND bonplan.bonplan_date_fin >= '" . $date_of_day . "') )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (bonplan.bonplan_date_debut >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') OR (bonplan.bonplan_date_fin >= '" . $first_day_user_month . "' AND bonplan.bonplan_date_fin <= '" . $last_day_user_month . "') )";

        }

        if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null && $_iVilleId != "0") {
            if (is_array($_iVilleId)) {
                $zSql_all_iville_request = "";
                $zSql_all_iville_request .= " (";
                if (sizeof($_iVilleId) == 1) {
                    if ($_iVilleId[0] != 0 && $_iVilleId[0] != "" && $_iVilleId[0] != null && $_iVilleId[0] != "0") {
                        $zSql_all_iville_request .= " commercants.IdVille = '" . $_iVilleId[0] . "' ";
                    } else {
                        $zSql_all_iville_request .= " 0=0 ";
                    }
                } else {
                    $zSql_all_iville_request .= " commercants.IdVille = '" . $_iVilleId[0] . "' ";
                    for ($i_iVilleId = 1; $i_iVilleId < count($_iVilleId); $i_iVilleId++) {
                        $zSql_all_iville_request .= " OR commercants.IdVille = '" . $_iVilleId[$i_iVilleId] . "' ";
                    }
                }
                $zSql_all_iville_request .= " ) ";
                ////$this->firephp->log($zSql_all_iville_request, 'zSql_all_iville_request');
                //$zSqlListePartenaire .= $zSql_all_iville_request;
            }
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " commercants.IdVille =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            $zSqlListePartenaire .= " AND bonplan.bonplan_date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            $zSqlListePartenaire .= " AND bonplan.bonplan_date_fin = '" . $inputDatefin . "'";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";
        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " rubriques.IdRubrique = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " rubriques.IdRubrique = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR rubriques.IdRubrique = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }
        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " sous_rubriques.IdSousRubrique = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " sous_rubriques.IdSousRubrique = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR sous_rubriques.IdSousRubrique = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }







        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);

        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(commercant.nom_societe ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(bonplan.bonplan_titre ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(bonplan.bonplan_texte ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(agenda_subcateg.subcateg ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }


        // $zSqlListePartenaire .= " ) ";

        if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") || (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) {
            $zSqlListePartenaire .= " AND ( ";

            if (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") $zSqlListePartenaire .= $zSql_all_icommercant_request;
            if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") && (isset($zSql_all_iville_request) && $zSql_all_iville_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_iville_request;
            else if (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") $zSqlListePartenaire .= $zSql_all_iville_request;
            if (((isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "")) && (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
            else if (isset($zSql_all_categ_request) && $zSql_all_categ_request != "") $zSqlListePartenaire .= $zSql_all_categ_request;

            $zSqlListePartenaire .= " ) ";
        }

        $zSqlListePartenaire .= " AND (commercants.IsActif = '1' OR commercants.IsActif = '2') ";

        $zSqlListePartenaire .= " group by rubriques.IdRubrique ";

        //if ($iOrderBy == '1') $iOrderBy_value = " order by agenda.last_update desc ";
        //else if ($iOrderBy == '2') $iOrderBy_value = " order by agenda.accesscount desc ";
        // $iOrderBy_value = " order by annonce_categ.category asc ";

        //$zSqlListePartenaire .= $iOrderBy_value;

        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $_limitend;
        //echo $zSqlListePartenaire;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListePartenaire');
        //die("<br/>STOP");

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }
    function GetBonplanCategoriePrincipale()
    {
        $sqlcat = "
            SELECT
            rubriques.IdRubrique,
            rubriques.Nom,
            COUNT(bonplan.bonplan_id) as nb_bonplan
            FROM
            commercants
            INNER JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
            INNER JOIN bonplan ON commercants.IdCommercant = bonplan.bonplan_commercant_id
            INNER JOIN rubriques ON rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique
            WHERE
            commercants.IsActif = 1 AND commercants.referencement_bonplan = 1 ";


        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcat .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcat .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcat .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
            }
            $sqlcat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcat .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
            GROUP BY
            rubriques.IdRubrique
            ORDER BY rubriques.Nom
        ";

        //log_message('error', 'CHECKING_BY_SUB_CATEG_NUMBER_BONPLAN : ' . $sqlcat);
        $qryCategorie = $this->db->query($sqlcat);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }
    function GetByCommercantRefBonplan($bonplan_id=0)
    {
        $Query_txt = "
            SELECT
            bonplan.*,
            villes.ville_nom,
            commercants.NomSociete,
            rubriques.Nom,
            sous_rubriques.Nom

            FROM
                bonplan
INNER JOIN commercants ON bonplan.bonplan_commercant_id = commercants.IdCommercant
INNER JOIN villes ON bonplan.bonplan_ville_id = villes.IdVille
INNER JOIN ass_commercants_rubriques ON commercants.IdCommercant = ass_commercants_rubriques.IdCommercant
INNER JOIN rubriques ON ass_commercants_rubriques.IdRubrique = rubriques.IdRubrique
INNER JOIN ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
INNER JOIN sous_rubriques ON ass_commercants_sousrubriques.IdSousRubrique = sous_rubriques.IdSousRubrique

            WHERE
            commercants.referencement_bonplan >= 1
        "; //var_dump($Query_txt); die();
        $qry = $this->db->query($Query_txt);
        $result_main = $qry->row();
        return $result_main;
    }
    function deleteOldAgenda_fin8jours()
    {
        $date_of_day = date("Y-m-d");
        $time_date = strtotime($date_of_day);
        $time_date_limit = $time_date - 581200;
        $time_date_limit_article = $time_date - 2901200;
        $date_limit = date("Y-m-d", $time_date_limit);
        $date_limit_article = date("Y-m-d", $time_date_limit_article);

        //REMOVING ALL WITH DATE_FIN < MORE 1 WEEK FOR AGENDA
        $zSqlAlldeposantAgendaDatetime = "
            SELECT bonplan.bonplan_id, COUNT(bonplan.bonplan_id) as nb_bonplan FROM bonplan 
            WHERE  bonplan.bonplan_date_fin <= '" . $date_limit . "'
            GROUP BY bonplan.bonplan_id 
             ;";
        $bonplan_list_expire = $this->db->query($zSqlAlldeposantAgendaDatetime);
        $bonplan_list_expire_result = $bonplan_list_expire->result();
        if (count($bonplan_list_expire_result>0)) {
            foreach ($bonplan_list_expire_result as $item_n) {
                if (intval($item_n->nb_agenda_time)<=1) {
                    $this->db->query(" DELETE FROM bonplan WHERE bonplan.bonplan_id= ".$item_n->bonplan_id);
                    $this->db->query(" DELETE FROM bonplan WHERE bonplan.bonplan_id= ".$item_n->bonplan_id);
                } else {
                    $sql_date_time_agenda_id = "
                    DELETE
                    FROM
                    bonplan
                    WHERE
                    bonplan_id = '".$item_n->bonplan_id."'
                    AND bonplan.bonplan_date_fin <= '" . $date_limit . "' ;";
                    $this->db->query($sql_date_time_agenda_id);
                }
            }
        }

        //REMOVING ALL WITH DATE_FIN < MORE 1 WEEK FOR ARTICLE
        $zSqlAlldeposantarticleDatetime_article = "
            SELECT bonplan.bonplan_id, COUNT(bonplan.bonplan_id) as nb_bonplan_time FROM bonplan 
            WHERE  bonplan.bonplan_date_fin <= '" . $date_limit_article . "'
            GROUP BY bonplan_id 
             ;";
        $article_datetime_list_expire = $this->db->query($zSqlAlldeposantarticleDatetime_article);
        $article_datetime_list_expire_result = $article_datetime_list_expire->result();
        if (count($article_datetime_list_expire_result>0)) {
            foreach ($article_datetime_list_expire_result as $item_n) {
                if (intval($item_n->nb_article_time)<=1) {
                    $this->db->query(" DELETE FROM bonplan WHERE bonplan_id= ".$item_n->bonplan_id);
                    $this->db->query(" DELETE FROM bonplan WHERE bonplan_id= ".$item_n->bonplan_id);
                } else {
                    $sql_date_time_article_id = "
                    DELETE
                    FROM
                    bonplan
                    WHERE
                    bonplan_id = '".$item_n->bonplan_id."'
                    AND bonplan.bonplan_date_fin <= '" . $date_limit_article . "' ;";
                    $this->db->query($sql_date_time_article_id);
                }
            }
        }
}

    public function get_bonplan_by_id($id){
        $this->db->where('bonplan_id',$id);
        $res= $this->db->get('bonplan');
        return $res->row();
    }
    function listeBonPlanRecherche_sans_referencement_bonplan($_iCategorieId = 0, $i_CommercantId = 0, $_zMotCle = "", $_iFavoris = "", $_limitstart = 0, $_limitend = 10000, $_iIdVille = 0, $_iIdDepartement = 0, $iOrderBy="", $inputFromGeo = "0", $inputGeoValue = "10", $inputGeoLatitude="0", $inputGeoLongitude="0", $session_iWhereMultiple = "",$inputString_bonplan_type=""){
        $txtWhereFavoris = "";

        $thisss =& get_instance();
        $thisss->load->library('ion_auth');
        $thisss->load->model("ion_auth_used_by_club");

        if ($thisss->ion_auth->logged_in()){
            $user_ion_auth = $thisss->ion_auth->user()->row();
            $iduser = $thisss->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            if ($iduser==null || $iduser==0 || $iduser==""){
                $iduser = $thisss->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
            }
        } else $iduser=0;


        if($_iFavoris == "1" && $iduser != "") {
            $sqlFavoris = "
                SELECT
                    CONCAT('(',GROUP_CONCAT(ass_commercants_users.IdCommercant SEPARATOR ','),')') AS IdCommercant
                FROM
                    ass_commercants_users
                WHERE
                    ass_commercants_users.IdUser = '" . $iduser . "'
                GROUP BY ass_commercants_users.IdUser
            ";
            $QueryFavoris = $this->db->query($sqlFavoris) ;
            if($QueryFavoris->num_rows() > 0) {
                $colFavoris = $QueryFavoris->result();
                $txtWhereFavoris .= " AND commercants.IdCommercant IN " . $colFavoris[0]->IdCommercant;
            } else {
                $txtWhereFavoris .= " AND commercants.IdCommercant IN (0) ";
            }
        }

        $zSqlListeBonPlan = "
            SELECT
            bonplan.*,
            commercants.Photo5 AS Photo5,
            commercants.NomSociete AS NomSociete,
            villes.Nom AS ville,
            commercants.Adresse1 AS quartier,
            commercants.Adresse2 AS rue,
            commercants.IdCommercant,
            sous_rubriques.IdSousRubrique,
            sous_rubriques.IdRubrique,
            sous_rubriques.Nom,
            commercants.IdVille,
            commercants.nom_url";

        if ($inputFromGeo == "1") {
            $zSqlListeBonPlan .= " , (6371 * acos(cos(radians($inputGeoLatitude)) * cos(radians(commercants.latitude)) * cos(radians(commercants.longitude) - radians($inputGeoLongitude)) + sin(radians($inputGeoLatitude)) * sin(radians(commercants.latitude)))) AS distance ";
        }

        $zSqlListeBonPlan .= "
            FROM
            bonplan
            Inner Join commercants ON commercants.IdCommercant = bonplan.bonplan_commercant_id
            Left Join villes ON villes.IdVille = commercants.IdVille
            Inner Join ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
            Inner Join sous_rubriques ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique
            WHERE
            commercants.IsActif = 1 AND ( (bp_unique_date_fin >= NOW()) OR (bp_multiple_date_fin >= NOW()) OR (bonplan_date_fin >= NOW()) )  
        ";

        if ($inputFromGeo == "1") {
            $zSqlListeBonPlan .= " AND commercants.latitude != '' AND commercants.longitude != '' ";
        }

        /*if($_iCategorieId != 0){
            $zSqlListeBonPlan .= " AND sous_rubriques.IdSousRubrique =" . $_iCategorieId ;
        }*/
        if(isset($_iCategorieId) && is_array($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0){
            $zSqlListeBonPlan .= " AND (";
            if (sizeof($_iCategorieId)==1){
                $zSqlListeBonPlan .= " sous_rubriques.IdSousRubrique = " . $_iCategorieId[0];
            } else {
                $zSqlListeBonPlan .= " sous_rubriques.IdSousRubrique = " . $_iCategorieId[0];
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand ++) {
                    $zSqlListeBonPlan .= " OR sous_rubriques.IdSousRubrique = " . $_iCategorieId[$ii_rand];
                }
            }
            $zSqlListeBonPlan .= " )";
        }
        if($i_CommercantId != 0){
            $zSqlListeBonPlan .= " AND commercants.IdCommercant = '$i_CommercantId' " ;
        }
        /*
        if($_zMotCle != ""){
            $zSqlListeBonPlan .= " AND bonplan_titre LIKE '%" . $_zMotCle . "%'" ;
        }
        */
        $SearchedWordsString = str_replace("+", " ", $_zMotCle) ;
        $SearchedWordsString = str_replace("'", "\'", $SearchedWordsString) ;
        $SearchedWordsString = str_replace('"', '\"', $SearchedWordsString) ;
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString) ;

        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i ++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search) ;
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListeBonPlan .= "\n AND " ;
                }
                $zSqlListeBonPlan .= " ( \n" .
                    " UPPER(bonplan.bonplan_titre ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(bonplan.bonplan_texte ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.Adresse1 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.Adresse2 ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(commercants.NomSociete ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(villes.Nom ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                ";
                if ($i != (sizeof($SearchedWords) - 1)){
                    $zSqlListeBonPlan .= "\n OR ";
                }
            }
        }
        if($_iFavoris == "1" && $iduser != "") {
            $zSqlListeBonPlan .= $txtWhereFavoris;
        }

        //add ville id list on query
        //
        if($_iIdVille != 0 && $_iIdVille != "" && $_iIdVille != null){
            $zSqlListeBonPlan .= " AND commercants.IdVille=" . $_iIdVille ;
        } elseif ($_iIdDepartement != 0 && $_iIdDepartement != "" && $_iIdDepartement != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iIdDepartement);
            if (count($villeByIdDepartement)>0) {
                $zSqlListeBonPlan .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListeBonPlan .= " commercants.IdVille=" . $key_villeDep->IdVille ;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListeBonPlan .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListeBonPlan .= ")";
            }
        }


        if ($session_iWhereMultiple != "") {
            $zSqlListeBonPlan .= " AND bonplan.bon_plan_utilise_plusieurs = '".$session_iWhereMultiple."' ";
        }

        if ($inputString_bonplan_type != "" && $inputString_bonplan_type!="0" && $inputString_bonplan_type!=null && $inputString_bonplan_type!=0) {
            $zSqlListeBonPlan .= " AND bonplan.bonplan_type = '".$inputString_bonplan_type."' ";
        }

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlListeBonPlan .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlListeBonPlan .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlListeBonPlan .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlListeBonPlan .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListeBonPlan .= " OR ";
            }
            $zSqlListeBonPlan .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlListeBonPlan .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE



        //SORTEZ.ORG => NO BASIC PARTNER ACCOUT
        $zSqlListeBonPlan .= " AND commercants.user_ionauth_id IN (SELECT users_groups_ionauth.user_ionauth_id FROM users_groups_ionauth WHERE users_groups_ionauth.group_ionauth_id = 4 OR users_groups_ionauth.group_ionauth_id = 5) ";
        //SORTEZ.ORG => NO BASIC PARTNER ACCOUT

        // demo account commercant should not appear on annuaire list
        $zSqlListeBonPlan .= " AND commercants.IdCommercant <> '301298'  AND commercants.IdCommercant <> '301266' ";
        //$zSqlListeBonPlan .= " AND commercants.IdCommercant <> '301298' ";

        if ($inputFromGeo == "1") {
            $zSqlListeBonPlan .= "  HAVING distance < $inputGeoValue  ";
        }

        if ($iOrderBy=='1') $iOrderBy_value = " order by bonplan.bonplan_id desc ";
        else if ($iOrderBy=='2') $iOrderBy_value = " order by commercants.nbrevisites desc ";
        else if ($iOrderBy=='3') $iOrderBy_value = " order by bonplan.bonplan_id asc ";
        else $iOrderBy_value = " order by bonplan_date_fin asc ";

        $zSqlListeBonPlan .= $iOrderBy_value;

        $zSqlListeBonPlan .= " LIMIT ".$_limitstart.",".$_limitend ;

        //echo $zSqlListeBonPlan."<br/><br/>";
        ////$this->firephp->log($iOrderBy, 'iOrderBy_3');

        $zQueryListeBonPlan = $this->db->query($zSqlListeBonPlan) ;
        return $zQueryListeBonPlan->result() ;
    }
}