<?php

class mdlcategorie extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function GetAutocompleteCategorie($queryString = 0)
    {
        $Sql = "select * from rubriques where Nom like '%$queryString%'  order by Nom ";
        $Query = $this->db->query($Sql);
        return $Query->result();
    }

    function GetAll()
    {
        $qryCategorie = $this->db->query("
            SELECT * 
            FROM
                rubriques
            ORDER BY Nom ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAllSousrubriqueByRubrique($IdRubrique)
    {
        $qryCategorie = $this->db->query("
            SELECT * 
            FROM
                sous_rubriques
            WHERE 
                IdRubrique = '" . $IdRubrique . "'
            ORDER BY Nom ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetCommercantSouscategorie()
    {
        $queryCat = "
            SELECT
            sous_rubriques.IdRubrique,
            sous_rubriques.Nom,
            SousRubriqueCom.IdCommercant,
            count(sous_rubriques.IdSousRubrique) as nbCommercant
            FROM
            sous_rubriques
           INNER JOIN (
            SELECT
            ass_commercants_sousrubriques.IdCommercant,
            ass_commercants_sousrubriques.IdSousRubrique
            FROM
            ass_commercants_sousrubriques
            Inner Join commercants ON commercants.IdCommercant = ass_commercants_sousrubriques.IdCommercant
            WHERE
            commercants.IsActif =  '1' AND
            ass_commercants_sousrubriques.IdSousRubrique <>  '0'
            ) as SousRubriqueCom ON sous_rubriques.IdSousRubrique = SousRubriqueCom.IdSousRubrique
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $queryCat .= "
            INNER JOIN commercants on SousRubriqueCom.IdCommercant = commercants.IdCommercant
            AND commercants.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0" && is_numeric($localdata_IdVille)) {
            $queryCat .= "
            INNER JOIN commercants on SousRubriqueCom.IdCommercant = commercants.IdCommercant
            AND commercants.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $queryCat .= "
            INNER JOIN commercants on SousRubriqueCom.IdCommercant = commercants.IdCommercant ";
            $queryCat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $queryCat .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $queryCat .= " OR ";
            }
            $queryCat .= " ) ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $queryCat .= " INNER JOIN commercants on SousRubriqueCom.IdCommercant = commercants.IdCommercant
            AND commercants.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE

        $queryCat .= "
            GROUP BY
            sous_rubriques.IdSousRubrique, SousRubriqueCom.IdCommercant
        ";

        $qryCategorie = $this->db->query($queryCat);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    

    function GetCommercantCategorie()
    {

        $qryCategorie = $this->db->query("
            SELECT
            rubriques.IdRubrique,
            rubriques.Nom,
            COUNT(commercants.IdCommercant)  nbCommercant
            FROM
            commercants
            LEFT OUTER JOIN bonplan ON commercants.IdCommercant = bonplan.bonplan_commercant_id
            LEFT OUTER JOIN annonce ON commercants.IdCommercant = annonce.annonce_commercant_id
            INNER JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
            Inner Join ass_commercants_abonnements ON commercants.IdCommercant = ass_commercants_abonnements.IdCommercant
            INNER JOIN rubriques ON rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique
            WHERE
            commercants.IsActif = 1 AND commercants.referencement_annuaire = 1 AND
            ass_commercants_rubriques.IdRubrique !=0 AND commercants.IdCommercant <> 301299 
AND commercants.user_ionauth_id IN (SELECT users_groups_ionauth.user_ionauth_id FROM users_groups_ionauth WHERE users_groups_ionauth.group_ionauth_id = 4 OR users_groups_ionauth.group_ionauth_id = 5)
            GROUP BY
            rubriques.IdRubrique
            ORDER BY rubriques.Nom
            ");//DATEDIFF(ass_commercants_abonnements.DateFin ,CURDATE()) >0


        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetCommercantSouscategorieByRubrique($idRubrique)
    {
        /*$qryCategorie = $this->db->query("
            SELECT
            sous_rubriques.IdSousRubrique,
            sous_rubriques.IdRubrique,
            sous_rubriques.Nom,
            SousRubriqueCom.IdCommercant,
            count(sous_rubriques.IdSousRubrique) as nbCommercant
            FROM
            sous_rubriques
           INNER JOIN (
            SELECT
            ass_commercants_sousrubriques.IdCommercant,
            ass_commercants_sousrubriques.IdSousRubrique
            FROM
            ass_commercants_sousrubriques
            Inner Join commercants ON commercants.IdCommercant = ass_commercants_sousrubriques.IdCommercant
            WHERE
            commercants.IsActif =  '1' AND
            ass_commercants_sousrubriques.IdSousRubrique <>  '0'
            ) as SousRubriqueCom ON sous_rubriques.IdSousRubrique = SousRubriqueCom.IdSousRubrique
            WHERE sous_rubriques.IdRubrique = '".$idRubrique."'
            GROUP BY
            sous_rubriques.IdSousRubrique
        ");*/
        $qryCategorie = $this->db->query("
            SELECT
            sous_rubriques.IdSousRubrique,
            sous_rubriques.Nom,
            COUNT(commercants.IdCommercant) as nbCommercant
            FROM
            sous_rubriques
            INNER JOIN ass_commercants_sousrubriques ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique
            INNER JOIN commercants ON commercants.IdCommercant = ass_commercants_sousrubriques.IdCommercant
            WHERE
            commercants.IsActif = 1 AND
            ass_commercants_sousrubriques.IdSousRubrique <> 0 AND
            sous_rubriques.IdRubrique = '" . $idRubrique . "'
            GROUP BY
            sous_rubriques.IdSousRubrique

        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetCommercantSouscategorieByRubrique_x($idRubrique)
    {
        $qry = "
            SELECT
               sous_rubriques.IdSousRubrique,
               sous_rubriques.Nom,
               COUNT(commercants.IdCommercant) as nbCommercant
            FROM
                commercants
                INNER JOIN villes ON villes.IdVille = commercants.IdVille
		        INNER JOIN ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
                INNER JOIN  sous_rubriques ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique
                INNER JOIN  rubriques ON rubriques.IdRubrique = sous_rubriques.IdRubrique
            WHERE
                commercants.IsActif = 1";


        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $qry .= " AND villes.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $qry .= " AND villes.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $qry .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $qry .= " villes.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $qry .= " OR ";
            }
            $qry .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $qry .= " AND villes.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        if ($idRubrique != "0" && $idRubrique != "" && $idRubrique != null) $qry .= " AND rubriques.IdRubrique = '" . $idRubrique . "' ";
        $qry .= "
            GROUP BY sous_rubriques.IdSousRubrique

        ";

        $qryCategorie = $this->db->query($qry);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetCommercantSouscategorieByRubriqueVilleDepartement_x($idRubrique, $IdVille, $_iIdDepartement)
    {
        $qry = "
            SELECT
               sous_rubriques.IdSousRubrique,
               sous_rubriques.Nom,
               COUNT(commercants.IdCommercant) as nbCommercant
            FROM
                commercants
                INNER JOIN villes ON villes.IdVille = commercants.IdVille
                INNER JOIN ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
                INNER JOIN  sous_rubriques ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique
                INNER JOIN  rubriques ON rubriques.IdRubrique = sous_rubriques.IdRubrique
            WHERE
                commercants.IsActif = 1";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $qry .= " AND villes.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $qry .= " AND villes.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $qry .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $qry .= " villes.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $qry .= " OR ";
            }
            $qry .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $qry .= " AND villes.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        if ($idRubrique != "0" && $idRubrique != "" && $idRubrique != null) $qry .= " AND rubriques.IdRubrique = '" . $idRubrique . "' ";
        //if ($IdVille!="0" && $IdVille!="" && $IdVille!=null) $qry .= " AND villes.IdVille = '".$IdVille."' ";
        if ($IdVille != "0" && $IdVille != "" && $IdVille != null) {
            $qry .= " AND villes.IdVille = '" . $IdVille . "' ";
        } elseif ($_iIdDepartement != "0" && $_iIdDepartement != "" && $_iIdDepartement != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iIdDepartement);
            if (count($villeByIdDepartement) > 0) {
                $qry .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $qry .= " villes.IdVille=" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $qry .= " OR ";
                    }
                    $iii_villedep++;
                }
                $qry .= ")";
            }
        }

        $qry .= "
                GROUP BY sous_rubriques.IdSousRubrique

            ";

        //echo "<br/>".$qry."<br/>";

        $qryCategorie = $this->db->query($qry);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetCommercantSouscategorieByRubriqueVille_x($idRubrique, $IdVille)
    {
        $qry = "
            SELECT
               sous_rubriques.IdSousRubrique,
               sous_rubriques.Nom,
               COUNT(commercants.IdCommercant) as nbCommercant
            FROM
                commercants
                INNER JOIN villes ON villes.IdVille = commercants.IdVille
		        INNER JOIN ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
                INNER JOIN  sous_rubriques ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique
                INNER JOIN  rubriques ON rubriques.IdRubrique = sous_rubriques.IdRubrique
            WHERE
                commercants.IsActif = 1";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $qry .= " AND villes.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $qry .= " AND villes.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $qry .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $qry .= " villes.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $qry .= " OR ";
            }
            $qry .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $qry .= " AND villes.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE               

        if ($idRubrique != "0" && $idRubrique != "" && $idRubrique != null) $qry .= " AND rubriques.IdRubrique = '" . $idRubrique . "' ";
        if ($IdVille != "0" && $IdVille != "" && $IdVille != null) $qry .= " AND villes.IdVille = '" . $IdVille . "' ";

        $qry .= "
                GROUP BY sous_rubriques.IdSousRubrique

            ";

        $qryCategorie = $this->db->query($qry);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetCommercantSouscategorieByRubriqueIdVille($idRubrique, $IdVille)
    {
        $qryCategorie = $this->db->query("
            SELECT
            sous_rubriques.IdSousRubrique,
            sous_rubriques.IdRubrique,
            sous_rubriques.Nom,
            SousRubriqueCom.IdCommercant,
            count(sous_rubriques.IdSousRubrique) as nbCommercant
            FROM
            sous_rubriques
           INNER JOIN (
            SELECT
            ass_commercants_sousrubriques.IdCommercant,
            ass_commercants_sousrubriques.IdSousRubrique
            FROM
            ass_commercants_sousrubriques
            Inner Join commercants ON commercants.IdCommercant = ass_commercants_sousrubriques.IdCommercant
            WHERE
            commercants.IsActif =  '1' AND
            commercants.IdVille =  '" . $IdVille . "' AND
            ass_commercants_sousrubriques.IdSousRubrique <>  '0'
            ) as SousRubriqueCom ON sous_rubriques.IdSousRubrique = SousRubriqueCom.IdSousRubrique
            WHERE sous_rubriques.IdRubrique = '" . $idRubrique . "'
            GROUP BY
            sous_rubriques.IdSousRubrique
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    

    function GetCommercantCategorie_x()
    {

        $sqlCategorie = "
            SELECT
                   rubriques.IdRubrique,
                   rubriques.Nom,
                   COUNT(commercants.IdCommercant) nbCommercant
                FROM
                    commercants
                    INNER JOIN villes ON villes.IdVille = commercants.IdVille
                    INNER JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
                    Inner Join ass_commercants_abonnements ON commercants.IdCommercant = ass_commercants_abonnements.IdCommercant
                    INNER JOIN  rubriques ON rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique
                WHERE
                    commercants.IsActif = 1 AND
                    ass_commercants_rubriques.IdRubrique <> 0 ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlCategorie .= " AND villes.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlCategorie .= " AND villes.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlCategorie .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlCategorie .= " villes.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlCategorie .= " OR ";
            }
            $sqlCategorie .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlCategorie .= " AND villes.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $sqlCategorie .= "
                GROUP BY ass_commercants_rubriques.IdRubrique
                ORDER BY rubriques.Nom
            ";

        $qryCategorie = $this->db->query($sqlCategorie);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetCommercantCategoriebyVille($IdVille)
    {
        /*$qryCategorie = $this->db->query("
            SELECT
                    rubriques.IdRubrique,
                    rubriques.Nom,
                    RubriqueCom.IdCommercant,
                    count(RubriqueCom.IdCommercant) as nbCommercant
                    FROM
                    rubriques
             INNER JOIN (
                    SELECT
                    ass_commercants_rubriques.IdCommercant,
                    ass_commercants_rubriques.IdRubrique
                    FROM
                    ass_commercants_rubriques
                    Inner Join commercants ON commercants.IdCommercant = ass_commercants_rubriques.IdCommercant
                    WHERE
                    commercants.IsActif =  '1' AND
                    ass_commercants_rubriques.IdRubrique <>  '0' AND
                    commercants.IdVille = '".$IdVille."'
                    ) as RubriqueCom ON rubriques.IdRubrique = RubriqueCom.IdRubrique
                    GROUP BY
                    rubriques.IdRubrique

        ");*/
        $qryCategorie = $this->db->query("
            SELECT
            rubriques.IdRubrique,
            rubriques.Nom,
            COUNT(commercants.IdCommercant) nbCommercant
            FROM
            commercants
            INNER JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
            INNER JOIN rubriques ON rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique
            WHERE
            commercants.IsActif = 1 AND
            ass_commercants_rubriques.IdRubrique <> 0 AND
            commercants.IdVille = '" . $IdVille . "'
            GROUP BY
            rubriques.IdRubrique
        ");
        ////$this->firephp->log($qryCategorie, 'qryCategorie');
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetCommercantCategoriebyVille_x($_iIdDepartement = "0", $IdVille)
    {

        $qry = "
            SELECT
               rubriques.IdRubrique,
               rubriques.Nom,
               COUNT(commercants.IdCommercant) nbCommercant
            FROM
                commercants
                INNER JOIN villes ON villes.IdVille = commercants.IdVille
		        INNER JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
                Inner Join ass_commercants_abonnements ON commercants.IdCommercant = ass_commercants_abonnements.IdCommercant
                INNER JOIN  rubriques ON rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique
            WHERE
                commercants.IsActif = 1 AND
                ass_commercants_rubriques.IdRubrique <> 0


                ";//DATEDIFF(ass_commercants_abonnements.DateFin ,CURDATE()) >0

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $qry .= " AND villes.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $qry .= " AND villes.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $qry .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $qry .= " villes.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $qry .= " OR ";
            }
            $qry .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $qry .= " AND villes.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        //if ($IdVille!="0" && $IdVille!="" && $IdVille!=null) $qry .= " AND villes.IdVille = '".$IdVille."' ";

        if ($IdVille != "0" && $IdVille != "" && $IdVille != null) {
            $qry .= " AND villes.IdVille = '" . $IdVille . "' ";
        } elseif ($_iIdDepartement != "0" && $_iIdDepartement != "" && $_iIdDepartement != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iIdDepartement);
            if (count($villeByIdDepartement) > 0) {
                $qry .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $qry .= " villes.IdVille=" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $qry .= " OR ";
                    }
                    $iii_villedep++;
                }
                $qry .= ")";
            }
        }


        $qry .= "
            GROUP BY ass_commercants_rubriques.IdRubrique
            ORDER BY rubriques.Nom
        ";

        $qryCategorie = $this->db->query($qry);

        //$this->firephp->log($qryCategorie, 'qryCategorie');

        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetBonplanSouscategorie()
    {
        $sqlcateg = "
            SELECT
            sous_rubriques.IdRubrique,
            sous_rubriques.Nom,
            commercants.IdCommercant,
            sous_rubriques.IdSousRubrique,
            count(bonplan.bonplan_id) as nb_bonplan
            FROM
            sous_rubriques
            Inner Join ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdSousRubrique = sous_rubriques.IdSousRubrique
            Inner Join commercants ON commercants.IdCommercant = ass_commercants_sousrubriques.IdCommercant
            Inner Join bonplan ON bonplan.bonplan_commercant_id = commercants.IdCommercant
            WHERE commercants.IsActif = 1";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcateg .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcateg .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcateg .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcateg .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcateg .= " OR ";
            }
            $sqlcateg .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcateg .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $sqlcateg .= "
            GROUP BY
            sous_rubriques.IdSousRubrique, commercants.IdCommercant
        ";

        $qryCategorie = $this->db->query($sqlcateg);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetBonplanSouscategorieByRubrique($IdRubrique)
    {

        $qry = "
            SELECT
            sous_rubriques.IdRubrique,
            sous_rubriques.Nom,
            sous_rubriques.IdSousRubrique,
            count(bonplan.bonplan_id) as nb_bonplan
            FROM
            sous_rubriques
            Inner Join ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdSousRubrique = sous_rubriques.IdSousRubrique
            Inner Join commercants ON commercants.IdCommercant = ass_commercants_sousrubriques.IdCommercant
            Inner Join bonplan ON bonplan.bonplan_commercant_id = commercants.IdCommercant
            WHERE commercants.IsActif = 1 ";


        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $qry .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $qry .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $qry .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $qry .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $qry .= " OR ";
            }
            $qry .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $qry .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        if ($IdRubrique != "0" && $IdRubrique != "" && $IdRubrique != null) $qry .= " AND sous_rubriques.IdRubrique = '" . $IdRubrique . "' ";
        $qry .= "
            GROUP BY
            sous_rubriques.IdSousRubrique
       ";

        //echo $qry."<br/><br/>";
        //log_message('error', 'CHECKING_BY_SUB_CATEG_NUMBER_BONPLAN : ' . $qry);

        $qryCategorie = $this->db->query($qry);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetBonplanCategoriePrincipale()
    {
        $sqlcat = "
            SELECT
            rubriques.IdRubrique,
            rubriques.Nom,
            COUNT(bonplan.bonplan_id) as nb_bonplan
            FROM
            commercants
            INNER JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
            INNER JOIN bonplan ON commercants.IdCommercant = bonplan.bonplan_commercant_id
            INNER JOIN rubriques ON rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique
            WHERE
            commercants.IsActif = 1 AND commercants.referencement_bonplan = 1 ";


        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcat .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcat .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcat .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
            }
            $sqlcat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcat .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
            GROUP BY
            rubriques.IdRubrique
            ORDER BY rubriques.Nom
        ";

        //log_message('error', 'CHECKING_BY_SUB_CATEG_NUMBER_BONPLAN : ' . $sqlcat);
        $qryCategorie = $this->db->query($sqlcat);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetBonplanCategoriePrincipaleByVille($_iIdDepartement = "0", $IdVille)
    {

        $qry = "
            SELECT
            rubriques.IdRubrique,
            rubriques.Nom,
            COUNT(bonplan.bonplan_id) as nb_bonplan
            FROM
            commercants
            INNER JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
            INNER JOIN bonplan ON commercants.IdCommercant = bonplan.bonplan_commercant_id
            INNER JOIN rubriques ON rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique
            WHERE
            commercants.IsActif = 1 AND commercants.referencement_bonplan = 1 ";
        if ($IdVille != "0" && $IdVille != "" && $IdVille != null) {
            $qry .= " AND commercants.IdVille = '" . $IdVille . "' ";
        } elseif ($_iIdDepartement != "0" && $_iIdDepartement != "" && $_iIdDepartement != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iIdDepartement);
            if (count($villeByIdDepartement) > 0) {
                $qry .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $qry .= " commercants.IdVille=" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $qry .= " OR ";
                    }
                    $iii_villedep++;
                }
                $qry .= ")";
            }
        }


        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $qry .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $qry .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $qry .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $qry .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $qry .= " OR ";
            }
            $qry .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $qry .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $qry .= "
            GROUP BY
            rubriques.IdRubrique
            ORDER BY rubriques.Nom
       ";

        $qryCategorie = $this->db->query($qry);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetBonplanCategoriePrincipaleBycateg($_iCommercantId = "0")
    {

        $qry = "
            SELECT
            rubriques.IdRubrique,
            rubriques.Nom,
            COUNT(bonplan.bonplan_id) as nb_bonplan
            FROM
            commercants
            INNER JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
            INNER JOIN bonplan ON commercants.IdCommercant = bonplan.bonplan_commercant_id
            INNER JOIN rubriques ON rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique
            WHERE
            commercants.IsActif = 1 AND commercants.referencement_bonplan = 1 ";
        if ($_iCommercantId != "0" && $_iCommercantId != "" && $_iCommercantId != null) {
            $qry .= " AND ass_commercants_rubriques.IdCommercant = '" . $_iCommercantId . "' ";
        }


        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $qry .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $qry .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $qry .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $qry .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $qry .= " OR ";
            }
            $qry .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $qry .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $qry .= "
            GROUP BY
            rubriques.IdRubrique
            ORDER BY rubriques.Nom
       ";

        $qryCategorie = $this->db->query($qry);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetFidelitySouscategorie()
    {
        $sqlcateg = "
            SELECT
            sous_rubriques.IdRubrique,
            sous_rubriques.Nom,
            commercants.IdCommercant,
            sous_rubriques.IdSousRubrique,
            COUNT(card_remise.id) as nb_remise,
            COUNT(card_tampon.id) as nb_tampon,
            COUNT(card_capital.id) as nb_capital
            FROM
            sous_rubriques
            Inner Join ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdSousRubrique = sous_rubriques.IdSousRubrique
            Inner Join commercants ON commercants.IdCommercant = ass_commercants_sousrubriques.IdCommercant
            LEFT OUTER JOIN card_capital ON card_capital.id_commercant = commercants.IdCommercant
            LEFT OUTER JOIN card_remise ON card_remise.id_commercant = commercants.IdCommercant
            LEFT OUTER JOIN card_tampon ON card_tampon.id_commercant = commercants.IdCommercant
            WHERE 
            commercants.IsActif = 1 
            AND (commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_remise) 
            OR commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_tampon) 
            or commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_capital))
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcateg .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcateg .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcateg .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcateg .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcateg .= " OR ";
            }
            $sqlcateg .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcateg .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $sqlcateg .= "
            GROUP BY
            sous_rubriques.IdSousRubrique, commercants.IdCommercant
        ";

        $qryCategorie = $this->db->query($sqlcateg);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetFidelityCategoriePrincipale()
    {
        $sqlcat = "
            SELECT
            rubriques.IdRubrique,
            rubriques.Nom,
            COUNT(card_remise.id) as nb_remise,
            COUNT(card_tampon.id) as nb_tampon,
            COUNT(card_capital.id) as nb_capital
            FROM
            commercants
            INNER JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
            INNER JOIN rubriques ON rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique
            LEFT OUTER JOIN card_capital ON card_capital.id_commercant = commercants.IdCommercant
            LEFT OUTER JOIN card_remise ON card_remise.id_commercant = commercants.IdCommercant
            LEFT OUTER JOIN card_tampon ON card_tampon.id_commercant = commercants.IdCommercant
            WHERE 
            commercants.IsActif = 1 AND commercants.referencement_fidelite = 1  
             ";


        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcat .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcat .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcat .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
            }
            $sqlcat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcat .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        $sqlcat .= "
            AND (commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_remise WHERE is_activ = 1) 
            OR commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_tampon where is_activ = 1) 
            or commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_capital where is_activ = 1))
        ";

        $sqlcat .= "
            GROUP BY
            rubriques.IdRubrique
            ORDER BY rubriques.Nom
        ";

        //log_message('error', 'CHECKING_BY_SUB_CATEG_NUMBER_BONPLAN : ' . $sqlcat);
        $qryCategorie = $this->db->query($sqlcat);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetFidelityCategoriePrincipaleByVille($_iIdDepartement = "0", $IdVille)
    {

        $qry = "
             SELECT
            rubriques.IdRubrique,
            rubriques.Nom,
            COUNT(card_remise.id) as nb_remise,
            COUNT(card_tampon.id) as nb_tampon,
            COUNT(card_capital.id) as nb_capital
            FROM
            commercants
            INNER JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
            INNER JOIN rubriques ON rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique
            LEFT OUTER JOIN card_capital ON card_capital.id_commercant = commercants.IdCommercant
            LEFT OUTER JOIN card_remise ON card_remise.id_commercant = commercants.IdCommercant
            LEFT OUTER JOIN card_tampon ON card_tampon.id_commercant = commercants.IdCommercant
            WHERE 
            commercants.IsActif = 1 AND commercants.referencement_fidelite = 1  
            ";


        if ($IdVille != "0" && $IdVille != "" && $IdVille != null) {
            $qry .= " AND commercants.IdVille = '" . $IdVille . "' ";
        } elseif ($_iIdDepartement != "0" && $_iIdDepartement != "" && $_iIdDepartement != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iIdDepartement);
            if (count($villeByIdDepartement) > 0) {
                $qry .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $qry .= " commercants.IdVille=" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $qry .= " OR ";
                    }
                    $iii_villedep++;
                }
                $qry .= ")";
            }
        }


        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $qry .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $qry .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $qry .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $qry .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $qry .= " OR ";
            }
            $qry .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $qry .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        $qry .= "
            AND (commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_remise WHERE is_activ = 1) 
            OR commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_tampon where is_activ = 1) 
            or commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_capital where is_activ = 1))
        ";

        $qry .= "
            GROUP BY
            rubriques.IdRubrique
            ORDER BY rubriques.Nom
        ";

        $qryCategorie = $this->db->query($qry);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetFidelitySouscategorieByRubrique($IdRubrique)
    {

        $qry = "
            SELECT
            sous_rubriques.IdRubrique,
            sous_rubriques.Nom,
            sous_rubriques.IdSousRubrique,
            COUNT(card_remise.id) as nb_remise,
            COUNT(card_tampon.id) as nb_tampon,
            COUNT(card_capital.id) as nb_capital
            FROM
            sous_rubriques
            Inner Join ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdSousRubrique = sous_rubriques.IdSousRubrique
            Inner Join commercants ON commercants.IdCommercant = ass_commercants_sousrubriques.IdCommercant
            LEFT OUTER JOIN card_capital ON card_capital.id_commercant = commercants.IdCommercant
            LEFT OUTER JOIN card_remise ON card_remise.id_commercant = commercants.IdCommercant
            LEFT OUTER JOIN card_tampon ON card_tampon.id_commercant = commercants.IdCommercant
            WHERE commercants.IsActif = 1
            AND (commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_remise WHERE is_activ = 1) 
            OR commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_tampon where is_activ = 1) 
            or commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_capital where is_activ = 1))
            ";


        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $qry .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $qry .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $qry .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $qry .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $qry .= " OR ";
            }
            $qry .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $qry .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        if ($IdRubrique != "0" && $IdRubrique != "" && $IdRubrique != null) $qry .= " AND sous_rubriques.IdRubrique = '" . $IdRubrique . "' ";
        $qry .= "
            GROUP BY
            sous_rubriques.IdSousRubrique
       ";

        //echo $qry."<br/><br/>";

        $qryCategorie = $this->db->query($qry);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function verifier_categorie_commercant($IdRubrique)
    {
        $qryCategorie = $this->db->query("
            SELECT
            ass_commercants_rubriques.IdCommercant,
            ass_commercants_rubriques.IdRubrique
            FROM
            ass_commercants_rubriques
            WHERE
            ass_commercants_rubriques.IdRubrique = '" . $IdRubrique . "'
       ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function verifier_souscategorie_commercant($IdSousRubrique)
    {
        $qryCategorie = $this->db->query("
            SELECT
            ass_commercants_sousrubriques.IdCommercant,
            ass_commercants_sousrubriques.IdSousRubrique
            FROM
            ass_commercants_sousrubriques
            WHERE
            ass_commercants_sousrubriques.IdSousRubrique = '" . $IdSousRubrique . "'
       ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetById($argId)
    {
        $Query = $this->db->get_where("rubriques", array("IdRubrique" => $argId));
        return $Query->row();
    }

    function GetByIdSousCateg($argId)
    {
        $Query = $this->db->get_where("sous_rubriques", array("IdSousRubrique" => $argId));
        return $Query->row();
    }

    function Insert($prmData)
    {
        $this->db->insert("rubriques", $prmData);
        return $this->db->insert_id();
    }

    function InsertSousCateg($prmData)
    {
        $this->db->insert("sous_rubriques", $prmData);
        return $this->db->insert_id();
    }

    function Update($prmData)
    {
        $prmData = (array)$prmData;
        $this->db->where("IdRubrique", $prmData["IdRubrique"]);
        $this->db->update("rubriques", $prmData);
        $objCommercant = $this->GetById($prmData["IdRubrique"]);
        return $objCommercant->IdRubrique;
    }

    function UpdateSousCateg($prmData)
    {
        $prmData = (array)$prmData;
        $this->db->where("IdSousRubrique", $prmData["IdSousRubrique"]);
        $this->db->update("sous_rubriques", $prmData);
        $objCommercant = $this->GetByIdSousCateg($prmData["IdSousRubrique"]);
        return $objCommercant->IdSousRubrique;
    }

    function Delete($prmId)
    {
        $qryBonplan = $this->db->query("DELETE FROM rubriques WHERE IdRubrique = ?", $prmId);
        return $qryBonplan;
    }

    function DeleteSousCateg($prmId)
    {
        $qryBonplan = $this->db->query("DELETE FROM sous_rubriques WHERE IdSousRubrique = ?", $prmId);
        return $qryBonplan;
    }

public function getcateg(){
       $this->db->where('agenda_typeid',7);
       $res= $this->db->get("agenda_categ");
       return $res->result();
}
public function save_categ($categ){
        $this->db->where('category',$categ);
        $test=$this->db->get('agenda_categ');
    if ($test->num_rows()>0){
        return $test->row()->agenda_categid;
    }else{$field=array("category"=>$categ,
        "agenda_typeid"=>7);
        $this->db->insert('agenda_categ',$field);
        $insert_id = $this->db->insert_id();
        return  $insert_id;}
}
public function getcategoriesplat(){
    $qryCategorie = $this->db->query("
            SELECT
            rubriques.IdRubrique,
            rubriques.Nom,
            sous_rubriques.Nom AS rubrique,
            COUNT(commercants.IdCommercant)  nbCommercant
            FROM
            commercants
            LEFT OUTER JOIN bonplan ON commercants.IdCommercant = bonplan.bonplan_commercant_id
            LEFT OUTER JOIN annonce ON commercants.IdCommercant = annonce.annonce_commercant_id
            INNER JOIN plat_du_jour ON commercants.IdCommercant = plat_du_jour.IdCommercant
            INNER JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
            Inner Join ass_commercants_abonnements ON commercants.IdCommercant = ass_commercants_abonnements.IdCommercant
            INNER JOIN rubriques ON rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique
            Inner Join ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
            Inner Join sous_rubriques ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique
            WHERE
            commercants.IsActif = 1 AND commercants.referencement_resto = 1 AND
            sous_rubriques.IdSousRubrique !=0 AND commercants.IdCommercant <> 301299 
            AND commercants.user_ionauth_id IN (SELECT users_groups_ionauth.user_ionauth_id FROM users_groups_ionauth WHERE users_groups_ionauth.group_ionauth_id = 4 OR users_groups_ionauth.group_ionauth_id = 5)
            GROUP BY
            sous_rubriques.IdSousRubrique
            ORDER BY sous_rubriques.Nom
            ");//DATEDIFF(ass_commercants_abonnements.DateFin ,CURDATE()) >0


    if ($qryCategorie->num_rows() > 0) {
        return $qryCategorie->result();
    }
}
}