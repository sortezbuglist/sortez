<?php
class mdl_card extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }

	function getById($id=0){
		$Sql = "select * from card where id =". $id ;
		$Query = $this->db->query($Sql);
		return $Query->row();
	}

    function getByIdUser($id=0){
        $Sql = "select * from card where id_user ='".$id."' LIMIT 1" ;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByCard($numcard=0){
        $Sql = "
            select * from card where 0=0
             and ( num_id_card_virtual ='".$numcard."' OR num_id_card_physical ='".$numcard."' )
             LIMIT 1
             " ;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getWhere($where=""){
        $query = "
            SELECT *
            FROM
                card
            WHERE
                0=0
            ";
        if (isset($where) && $where!="") $query .= " AND ".$where;

        $qry = $this->db->query($query);
        if($qry->num_rows() > 0) {
            return $qry->result();  
        }
    }

    function getWhereOne($where=""){
        $query = "
            SELECT *
            FROM
                card
            WHERE
                0=0
            ";
        if (isset($where) && $where!="") $query .= " AND ".$where;

        $query .= " LIMIT 1";

        $Query_result = $this->db->query($query);
        return $Query_result->row();
    }

	function GetAll(){
        $qry = $this->db->query("
            SELECT * 
            FROM
                card
            ORDER BY id DESC
        ");
        if($qry->num_rows() > 0) {
            return $qry->result();
        }
    }


    function delete($prmId){
    
        $qry = $this->db->query("DELETE FROM card WHERE id = ?", $prmId) ;
        return $qry ;
    }

    function insert($prmData) {
        $this->db->insert("card", $prmData);
        return $this->db->insert_id();
    }

    function update($prmData) {
        $this->db->where("id", $prmData["id"]);
        $this->db->update("card", $prmData);
        $obj = $this->getById($prmData["id"]);
        return $obj->id;
    }


    function GetUsers_withoutcard(){
        $qryUtilisateur = $this->db->query("
            SELECT
                    *
            FROM
                    users
            WHERE
                    user_ionauth_id <> '0'
            AND
            IdUser not in (
                SELECT
                    id_user
                FROM
                    card
            )
            ORDER BY
            users.Nom
        ");
        if($qryUtilisateur->num_rows() > 0) {
            return $qryUtilisateur->result();
        }
    }

    function GetUsers_withoutcard_edit($cardId){
        $query = "
            SELECT
                    *
            FROM
                    users
            WHERE
                    user_ionauth_id <> '0'
            AND
            IdUser not in (
                SELECT
                    id_user
                FROM
                    card
                WHERE
                    id<>'".$cardId."'
            )
            ORDER BY
            users.Nom
        ";
        //echo $query;
        $qryUtilisateur = $this->db->query($query);
        if($qryUtilisateur->num_rows() > 0) {
            return $qryUtilisateur->result();
        }
    }


    function num_id_card_physical_check ($card_num, $card_id){
        $query = "
            SELECT
                    *
            FROM
                    card
            WHERE
                0=0
            AND
                num_id_card_physical='".$card_num."'";

        if (isset($card_id) && $card_id!="0" && $card_id!="")
            $query .= " AND
                id<>'".$card_id."'";

        $qryUtilisateur = $this->db->query($query);
        if($qryUtilisateur->num_rows() > 0) {
            return $qryUtilisateur->result();
        }
    }

    function num_id_card_virtual_check ($card_num, $card_id){
        $query = "
            SELECT
                    *
            FROM
                    card
            WHERE
                0=0
            AND
                num_id_card_virtual='".$card_num."'";

        if (isset($card_id) && $card_id!="0" && $card_id!="")
            $query .= " AND
                id<>'".$card_id."'";

        $qryUtilisateur = $this->db->query($query);
        if($qryUtilisateur->num_rows() > 0) {
            return $qryUtilisateur->result();
        }
    }

    function check_num_card($num_card){
        $Sql = "
            SELECT
                    *
            FROM
                    card
            WHERE
                   ( num_id_card_physical='".$num_card."' OR num_id_card_virtual='".$num_card."' )
            LIMIT 1
            ";
        $Query = $this->db->query($Sql);
        return $Query->row();

    }


    function check_scan_card($num_card){
        $Sql = "
            SELECT
                    *
            FROM
                    card
            WHERE
                   ( num_id_card_physical='".$num_card."' OR num_id_card_virtual='".$num_card."' )
            LIMIT 1
            ";
        $Query = $this->db->query($Sql);
        return $Query->row();

    }


    function check_num_card_linked($card_id){
        $Sql = "
            SELECT
                    *
            FROM
                    card_user_link
            WHERE
                   id_card='".$card_id."'
            LIMIT 1
            ";
        $Query = $this->db->query($Sql);
        return $Query->row();

    }

	function  get_reservation_by_user_id($user_id,$valid=1){
		$this->db->select("commercants.*,bonplan.*,assoc_client_bonplan.*");
	   	$this->db->from("assoc_client_bonplan");
	   	$this->db->join("bonplan","assoc_client_bonplan.id_bonplan=bonplan.bonplan_id");
	   	$this->db->join("commercants","bonplan.bonplan_commercant_id=commercants.IdCommercant");
	   	$this->db->where('assoc_client_bonplan.valide',$valid);
	   	$this->db->where('assoc_client_bonplan.id_client',$user_id);
		$query = $this->db->get();
		return ($query->num_rows > 0) ? $query->result() : array();
	}

}