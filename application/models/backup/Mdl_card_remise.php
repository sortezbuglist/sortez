<?php
class mdl_card_remise extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    function getById($id=0){
        $Sql = "select * from card_remise where id =". $id ;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByIdIonauth($id_ionauth=0){
        $Sql = "select * from card_remise where id_ionauth ='". $id_ionauth ."' LIMIT 1;";
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByIdCommercant($id_comm=0){
        $Sql = "select * from card_remise where id_commercant =". $id_comm ." LIMIT 1;";
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByIdRubrique($IdRubrique=0){
        $Sql = "
            SELECT
                card_remise.id,
                ass_commercants_rubriques.IdCommercant,
                ass_commercants_rubriques.IdRubrique
                FROM
                card_remise
                INNER JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = card_remise.id_commercant
                INNER JOIN commercants ON commercants.IdCommercant = card_remise.id_commercant
                WHERE
                card_remise.is_activ = 1 AND
                commercants.IsActif = 1 AND
                ass_commercants_rubriques.IdRubrique = '". $IdRubrique ."'
        ";
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByIdSousRubrique($IdSousRubrique=0){
        $Sql = "
            SELECT
                card_remise.id,
                ass_commercants_sousrubriques.IdCommercant,
                ass_commercants_sousrubriques.IdSousRubrique
                FROM
                card_remise
                INNER JOIN ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = card_remise.id_commercant
                WHERE
                card_remise.is_activ = 1 AND
                ass_commercants_sousrubriques.IdSousRubrique = '". $IdSousRubrique ."'
        ";
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function GetAll(){
        $qry = $this->db->query("
            SELECT * 
            FROM
                card_remise
            ORDER BY id DESC
        ");
        if($qry->num_rows() > 0) {
            return $qry->result();
        }
    }

    function GetWhere($where='0=0'){
        $Sql = "
            SELECT * 
            FROM
                card_remise 
            ".$where." 
            ORDER BY id DESC
        ";
        $qry = $this->db->query($Sql);
        if($qry->num_rows() > 0) {
            return $qry->result();
        }
    }


    function delete($prmId){

        $qry = $this->db->query("DELETE FROM card_remise WHERE id = ?", $prmId) ;
        return $qry ;
    }

    function insert($prmData) {
        $this->db->insert("card_remise", $prmData);
        return $this->db->insert_id();
    }

    function update($prmData) {
        $this->db->where("id", $prmData["id"]);
        $this->db->update("card_remise", $prmData);
        $obj = $this->getById($prmData["id"]);
        return $obj->id;
    }
    
    function is_remise_active($id_commercant){
    	$this->db->select('is_activ');
    	$query = $this->db->get_where('card_remise', array('id_commercant'=>$id_commercant));
    	 
    	return $query->first_row();
    }

    function id_remise_active($id_commercant){
        $this->db->select('id,description');
        $query = $this->db->get_where('card_remise', array('id_commercant'=>$id_commercant, 'is_activ'=>'1'));
         
        return $query->first_row();
    }
    
}