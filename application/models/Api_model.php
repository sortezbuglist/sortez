<?php



class Api_model extends CI_Model
{

    private $_table = 'assoc_client_bonplan';

    private $_users = 'users';

    private $_bonplan = 'bonplan';

    private $_commercants = 'commercants';


    public function get_login_by_id($login)
    {

        $this->db->select('commercants.* , villes.*');

        $this->db->from('commercants');

        $this->db->join('villes', 'villes.IdVille = commercants.IdVille');

        $this->db->where("commercants.Login", $login);

        $query = $this->db->get();

        return $query->row();

    }

    public function get_login_by_id2($login)
    {

        $this->db->select('commercants.* , villes.*');

        $this->db->from('commercants');

        $this->db->join('villes', 'villes.IdVille = commercants.IdVille');

        $this->db->where("commercants.Login", $login);

        $query = $this->db->get();

        return $query->row();

    }

    public function get_id_commercant($login)
    {

        $this->db->select('IdCommercant');

        $this->db->where("Login", $login);

        $query = $this->db->get('commercants', "IdCommercant");

        return $query->row();


    }

    public function getall($data)
    {

        $this->db->where("id_commercant", $data->IdCommercant);

        $qry = $this->db->get("assoc_client_commercant", "id_client");

        return $qry->result();

    }

    public function get_client_list($idcomm)
    {

        $this->db->where("id_commercant", $idcomm);

        $query = $this->db->get('assoc_client_commercant');

        return $query->result();

    }

    public function get_name_client($idclient)
    {

        $this->db->where("IdUser", $idclient);

        $query = $this->db->get('users');

        return $query->result_array();

    }

    public function get_name($data)
    {

        $this->db->where("IdUser", $data->id_client);

        $query = $this->db->get('users');

        return $query->result_array();

    }

    public function get_detail_client($id_client, $id_commercant)
    {


        $this->db->where("id_user", $id_client);

        $this->db->where("id_commercant", $id_commercant);

        $query = $this->db->get('card_fiche_client_tampon');

        return $query->result_array();

    }


    public function get_list_by_criteria($criteres, $order_by = array())
    {

        $this->db->select(

            'assoc_client_bonplan.id,

			users.IdUser,

			users.Nom,

			users.Prenom,

			users.Email,

			users.Telephone,

			users.Portable,

			assoc_client_bonplan.date_validation,

			bonplan.bonplan_commercant_id,

			bonplan.bonplan_id,

			bonplan.bonplan_texte,

			bonplan.bonplan_titre,

			bonplan.bonplan_date_fin,

			villes.Nom as ville_client,

			users.Email as user_email,

			users.Portable as user_tel,

			commercants.NomSociete,

			commercants.nom_url,

			assoc_client_bonplan.valide,

			assoc_client_bonplan.valide as bonplan_valide

			');

        $this->db->from($this->_table);

        $this->db->join($this->_users, $this->_table . '.id_client = ' . $this->_users . '.IdUser');

        $this->db->join($this->_bonplan, $this->_table . '.id_bonplan = ' . $this->_bonplan . '.bonplan_id');

        $this->db->join('villes', 'villes.idVille = users.idVille');

        $this->db->join($this->_commercants, $this->_commercants . '.IdCommercant = ' . $this->_bonplan . '.bonplan_commercant_id');


        if (array_key_exists('valide', $criteres)) {

            $this->db->where($this->_table . '.valide', $criteres['valide']);

            $this->db->where('assoc_client_bonplan.date_validation !=', '0000-00-00');

        }

        if (array_key_exists('id_commercant', $criteres)) {

            $this->db->where($this->_bonplan . '.bonplan_commercant_id ', $criteres['id_commercant']);

        }

        if (array_key_exists('id_user', $criteres)) {

            $this->db->where($this->_users . '.IdUser ', $criteres['id_user']);

        }

        if (array_key_exists('id', $criteres)) {

            $this->db->where($this->_table . '.id ', $criteres['id']);

        }


        if (!empty($order_by) && (current($order_by) == "desc" || current($order_by) == "asc")) {

            $this->db->order_by(key($order_by), current($order_by));

        }


        $query = $this->db->get();

        if (array_key_exists('num_rows', $criteres) && $criteres['num_rows']) {

            return $query->num_rows();

        }


        if (array_key_exists('query', $criteres) && $criteres['query']) {

            return $query;

        }


        return ($query->num_rows() > 0) ? $query->result_array() : false;

    }

    public function get_list_by_criteria2($criteres, $order_by = array())
    {

        $this->db->select(

            'assoc_client_bonplan.id,

			users.IdUser,

			users.Nom,

			users.Prenom,

			users.Email,

			users.Telephone,

			users.Portable,

			assoc_client_bonplan.date_validation,

			bonplan.*,

			villes.Nom as ville_client,

			users.Email as user_email,

			users.Portable as user_tel,

			commercants.NomSociete,

			commercants.nom_url,

			assoc_client_bonplan.valide,

			assoc_client_bonplan.date_visit,

			assoc_client_bonplan.valide as bonplan_valide

			');

        $this->db->from($this->_table);

        $this->db->join($this->_users, $this->_table . '.id_client = ' . $this->_users . '.IdUser');

        $this->db->join($this->_bonplan, $this->_table . '.id_bonplan = ' . $this->_bonplan . '.bonplan_id');

        $this->db->join('villes', 'villes.idVille = users.idVille');

        $this->db->join($this->_commercants, $this->_commercants . '.IdCommercant = ' . $this->_bonplan . '.bonplan_commercant_id');


        if (array_key_exists('valide', $criteres)) {

            $this->db->where($this->_table . '.valide', $criteres['valide']);

            $this->db->where('assoc_client_bonplan.date_validation !=', '0000-00-00');

        }

        if (array_key_exists('id_commercant', $criteres)) {

            $this->db->where($this->_bonplan . '.bonplan_commercant_id ', $criteres['id_commercant']);

        }

        if (array_key_exists('id_user', $criteres)) {

            $this->db->where($this->_users . '.IdUser ', $criteres['id_user']);

        }

        if (array_key_exists('id', $criteres)) {

            $this->db->where($this->_table . '.id ', $criteres['id']);

        }


        if (!empty($order_by) && (current($order_by) == "desc" || current($order_by) == "asc")) {

            $this->db->order_by(key($order_by), current($order_by));

        }


        $query = $this->db->get();

        if (array_key_exists('num_rows', $criteres) && $criteres['num_rows']) {

            return $query->result();

        }


        if (array_key_exists('query', $criteres) && $criteres['query']) {

            return $query;

        }


        return ($query->num_rows() > 0) ? $query->result() : false;

    }

    function insertBonplan($prmData)
    {

        if ($prmData['bonplan_id'] == '') $prmData['bonplan_id'] = null;

        if ($prmData['bonplan_date_debut'] == '' || $prmData['bonplan_date_debut'] == '--') $prmData['bonplan_date_debut'] = null;

        if ($prmData['bonplan_date_fin'] == '' || $prmData['bonplan_date_fin'] == '--') $prmData['bonplan_date_fin'] = null;

        if ($prmData['bp_unique_date_debut'] == '' || $prmData['bp_unique_date_debut'] == '--') $prmData['bp_unique_date_debut'] = null;

        if ($prmData['bp_unique_date_fin'] == '' || $prmData['bp_unique_date_fin'] == '--') $prmData['bp_unique_date_fin'] = null;


        if (isset($prmData['bp_unique_date_visit']) AND isset($prmData['bp_multiple_date_visit']) AND isset($prmData['activ_bp_reserver_paypal'])) {

            if ($prmData['activ_bp_reserver_paypal'] == '') $prmData['activ_bp_reserver_paypal'] = 0;

            if ($prmData['bp_multiple_date_visit'] == '' || $prmData['bp_multiple_date_visit'] == '--') $prmData['bp_multiple_date_visit'] = null;

            if ($prmData['bp_unique_date_visit'] == '' || $prmData['bp_unique_date_visit'] == '--') $prmData['bp_unique_date_visit'] = null;


        }

        if ($prmData['bp_multiple_date_debut'] == '' || $prmData['bp_multiple_date_debut'] == '--') $prmData['bp_multiple_date_debut'] = null;

        if ($prmData['bp_multiple_date_fin'] == '' || $prmData['bp_multiple_date_fin'] == '--') $prmData['bp_multiple_date_fin'] = null;

        if ($prmData['activ_bp_unique_prix'] == '') $prmData['activ_bp_unique_prix'] = 0;

        if ($prmData['activ_bp_unique_qttprop'] == '') $prmData['activ_bp_unique_qttprop'] = 0;

        if ($prmData['activ_bp_unique_nbmax'] == '') $prmData['activ_bp_unique_nbmax'] = 0;

        if ($prmData['activ_bp_unique_date_visit'] == '') $prmData['activ_bp_unique_date_visit'] = 0;

        if ($prmData['activ_bp_unique_reserv_regler'] == '') $prmData['activ_bp_unique_reserv_regler'] = 0;

        if ($prmData['activ_bp_multiple_prix'] == '') $prmData['activ_bp_multiple_prix'] = 0;

        if ($prmData['activ_bp_multiple_qttprop'] == '') $prmData['activ_bp_multiple_qttprop'] = 0;

        if ($prmData['activ_bp_multiple_nbmax'] == '') $prmData['activ_bp_multiple_nbmax'] = 0;

        if ($prmData['activ_bp_multiple_date_visit'] == '') $prmData['activ_bp_multiple_date_visit'] = 0;

        if ($prmData['activ_bp_multiple_reserv_regler'] == '') $prmData['activ_bp_multiple_reserv_regler'] = 0;


        $this->db->insert("bonplan", $prmData);

        return $this->db->insert_id();

    }

    function modifBonplan($prmData)
    {

        if ($prmData['bonplan_id'] == '') $prmData['bonplan_id'] = null;

        if ($prmData['bonplan_date_debut'] == '' || $prmData['bonplan_date_debut'] == '--') $prmData['bonplan_date_debut'] = null;

        if ($prmData['bonplan_date_fin'] == '' || $prmData['bonplan_date_fin'] == '--') $prmData['bonplan_date_fin'] = null;

        if ($prmData['bp_unique_date_debut'] == '' || $prmData['bp_unique_date_debut'] == '--') $prmData['bp_unique_date_debut'] = null;

        if ($prmData['bp_unique_date_fin'] == '' || $prmData['bp_unique_date_fin'] == '--') $prmData['bp_unique_date_fin'] = null;


        if (isset($prmData['bp_unique_date_visit']) AND isset($prmData['bp_multiple_date_visit']) AND isset($prmData['activ_bp_reserver_paypal'])) {

            if ($prmData['activ_bp_reserver_paypal'] == '') $prmData['activ_bp_reserver_paypal'] = 0;

            if ($prmData['bp_multiple_date_visit'] == '' || $prmData['bp_multiple_date_visit'] == '--') $prmData['bp_multiple_date_visit'] = null;

            if ($prmData['bp_unique_date_visit'] == '' || $prmData['bp_unique_date_visit'] == '--') $prmData['bp_unique_date_visit'] = null;


        }

        if ($prmData['bp_multiple_date_debut'] == '' || $prmData['bp_multiple_date_debut'] == '--') $prmData['bp_multiple_date_debut'] = null;

        if ($prmData['bp_multiple_date_fin'] == '' || $prmData['bp_multiple_date_fin'] == '--') $prmData['bp_multiple_date_fin'] = null;

        if ($prmData['activ_bp_unique_prix'] == '') $prmData['activ_bp_unique_prix'] = 0;

        if ($prmData['activ_bp_unique_qttprop'] == '') $prmData['activ_bp_unique_qttprop'] = 0;

        if ($prmData['activ_bp_unique_nbmax'] == '') $prmData['activ_bp_unique_nbmax'] = 0;

        if ($prmData['activ_bp_unique_date_visit'] == '') $prmData['activ_bp_unique_date_visit'] = 0;

        if ($prmData['activ_bp_unique_reserv_regler'] == '') $prmData['activ_bp_unique_reserv_regler'] = 0;

        if ($prmData['activ_bp_multiple_prix'] == '') $prmData['activ_bp_multiple_prix'] = 0;

        if ($prmData['activ_bp_multiple_qttprop'] == '') $prmData['activ_bp_multiple_qttprop'] = 0;

        if ($prmData['activ_bp_multiple_nbmax'] == '') $prmData['activ_bp_multiple_nbmax'] = 0;

        if ($prmData['activ_bp_multiple_date_visit'] == '') $prmData['activ_bp_multiple_date_visit'] = 0;

        if ($prmData['activ_bp_multiple_reserv_regler'] == '') $prmData['activ_bp_multiple_reserv_regler'] = 0;

        $this->db->where('bonplan_id', $prmData['bonplan_id']);

        $this->db->update("bonplan", $prmData);


    }

    public function modifaccount($_POSTe)
    {

        $id_commercant = $_POSTe["IdCommercant"];

        $field = array("IdCommercant" => $id_commercant,

            "Nom" => $_POSTe["Nom"],

            "Prenom" => $_POSTe["Prenom"],

            "CodePostal" => $_POSTe["CodePostal"],

            "TelFixe" => $_POSTe["TelFixe"],

            "Civilite" => $_POSTe["Civilite"],

            "Email" => $_POSTe["Email"]);

        //print_r($id_commercant);die();

        $this->db->where("IdCommercant", $id_commercant);

        $request = $this->db->update("commercants", $field);

        if ($request) {

            return true;

        } else {

            return false;

        }

    }

    public function get_remisedirect($IdCommercant)
    {

        $this->db->where("id_commercant", $IdCommercant);

        $request = $this->db->get("card_remise");

        return $request->row();

    }

    public function getcoupdetampon($IdCommercant)
    {

        $this->db->where("id_commercant", $IdCommercant);

        $request = $this->db->get("card_tampon");

        return $request->row();

    }

    public function getcapital($IdCommercant)
    {

        $this->db->where("id_commercant", $IdCommercant);

        $request = $this->db->get("card_capital");

        return $request->row();

    }

    public function getbonplan($IdCommercant)
    {

        $this->db->where("bonplan_commercant_id", $IdCommercant);

        $request = $this->db->get("bonplan");

        return $request->result_array();

    }

    public function get_ion_auth_by_commercant($id_commercant)
    {

        //$this->db->select("user_ionauth_id");

        $this->db->where("IdCommercant", $id_commercant);

        $request = $this->db->get("commercants");

        return $request->row();

    }

    public function getbonplannbre($IdCommercant)
    {

        $this->db->where("IdCommercant", $IdCommercant);

        $request = $this->db->get("commercants");

        return $request->row();

    }

    public function getnbrebonplan($IdCommercant)
    {

        $this->db->where("bonplan_commercant_id", $IdCommercant);

        $request = $this->db->get("bonplan");

        return $request->num_rows();

    }

    public function deletebonplan($IdCommercant)
    {

        $this->db->where("bonplan_titre", $IdCommercant);

        $request = $this->db->delete("bonplan");

        return $request;

    }

    function getById($id)
    {

        $this->db->where("id", $id);

        $request = $this->db->get("assoc_client_bonplan");

        return $request->row();

    }

    public function get_list_by_criteria3($criteres, $order_by = array())
    {

        $this->db->select(

            'assoc_client_bonplan.id,

			users.IdUser,

			users.Nom,

			users.Prenom,

			users.Email,

			users.Telephone,

			users.Portable,

			assoc_client_bonplan.date_validation,

			bonplan.*,

			villes.Nom as ville_client,

			users.Email as user_email,

			users.Portable as user_tel,

			commercants.NomSociete,

			commercants.nom_url,

			assoc_client_bonplan.valide,

			assoc_client_bonplan.valide as bonplan_valide

			');

        $this->db->from($this->_table);

        $this->db->join($this->_users, $this->_table . '.id_client = ' . $this->_users . '.IdUser');

        $this->db->join($this->_bonplan, $this->_table . '.id_bonplan = ' . $this->_bonplan . '.bonplan_id');

        $this->db->join('villes', 'villes.idVille = users.idVille');

        $this->db->join($this->_commercants, $this->_commercants . '.IdCommercant = ' . $this->_bonplan . '.bonplan_commercant_id');


        if (array_key_exists('valide', $criteres)) {

            $this->db->where($this->_table . '.valide', $criteres['valide']);

            $this->db->where('assoc_client_bonplan.date_validation !=', '0000-00-00');

        }

        if (array_key_exists('id_commercant', $criteres)) {

            $this->db->where($this->_bonplan . '.bonplan_commercant_id ', $criteres['id_commercant']);

        }

        if (array_key_exists('id_user', $criteres)) {

            $this->db->where($this->_users . '.IdUser ', $criteres['id_user']);

        }

        if (array_key_exists('id', $criteres)) {

            $this->db->where($this->_table . '.id ', $criteres['id']);

        }


        if (!empty($order_by) && (current($order_by) == "desc" || current($order_by) == "asc")) {

            $this->db->order_by(key($order_by), current($order_by));

        }


        $query = $this->db->get();

        if (array_key_exists('num_rows', $criteres) && $criteres['num_rows']) {

            return $query->num_rows();

        }


        if (array_key_exists('query', $criteres) && $criteres['query']) {

            return $query;

        }


        return ($query->num_rows() > 0) ? $query->result() : false;

    }

    public function get_namecommercant($login)
    {

        $this->db->where('login', $login);

        $request = $this->db->get("commercants");

        return $request->row();

    }

    public function getclientbyidcommercant($login)
    {

        $this->db->where('login', $login);

        $request = $this->db->get("users");

        return $request->result();

    }

    public function get_plat_list_by_login($idcom, $idclient = '0')
    {


        if ($idcom != null) {

            $this->db->select("reservation_plat.*,reservation_plat.id as idRes,users.*,card.num_id_card_virtual,plat_gli.*,commercants.login,commercants.login as logincom,plat_gli_details.*");

            $this->db->from('plat_gli');

            $this->db->join('reservation_plat', 'reservation_plat.IdPlat = plat_gli.id_plat');

            $this->db->join('plat_gli_details', 'plat_gli_details.id_plat_gli = plat_gli.id_plat');

            $this->db->join('card', 'card.num_id_card_virtual = reservation_plat.num_carte');

            $this->db->join('users', 'users.IdUser = card.id_user');

            $this->db->join('commercants', 'commercants.IdCommercant = plat_gli.IdCommercant');

            $this->db->where('plat_gli.IdCommercant', $idcom);

            $this->db->where('plat_gli_details.date1 >', date('Y-m-d'));

            $this->db->where('reservation_plat.etat', '0');

            if (isset($idclient) AND $idclient != '0') {

                $this->db->where('users.IdUser', $idclient);

            }

            //WHERE sendDate <= getdate AND sendDate >= getdate()

            $res = $this->db->get();

            return $res->result();


        }

    }

    public function get_table_list_by_login($idcom)
    {

        $this->db->select('reservation_type_table.*,reservations.id as idRes,reservations.*,villes.Nom as nomVille');

        $this->db->from('reservations');

        $this->db->join('reservation_type_table', 'reservation_type_table.id=reservations.id_type_reservation');

        $this->db->join('villes', 'villes.IdVille=reservation_type_table.id_ville');

        $this->db->where('reservations.IdCommercant', $idcom);

        $this->db->where('reservations.etat_reservation', '1');

        $res = $this->db->get();

        return $res->result();


    }

    public function get_sejour_list_by_login($idcom)
    {

        $this->db->select('reservation_type_sejour.*,reservations.id as idRes,reservations.*,villes.Nom as nomVille');

        $this->db->from('reservations');

        $this->db->join('reservation_type_sejour', 'reservation_type_sejour.id=reservations.id_type_reservation');

        $this->db->join('villes', 'villes.IdVille=reservation_type_sejour.id_ville');

        $this->db->where('reservations.IdCommercant', $idcom);

        $this->db->where('reservations.etat_reservation', '1');

        $res = $this->db->get();

        return $res->result();

    }

    public function get_res_table_by_id($idres)
    {

        $this->db->select('reservation_type_table.*,reservations.id as idRes,reservations.*');

        $this->db->from('reservations');

        $this->db->join('reservation_type_table', 'reservation_type_table.id=reservations.id_type_reservation');

        $this->db->where('reservations.id', $idres);

        $this->db->where('reservations.etat_reservation', '1');

        $res = $this->db->get();

        return $res->result();

    }

    public function get_res_sejour_by_id($idres)
    {

        $this->db->select('reservation_type_sejour.*,reservations.id as idRes,reservations.*');

        $this->db->from('reservations');

        $this->db->join('reservation_type_sejour', 'reservation_type_sejour.id=reservations.id_type_reservation');

        $this->db->where('reservations.id', $idres);

        $this->db->where('reservations.etat_reservation', '1');

        $res = $this->db->get();

        return $res->result();

    }

    public function validate_table($idres)
    {

        $array = array("etat_reservation" => 2);

        $this->db->where('id', $idres);

        $this->db->update('reservations', $array);

    }

    public function validate_sejour($idres)
    {

        $array = array("etat_reservation" => 2);

        $this->db->where('id', $idres);

        $this->db->update('reservations', $array);

    }

    public function get_detail_plat_by_idres($idres)
    {

        $this->db->select('reservation_plat.*,reservation_plat.id as idRes,plat_gli.*,plat_gli.id_plat as idplat*,card.*,users.*,plat_gli_details.*');

        $this->db->from('reservation_plat');

        $this->db->join('plat_gli', 'reservation_plat.IdPlat=plat_gli.id_plat');

        $this->db->join('plat_gli_details', 'plat_gli_details.id_plat_gli=plat_gli.id_plat');

        $this->db->join('card', 'reservation_plat.num_carte=card.num_id_card_virtual');

        $this->db->join('users', 'card.id_user=users.IdUser');

        $this->db->where('reservation_plat.id', $idres);

        $this->db->where('reservation_plat.etat', '0');

        $res = $this->db->get();

        return $res->result();

    }

    public function valid_res_plat($id)
    {

        $field = array('etat' => '1',

            'date_validation' => date('Y-m-d'));

        $this->db->where('id', $id);

        $this->db->update('reservation_plat', $field);

    }

    function Get_users_ById($prmId = 0, $id_commercant = 0)

    {

        $this->db->select('users.*,commercants.*,commercants.login as logincom,,assoc_client_commercant.*,users.Nom as nomClient,users.Prenom as prenomClient,villes.*');

        $this->db->from('users');

        $this->db->join('assoc_client_commercant', 'assoc_client_commercant.id_client=users.IdUser');

        $this->db->join('commercants', 'commercants.IdCommercant=assoc_client_commercant.id_commercant');

        $this->db->join('villes', 'villes.IdVille=users.IdVille');

        $this->db->where('users.IdUser', $prmId);

        $this->db->where('commercants.IdCommercant', $id_commercant);

        $res = $this->db->get();

        return $res->row();


    }

    public function get_list_bonplan_by_criteria($idcom, $iduser)
    {

        $this->db->select(

            'assoc_client_bonplan.*,

			users.*,

			bonplan.*,

			villes.Nom as ville_client,

			users.Email as user_email,

			users.Portable as user_tel,

			commercants.NomSociete,

			commercants.nom_url,

			commercants.IdCommercant,

			assoc_client_bonplan.valide as bonplan_valide

			');

        $this->db->from('assoc_client_bonplan');

        $this->db->join('users', 'assoc_client_bonplan.id_client=users.IdUser');

        $this->db->join('bonplan', 'assoc_client_bonplan.id_bonplan = bonplan.bonplan_id');

        $this->db->join('villes', 'villes.idVille = users.idVille');

        $this->db->join('commercants', 'commercants.IdCommercant = bonplan.bonplan_commercant_id');

        $this->db->where('users.IdUser', $iduser);

        $this->db->where('assoc_client_bonplan.id_client', $iduser);

        $this->db->where('commercants.IdCommercant', $idcom);

        $this->db->where('assoc_client_bonplan.valide', '1');

        $query = $this->db->get();


        return $query->row();

    }

    public function get_historic_plat_by_criteria($idcom, $iduser)
    {

        $this->db->select('reservation_plat.*,reservation_plat.id as idRes,plat_gli.*,plat_gli.id_plat as idplat*,card.*,users.*,commercants.*,plat_gli_details.*');

        $this->db->from('reservation_plat');

        $this->db->join('plat_gli', 'reservation_plat.IdPlat=plat_gli.id_plat');

        $this->db->join('plat_gli_details', 'plat_gli_details.id_plat_gli=plat_gli.id_plat');

        $this->db->join('card', 'reservation_plat.num_carte=card.num_id_card_virtual');

        $this->db->join('commercants', 'reservation_plat.IdCommercant=commercants.IdCommercant');

        $this->db->join('users', 'card.id_user=users.IdUser');

        $this->db->where('users.IdUser', $iduser);

        $this->db->where('reservation_plat.IdCommercant', $idcom);

        $this->db->where('reservation_plat.etat', '1');

        //$this->db->where('card.num_id_card_virtual','reservation_plat.num_carte');

        $res = $this->db->get();

        return $res->result();

    }


    public function get_client_fidelity_bonplan_by_commercant_id($id_commercant)
    {

        $this->db->select('villes.Nom as Ville,assoc_client_commercant.*,users.*');

        $this->db->from('assoc_client_commercant');

        $this->db->join('users', 'assoc_client_commercant.id_client = users.IdUser');

        $this->db->join('villes', 'villes.idVille = users.idVille');

        $where['assoc_client_commercant.id_commercant'] = $id_commercant;

        $this->db->where($where);

        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->result() : false;

    }

    public function get_plat_list_by_login_date_demande($idcom, $idclient = '0')
    {

        if ($idcom != null) {

            $this->db->select("reservation_plat.*,reservation_plat.id as idRes,users.*,card.num_id_card_virtual,plat_gli.*,commercants.login,commercants.login as logincom,plat_gli_details.*");

            $this->db->from('plat_gli');

            $this->db->join('reservation_plat', 'reservation_plat.IdPlat = plat_gli.id_plat');

            $this->db->join('plat_gli_details', 'plat_gli_details.id_plat_gli = plat_gli.id_plat');

            $this->db->join('card', 'card.num_id_card_virtual = reservation_plat.num_carte');

            $this->db->join('users', 'users.IdUser = card.id_user');

            $this->db->join('commercants', 'commercants.IdCommercant = plat_gli.IdCommercant');

            $this->db->where('plat_gli.IdCommercant', $idcom);

            $this->db->where('plat_gli_details.date1 >', date('Y-m-d'));

            $this->db->where('reservation_plat.etat', '0');

            $this->db->order_by("reservation_plat.date_reservation", "desc");

            if (isset($idclient) AND $idclient != '0') {

                $this->db->where('users.IdUser', $idclient);

            }

            //WHERE sendDate <= getdate AND sendDate >= getdate()

            $res = $this->db->get();

            return $res->result();

        }

    }

    public function get_table_list_by_login_date_demande($idcom)
    {

        $this->db->select('reservation_type_table.*,reservations.id as idRes,reservations.*,villes.Nom as nomVille');

        $this->db->from('reservations');

        $this->db->join('reservation_type_table', 'reservation_type_table.id=reservations.id_type_reservation');

        $this->db->join('villes', 'villes.IdVille=reservation_type_table.id_ville');

        $this->db->where('reservations.IdCommercant', $idcom);

        $this->db->where('reservations.etat_reservation', '1');

        $this->db->order_by("reservation_type_table.date_de_reservation", "desc");

        $res = $this->db->get();

        return $res->result();


    }

    public function get_table_list_by_login_date_debut($idcom)
    {

        $this->db->select('reservation_type_table.*,reservations.id as idRes,reservations.*,villes.Nom as nomVille');

        $this->db->from('reservations');

        $this->db->join('reservation_type_table', 'reservation_type_table.id=reservations.id_type_reservation');

        $this->db->join('villes', 'villes.IdVille=reservation_type_table.id_ville');

        $this->db->where('reservations.IdCommercant', $idcom);

        $this->db->where('reservations.etat_reservation', '1');

        $this->db->order_by("reservation_type_table.date_res", "desc");

        $res = $this->db->get();

        return $res->result();


    }

    public function get_sejour_list_by_login_date_demande($idcom)
    {

        $this->db->select('reservation_type_sejour.*,reservations.id as idRes,reservations.*,villes.Nom as nomVille');

        $this->db->from('reservations');

        $this->db->join('reservation_type_sejour', 'reservation_type_sejour.id=reservations.id_type_reservation');

        $this->db->join('villes', 'villes.IdVille=reservation_type_sejour.id_ville');

        $this->db->where('reservations.IdCommercant', $idcom);

        $this->db->where('reservations.etat_reservation', '1');

        $this->db->order_by("reservation_type_sejour.date_de_reservation", "desc");

        $res = $this->db->get();

        return $res->result();

    }

    public function get_sejour_list_by_login_date_debut($idcom)
    {

        $this->db->select('reservation_type_sejour.*,reservations.id as idRes,reservations.*,villes.Nom as nomVille');

        $this->db->from('reservations');

        $this->db->join('reservation_type_sejour', 'reservation_type_sejour.id=reservations.id_type_reservation');

        $this->db->join('villes', 'villes.IdVille=reservation_type_sejour.id_ville');

        $this->db->where('reservations.IdCommercant', $idcom);

        $this->db->where('reservations.etat_reservation', '1');

        $this->db->order_by("reservation_type_sejour.date_debut_res", "desc");

        $res = $this->db->get();

        return $res->result();

    }

    function infoCommercant($_iCommercantId)
    {

        $Sql = " SELECT

                        commercants.*,

                        rubriques.IdRubrique,

                        rubriques.Nom AS Rubrique,

                        commercants.activite1,

                        commercants.activite2,

                        commercants.labelactivite1,

                        commercants.labelactivite2,

                        sous_rubriques.Nom AS sousrubrique,

                        villes.Nom AS ville

                        FROM

                        commercants

                        Left Outer Join ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant

                        Left Outer Join rubriques ON ass_commercants_rubriques.IdRubrique = rubriques.IdRubrique

                        Left Outer Join ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant

                        Left Outer Join sous_rubriques ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique

                        Left Outer Join villes ON villes.IdVille = commercants.IdVille";


        if (isset($_iCommercantId) && $_iCommercantId != 0 && $_iCommercantId != "" && $_iCommercantId != NULL && $_iCommercantId != "0") $Sql .= "  where commercants.IdCommercant = " . $_iCommercantId;


        //show_error($Sql );


        $Query = $this->db->query($Sql);

        $toInfoCommercant = $Query->result();

        if ($Query->num_rows() > 0) {

            return $toInfoCommercant[0];

        } else {

            show_error("Ce commerçant est inconnu : " . $_iCommercantId . " (Erreur 012)");

            return -1;

        }

    }


    function infoCommercant2($_iCommercantId)
    {

        $this->db->where('IdCommercant', $_iCommercantId);

        $req = $this->db->get('commercants');

        return $req->row();

    }


    public function save_name_com_photo($idcom, $array)
    {

        $this->db->where('IdCommercant', $idcom);

        $this->db->update('commercants', $array);

    }


    public function liste_plat($idcom)
    {

        $this->db->where('IdCommercant', $idcom);

        $result = $this->db->get('plat_gli');

        return $result->result();

    }


    public function get_all_bonplan_by_idcom($idcom)
    {
        $this->db->select('commercants.user_ionauth_id , bonplan.*');
        $this->db->from('bonplan');
        $this->db->join('commercants', 'commercants.IdCommercant=bonplan.bonplan_commercant_id');
        $this->db->where('bonplan_commercant_id', $idcom);
        $res = $this->db->get();
        return $res->result();

    }


    public function delete_bonplan_by_id($id)
    {

        $this->db->where('bonplan_id', $id);

        $this->db->delete('bonplan');

    }

    public function delete_plat_by_id($id)
    {

        $this->db->where('id', $id);

        $this->db->delete('plat_gli');

    }


    function insert_plat($prmData)
    {

        $field = array("IdCommercant" => $prmData["idcommercant"],
            'IdUsers_ionauth' => $prmData["IdUsers_ionauth"],

            "date_debut_plat" => $prmData["date_debut"],

            "date_fin_plat" => $prmData["date_fin"],

            "heure_debut_reservation" => $prmData["heure_debut"],

            "heure_fin_reservation" => $prmData["heure_fin"],

            "description_plat" => $prmData["txt_plat"],

            "prix_plat" => $prmData["prix_plat"],

            "nbre_plat_propose"=>$prmData["nbr_plat"],
            
            "IsActif"=>$prmData["IsActif"]);

        //var_dump($field);
        
        $request = $this->db->insert("plat_gli", $field);

        if ($request) {

            return true;

        } else {

            return false;

        }

    }


    function update_by_idplat($prmData)
    {

        $this->db->where('id', $prmData);

        $result = $this->db->get('plat_du_jour');

        return $result->result();

    }


    function update_plat($prmData)
    {

        $id = $prmData["idplat"];

        $field = array("date_debut_plat" => $prmData["date_debut"],

            "date_fin_plat" => $prmData["date_fin"],

            "heure_debut_reservation" => $prmData["heure_debut"],

            "heure_fin_reservation" => $prmData["heure_fin"],

            "description_plat" => $prmData["txt_plat"],

            "prix_plat" => $prmData["prix_plat"],

            "nbre_plat_propose" => $prmData["nbr_plat"],
            
            "IsActif" => $prmData["IsActif"]);

        //var_dump($id);die("kkkk");

        $this->db->where("id", $id);

        $this->db->update("plat_du_jour", $field);

    }


    public function save_bonplan_image($idcom, $idbonplan, $field)
    {

        $this->db->where('bonplan_id', $idbonplan);

        $this->db->where('bonplan_commercant_id', $idcom);

        $this->db->update('bonplan', $field);

    }


    //save plat image

    public function save_plat_image($idcom, $id_plat, $field)
    {

        $this->db->where('id', $id_plat);

        $this->db->where('IdCommercant', $idcom);

        $this->db->update('plat_du_jour', $field);

    }


    public function delete_bonplan_image($idbonplan)
    {

        $this->db->where('bonplan_id', $idbonplan);

        $array = array(

            'bonplan_photo1' => null

        );

        $this->db->update('bonplan', $array);

    }

    public function get_bonplan_by_id($idbonplan)
    {

        $this->db->where('bonplan_id', $idbonplan);

        $res = $this->db->get('bonplan');

        return $res->result();

    }


    //delete plat image

    public function delete_plat_image($id_plat)
    {

        echo "idplat delete photo =";

        var_dump($id_plat);

        // $this->db->where('id',$id_plat);

        // $res=$this->db->get('plat_du_jour')->row();

        // //var_dump($res);die("requette del image");

        // $array=array(

        //     'photo'=>null

        // );

        // $this->db->update('plat_du_jour',$array);

        // return $res->photo;

        $req = "UPDATE `plat_du_jour` SET `photo`= null WHERE `id`=" . $id_plat;

        var_dump($req);

        $Resreq = $this->db->query($req);

        var_dump($Resreq);

    }

    public function delete_bp_image($idbonplan)
    {

        echo "idplat delete photo =";

        var_dump($idbonplan);

        // $this->db->where('id',$id_plat);

        // $res=$this->db->get('plat_du_jour')->row();

        // //var_dump($res);die("requette del image");

        // $array=array(

        //     'photo'=>null

        // );

        // $this->db->update('plat_du_jour',$array);

        // return $res->photo;

        $req = "UPDATE `bonplan` SET `bonplan_photo1`= null WHERE `bonplan_id`=" . $idbonplan;

        var_dump($req);

        $Resreq = $this->db->query($req);

        var_dump($Resreq);

    }

    //get by id nom photo

    public function get_nomPhoto_by_id($id_plat)
    {

        $this->db->where('id', $id_plat);

        $res = $this->db->get('plat_du_jour');

        return $res->row();

    }

    public function get_nomPhotobp_by_id($idbonplan)
    {

        $this->db->where('bonplan_id', $idbonplan);

        $res = $this->db->get('bonplan');

        return $res->row();

    }

    public function get_bonplan_photo($id)
    {

        $this->db->where('bonplan_id', $id);

        $res = $this->db->get('bonplan');

        return $res->row();

    }


    public function get_list_agendaMobile($idville)
    {

        // $req ="UPDATE `bonplan` SET `bonplan_photo1`= null WHERE `bonplan_id`=".$idbonplan;

        $req = "SELECT
            agenda.IdVille,            
            agenda.id,            
            agenda.photo1,            
            agenda.adresse_localisation,            
            agenda.description,            
            agenda.telephone,            
            agenda.nom_manifestation,            
            agenda.nom_societe,            
            agenda.date_debut,            
            agenda.date_fin,
            villes.IdVille,
            villes.ville_nom,
            villes.ville_latitude_deg,
            villes.ville_longitude_deg,
            villes.CodePostal           

            FROM  agenda

            INNER JOIN villes ON agenda.IdVille = villes.IdVille

            WHERE agenda.codepostal=" . $idville . " LIMIT 3";


        $Resreq = $this->db->query($req);


        return $Resreq->result();

    }

    public function get_agenda_by_id($id)
    {

        // $req ="UPDATE `bonplan` SET `bonplan_photo1`= null WHERE `bonplan_id`=".$idbonplan;

        $this->db->where('id', $id);

        $res = $this->db->get('agenda');

        return $res->row();

    }
    public function get_list_annuaireMobile($idville){
        $this->db->select('commercants.Idcommercant, commercants.CodePostal , commercants.adresse_localisation , commercants.user_ionauth_id , commercants.NomSociete , commercants.photo1 , commercants.Caracteristiques , rubriques.Nom as rubriques , villes.ville_nom');
        $this->db->from('commercants');
        $this->db->join('villes', 'commercants.IdVille = villes.IdVille');
        $this->db->join('ass_commercants_rubriques', 'commercants.IdCommercant = ass_commercants_rubriques.IdCommercant');
        $this->db->join('rubriques', 'ass_commercants_rubriques.IdRubrique = rubriques.IdRubrique');
        $this->db->where('villes.CodePostal', $idville);
        $this->db->where('commercants.IsActif = 1');
        $this->db->where('commercants.referencement_annuaire = 1');
        $res = $this->db->get();
        return $res->result();
    }

    public function get_annonce_by_codepostal($codepostal)
    {

        $this->db->select('annonce.annonce_photo1 AS photo,
                    annonce.annonce_description_courte AS texte_courte,
                    annonce.annonce_prix_neuf AS prix_vente,
                    annonce.annonce_prix_solde AS prix_ancien,
                    annonce.annonce_etat AS etat,
                    commercants.NomSociete AS NomSociete,
                    villes.Nom AS ville,
                    commercants.Adresse1 AS quartier,
                    commercants.Adresse2 AS rue,
                    commercants.IdCommercant,
                    commercants.nom_url,
                    sous_rubriques.Nom AS rubrique,
                    sous_rubriques.IdSousRubrique,
                    sous_rubriques.IdRubrique,
                    annonce.annonce_id,
                    annonce.annonce_description_longue,
                    annonce.annonce_photo1,
                    annonce.annonce_photo2,
                    annonce.annonce_photo3,
                    annonce.annonce_photo4,
                    annonce.module_paypal,
                    annonce.retrait_etablissement, 
                    annonce.livraison_domicile,
                    annonce.annonce_commercant_id');
        $this->db->from('annonce');
        $this->db->join('commercants', 'commercants.IdCommercant = annonce.annonce_commercant_id');
        $this->db->join('villes', 'villes.IdVille = commercants.IdVille');
        $this->db->join('ass_commercants_sousrubriques', 'ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant');
        $this->db->join('sous_rubriques', 'sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique');
        $this->db->where("villes.CodePostal",$codepostal);
        $this->db->where("commercants.referencement_annonce", '1');
//        $this->db->where("commercants.annonce", '1');
        $this->db->where("commercants.IsActif", '1');
        $query = $this->db->get();
        return $query->result();

    }

    public function get_all_list_agendaMobile($codepostal)
    {
        $req = "SELECT
            agenda.IdVille,            
            agenda.id,            
            agenda.photo1,            
            agenda.adresse_localisation,            
            agenda.description,            
            agenda.telephone,            
            agenda.nom_manifestation,            
            agenda.date_debut,            
            agenda.date_fin,
            villes.IdVille,
            villes.ville_nom,
            villes.ville_latitude_deg,
            villes.ville_longitude_deg,
            villes.CodePostal
            FROM  agenda
            INNER JOIN villes ON agenda.IdVille = villes.IdVille
            WHERE agenda.codepostal=" . $codepostal;
        $Resreq = $this->db->query($req);
        return $Resreq->result();
    }

    public function get_annuaire_by_id($id)
    {

        // $req ="UPDATE `bonplan` SET `bonplan_photo1`= null WHERE `bonplan_id`=".$idbonplan;

        $this->db->where('IdCommercant', $id);

        $res = $this->db->get('commercants');

        return $res->row();

    }

    public function get_by_id_annonce_by_cod_postal($id)
    {
        $this->db->select('annonce.*,commercants.*,villes.*');
        $this->db->from('annonce');
        $this->db->join('commercants', 'commercants.IdCommercant = annonce.annonce_commercant_id');
        $this->db->join('villes', 'villes.IdVille = commercants.IdVille_localisation');
        //$this->db->where("villes.CodePostal",$codepostal);
        $this->db->where("commercants.referencement_annonce", '1');
        $this->db->where("commercants.annonce", '1');
        $this->db->where("commercants.IsActif", '1');
        $this->db->where("annonce.annonce_id", $id);
        $query = $this->db->get();
        return $query->row();
    }

    public function get_3_annuaire($idville)
    {
        $this->db->select('commercants.user_ionauth_id , commercants.NomSociete , commercants.photo1 , commercants.Caracteristiques , rubriques.Nom as rubriques , villes.ville_nom');
        $this->db->from('commercants');
        $this->db->join('villes', 'commercants.IdVille = villes.IdVille');
        $this->db->join('ass_commercants_rubriques', 'commercants.IdCommercant = ass_commercants_rubriques.IdCommercant');
        $this->db->join('rubriques', 'ass_commercants_rubriques.IdRubrique = rubriques.IdRubrique');
        $this->db->where('villes.CodePostal', $idville);
        $this->db->where('commercants.IsActif = 1');
        $this->db->where('commercants.referencement_annuaire = 1');
        $this->db->LIMIT('3');
        $res = $this->db->get();
        return $res->result();
    }

    public function get_3_annonce($codepostal)
    {
        $this->db->select('annonce.annonce_photo1 AS photo,
                    annonce.annonce_description_courte AS texte_courte,
                    annonce.annonce_prix_neuf AS prix_vente,
                    annonce.annonce_prix_solde AS prix_ancien,
                    annonce.annonce_etat AS etat,
                    commercants.NomSociete AS NomSociete,
                    villes.Nom AS ville,
                    villes.CodePostal AS code,
                    commercants.Adresse1 AS quartier,
                    commercants.Adresse2 AS rue,
                    commercants.IdCommercant,
                    commercants.nom_url,
                    commercants.NomSociete,
                    commercants.user_ionauth_id,
                    sous_rubriques.Nom AS rubrique,
                    sous_rubriques.IdSousRubrique,
                    sous_rubriques.IdRubrique,
                    annonce.annonce_id,
                    annonce.annonce_description_longue,
                    annonce.annonce_photo1,
                    annonce.annonce_photo2,
                    annonce.annonce_photo3,
                    annonce.annonce_photo4,
                    annonce.module_paypal,
                    annonce.retrait_etablissement, 
                    annonce.livraison_domicile,
                    annonce.annonce_etat,
                    annonce.annonce_commercant_id');
        $this->db->from('annonce');
        $this->db->join('commercants', 'commercants.IdCommercant = annonce.annonce_commercant_id');
        $this->db->join('villes', 'villes.IdVille = commercants.IdVille');
        $this->db->join('ass_commercants_sousrubriques', 'ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant');
        $this->db->join('sous_rubriques', 'sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique');
        $this->db->where("villes.CodePostal",$codepostal);
        $this->db->where("commercants.referencement_annonce = 1");
        $this->db->where("commercants.IsActif = 1");
        $this->db->LIMIT('3');
        $query = $this->db->get();
        return $query->result();

    }
    public function get_agendaMobile_byId($id){
        $req ="SELECT
            agenda.IdVille,            
            agenda.id,            
            agenda.photo1,            
            agenda.adresse_localisation,            
            agenda.description,            
            agenda.telephone,            
            agenda.nom_manifestation,            
            agenda.date_debut,            
            agenda.date_fin,
            villes.IdVille,
            villes.ville_nom,
            villes.ville_latitude_deg,
            villes.ville_longitude_deg,
            villes.CodePostal
            FROM  agenda
            INNER JOIN villes ON agenda.IdVille = villes.IdVille
            WHERE agenda.id=".$id;       
        $Resreq =  $this->db->query($req);
        return $Resreq->row();
    }
    public function get_list_articleMobile($idville)
    {
        $this->db->select('
                article_datetime.id as datetime_id, 
                article_datetime.date_debut as datetime_debut, 
                article_datetime.date_fin as datetime_fin, 
                article_datetime.heure_debut as datetime_heure_debut, 
                article.*,
                villes.Nom AS ville,
                agenda_categ.category,
                commercants.NomSociete');
        $this->db->from('article');
        $this->db->join('villes', 'article.IdVille_localisation = villes.IdVille');
        $this->db->join('agenda_categ', 'agenda_categ.agenda_categid = article.article_categid');
        $this->db->join('commercants', 'commercants.IdCommercant = article.IdCommercant');
        $this->db->join('article_datetime', 'article_datetime.article_id = article.id');
        $this->db->where('villes.CodePostal', $idville);
        $this->db->where('article.IsActif = 1 OR article.IsActif = 2');
        $this->db->where('commercants.IsActif = 1');
        $this->db->where('commercants.referencement_article = 1');
        $res = $this->db->get();
        return $res->result();
//        var_dump($res->result()); die("test");
    }

    public function get_article_by_id($id)
    {
        $this->db->select('article.*,commercants.*');
        $this->db->where('id', $id);
        $this->db->from('article');
        $this->db->join('commercants', 'commercants.IdCommercant = article.IdCommercant');
        $res = $this->db->get();


        return $res->row();
    }
    public function get_3_article($idville)
    {
        $this->db->select('
                article_datetime.id as datetime_id, 
                article_datetime.date_debut as datetime_debut, 
                article_datetime.date_fin as datetime_fin, 
                article_datetime.heure_debut as datetime_heure_debut, 
                article.*,
                villes.Nom AS ville,
                agenda_categ.category,
                commercants.NomSociete');
        $this->db->from('article');
        $this->db->join('villes', 'article.IdVille_localisation = villes.IdVille');
        $this->db->join('agenda_categ', 'agenda_categ.agenda_categid = article.article_categid');
        $this->db->join('commercants', 'commercants.IdCommercant = article.IdCommercant');
        $this->db->join('article_datetime', 'article_datetime.article_id = article.id');
        $this->db->where('villes.CodePostal', $idville);
        $this->db->where('article.IsActif = 1 OR article.IsActif = 2');
        $this->db->where('commercants.IsActif = 1');
        $this->db->where('commercants.referencement_article = 1');
        $this->db->LIMIT('3');
        $res = $this->db->get();
        return $res->result();
    }

    public function get_commande_list_by_idcom($idcom){
            $this->db->select('commande_soutenons_list.*,users.Email as User_email,users.Portable as User_tel,commercants.*');
            $this->db->from('commande_soutenons_list');
            $this->db->join('users', 'users.IdUser = commande_soutenons_list.id_client');
            $this->db->join('commercants', 'commercants.IdCommercant = commande_soutenons_list.idCom');
            $this->db->where("commande_soutenons_list.idCom",$idcom);
            $this->db->order_by("commande_soutenons_list.created_at",'desc');
            $rest = $this->db->get()->result();
            return $rest;
    }

    public function get_command_details($id){
        $this->db->select('commande_soutenons_details.*,article_soutenons.true_title');
        $this->db->where("commande_soutenons_details.id_commande",$id);
        $this->db->join('article_soutenons', 'article_soutenons.id = commande_soutenons_details.id_produit','left');
        $rest = $this->db->get("commande_soutenons_details")->result();
        return $rest;

    }

    public function getuser_by_id($id){
        $this->db->where('users.IdUser',$id);
        return $this->db->get('users')->row();
    }
    public function get_commande_list_by_id($id){
        $this->db->select('commande_soutenons_list.*');
        $this->db->from('commande_soutenons_list');
        $this->db->where("commande_soutenons_list.id",$id);
        $rest = $this->db->get()->row();
        return $rest;
}

    public function get_prospect_list_by_idcom($idcom){
        $this->db->select('prospect_externe.*');
        $this->db->from('prospect_externe');
        $this->db->where("prospect_externe.IdComprospect",$idcom);
        $rest = $this->db->get()->result();
        return $rest;
    }

    public function save_prospect_by_id_com($fields){
        $this->db->where('Email',$fields['Email']);
        $res = $this->db->get('prospect_externe')->row();
        if(empty($res)){
            $this->db->insert('prospect_externe',$fields);
        }
    }

    public function change_commande_status($idcomande,$status){
        $field = array(
            "etat_commande" => $status,
        );
        $this->db->where('id',$idcomande);
        $this->db->update('commande_soutenons_list',$field);
        return true;
    }

    public function get_commande_list_menu_by_idcom($idcom){
        $this->db->select('commande_menu_list.*');
        $this->db->from('commande_menu_list');
        $this->db->where("commande_menu_list.idCom",$idcom);
        $rest = $this->db->get()->result();
        return $rest;
}

public function get_commande_list_menu_by_id($id){
    $this->db->select('commande_menu_list.*');
    $this->db->from('commande_menu_list');
    $this->db->where("commande_menu_list.id",$id);
    $rest = $this->db->get()->row();
    return $rest;
}

public function change_commande_status_menu($idcomande,$status){
    $field = array(
        "etat_commande" => $status,
    );
    $this->db->where('id',$idcomande);
    $this->db->update('commande_menu_list',$field);
    return true;
}

public function delete_prospect($id){
    $this->db->where('id',$id);
    $this->db->delete('prospect_externe');
}

public function get_command_data($id_user,$id_comm){
    $this->db->select('commande_soutenons_list.*,users.Email as User_email,users.Portable as User_tel,commercants.*');
    $this->db->from('commande_soutenons_list');
    $this->db->join('users', 'users.IdUser = commande_soutenons_list.id_client');
    $this->db->join('commercants', 'commercants.IdCommercant = commande_soutenons_list.idCom');
    $this->db->where("commande_soutenons_list.idCom",$id_comm);
    $this->db->where("commande_soutenons_list.id_client",$id_user);
    $this->db->order_by("commande_soutenons_list.created_at",'desc');
    $rest = $this->db->get()->result();
    return $rest;
}

public function get_command_details_data($id,$id_user){
    $this->db->select('commande_soutenons_details.*,article_soutenons.true_title');
    $this->db->where("commande_soutenons_details.id_commande",$id);
    $this->db->where("commande_soutenons_details.idclient",$id_user);
    $this->db->join('article_soutenons', 'article_soutenons.id = commande_soutenons_details.id_produit','left');
    $rest = $this->db->get("commande_soutenons_details")->result();
    return $rest;
}

}