<?php
class mdlcommercant extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    function GetAutocompleteCommercant($queryString=0){
        $Sql = "select * from commercants where NomSociete like '$queryString%'  order by NomSociete " ;
        $Query = $this->db->query($Sql);
        return $Query->result();
    }

//SI agenda existe 

    function check_agenda($IdCommercant=0){        
        $sql="SELECT * FROM `agenda` WHERE IdCommercant = '" . $IdCommercant . "' AND IsActif = '1'";
        $Query = $this->db->query($sql);
        return $Query->result();
    }



    function infoCommercant($_iCommercantId){
        /*$Sql = "
            select * from commercants
                LEFT JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
                LEFT JOIN rubriques ON ass_commercants_rubriques.IdRubrique = rubriques.IdRubrique
            where commercants.IdCommercant=" . $_iCommercantId ;    */

            /*$Sql = " select commercants.IdCommercant,NomSociete,Siret,Civilite,commercants.Nom, Prenom,Adresse1,
                    Adresse2,IdVille,CodePostal,Handicape,TelFixe,TelMobile,Email,Responsabilite,
                    TelDirect,Article,Photo1,Photo2,Photo3,Photo4,SiteWeb,Horaires,Vacances,Caracteristiques,
                    IdAssociation,ImagePrincipale,Video,Pdf,Facebook,Login,`Password`,Logo,IsActif,NbrPoints,
                    adresse,Twitter,Conditions,Photo5,PhotoAccueil,URI,Conditions_paiement,rubriques.IdRubrique,rubriques.Nom Rubrique,commercants.activite1,commercants.activite2,commercants.labelactivite1,commercants.labelactivite2
                     from commercants
                    LEFT OUTER JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
                    LEFT OUTER JOIN rubriques ON ass_commercants_rubriques.IdRubrique = rubriques.IdRubrique
                    where commercants.IdCommercant=" . $_iCommercantId ;*/


                        $Sql = " SELECT
                        commercants.*,
                        rubriques.IdRubrique,
                        rubriques.Nom AS Rubrique,
                        commercants.activite1,
                        commercants.activite2,
                        commercants.labelactivite1,
                        commercants.labelactivite2,
                        sous_rubriques.Nom AS sousrubrique,
                        villes.Nom AS ville
                        FROM
                        commercants
                        Left Outer Join ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
                        Left Outer Join rubriques ON ass_commercants_rubriques.IdRubrique = rubriques.IdRubrique
                        Left Outer Join ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
                        Left Outer Join sous_rubriques ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique
                        Left Outer Join villes ON villes.IdVille = commercants.IdVille";

                        if (isset($_iCommercantId) && $_iCommercantId!=0 && $_iCommercantId!="" && $_iCommercantId!=NULL && $_iCommercantId!="0") $Sql .= "  where commercants.IdCommercant = " . $_iCommercantId ;

                //show_error($Sql );

                $Query = $this->db->query($Sql);

        $toInfoCommercant = $Query->result();
        

                if ($Query->num_rows() > 0) {
                    return $toInfoCommercant[0] ;


                }
                else {
                    show_error("Ce commerçant est inconnu : ".$_iCommercantId." (Erreur 012)");
                    return -1;
                }
    }
     function GetAll() {
        $qryCommercant =  $this->db->query("
            SELECT
                *
            FROM
                commercants
        ");
        return $qryCommercant->result();
    }
    function GetAllCommercant() {
        $qryCommercant =  $this->db->query("
        SELECT commercants.NomSociete AS NomSociete, commercants.Adresse1 AS quartier, commercants.Adresse2 AS rue, commercants.IdCommercant, bonplan. * , villes.Nom AS ville, villes.NomSimple AS VilleNomSimple
        FROM commercants
        LEFT OUTER JOIN bonplan ON commercants.IdCommercant = bonplan.bonplan_commercant_id
        LEFT JOIN villes ON villes.IdVille = commercants.IdVille");
        return $qryCommercant->result();
    }
    //recup lien doctolib commercant
    public function getAll_by_idcom($idcom){
         $this->db->where('IdCommercant',$idcom);
       $res= $this->db->get('pack_commercant_services');
       return $res->row();

    }    
    function GetAllCommercant_with_annonce() {
        $sqlComm = "
        SELECT 
        commercants.NomSociete AS NomSociete, 
        commercants.Adresse1 AS quartier, 
        commercants.Adresse2 AS rue, 
        commercants.IdCommercant, 
        bonplan. * , 
        villes.Nom AS ville, 
        villes.NomSimple AS VilleNomSimple,
        count(annonce_id) as annonce_nb
        FROM commercants
        LEFT OUTER JOIN bonplan ON commercants.IdCommercant = bonplan.bonplan_commercant_id
        LEFT JOIN villes ON villes.IdVille = commercants.IdVille
        INNER JOIN annonce on annonce.annonce_commercant_id = commercants.IdCommercant
        WHERE commercants.IsActif = '1'
        ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlComm .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlComm .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlComm .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlComm .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlComm .= " OR ";
            }
            $sqlComm .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlComm .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $sqlComm .= "
        group by IdCommercant, bonplan.bonplan_id
        ";

        $qryCommercant =  $this->db->query($sqlComm);
        return $qryCommercant->result();
    }

    function GetAllCommercant_with_bonplan() {
        $sqlcom = "
                SELECT
                commercants.NomSociete AS NomSociete,
                commercants.Adresse1 AS quartier,
                commercants.Adresse2 AS rue,
                commercants.IdCommercant,
                Count(bonplan.bonplan_id) AS bonplan_nb
                FROM commercants
                INNER JOIN bonplan ON commercants.IdCommercant = bonplan.bonplan_commercant_id
                WHERE commercants.IsActif = 1 ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcom .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcom .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcom .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcom .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcom .= " OR ";
            }
            $sqlcom .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcom .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE



        $sqlcom .= "
                group by IdCommercant
        ";

        $qryCommercant =  $this->db->query($sqlcom);
        return $qryCommercant->result();
    }

    function GetAllCommercant_with_fidelity() {
        $sqlcom = "
                SELECT
                commercants.NomSociete AS NomSociete,
                commercants.Adresse1 AS quartier,
                commercants.Adresse2 AS rue,
                commercants.IdCommercant,
                COUNT(card_remise.id) as nb_remise,
                COUNT(card_tampon.id) as nb_tampon,
                COUNT(card_capital.id) as nb_capital
                FROM commercants
                LEFT OUTER JOIN card_capital ON card_capital.id_commercant = commercants.IdCommercant
                LEFT OUTER JOIN card_remise ON card_remise.id_commercant = commercants.IdCommercant
                LEFT OUTER JOIN card_tampon ON card_tampon.id_commercant = commercants.IdCommercant
                WHERE commercants.IsActif = 1
                AND (commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_remise) 
                OR commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_tampon) 
                or commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_capital))
                ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcom .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcom .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcom .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcom .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcom .= " OR ";
            }
            $sqlcom .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcom .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE



        $sqlcom .= "
                group by IdCommercant
        ";

        $qryCommercant =  $this->db->query($sqlcom);
        return $qryCommercant->result();
    }

    function GetRubriqueId($IdCom = 0) {

        $qryString = "SELECT   * FROM ass_commercants_rubriques ";
        if (isset($IdCom) && $IdCom!=0 && $IdCom!="" && $IdCom!=NULL && $IdCom!="0")
        $qryString .= " WHERE IdCommercant = " . $IdCom ;
        $qryString .= " LIMIT 1;";

        return $this->db->query($qryString)->row();
    }


    function GetIdCommercantfromUrl($fromUrl = "") {
        $qryCommercant =  $this->db->query("SELECT * FROM commercants WHERE nom_url = '$fromUrl' LIMIT 1");
        //var_dump($fromUrl);die();
        //var_dump($qryCommercant);die();
        if ($qryCommercant->num_rows() > 0) {
            $Res = $qryCommercant->result();
            //var_dump($Res);die();
            return $Res[0]->IdCommercant;
            //var_dump($Res[0]);die();
        }
    }



    function Update_nbrevisites_commercant($_iCommercantId, $nbvisite){
        $zSqlnbrevisites_commercant = "UPDATE commercants SET nbrevisites='.$nbvisite.' WHERE IdCommercant=" . $_iCommercantId ;
        $this->db->query($zSqlnbrevisites_commercant) ;
    }
    public function get_commercant_actif_article($IdCommercant=0){

        $zSqlListePartenaire = "

        SELECT
            commercants.IdCommercant,
            commercants.NomSociete,
            commercants.Nom,
            count(commercants.IdCommercant) as nbcommercant
            FROM
            commercants
            Inner Join article ON article.IdCommercant = commercants.IdCommercant
            where commercants.IsActif = 1 AND article.IsActif = 1 AND article.agenda_article_type_id=1
            AND (article.IsActif = '1' OR article.IsActif = '2') 
            AND commercants.referencement_article = '1' 
            
        ";

        if(isset($IdCommercant) && $IdCommercant!=0 && is_numeric($IdCommercant) && $IdCommercant!="0")
            $zSqlListePartenaire .= "
                AND commercants.IdCommercant = ".$IdCommercant."
            ";

        //LOCALDATA FILTRE
        $zSqlListePartenaire .= "
        GROUP BY
            commercants.IdCommercant
            
        ORDER BY commercants.Nom Asc
         
        ";
        $qryCategorie = $this->db->query($zSqlListePartenaire);
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
}
    public function get_glissiere_by_id_com($idcom){
        $this->db->where('IdCommercant',$idcom);
        $res=$this->db->get('glissieres');
        return $res->row();
    }
    public function get_bloc_by_id_com($idcom){
        $this->db->where('IdCommercant',$idcom);
        $res=$this->db->get('bloc_info');
        return $res->row();
    }
    public function gest_by_id_user($iduser){
        $this->db->where('user_ionauth_id',$iduser);
        $res=$this->db->get('commercants');
        return $res->row();
    }
    public function get_user_by_id_card($num_card){
        $this->db->select('users.*');
        $this->db->join('card','card.id_user=users.IdUser');
        $this->db->where('card.num_id_card_virtual',$num_card);
        $res= $this->db->get('users');
        if ($res->num_rows() !=0){
            return $res->row();
        }else{
            return false;
        }
    }
    //save domain name commercant
    public function save_recap_validation($idCom ,$nom_domain ,$sitekey,$secretkey){
        $update_Com = "UPDATE commercants SET domain_name='$nom_domain',domain_sitekey='$sitekey',domain_secretKey='$secretkey' WHERE IdCommercant=".$idCom ;
        $this->db->query($update_Com) ;
    }
    public function change_id_com($old,$new){
        $field = array(
            "IdCommercant" => $new,
        );
        $this->db->where("IdCommercant",$old);
        $this->db->update("agenda",$field);

    }
    public function insert_video_bg($prmdata){
        $this->db->insert('video_commercant',$prmdata);
        $this->db->where('IdCommercant',$prmdata['IdCommercant']);
        $result = $this->db->get('video_commercant');
        return $result->row();
    }
    public function get_video_bg_by_idcom($idcom){
        $this->db->where('IdCommercant',$idcom);
        $result = $this->db->get('video_commercant');
        return $result->row();
    }
    public function delete_video_bg_by_idcom($idcom){
        $this->db->where('IdCommercant',$idcom);
        $this->db->delete('video_commercant');
    }
    //begin kappa 
    /** 
     * 22.02.22
     * save img for btn square click&collect
     * 
     */
    function save_click($idcom, $data){
 
    $this->db->where("IdCommercant", $idcom);
    $this->db->update("commercants", $data);

    }
    function save_click_icon($icat, $img_file){

    $image = array('click_icon'=>$img_file);
    $this->db->where("IdCommercant", $icat);
    $this->db->update("commercants", $image);
    }
    function delete_click_menu($idcom){

      $Sql = "
            UPDATE commercants
            SET click_icon = null
            WHERE IdCommercant = '".$idcom."'
        ";
        $Query = $this->db->query($Sql);
    }

    /** 
     * 04.03.22
     * get login and pdw
     * to send_mail basic account
     */
    function get_log_pwd($IdCommercant){

    $this->db->where("login", $IdCommercant);
    $data = $this->db->get('liste_login_import_commercant');
    return $data->row();

    }
/*****AFFICHER MDP DANS LA TABLE LISTE_LOGIN_IMPORT**************/
    function get_log_pwd_claire($login){

        // $Sql = "select * from villes where IdVille =" . $id;

        // $sql="SELECT liste_login_import_commercant.login, liste_login_import_commercant.password as pwdClaire,users_ionauth.password FROM `liste_login_import_commercant`,users_ionauth WHERE liste_login_import_commercant.login=users_ionauth.username LIMIT 10";
        $sql="SELECT * FROM liste_login_import_commercant WHERE login='".$login."'";
        $Query = $this->db->query($sql);
        $com = $Query->result();   
        return $com;        


    }
/*****INSERTION DANS LA TABLE LISTE_LOGIN_IMPORT*******/    
    function insertion_liste_login_import($log_mdp){
        // $data=array(
        // 'titre_article'=>$this->input->post('nom'),
        // 'message'=>$this->input->post('mdp')
        // );

            return $this->db->insert('liste_login_import_commercant',$log_mdp);        
            
    }    
    /** 
     * 15.04.22
     * get name and ville
     * to send_newsletter basic account
     */
    
    function get_by_mail($mail){

    $get = "SELECT commercants.NomSociete AS enseigne,
    commercants.nom_url AS url,
    commercants.adresse1 AS adresse,
     villes.Nom AS ville,
    sous_rubriques.Nom AS sousrubrique,
    rubriques.Nom AS Rubrique
        FROM commercants
        Left Outer Join ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
        Left Outer Join rubriques ON ass_commercants_rubriques.IdRubrique = rubriques.IdRubrique
        Left Outer Join ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
        Left Outer Join sous_rubriques ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique
        LEFT OUTER JOIN villes ON villes.IdVille = commercants.IdVille
        where commercants.Email = '".$mail."'
        ";
    $data = $this->db->query($get);
        return $data->row();
    }
//end kappadev

}