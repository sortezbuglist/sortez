<?php
class mdl_commune extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
	
	function getById($id=0){
		$Sql = "select * from com_cant where id_commune =". $id ;		
		$Query = $this->db->query($Sql);
		return $Query->row();
	}

	function GetAll(){
        $qryVille = $this->db->query("
           SELECT
			com_cant.N_Com_min,
			com_cant.id_commune
			FROM
			com_cant

        ");
        if($qryVille->num_rows() > 0) {
            return $qryVille->result();
        }
    }
   
    function update($prmData) {
        $this->db->where("id_commune", $prmData["id_commune"]);
        $this->db->update("com_cant", $prmData);
        $objResult = $this->getById($prmData["id_commune"]);
        return $objResult->id_commune;
    }

}