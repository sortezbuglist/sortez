<?php
class Mdl_lampe_avenue extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    function GetAll_prod(){
        $qryVilles = $this->db->query("
            SELECT
                *
            FROM
                ps_category_product
            ORDER BY id_category ASC
        ");
        if($qryVilles->num_rows() > 0) {
            return $qryVilles->result();
        }
    }

    function GetAll_dev(){
        $qryVilles = $this->db->query("
            SELECT
                *
            FROM
                sa_category_product
            ORDER BY id_category ASC
        ");
        if($qryVilles->num_rows() > 0) {
            return $qryVilles->result();
        }
    }
    function Get_dev_by_id_category($id=0){
        $Sql = "select * from sa_category_product where id_category =". $id  ;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }
    function Get_prod_by_id_category($id=0){
        $Sql = "select * from ps_category_product where id_category =". $id  ;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function Update_prod($prmData) {
        $this->db->where("id_category", $prmData["id_category"]);
        $this->db->where("id_product", $prmData["id_product"]);
        $this->db->update("ps_category_product", $prmData);
        return $prmData["id_product"];
    }
    function Update_dev($prmData) {
        $this->db->where("id_category", $prmData["id_category"]);
        $this->db->where("id_product", $prmData["id_product"]);
        $result_final = $this->db->update("sa_category_product", $prmData);
        return $prmData["id_product"];
    }
    function Get_prod_by_id_product_id_category($id_category=0,$id_product=0){
        $Sql = "select * from ps_category_product where id_category = ".$id_category." and id_product =". $id_product . " LIMIT 1"  ;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }
    function Get_dev_by_id_product_id_category($id_category=0,$id_product=0){
        $Sql = "select * from sa_category_product where id_category = ".$id_category." and id_product =". $id_product . " LIMIT 1" ;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }
    function Get_all_id_product(){
        $qryVilles = $this->db->query("
            SELECT
                *
            FROM
                sa_product
            ORDER BY id_product ASC
        ");
        if($qryVilles->num_rows() > 0) {
            return $qryVilles->result();
        }
    }

    function Get_dev_by_id_product($id=0){
        $Sql = "select * from sa_category_product where id_product =". $id  ." order by id_category ASC ";
        $qryVilles = $this->db->query($Sql);
        if($qryVilles->num_rows() > 0) {
            return $qryVilles->result();
        }
    }
    function Get_prod_by_id_product($id=0){
        $Sql = "select * from ps_category_product where id_product =". $id  ." order by id_category ASC ";
        $qryVilles = $this->db->query($Sql);
        if($qryVilles->num_rows() > 0) {
            return $qryVilles->result();
        }
    }


}