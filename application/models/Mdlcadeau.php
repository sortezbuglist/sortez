<?php
class mdlcadeau extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
   
		 function Delete($argId) {
			if ($argId > 0) {
				return $this->db->query("delete from cadeaux where Idcadeau = " . $argId);
			}
			return false;
		}
		 /**
		 * Compter le nombre d'enregistrement selon une ou des conditions
		 *
		 * @param String $argConditions		Condition de comptage
		 * @return int	Nombre d'enregistrement trouv�
		 */
		function Compter($argConditions = 1) {
			$Sql = "select count(Idcadeau) as Nb from cadeaux where " . $argConditions;
			$Query = $this->db->query($Sql);

			if ($Query) {
				$Res = $Query->row();
				return $Res->Nb;
			}
			return 0;
		}
		function GetById($argId) {
         $Query = $this->db->get_where("cadeaux", array ("Idcadeau" => $argId));
         return $Query->row();
        }
		function GetListe($argLimit = 1000000000, $argOffset = 0, $zSqlCondition = 1) {

        $this->Sql = "
			SELECT *
			FROM cadeaux WHERE
			" . $zSqlCondition . "
			 LIMIT " . $argOffset . " , " . $argLimit .  ";" ;
      
		   $Query = $this->db->query($this->Sql);
		    /* print_r($Query->result());exit(); */
           return $Query->result();
        }
        
        
        function GetListeCadeauxvalides($date) {

        $this->Sql = "
			SELECT *
			FROM cadeaux WHERE
                        Qtedisponible >0 and Valabledebut <= '".$date."' and Valablefin >= '".$date."'
                        order by Idcadeau desc
                        ;" ;
      
		   $Query = $this->db->query($this->Sql);
		    /* print_r($Query->result());exit(); */
           return $Query->result();
        }
		
		function getCadeauFidelisation($date){
		  $this->Sql = "
			SELECT *
			FROM cadeaux WHERE Qtedisponible >0 and Valabledebut <= '".$date."' and Valablefin >= '".$date."' ";       
		   $Query = $this->db->query($this->Sql);
           return $Query->result();
		}
		
		function getMaxNumMailBonPlan(){
		 
         $this->Sql = "
			SELECT max(id) as numero
			FROM assoc_client_bonplan  ";       
		    $Query = $this->db->query($this->Sql);
			if ($Query) {
				$Res = $Query->row();
				return $Res->numero;
			}
			return 0;	 
           
		}
		function demanderBonPlan($prmData){
		  $this->db->insert("assoc_client_bonplan", $prmData);
          return $this->db->insert_id();		 
           
		}
		function isValideNum($prmData){
		   $this->Sql = "
			SELECT *
			FROM assoc_client_bonplan  where utilise = 0 AND code_cadeaux_client ='".$prmData[0]."' AND code_cadeaux_commercant ='".$prmData[1]."' LIMIT 1";       
		    $Query = $this->db->query($this->Sql);
           
			return $Query->row();	 
           
		}
		
		function ValiderNum($prmData){
		   $this->Sql = "
			UPDATE assoc_client_bonplan set utilise =1 where id =".$prmData;       
		    $Query = $this->db->query($this->Sql);
                    
		}
		function AjouterPointsClients($prmData,$NbrPoints){
		
		   $this->Sql = "
			UPDATE users set NbrPoints =".$NbrPoints." where IdUser = ".$prmData;       
		    $Query = $this->db->query($this->Sql);
                    
		}
		function getClientAssocBonPlan($id_client,$id_bonplan){
		   $this->Sql = "
			SELECT *
			FROM assoc_client_bonplan  where id_client ='".$id_client."'" ." AND id_bonplan =".$id_bonplan." order by id desc"  ;       
		    $Query = $this->db->query($this->Sql);
           
			return $Query->row();	 
           
		}
                
                function getMaxIdcadeau(){
		 
                        $this->Sql = "
			SELECT max(Idcommande) as id
			FROM commande  ";       
		    $Query = $this->db->query($this->Sql);
			if ($Query) {
				$Res = $Query->row();
				return $Res->id;
			}
			return 0;	 
           
		}
		
	}