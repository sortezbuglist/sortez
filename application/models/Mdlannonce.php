<?php
class mdlannonce extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }
   
    function listeAnnonce()
    {
        //$zSqlListeAnnonce = "SELECT * FROM annonce LEFT JOIN categorie ON annonce.sous_rubriquesorie_id=categorie.categorie_id LEFT JOIN commercants ON commercants.IdCommercant=annonce.annonce_commercant_id LEFT JOIN villes ON villes.IdVille=commercants.IdVille" ;
        $zSqlListeAnnonce = "
                SELECT
                annonce.annonce_photo1 AS photo,
                annonce.annonce_description_courte AS texte_courte,
                annonce.annonce_prix_neuf AS prix_vente,
                annonce.annonce_etat AS etat,
                commercants.NomSociete AS NomSociete,
                villes.Nom AS ville,
                commercants.Adresse1 AS quartier,
                commercants.Adresse2 AS rue,
                commercants.IdCommercant,
                annonce.annonce_id,
                sous_rubriques.Nom AS rubrique
                FROM
                annonce
                Left Join commercants ON commercants.IdCommercant = annonce.annonce_commercant_id
                Left Join villes ON villes.IdVille = commercants.IdVille
                Inner Join ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
                Inner Join sous_rubriques ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique
                order by annonce.annonce_id desc
    
                ";
        $zQueryListeAnnonce = $this->db->query($zSqlListeAnnonce);
        return $zQueryListeAnnonce->result();
    }



    function GetAllAnnonceforNewsletter()
    {
        $qryCommercant = $this->db->query("
            SELECT
            annonce.*,
            commercants.IdCommercant,
            commercants.NomSociete,
            commercants.Adresse1,
            commercants.Adresse2,
            villes.Nom AS ville,
            sous_rubriques.Nom as sousrubrique
            FROM
            annonce
            Inner Join commercants ON commercants.IdCommercant = annonce.annonce_commercant_id
            Inner Join villes ON villes.IdVille = commercants.IdVille
            Inner Join ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
            left outer join sous_rubriques ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique
            WHERE
                        commercants.IsActif =  '1'
            ORDER BY
                        annonce.annonce_id DESC
            LIMIT 4
        ");
        return $qryCommercant->result();
    }


    function nombreAnnonceParDiffuseur($_iCommercantId)
    {

        $zSqlListeAnnonceParDiffuseur = "
		SELECT
		annonce.annonce_photo1 AS photo,
		rubriques.Nom AS rubrique,
		annonce.annonce_description_courte AS texte_courte,
		annonce.annonce_prix_neuf AS prix_vente,
		annonce.annonce_etat AS etat,
		commercants.NomSociete AS NomSociete,
		villes.Nom AS ville,
		commercants.Adresse1 AS quartier,
		commercants.Adresse2 AS rue,
		commercants.IdCommercant,
		annonce_id
		FROM
		annonce
		LEFT JOIN commercants ON commercants.IdCommercant=annonce.annonce_commercant_id
		LEFT JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
		LEFT JOIN rubriques ON rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique
		LEFT JOIN villes ON villes.IdVille=commercants.IdVille
		WHERE 0=0
		";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlListeAnnonceParDiffuseur .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlListeAnnonceParDiffuseur .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlListeAnnonceParDiffuseur .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlListeAnnonceParDiffuseur .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListeAnnonceParDiffuseur .= " OR ";
            }
            $zSqlListeAnnonceParDiffuseur .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlListeAnnonceParDiffuseur .= " AND commercants.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        if (isset($_iCommercantId) && $_iCommercantId != 0 && $_iCommercantId != "" && $_iCommercantId != NULL && $_iCommercantId != "0")
            $zSqlListeAnnonceParDiffuseur .= "
		    AND annonce_commercant_id=" . $_iCommercantId;

        $zQueryListeAnnonceParDiffuseur = $this->db->query($zSqlListeAnnonceParDiffuseur);
        $toListeAnnonceParDiffuseur = $zQueryListeAnnonceParDiffuseur->result();
        return sizeof($toListeAnnonceParDiffuseur);
    }

    function listeAnnonceParCommercant($_iCommercantId)
    {
        $zSqlListeAnnonceParDiffuseur = "
              SELECT
              annonce.annonce_photo1 AS photo,
              annonce.annonce_photo1,
              annonce.annonce_photo2,
              annonce.annonce_photo3,
              annonce.annonce_photo4,
              annonce.annonce_prix_solde AS ancien_prix,
              rubriques.Nom AS rubrique,
              annonce.annonce_description_courte AS texte_courte,
              annonce.annonce_prix_neuf AS prix_vente,
              annonce.annonce_etat AS etat,
              commercants.NomSociete AS NomSociete,
              villes.Nom AS ville,
              commercants.Adresse1 AS quartier,
              commercants.Adresse2 AS rue,
              commercants.IdCommercant,
              annonce_id
              FROM
              annonce
              LEFT JOIN commercants ON commercants.IdCommercant=annonce.annonce_commercant_id
              LEFT JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
              LEFT JOIN rubriques ON rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique
              LEFT JOIN villes ON villes.IdVille=commercants.IdVille
              WHERE
              annonce_commercant_id = '" . $_iCommercantId . "'
              ORDER BY annonce.order_partner ASC, annonce_id DESC";
        $zQueryListeAnnonceParDiffuseur = $this->db->query($zSqlListeAnnonceParDiffuseur);
        $toListeAnnonceParDiffuseur = $zQueryListeAnnonceParDiffuseur->result();
        return $toListeAnnonceParDiffuseur;
    }
    function listeAnnonceParCommercant_soutenons($_iCommercantId)
    {
        $zSqlListeAnnonceParDiffuseur = "
              SELECT
              annonce.annonce_photo1 AS photo,
              annonce.annonce_photo1,
              annonce.annonce_photo2,
              annonce.annonce_photo3,
              annonce.annonce_photo4,
              annonce.annonce_prix_solde AS ancien_prix,
              rubriques.Nom AS rubrique,
              annonce.annonce_description_courte AS texte_courte,
              annonce.annonce_description_longue AS texte_longue,
              annonce.annonce_prix_neuf AS prix_vente,
              annonce.annonce_etat AS etat,
              commercants.NomSociete AS NomSociete,
              villes.Nom AS ville,
              commercants.Adresse1 AS quartier,
              commercants.Adresse2 AS rue,
              commercants.IdCommercant,
              annonce_id
              FROM
              annonce
              LEFT JOIN commercants ON commercants.IdCommercant=annonce.annonce_commercant_id
              LEFT JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
              LEFT JOIN rubriques ON rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique
              LEFT JOIN villes ON villes.IdVille=commercants.IdVille
              WHERE
              annonce_commercant_id = '" . $_iCommercantId . "'
              ORDER BY annonce.order_partner ASC, annonce_id DESC";
        $zQueryListeAnnonceParDiffuseur = $this->db->query($zSqlListeAnnonceParDiffuseur);
        $toListeAnnonceParDiffuseur = $zQueryListeAnnonceParDiffuseur->result();
        return $toListeAnnonceParDiffuseur;
    }

    function check_commercant_annonce($IdCommercant)
    {
        $zSqlListeAnnonceParDiffuseur = "
        SELECT
                commercants.IdCommercant,
                annonce.annonce_id,
                commercants.annonce,
                commercants.IsActif
                FROM
                commercants
                INNER JOIN annonce ON annonce.annonce_commercant_id = commercants.IdCommercant
                WHERE
                commercants.IdCommercant = '" . $IdCommercant . "' AND
                commercants.IsActif = 1 AND 
                commercants.annonce = 1
        ";
        $zQueryListeAnnonceParDiffuseur = $this->db->query($zSqlListeAnnonceParDiffuseur);
        $toListeAnnonceParDiffuseur = $zQueryListeAnnonceParDiffuseur->result();
        return $toListeAnnonceParDiffuseur;
    }

    function listeAnnonceRecherche($_iSousRubriqueId = 0, $_iCommercantId = 0, $_zMotCle = "", $_iEtat = "", $_limitstart = 0, $_limitend = 10000, $_iIdVille = 0, $_iIdDepartement = 0, $iOrderBy = '', $inputFromGeo = "0", $inputGeoValue = "10", $inputGeoLatitude = "0", $inputGeoLongitude = "0")
    {
        /*Prendre la rubrique***/
        /*$zSqlRubrique = "SELECT * FROM sous_rubriques WHERE IdSousRubrique=" . $_iSousRubriqueId ;
        $zQueryRubrique = $this->db->query($zSqlRubrique) ;
        $toRubrique = $zQueryRubrique->result() ;*/
        /************************/
        // var_dump($_iCommercantId);die("id comm dans model");
        $_zMotCle = str_replace("'", "\'", $_zMotCle);
        $_zMotCle = str_replace('"', '\"', $_zMotCle);

        $zSqlListeAnnonce = "
                    SELECT
                    annonce.annonce_photo1 AS photo,
                    annonce.annonce_description_courte AS texte_courte,
                    annonce.annonce_prix_neuf AS prix_vente,
                    annonce.annonce_prix_solde AS prix_ancien,
                    annonce.annonce_etat AS etat,
                    commercants.NomSociete AS NomSociete,
                    villes.Nom AS ville,
                    commercants.Adresse1 AS quartier,
                    commercants.Adresse2 AS rue,
                    commercants.IdCommercant,
                    commercants.nom_url,
                    sous_rubriques.Nom AS rubrique,
                    sous_rubriques.IdSousRubrique,
                    sous_rubriques.IdRubrique,
                    annonce.annonce_id,
                    annonce.annonce_description_longue,
                    annonce.annonce_photo1,
                    annonce.annonce_photo2,
                    annonce.annonce_photo3,
                    annonce.annonce_photo4,
                    annonce.module_paypal,
                    annonce.retrait_etablissement, 
                    annonce.livraison_domicile,
                    annonce.annonce_commercant_id ";

        if ($inputFromGeo == "1") {
            $zSqlListeAnnonce .= " , (6371 * acos(cos(radians($inputGeoLatitude)) * cos(radians(commercants.latitude)) * cos(radians(commercants.longitude) - radians($inputGeoLongitude)) + sin(radians($inputGeoLatitude)) * sin(radians(commercants.latitude)))) AS distance ";
        }

        $zSqlListeAnnonce .= "
                    FROM
                    annonce
                    Left Join commercants ON commercants.IdCommercant = annonce.annonce_commercant_id
                    Left Join villes ON villes.IdVille = commercants.IdVille
                    Inner Join ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
                    Inner Join sous_rubriques ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique
                    WHERE
                    0=0
                    AND commercants.IsActif = '1' AND commercants.referencement_annonce = '1' 

                    ";

        if ($inputFromGeo == "1") {
            $zSqlListeAnnonce .= " AND commercants.latitude != '' AND commercants.longitude != '' ";
        }

        /*if($_iSousRubriqueId != 0){
            $zSqlListeAnnonce .= " AND sous_rubriques.IdSousRubrique=" . $_iSousRubriqueId ;
        }*/
        if (isset($_iSousRubriqueId) && $_iSousRubriqueId[0] != NULL && $_iSousRubriqueId[0] != "" && $_iSousRubriqueId[0] != 0) {
            $zSqlListeAnnonce .= " AND (";
            if (sizeof($_iSousRubriqueId) == 1) {
                $zSqlListeAnnonce .= " sous_rubriques.IdSousRubrique = " . $_iSousRubriqueId[0];
            } else {
                $zSqlListeAnnonce .= " sous_rubriques.IdSousRubrique = " . $_iSousRubriqueId[0];
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousRubriqueId); $ii_rand++) {
                    $zSqlListeAnnonce .= " OR sous_rubriques.IdSousRubrique = " . $_iSousRubriqueId[$ii_rand];
                }
            }
            $zSqlListeAnnonce .= " )";
        }
        if ($_iCommercantId != 0 && $_iCommercantId != "" && $_iCommercantId != null) {
            $zSqlListeAnnonce .= " AND annonce.annonce_commercant_id=" . $_iCommercantId;
        }
        if ($_zMotCle != "") {
            $zSqlListeAnnonce .= " AND (";
            $zSqlListeAnnonce .= "UPPER(annonce.annonce_description_courte) LIKE '%" . strtoupper($_zMotCle) . "%' ";
            $zSqlListeAnnonce .= "OR UPPER(annonce.annonce_description_longue) LIKE '%" . strtoupper($_zMotCle) . "%'";
            $zSqlListeAnnonce .= ")";
        }
        if ($_iEtat != "" && $_iEtat != NULL) {
            $zSqlListeAnnonce .= " AND annonce.annonce_etat=" . $_iEtat;
        }

        /*if($_iIdVille != 0 && $_iIdVille != "" && $_iIdVille != null){
            $zSqlListeAnnonce .= " AND commercants.IdVille=" . $_iIdVille ;
        }*/
        if ($_iIdVille != 0 && $_iIdVille != "" && $_iIdVille != null) {
            $zSqlListeAnnonce .= " AND commercants.IdVille=" . $_iIdVille;
        } elseif ($_iIdDepartement != 0 && $_iIdDepartement != "" && $_iIdDepartement != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iIdDepartement);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListeAnnonce .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListeAnnonce .= " commercants.IdVille=" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListeAnnonce .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListeAnnonce .= ")";
            }
        }


        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlListeAnnonce .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            if (!isset($localdata_IdVille_all)){
                $zSqlListeAnnonce .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
            } else {
                $zSqlListeAnnonce .= " AND ( ";
                    for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                        $zSqlListeAnnonce .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                        if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListeAnnonce .= " OR ";
                    }
                $zSqlListeAnnonce .= " ) ";
            }
            
        }  else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlListeAnnonce .= " AND commercants.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        //SORTEZ.ORG => NO BASIC PARTNER ACCOUT
        $zSqlListeAnnonce .= " AND commercants.user_ionauth_id IN (SELECT users_groups_ionauth.user_ionauth_id FROM users_groups_ionauth WHERE users_groups_ionauth.group_ionauth_id = 4 OR users_groups_ionauth.group_ionauth_id = 5) ";
        //SORTEZ.ORG => NO BASIC PARTNER ACCOUT

        // demo account commercant should not appear on annuaire list
        $zSqlListeAnnonce .= " AND commercants.IdCommercant <> '301298'  AND commercants.IdCommercant <> '301266' ";
        //$zSqlListeAnnonce .= " AND commercants.IdCommercant <> '301298' ";

        if ($inputFromGeo == "1") {
            $zSqlListeAnnonce .= "  HAVING distance < $inputGeoValue  ";
        }

        if ($iOrderBy == '1') $iOrderBy_value = " order by annonce.annonce_id desc ";
        else if ($iOrderBy == '2') $iOrderBy_value = " order by commercants.nbrevisites desc ";
        else if ($iOrderBy == '3') $iOrderBy_value = " order by annonce.annonce_id asc ";
        else $iOrderBy_value = " order by annonce.annonce_id desc ";

        $zSqlListeAnnonce .= $iOrderBy_value;

        $zSqlListeAnnonce .= " LIMIT " . $_limitstart . "," . $_limitend;


        //echo  $zSqlListeAnnonce;

        //var_dump($zSqlListeAnnonce); echo $zSqlListeAnnonce;die();

        $zQueryListeAnnonce = $this->db->query($zSqlListeAnnonce);
        return $zQueryListeAnnonce->result();
    }


    function detailAnnonce($_iAnnonceId)
    {
        $zSqlDetailAnnonce = "
			SELECT commercants.NomSociete AS NomSociete, commercants.IdCommercant AS id_commercant,
			rubriques.Nom AS rubrique, commercants.Adresse1 AS quartier, commercants.Adresse2 AS rue,
			annonce.annonce_photo1 AS photo1,
			annonce.annonce_photo2 AS photo2,
			annonce.annonce_photo3 AS photo3,
			annonce.annonce_photo4 AS photo4,
			annonce.annonce_description_courte AS texte_courte,
			annonce.annonce_description_longue AS texte_longue, annonce.annonce_prix_solde AS ancien_prix,
			annonce.annonce_prix_neuf AS prix_vente, annonce.annonce_etat AS etat, annonce_id, annonce_commercant_id,
			annonce_date_debut, annonce_date_fin, sous_rubriques.Nom AS sous_rubrique, villes.NomSimple AS 'VilleNomSimple',
			annonce.annonce_quantite,
			annonce.retrait_etablissement,
			annonce.livraison_domicile,
			annonce.module_paypal,
			annonce.module_paypal_btnpaypal,
			annonce.module_paypal_panierpaypal,
			annonce.slide_type
			FROM annonce
				LEFT JOIN commercants ON commercants.IdCommercant=annonce.annonce_commercant_id
                LEFT JOIN villes ON villes.IdVille=commercants.IdVIlle
				LEFT JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
				LEFT JOIN rubriques ON rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique
				LEFT JOIN sous_rubriques ON sous_rubriques.IdSousRubrique = annonce.annonce_sous_rubriques_id
			WHERE annonce_id = " . $_iAnnonceId . "

		";
        $zQueryDetailAnnonce = $this->db->query($zSqlDetailAnnonce);
        $toDetailAnnonce = $zQueryDetailAnnonce->row();
        return $toDetailAnnonce;
    }

    function insertAnnonce($prmData)
    {
        if ($prmData['annonce_id'] == '') $prmData['annonce_id'] = null;
        if ($prmData['annonce_date_debut'] == '' || $prmData['annonce_date_debut'] == '--') $prmData['annonce_date_debut'] = null;
        if ($prmData['annonce_date_fin'] == '' || $prmData['annonce_date_fin'] == '--') $prmData['annonce_date_fin'] = null;
        $this->db->insert("annonce", $prmData);
        return $this->db->insert_id();
    }

    function updateAnnonce($prmData)
    {
        $prmData = (array)$prmData;
        if ($prmData["annonce_date_debut"] == "--" || $prmData["annonce_date_debut"] == "") $prmData["annonce_date_debut"] = null;
        if ($prmData["annonce_date_fin"] == "--" || $prmData["annonce_date_fin"] == "") $prmData["annonce_date_fin"] = null;
        $this->db->where("annonce_id", $prmData["annonce_id"]);
        $this->db->update("annonce", $prmData);
        $objAnnonce = $this->GetById($prmData["annonce_id"]);
        return $objAnnonce->annonce_id;
    }

    function GetById($prmId)
    {
        $qryAnnonce = $this->db->query("SELECT * FROM annonce WHERE annonce_id = ?", $prmId);
        if ($qryAnnonce->num_rows() > 0) {
            $Res = $qryAnnonce->result();
            return $Res[0];
        }
    }

    function listeMesAnnonces($prmId)
    {
        $zSql = "SELECT * FROM annonce WHERE annonce_commercant_id = '" . $prmId . "' ORDER BY order_partner asc";
        $zQueryListeAnnonce = $this->db->query($zSql);
        return $zQueryListeAnnonce->result();
    }

    function GetAnnonce($prmId)
    {
        $qryAnnonce = $this->db->query("SELECT * FROM annonce WHERE annonce_id = ?", $prmId);
        if ($qryAnnonce->num_rows() > 0) {
            $Res = $qryAnnonce->result();
            return $Res[0];
        }
    }

    function supprimeAnnonces($prmId)
    {

        $zRpsAnnonce = $this->db->query("SELECT * FROM annonce WHERE annonce_id = ?", $prmId);
        $toAnnonce = $zRpsAnnonce->result();
        $oAnnonce = $toAnnonce[0];
        if (is_file("application/resources/front/images/" . $oAnnonce->annonce_photo1)) {
            unlink("application/resources/front/images/" . $oAnnonce->annonce_photo1);
        }
        if (is_file("application/resources/front/images/" . $oAnnonce->annonce_photo2)) {
            unlink("application/resources/front/images/" . $oAnnonce->annonce_photo2);
        }
        if (is_file("application/resources/front/images/" . $oAnnonce->annonce_photo3)) {
            unlink("application/resources/front/images/" . $oAnnonce->annonce_photo3);
        }
        if (is_file("application/resources/front/images/" . $oAnnonce->annonce_photo4)) {
            unlink("application/resources/front/images/" . $oAnnonce->annonce_photo4);
        }
        $qryAnnonce = $this->db->query("DELETE FROM annonce WHERE annonce_id = ?", $prmId);
        return $qryAnnonce;
    }

    function getValideAnnonceByIdCommernats($prmListCommercants = "")
    {
        $qryBonplan = $this->db->query("
            SELECT
            	annonce.*,
            	commercants.NomSociete
            FROM
            	(annonce
            	LEFT JOIN	commercants ON commercants.IdCommercant = annonce.annonce_commercant_id)
            WHERE
            	annonce.annonce_commercant_id IN ($prmListCommercants)
            	AND NOW() BETWEEN	annonce.annonce_date_debut AND annonce.annonce_date_fin
        ");
        return $qryBonplan->result();
    }

    function getlisteAnnonce()
    {
        //$zSqlListeAnnonce = "SELECT * FROM annonce LEFT JOIN categorie ON annonce.sous_rubriquesorie_id=categorie.categorie_id LEFT JOIN commercants ON commercants.IdCommercant=annonce.annonce_commercant_id LEFT JOIN villes ON villes.IdVille=commercants.IdVille" ;
        $zSqlListeAnnonce = "SELECT  *  FROM annonce LEFT JOIN commercants ON commercants.IdCommercant=annonce.annonce_commercant_id LEFT JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant LEFT JOIN rubriques ON rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique LEFT JOIN villes ON villes.IdVille=commercants.IdVille";
        $zQueryListeAnnonce = $this->db->query($zSqlListeAnnonce);
        return $zQueryListeAnnonce->result();
    }

    function GetById_annonce_etat($id_annonce = 0)
    {
        $qry = $this->db->query("
            SELECT
              annonce.annonce_id,
                 annonce.annonce_photo1 AS photo1,
                 annonce.annonce_photo2 AS photo2,
                 annonce.annonce_photo3 AS photo3,
                 annonce.annonce_photo4 AS photo4,
                 annonce.annonce_date_debut , 
                 annonce.annonce_date_fin, 
                 annonce.annonce_description_courte AS texte_courte,
                 annonce.annonce_description_longue AS texte_longue,
                 annonce.annonce_prix_neuf AS prix_vente,
                 annonce.annonce_prix_solde AS prix_ancien,
                 annonce.annonce_etat AS etat,
                 annonce.annonce_commercant_id,
                 annonce.module_paypal,
                 annonce.module_paypal_btnpaypal,
                 annonce.module_paypal_panierpaypal,
                  
                
                villes.Nom AS ville,
                 
                commercants.NomSociete AS NomSociete
                
             

            FROM
                annonce
              INNER JOIN commercants ON commercants.IdCommercant = annonce.annonce_commercant_id
            
           LEFT JOIN villes ON villes.IdVille=commercants.IdVIlle
                 
                
                
                
             
            WHERE annonce_id = '" . $id_annonce . "'
            AND (annonce_etat = '0' OR annonce_etat = '1')
            LIMIT 1
        ");
        return $qry->row();
    }

    function GetAnnonceCategorie()
    {
        $queryCat = "
           SELECT
                sous_rubriques.IdRubrique,
                sous_rubriques.Nom
                FROM
                sous_rubriques
                INNER JOIN ass_commercants_rubriques ON sous_rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique
                INNER JOIN commercants ON commercants.IdCommercant = ass_commercants_rubriques.IdCommercant
                INNER JOIN annonce ON ass_commercants_rubriques.IdCommercant = annonce.annonce_commercant_id
                where commercants.IsActif = 1";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $queryCat .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $queryCat .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $queryCat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $queryCat .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $queryCat .= " OR ";
            }
            $queryCat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $queryCat .= " AND commercants.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        $queryCat .= "
            GROUP BY
            sous_rubriques.IdRubrique
            ";

        $qryCategorie = $this->db->query($queryCat);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function getAlldeposantAnnonce($_iCommercantId = "0")
    {

        $zSqlAlldeposantAgenda = "SELECT
        commercants.NomSociete,
        commercants.IdCommercant
        FROM
        annonce
        INNER JOIN commercants ON annonce.annonce_commercant_id = commercants.IdCommercant
        WHERE
        commercants.referencement_annonce = 1
        AND commercants.IsActif = 1
                                              ";

        if ($_iCommercantId != "0" && $_iCommercantId != NULL && $_iCommercantId != "" && $_iCommercantId != 0) {
            $zSqlAlldeposantAgenda .= " AND annonce.annonce_commercant_id = '" . $_iCommercantId . "'";
        }

        $zSqlAlldeposantAgenda .= " GROUP BY
            annonce.annonce_commercant_id";

        $zQueryAlldeposantAgenda = $this->db->query($zSqlAlldeposantAgenda);
        return $zQueryAlldeposantAgenda->result();
    }

    function GetAnnonceCategorie_ByIdCommercant($_iCommercantId)
    {

        $sqlcat = "SELECT
commercants.*,
sous_rubriques.IdRubrique,
sous_rubriques.Nom
FROM
sous_rubriques
INNER JOIN ass_commercants_rubriques ON sous_rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique
INNER JOIN commercants ON commercants.IdCommercant = ass_commercants_rubriques.IdCommercant
INNER JOIN annonce ON ass_commercants_rubriques.IdCommercant = annonce.annonce_commercant_id
where annonce.annonce_commercant_id ='" . $_iCommercantId . "' 
        AND commercants.referencement_annonce= 1
        AND commercants.IsActif = 1" ;


        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcat .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcat .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcat .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
            }
            $sqlcat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcat .= " AND commercants.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        $sqlcat .= "
            GROUP BY
            sous_rubriques.IdRubrique
            ";

        $qryCategorie = $this->db->query($sqlcat);

        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function Increment_accesscount($id_annonce = 0)
    {
        $current_agenda = $this->getById($id_annonce);
        $last_accesscount_nb = intval($current_agenda->accesscount);
        $last_accesscount_nb_total = $last_accesscount_nb + 1;
        $zSql = "UPDATE annonce SET accesscount='" . $last_accesscount_nb_total . "' WHERE annonce_id=" . $id_annonce;
        $this->db->query($zSql);
    }


    function GetAnnonceSouscategorie()
    {
        $queryCat = "
            SELECT
            sous_rubriques.IdSousRubrique,
            sous_rubriques.IdRubrique,
            sous_rubriques.Nom,
            ass_commercants_sousrubriques.IdCommercant,
            COUNT(annonce.annonce_id) as nb_annonce
            FROM
            annonce
            Left Join commercants ON commercants.IdCommercant = annonce.annonce_commercant_id
            Inner Join ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
            Inner Join sous_rubriques ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique
            WHERE commercants.IsActif = '1'
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $queryCat .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $queryCat .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $queryCat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $queryCat .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $queryCat .= " OR ";
            }
            $queryCat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $queryCat .= " AND commercants.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        $queryCat .= "
            GROUP BY
            sous_rubriques.IdSousRubrique, ass_commercants_sousrubriques.IdCommercant

        ";

        $qryCategorie = $this->db->query($queryCat);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetAnnonceSouscategorieByRubrique($IdRubrique)
    {

        $qry = "
            SELECT
            sous_rubriques.IdSousRubrique,
            sous_rubriques.IdRubrique,
            sous_rubriques.Nom,
            COUNT(annonce.annonce_id) as nb_annonce
            FROM
            annonce
            Left Join commercants ON commercants.IdCommercant = annonce.annonce_commercant_id
            Inner Join ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
            Inner Join sous_rubriques ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique
            WHERE 0=0 AND commercants.IsActif = 1  AND commercants.referencement_annonce = '1'  ";


        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $qry .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $qry .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $qry .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $qry .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $qry .= " OR ";
            }
            $qry .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $qry .= " AND commercants.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        if ($IdRubrique != "0" && $IdRubrique != "" && $IdRubrique != null) $qry .= " AND sous_rubriques.IdRubrique = '" . $IdRubrique . "' ";
        $qry .= "
            GROUP BY
            sous_rubriques.IdSousRubrique

        ";
        //log_message('error', 'CHECKING_BY_SUB_CATEG_NUMBER_ANNONCE : ' . $qry);

        $qryCategorie = $this->db->query($qry);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetAnnonceCategoriePrincipale()
    {

        $sqlCategorie = "
            SELECT
                rubriques.Nom,
                rubriques.IdRubrique,
                COUNT(annonce.annonce_id) as nb_annonce
                FROM
                annonce
                INNER JOIN ass_commercants_rubriques ON annonce.annonce_commercant_id = ass_commercants_rubriques.IdCommercant
                INNER JOIN rubriques ON rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique
                INNER JOIN commercants ON commercants.IdCommercant = ass_commercants_rubriques.IdCommercant
                WHERE commercants.IsActif = '1'  AND commercants.referencement_annonce = '1' 
                ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlCategorie .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlCategorie .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlCategorie .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlCategorie .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlCategorie .= " OR ";
            }
            $sqlCategorie .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlCategorie .= " AND commercants.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $sqlCategorie .= "
                GROUP BY
                rubriques.IdRubrique
                ORDER BY rubriques.Nom
            ";

        $qryCategorie = $this->db->query($sqlCategorie);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetAnnonceCategoriePrincipaleByVille($_iIdDepartement, $IdVille)
    {

        $qry = "
            SELECT
            rubriques.Nom,
            rubriques.IdRubrique,
            COUNT(annonce.annonce_id) as nb_annonce
            FROM
            annonce
            INNER JOIN ass_commercants_rubriques ON annonce.annonce_commercant_id = ass_commercants_rubriques.IdCommercant
            INNER JOIN rubriques ON rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique
            INNER JOIN commercants ON commercants.IdCommercant = ass_commercants_rubriques.IdCommercant
            WHERE
            commercants.IsActif = 1  AND commercants.referencement_annonce = '1'
            ";


        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $qry .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $qry .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $qry .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $qry .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $qry .= " OR ";
            }
            $qry .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $qry .= " AND commercants.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        //if ($IdVille!="0" && $IdVille!="" && $IdVille!=null) $qry .= "AND  commercants.IdVille = '".$IdVille."' ";
        if ($IdVille != "0" && $IdVille != "" && $IdVille != null) {
            $qry .= " AND commercants.IdVille = '" . $IdVille . "' ";
        } elseif ($_iIdDepartement != "0" && $_iIdDepartement != "" && $_iIdDepartement != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iIdDepartement);
            if (count($villeByIdDepartement) > 0) {
                $qry .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $qry .= " commercants.IdVille=" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $qry .= " OR ";
                    }
                    $iii_villedep++;
                }
                $qry .= ")";
            }
        }


        $qry .= "
            GROUP BY
            rubriques.IdRubrique
            ORDER BY rubriques.Nom
        ";

        $qryCategorie = $this->db->query($qry);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function effacer_annonce_photo($prmIdAnnonce, $prmFiles)
    {
        $zSqleffacer_annonce_photo = "UPDATE annonce SET `" . $prmFiles . "`='' WHERE annonce_id=" . $prmIdAnnonce;
        //var_dump($zSqleffacer_annonce_photo); die();
        $this->db->query($zSqleffacer_annonce_photo);
    }


    function save_order_partner($idCommercant, $annonce_id, $inputOrderPartner)
    {
        $zSql = "UPDATE annonce SET `order_partner`='" . $inputOrderPartner . "' WHERE annonce_id='" . $annonce_id . "' AND annonce_commercant_id='" . $idCommercant . "';";
        //var_dump($zSqleffacer_annonce_photo); die();
        $this->db->query($zSql);
    }


    function file_manager_update($_iArticleId = '0', $_iUserId = '0', $_iField = 'annonce_photo1', $_iValue = '')
    {
        $zSql = "UPDATE annonce SET `" . $_iField . "` = '" . $_iValue . "' WHERE annonce_id = " . $_iArticleId . " AND `annonce_commercant_id` = '" . $_iUserId . "' ";
        $zQuery = $this->db->query($zSql);
    }


    function listeAgendaRecherche_filtre_agenda_perso($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0", $referencement_annonce = 1,$etat=0,$ville)
    {

        $zSqlListePartenaire = "
            SELECT
                 annonce.*,
                 sous_rubriques.Nom AS rubrique,
                 sous_rubriques.IdSousRubrique,
                 sous_rubriques.IdRubrique,
                 villes.Nom AS ville,
                 commercants.NomSociete AS NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                annonce
             
                INNER JOIN commercants ON annonce.annonce_commercant_id = commercants.IdCommercant
                INNER JOIN ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
                INNER JOIN sous_rubriques ON ass_commercants_sousrubriques.IdSousRubrique = sous_rubriques.IdSousRubrique
                INNER JOIN villes ON commercants.IdVille = villes.IdVille            
             WHERE
                commercants.IsActif = 1
                AND commercants.referencement_annonce = 1     
                ";
        if ($referencement_annonce == 1)
        $zSqlListePartenaire .= " AND commercants.referencement_annonce = " . $referencement_annonce . " ";

        $zSqlListePartenaire .= "
                AND (annonce.annonce_id <> '0' OR annonce.annonce_id <> NULL OR annonce.annonce_id <> 0) 

        ";
        if ($etat != 'all' && $etat !=""){
        $zSqlListePartenaire .= "
                AND (annonce.annonce_etat = ".$etat.") 
        ";
        }
        if (is_array($inputIdCommercant)) {
            $zSql_all_icommercant_request = "";
            $zSql_all_icommercant_request .= " (";
            if (sizeof($inputIdCommercant) == 1) {
                if ($inputIdCommercant[0] != "0" && $inputIdCommercant[0] != "" && $inputIdCommercant[0] != NULL && $inputIdCommercant[0] != 0) {
                    $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                } else {
                    $zSql_all_icommercant_request .= " 0=0 ";
                }
            } else {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                for ($i_iCommercantId = 1; $i_iCommercantId < count($inputIdCommercant); $i_iCommercantId++) {
                    $zSql_all_icommercant_request .= " OR commercants.IdCommercant = '" . $inputIdCommercant[$i_iCommercantId] . "' ";
                }
            }
            $zSql_all_icommercant_request .= " ) ";
            //$zSqlListePartenaire .= $zSql_all_icommercant_request;
        } else if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }


         if ($inputQuand != "0") {
             $date_of_day = date("Y-m-d");
             $time_date = strtotime($date_of_day);
             $next_sunday = strtotime('next sunday, 12pm', $time_date);
             $last_sunday = strtotime('last sunday, 12pm', $time_date);
             $next_saturday = strtotime('next saturday, 11:59am', $time_date);
             $next_monday = strtotime('next monday, 11:59am', $time_date);
             $format_date = 'Y-m-d';
             $next_sunday_day = date($format_date, $next_sunday);
             $last_sunday_day = date($format_date, $last_sunday);
             $next_saturday_day = date($format_date, $next_saturday);
             $next_monday_day = date($format_date, $next_monday);
             $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
             $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
             $first_day_month = date('Y-m-01', strtotime($date_of_day));
             $last_day_month = date('Y-m-t', strtotime($date_of_day));
             $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
             $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);


             if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $date_of_day . "' AND annonce.annonce_date_fin <= '" . $date_of_day . "' ) OR ( annonce.annonce_date_debut IS NULL AND annonce.annonce_date_fin >= '" . $date_of_day . "' ) )";
             if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $next_saturday_day . "' AND annonce.annonce_date_fin <= '" . $next_sunday_day . "') OR (annonce.annonce_date_fin >= '" . $next_saturday_day . "' AND annonce.annonce_date_fin <= '" . $next_sunday_day . "') )";
             if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $last_sunday_day . "' AND annonce.annonce_date_fin <= '" . $next_saturday_day . "') OR (annonce.annonce_date_fin >= '" . $last_sunday_day . "' AND annonce.annonce_date_fin <= '" . $next_saturday_day . "') )";
             if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $last_sunday_of_next_monday . "' AND annonce.annonce_date_fin <= '" . $next_saturday_of_next_monday . "') OR (annonce.annonce_date_fin >= '" . $last_sunday_of_next_monday . "' AND annonce.annonce_date_fin <= '" . $next_saturday_of_next_monday . "') )";
             if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_month . "' AND annonce.annonce_date_fin <= '" . $last_day_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_month . "' AND annonce.annonce_date_fin <= '" . $last_day_month . "' AND annonce.annonce_date_fin >= '" . $date_of_day . "') )";


             if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
             if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
             if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
             if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
             if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
             if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
             if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
             if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
             if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
             if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
             if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
             if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";


         }

          if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null && $_iVilleId != "0") {
              if (is_array($_iVilleId)) {
                  $zSql_all_iville_request = "";
                  $zSql_all_iville_request .= " (";
                  if (sizeof($_iVilleId) == 1) {
                      if ($_iVilleId[0] != 0 && $_iVilleId[0] != "" && $_iVilleId[0] != null && $_iVilleId[0] != "0") {
                          $zSql_all_iville_request .= " commercants.IdVille = '" . $_iVilleId[0] . "' ";
                      } else {
                          $zSql_all_iville_request .= " 0=0 ";
                      }
                  } else {
                      $zSql_all_iville_request .= " commercants.IdVille = '" . $_iVilleId[0] . "' ";
                      for ($i_iVilleId = 1; $i_iVilleId < count($_iVilleId); $i_iVilleId++) {
                          $zSql_all_iville_request .= " OR commercants.IdVille = '" . $_iVilleId[$i_iVilleId] . "' ";
                      }
                  }
                  $zSql_all_iville_request .= " ) ";
                  ////$this->firephp->log($zSql_all_iville_request, 'zSql_all_iville_request');
                  //$zSqlListePartenaire .= $zSql_all_iville_request;
              }
          }
        elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " commercants.IdVille =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            $zSqlListePartenaire .= " AND annonce.annonce_date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            $zSqlListePartenaire .= " AND annonce.annonce_date_fin = '" . $inputDatefin . "'";
        }
        if ($ville != "0" ) {
            $zSqlListePartenaire .= " AND villes.IdVille = " . $ville;
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " sous_rubriques.IdRubrique = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " sous_rubriques.IdRubrique = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR sous_rubriques.IdRubrique = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }


        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " sous_rubriques.IdSousRubrique = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " sous_rubriques.IdSousRubrique = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR sous_rubriques.IdSousRubrique = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }





        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);
        //var_dump($SearchedWordsString);
        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(annonce.annonce_description_courte ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(annonce.annonce_description_longue ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }




        //$zSqlListePartenaire .= " ) ";

         if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") || (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) {
             $zSqlListePartenaire .= " AND ( ";

             if (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") $zSqlListePartenaire .= $zSql_all_icommercant_request;
             if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") && (isset($zSql_all_iville_request) && $zSql_all_iville_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_iville_request;
             else if (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") $zSqlListePartenaire .= $zSql_all_iville_request;
             if (((isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "")) && (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
             else if (isset($zSql_all_categ_request) && $zSql_all_categ_request != "") $zSqlListePartenaire .= $zSql_all_categ_request;

             $zSqlListePartenaire .= " ) ";
         }

        $zSqlListePartenaire .= " group by annonce.annonce_id ";


        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $_limitend;
        //echo $zSqlListePartenaire;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListePartenaire');
        //die("<br/>STOP");

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }

    function listeAgendaRecherche_agenda_perso_liste_categ($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0", $referencement_annonce = 1)
        {

            $zSqlListePartenaire = "
            SELECT
                 COUNT(annonce.annonce_id) as nb_annonce,
                 annonce.*,
                 sous_rubriques.Nom AS rubrique,
                 sous_rubriques.IdSousRubrique,
                 sous_rubriques.IdRubrique,
                 villes.Nom AS ville,
                 commercants.NomSociete AS NomSociete,
                 rubriques.Nom,
                 rubriques.IdRubrique
                ";

            $zSqlListePartenaire .= "
            FROM
                annonce
                INNER JOIN commercants ON annonce.annonce_commercant_id = commercants.IdCommercant
                INNER JOIN ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
                INNER JOIN sous_rubriques ON ass_commercants_sousrubriques.IdSousRubrique = sous_rubriques.IdSousRubrique
                INNER JOIN ass_commercants_rubriques ON commercants.IdCommercant = ass_commercants_rubriques.IdCommercant
                INNER JOIN rubriques ON ass_commercants_rubriques.IdRubrique = rubriques.IdRubrique
                INNER JOIN villes ON commercants.IdVille = villes.IdVille            
             WHERE
                commercants.IsActif = 1
                AND commercants.referencement_annonce = 1 
                
            
            
                ";
            if ($referencement_annonce == 1)
                $zSqlListePartenaire .= " AND commercants.referencement_annonce = " . $referencement_annonce . " ";

            $zSqlListePartenaire .= "
                AND (annonce.annonce_id <> '0' OR annonce.annonce_id <> NULL OR annonce.annonce_id <> 0) 

        ";

            if (is_array($inputIdCommercant)) {
                $zSql_all_icommercant_request = "";
                $zSql_all_icommercant_request .= " (";
                if (sizeof($inputIdCommercant) == 1) {
                    $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                } else {
                    $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                    for ($i_iCommercantId = 1; $i_iCommercantId < count($inputIdCommercant); $i_iCommercantId++) {
                        $zSql_all_icommercant_request .= " OR commercants.IdCommercant = '" . $inputIdCommercant[$i_iCommercantId] . "' ";
                    }
                }
                $zSql_all_icommercant_request .= " ) ";

                //$zSqlListePartenaire .= $zSql_all_icommercant_request;
            } else if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
                $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
            }

            if ($IdUsers_ionauth != "0" && $IdUsers_ionauth != "" && $IdUsers_ionauth != NULL && $IdUsers_ionauth != 0) {
                $zSqlListePartenaire .= " AND annonce.IdUsers_ionauth = '" . $IdUsers_ionauth . "'";
            }

            if ($inputQuand != "0") {
                $date_of_day = date("Y-m-d");
                $time_date = strtotime($date_of_day);
                $next_sunday = strtotime('next sunday, 12pm', $time_date);
                $last_sunday = strtotime('last sunday, 12pm', $time_date);
                $next_saturday = strtotime('next saturday, 11:59am', $time_date);
                $next_monday = strtotime('next monday, 11:59am', $time_date);
                $format_date = 'Y-m-d';
                $next_sunday_day = date($format_date, $next_sunday);
                $last_sunday_day = date($format_date, $last_sunday);
                $next_saturday_day = date($format_date, $next_saturday);
                $next_monday_day = date($format_date, $next_monday);
                $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
                $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
                $first_day_month = date('Y-m-01', strtotime($date_of_day));
                $last_day_month = date('Y-m-t', strtotime($date_of_day));
                $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
                $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);


                if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $date_of_day . "' AND annonce.annonce_date_fin <= '" . $date_of_day . "' ) OR ( annonce.annonce_date_debut IS NULL AND annonce.annonce_date_fin >= '" . $date_of_day . "' ) )";
                if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $next_saturday_day . "' AND annonce.annonce_date_fin <= '" . $next_sunday_day . "') OR (annonce.annonce_date_fin >= '" . $next_saturday_day . "' AND annonce.annonce_date_fin <= '" . $next_sunday_day . "') )";
                if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $last_sunday_day . "' AND annonce.annonce_date_fin <= '" . $next_saturday_day . "') OR (annonce.annonce_date_fin >= '" . $last_sunday_day . "' AND annonce.annonce_date_fin <= '" . $next_saturday_day . "') )";
                if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $last_sunday_of_next_monday . "' AND annonce.annonce_date_fin <= '" . $next_saturday_of_next_monday . "') OR (annonce.annonce_date_fin >= '" . $last_sunday_of_next_monday . "' AND annonce.annonce_date_fin <= '" . $next_saturday_of_next_monday . "') )";
                if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_month . "' AND annonce.annonce_date_fin <= '" . $last_day_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_month . "' AND annonce.annonce_date_fin <= '" . $last_day_month . "' AND annonce.annonce_date_fin >= '" . $date_of_day . "') )";

                if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
                if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
                if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
                if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
                if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
                if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
                if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
                if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
                if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
                if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
                if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
                if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";

            }

            if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null && $_iVilleId != "0") {
                if (is_array($_iVilleId)) {
                    $zSql_all_iville_request = "";
                    $zSql_all_iville_request .= " (";
                    if (sizeof($_iVilleId) == 1) {
                        if ($_iVilleId[0] != 0 && $_iVilleId[0] != "" && $_iVilleId[0] != null && $_iVilleId[0] != "0") {
                            $zSql_all_iville_request .= " commercants.IdVille = '" . $_iVilleId[0] . "' ";
                        } else {
                            $zSql_all_iville_request .= " 0=0 ";
                        }
                    } else {
                        $zSql_all_iville_request .= " commercants.IdVille = '" . $_iVilleId[0] . "' ";
                        for ($i_iVilleId = 1; $i_iVilleId < count($_iVilleId); $i_iVilleId++) {
                            $zSql_all_iville_request .= " OR commercants.IdVille = '" . $_iVilleId[$i_iVilleId] . "' ";
                        }
                    }
                    $zSql_all_iville_request .= " ) ";
                    ////$this->firephp->log($zSql_all_iville_request, 'zSql_all_iville_request');
                    //$zSqlListePartenaire .= $zSql_all_iville_request;
                }
            } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
                # code...
                model_load_model("mdldepartement");
                $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
                if (count($villeByIdDepartement) > 0) {
                    $zSqlListePartenaire .= " AND (";
                    $iii_villedep = 1;
                    foreach ($villeByIdDepartement as $key_villeDep) {
                        # code...
                        $zSqlListePartenaire .= " commercants.IdVille =" . $key_villeDep->IdVille;
                        if ($iii_villedep != count($villeByIdDepartement)) {
                            $zSqlListePartenaire .= " OR ";
                        }
                        $iii_villedep++;
                    }
                    $zSqlListePartenaire .= ")";
                }
            }


            if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
                $zSqlListePartenaire .= " AND annonce.annonce_date_debut = '" . $inputDatedebut . "'";
            }

            if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
                $zSqlListePartenaire .= " AND annonce.annonce_date_fin = '" . $inputDatefin . "'";
            }


            $zSql_all_categ_request = "";
            $zSql_all_subcateg_request = "";
            if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
                $zSql_all_categ_request .= " (";
                if (sizeof($_iCategorieId) == 1) {
                    $zSql_all_categ_request .= " rubriques.IdRubrique = '" . $_iCategorieId[0] . "' ";
                } else {
                    $zSql_all_categ_request .= " rubriques.IdRubrique = '" . $_iCategorieId[0] . "' ";
                    for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                        $zSql_all_categ_request .= " OR rubriques.IdRubrique = '" . $_iCategorieId[$ii_rand] . "' ";
                    }
                }
                $zSql_all_categ_request .= " ) ";
            }
            if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
                $zSql_all_subcateg_request .= " (";
                if (sizeof($_iSousCategorieId) == 1) {
                    $zSql_all_subcateg_request .= " sous_rubriques.IdSousRubrique = '" . $_iSousCategorieId[0] . "' ";
                } else {
                    $zSql_all_subcateg_request .= " sous_rubriques.IdSousRubrique = '" . $_iSousCategorieId[0] . "' ";
                    for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                        $zSql_all_subcateg_request .= " OR sous_rubriques.IdSousRubrique = '" . $_iSousCategorieId[$ii_rand] . "' ";
                    }
                }
                $zSql_all_subcateg_request .= " ) ";
            }







            $SearchedWordsString = str_replace("+", " ", $_zMotCle);
            $SearchedWordsString = trim($SearchedWordsString);
            $SearchedWords = explode(" ", $SearchedWordsString);

            //$SearchedWordsString = trim($_zMotCle);
            //$SearchedWords = explode(" ", $SearchedWordsString) ;

            for ($i = 0; $i < sizeof($SearchedWords); $i++) {
                $Search = $SearchedWords[$i];
                $Search = str_replace(" ", "", $Search);
                if ($Search != "") {
                    if ($i == 0) {
                        $zSqlListePartenaire .= "\n AND ";
                    }
                    $zSqlListePartenaire .= " ( \n" .
                        " UPPER(commercant.nom_societe ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(annonce.annonce_etat ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(annonce.annonce_description_courte ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(annonce.annonce_prix_solde ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " OR UPPER(agenda_subcateg.subcateg ) LIKE '%" . strtoupper($Search) . "%'\n" .
                        " )
                    ";
                    if ($i != (sizeof($SearchedWords) - 1)) {
                        $zSqlListePartenaire .= "\n OR ";
                    }
                }
            }


           // $zSqlListePartenaire .= " ) ";

            if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") || (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) {
                $zSqlListePartenaire .= " AND ( ";

                if (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") $zSqlListePartenaire .= $zSql_all_icommercant_request;
                if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") && (isset($zSql_all_iville_request) && $zSql_all_iville_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_iville_request;
                else if (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") $zSqlListePartenaire .= $zSql_all_iville_request;
                if (((isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "")) && (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
                else if (isset($zSql_all_categ_request) && $zSql_all_categ_request != "") $zSqlListePartenaire .= $zSql_all_categ_request;

                $zSqlListePartenaire .= " ) ";
            }

            $zSqlListePartenaire .= " AND (commercants.IsActif = '1' OR commercants.IsActif = '2') ";

            $zSqlListePartenaire .= " group by rubriques.IdRubrique ";

            //if ($iOrderBy == '1') $iOrderBy_value = " order by agenda.last_update desc ";
            //else if ($iOrderBy == '2') $iOrderBy_value = " order by agenda.accesscount desc ";
            // $iOrderBy_value = " order by sous_rubriques.category asc ";

            //$zSqlListePartenaire .= $iOrderBy_value;

            $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

            //echo $_limitend;
            //echo $zSqlListePartenaire;
            ////$this->firephp->log($zSqlListePartenaire, 'zSqlListePartenaire');
            //die("<br/>STOP");

            $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
            return $zQueryListeBonPlan->result();
        }

    function GetByCommercantRefAnnonce($annonce_id=0)
    {
        $Query_txt = "
            SELECT
            annonce.*,
            commercants.NomSociete,
            sous_rubriques.Nom

            FROM
                annonce
            INNER JOIN commercants ON annonce.annonce_commercant_id = commercants.IdCommercant
            INNER JOIN ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
            INNER JOIN sous_rubriques ON ass_commercants_sousrubriques.IdSousRubrique = sous_rubriques.IdSousRubrique

            WHERE
            commercants.referencement_annonce >= 1
        "; //var_dump($Query_txt); die();
        $qry = $this->db->query($Query_txt);
        $result_main = $qry->row();
        return $result_main;
    }

    function listeAgendaRecherche_filtre_agenda_perso_check_categ($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0")
    {

        $zSqlListePartenaire = "
            SELECT
                 annonce.*,
                 sous_rubriques.Nom AS rubrique,
                 sous_rubriques.IdSousRubrique,
                 sous_rubriques.IdRubrique,
                 villes.Nom AS ville,
                 commercants.NomSociete AS NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                annonce
                INNER JOIN commercants ON annonce.annonce_commercant_id = commercants.IdCommercant
                INNER JOIN ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
                INNER JOIN sous_rubriques ON ass_commercants_sousrubriques.IdSousRubrique = sous_rubriques.IdSousRubrique
                INNER JOIN villes ON commercants.IdVille = villes.IdVille            
             WHERE
                commercants.IsActif = 1
                AND commercants.referencement_annonce = 1     
                ";

        $zSqlListePartenaire .= "
                AND (annonce.annonce_id <> '0' OR annonce.annonce_id <> NULL OR annonce.annonce_id <> 0) 

        ";

        if (is_array($inputIdCommercant)) {
            $zSql_all_icommercant_request = "";
            $zSql_all_icommercant_request .= " (";
            if (sizeof($inputIdCommercant) == 1) {
                if ($inputIdCommercant[0] != "0" && $inputIdCommercant[0] != "" && $inputIdCommercant[0] != NULL && $inputIdCommercant[0] != 0) {
                    $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                } else {
                    $zSql_all_icommercant_request .= " 0=0 ";
                }
            } else {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                for ($i_iCommercantId = 1; $i_iCommercantId < count($inputIdCommercant); $i_iCommercantId++) {
                    $zSql_all_icommercant_request .= " OR commercants.IdCommercant = '" . $inputIdCommercant[$i_iCommercantId] . "' ";
                }
            }
            $zSql_all_icommercant_request .= " ) ";
            //$zSqlListePartenaire .= $zSql_all_icommercant_request;
        } else if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }


        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);


            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $date_of_day . "' AND annonce.annonce_date_fin <= '" . $date_of_day . "' ) OR ( annonce.annonce_date_debut IS NULL AND annonce.annonce_date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $next_saturday_day . "' AND annonce.annonce_date_fin <= '" . $next_sunday_day . "') OR (annonce.annonce_date_fin >= '" . $next_saturday_day . "' AND annonce.annonce_date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $last_sunday_day . "' AND annonce.annonce_date_fin <= '" . $next_saturday_day . "') OR (annonce.annonce_date_fin >= '" . $last_sunday_day . "' AND annonce.annonce_date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $last_sunday_of_next_monday . "' AND annonce.annonce_date_fin <= '" . $next_saturday_of_next_monday . "') OR (annonce.annonce_date_fin >= '" . $last_sunday_of_next_monday . "' AND annonce.annonce_date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_month . "' AND annonce.annonce_date_fin <= '" . $last_day_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_month . "' AND annonce.annonce_date_fin <= '" . $last_day_month . "' AND annonce.annonce_date_fin >= '" . $date_of_day . "') )";


            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";


        }

        if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null && $_iVilleId != "0") {
            if (is_array($_iVilleId)) {
                $zSql_all_iville_request = "";
                $zSql_all_iville_request .= " (";
                if (sizeof($_iVilleId) == 1) {
                    if ($_iVilleId[0] != 0 && $_iVilleId[0] != "" && $_iVilleId[0] != null && $_iVilleId[0] != "0") {
                        $zSql_all_iville_request .= " commercants.IdVille = '" . $_iVilleId[0] . "' ";
                    } else {
                        $zSql_all_iville_request .= " 0=0 ";
                    }
                } else {
                    $zSql_all_iville_request .= " commercants.IdVille = '" . $_iVilleId[0] . "' ";
                    for ($i_iVilleId = 1; $i_iVilleId < count($_iVilleId); $i_iVilleId++) {
                        $zSql_all_iville_request .= " OR commercants.IdVille = '" . $_iVilleId[$i_iVilleId] . "' ";
                    }
                }
                $zSql_all_iville_request .= " ) ";
                ////$this->firephp->log($zSql_all_iville_request, 'zSql_all_iville_request');
                //$zSqlListePartenaire .= $zSql_all_iville_request;
            }
        }
        elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " commercants.IdVille =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            $zSqlListePartenaire .= " AND annonce.annonce_date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            $zSqlListePartenaire .= " AND annonce.annonce_date_fin = '" . $inputDatefin . "'";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " rubriques.IdRubrique = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " rubriques.IdRubrique = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR rubriques.IdRubrique = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }

        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " sous_rubriques.IdSousRubrique = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " sous_rubriques.IdSousRubrique = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR sous_rubriques.IdSousRubrique = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }





        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);

        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(commercant.nom_societe ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(annonce.annonce_etat ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(annonce.annonce_description_courte ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(annonce.annonce_prix_solde ) LIKE '%" . strtoupper($Search) . "%'\n" .

                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }


        //$zSqlListePartenaire .= " ) ";

        if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") || (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) {
            $zSqlListePartenaire .= " AND ( ";

            if (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") $zSqlListePartenaire .= $zSql_all_icommercant_request;
            if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") && (isset($zSql_all_iville_request) && $zSql_all_iville_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_iville_request;
            else if (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") $zSqlListePartenaire .= $zSql_all_iville_request;
            if (((isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "")) && (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
            else if (isset($zSql_all_categ_request) && $zSql_all_categ_request != "") $zSqlListePartenaire .= $zSql_all_categ_request;

            $zSqlListePartenaire .= " ) ";
        }

        //$zSqlListePartenaire .= " AND (commercant.IsActif = '1' OR commercant.IsActif = '2') ";

        $zSqlListePartenaire .= " group by annonce.annonce_id ";



        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $_limitend;
        //echo $zSqlListePartenaire;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListePartenaire');
        //die("<br/>STOP");

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }


    public function get_annonce_by_id($id){

        $this->db->where('id',$id);
        $this->db->where('is_activ',1);
        $res= $this->db->get('card_capital');
        if ($res->num_rows()>0){return $res->row();}

        else{
            $this->db->where('id',$id);
            $this->db->where('is_activ',1);
            $res= $this->db->get('card_remise');
            if ($res->num_rows()>0){return $res->row();}

            else{
                $this->db->where('id',$id);
                $this->db->where('is_activ',1);
                $res= $this->db->get('card_tampon');
                return $res->row();
            }
        }
    }
    function listeAgendaRecherche_filtre_agenda_perso_occasion($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0", $referencement_annonce = 1,$ville)
    {

        $zSqlListePartenaire = "
            SELECT
                 annonce.*,
                 sous_rubriques.Nom AS rubrique,
                 sous_rubriques.IdSousRubrique,
                 sous_rubriques.IdRubrique,
                 villes.Nom AS ville,
                 commercants.NomSociete AS NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                annonce
             
                INNER JOIN commercants ON annonce.annonce_commercant_id = commercants.IdCommercant
                INNER JOIN ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
                INNER JOIN sous_rubriques ON ass_commercants_sousrubriques.IdSousRubrique = sous_rubriques.IdSousRubrique
                INNER JOIN villes ON commercants.IdVille = villes.IdVille            
             WHERE
                commercants.IsActif = 1
                AND commercants.referencement_annonce = 1     
                ";
        if ($referencement_annonce == 1)
            $zSqlListePartenaire .= " AND commercants.referencement_annonce = " . $referencement_annonce . " ";

        $zSqlListePartenaire .= "
                AND (annonce.annonce_id <> '0' OR annonce.annonce_id <> NULL OR annonce.annonce_id <> 0) 

        ";

            $zSqlListePartenaire .= "
                AND (annonce.annonce_etat =0) 
        ";

        if (is_array($inputIdCommercant)) {
            $zSql_all_icommercant_request = "";
            $zSql_all_icommercant_request .= " (";
            if (sizeof($inputIdCommercant) == 1) {
                if ($inputIdCommercant[0] != "0" && $inputIdCommercant[0] != "" && $inputIdCommercant[0] != NULL && $inputIdCommercant[0] != 0) {
                    $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                } else {
                    $zSql_all_icommercant_request .= " 0=0 ";
                }
            } else {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                for ($i_iCommercantId = 1; $i_iCommercantId < count($inputIdCommercant); $i_iCommercantId++) {
                    $zSql_all_icommercant_request .= " OR commercants.IdCommercant = '" . $inputIdCommercant[$i_iCommercantId] . "' ";
                }
            }
            $zSql_all_icommercant_request .= " ) ";
            //$zSqlListePartenaire .= $zSql_all_icommercant_request;
        } else if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }


        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);


            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $date_of_day . "' AND annonce.annonce_date_fin <= '" . $date_of_day . "' ) OR ( annonce.annonce_date_debut IS NULL AND annonce.annonce_date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $next_saturday_day . "' AND annonce.annonce_date_fin <= '" . $next_sunday_day . "') OR (annonce.annonce_date_fin >= '" . $next_saturday_day . "' AND annonce.annonce_date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $last_sunday_day . "' AND annonce.annonce_date_fin <= '" . $next_saturday_day . "') OR (annonce.annonce_date_fin >= '" . $last_sunday_day . "' AND annonce.annonce_date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $last_sunday_of_next_monday . "' AND annonce.annonce_date_fin <= '" . $next_saturday_of_next_monday . "') OR (annonce.annonce_date_fin >= '" . $last_sunday_of_next_monday . "' AND annonce.annonce_date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_month . "' AND annonce.annonce_date_fin <= '" . $last_day_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_month . "' AND annonce.annonce_date_fin <= '" . $last_day_month . "' AND annonce.annonce_date_fin >= '" . $date_of_day . "') )";


            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";


        }

        if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null && $_iVilleId != "0") {
            if (is_array($_iVilleId)) {
                $zSql_all_iville_request = "";
                $zSql_all_iville_request .= " (";
                if (sizeof($_iVilleId) == 1) {
                    if ($_iVilleId[0] != 0 && $_iVilleId[0] != "" && $_iVilleId[0] != null && $_iVilleId[0] != "0") {
                        $zSql_all_iville_request .= " commercants.IdVille = '" . $_iVilleId[0] . "' ";
                    } else {
                        $zSql_all_iville_request .= " 0=0 ";
                    }
                } else {
                    $zSql_all_iville_request .= " commercants.IdVille = '" . $_iVilleId[0] . "' ";
                    for ($i_iVilleId = 1; $i_iVilleId < count($_iVilleId); $i_iVilleId++) {
                        $zSql_all_iville_request .= " OR commercants.IdVille = '" . $_iVilleId[$i_iVilleId] . "' ";
                    }
                }
                $zSql_all_iville_request .= " ) ";
                ////$this->firephp->log($zSql_all_iville_request, 'zSql_all_iville_request');
                //$zSqlListePartenaire .= $zSql_all_iville_request;
            }
        }
        elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " commercants.IdVille =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            $zSqlListePartenaire .= " AND annonce.annonce_date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            $zSqlListePartenaire .= " AND annonce.annonce_date_fin = '" . $inputDatefin . "'";
        }
        if ($ville != "0" ) {
            $zSqlListePartenaire .= " AND villes.IdVille = '" . $ville . "'";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " sous_rubriques.IdRubrique = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " sous_rubriques.IdRubrique = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR sous_rubriques.IdRubrique = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }


        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " sous_rubriques.IdSousRubrique = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " sous_rubriques.IdSousRubrique = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR sous_rubriques.IdSousRubrique = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }





        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);
        //var_dump($SearchedWordsString);
        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(annonce.annonce_description_courte ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(annonce.annonce_description_longue ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }




        //$zSqlListePartenaire .= " ) ";

        if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") || (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) {
            $zSqlListePartenaire .= " AND ( ";

            if (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") $zSqlListePartenaire .= $zSql_all_icommercant_request;
            if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") && (isset($zSql_all_iville_request) && $zSql_all_iville_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_iville_request;
            else if (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") $zSqlListePartenaire .= $zSql_all_iville_request;
            if (((isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "")) && (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
            else if (isset($zSql_all_categ_request) && $zSql_all_categ_request != "") $zSqlListePartenaire .= $zSql_all_categ_request;

            $zSqlListePartenaire .= " ) ";
        }

        $zSqlListePartenaire .= " group by annonce.annonce_id ";


        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $_limitend;
        //echo $zSqlListePartenaire;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListePartenaire');
        //die("<br/>STOP");

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }
    function listeAgendaRecherche_ville_agenda_perso($_iCategorieId = 0, $_iVilleId = 0, $_iDepartementId = 0, $_zMotCle = "", $_iSousCategorieId = 0, $_limitstart = 0, $_limitend = 10000, $iOrderBy = "", $inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $inputIdCommercant = "0", $IdUsers_ionauth = "0", $referencement_annonce = 1,$etat,$ville)
    {

        $zSqlListePartenaire = "
            SELECT
                 annonce.*,
                 sous_rubriques.Nom AS rubrique,
                 sous_rubriques.IdSousRubrique,
                 sous_rubriques.IdRubrique,
                 villes.Nom AS ville,
                 villes.IdVille,
                 COUNT(villes.IdVille) as nbannonce,
                 commercants.NomSociete AS NomSociete
                ";

        $zSqlListePartenaire .= "
            FROM
                annonce
             
                INNER JOIN commercants ON annonce.annonce_commercant_id = commercants.IdCommercant
                INNER JOIN ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
                INNER JOIN sous_rubriques ON ass_commercants_sousrubriques.IdSousRubrique = sous_rubriques.IdSousRubrique
                INNER JOIN villes ON commercants.IdVille = villes.IdVille            
             WHERE
                commercants.IsActif = 1
                AND commercants.referencement_annonce = 1     
                ";
        if ($referencement_annonce == 1)
            $zSqlListePartenaire .= " AND commercants.referencement_annonce = " . $referencement_annonce . " ";

        $zSqlListePartenaire .= "
                AND (annonce.annonce_id <> '0' OR annonce.annonce_id <> NULL OR annonce.annonce_id <> 0) 

        ";
        if ($etat != 'all' && $etat !=""){
            $zSqlListePartenaire .= "
                AND (annonce.annonce_etat = ".$etat.") 
        ";
        }
        if (is_array($inputIdCommercant)) {
            $zSql_all_icommercant_request = "";
            $zSql_all_icommercant_request .= " (";
            if (sizeof($inputIdCommercant) == 1) {
                if ($inputIdCommercant[0] != "0" && $inputIdCommercant[0] != "" && $inputIdCommercant[0] != NULL && $inputIdCommercant[0] != 0) {
                    $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                } else {
                    $zSql_all_icommercant_request .= " 0=0 ";
                }
            } else {
                $zSql_all_icommercant_request .= " commercants.IdCommercant = '" . $inputIdCommercant[0] . "' ";
                for ($i_iCommercantId = 1; $i_iCommercantId < count($inputIdCommercant); $i_iCommercantId++) {
                    $zSql_all_icommercant_request .= " OR commercants.IdCommercant = '" . $inputIdCommercant[$i_iCommercantId] . "' ";
                }
            }
            $zSql_all_icommercant_request .= " ) ";
            //$zSqlListePartenaire .= $zSql_all_icommercant_request;
        } else if ($inputIdCommercant != "0" && $inputIdCommercant != "" && $inputIdCommercant != NULL && $inputIdCommercant != 0) {
            $zSqlListePartenaire .= " AND commercants.IdCommercant = '" . $inputIdCommercant . "'";
        }


        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);


            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $date_of_day . "' AND annonce.annonce_date_fin <= '" . $date_of_day . "' ) OR ( annonce.annonce_date_debut IS NULL AND annonce.annonce_date_fin >= '" . $date_of_day . "' ) )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $next_saturday_day . "' AND annonce.annonce_date_fin <= '" . $next_sunday_day . "') OR (annonce.annonce_date_fin >= '" . $next_saturday_day . "' AND annonce.annonce_date_fin <= '" . $next_sunday_day . "') )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $last_sunday_day . "' AND annonce.annonce_date_fin <= '" . $next_saturday_day . "') OR (annonce.annonce_date_fin >= '" . $last_sunday_day . "' AND annonce.annonce_date_fin <= '" . $next_saturday_day . "') )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $last_sunday_of_next_monday . "' AND annonce.annonce_date_fin <= '" . $next_saturday_of_next_monday . "') OR (annonce.annonce_date_fin >= '" . $last_sunday_of_next_monday . "' AND annonce.annonce_date_fin <= '" . $next_saturday_of_next_monday . "') )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_month . "' AND annonce.annonce_date_fin <= '" . $last_day_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_month . "' AND annonce.annonce_date_fin <= '" . $last_day_month . "' AND annonce.annonce_date_fin >= '" . $date_of_day . "') )";


            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( (annonce.annonce_date_debut >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') OR (annonce.annonce_date_fin >= '" . $first_day_user_month . "' AND annonce.annonce_date_fin <= '" . $last_day_user_month . "') )";


        }

        if ($_iVilleId != 0 && $_iVilleId != "" && $_iVilleId != null && $_iVilleId != "0") {
            if (is_array($_iVilleId)) {
                $zSql_all_iville_request = "";
                $zSql_all_iville_request .= " (";
                if (sizeof($_iVilleId) == 1) {
                    if ($_iVilleId[0] != 0 && $_iVilleId[0] != "" && $_iVilleId[0] != null && $_iVilleId[0] != "0") {
                        $zSql_all_iville_request .= " commercants.IdVille = '" . $_iVilleId[0] . "' ";
                    } else {
                        $zSql_all_iville_request .= " 0=0 ";
                    }
                } else {
                    $zSql_all_iville_request .= " commercants.IdVille = '" . $_iVilleId[0] . "' ";
                    for ($i_iVilleId = 1; $i_iVilleId < count($_iVilleId); $i_iVilleId++) {
                        $zSql_all_iville_request .= " OR commercants.IdVille = '" . $_iVilleId[$i_iVilleId] . "' ";
                    }
                }
                $zSql_all_iville_request .= " ) ";
                ////$this->firephp->log($zSql_all_iville_request, 'zSql_all_iville_request');
                //$zSqlListePartenaire .= $zSql_all_iville_request;
            }
        }
        elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " commercants.IdVille =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null) {
            $zSqlListePartenaire .= " AND annonce.annonce_date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null) {
            $zSqlListePartenaire .= " AND annonce.annonce_date_fin = '" . $inputDatefin . "'";
        }

        $zSql_all_categ_request = "";
        $zSql_all_subcateg_request = "";

        if (isset($_iCategorieId) && $_iCategorieId[0] != NULL && $_iCategorieId[0] != "" && $_iCategorieId[0] != 0) {
            $zSql_all_categ_request .= " (";
            if (sizeof($_iCategorieId) == 1) {
                $zSql_all_categ_request .= " sous_rubriques.IdRubrique = '" . $_iCategorieId[0] . "' ";
            } else {
                $zSql_all_categ_request .= " sous_rubriques.IdRubrique = '" . $_iCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iCategorieId); $ii_rand++) {
                    $zSql_all_categ_request .= " OR sous_rubriques.IdRubrique = '" . $_iCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_categ_request .= " ) ";
        }


        if (isset($_iSousCategorieId) && $_iSousCategorieId[0] != NULL && $_iSousCategorieId[0] != "" && $_iSousCategorieId[0] != 0) {
            $zSql_all_subcateg_request .= " (";
            if (sizeof($_iSousCategorieId) == 1) {
                $zSql_all_subcateg_request .= " sous_rubriques.IdSousRubrique = '" . $_iSousCategorieId[0] . "' ";
            } else {
                $zSql_all_subcateg_request .= " sous_rubriques.IdSousRubrique = '" . $_iSousCategorieId[0] . "' ";
                for ($ii_rand = 1; $ii_rand < sizeof($_iSousCategorieId); $ii_rand++) {
                    $zSql_all_subcateg_request .= " OR sous_rubriques.IdSousRubrique = '" . $_iSousCategorieId[$ii_rand] . "' ";
                }
            }
            $zSql_all_subcateg_request .= " ) ";
        }





        $SearchedWordsString = str_replace("+", " ", $_zMotCle);
        $SearchedWordsString = trim($SearchedWordsString);
        $SearchedWords = explode(" ", $SearchedWordsString);
        //var_dump($SearchedWordsString);
        //$SearchedWordsString = trim($_zMotCle);
        //$SearchedWords = explode(" ", $SearchedWordsString) ;

        for ($i = 0; $i < sizeof($SearchedWords); $i++) {
            $Search = $SearchedWords[$i];
            $Search = str_replace(" ", "", $Search);
            if ($Search != "") {
                if ($i == 0) {
                    $zSqlListePartenaire .= "\n AND ";
                }
                $zSqlListePartenaire .= " ( \n" .
                    " UPPER(annonce.annonce_description_courte ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " OR UPPER(annonce.annonce_description_longue ) LIKE '%" . strtoupper($Search) . "%'\n" .
                    " )
                    ";
                if ($i != (sizeof($SearchedWords) - 1)) {
                    $zSqlListePartenaire .= "\n OR ";
                }
            }
        }




        //$zSqlListePartenaire .= " ) ";

        if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") || (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) {
            $zSqlListePartenaire .= " AND ( ";

            if (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") $zSqlListePartenaire .= $zSql_all_icommercant_request;
            if ((isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "") && (isset($zSql_all_iville_request) && $zSql_all_iville_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_iville_request;
            else if (isset($zSql_all_iville_request) && $zSql_all_iville_request != "") $zSqlListePartenaire .= $zSql_all_iville_request;
            if (((isset($zSql_all_iville_request) && $zSql_all_iville_request != "") || (isset($zSql_all_icommercant_request) && $zSql_all_icommercant_request != "")) && (isset($zSql_all_categ_request) && $zSql_all_categ_request != "")) $zSqlListePartenaire .= " AND " . $zSql_all_categ_request;
            else if (isset($zSql_all_categ_request) && $zSql_all_categ_request != "") $zSqlListePartenaire .= $zSql_all_categ_request;

            $zSqlListePartenaire .= " ) ";
        }

        $zSqlListePartenaire .= " group by villes.IdVille ";


        $zSqlListePartenaire .= " LIMIT " . $_limitstart . "," . $_limitend;

        //echo $_limitend;
        //echo $zSqlListePartenaire;
        ////$this->firephp->log($zSqlListePartenaire, 'zSqlListePartenaire');
        //die("<br/>STOP");

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        return $zQueryListeBonPlan->result();
    }
  //begin icon_boutic kappa
  function save_data_btc_icon($data){
    
           $query = $this->db->get_where('annonce_btn', array('IdCommercant' => $data['IdCommercant']));

                $count = $query->num_rows();

            if($count === 0){

                $this->db->insert("annonce_btn", $data);
                
             }else{

                $this->db->where("IdCommercant", $data['IdCommercant']);
                $this->db->update("annonce_btn", $data);
            
            }
     }

  
    
  public function add_photo_icon_btc($icat, $img_file){
        echo $img_file;
        $Sql = "
            UPDATE annonce_btn
            SET icon_btc = '".$img_file."'
            WHERE IdCommercant = '".$icat."'
        ";
        $Query = $this->db->query($Sql);
    }

  public function delete_icon_btc($idcom){
      $Sql = "
            UPDATE annonce_btn
            SET icon_btc = null
            WHERE IdCommercant = '".$idcom."'
        ";
        $Query = $this->db->query($Sql);
    }
    

 function get_btc_icon($IdCommercant){

        $this->db->where("IdCommercant", $IdCommercant);
       $get= $this->db->get("annonce_btn");
       return $get->row();
}    //end icon_boutic kappa

}
