<?php
class mdlimagespub extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	function GetByImagespubId($prmId) {
        $qryImagespub =  $this->db->query("SELECT * FROM imagespub WHERE imagespub_id = ?", $prmId);
        if ($qryImagespub->num_rows() > 0) {
            $Res = $qryImagespub->result();
            return $Res[0];
		}
    }
	
	function GetByImagespubActiv() {
        $qryImagespub =  $this->db->query("SELECT * FROM imagespub WHERE activer = 1");
        if ($qryImagespub->num_rows() > 0) {
            $Res = $qryImagespub->result();
            return $Res[0];
		}
    }
	
	function listeImagespub(){
		$zQueryListeImagespub = $this->db->query("SELECT * FROM imagespub") ;
		return $zQueryListeImagespub->result() ;
    }
	

	function supprimeImagespub($prmId){
	
		$zRpsImagespub =  $this->db->query("SELECT * FROM imagespub WHERE imagespub_id = ?", $prmId);
		$toImagespub = $zRpsImagespub->result() ;
		$oImagespub = $toImagespub[0] ;
		if (is_file("application/resources/front/images/".$oImagespub->imagespub_image)){
			unlink ("application/resources/front/images/".$oImagespub->imagespub_image) ;
		}
		$qryImagespub = $this->db->query("DELETE FROM imagespub WHERE imagespub_id = ?", $prmId) ;
		return $qryImagespub ;
    }
	
	function activepub ($prmId){
		$zRpsImagespub =  $this->db->query("UPDATE imagespub SET activer = '0' WHERE activer = ?", 1);

		$argData['activer'] = 1 ;
		$this->db->where("imagespub_id", $prmId);
		$this->db->update("imagespub", $argData);
		$objSustainability = $this->GetByImagespubId($prmId);
		return $objSustainability ;
	}
	
	function SaveImagespub($argData){
		$argData = (array) $argData ;
		
		if (isset($argData["imagespub_id"]) && IsValidId($argData["imagespub_id"]) ){
			$path = "application/resources/front/images/" ;	 
			$tResult = $this->GetByImagespubId($argData["imagespub_id"]) ;
			if (is_file($path.$tResult->imagespub_image) && $tResult->imagespub_image != $argData["imagespub_image"]){
				unlink ($path.$tResult->imagespub_image) ;
			}
			$this->db->where("imagespub_id", $argData["imagespub_id"]);
			$this->db->update("imagespub", $argData);
			$objSustainability = $this->GetByImagespubId($argData["imagespub_id"]);
			return $objSustainability ;
		}else{
			$this->db->insert("imagespub", $argData);
			return $this->db->insert_id();
		}
	}
	
}