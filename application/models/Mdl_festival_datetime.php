<?php
class mdl_festival_datetime extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    function getAll(){
        $result = $this->db->query("
                SELECT
                    *
                FROM
                    festival_datetime
                ORDER BY id DESC

        ");
        if($result->num_rows() > 0) {
            return $result->result();
        }
    }

    function getByFestivalId($article_id = 0){
        $sql = "
                SELECT
                    *
                FROM
                    festival_datetime
                    WHERE festival_id 
                ORDER BY id ASC
                ";
        $qryCategorie = $this->db->query($sql);
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function getById($id=0){
        $Sql = " select * from festival_datetime where id = ". $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }


    function insert($prmData) {
        if ($prmData["id"]=='0' || $prmData["id"]==0) $prmData["id"]=null;
        $this->db->insert("festival_datetime", $prmData);
        return $this->getById($this->db->insert_id());
    }

    function update($prmData) {
        $prmData = (array)$prmData;
        $this->db->where("id", $prmData["id"]);
        $this->db->update("festival_datetime", $prmData);
        return $this->getById($prmData["id"]);
    }

    function delete($prmId=0){

        $qryBonplan = $this->db->query("DELETE FROM festival_datetime WHERE id = ?", $prmId);
        return $qryBonplan ;
    }

    function getWhere($prmWhere = "0 = 0", $prmOffset = 0, $prmLimit = 0, $prmOrder = " festival_datetime.id ASC ") {
        if (empty($prmOrder)) {
            $prmOrder = " festival_datetime.id ASC ";
        }

        $qryString = "
            SELECT
                *
            FROM
                festival_datetime
            WHERE " . $prmWhere . "
            ORDER BY " . $prmOrder . "";
        if (!(($prmOffset == 0) and ($prmLimit == 0))) {
            $qryString .= " LIMIT $prmOffset, $prmLimit";
        }
        return $this->db->query($qryString)->result();
    }


}