<?php



class Api_model_particulier extends CI_Model
{

    public function get_user_info_by_username($login){
        $this->db->select("users.*,card.*,villes.ville_nom");
        $this->db->from('users');
        $this->db->join('card', 'card.id_user = users.IdUser');
        $this->db->join('villes', 'villes.IdVille = users.IdVille');
        $this->db->where('users.Login',$login);
        $resultat = $this->db->get();
        return $resultat->row();
    }

    public function get_commande_list_by_iduser($id_client){
        $this->db->select('commande_soutenons_list.*,users.Email as User_email,users.Portable as User_tel,commercants.*');
        $this->db->from('commande_soutenons_list');
        $this->db->join('users', 'users.IdUser = commande_soutenons_list.id_client');
        $this->db->join('commercants', 'commercants.IdCommercant = commande_soutenons_list.idCom');
        $this->db->where("commande_soutenons_list.id_client",$id_client);
        $this->db->order_by("commande_soutenons_list.created_at",'desc');
        $rest = $this->db->get()->result();
        return $rest;
    }
    
    public function get_commande_details($id){
        $this->db->select('commande_soutenons_details.*,article_soutenons.true_title');
        $this->db->where("commande_soutenons_details.id_commande",$id);
        $this->db->join('article_soutenons', 'article_soutenons.id = commande_soutenons_details.id_produit','left');
        $rest = $this->db->get("commande_soutenons_details")->result();
        return $rest;
    }
    public function update_user($data,$id_user){
        $this->db->where('IdUser',$id_user);
        $this->db->update('users',$data);
    }
}