<?php

class mdl_categories_annonce extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function getById($id = 0)
    {
        $Sql = "select * from rubriques where  IdRubrique =" . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByIdSousCateg($id = 0)
    {
        $Sql = "select * from sous_rubrique where IdSousRubrique =" . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByIdCateg($id = 0)
    {
        $Sql = "select * from rubriques where WHERE  IdRubrique =" . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function GetAll()
    {
        $qryagenda_type = $this->db->query("
            SELECT *
            FROM
                rubriques
            
            ORDER BY Nom ASC
        ");
        if ($qryagenda_type->num_rows() > 0) {
            return $qryagenda_type->result();
        }
    }

    function Insert($prmData)
    {
        $this->db->insert("rubriques", $prmData);
        return $this->db->insert_id();
    }

    function Update($prmData)
    {
        $prmData = (array)$prmData;
        $this->db->where("IdRubrique", $prmData["IdRubrique"]);
        $this->db->update("rubriques", $prmData);
        $objCommercant = $this->GetById($prmData["IdRubrique"]);
        return $objCommercant->IdRubrique;
    }

    function Delete($prmId)
    {
        $qryBonplan = $this->db->query("DELETE FROM rubriques WHERE IdRubrique = ?", $prmId);
        return $qryBonplan;
    }


    function InsertSousCateg($prmData)
    {
        $this->db->insert("sous_rubrique", $prmData);
        return $this->db->insert_id();
    }

    function UpdateSousCateg($prmData)
    {
        $prmData = (array)$prmData;
        $this->db->where("IdSousRubrique", $prmData["IdSousRubrique"]);
        $this->db->update("sous_rubrique", $prmData);
        $objCommercant = $this->GetByIdSousCateg($prmData["IdSousRubrique"]);
        return $objCommercant->IdSousRubrique;
    }

    function DeleteSousCateg($prmId)
    {
        $qryBonplan = $this->db->query("DELETE FROM agenda_subcateg WHERE agenda_subcategid = ?", $prmId);
        return $qryBonplan;
    }

    function GetAllSousrubrique()
    {
        $qryCategorie = $this->db->query("
            SELECT *
            FROM
                sous_rubrique
            ORDER BY subcateg ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAllSousrubriqueByRubrique($IdRubrique)
    {
        $qryCategorie = $this->db->query("
            SELECT *
            FROM
                sous_rubrique
            WHERE
                IdRubrique = '" . $IdRubrique . "'
            ORDER BY subcateg ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function verifier_categorie_agenda($IdRubrique)
    {
        $qryCategorie = $this->db->query("
            SELECT
            agenda.*
            FROM
            agenda
            WHERE
            agenda.agenda_categid = '" . $IdRubrique . "'
       ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function verifier_souscategorie_agenda($IdSousRubrique)
    {
        $qryCategorie = $this->db->query("
            SELECT
            agenda.*
            FROM
            agenda
            WHERE
            agenda.agenda_subcategid = '" . $IdSousRubrique . "'
       ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAllCategByTypeagenda($agenda_typeid)
    {
        $qryCategorie = $this->db->query("
            SELECT *
            FROM
                rubriques
            WHERE
                annonce_typeid = '" . $agenda_typeid . "'
            ORDER BY Nom ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAllSousCategByTypeagenda($agenda_categid)
    {
        $qryCategorie = $this->db->query("
            SELECT *
            FROM
                sous_rubrique
            WHERE
                IdRubrique = '" . $agenda_categid . "'
            ORDER BY subcateg ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetAgendaSouscategorieByRubrique($idRubrique, $inputQuand = "0", $inputDatedebut = "", $inputDatefin = "", $_iDepartementId = "0", $_iVilleId = "0")
    {

        $zSqlListePartenaire = "
            SELECT
            sous_rubrique.IdSousRubrique,
            sous_rubrique.IdRubrique,
            sous_rubrique.subcateg,
            COUNT(annonce_datetime.id) as nb_agenda,
            COUNT(annonce.annonce_id) as nb_agenda_id
            FROM
            sous_rubrique
             
            LEFT OUTER JOIN annonce_datetime ON annonce_datetime.annonce_id = annonce.annonce_id
            WHERE
            sous_rubrique.IdRubrique = '" . $idRubrique . "'
            
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $zSqlListePartenaire .= " AND agenda.IdVille_localisation = '2031' ";
        } elseif (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0" && $localdata_IdVille != null) {
            $zSqlListePartenaire .= " AND agenda.IdVille_localisation = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $zSqlListePartenaire .= " AND agenda.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));

            if ($inputQuand == "1") $zSqlListePartenaire .= " AND annonce_datetime.date_debut = '" . $date_of_day . "'";
            if ($inputQuand == "2") $zSqlListePartenaire .= " AND ( annonce_datetime.date_debut>= '" . $next_saturday_day . "' AND annonce_datetime.date_fin <= '" . $next_sunday_day . "' )";
            if ($inputQuand == "3") $zSqlListePartenaire .= " AND ( annonce_datetime.date_debut>= '" . $last_sunday_day . "' AND annonce_datetime.date_fin <= '" . $next_saturday_day . "' )";
            if ($inputQuand == "4") $zSqlListePartenaire .= " AND (annonce_datetime.date_debut>= '" . $last_sunday_of_next_monday . "' AND annonce_datetime.date_fin<= '" . $next_saturday_of_next_monday . "' )";
            if ($inputQuand == "5") $zSqlListePartenaire .= " AND ( annonce_datetime.date_debut>= '" . $first_day_month . "' AND  annonce_datetime.date_fin <= '" . $last_day_month . "' )";
        }

        if ($_iVilleId != "" && $_iVilleId != NULL && $_iVilleId != "0") {
            $zSqlListePartenaire .= " AND agenda.IdVille_localisation = '" . $_iVilleId . "'";
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " agenda.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }

        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null && $inputDatedebut != "0") {
            $zSqlListePartenaire .= " AND annonce_datetime.date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null && $inputDatefin != "0") {
            $zSqlListePartenaire .= " AND annonce_datetime.date_fin = '" . $inputDatefin . "'";
        }

        $zSqlListePartenaire .= "
            GROUP BY
            sous_rubrique.IdSousRubrique
            ";

        //var_dump($zSqlListePartenaire);
        //echo $zSqlListePartenaire."<br/>"; //die();
        //log_message('error', 'william : '.$zSqlListePartenaire);

        $qryCategorie = $this->db->query($zSqlListePartenaire);
        //if($qryCategorie->num_rows() > 0) {
        return $qryCategorie->result();
        //var_dump($qryCategorie->result()); die();
        //} else return "0";
    }


    function effacerimages_categ($_iCategId)
    {
        $zSql = "UPDATE rubriques SET images ='' WHERE IdRubrique =" . $_iCategId;
        $zQuery = $this->db->query($zSql);
    }


}