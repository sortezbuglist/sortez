<?php
class mdlmapgoogle extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
	
	function getMap($id=1){
		$Sql = "select * from wpvc_sb_google_map where map_id =". $id ;		
		$Query = $this->db->query($Sql);
		return $Query->row();
	}
	
    function getIdVilleByIdBonplan($IdBonplan){
        $Sql = "
            SELECT
            commercants.IdVille,
            bonplan.bonplan_titre,
            bonplan.bonplan_id,
            commercants.IdCommercant,
            commercants.NomSociete,
            commercants.Adresse1,
            rubriques.Nom as rubrique,
            sous_rubriques.Nom as sousrubrique,
            bonplan.bonplan_photo1,
            commercants.nom_url
            FROM
            bonplan
            INNER JOIN commercants ON commercants.IdCommercant = bonplan.bonplan_commercant_id
            INNER JOIN ass_commercants_rubriques ON commercants.IdCommercant = ass_commercants_rubriques.IdCommercant
            INNER JOIN rubriques ON ass_commercants_rubriques.IdRubrique = rubriques.IdRubrique
            INNER JOIN sous_rubriques ON rubriques.IdRubrique = sous_rubriques.IdRubrique
            WHERE
            bonplan.bonplan_id = '".$IdBonplan."' 
            LIMIT 1
            ";  
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function setMap($array_Id, $page_category = "bonplan"){

        $address            = array();
        $textfordirlink     = array();
        $icon               = array();
        $animation          = array();
        $infowindow         = array();
        $content            = array();
        $latitude           = 0;
        $longitude          = 0;


        if (is_array($array_Id) && count($array_Id)) {
            for($ii = 0; $ii < count($array_Id); $ii++) {
                $address_to_localize = '';
                if ($page_category = "bonplan") {
                    $oBonplan = $this->getIdVilleByIdBonplan($array_Id[$ii]);
                    $IdVille = $oBonplan->IdVille;
                    $full_address_contet = '<div class="itemgooglemaptitle" id="itemgooglemaptitle_'.$page_category.'_'.$array_Id[$ii].'">'.$oBonplan->bonplan_titre.'</div>';
                    $oCommercant = $this->getCommercantbyIdBonplan($array_Id[$ii]);
                    $address_to_localize = $oCommercant->Adresse1." ".$oCommercant->Adresse2." ".$oCommercant->Nom;
                }
                model_load_model("mdlville");
                $oVille = $this->mdlville->getVilleById($IdVille);

                $url_to_localise = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode(trim($address_to_localize))."&sensor=false";
                $location = json_decode(file_get_contents($url_to_localise));		//Get latitude, longitude from geocode api
				//var_dump($location); die("STOP");
				if(strtoupper($location->status) == 'OK') {
					$latitude = $location->results[0]->geometry->location->lat;
					$longitude = $location->results[0]->geometry->location->lng;
				} else {
					$latitude = str_replace(',', '.', $oVille->ville_latitude_deg);
                	$longitude = str_replace(',', '.', $oVille->ville_longitude_deg);
				}

				//$latitude = str_replace(',', '.', $oVille->ville_latitude_deg);
                //$longitude = str_replace(',', '.', $oVille->ville_longitude_deg);

                $address[$ii] = $latitude.",".$longitude;
                $textfordirlink[$ii] = 'Directions';
                $icon[$ii] = 'magenta-8';
                $animation[$ii] = 'BOUNCE';
                $infowindow[$ii] = 'yes';
                $content[$ii] = str_replace("'", " ", str_replace('"', ' ', $full_address_contet));
                //$content[$ii] = str_replace("'", "\'", str_replace('"', '\"', $full_address_contet." - ".$array_Id[$ii]));
            }
        } else {
            $array_Id = array();
        }


            
        $markers = array();
        
        $total_markers = count($address);
        for($i = 0; $i < $total_markers; $i++) {
            
            $markers[] = array(
                'address'                   =>  $address[$i],
                'latitude'                  =>  $latitude,
                'longitude'                 =>  $longitude,
                'textfordirectionslink'     =>  $textfordirlink[$i],
                'icon'                      =>  $icon[$i],
                'animation'                 =>  $animation[$i],
                'infowindow'                =>  $infowindow[$i],
                'content'                   =>  $content[$i]
            );
        }

        //var_dump(serialize($markers)); die("stop0");
        $Sql_setMap = "UPDATE `wpvc_sb_google_map` SET `markers` = '".serialize($markers)."' WHERE `wpvc_sb_google_map`.`map_id` = 1;";   
        //echo $Sql_setMap;die("sqlMap");  
        $this->db->query($Sql_setMap);


    }

    

    function update($prmData) {
        $this->db->where("map_id", $prmData["map_id"]);
        $this->db->update("wpvc_sb_google_map", $prmData);
        $objResult = $this->getById($prmData["map_id"]);
        return $objResult->map_id;
    }

    function getCommercantbyIdBonplan($IdBonplan) {

    	$Sql = "
            SELECT
			commercants.Adresse1,
			commercants.Adresse2,
			villes.Nom
			FROM
			bonplan
			INNER JOIN commercants ON bonplan.bonplan_commercant_id = commercants.IdCommercant
			INNER JOIN villes ON villes.IdVille = commercants.IdVille
			WHERE
			bonplan.bonplan_id = '".$IdBonplan."' 
			LIMIT 1
            ";  
        $Query = $this->db->query($Sql);
        return $Query->row();

    }

    


}