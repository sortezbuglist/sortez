<?php

class Livreor_model extends CI_Model
{
	private $table = 'livreor_commentaires';
	
	public function ajouter_commentaire($nom)
	{
		
	 $save=$this->db->insert($this->table,$nom);
	 if ($save) {
	 	return 1;
	 }else{
	 	return 0;
	 }
	}
	
	public function count($Idcommercant)
	{
		$this->db->where('idcommercant',$Idcommercant);
		$nbr = $this->db->get('livreor_commentaires'); 
		return $nbr->result();
	}
	
	public function get_commentaires($nb, $debut = 0,$Idcommercant)
	{
		if(!is_integer($nb) OR $nb < 1 OR !is_integer($debut) OR $debut < 0)
		{
			return false;
		}
		
		return $this->db->select('`id`, `nom`, `mail`, `mobile`, `message`, `vote`, DATE_FORMAT(`date`,\'%d/%m/%Y &agrave; %H:%i:%s\') AS \'date\'', false)
				->from($this->table)
				->where('idcommercant',$Idcommercant)
				->order_by('id', 'desc')
				->limit($nb, $debut)
				->get()
				->result();
	}
}

/* End of file livreor_model.php */
/* Location: ./application/models/livreor_model.php */