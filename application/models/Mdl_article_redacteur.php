<?php

class mdl_article_redacteur extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function getAll()
    {
        $qryCategorie = $this->db->query("
                SELECT
                    *
                FROM
                    article_redacteur
                ORDER BY nom ASC

        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function getByIdCommercant($IdCommercant = 0)
    {
        $sql = "
                SELECT
                    *
                FROM
                    article_redacteur
                    WHERE IdCommercant = " . $IdCommercant . " 
                ORDER BY nom ASC
                ";
        $qryCategorie = $this->db->query($sql);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function getById($id = 0)
    {
        $Sql = " select * from article_redacteur where id = " . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByNom($nom = '', $IdCommercant = 0)
    {
        $Sql = " select * from article_redacteur where nom = '" . $nom . "' ";
        if (isset($IdCommercant) && $IdCommercant != 0 && $IdCommercant != '0' && $IdCommercant != null)
            $Sql .= " AND IdCommercant = '" . $IdCommercant . "' ";
        $Query = $this->db->query($Sql);
        if ($Query->num_rows() > 0) {
            return $Query->result();
        }
    }


    function insert($prmData)
    {
        $this->db->insert("article_redacteur", $prmData);
        return $this->getById($this->db->insert_id());
    }

    function update($prmData)
    {
        $prmData = (array)$prmData;
        $this->db->where("id", $prmData["id"]);
        $this->db->update("article_redacteur", $prmData);
        return $this->getById($prmData["id"]);
    }

    function delete($id)
    {
        return $this->db->delete("article_redacteur", array('id' => $id));
    }

    function getWhere($prmWhere = "0 = 0", $prmOffset = 0, $prmLimit = 0, $prmOrder = " article_redacteur.nom ASC ")
    {
        if (empty($prmOrder)) {
            $prmOrder = " article_redacteur.nom ASC ";
        }

        $qryString = "
            SELECT
                *
            FROM
                article_redacteur
            WHERE " . $prmWhere . "
            ORDER BY " . $prmOrder . "";
        if (!(($prmOffset == 0) and ($prmLimit == 0))) {
            $qryString .= " LIMIT $prmOffset, $prmLimit";
        }
        return $this->db->query($qryString)->result();
    }


}