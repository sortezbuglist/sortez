<?php
class Commercant extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
       
    function Insert($prmData) {
        $prmData["URI"] = $this->getURI($prmData["IdVille"],$prmData["NomSociete"]);
        $this->db->insert("commercants", $prmData);
        return $this->db->insert_id();
    }
    
    function GetById($prmId) {
        $qryCommercant =  $this->db->query("SELECT * FROM commercants WHERE IdCommercant = ?", $prmId);
        if ($qryCommercant->num_rows() > 0) {
            $Res = $qryCommercant->result();
            return $Res[0];
        }
    }
    
    function setUrlFormat($prmData, $IdCommercant) {
                $change_url = $prmData;
                
                if (isset($IdCommercant) && $IdCommercant!=0){
                    $objCommercant = $this->GetById($IdCommercant);
                    //die("dddddddddddddd ".$prmData." ".$IdCommercant." ".$objCommercant->nom_url);
                }
                
                if (isset($objCommercant) && ($objCommercant->nom_url == $prmData || $objCommercant->nom_url == $prmData."1" || $objCommercant->nom_url == $prmData."2" || $objCommercant->nom_url == $prmData."3" || $objCommercant->nom_url == $prmData."4" || $objCommercant->nom_url == $prmData."5" || $objCommercant->nom_url == $prmData."6" || $objCommercant->nom_url == $prmData."7" || $objCommercant->nom_url == $prmData."8" || $objCommercant->nom_url == $prmData."9" || $objCommercant->nom_url == $prmData."10")){
                        return $objCommercant->nom_url;
                    } else {

                    $thisss =& get_instance();

                $articleRequest_verif = "SELECT * FROM commercants where nom_url='".mysqli_real_escape_string($thisss->db->conn_id,$change_url)."'";
                $articlesResult_verif = $this->db->query($articleRequest_verif);
                $change_url_final = $change_url;
                
                if ($articlesResult_verif->num_rows()>0) {
                    $change_url_final = $change_url."1";
                    $articleRequest_verif1 = "SELECT * FROM commercants where nom_url='".mysqli_real_escape_string($thisss->db->conn_id,$change_url)."1'";
                    $articlesResult_verif1 = $this->db->query($articleRequest_verif1);
                    if ($articlesResult_verif1->num_rows()>0) {
                        $change_url_final = $change_url."2";
                        $articleRequest_verif2 = "SELECT * FROM commercants where nom_url='".mysqli_real_escape_string($thisss->db->conn_id,$change_url)."2'";
                        $articlesResult_verif2 = $this->db->query($articleRequest_verif2);
                        if ($articlesResult_verif2->num_rows()>0) {
                            $change_url_final = $change_url."3";
                            $articleRequest_verif3 = "SELECT * FROM commercants where nom_url='".mysqli_real_escape_string($thisss->db->conn_id,$change_url)."3'";
                            $articlesResult_verif3 = $this->db->query($articleRequest_verif3);
                            if ($articlesResult_verif3->num_rows()>0) {
                                $change_url_final = $change_url."4";
                                $articleRequest_verif4 = "SELECT * FROM commercants where nom_url='".mysqli_real_escape_string($thisss->db->conn_id,$change_url)."4'";
                                $articlesResult_verif4 = $this->db->query($articleRequest_verif4);
                                if ($articlesResult_verif4->num_rows()>0) {
                                    $change_url_final = $change_url."5";
                                    $articleRequest_verif5 = "SELECT * FROM commercants where nom_url='".mysqli_real_escape_string($thisss->db->conn_id,$change_url)."5'";
                                    $articlesResult_verif5 = $this->db->query($articleRequest_verif5);
                                    if ($articlesResult_verif5->num_rows()>0) {
                                        $change_url_final = $change_url."6";
                                        $articleRequest_verif6 = "SELECT * FROM commercants where nom_url='".mysqli_real_escape_string($thisss->db->conn_id,$change_url)."6'";
                                        $articlesResult_verif6 = $this->db->query($articleRequest_verif6);
                                        if ($articlesResult_verif6->num_rows()>0) {
                                            $change_url_final = $change_url."7";
                                            $articleRequest_verif7 = "SELECT * FROM commercants where nom_url='".mysqli_real_escape_string($thisss->db->conn_id,$change_url)."7'";
                                            $articlesResult_verif7 = $this->db->query($articleRequest_verif7);
                                            if ($articlesResult_verif7->num_rows()>0) {
                                                $change_url_final = $change_url."8";
                                                $articleRequest_verif8 = "SELECT * FROM commercants where nom_url='".mysqli_real_escape_string($thisss->db->conn_id,$change_url)."8'";
                                                $articlesResult_verif8 = $this->db->query($articleRequest_verif8);
                                                if ($articlesResult_verif8->num_rows()>0) {
                                                    $change_url_final = $change_url."9";
                                                    $articleRequest_verif9 = "SELECT * FROM commercants where nom_url='".mysqli_real_escape_string($thisss->db->conn_id,$change_url)."9'";
                                                    $articlesResult_verif9 = $this->db->query($articleRequest_verif9);
                                                    if ($articlesResult_verif9->num_rows()>0) {
                                                        $change_url_final = $change_url."10";
                                                        $articleRequest_verif10 = "SELECT * FROM commercants where nom_url='".mysqli_real_escape_string($thisss->db->conn_id,$change_url)."10'";
                                                        $articlesResult_verif10 = $this->db->query($articleRequest_verif10);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
                return $change_url_final;
                    
                
                
                    }
                
                
    }
    
    function GetCatById($prmId) {
        $query = "SELECT
                    commercants.*,
                    sous_rubriques.Nom AS sous_rubrique,
                    rubriques.Nom AS rubrique
                    FROM
                    commercants
                    Inner Join ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
                    Inner Join ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
                    Inner Join sous_rubriques ON sous_rubriques.IdSousRubrique = ass_commercants_sousrubriques.IdSousRubrique
                    Inner Join rubriques ON rubriques.IdRubrique = ass_commercants_rubriques.IdRubrique
                    WHERE
                    commercants.IdCommercant = ".$prmId ;
        $qryCommercant =  $this->db->query($query);
        if ($qryCommercant->num_rows() > 0) {
            $Res = $qryCommercant->result();
            return $Res[0];
        }
    }


    function GetWhere($prmId="") {
        
        if ($prmId!="") {
                $query = " SELECT
                            commercants.*
                            FROM commercants 
                            WHERE
                                0=0 ";
                if ($prmId!="")
                $query .= " AND ".$prmId." " ;

                $qryCommercant =  $this->db->query($query);
                if ($qryCommercant->num_rows() > 0) { 
                    $Res = $qryCommercant->result();
                    return $Res[0];
                }

            }
    }


    function CountWhere_pvc($prmWhere = "0=0") {
        $Res = $this->db->query("SELECT COUNT(IdCommercant) AS 'Decompte' FROM (commercants LEFT JOIN villes On villes.IdVille = commercants.IdVille) WHERE " . $prmWhere)->result();
        return $Res[0]->Decompte;
    }


    function CountWhere_pvc_association($prmWhere = "0=0") {
        $sql = " SELECT COUNT(IdCommercant) AS 'Decompte' FROM (commercants LEFT JOIN villes On villes.IdVille = commercants.IdVille) WHERE " . $prmWhere;

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sql .= " AND villes.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sql .= " AND villes.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sql .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sql .= " villes.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sql .= " OR ";
            }
            $sql .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sql .= " AND villes.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE
        
        $Res = $this->db->query($sql)->result();
        return $Res[0]->Decompte;
    }


    function GetWhere_pvc($prmWhere = "0 = 0", $prmOffset = 0, $prmLimit = 0, $prmOrder = " commercants.Nom ASC ") {
        // if ($prmWhere == "") {
        //     $prmWhere = " 0 = 0 ";
        // }
        if (empty($prmWhere)) {
            $prmWhere = " 0 = 0 ";
        }
        if (empty($prmOrder)) {
            $prmOrder = " commercants.Nom ASC ";
        }        
        
        $qryString = " SELECT commercants.*, villes.Nom AS 'NomVille'
                    FROM
                        (commercants LEFT JOIN villes ON villes.IdVille = commercants.IdVille)

                    WHERE  " . $prmWhere . "
            ORDER BY ". $prmOrder ." ";
        if (!(($prmOffset == 0) and ($prmLimit == 0))) {
            $qryString .= " LIMIT $prmOffset, $prmLimit";
        }
       // var_dump($qryString);
        // print_r($qryString);
        return $this->db->query($qryString)->result();
    }

    //modele litre dernier inscrit par mamitiana
    function listeDerniereInscrit($prmWhere = "IsActif = 1", $prmOffset = 0, $prmLimit = 0, $prmOrder = " commercants.Nom ASC ") {

$t=time(); 
$t2=2592000;
//echo $t2;
$res=$t-$t2;

        if (empty($prmWhere)) {
            $prmWhere = " IsActif = 1 ";
        }
        if (empty($prmOrder)) {
            $prmOrder = " commercants.Nom ASC ";
        }    


//         SELECT 
// commercants.*, ass_commercants_abonnements.DateDebut AS 'dateCreationCompt', villes.Nom AS 'NomVille' 
// FROM (commercants LEFT JOIN ass_commercants_abonnements ON ass_commercants_abonnements.IdCommercant = commercants.IdCommercant 
// LEFT JOIN villes ON villes.IdVille = commercants.IdVille) 
// ORDER BY idcommercant DESC LIMIT 0, 5;


    //var_dump($prmOrder);die();

    $prmOrder=  "commercants.datecreation DESC";

        $qryString = " SELECT commercants.*, villes.Nom AS 'NomVille', ass_commercants_abonnements.DateDebut AS 'dateAbonnement',ass_commercants_abonnements.DateFin AS 'datefinAbonnement'
                    FROM
                        (commercants LEFT JOIN villes ON villes.IdVille = commercants.IdVille
                        LEFT JOIN ass_commercants_abonnements ON ass_commercants_abonnements.IdCommercant = commercants.IdCommercant)

                    WHERE  " . $prmWhere . " AND commercants.datecreation BETWEEN ".$res." AND ".$t."

                                ORDER BY ". $prmOrder ." "; 
        if (!(($prmOffset == 0) and ($prmLimit == 0))) {
            $qryString .= " LIMIT $prmOffset, $prmLimit";
        }
       //print_r($qryString);die();
;       // print_r($qryString);
        return $this->db->query($qryString)->result();
    }



    function GetWhere_pvc_association($prmWhere = "0 = 0", $prmOffset = 0, $prmLimit = 0, $prmOrder = " commercants.Nom ASC ") {
        if (empty($prmOrder)) {
            $prmOrder = " commercants.Nom ASC ";
        }
        
        
        $qryString = "
            SELECT
                commercants.*,
                villes.Nom AS 'NomVille'
            FROM
                (commercants
                LEFT JOIN villes ON villes.IdVille = commercants.IdVille)
            WHERE " . $prmWhere . "
            ";


        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $qryString .= " AND villes.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $qryString .= " AND villes.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $qryString .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $qryString .= " villes.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $qryString .= " OR ";
            }
            $qryString .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $qryString .= " AND villes.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $qryString .= " ORDER BY " . $prmOrder . " ";

        if (!(($prmOffset == 0) and ($prmLimit == 0))) {
            $qryString .= " LIMIT $prmOffset, $prmLimit";
        }
        return $this->db->query($qryString)->result();
    }
    


    
    function Update($prmData) {
        $prmData = (array)$prmData;
        $prmData["URI"] = $this->getURI($prmData["IdVille"],$prmData["NomSociete"]);
        $this->db->where("IdCommercant", $prmData["IdCommercant"]);
        $this->db->update("commercants", $prmData);
        $objCommercant = $this->GetById($prmData["IdCommercant"]);
        return $objCommercant->IdCommercant;
    }

    function UpdateQrcodeByIdCommercant($IdCommercant = '0', $qrCodetext = '', $qrCodeimg = ''){
        $query = "
            UPDATE `commercants` SET `qrcode_text` = '".$qrCodetext."', `qrcode_img` = '".$qrCodeimg."' WHERE `commercants`.`IdCommercant` = '".$IdCommercant."';
        ";
        $qryCommercant =  $this->db->query($query);
    }

    function Updatetextmenuannonce($IdCommercant = '0', $textmenuannonce = ''){
        $query = "
            UPDATE `commercants` SET `textmenuannonce` = '".$textmenuannonce."' WHERE `commercants`.`IdCommercant` = '".$IdCommercant."';
        ";
        $qryCommercant =  $this->db->query($query);
    }
    
    function getURI($prmIdVille = 0, $prmNom = "") {
        $URI = "";
        $qryVille =  $this->db->query("SELECT * FROM villes WHERE IdVille = ?", $prmIdVille);
        if ($qryVille->num_rows() > 0) {
            $Res = $qryVille->result();
            $objVille = $Res[0];
            $URI = RemoveExtendedChars2($objVille->Nom) . "/" . RemoveExtendedChars2($prmNom);
        }
        return $URI;
    }
//     function Get_by_All() {
//         $qryCommercant =  $this->db->query("
//             SELECT
//                 IdCommercant, idstatut
//             FROM
//                 commercants
//              WHERE IdCommercant >= 363808;
//         ");
//         return $qryCommercant->result();
//     }
//     function update_idstate_sousrubrique($idcom, $idstat){

//     $Sql = "
//             UPDATE ass_commercants_sousrubriques 
//             SET IdSousRubrique = '".$idstat."'
//             WHERE IdCommercant = '".$idcom."'

//         ";
//         $Query = $this->db->query($Sql);
//  }
//  function get_idsous_idstate(){

// $qryCommercant =  $this->db->query("
//             SELECT
//                 IdCommercant,IdSousRubrique
//             FROM
//                 ass_commercants_sousrubriques
//              WHERE IdCommercant >= 363808;
//         ");
//         return $qryCommercant->result();

//  }
    
    function GetAll() {
        $qryCommercant =  $this->db->query("
            SELECT
                *
            FROM
                commercants
        ");
        return $qryCommercant->result();
    }
    
    function GetAllCommercantforNewsletter() {
        $qryCommercant =  $this->db->query("
            SELECT
            commercants.*,
            sous_rubriques.Nom AS sousrubrique,
            rubriques.Nom AS rubrique,
            villes.Nom ville
            FROM
            commercants
            Inner Join ass_commercants_sousrubriques ON ass_commercants_sousrubriques.IdCommercant = commercants.IdCommercant
            Left Outer Join sous_rubriques ON ass_commercants_sousrubriques.IdSousRubrique = sous_rubriques.IdSousRubrique
            Left Outer Join rubriques ON sous_rubriques.IdRubrique = rubriques.IdRubrique
            left outer join villes ON commercants.IdVille = villes.IdVille
            where IsActif=1
            order by commercants.IdCommercant desc
            LIMIT 10
        ");
        return $qryCommercant->result();
    }
    
    function GetByURI($prmVille = "", $prmNomCommerce = "") {
        $qryCommercant =  $this->db->query("SELECT * FROM commercants WHERE URI = '$prmVille/$prmNomCommerce'");
        if ($qryCommercant->num_rows() > 0) {
            $Res = $qryCommercant->result();
            return $Res[0];
        }
    }
    
    function effacerPhotoLogoCommercant($_iCommercantId){
            $zSqlEffacerPhotoCommercant = "UPDATE commercants SET Logo='' WHERE IdCommercant=" . $_iCommercantId ;
            $zQueryEffacerPhotoCommercant = $this->db->query($zSqlEffacerPhotoCommercant) ;
    }
    function effacerPhotoCommercant($_iCommercantId){
            $zSqlEffacerPhotoCommercant = "UPDATE commercants SET PhotoAccueil='' WHERE IdCommercant=" . $_iCommercantId ;
            $zQueryEffacerPhotoCommercant = $this->db->query($zSqlEffacerPhotoCommercant) ;
    }
    function effacerbandeau_top($_iCommercantId){
            $zSqlEffacerPhotoCommercant = "UPDATE commercants SET bandeau_top='' WHERE IdCommercant=" . $_iCommercantId ;
            $zQueryEffacerPhotoCommercant = $this->db->query($zSqlEffacerPhotoCommercant) ;
    }
    function effacerbackground_image($_iCommercantId){
            $zSqlEffacerPhotoCommercant = "UPDATE commercants SET background_image='' WHERE IdCommercant=" . $_iCommercantId ;
            $zQueryEffacerPhotoCommercant = $this->db->query($zSqlEffacerPhotoCommercant) ;
    }
    function effacerPhoto1($_iCommercantId){
            $zSqlEffacerPhotoCommercant = "UPDATE commercants SET Photo1='' WHERE IdCommercant=" . $_iCommercantId ;
            $zQueryEffacerPhotoCommercant = $this->db->query($zSqlEffacerPhotoCommercant) ;
    }
    function effacerPhoto2($_iCommercantId){
            $zSqlEffacerPhotoCommercant = "UPDATE commercants SET Photo2='' WHERE IdCommercant=" . $_iCommercantId ;
            $zQueryEffacerPhotoCommercant = $this->db->query($zSqlEffacerPhotoCommercant) ;
    }
    function effacerPhoto3($_iCommercantId){
            $zSqlEffacerPhotoCommercant = "UPDATE commercants SET Photo3='' WHERE IdCommercant=" . $_iCommercantId ;
            $zQueryEffacerPhotoCommercant = $this->db->query($zSqlEffacerPhotoCommercant) ;
    }
    function effacerPhoto4($_iCommercantId){
            $zSqlEffacerPhotoCommercant = "UPDATE commercants SET Photo4='' WHERE IdCommercant=" . $_iCommercantId ;
            $zQueryEffacerPhotoCommercant = $this->db->query($zSqlEffacerPhotoCommercant) ;
    }
    function effacerPhoto5($_iCommercantId){
            $zSqlEffacerPhotoCommercant = "UPDATE commercants SET Photo5='' WHERE IdCommercant=" . $_iCommercantId ;
            $zQueryEffacerPhotoCommercant = $this->db->query($zSqlEffacerPhotoCommercant) ;
    }
    function effacerphotogauche($_iCommercantId){
            $zSqlEffacerPhotoCommercant = "UPDATE commercants SET photogauche='' WHERE IdCommercant=" . $_iCommercantId ;
            $zQueryEffacerPhotoCommercant = $this->db->query($zSqlEffacerPhotoCommercant) ;
    }
    function effacerphotodroite($_iCommercantId){
            $zSqlEffacerPhotoCommercant = "UPDATE commercants SET photodroite='' WHERE IdCommercant=" . $_iCommercantId ;
            $zQueryEffacerPhotoCommercant = $this->db->query($zSqlEffacerPhotoCommercant) ;
    }
    function effacerPdf($_iCommercantId){
            $zSqlEffacerPhotoCommercant = "UPDATE commercants SET Pdf='' WHERE IdCommercant=" . $_iCommercantId ;
            $zQueryEffacerPhotoCommercant = $this->db->query($zSqlEffacerPhotoCommercant) ;
    }
    function effacer_activite1_image1($_iCommercantId){
        $zSqlEffacerPhotoCommercant = "UPDATE commercants SET activite1_image1='' WHERE IdCommercant=" . $_iCommercantId ;
        $zQueryEffacerPhotoCommercant = $this->db->query($zSqlEffacerPhotoCommercant) ;
    }
    function effacer_activite1_image2($_iCommercantId){
        $zSqlEffacerPhotoCommercant = "UPDATE commercants SET activite1_image2='' WHERE IdCommercant=" . $_iCommercantId ;
        $zQueryEffacerPhotoCommercant = $this->db->query($zSqlEffacerPhotoCommercant) ;
    }
    function effacer_activite1_image3($_iCommercantId){
        $zSqlEffacerPhotoCommercant = "UPDATE commercants SET activite1_image3='' WHERE IdCommercant=" . $_iCommercantId ;
        $zQueryEffacerPhotoCommercant = $this->db->query($zSqlEffacerPhotoCommercant) ;
    }
    function effacer_activite1_image4($_iCommercantId){
        $zSqlEffacerPhotoCommercant = "UPDATE commercants SET activite1_image4='' WHERE IdCommercant=" . $_iCommercantId ;
        $zQueryEffacerPhotoCommercant = $this->db->query($zSqlEffacerPhotoCommercant) ;
    }
    function effacer_activite1_image5($_iCommercantId){
        $zSqlEffacerPhotoCommercant = "UPDATE commercants SET activite1_image5='' WHERE IdCommercant=" . $_iCommercantId ;
        $zQueryEffacerPhotoCommercant = $this->db->query($zSqlEffacerPhotoCommercant) ;
    }
    function effacer_activite2_image1($_iCommercantId){
        $zSqlEffacerPhotoCommercant = "UPDATE commercants SET activite2_image1='' WHERE IdCommercant=" . $_iCommercantId ;
        $zQueryEffacerPhotoCommercant = $this->db->query($zSqlEffacerPhotoCommercant) ;
    }
    function effacer_activite2_image2($_iCommercantId){
        $zSqlEffacerPhotoCommercant = "UPDATE commercants SET activite2_image2='' WHERE IdCommercant=" . $_iCommercantId ;
        $zQueryEffacerPhotoCommercant = $this->db->query($zSqlEffacerPhotoCommercant) ;
    }
    function effacer_activite2_image3($_iCommercantId){
        $zSqlEffacerPhotoCommercant = "UPDATE commercants SET activite2_image3='' WHERE IdCommercant=" . $_iCommercantId ;
        $zQueryEffacerPhotoCommercant = $this->db->query($zSqlEffacerPhotoCommercant) ;
    }
    function effacer_activite2_image4($_iCommercantId){
        $zSqlEffacerPhotoCommercant = "UPDATE commercants SET activite2_image4='' WHERE IdCommercant=" . $_iCommercantId ;
        $zQueryEffacerPhotoCommercant = $this->db->query($zSqlEffacerPhotoCommercant) ;
    }
    function effacer_activite2_image5($_iCommercantId){
        $zSqlEffacerPhotoCommercant = "UPDATE commercants SET activite2_image5='' WHERE IdCommercant=" . $_iCommercantId ;
        $zQueryEffacerPhotoCommercant = $this->db->query($zSqlEffacerPhotoCommercant) ;
    }
    function effacer_presentation_1_image_1($_iCommercantId,$prmFiles){
        $field=array('IdCommercant'=>$_iCommercantId,
           $prmFiles=>'', );
        $this->db->where('IdCommercant',$_iCommercantId);
        $this->db->update('glissieres',$field);

    }
    function effacer_image($_iCommercantId,$prmFiles){
        $field=array('IdCommercant'=>$_iCommercantId,
            $prmFiles=>'' );
        $this->db->where('IdCommercant',$_iCommercantId);
        $this->db->update('bloc_info',$field);

    }

    function delete_validate($prmId){
    
        $qryBonplan = $this->db->query("DELETE FROM commercants WHERE IdCommercant = ?", $prmId) ;
        return $qryBonplan ;
    }


    function getByCritere($where=array()){
        $this->db->join('villes', 'commercants.IdVille = villes.IdVille');
        $query = $this->db->get_where('commercants', $where);
        return  $query->row();
    }


    function file_manager_update($_iArticleId = '0', $_iUserId = '0', $_iField = 'Photo1', $_iValue = ''){
        $zSql = "UPDATE commercants SET `".$_iField."` = '".$_iValue."' WHERE IdCommercant = ".$_iArticleId." AND `user_ionauth_id` = '".$_iUserId."' " ;
        $zQuery = $this->db->query($zSql);
    }
    function file_manager_update_glissiere($_iArticleId = '0', $_iUserId = '0', $_iField = '', $_iValue = ''){

        $this->db->where('IdCommercant',$_iArticleId);
        $this->db->where('id_ionauth',$_iUserId);
      $res=$this->db->get('glissieres');
      $this->db->where('id_glissiere',$res->row()->id_glissiere);
      $field=array($_iField=>$_iValue,'IdCommercant'=>$_iArticleId,'id_glissiere'=>$res->row()->id_glissiere);
      $this->db->update('glissieres',$field);
        }
    function file_manager_update_bloc_info($_iArticleId = '0', $_iUserId = '0', $_iField = '', $_iValue = ''){

       $this->db->where('IdCommercant',$_iArticleId);
      $res=$this->db->get('bloc_info');
      $this->db->where('id',$res->row()->id);
      $field=array($_iField=>$_iValue,'IdCommercant'=>$_iArticleId,'id'=>$res->row()->id);
      $this->db->update('bloc_info',$field);
        }

public function getByIdglissiere($id){

        $this->db->where('IdCommercant',$id);
        $res= $this->db->get('glissieres');
        return $res->row();

}
public function get_bonplan_commercant_by_id($idcom){

    $zSql = "SELECT * FROM bonplan
 WHERE 
    bonplan.bonplan_commercant_id =". $idcom  ;
    $zQuery = $this->db->query($zSql);
    //var_dump($zQuery->num_rows());die();
    if ($zQuery->num_rows()>0){return $zQuery->result();} else return null;
}
public function get_bonplan_by_id($id){
        $this->db->where('bonplan_id',$id);
        $res= $this->db->get('bonplan');
        return $res->row();
}
public function get_ionauth_by_id_com($idcom){
        $this->db->where('IdCommercant',$idcom);
       $res= $this->db->get('commercants');
       return $res->row();
}
public function get_fidelity_commercant_by_id($idcom){

    $zSql = "SELECT * FROM card_capital
 WHERE 
 card_capital.is_activ =1 AND
    card_capital.id_commercant =". $idcom  ;
    $zQuery = $this->db->query($zSql);
    //var_dump($zQuery->num_rows());die();
    if ($zQuery->num_rows()>0){return $zQuery->row();}else{
        $zSql2 = "SELECT * FROM card_remise
         WHERE 
            card_remise.is_activ =1 AND
            card_remise.id_commercant =". $idcom  ;
        $zQuery2 = $this->db->query($zSql2);
        if ($zQuery2->num_rows()>0){return $zQuery2->row();}else{
            $zSql3 = "SELECT * FROM card_tampon
         WHERE 
            card_tampon.is_activ =1 AND
            card_tampon.id_commercant =". $idcom  ;
            $zQuery3 = $this->db->query($zSql3);
            return $zQuery3->row();
        }
    }

}
public function get_annonce_by_id($id,$idcom){

    $zSql = "SELECT * FROM card_capital
 WHERE 
 card_capital.id_commercant=$idcom AND
 card_capital.is_activ =1 AND
    card_capital.id =". $id  ;
    $zQuery = $this->db->query($zSql);
    //var_dump($zQuery->num_rows());die();
    if ($zQuery->num_rows()>0){return $zQuery->row();}else{
        $zSql2 = "SELECT * FROM card_remise
         WHERE 
            card_remise.id_commercant=$idcom AND
            card_remise.is_activ =1 AND
            card_remise.id =". $id  ;
        $zQuery2 = $this->db->query($zSql2);
        if ($zQuery2->num_rows()>0){return $zQuery2->row();}else{
            $zSql3 = "SELECT * FROM card_tampon
         WHERE 
            card_tampon.id_commercant=$idcom AND
            card_tampon.is_activ =1 AND
            card_tampon.id =". $id  ;
            $zQuery3 = $this->db->query($zSql3);
            return $zQuery3->row();
        }
    }
}
public function updateaffichbouton($array,$idcom){
        $this->db->where('IdCommercant',$idcom);
        $this->db->update('commercants',$array);
}


        function file_manager_update_bloc_newsletter($_iArticleId = '0', $_iUserId = '0', $_iField = '', $_iValue = ''){

       $this->db->where('IdCommercant',$_iArticleId);
      $res=$this->db->get('bloc_newsletter');
      $this->db->where('idbloc',$res->row()->idbloc);
      $field=array($_iField=>$_iValue,'IdCommercant'=>$_iArticleId,'idbloc'=>$res->row()->idbloc);
      $this->db->update('bloc_newsletter',$field);
        }

    public function commercant_idville_localisation(){
        $req ="SELECT
                commercants.IdCommercant,
                commercants.IdVille,
                commercants.IdVille_localisation
                FROM
                commercants";
        $Resreq = $this->db->query($req);
        return $Resreq->result();     
    }
    public function update_commercant_idville_localisation($idcom,$IdVille_localisation){
        $req =$req = "UPDATE `commercants` SET `IdVille_localisation`='".$IdVille_localisation."' WHERE `IdCommercant`=".$idcom;
        $this->db->query($req);    
    }



    // function Get_by_All() {
    //     $qryCommercant =  $this->db->query("
    //     SELECT ass_commercants_abonnements.IdCommercant, ass_commercants_abonnements.IdAbonnement, commercants.Email, commercants.referencement_resto FROM `ass_commercants_abonnements` LEFT JOIN commercants ON ass_commercants_abonnements.IdCommercant = commercants.IdCommercant WHERE ass_commercants_abonnements.IdAbonnement = 3;
    //     ");
    //     return $qryCommercant->result();
    // }
    // function update_idstate_sousrubrique($idcom){
    //     $Sql = "UPDATE `commercants` SET `referencement_resto` = '0' WHERE `commercants`.`IdCommercant` >= 342232;";
    //     $Query = $this->db->query($Sql);
    // }

    // insert new register fields
    public function insert_new_fields($prmData){
        $this->db->insert("pack_commercant_services", $prmData);
        return $this->db->insert_id();
    }
    public function getAll_by_idcom($idcom){
         $this->db->where('IdCommercant',$idcom);
       $res= $this->db->get('pack_commercant_services');
       return $res->row();

    }

    function Update_news($id, $data) {

    $this->db->where('IdCommercant',$id);
    $query = $this->db->get('pack_commercant_services');
    $row = $query->num_rows();
    if($row > 0){  
        //var_dump($data);
        if ($id!=null && $data!=null) {
            $this->db->where('IdCommercant',$id);         
            $this->db->update('pack_commercant_services', $data);  
        }
    }else{
        $data['IdCommercant'] = $id;
        $this->db->insert('pack_commercant_services', $data);
    }
    }
}