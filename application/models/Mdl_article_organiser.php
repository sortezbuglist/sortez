<?php

class mdl_article_organiser extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function getAll()
    {
        $qryCategorie = $this->db->query("
                SELECT
                    *
                FROM
                    article_organiser
                ORDER BY nom ASC

        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function getByIdCommercant($IdCommercant = 0)
    {
        $sql = "
                SELECT
                    *
                FROM
                    article_organiser
                    WHERE IdCommercant = " . $IdCommercant . " 
                ORDER BY name ASC
                ";
        $qryCategorie = $this->db->query($sql);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function getById($id = 0)
    {
        $Sql = " select * from article_organiser where id = " . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByName($name = '', $IdCommercant = 0)
    {
        $Sql = " select * from article_organiser where name = '" . $name . "' ";
        if (isset($IdCommercant) && $IdCommercant != 0 && $IdCommercant != '0' && $IdCommercant != null)
            $Sql .= " AND IdCommercant = '" . $IdCommercant . "' ";
        $Query = $this->db->query($Sql);
        if ($Query->num_rows() > 0) {
            return $Query->result();
        }
    }


    function insert($prmData)
    {
        $this->db->insert("article_organiser", $prmData);
        return $this->getById($this->db->insert_id());
    }

    function update($prmData)
    {
        $prmData = (array)$prmData;
        $this->db->where("id", $prmData["id"]);
        $this->db->update("article_organiser", $prmData);
        return $this->getById($prmData["id"]);
    }

    function delete($id)
    {
        return $this->db->delete("article_organiser", array('id' => $id));
    }

    function getWhere($prmWhere = "0 = 0", $prmOffset = 0, $prmLimit = 0, $prmOrder = " article_organiser.name ASC ")
    {
        if (empty($prmOrder)) {
            $prmOrder = " article_organiser.name ASC ";
        }

        $qryString = "
            SELECT
                *
            FROM
                article_organiser
            WHERE " . $prmWhere . "
            ORDER BY " . $prmOrder . "";
        if (!(($prmOffset == 0) and ($prmLimit == 0))) {
            $qryString .= " LIMIT $prmOffset, $prmLimit";
        }
        return $this->db->query($qryString)->result();
    }
public function saveDatatourismeOrganiser($field){
$this->db->insert("article_organiser",$field);
return $this->db->insert_id();
}

}