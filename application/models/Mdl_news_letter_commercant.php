<?php
class Mdl_news_letter_commercant extends CI_Model{
    private $_table = "client_commercant";

    function __construct() {
        parent::__construct();
    }

    function save($abonner){
       $save= $this->db->insert('client_commercant',$abonner);
       if ($save){
           return 1;
       }else{
           return 0;
       }
    }
     function verify($mail){
        $this->db->where('email',$mail);
        $res=$this->db->get('client_commercant');
        return $res->num_rows();

    }
    function save_stat_newsletter($field){
       $save= $this->db->insert('Newsletter_stat',$field);
       if ($save){
           return 1;
       }else{
           return 0;
       }
    }
    function get_stat_by_idcom($idcom){
        $this->db->where('IdCommercant',$idcom);
        $save=$this->db->get('Newsletter_stat');
        return $save->result();
    }
    function get_data_by_id($id){
        $this->db->where('id',$id);
        $save=$this->db->get('Newsletter_stat');
        return $save->row();
    }
    function save_csv_file_client($data){
       $save= $this->db->insert('client_list_csv',$data);
       if ($save){
           return true;
       }else{
           return false;
       }
    }
    function get_csv_client_by_commercant_id($idcom){
        $this->db->select('mail');
        $this->db->where('IdCommercant',$idcom);
        $save=$this->db->get('client_list_csv');
        return $save->result();
    }
    function delete_csv($idcom){
        $this->db->where('IdCommercant', $idcom);
        $this->db->delete('client_list_csv');
    }
    function delete_stat_news($id){
        $this->db->where('id', $id);
        $this->db->delete('Newsletter_stat');
    }



 
    
    function getId($iduser){
        $this->db->where('IdCommercant',$iduser);
        $save=$this->db->get('bloc_newsletter');
        return $save->result();    

    }

    function recuperer($iduser){
      $this->db->select('*');
      $this->db->where('IdCommercant',$iduser);
      $save=$this->db->get('bloc_newsletter');
        return $save->result();
    }
    function delete_img_newsletter($iduser,$prmFiles){
      $field=array('IdCommercant'=>$iduser,
          $prmFiles=>'' );
        $this->db->where('IdCommercant',$iduser);
        $this->db->update('bloc_newsletter',$field);
    }

}   