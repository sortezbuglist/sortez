<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class newsletter_unsubscribe extends CI_Model {
	
	private $_table = 'newsletter_unsubscribe';
	
	public function __construct() {
		parent::__construct ();
	}
	
	public function get_all($criteres = array()) {
		$this->db->select('*');
		$this->db->from($this->_table);
		if(!empty($criteres)){
			$this->db->where($criteres);
		}
		$query = $this->db->get();
		return $query->result();
	}
	
	public function insert($data) {
		$this->db->insert($this->_table, $data);
		return $this->db->insert_id();
	}

	function update($prmData) {
		$prmData = (array)$prmData;
		$this->db->where($this->_table.'.id', $prmData["id"]);
		$this->db->update($this->_table, $prmData);
		return $this->get_by_id($prmData["id"]);
	}
	
	public function get_by_id($id){
		$this->db->select('*');
		$this->db->from($this->_table);
		$this->db->where(array('id_client'=>$id));
		
		$query = $this->db->get();
		return  ($query->num_rows() > 0) ? $query->first_row() : false;
	}
	
	public function get_by_mail($mail){
		$this->db->select('*');
		$this->db->from($this->_table);
		$this->db->where($this->_table.'.mail', $mail);
		$query = $this->db->get();
        return $query->result();
	}

	public function delete($where){
		return $this->db->delete($this->_table, $where);
	}
	
}
