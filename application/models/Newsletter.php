<?php 
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class newsletter extends CI_Model {

    private $_table = 'newsletter';

    public function __construct() {
        parent::__construct ();
    }

    public function getAll($criteres = array()) {
        $this->db->select('*');
        $this->db->from($this->_table);
        $this->db->order_by('id desc');
        if(!empty($criteres)){
            $this->db->where($criteres);
        }
        $query = $this->db->get();
        return $query->result();
    }

    public function insert($data) {
        $this->db->insert($this->_table, $data);
        return $this->db->insert_id();
    }

    function update($prmData) {
        $prmData = (array)$prmData;
        $this->db->where($this->_table.'.id', $prmData["id"]);
        $this->db->update($this->_table, $prmData);
        return $this->get_by_id($prmData["id"]);
    }

    public function getById($id){
        $this->db->select('*');
        $this->db->from($this->_table);
        $this->db->where(array('id'=>$id));

        $query = $this->db->get();
        return  ($query->num_rows() > 0) ? $query->first_row() : false;
    }

    public function getByEmail($mail){
        $this->db->select('*');
        $this->db->from($this->_table);
        $this->db->where($this->_table.'.email_receiver', $mail);
        $query = $this->db->get();
        return  ($query->num_rows() > 0) ? $query->first_row() : false;
    }

    public function delete($id=0){
        $this->db->where('id', $id);
        return $this->db->delete($this->_table);
    }
    public function save_newsletter_stat($field){
    $save= $this->db->insert('save_newsletter_stat',$field);
       if ($save){
           return 1;
       }else{
           return 0;
       } 
    }
    public function get_saved_mail(){
       $result = $this->db->get('save_newsletter_stat');
       return $result->result();
    }

    public function delete_newsletter_stat($id){
    $this->db->where('id', $id);
    $delete_stat= $this->db->delete('save_newsletter_stat');
       if ($delete_stat){
           return 1;
       }else{
           return 0;
       } 
    }
    function get_data_by_id($id){
        $this->db->where('id',$id);
        $loaded=$this->db->get('save_newsletter_stat');
        return $loaded->row();
    }
    public function changeVille($idcom,$idville,$codepostal,$adresse,$location){
        $this->db->where('IdCommercant',$idcom);
        $res = $this->db->get('agenda');
        if ($res->num_rows() > 1){
            $list = $res->result();
            if (isset($list)){
                $arrays = array(
                    "IdVille" => $idville,
                    "IdVille_localisation" =>$idville,
                    "codepostal" => "06400",
                    "codepostal_localisation" =>"06400",
                    "adresse1" =>$adresse,
                    "adresse_localisation" =>$adresse,
                    "location_id"=>$location
                );
            }
            foreach ($list as $lists){
                $this->db->where("id",$lists->id);
                $this->db->update('agenda',$arrays);
            }
        }elseif($res->num_rows() ===1){
            $list = $res->row();
            if (isset($list)){
                $arrays = array(
                    "IdVille" => $idville,
                    "IdVille_localisation" =>$idville,
                    "codepostal" => $codepostal,
                    "codepostal_localisation" =>$codepostal,
                    "adresse1" =>$adresse,
                    "adresse_localisation" =>$adresse
                );
                $this->db->where("id",$list->id);
                $this->db->update('agenda',$arrays);
            }
        }
    return true;
    }
    
}