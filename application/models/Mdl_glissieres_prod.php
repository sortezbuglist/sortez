<?php

class Mdl_glissieres_prod extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function getById($id = 0)
    {
        $Sql = "select * from glissieres_prod where id_glissiere =" . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getWhere($where = '')
    {
        if (isset($where) && $where!='') {
            $Sql = " select * from glissieres_prod where 0=0 AND ".$where;
            $Query = $this->db->query($Sql);
        }
        if (isset($Query) && $Query->num_rows() > 0) {
            return $Query->result();
        } else return null;
    }

    function GetAll()
    {
        $qryLocalisation = $this->db->query("
            SELECT * 
            FROM
                glissieres_prod
            ORDER BY id_glissiere DESC 
        ");
        if ($qryLocalisation->num_rows() > 0) {
            return $qryLocalisation->result();
        }
    }

    function delete($prmId)
    {

        $qryBonplan = $this->db->query("DELETE FROM glissieres_prod WHERE id_glissiere = ?", $prmId);
        return $qryBonplan;
    }

    function insert($prmData)
    {
        $this->db->insert("glissieres_prod", $prmData);
        return $this->db->insert_id();
    }

    function update($prmData)
    {
        $this->db->where("id_glissiere", $prmData["id_glissiere"]);
        $this->db->update("glissieres_prod", $prmData);
        $objAnnonce = $this->getById($prmData["id_glissiere"]);
        return $objAnnonce->id_glissiere;
    }
}



































