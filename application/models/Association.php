<?php
class association extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
	function GetAutocomplete($queryString=0){
		$Sql = "select * from associations where Nom like '%$queryString%'  order by Nom " ;		
		$Query = $this->db->query($Sql);
		return $Query->result();
	}
	function getById($id=0){
		$Sql = "select * from associations where IdAssociation = ". $id  ;		
		$Query = $this->db->query($Sql);
		return $Query->row();
	}
    function GetAll(){
        $qryassociations = $this->db->query("
            SELECT
                *
            FROM
                associations
            ORDER BY Nom ASC
        ");
        if($qryassociations->num_rows() > 0) {
            return $qryassociations->result();
        }
    }


    function Insert($prmData) {
        $this->db->insert("associations", $prmData);
        return $this->getById($this->db->insert_id());
    }
    
    function Update($prmData) {
        $prmData = (array)$prmData;
        $this->db->where("IdAssociation", $prmData["IdAssociation"]);
        $this->db->update("associations", $prmData);
        return $this->GetById($prmData["IdAssociation"]);
    }

    function getWhere($prmWhere = "0 = 0", $prmOffset = 0, $prmLimit = 0, $prmOrder = " associations.Nom ASC ") {
        if (empty($prmOrder)) {
            $prmOrder = " associations.Nom ASC ";
        }
        
        
        $qryString = "
            SELECT
                associations.*,
                villes.Nom AS 'NomVille'
            FROM
                (associations
                LEFT JOIN villes ON villes.IdVille = associations.IdVille)
            WHERE " . $prmWhere . "
            ORDER BY " . $prmOrder . "";
        if (!(($prmOffset == 0) and ($prmLimit == 0))) {
            $qryString .= " LIMIT $prmOffset, $prmLimit";
        }
        return $this->db->query($qryString)->result();
    }

    function CountWhere_pvc($prmWhere = "0=0") {
        $Res = $this->db->query("SELECT COUNT(IdAssociation) AS 'Decompte' FROM (associations LEFT JOIN villes On villes.IdVille = associations.IdVille) WHERE " . $prmWhere)->result();
        return $Res[0]->Decompte;
    }



    

}