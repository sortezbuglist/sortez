<?php
class mdl_departement extends CI_Model{
	 function __construct() {
        parent::__construct();
    }
	
	 function GetAll(){
        $qryVilles = $this->db->query("
            SELECT
				departement.departement_nom,
				departement.departement_id
				FROM
				departement
      ");
        if($qryVilles->num_rows() > 0) {
            return $qryVilles->result();
        }
    }
	
	// function getById($id){

	// 	$this->db->where("departement_id", $id);
	// 	$this->db->get("departement_nom");
	// 	return result();
	// }
	
	function update($prmData) {
        $this->db->where("departement_id", $prmData["departement_id"]);
        $this->db->update("departement", $prmData);
        $objResult = $this->getById($prmData["departement_id"]);
        return $objResult->departement_id;
    }
	
}