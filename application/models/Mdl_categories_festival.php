<?php

class mdl_categories_festival extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function getById($id = 34)
    {
        $Sql = "select * from agenda_categ where agenda_categid =" . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByIdSousCateg($id = 0)
    {
        $Sql = "select * from agenda_subcateg where agenda_subcategid =" . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function GetAll()
    {
        $qryfestival_type = $this->db->query("
            SELECT *
            FROM
                agenda_categ
            ORDER BY category ASC
        ");
        if ($qryfestival_type->num_rows() > 0) {
            return $qryfestival_type->result();
        }
    }

    function Insert($prmData)
    {
        $this->db->insert("agenda_categ", $prmData);
        return $this->db->insert_id();
    }

    function Update($prmData)
    {
        $prmData = (array)$prmData;
        $this->db->where("festival_categid", $prmData["festival_categid"]);
        $this->db->update("festival_categ", $prmData);
        $objCommercant = $this->GetById($prmData["festival_categid"]);
        return $objCommercant->festival_categid;
    }

    function Delete($prmId)
    {
        $qryBonplan = $this->db->query("DELETE FROM festival_categ WHERE festival_categid = ?", $prmId);
        return $qryBonplan;
    }


    function InsertSousCateg($prmData)
    {
        $this->db->insert("festival_subcateg", $prmData);
        return $this->db->insert_id();
    }

    function UpdateSousCateg($prmData)
    {
        $prmData = (array)$prmData;
        $this->db->where("festival_subcategid", $prmData["festival_subcategid"]);
        $this->db->update("festival_subcateg", $prmData);
        $objCommercant = $this->GetByIdSousCateg($prmData["festival_subcategid"]);
        return $objCommercant->festival_subcategid;
    }

    function DeleteSousCateg($prmId)
    {
        $qryBonplan = $this->db->query("DELETE FROM festival_subcateg WHERE festival_subcategid = ?", $prmId);
        return $qryBonplan;
    }

    function GetAllSousrubrique()
    {
        $qryCategorie = $this->db->query("
            SELECT *
            FROM
                agenda_subcateg
            ORDER BY subcateg ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAllSousrubriqueByRubrique($IdRubrique)
    {
        $qryCategorie = $this->db->query("
            SELECT *
            FROM
                agenda_subcateg
            WHERE
                agenda_categid = '" . $IdRubrique . "'
            ORDER BY subcateg ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function verifier_categorie_festival($IdRubrique)
    {
        $qryCategorie = $this->db->query("
            SELECT
            festival.*
            FROM
            festival
            WHERE
            festival.article_categid = '" . $IdRubrique . "'
       ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function verifier_souscategorie_article($IdSousRubrique)
    {
        $qryCategorie = $this->db->query("
            SELECT
            festival.*
            FROM
            festival
            WHERE
            festival.festival = '" . $IdSousRubrique . "'
       ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAllCategByTypeArticle($article_typeid)
    {
        $qryCategorie = $this->db->query("
            SELECT *
            FROM
                festival
            WHERE
                festival = '" . $article_typeid . "'
            ORDER BY category ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAllSousCategByTypeArticle($article_categid)
    {
        $qryCategorie = $this->db->query("
            SELECT *
            FROM
                festival
            WHERE
                festival = '" . $article_categid . "'
            ORDER BY subcateg ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetArticleSouscategorieByRubrique($idRubrique=34, $inputQuand = "0", $inputDatedebut = "", $inputDatefin = "", $_iDepartementId = "0", $_iVilleId = "0")
    {

        $zSqlListePartenaire = "
            SELECT
            agenda_subcateg.agenda_subcategid,
            agenda_subcateg.agenda_categid,
            agenda_subcateg.subcateg,
            COUNT(festival.id) as nb_festival,
            COUNT(festival.id) as nb_festival_id
            FROM
            agenda_subcateg
            INNER JOIN festival ON agenda_subcateg.agenda_subcategid = festival.festival_subcategid
            LEFT OUTER JOIN festival ON festival.festival_id = festival.id
            WHERE
            agenda_subcateg.agenda_categid = '" . $idRubrique . "'
            AND
            (festival.IsActif = 1 OR festival.IsActif = 2)
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlListePartenaire .= " AND festival.IdVille_localisation = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlListePartenaire .= " AND festival.IdVille_localisation = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlListePartenaire .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlListePartenaire .= " festival.IdVille_localisation = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListePartenaire .= " OR ";
            }
            $zSqlListePartenaire .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlListePartenaire .= " AND festival.IdVille_localisation IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));

            if ($inputQuand == "1") $zSqlListePartenaire .= " AND festival.date_debut = '" . $date_of_day . "'";
            if ($inputQuand == "2") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $next_saturday_day . "' AND festival.date_fin <= '" . $next_sunday_day . "' )";
            if ($inputQuand == "3") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $last_sunday_day . "' AND festival.date_fin <= '" . $next_saturday_day . "' )";
            if ($inputQuand == "4") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $last_sunday_of_next_monday . "' AND festival.date_fin <= '" . $next_saturday_of_next_monday . "' )";
            if ($inputQuand == "5") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_month . "' AND festival.date_fin <= '" . $last_day_month . "' )";
        }

        if ($_iVilleId != "" && $_iVilleId != NULL && $_iVilleId != "0") {
            $zSqlListePartenaire .= " AND festival.IdVille_localisation = '" . $_iVilleId . "'";
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " festival.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }

        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null && $inputDatedebut != "0") {
            $zSqlListePartenaire .= " AND festival.date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null && $inputDatefin != "0") {
            $zSqlListePartenaire .= " AND festival.date_fin = '" . $inputDatefin . "'";
        }

        $zSqlListePartenaire .= "
            GROUP BY
            agenda_subcateg.agenda_subcategid
            ";

        //var_dump($zSqlListePartenaire);
        //echo $zSqlListePartenaire."<br/>"; //die();
        //log_message('error', 'CHECKING_BY_SUB_CATEG_NUMBER : ' . $zSqlListePartenaire);

        $qryCategorie = $this->db->query($zSqlListePartenaire);
        //if($qryCategorie->num_rows() > 0) {
        return $qryCategorie->result();
        //var_dump($qryCategorie->result()); die();
        //} else return "0";
    }


    function GetFestivalSouscategorieByRubrique($idRubrique, $inputQuand = "0", $inputDatedebut = "", $inputDatefin = "", $_iDepartementId = "0", $_iVilleId = "0")
    {

        $zSqlListePartenaire = "
            SELECT
            agenda_subcateg.agenda_subcategid,
            agenda_subcateg.agenda_categid,
            agenda_subcateg.subcateg,
            COUNT(festival.id) as nb_festival
            FROM
            agenda_subcateg
            INNER JOIN festival ON agenda_subcateg.agenda_subcategid = festival.article_subcategid
            WHERE
            agenda_subcateg.agenda_categid = '" . $idRubrique . "'
            AND
            (festival.IsActif = 1 OR festival.IsActif = 2)
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlListePartenaire .= " AND festival.IdVille_localisation = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlListePartenaire .= " AND festival.IdVille_localisation = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlListePartenaire .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlListePartenaire .= " festival.IdVille_localisation = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListePartenaire .= " OR ";
            }
            $zSqlListePartenaire .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlListePartenaire .= " AND festival.IdVille_localisation IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));

            if ($inputQuand == "1") $zSqlListePartenaire .= " AND agenda_datetime.date_debut = '" . $date_of_day . "'";
            if ($inputQuand == "2") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $next_saturday_day . "' AND festival.date_fin <= '" . $next_sunday_day . "' )";
            if ($inputQuand == "3") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $last_sunday_day . "' AND festival.date_fin <= '" . $next_saturday_day . "' )";
            if ($inputQuand == "4") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $last_sunday_of_next_monday . "' AND festival.date_fin <= '" . $next_saturday_of_next_monday . "' )";
            if ($inputQuand == "5") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_month . "' AND festival.date_fin <= '" . $last_day_month . "' )";
        }

        if ($_iVilleId != "" && $_iVilleId != NULL && $_iVilleId != "0") {
            $zSqlListePartenaire .= " AND festival.IdVille_localisation = '" . $_iVilleId . "'";
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " festival.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }

        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null && $inputDatedebut != "0") {
            $zSqlListePartenaire .= " AND festival.date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null && $inputDatefin != "0") {
            $zSqlListePartenaire .= " AND festival.date_fin = '" . $inputDatefin . "'";
        }

        $zSqlListePartenaire .= "
            GROUP BY
            agenda_subcateg.agenda_subcategid
            ";

        //var_dump($zSqlListePartenaire);
        //echo $zSqlListePartenaire."<br/>"; //die();
        //log_message('error', 'william : '.$zSqlListePartenaire);

        $qryCategorie = $this->db->query($zSqlListePartenaire);
        //if($qryCategorie->num_rows() > 0) {
        return $qryCategorie->result();
        //var_dump($qryCategorie->result()); die();
        //} else return "0";
    }




    function GetArticleCategorie_by_params($inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $_iDepartementId = null, $_iVilleId = "0")
    {
        $zSqlListePartenaire = "
            SELECT
                agenda_categ.agenda_categid,
                agenda_categ.category,
                COUNT(festival.id) AS nb_festival
                ";

        $zSqlListePartenaire .= "
            FROM
                festival
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = festival.article_categid

            WHERE
                (festival.IsActif = '1' OR festival.IsActif = '2')

        ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlListePartenaire .= " AND festival.IdVille_localisation = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlListePartenaire .= " AND festival.IdVille_localisation = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlListePartenaire .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlListePartenaire .= " festival.IdVille_localisation = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListePartenaire .= " OR ";
            }
            $zSqlListePartenaire .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlListePartenaire .= " AND festival.IdVille_localisation IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        if ($inputQuand != "0" && $inputQuand != "" && $inputQuand != null) {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $date_of_day . "' AND festival.date_fin <= '" . $date_of_day . "' )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $next_saturday_day . "' AND festival.date_fin <= '" . $next_sunday_day . "' )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $last_sunday_day . "' AND festival.date_fin <= '" . $next_saturday_day . "' )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $last_sunday_of_next_monday . "' AND festival.date_fin <= '" . $next_saturday_of_next_monday . "' )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_month . "' AND festival.date_fin <= '" . $last_day_month . "' )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( festival.date_debut >= '" . $first_day_user_month . "' AND festival.date_debut <= '" . $last_day_user_month . "' )";
        }

        if ($_iVilleId != "0" && $_iVilleId != "" && $_iVilleId != null) {
            $zSqlListePartenaire .= " AND festival.IdVille_localisation = '" . $_iVilleId . "'";
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " festival.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null && $inputDatedebut != "0") {
            $zSqlListePartenaire .= " AND festival.date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null && $inputDatefin != "0") {
            $zSqlListePartenaire .= " AND festival.date_fin = '" . $inputDatefin . "'";
        }

        $zSqlListePartenaire .= " group by agenda_categ.agenda_categid ";

        //$iOrderBy_value = " order by festival.date_debut asc ";//
        $iOrderBy_value = " order by agenda_categ.category asc ";

        $zSqlListePartenaire .= $iOrderBy_value;

        //echo $zSqlListePartenaire;
        log_message('error', "willart : " . $zSqlListePartenaire);
        ////$this->firephp->log($zSqlListePartenaire, 'query_category');

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        if ($zQueryListeBonPlan->num_rows() > 0) {
            return $zQueryListeBonPlan->result();
        }


    }


}