<?php
class mdlbonplanpagination extends CI_Model{

    function __construct() {
        parent::__construct();
    }
	function Compter() {
        $Sql = "select count(commercants.IdCommercant) as Nb from commercants";
        $Query = $this->db->query($Sql);

        if ($Query) {
            $Res = $Query->row();
            return $Res->Nb;
        }

        return 0;
    }
    function GetListeBonplanPagination($argLimit = 1000000000, $argOffset = 0) {
        $zSqlListeBonPlan = "
            SELECT
                bonplan.* ,commercants.Photo5 as Photo5,
                commercants.NomSociete AS NomSociete,
                villes.Nom AS ville,
                commercants.Adresse1 AS quartier,
                commercants.Adresse2 AS rue,
                commercants.IdCommercant
            FROM
                bonplan
                LEFT JOIN commercants ON commercants.IdCommercant = bonplan.bonplan_commercant_id
                LEFT JOIN villes ON villes.IdVille = commercants.IdVille
                LIMIT " .$argOffset. " , " .$argLimit;
        $zQueryListeBonPlan = $this->db->query($zSqlListeBonPlan) ;
        return $zQueryListeBonPlan->result() ;
    }
}