<?php
class Mdl_types_article_olivia extends CI_Model{

    function __construct() {
        parent::__construct();
    }

     function getById($id=0){
        $Sql = "select * from article_type-olivia where article_typeid =". $id;
        if(isset($id) && $id!=0 && $id!="" && is_numeric($id)) {
             $Query = $this->db->query($Sql);
            return $Query->row();
         } else {
             return null;
         }
     }


    function GetAll(){
        $qryarticle_type = $this->db->query("
            SELECT *
            FROM
                article_type_olivia
            ORDER BY article_type ASC
        ");
        if($qryarticle_type->num_rows() > 0) {
            return $qryarticle_type->result();
        }
    }


     function GetAllSousrubriqueByRubrique($IdRubrique)
    {
        $qryCategorie = $this->db->query("
            SELECT * 
            FROM
                sous_rubriques
            WHERE 
                IdRubrique = '" . $IdRubrique . "'
            ORDER BY Nom ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function supprimearticle_type($prmId){

        $qryTypeArticle = $this->db->query("DELETE FROM article_type_olivia WHERE article_typeid = ?", $prmId) ;
        return $qryTypeArticle ;
    }

    function insertarticle_type($prmData) {
        $this->db->insert("article_type", $prmData);
        return $this->db->insert_id();
    }

    function updatearticle_type($prmData) {
        $this->db->where("article_typeid", $prmData["article_typeid"]);
        $this->db->update("article_type", $prmData);
        $objTypeArticle = $this->getById($prmData["article_typeid"]);
        return $objTypeArticle->article_typeid;
    }

    function verifier_type_article($prmId){

        $querk = "SELECT
                    *
                    FROM
                    article_categ
                    WHERE
                    article_categ.article_typeid = '".$prmId."'";
        $qryarticle_type = $this->db->query($querk);
        if($qryarticle_type->num_rows() > 0) {
            return $qryarticle_type->result();
        }
    }

}