<?php

class Mdl_reservation extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    public function save_reservation_sejour($field){
       $this->db->insert('reservation_type_sejour',$field);
       $id_res=$this->db->insert_id();
       $field2=array(
           "IdCommercant"=>$field['Id_commercant'],
           "id_client"=>$field['id_client'],
           "etat_reservation"=>'1',
           "id_type_reservation"=>$id_res
       );
       $save_to_reservation_global1=$this->db->insert('reservations',$field2);
       if ($save_to_reservation_global1){
           return 1;
       }else{
           return 0;
       }

    }
    public function get_reservation_gite_by_idcom($idcom){
        $this->db->where('IdCommercant',$idcom);
        $res=$this->db->get('liste_gite_commercant');
        return $res->row();
    }
    public function get_Gite_by_id($id){
        $this->db->where('id',$id);
        $res=$this->db->get('liste_gite_commercant');
        return $res->row();
    }
    public function save_gite($field){
       $save= $this->db->insert('liste_gite_commercant',$field);
        if ($save){
            return '1';
        }else{
            return '0';
        }
    }
    public function update_gite($field){
        $this->db->where('id',$field['id']);
       $save= $this->db->update('liste_gite_commercant',$field);
       if ($save){
           return '1';
       }else{
           return '0';
       }
    }
    public function delete_gite($id){
        $this->db->where('id',$id);
        $this->db->delete('liste_gite_commercant');
    }

    public function save_reservation_table($field){
        $this->db->insert('reservation_type_table',$field);
        $id_res=$this->db->insert_id();
        $field2=array(
            "IdCommercant"=>$field['Id_commercant'],
            "id_client"=>$field['id_client'],
            "etat_reservation"=>'1',
            "id_type_reservation"=>$id_res
        );
        $save_to_reservation_global1=$this->db->insert('reservations',$field2);
        if ($save_to_reservation_global1){
            return 1;
        }else{
            return 0;
        }

    }
    public function get_gie_infoby_idcom($idcom){
    $this->db->where('IdCommercant',$idcom);
   $res= $this->db->get('infocom_reservation');
   return $res->row();

    }
}