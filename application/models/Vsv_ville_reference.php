
<?php
class vsv_ville_reference extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
	function getById($id=0){
		$Sql = "select * from vsv_ville_reference where id =". $id  ;
		$Query = $this->db->query($Sql);
		return $Query->row();
	}



    function getAll(){
        $request = $this->db->query("
            SELECT
                *
            FROM
                vsv_ville_reference
            ORDER BY id DESC
        ");
        if($request->num_rows() > 0) {
            return $request->result();
        }
    }

	function getWhere($where=''){
        $sql = "
            SELECT
                *
            FROM
                vsv_ville_reference
            WHERE 
              0=0 
        ";
        if (isset($where) && $where != '') $sql .= " AND ".$where;
        $request = $this->db->query($sql);
        if($request->num_rows() > 0) {
            return $request->result();
        }
    }

    function insert($prmData) {
        $this->db->insert("vsv_ville_reference", $prmData);
        return $this->db->insert_id();
    }

    function update($prmData) {
        $this->db->where("id", $prmData["id"]);
        $this->db->update("vsv_ville_reference", $prmData);
        $objA = $this->getById($prmData["id"]);
        return $objA->id;
    }

    function delete($prmId=0){
        $qryBonplan = $this->db->query("DELETE FROM vsv_ville_reference WHERE id = ?", $prmId);
        return $qryBonplan ;
    }

    function deleteWhere($where=""){
        $sql = " DELETE FROM vsv_ville_reference WHERE ";
        if (isset($where) && $where != "") {
            $sql .= $where;
            $qryBonplan = $this->db->query($sql);
            return $qryBonplan ;
        } else return null;

    }


}