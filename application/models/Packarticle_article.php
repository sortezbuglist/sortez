
<?php
class packarticle_article extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
	function getById($id=0){
        if (isset($id)&&$id!=0&&$id!=''){
            $Sql = "SELECT * FROM packarticle_article WHERE id =". $id  ;
            $Query = $this->db->query($Sql);
            return $Query->row();
        } else return null;
	}

    function getAll(){
        $request = $this->db->query("SELECT
                *
            FROM
                packarticle_article
            ORDER BY id DESC
        ");
        if($request->num_rows() > 0) {
            return $request->result();
        }
    }

    function getAll_valuejoin($id_user_ionauth=1, $id_planing=0, $id_status=0){
        $sql = " SELECT
            packarticle_article.*,
            packarticle_order.user_id
            FROM
            packarticle_article
            INNER JOIN packarticle_order ON packarticle_article.order_id = packarticle_order.id 
            WHERE 0=0 ";
        if (isset($id_user_ionauth)&& $id_user_ionauth!=1 && $id_user_ionauth!='' && $id_user_ionauth!=false) $sql .= " AND packarticle_order.user_id = ".$id_user_ionauth;
        if (isset($id_planing)&& $id_planing!=0 && $id_planing!='' && $id_planing!=false) $sql .= " AND packarticle_article.planing_id = ".$id_planing;
        if (isset($id_status)&& $id_status!=0 && $id_status!='' && $id_status!=false) $sql .= " AND packarticle_article.status_id = ".$id_status;
        $request = $this->db->query($sql);
        if($request->num_rows() > 0) {
            return $request->result();
        }
    }

	function getWhere($where=''){
        $sql = "SELECT
                *
            FROM
                packarticle_article
            WHERE 
              0=0 
        ";
        if (isset($where) && $where != '') $sql .= " AND ".$where;
        $request = $this->db->query($sql);
        if($request->num_rows() > 0) {
            return $request->result();
        }
    }

    function insert($prmData) {
        $this->db->insert("packarticle_article", $prmData);
        return $this->db->insert_id();
    }

    function update($prmData) {
        $this->db->where("id", $prmData["id"]);
        $this->db->update("packarticle_article", $prmData);
        $objA = $this->getById($prmData["id"]);
        return $objA->id;
    }

    function delete($prmId=0){
        $qryBonplan = $this->db->query("DELETE FROM packarticle_article WHERE id = ?", $prmId);
        return $qryBonplan ;
    }
    public function getusernameionauth(){
        $this->db->select('*');
        $this->db->from('packarticle_order');
        $this->db->join('users_ionauth','packarticle_order.user_id=users_ionauth.id');
        $query=$this->db->get();
        return $query->result();


    }
    public function geteditionpackartplanning(){
        $this->db->select('*');
        $this->db->from('packarticle_article');
        $this->db->join('packarticle_planing','packarticle_article.planing_id=packarticle_planing.id');
        $query=$this->db->get();
        return $query->result();

    }


    public function filter($idtrue){
        $username=$this->input->post('username');
        $edit=$this->input->post('edit');


        $idedit= $username=$this->input->post('edit');
        $idetat= $username=$this->input->post('etat');

        $this->db->select('*');
        $this->db-> from('packarticle_article');
        $this->db->join('packarticle_order','packarticle_order.id = packarticle_article.order_id');
        $this->db->where_in('packarticle_order.user_id', $username);
        $fil=$this->db->get();
        return $fil->row();



        //$etat=$this->input->post('etat');
        /* $filtre="
             SELECT *
      FROM       packarticle_order AS username, packarticle_planing AS edition,
             WHERE username.id=$username,
             AND edition.id=$edit,
             INNER JOIN edition ON username.ID = edition.id
                   ";
        return $filtre->result();*/
    }

}

/*
SELECT *
FROM news AS n, auteur AS a
WHERE n.id_auteur = a.id_auteur
AND nom="DUPONT"
AND prenom="Marchel"*/