<?php
class mdldepartement extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
	function GetAutocomplete($queryString=0){
		$Sql = "select * from departement where departement_nom like '%$queryString%'  order by departement_nom " ;		
		$Query = $this->db->query($Sql);
		return $Query->result();
	}
	function getById($id=0){
		$Sql = "select * from departement where departement_id =". $id ;		
		$Query = $this->db->query($Sql);
		return $Query->row();
	}
	function GetAll(){
        $qryVille = $this->db->query("
            SELECT departement_id, departement_code, departement_nom
            FROM
                departement
            ORDER BY departement_nom ASC
        ");
        if($qryVille->num_rows() > 0) {
            return $qryVille->result();
        }
    }
    function getVillesByIdDepartement($departement_id = 0){
        $query = "
            SELECT
            departement.departement_id,
            departement.departement_code,
            villes.IdVille,
            villes.Nom,
            villes.CodePostal
            FROM
            departement
            INNER JOIN villes ON villes.ville_departement = departement.departement_code
            ";
        if (isset($departement_id) && intval($departement_id) != 9999)
        $query .= " WHERE
            departement.departement_id = '".$departement_id."'
            ";
        $query .= " GROUP BY
            villes.IdVille
        ";
        $qryVille = $this->db->query($query);
        if($qryVille->num_rows() > 0) {
            return $qryVille->result();
        }
    }
    
    function delete($prmId){
    
        $qryBonplan = $this->db->query("DELETE FROM departement WHERE departement_id = ?", $prmId) ;
        return $qryBonplan ;
    }

    function insert($prmData) {
        $this->db->insert("departement", $prmData);
        return $this->db->insert_id();
    }

    function update($prmData) {
        $this->db->where("departement_id", $prmData["departement_id"]);
        $this->db->update("departement", $prmData);
        $objResult = $this->getById($prmData["departement_id"]);
        return $objResult->departement_id;
    }

    function GetBonplanDepartements(){
        
        $sqlcat = "
          SELECT
            departement.departement_id,
            departement.departement_code,
            departement.departement_nom,
            COUNT(IdCommercant) as nbCommercant
            FROM
            bonplan
            Inner Join commercants ON bonplan.bonplan_commercant_id = commercants.IdCommercant
            Inner Join villes ON villes.IdVille = commercants.IdVille
            Inner Join departement ON departement.departement_code = villes.ville_departement
            where commercants.IsActif = 1 ";


        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcat .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcat .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcat .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
            }
            $sqlcat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcat .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
            GROUP BY
            departement.departement_id ";

        $qryCategorie = $this->db->query($sqlcat);
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetFidelityDepartements(){
        
        $sqlcat = "
          SELECT
            departement.departement_id,
            departement.departement_code,
            departement.departement_nom,
            COUNT(IdCommercant) as nbCommercant
            FROM
            departement
            Inner Join villes ON villes.ville_departement = departement.departement_code
            Inner Join commercants ON  commercants.IdVille = villes.IdVille
            where commercants.IsActif = 1
            AND (commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_remise) 
            OR commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_tampon) 
            or commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_capital))
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcat .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcat .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcat .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
            }
            $sqlcat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcat .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
            GROUP BY
            departement.departement_id ";

        $qryCategorie = $this->db->query($sqlcat);
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetFidelityDepartements_soutenons(){

        $sqlcat = "
          SELECT
            departement.departement_id,
            departement.departement_code,
            departement.departement_nom,
            COUNT(commercants.IsActif) as nbCommercant
            FROM
            departement
            Inner Join villes ON villes.ville_departement = departement.departement_code
            Inner Join commercants ON  commercants.IdVille = villes.IdVille
            where commercants.IsActif = 1
            AND (commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_remise WHERE card_remise.is_activ = 1 AND NOW() BETWEEN    card_remise.date_debut AND card_remise.date_fin ) 
            OR commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_tampon WHERE card_tampon.is_activ = 1 AND NOW() BETWEEN    card_tampon.date_debut AND card_tampon.date_fin) 
            or commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_capital WHERE card_capital.is_activ = 1 AND NOW() BETWEEN    card_capital.date_debut AND card_capital.date_fin))
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcat .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcat .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcat .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
            }
            $sqlcat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcat .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
            GROUP BY
            departement.departement_id ";

        $qryCategorie = $this->db->query($sqlcat);
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAnnuaireDepartements_pvc(){
        $sqlcat = "
          SELECT
            departement.departement_id,
            departement.departement_code,
            departement.departement_nom,
            COUNT(id) as nbCommercant
            FROM
            annuaire
            Inner Join villes ON villes.IdVille = annuaire.IdVille_localisation
            Inner Join departement ON departement.departement_code = villes.ville_departement
            where annuaire.IsActif = 1 ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcat .= " AND villes.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcat .= " AND villes.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcat .= " villes.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
            }
            $sqlcat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcat .= " AND villes.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
            GROUP BY
            departement.departement_id ";

        $qryCategorie = $this->db->query($sqlcat);
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAgendaDepartements_pvc(){
        
        $sqlcat = "
          SELECT
            departement.departement_id,
            departement.departement_code,
            departement.departement_nom,
            COUNT(id) as nbCommercant
            FROM
            agenda
            Inner Join villes ON villes.IdVille = agenda.IdVille_localisation
            Inner Join departement ON departement.departement_code = villes.ville_departement
            where agenda.IsActif = 1 ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcat .= " AND villes.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcat .= " AND villes.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcat .= " villes.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
            }
            $sqlcat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcat .= " AND villes.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
            GROUP BY
            departement.departement_id ";

        $qryCategorie = $this->db->query($sqlcat);
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }
	
    function GetArticleDepartements_pvc(){
        
        $sqlcat = "
          SELECT
            departement.departement_id,
            departement.departement_code,
            departement.departement_nom,
            COUNT(id) as nbCommercant
            FROM
            article
            Inner Join villes ON villes.IdVille = article.IdVille_localisation
            Inner Join departement ON departement.departement_code = villes.ville_departement
            where article.IsActif = 1 ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcat .= " AND villes.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcat .= " AND villes.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcat .= " villes.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
            }
            $sqlcat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcat .= " AND villes.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
            GROUP BY
            departement.departement_id ";

        $qryCategorie = $this->db->query($sqlcat);
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }	

    function GetFestivalDepartements_pvc(){
        
        $sqlcat = "
          SELECT
            departement.departement_id,
            departement.departement_code,
            departement.departement_nom,
            COUNT(id) as nbCommercant
            FROM
            festival
            Inner Join villes ON villes.IdVille = festival.IdVille_localisation
            Inner Join departement ON departement.departement_code = villes.ville_departement
            where festival.IsActif = 1 ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcat .= " AND villes.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcat .= " AND villes.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcat .= " villes.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
            }
            $sqlcat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcat .= " AND villes.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
            GROUP BY
            departement.departement_id ";

        $qryCategorie = $this->db->query($sqlcat);
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }   

    function GetBonplanDepartement_pvc(){
        $sqlcat = "
          SELECT
            departement.departement_id,
            departement.departement_code,
            departement.departement_nom,
            COUNT(bonplan_id) as nb_bonplan
            FROM
            bonplan
            Inner Join villes ON villes.IdVille = bonplan.bonplan_ville_id
            Inner Join departement ON departement.departement_code = villes.ville_departement
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcat .= " AND villes.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcat .= " AND villes.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcat .= " villes.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
            }
            $sqlcat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcat .= " AND villes.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
            GROUP BY
            departement.departement_id ";

        $qryCategorie = $this->db->query($sqlcat);
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetBonplanDepartement_pvc_soutenons(){
        $sqlcat = "
          SELECT
            departement.departement_id,
            departement.departement_code,
            departement.departement_nom,
            COUNT(bonplan_id) as nbCommercant
            FROM
            bonplan
            Inner Join villes ON villes.IdVille = bonplan.bonplan_ville_id
            Inner Join departement ON departement.departement_code = villes.ville_departement
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcat .= " AND villes.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcat .= " AND villes.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcat .= " villes.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
            }
            $sqlcat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcat .= " AND villes.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
            GROUP BY
            departement.departement_id ";

        $qryCategorie = $this->db->query($sqlcat);
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }



    function GetAnnonceDepartements(){
        
        $sqlcat = "
          SELECT
            departement.departement_id,
            departement.departement_code,
            departement.departement_nom,
            COUNT(IdCommercant) as nbCommercant
            FROM
            annonce
            Inner Join commercants ON annonce.annonce_commercant_id = commercants.IdCommercant
            Inner Join villes ON villes.IdVille = commercants.IdVille
            Inner Join departement ON departement.departement_code = villes.ville_departement
            where commercants.IsActif = 1 ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcat .= " AND villes.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcat .= " AND villes.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcat .= " villes.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
            }
            $sqlcat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcat .= " AND villes.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
            GROUP BY
            departement.departement_id ";

        $qryCategorie = $this->db->query($sqlcat);
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }



    function GetDepByCodePostal($CodePostal = "0"){

        if ($CodePostal != "0") {

            $qry = "
                    SELECT departement_id, departement_code, departement_nom
                    FROM
                    departement
                    INNER JOIN villes ON villes.ville_departement = departement.departement_code
                    WHERE
                    villes.CodePostal LIKE '%".$CodePostal."%'
                    GROUP BY
                    departement.departement_id
                    ORDER BY
                    departement.departement_nom ASC
                    ";

            $qryVille = $this->db->query($qry);
            if($qryVille->num_rows() > 0) {
                return $qryVille->result();
            }
        }
        
    }

    function GetArticleDepartements_pvc_revue(){

        $sqlcat = "
          SELECT
            departement.departement_id,
            departement.departement_code,
            departement.departement_nom,
            COUNT(id) as nbCommercant
            FROM
            article
            Inner Join villes ON villes.IdVille = article.IdVille_localisation
            Inner Join departement ON departement.departement_code = villes.ville_departement
            where article.IsActif = 1 AND article.agenda_article_type_id='3'";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcat .= " AND villes.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcat .= " AND villes.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcat .= " villes.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
            }
            $sqlcat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcat .= " AND villes.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
            GROUP BY
            departement.departement_id ";

        $qryCategorie = $this->db->query($sqlcat);
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }
//    function GetplatDepartements_pvc(){
//
//        $sqlcat = "
//          SELECT
//            departement.departement_id,
//            departement.departement_code,
//            departement.departement_nom,
//            COUNT(id) as nbCommercant
//            FROM
//            commercants
//            INNER JOIN plat_du_jour ON plat_du_jour.IdCommercant = commercants.IdCommercant
//            Inner Join villes ON villes.IdVille = commercants.IdVille_localisation
//            Inner Join departement ON departement.departement_code = villes.ville_departement
//            where plat_du_jour.IsActif = 1 ";
//        //LOCALDATA FILTRE
//        $this_session_localdata =& get_instance();
//        $this_session_localdata->load->library('session');
//        $localdata_value = $this_session_localdata->session->userdata('localdata');
//        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
//        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
//        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
//        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
//        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
//            $sqlcat .= " AND villes.IdVille = '2031' ";
//        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
//            $sqlcat .= " AND villes.IdVille = '".$localdata_IdVille."' ";
//        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
//            $sqlcat .= " AND ( ";
//            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
//                $sqlcat .= " villes.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
//                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
//            }
//            $sqlcat .= " ) ";
//        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
//            $sqlcat .= " AND villes.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
//        }
//        //LOCALDATA FILTRE
//
//
//        $sqlcat .= "
//            GROUP BY
//            departement.departement_id ";
//
//        $qryCategorie = $this->db->query($sqlcat);
//        if($qryCategorie->num_rows() > 0) {
//            return $qryCategorie->result();
//        }
//    }
    function GetplatDepartements_pvc()
    {

        $sqlcat = "
          SELECT
            departement.departement_id,
            departement.departement_code,
            departement.departement_nom,
            COUNT(plat_du_jour.id) as nbCommercant
            FROM
            plat_du_jour
            Inner Join commercants ON plat_du_jour.IdCommercant = commercants.IdCommercant
            Inner Join villes ON villes.IdVille = commercants.IdVille_localisation
            Inner Join departement ON departement.departement_code = villes.ville_departement
            where plat_du_jour.IsActif = 1 ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $sqlcat .= " AND villes.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $sqlcat .= " AND villes.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $sqlcat .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $sqlcat .= " villes.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $sqlcat .= " OR ";
            }
            $sqlcat .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $sqlcat .= " AND villes.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
            GROUP BY
            departement.departement_id ";

        $qryCategorie = $this->db->query($sqlcat);
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }
}