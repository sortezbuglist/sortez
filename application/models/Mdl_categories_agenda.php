<?php

class mdl_categories_agenda extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function getById($id = 0)
    {
        $this->db->where("agenda_categid",$id);
        $res=$this->db->get("agenda_categ");
        return$res->row();
        //var_dump($id);die();
       /* $Sql = "select * from agenda_categ where  agenda_categid =" . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();*/
    }

    function getByIdSousCateg($id = 0)
    {
        $Sql = "select * from agenda_subcateg where agenda_subcategid =" . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByIdCateg($id = 0)
    {
        $Sql = "select * from agenda_categ where agenda_categid =" . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function GetAll()
    {
        $qryagenda_type = $this->db->query("
            SELECT *
            FROM
                agenda_categ 
            WHERE 
                agenda_categ.agenda_typeid != 9
           ORDER BY category ASC
        ");
        if ($qryagenda_type->num_rows() > 0) {
            return $qryagenda_type->result();
        }
    }

    function GetAllcodename()
    {
        $qryagenda_type = $this->db->query("

            SELECT *
            FROM
                codename
            
            ORDER BY nom ASC
            
        ");
        if ($qryagenda_type->num_rows() > 0) {
            return $qryagenda_type->result();
        }
    }

    function Insert($prmData)
    {
        $this->db->insert("agenda_categ", $prmData);
        return $this->db->insert_id();
    }

    function Update($prmData)
    {
        $prmData = (array)$prmData;
        $this->db->where("agenda_categid", $prmData["agenda_categid"]);
        $this->db->update("agenda_categ", $prmData);
        $objCommercant = $this->GetById($prmData["agenda_categid"]);
        return $objCommercant->agenda_categid;
    }

    function Delete($prmId)
    {
        $qryBonplan = $this->db->query("DELETE FROM agenda_categ WHERE agenda_categid = ?", $prmId);
        return $qryBonplan;
    }


    function InsertSousCateg($prmData)
    {
        $this->db->insert("agenda_subcateg", $prmData);
        return $this->db->insert_id();
    }

    function UpdateSousCateg($prmData)
    {
        $prmData = (array)$prmData;
        $this->db->where("agenda_subcategid", $prmData["agenda_subcategid"]);
        $this->db->update("agenda_subcateg", $prmData);
        $objCommercant = $this->GetByIdSousCateg($prmData["agenda_subcategid"]);
        return $objCommercant->agenda_subcategid;
    }

    function DeleteSousCateg($prmId)
    {
        $qryBonplan = $this->db->query("DELETE FROM agenda_subcateg WHERE agenda_subcategid = ?", $prmId);
        return $qryBonplan;
    }

    function GetAllSousrubrique()
    {
        $qryCategorie = $this->db->query("
            SELECT *
            FROM
                agenda_subcateg
            ORDER BY subcateg ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAllSousrubriqueByRubrique($IdRubrique)
    {
        $qryCategorie = $this->db->query("
            SELECT *
            FROM
                agenda_subcateg
            WHERE
                agenda_categid = '" . $IdRubrique . "'
            ORDER BY subcateg ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function verifier_categorie_agenda($IdRubrique)
    {
        $qryCategorie = $this->db->query("
            SELECT
            agenda.*
            FROM
            agenda
            WHERE
            agenda.agenda_categid = '" . $IdRubrique . "'
       ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function verifier_souscategorie_agenda($IdSousRubrique)
    {
        $qryCategorie = $this->db->query("
            SELECT
            agenda.*
            FROM
            agenda
            WHERE
            agenda.agenda_subcategid = '" . $IdSousRubrique . "'
       ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAllCategByTypeagenda($agenda_typeid)
    {
        $qryCategorie = $this->db->query("
            SELECT *
            FROM
                agenda_categ
            WHERE
                agenda_typeid = '" . $agenda_typeid . "'
            ORDER BY category ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAllSousCategByTypeagenda($agenda_categid)
    {
        $qryCategorie = $this->db->query("
            SELECT *
            FROM
                agenda_subcateg
            WHERE
                agenda_categid = '" . $agenda_categid . "'
            ORDER BY subcateg ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetAgendaSouscategorieByRubrique($idRubrique, $inputQuand = "0", $inputDatedebut = "", $inputDatefin = "", $_iDepartementId = "0", $_iVilleId = "0")
    {

        $zSqlListePartenaire = "
            SELECT
            agenda_subcateg.agenda_subcategid,
            agenda_subcateg.agenda_categid,
            agenda_subcateg.subcateg,
            COUNT(agenda_datetime.id) as nb_agenda,
            COUNT(agenda.id) as nb_agenda_id
            FROM
            agenda_subcateg
            INNER JOIN agenda ON agenda_subcateg.agenda_subcategid = agenda.agenda_subcategid
            LEFT OUTER JOIN agenda_datetime ON agenda_datetime.agenda_id = agenda.id
            WHERE
            agenda_subcateg.agenda_categid = '" . $idRubrique . "'
            AND
            (agenda.IsActif = 1 OR agenda.IsActif = 2)
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlListePartenaire .= " AND agenda.IdVille_localisation = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlListePartenaire .= " AND agenda.IdVille_localisation = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlListePartenaire .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlListePartenaire .= " agenda.IdVille_localisation = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListePartenaire .= " OR ";
            }
            $zSqlListePartenaire .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlListePartenaire .= " AND agenda.IdVille_localisation IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));

            if ($inputQuand == "1") $zSqlListePartenaire .= " AND agenda_datetime.date_debut = '" . $date_of_day . "'";
            if ($inputQuand == "2") $zSqlListePartenaire .= " AND ( agenda_datetime.date_debut >= '" . $next_saturday_day . "' AND agenda_datetime.date_fin <= '" . $next_sunday_day . "' )";
            if ($inputQuand == "3") $zSqlListePartenaire .= " AND ( agenda_datetime.date_debut >= '" . $last_sunday_day . "' AND agenda_datetime.date_fin <= '" . $next_saturday_day . "' )";
            if ($inputQuand == "4") $zSqlListePartenaire .= " AND ( agenda_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND agenda_datetime.date_fin <= '" . $next_saturday_of_next_monday . "' )";
            if ($inputQuand == "5") $zSqlListePartenaire .= " AND ( agenda_datetime.date_debut >= '" . $first_day_month . "' AND agenda_datetime.date_fin <= '" . $last_day_month . "' )";
        }

        if ($_iVilleId != "" && $_iVilleId != NULL && $_iVilleId != "0") {
            $zSqlListePartenaire .= " AND agenda.IdVille_localisation = '" . $_iVilleId . "'";
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " agenda.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }

        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null && $inputDatedebut != "0") {
            $zSqlListePartenaire .= " AND agenda_datetime.date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null && $inputDatefin != "0") {
            $zSqlListePartenaire .= " AND agenda_datetime.date_fin = '" . $inputDatefin . "'";
        }

        $zSqlListePartenaire .= "
            GROUP BY
            agenda_subcateg.agenda_subcategid
            ";

        //var_dump($zSqlListePartenaire); 
        //echo $zSqlListePartenaire."<br/>"; //die();
        //log_message('error', 'william : '.$zSqlListePartenaire);

        $qryCategorie = $this->db->query($zSqlListePartenaire);
        //if($qryCategorie->num_rows() > 0) {
        return $qryCategorie->result();
        //var_dump($qryCategorie->result()); die();
        //} else return "0";
    }


    function effacerimages_categ($_iCategId)
    {
        $zSql = "UPDATE agenda_categ SET images ='' WHERE agenda_categid =" . $_iCategId;
        $zQuery = $this->db->query($zSql);
    }

    function getCodename($codename=''){
        $sql = "
            SELECT
                agenda_categ.agenda_categid,
                agenda_categ.agenda_typeid,
                agenda_categ.category,
                agenda_categ.images,
                agenda_categ.codename 
            FROM
                agenda_categ 
            WHERE
                agenda_categ.codename = '".$codename."'
            ";
        $request = $this->db->query($sql);
        if($request->num_rows() > 0) {
            return $request->result();
        }
    }

    function getWhere($where=''){
        $sql = "
            SELECT
                agenda_categ.agenda_categid,
                agenda_categ.agenda_typeid,
                agenda_categ.category,
                agenda_categ.images,
                agenda_categ.codename
            FROM
                agenda_categ
            ";
        if (isset($where) && $where != '') $sql .= " WHERE ".$where;
        $request = $this->db->query($sql);
        if($request->num_rows() > 0) {
            return $request->result();
        }
    }
    function searchBycategName($search){
        $searchQuery = 'SELECT * FROM `agenda_categ` WHERE agenda_typeid != 9 AND category LIKE "%'.$search.'";';
        $queryResult = $this->db->query($searchQuery);
        return $queryResult->result();
    }


}