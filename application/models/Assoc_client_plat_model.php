<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class Assoc_client_plat_model extends CI_Model {

    public function __construct() {
        parent::__construct ();
    }

    public function get_list_reservation_plat_by_idcom($idcom){
    $this->db->select('reservation_plat.*,reservation_plat.id as idRes,plat_gli.*,plat_gli.id_plat as idplat*,card.*,users.*');
    $this->db->from('reservation_plat');
    $this->db->join('plat_gli','reservation_plat.IdPlat=plat_gli.id_plat');
    $this->db->join('card','reservation_plat.num_carte=card.num_id_card_virtual');
    $this->db->join('users','card.id_user=users.IdUser');
    $this->db->where('reservation_plat.IdCommercant',$idcom);
    $this->db->where('reservation_plat.etat','0');
    //$this->db->where('card.num_id_card_virtual','reservation_plat.num_carte');
    $res=$this->db->get();
    return $res->result();
    }
    public function valid_res_plat($id){
        $field=array('etat'=>'1',
            'date_validation'=>date('Y-m-d'));
        $this->db->where('id',$id);
        $this->db->update('reservation_plat',$field);
    }
    public function get_valided_by_idcom($idcom){
        $this->db->select('reservation_plat.*,reservation_plat.id as idRes,plat_du_jour.*,plat_du_jour.id as idplat*,card.*,users.*');
        $this->db->from('reservation_plat');
        $this->db->join('plat_du_jour','reservation_plat.IdPlat=plat_du_jour.id');
        $this->db->join('card','reservation_plat.num_carte=card.num_id_card_virtual');
        $this->db->join('users','card.id_user=users.IdUser');
        $this->db->where('reservation_plat.IdCommercant',$idcom);
        $this->db->where('reservation_plat.etat','1');
        //$this->db->where('card.num_id_card_virtual','reservation_plat.num_carte');
        $res=$this->db->get();
        return $res->result();
    }
    public function delete_res_plat($idres){
        $this->db->where('id',$idres);
        $this->db->delete('reservation_plat');
    }
    public function get_res_plat_user_by_iduser($iduser){
        $this->db->select('reservation_plat.*,reservation_plat.id as idRes,plat_du_jour.*,plat_du_jour.id as idplat*,card.*,users.*,commercants.*');
        $this->db->from('reservation_plat');
        $this->db->join('plat_du_jour','reservation_plat.IdPlat=plat_du_jour.id');
        $this->db->join('card','reservation_plat.num_carte=card.num_id_card_virtual');
        $this->db->join('commercants','reservation_plat.IdCommercant=commercants.IdCommercant');
        $this->db->join('users','card.id_user=users.IdUser');
        $this->db->where('users.IdUser',$iduser);
        $this->db->where('reservation_plat.etat','0');
        //$this->db->where('card.num_id_card_virtual','reservation_plat.num_carte');
        $res=$this->db->get();
        return $res->result();
    }
    public function get_res_plat_user_by_iduser_valided($iduser){

        $this->db->select('reservation_plat.*,reservation_plat.id as idRes,plat_du_jour.*,plat_du_jour.id as idplat*,card.*,users.*,commercants.*');
        $this->db->from('reservation_plat');
        $this->db->join('plat_du_jour','reservation_plat.IdPlat=plat_du_jour.id');
        $this->db->join('card','reservation_plat.num_carte=card.num_id_card_virtual');
        $this->db->join('commercants','reservation_plat.IdCommercant=commercants.IdCommercant');
        $this->db->join('users','card.id_user=users.IdUser');
        $this->db->where('users.IdUser',$iduser);
        $this->db->where('reservation_plat.etat','1');
        //$this->db->where('card.num_id_card_virtual','reservation_plat.num_carte');
        $res=$this->db->get();
        return $res->result();
    }
}
