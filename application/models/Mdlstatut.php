<?php
class mdlstatut extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
	function GetAutocompleteStatut($queryString=0){
		$Sql = "select * from statut where Nom like '%$queryString%'  order by Nom " ;		
		$Query = $this->db->query($Sql);
		return $Query->result();
	}
	function getStatutById($id=0){
		$Sql = "select * from statut where id =". $id  ;		
		$Query = $this->db->query($Sql);
		return $Query->row();
	}
	function GetAll(){
        $qryStatut = $this->db->query("
            SELECT * 
            FROM
                statut 
            ORDER BY id ASC
        ");
        if($qryStatut->num_rows() > 0) {
            return $qryStatut->result();
        }
    }
}