<?php

class Mdl_sitemap extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_commercant_372_742(){
        $querk = "SELECT * FROM commercants LIMIT 742 OFFSET 372 ";
        $qrytest = $this->db->query($querk);
        if($qrytest->num_rows() > 0) {
            return $qrytest->result();
        }
    }

    function get_art_372_742_by_idCom($idCom){
        $querk = "SELECT * FROM article WHERE  IdCommercant=".$idCom;
        $qrytest = $this->db->query($querk);
        if($qrytest->num_rows() > 0) {
            return $qrytest->result();
        }
    }
    function get_all_commercant_1_200(){
   $this->db->limit(371);
   //$this->db->limit(371);
   $res= $this->db->get('commercants');
   return $res->result();
    }
    function get_art_1_200_by_id($id){
        $this->db->where('IdCommercant',$id);
        $res=$this->db->get('article');
        return $res->result();
    }

    function get_ag_1_200_by_id($id){
        $this->db->where('IdCommercant',$id);
        $res=$this->db->get('agenda');
        return $res->result();
    }

    function get_bp_1_200_by_id($id){
        $this->db->where('bonplan_commercant_id',$id);
        $res=$this->db->get('bonplan');
        return $res->result();
    }
    function get_bout_1_200_by_id($id){
        $this->db->where('annonce_commercant_id',$id);
        $res=$this->db->get('annonce');
        return $res->result();
    }
    function get_plat_1_200_by_id($id){
        $this->db->where('IdCommercant',$id);
        $res=$this->db->get('plat_du_jour');
        return $res->result();
    }
    function get_ag_372_742_by_idCom($idCom){
        $querk = "SELECT * FROM agenda WHERE  IdCommercant=".$idCom;
        $qrytest = $this->db->query($querk);
        if($qrytest->num_rows() > 0) {
            return $qrytest->result();
        }
    }
    function get_bp_372_742_by_idCom($idCom){
        $querk = "SELECT * FROM bonplan WHERE  bonplan_commercant_id=".$idCom;
        $qrytest = $this->db->query($querk);
        if($qrytest->num_rows() > 0) {
            return $qrytest->result();
        }
    }
    function get_bout_372_742_by_idCom($idCom){
        $querk = "SELECT * FROM annonce WHERE  annonce_commercant_id=".$idCom;
        $qrytest = $this->db->query($querk);
        if($qrytest->num_rows() > 0) {
            return $qrytest->result();
        }
    }
    function get_all_agenda(){
       $res= $this->db->get('agenda');
       return $res->result();
    }

    function get_all_article(){
        $querk = "SELECT * FROM article";
        $qrytest = $this->db->query($querk);
        if($qrytest->num_rows() > 0) {
            return $qrytest->result();
        }
    }
    function get_all_commercants(){

        $querk = "SELECT * FROM commercants";
        $qrytest = $this->db->query($querk);
        if($qrytest->num_rows() > 0) {
            return $qrytest->result();
        }
    }
    function get_commercants_742_1113(){

        $querk = "SELECT * FROM commercants LIMIT 1113 OFFSET 743 ";
        $qrytest = $this->db->query($querk);
        if($qrytest->num_rows() > 0) {
            return $qrytest->result();
        }
    }
    function get_art_742_1113_by_id($id){
        $this->db->where('IdCommercant',$id);
        $res=$this->db->get('article');
        return $res->result();
    }

    function get_ag_742_1113_by_id($id){
        $this->db->where('IdCommercant',$id);
        $res=$this->db->get('agenda');
        return $res->result();
    }

    function get_bp_742_1113_by_id($id){
        $this->db->where('bonplan_commercant_id',$id);
        $res=$this->db->get('bonplan');
        return $res->result();
    }
    function get_bout_742_1113_by_id($id){
        $this->db->where('annonce_commercant_id',$id);
        $res=$this->db->get('annonce');
        return $res->result();
    }
    function get_plat_742_1113_by_id($id){
        $this->db->where('IdCommercant',$id);
        $res=$this->db->get('plat_du_jour');
        return $res->result();
    }
    function bonPlanParCommercant($_iCommercantId){

        $zSqlBonPlanParCommercant = "
        SELECT * , commercants.NomSociete AS NomSociete, villes.Nom AS ville,
        commercants.Adresse1 AS quartier, commercants.Adresse2 AS rue,
        commercants.IdCommercant
        FROM
        bonplan
        LEFT JOIN commercants ON commercants.IdCommercant=bonplan.bonplan_commercant_id
        LEFT JOIN villes ON villes.IdVille=commercants.IdVille
        WHERE 0=0
        ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlBonPlanParCommercant .= " AND commercants.IdVille = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlBonPlanParCommercant .= " AND commercants.IdVille = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlBonPlanParCommercant .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlBonPlanParCommercant .= " commercants.IdVille = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlBonPlanParCommercant .= " OR ";
            }
            $zSqlBonPlanParCommercant .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlBonPlanParCommercant .= " AND commercants.IdVille IN (SELECT IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE



        if (isset($_iCommercantId) && $_iCommercantId!=0 && $_iCommercantId!="" && $_iCommercantId!=NULL && $_iCommercantId!="0")
            $zSqlBonPlanParCommercant .= "
		    AND
        commercants.IdCommercant=" . $_iCommercantId ;

        $zQueryBonPlanParCommercant = $this->db->query($zSqlBonPlanParCommercant) ;
        //$toBonPlanParCommercant = $zQueryBonPlanParCommercant->result() ;
        $toBonPlanParCommercant = $zQueryBonPlanParCommercant->row() ;
        //return $toBonPlanParCommercant[0] ;
        return $toBonPlanParCommercant ;
    }

}