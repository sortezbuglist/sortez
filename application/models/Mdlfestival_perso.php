<?php
class mdlfestival_perso extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    function getarticle_persoById($id=0){
        $Sql = "select * from festival_perso where id =". $id  ;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }
    function GetAll(){
        $qryarticle_perso = $this->db->query("
            SELECT *
            FROM
                festival_perso
            ORDER BY id DESC
        ");
        if($qryarticle_perso->num_rows() > 0) {
            return $qryarticle_perso->result();
        }
    }


    function GetArticle_article_persoByUser($IdCommercant="0", $IdUsers_ionauth="0"){

        $qryCategorie_sql = "SELECT
                festival_perso.*
                FROM
                festival_perso
                WHERE
                0=0 ";
        if (isset($IdCommercant) && $IdCommercant!="0" && $IdCommercant!="" && $IdCommercant!=NULL && $IdCommercant!=0) {
            $qryCategorie_sql .= " AND festival_perso.IdCommercant = '".$IdCommercant."'";
        }
        if (isset($IdUsers_ionauth) && $IdUsers_ionauth!="0" && $IdUsers_ionauth!="" && $IdUsers_ionauth!=NULL && $IdUsers_ionauth!=0) {
            $qryCategorie_sql .= " AND festival_perso.IdUsers_ionauth = '".$IdUsers_ionauth."'";
        }
        $qryCategorie_sql .= " LIMIT 1";

        ////$this->firephp->log($qryCategorie_sql, 'qryCategorie_sql');

        $Query = $this->db->query($qryCategorie_sql);
        return $Query->row();
    }

    function GetArticle_article_persoByIdUsers_ionauth($user_ionauth_id){
        $qryCategorie = $this->db->query("
                SELECT
                festival_perso.Nom,
                festival_perso.Idarticle_perso,
                COUNT(festival.id) as nb_agenda
                FROM
                festival
                INNER JOIN festival_perso ON festival.Idarticle_perso_localisation = festival_perso.Idarticle_perso
                WHERE
                (festival.IsActif = 1 OR
                festival.IsActif = 2) AND
                festival.IdUsers_ionauth = '".$user_ionauth_id."'
                GROUP BY
                festival_perso.Idarticle_perso
                ORDER BY
                festival_perso.Nom ASC

        ");
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetCommercant_article_perso(){
        $qryCategorie = $this->db->query("
            SELECT
            commercants.IdCommercant,
            festival_perso.Idarticle_perso,
            festival_perso.Nom,
            festival_perso.CodePostal,
            festival_perso.NomSimple,
            commercants.IsActif,
            COUNT(commercants.IdCommercant) as nbCommercant
            FROM
            commercants
            Inner Join festival_perso ON commercants.Idarticle_perso = festival_perso.Idarticle_perso
            Inner Join ass_commercants_abonnements ON commercants.IdCommercant = ass_commercants_abonnements.IdCommercant
            INNER JOIN ass_commercants_sousrubriques ON commercants.IdCommercant = ass_commercants_sousrubriques.IdCommercant
            WHERE
            commercants.IsActif =  '1' AND
            DATEDIFF(ass_commercants_abonnements.DateFin ,CURDATE()) >0
            GROUP BY
            festival_perso.Idarticle_perso
            ORDER BY
            festival_perso.Nom ASC
        ");
        if($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function delete_article_perso($prmId){

        $qryBonplan = $this->db->query("DELETE FROM festival_perso WHERE id = ?", $prmId) ;
        return $qryBonplan ;
    }

    function insert_article_perso($prmData) {
        $this->db->insert("festival_perso", $prmData);
        return $this->db->insert_id();
    }

    function update_article_perso($prmData) {
        $this->db->where("id", $prmData["id"]);
        $this->db->update("festival_perso", $prmData);
        $objAnnonce = $this->getarticle_persoById($prmData["id"]);
        return $objAnnonce->id;
    }
    function GetAgenda_agenda_persoByUser($IdCommercant="0", $IdUsers_ionauth="0"){

        $qryCategorie_sql = "SELECT
                festival_perso.*
                FROM
                festival_perso
                WHERE
                0=0 ";
        if (isset($IdCommercant) && $IdCommercant!="0" && $IdCommercant!="" && $IdCommercant!=NULL && $IdCommercant!=0) {
            $qryCategorie_sql .= " AND festival_perso.IdCommercant = '".$IdCommercant."'";
        }
        if (isset($IdUsers_ionauth) && $IdUsers_ionauth!="0" && $IdUsers_ionauth!="" && $IdUsers_ionauth!=NULL && $IdUsers_ionauth!=0) {
            $qryCategorie_sql .= " AND festival_perso.IdUsers_ionauth = '".$IdUsers_ionauth."'";
        }
        $qryCategorie_sql .= " LIMIT 1";

        ////$this->firephp->log($qryCategorie_sql, 'qryCategorie_sql');

        $Query = $this->db->query($qryCategorie_sql);
        return $Query->row();
    }
    function GetFestival_festival_persoByUser($IdCommercant="0", $IdUsers_ionauth="0"){

        $qryCategorie_sql = "SELECT
                festival_perso.*
                FROM
                festival_perso
                WHERE
                0=0 ";
        if (isset($IdCommercant) && $IdCommercant!="0" && $IdCommercant!="" && $IdCommercant!=NULL && $IdCommercant!=0) {
            $qryCategorie_sql .= " AND festival_perso.IdCommercant = '".$IdCommercant."'";
        }
        if (isset($IdUsers_ionauth) && $IdUsers_ionauth!="0" && $IdUsers_ionauth!="" && $IdUsers_ionauth!=NULL && $IdUsers_ionauth!=0) {
            $qryCategorie_sql .= " AND festival_perso.IdUsers_ionauth = '".$IdUsers_ionauth."'";
        }
        $qryCategorie_sql .= " LIMIT 1";

        ////$this->firephp->log($qryCategorie_sql, 'qryCategorie_sql');

        $Query = $this->db->query($qryCategorie_sql);
        return $Query->row();
    }
}