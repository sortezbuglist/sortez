<?php

class mdl_categories_article extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function getById($id = 0)
    {
        $Sql = "select * from article_categ where article_categid =" . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByIdSousCateg($id = 0)
    {
        $Sql = "select * from article_subcateg where article_subcategid =" . $id;
        if(isset($id) && $id!=0 && $id!="" && is_numeric($id)) {
            $Query = $this->db->query($Sql);
            return $Query->row();
        } else {
            return null;
        }
    }

    function GetAll()
    {
        $qryarticle_type = $this->db->query("
            SELECT *
            FROM
                article_categ
            ORDER BY category ASC
        ");
        if ($qryarticle_type->num_rows() > 0) {
            return $qryarticle_type->result();
        }
    }

    function Insert($prmData)
    {
        $this->db->insert("article_categ", $prmData);
        return $this->db->insert_id();
    }

    function Update($prmData)
    {
        $prmData = (array)$prmData;
        $this->db->where("article_categid", $prmData["article_categid"]);
        $this->db->update("article_categ", $prmData);
        $objCommercant = $this->GetById($prmData["article_categid"]);
        return $objCommercant->article_categid;
    }

    function Delete($prmId)
    {
        $qryBonplan = $this->db->query("DELETE FROM article_categ WHERE article_categid = ?", $prmId);
        return $qryBonplan;
    }


    function InsertSousCateg($prmData)
    {
        $this->db->insert("article_subcateg", $prmData);
        return $this->db->insert_id();
    }

    function UpdateSousCateg($prmData)
    {
        $prmData = (array)$prmData;
        $this->db->where("article_subcategid", $prmData["article_subcategid"]);
        $this->db->update("article_subcateg", $prmData);
        $objCommercant = $this->GetByIdSousCateg($prmData["article_subcategid"]);
        return $objCommercant->article_subcategid;
    }

    function DeleteSousCateg($prmId)
    {
        $qryBonplan = $this->db->query("DELETE FROM article_subcateg WHERE article_subcategid = ?", $prmId);
        return $qryBonplan;
    }

    function GetAllSousrubrique()
    {
        $qryCategorie = $this->db->query("
            SELECT *
            FROM
                article_subcateg
            ORDER BY subcateg ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAllSousrubriqueByRubrique($IdRubrique)
    {
        $qryCategorie = $this->db->query("
            SELECT *
            FROM
                article_subcateg
            WHERE
                article_categid = '" . $IdRubrique . "'
            ORDER BY subcateg ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function verifier_categorie_article($IdRubrique)
    {
        $qryCategorie = $this->db->query("
            SELECT
            articles.*
            FROM
            articles
            WHERE
            articles.article_categid = '" . $IdRubrique . "'
       ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function verifier_souscategorie_article($IdSousRubrique)
    {
        $qryCategorie = $this->db->query("
            SELECT
            articles.*
            FROM
            articles
            WHERE
            articles.article_subcategid = '" . $IdSousRubrique . "'
       ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAllCategByTypeArticle($article_typeid)
    {
        $qryCategorie = $this->db->query("
            SELECT *
            FROM
                article_categ
            WHERE
                article_typeid = '" . $article_typeid . "'
            ORDER BY category ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAllSousCategByTypeArticle($article_categid)
    {
        $qryCategorie = $this->db->query("
            SELECT *
            FROM
                article_subcateg
            WHERE
                article_categid = '" . $article_categid . "'
            ORDER BY subcateg ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetArticleSouscategorieByRubrique($idRubrique, $inputQuand = "0", $inputDatedebut = "", $inputDatefin = "", $_iDepartementId = "0", $_iVilleId = "0")
    {

        $zSqlListePartenaire = "
            SELECT
            agenda_subcateg.agenda_subcategid,
            agenda_subcateg.agenda_categid,
            agenda_subcateg.subcateg,
            COUNT(article_datetime.id) as nb_article,
            COUNT(article.id) as nb_article_id
            FROM
            agenda_subcateg
            INNER JOIN article ON agenda_subcateg.agenda_subcategid = article.article_subcategid
            LEFT OUTER JOIN article_datetime ON article_datetime.article_id = article.id
            WHERE
            agenda_subcateg.agenda_categid = '" . $idRubrique . "'
            AND
            (article.IsActif = 1 OR article.IsActif = 2)
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlListePartenaire .= " AND article.IdVille_localisation = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlListePartenaire .= " AND article.IdVille_localisation = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlListePartenaire .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlListePartenaire .= " article.IdVille_localisation = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListePartenaire .= " OR ";
            }
            $zSqlListePartenaire .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlListePartenaire .= " AND article.IdVille_localisation IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));

            if ($inputQuand == "1") $zSqlListePartenaire .= " AND article_datetime.date_debut = '" . $date_of_day . "'";
            if ($inputQuand == "2") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $next_saturday_day . "' AND article_datetime.date_fin <= '" . $next_sunday_day . "' )";
            if ($inputQuand == "3") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $last_sunday_day . "' AND article_datetime.date_fin <= '" . $next_saturday_day . "' )";
            if ($inputQuand == "4") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND article_datetime.date_fin <= '" . $next_saturday_of_next_monday . "' )";
            if ($inputQuand == "5") $zSqlListePartenaire .= " AND ( article_datetime.date_debut >= '" . $first_day_month . "' AND article_datetime.date_fin <= '" . $last_day_month . "' )";
        }

        if ($_iVilleId != "" && $_iVilleId != NULL && $_iVilleId != "0") {
            $zSqlListePartenaire .= " AND article.IdVille_localisation = '" . $_iVilleId . "'";
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " article.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }

        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null && $inputDatedebut != "0") {
            $zSqlListePartenaire .= " AND article_datetime.date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null && $inputDatefin != "0") {
            $zSqlListePartenaire .= " AND article_datetime.date_fin = '" . $inputDatefin . "'";
        }

        $zSqlListePartenaire .= "
            GROUP BY
            agenda_subcateg.agenda_subcategid
            ";

        //var_dump($zSqlListePartenaire); 
        //echo $zSqlListePartenaire."<br/>"; //die();
        //log_message('error', 'CHECKING_BY_SUB_CATEG_NUMBER : ' . $zSqlListePartenaire);

        $qryCategorie = $this->db->query($zSqlListePartenaire);
        //if($qryCategorie->num_rows() > 0) {
        return $qryCategorie->result();
        //var_dump($qryCategorie->result()); die();
        //} else return "0";
    }


    function GetArticleCategorie_by_params($inputQuand = "0", $inputDatedebut = "0000-00-00", $inputDatefin = "0000-00-00", $_iDepartementId = null, $_iVilleId = "0")
    {
        $zSqlListePartenaire = "
            SELECT
                agenda_categ.agenda_categid,
                agenda_categ.category,
                COUNT(article.id) AS nb_article
                ";

        $zSqlListePartenaire .= "
            FROM
                article
                INNER JOIN agenda_categ ON agenda_categ.agenda_categid = article.article_categid

            WHERE
                (article.IsActif = '1' OR article.IsActif = '2')

        ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlListePartenaire .= " AND article.IdVille_localisation = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlListePartenaire .= " AND article.IdVille_localisation = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlListePartenaire .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlListePartenaire .= " article.IdVille_localisation = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListePartenaire .= " OR ";
            }
            $zSqlListePartenaire .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlListePartenaire .= " AND article.IdVille_localisation IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        if ($inputQuand != "0" && $inputQuand != "" && $inputQuand != null) {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));
            $first_day_user_month = date('Y-' . $inputQuand . '-01', $time_date);
            $last_day_user_month = date('Y-' . $inputQuand . '-t', $time_date);

            if ($inputQuand == "101") $zSqlListePartenaire .= " AND ( article.date_debut >= '" . $date_of_day . "' AND article.date_fin <= '" . $date_of_day . "' )";
            if ($inputQuand == "202") $zSqlListePartenaire .= " AND ( article.date_debut >= '" . $next_saturday_day . "' AND article.date_fin <= '" . $next_sunday_day . "' )";
            if ($inputQuand == "303") $zSqlListePartenaire .= " AND ( article.date_debut >= '" . $last_sunday_day . "' AND article.date_fin <= '" . $next_saturday_day . "' )";
            if ($inputQuand == "404") $zSqlListePartenaire .= " AND ( article.date_debut >= '" . $last_sunday_of_next_monday . "' AND article.date_fin <= '" . $next_saturday_of_next_monday . "' )";
            if ($inputQuand == "505") $zSqlListePartenaire .= " AND ( article.date_debut >= '" . $first_day_month . "' AND article.date_fin <= '" . $last_day_month . "' )";

            if ($inputQuand == "01") $zSqlListePartenaire .= " AND ( article.date_debut >= '" . $first_day_user_month . "' AND article.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "02") $zSqlListePartenaire .= " AND ( article.date_debut >= '" . $first_day_user_month . "' AND article.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "03") $zSqlListePartenaire .= " AND ( article.date_debut >= '" . $first_day_user_month . "' AND article.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "04") $zSqlListePartenaire .= " AND ( article.date_debut >= '" . $first_day_user_month . "' AND article.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "05") $zSqlListePartenaire .= " AND ( article.date_debut >= '" . $first_day_user_month . "' AND article.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "06") $zSqlListePartenaire .= " AND ( article.date_debut >= '" . $first_day_user_month . "' AND article.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "07") $zSqlListePartenaire .= " AND ( article.date_debut >= '" . $first_day_user_month . "' AND article.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "08") $zSqlListePartenaire .= " AND ( article.date_debut >= '" . $first_day_user_month . "' AND article.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "09") $zSqlListePartenaire .= " AND ( article.date_debut >= '" . $first_day_user_month . "' AND article.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "10") $zSqlListePartenaire .= " AND ( article.date_debut >= '" . $first_day_user_month . "' AND article.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "11") $zSqlListePartenaire .= " AND ( article.date_debut >= '" . $first_day_user_month . "' AND article.date_debut <= '" . $last_day_user_month . "' )";
            if ($inputQuand == "12") $zSqlListePartenaire .= " AND ( article.date_debut >= '" . $first_day_user_month . "' AND article.date_debut <= '" . $last_day_user_month . "' )";
        }

        if ($_iVilleId != "0" && $_iVilleId != "" && $_iVilleId != null) {
            $zSqlListePartenaire .= " AND article.IdVille_localisation = '" . $_iVilleId . "'";
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " article.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }


        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null && $inputDatedebut != "0") {
            $zSqlListePartenaire .= " AND article.date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null && $inputDatefin != "0") {
            $zSqlListePartenaire .= " AND article.date_fin = '" . $inputDatefin . "'";
        }

        $zSqlListePartenaire .= " group by agenda_categ.agenda_categid ";

        //$iOrderBy_value = " order by article.date_debut asc ";//
        $iOrderBy_value = " order by agenda_categ.category asc ";

        $zSqlListePartenaire .= $iOrderBy_value;

        //echo $zSqlListePartenaire;
        log_message('error', "willart : " . $zSqlListePartenaire);
        ////$this->firephp->log($zSqlListePartenaire, 'query_category');

        $zQueryListeBonPlan = $this->db->query($zSqlListePartenaire);
        if ($zQueryListeBonPlan->num_rows() > 0) {
            return $zQueryListeBonPlan->result();
        }


    }


}