<?php

class mdl_categories_bonplan extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function getById($id = 0)
    {

        $Sql = "select * from agenda_categ where  agenda_categid =" . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByIdSousCateg($id = 0)
    {
        $Sql = "select * from bonplan_subcateg where bonplan_subcategid =" . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getByIdCateg($id = 0)
    {
        $Sql = "select * from bonplan_categ where WHERE  bonplan_categid =" . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function GetAll()
    {
        $qrybonplan_type = $this->db->query("
            SELECT *
            FROM
                bonplan_categ
            
            ORDER BY category ASC
        ");
        if ($qrybonplan_type->num_rows() > 0) {
            return $qrybonplan_type->result();
        }
    }

    function Insert($prmData)
    {
        $this->db->insert("bonplan_categ", $prmData);
        return $this->db->insert_id();
    }

    function Update($prmData)
    {
        $prmData = (array)$prmData;
        $this->db->where("bonplan_categid", $prmData["bonplan_categid"]);
        $this->db->update("bonplan_categ", $prmData);
        $objCommercant = $this->GetById($prmData["bonplan_categid"]);
        return $objCommercant->bonplan_categid;
    }

    function Delete($prmId)
    {
        $qryBonplan = $this->db->query("DELETE FROM bonplan_categ WHERE bonplan_categid = ?", $prmId);
        return $qryBonplan;
    }


    function InsertSousCateg($prmData)
    {
        $this->db->insert("bonplan_subcateg", $prmData);
        return $this->db->insert_id();
    }

    function UpdateSousCateg($prmData)
    {
        $prmData = (array)$prmData;
        $this->db->where("bonplan_subcategid", $prmData["bonplan_subcategid"]);
        $this->db->update("bonplan_subcateg", $prmData);
        $objCommercant = $this->GetByIdSousCateg($prmData["bonplan_subcategid"]);
        return $objCommercant->bonplan_subcategid;
    }

    function DeleteSousCateg($prmId)
    {
        $qryBonplan = $this->db->query("DELETE FROM bonplan_subcateg WHERE bonplan_subcategid = ?", $prmId);
        return $qryBonplan;
    }

    function GetAllSousrubrique()
    {
        $qryCategorie = $this->db->query("
            SELECT *
            FROM
                bonplan_subcateg
            ORDER BY subcateg ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAllSousrubriqueByRubrique($IdRubrique)
    {
        $qryCategorie = $this->db->query("
            SELECT *
            FROM
                bonplan_subcateg
            WHERE
                bonplan_categid = '" . $IdRubrique . "'
            ORDER BY subcateg ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function verifier_categorie_bonplan($IdRubrique)
    {
        $qryCategorie = $this->db->query("
            SELECT
            bonplan.*
            FROM
            bonplan
            WHERE
            bonplan.bonplan_categid = '" . $IdRubrique . "'
       ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function verifier_souscategorie_bonplan($IdSousRubrique)
    {
        $qryCategorie = $this->db->query("
            SELECT
            bonplan.*
            FROM
            bonplan
            WHERE
            bonplan.bonplan_subcategid = '" . $IdSousRubrique . "'
       ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAllCategByTypebonplan($bonplan_typeid)
    {
        $qryCategorie = $this->db->query("
            SELECT *
            FROM
                bonplan_categ
            WHERE
                bonplan_typeid = '" . $bonplan_typeid . "'
            ORDER BY category ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAllSousCategByTypebonplan($bonplan_categid)
    {
        $qryCategorie = $this->db->query("
            SELECT *
            FROM
                bonplan_subcateg
            WHERE
                bonplan_categid = '" . $bonplan_categid . "'
            ORDER BY subcateg ASC
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetbonplanSouscategorieByRubrique($idRubrique, $inputQuand = "0", $inputDatedebut = "", $inputDatefin = "", $_iDepartementId = "0", $_iVilleId = "0")
    {

        $zSqlListePartenaire = "
            SELECT
            bonplan_subcateg.bonplan_subcategid,
            bonplan_subcateg.bonplan_categid,
            bonplan_subcateg.subcateg,
            COUNT(bonplan_datetime.id) as nb_bonplan,
            COUNT(bonplan.id) as nb_bonplan_id
            FROM
            bonplan_subcateg
            INNER JOIN bonplan ON bonplan_subcateg.bonplan_subcategid = bonplan.bonplan_subcategid
            LEFT OUTER JOIN bonplan_datetime ON bonplan_datetime.bonplan_id = bonplan.id
            WHERE
            bonplan_subcateg.bonplan_categid = '" . $idRubrique . "'
            AND
            (bonplan.IsActif = 1 OR bonplan.IsActif = 2)
            ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $this->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $this->session->userdata('localdata_IdVille_all');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
            $zSqlListePartenaire .= " AND bonplan.bonplan_ville_id = '2031' ";
        } else if(isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && is_numeric($localdata_IdVille)){
            $zSqlListePartenaire .= " AND bonplan.bonplan_ville_id = '".$localdata_IdVille."' ";
        } else if (isset($localdata_IdVille_all) && is_array($localdata_IdVille_all) && count($localdata_IdVille_all)>0) {
            $zSqlListePartenaire .= " AND ( ";
            for ($iiik = 0; $iiik < sizeof($localdata_IdVille_all); $iiik ++) {
                $zSqlListePartenaire .= " bonplan.bonplan_ville_id = '".$localdata_IdVille_all[$iiik]."' ";
                if ($iiik < (sizeof($localdata_IdVille_all) - 1)) $zSqlListePartenaire .= " OR ";
            }
            $zSqlListePartenaire .= " ) ";
        } else if(isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && is_numeric($localdata_IdDepartement)){
            $zSqlListePartenaire .= " AND bonplan.bonplan_ville_id IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '".$localdata_IdDepartement."')";
        }
        //LOCALDATA FILTRE

        if ($inputQuand != "0") {
            $date_of_day = date("Y-m-d");
            $time_date = strtotime($date_of_day);
            $next_sunday = strtotime('next sunday, 12pm', $time_date);
            $last_sunday = strtotime('last sunday, 12pm', $time_date);
            $next_saturday = strtotime('next saturday, 11:59am', $time_date);
            $next_monday = strtotime('next monday, 11:59am', $time_date);
            $format_date = 'Y-m-d';
            $next_sunday_day = date($format_date, $next_sunday);
            $last_sunday_day = date($format_date, $last_sunday);
            $next_saturday_day = date($format_date, $next_saturday);
            $next_monday_day = date($format_date, $next_monday);
            $last_sunday_of_next_monday = date($format_date, strtotime('last sunday, 12pm', strtotime($next_monday_day)));
            $next_saturday_of_next_monday = date($format_date, strtotime('next saturday, 11:59am', strtotime($next_monday_day)));
            $first_day_month = date('Y-m-01', strtotime($date_of_day));
            $last_day_month = date('Y-m-t', strtotime($date_of_day));

            if ($inputQuand == "1") $zSqlListePartenaire .= " AND bonplan_datetime.date_debut = '" . $date_of_day . "'";
            if ($inputQuand == "2") $zSqlListePartenaire .= " AND ( bonplan_datetime.date_debut >= '" . $next_saturday_day . "' AND bonplan_datetime.date_fin <= '" . $next_sunday_day . "' )";
            if ($inputQuand == "3") $zSqlListePartenaire .= " AND ( bonplan_datetime.date_debut >= '" . $last_sunday_day . "' AND bonplan_datetime.date_fin <= '" . $next_saturday_day . "' )";
            if ($inputQuand == "4") $zSqlListePartenaire .= " AND ( bonplan_datetime.date_debut >= '" . $last_sunday_of_next_monday . "' AND bonplan_datetime.date_fin <= '" . $next_saturday_of_next_monday . "' )";
            if ($inputQuand == "5") $zSqlListePartenaire .= " AND ( bonplan_datetime.date_debut >= '" . $first_day_month . "' AND bonplan_datetime.date_fin <= '" . $last_day_month . "' )";
        }

        if ($_iVilleId != "" && $_iVilleId != NULL && $_iVilleId != "0") {
            $zSqlListePartenaire .= " AND bonplan.IdVille_localisation = '" . $_iVilleId . "'";
        } elseif ($_iDepartementId != 0 && $_iDepartementId != "" && $_iDepartementId != null) {
            # code...
            model_load_model("mdldepartement");
            $villeByIdDepartement = $this->mdldepartement->getVillesByIdDepartement($_iDepartementId);
            if (count($villeByIdDepartement) > 0) {
                $zSqlListePartenaire .= " AND (";
                $iii_villedep = 1;
                foreach ($villeByIdDepartement as $key_villeDep) {
                    # code...
                    $zSqlListePartenaire .= " bonplan.IdVille_localisation =" . $key_villeDep->IdVille;
                    if ($iii_villedep != count($villeByIdDepartement)) {
                        $zSqlListePartenaire .= " OR ";
                    }
                    $iii_villedep++;
                }
                $zSqlListePartenaire .= ")";
            }
        }

        if ($inputDatedebut != "0000-00-00" && $inputDatedebut != "" && $inputDatedebut != null && $inputDatedebut != "0") {
            $zSqlListePartenaire .= " AND bonplan_datetime.date_debut = '" . $inputDatedebut . "'";
        }

        if ($inputDatefin != "0000-00-00" && $inputDatefin != "" && $inputDatefin != null && $inputDatefin != "0") {
            $zSqlListePartenaire .= " AND bonplan_datetime.date_fin = '" . $inputDatefin . "'";
        }

        $zSqlListePartenaire .= "
            GROUP BY
            bonplan_subcateg.bonplan_subcategid
            ";

        //var_dump($zSqlListePartenaire);
        //echo $zSqlListePartenaire."<br/>"; //die();
        //log_message('error', 'william : '.$zSqlListePartenaire);

        $qryCategorie = $this->db->query($zSqlListePartenaire);
        //if($qryCategorie->num_rows() > 0) {
        return $qryCategorie->result();
        //var_dump($qryCategorie->result()); die();
        //} else return "0";
    }


    function effacerimages_categ($_iCategId)
    {
        $zSql = "UPDATE bonplan_categ SET images ='' WHERE bonplan_categid =" . $_iCategId;
        $zQuery = $this->db->query($zSql);
    }


}