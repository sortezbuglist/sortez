<?php
class Mdl_gestion_livre_dor extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    function getById($id=0){
        $Sql = "select * from livreor_commentaires where id =". $id;
        if(isset($id) && $id!=0 && $id!="" && is_numeric($id)) {
            $Query = $this->db->query($Sql);
            return $Query->row();
        } else {
            return null;
        }
    }
    function GetAll(){
        $qryIdCommercant = $this->db->query("
            SELECT *
            FROM
                livreor_commentaires
            ORDER BY livreor_commentaires.id ASC
        ");
        if($qryIdCommercant->num_rows() > 0) {
            return $qryIdCommercant->result();
        }
    }
    function getbyidcom($idcom){
        $this->db->where('idcommercant',$idcom);
        $res=$this->db->get('livreor_commentaires');
        return $res->result();
    }

    function supprime_commentaire($prmId){

       $this->db->where('id',$prmId);
       $suppr=$this->db->delete('livreor_commentaires');
       if ($suppr){
           return true;
       }else{
           return false;
       }
    }

    //function insertarticle_type($prmData) {
        //$this->db->insert("article_type", $prmData);
        //return $this->db->insert_id();
   // }

    function update_commentaire($prmData) {
        $this->db->where("id", $prmData["id"]);
        $this->db->update("livreor_commentaires", $prmData);
        $objlivredor = $this->getById($prmData["id"]);
        return $objlivredor->id;
    }
    public function save_comment_google($array){
        if (isset($array['idcommercant']) AND $array['idcommercant']!=0){
            $this->db->where('IdCommercant',$array['idcommercant']);
            $this->db->update("commercants",$array);
        }else{
            $this->db->insert("commercants",$array);
        }

    }
    public function update_affich_livre($field){
        $this->db->where('IdCommercant',$field['IdCommercant']);
        $this->db->update('glissieres',$field);
    }
}