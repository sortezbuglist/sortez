<?php
/**
* Retourne le chemin du dossier images
* @param string $argSite (valeur possible : NULL, admin/, public/, professionel/)
* @return string Chemin des images
*/
function GetImagePath($argSite = "") {
    return base_url() . "application/resources/" . $argSite . "images";
}

/**
* Retourne le chemin du dossier pdf
* @param string $argSite (valeur possible : NULL, admin/, public/, professionel/)
* @return string Chemin des images
*/
function GetPdfPath($argSite = "") {
    return base_url() . "application/resources/" . $argSite . "pdf";
}


/**
* Retourne le chemin du dossier css
* @param string $argSite (valeur possible : NULL, admin/, public/, professionel/)
* @return string Chemin des css
*/
function GetCssPath($argSite = "") {
    return base_url() . "application/resources/" . $argSite . "css";
}

/**
* Retourne le chemin du dossier js
* @param string $argSite (valeur possible : NULL, admin/, public/, professionel/)
* @return string Chemin des js
*/
function GetJsPath($argSite = "") {
    return base_url() . "application/resources/" . $argSite . "js";
}

/**
* Retourne le chemin du dossier js
* @param string $argSite (valeur possible : NULL, admin/, public/, professionel/)
* @return string Chemin des js
*/
function GetFlashPath($argSite = "") {
    return base_url() . "application/resources/" . $argSite . "flash";
}

/**
* @return Nom du navigateur client
*
*/
function GetNavigateur() {
    $HTTP_USER_AGENT = $_SERVER['HTTP_USER_AGENT'];

    //Detection du browser
    if(eregi('Safari', $HTTP_USER_AGENT) && !eregi('Konqueror', $HTTP_USER_AGENT))
        $Navigateur = 'Safari';

    elseif (eregi('msie', $HTTP_USER_AGENT) && !eregi('opera', $HTTP_USER_AGENT))
        $Navigateur = 'InternetExplorer';

    elseif (eregi('opera', $HTTP_USER_AGENT))
        $Navigateur = 'Opera';

    elseif (eregi('Mozilla', $HTTP_USER_AGENT))
        $Navigateur = 'MozillaFireFox';

    else {
        $Navigateur = $HTTP_USER_AGENT;
    }

    return $Navigateur;
}

/**
* @return Chemin physique du site sur le disque du serveur
*/
function RootPath() {
    $PathInArray = explode("/",$_SERVER['SCRIPT_FILENAME']) ;
    $PathInArray[sizeof($PathInArray) - 1]  = "" ;
    return implode("/",$PathInArray);
}

function MailAdressToNoSpam($argEmail) {
    $Res = preg_replace("/@/", "__AT__", $argEmail);
    $Res = preg_replace("/\./", "__POINT__", $Res);

    return $Res;
}

function NoSpamToMailAdress($argEmail) {
    $Res = preg_replace("/__AT__/", "@", $argEmail);
    $Res = preg_replace("/__POINT__/", ".", $Res);

    return $Res;
}

/**
* @return Nom de la page en cours
*
*/
function GetPageName($arg) {
    // ...
}

/**
* @param string Nom du contr�leur � appeler
* @return Url du site public
*/
function GetPublicSiteUrl($argCtrl = "") {
    return (empty($argCtrl)) ? site_url("public/accueil") : site_url("public/" . $argCtrl);
}


/**
* @param string Nom du contr�leur � appeler
* @return Url du site public
*/
function GetProfessionnelSiteUrl($argCtrl = "") {
    return (empty($argCtrl)) ? site_url("professionnel/accueil") : site_url("professionnel/" . $argCtrl);
}

/**
* @param string Nom du contr�leur � appeler
* @return Url du site pro
*/
function GetProSiteUrl($argCtrl = "") {
    return (empty($argCtrl)) ? site_url("professionnel/accueil") : site_url("professionnel/" . $argCtrl);
}

/**
* @param string Nom du contr�leur � appeler
* @return Url du site admin
*/
function GetAdminSiteUrl($argCtrl = "") {
    return (empty($argCtrl)) ? site_url("admin/accueil") : site_url("admin/" . $argCtrl);
}

/**
* @param mixed $argId
* @return true si l'identifiant est valide
*/
function IsValidId($argId){
  $Integer = $argId;
  settype($Integer, 'integer');
  if($argId == $Integer AND is_numeric($argId) AND $argId > 0)
    return true;
  return false;
}

function NoPix() {
    global $PrefixDir;

    echo "<img src=\"" . GetImagePath() . "/nopix.gif\">";
}

/**
* Generation des liens de pagination
* @param string $argBaseUrl Url cible
* @param integer $argTotalRows Nombre total des lignes
* @param integer $argPerPage Nombre de ligne � afficher par page
*/
function GetPaginationLinks($argBaseUrl, $argTotalRows, $argPerPage = 20) {
    $MyCodeIgniter =& get_instance();       // echo print_r($MyCodeIgniter);

    $MyCodeIgniter->load->library("pagination");

    $config["base_url"] = $argBaseUrl;
    $config['total_rows'] = $argTotalRows;
    $config['per_page'] = $argPerPage;

    $config['uri_segment'] = 4;
    $config['full_tag_open'] = '<p>Pages : ';
    $config['full_tag_close'] = '</p>';

    $config['num_tag_open'] = '&nbsp;';
    $config['num_tag_close'] = '&nbsp;';

    $config['cur_tag_open'] = '&nbsp;<span style="color:red; font-weight: bold;">';
    $config['cur_tag_close'] = '</span>&nbsp;';

    $MyCodeIgniter->pagination->initialize($config);

    return $MyCodeIgniter->pagination->create_links() . "<br />";
}

function CreateDir($argDir) {
    if (!is_dir($argDir)) {
        mkdir($argDir, 0777);
    }

    return true;
}

/**
* Retourne les crit�res sql d'une recherche
* @param array $argCols Liste des colonnes pour executer la recherche
* @param array $argKeywords Liste des mots cl�s � rechercher
*
* @return string Chaine SQL
*/
function GetSearchingCriteria ($argCols, $argKeywords) {
    $Res = "";

    foreach ($argKeywords as $Keyword) {
        foreach ($argCols as $Col) {
            $Res .= (empty($Res)) ? "" : " or ";
            $Res .= $Col . " like ('%" . $Keyword . "%') ";
        }
    }

    return $Res;
}

function GetImageName($argThumbMarker, $argFilename) {
    $Extension = end(explode(".", $argFilename));
    $Thumb = preg_replace("/." . $Extension . "/", $argThumbMarker . "." . $Extension, $argFilename);

    return $Thumb;
}
function GetImageBougie () {
    $zCheminExplode = base_url() ;
    $zCheminExplode = explode ("dev/", $zCheminExplode) ;
    return $zCheminExplode[0] ;
}
/**
* Converion de date YYYY-MM-DD en DD-MM-YYYY
*/
function translate_date_to_fr($date) {
    $dateTotranslate = explode("-",$date);
    switch($dateTotranslate[1]) {
        case "01":
            $Mois = "Janvier";
            break;
        case "02":
            $Mois = "F&eacute;vrier";
            break;
        case "03":
            $Mois = "Mars";
            break;
        case "04":
            $Mois = "Avril";
            break;
        case "05":
            $Mois = "Mai";
            break;
        case "06":
            $Mois = "Juin";
            break;
        case "07":
            $Mois = "Juillet";
            break;
        case "08":
            $Mois = "Ao&ucirc;t";
            break;
        case "09":
            $Mois = "Septembre";
            break;
        case "10":
            $Mois = "Octobre";
            break;
        case "11":
            $Mois = "Novembre";
            break;
        case "12":
            $Mois = "D&eacute;cembre";
            break;
    }
    $translatedDate = $dateTotranslate[2] . " " . $Mois . " " . $dateTotranslate[0];
    return $translatedDate;
}

function convert_datetime_to_fr ($date){
    $dateTotranslate = explode(" ",$date);
    $date_day = translate_date_to_fr($dateTotranslate[0]);
    return $date_day." - ".$dateTotranslate[1];
}

/**
* Converion de date DD/MM/YYYY en YYYY-MM-DD
*
*/
function convertDate ($_zDate)
{
    $tzDateExploded = explode ("/", $_zDate) ;
    $zDateRetour = $tzDateExploded[2] . "-" . $tzDateExploded[1] . "-" . $tzDateExploded[0] ;
    return $zDateRetour ;  
}


/**
 * Converion de date DD, d MM yy en YYYY-MM-DD
 *
 */
function convert_Frenchdate_to_Sqldate ($_zDate)
{
    $tzDateExploded_1 = explode (",", $_zDate) ;
    $tzDateExploded = explode (" ", trim($tzDateExploded_1[1])) ;
    if ($tzDateExploded[1] == 'Janvier' || $tzDateExploded[1] == 'Jan') $tzDateExploded[1] = "01";
    if ($tzDateExploded[1] == 'Févier' || $tzDateExploded[1] == 'Fevier' || $tzDateExploded[1] == 'Feb') $tzDateExploded[1] = "02";
    if ($tzDateExploded[1] == 'Mars' || $tzDateExploded[1] == 'Mar') $tzDateExploded[1] = "03";
    if ($tzDateExploded[1] == 'Avril' || $tzDateExploded[1] == 'Apr') $tzDateExploded[1] = "04";
    if ($tzDateExploded[1] == 'Mai' || $tzDateExploded[1] == 'May') $tzDateExploded[1] = "05";
    if ($tzDateExploded[1] == 'Juin' || $tzDateExploded[1] == 'Jun') $tzDateExploded[1] = "06";
    if ($tzDateExploded[1] == 'Juillet' || $tzDateExploded[1] == 'Jul') $tzDateExploded[1] = "07";
    if ($tzDateExploded[1] == 'Août' || $tzDateExploded[1] == 'Aout' || $tzDateExploded[1] == 'Aug') $tzDateExploded[1] = "08";
    if ($tzDateExploded[1] == 'Septembre' || $tzDateExploded[1] == 'Sep') $tzDateExploded[1] = "09";
    if ($tzDateExploded[1] == 'Octobre' || $tzDateExploded[1] == 'Oct') $tzDateExploded[1] = "10";
    if ($tzDateExploded[1] == 'Novembre' || $tzDateExploded[1] == 'Nov') $tzDateExploded[1] = "11";
    if ($tzDateExploded[1] == 'Décembre' || $tzDateExploded[1] == 'Decembre' || $tzDateExploded[1] == 'Dec') $tzDateExploded[1] = "12";
    $zDateRetour = $tzDateExploded[2] . "-" . $tzDateExploded[1] . "-" . $tzDateExploded[0] ;
    return $zDateRetour ;
}


/**
 * Converion de date YYYY-MM-DD en DD, d MM yy
 *
 */
function convert_Sqldate_to_Frenchdate ($_zDate)
{
    $day_week = date('l', strtotime( $_zDate));
    if ($day_week == "Monday") $day_week = "Lundi";
    if ($day_week == "Tuesday") $day_week = "Mardi";
    if ($day_week == "Wednesday") $day_week = "Mercredi";
    if ($day_week == "Thursday") $day_week = "Jeudi";
    if ($day_week == "Friday") $day_week = "Vendredi";
    if ($day_week == "Saturday") $day_week = "Samedi";
    if ($day_week == "Sunday") $day_week = "Dimanche";
    $tzDateExploded = explode ("-", $_zDate) ;
    if (isset($tzDateExploded[1]) && $tzDateExploded[1] == '01') $tzDateExploded[1] = "Janvier";
    if (isset($tzDateExploded[1]) && $tzDateExploded[1] == '02') $tzDateExploded[1] = "Févier";
    if (isset($tzDateExploded[1]) && $tzDateExploded[1] == '03') $tzDateExploded[1] = "Mars";
    if (isset($tzDateExploded[1]) && $tzDateExploded[1] == '04') $tzDateExploded[1] = "Avril";
    if (isset($tzDateExploded[1]) && $tzDateExploded[1] == '05') $tzDateExploded[1] = "Mai";
    if (isset($tzDateExploded[1]) && $tzDateExploded[1] == '06') $tzDateExploded[1] = "Juin";
    if (isset($tzDateExploded[1]) && $tzDateExploded[1] == '07') $tzDateExploded[1] = "Juillet";
    if (isset($tzDateExploded[1]) && $tzDateExploded[1] == '08') $tzDateExploded[1] = "Août";
    if (isset($tzDateExploded[1]) && $tzDateExploded[1] == '09') $tzDateExploded[1] = "Septembre";
    if (isset($tzDateExploded[1]) && $tzDateExploded[1] == '10') $tzDateExploded[1] = "Octobre";
    if (isset($tzDateExploded[1]) && $tzDateExploded[1] == '11') $tzDateExploded[1] = "Novembre";
    if (isset($tzDateExploded[1]) && $tzDateExploded[1] == '12') $tzDateExploded[1] = "Décembre";
    $zDateRetour = $day_week.", ".$tzDateExploded[2] . " " . $tzDateExploded[1] . " " . $tzDateExploded[0] ;
    return $zDateRetour ;
}




/**
* Converion de date YYYY-MM-DD en DD/MM/YYYY
*
*/
function convertDateWithSlashes ($_zDate)
{
    $zDateRetour = "";
    $tzDateExploded = explode ("-", $_zDate) ;
    $zDateRetour .= $tzDateExploded[2] ?? '' ;
    $zDateRetour .= "/" ;
    $zDateRetour .= $tzDateExploded[1] ?? '' ;
    $zDateRetour .= "/" ;
    $zDateRetour .= $tzDateExploded[0] ?? '' ;
    return $zDateRetour ;
}
/**
* Arrondir � 2 chiffres apr�s la virgule
*
*/
function rounder($value){ //2 chiffres apr�s la virgule
    $buffer = $value * 100;
    $rounded = round($buffer);
    $buffer = $rounded / 100;
    $point_pos = strpos($buffer, ".");
    if ($point_pos == FALSE)
        $buffer .= ".00";
    else {
        if ((strlen($buffer) - $point_pos) == 2) $buffer .= "0";
    }
    return $buffer;
}

//Enveler la couleur, alignement dans une chaine
function textSimple($chaine){        
    //Enlever les <p> et </p> 
    
    $posdebut= strpos($chaine, "<p>");
    if ($posdebut !== false) {
        $chaine = str_replace("<p>", "", $chaine);
        $chaine = str_replace("</p>", "", $chaine);            
    }
    
    $posdebut1 = strpos($chaine, "<p");        
    if ($posdebut1 !== false) {    
        $posfin1 = strpos($chaine, ">", $posdebut1);                                 
        $chaine = substr_replace($chaine, "", $posdebut1, $posfin1 + 1);
        $chaine = str_replace("</p>", "", $chaine);    
    }    
    
    //Enlever les <span> et </span> 
    $posdebut0 = strpos($chaine, "<span>");
    if ($posdebut0  !== false) {
        $chaine = str_replace("<span>", "", $chaine);
        $chaine = str_replace("</span>", "", $chaine);            
    }
    
    $posdebut2 = strpos($chaine, "<span");    
    if ($posdebut2 !== false) {    
        $posfin2 = strpos($chaine, ">", $posdebut2);                                 
        $chaine = substr_replace($chaine, "", $posdebut2, $posfin2 + 1);
        $chaine = str_replace("</span>", "", $chaine);
    }
    
    return $chaine ;        
}

 function RecupereMotsChaine($MaChaine,$nbMot) 
 { 
     $nbTotalMot = str_word_count($MaChaine);
     $NouvelleChaine = '';
     
     if ($nbTotalMot <= $nbMot){
         $NouvelleChaine = $MaChaine ;
     }
     else {
        $ChaineTab=explode(" ",$MaChaine); 
        for($i=0;$i<$nbMot;$i++) 
        { 
           $NouvelleChaine.=" "."$ChaineTab[$i]"; 
        }
     }
    return $NouvelleChaine; 
 }
function RandomString($argNbChars = 5) {
    $String = "";
    $List = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    mt_srand((double) microtime() * 1000000);

    $NewString = "";
    While(strlen($NewString) < $argNbChars) {
        $NewString .= $List[mt_rand(0, strlen($List)-1)];
    }

    return $NewString;
}

function SaveSession($argLogin, $argPassword) {
    $ecoco2ident = RandomString(5) . $argPassword . RandomString(2) . "#*#!#" . RandomString(2) . $argLogin . RandomString(5);
    $mExpDate = time() + (3600 * 24 * 15); // Ce cookie durera 15 jours
    setcookie("CLUBPROXIMITE-IDENT", $ecoco2ident, $mExpDate, "/");
}

function DestroySession() {
    setcookie("CLUBPROXIMITE-IDENT", "", -1, "/");
}
 
 function GetSession() {
    if (empty($_COOKIE["CLUBPROXIMITE-IDENT"])) {
        return false;
    }

    $x = explode("#*#!#", $_COOKIE["CLUBPROXIMITE-IDENT"]);
    //debug($x);
    if (sizeof($x) != 2) {
        return false;
    }

    $Res["Password"] = substr($x[0], 5, -2);
    $Res["Login"] = substr($x[1], 2, -5);

    return $Res;
}
function debug($argTest) {
    echo "<pre>";
    print_r($argTest);
    echo "</pre>";
    exit;
}
function GetImageContentPath($argSite = "") {
    return base_url() . "application/content/" . $argSite . "images";
}
function ajoutZeroPourString($_zChaine, $_iNombreCaractere){
 $iNombreCaractere = strlen($_zChaine) ;
 $zChaine = "" ;
 for ($i=0; $i<($_iNombreCaractere - 1); $i++){
  $zChaine .= "0" ; 
 }
 $zChaine .= $_zChaine ;
 return $zChaine ;
}

/**
 * la fonction suivante va envoyer une notification
 */
function envoi_notification_old($prmDestinataires = null, $prmSujet = "", $prmContenu = "") {
    $CI =& get_instance();
    $bRet = false;
    if($prmDestinataires) {
        $CI->load->library('phpmailer');
        // $CI->load->plugin('phpmailer');
        
        //Parametrage avant envoi mail
            $Mail = new phpmailer();
    
            if ($CI->config->item("dev_local")) {
                $Mail->IsSMTP();
                $Mail->Host = $CI->config->item("mail_server");// Adresse du serveur pop
                $Mail->Mailer = $CI->config->item("mail_protocol");
            } else {
                $Mail->isMail();
            }
            $Mail->From = $CI->config->item("mail_sender_adress");
            $Mail->FromName = $CI->config->item("mail_sender_name");
            
            foreach($prmDestinataires as $arrDestinataire) {
                $Mail->AddAddress($arrDestinataire["Email"], $arrDestinataire["Name"]);
            }
            
            $Mail->IsHTML(true);
            
            $Mail->Subject = "[" . $CI->config->item("mail_header") . "] " . $prmSujet;
            $Message = '
                <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
                
                <head>
                    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
                    <meta name="author" content="ibonia" />
                
                    <title>Notifications</title>
                </head>
                
                <body>
                '
                 . $prmContenu . 
                '
                </body>
                </html>
            ';
            $Mail->Body = utf8_decode($Message);
        if($Mail->Send()) $bRet =  true;
    }
    return $bRet;
}



/**
 * la fonction suivante va envoyer une notification
 */
function envoi_notification($prmDestinataires = null, $prmSujet = "Notification Sortez", $prmContenu = "", $prmEnvoyeur = "contact@sortez.org", $prmEnvoyeurName = "Sortez") {
    $bRet = false;

    if(isset($prmDestinataires)) {


        if($_SERVER['SERVER_NAME']=="localhost") $base_path_system = str_replace('system\\', '', BASEPATH);
        else $base_path_system = str_replace('system/', '', BASEPATH);
        require_once($base_path_system.'application/resources/phpmailer/PHPMailerAutoload.php');
        $mail             = new PHPMailer();
        $mail->IsHTML(true);
        //$mail->CharSet = "text/html; charset=UTF-8;";
        $mail->CharSet = "UTF-8";
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->Host       = "mail.sortez.org"; // SMTP server
        $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                                                   // 1 = errors and messages
                                                   // 2 = messages only
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->Host       = "ssl0.ovh.net"; // sets the SMTP server
        $mail->SMTPSecure = "ssl";
        $mail->Port       = 465;                    // set the SMTP port for the GMAIL server
        $mail->Username   = "mail@sortez.org"; // SMTP account username
        $mail->Password   = 'J^ou~#|~[{`\'5((*_~#{[]@_(-)';        // SMTP account password
        $mail->SetFrom($prmEnvoyeur, $prmEnvoyeurName);
        $mail->AddReplyTo($prmEnvoyeur, $prmEnvoyeurName);
        $mail->Subject    = $prmSujet;
        $mail->AltBody    = "To view the message, please use an HTML compatible email viewer !"; // optional, comment out and test
        $prmContenu = '<div style="font-family:Arial, Helvetica, sans-serif; font-size:13px;">'.$prmContenu.'<p><hr><strong>Priviconcept</strong><br/>SAS 18, rue des Combattants d\'Afrique du Nord 06000 Nice<br/><a href="mailto:contact@sortez.org">contact@sortez.org</a></p></div>';
        $mail->MsgHTML($prmContenu);
        $mail->AddAddress($prmDestinataires[0]['Email'], $prmDestinataires[0]['Name']);
        if($mail->Send()) {
          $bRet =  true;
        } else {
           return $mail->ErrorInfo;
        }


        /*
        $CI =& get_instance();
        $mail = '<div style="font-family:Arial, Helvetica, sans-serif; font-size:13px;">'.$prmContenu.'<p><hr><strong>Priviconcept</strong><br/>SAS 18, rue des Combattants d\'Afrique du Nord 06000 Nice<br/><a href="mailto:contact@sortez.org">contact@sortez.org</a></p></div>';
        $CI->load->library('email');
        $CI->email->from($prmEnvoyeur, $prmEnvoyeurName);
        $CI->email->to($prmDestinataires[0]['Email']);
        $CI->email->subject($prmSujet);
        $CI->email->message($mail);
        $result_mail = $CI->email->send();
        if($result_mail) {
            $bRet =  true;
        } else {
            $bRet =  false;
        }
        */


    }
    return $bRet;
}


function doUploadResize($file,$docfile,$DocAssocie=""){
$zFileName = ""; 
if($_FILES[$file]["name"]!="")
        {
         $_zExtension = strrchr($_FILES[$file]['name'], '.') ;   
         $sFileName=	random_string('unique', 16);
         $DocAssocie=random_string('unique', 10) . $_zExtension  ;        
        
        }
        // else 
        // {   
        	// $DocAssocie = $this->input->post("DocAssocie");
        // }
		if (!empty($_FILES[$file]["name"])) {
	            $_FILES[$file]["type"] = "application/pdf";
	            $_FILES[$file]["name"] =$DocAssocie;
					  $PathWithIndex = $_SERVER['SCRIPT_FILENAME'];
                      $path = str_replace("portail.php", "", $PathWithIndex)."$docfile";

					  if (file_exists($_FILES[$file]['tmp_name'])) {
					    $srcFile = $DocAssocie;
					    if(move_uploaded_file($_FILES[$file]['tmp_name'], $path . basename($srcFile))) {
						  $zFileName = basename($srcFile);

                                                  //resizing image with witdth > 560
                                                  /*if(file_exists($docfile.$zFileName)) {
                                                        $size_mage_image = getimagesize($docfile.$zFileName);
                                                        resize_file_to($docfile.$zFileName, 426, 900,'','');
                                                  }*/
                                                  //resizing image with witdth > 560


						}
					  }

        	 }
		 return $zFileName;

}


function doUpload($file,$docfile,$DocAssocie=""){
$zFileName = "";
if($_FILES[$file]["name"]!="")
        {
         $_zExtension = strrchr($_FILES[$file]['name'], '.') ;
         $sFileName=	random_string('unique', 16);
         $DocAssocie=random_string('unique', 10) . $_zExtension  ;

        }
        // else
        // {
        	// $DocAssocie = $this->input->post("DocAssocie");
        // }
		if (!empty($_FILES[$file]["name"])) {
	            $_FILES[$file]["type"] = "application/pdf";
	            $_FILES[$file]["name"] =$DocAssocie;
					  $PathWithIndex = $_SERVER['SCRIPT_FILENAME'];
                      $path = str_replace("portail.php", "", $PathWithIndex)."$docfile";

					  if (file_exists($_FILES[$file]['tmp_name'])) {
					    $srcFile = $DocAssocie;
					    if(move_uploaded_file($_FILES[$file]['tmp_name'], $path . basename($srcFile))) {
						  $zFileName = basename($srcFile);

                                                  //resizing image with witdth > 560
                                                  /*if(file_exists($docfile.$zFileName)) {
                                                        $size_mage_image = getimagesize($docfile.$zFileName);
                                                        if ($size_mage_image[0]>560) resize_file_to($docfile.$zFileName, 560, 560,'','');
                                                  }*/
                                                  //resizing image with witdth > 560


						}
					  }

        	 }
		 return $zFileName;

}

function doUploadCSV($file,$docfile,$DocAssocie=""){
    $zFileName = "";
    if($_FILES[$file]["name"]!="")
    {
        $_zExtension = strrchr($_FILES[$file]['name'], '.') ;
        $sFileName=	random_string('unique', 16);
        $DocAssocie=random_string('unique', 10) . $_zExtension  ;

    }
    // else
    // {
    // $DocAssocie = $this->input->post("DocAssocie");
    // }
    if (!empty($_FILES[$file]["name"])) {
        $_FILES[$file]["type"] = "text/csv";
        $_FILES[$file]["name"] =$DocAssocie;
        $PathWithIndex = $_SERVER['SCRIPT_FILENAME'];
        $path = str_replace("portail.php", "", $PathWithIndex)."$docfile";

        if (file_exists($_FILES[$file]['tmp_name'])) {
            $srcFile = $DocAssocie;
            if(move_uploaded_file($_FILES[$file]['tmp_name'], $path . basename($srcFile))) {
                $zFileName = basename($srcFile);

                //resizing image with witdth > 560
                /*if(file_exists($docfile.$zFileName)) {
                      $size_mage_image = getimagesize($docfile.$zFileName);
                      if ($size_mage_image[0]>560) resize_file_to($docfile.$zFileName, 560, 560,'','');
                }*/
                //resizing image with witdth > 560


            }
        }

    }
    return $zFileName;

}

function doUpload_audio($file,$docfile,$DocAssocie=""){
    $zFileName = "";
    if($_FILES[$file]["name"]!="")
    {
        $_zExtension = strrchr($_FILES[$file]['name'], '.') ;
        $sFileName=	random_string('unique', 16);
        $DocAssocie=random_string('unique', 10) . $_zExtension  ;
    }

    if (!empty($_FILES[$file]["name"])) {
        $_FILES[$file]["type"] = "application/mpeg";
        $_FILES[$file]["name"] =$DocAssocie;
        $PathWithIndex = $_SERVER['SCRIPT_FILENAME'];
        $path = str_replace("portail.php", "", $PathWithIndex)."$docfile";

        if (file_exists($_FILES[$file]['tmp_name'])) {
            $srcFile = $DocAssocie;
            if(move_uploaded_file($_FILES[$file]['tmp_name'], $path . basename($srcFile))) {
                $zFileName = basename($srcFile);
            }
        }

    }
    return $zFileName;
}

function doUpload_video($file,$docfile,$DocAssocie=""){
    $zFileName = "";
    if($_FILES[$file]["name"]!="")
    {
        $_zExtension = strrchr($_FILES[$file]['name'], '.') ;
        $sFileName=	random_string('unique', 16);
        $DocAssocie=random_string('unique', 10) . $_zExtension  ;
    }

    if (!empty($_FILES[$file]["name"])) {
        $_FILES[$file]["type"] = "application/mp4";
        $_FILES[$file]["name"] =$DocAssocie;
        $PathWithIndex = $_SERVER['SCRIPT_FILENAME'];
        $path = str_replace("portail.php", "", $PathWithIndex)."$docfile";

        if (file_exists($_FILES[$file]['tmp_name'])) {
            $srcFile = $DocAssocie;
            if(move_uploaded_file($_FILES[$file]['tmp_name'], $path . basename($srcFile))) {
                $zFileName = basename($srcFile);
            }
        }

    }
    return $zFileName;
}

function verify_proportion_largeur_hauteur($image){
    $img_infos = getimagesize($image);
    $img_width = $img_infos[0]; // Largeur de l'image
    $img_height = $img_infos[1]; // Hauteur de l'image
    $image_difference = (intval($img_width)*3)/4;

    if((intval($img_height)+1)==intval($image_difference) || (intval($img_height)-1)==intval($image_difference) || (intval($img_height))==intval($image_difference)){
        return true;
    } else {
        return false;
    }
}
/*function RemoveExtendedChars2($prmInput) {
    $prmInput = utf8_encode($prmInput) ;
    $prmInput = str_replace(utf8_encode("Ÿ"), "y", $prmInput) ;
    $prmInput = utf8_decode($prmInput);
    $prmInput = trim($prmInput);
    $prmInput = utf8_decode($prmInput);
    $prmInput = strtr($prmInput,"�����������������������������������������������������","aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn");
    utf8_encode($prmInput) ; //contourner le probl�me
    $prmInput = str_replace("�", "ae", $prmInput);//contourner le probl�me
    $prmInput = str_replace("�", "ae", $prmInput);//contourner le probl�me
    $prmInput = preg_replace('/([^a-z0-9_]+)/i', '-', $prmInput);
    $prmInput = strtolower($prmInput);
    $prmInput = str_replace('"', "'", $prmInput);
    //$prmInput = str_replace('*', "oe", $prmInput);
    return $prmInput;
}*/


function RemoveExtendedChars2($str, $charset='utf-8')
{
    $str = htmlentities($str, ENT_NOQUOTES, $charset);

    $str = preg_replace('#&([A-za-z])(?:acute|cedil|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
    $str = preg_replace('#&[^;]+;#', '-', $str); // supprime les autres caractères
    $str = str_replace(' ', '-', $str);
    $str = str_replace('.', '-', $str);
    $str = str_replace("'", '-', $str);
    $str = str_replace('"', '-', $str);
    $str = str_replace("(", '-', $str);
    $str = str_replace(")", '-', $str);
    $str = str_replace(',', '-', $str);
    $str = str_replace(';', '-', $str);
    $str = str_replace(':', '-', $str);
    $str = str_replace('€', '-', $str);
    $str = str_replace('$', '-', $str);
    $str = str_replace('*', '-', $str);
    $str = str_replace('#', '-', $str);
    $str = str_replace('~', '-', $str);
    $str = strtolower($str);

    return $str;
}

function getHeaderLogo() {
    $Logo = null;
    $CI =& get_instance();
    $CI->load->model("parametre");
    $colLogos = $CI->parametre->getWhereCodeIn(array("PhotoHeader"));
    if(sizeof($colLogos) > 0) {
        $Logo = $colLogos[0]->Contenu;
    }
    return $Logo;
}

//limitation de nombre de caractère à afficher dans déscription
function truncate($text,$numb,$etc = "...") {
    $text = $text;
    if (strlen($text) > $numb) {
    $text = substr($text, 0, $numb);
    $text = substr($text,0,strrpos($text," "));

    $punctuation = ".!?:;,-"; //punctuation you want removed

    $text = (strspn(strrev($text),  $punctuation)!=0)
            ?
            substr($text, 0, -strspn(strrev($text),  $punctuation))
            :
    $text;

    $text = $text.$etc;
    }

    return $text;
}


function newAleatoireChaine( $chrs = "") {

		if( $chrs == "" ) $chrs = 8;



		$chaine = "";



		$list = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		mt_srand((double)microtime()*1000000);

		$newstring="";



		while( strlen( $newstring )< $chrs ) {

			$newstring .= $list[mt_rand(0, strlen($list)-1)];

		}

		return $newstring;

	}



function supprime_espace($copiest){
    $tabCar = array(" ", "\t", "\n", "\r", "\0", "\x0B", "\xA0");
    $copiest = str_replace($tabCar, array(), $copiest);
    return $copiest;
}


function date_french() {
    $mois = array(1=>'janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
    $jours = array('dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi');

    return $jours[date('w')].' '.date('j').' '.$mois[date('n')].' '.date('Y');
}


function delete_directory_rand($dirname) {
   if (is_dir($dirname))
      $dir_handle = opendir($dirname);
   if (!isset($dir_handle))
      return false;
   while($file = readdir($dir_handle)) {
      if ($file != "." && $file != "..") {
         if (!is_dir($dirname."/".$file))
            unlink($dirname."/".$file);
         else
            delete_directory($dirname.'/'.$file);
      }
   }
   closedir($dir_handle);
   rmdir($dirname);
   return true;
}

function curPageURL() {
    $pageURL = 'http';
    if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}


function UploadFilesSiteUser($input_file_name,$upload_path,$isThumbImage='FALSE',$allowed_types='gif|jpg|png|mp4|mp3',$max_size='1000',$max_width='1024',$max_height='768') {

    $config_upload_help['upload_path'] = $upload_path;
    $config_upload_help['allowed_types'] = $allowed_types;
    $config_upload_help['max_size']	= $max_size;
    $config_upload_help['max_width']  = $max_width;
    $config_upload_help['max_height']  = $max_height;
    if ($isThumbImage=="FALSE") $config_upload_help['file_name']  = md5(rand() * time());
    else $config_upload_help['file_name']  = "thumb_".md5(rand() * time());

    $thiss = &get_instance();
    $thiss->load->library('upload', $config_upload_help);
    $thiss->upload->initialize($config_upload_help);

    $this_firephp = &get_instance();
    $this_firephp->load->library('firephp');
    $this_firephp->firephp->log($config_upload_help, 'config_upload_help');


    $data = array('upload_data' => $thiss->upload->data());
    $mimetype= $data['upload_data']['file_type'];
    //echo $mimetype;
    $this_firephp->firephp->log($mimetype, 'mimetype');
    $this_firephp->firephp->log($data, 'data');

    $this_firephp->firephp->log($input_file_name, 'input_file_name');
    $this_firephp->firephp->log($_SERVER['CONTENT_LENGTH'], 'content_length');
    //$this_firephp->firephp->log(var_dump($_FILES), 'vardump_file');//testing

    ////TESTING
    if ( $_SERVER['REQUEST_METHOD'] == 'POST' &&  empty($_FILES) && $_SERVER['CONTENT_LENGTH'] > 0 ){
        $displayMaxSize = ini_get('post_max_size');
        /*switch ( substr($displayMaxSize,-1) )
        {
          case 'G':
            $displayMaxSize = $displayMaxSize * 1024;
          case 'M':
            $displayMaxSize = $displayMaxSize * 1024;
          case 'K':
             $displayMaxSize = $displayMaxSize * 1024;
        }*/
        $error_large = 'Posted data is too large. '.           $_SERVER['CONTENT_LENGTH'].           ' bytes exceeds the maximum size of '.           $displayMaxSize.' bytes.';
        $this_firephp->firephp->log($error_large, 'error_large');
    }
    ////TESTING



    if (!$thiss->upload->do_upload($input_file_name))
    {
        $error = array('Erreur' => $thiss->upload->display_errors());
        return $error;
    }
    else
    {
        /*$data = array('upload_data' => $thiss->upload->data());
        $mimetype= $data['upload_data']['file_type'];
        //echo $mimetype;
        track_error("mimetype_ : ".$mimetype, "","");*/

        $item_upload_data = $thiss->upload->data();
        return $item_upload_data;
    }

}

/**
    *
    * Allow models to use other models
    *
    * This is a substitute for the inability to load models
    * inside of other models in CodeIgniter.  Call it like
    * this:
    *
    * $salaries = model_load_model('salary');
    * ...
    * $salary = $salaries->get_salary($employee_id);
    *
    * @param string $model_name The name of the model that is to be loaded
    *
    * @return object The requested model object
    *
    */
function model_load_model($model_name)
   {
      $CI =& get_instance();
      $CI->load->model($model_name);
      return $CI->$model_name;
   }


 function get_fidelity($id_commercant){
    if(!empty($id_commercant)){
        $fidelity = array();
        $ci =& get_instance();

        $ci->load->model('mdl_card_remise');
        $ci->load->model('mdl_card_tampon');
        $ci->load->model('mdl_card_capital');
        $ci->load->model('commercant');

        $tampon=$ci->mdl_card_tampon->getByIdCommercant($id_commercant);
        $remise=$ci->mdl_card_remise->getByIdCommercant($id_commercant);
        $capital=$ci->mdl_card_capital->getByIdCommercant($id_commercant);
        $commercant = $ci->commercant->GetById($id_commercant);

        $reponse = new stdClass();

        $reponse->is_tampon_active = (!empty($tampon) && $tampon->is_activ) ? 1 : 0;
        $reponse->tampon = $tampon;
        $reponse->is_remise_active = (!empty($remise) && $remise->is_activ) ? 1 : 0;
        $reponse->remise = $remise;
        $reponse->is_capital_active = (!empty($capital) && $capital->is_activ) ? 1 : 0;
        $reponse->capital = $capital;
        $reponse->has_fidelity = ($reponse->is_tampon_active || $reponse->is_remise_active || $reponse->is_capital_active) ? 1 : 0;
        $reponse->commercant = $commercant;

        return $reponse;
    }
}


function get_fiche_client_remise($id_user,$id_commercant){
    $CI = &get_instance();

    $CI->load->model('mdl_card_fiche_client_remise');

    $fiche_client_array = $CI->mdl_card_fiche_client_remise->getByCritere(array('card_fiche_client_remise.id_user'=>$id_user,'card_fiche_client_remise.id_commercant'=>$id_commercant));

    return (!empty($fiche_client_array)) ? current($fiche_client_array) : false;
}

function get_fiche_client_capital($id_user,$id_commercant){
    $CI = &get_instance();

    $CI->load->model('mdl_card_fiche_client_capital');

    $fiche_client_array = $CI->mdl_card_fiche_client_capital->getByCritere(array('card_fiche_client_capital.id_user'=>$id_user,'card_fiche_client_capital.id_commercant'=>$id_commercant));

    return (!empty($fiche_client_array)) ? current($fiche_client_array) : false;
}

function get_fiche_client_tampon($id_user,$id_commercant){
    $CI = &get_instance();

    $CI->load->model('mdl_card_fiche_client_tampon');

    $fiche_client_array = $CI->mdl_card_fiche_client_tampon->getByCritere(array('card_fiche_client_tampon.id_user'=>$id_user,'card_fiche_client_tampon.id_commercant'=>$id_commercant));

    return (!empty($fiche_client_array)) ? current($fiche_client_array) : false;
}

function get_fiche_client($offre,$id_user,$id_commercant){
    $CI = &get_instance();

    $CI->load->model('mdl_card_fiche_client_tampon');
    $CI->load->model('mdl_card_fiche_client_capital');
    $CI->load->model('mdl_card_fiche_client_remise');

    $fiche_client_array = array();
    if($offre==="tampon"){
        $fiche_client_array = $CI->mdl_card_fiche_client_tampon->getByCritere(array('card_fiche_client_tampon.id_user'=>$id_user,'card_fiche_client_tampon.id_commercant'=>$id_commercant));
    }
    if($offre === "capital"){
        $fiche_client_array = $CI->mdl_card_fiche_client_capital->getByCritere(array('card_fiche_client_capital.id_user'=>$id_user,'card_fiche_client_capital.id_commercant'=>$id_commercant));
    }
    if($offre === "remise"){
        $fiche_client_array = $CI->mdl_card_fiche_client_remise->getByCritere(array('card_fiche_client_remise.id_user'=>$id_user,'card_fiche_client_remise.id_commercant'=>$id_commercant));
    }

    return (!empty($fiche_client_array)) ? current($fiche_client_array) : false;
}


function get_user_bonplan($id_user,$d_commercant){
    $CI = &get_instance();

    $CI->load->model('assoc_client_bonplan_model');

    $clent_bonplan_array = $CI->assoc_client_bonplan_model->get_where(array('commercants.IdCommercant'=>$d_commercant,'id_client'=>$id_user));

    return (!empty($clent_bonplan_array)) ? ($clent_bonplan_array) : false;
}

function get_user_bonplan_mobile($id_user,$d_commercant){
    $CI = &get_instance();

    $CI->load->model('assoc_client_bonplan_model');

    $clent_bonplan_array = $CI->assoc_client_bonplan_model->get_where_mobile(array('commercants.IdCommercant'=>$d_commercant,'id_client'=>$id_user));

    return (!empty($clent_bonplan_array)) ? ($clent_bonplan_array) : false;
}

function get_user_bonplan_by_user_id($id_user){
    $CI = &get_instance();

    $CI->load->model('assoc_client_bonplan_model');

    $clent_bonplan_array = $CI->assoc_client_bonplan_model->get_where(array('id_client'=>$id_user));

    return (!empty($clent_bonplan_array)) ? ($clent_bonplan_array) : false;
}

function blue_class_progress($solde,$objectif){
    $objectif = !empty($objectif) ? $objectif : 1;
    $pure_value = round((24*$solde)/$objectif, 0, PHP_ROUND_HALF_UP);
    $pure_value = ($pure_value==0) ? 1 : $pure_value;
    return "pure-u-".$pure_value."-24 bg-red pad-tb-24 align-right price no-radius pad-tb-5";
}

function red_class_progess($solde,$objectif){
    $objectif = !empty($objectif) ? $objectif : 1;
    $pure = round(24 - round((24*$solde)/$objectif, 0, PHP_ROUND_HALF_UP));
    $pure = ($pure==24) ? 23 : $pure;
    return "pure-u-".$pure."-24 bg-blue pad-tb-24 align-right price no-radius pad-tb-5";
}

function get_offre_active($id_commercant){

    $CI = &get_instance();
    $CI->load->model('mdl_card_remise');
    $CI->load->model('mdl_card_tampon');
    $CI->load->model('mdl_card_capital');

    $remise_active = $CI->mdl_card_remise->is_remise_active($id_commercant);
    $tampon_active = $CI->mdl_card_tampon->is_tampon_active($id_commercant);
    $capital_active = $CI->mdl_card_capital->is_capital_active($id_commercant);


    $is_remise_active = (!empty($remise_active) && $remise_active->is_activ) ? 1 : 0;
    $is_tampon_active = (!empty($tampon_active) && $tampon_active->is_activ) ? 1 : 0;
    $is_capital_active = (!empty($capital_active) && $capital_active->is_activ) ? 1 : 0;

    //if(!$is_remise_active && !$is_tampon_active && !$is_capital_active) {
    $active = 0;
    if($is_remise_active ){
        $active = "remise";
    }else if($is_capital_active ){
        $active = "capital";
    }else if($is_tampon_active ){
        $active = "tampon";
    }

    return $active;
}

function solde_desc($a, $b) {
    if($a->solde == $b->solde) {
        return 0;
    }
    return ($b->solde > $a->solde) ? 1 : -1;
}

function solde_asc($a, $b) {
    if($a->solde == $b->solde) {
        return 0;
    }
    return ($b->solde < $a->solde) ? 1 : -1;
}

function SupprimeLesAccents($mot){
    return strtr( $mot, "ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ",
            "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn" );
}

function clean_string($string) {
    $string = strtolower(trim($string));

    // escape first the unescaped elements by mb_convert_encoding ('\'', '"', and '&')
    $string = preg_replace("#[ ,.;:'%&()\-\"]+#", "-", $string);
    $string = mb_convert_encoding($string, 'HTML-ENTITIES', 'UTF-8');

    // replace accents
    $string = preg_replace(array('/ß/', '/&(..)lig;/', '/&([aouAOU])uml;/', '/&(.)[^;]*;/'), array('ss', "$1", "$1".'e', "$1"), $string);

    // replace all special characters except '-'
    $string = preg_replace("/[^a-z0-9-]/i", '-', $string);

    return $string;
}

function photo_commercant($commercant){
    if(is_object($commercant)){
        $photos1 = $commercant->Photo1;
        $photos2 = $commercant->Photo2;
        $photos3 = $commercant->Photo3;


        $dir = FCPATH.'../portail/application/resources/front/images/agenda/photoCommercant/';
        $http_link = 'http://privicarte.fr/portail/application/resources/front/images/agenda/photoCommercant/';


        if(is_file($dir.$commercant->Photo1) ){
            return $http_link.$commercant->Photo1;
        }
        else if(is_file($dir.$commercant->Photo2) ){
            return $http_link.$commercant->Photo2;
        }
        else if(is_file($dir.$commercant->Photo3) ){
            return $http_link.$commercant->Photo3;
        }
        else{
            return base_url().'application/resources/front/images/wp71b211d2_06.png';
        }

    }
}

function mail_fidelisation($data,$to){

    $CI =& get_instance();
    $mail = $CI->load->view('mobile2014/email/ajout_fidelisation',$data,TRUE);

    /*$CI->load->library('email');

    $CI->email->from('contact@privicarte.fr', 'SORTEZ');
    $CI->email->to($to);

    $CI->email->subject('[Sortez] Nouveau solde');
    $CI->email->message($mail);

    $CI->email->send();*/

    //  echo $CI->email->print_debugger();


    $zContactEmailCom_copy = array();
    $zContactEmailCom_copy[] = array("Email" => $to, "Name" => "Pro Sortez");
    $sending_mail_privicarte_copy = envoi_notification($zContactEmailCom_copy, '[Sortez] Nouveau solde', $mail);

    if ($sending_mail_privicarte_copy) return true; else return false;

}

function generate_client_card($idUser_ion_auth=0){

    $CI =& get_instance();
    $CI->load->model("mdl_card");
    $CI->load->model("mdl_card_bonplan_used");
    $CI->load->model("mdl_card_capital");
    $CI->load->model("mdl_card_tampon");
    $CI->load->model("mdl_card_user_link");
    $CI->load->model("user");
    $CI->load->model("Commercant");
    $CI->load->model("mdlbonplan");
    $CI->load->model('assoc_client_bonplan_model');
    $CI->load->model('mdl_card_fiche_client_capital');
    $CI->load->model('mdl_card_fiche_client_tampon');
    $CI->load->model('mdl_card_fiche_client_remise');
    $CI->load->library('session');
    $CI->load->library("pagination");
    $CI->load->library('ion_auth');
    $CI->load->model("ion_auth_used_by_club");

    $user_ion_auth = $CI->ion_auth->user($idUser_ion_auth)->row();

    if (isset($user_ion_auth) AND is_object($user_ion_auth)) {
        
    $iduser = $CI->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);

    $where_clause = " id_user='".$iduser."' AND id_ionauth_user='".$user_ion_auth->id."' ";

    $num_id_card = rand(100000,999999);
    if (count($CI->mdl_card->num_id_card_physical_check($num_id_card,'0')>0) || count($CI->mdl_card->num_id_card_virtual_check($num_id_card,'0'))>0) {
        $num_id_card = rand(100000,999999);
        if (count($CI->mdl_card->num_id_card_physical_check($num_id_card,'0')>0) || count($CI->mdl_card->num_id_card_virtual_check($num_id_card,'0'))>0) {
            $num_id_card = rand(100000,999999);
            if (count($CI->mdl_card->num_id_card_physical_check($num_id_card,'0')>0) || count($CI->mdl_card->num_id_card_virtual_check($num_id_card,'0'))>0) {
                $num_id_card = rand(100000,999999);
                if (count($CI->mdl_card->num_id_card_physical_check($num_id_card,'0')>0) || count($CI->mdl_card->num_id_card_virtual_check($num_id_card,'0'))>0) {
                    $num_id_card = rand(100000,999999);
                    if (count($CI->mdl_card->num_id_card_physical_check($num_id_card,'0')>0) || count($CI->mdl_card->num_id_card_virtual_check($num_id_card,'0'))>0) {
                        $num_id_card = rand(100000,999999);
                        if (count($CI->mdl_card->num_id_card_physical_check($num_id_card,'0')>0) || count($CI->mdl_card->num_id_card_virtual_check($num_id_card,'0'))>0) {
                            $num_id_card = rand(100000,999999);
                        }
                    }
                }
            }
        }
    }

    $obj_card['id'] = '0';
    $obj_card['num_id_card_virtual'] = $num_id_card;
    $obj_card['num_id_card_physical'] = $obj_card['num_id_card_virtual'];
    $obj_card['pin_code'] = rand(1000,9999);
    $obj_card['qrcode'] = '1';
    $obj_card['description'] = '';
    $obj_card['is_activ'] = '1';
    $obj_card['id_user'] = $iduser;
    $obj_card['id_ionauth_user'] = $user_ion_auth->id;
    $obj_card['virtual_card_img'] = '';

    //$CI->firephp->log($obj_card, 'obj_card');

    $CI->mdl_card->insert($obj_card);

    $obj_user_card_recheck = $CI->mdl_card->getWhereOne($where_clause);
    if (isset($obj_user_card_recheck) && is_object($obj_user_card_recheck) && $obj_user_card_recheck->num_id_card_virtual!=""){
        $num_card_to_generate = $obj_user_card_recheck->num_id_card_virtual;
    } else {
        $num_card_to_generate = "0";
    }
    //confirm card and user link
    $obj_card_user_link['id'] = '0';
    $obj_card_user_link['id_card'] = $obj_user_card_recheck->id;
    $obj_card_user_link['id_ionauth'] = $obj_user_card_recheck->id_ionauth_user;
    $obj_card_user_link['id_user'] = $obj_user_card_recheck->id_user;
    $obj_card_user_link['date_link'] = date("Y-m-d");
    $CI->mdl_card_user_link->insert($obj_card_user_link);


    $base_path_system = str_replace('system/', '', BASEPATH);

    //generating image QRCODE ---------------------------------
    //$CI->load->library('ciqrcode');
    $params_qrcode['data'] = $num_card_to_generate;
    $params_qrcode['level'] = 'H';
    $params_qrcode['size'] = 10;
    $params_qrcode['savename'] = $base_path_system."application/resources/front/images/cards/qrcode_".$num_card_to_generate.".png";
    qrcodeGenerator($params_qrcode);
    //generating image CARD now********************************************************************
    $CI->load->library('image_moo');
    $file_to_resize_xxx = $base_path_system."application/resources/front/images/cards/cartefidelite.png";
    $bandeau_color_image = $num_card_to_generate.'.png';
    $file_to_resize_yyy = $base_path_system."application/resources/front/images/cards/".$bandeau_color_image;
    if(file_exists($file_to_resize_xxx)) {
        $CI->image_moo
            ->load($file_to_resize_xxx)
            ->load_watermark($base_path_system."application/resources/front/images/cards/qrcode_".$num_card_to_generate.".png")
            ->resize(750,492,true)
            ->watermark(9)
            ->save($file_to_resize_yyy,true);
        $error_image_imoo_xxx = $CI->image_moo->display_errors();
    }
    $file_to_resize_zzz = $base_path_system."application/resources/front/images/cards/".$bandeau_color_image;

    if(file_exists($file_to_resize_yyy)) {
        $CI->image_moo
            ->load($file_to_resize_yyy)
            ->make_watermark_text("N° ".$num_card_to_generate,$base_path_system."system/fonts/ARIALN.TTF", 16, "#000000")
            ->resize(750,492,true)
            ->watermark(6)
            ->save($file_to_resize_zzz,true);
        $error_image_imoo_xxx = $CI->image_moo->display_errors();
    }

    if(file_exists($file_to_resize_zzz)) {

        $where_clause_data = " id_user='".$iduser."' AND id_ionauth_user='".$user_ion_auth->id."' ";
        $obj_user_card_data = $CI->mdl_card->getWhereOne($where_clause_data);
        $obj_card_to_edit['id'] = $obj_user_card_data->id;
        $obj_card_to_edit['num_id_card_virtual'] = $obj_user_card_data->num_id_card_virtual;
        $obj_card_to_edit['num_id_card_physical'] = $obj_user_card_data->num_id_card_physical;
        $obj_card_to_edit['pin_code'] = $obj_user_card_data->pin_code;
        $obj_card_to_edit['qrcode'] = $obj_user_card_data->qrcode;
        $obj_card_to_edit['description'] = $obj_user_card_data->description;
        $obj_card_to_edit['is_activ'] = $obj_user_card_data->is_activ;
        $obj_card_to_edit['id_user'] = $obj_user_card_data->id_user;
        $obj_card_to_edit['id_ionauth_user'] = $obj_user_card_data->id_ionauth_user;
        $obj_card_to_edit['virtual_card_img'] = $bandeau_color_image;
        $CI->mdl_card->update($obj_card_to_edit);

    }

    return $num_id_card;

    }else{
        return 0;
    }


}


function check_subdomain(){

	if(isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="sortez.org")
	{
	    $thiss =& get_instance();
		$thiss->load->library('session');
		$thiss->session->unset_userdata('localdata');
		$thiss->session->unset_userdata('localdata_IdVille');
		$thiss->session->unset_userdata('localdata_IdDepartement');

	    if ($_SERVER['HTTP_HOST'] == "cagnes-commerces.sortez.org") {
	    	$thiss->session->set_userdata('localdata_IdVille', '2031');
	    } else if ($_SERVER['HTTP_HOST'] == "villeneuve-loubet.sortez.org") {
	    	$thiss->session->set_userdata('localdata_IdVille', '2004');
	    }

	}

}

function check_vivresaville_id_ville(){
    $thiss =& get_instance();
    $thiss->load->library('session');
    $thiss->load->model('vsv_ville_session');

    $id_session_ci = session_id();
    $ip_adress = $thiss->input->ip_address();
    $localdata_IdVille = $thiss->session->userdata('localdata_IdVille');
    if(isset($localdata_IdVille)&&$localdata_IdVille==false) $localdata_IdVille=null;
    $localdata_IdVille_parent = $thiss->session->userdata('localdata_IdVille_parent');
    if(isset($localdata_IdVille_parent)&&$localdata_IdVille_parent==false) $localdata_IdVille_parent=null;
    $localdata_IdVille_all = $thiss->session->userdata('localdata_IdVille_all');
    if(isset($localdata_IdVille_all)&&$localdata_IdVille_all==false) $localdata_IdVille_all=null;
    else if (is_array($localdata_IdVille_all)) $localdata_IdVille_all=json_encode($localdata_IdVille_all);

    $prmData['id_session_ci'] =$id_session_ci;
    $prmData['ip_adress'] =$ip_adress;
    $prmData['id_ville'] =$localdata_IdVille;
    $prmData['id_ville_parent'] =$localdata_IdVille_parent;
    $prmData['all_id_ville_other'] =$localdata_IdVille_all;

    if($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL) {
        $sql_query = " id_session_ci='".$id_session_ci."' AND ip_adress='".$ip_adress."' order by id DESC LIMIT 1 ";
        $current_vsv_session = $thiss->vsv_ville_session->getWhere($sql_query);
        if (isset($current_vsv_session) && count($current_vsv_session)>0) {
            $prmData['id'] = $current_vsv_session[0]->id;
            $current_vsv_session_id = $thiss->vsv_ville_session->update($prmData);
        } else {
            $current_vsv_session_id = $thiss->vsv_ville_session->insert($prmData);
        }
    }

    if($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL) {
        if (!isset($localdata_IdVille) || $localdata_IdVille==false) {
            redirect("vivresaville/ListVille");
        } else {
            $thiss->session->set_userdata('localdata_IdVille', $localdata_IdVille);
        }
    }

    if($_SERVER['SERVER_NAME'] == DOMAIN_SORTEZ_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_SORTEZ_GLOBAL) {
        $all_ville_sortez_magazine = array();
        //Ville list for : Vars, les Aples et Monaco
        $all_ville_sortez_magazine[] = "33710"; $all_ville_sortez_magazine[] = "33611"; $all_ville_sortez_magazine[] = "33629";
        $all_ville_sortez_magazine[] = "33622"; $all_ville_sortez_magazine[] = "33684"; $all_ville_sortez_magazine[] = "33571";
        $all_ville_sortez_magazine[] = "33667"; $all_ville_sortez_magazine[] = "33644"; $all_ville_sortez_magazine[] = "33577";
        $all_ville_sortez_magazine[] = "33608"; $all_ville_sortez_magazine[] = "26968"; $all_ville_sortez_magazine[] = "7971";
        $all_ville_sortez_magazine[] = "33712"; $all_ville_sortez_magazine[] = "33653"; $all_ville_sortez_magazine[] = "33618";
        $all_ville_sortez_magazine[] = "33661"; $all_ville_sortez_magazine[] = "33601"; $all_ville_sortez_magazine[] = "33671";
        $all_ville_sortez_magazine[] = "33599"; $all_ville_sortez_magazine[] = "33615"; $all_ville_sortez_magazine[] = "33652";
        $all_ville_sortez_magazine[] = "33641"; $all_ville_sortez_magazine[] = "33619"; $all_ville_sortez_magazine[] = "36941";
        $all_ville_sortez_magazine[] = "33592"; $all_ville_sortez_magazine[] = "33620"; $all_ville_sortez_magazine[] = "36944";
        $all_ville_sortez_magazine[] = "33714"; $all_ville_sortez_magazine[] = "33602"; $all_ville_sortez_magazine[] = "33637";
        $all_ville_sortez_magazine[] = "33666"; $all_ville_sortez_magazine[] = "33648"; $all_ville_sortez_magazine[] = "33649";
        $all_ville_sortez_magazine[] = "33631"; $all_ville_sortez_magazine[] = "33612"; $all_ville_sortez_magazine[] = "33669";
        $all_ville_sortez_magazine[] = "33688"; $all_ville_sortez_magazine[] = "33654"; $all_ville_sortez_magazine[] = "33688";
        $all_ville_sortez_magazine[] = "15590";$all_ville_sortez_magazine[] = "29483";
        $thiss->load->model('mdlville');
        $all_06_ville = $thiss->mdlville->getWhere(" ville_departement='06' ");
        $ii = 0;
        if(isset($all_06_ville)) {
            foreach ($all_06_ville as $item) {
                $item_data = $item;
                $all_ville_sortez_magazine[] = $item_data->IdVille;
                $ii = $ii + 1;
            }
        }
        $thiss->session->set_userdata('localdata_IdVille_all', $all_ville_sortez_magazine);
    }
}

function get_days_between_dates($date1="0000-00-00", $date2="0000-00-00") {
    $from=date_create(date($date1));
    $to=date_create($date2);
    $diff=date_diff($to,$from);
    //print_r($diff);
    return substr($diff->format('%R%a jours'),1);
}
function convert_xml_date_to_sql_date($_zDate){
    $tzDateExploded = explode (" ", $_zDate) ;
    if ($tzDateExploded[2] == 'Janvier' || $tzDateExploded[2] == 'Jan') $tzDateExploded[2] = "01";
    if ($tzDateExploded[2] == 'Jan' || $tzDateExploded[2] == 'Jan') $tzDateExploded[2] = "01";
    if ($tzDateExploded[2] == 'Février' || $tzDateExploded[2] == 'Feb') $tzDateExploded[2] = "02";
    if ($tzDateExploded[2] == 'Feb' || $tzDateExploded[2] == 'Feb') $tzDateExploded[2] = "02";
    if ($tzDateExploded[2] == 'Mars' || $tzDateExploded[2] == 'Mar') $tzDateExploded[2] = "03";
    if ($tzDateExploded[2] == 'Avril' || $tzDateExploded[2] == 'Apr') $tzDateExploded[2] = "04";
    if ($tzDateExploded[2] == 'Mai' || $tzDateExploded[2] == 'Ma') $tzDateExploded[2] = "05";
    if ($tzDateExploded[2] == 'May' || $tzDateExploded[2] == 'Ma') $tzDateExploded[2] = "05";
    if ($tzDateExploded[2] == 'Juin' || $tzDateExploded[2] == 'Jun') $tzDateExploded[2] = "06";
    if ($tzDateExploded[2] == 'Juillet' || $tzDateExploded[2] == 'Jul') $tzDateExploded[2] = "07";
    if ($tzDateExploded[2] == 'Août' || $tzDateExploded[2] == 'Aout') $tzDateExploded[2] = "08";
    if ($tzDateExploded[2] == 'Aug' || $tzDateExploded[2] == 'Aout') $tzDateExploded[2] = "08";
    if ($tzDateExploded[2] == 'Septembre' || $tzDateExploded[2] == 'Sep') $tzDateExploded[2] = "09";
    if ($tzDateExploded[2] == 'Octobre' || $tzDateExploded[2] == 'Oct') $tzDateExploded[2] = "10";
    if ($tzDateExploded[2] == 'Novembre' || $tzDateExploded[2] == 'Nov') $tzDateExploded[2] = "11";
    if ($tzDateExploded[2] == 'Décembre' || $tzDateExploded[2] == 'Dec') $tzDateExploded[2] = "12";
    $zDateRetour = $tzDateExploded[3] . "-" . $tzDateExploded[2] . "-" . $tzDateExploded[1] ;
    return $zDateRetour ;

}

function verify_all_pro_subscription() {

    $thiss =& get_instance();
    $thiss->load->library('session');
    $thiss->load->model("AssCommercantAbonnement");
    $thiss->load->model("Commercant");
    $thiss->load->model("notification");

    ////////////////////////////////////////////////START VERIFY SUBSCRIPTION
    $Expiration3mois = $thiss->AssCommercantAbonnement->Expiration3mois();
    $Expiration2mois = $thiss->AssCommercantAbonnement->Expiration2mois();
    $Expiration15jours = $thiss->AssCommercantAbonnement->Expiration15jours();
    $Expiration1jours = $thiss->AssCommercantAbonnement->Expiration1jours();

    //$Expiration3mois = $thiss->AssCommercantAbonnement->Expiration15jours();

    if (count($Expiration3mois) == 0 && count($Expiration2mois) == 0 && count($Expiration15jours) == 0 && count($Expiration1jours == 0)) {
    } else {

        /**
         * Envoi mail vers admin
         */
        $colDestAdmin = array();
        $colDestAdmin[] = array("Email" => $thiss->config->item("mail_sender_adress"), "Name" => $thiss->config->item("mail_sender_name"));
        $colDestAdmin_contact[] = array("Email" => "contact@sortez.org", "Name" => "Sortez info");
        //$colDestAdmin[] = array("Email"=>"william.arthur.harilantoniaina@gmail.com","Name"=>"William_test");

        // Sujet
        $txtSujetAdmin = "Expiration Abonnement Commercant";

        $txtContenuAdmin = "
                        <p>Bonjour ,</p>
                        <p><strong>Les Commercant list&eacute; ci-dessous ont des abonnement qui vont expirer bient&ocirc;t :</strong></p>
                    ";

        //Liste pour les expirations dans 3 mois
        if (isset($Expiration3mois) && count($Expiration3mois) != 0) {
            $txtContenuAdmin .= '
                        <p>Les abonnements ci-dessous vont expirer dans exactement 3 mois : </p>
                          <table width="100%" border="1" cellspacing="0" cellpadding="0">
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nom</th>
                            <th scope="col">NomSoci&eacute;t&eacute;</th>
                            <th scope="col">Abonnement</th>
                            <th scope="col">Debut</th>
                            <th scope="col">Fin</th>
                            <th scope="col">Lien</th>
                          </tr>';

            foreach ($Expiration3mois as $oExpiration3mois) {

                if ($oExpiration3mois->IdAbonnement == 1) $ComAbonnement = "Partenaire";
                else if ($oExpiration3mois->IdAbonnement == 2) $ComAbonnement = "Partenaire + Bon Plans";
                else if ($oExpiration3mois->IdAbonnement == 3) $ComAbonnement = "Partenaire + Annonces professionnelles";
                else if ($oExpiration3mois->IdAbonnement == 4) $ComAbonnement = "Partenaire + Bon Plans + Annonces professionnelles";
                else $ComAbonnement = "Non definie";


                $commercant3mois = $thiss->Commercant->GetById($oExpiration3mois->IdCommercant);
                $txtContenuAdmin .= '<tr>
                                <td>' . $oExpiration3mois->IdCommercant . '</td>
                                <td>' . $commercant3mois->Nom . ' ' . $commercant3mois->Prenom . '</td>
                                <td>' . $commercant3mois->NomSociete . '</td>
                                <td>' . $ComAbonnement . '</td>
                                <td>' . convertDateWithSlashes($oExpiration3mois->DateDebut) . '</td>
                                <td>' . convertDateWithSlashes($oExpiration3mois->DateFin) . '</td>
                                <td><a href="' . site_url("front/annonce/ficheCommercantAnnonce/" . $oExpiration3mois->IdCommercant) . '">Page</a></td>
                              </tr>
                            </table>
                            ';
            }
        }

        if (isset($Expiration2mois) && count($Expiration2mois) != 0) {
            $txtContenuAdmin .= '
                        <p>Les abonnements ci-dessous vont expirer dans exactement 2 mois : </p>
                          <table width="100%" border="1" cellspacing="0" cellpadding="0">
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nom</th>
                            <th scope="col">NomSoci&eacute;t&eacute;</th>
                            <th scope="col">Abonnement</th>
                            <th scope="col">Debut</th>
                            <th scope="col">Fin</th>
                            <th scope="col">Lien</th>
                          </tr>';

            foreach ($Expiration2mois as $oExpiration2mois) {

                if ($oExpiration2mois->IdAbonnement == 1) $ComAbonnement = "Partenaire";
                else if ($oExpiration2mois->IdAbonnement == 2) $ComAbonnement = "Partenaire + Bon Plans";
                else if ($oExpiration2mois->IdAbonnement == 3) $ComAbonnement = "Partenaire + Annonces professionnelles";
                else if ($oExpiration2mois->IdAbonnement == 4) $ComAbonnement = "Partenaire + Bon Plans + Annonces professionnelles";
                else $ComAbonnement = "Non definie";


                $commercant2mois = $thiss->Commercant->GetById($oExpiration2mois->IdCommercant);
                $txtContenuAdmin .= '<tr>
                                <td>' . $oExpiration2mois->IdCommercant . '</td>
                                <td>' . $commercant2mois->Nom . ' ' . $commercant2mois->Prenom . '</td>
                                <td>' . $commercant2mois->NomSociete . '</td>
                                <td>' . $ComAbonnement . '</td>
                                <td>' . convertDateWithSlashes($oExpiration2mois->DateDebut) . '</td>
                                <td>' . convertDateWithSlashes($oExpiration2mois->DateFin) . '</td>
                                <td><a href="' . site_url("front/annonce/ficheCommercantAnnonce/" . $oExpiration2mois->IdCommercant) . '">Page</a></td>
                              </tr>
                            </table>
                            ';
            }
        }


        if (isset($Expiration15jours) && count($Expiration15jours) != 0) {
            $txtContenuAdmin .= '
                        <p>Les abonnements ci-dessous vont expirer dans exactement 15 jours : </p>
                          <table width="100%" border="1" cellspacing="0" cellpadding="0">
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nom</th>
                            <th scope="col">NomSoci&eacute;t&eacute;</th>
                            <th scope="col">Abonnement</th>
                            <th scope="col">Debut</th>
                            <th scope="col">Fin</th>
                            <th scope="col">Lien</th>
                          </tr>';

            foreach ($Expiration15jours as $oExpiration15jours) {

                if ($oExpiration15jours->IdAbonnement == 1) $ComAbonnement = "Partenaire";
                else if ($oExpiration15jours->IdAbonnement == 2) $ComAbonnement = "Partenaire + Bon Plans";
                else if ($oExpiration15jours->IdAbonnement == 3) $ComAbonnement = "Partenaire + Annonces professionnelles";
                else if ($oExpiration15jours->IdAbonnement == 4) $ComAbonnement = "Partenaire + Bon Plans + Annonces professionnelles";
                else $ComAbonnement = "Non definie";


                $commercant15jours = $thiss->Commercant->GetById($oExpiration15jours->IdCommercant);
                $txtContenuAdmin .= '<tr>
                                <td>' . $oExpiration15jours->IdCommercant . '</td>
                                <td>' . $commercant15jours->Nom . ' ' . $commercant15jours->Prenom . '</td>
                                <td>' . $commercant15jours->NomSociete . '</td>
                                <td>' . $ComAbonnement . '</td>
                                <td>' . convertDateWithSlashes($oExpiration15jours->DateDebut) . '</td>
                                <td>' . convertDateWithSlashes($oExpiration15jours->DateFin) . '</td>
                                <td><a href="' . site_url("front/annonce/ficheCommercantAnnonce/" . $oExpiration15jours->IdCommercant) . '">Page</a></td>
                              </tr>
                            </table>
                            ';
            }
        }


        if (isset($Expiration1jours) && count($Expiration1jours) != 0) {
            $txtContenuAdmin .= '
                        <p>Les abonnements ci-dessous vont expirer dans exactement 1 jour : </p>
                          <table width="100%" border="1" cellspacing="0" cellpadding="0">
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nom</th>
                            <th scope="col">NomSoci&eacute;t&eacute;</th>
                            <th scope="col">Abonnement</th>
                            <th scope="col">Debut</th>
                            <th scope="col">Fin</th>
                            <th scope="col">Lien</th>
                          </tr>';

            foreach ($Expiration1jours as $oExpiration1jours) {

                if ($oExpiration1jours->IdAbonnement == 1) $ComAbonnement = "Partenaire";
                else if ($oExpiration1jours->IdAbonnement == 2) $ComAbonnement = "Partenaire + Bon Plans";
                else if ($oExpiration1jours->IdAbonnement == 3) $ComAbonnement = "Partenaire + Annonces professionnelles";
                else if ($oExpiration1jours->IdAbonnement == 4) $ComAbonnement = "Partenaire + Bon Plans + Annonces professionnelles";
                else $ComAbonnement = "Non definie";


                $commercant1jours = $thiss->Commercant->GetById($oExpiration1jours->IdCommercant);
                $txtContenuAdmin .= '<tr>
                                <td>' . $oExpiration1jours->IdCommercant . '</td>
                                <td>' . $commercant1jours->Nom . ' ' . $commercant1jours->Prenom . '</td>
                                <td>' . $commercant1jours->NomSociete . '</td>
                                <td>' . $ComAbonnement . '</td>
                                <td>' . convertDateWithSlashes($oExpiration1jours->DateDebut) . '</td>
                                <td>' . convertDateWithSlashes($oExpiration1jours->DateFin) . '</td>
                                <td><a href="' . site_url("front/annonce/ficheCommercantAnnonce/" . $oExpiration1jours->IdCommercant) . '">Page</a></td>
                              </tr>
                            </table>
                            ';
            }
        }


        $txtContenuAdmin .= "<p>Cordialement,</p>";

        $current_day_date = date("l");
        $notification_sent = $thiss->notification->isNotificationSent(1);
        //var_dump($notification_sent); die();
        //$colDestAdmin_contact
        if ($current_day_date == "Monday" && isset($notification_sent->is_sent) && $notification_sent->is_sent == "0") {
            @envoi_notification($colDestAdmin, $txtSujetAdmin, $txtContenuAdmin);
            @envoi_notification($colDestAdmin_contact, $txtSujetAdmin, $txtContenuAdmin);
            $thiss->notification->enable(1);
        } else if (isset($Expiration1jours) && count($Expiration1jours) != 0 && isset($notification_sent->is_sent) && $notification_sent->is_sent == "0") {
            @envoi_notification($colDestAdmin, $txtSujetAdmin, $txtContenuAdmin);
            @envoi_notification($colDestAdmin_contact, $txtSujetAdmin, $txtContenuAdmin);
            $thiss->notification->enable(1);
        } else {
            $thiss->notification->disable(1);
        }
        //echo $txtContenuAdmin;

    }

    /////////////////////////////////////////////////////////////////////////////////////END VERIFY SUBSCRIPTION

}


function delete_revue_fin_2journ()
{

    delete_datetime_id_not_affected();

    $ci =& get_instance();
    $date_of_day = date("Y-m-d");
    $time_date = strtotime($date_of_day);
    $time_date_limit = $time_date - 259200;
    $date_limit = date("Y-m-d", $time_date_limit);

    //REMOVING ALL WITH DATE_FIN < MORE 1 WEEK FOR AGENDA
    $zSqlAlldeposantAgendaDatetime = "

            SELECT article.id , COUNT(article.id) as nb_article_time FROM article 
            WHERE agenda_article_type_id = '3' AND  article.date_fin <= '" . $date_limit . "'
            GROUP BY article.id 
             
             ;";

    $article_datetime_list_expire = $ci->db->query($zSqlAlldeposantAgendaDatetime);
    $article_datetime_list_expire_result = $article_datetime_list_expire->result();

    if (is_array($article_datetime_list_expire_result) || is_object($article_datetime_list_expire_result)) {

        foreach ($article_datetime_list_expire_result as $item_n) {
            if (intval($item_n->nb_article_time) <= 1) {
                $ci->db->query(" DELETE FROM article_datetime WHERE article_id= " . $item_n->id);
                $ci->db->query(" DELETE FROM article WHERE agenda_article_type_id='3' AND id= " . $item_n->id);
            } else {
                $sql_date_time_article_id = "
                    DELETE
                    FROM
                    article_datetime
                    WHERE
                    article_id = '" . $item_n->id . "'
                    AND article_datetime.date_fin <= '" . $date_limit . "' ;";
                $ci->db->query($sql_date_time_article_id);
            }
            unlink('/home/priviconfw/www/sortez.org/application/resources/front/photoCommercant/imagesbank/rss_image/'.$item_n->photo1);
        }
    }

    if (!is_array($article_datetime_list_expire)){
        $zSqlAlldeposantAgendaDatetime = "

            SELECT article.id , COUNT(article.id) as nb_article_time FROM article 
            WHERE agenda_article_type_id='3' AND  article.date_debut <= '" . $date_limit . "'
            GROUP BY article.id 
             
             ;";
        $article_datetime_list_expire = $ci->db->query($zSqlAlldeposantAgendaDatetime);
        $article_datetime_list_expire_result = $article_datetime_list_expire->result();
        if (is_array($article_datetime_list_expire_result) || is_object($article_datetime_list_expire_result)) {

            foreach ($article_datetime_list_expire_result as $item_n) {
                if (intval($item_n->nb_article_time) <= 1) {
                    $ci->db->query(" DELETE FROM article_datetime WHERE article_id= " . $item_n->id);
                    $ci->db->query(" DELETE FROM article WHERE agenda_article_type_id='3' AND id= " . $item_n->id);
                } else {
                    $sql_date_time_article_id = "
                    DELETE
                    FROM
                    article_datetime
                    WHERE
                    article_id = '" . $item_n->id . "'
                    AND article_datetime.date_fin <= '" . $date_limit . "' ;";
                    $ci->db->query($sql_date_time_article_id);
                }
            }
        }
    }
}

function delete_datetime_id_not_affected(){
    $ci =& get_instance();
    $article_date_time = " DELETE FROM article_datetime WHERE article_datetime.article_id NOT IN (select article.id from article) ";
    $agenda_date_time = " DELETE FROM agenda_datetime WHERE agenda_datetime.agenda_id NOT IN (select agenda.id from agenda) ";
    $ci->db->query($article_date_time);
    $ci->db->query($agenda_date_time);
}

function start_rss(){
    $ci =& get_instance();
    $ci->load->model('xml_models');
   $all= $ci->xml_models->get_alllink();
   if (is_array($all)){
       foreach ($all as $linked){
           $id=$linked->id;
           if ($id !="" AND $id !=null){
               run6h_rss($id);

           }
       }
   }

}

    function run6h_rss($id){

        $ci =& get_instance();

        $ci->load->model('mdlarticle');
        $ci->load->model('xml_models');
        $item=$ci->xml_models->get_link($id);
                    $lien = $item->source;
                    
                    $categ = $item->agenda_categid;

                    if ($lien != "" AND $categ != "" AND $lien != null  AND $categ != null) {

                        $ci->load->helper('clubproximite');

                        $xml = simplexml_load_file($lien);
                            if ($xml){
                                if (isset($xml->channel->item)) {
                                    //var_dump($xml->channel->item);die();

                                    foreach ($xml->channel->item as $donnee) {
                                        // var_dump($donnee->description);die();
                                        $is = (string)$donnee->description;
                                        $exploded = explode(';', htmlspecialchars($is));
                                        $results = "";

                                        foreach ($exploded as $val) {

                                            if (preg_match("/\jpg\b/i", $val)) {
                                                $results = $val;
                                            }
                                        }
                                        //var_dump($results);
                                        if ($results != "") {
                                            $finals = htmlentities($results, ENT_QUOTES);
                                            $normal = str_replace("&amp;quot", '', $finals);
                                        } else {
                                            if (isset($donnee->enclosure)) {
                                                $enclosure = $donnee->enclosure['url'];
                                                // var_dump($enclosure);
                                            } else {
                                                $photo = null;
                                            }

                                        }
                                        if (isset($normal) AND $normal != null AND $normal != "") {
                                            $final = $normal;
                                        } else {
                                            if (isset($enclosure) AND $enclosure != null AND $enclosure != "") {
                                                //$final=$enclosure;
                                                $test = explode("?", $enclosure);
                                                $utile = $test[0];
                                                $finals = str_replace(" ", '', $utile);
                                                $final = $finals;
                                            }
                                        }


                                        if (isset($final) && $final != null) {
                                            $info = pathinfo($final);
                                            $contents = file_get_contents($final);
                                            $_zExtension = strrchr($info['basename'], '.');
                                            $_zFilename = random_string('unique', 10) . $_zExtension;
                                            $file = 'application/resources/front/photoCommercant/imagesbank/rss_image/' . $_zFilename;
                                            $dir='application/resources/front/photoCommercant/imagesbank/rss_image/';
                                            if (is_dir($dir) == true)
                                            {
                                                @file_put_contents($file, $contents);
                                                //$uploaded_file = new UploadedFile($file, $_zFilename);
                                                if (isset($_zFilename)) $photo = $_zFilename;// save image name on agenda table
                                                if ($file != "") {
                                                    if (is_file($file)) {

                                                        $base_path_system = str_replace('system/', '', BASEPATH);
                                                        $image_path_resize_home = $base_path_system . "/" . $file;
                                                        $image_path_resize_home_final = $base_path_system . "/" . $file;
                                                        $this_imgmoo =& get_instance();
                                                        $this_imgmoo->load->library('image_moo');
                                                        $this_imgmoo->image_moo
                                                            ->load($image_path_resize_home)
                                                            ->resize_crop(640, 480, false)
                                                            ->save($image_path_resize_home_final, true);

                                                    }
                                                }

                                            }else{
                                                mkdir ($dir, 0777);
                                                @file_put_contents($file, $contents);
                                                //$uploaded_file = new UploadedFile($file, $_zFilename);
                                                if (isset($_zFilename)) $photo = $_zFilename;// save image name on agenda table
                                                if ($file != "") {
                                                    if (is_file($file)) {
                                                        $base_path_system = str_replace('system/', '', BASEPATH);
                                                        $image_path_resize_home = $base_path_system . "/" . $file;
                                                        $image_path_resize_home_final = $base_path_system . "/" . $file;
                                                        $this_imgmoo =& get_instance();
                                                        $this_imgmoo->load->library('image_moo');
                                                        $this_imgmoo->image_moo
                                                            ->load($image_path_resize_home)
                                                            ->resize_crop(640, 480, false)
                                                            ->save($image_path_resize_home_final, true);

                                                    }
                                                }
                                            }

                                        }else{$photo=null;}

                                        if (!isset($photo) OR $photo == "") {
                                            $photo = null;
                                        }
/////////////////////////formater date debut et date fin/////////////////////////////

                                        if (isset($donnee->pubDate)) {
                                            $pubdate = $donnee->pubDate;
                                            $testdateformat = explode("-", $pubdate);
                                            if (isset($testdateformat[2])) {
                                                $day = explode(" ", $testdateformat[2]);
                                                $date_pub = $testdateformat[0] . "-" . $testdateformat[1] . "-" . $day[0];
                                            } else {
                                                $ci->load->helper("clubproximite_helper");
                                                $datebrutr = $donnee->pubDate;
                                                $date = convert_xml_date_to_sql_date($datebrutr);
                                                $date_pub = $date;
                                            }
                                        } else {
                                            $date_pub = null;
                                        }

////////////////////////////////////////////////////////////////////////////////////

                                        $date_depot = date("y-m-d");
                                        $fieldb = array('nom_manifestation' => (string)$donnee->title,
                                            'siteweb' => (string)$donnee->link,
                                            'description' => (string)$donnee->description,
                                            'date_depot' => $date_depot,
                                            "article_categid" => $categ,
                                            "IsActif" => 1,
                                            "IdCommercant" => 301299,
                                            "IdUsers_ionauth" => 1992,
                                            "photo1" => $photo,
                                            "date_debut" => $date_pub,
                                            "agenda_article_type_id"=>'3',
                                        );

                                        $test = $ci->xml_models->testexist2((string)$donnee->title);
                                        if ($test == 1 AND (string)$donnee->title !== "") {
                                           $saved= $ci->xml_models->save_to_true_sortez($fieldb);
                                           if ($saved){
                                               $field2= array(  "article_id"=>$saved,
                                                                "date_debut"=>$date_pub);
                                               $ci->xml_models->save2($field2);
                                           }

                                        }

                                    }

                                }
                            }
                        }

                    }
function deleteOldAgenda_fin8jours()
{
    $vic=get_instance();
    $date_of_day = date("Y-m-d");
    $time_date = strtotime($date_of_day);
    $time_date_limit = $time_date - 345600;
    $time_date_limit_article = $time_date - 345600;
    $date_limit = date("Y-m-d", $time_date_limit);
    $date_limit_article = date("Y-m-d", $time_date_limit_article);

    //REMOVING ALL WITH DATE_FIN < MORE 1 WEEK FOR AGENDA
    $zSqlAlldeposantAgendaDatetime = "
            SELECT agenda_datetime.agenda_id, agenda_datetime.id, COUNT(id) as nb_agenda_time FROM agenda_datetime 
            WHERE  agenda_datetime.date_fin <= '" . $date_limit . "'
            GROUP BY agenda_datetime.id 
             ;";
    $agenda_datetime_list_expire = $vic->db->query($zSqlAlldeposantAgendaDatetime);
    $agenda_datetime_list_expire_result = $agenda_datetime_list_expire->result();
    if (is_array($agenda_datetime_list_expire_result) || is_object($agenda_datetime_list_expire_result)) {
        foreach ($agenda_datetime_list_expire_result as $item_n) {
            if (intval($item_n->nb_agenda_time) <= 1) {
                $vic->db->query(" DELETE FROM agenda_datetime WHERE id= " . $item_n->id);
                $vic->db->query(" DELETE FROM agenda WHERE id= " . $item_n->agenda_id);
            } else {
                $sql_date_time_agenda_id = "
                    DELETE
                    FROM
                    agenda_datetime
                    WHERE
                    agenda_id = '" . $item_n->agenda_id . "'
                    AND agenda_datetime.date_fin <= '" . $date_limit . "' ;";
                $vic->db->query($sql_date_time_agenda_id);
            }
        }
    }

    //REMOVING ALL WITH DATE_FIN < MORE 1 WEEK FOR ARTICLE
    $zSqlAlldeposantarticleDatetime_article = "
            SELECT article_datetime.article_id, article_datetime.id, COUNT(id) as nb_article_time FROM article_datetime 
            WHERE  article_datetime.date_fin <= '" . $date_limit_article . "'
            GROUP BY article_datetime.id 
             ;";
    $article_datetime_list_expire = $vic->db->query($zSqlAlldeposantarticleDatetime_article);
    $article_datetime_list_expire_result = $article_datetime_list_expire->result();
    if (is_array($article_datetime_list_expire_result) || is_object($article_datetime_list_expire_result)) {
        foreach ($article_datetime_list_expire_result as $item_n) {
            if (intval($item_n->nb_article_time) <= 1) {
                $vic->db->query(" DELETE FROM article_datetime WHERE id= " . $item_n->id);
                $vic->db->query(" DELETE FROM article WHERE id= " . $item_n->article_id);
            } else {
                $sql_date_time_article_id = "
                    DELETE
                    FROM
                    article_datetime
                    WHERE
                    article_id = '" . $item_n->article_id . "'
                    AND article_datetime.date_fin <= '" . $date_limit_article . "' ;";
                $vic->db->query($sql_date_time_article_id);
            }
        }
    }

}
function remove_2500 ($tab)
{
    $data = new stdClass ;
    if(is_array($tab) && !empty($tab))
    {
        foreach($tab as $key => $val)
        {
            if(is_array($val)){
                if (preg_match('/2.5.0.0/',$val)){
                    $data->$key = remove_2500(preg_replace('/2.5.0.0/','',$val));
                }else{
                    $data->$key = remove_2500(preg_replace('/<div> <\/div>/','',$val));
                }
            }else{
                if (preg_match('/2.5.0.0/',$val)){
                    $data->$key = preg_replace('/2.5.0.0/','',$val);
                }else {
                    $data->$key = preg_replace('/<div> <\/div>/','',$val);
                }
            }
        }
    }
    return $array = (array)$data;
}

//test code mams

function envoi_notification_nwsltr($prmDestinataires = null, $prmSujet = "Notification Sortez", $prmContenu = "", $prmEnvoyeur = "contact@sortez.org", $prmEnvoyeurName = "Sortez") {
    $bRet = false;

    if(isset($prmDestinataires)) {
        if($_SERVER['SERVER_NAME']=="localhost") $base_path_system = str_replace('system\\', '', BASEPATH);
        else $base_path_system = str_replace('system/', '', BASEPATH);
        require_once($base_path_system.'application/resources/phpmailer/PHPMailerAutoload.php');
        $mail             = new PHPMailer();
        $mail->IsHTML(false);
        //$mail->CharSet = "text/html; charset=UTF-8;";
        $mail->CharSet = "UTF-8";
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->Host       = "mail.sortez.org"; // SMTP server
        $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                                                   // 1 = errors and messages
                                                   // 2 = messages only
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->Host       = "ssl0.ovh.net"; // sets the SMTP server
        $mail->SMTPSecure = "ssl";
        $mail->Port       = 465;                    // set the SMTP port for the GMAIL server
        $mail->Username   = "mail@sortez.org"; // SMTP account username
        $mail->Password   = 'J^ou~#|~[{`\'5((*_~#{[]@_(-)';        // SMTP account password
        $mail->SetFrom($prmEnvoyeur, $prmEnvoyeurName);
        $mail->AddReplyTo($prmEnvoyeur, $prmEnvoyeurName);
        $mail->Subject    = $prmSujet;
        $mail->AltBody    = "To view the message, please use an HTML compatible email viewer !"; // optional, comment out and test
        // $prmContenu = '';
        $mail->MsgHTML($prmContenu);
        $mail->AddAddress($prmDestinataires[0]['Email'], $prmDestinataires[0]['Name']);
        if($mail->Send()) {
          $bRet =  true;
        } else {
           return $mail->ErrorInfo;
        }

    }
    return $bRet;
}

//fin test code mams


function envoi_notification_standard($prmDestinataires = null, $prmSujet = "Notification Sortez", $prmContenu = "", $prmEnvoyeur = "contact@sortez.org", $prmEnvoyeurName = "Sortez") {

    $bRet = false;
    if(isset($prmDestinataires)) {
        if($_SERVER['SERVER_NAME']=="localhost") $base_path_system = str_replace('system\\', '', BASEPATH);
        else $base_path_system = str_replace('system/', '', BASEPATH);
        require_once($base_path_system.'application/resources/phpmailer/PHPMailerAutoload.php');
        $mail             = new PHPMailer();
        $mail->IsHTML(false);
        //$mail->CharSet = "text/html; charset=UTF-8;";
        $mail->CharSet = "UTF-8";
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->Host       = "mail.sortez.org"; // SMTP server
        $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
        // 1 = errors and messages
        // 2 = messages only
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->Host       = "ssl0.ovh.net"; // sets the SMTP server
        $mail->SMTPSecure = "ssl";
        $mail->Port       = 465;                    // set the SMTP port for the GMAIL server
        $mail->Username   = "mail@sortez.org"; // SMTP account username
        $mail->Password   = 'J^ou~#|~[{`\'5((*_~#{[]@_(-)';        // SMTP account password
        $mail->SetFrom($prmEnvoyeur, $prmEnvoyeurName);
        $mail->AddReplyTo($prmEnvoyeur, $prmEnvoyeurName);
        $mail->Subject    = $prmSujet;
        $mail->AltBody    = "To view the message, please use an HTML compatible email viewer !"; // optional, comment out and test
        $prmContenu = '';
        $mail->MsgHTML($prmContenu);
        $mail->AddAddress($prmDestinataires[0]['Email'], $prmDestinataires[0]['Name']);
        if($mail->Send()) {
            $bRet =  true;
        } else {
            return $mail->ErrorInfo;
        }


        /*
        $CI =& get_instance();
        $mail = '<div style="font-family:Arial, Helvetica, sans-serif; font-size:13px;">'.$prmContenu.'<p><hr><strong>Priviconcept</strong><br/>SAS 18, rue des Combattants d\'Afrique du Nord 06000 Nice<br/><a href="mailto:contact@sortez.org">contact@sortez.org</a></p></div>';
        $CI->load->library('email');
        $CI->email->from($prmEnvoyeur, $prmEnvoyeurName);
        $CI->email->to($prmDestinataires[0]['Email']);
        $CI->email->subject($prmSujet);
        $CI->email->message($mail);
        $result_mail = $CI->email->send();
        if($result_mail) {
            $bRet =  true;
        } else {
            $bRet =  false;
        }
        */
    }
    return $bRet;
}
 function qrcodeGenerator ($params_qrcode)
{
    $CI =& get_instance();
    $CI->load->library('phpqrcode/ci_qr_code');
    $CI->config->load('qr_code');
    $qr_code_config = array();
    $qr_code_config['cacheable'] = $CI->config->item('cacheable');
    $qr_code_config['cachedir'] = $CI->config->item('cachedir');
    $qr_code_config['imagedir'] = $CI->config->item('imagedir');
    $qr_code_config['errorlog'] = $CI->config->item('errorlog');
    $qr_code_config['ciqrcodelib'] = $CI->config->item('ciqrcodelib');
    $qr_code_config['quality'] = $CI->config->item('quality');
    $qr_code_config['size'] = $CI->config->item('size');
    $qr_code_config['black'] = $CI->config->item('black');
    $qr_code_config['white'] = $CI->config->item('white');
    $CI->ci_qr_code->initialize($qr_code_config);

    $CI->ci_qr_code->generate($params_qrcode);

}

// function  statistiques()
//{
//    if($_SERVER['HTTP_HOST'] == "www.testpriviconcept.ovh"){
//        $link = mysqli_connect("priviconfwsortez.mysql.db", "priviconfwdb2", "KJLiu5oi65", "priviconfwdb2");
//        // $conn = new mysqli(priviconfwsortez.mysql.db", "priviconfwdb2", KJLiu5oi65", "priviconfwdb2");
//        // Check connection
//        if($link === false){
//            die("ERROR: Could not connect. " . mysqli_connect_error());
//        }else{
//            // $sql = "INSERT INTO `statistiques`(`id`, `visite_sortez`) VALUES (null,1)";
//            //debut
//            $ip_address = $_SERVER["REMOTE_ADDR"];
////            var_dump($ip_address);die('ok');
//            $get_id = "SELECT * FROM `statistiques` WHERE `Adress_Ip`='$ip_address'";
//            $get_data = mysqli_query($link, $get_id);
////            var_dump($get_data);die('kokokokokokokko');
//            if(mysqli_num_rows($get_data) > 0 ){
//                        while ($row_get_id = mysqli_fetch_array($get_data)) {
//                            $visite_sortez=$row_get_id['visite_sortez'];
//                            $user_agent = $_SERVER['HTTP_USER_AGENT']; //user browser
//                            $ip_address = $_SERVER["REMOTE_ADDR"];     // user ip adderss
//                            date_default_timezone_set('UTC');
//                            $date = date("Y-m-d");
//                            $time = date("H:i:s");
////                        var_dump($ip_address);die('kokko adress IP');
//                            $visite_sortez=$visite_sortez+1;
//                            $sql = "UPDATE `statistiques` SET `visite_sortez`='.$visite_sortez.' WHERE `Adress_Ip`='$ip_address' ";
//                            if(mysqli_query($link, $sql)){
//
//                            }else{
//
//                            }
//                        }
//                        mysqli_free_result($result_get_id);
//            }
//            else{
//                $user_agent = $_SERVER['HTTP_USER_AGENT']; //user browser
//                $ip_address = $_SERVER["REMOTE_ADDR"];     // user ip adderss
//                date_default_timezone_set('UTC');
//                $date = date("Y-m-d");
//                $time = date("H:i:s");
//                var_dump($user_agent);die('kokokokokokokoko');
//                $sql = "INSERT INTO `statistiques`(`id`, `Adress_Ip`,`visite_sortez`,`browser`,`date`,`time`) VALUES (null,$ip_address,1,$user_agent,$date,$time)";
//            }
//        }
//    }
//}

// function statistic for the recuperation number of click
// edit by Betadev Randevteam
function  statistiques()
{
    if($_SERVER['HTTP_HOST'] == "www.sortez.org"){
        $link = mysqli_connect("priviconfwsortez.mysql.db", "priviconfwsortez", "jk98rezOPI", "priviconfwsortez");
        // Check connection
        if($link === false){
            die("ERROR: Could not connect. " . mysqli_connect_error());
        }else{
            //debut
            $ua=getBrowser();
            $yourbrowser= $ua['name'] . " " . $ua['version'];
            $random_string = chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)); // random(ish) 13 character
            if(isset($_COOKIE['User']) && $_COOKIE['User'] != null){
                $name = $_COOKIE['User'];
                $get_id = "SELECT * FROM `statistiques` WHERE `Adress_Ip`='$name'";
                $get_data = mysqli_query($link, $get_id);
                if(mysqli_num_rows($get_data) > 0 ) {
                        $row = $get_data->fetch_assoc();
                        date_default_timezone_set('UTC');
                        $date = date("Y-m-d");
                        $time = date("H:i:s");
                        $visite_sortez = $row['visite_sortez'] + 1;
                        if($row['date'] == $date && $row['Adress_Ip'] == $name){
                            $sql = "UPDATE `statistiques` SET `visite_sortez`='.$visite_sortez.',`browser`='$yourbrowser',`date`='$date',`time`='$time' WHERE `Adress_Ip`='$name' ";
                            $link->query($sql);
                        }
                        else{
                            setcookie("User", "$random_string", time()+2*24*60*60);
                            $name = ($_COOKIE['User'] = $random_string);
                            $sql = "INSERT INTO `statistiques`(`id`, `Adress_Ip`, `visite_sortez`, `browser`, `date`, `time`) VALUES ('','$name','1','$yourbrowser','$date','$time')";
                            $link->query($sql);
                        }
                }elseif(mysqli_num_rows($get_data) == 0){
                    setcookie("User", "$random_string", time()+2*24*60*60);
                    $name = ($_COOKIE['User'] = $random_string);
                    date_default_timezone_set('UTC');
                    $date = date("Y-m-d");
                    $time = date("H:i:s");
                    $sql = "INSERT INTO `statistiques`(`id`, `Adress_Ip`, `visite_sortez`, `browser`, `date`, `time`) VALUES ('','$name','1','$yourbrowser','$date','$time')";
                    $link->query($sql);

                }
            }else{
                setcookie("User", "$random_string", time()+2*24*60*60);
                $name = ($_COOKIE['User'] = $random_string);
                date_default_timezone_set('UTC');
                $date = date("Y-m-d");
                $time = date("H:i:s");
                $sql = "INSERT INTO `statistiques`(`id`, `Adress_Ip`, `visite_sortez`, `browser`, `date`, `time`) VALUES ('','$name','1','$yourbrowser','$date','$time')";
                $link->query($sql);
            }
//            $ip_address = $_SERVER['REMOTE_ADDR'];
//            if($ip_address == '197.158.89.124'){
//                $_SERVER['REMOTE_ADDR'] = '197.158.89.125';
//                $ip_address = $_SERVER['REMOTE_ADDR'];
////                var_dump($ip_address);die('ok');
////                return $_SERVER['REMOTE_ADDR'];
//            }
//            $server = $_SERVER;
//
//            $User = $_SERVER['HTTP_COOKIE'];
//            $City = $_SERVER['GEOIP_CITY'];
//
//
////            var_dump($server);
////            var_dump($ip_address);
//            $get_id = "SELECT * FROM `statistiques` WHERE `Adress_Ip`='$ip_address'";
//            $get_data = mysqli_query($link, $get_id);
//            if(mysqli_num_rows($get_data) == 1 ) {
////                var_dump('if');die('koko');
//                while ($row = $get_data->fetch_assoc()) {
////                echo "id: " . $row["id"]. " - Name: " . $row["visite_sortez"]. "<br>";
//                    date_default_timezone_set('UTC');
//                    $date = date("Y-m-d");
//                    $time = date("H:i:s");
//                    $visite_sortez = $row["visite_sortez"] + 1;
//                    $sql = "UPDATE `statistiques` SET `visite_sortez`='.$visite_sortez.',`date`='$date',`time`='$time' WHERE `Adress_Ip`='$ip_address' ";
////            $sql = " UPDATE `statistiques` SET `date`='2019-07-13',`time`='12:13:47' WHERE 1 ";
//
//                    $link->query($sql);
//                }
//            }
//            elseif(mysqli_num_rows($get_data) == 0){
////                var_dump('else');
//                $ip_address = $_SERVER["REMOTE_ADDR"];     // user ip adderss
//                date_default_timezone_set('UTC');
//                $date = date("Y-m-d");
//                $time = date("H:i:s");
////                var_dump($user_agent);die('kokokokokokokoko');
//                $sql = "INSERT INTO `statistiques`(`id`, `Adress_Ip`, `visite_sortez`, `browser`, `date`, `time`) VALUES ('','$ip_address','1','$yourbrowser','$date','$time')";
//                $link->query($sql);
//            }
        }
    }
}
// function get browser
// edit by betadev
function getBrowser()
{
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }

    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
    {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    }
    elseif(preg_match('/Firefox/i',$u_agent))
    {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    }
    elseif(preg_match('/Chrome/i',$u_agent))
    {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    }
    elseif(preg_match('/Safari/i',$u_agent))
    {
        $bname = 'Apple Safari';
        $ub = "Safari";
    }
    elseif(preg_match('/Opera/i',$u_agent))
    {
        $bname = 'Opera';
        $ub = "Opera";
    }
    elseif(preg_match('/Netscape/i',$u_agent))
    {
        $bname = 'Netscape';
        $ub = "Netscape";
    }

    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
        ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }

    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }

    // check if we have a number
    if ($version==null || $version=="") {$version="?";}

    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern
    );
}





