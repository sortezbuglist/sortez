<?php
function convert_ind_to_sql_date_counted7($exploded){
    if ($exploded[3] == 'janvier' || $exploded[3] == 'janv.') $exploded[3] = "01";
    if ($exploded[3] == 'février' || $exploded[3] == 'Fevier.' || $exploded[3] == 'févr.') $exploded[3] = "02";
    if ($exploded[3] == 'mars' || $exploded[3] == 'mars.') $exploded[3] = "03";
    if ($exploded[3] == 'avril' || $exploded[3] == 'avr.') $exploded[3] = "04";
    if ($exploded[3] == 'mai' || $exploded[3] == 'mai.') $exploded[3] = "05";
    if ($exploded[3] == 'juin' || $exploded[3] == 'juin.') $exploded[3] = "06";
    if ($exploded[3] == 'juillet' || $exploded[3] == 'juil.') $exploded[3] = "07";
    if ($exploded[3] == 'août' || $exploded[3] == 'aout.' || $exploded[3] == 'Aug.') $exploded[3] = "08";
    if ($exploded[3] == 'septembre' || $exploded[3] == 'sept.') $exploded[3] = "09";
    if ($exploded[3] == 'octobre' || $exploded[3] == 'oct.') $exploded[3] = "10";
    if ($exploded[3] == 'novembre' || $exploded[3] == 'nov.') $exploded[3] = "11";
    if ($exploded[3] == 'décembre' || $exploded[3] == 'Decembre.' || $exploded[3] == 'Déc.') $exploded[3] = "12";

    $date_debut=$exploded[4].'-'.$exploded[3].'-'.$exploded[2];
    //$heure_debut=$exploded[6];
    return $date_debut;
}
function convert_ind_to_sql_heure_counted7($exploded){
    $d = "https://www.sortez.org/infos-consommateurs.html";
    $exploded = explode(' ',$d);
    if ($exploded[3] == 'janvier' || $exploded[3] == 'janv.') $exploded[3] = "01";
    if ($exploded[3] == 'février' || $exploded[3] == 'Fevier.' || $exploded[3] == 'févr.') $exploded[3] = "02";
    if ($exploded[3] == 'mars' || $exploded[3] == 'mars.') $exploded[3] = "03";
    if ($exploded[3] == 'avril' || $exploded[3] == 'avr.') $exploded[3] = "04";
    if ($exploded[3] == 'mai' || $exploded[3] == 'mai.') $exploded[3] = "05";
    if ($exploded[3] == 'juin' || $exploded[3] == 'juin.') $exploded[3] = "06";
    if ($exploded[3] == 'juillet' || $exploded[3] == 'juil.') $exploded[3] = "07";
    if ($exploded[3] == 'août' || $exploded[3] == 'aout.' || $exploded[3] == 'Aug.') $exploded[3] = "08";
    if ($exploded[3] == 'septembre' || $exploded[3] == 'sept.') $exploded[3] = "09";
    if ($exploded[3] == 'octobre' || $exploded[3] == 'oct.') $exploded[3] = "10";
    if ($exploded[3] == 'novembre' || $exploded[3] == 'nov.') $exploded[3] = "11";
    if ($exploded[3] == 'décembre' || $exploded[3] == 'Decembre.' || $exploded[3] == 'Déc.') $exploded[3] = "12";
    //$date_debut=$exploded[4].'-'.$exploded[3].'-'.$exploded[2];
    $heure_debut=$exploded[6];
    return $heure_debut;
}


function convert_ind_to_sql_date_debut_counted13($exploded){
    if ($exploded[3] == 'janvier' || $exploded[3] == 'janv.') $exploded[3] = "01";
    if ($exploded[3] == 'février' || $exploded[3] == 'Fevier.' || $exploded[3] == 'févr.') $exploded[3] = "02";
    if ($exploded[3] == 'mars' || $exploded[3] == 'mars.') $exploded[3] = "03";
    if ($exploded[3] == 'avril' || $exploded[3] == 'avr.') $exploded[3] = "04";
    if ($exploded[3] == 'mai' || $exploded[3] == 'mai.') $exploded[3] = "05";
    if ($exploded[3] == 'juin' || $exploded[3] == 'juin.') $exploded[3] = "06";
    if ($exploded[3] == 'juillet' || $exploded[3] == 'juil.') $exploded[3] = "07";
    if ($exploded[3] == 'août' || $exploded[3] == 'aout.' || $exploded[3] == 'Aug.') $exploded[3] = "08";
    if ($exploded[3] == 'septembre' || $exploded[3] == 'sept.') $exploded[3] = "09";
    if ($exploded[3] == 'octobre' || $exploded[3] == 'oct.') $exploded[3] = "10";
    if ($exploded[3] == 'novembre' || $exploded[3] == 'nov.') $exploded[3] = "11";
    if ($exploded[3] == 'décembre' || $exploded[3] == 'Decembre.' || $exploded[3] == 'Déc.') $exploded[3] = "12";


    if ($exploded[9] == 'janvier' || $exploded[9] == 'janv.') $exploded[9] = "01";
    if ($exploded[9] == 'février' || $exploded[9] == 'Fevier.' || $exploded[9] == 'févr.') $exploded[9] = "02";
    if ($exploded[9] == 'mars' || $exploded[9] == 'mars.') $exploded[9] = "03";
    if ($exploded[9] == 'avril' || $exploded[9] == 'avr.') $exploded[9] = "04";
    if ($exploded[9] == 'mai' || $exploded[9] == 'mai.') $exploded[9] = "05";
    if ($exploded[9] == 'juin' || $exploded[9] == 'juin.') $exploded[9] = "06";
    if ($exploded[9] == 'juillet' || $exploded[9] == 'juil.') $exploded[9] = "07";
    if ($exploded[9] == 'août' || $exploded[9] == 'aout.' || $exploded[9] == 'Aug.') $exploded[9] = "08";
    if ($exploded[9] == 'septembre' || $exploded[9] == 'sept.') $exploded[9] = "09";
    if ($exploded[9] == 'octobre' || $exploded[9] == 'oct.') $exploded[9] = "10";
    if ($exploded[9] == 'novembre' || $exploded[9] == 'nov.') $exploded[9] = "11";
    if ($exploded[9] == 'décembre' || $exploded[9] == 'Decembre.' || $exploded[9] == 'Déc.') $exploded[9] = "12";

    $date_debut=$exploded[4].'-'.$exploded[3].'-'.$exploded[2];
    return $date_debut;
}


function convert_ind_to_sql_date_fin_counted13($exploded){
    if ($exploded[3] == 'janvier' || $exploded[3] == 'janv.') $exploded[3] = "01";
    if ($exploded[3] == 'février' || $exploded[3] == 'Fevier.' || $exploded[3] == 'févr.') $exploded[3] = "02";
    if ($exploded[3] == 'mars' || $exploded[3] == 'mars.') $exploded[3] = "03";
    if ($exploded[3] == 'avril' || $exploded[3] == 'avr.') $exploded[3] = "04";
    if ($exploded[3] == 'mai' || $exploded[3] == 'mai.') $exploded[3] = "05";
    if ($exploded[3] == 'juin' || $exploded[3] == 'juin.') $exploded[3] = "06";
    if ($exploded[3] == 'juillet' || $exploded[3] == 'juil.') $exploded[3] = "07";
    if ($exploded[3] == 'août' || $exploded[3] == 'aout.' || $exploded[3] == 'Aug.') $exploded[3] = "08";
    if ($exploded[3] == 'septembre' || $exploded[3] == 'sept.') $exploded[3] = "09";
    if ($exploded[3] == 'octobre' || $exploded[3] == 'oct.') $exploded[3] = "10";
    if ($exploded[3] == 'novembre' || $exploded[3] == 'nov.') $exploded[3] = "11";
    if ($exploded[3] == 'décembre' || $exploded[3] == 'Decembre.' || $exploded[3] == 'Déc.') $exploded[3] = "12";


    if ($exploded[9] == 'janvier' || $exploded[9] == 'janv.') $exploded[9] = "01";
    if ($exploded[9] == 'février' || $exploded[9] == 'Fevier.' || $exploded[9] == 'févr.') $exploded[9] = "02";
    if ($exploded[9] == 'mars' || $exploded[9] == 'mars.') $exploded[9] = "03";
    if ($exploded[9] == 'avril' || $exploded[9] == 'avr.') $exploded[9] = "04";
    if ($exploded[9] == 'mai' || $exploded[9] == 'mai.') $exploded[9] = "05";
    if ($exploded[9] == 'juin' || $exploded[9] == 'juin.') $exploded[9] = "06";
    if ($exploded[9] == 'juillet' || $exploded[9] == 'juil.') $exploded[9] = "07";
    if ($exploded[9] == 'août' || $exploded[9] == 'aout.' || $exploded[9] == 'Aug.') $exploded[9] = "08";
    if ($exploded[9] == 'septembre' || $exploded[9] == 'sept.') $exploded[9] = "09";
    if ($exploded[9] == 'octobre' || $exploded[9] == 'oct.') $exploded[9] = "10";
    if ($exploded[9] == 'novembre' || $exploded[9] == 'nov.') $exploded[9] = "11";
    if ($exploded[9] == 'décembre' || $exploded[9] == 'Decembre.' || $exploded[9] == 'Déc.') $exploded[9] = "12";

    $date_fin=$exploded[10].'-'.$exploded[9].'-'.$exploded[8];
    return $date_fin;
}


function convert_ind_to_sql_heure_debut_counted13($exploded){
    if ($exploded[3] == 'janvier' || $exploded[3] == 'janv.') $exploded[3] = "01";
    if ($exploded[3] == 'février' || $exploded[3] == 'Fevier.' || $exploded[3] == 'févr.') $exploded[3] = "02";
    if ($exploded[3] == 'mars' || $exploded[3] == 'mars.') $exploded[3] = "03";
    if ($exploded[3] == 'avril' || $exploded[3] == 'avr.') $exploded[3] = "04";
    if ($exploded[3] == 'mai' || $exploded[3] == 'mai.') $exploded[3] = "05";
    if ($exploded[3] == 'juin' || $exploded[3] == 'juin.') $exploded[3] = "06";
    if ($exploded[3] == 'juillet' || $exploded[3] == 'juil.') $exploded[3] = "07";
    if ($exploded[3] == 'août' || $exploded[3] == 'aout.' || $exploded[3] == 'Aug.') $exploded[3] = "08";
    if ($exploded[3] == 'septembre' || $exploded[3] == 'sept.') $exploded[3] = "09";
    if ($exploded[3] == 'octobre' || $exploded[3] == 'oct.') $exploded[3] = "10";
    if ($exploded[3] == 'novembre' || $exploded[3] == 'nov.') $exploded[3] = "11";
    if ($exploded[3] == 'décembre' || $exploded[3] == 'Decembre.' || $exploded[3] == 'Déc.') $exploded[3] = "12";


    if ($exploded[9] == 'janvier' || $exploded[9] == 'janv.') $exploded[9] = "01";
    if ($exploded[9] == 'février' || $exploded[9] == 'Fevier.' || $exploded[9] == 'févr.') $exploded[9] = "02";
    if ($exploded[9] == 'mars' || $exploded[9] == 'mars.') $exploded[9] = "03";
    if ($exploded[9] == 'avril' || $exploded[9] == 'avr.') $exploded[9] = "04";
    if ($exploded[9] == 'mai' || $exploded[9] == 'mai.') $exploded[9] = "05";
    if ($exploded[9] == 'juin' || $exploded[9] == 'juin.') $exploded[9] = "06";
    if ($exploded[9] == 'juillet' || $exploded[9] == 'juil.') $exploded[9] = "07";
    if ($exploded[9] == 'août' || $exploded[9] == 'aout.' || $exploded[9] == 'Aug.') $exploded[9] = "08";
    if ($exploded[9] == 'septembre' || $exploded[9] == 'sept.') $exploded[9] = "09";
    if ($exploded[9] == 'octobre' || $exploded[9] == 'oct.') $exploded[9] = "10";
    if ($exploded[9] == 'novembre' || $exploded[9] == 'nov.') $exploded[9] = "11";
    if ($exploded[9] == 'décembre' || $exploded[9] == 'Decembre.' || $exploded[9] == 'Déc.') $exploded[9] = "12";


    $heure_debut=$exploded[6];

    return $heure_debut;
}

function convert_date_debut_fin_to_sql_date_debut($all,$date){

    $date_debut_brute=$all[0];
    $deb=explode( ' ',$date_debut_brute);
    if ($deb[2] == 'janvier' || $deb[2] == 'jan') $deb[2] = "01";
    if ($deb[2] == 'février' || $deb[2] == 'Fevier' || $deb[2] == 'fev') $deb[2] = "02";
    if ($deb[2] == 'mars' || $deb[2] == 'mars') $deb[2] = "03";
    if ($deb[2] == 'avril' || $deb[2] == 'avr') $deb[2] = "04";
    if ($deb[2] == 'mai' || $deb[2] == 'mai') $deb[2] = "05";
    if ($deb[2] == 'juin' || $deb[2] == 'juin') $deb[2] = "06";
    if ($deb[2] == 'juillet' || $deb[2] == 'juil') $deb[2] = "07";
    if ($deb[2] == 'août' || $deb[2] == 'aout' || $deb[2] == 'Aug') $deb[2] = "08";
    if ($deb[2] == 'septembre' || $deb[2] == 'sept') $deb[2] = "09";
    if ($deb[2] == 'octobre' || $deb[2] == 'oct') $deb[2] = "10";
    if ($deb[2] == 'novembre' || $deb[2] == 'nov') $deb[2] = "11";
    if ($deb[2] == 'décembre' || $deb[2] == 'Decembre' || $deb[2] == 'dec') $deb[2] = "12";
    $date_debut=$deb[3]."-".$deb[2].'-'.$deb[1];

    if (preg_match('/:/',$date)){
        $heure=explode('-',$date);
        $heure_debut_b=preg_replace("/ /","",$heure);
        $heure_debut=$heure_debut_b[1];
    }
    $date_fin_brute=$all[1];
    $debs=explode( ' ',$date_fin_brute);
    if (count($debs)>3){

        if ($debs[3] == 'janvier' || $debs[3] == 'jan') $debs[3] = "01";
        if ($debs[3] == 'février' || $debs[3] == 'Fevier' || $debs[3] == 'fev') $debs[3] = "02";
        if ($debs[3] == 'mars' || $debs[3] == 'mars') $debs[3] = "03";
        if ($debs[3] == 'avril' || $debs[3] == 'avr') $debs[3] = "04";
        if ($debs[3] == 'mai' || $debs[3] == 'mai') $debs[3] = "05";
        if ($debs[3] == 'juin' || $debs[3] == 'juin') $debs[3] = "06";
        if ($debs[3] == 'juillet' || $debs[3] == 'juil') $debs[3] = "07";
        if ($debs[3] == 'août' || $debs[3] == 'aout' || $debs[3] == 'Aug') $debs[3] = "08";
        if ($debs[3] == 'septembre' || $debs[3] == 'sept') $debs[3] = "09";
        if ($debs[3] == 'octobre' || $debs[3] == 'oct') $debs[3] = "10";
        if ($debs[3] == 'novembre' || $debs[3] == 'nov') $debs[3] = "11";
        if ($debs[3] == 'décembre' || $debs[3] == 'Decembre' || $debs[3] == 'dec') $debs[3] = "12";
        $date_fin=$debs[4]."-".$debs[3].'-'.$debs[2];

    }
return $date_debut;
}

function convert_date_debut_fin_to_sql_date_fin($all,$date){

    $date_debut_brute=$all[0];
    $deb=explode( ' ',$date_debut_brute);
    if ($deb[2] == 'janvier' || $deb[2] == 'jan') $deb[2] = "01";
    if ($deb[2] == 'février' || $deb[2] == 'Fevier' || $deb[2] == 'fev') $deb[2] = "02";
    if ($deb[2] == 'mars' || $deb[2] == 'mars') $deb[2] = "03";
    if ($deb[2] == 'avril' || $deb[2] == 'avr') $deb[2] = "04";
    if ($deb[2] == 'mai' || $deb[2] == 'mai') $deb[2] = "05";
    if ($deb[2] == 'juin' || $deb[2] == 'juin') $deb[2] = "06";
    if ($deb[2] == 'juillet' || $deb[2] == 'juil') $deb[2] = "07";
    if ($deb[2] == 'août' || $deb[2] == 'aout' || $deb[2] == 'Aug') $deb[2] = "08";
    if ($deb[2] == 'septembre' || $deb[2] == 'sept') $deb[2] = "09";
    if ($deb[2] == 'octobre' || $deb[2] == 'oct') $deb[2] = "10";
    if ($deb[2] == 'novembre' || $deb[2] == 'nov') $deb[2] = "11";
    if ($deb[2] == 'décembre' || $deb[2] == 'Decembre' || $deb[2] == 'dec') $deb[2] = "12";
    $date_debut=$deb[3]."-".$deb[2].'-'.$deb[1];

    if (preg_match('/:/',$date)){
        $heure=explode('-',$date);
        $heure_debut_b=preg_replace("/ /","",$heure);
        $heure_debut=$heure_debut_b[1];
    }
    $date_fin_brute=$all[1];
    $debs=explode( ' ',$date_fin_brute);
    if (count($debs)>3){

        if ($debs[3] == 'janvier' || $debs[3] == 'jan') $debs[3] = "01";
        if ($debs[3] == 'février' || $debs[3] == 'Fevier' || $debs[3] == 'fev') $debs[3] = "02";
        if ($debs[3] == 'mars' || $debs[3] == 'mars') $debs[3] = "03";
        if ($debs[3] == 'avril' || $debs[3] == 'avr') $debs[3] = "04";
        if ($debs[3] == 'mai' || $debs[3] == 'mai') $debs[3] = "05";
        if ($debs[3] == 'juin' || $debs[3] == 'juin') $debs[3] = "06";
        if ($debs[3] == 'juillet' || $debs[3] == 'juil') $debs[3] = "07";
        if ($debs[3] == 'août' || $debs[3] == 'aout' || $debs[3] == 'Aug') $debs[3] = "08";
        if ($debs[3] == 'septembre' || $debs[3] == 'sept') $debs[3] = "09";
        if ($debs[3] == 'octobre' || $debs[3] == 'oct') $debs[3] = "10";
        if ($debs[3] == 'novembre' || $debs[3] == 'nov') $debs[3] = "11";
        if ($debs[3] == 'décembre' || $debs[3] == 'Decembre' || $debs[3] == 'dec') $debs[3] = "12";
        $date_fin=$debs[4]."-".$debs[3].'-'.$debs[2];

    }
    return $date_fin;
}


function convert_date_debut_fin_to_sql_heure_debut($all,$date){

    $date_debut_brute=$all[0];
    $deb=explode( ' ',$date_debut_brute);
    if ($deb[2] == 'janvier' || $deb[2] == 'jan') $deb[2] = "01";
    if ($deb[2] == 'février' || $deb[2] == 'Fevier' || $deb[2] == 'fev') $deb[2] = "02";
    if ($deb[2] == 'mars' || $deb[2] == 'mars') $deb[2] = "03";
    if ($deb[2] == 'avril' || $deb[2] == 'avr') $deb[2] = "04";
    if ($deb[2] == 'mai' || $deb[2] == 'mai') $deb[2] = "05";
    if ($deb[2] == 'juin' || $deb[2] == 'juin') $deb[2] = "06";
    if ($deb[2] == 'juillet' || $deb[2] == 'juil') $deb[2] = "07";
    if ($deb[2] == 'août' || $deb[2] == 'aout' || $deb[2] == 'Aug') $deb[2] = "08";
    if ($deb[2] == 'septembre' || $deb[2] == 'sept') $deb[2] = "09";
    if ($deb[2] == 'octobre' || $deb[2] == 'oct') $deb[2] = "10";
    if ($deb[2] == 'novembre' || $deb[2] == 'nov') $deb[2] = "11";
    if ($deb[2] == 'décembre' || $deb[2] == 'Decembre' || $deb[2] == 'dec') $deb[2] = "12";
    $date_debut=$deb[3]."-".$deb[2].'-'.$deb[1];

    if (preg_match('/:/',$date)){
        $heure=explode('-',$date);
        $heure_debut_b=preg_replace("/ /","",$heure);
        $heure_debut=$heure_debut_b[1];
    }
    $date_fin_brute=$all[1];
    $debs=explode( ' ',$date_fin_brute);
    if (count($debs)>3){

        if ($debs[3] == 'janvier' || $debs[3] == 'jan') $debs[3] = "01";
        if ($debs[3] == 'février' || $debs[3] == 'Fevier' || $debs[3] == 'fev') $debs[3] = "02";
        if ($debs[3] == 'mars' || $debs[3] == 'mars') $debs[3] = "03";
        if ($debs[3] == 'avril' || $debs[3] == 'avr') $debs[3] = "04";
        if ($debs[3] == 'mai' || $debs[3] == 'mai') $debs[3] = "05";
        if ($debs[3] == 'juin' || $debs[3] == 'juin') $debs[3] = "06";
        if ($debs[3] == 'juillet' || $debs[3] == 'juil') $debs[3] = "07";
        if ($debs[3] == 'août' || $debs[3] == 'aout' || $debs[3] == 'Aug') $debs[3] = "08";
        if ($debs[3] == 'septembre' || $debs[3] == 'sept') $debs[3] = "09";
        if ($debs[3] == 'octobre' || $debs[3] == 'oct') $debs[3] = "10";
        if ($debs[3] == 'novembre' || $debs[3] == 'nov') $debs[3] = "11";
        if ($debs[3] == 'décembre' || $debs[3] == 'Decembre' || $debs[3] == 'dec') $debs[3] = "12";
        $date_fin=$debs[4]."-".$debs[3].'-'.$debs[2];

    }
    return $heure_debut;
}

function convert_date_nik_to_sql_date($exploded){

    if ($exploded[2] == 'janv.') $exploded[2] = "01";
    if ($exploded[2] == 'févr.' || $exploded[2] == 'Feb.') $exploded[2] = "02";
    if ($exploded[2] == 'mars.' OR $exploded[2] == 'mars' ) $exploded[2] = "03";
    if ($exploded[2] == 'avril.' OR $exploded[2] == 'avril') $exploded[2] = "04";
    if ($exploded[2] == 'mai.' OR $exploded[2] == 'mai') $exploded[2] = "05";
    if ($exploded[2] == 'juin.' OR $exploded[2] == 'juin') $exploded[2] = "06";
    if ($exploded[2] == 'juillet.') $exploded[2] = "07";
    if ($exploded[2] == 'Aout.' || $exploded[2] == 'Août') $exploded[2] = "08";
    if ($exploded[2] == 'Sep.') $exploded[2] = "09";
    if ($exploded[2] == 'oct.') $exploded[2] = "10";
    if ($exploded[2] == 'nov.') $exploded[2] = "11";
    if ($exploded[2] == 'déc.' || $exploded[2] == 'Dec.') $exploded[2] = "12";
    if ($exploded[1]<10)$exploded[1]='0'.$exploded[1];

    $date_debut=$exploded[3].'-'.$exploded[2].'-'.$exploded[1];
    return $date_debut;
}

function convert_nik_date_with_date_fin_to_sql_date_debut($exploded_date_debut,$exploded_date_debut_date_fin){

    if ($exploded_date_debut[1] == 'janv.') $exploded_date_debut[1] = "01";
    if ($exploded_date_debut[1] == 'févr.' || $exploded_date_debut[1] == 'Feb.') $exploded_date_debut[1] = "02";
    if ($exploded_date_debut[1] == 'mars.') $exploded_date_debut[1] = "03";
    if ($exploded_date_debut[1] == 'avril.') $exploded_date_debut[1] = "04";
    if ($exploded_date_debut[1] == 'mai.') $exploded_date_debut[1] = "05";
    if ($exploded_date_debut[1] == 'juin.') $exploded_date_debut[1] = "06";
    if ($exploded_date_debut[1] == 'juillet.') $exploded_date_debut[1] = "07";
    if ($exploded_date_debut[1] == 'Aout.' || $exploded_date_debut[1] == 'Aug') $exploded_date_debut[1] = "08";
    if ($exploded_date_debut[1] == 'Sep.') $exploded_date_debut[1] = "09";
    if ($exploded_date_debut[1] == 'oct.') $exploded_date_debut[1] = "10";
    if ($exploded_date_debut[1] == 'nov.') $exploded_date_debut[1] = "11";
    if ($exploded_date_debut[1] == 'déc.' || $exploded_date_debut[1] == 'Dec.') $exploded_date_debut[1] = "12";
    if ($exploded_date_debut[0]<10)$exploded_date_debut[1]='0'.$exploded_date_debut[0];

    if ($exploded_date_debut_date_fin[1] == 'janv.') $exploded_date_debut_date_fin[1] = "01";
    if ($exploded_date_debut_date_fin[1] == 'févr.' || $exploded_date_debut_date_fin[1] == 'Feb.') $exploded_date_debut_date_fin[1] = "02";
    if ($exploded_date_debut_date_fin[1] == 'mars.') $exploded_date_debut_date_fin[1] = "03";
    if ($exploded_date_debut_date_fin[1] == 'avril.') $exploded_date_debut_date_fin[1] = "04";
    if ($exploded_date_debut_date_fin[1] == 'mai.') $exploded_date_debut_date_fin[1] = "05";
    if ($exploded_date_debut_date_fin[1] == 'juin.') $exploded_date_debut_date_fin[1] = "06";
    if ($exploded_date_debut_date_fin[1] == 'juillet.') $exploded_date_debut_date_fin[1] = "07";
    if ($exploded_date_debut_date_fin[1] == 'Aout.' || $exploded_date_debut_date_fin[1] == 'Aug') $exploded_date_debut_date_fin[1] = "08";
    if ($exploded_date_debut_date_fin[1] == 'Sep.') $exploded_date_debut_date_fin[1] = "09";
    if ($exploded_date_debut_date_fin[1] == 'oct.') $exploded_date_debut_date_fin[1] = "10";
    if ($exploded_date_debut_date_fin[1] == 'nov.') $exploded_date_debut_date_fin[1] = "11";
    if ($exploded_date_debut_date_fin[1] == 'déc.' || $exploded_date_debut_date_fin[1] == 'Dec.') $exploded_date_debut_date_fin[1] = "12";
    if ($exploded_date_debut_date_fin[0]<10)$exploded_date_debut_date_fin[1]='0'.$exploded_date_debut_date_fin[0];

    $date_debut_br=$exploded_date_debut_date_fin[2].'-'.$exploded_date_debut[1].'-'.$exploded_date_debut[0];
    $date_fin=$exploded_date_debut_date_fin[2].'-'.$exploded_date_debut_date_fin[1].'-'.$exploded_date_debut_date_fin[0];
    if (strtotime($date_debut_br) > strtotime($date_fin) ){
        $year_date_debut=$exploded_date_debut_date_fin[2]-1;
    }else{
        $year_date_debut=$exploded_date_debut_date_fin[2];
    }
    $date_debut=$year_date_debut.'-'.$exploded_date_debut[1].'-'.$exploded_date_debut[0];
    return $date_debut;
}

function convert_nik_date_with_date_fin_to_sql_date_fin($exploded_date_debut,$exploded_date_debut_date_fin){

    if ($exploded_date_debut[1] == 'janv.') $exploded_date_debut[1] = "01";
    if ($exploded_date_debut[1] == 'févr.' || $exploded_date_debut[1] == 'Feb.') $exploded_date_debut[1] = "02";
    if ($exploded_date_debut[1] == 'mars.') $exploded_date_debut[1] = "03";
    if ($exploded_date_debut[1] == 'avril.') $exploded_date_debut[1] = "04";
    if ($exploded_date_debut[1] == 'mai.') $exploded_date_debut[1] = "05";
    if ($exploded_date_debut[1] == 'juin.') $exploded_date_debut[1] = "06";
    if ($exploded_date_debut[1] == 'juillet.') $exploded_date_debut[1] = "07";
    if ($exploded_date_debut[1] == 'Aout.' || $exploded_date_debut[1] == 'Aug') $exploded_date_debut[1] = "08";
    if ($exploded_date_debut[1] == 'Sep.') $exploded_date_debut[1] = "09";
    if ($exploded_date_debut[1] == 'oct.') $exploded_date_debut[1] = "10";
    if ($exploded_date_debut[1] == 'nov.') $exploded_date_debut[1] = "11";
    if ($exploded_date_debut[1] == 'déc.' || $exploded_date_debut[1] == 'Dec.') $exploded_date_debut[1] = "12";
    if ($exploded_date_debut[0]<10)$exploded_date_debut[1]='0'.$exploded_date_debut[0];

    if ($exploded_date_debut_date_fin[1] == 'janv.') $exploded_date_debut_date_fin[1] = "01";
    if ($exploded_date_debut_date_fin[1] == 'févr.' || $exploded_date_debut_date_fin[1] == 'Feb.') $exploded_date_debut_date_fin[1] = "02";
    if ($exploded_date_debut_date_fin[1] == 'mars.') $exploded_date_debut_date_fin[1] = "03";
    if ($exploded_date_debut_date_fin[1] == 'avril.') $exploded_date_debut_date_fin[1] = "04";
    if ($exploded_date_debut_date_fin[1] == 'mai.') $exploded_date_debut_date_fin[1] = "05";
    if ($exploded_date_debut_date_fin[1] == 'juin.') $exploded_date_debut_date_fin[1] = "06";
    if ($exploded_date_debut_date_fin[1] == 'juillet.') $exploded_date_debut_date_fin[1] = "07";
    if ($exploded_date_debut_date_fin[1] == 'Aout.' || $exploded_date_debut_date_fin[1] == 'Aug') $exploded_date_debut_date_fin[1] = "08";
    if ($exploded_date_debut_date_fin[1] == 'Sep.') $exploded_date_debut_date_fin[1] = "09";
    if ($exploded_date_debut_date_fin[1] == 'oct.') $exploded_date_debut_date_fin[1] = "10";
    if ($exploded_date_debut_date_fin[1] == 'nov.') $exploded_date_debut_date_fin[1] = "11";
    if ($exploded_date_debut_date_fin[1] == 'déc.' || $exploded_date_debut_date_fin[1] == 'Dec.') $exploded_date_debut_date_fin[1] = "12";
    if ($exploded_date_debut_date_fin[0]<10)$exploded_date_debut_date_fin[1]='0'.$exploded_date_debut_date_fin[0];

    $date_debut_br=$exploded_date_debut_date_fin[2].'-'.$exploded_date_debut[1].'-'.$exploded_date_debut[0];
    $date_fin=$exploded_date_debut_date_fin[2].'-'.$exploded_date_debut_date_fin[1].'-'.$exploded_date_debut_date_fin[0];
    if (strtotime($date_debut_br) > strtotime($date_fin) ){
        $year_date_debut=$exploded_date_debut_date_fin[2]-1;
    }else{
        $year_date_debut=$exploded_date_debut_date_fin[2];
    }
    $date_debut=$year_date_debut.'-'.$exploded_date_debut[1].'-'.$exploded_date_debut[0];
    return $date_debut;
}

function convert_date_debut_m_to_sql_date($date_bruted,$jour){

    if ($date_bruted == 'Janvier' || $date_bruted == 'jan') $date_bruted = "01";
    if ($date_bruted == 'février' || $date_bruted == 'Fevier' || $date_bruted == 'fev' || $date_bruted == "fév") $date_bruted = "02";
    if ($date_bruted == 'Mars' || $date_bruted == 'mars') $date_bruted = "03";
    if ($date_bruted == 'Avril' || $date_bruted == 'avr') $date_bruted = "04";
    if ($date_bruted == 'Mai' || $date_bruted == 'mai') $date_bruted = "05";
    if ($date_bruted == 'Juin' || $date_bruted == 'juin') $date_bruted = "06";
    if ($date_bruted == 'Juillet' || $date_bruted == 'juil') $date_bruted = "07";
    if ($date_bruted == 'Août' || $date_bruted == 'aout' || $date_bruted == 'Aug') $date_bruted = "08";
    if ($date_bruted == 'Septembre' || $date_bruted == 'sept') $date_bruted = "09";
    if ($date_bruted == 'Octobre' || $date_bruted == 'oct') $date_bruted = "10";
    if ($date_bruted == 'Novembre' || $date_bruted == 'nov') $date_bruted = "11";
    if ($date_bruted == 'Décembre' || $date_bruted == 'Decembre' || $date_bruted == 'dec') $date_bruted = "12";

    $date_debut = date("Y").'-'.$date_bruted.'-'.$jour;
    return $date_debut;
}


function convert_date_debut_grassev_counted_3_to_sql_date_debut($exploded){

    if (count($exploded)==3){
        if ($exploded[2] == 'JANVIER' || $exploded[2] == 'Jan') $exploded[2] = "01";
        if ($exploded[2] == 'FEVRIER' || $exploded[2] == 'Fevier' || $exploded[2] == 'Feb') $exploded[1] = "02";
        if ($exploded[2] == 'MARS' || $exploded[2] == 'Mar') $exploded[2] = "03";
        if ($exploded[2] == 'AVRIL' || $exploded[2] == 'Apr') $exploded[2] = "04";
        if ($exploded[2] == 'MAI' || $exploded[2] == 'May') $exploded[2] = "05";
        if ($exploded[2] == 'JUIN' || $exploded[2] == 'Jun') $exploded[2] = "06";
        if ($exploded[2] == 'JUILLET' || $exploded[2] == 'Jul') $exploded[2] = "07";
        if ($exploded[2] == 'AOUT' || $exploded[2] == 'Aout' || $exploded[2] == 'Aug') $exploded[1] = "08";
        if ($exploded[2] == 'SEPTEMBRE' || $exploded[2] == 'Sep') $exploded[2] = "09";
        if ($exploded[2] == 'OCTOBRE' || $exploded[2] == 'Oct') $exploded[2] = "10";
        if ($exploded[2] == 'NOVEMBRE' || $exploded[2] == 'Nov') $exploded[2] = "11";
        if ($exploded[2] == 'DECEMBRE' || $exploded[2] == 'Decembre' || $exploded[2] == 'Dec') $exploded[1] = "12";

        if ($exploded[2] < date('m')-1 ){
            $year=date("Y")+1;
        }else{
            $year=date("Y");
        }
        if ($exploded[1] < 10){$zero='0';}else{$zero="";}
        $date_debut=$year.'-'.$exploded[2].'-'.$zero.preg_replace('/0/','',$exploded[1]);
}
    return $date_debut;
}

function convert_date_debut_grassev_counted_6_to_sql_date_debut($exploded){

    if ($exploded[2] == 'JANVIER' || $exploded[2] == 'Jan') $exploded[2] = "01";
    if ($exploded[2] == 'FEVRIER' || $exploded[2] == 'Fevier' || $exploded[2] == 'Feb') $exploded[1] = "02";
    if ($exploded[2] == 'MARS' || $exploded[2] == 'Mar') $exploded[2] = "03";
    if ($exploded[2] == 'AVRIL' || $exploded[2] == 'Apr') $exploded[2] = "04";
    if ($exploded[2] == 'MAI' || $exploded[2] == 'May') $exploded[2] = "05";
    if ($exploded[2] == 'JUIN' || $exploded[2] == 'Jun') $exploded[2] = "06";
    if ($exploded[2] == 'JUILLET' || $exploded[2] == 'Jul') $exploded[2] = "07";
    if ($exploded[2] == 'AOUT' || $exploded[2] == 'Aout' || $exploded[2] == 'Aug') $exploded[1] = "08";
    if ($exploded[2] == 'SEPTEMBRE' || $exploded[2] == 'Sep') $exploded[2] = "09";
    if ($exploded[2] == 'OCTOBRE' || $exploded[2] == 'Oct') $exploded[2] = "10";
    if ($exploded[2] == 'NOVEMBRE' || $exploded[2] == 'Nov') $exploded[2] = "11";
    if ($exploded[2] == 'DECEMBRE' || $exploded[2] == 'Decembre' || $exploded[2] == 'Dec') $exploded[1] = "12";

    if ($exploded[5] == 'JANVIER' || $exploded[5] == 'Jan') $exploded[5] = "01";
    if ($exploded[5] == 'FEVRIER' || $exploded[5] == 'Fevier' || $exploded[5] == 'Feb') $exploded[5] = "02";
    if ($exploded[5] == 'MARS' || $exploded[5] == 'Mar') $exploded[5] = "03";
    if ($exploded[5] == 'AVRIL' || $exploded[5] == 'Apr') $exploded[5] = "04";
    if ($exploded[5] == 'MAI' || $exploded[5] == 'May') $exploded[5] = "05";
    if ($exploded[5] == 'JUIN' || $exploded[5] == 'Jun') $exploded[5] = "06";
    if ($exploded[5] == 'JUILLET' || $exploded[5] == 'Jul') $exploded[5] = "07";
    if ($exploded[5] == 'AOUT' || $exploded[5] == 'Aout' || $exploded[5] == 'Aug') $exploded[5] = "08";
    if ($exploded[5] == 'SEPTEMBRE' || $exploded[5] == 'Sep') $exploded[5] = "09";
    if ($exploded[5] == 'OCTOBRE' || $exploded[5] == 'Oct') $exploded[5] = "10";
    if ($exploded[5] == 'NOVEMBRE' || $exploded[5] == 'Nov') $exploded[5] = "11";
    if ($exploded[5] == 'DECEMBRE' || $exploded[5] == 'Decembre' || $exploded[5] == 'Dec') $exploded[5] = "12";

    if ($exploded[2] < date('m')-1 ){
        $year1=date("Y")+1;
    }else{
        $year1=date("Y");
    }
    if ($exploded[5] < date('m')-1 ){
        $year2=date("Y")+1;
    }else{
        $year2=date("Y");
    }
    $date_debut=$year1.'-'.$exploded[2].'-'.preg_replace('/0/','',$exploded[1]);
    $date_fin=$year2.'-'.$exploded[5].'-'.preg_replace('/0/','',$exploded[4]);
    return $date_debut;
}



function convert_date_debut_grassev_counted_6_to_sql_date_fin($exploded){

    if ($exploded[2] == 'JANVIER' || $exploded[2] == 'Jan') $exploded[2] = "01";
    if ($exploded[2] == 'FEVRIER' || $exploded[2] == 'Fevier' || $exploded[2] == 'Feb') $exploded[1] = "02";
    if ($exploded[2] == 'MARS' || $exploded[2] == 'Mar') $exploded[2] = "03";
    if ($exploded[2] == 'AVRIL' || $exploded[2] == 'Apr') $exploded[2] = "04";
    if ($exploded[2] == 'MAI' || $exploded[2] == 'May') $exploded[2] = "05";
    if ($exploded[2] == 'JUIN' || $exploded[2] == 'Jun') $exploded[2] = "06";
    if ($exploded[2] == 'JUILLET' || $exploded[2] == 'Jul') $exploded[2] = "07";
    if ($exploded[2] == 'AOUT' || $exploded[2] == 'Aout' || $exploded[2] == 'Aug') $exploded[1] = "08";
    if ($exploded[2] == 'SEPTEMBRE' || $exploded[2] == 'Sep') $exploded[2] = "09";
    if ($exploded[2] == 'OCTOBRE' || $exploded[2] == 'Oct') $exploded[2] = "10";
    if ($exploded[2] == 'NOVEMBRE' || $exploded[2] == 'Nov') $exploded[2] = "11";
    if ($exploded[2] == 'DECEMBRE' || $exploded[2] == 'Decembre' || $exploded[2] == 'Dec') $exploded[1] = "12";

    if ($exploded[5] == 'JANVIER' || $exploded[5] == 'Jan') $exploded[5] = "01";
    if ($exploded[5] == 'FEVRIER' || $exploded[5] == 'Fevier' || $exploded[5] == 'Feb') $exploded[5] = "02";
    if ($exploded[5] == 'MARS' || $exploded[5] == 'Mar') $exploded[5] = "03";
    if ($exploded[5] == 'AVRIL' || $exploded[5] == 'Apr') $exploded[5] = "04";
    if ($exploded[5] == 'MAI' || $exploded[5] == 'May') $exploded[5] = "05";
    if ($exploded[5] == 'JUIN' || $exploded[5] == 'Jun') $exploded[5] = "06";
    if ($exploded[5] == 'JUILLET' || $exploded[5] == 'Jul') $exploded[5] = "07";
    if ($exploded[5] == 'AOUT' || $exploded[5] == 'Aout' || $exploded[5] == 'Aug') $exploded[5] = "08";
    if ($exploded[5] == 'SEPTEMBRE' || $exploded[5] == 'Sep') $exploded[5] = "09";
    if ($exploded[5] == 'OCTOBRE' || $exploded[5] == 'Oct') $exploded[5] = "10";
    if ($exploded[5] == 'NOVEMBRE' || $exploded[5] == 'Nov') $exploded[5] = "11";
    if ($exploded[5] == 'DECEMBRE' || $exploded[5] == 'Decembre' || $exploded[5] == 'Dec') $exploded[5] = "12";

    if ($exploded[2] < date('m')-1 ){
        $year1=date("Y")+1;
    }else{
        $year1=date("Y");
    }
    if ($exploded[5] < date('m')-1 ){
        $year2=date("Y")+1;
    }else{
        $year2=date("Y");
    }
    $date_debut=$year1.'-'.$exploded[2].'-'.preg_replace('/0/','',$exploded[1]);
    $date_fin=$year2.'-'.$exploded[5].'-'.preg_replace('/0/','',$exploded[4]);
    return $date_fin;
}

function convert_date_debut_grassev_counted_8_to_sql_date_debut($exploded){

    if ($exploded[3] == 'JANVIER' || $exploded[3] == 'Jan') $exploded[3] = "01";
    if ($exploded[3] == 'FEVRIER' || $exploded[3] == 'Fevier' || $exploded[3] == 'Feb') $exploded[3] = "02";
    if ($exploded[3] == 'MARS' || $exploded[3] == 'Mar') $exploded[3] = "03";
    if ($exploded[3] == 'AVRIL' || $exploded[3] == 'Apr') $exploded[3] = "04";
    if ($exploded[3] == 'MAI' || $exploded[3] == 'May') $exploded[3] = "05";
    if ($exploded[3] == 'JUIN' || $exploded[3] == 'Jun') $exploded[3] = "06";
    if ($exploded[3] == 'JUILLET' || $exploded[3] == 'Jul') $exploded[3] = "07";
    if ($exploded[3] == 'AOUT' || $exploded[3] == 'Aout' || $exploded[3] == 'Aug') $exploded[3] = "08";
    if ($exploded[3] == 'SEPTEMBRE' || $exploded[3] == 'Sep') $exploded[3] = "09";
    if ($exploded[3] == 'OCTOBRE' || $exploded[3] == 'Oct') $exploded[3] = "10";
    if ($exploded[3] == 'NOVEMBRE' || $exploded[3] == 'Nov') $exploded[3] = "11";
    if ($exploded[3] == 'DECEMBRE' || $exploded[3] == 'Decembre' || $exploded[3] == 'Dec') $exploded[3] = "12";

    if ($exploded[7] == 'JANVIER' || $exploded[7] == 'Jan') $exploded[7] = "01";
    if ($exploded[7] == 'FEVRIER' || $exploded[7] == 'Fevier' || $exploded[7] == 'Feb') $exploded[7] = "02";
    if ($exploded[7] == 'MARS' || $exploded[7] == 'Mar') $exploded[7] = "03";
    if ($exploded[7] == 'AVRIL' || $exploded[7] == 'Apr') $exploded[7] = "04";
    if ($exploded[7] == 'MAI' || $exploded[7] == 'May') $exploded[7] = "05";
    if ($exploded[7] == 'JUIN' || $exploded[7] == 'Jun') $exploded[7] = "06";
    if ($exploded[7] == 'JUILLET' || $exploded[7] == 'Jul') $exploded[7] = "07";
    if ($exploded[7] == 'AOUT' || $exploded[7] == 'Aout' || $exploded[7] == 'Aug') $exploded[7] = "08";
    if ($exploded[7] == 'SEPTEMBRE' || $exploded[7] == 'Sep') $exploded[7] = "09";
    if ($exploded[7] == 'OCTOBRE' || $exploded[7] == 'Oct') $exploded[7] = "10";
    if ($exploded[7] == 'NOVEMBRE' || $exploded[7] == 'Nov') $exploded[7] = "11";
    if ($exploded[7] == 'DECEMBRE' || $exploded[7] == 'Decembre' || $exploded[7] == 'Dec') $exploded[7] = "12";

    if ($exploded[3] < date('m')-1 ){
        $year1=date("Y")+1;
    }else{
        $year1=date("Y");
    }
    if ($exploded[7] < date('m')-1 ){
        $year2=date("Y")+1;
    }else{
        $year2=date("Y");
    }
    if ($exploded[2]<10){$zero1='0';}else{$zero1='';}
    if ($exploded[6]<10){$zero2='0';}else{$zero2='';}
    $date_debut=$year1.'-'.$exploded[3].'-'.$zero1.preg_replace('/0/','',$exploded[2]);
    $date_fin=$year2.'-'.$exploded[7].'-'.$zero2.preg_replace('/0/','',$exploded[6]);
    return $date_debut;
}

function convert_date_debut_grassev_counted_8_to_sql_date_fin($exploded){

    if ($exploded[3] == 'JANVIER' || $exploded[3] == 'Jan') $exploded[3] = "01";
    if ($exploded[3] == 'FEVRIER' || $exploded[3] == 'Fevier' || $exploded[3] == 'Feb') $exploded[3] = "02";
    if ($exploded[3] == 'MARS' || $exploded[3] == 'Mar') $exploded[3] = "03";
    if ($exploded[3] == 'AVRIL' || $exploded[3] == 'Apr') $exploded[3] = "04";
    if ($exploded[3] == 'MAI' || $exploded[3] == 'May') $exploded[3] = "05";
    if ($exploded[3] == 'JUIN' || $exploded[3] == 'Jun') $exploded[3] = "06";
    if ($exploded[3] == 'JUILLET' || $exploded[3] == 'Jul') $exploded[3] = "07";
    if ($exploded[3] == 'AOUT' || $exploded[3] == 'Aout' || $exploded[3] == 'Aug') $exploded[3] = "08";
    if ($exploded[3] == 'SEPTEMBRE' || $exploded[3] == 'Sep') $exploded[3] = "09";
    if ($exploded[3] == 'OCTOBRE' || $exploded[3] == 'Oct') $exploded[3] = "10";
    if ($exploded[3] == 'NOVEMBRE' || $exploded[3] == 'Nov') $exploded[3] = "11";
    if ($exploded[3] == 'DECEMBRE' || $exploded[3] == 'Decembre' || $exploded[3] == 'Dec') $exploded[3] = "12";

    if ($exploded[7] == 'JANVIER' || $exploded[7] == 'Jan') $exploded[7] = "01";
    if ($exploded[7] == 'FEVRIER' || $exploded[7] == 'Fevier' || $exploded[7] == 'Feb') $exploded[7] = "02";
    if ($exploded[7] == 'MARS' || $exploded[7] == 'Mar') $exploded[7] = "03";
    if ($exploded[7] == 'AVRIL' || $exploded[7] == 'Apr') $exploded[7] = "04";
    if ($exploded[7] == 'MAI' || $exploded[7] == 'May') $exploded[7] = "05";
    if ($exploded[7] == 'JUIN' || $exploded[7] == 'Jun') $exploded[7] = "06";
    if ($exploded[7] == 'JUILLET' || $exploded[7] == 'Jul') $exploded[7] = "07";
    if ($exploded[7] == 'AOUT' || $exploded[7] == 'Aout' || $exploded[7] == 'Aug') $exploded[7] = "08";
    if ($exploded[7] == 'SEPTEMBRE' || $exploded[7] == 'Sep') $exploded[7] = "09";
    if ($exploded[7] == 'OCTOBRE' || $exploded[7] == 'Oct') $exploded[7] = "10";
    if ($exploded[7] == 'NOVEMBRE' || $exploded[7] == 'Nov') $exploded[7] = "11";
    if ($exploded[7] == 'DECEMBRE' || $exploded[7] == 'Decembre' || $exploded[7] == 'Dec') $exploded[7] = "12";

    if ($exploded[3] < date('m')-1 ){
        $year1=date("Y")+1;
    }else{
        $year1=date("Y");
    }
    if ($exploded[7] < date('m')-1 ){
        $year2=date("Y")+1;
    }else{
        $year2=date("Y");
    }
    if ($exploded[2]<10){$zero1='0';}else{$zero1='';}
    if ($exploded[6]<10){$zero2='0';}else{$zero2='';}
    $date_debut=$year1.'-'.$exploded[3].'-'.$zero1.preg_replace('/0/','',$exploded[2]);
    $date_fin=$year2.'-'.$exploded[7].'-'.$zero2.preg_replace('/0/','',$exploded[6]);
    return $date_fin;
}

function convert_date_debut_grassev_counted_7_to_sql_date_debut($exploded){

    if ($exploded[6] == 'JANVIER' || $exploded[6] == 'Jan') $exploded[6] = "01";
    if ($exploded[6] == 'FEVRIER' || $exploded[6] == 'Fevier' || $exploded[6] == 'Feb') $exploded[6] = "02";
    if ($exploded[6] == 'MARS' || $exploded[6] == 'Mar') $exploded[6] = "03";
    if ($exploded[6] == 'AVRIL' || $exploded[6] == 'Apr') $exploded[6] = "04";
    if ($exploded[6] == 'MAI' || $exploded[6] == 'May') $exploded[6] = "05";
    if ($exploded[6] == 'JUIN' || $exploded[6] == 'Jun') $exploded[6] = "06";
    if ($exploded[6] == 'JUILLET' || $exploded[6] == 'Jul') $exploded[6] = "07";
    if ($exploded[6] == 'AOUT' || $exploded[6] == 'Aout' || $exploded[6] == 'Aug') $exploded[6] = "08";
    if ($exploded[6] == 'SEPTEMBRE' || $exploded[6] == 'Sep') $exploded[6] = "09";
    if ($exploded[6] == 'OCTOBRE' || $exploded[6] == 'Oct') $exploded[6] = "10";
    if ($exploded[6] == 'NOVEMBRE' || $exploded[6] == 'Nov') $exploded[6] = "11";
    if ($exploded[6] == 'DECEMBRE' || $exploded[6] == 'Decembre' || $exploded[6] == 'Dec') $exploded[6] = "12";


    if ($exploded[6] < date('m')-1 ){
        $year=date("Y")+1;
    }else{
        $year=date("Y");
    }
    if ($exploded[2]<10){$zero1='0';}else{$zero1='';}
    if ($exploded[6]<10){$zero2='0';}else{$zero2='';}


    $date_debut=$year.'-'.$exploded[6].'-'.$zero1.preg_replace('/0/','',$exploded[2]);
    $date_fin=$year.'-'.$exploded[6].'-'.$zero2.preg_replace('/0/','',$exploded[5]);
    return $date_debut;
}

function convert_date_debut_grassev_counted_7_to_sql_date_fin($exploded){

    if ($exploded[6] == 'JANVIER' || $exploded[6] == 'Jan') $exploded[6] = "01";
    if ($exploded[6] == 'FEVRIER' || $exploded[6] == 'Fevier' || $exploded[6] == 'Feb') $exploded[6] = "02";
    if ($exploded[6] == 'MARS' || $exploded[6] == 'Mar') $exploded[6] = "03";
    if ($exploded[6] == 'AVRIL' || $exploded[6] == 'Apr') $exploded[6] = "04";
    if ($exploded[6] == 'MAI' || $exploded[6] == 'May') $exploded[6] = "05";
    if ($exploded[6] == 'JUIN' || $exploded[6] == 'Jun') $exploded[6] = "06";
    if ($exploded[6] == 'JUILLET' || $exploded[6] == 'Jul') $exploded[6] = "07";
    if ($exploded[6] == 'AOUT' || $exploded[6] == 'Aout' || $exploded[6] == 'Aug') $exploded[6] = "08";
    if ($exploded[6] == 'SEPTEMBRE' || $exploded[6] == 'Sep') $exploded[6] = "09";
    if ($exploded[6] == 'OCTOBRE' || $exploded[6] == 'Oct') $exploded[6] = "10";
    if ($exploded[6] == 'NOVEMBRE' || $exploded[6] == 'Nov') $exploded[6] = "11";
    if ($exploded[6] == 'DECEMBRE' || $exploded[6] == 'Decembre' || $exploded[6] == 'Dec') $exploded[6] = "12";


    if ($exploded[6] < date('m')-1 ){
        $year=date("Y")+1;
    }else{
        $year=date("Y");
    }
    if ($exploded[2]<10){$zero1='0';}else{$zero1='';}
    if ($exploded[6]<10){$zero2='0';}else{$zero2='';}


    $date_debut=$year.'-'.$exploded[6].'-'.$zero1.preg_replace('/0/','',$exploded[2]);
    $date_fin=$year.'-'.$exploded[6].'-'.$zero2.preg_replace('/0/','',$exploded[5]);
    return $date_fin;
}

function convert_heure_debut_grassev_to_sql_heure($alldecode,$date_origins){

    $heure=$date_origins;
    $exploded=explode(' ',$heure);
    if (count($exploded)==1){
        $heure_debut=preg_replace('/h/','',$exploded[0]).':00';
        return $heure_debut;
    }elseif (count($exploded)==3){
        $heure_debut=preg_replace('/h/','',$exploded[0]).':00';
        return $heure_debut;
    }

}

function convert_date_debut_norm_to_sql_date($exploded){


    if ($exploded[1] == 'janvier' || $exploded[1] == 'Jan') $exploded[1] = "01";
    if ($exploded[1] == 'février' || $exploded[1] == 'Fevier' || $exploded[1] == 'Feb') $exploded[1] = "02";
    if ($exploded[1] == 'mars' || $exploded[1] == 'Mar') $exploded[1] = "03";
    if ($exploded[1] == 'avril' || $exploded[1] == 'Apr') $exploded[1] = "04";
    if ($exploded[1] == 'mai' || $exploded[1] == 'May') $exploded[1] = "05";
    if ($exploded[1] == 'juin' || $exploded[1] == 'Jun') $exploded[1] = "06";
    if ($exploded[1] == 'juillet' || $exploded[1] == 'Jul') $exploded[1] = "07";
    if ($exploded[1] == 'août' || $exploded[1] == 'Aout' || $exploded[1] == 'Aug') $exploded[1] = "08";
    if ($exploded[1] == 'septembre' || $exploded[1] == 'Sep') $exploded[1] = "09";
    if ($exploded[1] == 'octobre' || $exploded[1] == 'Oct') $exploded[1] = "10";
    if ($exploded[1] == 'novembre' || $exploded[1] == 'Nov') $exploded[1] = "11";
    if ($exploded[1] == 'décembre' || $exploded[1] == 'Decembre' || $exploded[1] == 'Dec') $exploded[1] = "12";
    $date_debut=$exploded[2].'-'.$exploded[1].'-'.$exploded[0];

    return $date_debut;

}

function convert_date_debut_counted_2_to_sql_date_debut($date_brute,$date_fin_orgins){
    if ($date_brute[1] == 'Janvier' || $date_brute[1] == 'jan') $date_brute[1] = "01";
    if ($date_brute[1] == 'Février' || $date_brute[1] == 'fév' || $date_brute[1] == 'Feb') $date_brute[1] = "02";
    if ($date_brute[1] == 'Mars' || $date_brute[1] == 'mar') $date_brute[1] = "03";
    if ($date_brute[1] == 'Avril' || $date_brute[1] == 'avr') $date_brute[1] = "04";
    if ($date_brute[1] == 'Mai' || $date_brute[1] == 'mai') $date_brute[1] = "05";
    if ($date_brute[1] == 'Juin' || $date_brute[1] == 'juin') $date_brute[1] = "06";
    if ($date_brute[1] == 'Juillet' || $date_brute[1] == 'juil') $date_brute[1] = "07";
    if ($date_brute[1] == 'Août' || $date_brute[1] == 'aout' || $date_brute[1] == 'Aug') $date_brute[1] = "08";
    if ($date_brute[1] == 'Septembre' || $date_brute[1] == 'sept') $date_brute[1] = "09";
    if ($date_brute[1] == 'Octobre' || $date_brute[1] == 'oct') $date_brute[1] = "10";
    if ($date_brute[1] == 'Novembre' || $date_brute[1] == 'nov') $date_brute[1] = "11";
    if ($date_brute[1] == 'Décembre' || $date_brute[1] == 'Decembre' || $date_brute[1] == 'dec'   || $date_brute[1] == 'déc') $date_brute[1] = "12";

    if ($date_brute[1] < date('m')-1){

        $year=date('Y')+1;

    }else{$year=date('Y');}
    if(strlen($date_brute[0]) == 1){
        $date_brute[0] = "0".$date_brute[0];
    }
    $date_debut = $year . "-" . $date_brute[1] . "-" . $date_brute[0];

    if (isset($date_fin_orgins) And $date_fin_orgins != null) {

        $date_brute = explode(" ", $date_fin_orgins);
        if ($date_brute[1] == 'Janvier' || $date_brute[1] == 'jan') $date_brute[1] = "01";
        if ($date_brute[1] == 'février' || $date_brute[1] == 'fév' || $date_brute[1] == 'Feb'  || $date_brute[1] == 'féf') $date_brute[1] = "02";
        if ($date_brute[1] == 'Mars' || $date_brute[1] == 'mar') $date_brute[1] = "03";
        if ($date_brute[1] == 'Avril' || $date_brute[1] == 'avr') $date_brute[1] = "04";
        if ($date_brute[1] == 'Mai' || $date_brute[1] == 'mai') $date_brute[1] = "05";
        if ($date_brute[1] == 'Juin' || $date_brute[1] == 'juin') $date_brute[1] = "06";
        if ($date_brute[1] == 'Juillet' || $date_brute[1] == 'juil') $date_brute[1] = "07";
        if ($date_brute[1] == 'Août' || $date_brute[1] == 'aout' || $date_brute[1] == 'Aug') $date_brute[1] = "08";
        if ($date_brute[1] == 'Septembre' || $date_brute[1] == 'sept') $date_brute[1] = "09";
        if ($date_brute[1] == 'Octobre' || $date_brute[1] == 'oct') $date_brute[1] = "10";
        if ($date_brute[1] == 'Novembre' || $date_brute[1] == 'nov') $date_brute[1] = "11";
        if ($date_brute[1] == 'Décembre' || $date_brute[1] == 'Decembre' || $date_brute[1] == 'dec'  || $date_brute[1] == 'déc') $date_brute[1] = "12";

        $date_fin = $year . "-" . $date_brute[1] . "-" . $date_brute[0];
    }
    return $date_debut;
}


function convert_date_debut_counted_2_to_sql_date_fin($date_brute,$date_fin_orgins){
    if ($date_brute[1] == 'Janvier' || $date_brute[1] == 'jan') $date_brute[1] = "01";
    if ($date_brute[1] == 'février' || $date_brute[1] == 'fév' || $date_brute[1] == 'Feb') $date_brute[1] = "02";
    if ($date_brute[1] == 'Mars' || $date_brute[1] == 'mar') $date_brute[1] = "03";
    if ($date_brute[1] == 'Avril' || $date_brute[1] == 'avr') $date_brute[1] = "04";
    if ($date_brute[1] == 'Mai' || $date_brute[1] == 'mai') $date_brute[1] = "05";
    if ($date_brute[1] == 'Juin' || $date_brute[1] == 'juin') $date_brute[1] = "06";
    if ($date_brute[1] == 'Juillet' || $date_brute[1] == 'juil') $date_brute[1] = "07";
    if ($date_brute[1] == 'Août' || $date_brute[1] == 'aout' || $date_brute[1] == 'Aug') $date_brute[1] = "08";
    if ($date_brute[1] == 'Septembre' || $date_brute[1] == 'sept') $date_brute[1] = "09";
    if ($date_brute[1] == 'Octobre' || $date_brute[1] == 'oct') $date_brute[1] = "10";
    if ($date_brute[1] == 'Novembre' || $date_brute[1] == 'nov') $date_brute[1] = "11";
    if ($date_brute[1] == 'Décembre' || $date_brute[1] == 'Decembre' || $date_brute[1] == 'dec'   || $date_brute[1] == 'déc') $date_brute[1] = "12";

    if ($date_brute[1] < date('m')-1){

        $year=date('Y')+1;

    }else{
        $year=date('Y');
    }

    if (isset($date_fin_orgins) And $date_fin_orgins != null) {

        $date_brute = explode(" ", $date_fin_orgins);
        if ($date_brute[1] == 'Janvier' || $date_brute[1] == 'jan') $date_brute[1] = "01";
        if ($date_brute[1] == 'février' || $date_brute[1] == 'fév' || $date_brute[1] == 'Feb'  || $date_brute[1] == 'féf') $date_brute[1] = "02";
        if ($date_brute[1] == 'Mars' || $date_brute[1] == 'mar') $date_brute[1] = "03";
        if ($date_brute[1] == 'Avril' || $date_brute[1] == 'avr') $date_brute[1] = "04";
        if ($date_brute[1] == 'Mai' || $date_brute[1] == 'mai') $date_brute[1] = "05";
        if ($date_brute[1] == 'Juin' || $date_brute[1] == 'juin') $date_brute[1] = "06";
        if ($date_brute[1] == 'Juillet' || $date_brute[1] == 'juil') $date_brute[1] = "07";
        if ($date_brute[1] == 'Août' || $date_brute[1] == 'aout' || $date_brute[1] == 'Aug') $date_brute[1] = "08";
        if ($date_brute[1] == 'Septembre' || $date_brute[1] == 'sept') $date_brute[1] = "09";
        if ($date_brute[1] == 'Octobre' || $date_brute[1] == 'oct') $date_brute[1] = "10";
        if ($date_brute[1] == 'Novembre' || $date_brute[1] == 'nov') $date_brute[1] = "11";
        if ($date_brute[1] == 'Décembre' || $date_brute[1] == 'Decembre' || $date_brute[1] == 'dec'  || $date_brute[1] == 'déc') $date_brute[1] = "12";

        $date_fin = $year . "-" . $date_brute[1] . "-" . $date_brute[0];
    }
    return $date_fin;
}

function convert_date_debut_counted_bigger_than_3_to_sql_date_debut($date_brute){

    if ($date_brute[2] == 'janvier' || $date_brute[2] == 'Jan') $date_brute[2] = "01";
    if ($date_brute[2] == 'février' || $date_brute[2] == 'Fevier' || $date_brute[2] == 'Feb' || $date_brute[2] == 'féf') $date_brute[2] = "02";
    if ($date_brute[2] == 'mars' || $date_brute[2] == 'Mar') $date_brute[2] = "03";
    if ($date_brute[2] == 'avril' || $date_brute[2] == 'Apr') $date_brute[2] = "04";
    if ($date_brute[2] == 'mai' || $date_brute[2] == 'May') $date_brute[2] = "05";
    if ($date_brute[2] == 'juin' || $date_brute[2] == 'Jun') $date_brute[2] = "06";
    if ($date_brute[2] == 'juillet' || $date_brute[2] == 'Jul') $date_brute[2] = "07";
    if ($date_brute[2] == 'août' || $date_brute[2] == 'Aout' || $date_brute[2] == 'Aug') $date_brute[2] = "08";
    if ($date_brute[2] == 'septembre' || $date_brute[2] == 'Sep') $date_brute[2] = "09";
    if ($date_brute[2] == 'octobre' || $date_brute[2] == 'Oct') $date_brute[2] = "10";
    if ($date_brute[2] == 'novembre' || $date_brute[2] == 'Nov') $date_brute[2] = "11";
    if ($date_brute[2] == 'décembre' || $date_brute[2] == 'Decembre' || $date_brute[2]=="déc" || $date_brute[2] == 'Dec') $date_brute[2] = "12";
    $date_debut = $date_brute[3] . "-" . $date_brute[2] . "-" . $date_brute[1];
    return $date_debut;
}

function convert_pregmatched_motifss_and_cainess_to_sql_date_debut($exploded){

    if ($exploded) {
        $jour1 = $exploded[0];
        $mois2 = $exploded[2];
        $mois = $exploded[1];
        $mois1 = $mois[0] . $mois[1];
        $jour2 = $mois[2] . $mois[3];
        $date_debut = date("Y") . '-' . $mois1 . '-' . $jour1;
        $date_fin = date("Y") . '-' . $mois2 . '-' . $jour2;
    }

    return $date_debut;
}

function convert_pregmatched_motifss_and_cainess_to_sql_date_fin($exploded){

    if ($exploded) {
        $jour1 = $exploded[0];
        $mois2 = $exploded[2];
        $mois = $exploded[1];
        $mois1 = $mois[0] . $mois[1];
        $jour2 = $mois[2] . $mois[3];
        $date_debut = date("Y") . '-' . $mois1 . '-' . $jour1;
        $date_fin = date("Y") . '-' . $mois2 . '-' . $jour2;
    }

    return $date_fin;
}

function convert_pregmatched_motif_long_and_chaine_long_to_sql_date_debut($exploded,$date_fin_origins){

    if ($exploded) {
        if ($exploded[2] == 'janvier' || $exploded[2] == 'Jan') $exploded[2] = "01";
        if ($exploded[2] == 'février' || $exploded[2] == 'Fevier' || $exploded[2] == 'Feb') $exploded[2] = "02";
        if ($exploded[2] == 'mars' || $exploded[2] == 'Mar') $exploded[2] = "03";
        if ($exploded[2] == 'avril' || $exploded[2] == 'Apr') $exploded[2] = "04";
        if ($exploded[2] == 'mai' || $exploded[2] == 'May') $exploded[2] = "05";
        if ($exploded[2] == 'juin' || $exploded[2] == 'Jun') $exploded[2] = "06";
        if ($exploded[2] == 'juillet' || $exploded[2] == 'Jul') $exploded[2] = "07";
        if ($exploded[2] == 'août' || $exploded[2] == 'Aout' || $exploded[2] == 'Aug') $exploded[2] = "08";
        if ($exploded[2] == 'septembre' || $exploded[2] == 'Sep') $exploded[2] = "09";
        if ($exploded[2] == 'octobre' || $exploded[2] == 'Oct') $exploded[2] = "10";
        if ($exploded[2] == 'novembre' || $exploded[2] == 'Nov') $exploded[2] = "11";
        if ($exploded[2] == 'décembre' || $exploded[2] == 'Decembre' || $exploded[2] == 'Dec') $exploded[2] = "12";

        if ($exploded[2] < date('m')){

            $year=date('Y')+1;

        }else{$year=date('Y');}

        $date_debut =$year . "-" . $exploded[2] . "-" . $exploded[1];
        $heure=$exploded[3];
        $heure_exploded=explode('h',$heure);
        $heure_debut=$heure_exploded[0].':'.$heure_exploded[1];
        if (isset($date_fin_origins)) {
            $date_fin_long = $date_fin_origins;

            $date_fin_brute = $exploded = explode(" ", $date_fin_long);
            if ($date_fin_brute[2] == 'janvier' || $date_fin_brute[2] == 'Jan') $date_fin_brute[2] = "01";
            if ($date_fin_brute[2] == 'février' || $date_fin_brute[2] == 'Fevier' || $date_fin_brute[2] == 'Feb') $date_fin_brute[2] = "02";
            if ($date_fin_brute[2] == 'mars' || $date_fin_brute[2] == 'Mar') $date_fin_brute[2] = "03";
            if ($date_fin_brute[2] == 'avril' || $date_fin_brute[2] == 'Apr') $date_fin_brute[2] = "04";
            if ($date_fin_brute[2] == 'mai' || $date_fin_brute[2] == 'May') $date_fin_brute[2] = "05";
            if ($date_fin_brute[2] == 'juin' || $date_fin_brute[2] == 'Jun') $date_fin_brute[2] = "06";
            if ($date_fin_brute[2] == 'juillet' || $date_fin_brute[2] == 'Jul') $date_fin_brute[2] = "07";
            if ($date_fin_brute[2] == 'août' || $date_fin_brute[2] == 'Aout' || $date_fin_brute[2] == 'Aug') $date_fin_brute[2] = "08";
            if ($date_fin_brute[2] == 'septembre' || $date_fin_brute[2] == 'Sep') $date_fin_brute[2] = "09";
            if ($date_fin_brute[2] == 'octobre' || $date_fin_brute[2] == 'Oct') $date_fin_brute[2] = "10";
            if ($date_fin_brute[2] == 'novembre' || $date_fin_brute[2] == 'Nov') $date_fin_brute[2] = "11";
            if ($date_fin_brute[2] == 'décembre' || $date_fin_brute[2] == 'Decembre' || $date_fin_brute[2] == 'Dec') $date_fin_brute[2] = "12";

            if ($exploded[2] < date('m')){

                $year=date('Y')+1;

            }else{$year=date('Y');}

            $date_fin = $year . "-" . $date_fin_brute[2] . "-" . $date_fin_brute[1];
            $heure=$date_fin_brute[3];
            $heure_exploded=explode('h',$heure);
            $heure_debut=$heure_exploded[0].':'.$heure_exploded[1];
        }
    }

    return $date_debut;
}

function convert_pregmatched_motif_long_and_chaine_long_to_sql_date_fin($exploded,$date_fin_origins){

    if ($exploded) {
        if ($exploded[2] == 'janvier' || $exploded[2] == 'Jan') $exploded[2] = "01";
        if ($exploded[2] == 'février' || $exploded[2] == 'Fevier' || $exploded[2] == 'Feb') $exploded[2] = "02";
        if ($exploded[2] == 'mars' || $exploded[2] == 'Mar') $exploded[2] = "03";
        if ($exploded[2] == 'avril' || $exploded[2] == 'Apr') $exploded[2] = "04";
        if ($exploded[2] == 'mai' || $exploded[2] == 'May') $exploded[2] = "05";
        if ($exploded[2] == 'juin' || $exploded[2] == 'Jun') $exploded[2] = "06";
        if ($exploded[2] == 'juillet' || $exploded[2] == 'Jul') $exploded[2] = "07";
        if ($exploded[2] == 'août' || $exploded[2] == 'Aout' || $exploded[2] == 'Aug') $exploded[2] = "08";
        if ($exploded[2] == 'septembre' || $exploded[2] == 'Sep') $exploded[2] = "09";
        if ($exploded[2] == 'octobre' || $exploded[2] == 'Oct') $exploded[2] = "10";
        if ($exploded[2] == 'novembre' || $exploded[2] == 'Nov') $exploded[2] = "11";
        if ($exploded[2] == 'décembre' || $exploded[2] == 'Decembre' || $exploded[2] == 'Dec') $exploded[2] = "12";

        if ($exploded[2] < date('m')){

            $year=date('Y')+1;

        }else{$year=date('Y');}

        $date_debut =$year . "-" . $exploded[2] . "-" . $exploded[1];
        $heure=$exploded[3];
        $heure_exploded=explode('h',$heure);
        $heure_debut=$heure_exploded[0].':'.$heure_exploded[1];
        if (isset($date_fin_origins)) {
            $date_fin_long = $date_fin_origins;

            $date_fin_brute = $exploded = explode(" ", $date_fin_long);
            if ($date_fin_brute[2] == 'janvier' || $date_fin_brute[2] == 'Jan') $date_fin_brute[2] = "01";
            if ($date_fin_brute[2] == 'février' || $date_fin_brute[2] == 'Fevier' || $date_fin_brute[2] == 'Feb') $date_fin_brute[2] = "02";
            if ($date_fin_brute[2] == 'mars' || $date_fin_brute[2] == 'Mar') $date_fin_brute[2] = "03";
            if ($date_fin_brute[2] == 'avril' || $date_fin_brute[2] == 'Apr') $date_fin_brute[2] = "04";
            if ($date_fin_brute[2] == 'mai' || $date_fin_brute[2] == 'May') $date_fin_brute[2] = "05";
            if ($date_fin_brute[2] == 'juin' || $date_fin_brute[2] == 'Jun') $date_fin_brute[2] = "06";
            if ($date_fin_brute[2] == 'juillet' || $date_fin_brute[2] == 'Jul') $date_fin_brute[2] = "07";
            if ($date_fin_brute[2] == 'août' || $date_fin_brute[2] == 'Aout' || $date_fin_brute[2] == 'Aug') $date_fin_brute[2] = "08";
            if ($date_fin_brute[2] == 'septembre' || $date_fin_brute[2] == 'Sep') $date_fin_brute[2] = "09";
            if ($date_fin_brute[2] == 'octobre' || $date_fin_brute[2] == 'Oct') $date_fin_brute[2] = "10";
            if ($date_fin_brute[2] == 'novembre' || $date_fin_brute[2] == 'Nov') $date_fin_brute[2] = "11";
            if ($date_fin_brute[2] == 'décembre' || $date_fin_brute[2] == 'Decembre' || $date_fin_brute[2] == 'Dec') $date_fin_brute[2] = "12";

            if ($exploded[2] < date('m')){

                $year=date('Y')+1;

            }else{$year=date('Y');}

            $date_fin = $year . "-" . $date_fin_brute[2] . "-" . $date_fin_brute[1];
            $heure=$date_fin_brute[3];
            $heure_exploded=explode('h',$heure);
            $heure_debut=$heure_exploded[0].':'.$heure_exploded[1];
        }
    }

    return $date_debut;
}

function convert_pregmatched_motif_long_and_chaine_long_to_sql_heure_debut($exploded,$date_fin_origins){

    if ($exploded) {
        if ($exploded[2] == 'janvier' || $exploded[2] == 'Jan') $exploded[2] = "01";
        if ($exploded[2] == 'février' || $exploded[2] == 'Fevier' || $exploded[2] == 'Feb') $exploded[2] = "02";
        if ($exploded[2] == 'mars' || $exploded[2] == 'Mar') $exploded[2] = "03";
        if ($exploded[2] == 'avril' || $exploded[2] == 'Apr') $exploded[2] = "04";
        if ($exploded[2] == 'mai' || $exploded[2] == 'May') $exploded[2] = "05";
        if ($exploded[2] == 'juin' || $exploded[2] == 'Jun') $exploded[2] = "06";
        if ($exploded[2] == 'juillet' || $exploded[2] == 'Jul') $exploded[2] = "07";
        if ($exploded[2] == 'août' || $exploded[2] == 'Aout' || $exploded[2] == 'Aug') $exploded[2] = "08";
        if ($exploded[2] == 'septembre' || $exploded[2] == 'Sep') $exploded[2] = "09";
        if ($exploded[2] == 'octobre' || $exploded[2] == 'Oct') $exploded[2] = "10";
        if ($exploded[2] == 'novembre' || $exploded[2] == 'Nov') $exploded[2] = "11";
        if ($exploded[2] == 'décembre' || $exploded[2] == 'Decembre' || $exploded[2] == 'Dec') $exploded[2] = "12";

        if ($exploded[2] < date('m')){

            $year=date('Y')+1;

        }else{$year=date('Y');}

        $date_debut =$year . "-" . $exploded[2] . "-" . $exploded[1];
        $heure=$exploded[3];
        $heure_exploded=explode('h',$heure);
        $heure_debut=$heure_exploded[0].':'.$heure_exploded[1];
        if (isset($date_fin_origins)) {
            $date_fin_long = $date_fin_origins;

            $date_fin_brute = $exploded = explode(" ", $date_fin_long);
            if ($date_fin_brute[2] == 'janvier' || $date_fin_brute[2] == 'Jan') $date_fin_brute[2] = "01";
            if ($date_fin_brute[2] == 'février' || $date_fin_brute[2] == 'Fevier' || $date_fin_brute[2] == 'Feb') $date_fin_brute[2] = "02";
            if ($date_fin_brute[2] == 'mars' || $date_fin_brute[2] == 'Mar') $date_fin_brute[2] = "03";
            if ($date_fin_brute[2] == 'avril' || $date_fin_brute[2] == 'Apr') $date_fin_brute[2] = "04";
            if ($date_fin_brute[2] == 'mai' || $date_fin_brute[2] == 'May') $date_fin_brute[2] = "05";
            if ($date_fin_brute[2] == 'juin' || $date_fin_brute[2] == 'Jun') $date_fin_brute[2] = "06";
            if ($date_fin_brute[2] == 'juillet' || $date_fin_brute[2] == 'Jul') $date_fin_brute[2] = "07";
            if ($date_fin_brute[2] == 'août' || $date_fin_brute[2] == 'Aout' || $date_fin_brute[2] == 'Aug') $date_fin_brute[2] = "08";
            if ($date_fin_brute[2] == 'septembre' || $date_fin_brute[2] == 'Sep') $date_fin_brute[2] = "09";
            if ($date_fin_brute[2] == 'octobre' || $date_fin_brute[2] == 'Oct') $date_fin_brute[2] = "10";
            if ($date_fin_brute[2] == 'novembre' || $date_fin_brute[2] == 'Nov') $date_fin_brute[2] = "11";
            if ($date_fin_brute[2] == 'décembre' || $date_fin_brute[2] == 'Decembre' || $date_fin_brute[2] == 'Dec') $date_fin_brute[2] = "12";

            if ($exploded[2] < date('m')){

                $year=date('Y')+1;

            }else{$year=date('Y');}

            $date_fin = $year . "-" . $date_fin_brute[2] . "-" . $date_fin_brute[1];
            $heure=$date_fin_brute[3];
            $heure_exploded=explode('h',$heure);
            $heure_debut=$heure_exploded[0].':'.$heure_exploded[1];
        }
    }

    return $heure_debut;
}

function convert_date_debut_menton_counted_12_to_sql_date_debut($exploded){

    if ($exploded[0] == 'janvier' || $exploded[0] == 'jan') $exploded[0] = "01";
    if ($exploded[0] == 'février' || $exploded[0] == 'Fevier' || $exploded[0] == 'fev') $exploded[0] = "02";
    if ($exploded[0] == 'mars' || $exploded[0] == 'mar') $exploded[0] = "03";
    if ($exploded[0] == 'avril' || $exploded[0] == 'avr') $exploded[0] = "04";
    if ($exploded[0] == 'mai' || $exploded[0] == 'mai') $exploded[0] = "05";
    if ($exploded[0] == 'juin' || $exploded[0] == 'juin') $exploded[0] = "06";
    if ($exploded[0] == 'juillet' || $exploded[0] == 'juil') $exploded[0] = "07";
    if ($exploded[0] == 'août' || $exploded[0] == 'aout' || $exploded[0] == 'Aug') $exploded[0] = "08";
    if ($exploded[0] == 'septembre' || $exploded[0] == 'sep') $exploded[0] = "09";
    if ($exploded[0] == 'octobre' || $exploded[0] == 'oct') $exploded[0] = "10";
    if ($exploded[0] == 'novembre' || $exploded[0] == 'nov') $exploded[0] = "11";
    if ($exploded[0] == 'décembre' || $exploded[0] == 'Decembre' || $exploded[0] == 'Ddec') $exploded[0] = "12";

    if ($exploded[0] < date('m')){

        $year=date('Y')+1;

    }else{$year=date('Y');}
    if ($exploded[1]<10){
        $zero="0";
    }else{$zero="";}
    $date_debut=$year.'-'.$exploded[0].'-'.$zero.$exploded[1];
    $heure_debut=$exploded[3].':00';
    return $date_debut;
}

function convert_date_debut_menton_counted_12_to_sql_heure_debut($exploded){

    if ($exploded[0] == 'janvier' || $exploded[0] == 'jan') $exploded[0] = "01";
    if ($exploded[0] == 'février' || $exploded[0] == 'Fevier' || $exploded[0] == 'fev') $exploded[0] = "02";
    if ($exploded[0] == 'mars' || $exploded[0] == 'mar') $exploded[0] = "03";
    if ($exploded[0] == 'avril' || $exploded[0] == 'avr') $exploded[0] = "04";
    if ($exploded[0] == 'mai' || $exploded[0] == 'mai') $exploded[0] = "05";
    if ($exploded[0] == 'juin' || $exploded[0] == 'juin') $exploded[0] = "06";
    if ($exploded[0] == 'juillet' || $exploded[0] == 'juil') $exploded[0] = "07";
    if ($exploded[0] == 'août' || $exploded[0] == 'aout' || $exploded[0] == 'Aug') $exploded[0] = "08";
    if ($exploded[0] == 'septembre' || $exploded[0] == 'sep') $exploded[0] = "09";
    if ($exploded[0] == 'octobre' || $exploded[0] == 'oct') $exploded[0] = "10";
    if ($exploded[0] == 'novembre' || $exploded[0] == 'nov') $exploded[0] = "11";
    if ($exploded[0] == 'décembre' || $exploded[0] == 'Decembre' || $exploded[0] == 'Ddec') $exploded[0] = "12";

    if ($exploded[0] < date('m')){

        $year=date('Y')+1;

    }else{$year=date('Y');}
    if ($exploded[1]<10){
        $zero="0";
    }else{$zero="";}
    $date_debut=$year.'-'.$exploded[0].'-'.$zero.$exploded[1];
    $heure_debut=$exploded[3].':00';
    return $heure_debut;
}

function convert_date_debut_menton_counted_15_to_sql_date_debut($exploded){

    if ($exploded[0] == 'janvier' || $exploded[0] == 'jan') $exploded[0] = "01";
    if ($exploded[0] == 'février' || $exploded[0] == 'Fevier' || $exploded[0] == 'fev') $exploded[0] = "02";
    if ($exploded[0] == 'mars' || $exploded[0] == 'mar') $exploded[0] = "03";
    if ($exploded[0] == 'avril' || $exploded[0] == 'avr') $exploded[0] = "04";
    if ($exploded[0] == 'mai' || $exploded[0] == 'mai') $exploded[0] = "05";
    if ($exploded[0] == 'juin' || $exploded[0] == 'juin') $exploded[0] = "06";
    if ($exploded[0] == 'juillet' || $exploded[0] == 'juil') $exploded[0] = "07";
    if ($exploded[0] == 'août' || $exploded[0] == 'aout' || $exploded[0] == 'Aug') $exploded[0] = "08";
    if ($exploded[0] == 'septembre' || $exploded[0] == 'sep') $exploded[0] = "09";
    if ($exploded[0] == 'octobre' || $exploded[0] == 'oct') $exploded[0] = "10";
    if ($exploded[0] == 'novembre' || $exploded[0] == 'nov') $exploded[0] = "11";
    if ($exploded[0] == 'décembre' || $exploded[0] == 'Decembre' || $exploded[0] == 'Ddec') $exploded[0] = "12";


    if ($exploded[8] == 'janvier' || $exploded[8] == 'jan') $exploded[8] = "01";
    if ($exploded[8] == 'février' || $exploded[8] == 'Fevier' || $exploded[8] == 'fev') $exploded[8] = "02";
    if ($exploded[8] == 'mars' || $exploded[8] == 'mar') $exploded[8] = "03";
    if ($exploded[8] == 'avril' || $exploded[8] == 'avr') $exploded[8] = "04";
    if ($exploded[8] == 'mai' || $exploded[8] == 'mai') $exploded[8] = "05";
    if ($exploded[8] == 'juin' || $exploded[8] == 'juin') $exploded[8] = "06";
    if ($exploded[8] == 'juillet' || $exploded[8] == 'juil') $exploded[8] = "07";
    if ($exploded[8] == 'août' || $exploded[8] == 'aout' || $exploded[8] == 'Aug') $exploded[8] = "08";
    if ($exploded[8] == 'septembre' || $exploded[8] == 'sep') $exploded[8] = "09";
    if ($exploded[8] == 'octobre' || $exploded[8] == 'oct') $exploded[8] = "10";
    if ($exploded[8] == 'novembre' || $exploded[8] == 'nov') $exploded[8] = "11";
    if ($exploded[8] == 'décembre' || $exploded[8] == 'Decembre' || $exploded[8] == 'Ddec') $exploded[8] = "12";

    if ($exploded[0] < date('m')){

        $year1=date('Y')+1;

    }else{$year1=date('Y');}

    if ($exploded[8] < date('m')){

        $year2=date('Y')+1;

    }else{$year2=date('Y');}

    if ($exploded[1]<10){
        $zero="0";
    }else{$zero="";}
    $date_debut=$year1.'-'.$exploded[0].'-'.$zero.$exploded[1];
    $date_fin=$year2.'-'.$exploded[8].'-'.$zero.$exploded[9] ?? '';
    $heure_debut=$exploded[3].':00';
    return $date_debut;

}

function convert_date_debut_menton_counted_15_to_sql_date_fin($exploded){

    if ($exploded[0] == 'janvier' || $exploded[0] == 'jan') $exploded[0] = "01";
    if ($exploded[0] == 'février' || $exploded[0] == 'Fevier' || $exploded[0] == 'fev') $exploded[0] = "02";
    if ($exploded[0] == 'mars' || $exploded[0] == 'mar') $exploded[0] = "03";
    if ($exploded[0] == 'avril' || $exploded[0] == 'avr') $exploded[0] = "04";
    if ($exploded[0] == 'mai' || $exploded[0] == 'mai') $exploded[0] = "05";
    if ($exploded[0] == 'juin' || $exploded[0] == 'juin') $exploded[0] = "06";
    if ($exploded[0] == 'juillet' || $exploded[0] == 'juil') $exploded[0] = "07";
    if ($exploded[0] == 'août' || $exploded[0] == 'aout' || $exploded[0] == 'Aug') $exploded[0] = "08";
    if ($exploded[0] == 'septembre' || $exploded[0] == 'sep') $exploded[0] = "09";
    if ($exploded[0] == 'octobre' || $exploded[0] == 'oct') $exploded[0] = "10";
    if ($exploded[0] == 'novembre' || $exploded[0] == 'nov') $exploded[0] = "11";
    if ($exploded[0] == 'décembre' || $exploded[0] == 'Decembre' || $exploded[0] == 'Ddec') $exploded[0] = "12";


    if ($exploded[8] == 'janvier' || $exploded[8] == 'jan') $exploded[8] = "01";
    if ($exploded[8] == 'février' || $exploded[8] == 'Fevier' || $exploded[8] == 'fev') $exploded[8] = "02";
    if ($exploded[8] == 'mars' || $exploded[8] == 'mar') $exploded[8] = "03";
    if ($exploded[8] == 'avril' || $exploded[8] == 'avr') $exploded[8] = "04";
    if ($exploded[8] == 'mai' || $exploded[8] == 'mai') $exploded[8] = "05";
    if ($exploded[8] == 'juin' || $exploded[8] == 'juin') $exploded[8] = "06";
    if ($exploded[8] == 'juillet' || $exploded[8] == 'juil') $exploded[8] = "07";
    if ($exploded[8] == 'août' || $exploded[8] == 'aout' || $exploded[8] == 'Aug') $exploded[8] = "08";
    if ($exploded[8] == 'septembre' || $exploded[8] == 'sep') $exploded[8] = "09";
    if ($exploded[8] == 'octobre' || $exploded[8] == 'oct') $exploded[8] = "10";
    if ($exploded[8] == 'novembre' || $exploded[8] == 'nov') $exploded[8] = "11";
    if ($exploded[8] == 'décembre' || $exploded[8] == 'Decembre' || $exploded[8] == 'Ddec') $exploded[8] = "12";

    if ($exploded[0] < date('m')){

        $year1=date('Y')+1;

    }else{$year1=date('Y');}

    if ($exploded[8] < date('m')){

        $year2=date('Y')+1;

    }else{$year2=date('Y');}

    if ($exploded[1]<10){
        $zero="0";
    }else{$zero="";}
    $date_debut=$year1.'-'.$exploded[0].'-'.$zero.$exploded[1];
    $date_fin=$year2.'-'.$exploded[8].'-'.$zero.$exploded[9] ?? '';
    $heure_debut=$exploded[3].':00';
    return $date_fin;

}

function convert_date_debut_menton_counted_15_to_sql_heure_debut($exploded){

    if ($exploded[0] == 'janvier' || $exploded[0] == 'jan') $exploded[0] = "01";
    if ($exploded[0] == 'février' || $exploded[0] == 'Fevier' || $exploded[0] == 'fev') $exploded[0] = "02";
    if ($exploded[0] == 'mars' || $exploded[0] == 'mar') $exploded[0] = "03";
    if ($exploded[0] == 'avril' || $exploded[0] == 'avr') $exploded[0] = "04";
    if ($exploded[0] == 'mai' || $exploded[0] == 'mai') $exploded[0] = "05";
    if ($exploded[0] == 'juin' || $exploded[0] == 'juin') $exploded[0] = "06";
    if ($exploded[0] == 'juillet' || $exploded[0] == 'juil') $exploded[0] = "07";
    if ($exploded[0] == 'août' || $exploded[0] == 'aout' || $exploded[0] == 'Aug') $exploded[0] = "08";
    if ($exploded[0] == 'septembre' || $exploded[0] == 'sep') $exploded[0] = "09";
    if ($exploded[0] == 'octobre' || $exploded[0] == 'oct') $exploded[0] = "10";
    if ($exploded[0] == 'novembre' || $exploded[0] == 'nov') $exploded[0] = "11";
    if ($exploded[0] == 'décembre' || $exploded[0] == 'Decembre' || $exploded[0] == 'Ddec') $exploded[0] = "12";


    if ($exploded[8] == 'janvier' || $exploded[8] == 'jan') $exploded[8] = "01";
    if ($exploded[8] == 'février' || $exploded[8] == 'Fevier' || $exploded[8] == 'fev') $exploded[8] = "02";
    if ($exploded[8] == 'mars' || $exploded[8] == 'mar') $exploded[8] = "03";
    if ($exploded[8] == 'avril' || $exploded[8] == 'avr') $exploded[8] = "04";
    if ($exploded[8] == 'mai' || $exploded[8] == 'mai') $exploded[8] = "05";
    if ($exploded[8] == 'juin' || $exploded[8] == 'juin') $exploded[8] = "06";
    if ($exploded[8] == 'juillet' || $exploded[8] == 'juil') $exploded[8] = "07";
    if ($exploded[8] == 'août' || $exploded[8] == 'aout' || $exploded[8] == 'Aug') $exploded[8] = "08";
    if ($exploded[8] == 'septembre' || $exploded[8] == 'sep') $exploded[8] = "09";
    if ($exploded[8] == 'octobre' || $exploded[8] == 'oct') $exploded[8] = "10";
    if ($exploded[8] == 'novembre' || $exploded[8] == 'nov') $exploded[8] = "11";
    if ($exploded[8] == 'décembre' || $exploded[8] == 'Decembre' || $exploded[8] == 'Ddec') $exploded[8] = "12";

    if ($exploded[0] < date('m')){

        $year1=date('Y')+1;

    }else{$year1=date('Y');}

    if ($exploded[8] < date('m')){

        $year2=date('Y')+1;

    }else{$year2=date('Y');}

    if ($exploded[1]<10){
        $zero="0";
    }else{$zero="";}
    $date_debut=$year1.'-'.$exploded[0].'-'.$zero.$exploded[1];
    $date_fin=$year2.'-'.$exploded[8].'-'.$zero.$exploded[9] ?? '';
    $heure_debut=$exploded[3].':00';
    return $heure_debut;

}

function convert_date_debut_menton_counted_7_to_sql_date_debut($exploded){

    if ($exploded[0] == 'janvier' || $exploded[0] == 'jan') $exploded[0] = "01";
    if ($exploded[0] == 'février' || $exploded[0] == 'Fevier' || $exploded[0] == 'fev') $exploded[0] = "02";
    if ($exploded[0] == 'mars' || $exploded[0] == 'mar') $exploded[0] = "03";
    if ($exploded[0] == 'avril' || $exploded[0] == 'avr') $exploded[0] = "04";
    if ($exploded[0] == 'mai' || $exploded[0] == 'mai') $exploded[0] = "05";
    if ($exploded[0] == 'juin' || $exploded[0] == 'juin') $exploded[0] = "06";
    if ($exploded[0] == 'juillet' || $exploded[0] == 'juil') $exploded[0] = "07";
    if ($exploded[0] == 'août' || $exploded[0] == 'aout' || $exploded[0] == 'Aug') $exploded[0] = "08";
    if ($exploded[0] == 'septembre' || $exploded[0] == 'sep') $exploded[0] = "09";
    if ($exploded[0] == 'octobre' || $exploded[0] == 'oct') $exploded[0] = "10";
    if ($exploded[0] == 'novembre' || $exploded[0] == 'nov') $exploded[0] = "11";
    if ($exploded[0] == 'décembre' || $exploded[0] == 'Decembre' || $exploded[0] == 'Ddec') $exploded[0] = "12";
    if ($exploded[0] < date('m')){

        $year=date('Y')+1;

    }else{$year=date('Y');}
    if ($exploded[1]<10){
        $zero="0";
    }else{$zero="";}
    $date_debut=$year.'-'.$exploded[0].'-'.$zero.$exploded[1];
    $heure_debut=$exploded[3].':00';
    return $date_debut;
}

function convert_date_debut_menton_counted_7_to_sql_heure_debut($exploded){

    if ($exploded[0] == 'janvier' || $exploded[0] == 'jan') $exploded[0] = "01";
    if ($exploded[0] == 'février' || $exploded[0] == 'Fevier' || $exploded[0] == 'fev') $exploded[0] = "02";
    if ($exploded[0] == 'mars' || $exploded[0] == 'mar') $exploded[0] = "03";
    if ($exploded[0] == 'avril' || $exploded[0] == 'avr') $exploded[0] = "04";
    if ($exploded[0] == 'mai' || $exploded[0] == 'mai') $exploded[0] = "05";
    if ($exploded[0] == 'juin' || $exploded[0] == 'juin') $exploded[0] = "06";
    if ($exploded[0] == 'juillet' || $exploded[0] == 'juil') $exploded[0] = "07";
    if ($exploded[0] == 'août' || $exploded[0] == 'aout' || $exploded[0] == 'Aug') $exploded[0] = "08";
    if ($exploded[0] == 'septembre' || $exploded[0] == 'sep') $exploded[0] = "09";
    if ($exploded[0] == 'octobre' || $exploded[0] == 'oct') $exploded[0] = "10";
    if ($exploded[0] == 'novembre' || $exploded[0] == 'nov') $exploded[0] = "11";
    if ($exploded[0] == 'décembre' || $exploded[0] == 'Decembre' || $exploded[0] == 'Ddec') $exploded[0] = "12";
    if ($exploded[0] < date('m')){

        $year=date('Y')+1;

    }else{$year=date('Y');}
    if ($exploded[1]<10){
        $zero="0";
    }else{$zero="";}
    $date_debut=$year.'-'.$exploded[0].'-'.$zero.$exploded[1];
    $heure_debut=$exploded[3].':00';
    return $heure_debut;
}

function convert_date_debut_menton_counted_4_to_sql_date_debut($exploded){


    if ($exploded[0] == 'janvier' || $exploded[0] == 'jan') $exploded[0] = "01";
    if ($exploded[0] == 'février' || $exploded[0] == 'Fevier' || $exploded[0] == 'fev') $exploded[0] = "02";
    if ($exploded[0] == 'mars' || $exploded[0] == 'mar') $exploded[0] = "03";
    if ($exploded[0] == 'avril' || $exploded[0] == 'avr') $exploded[0] = "04";
    if ($exploded[0] == 'mai' || $exploded[0] == 'mai') $exploded[0] = "05";
    if ($exploded[0] == 'juin' || $exploded[0] == 'juin') $exploded[0] = "06";
    if ($exploded[0] == 'juillet' || $exploded[0] == 'juil') $exploded[0] = "07";
    if ($exploded[0] == 'août' || $exploded[0] == 'aout' || $exploded[0] == 'Aug') $exploded[0] = "08";
    if ($exploded[0] == 'septembre' || $exploded[0] == 'sep') $exploded[0] = "09";
    if ($exploded[0] == 'octobre' || $exploded[0] == 'oct') $exploded[0] = "10";
    if ($exploded[0] == 'novembre' || $exploded[0] == 'nov') $exploded[0] = "11";
    if ($exploded[0] == 'décembre' || $exploded[0] == 'Decembre' || $exploded[0] == 'Ddec') $exploded[0] = "12";
    if ($exploded[0] < date('m')){
        $year=date('Y')+1;

    }else{$year=date('Y');}
    if ($exploded[1]<10){
        $zero="0";
    }else{$zero="";}
    $date_debut=$year.'-'.$exploded[0].'-'.$zero.$exploded[1];
    return $date_debut;
}

function convert_standard_date_to_sql_date_debut($exploded){

    if ($exploded[1] == 'janvier' || $exploded[1] == 'Jan') $exploded[1] = "01";
    if ($exploded[1] == 'février' || $exploded[1] == 'Fevier' || $exploded[1] == 'Feb') $exploded[1] = "02";
    if ($exploded[1] == 'mars' || $exploded[1] == 'Mar') $exploded[1] = "03";
    if ($exploded[1] == 'avril' || $exploded[1] == 'Apr') $exploded[1] = "04";
    if ($exploded[1] == 'mai' || $exploded[1] == 'May') $exploded[1] = "05";
    if ($exploded[1] == 'juin' || $exploded[1] == 'Jun') $exploded[1] = "06";
    if ($exploded[1] == 'juillet' || $exploded[1] == 'Jul') $exploded[1] = "07";
    if ($exploded[1] == 'août' || $exploded[1] == 'Aout' || $exploded[1] == 'Aug') $exploded[1] = "08";
    if ($exploded[1] == 'septembre' || $exploded[1] == 'Sep') $exploded[1] = "09";
    if ($exploded[1] == 'octobre' || $exploded[1] == 'Oct') $exploded[1] = "10";
    if ($exploded[1] == 'novembre' || $exploded[1] == 'Nov') $exploded[1] = "11";
    if ($exploded[1] == 'décembre' || $exploded[1] == 'Decembre' || $exploded[1] == 'Dec') $exploded[1] = "12";

    if ($exploded[1] < date('m')){

        $year=date('Y')+1;

    }else{$year=date('Y');}
    $date_debut=$year.'-'.$exploded[1].'-'.$exploded[0];
    return $date_debut;
}

function convert_date_raphael_counted_8_to_sql_date_debut($mois,$date_decoded,$date){

    if ($mois == 'janvier' || $mois == 'Jan') $mois = "01";
    if ($mois == 'février' || $mois == 'Fevier' || $mois == 'Feb') $mois = "02";
    if ($mois == 'mars' || $mois == 'Mar') $mois = "03";
    if ($mois == 'avril' || $mois == 'Apr') $mois = "04";
    if ($mois == 'mai' || $mois == 'May') $mois = "05";
    if ($mois == 'juin' || $mois == 'Jun') $mois = "06";
    if ($mois == 'juillet' || $mois == 'Jul') $mois = "07";
    if ($mois == 'août' || $mois == 'Aout' || $mois == 'Aug') $mois = "08";
    if ($mois == 'septembre' || $mois == 'Sep') $mois = "09";
    if ($mois == 'octobre' || $mois == 'Oct') $mois = "10";
    if ($mois == 'novembre' || $mois == 'Nov') $mois = "11";
    if ($mois == 'décembre' || $mois == 'Decembre' || $mois == 'Dec') $mois = "12";
    if ($date_decoded[1] < 9){$zero='0';}else{$zero='';}

    $date_debut=$date_decoded[3].'-'.$mois.'-'.$zero.$date_decoded[1];
    $heure_exploded=explode('-',$date);
    $heure_debut=preg_replace('/ /','',$heure_exploded[1]);
    return $date_debut;
}

function convert_date_raphael_counted_8_to_sql_heure_debut($mois,$date_decoded,$date){

    if ($mois == 'janvier' || $mois == 'Jan') $mois = "01";
    if ($mois == 'février' || $mois == 'Fevier' || $mois == 'Feb') $mois = "02";
    if ($mois == 'mars' || $mois == 'Mar') $mois = "03";
    if ($mois == 'avril' || $mois == 'Apr') $mois = "04";
    if ($mois == 'mai' || $mois == 'May') $mois = "05";
    if ($mois == 'juin' || $mois == 'Jun') $mois = "06";
    if ($mois == 'juillet' || $mois == 'Jul') $mois = "07";
    if ($mois == 'août' || $mois == 'Aout' || $mois == 'Aug') $mois = "08";
    if ($mois == 'septembre' || $mois == 'Sep') $mois = "09";
    if ($mois == 'octobre' || $mois == 'Oct') $mois = "10";
    if ($mois == 'novembre' || $mois == 'Nov') $mois = "11";
    if ($mois == 'décembre' || $mois == 'Decembre' || $mois == 'Dec') $mois = "12";
    if ($date_decoded[1] < 9){$zero='0';}else{$zero='';}

    $date_debut=$date_decoded[3].'-'.$mois.'-'.$zero.$date_decoded[1];
    $heure_exploded=explode('-',$date);
    $heure_debut=preg_replace('/ /','',$heure_exploded[1]);
    return $heure_debut;
}

function convert_date_raphael_counted_13_to_sql_date_debut($date_decoded,$date){

    $mois=preg_replace('/,/','',$date_decoded[2]);

    if ($mois == 'janvier' || $mois == 'Jan') $mois = "01";
    if ($mois == 'février' || $mois == 'Fevier' || $mois == 'Feb') $mois = "02";
    if ($mois == 'mars' || $mois == 'Mar') $mois = "03";
    if ($mois == 'avril' || $mois == 'Apr') $mois = "04";
    if ($mois == 'mai' || $mois == 'May') $mois = "05";
    if ($mois == 'juin' || $mois == 'Jun') $mois = "06";
    if ($mois == 'juillet' || $mois == 'Jul') $mois = "07";
    if ($mois == 'août' || $mois == 'Aout' || $mois == 'Aug') $mois = "08";
    if ($mois == 'septembre' || $mois == 'Sep') $mois = "09";
    if ($mois == 'octobre' || $mois == 'Oct') $mois = "10";
    if ($mois == 'novembre' || $mois == 'Nov') $mois = "11";
    if ($mois == 'décembre' || $mois == 'Decembre' || $mois == 'Dec') $mois = "12";
    if ($date_decoded[1] < 9){$zero='0';}else{$zero='';}
    $date_debut=$date_decoded[3].'-'.$mois.'-'.$zero.$date_decoded[1];
    $heure_exploded=explode('-',$date);
    $heure_debut=preg_replace('/ /','',$heure_exploded[1]);

    $mois2=preg_replace('/,/','',$date_decoded[9]);

    if ($mois2 == 'janvier' || $mois2 == 'Jan') $mois2 = "01";
    if ($mois2 == 'février' || $mois2 == 'Fevier' || $mois2 == 'Feb') $mois2 = "02";
    if ($mois2 == 'mars' || $mois2 == 'Mar') $mois2 = "03";
    if ($mois2 == 'avril' || $mois2 == 'Apr') $mois2 = "04";
    if ($mois2 == 'mai' || $mois2 == 'May') $mois2 = "05";
    if ($mois2 == 'juin' || $mois2 == 'Jun') $mois2 = "06";
    if ($mois2 == 'juillet' || $mois2 == 'Jul') $mois2 = "07";
    if ($mois2 == 'août' || $mois2 == 'Aout' || $mois2 == 'Aug') $mois = "08";
    if ($mois2 == 'septembre' || $mois2 == 'Sep') $mois2 = "09";
    if ($mois2 == 'octobre' || $mois2 == 'Oct') $mois2 = "10";
    if ($mois2 == 'novembre' || $mois2 == 'Nov') $mois2 = "11";
    if ($mois2 == 'décembre' || $mois2 == 'Decembre' || $mois2 == 'Dec') $mois2 = "12";
    if ($date_decoded[1] < 9){$zero2='0';}else{$zero2='';}
    $date_fin=$date_decoded[10].'-'.$mois2.'-'.$zero2.$date_decoded[8];
    return $date_debut;
}

function convert_date_raphael_counted_13_to_sql_heure_debut($date_decoded,$date){

    $mois=preg_replace('/,/','',$date_decoded[2]);

    if ($mois == 'janvier' || $mois == 'Jan') $mois = "01";
    if ($mois == 'février' || $mois == 'Fevier' || $mois == 'Feb') $mois = "02";
    if ($mois == 'mars' || $mois == 'Mar') $mois = "03";
    if ($mois == 'avril' || $mois == 'Apr') $mois = "04";
    if ($mois == 'mai' || $mois == 'May') $mois = "05";
    if ($mois == 'juin' || $mois == 'Jun') $mois = "06";
    if ($mois == 'juillet' || $mois == 'Jul') $mois = "07";
    if ($mois == 'août' || $mois == 'Aout' || $mois == 'Aug') $mois = "08";
    if ($mois == 'septembre' || $mois == 'Sep') $mois = "09";
    if ($mois == 'octobre' || $mois == 'Oct') $mois = "10";
    if ($mois == 'novembre' || $mois == 'Nov') $mois = "11";
    if ($mois == 'décembre' || $mois == 'Decembre' || $mois == 'Dec') $mois = "12";
    if ($date_decoded[1] < 9){$zero='0';}else{$zero='';}
    $date_debut=$date_decoded[3].'-'.$mois.'-'.$zero.$date_decoded[1];
    $heure_exploded=explode('-',$date);
    $heure_debut=preg_replace('/ /','',$heure_exploded[1]);

    $mois2=preg_replace('/,/','',$date_decoded[9]);

    if ($mois2 == 'janvier' || $mois2 == 'Jan') $mois2 = "01";
    if ($mois2 == 'février' || $mois2 == 'Fevier' || $mois2 == 'Feb') $mois2 = "02";
    if ($mois2 == 'mars' || $mois2 == 'Mar') $mois2 = "03";
    if ($mois2 == 'avril' || $mois2 == 'Apr') $mois2 = "04";
    if ($mois2 == 'mai' || $mois2 == 'May') $mois2 = "05";
    if ($mois2 == 'juin' || $mois2 == 'Jun') $mois2 = "06";
    if ($mois2 == 'juillet' || $mois2 == 'Jul') $mois2 = "07";
    if ($mois2 == 'août' || $mois2 == 'Aout' || $mois2 == 'Aug') $mois = "08";
    if ($mois2 == 'septembre' || $mois2 == 'Sep') $mois2 = "09";
    if ($mois2 == 'octobre' || $mois2 == 'Oct') $mois2 = "10";
    if ($mois2 == 'novembre' || $mois2 == 'Nov') $mois2 = "11";
    if ($mois2 == 'décembre' || $mois2 == 'Decembre' || $mois2 == 'Dec') $mois2 = "12";
    if ($date_decoded[1] < 9){$zero2='0';}else{$zero2='';}
    $date_fin=$date_decoded[10].'-'.$mois2.'-'.$zero2.$date_decoded[8];
    return $heure_debut;
}

function convert_date_raphael_counted_13_to_sql_date_fin($date_decoded,$date){

    $mois=preg_replace('/,/','',$date_decoded[2]);

    if ($mois == 'janvier' || $mois == 'Jan') $mois = "01";
    if ($mois == 'février' || $mois == 'Fevier' || $mois == 'Feb') $mois = "02";
    if ($mois == 'mars' || $mois == 'Mar') $mois = "03";
    if ($mois == 'avril' || $mois == 'Apr') $mois = "04";
    if ($mois == 'mai' || $mois == 'May') $mois = "05";
    if ($mois == 'juin' || $mois == 'Jun') $mois = "06";
    if ($mois == 'juillet' || $mois == 'Jul') $mois = "07";
    if ($mois == 'août' || $mois == 'Aout' || $mois == 'Aug') $mois = "08";
    if ($mois == 'septembre' || $mois == 'Sep') $mois = "09";
    if ($mois == 'octobre' || $mois == 'Oct') $mois = "10";
    if ($mois == 'novembre' || $mois == 'Nov') $mois = "11";
    if ($mois == 'décembre' || $mois == 'Decembre' || $mois == 'Dec') $mois = "12";
    if ($date_decoded[1] < 9){$zero='0';}else{$zero='';}
    $date_debut=$date_decoded[3].'-'.$mois.'-'.$zero.$date_decoded[1];
    $heure_exploded=explode('-',$date);
    $heure_debut=preg_replace('/ /','',$heure_exploded[1]);

    $mois2=preg_replace('/,/','',$date_decoded[9]);

    if ($mois2 == 'janvier' || $mois2 == 'Jan') $mois2 = "01";
    if ($mois2 == 'février' || $mois2 == 'Fevier' || $mois2 == 'Feb') $mois2 = "02";
    if ($mois2 == 'mars' || $mois2 == 'Mar') $mois2 = "03";
    if ($mois2 == 'avril' || $mois2 == 'Apr') $mois2 = "04";
    if ($mois2 == 'mai' || $mois2 == 'May') $mois2 = "05";
    if ($mois2 == 'juin' || $mois2 == 'Jun') $mois2 = "06";
    if ($mois2 == 'juillet' || $mois2 == 'Jul') $mois2 = "07";
    if ($mois2 == 'août' || $mois2 == 'Aout' || $mois2 == 'Aug') $mois = "08";
    if ($mois2 == 'septembre' || $mois2 == 'Sep') $mois2 = "09";
    if ($mois2 == 'octobre' || $mois2 == 'Oct') $mois2 = "10";
    if ($mois2 == 'novembre' || $mois2 == 'Nov') $mois2 = "11";
    if ($mois2 == 'décembre' || $mois2 == 'Decembre' || $mois2 == 'Dec') $mois2 = "12";
    if ($date_decoded[1] < 9){$zero2='0';}else{$zero2='';}
    $date_fin=$date_decoded[10].'-'.$mois2.'-'.$zero2.$date_decoded[8];
    return $date_fin;
}

function convert_date_drag_to_sql_date_debut($date_brute){

    if ($date_brute[1] == 'Janvier' || $date_brute[1] == 'jan') $date_brute[1] = "01";
    if ($date_brute[1] == 'Février' || $date_brute[1] == 'Fevier' || $date_brute[1] == 'fev') $date_brute[1] = "02";
    if ($date_brute[1] == 'Mars' || $date_brute[1] == 'mars') $date_brute[1] = "03";
    if ($date_brute[1] == 'Avril' || $date_brute[1] == 'avr') $date_brute[1] = "04";
    if ($date_brute[1] == 'Mai' || $date_brute[1] == 'mai') $date_brute[1] = "05";
    if ($date_brute[1] == 'Juin' || $date_brute[1] == 'juin') $date_brute[1] = "06";
    if ($date_brute[1] == 'Juillet' || $date_brute[1] == 'juil') $date_brute[1] = "07";
    if ($date_brute[1] == 'Août' || $date_brute[1] == 'aout' || $date_brute[1] == 'Aug') $date_brute[1] = "08";
    if ($date_brute[1] == 'Septembre' || $date_brute[1] == 'sept') $date_brute[1] = "09";
    if ($date_brute[1] == 'Octobre' || $date_brute[1] == 'oct') $date_brute[1] = "10";
    if ($date_brute[1] == 'Novembre' || $date_brute[1] == 'nov') $date_brute[1] = "11";
    if ($date_brute[1] == 'Décembre' || $date_brute[1] == 'Decembre' || $date_brute[1] == 'dec') $date_brute[1] = "12";

    $date_debut=$date_brute[2].'-'.$date_brute[1].'-'.$date_brute[0];
    return $date_debut;
}

function convert_date_drag_to_sql_heure_debut($date_brute){

    if ($date_brute[1] == 'Janvier' || $date_brute[1] == 'jan') $date_brute[1] = "01";
    if ($date_brute[1] == 'Février' || $date_brute[1] == 'Fevier' || $date_brute[1] == 'fev') $date_brute[1] = "02";
    if ($date_brute[1] == 'Mars' || $date_brute[1] == 'mars') $date_brute[1] = "03";
    if ($date_brute[1] == 'Avril' || $date_brute[1] == 'avr') $date_brute[1] = "04";
    if ($date_brute[1] == 'Mai' || $date_brute[1] == 'mai') $date_brute[1] = "05";
    if ($date_brute[1] == 'Juin' || $date_brute[1] == 'juin') $date_brute[1] = "06";
    if ($date_brute[1] == 'Juillet' || $date_brute[1] == 'juil') $date_brute[1] = "07";
    if ($date_brute[1] == 'Août' || $date_brute[1] == 'aout' || $date_brute[1] == 'Aug') $date_brute[1] = "08";
    if ($date_brute[1] == 'Septembre' || $date_brute[1] == 'sept') $date_brute[1] = "09";
    if ($date_brute[1] == 'Octobre' || $date_brute[1] == 'oct') $date_brute[1] = "10";
    if ($date_brute[1] == 'Novembre' || $date_brute[1] == 'nov') $date_brute[1] = "11";
    if ($date_brute[1] == 'Décembre' || $date_brute[1] == 'Decembre' || $date_brute[1] == 'dec') $date_brute[1] = "12";

    $date_debut=$date_brute[2].'-'.$date_brute[1].'-'.$date_brute[0];
    $heure_debut=$date_brute[3];
    return $heure_debut;
}

function convert_date_debut_loubet_to_sql_date_debut($date_exploded){
    if ($date_exploded){

        $date=$date_exploded[0];
        $mois=preg_replace('([0-9])','',$date);
        if (!preg_match('([0-9])',$date)){
            $jours='01';
        }else{
            $jours=preg_replace('([a-zéû])','',$date);
        }

        if ($mois == 'Janvier' || $mois=="janv" || $mois == 'jan') $mois = "01";
        if ($mois == 'Février' || $mois =="fév" || $mois == 'Fevier' || $mois == 'fev' || $mois=="févr") $mois = "02";
        if ($mois == 'Mars' || $mois == 'mars' || $mois == 'mar') $mois = "03";
        if ($mois == 'Avril' || $mois == 'avr') $mois = "04";
        if ($mois == 'Mai' || $mois == 'mai') $mois = "05";
        if ($mois == 'Juin' || $mois == 'juin') $mois = "06";
        if ($mois == 'Juillet' || $mois == 'juil') $mois = "07";
        if ($mois == 'Août' || $mois == 'aout' || $mois == 'Aug') $mois = "08";
        if ($mois == 'Septembre' || $mois == 'sept') $mois = "09";
        if ($mois == 'Octobre' || $mois == 'oct') $mois = "10";
        if ($mois == 'Novembre' || $mois == 'nov') $mois = "11";
        if ($mois == 'Décembre' || $mois == 'Decembre' || $mois == 'dec'  || $mois == 'déc') $mois = "12";


        $date_debut=date('Y').'-'.$mois.'-'.$jours;

    }
    return $date_debut;
}
function convert_heure_debut_loubet_to_sql_heure_debut($heure_exploded)
{
    if (isset($heure_exploded[1]) AND $heure_exploded[1] !='' ){
        $second =$heure_exploded[1];
    }else{
        $second='00';
    }
    $heure_debut=$heure_exploded[0].':'.$second;
    return $heure_debut;
}
function convert_date_fin_loubet_to_sql_date_fin($date_exploded,$jour_fin)
{
    if ($date_exploded){

        $date=$date_exploded[0];
        $mois=preg_replace('([0-9])','',$date);
        $jours=preg_replace('([a-zéû])','',$date);

        if ($mois == 'Janvier' || $mois=="janv" || $mois == 'jan') $mois = "01";
        if ($mois == 'Février' || $mois == 'Fevier' || $mois == 'fev' || $mois =="fév" || $mois=="févr") $mois = "02";
        if ($mois == 'Mars' || $mois == 'mars' || $mois == 'mar') $mois = "03";
        if ($mois == 'Avril' || $mois == 'avr') $mois = "04";
        if ($mois == 'Mai' || $mois == 'mai') $mois = "05";
        if ($mois == 'Juin' || $mois == 'juin') $mois = "06";
        if ($mois == 'Juillet' || $mois == 'juil') $mois = "07";
        if ($mois == 'Août' || $mois == 'aout' || $mois == 'Aug') $mois = "08";
        if ($mois == 'Septembre' || $mois == 'sept') $mois = "09";
        if ($mois == 'Octobre' || $mois == 'oct') $mois = "10";
        if ($mois == 'Novembre' || $mois == 'nov') $mois = "11";
        if ($mois == 'Décembre' || $mois == 'Decembre' || $mois == 'dec'  || $mois == 'déc') $mois = "12";


        $date_fin=date('Y').'-'.$mois.'-'.$jour_fin;

    }
    return $date_fin;
}

function convert_mandelieu_date_to_sql_date_debut($date){
    $date_exploded=explode(' ',$date);
    if (count($date_exploded)==6){
        if ($date_exploded){


            if ($date_exploded[2] == 'janvier' || $date_exploded[2] == 'jan') $date_exploded[2] = "01";
            if ($date_exploded[2] == 'février' || $date_exploded[2] == 'Fevier' || $date_exploded[2] == 'fev') $date_exploded[2] = "02";
            if ($date_exploded[2] == 'mars' || $date_exploded[2] == 'mars' || $date_exploded[2] == 'mar') $date_exploded[2] = "03";
            if ($date_exploded[2] == 'avril' || $date_exploded[2] == 'avr') $date_exploded[2] = "04";
            if ($date_exploded[2] == 'mai' || $date_exploded[2] == 'mai') $date_exploded[2] = "05";
            if ($date_exploded[2] == 'juin' || $date_exploded[2] == 'juin') $date_exploded[2] = "06";
            if ($date_exploded[2] == 'juillet' || $date_exploded[2] == 'juil') $date_exploded[2] = "07";
            if ($date_exploded[2] == 'août' || $date_exploded[2] == 'aout' || $date_exploded[2] == 'Aug') $date_exploded[2] = "08";
            if ($date_exploded[2] == 'septembre' || $date_exploded[2] == 'sept') $date_exploded[2] = "09";
            if ($date_exploded[2] == 'octobre' || $date_exploded[2] == 'oct') $date_exploded[2] = "10";
            if ($date_exploded[2] == 'novembre' || $date_exploded[2] == 'nov') $date_exploded[2] = "11";
            if ($date_exploded[2] == 'décembre' || $date_exploded[2] == 'Decembre' || $date_exploded[2] == 'dec'  || $date_exploded[2] == 'déc') $date_exploded[2] = "12";


            $date_debut=$date_exploded[3].'-'.$date_exploded[2].'-'.preg_replace('/er/','',$date_exploded[1]);
            $heure=explode('à',$date);
            $heure_final=explode('h',$heure[1]);
            $heure_debut=preg_replace('/./','' ,$heure_final[0].':'.$heure_final[1]);
        }
    }elseif (count($date_exploded)== 8 ){

        if ($date_exploded){

            if ($date_exploded[2] == 'janvier' || $date_exploded[2] == 'jan') $date_exploded[2] = "01";
            if ($date_exploded[2] == 'février' || $date_exploded[2] == 'Fevier' || $date_exploded[2] == 'fev') $date_exploded[2] = "02";
            if ($date_exploded[2] == 'mars' || $date_exploded[2] == 'mars' || $date_exploded[2] == 'mar') $date_exploded[2] = "03";
            if ($date_exploded[2] == 'avril' || $date_exploded[2] == 'avr') $date_exploded[2] = "04";
            if ($date_exploded[2] == 'mai' || $date_exploded[2] == 'mai') $date_exploded[2] = "05";
            if ($date_exploded[2] == 'juin' || $date_exploded[2] == 'juin') $date_exploded[2] = "06";
            if ($date_exploded[2] == 'juillet' || $date_exploded[2] == 'juil') $date_exploded[2] = "07";
            if ($date_exploded[2] == 'août' || $date_exploded[2] == 'aout' || $date_exploded[2] == 'Aug') $date_exploded[2] = "08";
            if ($date_exploded[2] == 'septembre' || $date_exploded[2] == 'sept') $date_exploded[2] = "09";
            if ($date_exploded[2] == 'octobre' || $date_exploded[2] == 'oct') $date_exploded[2] = "10";
            if ($date_exploded[2] == 'novembre' || $date_exploded[2] == 'nov') $date_exploded[2] = "11";
            if ($date_exploded[2] == 'décembre' || $date_exploded[2] == 'Decembre' || $date_exploded[2] == 'dec'  || $date_exploded[2] == 'déc') $date_exploded[2] = "12";

            $date_debut=$date_exploded[3].'-'.$date_exploded[2].'-'.preg_replace('/er/','',$date_exploded[1]);
            $heure=$date_exploded[5];
            $heure_debut=preg_replace('/h/','' ,$heure.':00');

        }
    }

    if (!preg_match('([a-z])', $date_debut)) {
        return $date_debut;
    }
    else
    {
        return null;
    }
}
function convert_mandelieu_date_to_sql_heure_debut($date){
    $date_exploded=explode(' ',$date);
    if (count($date_exploded)==6){
        if ($date_exploded){


            if ($date_exploded[2] == 'janvier' || $date_exploded[2] == 'jan') $date_exploded[2] = "01";
            if ($date_exploded[2] == 'février' || $date_exploded[2] == 'Fevier' || $date_exploded[2] == 'fev') $date_exploded[2] = "02";
            if ($date_exploded[2] == 'mars' || $date_exploded[2] == 'mars' || $date_exploded[2] == 'mar') $date_exploded[2] = "03";
            if ($date_exploded[2] == 'avril' || $date_exploded[2] == 'avr') $date_exploded[2] = "04";
            if ($date_exploded[2] == 'mai' || $date_exploded[2] == 'mai') $date_exploded[2] = "05";
            if ($date_exploded[2] == 'juin' || $date_exploded[2] == 'juin') $date_exploded[2] = "06";
            if ($date_exploded[2] == 'juillet' || $date_exploded[2] == 'juil') $date_exploded[2] = "07";
            if ($date_exploded[2] == 'août' || $date_exploded[2] == 'aout' || $date_exploded[2] == 'Aug') $date_exploded[2] = "08";
            if ($date_exploded[2] == 'septembre' || $date_exploded[2] == 'sept') $date_exploded[2] = "09";
            if ($date_exploded[2] == 'octobre' || $date_exploded[2] == 'oct') $date_exploded[2] = "10";
            if ($date_exploded[2] == 'novembre' || $date_exploded[2] == 'nov') $date_exploded[2] = "11";
            if ($date_exploded[2] == 'décembre' || $date_exploded[2] == 'Decembre' || $date_exploded[2] == 'dec'  || $date_exploded[2] == 'déc') $date_exploded[2] = "12";


            $date_debut=$date_exploded[3].'-'.$date_exploded[2].'-'.$date_exploded[1];
            $heure=explode('à',$date);
            $heure_final=explode('h',$heure[1]);
            $heure_debut=preg_replace('/./','' ,$heure_final[0].':'.$heure_final[1]);
        }
    }elseif (count($date_exploded)== 8 ){
        if ($date_exploded){

            if ($date_exploded[2] == 'janvier' || $date_exploded[2] == 'jan') $date_exploded[2] = "01";
            if ($date_exploded[2] == 'février' || $date_exploded[2] == 'Fevier' || $date_exploded[2] == 'fev') $date_exploded[2] = "02";
            if ($date_exploded[2] == 'mars' || $date_exploded[2] == 'mars' || $date_exploded[2] == 'mar') $date_exploded[2] = "03";
            if ($date_exploded[2] == 'avril' || $date_exploded[2] == 'avr') $date_exploded[2] = "04";
            if ($date_exploded[2] == 'mai' || $date_exploded[2] == 'mai') $date_exploded[2] = "05";
            if ($date_exploded[2] == 'juin' || $date_exploded[2] == 'juin') $date_exploded[2] = "06";
            if ($date_exploded[2] == 'juillet' || $date_exploded[2] == 'juil') $date_exploded[2] = "07";
            if ($date_exploded[2] == 'août' || $date_exploded[2] == 'aout' || $date_exploded[2] == 'Aug') $date_exploded[2] = "08";
            if ($date_exploded[2] == 'septembre' || $date_exploded[2] == 'sept') $date_exploded[2] = "09";
            if ($date_exploded[2] == 'octobre' || $date_exploded[2] == 'oct') $date_exploded[2] = "10";
            if ($date_exploded[2] == 'novembre' || $date_exploded[2] == 'nov') $date_exploded[2] = "11";
            if ($date_exploded[2] == 'décembre' || $date_exploded[2] == 'Decembre' || $date_exploded[2] == 'dec'  || $date_exploded[2] == 'déc') $date_exploded[2] = "12";

            $date_debut=$date_exploded[3].'-'.$date_exploded[2].'-'.$date_exploded[1];
            $heure=$date_exploded[5];
            $heure_debut=preg_replace('/h/','' ,$heure.':00');

        }
        return $heure_debut;
    }
}
function convert_loubet_art_date_to_sql_date_date_debut($date_exploded){
    $mois=$date_exploded[0];
    if ($mois == 'Janvier' || $mois=="Janv" || $mois == 'jan' || $mois=='Jan') $mois = "01";
    if ($mois == 'Février' || $mois =="fév" || $mois == 'Fevier' || $mois == 'Fév' || $mois=="févr") $mois = "02";
    if ($mois == 'Mars' || $mois == 'mars' || $mois == 'mar') $mois = "03";
    if ($mois == 'Avril' || $mois == 'avr') $mois = "04";
    if ($mois == 'Mai' || $mois == 'mai') $mois = "05";
    if ($mois == 'Juin' || $mois == 'juin') $mois = "06";
    if ($mois == 'Juillet' || $mois == 'juil') $mois = "07";
    if ($mois == 'Août' || $mois == 'aout' || $mois == 'Aug') $mois = "08";
    if ($mois == 'Septembre' || $mois == 'sept') $mois = "09";
    if ($mois == 'Octobre' || $mois == 'oct') $mois = "10";
    if ($mois == 'Novembre' || $mois == 'nov') $mois = "11";
    if ($mois == 'Décembre' || $mois == 'Decembre' || $mois == 'dec'  || $mois == 'déc') $mois = "12";

    $day=preg_replace('/,/','',$date_exploded[1]);

    $date_debut=$date_exploded[2].'-'.$mois.'-'.$day;
    return $date_debut;
}

function convert_carlo_date_to_sql_date_debut($mois){
    if ($mois == 'Janvier' || $mois=="Janv" || $mois == 'jan' || $mois=='Jan') $mois = "01";
    if ($mois == 'Février' || $mois =="fév" || $mois == 'Fevier' || $mois == 'Fév' || $mois=="févr") $mois = "02";
    if ($mois == 'Mars' || $mois == 'mars' || $mois == 'mar') $mois = "03";
    if ($mois == 'Avril' || $mois == 'avr') $mois = "04";
    if ($mois == 'Mai' || $mois == 'mai') $mois = "05";
    if ($mois == 'Juin' || $mois == 'juin') $mois = "06";
    if ($mois == 'Juillet' || $mois == 'juil') $mois = "07";
    if ($mois == 'Août' || $mois == 'aout' || $mois == 'Aug') $mois = "08";
    if ($mois == 'Septembre' || $mois == 'sept') $mois = "09";
    if ($mois == 'Octobre' || $mois == 'oct') $mois = "10";
    if ($mois == 'Novembre' || $mois == 'nov') $mois = "11";
    if ($mois == 'Décembre' || $mois == 'Decembre' || $mois == 'dec'  || $mois == 'déc') $mois = "12";

    $date_debut= date('Y').'-'.$mois.'-01';
    return $date_debut;
}
function convert_fayence_art_date_to_sql_date_date_debut($date){
    if ($date[2] == 'janvier' || $date=="Janv" || $date[2] == 'jan' || $date=='Jan') $date[2] = "01";
    if ($date[2] == 'février' || $date[2] =="fév" || $date[2] == 'Fevier' || $date[2] == 'Fév' || $date=="févr") $date[2] = "02";
    if ($date[2] == 'mars' || $date[2] == 'mars' || $date[2] == 'mar') $date[2] = "03";
    if ($date[2] == 'avril' || $date[2] == 'avr') $date[2] = "04";
    if ($date[2] == 'mai' || $date[2] == 'mai') $date[2] = "05";
    if ($date[2] == 'juin' || $date[2] == 'juin') $date[2] = "06";
    if ($date[2] == 'juillet' || $date[2] == 'juil') $date[2] = "07";
    if ($date[2] == 'août' || $date[2] == 'aout' || $date[2] == 'Aug') $date[2] = "08";
    if ($date[2] == 'septembre' || $date[2] == 'sept') $date[2] = "09";
    if ($date[2] == 'octobre' || $date[2] == 'oct') $date[2] = "10";
    if ($date[2] == 'novembre' || $date[2] == 'nov') $date[2] = "11";
    if ($date[2] == 'décembre' || $date[2] == 'decembre' || $date[2] == 'dec'  || $date[2] == 'déc') $date[2] = "12";
    $date_debut= $date[3].'-'.$date[2]."-".$date["1"];
    return $date_debut;
}
function convert_dracenie_date_debut_to_sql_date_debut($date){
    $part_date = $date[0]." ".$date[1]." ".$date[2]." ".$date[3];
    $cleaned_date = str_replace(".","",$part_date);
    $date_debut_exploded = explode(" ",$cleaned_date);
    $date_debut_brut = $date_debut_exploded[1];
    $date_debut = str_replace(".","",date("Y")."-".explode("/",$date_debut_brut)[1]."-".str_replace("1er","01",explode("/",$date_debut_brut)[0]));
    $format ="Y-m-d";
    if (DateTime::createFromFormat($format,$date_debut ?? null)){
        return $date_debut;
    }else{
        if ($date_debut_exploded[2] == 'janvier' || $date_debut_exploded[2]=="Janv" || $date_debut_exploded[2] == 'jan' || $date_debut_exploded[2]=='Jan') $date_debut_exploded[2] = "01";
        if ($date_debut_exploded[2] == 'février' || $date_debut_exploded[2] =="fév" || $date_debut_exploded[2] == 'Fevier' || $date_debut_exploded[2] == 'Fév' || $date_debut_exploded[2]=="févr") $date_debut_exploded[2] = "02";
        if ($date_debut_exploded[2] == 'mars' || $date_debut_exploded[2] == 'mars' || $date_debut_exploded[2] == 'mar') $date_debut_exploded[2] = "03";
        if ($date_debut_exploded[2] == 'avril' || $date_debut_exploded[2] == 'avr') $date_debut_exploded[2] = "04";
        if ($date_debut_exploded[2] == 'mai' || $date_debut_exploded[2] == 'mai') $date_debut_exploded[2] = "05";
        if ($date_debut_exploded[2] == 'juin' || $date_debut_exploded[2] == 'juin') $date_debut_exploded[2] = "06";
        if ($date_debut_exploded[2] == 'juillet' || $date_debut_exploded[2] == 'juil') $date_debut_exploded[2] = "07";
        if ($date_debut_exploded[2] == 'août' || $date_debut_exploded[2] == 'aout' || $date_debut_exploded[2] == 'Aug') $date_debut_exploded[2] = "08";
        if ($date_debut_exploded[2] == 'septembre' || $date_debut_exploded[2] == 'sept') $date_debut_exploded[2] = "09";
        if ($date_debut_exploded[2] == 'octobre' || $date_debut_exploded[2] == 'oct') $date_debut_exploded[2] = "10";
        if ($date_debut_exploded[2] == 'novembre' || $date_debut_exploded[2] == 'nov') $date_debut_exploded[2] = "11";
        if ($date_debut_exploded[2] == 'décembre' || $date_debut_exploded[2] == 'decembre' || $date_debut_exploded[2] == 'dec'  || $date_debut_exploded[2] == 'déc') $date_debut_exploded[2] = "12";
        $date_debut= str_replace(".","",$date_debut_exploded[3].'-'.$date_debut_exploded[2]."-".str_replace("1er","01",$date_debut_exploded["1"]));
        if (DateTime::createFromFormat($format,$date_debut ?? null)){
            return $date_debut;
        }else{
            if(preg_match("/Toute l'année/",$cleaned_date)){
                $date_debut = str_replace(".","",date('Y')."-01-01");
                if (DateTime::createFromFormat($format,$date_debut ?? null)){
                    return $date_debut;
                }
            }else{
                // "Du vendredi 5 au samedi 6 juin 2020 à 20h30";
                if ($date_debut_exploded[6] == 'janvier' || $date_debut_exploded[6]=="Janv" || $date_debut_exploded[6] == 'jan' || $date_debut_exploded[6]=='Jan') $date_debut_exploded[6] = "01";
                if ($date_debut_exploded[6] == 'février' || $date_debut_exploded[6] =="fév" || $date_debut_exploded[6] == 'Fevier' || $date_debut_exploded[6] == 'Fév' || $date_debut_exploded[6]=="févr") $date_debut_exploded[6] = "02";
                if ($date_debut_exploded[6] == 'mars' || $date_debut_exploded[6] == 'mars' || $date_debut_exploded[6] == 'mar') $date_debut_exploded[2] = "03";
                if ($date_debut_exploded[6] == 'avril' || $date_debut_exploded[6] == 'avr') $date_debut_exploded[6] = "04";
                if ($date_debut_exploded[6] == 'mai' || $date_debut_exploded[6] == 'mai') $date_debut_exploded[6] = "05";
                if ($date_debut_exploded[6] == 'juin' || $date_debut_exploded[6] == 'juin') $date_debut_exploded[6] = "06";
                if ($date_debut_exploded[6] == 'juillet' || $date_debut_exploded[6] == 'juil') $date_debut_exploded[6] = "07";
                if ($date_debut_exploded[6] == 'août' || $date_debut_exploded[6] == 'aout' || $date_debut_exploded[6] == 'Aug') $date_debut_exploded[6] = "08";
                if ($date_debut_exploded[6] == 'septembre' || $date_debut_exploded[6] == 'sept') $date_debut_exploded[6] = "09";
                if ($date_debut_exploded[6] == 'octobre' || $date_debut_exploded[6] == 'oct') $date_debut_exploded[6] = "10";
                if ($date_debut_exploded[6] == 'novembre' || $date_debut_exploded[6] == 'nov') $date_debut_exploded[6] = "11";
                if ($date_debut_exploded[6] == 'décembre' || $date_debut_exploded[6] == 'decembre' || $date_debut_exploded[6] == 'dec'  || $date_debut_exploded[6] == 'déc') $date_debut_exploded[6] = "12";
                $date_debut = $date_debut_exploded[7]."-".$date_debut_exploded[6]."-".$date_debut_exploded[2];
                return $date_debut;
            }
        }
    }
}
function convert_dracenie_date_debut_to_sql_date_fin($date){
    $part_date = $date[0]." ".$date[1]." ".$date[2]." ".$date[3];
    $cleaned_date = str_replace(".","",$part_date);
    $date_debut_exploded = explode(" ",$cleaned_date);
    $date_debut_brut = $date_debut_exploded[3];
    $date_debut = explode("/",$date_debut_brut)[2]."-".explode("/",$date_debut_brut)[1]."-".explode("/",$date_debut_brut)[0];
    return str_replace(",","",$date_debut);
}

function convert_nice_date_debut_to_sql_date_debut($date){
    $date_debut_brute = explode("au", $date)[0];
    $date_debut_exploded = explode(" ",$date_debut_brute)[2];
    $date_debut_unslashed = explode("/",$date_debut_exploded);
    $date_debut = date("Y")."-".$date_debut_unslashed[1]."-".$date_debut_unslashed[0];
    $format ="Y-m-d";
    if (DateTime::createFromFormat($format,$date_debut ?? null)){
//    return str_replace(",","",$date_debut);
        return str_replace(" ","",$date_debut);
    }else{
        $date_br =  explode(" ",$date[1]);
        $date_bbut_a = explode("/",$date_br[2]);
        $date_debut =  date("Y")."-".$date_bbut_a[1]."-".$date_bbut_a[0];
        return str_replace(" ","",$date_debut);
    }
}

function convert_nice_date_fin_to_sql_date_fin($date){
    $date_debut_brute = explode("au", $date)[1];
    $date_debut_unslashed = explode("/",$date_debut_brute);
    $date_debut = date("Y")."-".$date_debut_unslashed[1]."-".$date_debut_unslashed[0];
    return  str_replace(" ","",$date_debut);
}
function convert_alphabet_date_debut_to_sql_date_debut($dates){
    $date = explode(" ",$dates);
    if ($date[3] == 'janvier' || $date=="Janv" || $date[3] == 'jan' || $date=='Jan') $date[3] = "01";
    if ($date[3] == 'février' || $date[3] =="fév" || $date[3] == 'Fevier' || $date[3] == 'Fév' || $date=="févr") $date[3] = "02";
    if ($date[3] == 'mars' || $date[3] == 'mars' || $date[3] == 'mar') $date[3] = "03";
    if ($date[3] == 'avril' || $date[3] == 'avr') $date[3] = "04";
    if ($date[3] == 'mai' || $date[3] == 'mai') $date[3] = "05";
    if ($date[3] == 'juin' || $date[3] == 'juin') $date[3] = "06";
    if ($date[3] == 'juillet' || $date[3] == 'juil') $date[3] = "07";
    if ($date[3] == 'août' || $date[3] == 'aout' || $date[3] == 'Aug') $date[3] = "08";
    if ($date[3] == 'septembre' || $date[3] == 'sept') $date[3] = "09";
    if ($date[3] == 'octobre' || $date[3] == 'oct') $date[3] = "10";
    if ($date[3] == 'novembre' || $date[3] == 'nov') $date[3] = "11";
    if ($date[3] == 'décembre' || $date[3] == 'decembre' || $date[3] == 'dec'  || $date[3] == 'déc') $date[3] = "12";
    $date_debut= $date[4].'-'.$date[3]."-".$date["2"];
    return  $date_debut;
}
function convert_date_helice_to_sql_date_debut($date_br){
    $date = explode(" ",$date_br);
    if ($date[2] == 'janvier' || $date=="Janv" || $date[2] == 'jan' || $date=='Jan') $date[2] = "01";
    if ($date[2] == 'février' || $date[2] =="fév" || $date[2] == 'Fevier' || $date[2] == 'Fév' || $date=="févr") $date[2] = "02";
    if ($date[2] == 'mars' || $date[2] == 'mars' || $date[2] == 'mar') $date[2] = "03";
    if ($date[2] == 'avril' || $date[2] == 'avr') $date[2] = "04";
    if ($date[2] == 'mai' || $date[2] == 'mai') $date[2] = "05";
    if ($date[2] == 'juin' || $date[2] == 'juin') $date[2] = "06";
    if ($date[2] == 'juillet' || $date[2] == 'juil') $date[2] = "07";
    if ($date[2] == 'août' || $date[2] == 'aout' || $date[2] == 'Aug') $date[2] = "08";
    if ($date[2] == 'septembre' || $date[2] == 'sept') $date[2] = "09";
    if ($date[2] == 'octobre' || $date[2] == 'oct') $date[2] = "10";
    if ($date[2] == 'novembre' || $date[2] == 'nov') $date[2] = "11";
    if ($date[2] == 'décembre' || $date[2] == 'decembre' || $date[2] == 'dec'  || $date[2] == 'déc') $date[2] = "12";
    if(strlen($date["1"]) ==1){
        $date["1"] = "0".$date["1"];
    }
    $date_debut= $date[3].'-'.$date[2]."-".$date["1"];
    return  $date_debut;
}
function convert_visit_monaco_to_sql_date_debut($date_br){
    $date = explode(" ",$date_br);
    if ($date[6] == 'janvier' || $date=="Janv" || $date[6] == 'jan' || $date=='Jan') $date[6] = "01";
    if ($date[6] == 'février' || $date[6] =="fév" || $date[6] == 'Fevier' || $date[6] == 'Fév' || $date=="févr") $date[6] = "02";
    if ($date[6] == 'mars' || $date[6] == 'mars' || $date[6] == 'mar') $date[6] = "03";
    if ($date[6] == 'avril' || $date[6] == 'avr') $date[6] = "04";
    if ($date[6] == 'mai' || $date[6] == 'mai') $date[6] = "05";
    if ($date[6] == 'juin' || $date[6] == 'juin') $date[6] = "06";
    if ($date[6] == 'juillet' || $date[6] == 'juil') $date[6] = "07";
    if ($date[6] == 'août' || $date[6] == 'aout' || $date[6] == 'Aug') $date[6] = "08";
    if ($date[6] == 'septembre' || $date[6] == 'sept') $date[6] = "09";
    if ($date[6] == 'octobre' || $date[6] == 'oct') $date[6] = "10";
    if ($date[6] == 'novembre' || $date[6] == 'nov') $date[6] = "11";
    if ($date[6] == 'décembre' || $date[6] == 'decembre' || $date[6] == 'dec'  || $date[6] == 'déc') $date[6] = "12";

    if(strlen($date["2"]) ==1){
        $date["2"] = "0".$date["2"];
    }
    $date_debut= $date[7].'-'.$date[6]."-".$date["2"];
    $date_debut = str_replace(",","",$date_debut);
    $format ="Y-m-d";
    if (DateTime::createFromFormat($format,$date_debut ?? null)){
        return str_replace(",","",$date_debut);
    }else{
        $date = explode(" ",$date_br);
        if ($date[2] == 'janvier' || $date=="Janv" || $date[2] == 'jan' || $date=='Jan') $date[2] = "01";
        if ($date[2] == 'février' || $date[2] =="fév" || $date[2] == 'Fevier' || $date[2] == 'Fév' || $date=="févr") $date[2] = "02";
        if ($date[2] == 'mars' || $date[2] == 'mars' || $date[2] == 'mar') $date[2] = "03";
        if ($date[2] == 'avril' || $date[2] == 'avr') $date[2] = "04";
        if ($date[2] == 'mai' || $date[2] == 'mai') $date[2] = "05";
        if ($date[2] == 'juin' || $date[2] == 'juin') $date[2] = "06";
        if ($date[2] == 'juillet' || $date[2] == 'juil') $date[2] = "07";
        if ($date[2] == 'août' || $date[2] == 'aout' || $date[2] == 'Aug') $date[2] = "08";
        if ($date[2] == 'septembre' || $date[2] == 'sept') $date[2] = "09";
        if ($date[2] == 'octobre' || $date[2] == 'oct') $date[2] = "10";
        if ($date[2] == 'novembre' || $date[2] == 'nov') $date[2] = "11";
        if ($date[2] == 'décembre' || $date[2] == 'decembre' || $date[2] == 'dec'  || $date[2] == 'déc') $date[2] = "12";

        if(strlen($date["1"]) ==1){
            $date["1"] = "0".$date["1"];
        }
        $date_debut= $date[3].'-'.$date[2]."-".$date["1"];
        return  str_replace(",","",$date_debut);
    }
}
?>