<?php

function resize_file_to($image_path, $height, $width, $id, $class, $others = "", $overwrite = true)
{
    $Extension = end(explode(".",$image_path));
    $explodeVal = ".".$Extension;
    $arrPathElement = explode($explodeVal,basename($image_path));
    ini_set("memory_limit" , "128M");
    // Get the CodeIgniter super object
    $CI =& get_instance();

    // Path to image thumbnail
    //$image_thumb = dirname($image_path) . '/' . $arrPathElement[0] . $Extension;
    $image_thumb = dirname($image_path) . '/' . $arrPathElement[0] . "." . $Extension;

    if( ! file_exists($image_thumb))
    {
        // LOAD LIBRARY
        $CI->load->library('image_lib');

        // CONFIGURE IMAGE LIBRARY
        $config['image_library']    = 'gd2';
        $config['source_image']        = $image_path;
        $config['new_image']        = $image_thumb;
        $config['quality'] = 100;
        $config['maintain_ratio'] = TRUE;
        $config['height']            = $height;
        $config['width']            = $width;
        $CI->image_lib->initialize($config);
        $CI->image_lib->resize();
        $CI->image_lib->clear();
    }
    else
    {
        if($overwrite)
        {
            // LOAD LIBRARY
            $CI->load->library('image_lib');

            // CONFIGURE IMAGE LIBRARY
            $config['image_library']    = 'gd2';
            $config['source_image']        = $image_path;
            $config['new_image']        = $image_thumb;
            $config['quality'] = 100;
            $config['maintain_ratio'] = TRUE;
            $config['height']            = $height;
            $config['width']            = $width;
            $CI->image_lib->initialize($config);
            $CI->image_lib->resize();
            $CI->image_lib->clear();
        }
    }

}

function image_thumb($image_path, $height, $width, $id, $class, $others = "", $overwrite = false) {
	$Extension = end ( explode ( ".", $image_path ) );
	$explodeVal = "." . $Extension;
	$arrPathElement = explode ( $explodeVal, basename ( $image_path ) );
	ini_set ( "memory_limit", "128M" );
	// Get the CodeIgniter super object
	$CI = & get_instance ();
	$CI->load->library ( 'image_moo' );
	// Path to image thumbnail
	$image_thumb = dirname ( $image_path ) . '/' . $arrPathElement [0] . '_thumb_' . $height . '_' . $width . "." . $Extension;
	
	if (! file_exists ( $image_thumb )) {
		$CI->image_moo->load ( $image_path )->set_background_colour ( "#fff" )->resize ( $width, $height, TRUE )->save ( $image_thumb );
	} else {
		if ($overwrite) {
			$CI->image_moo->load ( $image_path )->set_background_colour ( "#fff" )->resize ( $width, $height, TRUE )->save ( $image_thumb, true );
		}
	}
	return '<img id="' . $id . '" class="' . $class . '" src="' . base_url () . $image_thumb . '" ' . $others . ' />';
}

/*
 function image_thumb($image_path, $height, $width, $id, $class, $others = "", $overwrite = false)
{
    $Extension = end(explode(".",$image_path));
    $explodeVal = ".".$Extension;
    $arrPathElement = explode($explodeVal,basename($image_path));
    ini_set("memory_limit" , "128M");
    // Get the CodeIgniter super object
    $CI =& get_instance();

    // Path to image thumbnail
    //$image_thumb = dirname($image_path) . '/' . $arrPathElement[0] . '_thumb_' . $height . '_' . $width . "." . $Extension;
    $image_thumb = dirname($image_path) . '/' . $arrPathElement[0] . '_thumb_100_100.'. $Extension;

    if( ! file_exists($image_thumb))
    {
        // LOAD LIBRARY
        $CI->load->library('image_lib');

        // CONFIGURE IMAGE LIBRARY
        $config['image_library']    = 'gd2';
        $config['source_image']        = $image_path;
        $config['new_image']        = $image_thumb;
        $config['quality'] = 100;
        $config['maintain_ratio'] = TRUE;
        $config['height']            = $height;
        $config['width']            = $width;
        $CI->image_lib->initialize($config);
        $CI->image_lib->resize();
        $CI->image_lib->clear();
    }
    else
    {
        if($overwrite)
        {
            // LOAD LIBRARY
            $CI->load->library('image_lib');

            // CONFIGURE IMAGE LIBRARY
            $config['image_library']    = 'gd2';
            $config['source_image']        = $image_path;
            $config['new_image']        = $image_thumb;
            $config['quality'] = 100;
            $config['maintain_ratio'] = TRUE;
            $config['height']            = $height;
            $config['width']            = $width;
            $CI->image_lib->initialize($config);
            $CI->image_lib->resize();
            $CI->image_lib->clear();
        }
    }

    //return '<img id="' . $id . '" class="' . $class . '" src="' . base_url() . '/' . $image_thumb . '" ' . $others . ' />';
    //return '<img id="' . $id . '" class="' . $class . '" src="' . base_url() . $image_thumb . '" ' . $others . ' />';
    return '<img id="' . $id . '" class="' . $class . '" src="' . $image_thumb . '" ' . $others . ' height="150" width="200" />';
}
*/

function generate_thumb($image_path, $height, $width, $overwrite = false)
{
    $Extension = end(explode(".",$image_path));
    $explodeVal = ".".$Extension;
    $arrPathElement = explode($explodeVal,basename($image_path));
    ini_set("memory_limit" , "128M");
    // Get the CodeIgniter super object
    $CI =& get_instance();

    // Path to image thumbnail
    $image_thumb = dirname($image_path) . '/' . $arrPathElement[0] . '_thumb_' . $height . '_' . $width . "." . $Extension;

    if( ! file_exists($image_thumb))
    {
        // LOAD LIBRARY
        $CI->load->library('image_lib');

        // CONFIGURE IMAGE LIBRARY
        $config['image_library']    = 'gd2';
        $config['source_image']        = $image_path;
        $config['new_image']        = $image_thumb;
        $config['quality'] = 100;
        $config['maintain_ratio'] = TRUE;
        $config['height']            = $height;
        $config['width']            = $width;
        $CI->image_lib->initialize($config);
        $CI->image_lib->resize();
        $CI->image_lib->clear();
    }
    else
    {
        if($overwrite)
        {
            // LOAD LIBRARY
            $CI->load->library('image_lib');

            // CONFIGURE IMAGE LIBRARY
            $config['image_library']    = 'gd2';
            $config['source_image']        = $image_path;
            $config['new_image']        = $image_thumb;
            $config['quality'] = 100;
            $config['maintain_ratio'] = TRUE;
            $config['height']            = $height;
            $config['width']            = $width;
            $CI->image_lib->initialize($config);
            $CI->image_lib->resize();
            $CI->image_lib->clear();
        }
    }

    return $image_thumb;
}


function generate_mobile_bg($image_path, $height, $width, $id, $class, $others = "", $overwrite = false)
{
    $Extension = end(explode(".",$image_path));
    $explodeVal = ".".$Extension;
    $arrPathElement = basename($image_path);
    ini_set("memory_limit" , "128M");
    // Get the CodeIgniter super object
    $CI =& get_instance();

    // Path to image thumbnail
    $image_thumb = dirname($image_path) . '/mobile_' . $arrPathElement;

    if( ! file_exists($image_thumb))
    {
        // LOAD LIBRARY
        $CI->load->library('image_lib');

        // CONFIGURE IMAGE LIBRARY
        $config['image_library']    = 'gd2';
        $config['source_image']        = $image_path;
        $config['new_image']        = $image_thumb;
        $config['quality'] = 100;
        $config['maintain_ratio'] = TRUE;
        $config['width']            = $width;
        $config['height']            = $height;
        $CI->image_lib->initialize($config);
        $CI->image_lib->resize();
        $CI->image_lib->clear();
    }
    else
    {
        if($overwrite)
        {
            // LOAD LIBRARY
            $CI->load->library('image_lib');

            // CONFIGURE IMAGE LIBRARY
            $config['image_library']    = 'gd2';
            $config['source_image']        = $image_path;
            $config['new_image']        = $image_thumb;
            $config['quality'] = 100;
            $config['maintain_ratio'] = TRUE;
            $config['height']            = $height;
            $config['width']            = $width;
            $CI->image_lib->initialize($config);
            $CI->image_lib->resize();
            $CI->image_lib->clear();
        }
    }

    //return '<img id="' . $id . '" class="' . $class . '" src="' . base_url() . '/' . $image_thumb . '" ' . $others . ' />';
    return '<img id="' . $id . '" class="' . $class . '" src="' . base_url() . $image_thumb . '" ' . $others . ' />';
}

/**
 * Return image use in a background-image link
 *
 * @param unknown_type $image_path
 * @param unknown_type $height
 * @param unknown_type $width
 * @param unknown_type $overwrite
 * @return unknown
 */
function GetBackgroungImage($image_path, $height, $width, $overwrite = false) {
	$Extension = end(explode(".",$image_path));
    $explodeVal = ".".$Extension;
    $arrPathElement = explode($explodeVal,basename($image_path));
    ini_set("memory_limit" , "128M");
    // Get the CodeIgniter super object
    $CI =& get_instance();

    // Path to image thumbnail
    $image_thumb = dirname($image_path) . '/' . $arrPathElement[0] . '_thumb_' . $height . '_' . $width . "." . $Extension;

    if( ! file_exists($image_thumb))
    {
        // LOAD LIBRARY
        $CI->load->library('image_lib');

        // CONFIGURE IMAGE LIBRARY
        $config['image_library']    = 'gd2';
        $config['source_image']        = $image_path;
        $config['new_image']        = $image_thumb;
        $config['quality'] = 100;
        $config['maintain_ratio'] = TRUE;
        $config['height']            = $height;
        $config['width']            = $width;
        $CI->image_lib->initialize($config);
        $CI->image_lib->resize();
        $CI->image_lib->clear();
    }
    else
    {
        if($overwrite)
        {
            // LOAD LIBRARY
            $CI->load->library('image_lib');

            // CONFIGURE IMAGE LIBRARY
            $config['image_library']    = 'gd2';
            $config['source_image']        = $image_path;
            $config['new_image']        = $image_thumb;
            $config['quality'] = 100;
            $config['maintain_ratio'] = TRUE;
            $config['height']            = $height;
            $config['width']            = $width;
            $CI->image_lib->initialize($config);
            $CI->image_lib->resize();
            $CI->image_lib->clear();
        }
    }

    return base_url() . '/' . $image_thumb;
}

/* End of file image_helper.php */
/* Location: ./application/helpers/image_helper.php */