<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('css_url'))
{
	function css_url($nom)
	{
		return base_url() . 'assets/css/' . $nom . '.css';
	}
}

if ( ! function_exists('js_url'))
{
	function js_url($nom)
	{
		return base_url() . 'assets/js/' . $nom . '.js';
	}
}

if ( ! function_exists('img_url'))
{
	function img_url($nom)
	{
		return base_url() . 'assets/img/' . $nom;
	}
}

if ( ! function_exists('avatar_url'))
{
	function avatar_url($nom)
	{
		return base_url() . 'assets/avatars/' . $nom;
	}
}

if ( ! function_exists('thumb_avatar_url'))
{
	function thumb_avatar_url($nom)
	{
		return base_url() . 'assets/avatars/thumbs/' . $nom;
	}
}

if ( ! function_exists('illustr_url'))
{
	function illustr_url($nom)
	{
		return base_url() . 'assets/illustrations/' . $nom;
	}
}

if ( ! function_exists('thumb_illustr_url'))
{
	function thumb_illustr_url($nom)
	{
		return base_url() . 'assets/illustrations/thumbs/' . $nom;
	}
}


if ( ! function_exists('assets_url'))
{
	function assets_url($nom)
	{
		return base_url() . 'assets/' . $nom;
	}
}

if ( ! function_exists('img_qrcode'))
{
	function img_qrcode($nom)
	{
		return base_url() . 'application/resources/front/images/cards/qrcode_'. $nom.'.png';
	}
}


if ( ! function_exists('img_qrcode_path'))
{
	function img_qrcode_path($nom)
	{
		$base_path_system = str_replace('system/', '', BASEPATH);
		return $base_path_system . 'application/resources/front/images/cards/qrcode_'. $nom.'.png';
	}
}

