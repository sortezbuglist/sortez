<?php $data['empty'] = NULL; ?>
<?php $this->load->view('export/includes/header',$data); ?>
    <style type="text/css">
        <!--
        .btn_link_rose, .btn_link_rose:hover, .btn_link_rose:focus {
            color: #FFFFFF;
            text-decoration: none;
            background-color: #000000;
            margin-top: 10px;
            margin-bottom: 10px;
            padding-top: 10px;
            padding-right: 20px;
            padding-bottom: 10px;
            padding-left: 20px;
            border-radius: 25px;
        }

        body {
            padding-top: 0 !important;
        <?php if (isset($zCouleur)) { ?>
            background-color: #<?php echo $zCouleur;?>;
        <?php } else { ?>
            background-color: #ffffff;
        <?php } ?>

            <?php if (isset($zCouleurTextBouton)) { ?>color: #<?php echo $zCouleurTextBouton;?>; <?php } else { ?>color: #000000;<?php } ?>
        }

        .title_categ_black {
            height: 40px;
            width: 100%;
            text-align: center;
            font-family: arial;
            font-size: 18.7px;
            font-style: normal;
            font-variant: normal;
            font-weight: normal;
            line-height: 23px;
            text-decoration: none;
            padding-top: 9px;
            margin-bottom: 0px;
            margin-top: 0px;
            text-transform: uppercase;
            <?php if (isset($zCouleurBgBouton)) { ?>background-color: #<?php echo $zCouleurBgBouton;?>; <?php } else { ?>background-color: #333;<?php } ?>;
            <?php if (isset($zCouleurNbBtn) && intval($zCouleurNbBtn)==1) { ?>color: #ffffff; <?php } else { ?>color: #000000;<?php } ?>;
        }

        .container_slide_article > img {
            max-width: 100% !important;
        }

        .contact_info_article_list, .share_btn_article_list {
            list-style-type: none;
            padding: 0;
        }

        .contact_info_article_list li, .share_btn_article_list li {
            display: inline;
            padding: 0 5px;
        }

        -->
    </style>

    <style type="text/css">
        <!--
        .btn_link_rose, .btn_link_rose:hover, .btn_link_rose:focus {
            color: #FFFFFF;
            text-decoration: none;
            background-color: #000000;
            margin-top: 10px;
            margin-bottom: 10px;
            padding-top: 10px;
            padding-right: 20px;
            padding-bottom: 10px;
            padding-left: 20px;
            border-radius: 25px;
        }

        body {
            padding-top: 0 !important;
        }

        .container_slide_article > img {
            max-width: 100% !important;
        }

        .contact_info_article_list, .share_btn_article_list {
            list-style-type: none;
            padding: 0;
        }

        .contact_info_article_list li, .share_btn_article_list li {
            display: inline;
            padding: 0 5px;
        }

        .detail_agenda_title_container {
            text-align: center;
            padding-bottom: 10px;
            margin-top: 20px;
            margin-bottom: 10px;
            height: 100px;
            display: table;
            width: 100%;
        }

        .oDetailAgenda_subcateg {
            background-color: transparent;
            font-family: "Arial", sans-serif;
            font-size: 19px;
            font-style: normal;
            font-variant: normal;
            font-weight: 700;
            line-height: 24px;
            text-align: center;
            vertical-align: 0;
        }

        .oDetailAgenda_nom_manifestation {
            background-color: transparent;
            /*color: #e553b0;*/
            <?php if (isset($zCouleurTitre)) { ?>color: #<?php echo $zCouleurTitre;?>; <?php } else { ?>color: #3b579d;<?php } ?>
            font-family: "Arial", sans-serif;
            font-size: 22px;
            font-style: normal;
            font-variant: normal;
            font-weight: 700;
            line-height: 30.7px;
            margin-bottom: 6.7px;
            text-align: center;
            vertical-align: 0;
        }

        .oDetailAgenda_date_debut {
            background-color: transparent;
            font-family: "Arial", sans-serif;
            font-size: 18px;
            font-style: normal;
            font-variant: normal;
            font-weight: normal;
            line-height: 24px;
            text-align: center;
            vertical-align: 0;
        }

        .oDetailAgenda_nom_localisation {
            background-color: transparent;
            font-family: "Arial", sans-serif;
            font-size: 18px;
            font-style: normal;
            font-variant: normal;
            font-weight: normal;
            line-height: 24px;
            text-align: center;
            vertical-align: 0;
        }

        .oDetailAgenda_telephone {
            background-color: transparent;
            font-family: "Arial", sans-serif;
            font-size: 18px;
            font-style: normal;
            font-variant: normal;
            font-weight: normal;
            line-height: 30px;
            text-align: center;
            vertical-align: 0;
        }

        .btn_custom_export_bg,
        .btn_custom_export_bg:hover,
        .btn_custom_export_bg:focus,
        .btn_custom_export_bg:active
        {
            <?php if (isset($zCouleurNbBtn) && intval($zCouleurNbBtn)==1) { ?>color: #ffffff; <?php } else { ?>color: #000000;<?php } ?>;
        }
        div {
            display: block;
        }

        -->
    </style>


    <div style="display: table;width: 100%;
    <?php if (isset($zCouleur)) { ?>
            background-color: #<?php echo $zCouleur;?>;
    <?php } else { ?>
            background-color: #ffffff;
    <?php } ?>
            padding-top: 40px;">

        <div style="padding-right:5px; display:table; width:100%;
        <?php if (isset($zCouleur)) { ?>
                background-color: #<?php echo $zCouleur;?>;
        <?php } else { ?>
                background-color: #ffffff;
        <?php } ?>
                padding-top:20px;">

            <div class="col-lg-12 padding0" style="text-align:center; padding-bottom:20px; display: table; width: 100%;">
                <?php $this->load->view("agenda/includes/slide_details_agenda", $data); ?>
            </div>

            <div class="col-xs-12 padding0" style="text-align:center; min-height:40px; display: table; width: 100%;"><a
                        href="javascript:history.back();" class="btn btn_custom_export_bg">Retour &agrave; la liste des festivals</a>
            </div>


            <script type="text/javascript">
                $(document).ready(function () {
                    $("#contact_recommandation_nom").focusin(function () {
                        if ($(this).val() == "Votre nom *") $(this).val('');
                    });
                    $("#contact_recommandation_nom").focusout(function () {
                        if ($(this).val() == "") $(this).val('Votre nom *');
                    });

                    $("#contact_recommandation_mail").focusin(function () {
                        if ($(this).val() == "Votre courriel *") $(this).val('');
                    });
                    $("#contact_recommandation_mail").focusout(function () {
                        if ($(this).val() == "") $(this).val('Votre courriel *');
                    });

                    $("#contact_recommandation_mail_ami").focusin(function () {
                        if ($(this).val() == "Courriel de votre ami *") $(this).val('');
                    });
                    $("#contact_recommandation_mail_ami").focusout(function () {
                        if ($(this).val() == "") $(this).val('Courriel de votre ami *');
                    });

                    $("#contact_recommandation_msg").focusin(function () {
                        if ($(this).val() == "Votre message *") $(this).val('');
                    });
                    $("#contact_recommandation_msg").focusout(function () {
                        if ($(this).val() == "") $(this).val('Votre message *');
                    });

                    $("#contact_recommandation_reset").click(function () {
                        $("#contact_recommandation_nom").val('Votre nom *');
                        $("#contact_recommandation_mail_ami").val('Courriel de votre ami *');
                        $("#contact_recommandation_mail").val('Votre courriel *');
                        $("#contact_recommandation_msg").val('Votre message *');
                        $("#spanContactPrivicarteRecommandationForm").html('* champs obligatoires');
                    });

                    $("#contact_recommandation_send").click(function () {
                        $("#spanContactPrivicarteRecommandationForm").html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
                        var error = 0;
                        var contact_recommandation_msg = $("#contact_recommandation_msg").val();
                        if (contact_recommandation_msg == '' || contact_recommandation_msg == 'Votre message *') error = 1;
                        var contact_recommandation_nom = $("#contact_recommandation_nom").val();
                        if (contact_recommandation_nom == '' || contact_recommandation_nom == 'Votre nom *') error = 1;
                        var contact_recommandation_mail_ami = $("#contact_recommandation_mail_ami").val();
                        if (contact_recommandation_mail_ami == '' || contact_recommandation_mail_ami == 'Courriel de votre ami *') error = 1;
                        else if (!validateEmail(contact_recommandation_mail_ami)) error = 3;
                        var contact_recommandation_mail = $("#contact_recommandation_mail").val();
                        if (contact_recommandation_mail == '' || contact_recommandation_mail == 'Votre courriel *') error = 1;
                        else if (!validateEmail(contact_recommandation_mail)) error = 2;


                        if (error == 1) {
                            $("#spanContactPrivicarteRecommandationForm").html('<span style="color:#FF0000;">* Les champs sont obligatoires</span>');
                        } else if (error == 2) {
                            $("#spanContactPrivicarteRecommandationForm").html('<span style="color:#FF0000;">* Adresse email invalide</span>');
                            $("#contact_recommandation_mail").css('border-color', '#ff0000');
                            //alert("invalide mail");
                        } else if (error == 3) {
                            $("#spanContactPrivicarteRecommandationForm").html('<span style="color:#FF0000;">* Adresse email invalide</span>');
                            $("#contact_recommandation_mail_ami").css('border-color', '#ff0000');
                        } else {
                            $.post(
                                "<?php echo site_url("front/professionnels/contact_partner_sendmail/");?>",
                                {
                                    contact_recommandation_nom: contact_recommandation_nom,
                                    contact_recommandation_tel: '',
                                    contact_recommandation_mail: contact_recommandation_mail,
                                    contact_recommandation_msg: contact_recommandation_msg,
                                    contact_recommandation_mailto: contact_recommandation_mail_ami
                                },
                                function (data) {
                                    $("#spanContactPrivicarteRecommandationForm").html(data);
                                });
                        }

                        //alert(error);
                    });


                    $("#contact_partner_nom").focusin(function () {
                        if ($(this).val() == "Votre nom *") $(this).val('');
                    });
                    $("#contact_partner_nom").focusout(function () {
                        if ($(this).val() == "") $(this).val('Votre nom *');
                    });

                    $("#contact_partner_tel").focusin(function () {
                        if ($(this).val() == "Votre numéro de téléphone *") $(this).val('');
                    });
                    $("#contact_partner_tel").focusout(function () {
                        if ($(this).val() == "") $(this).val('Votre numéro de téléphone *');
                    });

                    $("#contact_partner_mail").focusin(function () {
                        if ($(this).val() == "Votre courriel *") $(this).val('');
                    });
                    $("#contact_partner_mail").focusout(function () {
                        if ($(this).val() == "") $(this).val('Votre courriel *');
                    });

                    $("#contact_partner_msg").focusin(function () {
                        if ($(this).val() == "Votre message *") $(this).val('');
                    });
                    $("#contact_partner_msg").focusout(function () {
                        if ($(this).val() == "") $(this).val('Votre message *');
                    });

                    $("#contact_partner_reset").click(function () {
                        $("#contact_partner_nom").val('Votre nom *');
                        $("#contact_partner_tel").val('Votre numéro de téléphone *');
                        $("#contact_partner_mail").val('Votre courriel *');
                        $("#contact_partner_msg").val('Votre message *');
                        $("#spanContactPartnerForm").html('* champs obligatoires');
                    });

                    $("#contact_partner_send").click(function () {
                        var error = 0;
                        var contact_partner_nom = $("#contact_partner_nom").val();
                        if (contact_partner_nom == '' || contact_partner_nom == 'Votre nom *') error = 1;
                        var contact_partner_tel = $("#contact_partner_tel").val();
                        if (contact_partner_tel == '' || contact_partner_tel == 'Votre numéro de téléphone *') error = 1;
                        var contact_partner_mail = $("#contact_partner_mail").val();
                        if (contact_partner_mail == '' || contact_partner_mail == 'Votre courriel *') error = 1;
                        if (!validateEmail(contact_partner_mail)) error = 2;
                        var contact_partner_msg = $("#contact_partner_msg").val();
                        if (contact_partner_msg == '' || contact_partner_msg == 'Votre message *') error = 1;
                        $("#spanContactPartnerForm").html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

                        if (error == 1) {
                            $("#spanContactPartnerForm").html('<span style="color:#FF0000;">* Les champs sont obligatoires</span>');
                        } else if (error == 2) {
                            $("#spanContactPartnerForm").html('<span style="color:#FF0000;">* Adresse email invalide</span>');
                            $("#contact_partner_mail").css('border-color', '#ff0000');
                        } else {
                            $.post(
                                "<?php echo site_url("front/professionnels/contact_partner_sendmail/");?>",
                                {
                                    contact_partner_nom: contact_partner_nom,
                                    contact_partner_tel: contact_partner_tel,
                                    contact_partner_mail: contact_partner_mail,
                                    contact_partner_msg: contact_partner_msg,
                                    contact_partner_mailto: "<?php echo $oDetailAgenda->email; ?>"
                                },
                                function (data) {
                                    $("#spanContactPartnerForm").html(data);
                                });
                        }
                    });


                    $("#IdVideoPartnerAgenda").fancybox({
                        autoScale: false,
                        overlayOpacity: 0.8, // Set opacity to 0.8
                        overlayColor: "#000000", // Set color to Black
                        padding: 5,
                        width: 520,
                        height: 800,
                        transitionIn: 'elastic',
                        transitionOut: 'elastic'
                    });
                    $("#IdContactPartnerForm").fancybox({
                        autoScale: false,
                        overlayOpacity: 0.8, // Set opacity to 0.8
                        overlayColor: "#000000", // Set color to Black
                        padding: 5,
                        width: 520,
                        height: 800,
                        transitionIn: 'elastic',
                        transitionOut: 'elastic'
                    });
                    $("#IdRecommandationPartnerForm").fancybox({
                        'autoScale': false,
                        'overlayOpacity': 0.8, // Set opacity to 0.8
                        'overlayColor': "#000000", // Set color to Black
                        'padding': 5,
                        'width': 520,
                        'height': 410,
                        'transitionIn': 'elastic',
                        'transitionOut': 'elastic'
                    });
                    $("#addthis_button_pvc").fancybox({
                        'autoScale': false,
                        'overlayOpacity': 0.8, // Set opacity to 0.8
                        'overlayColor': "#000000", // Set color to Black
                        'padding': 5,
                        'width': 800,
                        'height': 410,
                        'transitionIn': 'elastic',
                        'transitionOut': 'elastic',
                        'type': 'iframe'
                    });
                    $("#idFacebookProFormAgenda").fancybox({
                        autoScale: false,
                        overlayOpacity: 0.8, // Set opacity to 0.8
                        overlayColor: "#000000", // Set color to Black
                        padding: 5,
                        width: 520,
                        height: 800,
                        transitionIn: 'elastic',
                        transitionOut: 'elastic',
                        type: 'iframe'
                    });
                });
            </script>

            <!--Video content-->
            <div id="divVideoPartnerAgenda" style="display:none; background-color:#000000;">
                <table width="100%" id="tableContactPartnerForm" border="0" cellspacing="0" cellpadding="0"
                       style="text-align:center; width:540px; height:472px; background-color:#000000;">
                    <tr>
                        <td>
                            <?php
                            $link_video_club_agenda = preg_split('[v=]', $oDetailAgenda->video);
                            if (isset($link_video_club_agenda[1])) {
                                ?>
                                <object width="540" height="472">
                                    <param
                                            value="http://www.youtube.com/v/<?php echo $link_video_club_agenda[1]; ?>&autoplay=1&loop=1&showinfo=0&rel=0&fs=1&hd=1"
                                            name="movie">
                                    <param value="true" name="allowFullScreen">
                                    <param value="always" name="allowscriptaccess">
                                    <param value="transparent" name="wmode">
                                    <embed width="540" height="472" allowfullscreen="true" allowscriptaccess="always"
                                           wmode="transparent" type="application/x-shockwave-flash"
                                           style="width:540px;height:472px;"
                                           src="http://www.youtube.com/v/<?php echo $link_video_club_agenda[1]; ?>&autoplay=0&loop=1&showinfo=0&rel=0&fs=1&hd=1">
                                </object>
                            <?php } ?>
                        </td>
                    </tr>
                </table>
            </div>


            <!--Contact form contet-->
            <div id="divContactPartnerForm" style="display:none; background-color:#FFFFFF;">
                <form name="formContactPartnerForm" id="formContactPartnerForm" action="#">
                    <table width="100%" id="tableContactPartnerForm" border="0" cellspacing="0" cellpadding="0"
                           style="text-align:center; width:500px; height:400px;">
                        <tr>
                            <td>
                                <div style="font-family:arial; font-size:24px; font-weight:bold;">Nous Contacter</div>
                            </td>
                        </tr>
                        <tr>
                            <td><input type="text" name="contact_partner_nom" id="contact_partner_nom"
                                       value="Votre nom *"/></td>
                        </tr>
                        <tr>
                            <td><input type="text" name="contact_partner_tel" id="contact_partner_tel"
                                       value="Votre numéro de téléphone *"/></td>
                        </tr>
                        <tr>
                            <td><input type="text" name="contact_partner_mail" id="contact_partner_mail"
                                       value="Votre courriel *"/></td>
                        </tr>
                        <tr>
                            <td><textarea name="contact_partner_msg" id="contact_partner_msg">Votre message *</textarea>
                            </td>
                        </tr>
                        <tr>
                            <td><span id="spanContactPartnerForm">* champs obligatoires</span></td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td><input type="button" class="btn btn-default" name="contact_partner_reset"
                                                   id="contact_partner_reset" value="Retablir"/></td>
                                        <td><input type="button" class="btn btn-default" name="contact_partner_send"
                                                   id="contact_partner_send" value="Envoyer"/></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>

            <!--Recommandation form contet-->
            <div id="divContactRecommandationForm"
                 style="display:none; background-color:#FFFFFF; width:500px; height:400px;">
                <form name="formContactPrivicarteForm" id="formContactPrivicarteForm" action="#">
                    <table width="100%" id="tableContactPartnerForm" border="0" cellspacing="0" cellpadding="0"
                           style="text-align:center; width:500px; height:400px;">
                        <tr>
                            <td>
                                <div style="font-family:arial; font-size:24px; font-weight:bold;">Recommander &agrave;
                                    un ami
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><input type="text" name="contact_recommandation_nom" id="contact_recommandation_nom"
                                       value="Votre nom *"/></td>
                        </tr>
                        <tr>
                            <td><input type="text" name="contact_recommandation_mail_ami"
                                       id="contact_recommandation_mail_ami" value="Courriel de votre ami *"/></td>
                        </tr>
                        <tr>
                            <td><input type="text" name="contact_recommandation_mail" id="contact_recommandation_mail"
                                       value="Votre courriel *"/></td>
                        </tr>
                        <tr>
                            <td><textarea name="contact_recommandation_msg" id="contact_recommandation_msg">Votre message *</textarea>
                            </td>
                        </tr>
                        <tr>
                            <td><span id="spanContactPrivicarteRecommandationForm">* champs obligatoires</span></td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td><input type="button" class="btn btn-default"
                                                   name="contact_recommandation_reset" id="contact_recommandation_reset"
                                                   value="Retablir"/></td>
                                        <td><input type="button" class="btn btn-default"
                                                   name="contact_recommandation_send" id="contact_recommandation_send"
                                                   value="Envoyer"/></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>


            <?php
            //LOCALDATA FILTRE
            $this_session_localdata =& get_instance();
            $this_session_localdata->load->library('session');
            $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
            $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
            //var_dump($oDetailAgenda);
            ?>



            <?php if (isset($oDetailAgenda->siteweb) && $oDetailAgenda->siteweb != "") $siteweb_agenda = $oDetailAgenda->siteweb; else $siteweb_agenda = "javascript:void(0);"; ?>


            <div class="col-lg-12 padding0" style="display: table; width: 100%;">

                <div class="col-lg-12 padding0">
                    <div class="detail_agenda_title_container">
        <span class="oDetailAgenda_subcateg"><?php echo $oDetailAgenda->subcateg; ?>
        </span><br/>
                        <span
                                class="oDetailAgenda_nom_manifestation"><?php echo $oDetailAgenda->nom_manifestation; ?></span><br/>

    <span class="oDetailAgenda_date_debut">
        <?php if (isset($toFestival_datetime) && count($toFestival_datetime) > 0) { ?>
            <?php foreach ($toFestival_datetime as $objFestival_datetime) { ?>
                <?php
                if (isset($objFestival_datetime->date_debut) && $objFestival_datetime->date_debut != "0000-00-00" && ($objFestival_datetime->date_debut == $objFestival_datetime->date_fin)) {
                    echo "<br/>Le " . translate_date_to_fr($objFestival_datetime->date_debut);
                    if (isset($objFestival_datetime->heure_debut) && $objFestival_datetime->heure_debut != "0:00") echo " à " . str_replace(":", "h", $objFestival_datetime->heure_debut);
                } else {
                    if (isset($objFestival_datetime->date_debut) && $objFestival_datetime->date_debut != "0000-00-00") echo "<br/>Du " . translate_date_to_fr($objFestival_datetime->date_debut);
                    if (isset($objFestival_datetime->date_fin) && $objFestival_datetime->date_fin != "0000-00-00") {
                        if (isset($objFestival_datetime->date_debut) && $objFestival_datetime->date_debut != "0000-00-00") echo " au " . translate_date_to_fr($objFestival_datetime->date_fin);
                        else echo " Jusqu'au " . translate_date_to_fr($objFestival_datetime->date_fin);
                    }
                    if (isset($objFestival_datetime->heure_debut) && $objFestival_datetime->heure_debut != "0:00") echo " à " . str_replace(":", "h", $objFestival_datetime->heure_debut);
                }
                ?>
            <?php } ?>
        <?php } ?>

        </span><br/>

                        <span class="oDetailAgenda_nom_localisation">
    <?php
    if (isset($oDetailAgenda->location_id) && $oDetailAgenda->location_id != "0") {
        $obj_location_festival_details = $this->mdl_localisation->getById($oDetailAgenda->location_id);
        if (isset($obj_location_festival_details) && is_object($obj_location_festival_details)) {
            echo $obj_location_festival_details->location . "<br/>" . $obj_location_festival_details->location_address . " - ";
            $obj_ville_location_details = $this->mdlville->getVilleById($obj_location_festival_details->location_villeid);
            if (isset($obj_ville_location_details->Nom) && is_object($obj_ville_location_details)) {
                echo $obj_ville_location_details->CodePostal . " - " . $obj_ville_location_details->Nom;
            }
        }
    }
    ?>
    </span>

                        <?php
                        $this->load->model("mdlcommercant");
                        $oCommercant = $this->mdlcommercant->infoCommercant($oDetailAgenda->IdCommercant);
                        ?>

                        <span class="oDetailAgenda_telephone">
	<?php if ($oDetailAgenda->telephone != "") echo "<br/>Tél. " . $oDetailAgenda->telephone; ?>
    <?php if ($oDetailAgenda->mobile != "") echo "<br/>Mobile. " . $oDetailAgenda->mobile; ?>
    <?php if ($oDetailAgenda->fax != "") echo "<br/>Fax. " . $oDetailAgenda->fax; ?>
    </span>
                    </div>
                </div>

            </div>

        </div>


        <div class="container" style="display: block;">
            <div class="col-lg-12 padding0" style="text-align:justify; display: block;"><?php echo $oDetailAgenda->description; ?></div>

            <div class="col-lg-12" style="text-align:center; padding:15px 0 0; display: block;">
                <!-- Go to www.addthis.com/dashboard to customize your tools --> <div class="addthis_inline_share_toolbox"></div>
                <ul class="share_btn_article_list" style="display: none;">

                    <li>
                        <a href="javascript:void(0);"
                           onclick='javascript:window.open("https://www.facebook.com/sharer/sharer.php?u=<?php echo site_url('front/festival/details_event/' . $oDetailAgenda->id); ?>", "FacebookPrivicarteForm_share", "width=500, height=800");'
                           title="Partage Facebook"><img
                                    src="<?php echo GetImagePath("privicarte/"); ?>/article_fb.png"/></a>
                    </li>

                    <li>
                        <a href="javascript:void(0);"
                           onclick='javascript:window.open("https://twitter.com/home?status=<?php echo site_url('front/festival/details_event/' . $oDetailAgenda->id); ?>", "TwitterPrivicarteForm_share", "width=500, height=800");'
                           title="Partage Twitter"><img
                                    src="<?php echo GetImagePath("privicarte/"); ?>/article_twitter.png"/></a>
                    </li>

                    <li>
                        <a href="javascript:window.print()" title="Imprimer"><img
                                    src="<?php echo GetImagePath("privicarte/"); ?>/article_print.png"/></a>
                    </li>

                    <li>
                        <?php
                        if ((isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0" && $localdata_IdVille != NULL) || (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0" && $localdata_IdDepartement != NULL)) {
                            ?>
                            <a href="javascript:void(0);"
                               onclick='javascript:window.open("<?php echo site_url("article/details_article_contact/" . $oDetailAgenda->id . "/recommandation"); ?>", "<?php echo $oDetailAgenda->nom_manifestation; ?>", "width=500, height=400");'
                               title="Page Recommandation"><img
                                        src="<?php echo GetImagePath("privicarte/"); ?>/article_recomand.png"/></a>
                        <?php } else { ?>
                            <a href="#divContactRecommandationForm" id="IdRecommandationPartnerForm"
                               title="Recommandation"><img
                                        src="<?php echo GetImagePath("privicarte/"); ?>/article_recomand.png"/></a>
                        <?php } ?>
                    </li>


                </ul>
            </div>

            <div class="col-lg-12" style="text-align: center; padding:15px; display: block;">Festival d&eacute;pos&eacute;
                le <?php echo translate_date_to_fr($oDetailAgenda->date_depot); ?>
                par <?php echo $oInfoCommercant->NomSociete; ?></div>


        </div>


        <?php if (
            (isset($oDetailAgenda->description_tarif) && $oDetailAgenda->description_tarif != "") ||
            (isset($oDetailAgenda->conditions_promo) && $oDetailAgenda->conditions_promo != "") ||
            (isset($oDetailAgenda->reservation_enligne) && $oDetailAgenda->reservation_enligne != "")
        ) { ?>

            <div class="col-lg-12 title_categ_black" style="margin-bottom:0; display: block;">TARIF ET LIEN DE R&Eacute;SERVATION EN
                LIGNE
            </div>

            <div class="col-lg-12" style="padding:15px 0; background-color:#FFFFFF; display: block;">
                <div style="display: block;">
                    <div style='background-color: transparent;
        color: #000000;
        font-family: "Arial",sans-serif;
        font-size: 18.7px;
        font-style: normal;
        font-variant: normal;
        font-weight: 700;
        line-height: 23px;
        text-decoration: none; text-align:justify;
        vertical-align: 0;'><?php echo $oDetailAgenda->description_tarif; ?></div>
                    <div><?php echo $oDetailAgenda->conditions_promo; ?></div>
                </div>
                <div style="text-align:center; display: block;">
                    <?php
                    if ($oDetailAgenda->reservation_enligne != "") {
                        $reservation_enligne_agenda = $oDetailAgenda->reservation_enligne;
                        ?><a href="<?php echo $reservation_enligne_agenda; ?>" target="_blank"><img
                                src="<?php echo GetImagePath("privicarte/"); ?>/reservation_online.png" alt="reservation"/>
                        </a><?php
                    }
                    ?>
                </div>
            </div>

        <?php } ?>


        <div class="col-lg-12 title_categ_black proximite_conteneur_list" style="margin-top:0; margin-bottom: 0; display: table; width: 100%;">CONTACT & INFORMATIONS</div>
        <div class="col-xs-12 padding0">

        <?php
        if (isset($oDetailAgenda->organiser_id) && $oDetailAgenda->organiser_id != "0") {
            $obj_organiser_festival_details = $this->mdl_festival_organiser->getById($oDetailAgenda->organiser_id);
            if (isset($obj_organiser_festival_details) && is_object($obj_organiser_festival_details)) {
                ?>
                <div style="border: 1px solid; margin: 0 auto; text-align: center; width: 500px; display: table; margin-top: 15px; padding: 15px;">Ev&eacute;nement
                    organis&eacute; par
                    <?php
                    echo $obj_organiser_festival_details->name . "<br/>";
                    echo $obj_organiser_festival_details->postal_code . " - ";
                    if (isset($obj_organiser_festival_details->address1) && $obj_organiser_festival_details->address1 != "") echo $obj_organiser_festival_details->address1 . " - ";
                    if (isset($obj_organiser_festival_details->address2) && $obj_organiser_festival_details->address2 != "") echo $obj_organiser_festival_details->address2 . " - ";
                    if (isset($obj_organiser_festival_details->ville_id) && $obj_organiser_festival_details->ville_id != "0") echo $this->mdlville->getVilleById($obj_organiser_festival_details->ville_id)->Nom;
                    if (isset($obj_organiser_festival_details->tel) && $obj_organiser_festival_details->tel != "") echo "<br/>Tel : " . $obj_organiser_festival_details->tel;
                    if (isset($obj_organiser_festival_details->mobile) && $obj_organiser_festival_details->mobile != "") echo "<br/>Mobile : " . $obj_organiser_festival_details->mobile;
                    if (isset($obj_organiser_festival_details->website) && $obj_organiser_festival_details->website != "") $organiser_website_page = $obj_organiser_festival_details->website; else $organiser_webfestivalsite_page = "";//echo "<br/>Site Web : " . $obj_organiser_festival_details->website;
                    if (isset($obj_organiser_festival_details->facebook) && $obj_organiser_festival_details->facebook != "") $organiser_facebook_page = $obj_organiser_festival_details->facebook; else $organiser_facebook_page = "";//echo "<br/>Facebook : " . $obj_organiser_festival_details->facebook;
                    if (isset($obj_organiser_festival_details->twitter) && $obj_organiser_festival_details->twitter != "") $organiser_twitter_page = $obj_organiser_festival_details->twitter; else $organiser_twitter_page = "";//echo "<br/>Twitter : " . $obj_organiser_festival_details->twitter;
                    if (isset($obj_organiser_festival_details->googleplus) && $obj_organiser_festival_details->googleplus != "") $organiser_googleplus_page = $obj_organiser_festival_details->googleplus; else $organiser_googleplus_page = "";//echo "<br/>Google+ : " . $obj_organiser_festival_details->googleplus;
                    ?>
                    <div style="border: 1px solid; margin: 0 auto; text-align: center; width: 500px; display: table; margin-top: 15px; padding: 15px;">Ev&eacute;nement
                        organis&eacute; par
                        <?php
                        echo $obj_organiser_article_details->name . "<br/>";
                        echo $obj_organiser_article_details->postal_code . " - ";
                        if (isset($obj_organiser_article_details->address1) && $obj_organiser_article_details->address1 != "") echo $obj_organiser_article_details->address1 . " - ";
                        if (isset($obj_organiser_article_details->address2) && $obj_organiser_article_details->address2 != "") echo $obj_organiser_article_details->address2 . " - ";
                        if (isset($obj_organiser_article_details->ville_id) && $obj_organiser_article_details->ville_id != "0") echo $this->mdlville->getVilleById($obj_organiser_article_details->ville_id)->Nom;
                        if (isset($obj_organiser_article_details->tel) && $obj_organiser_article_details->tel != "") echo "<br/>Tel : " . $obj_organiser_article_details->tel;
                        if (isset($obj_organiser_article_details->mobile) && $obj_organiser_article_details->mobile != "") echo "<br/>Mobile : " . $obj_organiser_article_details->mobile;
                        if (isset($obj_organiser_article_details->website) && $obj_organiser_article_details->website != "") $organiser_website_page = $obj_organiser_article_details->website; else $organiser_website_page = "";//echo "<br/>Site Web : " . $obj_organiser_article_details->website;
                        if (isset($obj_organiser_article_details->facebook) && $obj_organiser_article_details->facebook != "") $organiser_facebook_page = $obj_organiser_article_details->facebook; else $organiser_facebook_page = "";//echo "<br/>Facebook : " . $obj_organiser_article_details->facebook;
                        if (isset($obj_organiser_article_details->twitter) && $obj_organiser_article_details->twitter != "") $organiser_twitter_page = $obj_organiser_article_details->twitter; else $organiser_twitter_page = "";//echo "<br/>Twitter : " . $obj_organiser_article_details->twitter;
                        if (isset($obj_organiser_article_details->googleplus) && $obj_organiser_article_details->googleplus != "") $organiser_googleplus_page = $obj_organiser_article_details->googleplus; else $organiser_googleplus_page = "";//echo "<br/>Google+ : " . $obj_organiser_article_details->googleplus;
                        ?>
                    </div>
                    <?php
                }
            }
            ?>

            <div class="col-lg-12 padding0" style="display: table; width: 100%;">

                <?php if (isset($oDetailAgenda->siteweb) && $oDetailAgenda->siteweb != "") $siteweb_agenda = $oDetailAgenda->siteweb; else $siteweb_agenda = "javascript:void(0);"; ?>

                <div class="col-lg-12" style="text-align:center; padding-top:15px !important;">

                    <ul class="contact_info_article_list">

                        <?php if ($oDetailAgenda->video != "") { ?>
                            <li>
                                <?php
                                if ((isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0" && $localdata_IdVille != NULL) || (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0" && $localdata_IdDepartement != NULL)) {
                                    ?>
                                    <a href="javascript:void(0);"
                                       onclick='javascript:window.open("<?php echo site_url("article/details_article_contact/" . $oDetailAgenda->id . "/video"); ?>", "<?php echo $oDetailAgenda->nom_manifestation; ?>", "width=527, height=460");'
                                       title="Page Video"><img
                                                src="<?php echo GetImagePath("privicarte/"); ?>/ico_youtube.png"
                                                alt="video"/></a>
                                <?php } else { ?>
                                    <a href="#divVideoPartnerAgenda" id="IdVideoPartnerAgenda" title="Vidéo"><img
                                                src="<?php echo GetImagePath("privicarte/"); ?>/ico_youtube.png"
                                                alt="video"/></a>
                                <?php } ?>
                            </li>
                        <?php } ?>



                        <?php if (isset($oDetailAgenda->pdf) && $oDetailAgenda->pdf != "") { ?>
                            <li>
                                <a href="<?php echo base_url() . "/application/resources/front/images/agenda/pdf/" . $oDetailAgenda->pdf; ?>"
                                   target="_blank" title="<?php echo $oDetailAgenda->titre_pdf; ?>"><img
                                            src="<?php echo GetImagePath("privicarte/"); ?>/ico_pdf.png" alt="pdf"/></a>
                            </li>
                        <?php } ?>


                        <li>
                            <?php
                            if ((isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0" && $localdata_IdVille != NULL) || (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0" && $localdata_IdDepartement != NULL)) {
                                ?>
                                <a href="javascript:void(0);"
                                   onclick='javascript:window.open("<?php echo site_url("article/details_article_contact/" . $oDetailAgenda->id . "/contact"); ?>", "<?php echo $oDetailAgenda->nom_manifestation; ?>", "width=500, height=400");'
                                   title="Page Contact"><img src="<?php echo GetImagePath("privicarte/"); ?>/ico_msg.png"/></a>
                            <?php } else { ?>
                                <a href="#divContactPartnerForm" id="IdContactPartnerForm" title="Contact"><img
                                            src="<?php echo GetImagePath("privicarte/"); ?>/ico_msg.png"/></a>
                            <?php } ?>
                        </li>


                        <?php
                        if (isset($iframe_session_navigation) && $iframe_session_navigation == "1") {
                        } else {
                            ?>
                            <?php if (isset($oDetailAgenda) && $oDetailAgenda->facebook != "" && $oDetailAgenda->facebook != NULL) { ?>
                                <?php
                                if (preg_match('#https://#', $oDetailAgenda->facebook) || preg_match('#http://#', $oDetailAgenda->facebook)) {
                                    $link_fb_to_show = $oDetailAgenda->facebook;
                                } else {
                                    $link_fb_to_show = "https://www.facebook.com/" . $oDetailAgenda->facebook;
                                }
                                ?>
                                <li>
                                    <a href="javascript:void(0);"
                                       onclick='javascript:window.open("<?php echo $link_fb_to_show; ?>", "FacebookPrivicarte", "width=850, height=800");'
                                       title="Page Facebook"><img
                                                src="<?php echo GetImagePath("privicarte/"); ?>/ico_fb.png"/></a>
                                </li>
                            <?php } ?>
                        <?php } ?>




                        <?php
                        if (isset($iframe_session_navigation) && $iframe_session_navigation == "1") {
                        } else {
                            if (isset($oInfoCommercant) && $oInfoCommercant->google_plus != "" && $oInfoCommercant->google_plus != NULL) {
                                if (preg_match('#https://#', $oInfoCommercant->google_plus) || preg_match('#http://#', $oInfoCommercant->google_plus)) {
                                    $link_google_plus_to_show = $oInfoCommercant->google_plus;
                                } else {
                                    $link_google_plus_to_show = "http://www.twitter.com/" . $oInfoCommercant->google_plus;
                                }
                                ?>
                                <li>
                                    <a href="javascript:void(0);"
                                       onclick='javascript:window.open("<?php echo $link_google_plus_to_show; ?>", "FacebookPrivicarte", "width=850, height=800");'
                                       title="Page Twitter"><img
                                                src="<?php echo GetImagePath("privicarte/"); ?>/ico_twt.png"/></a>
                                </li>
                            <?php } ?>
                        <?php } ?>

                        <?php if (isset($siteweb_agenda) && $siteweb_agenda != '' && $siteweb_agenda != null && $siteweb_agenda != 'http://www.') { ?>
                            <li>
                                <a href="<?php echo $siteweb_agenda; ?>" title="Site web" target="_blank"><img
                                            src="<?php echo GetImagePath("privicarte/"); ?>/ico_web.png"/></a>
                            </li>
                        <?php } ?>




                        <?php
                        $this->load->model("user");
                        $thisss =& get_instance();
                        $thisss->load->library('ion_auth');
                        $this->load->model("ion_auth_used_by_club");
                        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oDetailAgenda->IdCommercant);
                        if (isset($user_ion_auth_id)) $user_groups = $thisss->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
                        if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;


                        if ($thisss->ion_auth->logged_in()) {
                            $user_ion_auth = $thisss->ion_auth->user()->row();
                            $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                            if ($iduser == null || $iduser == 0 || $iduser == "") {
                                $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                            }
                        } else $iduser = 0;
                        if (isset($iduser) && $iduser != 0 && $iduser != NULL && $iduser != "") {
                            $oCommercantFavoris = $this->user->verify_favoris($iduser, $oDetailAgenda->IdCommercant);
                        }

                        ?>


                        <!--
             <?php
                        if (isset($iframe_session_navigation) && $iframe_session_navigation == "1") {
                        } else {
                            ?>
                <li>
                    <?php if (isset($oCommercantFavoris) && $oCommercantFavoris != NULL && $oCommercantFavoris->Favoris == "1") { ?>
                        <a href="<?php echo site_url('front/utilisateur/delete_favoris/' . $oDetailAgenda->IdCommercant); ?>" title="Supprimer de mes Favoris">
                            <img src="<?php echo GetImagePath("privicarte/"); ?>/ico_favoris.png" />
                        </a>
                    <?php } else { ?>
                        <a href="<?php echo site_url('front/utilisateur/ajout_favoris/' . $oDetailAgenda->IdCommercant); ?>" title="Ajouter à mes Favoris">
                            <img src="<?php echo GetImagePath("privicarte/"); ?>/ico.png" />
                        </a>
                    <?php } ?>
                </li>
                <?php } ?>
        -->


                    </ul>

                </div>

            </div>
        </div>

        <?php
        $bonPlanParCommercant = $this->mdlbonplan->bonPlanParCommercant($oCommercant->IdCommercant);
        //var_dump($bonPlanParCommercant);
        if (isset($bonPlanParCommercant) && count($bonPlanParCommercant) > 0) {
            ?>

            <script type="text/javascript">
                $(document).ready(function () {
                    $(".fancybox_<?php echo $oCommercant->IdCommercant; ?>").fancybox();
                    $("#id_bonplan_<?php echo $oCommercant->IdCommercant; ?>").fancybox({
                        autoScale: false,
                        overlayOpacity: 0.8, // Set opacity to 0.8
                        overlayColor: "#000000", // Set color to Black
                        padding: 5,
                        width: 1055,
                        height: 800,
                        transitionIn: 'elastic',
                        transitionOut: 'elastic',
                        type: 'iframe'
                    });
                });
            </script>


        <?php } ?>


        <div class="col-lg-12 title_categ_black proximite_conteneur_list" style="margin-bottom: 0; margin-top: 0; display: table;">Adresse & Plan d'acc&egrave;s</div>


        <div class="col-lg-12 padding0" style="display: table; width: 100%;">


            <div class="col-lg-12" style='background-color: transparent;
    font-family: "Arial",sans-serif;
    font-size: 14px;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 16px; display: table;
    text-align: center; margin-top: 15px;
    vertical-align: 0;'><?php //echo $oDetailAgenda->nom_localisation; ?>
                <?php
                if (isset($oDetailAgenda->location_id) && $oDetailAgenda->location_id != "0") {
                    $obj_location_festival_details = $this->mdl_localisation->getById($oDetailAgenda->location_id);
                    if (isset($obj_location_festival_details) && is_object($obj_location_festival_details)) {
                        echo $obj_location_festival_details->location . " - " . $obj_location_festival_details->location_address . " - ";
                        $obj_ville_location_details = $this->mdlville->getVilleById($obj_location_festival_details->location_villeid);
                        if (isset($obj_ville_location_details->Nom) && is_object($obj_ville_location_details)) {
                            echo $obj_ville_location_details->CodePostal . " - " . $obj_ville_location_details->Nom;
                        }
                    }
                }
                ?></div>
            <div class="col-lg-12" style="text-align:center; padding:15px 0;">
                <?php //if (isset($oDetailAgenda->ville)) $ville_map = $oDetailAgenda->ville; else $ville_map = ''; ?>
                <?php $ville_map = ''; ?>
                <?php if (isset($oDetailAgenda->adresse_localisation)) { ?>

                    <div class="col-lg-12">
                        <iframe
                                src="http://maps.google.fr/maps?f=q&amp;source=s_q&amp;hl=fr&amp;geocode=&amp;q=<?php if (isset($obj_location_article_details->location)) echo $obj_location_article_details->location . " "; ?><?php echo $oDetailAgenda->adresse_localisation . ", " . $oDetailAgenda->codepostal_localisation . " &nbsp;" . $ville_map; ?>&amp;aq=0&amp;ie=UTF8&amp;hq=&amp;hnear=<?php echo $oDetailAgenda->adresse_localisation . ", " . $oDetailAgenda->codepostal_localisation . " &nbsp;" . $ville_map; ?>&amp;t=m&amp;vpsrc=0&amp;output=embed"
                                width="100%" height="350"></iframe>
                    </div>

                <?php } ?>
            </div>
        </div>


        <?php if (isset($oDetailAgenda->activ_fb_comment) && $oDetailAgenda->activ_fb_comment == '1') { ?>
            <div class="col-lg-12 padding0" style="background-color: #fff;">
                <div class="fb-comments" data-href="<?php echo site_url("/article/details/" . $oDetailAgenda->id); ?>"
                     data-width="800" data-numposts="5"></div>
            </div>
        <?php } ?>


        <div style="margin:20px; display:none">
            <!--<strong>Organisateur :</strong><br/> <?php // echo $oDetailAgenda->organisateur ; ?> de <?php // echo $oDetailAgenda->ville ; ?><br/>
<?php // echo $oDetailAgenda->adresse_localisation ; ?> <?php // echo $oDetailAgenda->codepostal_localisation ; ?>
<?php // if ($oDetailAgenda->telephone!="") echo "<br/>Tél. ".$oDetailAgenda->telephone ; ?>
<?php // if ($oDetailAgenda->mobile!="") echo "<br/>Mobile. ".$oDetailAgenda->mobile ; ?>
<?php // if ($oDetailAgenda->fax!="") echo "<br/>Fax. ".$oDetailAgenda->fax ; ?>-->

            <?php echo "<br/><strong>Partenaire</strong> : " . $oCommercant->NomSociete; ?>
            <?php
            $this->load->model("mdlville");
            $oVilleCommercant = $this->mdlville->getVilleById($oCommercant->IdVille);
            if (isset($oVilleCommercant)) echo "<br/>" . $oVilleCommercant->Nom;
            echo " " . $oVilleCommercant->CodePostal . "<br/>";
            if (isset($oCommercant->TelFixe)) echo " Tel." . $oCommercant->TelFixe;
            if (isset($oCommercant->TelMobile)) echo " Mobile." . $oCommercant->TelMobile;
            ?>
            <?php if ($oDetailAgenda->date_depot != "") echo "<br/>Fiche déposée le " . translate_date_to_fr($oDetailAgenda->date_depot); ?>
            <?php if ($oDetailAgenda->last_update != "") echo "<br/>Fiche modifiée le " . translate_date_to_fr($oDetailAgenda->last_update); ?>

        </div>

        <div style='font-family: "Arial",sans-serif; display:none;
    font-size: 12px; margin-bottom:10px; text-align:center;
    font-weight: 700; margin-top:30px;
    line-height: 1.25em;'>Vous avez une question particulière à nous poser, adressez nous un mail express
        </div>

        <div style="margin-bottom:10px; margin-top:10px; display:none;">
            <center>
                <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/fields.check.js"></script>
                <script type="application/javascript">
                    $(document).ready(function () {
                        $("#btn_submit_form_module_detailbonnplan").click(function () {
                            //alert('test form submit');
                            txtErrorform = "";

                            var txtError_text_mail_form_module_detailbonnplan = "";
                            var text_mail_form_module_detailbonnplan = $("#text_mail_form_module_detailbonnplan").val();
                            if (text_mail_form_module_detailbonnplan == "") {
                                //$("#divErrorform_module_detailbonnplan").html('<font color="#FF0000">Veuillez saisir votre demande</font>');
                                txtErrorform += "1";
                                $("#text_mail_form_module_detailbonnplan").css('border-color', 'red');
                                $("#text_mail_form_module_detailbonnplan").focus();
                            } else {
                                $("#text_mail_form_module_detailbonnplan").css('border-color', '#E3E1E2');
                            }

                            var nom_mail_form_module_detailbonnplan = $("#nom_mail_form_module_detailbonnplan").val();
                            if (nom_mail_form_module_detailbonnplan == "") {
                                txtErrorform += "- Veuillez indiquer Votre nom_mail_form_module_detailbonnplan <br/>";
                                $("#nom_mail_form_module_detailbonnplan").css('border-color', 'red');
                                $("#nom_mail_form_module_detailbonnplan").focus();
                            } else {
                                $("#nom_mail_form_module_detailbonnplan").css('border-color', '#E3E1E2');
                            }

                            var email_mail_form_module_detailbonnplan = $("#email_mail_form_module_detailbonnplan").val();
                            if (email_mail_form_module_detailbonnplan == "" || !isEmail(email_mail_form_module_detailbonnplan)) {
                                txtErrorform += "- Veuillez indiquer Votre email_mail_form_module_detailbonnplan <br/>";
                                //alert("Veuillez indiquer Votre nom");
                                $("#email_mail_form_module_detailbonnplan").css('border-color', 'red');
                                $("#email_mail_form_module_detailbonnplan").focus();
                            } else {
                                $("#email_mail_form_module_detailbonnplan").css('border-color', '#E3E1E2');
                            }


                            if (txtErrorform == "") {
                                $("#form_module_detailbonnplan").submit();
                            }
                        });


                    });
                </script>
                <?php if (isset($user_ion_auth)) { ?>
                    <style type="text/css">
                        .inputhidder {
                            visibility: hidden;
                        }
                    </style>
                <?php } ?>

                <table border="0" align="center" style="text-align:center; width:100%;">
                    <tr>
                        <td style="text-align:left;"><img
                                    src="<?php echo GetImagePath("front/"); ?>/btn_new/info_annonce_img.png" alt="img"
                                    width="250"></td>
                        <td>
                            <form method="post" name="form_module_detailbonnplan" id="form_module_detailbonnplan"
                                  action="" enctype="multipart/form-data">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr class="inputhidder">
                                        <td>Votre Nom</td>
                                        <td><input id="nom_mail_form_module_detailbonnplan"
                                                   name="nom_mail_form_module_detailbonnplan" type="text"
                                                   value="<?php if (isset($user_ion_auth) && $user_ion_auth->first_name != "") echo $user_ion_auth->first_name; ?>">
                                        </td>
                                    </tr>
                                    <tr class="inputhidder">
                                        <td>Votre Téléphone</td>
                                        <td><input id="tel_mail_form_module_detailbonnplan"
                                                   name="tel_mail_form_module_detailbonnplan" type="text"
                                                   value="<?php if (isset($user_ion_auth) && $user_ion_auth->phone != "") echo $user_ion_auth->phone; ?>">
                                        </td>
                                    </tr>
                                    <tr class="inputhidder">
                                        <td>Votre Email</td>
                                        <td><input id="email_mail_form_module_detailbonnplan"
                                                   name="email_mail_form_module_detailbonnplan" type="text"
                                                   value="<?php if (isset($user_ion_auth) && $user_ion_auth->email != "") echo $user_ion_auth->email; ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <textarea id="text_mail_form_module_detailbonnplan" cols="28" rows="7"
                                                      name="text_mail_form_module_detailbonnplan"
                                                      style="width:246px; height:130px;font-family:Arial, Helvetica, sans-serif; font-size:10px;"></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <input id="btn_reset_form_module_detailbonnplan" type="reset"
                                                   value="Effacer" style="width:82px; height:22px;">
                                            <input id="btn_submit_form_module_detailbonnplan" type="button"
                                                   name="btn_submit_form_module_detailbonnplan" value="Envoyer"
                                                   style="width:90px; height:22px;">
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <div
                                    id="divErrorform_module_detailbonnplan"><?php if (isset($mssg_envoi_module_detail_bonplan)) echo $mssg_envoi_module_detail_bonplan; ?></div>
                        </td>
                    </tr>
                </table>
                <br/>

            </center>
        </div>


    </div>
<?php $this->load->view('export/includes/footer', $data); ?>