<?php $data['empty'] = null; ?>
<?php if (!isset($contentonly)||$contentonly!='1') { ?>
<?php $this->load->view('export/includes/header'); ?>

<?php $this->load->view('export/includes/filterbonplan'); ?>



<div class="proximite_conteneur_list col-lg-12 col-xs-12 paddingright0" id="proximite_conteneur_list_id">
    <?php } ?>

    <?php foreach ($toAgenda as $oAgenda) { ?>


        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 paddingleft0">
            <div class="export_image_container col-lg-12 padding0">
                <a href="<?php echo base_url(); ?>bonplan/bonplan_perso_details/<?php echo $oAgenda->bonplan_id; ?>/<?php if (isset($zCouleurBgBouton)) echo '?zCouleurBgBouton='.$zCouleurBgBouton; ?><?php if (isset($zCouleur)) echo '&zCouleur='.$zCouleur; ?><?php if (isset($zCouleurTitre)) echo '&zCouleurTitre='.$zCouleurTitre; ?>">
                    <?php
                    $thisss =& get_instance();
                    $thisss->load->model("ion_auth_used_by_club");
                    $ionauth_id = $thisss->ion_auth_used_by_club->get_ion_id_from_commercant_id($oAgenda->bonplan_commercant_id);
                    $photoCommercant_path = "application/resources/front/photoCommercant/imagesbank/" . $ionauth_id . "/";
                    $photoCommercant_path_old = "application/resources/front/photoCommercant/images/";
                    $image_home_vignette = "";
                    if (isset($oAgenda->bonplan_photo1) && $oAgenda->bonplan_photo1 != "" && is_file($photoCommercant_path . $oAgenda->bonplan_photo1) == true) {
                        $image_home_vignette = $oAgenda->bonplan_photo1;
                    } else if ($image_home_vignette == "" && isset($oAgenda->bonplan_photo2) && $oAgenda->bonplan_photo2 != "" && is_file($photoCommercant_path . $oAgenda->bonplan_photo2) == true) {
                        $image_home_vignette = $oAgenda->bonplan_photo2;
                    } else if ($image_home_vignette == "" && isset($oAgenda->bonplan_photo3) && $oAgenda->bonplan_photo3 != "" && is_file($photoCommercant_path . $oAgenda->bonplan_photo3) == true) {
                        $image_home_vignette = $oAgenda->bonplan_photo3;
                    } else if ($image_home_vignette == "" && isset($oAgenda->bonplan_photo4) && $oAgenda->bonplan_photo4 != "" && is_file($photoCommercant_path . $oAgenda->bonplan_photo4) == true) {
                        $image_home_vignette = $oAgenda->bonplan_photo4;
                    } else if ($image_home_vignette == "" && isset($oAgenda->bonplan_photo5) && $oAgenda->bonplan_photo5 != "" && is_file($photoCommercant_path . $oAgenda->bonplan_photo5) == true) {
                        $image_home_vignette = $oAgenda->bonplan_photo5;
                    } else if ($image_home_vignette == "" && isset($oAgenda->bonplan_photo1) && $oAgenda->bonplan_photo1 != "" && is_file($photoCommercant_path_old . $oAgenda->bonplan_photo1) == true) {
                        $image_home_vignette = $oAgenda->bonplan_photo1;
                    } else if ($image_home_vignette == "" && isset($oAgenda->bonplan_photo2) && $oAgenda->bonplan_photo2 != "" && is_file($photoCommercant_path_old . $oAgenda->bonplan_photo2) == true) {
                        $image_home_vignette = $oAgenda->bonplan_photo2;
                    } else if ($image_home_vignette == "" && isset($oAgenda->bonplan_photo3) && $oAgenda->bonplan_photo3 != "" && is_file($photoCommercant_path_old . $oAgenda->bonplan_photo3) == true) {
                        $image_home_vignette = $oAgenda->bonplan_photo3;
                    } else if ($image_home_vignette == "" && isset($oAgenda->bonplan_photo4) && $oAgenda->bonplan_photo4 != "" && is_file($photoCommercant_path_old . $oAgenda->bonplan_photo4) == true) {
                        $image_home_vignette = $oAgenda->bonplan_photo4;
                    } else if ($image_home_vignette == "" && isset($oAgenda->bonplan_photo5) && $oAgenda->bonplan_photo5 != "" && is_file($photoCommercant_path_old . $oAgenda->bonplan_photo5) == true) {
                        $image_home_vignette = $oAgenda->bonplan_photo5;
                    }

                    if ($image_home_vignette != "") {
                        if (isset($image_home_vignette) && $image_home_vignette != "" && is_file($photoCommercant_path . $image_home_vignette) == true)
                            echo '<img src="' . base_url() . $photoCommercant_path . $image_home_vignette . '" width="100%"/>';
                        else echo '<img src="' . base_url() . $photoCommercant_path_old . $image_home_vignette . '" width="100%"/>';
                    } else {
                        $image_home_vignette_to_show = GetImagePath("front/") . "/no_image_annuaire.png";
                        echo '<img src="' . $image_home_vignette_to_show . '" width="100%"/>';
                    }
                    ?>
                </a>
                <div class="export_categ_content"><?php echo $oAgenda->Nom; ?></div>
            </div>

            <div class="export_content_container col-lg-12 padding0" style="height:200px">
                <div class="col-lg-12 text-center eport_txt_content paddingbottom15"><?php //var_dump($oAgenda);?>
                    <div style="color: black" class="export_title col-lg-12 padding0">
                        <?php echo $oAgenda->NomSociete; ?>
                    </div>
                    <div style="color: black; font-size: 80%" class="export_title col-lg-12 padding0">
                        <?php echo $oAgenda->ville; ?>
                    </div>
                    <div class="export_title col-lg-12 padding0">
                        <?php echo $oAgenda->bonplan_titre; ?>
                    </div>
                </div>
                <div class="col-sm-12 bonplan_list_details"
                     id="item_list_details_<?php echo $oAgenda->bonplan_commercant_id; ?>"
                     style="padding-top:10px; background-color:transparent;">
                    <?php
                    if ($oAgenda->bp_simple_value >= "0") {
                        if (isset($oAgenda->bp_unique_value) && $oAgenda->bp_unique_value != "") $ancien_prix_bp = $oAgenda->bp_unique_value; else $ancien_prix_bp = "";
                        if (isset($oAgenda->bp_unique_prix) && $oAgenda->bp_unique_prix != "") $nouveau_prix_bp = $oAgenda->bp_unique_prix; else $nouveau_prix_bp = "";
                    } else if ($oAgenda->bp_simple_value >= "0") {
                        if (isset($oListeBonPlan->bp_multiple_value) && $oListeBonPlan->bp_multiple_value != "") $ancien_prix_bp = $oListeBonPlan->bp_multiple_value; else $ancien_prix_bp = "";
                        if (isset($oListeBonPlan->bp_multiple_prix) && $oListeBonPlan->bp_multiple_prix != "") $nouveau_prix_bp = $oListeBonPlan->bp_multiple_prix; else $nouveau_prix_bp = "";
                    } else {
                        if (isset($oListeBonPlan->bp_simple_value) && $oListeBonPlan->bp_simple_value != "") $ancien_prix_bp = $oListeBonPlan->bp_simple_value; else $ancien_prix_bp = "";
                        if (isset($oListeBonPlan->bp_simple_prix) && $oListeBonPlan->bp_simple_prix != "") $nouveau_prix_bp = $oListeBonPlan->bp_simple_prix; else $nouveau_prix_bp = "";
                    }
                    ?>

                    <div class="col-12" style="text-align:center;">
            	<span style='background-color: transparent;
                        color: #000000;
                        font-family: "Arial",sans-serif;
                        font-size: 18.7px;
                        font-style: normal;
                        font-variant: normal;
                        font-weight: normal;
                        line-height: 23px; background-image:url(<?php echo GetImagePath("privicarte/"); ?>/barre_prix_bp_span.png);
                        background-repeat: no-repeat;
                        background-position: center;
                        text-decoration: none;
                        vertical-align: 0;'><?php echo $ancien_prix_bp; ?> </span>
                        <span style='background-color: transparent;
    color: #cf1590;
    font-family: "Arial",sans-serif;
    font-size: 29.3px;
    font-style: normal;
    font-variant: normal;
    font-weight: 700;
    line-height: 35px;
    text-decoration: none;
    vertical-align: 0;'> <?php echo $nouveau_prix_bp; ?></span>
                    </div>
                </div>
                <div class="col-12" style="text-align:center;">
                    <?php
                    if ($oAgenda->bonplan_date_debut == $oAgenda->bonplan_date_fin) {
                        if (isset($oAgenda->bonplan_date_debut) && $oAgenda->bonplan_date_debut != "0000-00-00") echo "Le " . translate_date_to_fr($oAgenda->bonplan_date_debut);
                    } else {
                        if (isset($oAgenda->bonplan_date_debut) && $oAgenda->bonplan_date_debut != "0000-00-00") echo "Du " . translate_date_to_fr($oAgenda->bonplan_date_debut);
                        if (isset($oAgenda->bonplan_date_fin) && $oAgenda->bonplan_date_fin != "0000-00-00") {
                            if (isset($oAgenda->bonplan_date_debut) && $oAgenda->bonplan_date_debut != "0000-00-00") echo "<br/>au " . translate_date_to_fr($oAgenda->bonplan_date_debut);
                            else echo " Jusqu'au " . translate_date_to_fr($oAgenda->bonplan_date_fin);
                        }
                    }
                    ?>
                </div>
                <div class="" style="position: absolute; right: 15px; bottom: 15px;">
                    <div class="export_btn_more">
                        <a href="<?php echo base_url(); ?>bonplan/bonplan_perso_details/<?php echo $oAgenda->bonplan_id; ?>/<?php if (isset($zCouleurBgBouton)) echo '?zCouleurBgBouton='.$zCouleurBgBouton; ?><?php if (isset($zCouleurNbBtn)) echo '&zCouleurNbBtn='.$zCouleurNbBtn; ?><?php if (isset($zCouleurTextBouton)) echo '&zCouleurTextBouton='.$zCouleurTextBouton; ?><?php if (isset($zCouleur)) echo '&zCouleur='.$zCouleur; ?><?php if (isset($zCouleurTitre)) echo '&zCouleurTitre='.$zCouleurTitre; ?>"
                           title="D&eacute;tails" style="text-decoration:none;" class="btn export_btn_more_custom">
                            <!--<img src="<?php echo GetImagePath("privicarte/"); ?>/plus_infos_black.png"
                                     alt="details"/>-->
                            +
                        </a>
                    </div>
                </div>
            </div>
        </div>


    <?php } ?>

    <?php if (!isset($contentonly)||$contentonly!='1') { ?>
</div>


<?php $this->load->view('export/includes/footer'); ?>
<?php } ?>
