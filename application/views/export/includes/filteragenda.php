<input type="hidden" name="proximite_tiDepartement" id="proximite_tiDepartement"
       value="<?php if (isset($tiDepartement_array)) echo $tiDepartement_array; else echo "0"; ?>"/>

<input type="hidden" name="proximite_tiDeposant" id="proximite_tiDeposant"
       value="<?php if (isset($tiDeposant_array)) echo $tiDeposant_array; else echo "0"; ?>"/>

<input type="hidden" name="zCouleur" id="zCouleur"
       value="<?php if (isset($zCouleur)) echo $zCouleur; ?>"/>

<input type="hidden" name="zCouleurTitre" id="zCouleurTitre"
       value="<?php if (isset($zCouleurTitre)) echo $zCouleurTitre; ?>"/>

<input type="hidden" name="zCouleurTextBouton" id="zCouleurTextBouton"
       value="<?php if (isset($zCouleurTextBouton)) echo $zCouleurTextBouton; ?>"/>

<input type="hidden" name="zCouleurBgBouton" id="zCouleurBgBouton"
       value="<?php if (isset($zCouleurBgBouton)) echo $zCouleurBgBouton; ?>"/>

<input type="hidden" name="proximite_tiDossperso" id="proximite_tiDossperso"
       value="<?php if (isset($tiDossperso)) echo $tiDossperso; ?>"/>

<input type="hidden" name="tofilterville" id="tofilterville"
       value="<?php if (isset($tiville)) echo $tiville; ?>"/>

<input type="hidden" name="proximite_tiCategorie" id="proximite_tiCategorie"
       value="<?php if (isset($tiCategorie_for_init)) echo $tiCategorie_for_init; ?>"/>

<input type="hidden" name="proximite_tiCategorie_init" id="proximite_tiCategorie_init"
       value="<?php if (isset($tiCategorie_for_init)) echo $tiCategorie_for_init; ?>"/>

<input type="hidden" name="proximite_tiDatefilter" id="proximite_tiDatefilter"
       value="0"/>


<nav id="main_navbar_export" class="navbar navbar-default navbar-fixed-top" style="width: 100%; display: table;padding-top: 15px">
    <div style="width: 50%;text-align: center;margin: auto">
        <label for="filter_ville">Filtrer par Commune</label>

            <select class="form-control" id="filter_ville" name="filter_ville">
                <option value="">Toutes les Communes</option>
                <?php var_dump($allville); foreach ($allville as $ville){ ?>
                    <option <?php if (isset($tiville) AND $tiville == $ville->IdVille_localisation ){echo "selected";} ?> value="<?php echo $ville->IdVille_localisation; ?>" id="filtrer_<?php echo $ville->IdVille_localisation; ?>"><?php echo $ville->ville ;?> (<?php echo $this->mdl_agenda->get_count_filter_agenda($ville->IdVille_localisation);?>)</option>
                <?php } ?>
            </select>
        <script type="text/javascript">
            $("#filter_ville").change(function(){
                export_filter_submit_func();
            });
        </script>

    </div>
    <div class="proximite_conteneur_filtre col-lg-12 paddingright15 proximite_conteneur_list">
        <div class="col-lg-12 export_filter_content padding0">
            <div class="col-lg-12 padding0 export_filter_content_items">
                <div class="col-sm-3 col-xs-6 text-center paddingtop15 paddingbottom15" id="filter_category_link">
                    CATEGORIES
                </div>
                <div class="col-sm-3 col-xs-6 text-center paddingtop15 paddingbottom15" id="filter_date_link">
                    FILTRER
                </div>
                <div class="col-sm-3 col-xs-6 proximite_conteneur_filtreIdQuand text-center paddingtop15 paddingbottom15"
                     id="filter_keyword_link">
                    MOT CLES
                </div>
                <div class="col-sm-3 col-xs-6 text-center paddingtop15 paddingbottom15" id="filter_init_link">
                    REINITIALISER
                </div>
            </div>
        </div>
        <div class="col-lg-12 paddingtop15 padding0 text-center export_details_filter_content">
            <div id="filter_content_category">
                <?php
                foreach ($tiCategorie_list_array as $oCategorie_array) {
                    ?>
                    <a href="javascript:void(0);"
                       onclick="javascript:change_categ_filter_export(<?php echo $oCategorie_array->agenda_categid; ?>);"
                       class="btn btn-default">
                        <span id="nb_categ_<?php echo $oCategorie_array->agenda_categid ?>"> </span>  (<?php echo $oCategorie_array->category; ?>)
                    </a>
                    <?php
                }
                ?>
                <script type="text/javascript">
                    $(document).ready(function () {
                        setInterval(function(){
                            <?php  foreach ($tiCategorie_list_array as $oCategorie_array) { ?>
                            var categ_filter = "<?php echo $oCategorie_array->agenda_categid ?>";
                            var ville_filter = $("#filter_ville").val();
                            if (ville_filter == "" || ville_filter == undefined) {
                                ville_filter ="0";
                            }

                            $.get(
                                '<?php echo site_url("agenda/getnb_categ/"); ?>',
                                {
                                    categ_filter:categ_filter,
                                    filter_ville:ville_filter
                                },
                                function (zReponse) {
                                    $('#nb_categ_<?php echo $oCategorie_array->agenda_categid ?>').html(zReponse);
                                });
                            <?php
                            }
                            ?>
                        }, 3000);
                    });
                </script>
            </div>
            <div id="filter_content_date_id">
                <div class="col-sm-5ths col-xs-6 paddingleft0"><a href="javascript:void(0);" class="btn btn-default col-xs-12"
                                                                  onclick="javascript:change_date_filter_export(101);"><span id="now_fitre"></span></a></div>
                <div class="col-sm-5ths col-xs-6 paddingleft0"><a href="javascript:void(0);" class="btn btn-default col-xs-12"
                                                                  onclick="javascript:change_date_filter_export(202);"><span id="week_fitre"></span></a></div>
                <div class="col-sm-5ths col-xs-6 paddingleft0"><a href="javascript:void(0);" class="btn btn-default col-xs-12"
                                                                  onclick="javascript:change_date_filter_export(303);"><span id="full_week_fitre"></span></a></div>
                <div class="col-sm-5ths col-xs-6 paddingleft0"><a href="javascript:void(0);" class="btn btn-default col-xs-12"
                                                                  onclick="javascript:change_date_filter_export(404);"><span id="next_week_fitre"></span></a>
                </div>
                <div class="col-sm-5ths col-xs-6 padding0"><a href="javascript:void(0);" class="btn btn-default col-xs-12"
                                                              onclick="javascript:change_date_filter_export(505);"><span id="this_months_fitre"></span></a></div>
            </div>
            <div id="filter_content_keyword">
                <input type="text" class="form-control" id="keyword_input_export" value="" style="width: 75%; display: inline;"/>
                <button class="btn btn-success" id="btn_submit_keyword_filter">Chercher</button>
            </div>
        </div>
    </div>
</nav>