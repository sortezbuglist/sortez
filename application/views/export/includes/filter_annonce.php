<input type="hidden" name="proximite_tiDepartement" id="proximite_tiDepartement"
       value="<?php if (isset($tiDepartement_array)) echo $tiDepartement_array; else echo "0"; ?>"/>

<input type="hidden" name="proximite_tiDeposant" id="proximite_tiDeposant"
       value="<?php if (isset($tiDeposant_array)) echo $tiDeposant_array; else echo "0"; ?>"/>

<input type="hidden" name="zCouleur" id="zCouleur"
       value="<?php if (isset($zCouleur)) echo $zCouleur; ?>"/>

<input type="hidden" name="zCouleurTitre" id="zCouleurTitre"
       value="<?php if (isset($zCouleurTitre)) echo $zCouleurTitre; ?>"/>

<input type="hidden" name="zCouleurTextBouton" id="zCouleurTextBouton"
       value="<?php if (isset($zCouleurTextBouton)) echo $zCouleurTextBouton; ?>"/>

<input type="hidden" name="zCouleurBgBouton" id="zCouleurBgBouton"
       value="<?php if (isset($zCouleurBgBouton)) echo $zCouleurBgBouton; ?>"/>

<input type="hidden" name="proximite_tiDossperso" id="proximite_tiDossperso"
       value="<?php if (isset($tiDossperso)) echo $tiDossperso; ?>"/>

<input type="hidden" name="proximite_tiCategorie" id="proximite_tiCategorie"
       value="<?php if (isset($tiCategorie_for_init)) echo $tiCategorie_for_init; ?>"/>

<input type="hidden" name="proximite_tiCategorie_init" id="proximite_tiCategorie_init"
       value="<?php if (isset($tiCategorie_for_init)) echo $tiCategorie_for_init; ?>"/>

<input type="hidden" name="proximite_tietatfilter" id="proximite_tietatfilter"
       value=""/>

<input type="hidden" name="filter_ville" id="filter_ville"
       value="0"/>


<nav id="main_navbar_export" class="navbar navbar-default navbar-fixed-top" style="width: 100%; display: table;">
    <div style="width: 50%;text-align: center;margin: auto">
        <label for="filter_ville">Filtrer par Commune</label>

        <select class="form-control" id="inputStringVilleHidden_partenaires_annonce" name="filter_ville">
            <option value="">Toutes les Communes</option>
            <?php foreach ($toVille as $oVille){ ?>
                <option <?php if (isset($oVille) AND $oVille == $oVille->IdVille ){echo "selected";} ?> value="<?php echo $oVille->IdVille; ?>" id="filtrer_<?php echo $oVille->IdVille; ?>">
                    <?php echo $oVille->ville; ?>
                    &nbsp;(<?php echo $oVille->nbannonce; ?>)
                </option>
            <?php } ?>
        </select>
        <script type="text/javascript">
            $("#inputStringVilleHidden_partenaires_annonce").change(function(){
                change_cville_filter_export_annonce();
            });
        </script>

    </div>
    <div class="proximite_conteneur_filtre col-lg-12 paddingright15 proximite_conteneur_list">
        <div class="col-lg-12 export_filter_content padding0">
            <div class="col-lg-12 padding0 export_filter_content_items">
                <div class="col-sm-3 text-center paddingtop15 paddingbottom15" id="filter_category_link">
                    CATEGORIES
                </div>
                <div class="col-sm-3 text-center paddingtop15 paddingbottom15" id="filter_date_link">
                    FILTRER
                </div>
                <div class="col-sm-3 proximite_conteneur_filtreIdQuand text-center paddingtop15 paddingbottom15"
                     id="filter_keyword_link">
                    MOT CLES
                </div>
                <div class="col-sm-3 text-center paddingtop15 paddingbottom15" id="filter_init_link">
                    REINITIALISER
                </div>
            </div>
        </div>
        <div class="col-lg-12 paddingtop15 padding0 text-center export_details_filter_content">
            <div id="filter_content_category">
                <?php
                foreach ($tiCategorie_list_array as $oCategorie_array) {
                    ?>
                    <a href="javascript:void(0);"
                       onclick="javascript:change_categ_filter_export(<?php echo $oCategorie_array->IdRubrique; ?>);"
                       class="btn btn-default">
                        <?php echo $oCategorie_array->Nom; ?> (<?php echo $oCategorie_array->nb_annonce; ?>)
                    </a>
                    <?php
                }
                ?>
            </div>
            <div id="filter_content_date_id">
                <div class="col-sm-5ths paddingleft0"><a href="javascript:void(0);" class="btn btn-default col-xs-12"
                                                         onclick="javascript:change_etat_filter_export('all');">Tout <?php echo "(".$nbAll.")"; ?></a></div><?php // var_dump($toAgndaAujourdhui_global) ; ?>
                <div class="col-sm-5ths paddingleft0"><a href="javascript:void(0);" class="btn btn-default col-xs-12"
                                                         onclick="javascript:change_etat_filter_export(1);">Neuf <?php echo "(".$nbnew.")"; ?></a></div>
                <div class="col-sm-5ths paddingleft0"><a href="javascript:void(0);" class="btn btn-default col-xs-12"
                                                         onclick="javascript:change_etat_filter_export(0);">Revente <?php echo "(".$nboccas.")"; ?></a></div>
                <div class="col-sm-5ths paddingleft0"><a href="javascript:void(0);" class="btn btn-default col-xs-12"
                                                         onclick="javascript:change_etat_filter_export(2);">Service <?php echo "(".$nbservice.")"; ?></a>
                </div>
            </div>
            <div id="filter_content_keyword">
                <input type="text" class="form-control" id="keyword_input_export" value="" style="width: 75%; display: inline;"/>
                <button class="btn btn-success" id="btn_submit_keyword_filter">Chercher</button>
            </div>
        </div>
    </div>
</nav>