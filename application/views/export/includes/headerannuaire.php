<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php if(isset($currentpage) && $currentpage=="export_article") { ?>Article<?php } else {?>Annuaire<?php } ?></title>



    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <script type="text/javascript">

    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#filter_content_category').hide();
            $('#filter_content_date_id').show();
            $('#filter_content_keyword').hide();
            body_margin_top_height()

            $('#filter_category_link').click(function () {
                $('#filter_content_category').show();
                $('#filter_content_date_id').hide();
                $('#filter_content_keyword').hide();
                body_margin_top_height();
            });
            $('#filter_date_link').click(function () {
                $('#filter_content_category').hide();
                $('#filter_content_date_id').show();
                $('#filter_content_keyword').hide();
                body_margin_top_height();
            });
            $('#filter_keyword_link').click(function () {
                $('#filter_content_category').hide();
                $('#filter_content_date_id').hide();
                $('#filter_content_keyword').show();
                body_margin_top_height();
            });
            $('#filter_init_link').click(function () {
                window.location.reload();
            });
            $('#btn_submit_keyword_filter').click(function () {
                export_filter_submit_func();
            });
            $('#inputStringDepartementHidden').change(function () {
                //$('#inputStringDepartementHidden').attr('id','hidde');
                export_filter_submit_func();
            });
            $('#inputStringVilleHidden_partenaires').change(function () {
                //$('#inputStringDepartementHidden').attr('id','hidde');

                export_filter_submit_func();
            });
            $('#categorieCommercants').change(function () {
                //$('#inputStringDepartementHidden').attr('id','hidde');

                export_filter_submit_func();
            });
        });

        function change_categ_filter_export(idCateg=0) {
            $("#proximite_tiCategorie").val(idCateg);
            export_filter_submit_func();
        }
        function change_date_filter_export(idDate=0) {
            $("#proximite_tiCategorie").val($("#proximite_tiCategorie_init").val());
            $("#proximite_tiDatefilter").val('0');
            $("#proximite_tiDatefilter").val(idDate);
            export_filter_submit_func();
        }

        //function export_filter_submit_func(){
        //    $('#proximite_conteneur_list_id').html('<div style="text-align:center;"><img src="<?php //echo base_url();?>//application/resources/front/images/wait.gif" alt="loading...." id="img_loaging_agenda_x"/></div>');
        //    $("#frmRecherchePartenaire").submit();
        //
        //}

        function body_margin_top_height(){
            var width_ = $("#main_navbar_export").height() + 15;
            width_ = width_ + "px";
            //alert(width_);
            setTimeout(function(){ $("#proximite_conteneur_list_id").css("margin-top",width_); }, 500);
        }
     function reloadpage(){
         window.location.reload();
     }
        function export_filter_submit_func(){
            $('#proximite_conteneur_list_id').html('<div style="text-align:center;"><img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...." id="img_loaging_agenda_x"/></div>');
            var proximite_tiDepartement = $("#proximite_tiDepartement").val()+"_";
            var proximite_tiDeposant = $("#proximite_tiDeposant").val()+"_";
            var proximite_tiCategorie = $("#proximite_tiCategorie").val();
            var proximite_tiDossperso = $("#proximite_tiDossperso").val();
            var zCouleur = $("#zCouleur").val();
            var zCouleurTitre = $("#zCouleurTitre").val();
            var zCouleurTextBouton = $("#zCouleurTextBouton").val();
            var zCouleurBgBouton = $("#zCouleurBgBouton").val();
            var keyword_input_export = $("#keyword_input_export").val();
            var ville_filter = $("#inputStringVilleHidden_partenaires").val();
            $.get(
                '<?php  echo site_url("lannuaire/annuaire_perso/"); ?>',
                {
                    tiDepartement: proximite_tiDepartement,
                    tiDeposant: proximite_tiDeposant,
                    tiCategorie_filter: proximite_tiCategorie,
                    tiDossperso: proximite_tiDossperso,
                    zCouleur: zCouleur,
                    zCouleurTitre: zCouleurTitre,
                    zCouleurTextBouton: zCouleurTextBouton,
                    zCouleurBgBouton: zCouleurBgBouton,
                    keyword_input_exported: keyword_input_export,
                    contentonly: '1',
                    Villes:ville_filter

                },
                function (zReponse) {

                    $('#proximite_conteneur_list_id').html(zReponse);
                });
        }
    </script>
    <style type="text/css">
        <!--
#hidde{
    display:none;
}
        /*********william bootstrap customizer************/
        .padding0 {
            padding: 0 !important;
        }

        .padding15 {
            padding: 15px !important;
        }

        .padding30 {
            padding: 30px !important;
        }

        .paddingleft0 {
            padding-left: 0 !important;
        }

        .paddingleft15 {
            padding-left: 15px !important;
        }

        .paddingright0 {
            padding-right: 0 !important;
        }

        .paddingright15 {
            padding-right: 15px !important;
        }

        .paddingright30 {
            padding-right: 30px !important;
        }

        .paddingbottom0 {
            padding-bottom: 0 !important;
        }

        .paddingbottom15 {
            padding-bottom: 15px !important;
        }

        .paddingbottom30 {
            padding-bottom: 30px !important;
        }

        .paddingtop0 {
            padding-top: 0 !important;
        }

        .paddingtop5 {
            padding-top: 5px !important;
        }

        .paddingtop10 {
            padding-top: 10px !important;
        }

        .paddingbottom10 {
            padding-bottom: 10px !important;
        }

        .paddingtop15 {
            padding-top: 15px !important;
        }

        .paddingtop30 {
            padding-top: 30px !important;
        }

        .paddingtopbottom15 {
            padding-top: 15px;
            padding-bottom: 15px;
        }

        .margin0 {
            margin: 0 !important;
        }

        .marginleft0 {
            margin-left: 0 !important;
        }

        .marginleft15 {
            margin-left: 15px !important;
        }

        .marginright0 {
            margin-right: 0 !important;
        }

        .marginright15 {
            margin-right: 15px !important;
        }

        .marginbottom0 {
            margin-bottom: 0 !important;
        }

        .marginbottom5 {
            margin-bottom: 5px !important;
        }

        .marginbottom15 {
            margin-bottom: 15px !important;
        }

        .margintop0 {
            margin-top: 0 !important;
        }

        .margintop5 {
            margin-top: 5px !important;
        }

        .margintop10 {
            margin-top: 10px !important;
        }

        .margintop15 {
            margin-top: 15px !important;
        }

        .margintop30 {
            margin-top: 30px !important;
        }

        .justify {
            text-align: justify;
        }

        .textalignright {
            text-align: right;
        }

        .textalignleft {
            text-align: left;
        }

        .textaligncenter {
            text-align: center;
        }

        .width100p {
            width: 100% !important;
        }

        /*********william bootstrap customizer************/

        .col-xs-5ths,
        .col-sm-5ths,
        .col-md-5ths,
        .col-lg-5ths {
            position: relative;
            min-height: 1px;
            padding-right: 15px;
            padding-left: 15px;
        }

        .col-xs-5ths {
            width: 20%;
            float: left;
        }

        @media (max-device-width: 768px) {
            #filter_content_date_id .col-sm-5ths {
                padding: 0 !important;
            }
        }
        @media (min-width: 768px) {
            .col-sm-5ths {
                width: 20%;
                float: left;
            }
        }

        @media (min-width: 992px) {
            .col-md-5ths {
                width: 20%;
                float: left;
            }
        }

        @media (min-width: 1200px) {
            .col-lg-5ths {
                width: 20%;
                float: left;
            }
        }

        .export_content_container {
            background-color: #E5E5E5;
            border: 1px #eeeeee solid;
            margin-bottom: 15px;
            overflow: hidden;
        }

        .export_image_container {
        }

        .export_title {
            line-height: 20.00px;
            font-family: "Arial", sans-serif;
            font-style: normal;
            font-weight: 700;
            <?php if (isset($zCouleurTitre)) { ?>color: #<?php echo $zCouleurTitre;?>; <?php } else { ?>color: #3b579d;<?php } ?>
            background-color: transparent;
            text-decoration: none;
            font-variant: normal;
            font-size: 16.0px;
            vertical-align: 0;
        }

        .export_btn_more_custom {
            <?php if (isset($zCouleurBgBouton)) { ?>background-color: #<?php echo $zCouleurBgBouton;?>; <?php } else { ?>background-color: #333333;<?php } ?>;
            border-radius: 30px;
            <?php if (isset($zCouleurNbBtn) && intval($zCouleurNbBtn)==1) { ?>color: #ffffff; <?php } else { ?>color: #000000;<?php } ?>;
            font-size: 36px;
            padding: 0 15px;
            text-decoration: none;
        }
        .export_btn_more_custom:hover,
        .export_btn_more_custom:focus,
        .export_btn_more_custom:active
        {
            color: #fff;
        }

        .btn_custom_export_bg {
            <?php if (isset($zCouleurBgBouton)) { ?>background-color: #<?php echo $zCouleurBgBouton;?>; <?php } else { ?>background-color: #EFF1F2;<?php } ?>;
            <?php if (isset($zCouleurNbBtn) && intval($zCouleurNbBtn)==1) { ?>color: #ffffff; <?php } else { ?>color: #000000;<?php } ?>;
            border-radius: 15px;
        }

        .export_date_categ {
            font-weight: bold;
        }

        .export_subcateg {
            font-weight: bold;
        }
        .export_desc {
        }

        .export_btn_more {
        }

        .proximite_conteneur_filtre {
            text-align: left;
            padding: 15px 15px 0;
        }

        .export_filter_content {
            /*background-color: #000000;*/
            <?php if (isset($zCouleur)) { ?>background-color: #<?php echo $zCouleur;?>; <?php } else { ?>background-color: #000000;<?php } ?>;
            display: table;
            width: 100%;
        }

        .export_filter_content_items {
            <?php if (isset($zCouleurBgBouton)) { ?>background-color: #<?php echo $zCouleurBgBouton;?>; <?php } else { ?>background-color: #333333;<?php } ?>;
            <?php if (isset($zCouleurNbBtn) && intval($zCouleurNbBtn)==1) { ?>color: #ffffff; <?php } else { ?>color: #000000;<?php } ?>;
            font-family: Arial, sans-serif;
            font-weight: normal;
            text-decoration: none;
            text-align: center;
            text-transform: uppercase;
            font-style: normal;
            line-height: 18px;
            font-size: 18px;
            display: flow-root;
            cursor: pointer;
            min-height: 50px;
        }

        .export_filter_content_items div {
        <?php if (isset($zCouleurNbBtn) && intval($zCouleurNbBtn)==1) { ?>
            background-image: url(<?php echo base_url(); ?>/application/resources/sortez/images/bg_menu_filtre_export_white.png);
        <?php } else { ?>
            background-image: url(<?php echo base_url(); ?>/application/resources/sortez/images/bg_menu_filtre_export_black.png);
        <?php } ?>;
            background-repeat: no-repeat;
            background-position: right center;
        }

        @media (max-width: 768px) {
            /*.export_filter_content_items div {
                background-image: none;
            }*/
        }

        .export_filter_content_items div:last-child {
            background-image: none;
        }

        .export_categ_content {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 40px;
            background: rgba(0, 0, 0, 0.5);
            color: #FFFFFF;
            font-weight: normal;
            text-transform: uppercase;
            text-align: center;
            line-height: 40px;
        }

        @media (min-device-width: 2001px) {
            .export_content_container {
                height: 580px;
            }
        }
        @media (min-device-width: 1801px) and (max-width: 2000px) {
            .export_content_container {
                height: 550px;
            }
        }
        @media (min-device-width: 1601px) and (max-width: 1800px) {
            .export_content_container {
                height: 500px;
            }
        }

        @media (min-device-width: 1401px) and (max-width: 1600px) {
            .export_content_container {
                height: 450px;
            }
        }

        @media (min-device-width: 1200px) and (max-width: 1400px) {
            .export_content_container {
                height: 450px;
            }
        }

        @media (min-device-width: 1171px) and (max-width: 1200px) {
            .export_content_container {
                height: 470px;
            }
        }

        @media (min-device-width: 991px) and (max-width: 1170px) {
            .export_content_container {
                height: 460px;
            }
        }

        @media (min-device-width: 768px) and (max-width: 990px) {
            .export_content_container {
                height: 430px;
            }
        }

        .export_details_filter_content {
            font-family: Arial, sans-serif;
            font-weight: bold;
            text-decoration: none;
            text-align: center;
            font-style: normal;
            line-height: 18px;
            font-size: 14px;
            display: flow-root;
            cursor: pointer;
        }
        .export_details_filter_content .btn {
            font-size: 12px;
            margin-bottom: 15px;
        }
        #filter_content_date_id a:hover,
        #filter_content_date_id a:active,
        #filter_content_date_id a:focus
        {
            text-decoration: none;
        }

        .proximite_conteneur_list {
        <?php if (isset($zCouleur)) { ?>
            background-color: #<?php echo $zCouleur;?>;
        <?php } else { ?>
            background-color: #ffffff;
        <?php } ?>
        }
        body {
        <?php if (isset($zCouleur)) { ?>
            background-color: #<?php echo $zCouleur;?>;
        <?php } else { ?>
            background-color: #ffffff;
        <?php } ?>
        }
        .export_content_container {
            border: none;
        }

        -->
    </style>
    <!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-597edacf6ea83530"></script>


</head>

<body>


<div class="padding0">