<?php if (!isset($contentonly) || $contentonly != '1') { ?>

<?php $this->load->view('export/includes/headerannuaire'); ?>
<?php  $this->load->view('export/includes/filterannuaire'); ?>
<div class="proximite_conteneur_list col-lg-12 col-xs-12 paddingright0" id="proximite_conteneur_list_id">
    <?php } ?>

    <?php foreach ($toCommercant as $oCommercant) {

        $objasscommrubr = $this->sousRubrique->GetById($oCommercant->IdRubrique);

        $commercant_url_nom = $oCommercant->nom_url;
        $commercant_url_home = $commercant_url_nom;
        $thisss =& get_instance();
        $thisss->load->model("ion_auth_used_by_club");
        $ionauth_id = $thisss->ion_auth_used_by_club->get_ion_id_from_commercant_id($oCommercant->IdCommercant);

        $photoCommercant_path = "application/resources/front/photoCommercant/imagesbank/" . $ionauth_id . "/";
        $photoCommercant_path_old = "application/resources/front/photoCommercant/images/";
        ?>


        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 paddingleft0" >
            <div class="export_content_container col-lg-12 padding0" style="height: 460px !important;">
                <div class="export_image_container col-lg-12 padding0"  style="height: 226px!important;overflow: hi4">
                    <?php
                    $image_home_vignette = "";
                    if (isset($oCommercant->Photo1) && $oCommercant->Photo1 != "" && is_file($photoCommercant_path . $oCommercant->Photo1) == true) {
                        $image_home_vignette = $oCommercant->Photo1;
                    } else if ($image_home_vignette == "" && isset($oCommercant->Photo2) && $oCommercant->Photo2 != "" && is_file($photoCommercant_path . $oCommercant->Photo2) == true) {
                        $image_home_vignette = $oCommercant->Photo2;
                    } else if ($image_home_vignette == "" && isset($oCommercant->Photo3) && $oCommercant->Photo3 != "" && is_file($photoCommercant_path . $oCommercant->Photo3) == true) {
                        $image_home_vignette = $oCommercant->Photo3;
                    } else if ($image_home_vignette == "" && isset($oCommercant->Photo4) && $oCommercant->Photo4 != "" && is_file($photoCommercant_path . $oCommercant->Photo4) == true) {
                        $image_home_vignette = $oCommercant->Photo4;
                    } else if ($image_home_vignette == "" && isset($oCommercant->Photo5) && $oCommercant->Photo5 != "" && is_file($photoCommercant_path . $oCommercant->Photo5) == true) {
                        $image_home_vignette = $oCommercant->Photo5;
                    } else if ($image_home_vignette == "" && isset($oCommercant->Photo1) && $oCommercant->Photo1 != "" && is_file($photoCommercant_path_old . $oCommercant->Photo1) == true) {
                        $image_home_vignette = $oCommercant->Photo1;
                    } else if ($image_home_vignette == "" && isset($oCommercant->Photo2) && $oCommercant->Photo2 != "" && is_file($photoCommercant_path_old . $oCommercant->Photo2) == true) {
                        $image_home_vignette = $oCommercant->Photo2;
                    } else if ($image_home_vignette == "" && isset($oCommercant->Photo3) && $oCommercant->Photo3 != "" && is_file($photoCommercant_path_old . $oCommercant->Photo3) == true) {
                        $image_home_vignette = $oCommercant->Photo3;
                    } else if ($image_home_vignette == "" && isset($oCommercant->Photo4) && $oCommercant->Photo4 != "" && is_file($photoCommercant_path_old . $oCommercant->Photo4) == true) {
                        $image_home_vignette = $oCommercant->Photo4;
                    } else if ($image_home_vignette == "" && isset($oCommercant->Photo5) && $oCommercant->Photo5 != "" && is_file($photoCommercant_path_old . $oCommercant->Photo5) == true) {
                        $image_home_vignette = $oCommercant->Photo5;
                    }
                    ?>
                    <a onclick='javascript:window.open("<?php echo site_url($commercant_url_home); ?>/presentation", "<?php echo $commercant_url_home; ?>", "width=1045, height=800, scrollbars=yes");'
                       href='javascript:void(0);' >
                        <?php
                        if ($image_home_vignette != "") {
                            if (isset($image_home_vignette) && $image_home_vignette != "" && is_file($photoCommercant_path . $image_home_vignette) == true)
                                echo '<img style="max-height:396px" src="' . base_url() . $photoCommercant_path . $image_home_vignette . '" width="100%"/>';
                            else echo '<img style="max-height:396px" src="' . base_url() . $photoCommercant_path_old . $image_home_vignette . '" width="100%"/>';
                        } else {
                            $image_home_vignette_to_show = GetImagePath("front/") . "/no_image_annuaire.png";
                            echo '<img src="' . $image_home_vignette_to_show . '" width="100%"/>';
                        }
                        ?>
                    </a>
                    <div style="position:absolute; bottom:0; width:100%; height:40px; background:rgba(0, 0, 0, 0.5); color:#FFFFFF; font-weight: normal; text-transform:uppercase; text-align:center; line-height:40px;"><?php if ($objasscommrubr) echo $objasscommrubr->Nom; ?></div>
                </div>
                <div class="col-lg-12 text-center eport_txt_content paddingbottom15" style="background-color: #E5E5E5">
                    <div class="export_subcateg col-lg-12 padding0 paddingtop15">
                        <?php echo $oCommercant->NomSociete; ?>
                    </div>
                    <div class="export_title col-lg-12 padding0">
                        <?php echo $oCommercant->ville; ?>
                    </div>
                    <div class="export_address_item col-lg-12 padding0">
                        <div class="col-lg-12 padding0"
                             style="padding-top:15px !important; text-align:justify; height: 100px!important;">
                            <span style='background-color: transparent;
                                 color: #000000;
                                 font-family: "Arial",sans-serif;
                                 font-size: 14px;
                                 font-style: normal;
                                 font-variant: normal;
                                 font-weight: normal;
                                 line-height: 19px;
                                 text-decoration: none;
                                 vertical-align: 0;
                /*border:solid 2px;*'>
                            <?php echo truncate(strip_tags($oCommercant->Caracteristiques), 200, $etc = " ... ") ?>
                             </span>
                        </div>
                    </div>
                    <div class="export_btn_more">
                        <a onclick='javascript:window.open("<?php echo site_url($commercant_url_home); ?>/presentation", "<?php echo $commercant_url_home; ?>", "width=1045, height=800, scrollbars=yes");'
                           href='javascript:void(0); title="D&eacute;tails'><img
                                    src="<?php echo GetImagePath("privicarte/"); ?>/plus_infos_black.png"
                                    alt="partenaire" style="float: right" /></a>
                    </div>
                </div>
            </div>


        </div>


    <?php } ?>
</div>

    <?php if (!isset($contentonly) || $contentonly != '1') { ?>
<?php $this->load->view('export/includes/footer'); ?>
<?php } ?>
