<style type="text/css">
@media screen and (max-width: 990px) {
  .export_content_container .export_image_container {
    height: 225px !important;
  }
}
@media screen and (min-width: 991px) {
    .export_content_container .export_image_container {
    height: 250px !important;
  }
}
</style>
<?php $data['empty'] = null; ?>

<?php if (!isset($contentonly)||$contentonly!='1') { ?>
<?php $this->load->view('export/includes/header', $data); ?>


<?php $this->load->view('export/includes/filteragenda', $data); ?>



    <div class="proximite_conteneur_list col-lg-12 col-xs-12 paddingright0" id="proximite_conteneur_list_id">
<?php } ?>

        <?php foreach ($toAgenda as $oAgenda) { ?>


            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 paddingleft0 agenda">
                <div class="export_content_container col-lg-12 padding0">
                    <div class="export_image_container col-lg-12 padding0" style="overflow: hidden">
                        <a href="<?php echo base_url(); ?>agenda/agenda_perso_details/<?php echo $oAgenda->id; ?>/<?php if (isset($zCouleurBgBouton)) echo '?zCouleurBgBouton='.$zCouleurBgBouton; ?><?php if (isset($zCouleur)) echo '&zCouleur='.$zCouleur; ?><?php if (isset($zCouleurTitre)) echo '&zCouleurTitre='.$zCouleurTitre; ?>">
                            <?php
                            $thisss =& get_instance();
                            $thisss->load->model("ion_auth_used_by_club");
                            $ionauth_id = $thisss->ion_auth_used_by_club->get_ion_id_from_commercant_id($oAgenda->IdCommercant);
                            $photoCommercant_path = "application/resources/front/photoCommercant/imagesbank/" . $ionauth_id . "/";
                            $photoCommercant_path_old = "application/resources/front/photoCommercant/images/";
                            $image_home_vignette = "";
                            $photo1com=$this->mdl_agenda->get_photo1com($oAgenda->IdCommercant);
                            $isfile='application/resources/front/photoCommercant/images/'.$photo1com->Photo1;
                            $isfile2='application/resources/front/photoCommercant/imagesbank/'. $oAgenda->IdUsers_ionauth . "/".$photo1com->Photo1;
                            $photo1path2= base_url().'application/resources/front/photoCommercant/imagesbank/'. $ionauth_id . "/".$photo1com->Photo1;
                            $photo1path= base_url().'application/resources/front/photoCommercant/images/'.$photo1com->Photo1;
                            $photoCommercant_path2= "application/resources/front/photoCommercant/imagesbank/scrapped_image/";
                            $photoCommercant_path_old = "application/resources/front/images/article/photoCommercant/";

                            if (isset($oAgenda->photo1) && $oAgenda->photo1 != "" && is_file($photoCommercant_path . $oAgenda->photo1) == true) {
                                $image_home_vignette = $oAgenda->photo1;
                            }elseif (isset($photoCommercant_path2) AND is_file($photoCommercant_path2 . $oAgenda->photo1) ==true){
                                $image_home_vignette = $oAgenda->photo1;
                            } else if ($image_home_vignette == "" && isset($oAgenda->photo2) && $oAgenda->photo2 != "" && is_file($photoCommercant_path . $oAgenda->photo2) == true) {
                                $image_home_vignette = $oAgenda->photo2;
                            } else if ($image_home_vignette == "" && isset($oAgenda->photo3) && $oAgenda->photo3 != "" && is_file($photoCommercant_path . $oAgenda->photo3) == true) {
                                $image_home_vignette = $oAgenda->photo3;
                            } else if ($image_home_vignette == "" && isset($oAgenda->photo4) && $oAgenda->photo4 != "" && is_file($photoCommercant_path . $oAgenda->photo4) == true) {
                                $image_home_vignette = $oAgenda->photo4;
                            } else if ($image_home_vignette == "" && isset($oAgenda->photo5) && $oAgenda->photo5 != "" && is_file($photoCommercant_path . $oAgenda->photo5) == true) {
                                $image_home_vignette = $oAgenda->photo5;
                            } else if ($image_home_vignette == "" && isset($oAgenda->photo1) && $oAgenda->photo1 != "" && is_file($photoCommercant_path_old . $oAgenda->photo1) == true) {
                                $image_home_vignette = $oAgenda->photo1;
                            } else if ($image_home_vignette == "" && isset($oAgenda->photo2) && $oAgenda->photo2 != "" && is_file($photoCommercant_path_old . $oAgenda->photo2) == true) {
                                $image_home_vignette = $oAgenda->photo2;
                            } else if ($image_home_vignette == "" && isset($oAgenda->photo3) && $oAgenda->photo3 != "" && is_file($photoCommercant_path_old . $oAgenda->photo3) == true) {
                                $image_home_vignette = $oAgenda->photo3;
                            } else if ($image_home_vignette == "" && isset($oAgenda->photo4) && $oAgenda->photo4 != "" && is_file($photoCommercant_path_old . $oAgenda->photo4) == true) {
                                $image_home_vignette = $oAgenda->photo4;
                            } else if ($image_home_vignette == "" && isset($oAgenda->photo5) && $oAgenda->photo5 != "" && is_file($photoCommercant_path_old . $oAgenda->photo5) == true) {
                                $image_home_vignette = $oAgenda->photo5;
                            }

                            if ($image_home_vignette != "") {
                                if (isset($image_home_vignette) && $image_home_vignette != "" && is_file($photoCommercant_path . $image_home_vignette) == true)
                                    echo '<img src="' . base_url() . $photoCommercant_path . $image_home_vignette . '" width="100%" "/>';
                                else echo '<img src="' . base_url() . $photoCommercant_path2 . $image_home_vignette . '" width="100%" "/>';
                            } elseif (is_file($isfile)) {
                                $image_home_vignette_to_show = $photo1path;
                                echo '<img src="' . $image_home_vignette_to_show . '" width="100%" "/>';
                            }elseif (is_file($isfile2)){
                                $image_home_vignette_to_show = $photo1path2;
                                echo '<img src="' . $image_home_vignette_to_show . '" width="100%" "/>';
                            } else {
                                $image_home_vignette_to_show = GetImagePath("front/") . "/no_image_annuaire.png";
                                echo '<img src="' . $image_home_vignette_to_show . '" width="100%" "/>';
                            }
                            ?>
                        </a>
                        <div class="export_categ_content"><?php echo $oAgenda->category; ?></div>
                    </div>


                    <div style="height: 200px" class="col-lg-12 text-center eport_txt_content paddingbottom15"><?php //var_dump($oAgenda);?>
                        <div class="export_subcateg col-lg-12 padding0 paddingtop15">
                            <?php echo $oAgenda->subcateg; ?>
                        </div>
                        <div class="export_title col-lg-12 padding0">
                            <?php echo $oAgenda->nom_manifestation; ?>
                        </div>
                        <div class="export_date_categ col-lg-12 padding0">
                            <?php
                                if ($oAgenda->datetime_debut == $oAgenda->datetime_fin) {
                                    if (isset($oAgenda->datetime_debut) && $oAgenda->datetime_debut != "0000-00-00") echo "Le " . translate_date_to_fr($oAgenda->datetime_debut);
                                    if (isset($oAgenda->datetime_heure_debut) && $oAgenda->datetime_heure_debut != "0:00"   && $oAgenda->datetime_heure_debut != "") echo " à " . str_replace(":", "h", $oAgenda->datetime_heure_debut);
                                } else {
                                    if (isset($oAgenda->datetime_debut) && $oAgenda->datetime_debut != "0000-00-00") echo "Du " . translate_date_to_fr($oAgenda->datetime_debut);
                                    if (isset($oAgenda->datetime_fin) && $oAgenda->datetime_fin != "0000-00-00") {
                                        if (isset($oAgenda->datetime_debut) && $oAgenda->datetime_debut != "0000-00-00") echo "<br/>au " . translate_date_to_fr($oAgenda->datetime_fin);
                                        else echo " Jusqu'au " . translate_date_to_fr($oAgenda->datetime_fin);
                                    }
                                    if (isset($oAgenda->datetime_heure_debut) && $oAgenda->datetime_heure_debut != "0:00"  && $oAgenda->datetime_heure_debut != "") echo " à " . str_replace(":", "h", $oAgenda->datetime_heure_debut);
                                }
                                ?>
                        </div>
                        <div class="export_address_item col-lg-12 padding0">
                            <div>
                                <?php echo $oAgenda->adresse_localisation;if ($oAgenda->IdCommercant=="300221"){echo '-Théatre de Grasse';}//." ".$oAgenda->adresse_localisation ?>
                            </div>
                            <div>
                                <?php echo $oAgenda->ville; ?>
                            </div>
                        </div>

                    </div>
                    <div class="" style="position: absolute; right: 15px; bottom: 15px;">
                        <div class="export_btn_more">
                            <a href="<?php echo base_url(); ?>agenda/agenda_perso_details/<?php echo $oAgenda->id; ?>/<?php if (isset($zCouleurBgBouton)) echo '?zCouleurBgBouton='.$zCouleurBgBouton; ?><?php if (isset($zCouleurNbBtn)) echo '&zCouleurNbBtn='.$zCouleurNbBtn; ?><?php if (isset($zCouleurTextBouton)) echo '&zCouleurTextBouton='.$zCouleurTextBouton; ?><?php if (isset($zCouleur)) echo '&zCouleur='.$zCouleur; ?><?php if (isset($zCouleurTitre)) echo '&zCouleurTitre='.$zCouleurTitre; ?>"
                               title="D&eacute;tails" style="text-decoration:none;" class="btn export_btn_more_custom">
                                <!--<img src="<?php echo GetImagePath("privicarte/"); ?>/plus_infos_black.png"
                                     alt="details"/>-->
                                +
                            </a>
                        </div>
                    </div>

                </div>
            </div>


        <?php } ?>

<?php if (!isset($contentonly)||$contentonly!='1') { ?>
    </div>


<?php $this->load->view('export/includes/footer', $data); ?>
<?php } ?>
