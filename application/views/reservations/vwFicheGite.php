<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style type="text/css">
        .btnmenu{
            text-align: center;
            margin-top: 39px;
            background-color: #af4106;
            width:300px;
            padding-bottom: 10px;
            padding-top:10px;
            color:white;
            font-size:20px;


        }
        .titre{
            text-align: center;
            background-color: #af4106;
            color: white;
            height: 40px;
            line-height: 2.5;
        }
        .contenu{
            background-color: #e1e1e1;
            width: 700px;
            padding-top:10px;
            padding-bottom: 10px;
            margin:auto;
        }
        .btnvalide{
            width: 700px;
        }
        a{
            text-decoration: none;
        }
    </style>
</head>
<body>
<div class="container">

    <div class="row justify-content-center">
        <a href="<?php echo site_url();?>"><div class="col-sm-12 btnmenu">RETOUR SITE</div></a>
    </div>
    <div class="row justify-content-center">
        <a href="<?php echo site_url('auth/login');?>"><div class="col-sm-12 btnmenu" style="margin-top:20px;!important;">RETOUR MENU</div></a>
    </div>
    <div class="row justify-content-center">
        <a href="<?php echo site_url('front/Plat_du_jour/listePlatDuJour/');?>"><div class="col-sm-12 btnmenu" style="margin-top:20px;!important;">RETOUR LISTE</div></a>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-12 titre" style="width:700px;margin:auto;!important;margin-top: 30px">GITE</div>
            <div class="contenu col-sm-12" >
                <form action="<?php echo site_url('admin/Reservations/savegite'); ?>" method="POST">

                    <input type="hidden" name="gite[IdCommercant]" value=" <?php echo $idcom; ?>">
                    <?php if (isset($infogite) AND $infogite !=null){ ?><input type="hidden" name="gite[id]" value=" <?php if (isset($infogite) AND $infogite !=null){echo $infogite->id;} ?>"><?php } ?>
                    <table>
                        <tr>
                            <td>Nom<td><td><input class="form-control" value="<?php if (isset($infogite) AND $infogite !=null){echo $infogite->nom;} ?>"  type="text" name="gite[nom]" ><td>
                        </tr>
                        <tr>
                            <td>date debut<td><td><input class="form-control" value="<?php if (isset($infogite) AND $infogite !=null){echo $infogite->date_debut;} ?>" type="date" name="gite[date_debut]" ><td>
                        </tr>
                        <tr>
                            <td>date fin<td><td><input class="form-control" value="<?php if (isset($infogite) AND $infogite !=null){echo $infogite->date_fin;} ?>" type="date" name="gite[date_fin]" ><td>
                        </tr>

                        <tr>
                            <td>Url<td><td><input class="form-control" value="<?php if (isset($infogite) AND $infogite !=null){echo $infogite->url;} ?>" type="text" name="gite[url]"><td>
                        </tr>
                        <tr>
                            <td>Actif<td><td><select class="form-control" name="gite[IsActif]" style="width:250px;height;padding-top:5px;padding-bottom:5px;">
                                    <option  value="0" >Inactif</option>
                                    <option <?php if (isset($infogite) AND $infogite !=null AND $infogite->IsActif ==1 ){echo "selected";} ?> value="1" >Actif</option>

                                </select><td>
                        </tr>
                    </table>
            </div>

            <div class="col-sm-12 contenu text-center pt-4 pb-5" >
                <button onclick="return confirm('Valider les modifications?')" class="btn btn-success" type="submit"> Valider</button>
            </div>

        </div>

    </div>
</div>

</div>



</form>
</div>

</div>
</body>
</html>