<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="keywords" content="sorties, loisirs, concerts, spectacles, théâtres, alpes-maritimes, monaco, menton, nice, cannes, antibes, Théâtres, concerts, spectacles, animations, magazine, sortez, sortir, cannes, alpes-maritimes">
    <meta name="description" content="Le Magazine Sortez : l'essentiel des sorties et des loisirs sur les Alpes-Maritimes et Monaco, animations, concerts, spectacles, vie locale...">
    <meta name="author" content="Priviconcept">

    <meta property="fb:app_id" content="324903384595223" />
    <!--<meta property="fb:app_id" content="1292726270756136" />-->
    <meta property="og:locale" content="fr_FR" />
    <meta property="og:title" content="<?php if (isset($zTitle) && $zTitle != "" && $zTitle != NULL) echo $zTitle; else echo "Le Magazine Sortez"; ?>" />
    <meta property="og:description" content="<?php if (isset($zDescription) && $zDescription != "" && $zDescription != NULL) echo $zDescription; else echo "Sortez : L'essentiel des sorties et des loisirs Alpes-Maritimes et Monaco"; ?>" />
    <meta property="og:url" content='<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>' />
    <meta property="og:site_name" content="Le Magazine Sortez : L'essentiel des sorties et des loisirs Alpes-Maritimes et Monaco" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="<?php if (isset($zMetaImage) && $zMetaImage != "" && $zMetaImage != NULL) echo $zMetaImage; else echo "https://www.sortez.org/application/resources/sortez/images/logo.png"; ?>" />

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@Sortez_org" />
    <meta name="twitter:creator" content="@techsortez"/>
    <meta name="twitter:title" content="<?php if (isset($zTitle) && $zTitle != "" && $zTitle != NULL) echo $zTitle; else echo "Le Magazine Sortez"; ?>" />
    <meta name="twitter:description" content="<?php if (isset($zDescription) && $zDescription != "" && $zDescription != NULL) echo $zDescription; else echo "Sortez : L'essentiel des sorties et des loisirs Alpes-Maritimes et Monaco"; ?>" />
    <meta name="twitter:image" content="<?php if (isset($zMetaImage) && $zMetaImage != "" && $zMetaImage != NULL) echo $zMetaImage; else echo "https://www.sortez.org/application/resources/sortez/images/logo.png"; ?>" />

    <link rel="icon" href="images/favicon.ico">
    <?php if (!isset($data)) $data['data_init'] = true; ?>

    <title><?php if (isset($zTitle) && $zTitle != "" && $zTitle != NULL) echo $zTitle; else echo "Sortez"; ?></title>

    <!-- bootstrap core -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="<?php echo base_url();?>application/views/sortez_iframe/bootstrap/css/bootstrap-datepicker.standalone.min.css" rel="stylesheet" type="text/css">

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url();?>application/views/sortez_iframe/jquery/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url();?>application/views/sortez_iframe/bootstrap/js/popper.min.js"></script>
    <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
    <script src="<?php echo base_url();?>application/views/sortez_iframe/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>application/views/sortez_iframe/bootstrap/js/bootstrap-datepicker-fr.min.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script>
        $(function() {
            $("#skill_input").autocomplete({
                source: "<?php echo base_url('skills/autocompleteData'); ?>",
            });
        });
    </script>
    <script src="<?php echo base_url();?>application/views/sortez_iframe/jquery/jquery-migrate-1.4.1.min.js"></script>

    <?php $this->load->view("sortez_iframe/js/global_js", $data); ?>
    <script type="text/javascript" src="<?php echo base_url();?>application/views/sortez_iframe/js/global.js"></script>

    <!--<link rel="stylesheet" href="//code.jquery.com/mobile/1.5.0-alpha.1/jquery.mobile-1.5.0-alpha.1.min.css">
    <script src="//code.jquery.com/mobile/1.5.0-alpha.1/jquery.mobile-1.5.0-alpha.1.min.js"></script>-->

    <?php $this->load->view("sortez_iframe/css/global_css", $data); ?>
    <link href="<?php echo base_url();?>application/views/sortez_iframe/css/global.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>application/views/sortez_iframe/css/responsive_max_320.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>application/views/sortez_iframe/css/responsive_max_480.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>application/views/sortez_iframe/css/responsive_max_575.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>application/views/sortez_iframe/css/responsive_max_768.css" rel="stylesheet" type="text/css">
    <?php $this->load->view("sortez_iframe/css/responsive_max_768", $data); ?>
    <link href="<?php echo base_url();?>application/views/sortez_iframe/css/responsive_max_991.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>application/views/sortez_iframe/css/responsive_max_1199.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>application/views/sortez_iframe/css/responsive_min_1200.css" rel="stylesheet" type="text/css">

    <link href="<?php echo base_url();?>application/views/sortez_iframe/css/navbar.css" rel="stylesheet" type="text/css">
    <script type="application/javascript" src="https://www.sellsy.com/?_f=snippet&hash=JUQ4JTVFJTdEJTBEJUJDJTE3JTI5YiU1QiU5MiUwNyU3RDAlRTklMTFlJUE0VyUzQ3IlQzYlQzQlRDMlMDh6JTgxViU5MSVEMiVEQyVCNyU4OHglRUUlRTlpJTk2JUY4JTEwLiVGMTclOEZtZlolRjglMkYlOEIlQkMlQzctJTkyJTdGJUMwJTYwViUxNCVDNyVDMSUxNiVCOSVFQUM="></script>

</head>

<body class="<?php if (isset($localdata_IdVille)) echo $localdata_IdVille; ?>">

<?php
 
if(isset($pagecategory)!='packarticle')
    $this->load->view("privicarte/includes/facebook_login_js", $data);
?>