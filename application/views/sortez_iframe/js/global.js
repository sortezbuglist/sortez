$(document).ready(function () {
    var options = {
        format: 'dd/mm/yyyy',
        todayHighlight: true,
        autoclose: true,
        weekStart: 1,
        orientation: "top"
    };
    $('#inputStringDatedebutHidden_check').datepicker(options);
    $('#inputStringDatefinHidden_check').datepicker(options);

    var width_responsive = $(document).width();
    if (width_responsive < 992) {
        $("#span_leftcontener2013_form_partenaires").addClass("d-none");
        $("#span_leftcontener2013_form_bonplans").addClass("d-none");
        $("#span_leftcontener2013_form_annonces").addClass("d-none");
        $("#span_main_rubrique_banner_title").addClass("d-none");
        //$("#span_subcateg_main_rubrique_banner_title").addClass("d-none");
    }

    $("#navbar_brand_main_link").click(function () {
        $("#navbar_button_main_link").click();
    });
    $("#navbar_brand_rubriques_link").click(function () {
        $("#navbar_button_rubriques_link").click();
    });
    $("#navbar_brand_filter_link").click(function () {
        $("#navbar_button_filter_link").click();
    });

    // ===== Scroll to Top ====
    $(window).scroll(function () {
        if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
            $('#return-to-top').fadeIn(200);    // Fade in the arrow
        } else {
            $('#return-to-top').fadeOut(200);   // Else fade out the arrow
        }
    });
    $('#return-to-top').click(function () {      // When arrow is clicked
        $('body,html').animate({
            scrollTop: 0                       // Scroll to top of body
        }, 500);
    });
    $('#vsv_top_page_btn_main_btn').click(function () {      // When arrow is clicked
        $('body,html').animate({
            scrollTop: 0                       // Scroll to top of body
        }, 500);
    });
    $('#vsv_top_page_btn_main_txt').click(function () {      // When arrow is clicked
        $('body,html').animate({
            scrollTop: 0                       // Scroll to top of body
        }, 500);
    });
    //$('[data-toggle="popover"]').popover();


    var translation_a = $('#google_translate_element').html();
    $('#google_translate_element_2').html(translation_a);

});


function navbutton_category_link_mobile() {
    if (!$("#span_main_rubrique_banner_title").hasClass("d-none")) {
        $("#span_leftcontener2013_form_partenaires").addClass("d-none");
        $("#span_leftcontener2013_form_bonplans").addClass("d-none");
        $("#span_leftcontener2013_form_annonces").addClass("d-none");
        $("#span_main_rubrique_banner_title").addClass("d-none");
        $("#span_subcateg_main_rubrique_banner_title").addClass("d-none");
    } else {
        $("#span_leftcontener2013_form_partenaires").removeClass("d-none");
        $("#span_leftcontener2013_form_bonplans").removeClass("d-none");
        $("#span_leftcontener2013_form_annonces").removeClass("d-none");
        $("#span_main_rubrique_banner_title").removeClass("d-none");
        //$("#span_subcateg_main_rubrique_banner_title").removeClass("d-none");
    }
}
function navbutton_commune_link_mobile() {
    if ($("#span_ville_main_rubrique_banner_title").hasClass("d-none")) {
        $("#span_ville_main_rubrique_banner_title").removeClass("d-none");
        $("#span_ville_main_rubrique_banner_title").addClass("d-block");
    } else {
        $("#span_ville_main_rubrique_banner_title").removeClass("d-block");
        $("#span_ville_main_rubrique_banner_title").addClass("d-none");
    }
}