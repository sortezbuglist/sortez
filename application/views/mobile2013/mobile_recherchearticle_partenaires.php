<?php 
$data["zTitle"] = 'Recherche Article';
$this->load->view("mobile2013/includes/header.php", $data);
?>
<div style="width:100%; position:relative; float:left;">
<div style="background-image: url(<?php echo GetImagePath("privicarte/"); ?>/article_mobile_bg_title_search.jpg);
	background-repeat: no-repeat;
	background-position: center top;
	float: left;
	height: 114px;
	width: 100%;
	position: relative;
	background-color: #DA1181;">
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td style=" text-align:left;">
    <?php 
	$data["zTitle"] = 'Recherche Article';
	$this->load->view("mobile2013/includes/btn_sommaire.php", $data);
	?>
    </td>
    <td style=" text-align:right;">
    <?php 
	$this->load->view("mobile2013/includes/btn_back.php", $data);
	?>
    </td>
  </tr>
</table>
</div>

<div style="width:100%; height:auto; float:left;background-color: #FFFFFF; position:relative;">




<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/wpscripts2013/jquery-1.8.3.js"></script>
<script src="<?php echo GetJsPath("front/") ; ?>/js_geo/geo-min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">

$(document).ready(function() {
      
        $('#inputStringVilleHidden_agenda').change(function() {
            
            $("#span_categ_list_mobile").html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...."/>');
            var inputStringVilleHidden_agenda = $('#inputStringVilleHidden_agenda').val();     
            $.post(
              '<?php echo site_url("front/mobile/getcategoriearticle"); ?>',
              {inputStringVilleHidden_agenda: inputStringVilleHidden_agenda},
              function (zReponse)
              {
                  // alert (zReponse) ;
                  $('#span_categ_list_mobile').html(zReponse) ;       


             });
             //alert(inputStringHidden_main_form_mobile);
          });
		  
		  
		  $('#inputStringHidden_main_form_mobile').change(function() {
            
            $("#span_souscateg_list_mobile").html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="Uploading...."/>');
            var inputStringHidden_main_form_mobile = $('#inputStringHidden_main_form_mobile').val();  
			var inputStringVilleHidden_agenda = $('#inputStringVilleHidden_agenda').val();     
            $.post(
              '<?php echo site_url("front/mobile/getsouscategoriearticle"); ?>',
              {
			  	inputStringHidden_main_form_mobile: inputStringHidden_main_form_mobile,
			  	inputStringVilleHidden_agenda: inputStringVilleHidden_agenda
			  },
              function (zReponse)
              {
                  // alert (zReponse) ;
                  $('#span_souscateg_list_mobile').html(zReponse) ;       


             });
             //alert(inputStringHidden_main_form_mobile);
          });
		  
		  
			$("#inputzMotCle_agenda").focus(function(){
				var inputzMotCle_agenda = $("#inputzMotCle_agenda").val();
				if(inputzMotCle_agenda=='Mots clés') {
					$("#inputzMotCle_agenda").val('');
				}
			});
			$("#inputzMotCle_agenda").blur(function(){
				var inputzMotCle_agenda = $("#inputzMotCle_agenda").val();
				if(inputzMotCle_agenda=='') {
					$("#inputzMotCle_agenda").val('Mots clés');
				}
			});
          
          
          $("#btn_submit_search_partner_mobile").click(function(){
                var txtError = "";
				
				var inputzMotCle_agenda = $("#inputzMotCle_agenda").val();
                if(inputzMotCle_agenda=="" || inputzMotCle_agenda=="Mots clés") {
					//alert(inputzMotCle_agenda);
                    $("#inputzMotCle_agenda").val("");
                } 
				
				
				var inputStringHidden_main_form_mobile = $("#inputStringHidden_main_form_mobile").val();
                if(inputStringHidden_main_form_mobile=="" || inputStringHidden_main_form_mobile=="0") {
                    //txtError += "- Veuillez indiquer Votre inputStringHidden_main_form_mobile <br/>"; 
                    //alert("Veuillez indiquer Votre nom");
                    //$("#inputStringHidden_main_form_mobile").css('border-color', 'red');
                } else {
                    $("#inputStringHidden_main_form_mobile").css('border-color', '#E3E1E2');
                }

                var inputStringHidden_form_mobile = $("#inputStringHidden_form_mobile").val();
                if(inputStringHidden_form_mobile=="" || inputStringHidden_form_mobile=="0") {
                    //txtError += "- Veuillez indiquer Votre inputStringHidden_form_mobile <br/>"; 
                    //alert("Veuillez indiquer Votre nom");
                    //$("#inputStringHidden_form_mobile").css('border-color', 'red');
                } else {
                    $("#inputStringHidden_form_mobile").css('border-color', '#E3E1E2');
                }
				
				var inputStringVilleHidden_form_mobile = $("#inputStringVilleHidden_agenda").val();
               
                
				
				/*if ((inputStringHidden_form_mobile=="" || inputStringHidden_form_mobile=="0") && (inputStringVilleHidden_form_mobile=="" || inputStringVilleHidden_form_mobile=="0") && (inputStringHidden_main_form_mobile!="" && inputStringHidden_main_form_mobile!="0")) {
					$.post(
						  '<?php //echo site_url("front/mobile/getsouscategorie_agenda_to_mobile"); ?>',
						  {inputStringHidden_main_form_mobile: inputStringHidden_main_form_mobile,
						  inputStringVilleHidden_form_mobile: inputStringVilleHidden_form_mobile},
						  function (zReponse)
						  {
							  //alert (zReponse) ;
							  $("#inputStringHidden").val(","+zReponse);
							  $("#frmRechercheAgenda_mobile_page").submit();    
						 });
				} else {*/
					if(txtError == "") {
						//alert('ok');
						$("#inputStringHidden").val(","+inputStringHidden_main_form_mobile);
						$("#inputStringHidden_sub").val(","+inputStringHidden_form_mobile);
						$("#frmRechercheAgenda_mobile_page").submit();
						//$("#qsdf_x").click();
					}	
				//}
				
				
				
            });
			
			
			//click on "autour de ma position" button
			$("#btn_geolocalisation_mobile").click(function(){
				
				$("#loading_geolocalisation").html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...."/>');
				
				if(geo_position_js.init()){
					geo_position_js.getCurrentPosition(success_callback,error_callback,{enableHighAccuracy:true});
				}
				else{
					alert("Fonction non accessible");
				}
				
		   });
	
});


function success_callback(p)
	{
		//alert('lat='+p.coords.latitude.toFixed(2)+';lon='+p.coords.longitude.toFixed(2));
		$("#inputGeoLatitude").val(p.coords.latitude.toFixed(4));
		$("#inputGeoLongitude").val(p.coords.longitude.toFixed(4));
		
		var inputStringHidden_main_form_mobile = $("#inputStringHidden_main_form_mobile").val();
		var inputStringHidden_form_mobile = $("#inputStringHidden_form_mobile").val();
		$("#inputStringHidden").val(","+inputStringHidden_form_mobile);
		$("#inputFromGeolocalisation").val("1");
		var ttttt = $("#inputGeoLatitude").val();
		var tttttx = $("#inputGeoLongitude").val();
		//alert(ttttt+" / "+tttttx);
		$("#loading_geolocalisation").html("");
		$("#frmRechercheAgenda_mobile_page").submit();
	}
	
function error_callback(p)
	{
		alert('error='+p.message);
	}


function change_categ_getsouscateg_article(){
	
            $("#span_souscateg_list_mobile").html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="Uploading...."/>');
            var inputStringHidden_main_form_mobile = $('#inputStringHidden_main_form_mobile').val();  
			var inputStringVilleHidden_agenda = $('#inputStringVilleHidden_agenda').val();     
            $.post(
              '<?php echo site_url("front/mobile/getsouscategoriearticle"); ?>',
              {
			  	inputStringHidden_main_form_mobile: inputStringHidden_main_form_mobile,
			  	inputStringVilleHidden_agenda: inputStringVilleHidden_agenda
			  },
              function (zReponse)
              {
                  // alert (zReponse) ;
                  $('#span_souscateg_list_mobile').html(zReponse) ;       


             });
             //alert(inputStringHidden_main_form_mobile);
	}


</script>


<div class="main_mobile">
  
  <div id="loading_geolocalisation" class="loading_geolocalisation" style="text-align:center;"></div>
  	
    
    
<form name="frmRechercheAnnonce" id="frmRechercheAgenda_mobile_page" method="post" action="<?php echo site_url('article/liste/'); ?>">


<table width="100%" border="0" cellspacing="4" cellpadding="0" style="text-align:center;" align="center">
<input type="hidden" name="hdnFavoris" id="hdnFavoris" value="" />
<input type="hidden" name="from_mobile_search_page_partner" id="from_mobile_search_page_partner" value="1"/>
<input type="hidden" name="inputStringHidden" id="inputStringHidden" value=""/>
<input type="hidden" name="inputStringHidden_sub" id="inputStringHidden_sub" value=""/>

<?php 
$thisss =& get_instance();
$thisss->load->library('ion_auth');
$this->load->model("ion_auth_used_by_club");
if ($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
	$user_ion_auth = $thisss->ion_auth->user()->row();
	$iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
	$oUser = $this->user->GetById($iduser);
}
?>
<input type="hidden" name="inputFromGeolocalisation" id="inputFromGeolocalisation" value="0"/>
<input type="hidden" name="inputGeolocalisationValue" id="inputGeolocalisationValue" value="<?php if (isset($oUser)) echo $oUser->geodistance; else echo "10";?>"/>
<input type="hidden" name="inputGeoLatitude" id="inputGeoLatitude" value="0"/>
<input type="hidden" name="inputGeoLongitude" id="inputGeoLongitude" value="0"/>

  <tr>
    <td><a href="Javascript:void();" id="btn_geolocalisation_mobile"><img src="<?php echo GetImagePath("front/"); ?>/mobile2013/ar_alentours.png" width="288" height="64" alt="alentour" /></a>

    
    </td>
  </tr>
<tr>
    <td height="184" style="background-image:url(<?php echo GetImagePath("front/"); ?>/mobile2013/ar_multi.png); background-position:center top; background-repeat:no-repeat;">
    
    <?php 
	$this_session =& get_instance();
	$this_session->load->library('session');
	$session_inputStringQuandHidden_verification = $this_session->session->userdata('inputStringQuandHidden_x');
	$session_zMotCle_verification = $this_session->session->userdata('zMotCle_x');
	?>
    
    <div style="margin-bottom:5px; margin-top:5px;">
    <input type="text" name="zMotCle" id="inputzMotCle_agenda" value="<?php if (isset($session_zMotCle_verification) && $session_zMotCle_verification != "") echo $session_zMotCle_verification; else echo "Mots clés";?>" style="width:245px;"/>
    </div>
	


<?php
$this_session_localdata =& get_instance();
$this_session_localdata->load->library('session');
$localdata_value = $this_session_localdata->session->userdata('localdata');
$localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
if((isset($localdata_value) && $localdata_value!="")||(isset($localdata_IdVille) && $localdata_IdVille!="")){} else {
?>

	<?php 
	$this_session =& get_instance();
	$this_session->load->library('session');
	$session_iVilleId_verification = $this_session->session->userdata('iVilleId_x');
	?>
    <select id="inputStringVilleHidden_agenda" name="inputStringVilleHidden" size="1"  style="width:250px; display:none;" >
        <option value="0">Toutes les Communes</option>
        <?php foreach($toVille as $oVille){ ?>
            <option value="<?php echo $oVille->IdVille ; ?>"
            <?php if (isset($session_iVilleId_verification) && $session_iVilleId_verification==$oVille->IdVille) {?>selected<?php }?>
            ><?php echo $oVille->Nom." (".$oVille->nb_article.")" ; ?></option>
        <?php } ?>
    </select>
    
<?php
}
?>    
    
    
    <div id="span_categ_list_mobile" class="span_categ_list_mobile" style="margin-bottom:5px; margin-top:5px;">
    <select type="inputStringHidden_main" name="inputStringHidden_main" id="inputStringHidden_main_form_mobile" style="width:250px">
        <option value="0">Toutes les rubriques</option>
        <?php  foreach($toRubriquePrincipale as $oCategorie_main){ ?>
            <option value="<?php  echo $oCategorie_main->agenda_categid ; ?>"><?php  echo $oCategorie_main->category ; ?>&nbsp;(<?php  echo $oCategorie_main->nb_article ; ?>)</option>
        <?php  } ?>
    </select>
    </div>
    
    
    
    <div id="span_souscateg_list_mobile" class="span_souscateg_list_mobile" style="margin-bottom:5px;">
    <select type="inputStringHidden_sous" name="inputStringHidden_sous" id="inputStringHidden_form_mobile" style="width:250px">
        <option value="0">Toutes les sous-rubriques</option>
        <?php  foreach($toCategorie_sub as $oCategorie){ ?>
            <option value="<?php  echo $oCategorie->agenda_subcategid ; ?>"><?php  echo $oCategorie->subcateg ; ?>&nbsp;(<?php  echo $oCategorie->nb_article ; ?>)</option>
        <?php  } ?>
    </select>
    </div>
    
    
    <!--<div id="span_categ_list_mobile" class="span_categ_list_mobile" style="margin-bottom:5px; margin-top:5px;">
    
    <select id="inputStringQuandHidden_to_check" size="1" name="inputStringQuandHidden"  style="width:250px">
        <option value="0" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="0") {?>selected<?php // }?>>Toutes les dates <?php // if (isset($toAgndaTout_global)) echo "(".$toAgndaTout_global.")"; else echo "(0)"; ?></option>
        <option value="101" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="101") {?>selected<?php // }?>>Aujourd'hui <?php // if (isset($toAgndaAujourdhui_global)) echo "(".$toAgndaAujourdhui_global.")"; else echo "(0)"; ?></option>
        <option value="202" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="202") {?>selected<?php // }?>>Ce Week-end <?php // if (isset($toAgndaWeekend_global)) echo "(".$toAgndaWeekend_global.")"; else echo "(0)"; ?></option>
        <option value="303" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="303") {?>selected<?php // }?>>Cette semaine <?php // if (isset($toAgndaSemaine_global)) echo "(".$toAgndaSemaine_global.")"; else echo "(0)"; ?></option>
        <option value="404" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="404") {?>selected<?php // }?>>Semaine prochaine <?php // if (isset($toAgndaSemproch_global)) echo "(".$toAgndaSemproch_global.")"; else echo "(0)"; ?></option>
        <option value="505" <?php // // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="505") {?>selected<?php // // }?>>Ce mois <?php // // if (isset($toAgndaMois_global)) echo "(".$toAgndaMois_global.")"; else echo "(0)"; ?></option>
        <option value="01" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="01") {?>selected<?php // }?>>Janvier <?php // if (isset($toAgndaJanvier_global)) echo "(".$toAgndaJanvier_global.")"; else echo "(0)"; ?></option>
        <option value="02" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="02") {?>selected<?php // }?>>Février <?php // if (isset($toAgndaFevrier_global)) echo "(".$toAgndaFevrier_global.")"; else echo "(0)"; ?></option>
        <option value="03" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="03") {?>selected<?php // }?>>Mars <?php // if (isset($toAgndaMars_global)) echo "(".$toAgndaMars_global.")"; else echo "(0)"; ?></option>
        <option value="04" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="04") {?>selected<?php // }?>>Avril <?php // if (isset($toAgndaAvril_global)) echo "(".$toAgndaAvril_global.")"; else echo "(0)"; ?></option>
        <option value="05" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="05") {?>selected<?php // }?>>Mai <?php // if (isset($toAgndaMai_global)) echo "(".$toAgndaMai_global.")"; else echo "(0)"; ?></option>
        <option value="06" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="06") {?>selected<?php // }?>>Juin <?php // if (isset($toAgndaJuin_global)) echo "(".$toAgndaJuin_global.")"; else echo "(0)"; ?></option>
        <option value="07" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="07") {?>selected<?php // }?>>Juillet <?php // if (isset($toAgndaJuillet_global)) echo "(".$toAgndaJuillet_global.")"; else echo "(0)"; ?></option>
        <option value="08" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="08") {?>selected<?php // }?>>Août <?php // if (isset($toAgndaAout_global)) echo "(".$toAgndaAout_global.")"; else echo "(0)"; ?></option>
        <option value="09" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="09") {?>selected<?php // }?>>Septembre <?php // if (isset($toAgndaSept_global)) echo "(".$toAgndaSept_global.")"; else echo "(0)"; ?></option>
        <option value="10" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="10") {?>selected<?php // }?>>Octobre <?php // if (isset($toAgndaOct_global)) echo "(".$toAgndaOct_global.")"; else echo "(0)"; ?></option>
        <option value="11" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="11") {?>selected<?php // }?>>Novembre <?php // if (isset($toAgndaNov_global)) echo "(".$toAgndaNov_global.")"; else echo "(0)"; ?></option>
        <option value="12" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="12") {?>selected<?php // }?>>Décembre <?php // if (isset($toAgndaDec_global)) echo "(".$toAgndaDec_global.")"; else echo "(0)"; ?></option>
    </select>
    
    </div>-->
    
    </td>
  </tr>
  
  <tr>
    <td>
    <a href="Javascript:void();" id="btn_submit_search_partner_mobile">
    <img src="<?php echo GetImagePath("front/"); ?>/mobile2013/ar_validation.png" width="288" height="57" alt="validation" /></a>
    </td>
  </tr>
  
</table>
</form>
    
    
    
    
  </div>








</div>

</div>
<?php 
$this->load->view("mobile2013/includes/footer.php", $data);
?>

