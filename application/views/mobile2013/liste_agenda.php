<?php
if (isset($_GET['content_only_list']) && $_GET['content_only_list'] == '1') {
} else {

$data["zTitle"] = 'Mobile page';
$this->load->view("mobile2013/includes/header.php", $data);
?>
<div style="width:100%; height:auto; float:left;background-color: #FFFFFF;">
    <div style="background-image: url(<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp011a349f_06.png);
        background-repeat: no-repeat;
        background-position: center top;
        float: left;
        height: 90px;
        width: 100%;
        position: relative;
        background-color: #710036;">
        <table width="100%" border="0" cellspacing="5" cellpadding="5" style="height:100%;">
            <tr>
                <td style=" text-align:left;">
                    <?php
                    $data["zTitle"] = 'Mobile page';
                    $this->load->view("mobile2013/includes/btn_sommaire.php", $data);
                    ?>
                </td>
                <td style=" text-align:right;">
                    <?php
                    $this->load->view("mobile2013/includes/btn_back.php", $data);
                    ?>
                </td>
            </tr>
            <tr>
                <td><a href="<?php echo site_url("front/mobile/rechercheagenda/"); ?>"><img
                            src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp132903f3_06.png"
                            width="189" height="27" alt="recherche"/></a></td>
                <td style="text-align:right;">

                    <form name="frmRecherchePartenaire" id="frmRecherchePartenaire" method="post"
                          action="<?php echo site_url("agenda/liste"); ?>">
                        <?php
                        $this_session =& get_instance();
                        $this_session->load->library('session');
                        $session_inputStringQuandHidden_verification = $this_session->session->userdata('inputStringQuandHidden_x');
                        $session_zMotCle_verification = $this_session->session->userdata('zMotCle_x');
                        ?>
                        <input name="inputStringHidden" id="inputStringHidden" type="hidden" value="">
                        <input name="inputStringHidden_sub" id="inputStringHidden_sub" type="hidden" value="">
                        <input name="inputStringOrderByHidden" id="inputStringOrderByHidden" type="hidden" value="">
                        <input name="inputStringVilleHidden" id="inputStringVilleHidden" type="hidden" value="0"/>
                        <select id="inputStringQuandHidden" size="1" name="inputStringQuandHidden" style="width: auto;"
                                onChange="javascript:document.frmRecherchePartenaire.submit();">
                            <option value="0"
                                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "0") { ?>selected<?php } ?>>
                                Toutes <?php if (isset($toAgndaTout_global)) echo "(" . $toAgndaTout_global . ")"; else echo "(0)"; ?></option>
                            <option value="101"
                                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "101") { ?>selected<?php } ?>>
                                Aujourd'hui <?php if (isset($toAgndaAujourdhui_global)) echo "(" . $toAgndaAujourdhui_global . ")"; else echo "(0)"; ?></option>
                            <option value="202"
                                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "202") { ?>selected<?php } ?>>
                                Ce
                                Week-end <?php if (isset($toAgndaWeekend_global)) echo "(" . $toAgndaWeekend_global . ")"; else echo "(0)"; ?></option>
                            <option value="303"
                                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "303") { ?>selected<?php } ?>>
                                Cette
                                semaine <?php if (isset($toAgndaSemaine_global)) echo "(" . $toAgndaSemaine_global . ")"; else echo "(0)"; ?></option>
                            <option value="404"
                                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "404") { ?>selected<?php } ?>>
                                Semaine
                                prochaine <?php if (isset($toAgndaSemproch_global)) echo "(" . $toAgndaSemproch_global . ")"; else echo "(0)"; ?></option>
                            <!--<option value="505" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="505") {?>selected<?php // }?>>Ce mois <?php // if (isset($toAgndaMois_global)) echo "(".$toAgndaMois_global.")"; else echo "(0)"; ?></option>-->
                            <option value="01"
                                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "01") { ?>selected<?php } ?>>
                                Janvier <?php if (isset($toAgndaJanvier_global)) echo "(" . $toAgndaJanvier_global . ")"; else echo "(0)"; ?></option>
                            <option value="02"
                                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "02") { ?>selected<?php } ?>>
                                Février <?php if (isset($toAgndaFevrier_global)) echo "(" . $toAgndaFevrier_global . ")"; else echo "(0)"; ?></option>
                            <option value="03"
                                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "03") { ?>selected<?php } ?>>
                                Mars <?php if (isset($toAgndaMars_global)) echo "(" . $toAgndaMars_global . ")"; else echo "(0)"; ?></option>
                            <option value="04"
                                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "04") { ?>selected<?php } ?>>
                                Avril <?php if (isset($toAgndaAvril_global)) echo "(" . $toAgndaAvril_global . ")"; else echo "(0)"; ?></option>
                            <option value="05"
                                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "05") { ?>selected<?php } ?>>
                                Mai <?php if (isset($toAgndaMai_global)) echo "(" . $toAgndaMai_global . ")"; else echo "(0)"; ?></option>
                            <option value="06"
                                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "06") { ?>selected<?php } ?>>
                                Juin <?php if (isset($toAgndaJuin_global)) echo "(" . $toAgndaJuin_global . ")"; else echo "(0)"; ?></option>
                            <option value="07"
                                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "07") { ?>selected<?php } ?>>
                                Juillet <?php if (isset($toAgndaJuillet_global)) echo "(" . $toAgndaJuillet_global . ")"; else echo "(0)"; ?></option>
                            <option value="08"
                                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "08") { ?>selected<?php } ?>>
                                Août <?php if (isset($toAgndaAout_global)) echo "(" . $toAgndaAout_global . ")"; else echo "(0)"; ?></option>
                            <option value="09"
                                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "09") { ?>selected<?php } ?>>
                                Septembre <?php if (isset($toAgndaSept_global)) echo "(" . $toAgndaSept_global . ")"; else echo "(0)"; ?></option>
                            <option value="10"
                                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "10") { ?>selected<?php } ?>>
                                Octobre <?php if (isset($toAgndaOct_global)) echo "(" . $toAgndaOct_global . ")"; else echo "(0)"; ?></option>
                            <option value="11"
                                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "11") { ?>selected<?php } ?>>
                                Novembre <?php if (isset($toAgndaNov_global)) echo "(" . $toAgndaNov_global . ")"; else echo "(0)"; ?></option>
                            <option value="12"
                                    <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification == "12") { ?>selected<?php } ?>>
                                Décembre <?php if (isset($toAgndaDec_global)) echo "(" . $toAgndaDec_global . ")"; else echo "(0)"; ?></option>
                        </select>
                        <input type="hidden" name="zMotCle" id="zMotCle" value=""/>
                    </form>
                </td>
            </tr>
        </table>

    </div>

    <div style="width:100%; height:auto; float:left;background-color: #FFFFFF;font-family: Arial, Helvetica, sans-serif;
    font-size: 12px; color:#000;
    margin-bottom: 0;">
        <div style="width:100%; height:5px;"></div>


        <div style="float: left;
	width: 100%;
	position: relative;">


            <div>

                <ul id="Id_agenda_adresses" style="margin:0; padding:0; list-style:none;">

                    <?php } ?>

                    <?php foreach ($toAgenda as $oAgenda) { ?>

                        <li style="list-style:none; margin:0; padding:0;">

                            <a href="<?php echo site_url("agenda/details_event/" . $oAgenda->id); ?>"
                               style="color:#000000; text-decoration:none;">
                                <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                       style="padding-left:2px; margin-top:7px; margin-bottom:3px;">

                                    <tr>

                                        <td style="width:90px; padding-right:15px;" rowspan="2">

                                            <?php
                                            $photoCommercant_path = "application/resources/front/photoCommercant/imagesbank/" . $oAgenda->IdUsers_ionauth . "/";
                                            $photoCommercant_path_old = "application/resources/front/images/article/photoCommercant/";

                                            $image_home_vignette = "";
                                            if (isset($oAgenda->photo1) && $oAgenda->photo1 != "" && is_file($photoCommercant_path . $oAgenda->photo1) == true) {
                                                $image_home_vignette = $oAgenda->photo1;
                                            } else if ($image_home_vignette == "" && isset($oAgenda->photo2) && $oAgenda->photo2 != "" && is_file($photoCommercant_path . $oAgenda->photo2) == true) {
                                                $image_home_vignette = $oAgenda->photo2;
                                            } else if ($image_home_vignette == "" && isset($oAgenda->photo3) && $oAgenda->photo3 != "" && is_file($photoCommercant_path . $oAgenda->photo3) == true) {
                                                $image_home_vignette = $oAgenda->photo3;
                                            } else if ($image_home_vignette == "" && isset($oAgenda->photo4) && $oAgenda->photo4 != "" && is_file($photoCommercant_path . $oAgenda->photo4) == true) {
                                                $image_home_vignette = $oAgenda->photo4;
                                            } else if ($image_home_vignette == "" && isset($oAgenda->photo5) && $oAgenda->photo5 != "" && is_file($photoCommercant_path . $oAgenda->photo5) == true) {
                                                $image_home_vignette = $oAgenda->photo5;
                                            } else if ($image_home_vignette == "" && isset($oAgenda->photo1) && $oAgenda->photo1 != "" && is_file($photoCommercant_path_old . $oAgenda->photo1) == true) {
                                                $image_home_vignette = $oAgenda->photo1;
                                            } else if ($image_home_vignette == "" && isset($oAgenda->photo2) && $oAgenda->photo2 != "" && is_file($photoCommercant_path_old . $oAgenda->photo2) == true) {
                                                $image_home_vignette = $oAgenda->photo2;
                                            } else if ($image_home_vignette == "" && isset($oAgenda->photo3) && $oAgenda->photo3 != "" && is_file($photoCommercant_path_old . $oAgenda->photo3) == true) {
                                                $image_home_vignette = $oAgenda->photo3;
                                            } else if ($image_home_vignette == "" && isset($oAgenda->photo4) && $oAgenda->photo4 != "" && is_file($photoCommercant_path_old . $oAgenda->photo4) == true) {
                                                $image_home_vignette = $oAgenda->photo4;
                                            } else if ($image_home_vignette == "" && isset($oAgenda->photo5) && $oAgenda->photo5 != "" && is_file($photoCommercant_path_old . $oAgenda->photo5) == true) {
                                                $image_home_vignette = $oAgenda->photo5;
                                            }
                                            ////$this->firephp->log($image_home_vignette, 'image_home_vignette');

                                            //showing category img if all image of agenda is null
                                            $this->load->model("mdl_categories_agenda");
                                            $toCateg_for_agenda = $this->mdl_categories_agenda->getById($oAgenda->agenda_categid);
                                            if ($image_home_vignette == "" && isset($toCateg_for_agenda->images) && $toCateg_for_agenda->images != "" && is_file("application/resources/front/images/agenda/category/" . $toCateg_for_agenda->images) == true) {
                                                echo '<img src="' . GetImagePath("front/") . '/agenda/category/' . $toCateg_for_agenda->images . '" width="87"/>';
                                            } else {

                                                if ($image_home_vignette != "") {

                                                    if (isset($image_home_vignette) && $image_home_vignette != "" && is_file($photoCommercant_path . $image_home_vignette) == true)
                                                        echo '<img src="' . base_url() . $photoCommercant_path . $image_home_vignette . '" width="87"/>';
                                                    else echo '<img src="' . base_url() . $photoCommercant_path_old . $image_home_vignette . '" width="87"/>';

                                                } else {
                                                    $image_home_vignette_to_show = GetImagePath("front/") . "/wp71b211d2_06.png";
                                                    echo '<img src="' . $image_home_vignette_to_show . '" width="87" />';
                                                }

                                            }
                                            ?>

                                        </td>

                                        <td valign="top" style="padding-left:2px;" width="210">
            <span style="font-size:12px;">
            <strong>
                <?php echo $oAgenda->category; ?><br/>
                <?php echo $oAgenda->subcateg; ?><br/>
                <?php echo $oAgenda->nom_manifestation; ?><br/>
            </strong>
                <?php echo $oAgenda->adresse_localisation; ?> <?php echo $oAgenda->codepostal_localisation; ?>
                , <?php echo $oAgenda->ville; ?><br/>

                <?php
                if ($oAgenda->datetime_debut == $oAgenda->datetime_fin) {
                    if (isset($oAgenda->datetime_debut) && $oAgenda->datetime_debut != "0000-00-00") echo "Le " . translate_date_to_fr($oAgenda->datetime_debut);
                    if (isset($oAgenda->datetime_heure_debut) && $oAgenda->datetime_heure_debut != "0:00") echo " à " . str_replace(":", "h", $oAgenda->datetime_heure_debut);
                } else {
                    if (isset($oAgenda->datetime_debut) && $oAgenda->datetime_debut != "0000-00-00") echo "Du " . translate_date_to_fr($oAgenda->datetime_debut);
                    if (isset($oAgenda->datetime_fin) && $oAgenda->datetime_fin != "0000-00-00") {
                        if (isset($oAgenda->datetime_debut) && $oAgenda->datetime_debut != "0000-00-00") echo "<br/>au " . translate_date_to_fr($oAgenda->datetime_fin);
                        else echo " Jusqu'au " . translate_date_to_fr($oAgenda->datetime_fin);
                    }
                    if (isset($oAgenda->datetime_heure_debut) && $oAgenda->datetime_heure_debut != "0:00") echo " à " . str_replace(":", "h", $oAgenda->datetime_heure_debut);
                }
                ?>

            </span>


                                        </td>
                                        <td style="text-align:right;">
                                            <img
                                                src="<?php echo GetImagePath("front/"); ?>/mobile2013/fleche_agenda_mobile.png"
                                                width="30" height="62" alt="agenda"/>
                                        </td>
                                    </tr>
                                    <!--<tr>
          	<td colspan="2">
            	<div style="padding-top:20px;padding-left:15px; padding-right:15px;"><?php // echo truncate(strip_tags($oAgenda->description),170,$etc = " ... (suite) ...")?></div>
            </td>
          </tr>-->
                                </table>
                            </a>
                            <div style="height:3px; width:100%;; background-color:#850036;"></div>

                        </li>

                    <?php }


                    if (isset($_GET['content_only_list']) && $_GET['content_only_list'] == '1') {
                    } else {

                    ?>


                </ul>


                <?php if (isset($links_pagination) && $links_pagination != "") { ?>
                    <style type="text/css">
                        #view_pagination_ci strong {
                            color: #FF0000;
                            font-weight: bold;
                            background-color: #6C8599;
                            padding: 5px 15px;
                        }

                        #view_pagination_ci a {
                            background-color: #6C8599;
                            color: #FFFFFF;
                            padding: 5px 15px;
                        }
                    </style>
                    <!--<div id="view_pagination_ci" style="text-align: center; font-size:14px; height:20px; vertical-align:central; padding-top:5px; margin-bottom:15px; margin-top:5px;">
<span style="font-size:12px;"></span><?php //echo $links_pagination; ?>
</div>-->
                <?php } ?>

                <?php $this->load->view("mobile2013/liste_agenda_listing.php", $data); ?>

            </div>


        </div>


    </div>
</div>
<?php
$this->load->view("mobile2013/includes/footer.php", $data);

}//end content_only_list

?>

