<?php 
$data["zTitle"] = 'Mobile page';
$this->load->view("mobile2013/includes/main_header_favoris.php", $data);
?>

<?php $data['empty'] = null; ?>
<?php $this->load->view("sortez_vsv/includes/main_menu_mobile_index", $data); $this->load->view("mobile2013/includes/searchform_content_partenaire", $data); ?>



<div style="width:100%; height:auto; float:left;background-color: #FFFFFF; color:#000000; font-family: Arial, Helvetica, sans-serif;
    font-size: 12px;
    margin-bottom: 0;">


<style type="text/css">
.mobile_contener_all {
	width: 100%;
	margin-right: auto;
	margin-left: auto;
}
.header_mobile {
	float: left;
	width: 100%;
	position: relative;
}
.main_mobile {
	float: left;
	width: 100%;
	position: relative;
	padding-top:5px;
	padding-bottom:10px;
}
.main_favoris_card_title {
  color: fuchsia;
  font-size: 20px;
}
.main_favoris_card_title_ville {
  font-size: 16px;
}


</style>

<?php if (count($toCommercant)>0) { ?>
<div class="col-xs-12" style="text-align:center; font-weight:bold; padding:15px; font-size: 25px;">Mes favoris</div>
<?php } ?>



<div class="mobile_contener_all">
  
  <div class="main_mobile">

<?php 
if (isset($user_groups) && $user_groups->id!=2) echo '<div style=" color:#FF0000; font-weight:bold; text-align:center; padding:10px;">Seuls les consommateurs ont une liste de partenaires favoris.</div>';
?>

<div style="height:2px; background-color:#FFF; margin-bottom:4px;"></div>

<?php if (count($toCommercant)==0) { ?>
<div class="col-xs-12" style="text-align:center; font-weight:bold; padding:15px;">Liste vide</div>
<?php } ?>

<div style="height:3px; background-color:#000054;"></div>
  	
<?php  foreach($toCommercant as $oCommercant){ 

		$objasscommrubr = $this->sousRubrique->GetById($oCommercant->IdSousRubrique);
		
		$oInfoCommercant = $oCommercant;
		$base_path_system = str_replace('system/', '', BASEPATH);
		include($base_path_system.'application/views/front2013/includes/url_var_definition.php');
		//$this->load->view("front2013/includes/url_var_definition", $data); 
		?>

<div class="col-xl-12">
  <div class="row pr-3">
		<div class="<?php if (isset($main_tablet_screen)) { ?>col-sm-4<?php } else { ?>col-md-6<?php } ?>  col-xl-3 pr-0">

        <div class="row main_favoris_liste_row">

                      <div class="col-xs-12 p-0" style="height: 220px; overflow: hidden;">
                        
                        <?php 
                    $image_home_vignette = "";
                    if (isset($oCommercant->PhotoAccueil) && $oCommercant->PhotoAccueil != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->PhotoAccueil)==true){ $image_home_vignette = $oCommercant->PhotoAccueil;}
                    else if (isset($oCommercant->Photo1) && $oCommercant->Photo1 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo1)==true){ $image_home_vignette = $oCommercant->Photo1;}
                    else if (isset($oCommercant->Photo2) && $oCommercant->Photo2 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo2)==true){ $image_home_vignette = $oCommercant->Photo2;}
                    else if (isset($oCommercant->Photo3) && $oCommercant->Photo3 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo3)==true){ $image_home_vignette = $oCommercant->Photo3;}
                    else if (isset($oCommercant->Photo4) && $oCommercant->Photo4 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo4)==true){ $image_home_vignette = $oCommercant->Photo4;}
                    else if (isset($oCommercant->Photo5) && $oCommercant->Photo5 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo5)==true){ $image_home_vignette = $oCommercant->Photo5;}
                    ?>
                        <div style="margin-left: 60px;">
                    <img src="<?php if ($image_home_vignette != ""){ echo GetImagePath("front/photoCommercant/"); ?>/<?php echo $image_home_vignette ; }else{echo GetImagePath("front/")."/wp71b211d2_06.png";}?>" width="250" border="0" alt="<?php echo $commercant_url_nom;?>" >
                        </div>
                      </div>
        </div>

        <div
                    style="position:absolute; bottom:0; width:100%; height:40px; background:rgba(0, 0, 0, 0.5); color:#FFFFFF; font-weight: normal; text-transform:uppercase; text-align:center; line-height:40px;"><?php if ($objasscommrubr) echo $objasscommrubr->Nom; ?>
                
        </div>

    </div>
  </div>
</div>  	

                    
                      
                    
				  
              

               

<div class="padding15 main_favoris_card_text_container">

  <div class="col-lg-12 padding0">
    <div class="col-sm-12 padding0 main_favoris_card_title_container" style="text-align: center;">
      
                              <!--<span style='background-color: transparent;
    color: #000000;
    font-family: "Arial",sans-serif;
    font-size: 16px;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 19px;
    text-decoration: none;
    vertical-align: 0;'><?php if ($objasscommrubr_main) echo $objasscommrubr_main->Nom; ?></span><br/>-->
                        <span class="main_favoris_card_title"><?php echo $oCommercant->NomSociete; ?> <br> 
                          </span> 
                        <span class="main_favoris_card_title_ville"><?php echo $oCommercant->ville ; ?></span>

                        <!--<span style='background-color: transparent;
    color: #000000;
    font-family: "Arial",sans-serif;
    font-size: 16px;
    font-style: normal;
    font-variant: normal;
    font-weight: 700;
    line-height: 20px;
    text-decoration: none;
    vertical-align: 0;'><?php if ($objasscommrubr) echo $objasscommrubr->Nom; ?></span><br/> -->

    </div>
    
  </div>
  
</div>

<div class="col-lg-12 padding0" style="margin-left: 100px;">               
  <tr>
    <td width="25">
    	<a href="<?php echo site_url("front/utilisateur/delete_favoris/".$oCommercant->IdCommercant);?>"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/btn_delete_favoriss.png" width="50" alt="partenaire" /></a>
    </td>
    <td>
    	<a href="<?php echo site_url("front/utilisateur/delete_favoris/".$oCommercant->IdCommercant);?>" style="text-decoration:none; color:#000000; font-size: 15px;" class="sup_taille">Supprimer de mes favoris</a></td>
  </tr>
</div> 
    </td>
                
             
                <td valign="middle" width="10%" style="text-align: center;">
                	<a href="<?php echo site_url($commercant_url_home."/index");?>" style="color:#fff; text-decoration:none;">
                    <img src="<?php echo GetImagePath("front/"); ?>/mobile2013/fleche_compte_mobile.png" width="25" height="53" alt="cmpt" style="margin-left: 350px;"> </a>
                </td>
              </tr>
          
    </table>
        
    <div style="height:3px; background-color:#000054;"></div>
		
		
		<?php
		
	}
?>
    
 
    
  </div>
 
</div>


</div>

<?php 
$this->load->view("mobile2013/includes/footer.php", $data);
?>
