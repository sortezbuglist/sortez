<!DOCTYPE html>
<html lang="fr">
  
<!-- Mirrored from www.proximite-magazine.com/mobile/infos-particuliers.html by HTTrack Website Copier/3.x [XR&CO'2010], Wed, 11 Dec 2013 07:31:42 GMT -->
<head>
    <meta charset="UTF-8">
    <title>infos-particuliers</title>
    <meta name="generator" content="Serif WebPlus X7">
    <meta name="viewport" content="width=320">
    <link rel="stylesheet" type="text/css" href="<?php echo GetImagePath("front/"); ?>/mobile2013/wpscripts/wpstyles.css">
    <style type="text/css">
      .P-1 { line-height:13.3px;margin-left:29.5px;margin-bottom:4.0px;text-indent:-15.0px;font-family:"Arial", sans-serif;font-size:12.0px; }
      .P-2 { line-height:13.3px;margin-bottom:4.0px;font-family:"Arial", sans-serif;font-size:12.0px; }
      .P-3 { text-align:center;font-family:"Arial", sans-serif;font-weight:700;color:#000054;font-size:17.3px; }
      .P-4 { line-height:13.3px;font-family:"Arial", sans-serif;font-weight:700;font-size:13.3px; }
      .P-5 { line-height:13.3px;margin-bottom:4.0px;font-family:"Arial", sans-serif;font-style:italic;font-size:12.0px; }
      .C-1 { font-style:normal; }
      .P-6 { line-height:13.3px;font-family:"Arial", sans-serif;font-weight:700;font-size:12.0px; }
      .P-7 { text-align:justify;line-height:13.3px;margin-bottom:0.0px;font-family:"Arial", sans-serif;font-size:12.0px; }
      .P-8 { margin-bottom:0.0px;font-family:"Arial", sans-serif;font-weight:700;font-size:12.0px; }
      .P-9 { text-align:justify;line-height:13.3px;margin-bottom:4.0px;font-family:"Arial", sans-serif;font-size:12.0px; }
      .P-10 { text-align:justify;margin-bottom:4.0px;font-family:"Arial", sans-serif;font-style:italic;font-size:12.0px; }
      .P-11 { margin-left:48.0px;margin-bottom:4.0px;text-indent:-15.0px;color:#000054; }
      .P-12 { margin-bottom:4.0px;color:#000054; }
    </style>
  </head>
  <body style="height:2100px;background:#003566 url('<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp9e1de8e1_05_06.jpg') repeat scroll center top;">
    <div id="divMain" style="background:#ffffff;margin-left:auto;margin-right:auto;position:relative;width:320px;height:2100px;">
      <img alt="" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp4739d139_06.png" style="position:absolute;left:0px;top:345px;width:320px;height:221px;">
      <img src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wpfd5c09fc_06.png" alt="" width="320" height="242" style="position:absolute;left:0px;top:863px;width:320px;height:242px;">
      <img src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp32d0b0ce_06.png" alt="" width="105" height="200" style="position:absolute;left:10px;top:1040px;width:105px;height:200px;">
      <div style="position:absolute;left:128px;top:1083px;width:182px;height:163px;overflow:hidden;">
        <ul style="list-style-type:disc;margin:0;padding:0;">
          <li class="Corps-artistique P-1" style="text-indent:0;margin-left:30px;">Chez le partenaire, vous présentez votre carte.</li><li class="Corps-artistique P-1" style="text-indent:0;margin-left:30px;">Le partenaire avec notre application Mobile scanne votre code barre et confirme la validation de votre carte</li><li class="Corps-artistique P-1" style="text-indent:0;margin-left:30px;">Vous précisez la qualité&nbsp;» de l’offre du partenaire qui vous intéresse :</li>
        </ul>
        <p class="Corps-artistique P-2"><br></p>
      </div>
      <map id="map1" name="map1"><area shape="poly" coords="237,27,237,2,2,2,2,39,237,39" href="inscription-club.html" alt="" title=""></map>
      <img alt="inscription-club" usemap="#map1" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wpdd412345_06.png" style="position:absolute;left:40px;top:176px;width:245px;height:47px;">
      <map id="map2" name="map2">
        <area shape="poly" coords="312,35,314,29,314,13,312,9,283,9,281,16,281,35,283,36,311,36" href="sommaire.html" alt="" title="">
        <area shape="poly" coords="311,35,314,31,314,12,311,9,283,9,281,14,281,34,283,36,311,36" title="">
        <area shape="poly" coords="39,35,41,29,41,13,38,9,10,9,8,13,7,16,7,30,10,36,38,36" href="javascript:history.back()" alt="" title="">
      </map>
      <img src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp1448246e_06.png" alt="" width="320" height="114" usemap="#map2" style="position:absolute;left:0px;top:0px;width:320px;height:114px;">
      <div style="position:absolute;left:10px;top:121px;width:300px;height:50px;overflow:hidden;">
        <p class="Corps-artistique P-3">Demandez la carte Proximité et bénéficiez de multiples avantages</p>
      </div>
      <img alt="" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wpc918040a_06.png" style="position:absolute;left:120px;top:27px;width:79px;height:16px;">
      <img alt="" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wpc8e4e8c5_06.png" style="position:absolute;left:33px;top:858px;width:264px;height:22px;">
      <img alt="Les différentes offres" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp127aed20_06.png" style="position:absolute;left:41px;top:1250px;width:241px;height:38px;">
      <map id="map3" name="map3"><area shape="rect" coords="2,4,116,116" href="liste-agenda.html" alt="" title=""></map>
      <img alt="liste-agenda" usemap="#map3" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp3d4920aa_06.png" style="position:absolute;left:8px;top:1385px;width:121px;height:119px;">
      <map id="map4" name="map4"><area shape="poly" coords="125,2,7,0,0,159,120,160" href="contact.html" alt="" title=""></map>
      <img alt="contact" usemap="#map4" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wpbc23b620_06.png" style="position:absolute;left:0px;top:1548px;width:125px;height:160px;">
      <map id="map5" name="map5"><area shape="poly" coords="136,6,5,0,0,122,137,128" href="recherches-agenda.html" alt="" title=""></map>
      <img alt="recherches-agenda" usemap="#map5" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wpc04d666d_06.png" style="position:absolute;left:0px;top:1851px;width:137px;height:128px;">
      <div style="position:absolute;left:145px;top:1385px;width:165px;height:127px;overflow:hidden;">
        <p class="Corps-artistique P-4">Le programme Bon plan :</p>
        <p class="Corps-artistique P-2">Certains partenaires proposent &nbsp;sur leur fiche un bon plan : une offre exceptionnelle, unique.</p>
        <p class="Corps-artistique P-5">Exemple pour un restaurant : 1 menu acheté = 1 menu offert.<span class="C-1"></span></p>
      </div>
      <div style="position:absolute;left:145px;top:1553px;width:165px;height:245px;overflow:hidden;">
        <p class="Corps-artistique P-6">Le programme</p>
        <p class="Corps-artistique P-6">«&nbsp;Coup de tampon&nbsp;» :</p>
        <p class="Corps P-7">A chaque validation, votre compte est automatiquement augmenté d’un point.</p>
        <p class="Corps P-7">Le consommateur reçoit une confirmation par courriel lui rappelant les conditions des offres.</p>
        <p class="Corps P-7">-<wbr> Rappel de la valeur obtenu</p>
        <p class="Corps P-7">-<wbr> Rappel de l’objectif</p>
        <p class="Corps P-2">-<wbr> Le solde restant</p>
        <p class="Corps-artistique P-5">Exemple pour une pizzéria : 10 pizzas achetés = la onzième gratuite.<span class="C-1"></span></p>
      </div>
      <div style="position:absolute;left:145px;top:1798px;width:165px;height:286px;overflow:hidden;">
        <p class="Corps P-8">Le programme &nbsp;«&nbsp;capitalisation&nbsp;» :</p>
        <p class="Corps P-7">Le partenaire note le montant de votre achat. A chaque validation d’une transaction, le consommateur reçoit une confirmation par courriel lui rappelant les conditions des offres :</p>
        <p class="Corps P-7">-<wbr> Rappel de la valeur obtenu</p>
        <p class="Corps P-7">-<wbr> Rappel de l’objectif</p>
        <p class="Corps P-9">-<wbr> Le solde restant</p>
        <p class="Corps P-10">Exemple pour un magasin : 20€ offerts pour 200€ d’achats cumulés.<span class="C-1"></span></p>
        <p class="Corps-artistique P-2"><br></p>
      </div>
      <map id="map6" name="map6">
        <area shape="poly" coords="320,1,257,0,257,66,320,67" href="mon-compte.html" alt="" title="">
        <area shape="rect" coords="193,0,258,68" title="">
        <area shape="rect" coords="129,0,194,68" href="liste-complete-agenda.html" alt="" title="">
        <area shape="poly" coords="129,1,66,0,66,66,129,67" href="annuaire.html" alt="" title="">
        <area shape="poly" coords="66,1,0,0,0,66,66,67" href="magazine.html" alt="" title="">
      </map>
      <img src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp0ec8c170_06.png" alt="" width="320" height="67" usemap="#map6" style="position:absolute;left:0px;top:2033px;width:320px;height:67px;">
      <div style="position:absolute;left:10px;top:571px;width:300px;height:276px;overflow:hidden;">
        <ul style="list-style-type:disc;margin:0;padding:0;">
          <li class="Normal3 P-11" style="text-indent:0;margin-left:48px;">Vous gérez vos favoris</li><li class="Normal3 P-11" style="text-indent:0;margin-left:48px;">Vous bénéficiez de la réception d’alertes personnalisées</li><li class="Normal3 P-11" style="text-indent:0;margin-left:48px;">Vous validez les bons plans</li><li class="Normal3 P-11" style="text-indent:0;margin-left:48px;">Vous recevez notre newsletter hebdomadaire</li><li class="Normal3 P-11" style="text-indent:0;margin-left:48px;">Vous créez et gérez un agenda personnalisé</li><li class="Normal3 P-11" style="text-indent:0;margin-left:48px;">En la présentant chez nos parte-<wbr>naires, vous bénéficiez de immé-<wbr>diatement des remises et condi-<wbr>tions de fidélisation qu’ils ont concédés</li>
        </ul>
        <p class="Normal3 P-12"><br></p>
      </div>
      <img alt="" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp3034ccff_06.png" style="position:absolute;left:20px;top:1292px;width:290px;height:69px;">
      <map id="map7" name="map7">
        <area shape="poly" coords="165,48,167,47,168,45,167,44,166,46,165,47,162,47,163,45,164,43,163,41,162,40,160,41,158,42,158,46,160,47,161,49,165,49" title="">
        <area shape="poly" coords="161,45,160,44,160,42,161,42,162,43,162,44,161,44" title="">
        <area shape="poly" coords="154,48,154,44,158,44,159,42,154,42,154,36,155,35,153,34,152,36,152,42,151,43,149,44,152,44,152,49" title="">
        <area shape="poly" coords="146,48,147,46,148,44,149,42,151,41,149,40,147,41,146,43,145,42,143,43,143,45,144,47,144,49" title="">
        <area shape="poly" coords="143,49,144,48,143,46,142,44,141,42,140,40,138,41,136,42,135,44,134,46,135,48,137,49,138,47,140,45,141,47,142,49,143,50" title="">
        <area shape="poly" coords="136,47,136,45,138,43,139,44" title="">
        <area shape="poly" coords="131,48,133,47,135,46,133,45,132,46,130,47,128,46,127,45,129,44,130,42,128,41,126,43,125,45,126,47,127,49,131,49" title="">
        <area shape="poly" coords="120,49,121,48,120,46,119,44,118,42,117,40,115,41,113,42,112,44,111,46,112,48,114,49,115,47,117,45,118,47,119,49,120,50" title="">
        <area shape="poly" coords="113,47,113,45,115,43,116,44" title="">
        <area shape="poly" coords="111,48,111,45,110,44,110,42,111,37,110,36,110,34,109,35,108,36,109,37,108,38,108,44,109,45,109,49,111,49" title="">
        <area shape="poly" coords="114,39,113,37,112,36,111,33,110,32,112,30,113,27,115,25,109,25,108,28,106,26,106,25,102,25,101,26,103,28,104,31,105,33,104,34,103,37,101,39,104,40,106,39,108,37,110,39,113,40" title="">
        <area shape="poly" coords="84,40,86,40,83,37,82,35,85,32,85,28,82,26,81,25,73,25,73,29,69,25,64,24,60,24,60,40,65,40,67,37,71,35,73,32,73,40,78,40,79,37,81,41" title="">
        <area shape="poly" coords="137,39,137,27,136,25,132,25,131,28,130,30,129,33,128,29,126,25,121,25,121,40,125,38,125,32,126,35,127,39,131,39,131,36,132,33,133,37,133,40" title="">
        <area shape="poly" coords="95,37,94,37,92,36,90,32,93,30,95,30,96,31,97,33,97,35" title="">
        <area shape="poly" coords="79,33,78,33,78,29,80,30,80,32" title="">
        <area shape="poly" coords="67,33,65,33,65,29,68,29,68,32" title="">
        <area shape="poly" coords="152,40,152,29,156,29,156,40,167,40,167,36,161,36,161,34,166,34,166,31,161,31,161,28,167,28,167,25,156,25,156,26,144,26,143,27,144,29,148,29,148,41" title="">
        <area shape="poly" coords="143,39,143,25,138,25,138,40,143,40" title="">
        <area shape="poly" coords="120,39,120,25,115,25,115,28,114,32,115,36,115,40,119,40" title="">
        <area shape="poly" coords="99,40,101,37,102,33,102,30,98,26,96,25,91,25,86,30,86,33,87,37,91,41,96,41" title="">
        <area shape="poly" coords="162,23,163,23,164,22,165,21,165,20,164,19,163,19,162,20,161,21,160,22,161,23,162,24" title="">
        <area shape="poly" coords="167,38,167,36,161,36,161,34,166,34,166,31,161,31,161,28,167,28,167,25,156,25,156,40,167,40" title="">
        <area shape="poly" coords="162,23,163,22,164,21,165,20,164,19,163,20,162,21,161,22,160,23,161,24" title="">
        <area shape="poly" coords="152,40,152,29,156,29,156,26,144,26,144,29,148,29,148,41" title="">
        <area shape="poly" coords="143,26,143,25,138,25,138,40,143,40" title="">
        <area shape="poly" coords="137,39,136,25,132,25,129,33,126,25,121,25,121,40,125,40,125,31,128,39,130,39,133,31,133,40" title="">
        <area shape="poly" coords="120,26,120,25,115,25,115,40,119,40" title="">
        <area shape="poly" coords="114,39,110,32,114,26,114,25,109,25,108,28,106,25,101,25,102,26,105,32,102,39,106,40,106,39,108,36,110,39,110,40" title="">
        <area shape="poly" coords="96,37,93,37,92,36,91,35,91,31,92,30,95,30,96,31,97,33,97,35,96,35" title="">
        <area shape="poly" coords="97,40,100,38,101,34,100,30,98,26,96,25,92,25,89,28,87,31,86,34,88,38,91,41,96,41" title="">
        <area shape="poly" coords="82,40,85,39,83,37,82,35,85,32,84,29,83,26,81,25,73,25,73,41,74,40,78,40,79,37,80,40,82,41" title="">
        <area shape="poly" coords="79,33,78,33,78,29,79,29,79,30,80,30,80,32" title="">
        <area shape="poly" coords="67,33,65,33,65,29,67,29,68,30,68,32" title="">
        <area shape="poly" coords="65,39,67,37,69,35,71,34,72,31,72,28,70,26,68,25,65,24,60,24,60,40,65,40" title="">
        <area shape="poly" coords="177,14,42,14,41,46,177,49" title="">
        <area shape="poly" coords="178,119,184,112,184,105,185,65,184,23,183,15,179,11,170,11,93,10,21,10,13,15,12,25,12,107,13,115,17,119,26,119,104,120,163,120" href="ma-carte.html" alt="" title="">
      </map>
      <img src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp8d5c3a9f_06.png" alt="" width="199" height="134" usemap="#map7" style="position:absolute;left:62px;top:230px;width:199px;height:134px;">
    </div>
  </body>

<!-- Mirrored from www.proximite-magazine.com/mobile/infos-particuliers.html by HTTrack Website Copier/3.x [XR&CO'2010], Wed, 11 Dec 2013 07:32:17 GMT -->
</html>
