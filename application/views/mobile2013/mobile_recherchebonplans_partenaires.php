<?php 
$data["zTitle"] = 'Mobile page';
$this->load->view("mobile2013/includes/header.php", $data);
?>
<div style="width:100%; position:relative; float:left;">
<div style="background-image: url(<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wpafe24bd1_06.png);
	background-repeat: no-repeat;
	background-position: center top;
	float: left;
	height: 114px;
	width: 100%;
	position: relative;
	background-color: #B30000;">
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td style=" text-align:left;">
    <?php 
	$data["zTitle"] = 'Mobile page';
	$this->load->view("mobile2013/includes/btn_sommaire.php", $data);
	?>
    </td>
    <td style=" text-align:right;">
    <?php 
	$this->load->view("mobile2013/includes/btn_back.php", $data);
	?>
    </td>
  </tr>
</table>
</div>

<div style="width:100%; height:auto; float:left;background-color: #FFFFFF; position:relative;">




<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/wpscripts2013/jquery-1.8.3.js"></script>
<script src="<?php echo GetJsPath("front/") ; ?>/js_geo/geo-min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">

$(document).ready(function() {
      
        $('#inputStringVilleHidden_bonplans').change(function() {
            
            $("#span_categ_list_mobile").html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...."/>');
            var inputStringVilleHidden_bonplans = $('#inputStringVilleHidden_bonplans').val();     
            $.post(
              '<?php echo site_url("front/mobile/getcategoriebonplans"); ?>',
              {inputStringVilleHidden_bonplans: inputStringVilleHidden_bonplans},
              function (zReponse)
              {
                  // alert (zReponse) ;
                  $('#span_categ_list_mobile').html(zReponse) ;       


             });
             //alert(inputStringHidden_main_form_mobile);
          });
		  
		  
		  $('#inputStringHidden_main_form_mobile').change(function() {
            
            $("#span_souscateg_list_mobile").html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="Uploading...."/>');
            var inputStringHidden_main_form_mobile = $('#inputStringHidden_main_form_mobile').val();     
            $.post(
              '<?php echo site_url("front/mobile/getsouscategoriebonplans"); ?>',
              {inputStringHidden_main_form_mobile: inputStringHidden_main_form_mobile},
              function (zReponse)
              {
                  // alert (zReponse) ;
                  $('#span_souscateg_list_mobile').html(zReponse) ;       


             });
             //alert(inputStringHidden_main_form_mobile);
          });
		  
		  
		  
		  $("#inputzMotCle_agenda").focus(function(){
				var inputzMotCle_agenda = $("#inputzMotCle_agenda").val();
				if(inputzMotCle_agenda=='Mots clés') {
					$("#inputzMotCle_agenda").val('');
				}
			});
			$("#inputzMotCle_agenda").blur(function(){
				var inputzMotCle_agenda = $("#inputzMotCle_agenda").val();
				if(inputzMotCle_agenda=='') {
					$("#inputzMotCle_agenda").val('Mots clés');
				}
			});
		  
          
          
          $("#btn_submit_search_partner_mobile").click(function(){
                var txtError = "";
				
				var inputzMotCle_agenda = $("#inputzMotCle_agenda").val();
                if(inputzMotCle_agenda=="" || inputzMotCle_agenda=="Mots clés") {
					//alert(inputzMotCle_agenda);
                    $("#inputzMotCle_agenda").val("");
                }
				
                
				var inputStringHidden_main_form_mobile = $("#inputStringHidden_main_form_mobile").val();
                if(inputStringHidden_main_form_mobile=="" || inputStringHidden_main_form_mobile=="0") {

                    //txtError += "- Veuillez indiquer Votre inputStringHidden_main_form_mobile <br/>"; 
                    //alert("Veuillez indiquer Votre nom");
                    //$("#inputStringHidden_main_form_mobile").css('border-color', 'red');
                } else {
                    $("#inputStringHidden_main_form_mobile").css('border-color', '#E3E1E2');
                }

                var inputStringHidden_form_mobile = $("#inputStringHidden_form_mobile").val();
                if(inputStringHidden_form_mobile=="" || inputStringHidden_form_mobile=="0") {

                    //txtError += "- Veuillez indiquer Votre inputStringHidden_form_mobile <br/>"; 
                    //alert("Veuillez indiquer Votre nom");
                    //$("#inputStringHidden_form_mobile").css('border-color', 'red');
                } else {
                    $("#inputStringHidden_form_mobile").css('border-color', '#E3E1E2');
                }
				
				var inputStringVilleHidden_form_mobile = $("#inputStringVilleHidden_bonplans").val();
				
				
				if ((inputStringHidden_form_mobile=="" || inputStringHidden_form_mobile=="0") && (inputStringVilleHidden_form_mobile=="" || inputStringVilleHidden_form_mobile=="0") && (inputStringHidden_main_form_mobile!="" && inputStringHidden_main_form_mobile!="0")) {
					$.post(
						  '<?php echo site_url("front/mobile/getsouscategoriebonplans_to_mobile"); ?>',
						  {inputStringHidden_main_form_mobile: inputStringHidden_main_form_mobile,
						  inputStringVilleHidden_form_mobile: inputStringVilleHidden_form_mobile},
						  function (zReponse)
						  {
							  //alert (zReponse) ;
							  $("#inputStringHidden").val(","+zReponse);
							  $("#frmRechercheAnnonce_mobile_page").submit();    
						 });
				} else {
					if(txtError == "") {
						//alert('ok');
						$("#inputStringHidden").val(","+inputStringHidden_form_mobile);
						$("#frmRechercheAnnonce_mobile_page").submit();
						//$("#qsdf_x").click();
					}	
				}
				
				
            });
			
			
			//click on "autour de ma position" button
			$("#btn_geolocalisation_mobile").click(function(){
				
				$("#loading_geolocalisation").html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...."/>');
				
				if(geo_position_js.init()){
					geo_position_js.getCurrentPosition(success_callback,error_callback,{enableHighAccuracy:true});
				}
				else{
					alert("Fonction non accessible");
				}
				
		   });
	
});


function success_callback(p)
	{
		//alert('lat='+p.coords.latitude.toFixed(2)+';lon='+p.coords.longitude.toFixed(2));
		$("#inputGeoLatitude").val(p.coords.latitude.toFixed(4));
		$("#inputGeoLongitude").val(p.coords.longitude.toFixed(4));
		
		var inputStringHidden_main_form_mobile = $("#inputStringHidden_main_form_mobile").val();
		var inputStringHidden_form_mobile = $("#inputStringHidden_form_mobile").val();
		$("#inputStringHidden").val(","+inputStringHidden_form_mobile);
		$("#inputFromGeolocalisation").val("1");
		var ttttt = $("#inputGeoLatitude").val();
		var tttttx = $("#inputGeoLongitude").val();
		//alert(ttttt+" / "+tttttx);
		$("#loading_geolocalisation").html("");
		$("#frmRechercheAnnonce_mobile_page").submit();
	}
	
function error_callback(p)
	{
		alert('error='+p.message);
	}


function change_categ_getsouscateg_bonplans(){
	
            $("#span_souscateg_list_mobile").html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="Uploading...."/>');
            var inputStringHidden_main_form_mobile = $('#inputStringHidden_main_form_mobile').val();     
            $.post(
              '<?php echo site_url("front/mobile/getsouscategoriebonplans"); ?>',
              {inputStringHidden_main_form_mobile: inputStringHidden_main_form_mobile},
              function (zReponse)
              {
                  // alert (zReponse) ;
                  $('#span_souscateg_list_mobile').html(zReponse) ;       


             });
             //alert(inputStringHidden_main_form_mobile);
	}


</script>


<div class="main_mobile">
  
  <div id="loading_geolocalisation" class="loading_geolocalisation" style="text-align:center;"></div>
  	
    
    
<form name="frmRechercheAnnonce" id="frmRechercheAnnonce_mobile_page" method="post" action="<?php echo site_url('front/bonplan/index/'); ?>">
<table width="100%" border="0" cellspacing="4" cellpadding="0" style="text-align:center;" align="center">
<input type="hidden" name="hdnFavoris" id="hdnFavoris" value="" />
<input type="hidden" name="from_mobile_search_page_partner" id="from_mobile_search_page_partner" value="1"/>
<input type="hidden" name="inputStringHidden" id="inputStringHidden" value=""/>

<?php 
$thisss =& get_instance();
$thisss->load->library('ion_auth');
$this->load->model("ion_auth_used_by_club");
if ($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
	$user_ion_auth = $thisss->ion_auth->user()->row();
	$iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
	$oUser = $this->user->GetById($iduser);
}
?>
<input type="hidden" name="inputFromGeolocalisation" id="inputFromGeolocalisation" value="0"/>
<input type="hidden" name="inputGeolocalisationValue" id="inputGeolocalisationValue" value="<?php if (isset($oUser)) echo $oUser->geodistance; else echo "10";?>"/>
<input type="hidden" name="inputGeoLatitude" id="inputGeoLatitude" value="0"/>
<input type="hidden" name="inputGeoLongitude" id="inputGeoLongitude" value="0"/>

  <tr>
    <td><a href="Javascript:void();" id="btn_geolocalisation_mobile"><img src="<?php echo GetImagePath("front/"); ?>/mobile2013/bp_alentour.png" width="304" height="55" alt="alentour" /></a>

    
    </td>
  </tr>
<tr>
    <td height="200" style="background-image:url(<?php echo GetImagePath("front/"); ?>/mobile2013/bp_multicritere.png); background-position:center top; background-repeat:no-repeat;">
    
    
    <?php 
	$this_session =& get_instance();
	$this_session->load->library('session');
	$session_zMotCle_verification = $this_session->session->userdata('zMotCle_x');
	?>
    <div style="margin-bottom:5px; margin-top:5px;">
    <input type="text" name="zMotCle" id="inputzMotCle_agenda" value="<?php if (isset($session_zMotCle_verification) && $session_zMotCle_verification != "") echo $session_zMotCle_verification; else echo "Mots clés";?>" style="width:245px;"/>
    </div> 
    
    
<?php
$this_session_localdata =& get_instance();
$this_session_localdata->load->library('session');
$localdata_value = $this_session_localdata->session->userdata('localdata');
$localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
if((isset($localdata_value) && $localdata_value!="")||(isset($localdata_IdVille) && $localdata_IdVille!="")){} else {
?>    
	<select id="inputStringVilleHidden_bonplans" name="inputStringVilleHidden" style="width:250px">
    <option value="0">Toutes&nbsp;les&nbsp;communes</option>
        <?php 
        //////$this->firephp->log($toVille, 'toVille');
        foreach($toVille as $oVille){ ?>
            <option value="<?php echo $oVille->IdVille ; ?>" <?php if(isset($iVilleId) && $iVilleId == $oVille->IdVille) echo 'selected="selected"'; ?>><?php echo $oVille->Nom." (".$oVille->nb_commercant.")" ; ?></option>
        <?php } ?>
    </select>
<?php
}
?>   
    
    
    <div id="span_categ_list_mobile" class="span_categ_list_mobile" style="margin-bottom:5px; margin-top:5px;">
    <select type="inputStringHidden_main" name="inputStringHidden_main" id="inputStringHidden_main_form_mobile" style="width:250px">
        <option value="0">Toutes les rubriques</option>
        <?php foreach($toRubriquePrincipale as $oCategorie_main){ ?>
            <option value="<?php echo $oCategorie_main->IdRubrique ; ?>"><?php echo $oCategorie_main->Nom ; ?>&nbsp;(<?php echo $oCategorie_main->nb_bonplan ; ?>)</option>
        <?php } ?>
    </select>
    </div>
    <div id="span_souscateg_list_mobile" class="span_souscateg_list_mobile">
    <select type="inputStringHidden_sous" name="inputStringHidden_sous" id="inputStringHidden_form_mobile" style="width:250px">
        <option value="0">Toutes les sous-rubriques</option>
        <?php foreach($toCategorie as $oCategorie){ ?>
            <option value="<?php echo $oCategorie->IdSousRubrique ; ?>"><?php echo $oCategorie->Nom; ?>&nbsp;(<?php echo $oCategorie->nb_bonplan ; ?>)</option>
        <?php } ?>
    </select>
    </div>
    </td>
  </tr>
  
  <!--<tr>
    <td>
    <a href="Javascript:void();" id="btn_geolocalisation_mobile">
    <img src="<?php // echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp2b13b3ee_06.png" alt="" border="0">
    </a>
    </td>
  </tr>-->
  <tr>
    <td>
    <a href="Javascript:void();" id="btn_submit_search_partner_mobile">
    <img src="<?php echo GetImagePath("front/"); ?>/mobile2013/bp_validation.png" width="304" height="58" alt="validation" /></a>
    </td>
  </tr>
  
</table>
</form>
    
    
    
    
  </div>








</div>

</div>
<?php 
$this->load->view("mobile2013/includes/footer.php", $data);
?>

