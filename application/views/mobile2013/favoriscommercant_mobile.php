<?php
$data["zTitle"] = 'Mobile page';
$this->load->view("mobile2013/includes/main_header_favoris.php", $data);
?>
<!--excemple -->
<?php $data['empty'] = null; ?>
<?php $this->load->view("sortez_vsv/includes/main_menu_mobile_index", $data);
$this->load->view("mobile2013/includes/searchform_content_partenaire", $data); ?>


<div style="width:100%; height:auto; float:left;background-color: #FFFFFF; color:#000000; font-family: Arial, Helvetica, sans-serif;
    font-size: 12px;
    margin-bottom: 0;">


    <style type="text/css">
        .mobile_contener_all {
            width: 100%;
            margin-right: auto;
            margin-left: auto;
        }

        .header_mobile {
            float: left;
            width: 100%;
            position: relative;
        }

        .main_mobile {
            float: left;
            width: 100%;
            position: relative;
            padding: 0 15px;
        }

        .main_favoris_card_title {
            color: fuchsia;
            font-size: 20px;
        }

        .main_favoris_card_title_ville {
            font-size: 16px;
        }


    </style>

    <?php if (count($toCommercant) > 0) { ?>
        <div class="col-xs-12" style="text-align:center; font-weight:bold; padding:15px 15px 0 15px; font-size: 25px;">Mes favoris
        </div>
    <?php } ?>


    <div class="mobile_contener_all">

        <div class="main_mobile">

            <?php
            if (isset($user_groups) && $user_groups->id != 2) echo '<div style=" color:#FF0000; font-weight:bold; text-align:center; padding:10px;">Seuls les consommateurs ont une liste de partenaires favoris.</div>';
            ?>

            <div style="background-color:#FFF;"></div>

            <?php if (count($toCommercant) == 0) { ?>
                <div class="col-xs-12" style="text-align:center; font-weight:bold; padding:15px;">Liste vide</div>
            <?php } ?>


            <?php foreach ($toCommercant as $oCommercant) {

                $objasscommrubr = $this->sousRubrique->GetById($oCommercant->IdSousRubrique);

                $oInfoCommercant = $oCommercant;
                $base_path_system = str_replace('system/', '', BASEPATH);
                include($base_path_system . 'application/views/front2013/includes/url_var_definition.php');
                //$this->load->view("front2013/includes/url_var_definition", $data);

                $thisss =& get_instance();
                $thisss->load->model("ion_auth_used_by_club");
                $ionauth_id = $thisss->ion_auth_used_by_club->get_ion_id_from_commercant_id($oCommercant->IdCommercant);

                $photoCommercant_path = "application/resources/front/photoCommercant/imagesbank/" . $ionauth_id . "/";
                $photoCommercant_path_old = "application/resources/front/photoCommercant/images/";
                ?>

                <div class="col-xs-12 col-md-6  col-lg-3" style="margin: 15px 0 0 0; display: table; background-color: #E5E5E5; padding: 0;">

                    <div class="col-xs-12 padding0" style="height: 220px; overflow: hidden;">
                        <?php
                        $image_home_vignette = "";
                        if (isset($oCommercant->Photo1) && $oCommercant->Photo1 != "" && is_file($photoCommercant_path . $oCommercant->Photo1) == true) {
                            $image_home_vignette = $oCommercant->Photo1;
                        } else if ($image_home_vignette == "" && isset($oCommercant->Photo2) && $oCommercant->Photo2 != "" && is_file($photoCommercant_path . $oCommercant->Photo2) == true) {
                            $image_home_vignette = $oCommercant->Photo2;
                        } else if ($image_home_vignette == "" && isset($oCommercant->Photo3) && $oCommercant->Photo3 != "" && is_file($photoCommercant_path . $oCommercant->Photo3) == true) {
                            $image_home_vignette = $oCommercant->Photo3;
                        } else if ($image_home_vignette == "" && isset($oCommercant->Photo4) && $oCommercant->Photo4 != "" && is_file($photoCommercant_path . $oCommercant->Photo4) == true) {
                            $image_home_vignette = $oCommercant->Photo4;
                        } else if ($image_home_vignette == "" && isset($oCommercant->Photo5) && $oCommercant->Photo5 != "" && is_file($photoCommercant_path . $oCommercant->Photo5) == true) {
                            $image_home_vignette = $oCommercant->Photo5;
                        } else if ($image_home_vignette == "" && isset($oCommercant->Photo1) && $oCommercant->Photo1 != "" && is_file($photoCommercant_path_old . $oCommercant->Photo1) == true) {
                            $image_home_vignette = $oCommercant->Photo1;
                        } else if ($image_home_vignette == "" && isset($oCommercant->Photo2) && $oCommercant->Photo2 != "" && is_file($photoCommercant_path_old . $oCommercant->Photo2) == true) {
                            $image_home_vignette = $oCommercant->Photo2;
                        } else if ($image_home_vignette == "" && isset($oCommercant->Photo3) && $oCommercant->Photo3 != "" && is_file($photoCommercant_path_old . $oCommercant->Photo3) == true) {
                            $image_home_vignette = $oCommercant->Photo3;
                        } else if ($image_home_vignette == "" && isset($oCommercant->Photo4) && $oCommercant->Photo4 != "" && is_file($photoCommercant_path_old . $oCommercant->Photo4) == true) {
                            $image_home_vignette = $oCommercant->Photo4;
                        } else if ($image_home_vignette == "" && isset($oCommercant->Photo5) && $oCommercant->Photo5 != "" && is_file($photoCommercant_path_old . $oCommercant->Photo5) == true) {
                            $image_home_vignette = $oCommercant->Photo5;
                        }
                        ?>
                        <a onclick='javascript:window.open("<?php echo site_url($commercant_url_home); ?>/presentation", "<?php echo $commercant_url_home; ?>", "width=1045, height=800, scrollbars=yes");'
                           href='javascript:void(0);'>
                            <?php
                            if ($image_home_vignette != "") {
                                if (isset($image_home_vignette) && $image_home_vignette != "" && is_file($photoCommercant_path . $image_home_vignette) == true)
                                    echo '<img src="' . base_url() . $photoCommercant_path . $image_home_vignette . '" width="100%"/>';
                                else echo '<img src="' . base_url() . $photoCommercant_path_old . $image_home_vignette . '" width="100%"/>';
                            } else {
                                $image_home_vignette_to_show = GetImagePath("front/") . "/no_image_annuaire.png";
                                echo '<img src="' . $image_home_vignette_to_show . '" width="100%"/>';
                            }
                            ?>
                        </a>
                        <div style="position:absolute; top:180px; width:100%; height:40px; background:rgba(0, 0, 0, 0.5); color:#FFFFFF; font-weight: normal; text-transform:uppercase; text-align:center; line-height:40px;">
                            <?php if ($objasscommrubr) echo $objasscommrubr->Nom; ?>
                        </div>
                     </div>

                    <div class="col-xs-12 padding15 main_favoris_card_text_container">

                        <div class="col-lg-12 padding0">
                            <div class="col-sm-12 padding0 main_favoris_card_title_container" style="text-align: center;">

                                <!--<span style='background-color: transparent;
    color: #000000;
    font-family: "Arial",sans-serif;
    font-size: 16px;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 19px;
    text-decoration: none;
    vertical-align: 0;'><?php if ($objasscommrubr_main) echo $objasscommrubr_main->Nom; ?></span><br/>-->
                                <span class="main_favoris_card_title"><?php echo $oCommercant->NomSociete; ?> <br>
                          </span>
                                <span class="main_favoris_card_title_ville"><?php echo $oCommercant->ville; ?></span>

                                <!--<span style='background-color: transparent;
    color: #000000;
    font-family: "Arial",sans-serif;
    font-size: 16px;
    font-style: normal;
    font-variant: normal;
    font-weight: 700;
    line-height: 20px;
    text-decoration: none;
    vertical-align: 0;'><?php if ($objasscommrubr) echo $objasscommrubr->Nom; ?></span><br/> -->

                            </div>

                        </div>

                    </div>

                    <div class="col-xs-12 text-right" style="padding-bottom: 15px;">
                        <a href="<?php echo site_url("front/utilisateur/delete_favoris/" . $oCommercant->IdCommercant); ?>" title="Suppriemr de mes favoris">
                            <img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/btn_delete_favoriss.png"
                                 alt="partenaire" class="paddingright15"/>
                        </a>
                        <a href="<?php echo site_url($commercant_url_home . "/index"); ?>"
                           style="color:#fff; text-decoration:none;" title="Détails du Professionnel">
                            <img src="<?php echo base_url(); ?>application/resources/privicarte/images/plus_infos_black.png"
                                 alt="partenaire" class="paddingright15">
                        </a>
                    </div>

                </div>

                <?php
            }
            ?>


        </div>

    </div>


</div>

<?php
$this->load->view("mobile2013/includes/footer.php", $data);
?>
