<?php 
$data["zTitle"] = 'Mobile page';
$this->load->view("mobile2013/includes/header.php", $data);
?>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/wpscripts2013/jquery-1.8.3.js"></script>


<div style="width:320px; height:auto; float:left;background-color: #FFFFFF; font-family:Arial, Helvetica, sans-serif;font-size: 12px;">

<div style="background-image: url(<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp568e0424_06.png);
	background-repeat: no-repeat;
	background-position: center top;
	float: left;
	height: 90px;
	width: 320px;
	position: relative;
	background-color: #FFFFFF; margin-bottom:10px;">
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td style=" text-align:left;">
    <?php 
	$data["zTitle"] = 'Mobile page';
	$this->load->view("mobile2013/includes/btn_sommaire.php", $data);
	?>
    </td>
    <td style=" text-align:right;">
    <?php 
	$this->load->view("mobile2013/includes/btn_back.php", $data);
	?>
    </td>
  </tr>
  <tr>
    <td>
    <select name="proximite_tiCategorie" id="proximite_tiCategorie" style="width:100px;" onChange="javascript:change_list_agenda_perso();">
        <option value="0">Quoi ?</option>
        <?php 
		/*if (isset($tiCategorie_array) && is_array($tiCategorie_array)) {
				for ($k=0;$k<count($tiCategorie_array);$k++) {
					$this->load->model("mdl_categories_agenda");
					$tiCategorie_array_categorie = $this->mdl_categories_agenda->getById($tiCategorie_array[$k]);
				   	echo '<option value="'.$tiCategorie_array[$k].'">'.$tiCategorie_array_categorie->category.'</option>';
				}
			}*/
			
		foreach ($tiCategorie_array as $oCategorie_array) {
			echo '<option value="'.$oCategorie_array->agenda_categid.'">'.$oCategorie_array->category.'</option>';
		}
		?>
    </select>
    </td>
    <td>
    <div class="proximite_conteneur_filtreIdQuand" id="proximite_conteneur_filtreIdQuand" style="text-align:right">
    
    <select id="inputStringQuandHidden_to_check" size="1" name="inputStringQuandHidden_to_check" onChange="javascript:change_list_agenda_perso_filtreIdQuand();" style="width:100px;">
        <option value="0" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="0") {?>selected<?php }?>>Quand ?<?php if (isset($toAgndaTout_global)) echo "(".$toAgndaTout_global.")"; else echo "(0)"; ?></option>
        <option value="101" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="101") {?>selected<?php }?>>Aujourd'hui <?php if (isset($toAgndaAujourdhui_global)) echo "(".$toAgndaAujourdhui_global.")"; else echo "(0)"; ?></option>
        <option value="202" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="202") {?>selected<?php }?>>Ce Week-end <?php if (isset($toAgndaWeekend_global)) echo "(".$toAgndaWeekend_global.")"; else echo "(0)"; ?></option>
        <option value="303" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="303") {?>selected<?php }?>>Cette semaine <?php if (isset($toAgndaSemaine_global)) echo "(".$toAgndaSemaine_global.")"; else echo "(0)"; ?></option>
        <option value="404" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="404") {?>selected<?php }?>>Semaine prochaine <?php if (isset($toAgndaSemproch_global)) echo "(".$toAgndaSemproch_global.")"; else echo "(0)"; ?></option>
        <!--<option value="505" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="505") {?>selected<?php // }?>>Ce mois <?php // if (isset($toAgndaMois_global)) echo "(".$toAgndaMois_global.")"; else echo "(0)"; ?></option>-->
        <option value="01" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="01") {?>selected<?php }?>>Janvier <?php if (isset($toAgndaJanvier_global)) echo "(".$toAgndaJanvier_global.")"; else echo "(0)"; ?></option>
        <option value="02" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="02") {?>selected<?php }?>>Février <?php if (isset($toAgndaFevrier_global)) echo "(".$toAgndaFevrier_global.")"; else echo "(0)"; ?></option>
        <option value="03" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="03") {?>selected<?php }?>>Mars <?php if (isset($toAgndaMars_global)) echo "(".$toAgndaMars_global.")"; else echo "(0)"; ?></option>
        <option value="04" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="04") {?>selected<?php }?>>Avril <?php if (isset($toAgndaAvril_global)) echo "(".$toAgndaAvril_global.")"; else echo "(0)"; ?></option>
        <option value="05" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="05") {?>selected<?php }?>>Mai <?php if (isset($toAgndaMai_global)) echo "(".$toAgndaMai_global.")"; else echo "(0)"; ?></option>
        <option value="06" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="06") {?>selected<?php }?>>Juin <?php if (isset($toAgndaJuin_global)) echo "(".$toAgndaJuin_global.")"; else echo "(0)"; ?></option>
        <option value="07" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="07") {?>selected<?php }?>>Juillet <?php if (isset($toAgndaJuillet_global)) echo "(".$toAgndaJuillet_global.")"; else echo "(0)"; ?></option>
        <option value="08" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="08") {?>selected<?php }?>>Août <?php if (isset($toAgndaAout_global)) echo "(".$toAgndaAout_global.")"; else echo "(0)"; ?></option>
        <option value="09" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="09") {?>selected<?php }?>>Septembre <?php if (isset($toAgndaSept_global)) echo "(".$toAgndaSept_global.")"; else echo "(0)"; ?></option>
        <option value="10" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="10") {?>selected<?php }?>>Octobre <?php if (isset($toAgndaOct_global)) echo "(".$toAgndaOct_global.")"; else echo "(0)"; ?></option>
        <option value="11" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="11") {?>selected<?php }?>>Novembre <?php if (isset($toAgndaNov_global)) echo "(".$toAgndaNov_global.")"; else echo "(0)"; ?></option>
        <option value="12" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="12") {?>selected<?php }?>>Décembre <?php if (isset($toAgndaDec_global)) echo "(".$toAgndaDec_global.")"; else echo "(0)"; ?></option>
    </select>
    
    </div>
    </td>
  </tr>
</table>

</div>


<script type="text/javascript">

function change_list_agenda_perso(){
    var proximite_tiDepartement = $("#proximite_tiDepartement").val();
	var proximite_tiDeposant = $("#proximite_tiDeposant").val();
	var proximite_tiCategorie = $("#proximite_tiCategorie").val();
	var proximite_tiDossperso = $("#proximite_tiDossperso").val();
	//var inputStringQuandHidden_to_check = $("#inputStringQuandHidden_to_check").val();
	
	$('#proximite_conteneur_list_id').html('<div style="text-align:center;"><img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...." id="img_loaging_agenda_x"/></div>') ; 
	
	$('#proximite_conteneur_filtreIdQuand').html('<div style="text-align:center;"><img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...." id="img_loaging_agenda_x"/></div>') ; 
	
	$.post(
		'<?php echo site_url("agenda/agenda_perso_check"); ?>',
		{
		proximite_tiDepartement: proximite_tiDepartement,
		proximite_tiDeposant: proximite_tiDeposant,
		proximite_tiCategorie: proximite_tiCategorie,
		proximite_tiDossperso: proximite_tiDossperso
		},
		function (zReponse)
		{
			$('#proximite_conteneur_list_id').html(zReponse);
	   });
	   
	$.post(
		'<?php echo site_url("agenda/agenda_perso_check_filterQuand"); ?>',
		{
		proximite_tiDepartement: proximite_tiDepartement,
		proximite_tiDeposant: proximite_tiDeposant,
		proximite_tiCategorie: proximite_tiCategorie,
		proximite_tiDossperso: proximite_tiDossperso
		},
		function (zReponse)
		{
			$('#proximite_conteneur_filtreIdQuand').html(zReponse);
	   });   
	
} 

function change_list_agenda_perso_filtreIdQuand(){
    var proximite_tiDepartement = $("#proximite_tiDepartement").val();
	var proximite_tiDeposant = $("#proximite_tiDeposant").val();
	var proximite_tiCategorie = $("#proximite_tiCategorie").val();
	var proximite_tiDossperso = $("#proximite_tiDossperso").val();
	var inputStringQuandHidden_to_check = $("#inputStringQuandHidden_to_check").val();
	
	$('#proximite_conteneur_list_id').html('<div style="text-align:center;"><img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...." id="img_loaging_agenda_x"/></div>') ; 
	
	$.post(
		'<?php echo site_url("agenda/agenda_perso_check"); ?>',
		{
		proximite_tiDepartement: proximite_tiDepartement,
		proximite_tiDeposant: proximite_tiDeposant,
		proximite_tiCategorie: proximite_tiCategorie,
		proximite_tiDossperso: proximite_tiDossperso,
		inputStringQuandHidden_to_check: inputStringQuandHidden_to_check
		},
		function (zReponse)
		{
			$('#proximite_conteneur_list_id').html(zReponse);
	   });
	 
	
} 


function proximite_perso_view_details(idEvent) {
		//alert("it is ok !");
		$.post(
		'<?php echo site_url("agenda/set_iframe_session_navigation"); ?>',
		{idEvent: idEvent},
		function (zReponse)
		{
			if(zReponse=="1") window.location = "<?php echo base_url();?>agenda/details_event/"+idEvent;
	   });
}

</script>



<div style="width:320px;">

<div class="proximite_conteneur_filtre" style="margin-bottom:0px; margin-top:0px; text-align:left;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <input type="hidden" name="proximite_tiDepartement" id="proximite_tiDepartement" value="<?php if (isset($tiDepartement_array)) echo $tiDepartement_array; else echo "0"; ?>"/>
    <!--<td>
    <select name="proximite_tiDepartement" id="proximite_tiDepartement" style="width:200px;" onChange="javascript:change_list_agenda_perso();">
        <option value="0">Toutes les communes</option>
        <?php /* 
		if (isset($tiDepartement_array) && is_array($tiDepartement_array)) {
				for ($i=0;$i<count($tiDepartement_array);$i++) {
					$this->load->model("mdlville");
					$tiDepartement_array_ville = $this->mdlville->getVilleById($tiDepartement_array[$i]);
				   	echo '<option value="'.$tiDepartement_array[$i].'">'.$tiDepartement_array_ville->Nom.'</option>';
				}
			}*/
		?>
    </select>
    </td>-->
    <!--<td>
    <select name="proximite_tiDeposant" id="proximite_tiDeposant" style="width:200px;" onChange="javascript:change_list_agenda_perso();">
        <option value="0">Tous les déposants</option>
        <?php 
		/*if (isset($tiDeposant_array) && is_array($tiDeposant_array)) {
				for ($j=0;$j<count($tiDeposant_array);$j++) {
					$this->load->model("Commercant");
					$tiDeposant_array_commercant = $this->Commercant->GetById($tiDeposant_array[$j]);
				   	echo '<option value="'.$tiDeposant_array[$j].'">'.$tiDeposant_array_commercant->NomSociete.'</option>';
				}
			}*/
		?>
    </select>
    </td>-->
    <input type="hidden" name="proximite_tiDeposant" id="proximite_tiDeposant" value="<?php if (isset($tiDeposant_array)) echo $tiDeposant_array;  else echo "0"; ?>"/>
    <td>
    
    
    </td>
    <td>
    
    
    
    
    </td>
  </tr>
</table>
</div>

<input type="hidden" name="proximite_tiDossperso" id="proximite_tiDossperso" value="<?php if (isset($tiDossperso)) echo $tiDossperso;?>"/>

<div class="proximite_conteneur_list" id="proximite_conteneur_list_id">


<div style="height:3px; width:100%; background-color:#000000"></div>

<?php  foreach($toAgenda as $oAgenda){ ?>
		
      <!--<a href="javascript:void(0);" onClick="javascript:proximite_perso_view_details(<?php echo $oAgenda->id; ?>);" style="color:#000000; text-decoration:none;">-->
      <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin:3px">
          <tr>
            
            <td style="width:90px">
	            <a href="javascript:void(0);" onClick="javascript:proximite_perso_view_details(<?php echo $oAgenda->id; ?>);" style="color:#000000; text-decoration:none;">
            	<?php 
				$image_home_vignette = "";
				if (isset($oAgenda->photo1) && $oAgenda->photo1 != "" && is_file("application/resources/front/images/agenda/photoCommercant/".$oAgenda->photo1)==true){ $image_home_vignette = $oAgenda->photo1;}
				else if ($image_home_vignette == "" && isset($oAgenda->photo2) && $oAgenda->photo2 != "" && is_file("application/resources/front/images/agenda/photoCommercant/".$oAgenda->photo2)==true){ $image_home_vignette = $oAgenda->photo2;}
				else if ($image_home_vignette == "" && isset($oAgenda->photo3) && $oAgenda->photo3 != "" && is_file("application/resources/front/images/agenda/photoCommercant/".$oAgenda->photo3)==true){ $image_home_vignette = $oAgenda->photo3;}
				else if ($image_home_vignette == "" && isset($oAgenda->photo4) && $oAgenda->photo4 != "" && is_file("application/resources/front/images/agenda/photoCommercant/".$oAgenda->photo4)==true){ $image_home_vignette = $oAgenda->photo4;}
				else if ($image_home_vignette == "" && isset($oAgenda->photo5) && $oAgenda->photo5 != "" && is_file("application/resources/front/images/agenda/photoCommercant/".$oAgenda->photo5)==true){ $image_home_vignette = $oAgenda->photo5;}
				////$this->firephp->log($image_home_vignette, 'image_home_vignette');
				
				//showing category img if all image of agenda is null
				$this->load->model("mdl_categories_agenda");
				$toCateg_for_agenda = $this->mdl_categories_agenda->getById($oAgenda->agenda_categid);
				if ($image_home_vignette == "" && isset($toCateg_for_agenda->images) && $toCateg_for_agenda->images != "" && is_file("application/resources/front/images/agenda/category/".$toCateg_for_agenda->images)==true){ 
					echo '<img src="'.GetImagePath("front/").'/agenda/category/'.$toCateg_for_agenda->images.'" width="90"/>';
				} else {
				
					if ($image_home_vignette != ""){
						$img_photo_split_array = explode('.',$image_home_vignette);
						$img_photo_path = "application/resources/front/images/agenda/photoCommercant/".$img_photo_split_array[0]."_thumb_100_100.".$img_photo_split_array[1];
						if (is_file($img_photo_path)==false) {
							echo image_thumb("application/resources/front/images/agenda/photoCommercant/" . $image_home_vignette, 100, 100,'','');
						} else echo '<img src="'.GetImagePath("front/").'/agenda/photoCommercant/'.$img_photo_split_array[0]."_thumb_100_100.".$img_photo_split_array[1].'" width="90"/>';
						
					} else {
						$image_home_vignette_to_show = GetImagePath("front/")."/wp71b211d2_06.png";
						echo '<img src="'.$image_home_vignette_to_show.'" width="90"/>';
					}
				
				}
				?>
                </a>
            
            </td>
            <td width="190" valign="top" style="padding-left:3px; padding-right:0px;">
	            <a href="javascript:void(0);" onClick="javascript:proximite_perso_view_details(<?php echo $oAgenda->id; ?>);" style="color:#000000; text-decoration:none;">
                <span class="titre_agenda_perso">
                <strong>
                <?php echo $oAgenda->category ; ?><br />
                <?php echo $oAgenda->nom_manifestation ; ?><br />
                </strong>
                </span>
              <span style="font-size:12px;">
              <?php echo $oAgenda->ville ; ?>, <?php echo $oAgenda->adresse_localisation ; ?>, <?php echo $oAgenda->codepostal_localisation ; ?><br />
                <?php 
                if ($oAgenda->date_debut == $oAgenda->date_fin) echo "Le ".translate_date_to_fr($oAgenda->date_debut); 
                else echo "Du ".translate_date_to_fr($oAgenda->date_debut)." au ".translate_date_to_fr($oAgenda->date_fin);
                ?>
              </span>
              </a>
            </td>
            
            <td>
            	<a href="javascript:void(0);" onClick="javascript:proximite_perso_view_details(<?php echo $oAgenda->id; ?>);" style="color:#000000; text-decoration:none;">
                	<img src="<?php echo GetImagePath("front/"); ?>/mobile2013/fleche_agenda_mobile.png" width="30" height="62" alt="details" /> 
                </a>
            </td>
              
            
            
        </tr>
      </table>
      <!--</a>-->
      <div style="height:3px; width:100%; background-color:#000000"></div>
<?php  } ?>


</div>

</div>








</div>
<?php 
$this->load->view("mobile2013/includes/footer.php", $data);
?>