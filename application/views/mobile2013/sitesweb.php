<!DOCTYPE html>
<html lang="fr">
  
<!-- Mirrored from www.proximite-magazine.com/mobile/sites-web.html by HTTrack Website Copier/3.x [XR&CO'2010], Wed, 11 Dec 2013 07:31:16 GMT -->
<head>
    <meta charset="UTF-8">
    <title>sites-web</title>
    <meta name="generator" content="Serif WebPlus X7">
    <meta name="viewport" content="width=320">
    <link rel="stylesheet" type="text/css" href="<?php echo GetImagePath("front/"); ?>/mobile2013/wpscripts/wpstyles.css">
    <style type="text/css">
      .P-1 { text-align:center;margin-bottom:0.0px;font-family:"Arial", sans-serif;font-size:13.3px; }
      .P-2 { text-align:center;margin-bottom:0.0px;font-family:"Arial", sans-serif;font-weight:700;font-size:14.7px; }
      .P-3 { text-align:center; }
    </style>
  </head>
  <body style="height:480px;background:#003566 url('<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp9e1de8e1_05_06.jpg') repeat scroll center top;">
    <div id="divMain" style="background:#ffffff;margin-left:auto;margin-right:auto;position:relative;width:320px;height:480px;">
      <img alt="" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp7b85a3b4_05_06.jpg" style="position:absolute;left:64px;top:105px;width:209px;height:167px;">
      <map id="map1" name="map1">
        <area shape="poly" coords="219,39,213,39,213,36,218,36,218,33,213,33,213,31,219,31,219,27,209,27,209,42,219,42" title="">
        <area shape="poly" coords="207,42,208,42,208,28,204,28,204,36,200,28,196,28,196,43,199,42,200,42,200,34,204,42,204,43" title="">
        <area shape="rect" coords="190,28,195,43" title="">
        <area shape="poly" coords="188,42,188,38,182,38,188,32,188,28,177,28,177,32,183,32,177,38,177,43,188,43" title="">
        <area shape="poly" coords="177,42,176,39,175,36,174,33,174,29,173,28,166,28,166,32,165,35,164,38,163,41,163,42,168,42,168,40,172,40,172,43,177,43" title="">
        <area shape="poly" coords="171,36,169,36,169,34,170,33,171,35" title="">
        <area shape="poly" coords="158,42,161,41,162,41,162,34,156,34,156,37,158,38,154,38,154,33,157,32,160,31,160,27,155,27,153,29,151,31,150,34,149,36,150,39,152,41,153,42,156,43" title="">
        <area shape="poly" coords="150,42,149,39,148,36,147,33,147,29,146,28,139,28,139,32,138,35,137,38,136,41,136,42,141,42,141,40,145,40,145,43,150,43" title="">
        <area shape="poly" coords="144,36,142,36,142,34,143,33,144,35" title="">
        <area shape="poly" coords="135,28,131,28,128,36,126,28,121,28,121,42,125,42,125,34,127,41,130,42,130,41,132,34,132,42,136,42" title="">
        <area shape="poly" coords="113,39,107,39,107,36,112,36,112,33,107,33,107,31,113,31,113,27,103,27,103,42,113,42" title="">
        <area shape="poly" coords="102,39,96,39,96,28,92,28,92,42,102,42" title="">
        <area shape="poly" coords="312,35,314,29,314,13,312,9,283,9,281,16,281,35,283,36,311,36" href="<?php echo site_url("front/mobile/sommaire"); ?>" alt="" title="">
        <area shape="poly" coords="311,35,314,31,314,12,311,9,283,9,281,14,281,34,283,36,311,36" title="">
        <area shape="poly" coords="39,35,41,29,41,13,38,9,10,9,8,13,7,16,7,30,10,36,38,36" href="javascript:history.back()" alt="" title="">
      </map>
      <img src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp1e28521b_06.png" alt="" width="320" height="114" usemap="#map1" style="position:absolute;left:0px;top:0px;width:320px;height:114px;">
      <map id="map2" name="map2">
        <area shape="poly" coords="320,1,257,0,257,65,320,65" href="<?php echo site_url("auth") ; ?>" alt="" title="">
        <area shape="rect" coords="193,0,258,66" title="" href="<?php echo site_url("front/utilisateur/liste_favoris/") ; ?>">
        <area shape="rect" coords="129,0,194,66" href="<?php echo site_url("agenda") ; ?>" alt="" title="">
        <area shape="poly" coords="129,3,66,2,66,65,129,65" href="<?php echo base_url(); ?>" alt="" title="">
        <area shape="rect" coords="0,2,67,66" href="http://www.proximite-magazine.com/indexmobilemag.html" alt="" title="">
      </map>
      <img src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp2956457a_06.png" alt="" width="320" height="65" usemap="#map2" style="position:absolute;left:0px;top:415px;width:320px;height:65px;">
      <div style="position:absolute;left:19px;top:278px;width:276px;height:137px;overflow:hidden;">
        <p class="Corps P-1">Notre rédaction a sélectionnés a regroupés plusieurs sites nationaux et locaux dans les domaines suivants : administratifs, loisirs, transports, services &nbsp;divers, tourisme..<br>Nous vous invitons à les découvrir su votre tablette ou ordinateur sur :</p>
        <p class="Corps P-2">http://www.proximite-<wbr>magazine.com</p>
        <p class="Corps P-3"><br></p>
      </div>
    </div>
  </body>

<!-- Mirrored from www.proximite-magazine.com/mobile/sites-web.html by HTTrack Website Copier/3.x [XR&CO'2010], Wed, 11 Dec 2013 07:31:18 GMT -->
</html>
