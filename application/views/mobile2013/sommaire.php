<!DOCTYPE html>
<html lang="fr">
  
<!-- Mirrored from www.proximite-magazine.com/mobile/sommaire.html by HTTrack Website Copier/3.x [XR&CO'2010], Wed, 11 Dec 2013 07:28:05 GMT -->
<head>
    <meta charset="UTF-8">
    <title>sommaire</title>
    <meta name="generator" content="Serif WebPlus X7">
    <meta name="viewport" content="width=320">
    <link rel="stylesheet" type="text/css" href="<?php echo GetImagePath("front/"); ?>/mobile2013/wpscripts/wpstyles.css">
  </head>
  <body style="height:2000px;background:#003566 url('<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp9e1de8e1_05_06.jpg') repeat scroll center top;">
    <div id="divMain" style="background:#ffffff;margin-left:auto;margin-right:auto;position:relative;width:320px;height:2000px;">
      <a href="<?php echo site_url("front/contact"); ?>"><img src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wpbd4345bf_06.png" alt="" width="320" height="78" style="position:absolute;left:0px;top:1631px;width:320px;height:78px;"></a>
      <map id="map1" name="map1"><area shape="poly" coords="241,29,241,5,6,5,6,42,241,42" href="http://www.proximite-magazine.com/mobile/nos-selections.html" alt="" title=""></map>
      <img alt="nos-selections" usemap="#map1" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp6649e865_06.png" style="position:absolute;left:36px;top:1467px;width:249px;height:51px;">
      <img alt="" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wpd0ab1209_06.png" style="position:absolute;left:43px;top:145px;width:92px;height:11px;">
      <map id="map2" name="map2">
        <area shape="poly" coords="305,32,301,32,301,25,297,25,297,32,293,32,293,22,295,20,297,18,300,17,301,19,305,23" href="http://www.proximite-magazine.com/" alt="" title="">
        <area shape="poly" coords="306,24,305,21,303,19,301,17,299,15,297,17,295,19,293,21,292,23,290,21,292,20,295,19,296,16,298,14,299,13,301,15,303,17,305,19,307,21,308,22" href="http://www.proximite-magazine.com/" alt="" title="">
        <area shape="poly" coords="312,35,314,29,314,13,312,9,283,9,281,16,281,35,283,36,311,36" href="http://www.proximite-magazine.com/" alt="" title="">
        <area shape="poly" coords="305,29,305,23,302,20,299,17,297,18,295,20,293,22,293,32,297,32,297,25,301,25,301,32,305,32" title="">
        <area shape="poly" coords="292,23,295,20,296,18,298,17,299,16,306,23,308,22,306,20,304,18,299,13,295,17,293,19,295,17,295,14,292,14,292,20,290,22,291,24" title="">
        <area shape="rect" coords="293,14,296,20" title="">
        <area shape="poly" coords="308,22,299,13,290,22,291,23,299,16,307,23" title="">
        <area shape="poly" coords="305,23,299,17,293,23,293,32,297,32,297,25,301,25,301,32,305,32" title="">
        <area shape="poly" coords="311,35,314,31,314,12,311,9,283,9,281,14,281,34,283,36,311,36" title="">
        <area shape="poly" coords="196,42,199,42,195,38,198,35,198,30,192,27,186,27,186,36,187,38,186,40,186,42,191,42,191,39,194,42,194,43" title="">
        <area shape="poly" coords="185,39,185,27,181,27,180,28,180,40,179,34,178,30,177,27,170,27,170,28,169,35,167,41,167,32,166,28,162,27,161,30,160,32,159,35,158,31,157,27,152,27,152,37,151,34,150,31,150,27,146,27,145,30,144,32,143,35,142,31,141,27,136,27,136,37,135,41,135,42,140,42,140,37,142,41,145,41,146,38,147,34,147,42,155,42,156,38,156,35,157,38,158,42,161,39,162,37,163,34,163,42,172,42,175,39,176,42,185,42" title="">
        <area shape="poly" coords="130,42,133,40,134,38,135,35,134,31,131,28,130,27,125,27,121,31,120,33,120,37,117,34,113,33,112,32,114,31,117,33,120,30,117,27,112,27,109,28,107,32,110,35,113,37,115,39,111,37,108,39,106,40,110,43,119,43,120,40,120,38,121,41,127,43" title="">
        <area shape="poly" coords="174,36,173,36,173,33,174,32" title="">
        <area shape="poly" coords="128,39,126,39,126,38,124,37,124,35,125,34,126,32,129,32,129,33,130,34,130,37" title="">
        <area shape="poly" coords="193,35,191,35,191,31,192,31,193,32" title="">
        <area shape="poly" coords="210,41,210,38,204,38,204,36,209,36,209,33,204,33,204,30,210,30,210,27,199,27,199,42,210,42" title="">
        <area shape="poly" coords="210,41,210,38,204,38,204,36,209,36,209,33,204,33,204,30,210,30,210,27,199,27,199,42,210,42" title="">
        <area shape="poly" coords="195,42,198,41,196,39,195,37,197,35,198,32,197,30,195,28,192,27,186,27,186,42,187,43,187,42,191,42,192,39,193,42,194,43" title="">
        <area shape="poly" coords="192,35,191,35,191,31,192,31,193,32,193,34" title="">
        <area shape="poly" coords="185,28,185,27,181,27,181,28,180,42,185,42" title="">
        <area shape="poly" coords="180,41,179,38,179,34,178,31,177,28,177,27,170,27,170,31,169,34,168,37,167,40,167,42,172,42,172,39,175,40,176,42,180,42" title="">
        <area shape="poly" coords="175,36,173,36,173,33,174,32,174,34" title="">
        <area shape="poly" coords="167,41,166,28,162,27,162,28,159,35,157,28,157,27,152,27,152,28,151,41,155,42,156,33,158,41,160,41,163,33,163,42" title="">
        <area shape="poly" coords="151,41,150,28,146,27,146,28,143,35,141,28,141,27,136,27,136,28,135,41,139,42,140,33,142,41,144,41,147,33,147,42" title="">
        <area shape="poly" coords="128,39,126,39,126,38,125,37,124,35,125,34,125,32,128,32,129,33,130,34,130,37" title="">
        <area shape="poly" coords="130,42,132,40,134,38,134,32,133,30,131,28,129,27,125,27,123,29,121,31,120,34,120,38,121,40,123,42,125,43,129,43" title="">
        <area shape="poly" coords="115,42,118,41,119,38,118,35,115,34,113,33,112,32,114,31,116,33,117,33,120,30,118,28,116,27,111,27,109,29,108,32,109,35,112,36,115,37,115,38,111,38,110,37,107,40,109,42,112,43" title="">
        <area shape="poly" coords="39,35,41,29,41,13,38,9,10,9,8,13,7,16,7,30,10,36,38,36" href="javascript:history.back()" alt="" title="">
      </map>
      <img src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wpb3b64710_06.png" alt="" width="320" height="114" usemap="#map2" style="position:absolute;left:0px;top:0px;width:320px;height:114px;">
      <map id="map3" name="map3"><area shape="poly" coords="237,27,237,2,2,2,2,39,237,39" href="http://fr.calameo.com/read/00028096322e2845df403" alt="" title=""></map>
      <img alt="" usemap="#map3" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp1c9e1778_06.png" style="position:absolute;left:40px;top:165px;width:245px;height:47px;">
      <map id="map4" name="map4"><area shape="poly" coords="237,27,237,2,2,2,2,39,237,39" href="http://www.youtube.com/user/MAGAZINEPROXIMITE" alt="" title=""></map>
      <img alt="" usemap="#map4" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp36c4247d_06.png" style="position:absolute;left:40px;top:263px;width:245px;height:47px;">
      <map id="map5" name="map5"><area shape="poly" coords="236,27,236,2,1,2,1,39,236,39" href="http://www.proximite-magazine.com/mobile/sites-web.html" alt="" title=""></map>
      <img alt="sites-web" usemap="#map5" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp2d1f5e2e_06.png" style="position:absolute;left:40px;top:214px;width:245px;height:47px;">
      <map id="map6" name="map6"><area shape="poly" coords="239,26,239,1,3,1,3,38,239,38" href="<?php echo site_url("agenda") ; ?>" alt="" title=""></map>
      <img alt="liste-complete-agenda" usemap="#map6" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wpeedcbf08_06.png" style="position:absolute;left:40px;top:350px;width:246px;height:47px;">
      <map id="map7" name="map7"><area shape="poly" coords="237,27,237,2,2,2,2,39,237,39" href="<?php echo site_url("front/bonplan"); ?>" alt="" title=""></map>
      <img alt="liste-complete-bonplan" usemap="#map7" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp8a6db64f_06.png" style="position:absolute;left:40px;top:591px;width:245px;height:47px;">
      <map id="map8" name="map8"><area shape="poly" coords="237,27,237,2,2,2,2,39,237,39" href="<?php echo site_url("front/annonce"); ?>" alt="" title=""></map>
      <img alt="liste-complete-annonces" usemap="#map8" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp6b9b533a_06.png" style="position:absolute;left:40px;top:542px;width:245px;height:47px;">
      <map id="map9" name="map9"><area shape="poly" coords="236,28,236,3,3,3,3,40,236,40" href="<?php echo site_url("annuaire/bonnes_adresses"); ?>" alt="" title=""></map>
      <img alt="liste-complte-bonnes-adresses" usemap="#map9" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp2880b76d_06.png" style="position:absolute;left:40px;top:491px;width:245px;height:49px;">
      <img alt="" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wpbc034004_06.png" style="position:absolute;left:42px;top:461px;width:85px;height:12px;">
      <img alt="" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp613bc127_06.png" style="position:absolute;left:42px;top:331px;width:72px;height:13px;">
      <map id="map10" name="map10"><area shape="poly" coords="236,26,236,1,1,1,1,38,236,38" href="<?php echo site_url("front/particuliers/inscription"); ?>" alt="" title=""></map>
      <img alt="inscription-club" usemap="#map10" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp32f5e24e_06.png" style="position:absolute;left:41px;top:684px;width:245px;height:47px;">
		<?php 
	
		$this->load->library('ion_auth');
		$this->load->model("ion_auth_used_by_club");
		
		if ($this->ion_auth->logged_in()) { 
			if (isset($iduser)) $iduser = $iduser; else $iduser = "";
		} else $iduser = "";
		?>
      <map id="map11" name="map11"><area shape="poly" coords="237,27,237,2,2,2,2,39,237,39" href="<?php echo site_url("front/particuliers/inscription/")."/".$iduser;?>" alt="" title=""></map>
      <img alt="modification-compte" usemap="#map11" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp310c9f76_06.png" style="position:absolute;left:40px;top:830px;width:245px;height:47px;">
      <map id="map12" name="map12"><area shape="poly" coords="237,27,237,2,2,2,2,39,237,39" href="<?php echo site_url("front/utilisateur/liste_favoris/") ; ?>" alt="" title=""></map>
      <img alt="mes-favoris" usemap="#map12" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wpf50c2e3f_06.png" style="position:absolute;left:40px;top:929px;width:245px;height:47px;">
      <!--localisation.html--><map id="map13" name="map13"><area shape="poly" coords="237,27,237,2,2,2,2,39,237,39" href="#" alt="" title=""></map>
      <img alt="localisation" usemap="#map13" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp2fa67722_06.png" style="position:absolute;left:40px;top:879px;width:245px;height:47px;">
      <map id="map14" name="map14">
        <area shape="poly" coords="33,33,12,33,12,27,15,19,24,19,26,20,30,23,32,28" href="<?php echo site_url("auth/login"); ?>" alt="" title="">
        <area shape="poly" coords="237,27,237,2,2,2,2,39,237,39" href="<?php echo site_url("auth/login"); ?>" alt="" title="">
      </map>
      <img alt="accede-moncompte" usemap="#map14" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp677ee1f9_06.png" style="position:absolute;left:40px;top:780px;width:245px;height:47px;">
      <map id="map15" name="map15">
        <area shape="poly" coords="41,29,14,29,14,16,17,16,20,17,22,18,25,20,28,21,29,20,33,18,41,20" href="<?php echo site_url("front/contact"); ?>" alt="" title="">
        <area shape="poly" coords="237,27,237,2,2,2,2,39,237,39" href="<?php echo site_url("front/contact"); ?>" alt="" title="">
      </map>
      <img alt="contact" usemap="#map15" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp293bf5a4_06.png" style="position:absolute;left:40px;top:1029px;width:245px;height:47px;">
      <img alt="" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp60acea69_06.png" style="position:absolute;left:40px;top:981px;width:245px;height:47px;">
      <img alt="" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp8135993a_06.png" style="position:absolute;left:43px;top:661px;width:197px;height:13px;">
      <a href="http://www.proximite-magazine.com/mobile/infos-particuliers.html"><img alt="" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wpa07c4162_06.png" style="position:absolute;left:40px;top:1091px;width:240px;height:370px;"></a>
      <map id="map16" name="map16"><area shape="poly" coords="237,27,237,2,2,2,2,39,237,39" href="<?php echo site_url("front/particuliers/mon_agenda"); ?>" alt="" title=""></map>
      <img alt="liste-monagenda" usemap="#map16" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp1bade99e_06.png" style="position:absolute;left:40px;top:397px;width:245px;height:47px;">
      <img alt="" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp350671af_06.png" style="position:absolute;left:0px;top:1550px;width:320px;height:81px;">
      <!--<a href="infos-particuliers.html">
        <img alt="infos-particuliers" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp5533b116.gif" style="position:absolute;left:13px;top:1427px;width:320px;height:81px;">
      </a>-->
      <img alt="Demandez GRATUITEMENT La carte Proximité, et bénéficiez de multiples avantages" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wpc72ddc8d_06.png" style="position:absolute;left:132px;top:1552px;width:157px;height:51px;">
      <img alt="" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp84d47087_06.png" style="position:absolute;left:294px;top:1562px;width:16px;height:31px;">
      <img alt="" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp163402db_05_06.jpg" style="position:absolute;left:11px;top:1631px;width:116px;height:78px;">
      <img alt="" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp8f83c9ea_06.png" style="position:absolute;left:294px;top:1654px;width:16px;height:31px;">
      <map id="map17" name="map17"><area shape="poly" coords="237,26,237,2,2,2,2,39,237,39" href="http://www.proximite-magazine.com/mobile/ma-carte.html" alt="" title=""></map>
      <img alt="ma carte" usemap="#map17" src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wpab1e435e_06.png" style="position:absolute;left:40px;top:730px;width:245px;height:47px;">
      <map id="map18" name="map18">
        <area shape="poly" coords="102,30,103,29,102,28,102,29,101,30,99,30,99,29,101,29,101,27,100,26,98,26,97,27,97,29,99,31,102,31" title="">
        <area shape="poly" coords="100,28,98,28,99,27" title="">
        <area shape="poly" coords="95,31,95,28,98,28,98,27,95,27,95,22,94,23,93,24,93,27,92,28,93,29,93,31,94,31,95,32" title="">
        <area shape="poly" coords="90,30,91,29,92,28,93,27,93,26,91,26,90,27,89,28,88,27,88,31,90,31" title="">
        <area shape="poly" coords="88,31,88,29,87,28,87,26,85,26,83,28,82,29,82,31,85,31,85,30,86,29,87,30,87,31,88,32" title="">
        <area shape="rect" coords="84,28,86,30" title="">
        <area shape="poly" coords="82,30,83,29,81,29,80,30,78,30,78,28,80,28,80,27,78,27,77,28,77,31,81,31" title="">
        <area shape="poly" coords="74,31,74,29,73,28,73,26,71,26,71,27,69,27,69,29,68,30,68,31,71,31,71,29,72,30,73,31,74,32" title="">
        <area shape="rect" coords="70,28,72,30" title="">
        <area shape="poly" coords="68,31,68,22,67,22,67,32" title="">
        <area shape="poly" coords="68,25,70,24,69,22,68,20,69,18,70,16,67,16,66,18,65,16,62,16,63,18,64,20,65,21,63,23,63,25,66,25,67,24,67,26" title="">
        <area shape="poly" coords="52,25,51,23,52,21,52,18,50,17,49,16,45,16,45,19,44,17,42,16,37,16,37,26,40,26,41,24,43,23,45,21,45,26,48,26,49,24,49,26,53,26" title="">
        <area shape="poly" coords="84,25,84,16,81,16,81,17,79,21,78,19,78,16,75,16,75,18,74,19,74,16,71,16,71,26,73,25,74,23,74,26,77,26,77,21,78,23,78,25,80,24,81,23,82,25,84,26" title="">
        <area shape="poly" coords="103,25,103,23,99,23,102,22,102,20,99,19,99,18,103,18,103,16,99,16,101,14,99,13,98,15,99,16,96,17,88,17,88,16,87,17,85,16,85,26,88,26,88,19,91,19,91,26,94,26,94,19,96,21,96,26,103,26" title="">
        <area shape="poly" coords="58,24,57,24,56,23,56,20,57,19,58,19,60,21,60,22" title="">
        <area shape="poly" coords="48,22,48,19,49,19,49,21" title="">
        <area shape="rect" coords="40,19,43,22" title="">
        <area shape="poly" coords="61,25,62,24,62,18,60,17,59,16,57,16,55,17,54,19,53,20,53,23,54,25,55,26,60,26" title="">
        <area shape="poly" coords="103,25,103,23,99,23,99,22,102,22,102,20,99,20,99,18,103,18,103,16,99,16,101,14,99,13,98,15,99,16,96,16,96,26,103,26" title="">
        <area shape="poly" coords="93,19,96,19,96,17,88,17,88,19,91,19,91,26,94,26" title="">
        <area shape="poly" coords="88,17,85,16,85,26,88,26" title="">
        <area shape="poly" coords="84,25,84,16,81,16,81,17,79,21,78,17,78,16,75,16,75,17,74,25,74,26,77,26,77,20,78,25,80,25,81,20,82,25,82,26" title="">
        <area shape="poly" coords="74,17,73,17,71,16,71,26,73,26" title="">
        <area shape="poly" coords="68,21,70,17,70,16,67,16,66,18,65,16,62,16,62,17,65,21,62,25,65,25,66,23,67,25,70,25" title="">
        <area shape="poly" coords="59,24,57,24,56,23,56,20,57,19,58,19,60,21,59,22" title="">
        <area shape="poly" coords="60,25,62,24,62,19,61,17,59,16,57,16,55,17,54,19,53,21,54,23,55,25,55,26,60,26" title="">
        <area shape="poly" coords="52,25,51,23,52,21,52,18,50,17,49,16,45,16,45,26,48,26,49,24,49,26,53,26" title="">
        <area shape="rect" coords="48,19,50,22" title="">
        <area shape="rect" coords="40,19,43,22" title="">
        <area shape="poly" coords="40,25,41,23,43,22,44,20,44,17,42,16,37,16,37,26,40,26" title="">
        <area shape="poly" coords="109,10,26,10,25,29,109,31" title="">
        <area shape="poly" coords="111,74,113,70,113,11,107,7,12,7,8,13,7,44,8,72,12,75,110,75" href="http://www.proximite-magazine.com/mobile/ma-carte.html" alt="" title="">
      </map>
      <img src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wpd34fe58d_06.png" alt="" width="123" height="85" usemap="#map18" style="position:absolute;left:4px;top:1532px;width:123px;height:85px;">
    </div>
  </body>

<!-- Mirrored from www.proximite-magazine.com/mobile/sommaire.html by HTTrack Website Copier/3.x [XR&CO'2010], Wed, 11 Dec 2013 07:29:13 GMT -->
</html>
