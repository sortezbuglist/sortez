<?php $data["zTitle"] = 'Articles' ?>

<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>
<script>
	function ValidationFormArticle(){
		var zErreur = "";

		var article_typeid = $("#article_typeid").val();
		if (article_typeid == "" || article_typeid == "0"){
			zErreur += "Veuillez selectionner un Type d'article</br>" ;
			$("#article_typeid").css('border-color', 'red');
		} else {
			$("#article_typeid").css('border-color', '#E3E1E2');
		}

		var article_categid = $("#article_categid").val();
		if (article_categid == "" || article_categid == "0"){
			zErreur += "Veuillez selectionner une catégorie d'article</br>" ;
			$("#article_categid").css('border-color', 'red');
		} else {
			$("#article_categid").css('border-color', '#E3E1E2');
		}

		var article_subcategid = $("#article_subcategid").val();
		if (article_subcategid == "" || article_subcategid == "0"){
			zErreur += "Veuillez selectionner une sous-catégorie d'article</br>" ;
			$("#article_subcategid").css('border-color', 'red');
		} else {
			$("#article_subcategid").css('border-color', '#E3E1E2');
		}

		var title = $("#title").val();
		if (title == ""){
			zErreur += "Veuillez indiquer un Titre pour article</br>" ;
			$("#title").css('border-color', 'red');
		} else {
			$("#title").css('border-color', '#E3E1E2');
		}

		/*var content = $("#content").val();
		if (content == ""){
			zErreur += "Veuillez indiquer une description pour article</br>" ;
			$("#content").css('border-color', 'red');
		} else {
			$("#content").css('border-color', '#E3E1E2');
		}*/

		var email = $("#email").val();
		if (!isEmail(email)){
			zErreur += "Veuillez indiquer un Email valide pour l'article</br>" ;
			$("#email").css('border-color', 'red');
		} else {
			$("#email").css('border-color', '#E3E1E2');
		}



		if (zErreur != ""){
			//alert ('Tout les champs sont obligatoires !') ;
			$("#zErreur").html(zErreur);
		}else{
			document.frmCreationArticle.submit();
		}

		return false;
	}

	function getCategoryTypeArticle(){
	      var article_typeid = jQuery('#article_typeid').val();

		  $('#SpanCategoryTypeArticle').html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...."/>');

          $.post(
            '<?php echo site_url("admin/articles/getCategoryTypeArticle"); ?>' + '/' + article_typeid,
			{article_typeid: article_typeid},
            function (zReponse)
            {
                $('#SpanCategoryTypeArticle').html(zReponse);
           });
		   $('#SpanSousCategoryTypeArticle').html(zReponse);

            background-color: #0719DC;

	function getSousCategoryTypeArticle(){
	      var article_categid = jQuery('#article_categid').val();

		  $('#SpanSousCategoryTypeArticle').html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...."/>');

          $.post(
            '<?php echo site_url("admin/articles/getSousCategoryTypeArticle"); ?>' + '/' + article_categid,
			{article_categid: article_categid},
            function (zReponse)
            {
                $('#SpanSousCategoryTypeArticle').html(zReponse);
           });
	}

	function event_handicap_checkbox_clic() {
		if ($('#event_handicap_checkbox').attr('checked')) {
			$("#event_handicap").val("1");
		} else {
			$("#event_handicap").val("0");
		}
	}

	function event_parking_checkbox_clic() {
		if ($('#event_parking_checkbox').attr('checked')) {
			$("#event_parking").val("1");
		} else {
			$("#event_parking").val("0");
		}
	}

	function getCP_event_orgidville(){
	      var event_orgidville = jQuery('#event_orgidville').val();
          jQuery.get(
            '<?php echo site_url("front/professionnels/getPostalCode"); ?>' + '/' + event_orgidville,
            function (zReponse)
            {
                jQuery('#event_orgpostcode').val(zReponse);
           });
	}

	$(document).ready(function() {
		<?php if (!isset($idarticle) || $idarticle=="0" || $idarticle=="" || $idarticle==NULL) {?>
		$("#article_categid").attr("disabled", "disabled");
		$("#article_subcategid").attr("disabled", "disabled");
		<?php } ?>


            color: #0719DC !important;

	$(function() {
			$("#event_startdate").datepicker({
				dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
				dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
				monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
				dateFormat: 'DD, d MM yy',
				autoSize: true
			});
			$("#event_enddate").datepicker({
				dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
				dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
				monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
				dateFormat: 'DD, d MM yy',
				autoSize: true
			});
		});
</script>

<style type="text/css">
.input_fichearticle {
	width:400px;
}
</style>

    <div id="divFicheAnnonce" class="content" align="center">
	     <br>
	     <a style = "color:black; text-decoration:none;" href = "<?php echo site_url("admin/home" ) ; ?>"> <input type = "button" value= "Retour au menu" id = "btn"/> </a><br/><br/>
         <a style = "color:black; text-decoration:none;" href = "<?php echo site_url("admin/articles/index" ) ; ?>"> <input type = "button" value= "Retour aux articles" id = "btn"/> </a>
        <form name="frmCreationArticle" id="frmCreationArticle" action="<?php echo site_url("admin/articles/modification/"); ?>" method="post" enctype="multipart/form-data">

        <input type="hidden" name="Article[idarticle]" id="IdArticle_id" value="<?php if (isset($oArticle)) echo $oArticle->idarticle; else echo "0"; ?>" />
        <input type="hidden" name="Article[IdCommercant]" id="IdCommercant" value="<?php if (isset($IdCommercant)) echo $IdCommercant; else echo "0"; ?>" />

            <h1><?php echo $title ; ?></h1>
			<span><?php if (isset($msg)) { echo $msg ; } ?></span>
            <fieldset>
                <legend>Article</legend>
                <table cellpadding="3" cellspacing="2">

        }

    </style>





    <script type="text/javascript" src="<?php echo GetJsPath("admin/"); ?>/fields.check.js"></script>

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <script type="text/javascript">



        function limite_title(textarea, max) {

            if (textarea.value.length >= max) {

                textarea.value = textarea.value.substring(0, max);

            }

            var reste = max - textarea.value.length;

            var affichage_reste = reste + ' caract&egrave;res restants';

            document.getElementById('max_desc_title').innerHTML = affichage_reste;

        }



        function listeSousRubrique() {



            jQuery('#trReponseRub').html('<img src="<?php echo base_url();?>application/resources/admin/images/wait.gif" alt="loading...." id="img_loaging_agenda_x"/>');



            var irubId = jQuery('#IdCategory').val();

            jQuery.get(

                '<?php echo site_url("admin/Articles/GetallSubcateg_by_categ"); ?>' + '/' + irubId,

                function (zReponse) {

                    // alert (zReponse) ;

                    jQuery('#trReponseRub').html(zReponse);





                });

            //alert(irubId);

        }

        function getCP() {

            var ivilleId = jQuery('#IdVille').val();

            jQuery.get(

                '<?php echo site_url("admin/Articles/getPostalCode_localisation"); ?>' + '/' + ivilleId,

                function (zReponse) {

                    jQuery('#codepostal').val(zReponse);

                });

        }



        function getCP_localisation() {

            var ivilleId = jQuery('#IdVille_localisation').val();

            jQuery.get(

                '<?php echo site_url("admin/Articles/getPostalCode_localisation"); ?>' + '/' + ivilleId,

                function (zReponse) {

                    jQuery('#codepostal_localisation').val(zReponse);

                });

        }



        function deleteFile(_IdArticle, _FileName) {

            jQuery.ajax({

                url: '<?php echo base_url();?>admin/Articles/delete_files/' + _IdArticle + '/' + _FileName,

                dataType: 'html',

                type: 'POST',

                async: true,

                success: function (data) {

                    window.location.reload();

                }

            });



        }



        jQuery(document).ready(function () {



            jQuery("#img_loaging_agenda").hide();

            jQuery("#img_loaging_agenda_addbonplan").hide();

            //init add redacteur

            jQuery("#form_add_redacteur_div").hide();

            jQuery("#add_redacteur_form_loading").hide();

            jQuery("#add_redacteur_form_error").hide();

            //init save email validation article

            jQuery("#form_add_validation_article_div").hide();

            jQuery("#add_validation_article_form_loading").hide();

            jQuery("#add_validation_article_form_error").hide();

            jQuery("#add_validation_article_form_success").hide();

            //init send validation article

            jQuery("#envoyer_validation_article_form_loading").hide();

            jQuery("#envoyer_validation_article_form_error").hide();

            jQuery("#envoyer_validation_article_form_success").hide();

            //localisation init

            jQuery("#adresse_localisation_diffuseur_loading").hide();





            jQuery(".btnSinscrire").click(function () {



                var txtError = "";



                var nom_societe = jQuery("#nom_societe").val();

                if (nom_societe == "" || nom_societe == null) {

                    txtError += "- Veuillez indiquer qui vous-êtes.<br/>";

                    $("#nom_societe").css('border-color', '#FF0000');

                } else {

                    $("#nom_societe").css('border-color', '#FFFFFF');

                }



                var nom_manifestation = jQuery("#nom_manifestation").val();

                if (nom_manifestation == "" || nom_manifestation == null) {

                    txtError += "- Veuillez indiquer un titre à l'article.<br/>";

                    $("#nom_manifestation").css('border-color', '#FF0000');

                } else {

                    $("#nom_manifestation").css('border-color', '#FFFFFF');

                }



                var email = jQuery("#email").val();

                if (email != "") {

                    if (!isEmail(email)) {

                        txtError += "- Veuillez indiquer un adresse email valide.<br/>";

                        $("#email").css('border-color', '#FF0000');

                    } else {

                        $("#email").css('border-color', '#FFFFFF');

                    }

                }



                var date_depot = jQuery("#date_depot").val();

                if (date_depot == "" || date_depot == null) {

                    txtError += "- Veuillez indiquer la date de dépôt.<br/>";

                    $("#date_depot").css('border-color', '#FF0000');

                } else {

                    $("#date_depot").css('border-color', '#FFFFFF');

                }



                var IdCategory = jQuery("#IdCategory").val();

                if (IdCategory == "" || IdCategory == null || IdCategory == "0") {

                    txtError += "- Veuillez indiquer une categorie.<br/>";

                    $("#IdCategory").css('border-color', '#FF0000');

                } else {

                    $("#IdCategory").css('border-color', '#FFFFFF');

                }



                /*var IdSubcategory = jQuery("#IdSubcategory").val();

                 if(IdSubcategory=="" || IdSubcategory==null || IdSubcategory=="0") {

                 txtError += "- Veuillez indiquer une sous-categorie.<br/>";

                 $("#IdSubcategory").css('border-color', '#FF0000');

                 } else {

                 $("#IdSubcategory").css('border-color', '#FFFFFF');

                 }*/



                /*var date_debut = jQuery("#date_debut").val();

                 if(date_debut=="" || date_debut==null) {

                 txtError += "- Veuillez indiquer la date de début.<br/>";

                 $("#date_debut").css('border-color', '#FF0000');

                 } else {

                 $("#date_debut").css('border-color', '#FFFFFF');

                 }



                 var date_fin = jQuery("#date_fin").val();

                 if(date_fin=="" || date_debut==null) {

                 txtError += "- Veuillez indiquer la date de fin.<br/>";

                 $("#date_fin").css('border-color', '#FF0000');

                 } else {

                 $("#date_fin").css('border-color', '#FFFFFF');

                 }*/





                if (txtError == "") {

                    jQuery("#frmInscriptionProfessionnel").submit();

                } else {

                    jQuery("#div_error_agenda_submit").html(txtError);

                }

            });





            $("#adresse_localisation_diffuseur_checkbox").click(function () {

                jQuery("#adresse_localisation_diffuseur_loading").show();

                if ($('#adresse_localisation_diffuseur_checkbox').attr('checked')) {

                    $("#adresse_localisation_diffuseur").val("1");



                    jQuery.ajax({

                        url: '<?php echo site_url("adm/Articles/get_commercant_localisation"); ?>',

                        type: "post",

                        data: {IdCommercant: "<?php echo 301299;?>"},

                        dataType: 'json',

                        success: function (data) {

                            if (data != "error") {

                                jQuery("#nom_localisation").val(data.NomSociete);

                                jQuery("#adresse_localisation").val(data.adresse_localisation);

                                jQuery("#codepostal_localisation").val(data.codepostal_localisation);

                                jQuery("#IdVille_localisation").val(data.IdVille_localisation);

                                jQuery.post(

                                    '<?php echo site_url("admin/Articles/get_ville_localisation"); ?>',

                                    {IdVille: data.IdVille_localisation},

                                    function (zReponse) {

                                        jQuery("#IdVille_Nom_text_localisation").val(zReponse);

                                        jQuery("#adresse_localisation_diffuseur_loading").hide();

                                    });



                            } else {

                                jQuery("#add_redacteur_form_error").show();

                                jQuery("#adresse_localisation_diffuseur_loading").hide();

                            }

                        },

                        error: function (data) {

                            jQuery("#adresse_localisation_diffuseur_error").show();

                            jQuery("#adresse_localisation_diffuseur_error").val("Un erreur s'est produite !");

                            jQuery("#adresse_localisation_diffuseur_loading").hide();

                        }

                    });



                } else {

                    $("#adresse_localisation_diffuseur").val("0");

                    $("#adresse_localisation").val("");

                    $("#IdVille_localisation").val("0");

                    $("#codepostal_localisation").val("");

                    jQuery("#adresse_localisation_diffuseur_loading").hide();

                }





            });



            $("#diffusion_facebook_checkbox").click(function () {

                if ($('#diffusion_facebook_checkbox').attr('checked')) {

                    $("#diffusion_facebook").val("1");

                } else {

                    $("#diffusion_facebook").val("0");

                }

            });



            $("#diffusion_twitter_checkbox").click(function () {

                if ($('#diffusion_twitter_checkbox').attr('checked')) {

                    $("#diffusion_twitter").val("1");

                } else {

                    $("#diffusion_twitter").val("0");

                }

            });



            $("#diffusion_rss_checkbox").click(function () {

                if ($('#diffusion_rss_checkbox').attr('checked')) {

                    $("#diffusion_rss").val("1");

                } else {

                    $("#diffusion_rss").val("0");

                }

            });





            $("#diffusion_rss_checkbox").click(function () {

                if ($('#diffusion_rss_checkbox').attr('checked')) {

                    $("#diffusion_rss").val("1");

                } else {

                    $("#diffusion_rss").val("0");

                }

            });



            //ADD REDACTEUR START /////////////////////////////////////////////////////////////////////

            $("#add_redacteur_form_link_open").click(function () {

                jQuery("#add_redacteur_form_link_add").css('display', 'none');

                jQuery("#add_redacteur_form_link_edit").css('display', 'none');

                jQuery("#add_redacteur_form_link_delete").css('display', 'none');



                jQuery('#add_redacteur_name').val("");

                jQuery('#add_redacteur_id').val("0");



                jQuery("#form_add_redacteur_div").show('blind');

            });



            $("#add_redacteur_form_cancel").click(function () {

                jQuery("#form_add_redacteur_div").hide('blind');



                jQuery("#add_redacteur_form_link_add").css('display', 'inline');

                <?php if (isset($objArticle->redacteur_id) && $objArticle->redacteur_id != null && $objArticle->redacteur_id != "0") { ?>

                jQuery("#add_redacteur_form_link_edit").css('display', 'inline');

                jQuery("#add_redacteur_form_link_delete").css('display', 'inline');

                <?php } else { ?>

                jQuery("#add_redacteur_form_link_edit").css('display', 'none');

                jQuery("#add_redacteur_form_link_delete").css('display', 'none');

                <?php } ?>

                jQuery("#add_redacteur_form_error").css('display', 'none');

                jQuery("#add_redacteur_form_locading").css('display', 'none');

            });



            $("#add_redacteur_form_save").click(function () {

                jQuery("#add_redacteur_form_loading").show();

                if (jQuery('#add_redacteur_name').val() == "" || jQuery('#add_redacteur_name').val() == null) {

                    jQuery("#add_redacteur_form_error").show();

                    jQuery("#add_redacteur_form_locading").css('display', 'none');

                    jQuery("#add_redacteur_form_loading").hide();

                } else {

                    var add_redacteur_name = jQuery('#add_redacteur_name').val();

                    var add_redacteur_id = jQuery('#add_redacteur_id').val();

                    jQuery.post(

                        '<?php echo site_url("admin/Articles/add_redacteur_Article"); ?>',

                        {

                            add_redacteur_name: add_redacteur_name,

                            add_redacteur_id: add_redacteur_id,

                            IdCommercant: <?php echo 301299; ?>

                        },

                        function (zReponse) {

                            if (zReponse != "error") {

                                jQuery("#redacteur_article option[value='" + zReponse + "']").remove();

                                jQuery('#redacteur_article').append(jQuery('<option>', {

                                    value: zReponse,

                                    text: add_redacteur_name

                                }));

                                jQuery("#form_add_redacteur_div").hide('blind');

                                jQuery("#add_redacteur_form_loading").hide();

                                jQuery("#add_redacteur_form_error").hide();

                                jQuery("#add_redacteur_form_locading").css('display', 'none');

                                jQuery('#redacteur_article').val(zReponse);



                                jQuery("#add_redacteur_form_link_add").css('display', 'inline');

                                jQuery("#add_redacteur_form_link_edit").css('display', 'inline');

                                jQuery("#add_redacteur_form_link_delete").css('display', 'inline');

                            } else {

                                jQuery("#add_redacteur_form_error").show();

                                jQuery("#add_redacteur_form_loading").hide();

                                jQuery("#add_redacteur_form_locading").css('display', 'none');

                            }

                        });

                }

            });

            $("#redacteur_article").change(function () {

                jQuery("#add_redacteur_form_locading").css('display', 'inline');

                jQuery.ajax({

                    url: '<?php echo site_url("admin/Articles/check_redacteur_Article"); ?>',

                    type: "post",

                    data: {redacteur_id: jQuery(this).val()},

                    dataType: 'json',

                    success: function (data) {

                        if (data != "error") {

                            jQuery("#add_redacteur_name").val(data.nom);

                            jQuery("#add_redacteur_id").val(data.id);



                            jQuery("#add_redacteur_form_link_add").css('display', 'inline');

                            jQuery("#add_redacteur_form_link_edit").css('display', 'inline');

                            jQuery("#add_redacteur_form_link_delete").css('display', 'inline');

                            jQuery("#add_redacteur_form_error").hide();



                            jQuery("#add_redacteur_form_locading").css('display', 'none');

                        } else {

                            jQuery("#add_redacteur_form_error").show();

                            jQuery("#add_redacteur_form_link_edit").css('display', 'none');

                            jQuery("#add_redacteur_form_link_delete").css('display', 'none');

                            jQuery("#add_redacteur_form_locading").css('display', 'none');

                        }

                    },

                    error: function (data) {

                        jQuery("#add_redacteur_form_error").show();

                        jQuery("#add_redacteur_form_link_edit").css('display', 'none');

                        jQuery("#add_redacteur_form_link_delete").css('display', 'none');

                        jQuery("#add_redacteur_form_locading").css('display', 'none');

                    }

                });

            });

            $("#add_redacteur_form_link_edit").click(function () {

                jQuery("#add_redacteur_form_locading").css('display', 'inline');

                jQuery("#add_redacteur_id").val(jQuery("#redacteur_article").val());

                jQuery.ajax({

                    url: '<?php echo site_url("admin/Articles/check_redacteur_Article"); ?>',

                    type: "post",

                    data: {redacteur_id: jQuery("#redacteur_article").val()},

                    dataType: 'json',

                    success: function (data) {

                        if (data != "error") {

                            jQuery("#add_redacteur_name").val(data.nom);

                            jQuery("#add_redacteur_id").val(data.id);



                            jQuery("#add_redacteur_form_link_add").css('display', 'inline');

                            jQuery("#add_redacteur_form_link_edit").css('display', 'inline');

                            jQuery("#add_redacteur_form_link_delete").css('display', 'inline');



                            jQuery("#add_redacteur_form_locading").css('display', 'none');

                            jQuery("#form_add_redacteur_div").show('blind');

                        } else {

                            jQuery("#add_redacteur_form_error").show();

                            jQuery("#add_redacteur_form_locading").css('display', 'none');

                        }

                    },

                    error: function (data) {

                        jQuery("#add_redacteur_form_error").show();

                        jQuery("#add_redacteur_form_locading").css('display', 'none');

                    }

                });

            });

            $("#add_redacteur_form_link_delete").click(function () {

                if (confirm("Voulez-vous supprimer le redacteur séléctionné ?")) {

                    jQuery("#add_redacteur_form_locading").css('display', 'inline');

                    var redacteur_id = jQuery("#redacteur_article").val();

                    jQuery.post(

                        '<?php echo site_url("admin/Articles/delete_redacteur_Article"); ?>',

                        {

                            redacteur_id: redacteur_id

                        },

                        function (zReponse) {

                            //alert(zReponse);

                            if (zReponse != "error") {

                                jQuery("#redacteur_article option[value='" + redacteur_id + "']").remove();

                                jQuery('#redacteur_article').val("0");



                                jQuery("#add_redacteur_name").val("");

                                jQuery("#add_redacteur_id").val("0");



                                jQuery("#add_redacteur_form_link_add").css('display', 'inline');

                                jQuery("#add_redacteur_form_link_edit").css('display', 'none');

                                jQuery("#add_redacteur_form_link_delete").css('display', 'none');



                                jQuery("#add_redacteur_form_locading").css('display', 'none');

                                jQuery("#form_add_redacteur_div").hide('blind');

                            } else {

                                jQuery("#add_redacteur_form_error").show();

                                jQuery("#add_redacteur_form_locading").css('display', 'none');

                            }

                        });

                }

            });

            //ADD REDACTEUR END////////////////////////////////////////////////////////////////





            //ADD ORGANISER START ***************************************************************

            $("#add_organiser_form_link_add").click(function () {

                jQuery("#add_organiser_form_link_add").css('display', 'none');

                jQuery("#add_organiser_form_link_edit").css('display', 'none');

                jQuery("#add_organiser_form_link_save").css('display', 'inline');

                jQuery("#add_organiser_form_link_cancel").css('display', 'inline');

                jQuery("#add_organiser_form_link_delete").css('display', 'none');

                jQuery("#div_add_organiser_content").css('display', 'block');



                jQuery('#Article_id_organisateur').val("0");

                jQuery('#organisateur').val("");

                jQuery('#adresse1').val("");

                jQuery('#adresse2').val("");

                jQuery('#codepostal').val("");

                jQuery('#DepartementArticle').val("0");

                jQuery('#IdVille').val("0");

                jQuery('#telephone').val("");

                jQuery('#mobile').val("");

                jQuery('#fax').val("");

                jQuery('#email').val("");

                jQuery('#siteweb').val("");

                jQuery('#facebook').val("");

                jQuery('#twitter').val("");

                jQuery('#googleplus').val("");

            });



            $("#add_organiser_form_link_cancel").click(function () {

                jQuery("#add_organiser_form_link_add").css('display', 'inline');

                <?php if (isset($objArticle->organiser_id) && $objArticle->organiser_id != null && $objArticle->organiser_id != "0") { ?>

                jQuery("#add_organiser_form_link_edit").css('display', 'inline');

                <?php } else { ?>

                jQuery("#add_organiser_form_link_edit").css('display', 'none');

                <?php } ?>

                jQuery("#add_organiser_form_link_save").css('display', 'none');

                jQuery("#add_organiser_form_link_cancel").css('display', 'none');

                <?php if (isset($objArticle->organiser_id) && $objArticle->organiser_id != null && $objArticle->organiser_id != "0") { ?>

                jQuery("#add_organiser_form_link_delete").css('display', 'inline');

                <?php } else { ?>

                jQuery("#add_organiser_form_link_delete").css('display', 'none');

                <?php } ?>

                jQuery("#div_add_organiser_content").css('display', 'none');

                jQuery("#add_organiser_form_error").css('display', 'none');

                jQuery("#add_organiser_form_locading").css('display', 'none');

            });



            $("#add_organiser_form_link_save").click(function () {

                jQuery("#add_organiser_form_locading").css('display', 'inline');

                if (jQuery('#organisateur').val() == "" || jQuery('#organisateur').val() == null) {

                    jQuery("#add_organiser_form_error").show();

                    jQuery("#add_organiser_form_locading").css('display', 'none');

                } else {

                    jQuery.post(

                        '<?php echo site_url("admin/Articles/add_organiser_Article"); ?>',

                        {

                            IdCommercant: <?php echo 301299; ?>,

                            Article_id_organisateur: jQuery('#Article_id_organisateur').val(),

                            organisateur: jQuery('#organisateur').val(),

                            adresse1: jQuery('#adresse1').val(),

                            adresse2: jQuery('#adresse2').val(),

                            codepostal: jQuery('#codepostal').val(),

                            DepartementArticle: jQuery('#DepartementArticle').val(),

                            IdVille: jQuery('#IdVille').val(),

                            telephone: jQuery('#telephone').val(),

                            mobile: jQuery('#mobile').val(),

                            fax: jQuery('#fax').val(),

                            email: jQuery('#email').val(),

                            siteweb: jQuery('#siteweb').val(),

                            facebook: jQuery('#facebook').val(),

                            twitter: jQuery('#twitter').val(),

                            googleplus: jQuery('#googleplus').val()

                        },

                        function (zReponse) {

                            //alert(zReponse);

                            if (zReponse != "error") {

                                jQuery("#organiser_article option[value='" + zReponse + "']").remove();

                                jQuery('#organiser_article').append(jQuery('<option>', {

                                    value: zReponse,

                                    text: jQuery('#organisateur').val()

                                }));

                                jQuery("#add_organiser_form_locading").css('display', 'none');

                                jQuery("#add_organiser_form_error").css('display', 'none');

                                jQuery('#organiser_article').val(zReponse);

                                jQuery("#add_organiser_form_link_add").css('display', 'inline');

                                jQuery("#add_organiser_form_link_edit").css('display', 'inline');

                                jQuery("#add_organiser_form_link_save").css('display', 'none');

                                jQuery("#add_organiser_form_link_cancel").css('display', 'none');

                                jQuery("#add_organiser_form_link_delete").css('display', 'inline');

                                jQuery("#div_add_organiser_content").css('display', 'none');

                            } else {

                                jQuery("#add_organiser_form_error").show();

                            }

                        });

                }



            });



            $("#organiser_article").change(function () {

                jQuery("#add_organiser_form_locading").css('display', 'inline');

                jQuery.ajax({

                    url: '<?php echo site_url("admin/Articles/check_organiser_Article"); ?>',

                    type: "post",

                    data: {organiser_id: jQuery(this).val()},

                    dataType: 'json',

                    success: function (data) {

                        if (data != "error") {

                            jQuery("#organisateur").val(data.name);

                            jQuery("#adresse1").val(data.address1);

                            jQuery("#adresse2").val(data.address2);

                            jQuery("#codepostal").val(data.postal_code);

                            jQuery("#adresse_localisation").val(data.department_id);

                            jQuery("#codepostal_localisation").val(data.ville_id);

                            jQuery("#telephone").val(data.tel);

                            jQuery("#mobile").val(data.mobile);

                            jQuery("#fax").val(data.fax);

                            jQuery("#email").val(data.email);

                            jQuery("#siteweb").val(data.website);

                            jQuery("#facebook").val(data.facebook);

                            jQuery("#twitter").val(data.twitter);

                            jQuery("#googleplus").val(data.googleplus);

                            jQuery("#add_organiser_form_locading").css('display', 'none');

                            jQuery("#add_organiser_form_error").css('display', 'none');

                            jQuery("#codepostal").focus();

                            jQuery("#codepostal").blur();



                            jQuery("#add_organiser_form_link_add").css('display', 'inline');

                            jQuery("#add_organiser_form_link_edit").css('display', 'inline');

                            jQuery("#add_organiser_form_link_save").css('display', 'none');

                            jQuery("#add_organiser_form_link_cancel").css('display', 'none');

                            jQuery("#add_organiser_form_link_delete").css('display', 'inline');

                        } else {

                            jQuery("#add_organiser_form_error").show();

                            jQuery("#add_organiser_form_locading").css('display', 'none');

                        }

                    },

                    error: function (data) {

                        jQuery("#add_organiser_form_error").show();

                        jQuery("#add_organiser_form_locading").css('display', 'none');

                    }

                });

            });

            $("#add_organiser_form_link_edit").click(function () {

                jQuery("#add_organiser_form_locading").css('display', 'inline');

                jQuery("#Article_id_organisateur").val(jQuery("#organiser_article").val());

                jQuery.ajax({

                    url: '<?php echo site_url("admin/Articles/check_organiser_Article"); ?>',

                    type: "post",

                    data: {organiser_id: jQuery("#organiser_article").val()},

                    dataType: 'json',

                    success: function (data) {

                        if (data != "error") {

                            jQuery("#organisateur").val(data.name);

                            jQuery("#adresse1").val(data.address1);

                            jQuery("#adresse2").val(data.address2);

                            jQuery("#codepostal").val(data.postal_code);

                            jQuery("#adresse_localisation").val(data.department_id);

                            jQuery("#codepostal_localisation").val(data.ville_id);

                            jQuery("#telephone").val(data.tel);

                            jQuery("#mobile").val(data.mobile);

                            jQuery("#fax").val(data.fax);

                            jQuery("#email").val(data.email);

                            jQuery("#siteweb").val(data.website);

                            jQuery("#facebook").val(data.facebook);

                            jQuery("#twitter").val(data.twitter);

                            jQuery("#googleplus").val(data.googleplus);

                            jQuery("#add_organiser_form_locading").css('display', 'none');

                            jQuery("#add_organiser_form_error").css('display', 'none');

                            jQuery("#div_add_organiser_content").css('display', 'block');

                            jQuery("#codepostal").focus();

                            jQuery("#codepostal").blur();

                            jQuery("#add_organiser_form_link_add").css('display', 'none');

                            jQuery("#add_organiser_form_link_edit").css('display', 'none');

                            jQuery("#add_organiser_form_link_save").css('display', 'inline');

                            jQuery("#add_organiser_form_link_cancel").css('display', 'inline');

                            jQuery("#add_organiser_form_link_delete").css('display', 'none');

                        } else {

                            jQuery("#add_organiser_form_error").show();

                            jQuery("#add_organiser_form_locading").css('display', 'none');

                        }

                    },

                    error: function (data) {

                        jQuery("#add_organiser_form_error").show();

                        jQuery("#add_organiser_form_locading").css('display', 'none');

                    }

                });

            });

            $("#add_organiser_form_link_delete").click(function () {

                if (confirm("Voulez-vous supprimer l'organisateur séléctionné ?")) {

                    jQuery("#add_organiser_form_locading").css('display', 'inline');

                    var organiser_id = jQuery("#organiser_article").val();

                    jQuery.post(

                        '<?php echo site_url("admin/Articles/delete_organiser_Article"); ?>',

                        {

                            organiser_id: organiser_id

                        },

                        function (zReponse) {

                            //alert(zReponse);

                            if (zReponse != "error") {

                                jQuery("#organiser_article option[value='" + organiser_id + "']").remove();

                                jQuery('#organiser_article').val("0");

                                jQuery("#add_organiser_form_locading").css('display', 'none');

                                jQuery("#add_organiser_form_error").css('display', 'none');

                                jQuery("#add_organiser_form_link_add").css('display', 'inline');

                                jQuery("#add_organiser_form_link_edit").css('display', 'none');

                                jQuery("#add_organiser_form_link_save").css('display', 'none');

                                jQuery("#add_organiser_form_link_cancel").css('display', 'none');

                                jQuery("#add_organiser_form_link_delete").css('display', 'none');

                                jQuery("#div_add_organiser_content").css('display', 'none');



                                jQuery('#Article_id_organisateur').val("0");

                                jQuery('#organisateur').val("");

                                jQuery('#adresse1').val("");

                                jQuery('#adresse2').val("");

                                jQuery('#codepostal').val("");

                                jQuery('#DepartementArticle').val("0");

                                jQuery('#IdVille').val("0");

                                jQuery('#telephone').val("");

                                jQuery('#mobile').val("");

                                jQuery('#fax').val("");

                                jQuery('#email').val("");

                                jQuery('#siteweb').val("");

                                jQuery('#facebook').val("");

                                jQuery('#twitter').val("");

                                jQuery('#googleplus').val("");

                            } else {

                                jQuery("#add_organiser_form_error").show();

                                jQuery("#add_organiser_form_locading").css('display', 'none');

                            }

                        });

                }

            });

            //ADD ORGANISER END ***************************************************************





            //	ADD EMAIL VALIDATION ARTICLE START

            $("#add_validation_article_form_link_open").click(function () {

                jQuery("#add_validation_article_form_link_add").css('display', 'none');

                jQuery("#add_validation_article_form_link_edit").css('display', 'none');

                jQuery("#add_validation_article_form_link_delete").css('display', 'none');



                jQuery('#add_validation_article_name').val("");

                jQuery('#add_validation_article_id').val("0");

                jQuery("#form_add_validation_article_div").show('blind');

            });



            $("#add_validation_article_form_cancel").click(function () {

                jQuery("#form_add_validation_article_div").hide('blind');

            });



            $("#add_validation_article_form_save").click(function () {



                jQuery("#add_validation_article_form_loading").show();



                var add_validation_article_name = jQuery("#add_validation_article_name").val();

                var add_validation_article_email = jQuery("#add_validation_article_email").val();

                if (add_validation_article_name == "" || add_validation_article_name == null || !isEmail(add_validation_article_email) || add_validation_article_email == "" || add_validation_article_email == null) {

                    jQuery("#add_validation_article_name").css('border-color', '#FF0000');

                    jQuery("#add_validation_article_email").css('border-color', '#FF0000');

                    jQuery("#add_validation_article_form_loading").hide();

                } else {



                    var add_validation_article_name = jQuery('#add_validation_article_name').val();

                    var add_validation_article_email = jQuery('#add_validation_article_email').val();


                    jQuery.post(

                        '<?php echo site_url("admin/Articles/add_validation_Article_function"); ?>',

                        {

                            add_validation_article_name: add_validation_article_name,

                            add_validation_article_email: add_validation_article_email

                        },

                        function (zReponse) {

                            if (zReponse != "error") {

                                jQuery('#destinataire_validation_article').append(jQuery('<option>', {

                                    value: zReponse,

                                    text: add_validation_article_name + " / " + add_validation_article_email

                                }));

                                jQuery("#add_validation_article_form_loading").hide();

                                jQuery("#add_validation_article_form_error").hide();

                                jQuery("#form_add_validation_article_div").hide('blind');

                            } else {

                                jQuery("#add_validation_article_form_error").show();

                                jQuery("#add_validation_article_form_loading").hide();

                            }

                        });



                }



            });

            //	ADD EMAIL VALIDATION ARTICLE END





            //	SEND VALIDATION ARTICLE START

            $("#link_envoyer_validation_article").click(function () {



                jQuery("#envoyer_validation_article_form_loading").show();



                var destinataire_validation_article = jQuery("#destinataire_validation_article").val();

                if (destinataire_validation_article == "" || destinataire_validation_article == '0' || destinataire_validation_article == null) {

                    jQuery("#destinataire_validation_article").css('border-color', '#FF0000');

                    jQuery("#envoyer_validation_article_form_loading").hide();

                } else {



                    var validation_nom_manifestation = jQuery('#nom_manifestation').val();

                    //var validation_description = CKEDITOR.instances['description'].getData();

                    var article_id = "<?php if(isset($objArticle->id)) echo $objArticle->id;?>";

                    var article_date_depot = "<?php if(isset($objArticle->date_depot)) echo convert_Sqldate_to_Frenchdate($objArticle->date_depot);?>";

                    var destinataire_validation_article = jQuery("#destinataire_validation_article").val();

                    //alert(validation_description);



                    jQuery.post(

                        '<?php echo site_url("admin/Articles/envoyer_validation_Article_function"); ?>',

                        {

                            validation_nom_manifestation: validation_nom_manifestation,

                            //validation_description: validation_description,

                            article_id: article_id,

                            article_date_depot: article_date_depot,

                            destinataire_validation_article: destinataire_validation_article

                        },

                        function (zReponse) {

                            //alert(zReponse);

                            if (zReponse != "error") {

                                jQuery("#envoyer_validation_article_form_success").show();

                                jQuery("#envoyer_validation_article_form_error").hide();

                                jQuery("#envoyer_validation_article_form_loading").hide();

                            } else {

                                jQuery("#envoyer_validation_article_form_error").show();

                                jQuery("#envoyer_validation_article_form_success").hide();

                                jQuery("#envoyer_validation_article_form_loading").hide();

                            }

                        });



                }



            });

            //	SEND VALIDATION ARTICLE END





            $("#admin_article_activation_1").click(function () {

                if ($('#admin_article_activation_1').attr('checked')) {

                    $("#IsActif").val("1");

                }

            });

            $("#admin_article_activation_0").click(function () {

                if ($('#admin_article_activation_0').attr('checked')) {

                    $("#IsActif").val("0");

                }

            });

            $("#admin_article_activation_2").click(function () {

                if ($('#admin_article_activation_2').attr('checked')) {

                    $("#IsActif").val("2");

                }

            });

            $("#admin_article_activation_3").click(function () {

                if ($('#admin_article_activation_3').attr('checked')) {

                    $("#IsActif").val("3");

                }

            });





            jQuery('#location_article').change(function () {

                jQuery("#select_location_form_loading").css("display", "block");

                var location_id = jQuery(this).val();

                if (location_id != "0") {

                    jQuery.ajax({

                        url: '<?php echo site_url("admin/Articles/get_location_Article"); ?>',

                        type: "post",

                        data: {location_id: location_id},

                        dataType: 'json',

                        success: function (data) {

                            if (data != "error") {

                                jQuery("#adresse_localisation").val(data.location_address);

                                jQuery("#codepostal_localisation").val(data.location_postcode);

                                jQuery("#DepartementAgendaLocalisation").val(data.location_departementid);

                                jQuery("#IdVille_localisation").val(data.location_villeid);

                                jQuery('#codepostal_localisation').focus();

                                jQuery('#codepostal_localisation').blur();



                                jQuery("#add_location_id").val(data.location_id);

                                jQuery("#add_location_name").val(data.location);

                                jQuery("#add_location_adress").val(data.location_address);

                                jQuery("#add_location_codepostal").val(data.location_postcode);

                                jQuery("#add_location_departementid").val(data.location_departementid);

                                jQuery("#add_location_villeId").val(data.location_villeid);

                                jQuery('#add_location_codepostal').focus();

                                jQuery('#add_location_codepostal').blur();



                                jQuery.post(

                                    '<?php echo site_url("admin/Articles/get_ville_localisation"); ?>',

                                    {IdVille: data.location_villeid},

                                    function (zReponse) {

                                        jQuery("#IdVille_Nom_text_localisation").val(zReponse);

                                        jQuery("#select_location_form_loading").css("display", "none");

                                    });



                            } else {

                                jQuery("#select_location_form_loading").css("display", "none");

                            }

                        },

                        error: function (data) {

                            jQuery("#select_location_form_loading").css("display", "none");

                        }

                    });

                    jQuery("#add_location_form_link_open").css("display", "none");

                    jQuery("#edit_location_form_link_open").css("display", "inline-block");

                    jQuery("#delete_location_form_link_open").css("display", "inline-block");

                } else {

                    jQuery("#adresse_localisation").val("");

                    jQuery("#codepostal_localisation").val("");

                    jQuery("#DepartementAgendaLocalisation").val("0");

                    jQuery("#IdVille_localisation").val("0");

                    jQuery("#IdVille_Nom_text_localisation").val("");

                    jQuery("#select_location_form_loading").css("display", "none");



                    jQuery("#add_location_id").val("");

                    jQuery("#add_location_name").val("");

                    jQuery("#add_location_adress").val("");

                    jQuery("#add_location_codepostal").val("");

                    jQuery("#add_location_departementid").val("0");

                    jQuery("#add_location_villeId").val("0");



                    jQuery("#add_location_form_link_open").css("display", "inline-block");

                    jQuery("#edit_location_form_link_open").css("display", "none");

                    jQuery("#delete_location_form_link_open").css("display", "none");

                }

            });





            //radio buttton diffuseur_organisateur START

            $("#diffuseur_organisateur_1").click(function () {

                if ($('#diffuseur_organisateur_1').attr('checked')) {

                    $("#diffuseur_organisateur").val("1");

                    $("#img_loaging_agenda").show();

                    $.post(

                        '<?php echo site_url(); ?>article/check_commercant/',

                        {

                            IdCommercant: <?php echo 301299; ?>

                        },

                        function (zReponse) {

                            //alert ("reponse " + zReponse) ;

                            if (zReponse) {

                                var article_value = zReponse.split("==!!!==");

                                for (i = 0; i < article_value.length; i++) {

                                    var article_value_id = article_value[i].split("!!!==>");

                                    if (article_value_id[0] == "NomSociete") $("#organisateur").val(article_value_id[1]);

                                    if (article_value_id[0] == "Adresse1") $("#adresse1").val(article_value_id[1]);

                                    if (article_value_id[0] == "Adresse2") $("#adresse2").val(article_value_id[1]);

                                    if (article_value_id[0] == "IdVille") $("#IdVille").val(article_value_id[1]);

                                    if (article_value_id[0] == "CodePostal") $("#codepostal").val(article_value_id[1]);

                                    if (article_value_id[0] == "TelFixe") $("#telephone").val(article_value_id[1]);

                                    if (article_value_id[0] == "TelMobile") $("#mobile").val(article_value_id[1]);

                                    if (article_value_id[0] == "fax") $("#fax").val(article_value_id[1]);

                                    if (article_value_id[0] == "Email") $("#email").val(article_value_id[1]);

                                    if (article_value_id[0] == "SiteWeb") $("#siteweb").val(article_value_id[1]);

                                    if (article_value_id[0] == "Facebook") $("#facebook").val(article_value_id[1]);

                                    if (article_value_id[0] == "Twitter") $("#googleplus").val(article_value_id[1]);

                                    if (article_value_id[0] == "google_plus") $("#twitter").val(article_value_id[1]);

                                }

                                $("#img_loaging_agenda").hide();

                            }

                        });

                } else {

                    $("#diffuseur_organisateur").val("0");

                }

            });

            $("#diffuseur_organisateur_0").click(function () {

                if ($('#diffuseur_organisateur_0').attr('checked')) {

                    $("#diffuseur_organisateur").val("0");

                    $("#organisateur").val("");

                    $("#adresse1").val("");

                    $("#adresse2").val("");

                    $("#IdVille").val("0");

                    $("#codepostal").val("");

                    $("#telephone").val("");

                    $("#mobile").val("");

                    $("#fax").val("");

                    $("#email").val("");

                    $("#siteweb").val("http://www.");

                    $("#facebook").val("");

                    $("#googleplus").val("");

                    $("#twitter").val("");



                } else {

                    $("#diffuseur_organisateur").val("1");

                }

            });

            //radio buttton diffuseur_organisateur END





            //add_location_codepostal

            $("#add_location_codepostal").keyup(function () {

                var result = $(this).val();

                $("#codepostal_localisation").val(result);

            }).keydown(function () {

                var result = $(this).val();

                $("#codepostal_localisation").val(result);

            });





        });



        $(function () {

            $(".date_debut").datepicker({

                dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],

                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],

                monthNames: ['Janvier', 'Févier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],

                dateFormat: 'DD, d MM yy',

                autoSize: true,

                changeMonth: true,

                changeYear: true,

                yearRange: '1900:2020'

            });

            $(".date_fin").datepicker({

                dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],

                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],

                monthNames: ['Janvier', 'Févier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],

                dateFormat: 'DD, d MM yy',

                autoSize: true,

                changeMonth: true,

                changeYear: true,

                yearRange: '1900:2020'

            });

            $("#date_depot").datepicker({

                dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],

                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],

                monthNames: ['Janvier', 'Févier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],

                dateFormat: 'DD, d MM yy',

                autoSize: true,

                changeMonth: true,

                changeYear: true,

                yearRange: '1900:2020'

            });

        });



        function alert_save_agenda_before_view() {

            $("#id_save_agenda_before_view").html("Veuillez enregistrer l'article avant de visualiser !&nbsp;&nbsp;&nbsp;");

        }





        function CP_getDepartement() {

            jQuery('#departementCP_container').html('<img src="<?php echo GetImagePath("admin/");?>/loading.gif" />');

            var CodePostalSociete = jQuery('#codepostal').val();

            jQuery.post(

                '<?php echo site_url("admin/Articles/departementcp_Article"); ?>',

                {CodePostalSociete: CodePostalSociete},

                function (zReponse) {

                    jQuery('#departementCP_container').html(zReponse);

                });

        }





        function CP_getDepartement_localisation() {

            jQuery('#departementCP_container_localisation').html('<img src="<?php echo GetImagePath("admin/");?>/loading.gif" />');

            var CodePostalSociete = jQuery('#codepostal_localisation').val();

            jQuery.post(

                '<?php echo site_url("admin/Articles/departementcp_Article_localisation"); ?>',

                {CodePostalSociete: CodePostalSociete},

                function (zReponse) {

                    jQuery('#departementCP_container_localisation').html(zReponse);

                });

        }



        function CP_getDepartement_add_location() {

            jQuery('#departementCP_container_add_location').html('<img src="<?php echo GetImagePath("admin/");?>/loading.gif" />');

            var CodePostalSociete = jQuery('#add_location_codepostal').val();

            jQuery.post(

                '<?php echo site_url("admin/Articles/departementcp_Article_add_location"); ?>',

                {CodePostalSociete: CodePostalSociete},

                function (zReponse) {

                    jQuery('#departementCP_container_add_location').html(zReponse);

                });

        }



        function CP_getVille() {

            jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("admin/");?>/loading.gif" />');

            var CodePostalSociete = jQuery('#codepostal').val();

            jQuery.post(

                '<?php echo site_url("admin/Articles/villecp_Article"); ?>',

                {CodePostalSociete: CodePostalSociete},

                function (zReponse) {

                    jQuery('#villeCP_container').html(zReponse);

                });

        }



        function CP_getVille_localisation() {

            jQuery('#villeCP_container_localisation').html('<img src="<?php echo GetImagePath("admin/");?>/loading.gif" />');

            var CodePostalSociete = jQuery('#codepostal_localisation').val();

            jQuery.post(

                '<?php echo site_url("admin/Articles/villecp_article_localisation"); ?>',

                {CodePostalSociete: CodePostalSociete},

                function (zReponse) {

                    jQuery('#villeCP_container_localisation').html(zReponse);

                });

        }



        function CP_getVille_add_location() {

            jQuery('#villeCP_container_add_location').html('<img src="<?php echo GetImagePath("admin/");?>/loading.gif" />');

            var CodePostalSociete = jQuery('#add_location_codepostal').val();

            jQuery.post(

                '<?php echo site_url("admin/Articles/villecp_article_add_location"); ?>',

                {CodePostalSociete: CodePostalSociete},

                function (zReponse) {

                    jQuery('#villeCP_container_add_location').html(zReponse);

                });

        }





        function CP_getVille_D_CP() {

            jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("admin/");?>/loading.gif" />');

            var CodePostalSociete = jQuery('#codepostal').val();

            var departement_id = jQuery('#DepartementAgenda').val();

            jQuery.post(

                '<?php echo site_url("admin/Articles/villecp_Article"); ?>',

                {CodePostalSociete: CodePostalSociete, departement_id: departement_id},

                function (zReponse) {

                    jQuery('#villeCP_container').html(zReponse);

                });

        }



        function CP_getVille_D_CP_localisation() {

            jQuery('#villeCP_container_localisation').html('<img src="<?php echo GetImagePath("admin/");?>/loading.gif" />');

            var CodePostalSociete = jQuery('#codepostal_localisation').val();

            var departement_id = jQuery('#DepartementAgendaLocalisation').val();

            jQuery.post(

                '<?php echo site_url("admin/Articles/villecp_article_localisation"); ?>',

                {CodePostalSociete: CodePostalSociete, departement_id: departement_id},

                function (zReponse) {

                    jQuery('#villeCP_container_localisation').html(zReponse);

                });

        }





    </script>



    <style type="text/css">

        .stl_long_input_platinum {

            width: 413px;

        }



        .stl_long_input_platinum_td {

            width: 180px;

            height: 30px;

        }



        .div_stl_long_platinum {

            color: #ffffff;

            font-family: "Arial", sans-serif;

            font-size: 13px;

            font-weight: 700;

            font-weight: bold;

            line-height: 1.23em;

            padding-top: 10px;

            padding-bottom: 10px;

            margin: 15px 0;

            background-color: #0719DC;

        }



        .div_error_taille_3_4 {

            color: #F00;

        }



        .FieldError {

            color: #FF0000;

            font-weight: bold;

        }



        .result_fiche_article.alert {

            display: table;

            text-align: center;

            width: 100%;

        }



        .admin_bo_menu {

            display: inline-table;

            height: 50px;

        }



        .btn_adminbp, .btn_adminbp:hover, .btn_adminbp:focus {

            background-color: #0719DC;

            padding: 10px;

            color: #ffffff;

            text-decoration: none;

            font-weight: bold;

            display: table;

            text-align: center;

            width: 100%;

        }



        .greybg {

            background-color: #E5E5E5;

            padding: 15px;

            display: table;

            width: 100%;

        }

    </style>





    <div class="col-xs-12 admin_bo_menu textaligncenter">

        <h1 style="margin: 20px !important;">Mon Articles</h1>

    </div>



    <div class="col-xs-12 padding0">

        <div class="admin_bo_menu col-sm-4 paddingleft0"><a

                    href="<?php echo site_url('admin/utilisateur/contenupro/'); ?>"

                    class="btn_adminbp">Retour Menu</a></div>

        <div class="admin_bo_menu col-sm-4 padding0" style="min-height: 50px;"><a

                    href="<?php echo site_url("admin/articles/liste/" . 301299); ?>"

                    class="btn_adminbp">Retour aux Articles</a></div>

        <div class="admin_bo_menu col-sm-4 paddingright0" style="min-height: 50px;"><a

                    href="<?php echo site_url("admin/articles/fiche/" . 301299); ?>"


                    class="btn_adminbp">Ajouter un nouvel Articles</a></div>

    </div>





<?php if (isset($mssg) && $mssg == 1) { ?>

    <div class="result_fiche_article alert alert-success">Les modifications ont &eacute;t&eacute; enregistr&eacute;es

    </div><br/>

<?php } ?>





<?php if (isset($mssg) && $mssg != 1 && $mssg != "") { ?>

    <div class="result_fiche_article alert alert-danger">Une erreur est constatée, Veuillez vérifier la conformité de

        vos données

    </div><br/>

<?php } ?>



    <div class="col-xs-12" style="padding:0 !important;">



        <form name="frmInscriptionProfessionnel" id="frmInscriptionProfessionnel"

              action="<?php echo site_url("admin/Articles/save"); ?>" method="POST" enctype="multipart/form-data">



            <input type="hidden" name="Article[id]" id="idArticle"

                   value="<?php if (isset($objArticle->id)) echo htmlspecialchars($objArticle->id); else echo "0"; ?>">



            <div class="div_stl_long_platinum col-xs-12">Publication du Articles</div>

            <div class="col-xs-12">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Vous êtes : </label>

                        </td>

                        <td>

                            <?php

                            if (isset($objCommercant->IdVille_localisation) && $objCommercant->IdVille_localisation != "" && $objCommercant->IdVille_localisation != NULL) {

                                $this->load->Model("mdlville");

                                $obj_ville_commercant = $this->mdlville->getVilleById($objCommercant->IdVille_localisation);

                                if (isset($obj_ville_commercant) && count($obj_ville_commercant) != 0) $ville_name_to_show = $obj_ville_commercant->Nom; else $ville_name_to_show = "";

                            } else {

                                $ville_name_to_show = "";

                            }

                            ?>

                            <input type="text" name="Article[nom_societe]" id="nom_societe"

                                   value="<?php if (isset($objArticle->nom_societe) && $objArticle->nom_societe != "") echo htmlspecialchars($objArticle->nom_societe); else echo isset($objCommercant->NomSociete) . " " . isset($objCommercant->codepostal_localisation) . " " . $ville_name_to_show; ?>"

                                   class="form-control"/>

                        </td>

                    </tr>


                    <tr id="SpanCategoryTypeArticle">

                        <td>

                            <textarea rows="2" cols="20" onkeyup="limite_title(this,'100');"

                                      onkeydown="limite_title(this,'100');" name="Article[nom_manifestation]"

                                      id="nom_manifestation"

                                      class="input_width form-control"><?php if (isset($objArticle->nom_manifestation) && $objArticle->nom_manifestation != "") echo htmlspecialchars($objArticle->nom_manifestation); ?></textarea><br/><span

                                    id="max_desc_title"></span>

                        </td>

                    </tr>

                </table>

            </div>





            <div class="col-xs-12" style="margin-top:15px;">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                    <tr>

                        <td class="stl_long_input_platinum_td"><label class="label"> Rédacteur : </label></td>

                        <td>

                            <div class="col-xs-6" style="padding:0 !important;"><select id="redacteur_article"

                                                                                        name="Article[redacteur_id]"

                                                                                        class="form-control">

                                    <option value="0">Ajouter un rédacteur</option>

                                    <?php if (sizeof($col_Article_redacteur)) { ?>

                                        <?php foreach ($col_Article_redacteur as $objcol_Article_redacteur_xx) { ?>

                                            <option <?php if (isset($objArticle->redacteur_id) && $objArticle->redacteur_id == $objcol_Article_redacteur_xx->id) echo 'selected="selected"'; ?>

                                                    value="<?php echo $objcol_Article_redacteur_xx->id; ?>"><?php echo $objcol_Article_redacteur_xx->nom; ?></option>

                                        <?php } ?>

                                    <?php } ?>

                                </select></div>

                            <div class="col-xs-6">

                                <a href="javascript:void(0);" class="btn btn-primary"

                                   title="Ajouter un Redacteur"

                                   id="add_redacteur_form_link_open">+</a>

                                <a href="javascript:void(0);" class="btn btn-primary"

                                   id="add_redacteur_form_link_edit" title="Modifier le redacteur"

                                   style="<?php if (isset($objArticle->redacteur_id) && $objArticle->redacteur_id != null && $objArticle->redacteur_id != "0") { ?>display: inline;<?php } else { ?>display: none;<?php } ?>">

                                    <img

                                            style="border: none;width:15px;"

                                            src="<?php echo base_url(); ?>application/resources/privicarte/images/update_ico.png"></a>

                                <a href="javascript:void(0);" class="btn btn-danger"

                                   id="add_redacteur_form_link_delete" title="Supprimer le redacteur"

                                   style="<?php if (isset($objArticle->redacteur_id) && $objArticle->redacteur_id != null && $objArticle->redacteur_id != "0") { ?>display: inline;<?php } else { ?>display: none;<?php } ?>">

                                    <img

                                            style="border: none;width:15px;"

                                            src="<?php echo base_url(); ?>application/resources/privicarte/images/delete_ico.png"></a>

                                <img id="add_redacteur_form_locading"

                                     src="<?php echo GetImagePath("admin/"); ?>/loading.gif"

                                     style="float: right; margin-top: 10px; display: none;">

                            </div>

                        </td>


                    </tr>

                    <tr id="SpanSousCategoryTypeArticle">

                        <td>

                            <div class="col-xs-6" style="padding:0 !important;">

                                <input type="text"

                                       id="add_redacteur_name"

                                       name="add_redacteur_name"

                                       class="form-control"/>

                                <input type="hidden"

                                       id="add_redacteur_id"

                                       name="add_redacteur_id"

                                       class="form-control" value="0"/>

                            </div>

                            <div class="col-xs-6">

                                <a href="javascript:void(0);" id="add_redacteur_form_cancel" class="btn btn-warning">Annuler</a>

                                <a href="javascript:void(0);" id="add_redacteur_form_save" class="btn btn-success">Enregistrer</a>

                                <img id="add_redacteur_form_loading"

                                     src="<?php echo GetImagePath("admin/"); ?>/loading.gif"

                                     style="float: right; margin-top: 10px"/>

                            </div>

                        </td>
                        <td>
                            <select name="Article[article_subcategid]" id="article_subcategid" onChange="javascript:void(0);" class="input_fichearticle">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($colSousCategorie)) { ?>
                                    <?php foreach($colSousCategorie as $objcolSousCategorie_xx) { ?>
                                        <option <?php if(isset($oArticle->article_subcategid) && $oArticle->article_subcategid == $objcolSousCategorie_xx->article_subcategid) echo 'selected="selected"';?> value="<?php echo $objcolSousCategorie_xx->article_subcategid; ?>"><?php echo $objcolSousCategorie_xx->subcateg; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>

                    </tr>


                    <tr>

                        <td>

                            <label class="label">Les coordonnées de l'organisateur</label>

                        </td>

                        <td>

                            <label class="label">Le diffuseur est-il organisateur ? </label>

                        </td>

                        <td>

                            <input type="radio" value="1" name="diffuseur_organisateur_radio"

                                   id="diffuseur_organisateur_1"

                                   <?php if (isset($objArticle->diffuseur_organisateur) && $objArticle->diffuseur_organisateur == "1") { ?>checked<?php } ?>/>

                            OUI

                            <input type="radio" value="0" name="diffuseur_organisateur_radio"

                                   id="diffuseur_organisateur_0"

                                   <?php if (isset($objArticle->diffuseur_organisateur) && $objArticle->diffuseur_organisateur == "0") { ?>checked<?php } ?>/>

                            NON

                            <input type="hidden" name="Article[diffuseur_organisateur]" id="diffuseur_organisateur"

                                   value="<?php if (isset($objArticle->diffuseur_organisateur)) echo htmlspecialchars($objArticle->diffuseur_organisateur); else echo "0"; ?>">

                            &nbsp;<img src="<?php echo base_url(); ?>application/resources/admin/images/wait.gif"

                                       alt="loading...." id="img_loaging_agenda"/>

                        </td>

                    </tr>

                </table>

            </div>





            <div class="col-xs-12 greybg" id="div_add_organiser">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                    <tr>

                        <td class="stl_long_input_platinum_td"><label class="label"> Choisir l'Organisateur : </label>

                        </td>

                        <td>

                            <div class="col-xs-6" style="padding:0 !important;"><select id="organiser_article"

                                                                                        name="Article[organiser_id]"

                                                                                        class="form-control">

                                    <option value="0">--Choisir--</option>

                                    <?php if (sizeof($col_Article_organiser)) { ?>

                                        <?php foreach ($col_Article_organiser as $objcol_article_organiser_xx) { ?>

                                            <option <?php if (isset($objArticle->organiser_id) && $objArticle->organiser_id == $objcol_article_organiser_xx->id) echo 'selected="selected"'; ?>

                                                    value="<?php echo $objcol_article_organiser_xx->id; ?>"><?php echo $objcol_article_organiser_xx->name; ?></option>

                                        <?php } ?>

                                    <?php } ?>

                                </select></div>

                            <div class="col-xs-6">

                                <a href="javascript:void(0);" class="btn btn-primary"

                                   id="add_organiser_form_link_add" title="Ajouter un Organisateur">+</a>

                                <a href="javascript:void(0);" class="btn btn-primary"

                                   id="add_organiser_form_link_edit" title="Modifier l'Organisateur"

                                   style="<?php if (isset($objArticle->organiser_id) && $objArticle->organiser_id != null && $objArticle->organiser_id != "0") { ?>display: inline;<?php } else { ?>display: none;<?php } ?>">

                                    <img

                                            style="border: none;width:15px;"

                                            src="<?php echo base_url(); ?>application/resources/privicarte/images/update_ico.png"></a>

                                <a href="javascript:void(0);" class="btn btn-success"

                                   id="add_organiser_form_link_save" style="display: none;"

                                   title="Enregistrer l'Organisateur">

                                    <img style="border: none;width:15px;"

                                         src="<?php echo base_url(); ?>application/resources/privicarte/images/activated_ico.gif"></a>

                                <a href="javascript:void(0);" class="btn btn-danger"

                                   id="add_organiser_form_link_cancel" style="display: none;" title="Annuler">X</a>

                                <a href="javascript:void(0);" class="btn btn-danger"

                                   id="add_organiser_form_link_delete" title="Supprimer l'Organisateur"

                                   style="<?php if (isset($objArticle->organiser_id) && $objArticle->organiser_id != null && $objArticle->organiser_id != "0") { ?>display: inline;<?php } else { ?>display: none;<?php } ?>">

                                    <img

                                            style="border: none;width:15px;"

                                            src="<?php echo base_url(); ?>application/resources/privicarte/images/delete_ico.png"></a>

                                <img id="add_organiser_form_locading"

                                     src="<?php echo GetImagePath("admin/"); ?>/loading.gif"

                                     style="float: right; margin-top: 10px; display: none;">

                            </div>

                        </td>

                    </tr>

                </table>

            </div>



            <div class="col-xs-12 alert alert-danger" id="add_organiser_form_error"

                 style="margin: 15px 0; display: none;">Une errreur s'est produite ou ce nom existe d&eacute;j&agrave;!

            </div>



            <div class="col-xs-12 greybg" id="div_add_organiser_content" style="display: none; padding: 15px;">

                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:20px;">



                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Organisateur : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[organisateur]" id="organisateur"

                                   value="<?php if (isset($objArticle->organisateur) && $objArticle->organisateur != "") echo htmlspecialchars($objArticle->organisateur); ?>"

                                   class="stl_long_input_platinum form-control"/>

                            <input type="hidden" name="Article_id_organisateur" id="Article_id_organisateur"

                                   value="0"

                                   class="stl_long_input_platinum form-control"/>

                        </td>

                    </tr>

                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Adresse1 : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[adresse1]" id="adresse1"

                                   value="<?php if (isset($objArticle->adresse1)) echo htmlspecialchars($objArticle->adresse1); ?>"

                                   class="stl_long_input_platinum form-control"/>

                        </td>

                    </tr>

                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Adresse2 : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[adresse2]" id="adresse2"

                                   value="<?php if (isset($objArticle->adresse2)) echo htmlspecialchars($objArticle->adresse2); ?>"

                                   class="stl_long_input_platinum form-control"/>

                        </td>

                    </tr>





                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Code Postal : </label>

                        </td>

                        <td id="trReponseVille">

                            <input type="text" name="Article[codepostal]" id="codepostal"

                                   value="<?php if (isset($objArticle->codepostal)) echo $objArticle->codepostal; ?>"

                                   class="stl_long_input_platinum form-control"

                                   onblur="javascript:CP_getDepartement();CP_getVille();"/>

                        </td>

                    </tr>





                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Departement : </label>

                        </td>

                        <td>

    	<span id="departementCP_container">

        <select name="Article[departement_id]" id="DepartementAgenda" disabled="disabled"

                class="stl_long_input_platinum form-control" onchange="javascript:CP_getVille_D_CP();">

            <option value="0">-- Veuillez choisir --</option>

            <?php if (sizeof($colDepartement)) { ?>

                <?php foreach ($colDepartement as $objDepartement) { ?>

                    <option

                        <?php if (isset($objArticle->departement_id) && $objArticle->departement_id == $objDepartement->departement_id) { ?>selected="selected"<?php } ?>

                        value="<?php echo $objDepartement->departement_id; ?>"><?php echo $objDepartement->departement_nom . " - " . $objDepartement->departement_code; ?></option>

                <?php } ?>

            <?php } ?>

        </select>

        </span>

                        </td>

                    </tr>





                    <tr>

                        <td class="stl_long_input_platinum_td"><label class="label">Ville *</label></td>

                        <td>

    	<span id="villeCP_container">

        <input type="text"

               value="<?php if (isset($objArticle->IdVille)) echo $this->mdlville->getVilleById($objArticle->IdVille)->Nom; ?>"

               name="IdVille_Nom_text" id="IdVille_Nom_text" disabled="disabled"

               class="stl_long_input_platinum form-control"/>

        <input type="hidden" value="<?php if (isset($objArticle->IdVille)) echo $objArticle->IdVille; else echo "0"; ?>"

               name="Article[IdVille]" id="IdVille"/>

        </span>

                        </td>

                    </tr>





                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">T&eacute;l&eacute;phone : </label>

                        </td>

                        <td>
                        	<?php if(isset($oArticle->audio)) echo $oArticle->audio; else {?>
                            <input type="file" name="ArticleAudio" id="ArticleAudio"/>
                            <?php } ?>


                            <!--<object type="application/x-shockwave-flash" data="'.$container_payer_audio_path.'dewplayer.swf?mp3='.$music_to_load.'&autoplay=false" width="200" height="20">
                            <param name="movie" value="'.$container_payer_audio_path.'dewplayer.swf?mp3='.$music_to_load.'&autoplay=true" />
                            </object>
-->
                            <input type="hidden" name="ArticleAudioAssocie" id="ArticleAudioAssocie" value="<?php if(isset($oArticle->audio)) echo $oArticle->audio; ?>" />
                        </td>

                    </tr>

                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Mobile : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[mobile]" id="mobile"

                                   value="<?php if (isset($objArticle->mobile)) echo htmlspecialchars($objArticle->mobile); ?>"

                                   class="stl_long_input_platinum form-control"/>

                        </td>

                    </tr>

                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Fax : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[fax]" id="fax"

                                   value="<?php if (isset($objArticle->fax)) echo htmlspecialchars($objArticle->fax); ?>"

                                   class="stl_long_input_platinum form-control"/>

                        </td>

                    </tr>

                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Courriel : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[email]" id="email"

                                   value="<?php if (isset($objArticle->email)) echo htmlspecialchars($objArticle->email); ?>"

                                   class="stl_long_input_platinum form-control"/>

                            <div class="FieldError" id="divErrorEmailSociete"></div>

                        </td>

                    </tr>

                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Site internet : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[siteweb]" id="siteweb"

                                   value="<?php if (isset($objArticle->siteweb) && $objArticle->siteweb != "") echo htmlspecialchars($objArticle->siteweb); else echo 'http://www.'; ?>"

                                   class="stl_long_input_platinum form-control"/>

                        </td>

                    </tr>

                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Lien Facebook : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[facebook]" id="facebook"

                                   value="<?php if (isset($objArticle->facebook)) echo htmlspecialchars($objArticle->facebook); ?>"

                                   class="stl_long_input_platinum form-control"/>

                        </td>

                    </tr>

                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Lien Twitter : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[twitter]" id="twitter"

                                   value="<?php if (isset($objArticle->twitter)) echo htmlspecialchars($objArticle->twitter); ?>"

                                   class="stl_long_input_platinum form-control"/>

                        </td>

                    </tr>

                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Lien Google + : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[googleplus]" id="googleplus"

                                   value="<?php if (isset($objArticle->googleplus)) echo htmlspecialchars($objArticle->googleplus); ?>"

                                   class="stl_long_input_platinum form-control"/>

                        </td>

                    </tr>

                </table>

            </div>





            <div class="div_stl_long_platinum col-xs-12" style="margin-bottom: 0 !important; margin-top: 0 !important;">

                Détails du Articles

            </div>

            <div class="col-xs-12 greybg">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Date de dépôt : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[date_depot]" id="date_depot"

                                   value="<?php if (isset($objArticle->date_depot)) echo convert_Sqldate_to_Frenchdate($objArticle->date_depot); else echo convert_Sqldate_to_Frenchdate(date("Y-m-d")); ?>"

                                   class="form-control"/>

                        </td>

                    </tr>

                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Catégorie : </label>

                        </td>

                        <td>

                            <select name="Article[article_categid]" onchange="javascript:listeSousRubrique();"

                                    id="IdCategory" class="form-control">

                                <option value="0">-- Veuillez choisir --</option>

                                <?php if (sizeof($colCategorie)) { ?>

                                    <?php foreach ($colCategorie as $objcolCategorie_xx) { ?>

                                        <option <?php if (isset($objArticle->article_categid) && $objArticle->article_categid == $objcolCategorie_xx->article_categid) echo 'selected="selected"'; ?>

                                                value="<?php echo $objcolCategorie_xx->agenda_categid; ?>"><?php echo $objcolCategorie_xx->category; ?></option>
                                        <?php } ?>


                                <?php } ?>

                            </select>

                        </td>

                    </tr>

                    <tr id='trReponseRub'>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Sous-catégorie : </label>

                        </td>

                        <td>

                            <select name="Article[article_subcategid]" id="IdSubcategory" class="form-control" disabled>

                                <option value="">-- Veuillez choisir --</option>

                                <?php if (sizeof($colSousCategorie)) { ?>

                                    <?php foreach ($colSousCategorie as $objcolSousCategorie_xx) { ?>

                                        <option <?php if (isset($objArticle->article_subcategid) && $objArticle->article_subcategid == $objcolSousCategorie_xx->agenda_subcategid) echo 'selected="selected"'; ?>

                                                value="<?php echo $objcolSousCategorie_xx->agenda_subcategid; ?>"><?php echo $objcolSousCategorie_xx->subcateg; ?></option>

                                    <?php } ?>

                                <?php } ?>

                            </select>

                        </td>

                    </tr>





                    <tr>

                        <td colspan="2" style="padding-top:20px;">

                            <textarea name="Article[description]"

                                      id="description"><?php if (isset($objArticle->description)) echo htmlspecialchars($objArticle->description); ?></textarea>

                            <?php echo display_ckeditor(isset($ckeditor0)); ?>

                        </td>

                    </tr>





                </table>

            </div>





            <div class="div_stl_long_platinum col-xs-12" style="margin:0 !important;">Tarif et réservations en ligne
                <span style="margin-left:520px;color: black"><select id="active_date" name="active_date">
                        <option value="1" >Oui</option>
                        <option value="0">Non</option>
                    </select></span>
            </div>

            <script type="application/javascript">

                jQuery('#active_date').change(function () {

                    var active_tarif = jQuery(this).val();

                    if (active_tarif == "1") {

                        jQuery("#tarifs").css("display", "block");


                    } else {

                        jQuery("#tarifs").css("display", "none");


                    }

                });

            </script>

            <div class="col-xs-12" style="background-color:#E5E5E5; padding:15px;" id="tarifs">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">



                    <tr>

                        <td colspan="2" style="padding-bottom:20px;">

                            <textarea name="Article[description_tarif]"

                                      id="description_tarif"><?php if (isset($objArticle->description_tarif)) echo htmlspecialchars($objArticle->description_tarif); ?></textarea>

                            <?php echo display_ckeditor($ckeditor1); ?>

                        </td>

                    </tr>

                    <tr class="paddingtop15">

                        <td class="stl_long_input_platinum_td"><label class="label">Lien de réservation en ligne

                                : </label></td>

                        <td>

                            <input type="text" name="Article[reservation_enligne]" id="reservation_enligne"

                                   value="<?php if (isset($objArticle->reservation_enligne)) echo htmlspecialchars($objArticle->reservation_enligne); ?>"

                                   class="form-control"/>

                        </td>

                    </tr>

                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Ajouter le type de billetterie : </label>

                        </td>

                        <td>

                            <select name="Article[type_billet]" id="type_billet" class="form-control">

                                <option value="0">-- Veuillez choisir --</option>

                                <option

                                    <?php if (isset($objArticle->type_billet) && $objArticle->type_billet == '1') { ?>selected="selected"<?php } ?>

                                    value="1">Réservation en ligne

                                </option>

                                <option

                                    <?php if (isset($objArticle->type_billet) && $objArticle->type_billet == '2') { ?>selected="selected"<?php } ?>

                                    value="2">Billetterie en prévente

                                </option>

                                <option

                                    <?php if (isset($objArticle->type_billet) && $objArticle->type_billet == '3') { ?>selected="selected"<?php } ?>

                                    value="3">Billetterie en bon plan

                                </option>

                            </select>

                        </td>

                    </tr>

                    <tr>

                        <td colspan="2" style="padding-top:20px; text-align: right;">

                            <img src="<?php echo base_url(); ?>application/resources/sortez/images/type_billet.png"

                                 alt="type de billeterie" id="type_billet_img"/>

                        </td>

                    </tr>

                    <tr>

                        <td colspan="2">

                            <div class="col-xs-12"

                                 style="padding-top: 30px; <?php if (isset($objArticle->type_billet) && $objArticle->type_billet != '3') { ?>display: none;<?php } ?>"

                                 id="get_bonplan_idcommercant_container">

                                <div class="col-xs-4 padding0">

                                    Ajouter le bon plan et vignette

                                </div>

                                <div class="col-xs-4 padding0">

                                    <div id="get_bonplan_idcommercant_content">

                                        <?php $this->load->view("sortez/get_bonplan_idcommercant_article", $data); ?>

                                    </div>

                                </div>

                                <div class="col-xs-4 padding0" style="text-align: right;">

                                    <?php $open_menubonplan_url = site_url("admin/bonplan/listeMesBonplans/" . 301299); ?>

                                    <button class="btn btn-info"

                                            onclick="window.open('<?php echo $open_menubonplan_url; ?>'); return false;">

                                        Menu Bon Plan

                                    </button>

                                </div>

                            </div>

                        </td>

                    </tr>


                    <script type="application/javascript">

                        jQuery('#type_billet').change(function () {

                            jQuery("#get_bonplan_idcommercant_content").html('<img src="<?php echo GetImagePath("admin/");?>/loading.gif" />');

                            var type_billet = jQuery(this).val();

                            if (type_billet == "3") {

                                jQuery("#get_bonplan_idcommercant_container").css("display", "block");

                                jQuery.post(

                                    '<?php echo site_url("admin/bonplan/get_bonplan_idcommercant"); ?>',

                                    {IdCommercant: '<?php echo 301299;?>'},

                                    function (data) {

                                        jQuery("#get_bonplan_idcommercant_content").html(data);

                                    });

                            } else {

                                jQuery("#get_bonplan_idcommercant_container").css("display", "none");

                                jQuery("#get_bonplan_idcommercant_content").html("");

                            }

                        });

                    </script>

                </table>

            </div>
<p>.</p>




            <!--<div class="div_stl_long_platinum col-xs-12" style="margin:0 !important;">Plus d'informations : les liens articles</div>



            <div class="col-xs-12" style="background-color:#E5E5E5; padding:15px;">

                <div class="col-xs-12" style="padding:15px 0 !important;">Par défaut, l’ensemble des coordonnées

                    correspondront aux informations déposés article événement, nous vous conseillons de les intégrer

                    (site web, page Facebook, Twitter, Google+, téléphone, mail)…

                </div>

                <table width="100%" border="0" cellspacing="0" cellpadding="0">



                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">T&eacute;l&eacute;phone : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[telephone]" id="telephone"

                                   value="<?php if (isset($objArticle->telephone)) echo htmlspecialchars(isset($objArticle->telephone)); else echo htmlspecialchars(isset($objCommercant->TelFixe)); ?>"

                                   class="form-control"/>

                        </td>

                    </tr>

                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Mobile : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[mobile]" id="mobile"

                                   value="<?php if (isset($objArticle->mobile)) echo htmlspecialchars(isset($objArticle->mobile)); else echo htmlspecialchars(isset($objCommercant->TelMobile)); ?>"

                                   class="form-control"/>

                        </td>

                    </tr>

                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Fax : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[fax]" id="fax"

                                   value="<?php if (isset($objArticle->fax)) echo htmlspecialchars(isset($objArticle->fax)); else echo htmlspecialchars(isset($objCommercant->fax)); ?>"

                                   class="form-control"/>

                        </td>

                    </tr>

                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Mail de contact article : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[email]" id="email"

                                   value="<?php if (isset($objArticle->email)) echo htmlspecialchars(isset($objArticle->email)); else echo htmlspecialchars(isset($objCommercant->Email)); ?>"

                                   class="form-control"/>



                            <div class="FieldError" id="divErrorEmailSociete"></div>

                        </td>

                    </tr>

                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Site internet : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[siteweb]" id="siteweb"

                                   value="<?php if (isset($objArticle->siteweb) && $objArticle->siteweb != "") echo htmlspecialchars($objArticle->siteweb); else if (isset($objCommercant->SiteWeb)) echo htmlspecialchars($objCommercant->SiteWeb); else echo 'http://www.'; ?>"

                                   class="form-control"/>

                        </td>

                    </tr>

                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Lien Facebook : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[facebook]" id="facebook"

                                   value="<?php if (isset($objArticle->facebook)) echo htmlspecialchars(isset($objArticle->facebook)); else echo htmlspecialchars(isset($objCommercant->Facebook)); ?>"

                                   class="form-control"/>

                        </td>

                    </tr>

                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Lien Twitter : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[twitter]" id="twitter"

                                   value="<?php if (isset($objArticle->twitter)) echo htmlspecialchars(isset($objArticle->twitter)); else echo htmlspecialchars(isset($objCommercant->Twitter)); ?>"

                                   class="form-control"/>

                        </td>

                    </tr>

                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Lien Google + : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[googleplus]" id="googleplus"

                                   value="<?php if (isset($objArticle->googleplus)) echo htmlspecialchars(isset($objArticle->googleplus)); else echo htmlspecialchars(isset($objCommercant->google_plus)); ?>"

                                   class="form-control"/>

                        </td>

                    </tr>

                </table>

            </div>--->





            <div class="div_stl_long_platinum col-xs-12" style="margin-top:0 !important;">Intégration de documents

                multimédias <span style="font-size:12px;">(Photos format 4/3)</span></div>

            <div class="col-xs-12">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">



                    <input type="hidden" name="photo1Associe" id="photo1Associe"

                           value="<?php if (isset($objArticle->photo1)) echo htmlspecialchars($objArticle->photo1); ?>"/>

                    <input type="hidden" name="photo2Associe" id="photo2Associe"

                           value="<?php if (isset($objArticle->photo2)) echo htmlspecialchars($objArticle->photo2); ?>"/>

                    <input type="hidden" name="photo3Associe" id="photo3Associe"

                           value="<?php if (isset($objArticle->photo3)) echo htmlspecialchars($objArticle->photo3); ?>"/>

                    <input type="hidden" name="photo4Associe" id="photo4Associe"

                           value="<?php if (isset($objArticle->photo4)) echo htmlspecialchars($objArticle->photo4); ?>"/>

                    <input type="hidden" name="photo5Associe" id="photo5Associe"

                           value="<?php if (isset($objArticle->photo5)) echo htmlspecialchars($objArticle->photo5); ?>"/>



                    <?php

                    if (isset($mssg) && $mssg != '1') {

                        $img_error_verify_array = preg_split('/-/', $mssg);

                    }
                    if (!isset($commercant_url_home)) $commercant_url_home = '';

                    ?>



                    <?php if (isset($objArticle->id) && $objArticle->id != '0') { ?>

                        <tr>

                            <td class="stl_long_input_platinum_td">

                                <label class="label">Photo 1 : </label>

                            </td>

                            <td>

                                <div id="ArticlePhoto1_container">

                                    <?php if (!empty($objArticle->photo1)) { ?>

                                        <?php

                                        echo '<img  src="' . base_url() . 'application/resources/admin/photoCommercant/imagesbank/' . $objArticle->IdUsers_ionauth . '/' . $objArticle->photo1 . '" width="200"/>';

                                        ?>

                                        <a href="javascript:void(0);" class="btn btn-danger"

                                           onclick="deleteFile('<?php echo $objArticle->id; ?>','photo1');">Supprimer</a>

                                    <?php } else { ?>

                                        <a onclick='javascript:window.open("<?php echo site_url("media/index/?icat=" . $objArticle->id . "&cat=ar&img=photo1"); ?>", "<?php echo $commercant_url_home; ?>", "width=1045, height=675, scrollbars=yes");'

                                           href='javascript:void(0);' title="Photo 1" class="btn btn-default"

                                           id="ArticlePhoto1_link">Ajouter la photo 1</a>

                                    <?php } ?>

                                </div>

                                <div id="div_error_taille_Photo1"

                                     class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>

                            </td>

                        </tr>

                        <tr>

                            <td class="stl_long_input_platinum_td">

                                <label class="label">Photo 2 : </label>

                            </td>

                            <td>

                                <div id="ArticlePhoto2_container">

                                    <?php if (!empty($objArticle->photo2)) { ?>

                                        <?php

                                        echo '<img  src="' . base_url() . 'application/resources/admin/photoCommercant/imagesbank/' . $objArticle->IdUsers_ionauth . '/' . $objArticle->photo2 . '" width="200"/>';

                                        ?>

                                        <a href="javascript:void(0);" class="btn btn-danger"

                                           onclick="deleteFile('<?php echo $objArticle->id; ?>','photo2');">Supprimer</a>

                                    <?php } else { ?>

                                        <a onclick='javascript:window.open("<?php echo site_url("media/index/?icat=" . $objArticle->id . "&cat=ar&img=photo2"); ?>", "<?php echo $commercant_url_home; ?>", "width=1045, height=675, scrollbars=yes");'

                                           href='javascript:void(0);' title="Photo 2" class="btn btn-default"

                                           id="ArticlePhoto2_link">Ajouter la photo 2</a>

                                    <?php } ?>

                                </div>

                                <div id="div_error_taille_Photo2"

                                     class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo2", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>

                            </td>

                        </tr>

                        <tr>

                            <td class="stl_long_input_platinum_td">

                                <label class="label">Photo 3 : </label>

                            </td>

                            <td>

                                <div id="ArticlePhoto3_container">

                                    <?php if (!empty($objArticle->photo3)) { ?>

                                        <?php

                                        echo '<img  src="' . base_url() . 'application/resources/admin/photoCommercant/imagesbank/' . $objArticle->IdUsers_ionauth . '/' . $objArticle->photo3 . '" width="200"/>';

                                        ?>

                                        <a href="javascript:void(0);" class="btn btn-danger"

                                           onclick="deleteFile('<?php echo $objArticle->id; ?>','photo3');">Supprimer</a>

                                    <?php } else { ?>

                                        <a onclick='javascript:window.open("<?php echo site_url("media/index/?icat=" . $objArticle->id . "&cat=ar&img=photo3"); ?>", "<?php echo $commercant_url_home; ?>", "width=1045, height=675, scrollbars=yes");'

                                           href='javascript:void(0);' title="Photo 3" class="btn btn-default"

                                           id="ArticlePhoto3_link">Ajouter la photo 3</a>

                                    <?php } ?>

                                </div>

                                <div id="div_error_taille_Photo3"

                                     class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo3", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>

                            </td>

                        </tr>

                        <tr>

                            <td class="stl_long_input_platinum_td">

                                <label class="label">Photo 4 : </label>

                            </td>

                            <td>

                                <div id="ArticlePhoto4_container">

                                    <?php if (!empty($objArticle->photo4)) { ?>

                                        <?php

                                        echo '<img  src="' . base_url() . 'application/resources/admin/photoCommercant/imagesbank/' . $objArticle->IdUsers_ionauth . '/' . $objArticle->photo4 . '" width="200"/>';

                                        ?>

                                        <a href="javascript:void(0);" class="btn btn-danger"

                                           onclick="deleteFile('<?php echo $objArticle->id; ?>','photo4');">Supprimer</a>

                                    <?php } else { ?>

                                        <a onclick='javascript:window.open("<?php echo site_url("media/index/?icat=" . $objArticle->id . "&cat=ar&img=photo4"); ?>", "<?php echo $commercant_url_home; ?>", "width=1045, height=675, scrollbars=yes");'

                                           href='javascript:void(0);' title="Photo 4" class="btn btn-default"

                                           id="ArticlePhoto4_link">Ajouter la photo 4</a>

                                    <?php } ?>

                                </div>

                                <div id="div_error_taille_Photo4"

                                     class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo4", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>

                            </td>

                        </tr>

                        <tr>

                            <td class="stl_long_input_platinum_td">

                                <label class="label">Photo 5 : </label>

                            </td>

                            <td>

                                <div id="ArticlePhoto5_container">

                                    <?php if (!empty($objArticle->photo5)) { ?>

                                        <?php

                                        echo '<img  src="' . base_url() . 'application/resources/admin/photoCommercant/imagesbank/' . $objArticle->IdUsers_ionauth . '/' . $objArticle->photo5 . '" width="200"/>';

                                        ?>

                                        <a href="javascript:void(0);" class="btn btn-danger"

                                           onclick="deleteFile('<?php echo $objArticle->id; ?>','photo5');">Supprimer</a>

                                    <?php } else { ?>

                                        <a onclick='javascript:window.open("<?php echo site_url("media/index/?icat=" . $objArticle->id . "&cat=ar&img=photo5"); ?>", "<?php echo $commercant_url_home; ?>", "width=1045, height=675, scrollbars=yes");'

                                           href='javascript:void(0);' title="Photo 5" class="btn btn-default"

                                           id="ArticlePhoto5_link">Ajouter la photo 5</a>

                                    <?php } ?>

                                </div>

                                <div id="div_error_taille_Photo5"

                                     class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo5", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>

                            </td>

                        </tr>

                    <?php } else { ?>

                        <tr>

                            <td colspan="2">

                                <label class="alert alert-danger">Veuillez enregistrer votre Articles avant d'ajouter les

                                    images !</label>

                            </td>

                        </tr>

                    <?php } ?>



                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Lien vers votre vidéo<br/>YouTube ou DailyMotion : </label>

                        </td>

                        <td>

                            <span style="font-family: Verdana, Geneva, sans-serif; font-size:9px;">http://www.youtube.com/watch?v=IMX1M46MQAI</span><br/>

                            <input type="text" name="Article[video]" id="video"

                                   value="<?php if (isset($objArticle->video)) echo htmlspecialchars($objArticle->video); ?>"

                                   class="form-control"/>

                        </td>

                    </tr>

                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Lien vers un fichier audio : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[audio]" id="audio"

                                   value="<?php if (isset($objArticle->audio)) echo htmlspecialchars($objArticle->audio); ?>"

                                   class="form-control"/>

                        </td>

                    </tr>

                    <tr valign="middle">

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Document PDF : </label>

                            <input type="hidden" name="pdfAssocie" id="pdfAssocie"

                                   value="<?php if (isset($objArticle->pdf)) echo htmlspecialchars($objArticle->pdf); ?>"/>

                        </td>

                        <td>

                            <?php if (!empty($objArticle->pdf)) { ?>

                                <?php //echo $objArticle->Pdf; ?>



                                <?php if ($objArticle->pdf != "" && $objArticle->pdf != NULL && file_exists("application/resources/admin/images/article/pdf/" . $objArticle->pdf) == true) { ?>

                                <a

                                        href="<?php echo base_url(); ?>application/resources/admin/images/article/pdf/<?php echo $objArticle->pdf; ?>"

                                        target="_blank"><img

                                            src="<?php echo base_url(); ?>application/resources/admin/images/pdf_icone.png"

                                            width="64" alt="PDF"/></a><?php } ?>





                                <a href="javascript:void(0);" class="btn btn-danger"

                                   onclick="deleteFile('<?php echo $objArticle->id; ?>','pdf');">Supprimer</a><br/>



                            <?php } else { ?>

                                <input type="file" name="ArticlePdf" id="ArticlePdf" value=""

                                       class="form-control-file"/>

                            <?php } ?>

                        </td>

                    </tr>



                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Titre doc PDF : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[titre_pdf]" id="titre_pdf"

                                   value="<?php if (isset($objArticle->titre_pdf)) echo $objArticle->titre_pdf; ?>"

                                   class="form-control"/>

                        </td>

                    </tr>





                </table>

            </div>



            <div class="div_stl_long_platinum col-xs-12" style="margin-bottom: 0 !important;">Lieu de l'évènement
                <span style="margin-left:520px;color: black"><select name="event_active" id="event_active">
                        <option value="1" >Oui</option>
                        <option value="0">Non</option>
                    </select></span></div>
            <script type="application/javascript">

                jQuery('#event_active').change(function () {
                    var event_active = jQuery(this).val();

                    if (event_active == "1") {

                        jQuery("#lieu_event").css("display", "block");


                    } else {

                        jQuery("#lieu_event").css("display", "none");


                    }

                });

            </script>
            <div class="col-xs-12 greybg" id="lieu_event">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                    <tr>

                        <td class="stl_long_input_platinum_td" style="text-align:center;"><img

                                    id="adresse_localisation_diffuseur_loading"

                                    src="<?php echo GetImagePath("admin/"); ?>/loading.gif" style="margin-top: 10px"/></td>

                        <td>

                            <input type="checkbox" name="adresse_localisation_diffuseur_checkbox"

                                   id="adresse_localisation_diffuseur_checkbox"

                                   <?php if (isset($objArticle->adresse_localisation_diffuseur) && $objArticle->adresse_localisation_diffuseur == "1") { ?>checked<?php } ?>/>

                            <input type="hidden" name="Article[adresse_localisation_diffuseur]"

                                   id="adresse_localisation_diffuseur"

                                   value="<?php if (isset($objArticle->adresse_localisation_diffuseur)) echo htmlspecialchars($objArticle->adresse_localisation_diffuseur); else echo '0'; ?>">


                            <span style="font-size:14px;">Par défaut adresse de votre siège social<br/>(adresse dans Menu Mes contenus G&eacute;olocalisation)</span>

                            :

                            <div id="adresse_localisation_diffuseur_error" class="col-xs-12 alert alert-danger"

                                 style="padding:0;"></div>

                        </td>
                        <script type="text/javascript">
                            function verif ()
                            {
                                var etat = document.getElementById('check').checked;

                                if(etat)
                                {


                                    document.getElementById('1').className = 'on';
                                }
                                else
                                {

                                    document.getElementById('1').className = 'off';
                                }
                            }
                        </script>
                        <style type="text/css">
                            .on {
                                display: block;
                            }

                            .off {
                                display: none;
                            }
                        </style>

                        <div style="margin-left: 180px"><input type="checkbox" name="plusieurs_lieux" id="check" value="1" onChange="verif();">Si plusieurs lieux,cliquer ici</div>

                    </tr>

                    <tr style="display: none;">

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Nom du lieu : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[nom_localisation]" id="nom_localisation"

                                   value="<?php if (isset($objArticle->nom_localisation)) echo $objArticle->nom_localisation; ?>"

                                   class="form-control"/>

                        </td>

                    </tr>





                    <!--*********************************************************--->

                    <tr>

                        <td colspan="2">

                            <div class="col-xs-12 padding0" style="margin-top:15px; margin-bottom: 15px;">

                                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>

                                        <td class="stl_long_input_platinum_td"><label class="label"> Lieu : </label>

                                        </td>

                                        <td>

                                            <div class="col-xs-6" style="padding:0 !important;"><select

                                                        id="location_article"

                                                        name="Article[location_id]"

                                                        class="form-control">

                                                    <option value="0">Choisir un Lieu</option>

                                                    <?php if (sizeof($col_Article_location)) { ?>

                                                        <?php foreach ($col_Article_location as $objcol_article_location_xx) { ?>

                                                            <option <?php if (isset($objArticle->location_id) && $objArticle->location_id == $objcol_article_location_xx->location_id) echo 'selected="selected"'; ?>

                                                                    value="<?php echo $objcol_article_location_xx->location_id; ?>"><?php echo $objcol_article_location_xx->location; ?></option>

                                                        <?php } ?>

                                                    <?php } ?>

                                                </select></div>

                                            <div class="col-xs-6">

                                                <a href="javascript:void(0);" class="btn btn-success"

                                                   id="add_location_form_link_open" title="Ajouter un nouveau lieu">Ajouter

                                                    un lieu</a>

                                                <a href="javascript:void(0);" class="btn btn-primary"

                                                   id="edit_location_form_link_open" title="Modifier le lieu"

                                                   style="display: none;"><img style="border: none;width:15px;"

                                                                               src="<?php echo base_url(); ?>application/resources/privicarte/images/update_ico.png"></a>

                                                <a href="javascript:void(0);" class="btn btn-danger"

                                                   id="delete_location_form_link_open" title="Supprimer le lieu"

                                                   style="display: none;"><img style="border: none;width:15px;"

                                                                               src="<?php echo base_url(); ?>application/resources/privicarte/images/delete_ico.png"></a>

                                                <img id="select_location_form_loading"

                                                     src="<?php echo GetImagePath("admin/"); ?>/loading.gif"

                                                     style="float: right; margin-top: 10px; display: none;"/>

                                            </div>

                                        </td>

                                    </tr>

                                </table>

                            </div>



                            <div class="col-xs-12"

                                 style="background-color:#FFFFFF; margin-bottom: 15px; margin-top:15px; padding-top: 15px; padding-bottom: 15px;"

                                 id="form_add_location_div">

                                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>

                                        <td class="stl_long_input_platinum_td"><label class="label">Ajouter un nom Lieu

                                                : </label></td>

                                        <td>

                                            <div class="col-xs-6" style="padding:0 !important;">

                                                <input type="text"

                                                       id="add_location_name"

                                                       name="add_location_name"

                                                       class="form-control"/>

                                                <input type="hidden"

                                                       id="add_location_id"

                                                       name="add_location_id"

                                                       class="form-control" value="0"/>

                                            </div>

                                            <div class="col-xs-6">

                                                <a href="javascript:void(0);" id="add_location_form_cancel"

                                                   class="btn btn-warning">Annuler</a>

                                                <a href="javascript:void(0);" id="add_location_form_save"

                                                   class="btn btn-success">Ajouter</a>

                                                <img id="add_location_form_loading"

                                                     src="<?php echo GetImagePath("admin/"); ?>/loading.gif"

                                                     style="float: right; margin-top: 10px"/>

                                            </div>

                                            <div class="col-xs-12 alert alert-danger" id="add_location_form_error"

                                                 style="margin: 15px 0 0 0;">Une errreur s'est produite ou ce nom existe

                                                d&eacute;j&agrave; !

                                            </div>

                                        </td>

                                    </tr>

                                    <tr>

                                        <td class="stl_long_input_platinum_td">

                                            <label class="label">Adresse : </label>

                                        </td>

                                        <td>

                                            <input type="text" name="add_location_adress"

                                                   id="add_location_adress"

                                                   value=""

                                                   class="form-control"/>

                                        </td>

                                    </tr>

                                    <tr>

                                        <td class="stl_long_input_platinum_td">

                                            <label class="label">Code Postal : </label>

                                        </td>

                                        <td id="container_add_location_codepostal">

                                            <input type="text" name="add_location_codepostal"

                                                   id="add_location_codepostal"

                                                   value=""

                                                   class="form-control"

                                                   onblur="javascript:CP_getDepartement_add_location();CP_getVille_add_location();jQuery('#codepostal_localisation').focus();jQuery('#codepostal_localisation').blur();"/>

                                        </td>

                                    </tr>

                                    <tr>


                                <td class="stl_long_input_platinum_td">

                                            <label class="label">Departement : </label>

                                        </td>

                                        <td>

                                            <span id="departementCP_container_add_location">

                                                <select name="add_location_departementid"

                                                        id="add_location_departementid" disabled="disabled"

                                                        class="form-control"

                                                        onchange="javascript:CP_getVille_D_CP_localisation();">

                                                    <option value="0">-- Veuillez choisir --</option>

                                                    <?php if (sizeof($colDepartement)) { ?>

                                                        <?php foreach ($colDepartement as $objDepartement) { ?>

                                                            <option

                                                                    value="<?php echo $objDepartement->departement_id; ?>"><?php echo $objDepartement->departement_nom . " - " . $objDepartement->departement_code; ?></option>

                                                        <?php } ?>

                                                    <?php } ?>

                                                </select>

                                            </span>

                                        </td>

                                    </tr>

                                    <tr>

                                        <td class="stl_long_input_platinum_td">

                                            <label class="label">Ville : </label>

                                        </td>

                                        <td>

                                            <span id="villeCP_container_add_location">

                                            <input type="text"

                                                   value=""

                                                   name="IdVille_Nom_text_add_location"

                                                   id="IdVille_Nom_text_add_location" disabled="disabled"

                                                   class="form-control"/>

                                            <input type="hidden"

                                                   value="0"

                                                   name="add_location_villeId" id="add_location_villeId"/>

                                            </span>

                                        </td>

                                    </tr>

                                </table>

                            </div>

                        </td>

                    </tr>

                    <script type="application/javascript">

                        jQuery(document).ready(function () {

                            //init add redacteur

                            jQuery("#form_add_location_div").hide();

                            jQuery("#add_location_form_loading").hide();

                            jQuery("#add_location_form_error").hide();

                            //	ADD REDACTEUR START

                            $("#add_location_form_link_open").click(function () {

                                jQuery("#add_location_id").val("");

                                jQuery("#add_location_name").val("");

                                jQuery("#add_location_adress").val("");

                                jQuery("#add_location_codepostal").val("");

                                jQuery("#add_location_departementid").val("0");

                                jQuery("#add_location_villeId").val("0");



                                jQuery("#form_add_location_div").show('blind');

                            });



                            $("#add_location_form_cancel").click(function () {

                                jQuery("#form_add_location_div").hide('blind');

                            });



                            $("#edit_location_form_link_open").click(function () {

                                jQuery("#form_add_location_div").show('blind');

                            });



                            $("#delete_location_form_link_open").click(function () {

                                jQuery("#select_location_form_loading").css("display", "block");

                                if (confirm("Voulez-vous supprimer le lieu séléctionné ?")) {

                                    var location_article = jQuery('#location_article').val();

                                    jQuery.post(

                                        '<?php echo site_url("admin/Articles/delete_location_article"); ?>',

                                        {

                                            location_article: location_article

                                        },

                                        function (zReponse) {

                                            if (zReponse == "error") {

                                                alert("Une erreur est survenue, veuillez refaire l'opération !");

                                            } else {

                                                document.location.reload();

                                            }

                                        });

                                } else {

                                    jQuery("#select_location_form_loading").css("display", "none");

                                }

                            });



                            $("#add_location_form_save").click(function () {

                                jQuery("#add_location_form_loading").show();

                                var add_location_id = jQuery('#add_location_id').val();

                                var add_location_name = jQuery('#add_location_name').val();

                                var add_location_adress = jQuery('#add_location_adress').val();

                                var add_location_codepostal = jQuery('#add_location_codepostal').val();

                                var add_location_departementid = jQuery('#add_location_departementid').val();

                                var add_location_villeId = jQuery('#add_location_villeId').val();

                                if (add_location_name == "" || add_location_adress == "" || add_location_codepostal == "" || add_location_departementid == "" || add_location_departementid == "0" || add_location_villeId == "" || add_location_villeId == "0") {

                                    jQuery('#add_location_name').css("border-color", "#ff0000");

                                    jQuery('#add_location_adress').css("border-color", "#ff0000");

                                    jQuery('#add_location_codepostal').css("border-color", "#ff0000");

                                    jQuery('#add_location_departementid').css("border-color", "#ff0000");

                                    jQuery('#add_location_villeId').css("border-color", "#ff0000");

                                    jQuery('#IdVille_Nom_text_add_location').css("border-color", "#ff0000");

                                    jQuery("#add_location_form_loading").hide();

                                } else {



                                    jQuery.post(

                                        '<?php echo site_url("admin/Articles/add_location_Article"); ?>',

                                        {

                                            add_location_id: jQuery('#add_location_id').val(),

                                            add_location_name: jQuery('#add_location_name').val(),

                                            add_location_adress: jQuery('#add_location_adress').val(),

                                            add_location_codepostal: jQuery('#add_location_codepostal').val(),

                                            add_location_departementid: jQuery('#add_location_departementid').val(),

                                            add_location_villeId: jQuery('#add_location_villeId').val(),

                                            IdCommercant: <?php echo 301299; ?>

                                        },

                                        function (zReponse) {

                                            if (zReponse != "error") {

                                                jQuery("#location_article option[value='" + zReponse + "']").remove();

                                                jQuery('#location_article').append(jQuery('<option>', {

                                                    value: zReponse,

                                                    text: jQuery('#add_location_name').val()

                                                }));

                                                jQuery('#location_article').val(zReponse);

                                                jQuery('#nom_localisation').val(jQuery('#add_location_name').val());

                                                jQuery('#adresse_localisation').val(jQuery('#add_location_adress').val());

                                                jQuery('#codepostal_localisation').val(jQuery('#add_location_codepostal').val());

                                                jQuery('#DepartementAgendaLocalisation').val(jQuery('#add_location_departementid').val());

                                                jQuery('#IdVille_Nom_text_localisation').val(jQuery('#add_location_villeId').find(":selected").text());

                                                jQuery('#location_article').find(":selected").text(jQuery('#add_location_name').val());



                                                jQuery("#form_add_location_div").hide('blind');

                                                jQuery("#add_location_form_loading").hide();

                                                jQuery("#add_location_form_error").hide();



                                            } else {

                                                jQuery("#add_location_form_error").show();

                                                jQuery("#add_location_form_loading").hide();

                                            }

                                        }

                                    )

                                    ;

                                }

                            });

                            //ADD REDACTEUR END

                        })

                    </script>

                    <!--*********************************************************--->



                    <tr>

                        <td class="stl_long_input_platinum_td">

                            <label class="label">Adresse : </label>

                        </td>

                        <td>

                            <input type="text" name="Article[adresse_localisation]" id="adresse_localisation"

                                   value="<?php if (isset($objArticle->adresse_localisation)) echo $objArticle->adresse_localisation; ?>"

                                   class="form-control"/>

                        </td>

                    </tr>





                    <tr>
                        <td>&nbsp;

                        </td>
                        <td>&nbsp;

                        </td>

                    </tr>



                    <tr>
                        <td>&nbsp;

                        </td>

                        <td>

    	<span id="departementCP_container_localisation">

        <select name="Article[departement_id_localisation]" id="DepartementAgendaLocalisation" disabled="disabled"

                class="form-control" onchange="javascript:CP_getVille_D_CP_localisation();">

            <option value="0">-- Veuillez choisir --</option>

            <?php if (sizeof($colDepartement)) { ?>

                <?php foreach ($colDepartement as $objDepartement) { ?>

                    <option

                        <?php if (isset($objArticle->departement_id_localisation) && $objArticle->departement_id_localisation == $objDepartement->departement_id) { ?>selected="selected"<?php } ?>

                        value="<?php echo $objDepartement->departement_id; ?>"><?php echo $objDepartement->departement_nom . " - " . $objDepartement->departement_code; ?></option>

                <?php } ?>

            <?php } ?>

        </select>

        </span>

                        </td>

                    </tr>


					<tr>
                        <td></td>
                        <td align="left">
                        <input type="button" value="Annuler" onclick="javascript:document.location='<?php echo site_url("admin/articles/liste");?>';" />&nbsp;
                        <input type="button" value="Valider" onclick="javascript:ValidationFormArticle();" /></td>
                    </tr>





                    <tr>

                        <td colspan="2">

                            <?php if (isset($ville_to_show_onn_map)) $ville_map = $ville_to_show_onn_map; else $ville_map = ''; ?>

                            <?php if (isset($objArticle->adresse_localisation)) { ?>

                                <iframe style="margin-top:20px;" width="100%" height="483" frameborder="0"

                                        scrolling="no" marginheight="0" marginwidth="0"

                                        src="http://maps.google.fr/maps?key=AIzaSyDIUJWErPlxtFdEVrlbAp5yjvRF20FLLr8&amp;f=q&amp;source=s_q&amp;hl=fr&amp;geocode=&amp;q=<?php echo $objArticle->adresse_localisation . ", " . $objArticle->codepostal_localisation . " &nbsp;" . $ville_map; ?>&amp;aq=0&amp;ie=UTF8&amp;hq=&amp;hnear=<?php echo $objArticle->adresse_localisation . ", " . $objArticle->codepostal_localisation . " &nbsp;" . $ville_map; ?>&amp;t=m&amp;vpsrc=0&amp;output=embed"></iframe>

                            <?php } ?>

                        </td>

                    </tr>





                </table>
            </fieldset>

            </div>

        <span id="1" style="font-size: larger;text-align: center;font-family: arial;" class="off">    <div ><div></div><img src="<?php echo base_url(); ?>assets/img/plusieurs_lieux.jpg" style="height: 258px;"><span>Plusieurs lieux de rencontre concernant cet événement.<br>
                Pour plus de détails visualisez chaque manifestation liée à ce Articles.</span><br><br></div>

</span>


            <p>.</p>

            <div class="div_stl_long_platinum col-xs-12" style="margin-top: 0 !important;">Commentaires Facebook</div>

            <div class="col-xs-12">

                <div class="col-xs-3" style="padding:0;">Activation commentaires Facebook</div>

                <div class="col-xs-9" style="padding:0; text-align:right;">

                    <select id="activ_fb_comment_article" name="Article[activ_fb_comment]" class="form-control">

                        <option

                            <?php if (isset($objArticle->activ_fb_comment) && $objArticle->activ_fb_comment == "0") { ?>selected="selected"<?php } ?>

                            value="0">NON

                        </option>

                        <option

                            <?php if (isset($objArticle->activ_fb_comment) && $objArticle->activ_fb_comment == "1") { ?>selected="selected"<?php } ?>

                            value="1">OUI

                        </option>

                    </select>

                </div>

            </div>





            <div class="div_stl_long_platinum col-xs-12">

                <div class="col-xs-3" style="padding:0;">La Diffusion</div>



            </div>





            <div class="col-xs-12">

                <div class="col-xs-6" style="padding:0;">

                    <fieldset class="col-xs-12" style="padding:0; margin:0 0 20px 0;">

                        <legend>Publication</legend>

                        <table border="0" cellspacing="0" cellpadding="0" style="padding-left:20px;">

                            <tr>

                                <td><input type="radio" name="admin_article_activation" id="admin_article_activation_0"

                                           <?php if (isset($objArticle->IsActif) && $objArticle->IsActif == "0") { ?>checked<?php } ?>/>

                                </td>

                                <td><label class="label">En attente de validation</label></td>

                            </tr>

                            <tr>

                                <td><input type="radio" name="admin_article_activation" id="admin_article_activation_1"

                                           <?php if (isset($objArticle->IsActif) && $objArticle->IsActif == "1") { ?>checked<?php } ?>/>

                                </td>

                                <td><label class="label">Valid&eacute; & Activ&eacute;</label></td>

                            </tr>

                            <tr>

                                <td><input type="radio" name="admin_article_activation" id="admin_article_activation_2"

                                           <?php if (isset($objArticle->IsActif) && $objArticle->IsActif == "2") { ?>checked<?php } ?>/>

                                </td>

                                <td><label class="label">Annuler</label></td>

                            </tr>

                            <tr>

                                <td><input type="radio" name="admin_article_activation" id="admin_article_activation_3"

                                           <?php if (isset($objArticle->IsActif) && $objArticle->IsActif == "3") { ?>checked<?php } ?>/>

                                </td>

                                <td><label class="label">Supprimer</label></td>

                            </tr>





                        </table>

                        <input type="hidden" name="Article[IsActif]" id="IsActif"

                               value="<?php if (isset($objArticle->IsActif)) echo htmlspecialchars($objArticle->IsActif); else echo "0"; ?>">

                    </fieldset>





                    <div class="col-xs-12" style="padding:0;">

                        <div class="col-xs-12" style="margin:0 0 15px 0; width:250px;padding:0;">

                            <div class="col-xs-12" style="padding:0;">

                                <a href="javascript:void(0);" class="envoyer_validation_article"

                                   id="link_envoyer_validation_article">Envoyer pour validation</a>

                                <img id="envoyer_validation_article_form_loading"

                                     src="<?php echo GetImagePath("admin/"); ?>/loading.gif"

                                     style="float: right; margin-top: 10px"/>

                            </div>

                            <div class="col-xs-12 alert alert-danger" id="envoyer_validation_article_form_error"

                                 style="margin: 15px 0 0 0;">Une errreur s'est produite, veuillez renvoyer le message

                                dans quelques instants !

                            </div>

                            <div class="col-xs-12 alert alert-success" id="envoyer_validation_article_form_success"

                                 style="margin: 15px 0 0 0;">Le message a bien &eacute;t&eacute; envoy&eacute; ! Une

                                copie vous a &eacute;t&eacute; adress&eacute; !

                            </div>

                        </div>

                        <div style="margin:0 0 15px 0;">

                            <select id="destinataire_validation_article" name="destinataire_validation_article"

                                    class="form-control" style="width: 250px;">

                                <option value="0">Choisir le destinataire</option>

                                <?php if (sizeof($col_article_validation_email)) { ?>

                                    <?php foreach ($col_article_validation_email as $objcol_article_validation_email_xx) { ?>

                                        <option

                                                value="<?php echo $objcol_article_validation_email_xx->id; ?>"><?php echo $objcol_article_validation_email_xx->nom . ' / ' . $objcol_article_validation_email_xx->email; ?></option>

                                    <?php } ?>

                                <?php } ?>

                            </select>

                        </div>

                        <div style="margin:0 0 15px 0;">

                            <a href="javascript:void(0);" class="btn btn-primary"

                               title="Ajouter une adresse e-mail"

                               id="add_validation_article_form_link_open">+</a>

                            <a href="javascript:void(0);" class="btn btn-primary"

                               id="add_validation_article_form_link_edit" title="Modifier le validation_article"

                               style="<?php if (isset($objArticle->validation_article_id) && $objArticle->validation_article_id != null && $objArticle->validation_article_id != "0") { ?>display: inline;<?php } else { ?>display: none;<?php } ?>">

                                <img

                                        style="border: none;width:15px;"

                                        src="<?php echo base_url(); ?>application/resources/privicarte/images/update_ico.png"></a>

                            <a href="javascript:void(0);" class="btn btn-danger"

                               id="add_validation_article_form_link_delete" title="Supprimer le validation_article"

                               style="<?php if (isset($objArticle->validation_article_id) && $objArticle->validation_article_id != null && $objArticle->validation_article_id != "0") { ?>display: inline;<?php } else { ?>display: none;<?php } ?>">

                                <img

                                        style="border: none;width:15px;"

                                        src="<?php echo base_url(); ?>application/resources/privicarte/images/delete_ico.png"></a>

                            <img id="add_validation_article_form_locading"

                                 src="<?php echo GetImagePath("admin/"); ?>/loading.gif"

                                 style="float: right; margin-top: 10px; display: none;">

                        </div>

                    </div>





                    <div class="col-xs-12" style="background-color:#E5E5E5; padding:15px; margin-top:10px;"

                         id="form_add_validation_article_div">

                        <div class="col-xs-12" style="padding:0 !important;"><label class="label">Nom</label><input

                                    type="text" id="add_validation_article_name" name="add_validation_article_name"

                                    class="form-control"/>

                        </div>

                        <div class="col-xs-12" style="padding:0 !important;"><label class="label">E-mail</label><input

                                    type="text" id="add_validation_article_email" name="add_validation_article_email"

                                    class="form-control"/>

                        </div>

                        <div class="col-xs-12" style="padding: 0; margin-top: 15px;">

                            <a href="javascript:void(0);" id="add_validation_article_form_cancel"

                               class="btn btn-warning">Annuler</a>

                            <a href="javascript:void(0);" id="add_validation_article_form_save" class="btn btn-success">Enregistrer</a>

                            <img id="add_validation_article_form_loading"

                                 src="<?php echo GetImagePath("admin/"); ?>/loading.gif"

                                 style="float: right; margin-top: 10px"/>

                        </div>

                        <div class="col-xs-12 alert alert-danger" id="add_validation_article_form_error"

                             style="margin: 15px 0 0 0;">Une errreur s'est produite, veuillez renvoyer le message dans

                            quelques instants !

                        </div>

                    </div>





                    <div class="col-xs-12" style="padding:0 !important; text-alignter:center; margin-top:40px;">

                        <button type="button" class="btnSinscrire" value="Validation du dépôt" style="width: 250px;">

                            Enregistrement

                        </button>

                    </div>



                </div>

                <div class="col-xs-6" style="padding:0;">



                    <div style="text-align:center; margin-top:40px;" id="id_save_agenda_before_view">

                        <a href="<?php if (isset($objArticle->id)) echo site_url("admin/Articles/details_Article/" . $objArticle->id); else echo "javascript:alert_save_agenda_before_view();"; ?>" <?php if (isset($objArticle->id)) echo 'target="_blank"'; ?>><img

                                    src="<?php echo GetImagePath("privicarte/"); ?>/visualisation_article.png"

                                    alt="visualisation_article" title="previsualisatiton_agenda"/></a>

                    </div>



                </div>

            </div>



            <div id="div_error_agenda_submit" class="col-xs-12"

                 style="color:#FF0000; text-align:center; width:100%;padding:20px 0;"></div>





            <?php if (isset($objArticle->id)) { ?>

                <div class="div_stl_long_platinum col-xs-12">Partager cet évènement</div>

                <div

                        style="padding:0 !important; text-align:center ; margin-top:40px ; padding-bottom:35px; display: table; width: 100%;">

                    <a href="javascript:void(0);"

                       onclick='javascript:window.open("https://www.facebook.com/sharer/sharer.php?u=<?php echo site_url('article/details/' . $objArticle->id); ?>", "FacebookPrivicarteForm_share", "width=500, height=800");'

                       title="Partage Facebook"><img

                                src="<?php echo GetImagePath("privicarte/"); ?>/wp9efa6d49_06.png"/></a>

                </div>

                <div

                        style="padding:0 !important; text-align:center ; margin-top:40px ; padding-bottom:35px;display: table; width: 100%;">

                    <a href="javascript:void(0);"

                       onclick='javascript:window.open("https://twitter.com/home?status=<?php echo site_url('article/details/' . $objArticle->id); ?>", "TwitterPrivicarteForm_share", "width=500, height=800");'

                       title="Partage Twitter"><img src="<?php echo GetImagePath("privicarte/"); ?>/wp9cd46019_06.png"/></a>

                </div>

            <?php } ?>





        </form>





    </div>



<?php $this->load->view("frontAout2013/includes/footer_mini_2"); ?>