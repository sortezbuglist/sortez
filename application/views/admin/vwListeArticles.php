<?php $data["zTitle"] = 'Gestion Articles'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra'],  headers: {6: { sorter: false}, 7: {sorter: false} }})
				.tablesorterPager({container: $("#pager")});
		});
		
		function confirm_delete_article(IdRubrique){
			if (confirm("Voulez-vous supprimer cet article définitivement ?")) {
			   document.location="<?php echo site_url("admin/articles/suppression/");?>/"+IdRubrique;
		   }
		}
</script>

    <div id="divAdminHome" class="content" align="center">
	     <p><a style = "color:black; text-decoration:none;" href = "<?php echo site_url("admin/home" ) ; ?>"> <input type = "button" value= "retour au menu" id = "btn" onclick="document.location='<?php echo site_url("admin/home/" );?>';" class="btn btn-primary"/>  </a></p>
		 <p><a style = "color:black;text-decoration:none;" href = "<?php echo site_url("admin/articles/fiche/301299") ;?>"> <input type = "button" value= "Ajouter Article" id = "btn" class="btn btn-success"/>  </a></p>
        <h1>Admin Articles</h1>
    </div>
        <div style="color:#33CC33"><?php if (isset($msg)) echo $msg;?></div>
        <table cellpadding="1" class="tablesorter" style="text-align:left">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Titre</th>
                    <th>Catégorie</th>
                    <th>Souscatégorie</th>
                    <th>Mise à jour</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
            <?php if(sizeof($colArticles)) { ?>
                <?php foreach($colArticles as $objArticle) { ?>
                    <tr>
                        <td>
                            <a href="<?php echo site_url("admin/articles/fiche/".$objArticle->id); ?>">
                                <?php echo htmlspecialchars(stripcslashes($objArticle->id)); ?>
                            </a>
                        </td>
                        <td><?php if(isset($objArticle->nom_manifestation)) echo htmlspecialchars(stripcslashes($objArticle->nom_manifestation)); ?></td>
                        <td>
                        <?php 
						$this->load->model("mdl_categories_article");
						if(isset($objArticle->article_categid)) $oTypeArticle = $this->mdl_categories_agenda->getById($objArticle->article_categid);
        				if(isset($oTypeArticle->category)) echo $oTypeArticle->category;
						?>
                        </td>
                        <td>
                        <?php 
						$this->load->model("mdl_categories_article");
						if(isset($objArticle->article_subcategid)) $oTypeArticle = $this->mdl_categories_agenda->getByIdSousCateg($objArticle->article_subcategid);
        				if(isset($oTypeArticle->subcateg)) echo $oTypeArticle->subcateg;
						?>
                        </td>
                        <td><?php 
						//echo htmlspecialchars(stripcslashes($objArticle->lastupdate)); 
						if (isset($objArticle->last_update) && $objArticle->last_update != "" && $objArticle->last_update != NULL) {
							$tzDateExploded_1 = explode (" ", $objArticle->last_update) ;
							echo convert_Sqldate_to_Frenchdate($tzDateExploded_1[0]).", ".$tzDateExploded_1[1];
						}
						?></td>
                        <td>
                            <a href="#<?php //if(isset($objArticle) && is_object($objArticle) && isset($objArticle->id)) echo site_url("admin/articles/fiche/".$objArticle->id); else echo '#';?>">
                                Modifier
                            </a>
                        </td>
                        <td>
                            <a href="javascript:void();" onClick="confirm_delete_article(<?php if(isset($objArticle->id)) echo $objArticle->id;?>);">
                                Supprimer
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
            </tbody>
        </table>
		<div id="pager" class="pager">
			<img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
			<img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
			<input type="text" class="pagedisplay"/>
			<img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
			<img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
			<select class="pagesize" style="visibility:hidden">
				<option selected="selected"  value="10">10</option>
				<option value="20">20</option>
				<option value="30">30</option>
				<option  value="40">40</option>
			</select>
		</div>
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>