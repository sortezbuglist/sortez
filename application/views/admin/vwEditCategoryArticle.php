<?php $data["zTitle"] = 'Gestion Categorie Article'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script type="text/javascript" charset="utf-8">
	$(function() {
				$("#btnsubmit_editcategorie").click(function(){
					
					var txtError = "";
					
					//verify input content
					var inputcateg_editcategorie = $("#inputcateg_editcategorie").val();
					if(inputcateg_editcategorie=="") {
						txtError += "- Veuillez indiquer Votre inputcateg_editcategorie <br/>"; 
						$("#inputcateg_editcategorie").css('border-color', 'red');
					} else {
						$("#inputcateg_editcategorie").css('border-color', '#E3E1E2');
					}
					
					
					var article_typeid = $("#article_typeid").val();
					if(article_typeid=="" || article_typeid=="0") {
						txtError += "- Veuillez indiquer Votre article_typeid <br/>"; 
						$("#article_typeid").css('border-color', 'red');
					} else {
						$("#article_typeid").css('border-color', '#E3E1E2');
					}
					
					
					//alert(txtError);
					if(txtError == "") {
						$("#form_editcategorie").submit();
					}
					
				
				
			});
		});
</script>
      <br>
     
    <div id="divMesAnnonces" class="content" align="center">
                <a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>">Retour au menu</a><br/><br/>
                <a style = "color:black;" href = "<?php echo site_url("admin/categories_article" ) ; ?>">Retour à la liste des catégories</a><br/>
                <br>
                <p><div class="H1-C">Editer catégorie article</div></p>
				
					
<form action="<?php echo site_url("admin/categories_article/savecategorie"); ?>" method="post" enctype="multipart/form-data" id="form_editcategorie" name="form_editcategorie">
<table width="400" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Id : </td>
    <td>
    <input name="idrubrique_editcategorie" id="idrubrique_editcategorie" type="hidden" value="<?php if (isset($oCategorie) && $oCategorie->article_categid != 0)  echo $oCategorie->article_categid; else echo 0;?>">
	<?php if (isset($oCategorie) && $oCategorie->article_categid != 0)  echo $oCategorie->article_categid; else echo 0;?>
    </td>
  </tr>
  <tr>
    <td>Type Article : </td>
    <td>
    	<select name="article_typeid" id="article_typeid">
            <?php if(sizeof($oTypeArticle)) { ?>
                <?php foreach($oTypeArticle as $objTypeArticle) { ?>
                    <option <?php if(isset($oCategorie->article_typeid) && $objTypeArticle->article_typeid == $oCategorie->article_typeid) echo 'selected="selected"';?> value="<?php echo $objTypeArticle->article_typeid; ?>"><?php echo $objTypeArticle->article_type; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
    </td>
  </tr>
  <tr>
    <td>Catégorie : </td>
    <td><input name="inputcateg_editcategorie" id="inputcateg_editcategorie" type="text" style="width:300px;" value="<?php if (isset($oCategorie) && $oCategorie->category != NULL)  echo $oCategorie->category;?>"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
    <a style = "color:black;" href = "<?php echo site_url("admin/categories_article" ) ; ?>">
    <input name="annuler_editcategorie" id="annuler_editcategorie" type="button" value="Annuler"></a>&nbsp;
    <input name="btnsubmit_editcategorie" id="btnsubmit_editcategorie" type="button" value="Enregistrer">
    </td>
  </tr>
</table>
</form>
                    
		  
       
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>