<?php $data["zTitle"] = 'Import Datatourisme'; ?>
<html>
<head>
    <title>Gestion de revue de presse</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('application/views/sortez_vsv/css/revue.css') ?>">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url('application/views/sortez_vsv/bootstrap/css/bootstrap.css') ?>">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>
<body onload="activ_api()">
<div class="container">
    <div class="row">
        <div class="col-12">
        <form name="form" id="form" action="<?php echo site_url("admin/Webscrapping/save_api_data") ?>" method="post">
        <div class="linkfield">
        <label class="labellink">Saisir l' API</label>
        <input class="form-control" required type="text" name="api" id="api" value="">
        </div>
        <div class="linkfield">
        <label class="labellink">Saisir le token</label>
        <input class="form-control" required type="text" name="token" id="token" value="">
        </div>
        <div class="linkfield">
            <label for="postalCode">Id Commercant : </label>
            <input class="form-control" type="text" id="idcom" name="idcom">
        </div>
        <div class="col-12">
        <button type="submit" class="btn btn-success d-block m-auto" >Valider</button>
        </div>
    </form>
            </div>
    </div>
    <div class="row">
        <div class="col-12 m-4">
        <button onclick="get_all()" class="btn btn-success d-block m-auto" >Recuperer tout</button>
        </div>
    </div>
    <div class="row">
    <table class="table table-bordered table-responsive">
        <thead>
        <tr>
            <th>Id</th>
            <th>Commercant</th>
            <th>api</th>
            <th>token</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
            <?php $iiii = 1;?>
            <?php foreach( $allapi as $api){ ?>
                <tr>
                    <td><?php echo $api->id; ?></td>
                    <td style="width:100%">
                    <?php 
                    $thissss= &get_instance();
                    $thissss->load->model('mdlcommercant');
                    $nomcom = $thissss->mdlcommercant->infoCommercant($api->idcom)->NomSociete;
                    echo $nomcom;
                    ?>
                    </td>
                    <td><?php echo $api->api; ?></td>
                    <td><?php echo $api->token; ?></td>
                    <input type='hidden' name="id_recover" value='<?php echo json_encode($allapi); ?>' id="recover_id">
                    <td><a id="action_<?php echo $api->id;?>" href="<?php echo site_url('admin/Webscrapping/delete_api/').$api->id; ?>"><button>Supprimer</button></a></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    </div>
</div>
<input type="hidden" value="0" id="to_recuper">
<?php $this->load->view("admin/includes/vwFooter2013"); ?>
</body>
<style type="text/css">
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    /* Hide default HTML checkbox */
    .switch input {
        display: none;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: green;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }

    .titre {
        border: double;
        color: blue;
    }

    #pwidget {
        background-color: #31B0D5;
        width: 110px;
        padding: 2px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        border: 1px solid #31B0D5;
        text-align: center;
    }

    #progressbar {
        width: 100%;
        padding: 1px;
        background-color: white;
        border: 1px solid black;
        height: 28px;
        text-align: center;
    }

    #indicator {
        width: 0%;
        height: 28px;
        margin: 0;
        text-align: center;
        background-color: #449D44;
    }

    #progressnum {
        text-align: center;
        width: 100px;

    }

    body {
        background-image: url("http://localhost/sortez7/application/resources/privicarte/images/bg_all.jpg") !important;
        background-attachment: fixed;
    }

    @media screen and (min-width: 992px) {
        .container {
            background-color: white;
            height: auto;
            width: 800px;
        }
    }
</style>
<script type="text/javascript">

function get_all(first =0,is_runned =0){
    all_brute = $('#recover_id').val();
    all = JSON.parse(all_brute);
    last = all.length;
    if (first < last){
    id = all[first].id;
    $('#action_'+id).html("Lancement...<img src='<?php echo base_url('assets/images/load_recup.gif'); ?>'>");
    if(is_runned == 0){
        jQuery.ajax({
        type: "POST",
        url: "<?php echo site_url("admin/Webscrapping/make_run/"); ?>"+id,
        success: function (data) {
            console.log('run'+data);
            if(data == 'ok'){
                is_runned =1;
            }else{
                is_runned =3;
            }
            get_all(first,is_runned);
        },
        error: function (data) {
            first++;
            $("#to_recuper").val(first);
            alert('error make_run');
            get_all(first,is_runned)
        }
    });
    }else if(is_runned == 1){
        jQuery.ajax({
            type: "POST",
            url: "<?php echo site_url("admin/Webscrapping/check_run/"); ?>"+id,
            success: function (isfinished) {
                console.log('is'+isfinished);
                if(isfinished == 'ok'){
                    $('#action_'+id).html("Recuperation...<img src='<?php echo base_url('assets/images/load_lanc.gif'); ?>'>");
                    jQuery.ajax({
                        type: "POST",
                        url: "<?php echo site_url("admin/Webscrapping/save_all/"); ?>"+id,
                        success: function (saved) {
                            console.log('saved'+id+saved);
                            if(saved == 'ok'){
                                $('#action_'+id).html("<span style='color:green'>Reuperé</span>");
                                first ++;
                                $("#to_recuper").val(first);
                                is_runned = 0;
                                get_all(first,is_runned)
                            }else{
                                $('#action_'+id).html("<span style='color:red'>Erreur save</span>");
                                first ++;
                                $("#to_recuper").val(first);
                                is_runned = 0;
                                get_all(first,is_runned);
                            }
                        },
                        error: function (saved) {
                            $('#action_'+id).html("<span style='color:red'>Erreur script</span>");
                            first ++;
                            $("#to_recuper").val(first);
                            is_runned = 0;
                            get_all(first,is_runned);
                        }
                    });                    
                }else{
                    $('#action_'+id).html("Lancement...<img src='<?php echo base_url('assets/images/load_recup.gif'); ?>'>");
                    setTimeout(function(first,is_runned){ 
                    is_runned = 1;
                    first = $("#to_recuper").val();
                    get_all(first,1);
                    }, (1000*60));
                }
            },
            error: function (isfinished) {
                $('#action_'+id).html("<span style='color:red'>Erreur de lancement</span>");
            }
        });
    }
}else{
    alert('recuperation terminé');
}
}
</script>
</html>