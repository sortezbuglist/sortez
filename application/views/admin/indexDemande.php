<?php $data["zTitle"] = 'Import Datatourisme'; ?>
<html>
<head>
    <title>Liste de demandes</title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link  href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.min.css" rel="stylesheet">
   <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('application/views/sortez_vsv/css/revue.css') ?>">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url('application/views/sortez_vsv/bootstrap/css/bootstrap.css') ?>">
    <style type="text/css">
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    /* Hide default HTML checkbox */
    .switch input {
        display: none;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: green;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }

    .titre {
        border: double;
        color: blue;
    }

    #pwidget {
        background-color: #31B0D5;
        width: 110px;
        padding: 2px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        border: 1px solid #31B0D5;
        text-align: center;
    }

    #progressbar {
        width: 100%;
        padding: 1px;
        background-color: white;
        border: 1px solid black;
        height: 28px;
        text-align: center;
    }

    #indicator {
        width: 0%;
        height: 28px;
        margin: 0;
        text-align: center;
        background-color: #449D44;
    }

    #progressnum {
        text-align: center;
        width: 100px;

    }

    body {
        background-image: url("<?php echo base_url() ?>application/resources/privicarte/images/bg_all.jpg") !important;
        background-attachment: fixed;
    }

    @media screen and (min-width: 992px) {
        .container {
            background-color: white;
            
        }
    }
</style>
</head>
<body>
    <div class="container">
    <div class="row pt-4">
    <div class="col-12">
    <h3 class="text-center">Liste des demandes de nom de domaine</h3>
    </div>
        <div class="row">
            <div class="col-12 pl-4 table-responsive">
            <div class='text-center pb-4 pt-4'><a class="btn btn-info" href="<?php echo site_url('admin') ?>">Retour menu</a></div>
            <table class="table table-bordered">
  <thead class="thead-dark">
    <tr>
      <th scope="col">id</th>
      <th scope="col">Commercant</th>
      <th scope="col">Mail</th>
      <th scope="col">Message</th>
      <th scope="col">Nom de domaine</th>
      <th scope="col">Etat</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  <?php if(isset($demande) AND !empty($demande)){ ?>
  <?php foreach($demande as $odem){ ?>
        <script type="text/javascript">      
        function recap_validation(){
        var idCom=$("#idCom<?php echo $odem->id ?>").val();
        var nom_domain=$("#domain_name<?php echo $odem->id ?>").val();
        var sitekey=  $("#domain_siteKey<?php echo $odem->id ?>").val();
        var  secretkey=  $("#domain_secretKey<?php echo $odem->id ?>").val();
        $('#command_button').html('Envoie en cours ...');
        
        data='idCom='+idCom +'&nom_domain=www.'+nom_domain +'&sitekey='+sitekey+'&secretkey=' +secretkey;
        //alert(data);
        jQuery.ajax({
                url: "<?php echo site_url('front/professionnels/recap_validation');?>",
                dataType: 'text',
                type: 'POST',
                data: data,
                success: function (data) {
                   if (data == 'ok' ){
                     $('#command_button').html('reCAPTCHA enregistré');
                     $('#etats_<?php echo $odem->id;?>').html('validé');
                    // $("#valid_demande<?php //echo $odem->id ?>" ).html('validé');
                   }else{
                      alert('Une erreur s\'est produite');
                   }
                },
                error: function (data) {
                    // console.log(data);
                    alert('Une erreur s\'est produite');
                }
            });
        };
    </script>
    <tr>
      <td><?php echo $odem->id ?></td>
      <td>
      <?php 
      $thiss = &get_instance();
      $this->load->model('mdldemande');
      $nomcom = $this->mdldemande->getComNameById($odem->idCom);
       if(isset($nomcom) AND $nomcom !='' AND $nomcom != null){
           echo $nomcom;
       }
        ?>
      </td>
      <td><?php echo $odem->mailCom ?></td>
      <td><?php echo $odem->message ?></td>
      <td><?php echo $odem->nomDomaine ?></td>
      <td id="etats_<?php echo $odem->id; ?>"><?php if($odem->etatDemande == 0){
          echo 'Pas encore validé';
      }else{
          echo 'Validé';
      }
      
      ?></td>
      <td>
      <input id="etat_<?php echo $odem->id ?>" type="hidden" value="<?php echo $odem->id ?>">
      <a data-fancybox="" data-animation-duration="500" data-src="#registre_recap<?php echo $odem->id ?>" id="valid_demande<?php echo $odem->id ?>" class="btn btn-success">Valider</a>
      <div class="w-50" id="registre_recap<?php echo $odem->id ?>" style="display: none;width: 50%!important;">
                               <div class="row text-center">
                                  <div class="col-lg-12">
                                    <h5 style="font-weight: bold"> MA DEMANDE DE NOM DE DOMAINE </h5>
                   <p>Pour effectuer votre demande de nom de domaine merci de remplir et valider le formulaire ci-dessous</p>
                <span style="color: red;text-decoration: underline">Attention :</span> Cette demande ne constitue pas une réservation ferme, notre service réception entrera en contact avec vous dans les meilleurs délais pour valider votre réservation.
                Pour toute réservation dans moins de 24 heures, veuillez nous contacter par téléphone.</p>
                <label class="label" for="sender"></label>
                <input type="hidden" name="Societe[idCom]" id="idCom<?php echo $odem->id ?>"
                                          value="<?php if (isset($odem->idCom)) echo htmlspecialchars($odem->idCom); ?>"
                                          />
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                           <tr>
                               <td class="label" style="padding-bottom:10px;"> nom de domaine</td>
                               <td><input type="url" name="Societe[domain_name]" placeholder="mon_site.fr" id="domain_name<?php echo $odem->id ?>"
                                          value="<?php if (isset($odem->nomDomaine)) echo htmlspecialchars($odem->nomDomaine); ?>"
                                          class="form-control stl_long_input_platinum"/></td>
                           </tr>
                           <tr>
                               <td class="label" style="padding-bottom:10px;">siteKey du domaine</td>
                               <td><input type="text" name="Societe[domain_siteKey]" id="domain_siteKey<?php echo $odem->id ?>"
    
                                          class="form-control stl_long_input_platinum"/></td>
                           </tr>
                           <tr>
                               <td class="label" style="padding-bottom:10px;">secretKey du domaine</td>
                               <td><input type="text" name="Societe[domain_secretKey]" id="domain_secretKey<?php echo $odem->id ?>"
                                         
                                          class="form-control stl_long_input_platinum"/></td>
                           </tr>
                           <tr>
                               <td class="label" style="padding-bottom:10px;"><center></center></td>
                               <td style="color:red;">* sitekey / secretkey reCAPTCHA version 2 </td>
                           </tr>
                       </table>
                             <div id="command_button" class="i_verify" onclick="recap_validation()" style=" margin-top: 20px; text-align: center; margin-left: 200px; border: 1px solid #3653A3;border-radius: 5px; background-color: #3653A3; width: 250px;;text-decoration-color:white;text-transform: uppercase;color: white;font-weight: bold;text-align: center;">
                                  valider la commande
                            </div>
                     </div>
                     
                          </div>
                        </div>

      </td>

    </tr>
    <?php } ?>
    <?php } ?>
  </tbody>
</table>
            </div>
    </div>




</body>
</html>
