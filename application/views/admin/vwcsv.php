<?php $data['empty'] = null;  ?>
<?php $data["zTitle"] = 'Csv' ?>
<?php $this->load->view("privicarte/includes/main_header", $data); ?>
<style type="text/css">
    .btn_news:hover{
        background-color: deeppink;
    }
    .btn_news{
        background-color: blue;
    }
    a:hover{
        text-decoration: none;
    }
    body{
        background-image: url("https://www.sortez.org/wpimages/wpe3440087_06.jpg");
        margin: auto;
    }
</style>

<body>
<div class="container" style="background-color: #f6f6f6">
    <div id="" class="content" style="">
       <div class="row">
           <div class="col-12 text-center" >
               <?php if (isset($suucees) AND isset($echec)){ ?>
                   <span style="color: green"> Réussi: <?php echo $suucees;?></span><br>
               <span style="color: red">Echec: <?php echo $echec;?></span><br>
               <?php }else{ ?>
                    L'importation a échoué
               <?php } ?>
           </div>
           <div class="col-12 text-center p-4" style="font-size: 25px;text-decoration: underline">Importer les listes de vos clients depuis un CSV</div>
       </div>
        <form action="<?php echo site_url('admin/csv/verify_csv_file/'); ?>" method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-sm-12 text-center p-4">
                       <div class="text-center m-auto"><input class="m-auto" type="file" name="file" id="file" /></div>
            </div>
                <div class="col-sm-12 p-4">
                    <div class="text-center"><input class="btn btn-info" type="submit" name="submit"/></div>
                </div>
        </div>
        </form>
    </div>

</div>
</body>
