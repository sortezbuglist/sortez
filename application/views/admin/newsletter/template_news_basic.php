<div style="margin-left: auto; margin-right: auto;">
    <table border="0" cellpadding="0" style="color: black; margin-left: auto; margin-right: auto; table-layout: fixed; width: 980px;">
        <tbody>
            <tr>
                <td>
                    <div style="text-align: center;">
                        <a href="https://www.sortez.org/"><img alt="Magazine Sortez" src="https://www.sortez.org/application/resources/sortez/images/logo_sortez.png" style="border:none; height:auto; text-align:center; width:400px" ></a>
                        <div style="text-align:center; font-family: futura-lt-w01-book, futura-lt-w05-book, sans-serif; font-size: 18px;">Initiateur d&#39;id&eacute;es de sorties et de loisirs</div>
                        <img alt="Annuaire - Magazine Sortez" src="https://www.sortez.org/application/resources/sortez/images/banniere_news.png" style="border:none; text-align:center; width:100%">
                    </div>
                    
                    <div style="margin-left: 30px; margin-right: 30px; text-align: center;">
                        <img src="https://static.wixstatic.com/media/5fa146_ac7ca7a8011c4c0f8dbcfd4cd2d31e0b~mv2.png/v1/fill/w_600,h_305,al_c,usm_0.66_1.00_0.01,enc_auto/basique%20ordi.png" alt="basique ordi.png" style="width:600px;height:305px;object-fit:cover;object-position:50% 50%">
                    </div>
                    <div style="color: rgb(15, 105, 101)">
                        <h6 style="text-align:center; font-size:22px;">
                            <span style="font-size:35px;"><span style="font-style:italic;">
                                <span style="font-family:libre baskerville,serif;">
                                    <span style="color: rgb(15, 105, 101);">::enseigne::</span>
                                </span></span></span><br>

                            <span style="font-size:20px;">
                                <span style="font-style:italic;">
                                    <span style="font-family:libre baskerville,serif;color: rgb(15, 105, 101);">
                                        <span>::Rubrique::</span></span></span></span>
                                        <span style="font-size:20px;">
                                            <span style="font-style:italic;color: rgb(15, 105, 101);">
                                                <span style="font-family:libre baskerville,serif;">
                                                <span class="color_17"> - ::sousrubrique::</span></span></span></span><br>
                                    <span style="font-size:20px;">
                                        <span style="font-style:italic;color: rgb(15, 105, 101);">
                                        <span style="font-family:libre baskerville,serif;">
                                            <span class="color_17">::adresse::<br>
                                        ::ville::</span></span></span></span></h6></div>
                </td>
            </tr>
        </tbody>
    </table>
    <table border="0" cellpadding="0" style="color: black; margin-left: auto; margin-right: auto; table-layout: fixed; width: 980px;text-align: center;">
        <tbody>
            <tr>
                <td>
                    <p style="font-size: 17px; text-align: justify; color: black;">
                            <span style="font-weight:400; font-family: futura-lt-w01-book, futura-lt-w05-book, sans-serif;">Madame, Monsieur,</span>
                        
                    </p>


                    <p style="font-size: 17px; text-align: justify; color: black;">
                            <span style="font-weight: 400; font-family: futura-lt-w01-book, futura-lt-w05-book, sans-serif;">Nous avons référencé <span style="font-size: 25px;">GRATUITEMENT</span> votre établissement sur le site web du Magazine Sortez qui est le premier magazine mensuel offert diffusé sur les Alpes-maritimes et Monaco.</span>
                        
                    </p>

                    <p style="font-size: 17px; text-align: justify; color: black;">
                        <span style="font-weight: 400; font-family: futura-lt-w01-book, futura-lt-w05-book, sans-serif;"> Ce site bénéficie avec son magazine presse d'une audience mensuelle départementale importante de plus de 
                        <span style="font-size: 25px;">250 000</span>
                        lecteurs et internautes.</span> 
                    </p>

                    <p style="font-size: 17px; text-align: justify; color: black;">
                        <span style="font-weight: 400; font-family: futura-lt-w01-book, futura-lt-w05-book, sans-serif;">Votre établissement référencé bénéficie d'un abonnement basique et par conséquent d'une page informative simplifiée.</span>
                    </p>

                    <p style="font-size: 17px; text-align: justify; color: black;">
                        <span style="font-weight: 400; font-family: futura-lt-w01-book, futura-lt-w05-book, sans-serif;">Nous vous invitons à la découvrir.</span>
                    </p>
                </td>
            </tr>

            <tr style="text-align:center;">
                <td><a style="font-weight: normal;
                            font-style: normal;
                            font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;
                            font-size: 17px;
                            line-height: 1em;
                            letter-spacing: 0em;
                            text-align: center;
                            text-decoration:none;
    margin-left: auto;
    margin-right: auto;
    background-color: #014235;
    color: #fff;
    width: 300px;
    height: 50px;" href="https://www.sortez.org/::url::/presentation_commercants">
                                
                                    Accès à votre page informative</a>
                                
                           
                    </td>
                </tr>
                <tr>
                    <td>
                    <p style="font-size: 17px; text-align: justify; color: black;">
                        <span style="font-weight: 400; font-family: futura-lt-w01-book, futura-lt-w05-book, sans-serif;">
                            Si certaines données s'avèrent erronées ou manquantes, vous pouvez <span style="font-size: 25px;">GRATUITEMENT</span> les intégrer d'un simple clic sur un formulaire intuitif (coordonnées, courriel, téléphone,site web, texte de présentation,image, lien de réservation pour un rendez vous...).
                        </span>
                    </p>
                    <p style="font-size: 17px; text-align: justify; color: black;">
                        <span style="font-weight: 400; font-family: futura-lt-w01-book, futura-lt-w05-book, sans-serif;">
                            Il suffira à partir de votre page d'y accéder en  cliquant sur le bouton <span style="font-size: 25px;">"Je modifie ma page"</span>.
                        </span>
                    </p>
                    
                    <p style="font-size: 17px; text-align: justify; color: black;">
                        <span style="font-weight: 400; font-family: futura-lt-w01-book, futura-lt-w05-book, sans-serif;">
                            Par la suite, si vous le désirez, vous pouvez améliorer votre page basique avec les abonnements Premium ou Platinium et  bénéficier d'outils marketings innovants utilisés habituellement par les grandes enseignes.
                        </span>
                    </p>
                    
                    <p style="font-size: 17px; text-align: justify; color: black;">
                        <span style="font-weight: 400; font-family: futura-lt-w01-book, futura-lt-w05-book, sans-serif;">
                            L'équipe du Magazine Sortez est à votre disposition pour répondre à toutes vos questions...
                        </span>
                    </p>
                
                    <p style="font-size: 17px; text-align: justify; color: black;">
                        <span style="font-weight: 400; font-family: futura-lt-w01-book, futura-lt-w05-book, sans-serif;">
                            A bientôt...
                        </span>
                    </p>
                    </td>
                </tr>
                <tr>
                    <td>
                    
                              
                                    <a style="font-weight: normal;
                            font-style: normal;
                            font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;
                            font-size: 17px;
                            line-height: 1em;
                            letter-spacing: 0em;
                            text-align: center;
                            text-decoration:none;
    margin-left: auto;
    margin-right: auto;
    background-color: #014235;
    color: #fff;
    width: 300px;
    height: 50px;" href="https://www.magazine-sortez.org/nous-contacter">Nous contacter</a>
                                     </td>
                </tr>
                          <tr>
                            <td>
                    <div style=" text-align: center;">
                        <h3 class="font_3" style="text-align:center;">
                            <span style="font-size:50px; color: #35685e;">Qui sommes nous ?</span>
                        </h3>
                    
                    <span style="margin-left: 30px; margin-right: 30px;">
                        <img src="https://static.wixstatic.com/media/5fa146_52fe07662de5485987dde9e60c8bb9e5~mv2.jpg/v1/fill/w_600,h_456,al_c,q_80,usm_0.66_1.00_0.01,enc_auto/sortez-site.jpg" alt="sortez-site.jpg" style="width:600px;height:456px;object-fit:cover;object-position:50% 50%">
                    </span>
                    </div>
                    <div style="font-family: futura-lt-w01-book, futura-lt-w05-book, sans-serif; font-size: 16px;">
                        <ul>
                            <li> 
                                <p>
                                    Sortez.org est un média départemental qui propose une solution dynamique et innovante sur le print et le web pour stimuler l’information événementielle et commerciale sur les Alpes-Maritimes et Monaco ;
                                </p>
                            </li>
                        </ul>
                        <ul>
                            <li style="line-height: normal;">
                                <p>
                                    Tous les mois nous diffusons notre magazine presse à 50 000 exemplaires, la diffusion s'effectue par l'intermédiaire de 500 dépositaires (mairies, offices de tourisme, adresses culturelles, commerces de proximité) ;
                                </p>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <p>
                                    Tous les mois, nous touchons plus de 250 000 lecteurs et internautes ;
                                </p>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <p>
                                    Le magazine Sortez est le 1er magazine mensuel gratuit diffusé sur les Alpes-Maritimes.
                                </p>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    
                               
                                    <a style="font-weight: normal;
                            font-style: normal;
                            font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;
                            font-size: 17px;
                            line-height: 1em;
                            letter-spacing: 0em;
                            text-align: center;
                            text-decoration:none;
    margin-left: auto;
    margin-right: auto;
    background-color: #014235;
    color: #fff;
    width: 300px;
    height: 50px;" href="https://www.magazine-sortez.org/">Accès à notre site web</a>
                                
                             </td>
                </tr>
                <tr>
                    <td>

                    <div class="_2Hij5"><p class="font_8" style="font-size:18px; line-height:1.3em; text-align:center;"><span style="color:#000000;"><span style="text-decoration:underline;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><a href="<?php echo site_url("front/newsletter/unsubscribe/::email::"); ?>">Cliquez sur ce lien pour vous désabonner</a></span></span></span></p>
                        
                        <p class="font_8" style="font-size:18px; line-height:1.3em; text-align:center;"><span style="font-size:18px;"><span style="font-family:futura-lt-w01-book,sans-serif;"><span style="letter-spacing:0.03em;"><span style="color:#000000;">PRIVICONCEPT SAS</span></span></span></span></p>
                        
                        <p class="font_8" style="font-size:18px; line-height:1.3em; text-align:center;"><span style="font-size:18px;"><span style="font-family:futura-lt-w01-book,sans-serif;"><span style="letter-spacing:0.03em;"><span style="color:#000000;">Le Magazine Sortez</span></span></span></span><br>
                        <span style="font-size:16px;"><span style="font-family:futura-lt-w01-book,sans-serif;"><span style="letter-spacing:0.03em;"><span style="color:#000000;">427, chemin de Vosgelade, le Mas Raoum,</span></span></span></span></p>
                        
                        <p class="font_8" style="font-size:18px; line-height:1.3em; text-align:center;"><span style="font-size:16px;"><span style="font-family:futura-lt-w01-book,sans-serif;"><span style="letter-spacing:0.03em;"><span style="color:#000000;">06140 Vence<br>
                        WWW.MAGAZINE-SORTEZ.ORG</span></span></span></span></p>
                        
                        <p class="font_8" style="font-size:18px; line-height:1.3em; text-align:center;"><span style="font-size:16px;"><span style="font-family:futura-lt-w01-book,sans-serif;"><span style="letter-spacing:0.03em;"><span style="color:#000000;">TÉL. 0.72.05.59.35</span></span></span></span></p>
                        
                        <p class="font_8" style="font-size:14px; line-height:1.3em; text-align:center;"><span style="font-size:14px;"><span style="color:#605e5e;"><span style="font-style:normal;"><span style="font-weight:400;"><span style="font-family:futura-lt-w01-book,sans-serif;">Siret : 820 043 693 00010 - Code NAF : 6201Z</span></span></span></span></span></p>
                        
                        </div>
                </td>
            </tr>
        </tbody>
    </table>
    
                        
    
</div>