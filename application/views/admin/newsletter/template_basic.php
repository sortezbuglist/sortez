<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!doctype html>
<html lang="en">
  
  <body>
<div class="" style="width: 100%; max-width: 746px; margin: auto;    text-align: center;">
        <img class="pointer_cursor direct_btn_image" src="<?php echo base_url('assets/soutenons/visuels_news.webp')?>" 
        style="width: 746px;
                height: 520px;
                object-fit: cover;
                object-position: 50% 50%;">
        
<div>
    <p style="color: #292929;font-weight: bold;font-size: 25px;letter-spacing: 0.03em;font-family: avenir-lt-w01_85-heavy1475544,avenir-lt-w05_85-heavy,sans-serif; text-align: center;" >Monsieur, Madame<br><br><br>
        Votre établissement a été référencé <br>

    <p style="color: #E80EAE;font-weight:bold; margin-top:2%; font-size:35px;font-family: avenir-lt-w01_85-heavy1475544,avenir-lt-w05_85-heavy,sans-serif; text-align: center;">GRATUITEMENT</p>

    <p style="color:#292929; font-weight: bold; margin-top:4%;vertical-align: baseline;letter-spacing: 0.03em; font-size: 25px;font-family: avenir-lt-w01_85-heavy1475544,avenir-lt-w05_85-heavy,sans-serif; text-align: center;">Sur le site du Magazine Sortez <br>
        qui bénéficie tous les mois d'une audience départementale <br>
        avec plus de 250 000 lecteurs et internautes</p>
</div>
<div>
    <center>
        <span style="color:#E80EAE; font-weight: bold; margin-top:10%; font-size: 20px;font-family: avenir-lt-w01_85-heavy1475544,avenir-lt-w05_85-heavy,sans-serif;">Nom de votre enseigne : ::enseigne:: </span><br>
        <span style="color:#E80EAE; font-weight: bold; font-size: 20px;font-family: avenir-lt-w01_85-heavy1475544,avenir-lt-w05_85-heavy,sans-serif;text-align: center;">Ville : ::ville:: </span>
    </center>
</div>
        
<div>
        <p style="color:#292929; font-weight: bold; margin-top:4%; font-size: 20px;font-family: avenir-lt-w01_85-heavy1475544,avenir-lt-w05_85-heavy,sans-serif;letter-spacing: 0.03em;  text-align: center;">
        Nous vous invitons à consulter votre page informative <br>
            qui s'adapte à tous les écrans <br>
            et bénéficie d'un module de traduction Google</p>

       
                    <p style="color:#292929; font-weight: bold;margin-top:4%; font-size: 20px; letter-spacing: 0.03em;font-family: avenir-lt-w01_85-heavy1475544,avenir-lt-w05_85-heavy,sans-serif;  text-align: center;">
                    Si certaines données sont erronées ou manquantes <br>
            (texte de présentation, image...), vous pouvez en moins de 10 minutes <br>
            les modifier ou les ajouter à l'aide d'un formulaire intuitif.</p>

        <span style="color:#000000; color:#292929; letter-spacing: 0.03em; font-weight: bold;margin-top:10%; font-size: 20px;font-family: avenir-lt-w01_85-heavy1475544,avenir-lt-w05_85-heavy,sans-serif;  text-align: center;">
         <center>
         Pour accéder à ce dernier, veuillez vous identifier <br>
                en utilisant les codes suivants.
         </center> </span>


         <center>
         <div style="color:#E80EAE; font-weight: bold; font-size: 20px;font-family: avenir-lt-w01_85-heavy1475544,avenir-lt-w05_85-heavy,sans-serif;text-align: center;" >
            Votre identifiant provisoire : ::login::<br>
            Votre mot de passe provisoire : ::pwd::
        </div>
         </center>
        
</div>

<div class="row" style=" margin-top:4%;">

        <div class="flex" style="margin:auto;text-align: center;" >
            <img class="pointer_cursor direct_btn_image" src="<?php echo base_url('/assets/soutenons/acces_logo.png') ?> " style="">
            <img class="pointer_cursor direct_btn_image" src="<?php echo base_url('/assets/soutenons/acces_logo.png') ?> " style="">
        </div>   
        
</div>


<div class="" style="width: 100%; max-width: 746px; margin: auto; margin-top:10%;    text-align: center;">
        <img  src="<?php echo base_url('/assets/soutenons/banniere_news.webp')?>" 
        style="width: 746px;
                height: 300px;
                object-fit: cover;
                object-position: 50% 50%;">
<div>

<div>
    <p style="color:#292929; font-weight: bold; text-align: center;vertical-align: baseline;letter-spacing: 0.03em; font-size: 20px;
    font-family: avenir-lt-w01_85-heavy1475544,avenir-lt-w05_85-heavy,sans-serif; text-align: center;">
        Par la suite, si vous le désirez,<br>
        vous pouvez développer votre page <br>
        en souscrivant à un abonnement complémentaire <br>
        et accédez à des outils marketings innovants et à moindre coût <br>
        utilisés habituellement par les grandes enseignes.</p>
</div>

<a href="https://www.magazine-sortez.org/nous-contacter"><div style="margin: auto; width: 500px; " >
    <div  class="" style="    font-weight: 700;
    letter-spacing: 0.2em;
    font-family: avenir-lt-w01_85-heavy1475544,sans-serif;
    font-size: 20px;
    color: #FFFFFF;
    background: #2B7C74;
    width: 483px;
    height: 59px;
    text-align: center;
    margin-bottom: 20px;" >Nous contacter par courriel</div></a>
<div  class="" style="    font-weight: 700;
    letter-spacing: 0.2em;
    font-family: avenir-lt-w01_85-heavy1475544,sans-serif;
    font-size: 20px;
    color: #FFFFFF;
    background: #F60419;
    width: 483px;
    height: 59px;
    text-align: center;" >Demande de désincription</div>
</div>

<div>
    <p style="color:#292929; font-weight: bold; margin-top:4%;vertical-align: baseline;letter-spacing: 0.03em; font-size: 20px;
    font-family: avenir-lt-w01_85-heavy1475544,avenir-lt-w05_85-heavy,sans-serif; text-align: center;">
    Si vous ne voulez pas bénéficier de ce référencement,<br>
vous pouvez demander son annulation en cliquant sur le bouton ci-dessus.
    </p>
</div>
<div class="" id="footer_section_copyright" style="padding-right:15px;height: auto!important">
    <div style="background-color:#fff;">
        <div class="row text-center">

        
            <div class="col-lg-12 pt-4 mb-5" style="text-align: center;">
                <p class="Corps P-3 mb-0"><span style="font-size:25px;letter-spacing:0.03em;">PRIVICONCEPT SAS</span></p>
                <p class="Corps P-3 mb-0"><span style="font-size:16px; line-height:1.3em; text-align:center;font-style:italic;font-family:futura-lt-w01-book,sans-serif;">L'innovation au service de la vie</span></p>
                <p class="Corps P-3 mb-0"><span style="font-size:18px; line-height:1em; text-align:center;font-family:futura-lt-w01-book,sans-serif;">magazine-sotrez.org</span></p>
                <p class="Corps P-3 mb-0"><span style="font-size:18px; line-height:1em; text-align:center;font-family:futura-lt-w01-book,sans-serif;">vivreseville.fr - soutenonslecommercelocal.fr</span></p>

                <p class="Corps P-3 mb-0"><span style="font-size:18px;font-family:futura-lt-w01-book,sans-serif;">427, chemin de Vosgelade, le Mas Raoum, 06140 Vence </span></p>
                <p class="Corps P-4 mb-0"><span style="font-size:18px; line-height:1em; text-align:center;font-family:futura-lt-w01-book,sans-serif;">TÉL. 06 72 05 59 35</span></p>
                <p class="Corps P-6 mb-0"><span style="font-size:15px; line-height:1em; text-align:center;font-family:futura-lt-w01-book,sans-serif;">Siret 820 043 693 00010 <br> Code NAF 6201Z</span></p>
                <p class="Corps P-6 mb-0">
                    <a href="#" class="futura_sous_title_3" style="text-decoration: none; color: #E80EAE;font-size:15px;">Conditions générales - Mentions légales</a>
                </p>
            </div>
        </div>
    </div>
</div>




    
  </body>
</html>