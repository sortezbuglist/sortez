<?php $data["zTitle"] = 'Modification'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

    <script>
        function submit_btnSinscrire() {
            var txtError = "";
            var txtMail = $("#txtMail").val();
            if (txtMail == "") {
                txtError += "- Veuillez indiquer l'adresse email !";
            }
            $("#divErrortxtInscription").html(txtError);
            //alert(txtError);
            if (txtError == "") {
                $("#frmModificationParticulier").submit();
            }
        }
    </script>

    <script type="text/javascript">
        function return_to_liste_user() {
            document.location = "<?php echo site_url(); ?>admin/newsletters_mails/liste/";
        }
    </script>

    <div id="divInscriptionParticulier" class="content" align="center">
        <h1>Modification email prospect</h1>
        <form name="frmModificationParticulier" id="frmModificationParticulier"
              action="<?php echo site_url("admin/newsletters_mails/fiche_modifier"); ?>" method="POST"
              enctype="multipart/form-data">
                <table width="75%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <label>Adresse Email : </label>
                        </td>
                        <td>
                            <input type="text" name="Prostect[mail]" class="form-control" id="txtMail"
                                   value="<?php if (!empty($objUser->mail)) echo($objUser->mail); ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Nom : </label>
                        </td>
                        <td>
                            <input class="form-control" type="text" name="Prostect[name]" id="txtNom"
                                   value="<?php if (!empty($objUser->name)) echo htmlspecialchars(stripcslashes($objUser->name)); ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_part_form">
                            <label>Departement : </label></td>
                        <td class="td_part_form">
                            <select name="Prostect[departement_code]" id="departement_id"
                                    class="form-control"
                                    style="width:273px;">
                                <option value="0">-- Choisir --</option>
                                <?php if (sizeof($colDepartement)) { ?>
                                    <?php foreach ($colDepartement as $objDepartement) { ?>
                                        <option
                                            <?php if (isset($objUser->departement_code) && $objDepartement->departement_id == $objUser->departement_code) { ?>selected="selected"<?php } ?>
                                            value="<?php echo $objDepartement->departement_id; ?>"><?php echo $objDepartement->departement_nom; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <?php if (!empty($objUser->id) && $objUser->id != 0) { ?>
                                <input type="hidden" name="Prostect[id]" id="txtIdUser"
                                       value="<?php if (!empty($objUser->id)) echo $objUser->id; else echo "0"; ?>"/>
                            <?php } ?>
                        </td>
                        <td align="left">
                            <div style="padding-top: 30px;">
                                <input type="button" onclick="javascript:submit_btnSinscrire();"
                                       id="btnSinscrire" name="btnSinscrire" value="Enregistrer"
                                       class="btn btn-success"/>&nbsp;
                                <input
                                    type="button" value="Annuler" onclick="javascript:return_to_liste_user();"
                                    class="btn btn-danger"/>
                            </div>
                        </td>
                    </tr>
                </table>
            </form>
    </div>


    <div id="divErrortxtInscription"
         style="padding:0px 0px 0px 2px; font-family: Arial; font-size: 12px; color:#F00; margin-top:20px; text-align:center;"></div>

<?php $this->load->view("admin/includes/vwFooter2013"); ?>