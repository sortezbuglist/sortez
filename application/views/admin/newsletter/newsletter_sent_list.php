<?php
$CI =& get_instance();
$CI->load->model("newsletter");
$toNewsletter = $CI->newsletter->getAll();
?>

<div id="div_list_sent_container">
    <h2>Historique des envois</h2>
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Objet</th>
            <th>Categorie</th>
            <th>Date d'envoi</th>
            <th>Etat d'envoi</th>
            <th>Destinataire</th>
            <th>envoyé</th>
            <th>non-envoyé</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($toNewsletter as $item_nw) { ?>
            <?php
            if ($item_nw->type == '1') $type_destination = "Envoyé à une adresse";
            else if ($item_nw->type == '2') $type_destination = "Envoyé aux emails newsletter";
            else if ($item_nw->type == '3') $type_destination = "Envoyé aux emails consommateur";
            else $type_destination = "Envoyé à tous";
            if ($item_nw->type == '1' && isset($item_nw->email_receiver)) $type_destination .= "<br/>" . $item_nw->email_receiver;
            if ($item_nw->status == '1') $status_value = "SUCCESS";
            else $status_value = "ERROR";
            ?>
            <tr>
                <td><?php echo $item_nw->id; ?></td>
                <td><?php echo $item_nw->subject; ?></td>
                <td><?php echo $item_nw->category; ?></td>
                <td><?php echo convert_Sqldate_to_Frenchdate($item_nw->date_creation); ?></td>
                <td><?php echo $status_value; ?></td>
                <td><?php echo $type_destination; ?></td>
                <td><?php echo $item_nw->nb_sent; ?></td>
                <td><?php echo $item_nw->nb_blocked; ?></td>
                <td>
                    <button class="btn btn-danger" id="btn_remove_news_old"
                            onclick="javascript:confirm_delete_news(<?php echo $item_nw->id; ?>);">
                        Supprimer
                    </button>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    function confirm_delete_news(iDItem) {
        if (confirm("Voulez-vous supprimer cet utilisateur ?")) {
            jQuery.post(
                "<?php echo site_url("admin/newsletters/delete_sent_item/");?>",
                {iDItem: iDItem},
                function (data) {
                    jQuery("#div_list_sent_container").html(data);
                    var error_list_sent_container_var = '<div class="alert alert-success"><strong>Success !</strong> Supprimé avec succès.</div>';
                    jQuery("#div_list_sent_container").append(error_list_sent_container_var);
                });
        }
    }
</script>