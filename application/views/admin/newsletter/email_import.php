<div class="col-lg-12" style="border: 1px solid;
    margin-bottom: 30px;
    display: table;
    padding: 15px;
    text-align: center;">
    <form id="form_email_csv_import" action="<?php echo site_url('admin/newsletters_mails/liste/') ?>" method="post" enctype="multipart/form-data">
        <div class="col-sm-12" style="text-align: center; color: #4BA24B;">
            <?php if (isset($mssg_import)) echo $mssg_import; ?>
        </div>
        <div class="col-sm-3">
            <input type="file" id="id_email_csv_import" name="id_email_csv_import"/>
            <span><a href="<?php echo site_url('application/resources/csv_sample/test.csv'); ?>" target="_blank"
                     style="display: table;">Exexmple de fichier CSV</a></span>
        </div>
        <div class="col-sm-3">
            <button class="btn btn-success" type="button" id="btn_import_csv_email" onclick="return false;">Importer le fichier CSV</button>
            <div id="div_import_csv_email"></div>
        </div>
    </form>
    <div class="col-sm-3">
        <button class="btn btn-info" id="btn_check_duplicate_email" onclick="return false;">Vérifier / Supprimer les
            doublons
        </button>
        <div id="div_check_duplicate_email"></div>
    </div>
    <div class="col-sm-3">
        <button class="btn btn-danger" id="btn_delete_all_email" onclick="return false;">Supprimer tous les email
        </button>
        <div id="div_delete_all_email"></div>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery("#btn_check_duplicate_email").click(function () {
            jQuery('#div_check_duplicate_email').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            jQuery.post(
                '<?php echo site_url("admin/newsletters_mails/check_duplicate"); ?>',
                function (zReponse2) {
                    jQuery('#div_check_duplicate_email').html(zReponse2+" doublons identifiés - supprimés.");
                });
        });
        jQuery("#btn_import_csv_email").click(function () {
            jQuery('#div_import_csv_email').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var file_csv = jQuery("#id_email_csv_import").val();
            if (file_csv=="") {
                jQuery('#div_import_csv_email').html('<span style="color:#FF0000;">Veuillez indiquer un fichier csv conforme !</span>');
            } else {
                jQuery('#div_import_csv_email').html('');
                jQuery('#form_email_csv_import').submit();
            }
        });
    });
</script>