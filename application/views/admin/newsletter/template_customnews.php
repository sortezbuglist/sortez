<?php if (isset($from_site_link_sortez) && $from_site_link_sortez == '1') { ?>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Sortez - Newsletter</title>
    <meta charset="utf-8"/>
</head>
<body>
<?php } ?>

<?php $path_home_base_url_club = site_url(); ?>
<div
    style="width:600px;
    margin-left:auto;
    margin-right:auto;
    font-family:Verdana, Arial, Helvetica, sans-serif;
    color: #000;">
    <table width="600" border="0" cellspacing="0" cellpadding="0"
           style="background-color:#ffffff; color:#000000;table-layout: fixed;">
        <tr>
            <td>

                <div style="width: 100%; text-align: center;">
                    <a href="<?php echo base_url();?>">
                    <img
                        src="<?php echo base_url() ?>application/resources/sortez/images/logo.png"
                        style="border:none; text-align: center;"
                        alt="Magazine Sortez"/>
                    </a>
                </div>

                <div style="
                text-align: center;
                text-align: center;
                line-height: 45px;
                font-family: Futura Md, sans-serif;
                font-style: normal;
                font-weight: normal;
                background-color: transparent;
                font-variant: normal;
                font-size: 20px;
                vertical-align: 0;">
                    L'essentiel des sorties, des loisirs et de la vie locale !...
                </div>

                <div style="text-transform: uppercase;
                text-align: center;
                text-align: center;
                line-height: 45px;
                font-family: Futura Md, sans-serif;
                font-style: normal;
                font-weight: normal;
                background-color: transparent;
                font-variant: normal;
                font-size: 32px;
                vertical-align: 0;">
                    DÉCOUVREZ LES BONNES ADRESSES !…
                </div>

                <div style="text-transform: uppercase;
                text-align: center;
                text-align: center;
                line-height: 45px;
                font-family: Futura Md, sans-serif;
                font-style: normal;
                font-weight: normal;
                background-color: transparent;
                font-variant: normal;
                font-size: 26px;
                vertical-align: 0;">
                    PLUS DE 1 000 ÉTABLISSEMENTS RÉFÉRENCÉS
                </div>

                <div style="
                text-align: center;
                text-align: center;
                line-height: 45px;
                font-family: Futura Md, sans-serif;
                font-style: normal;
                font-weight: normal;
                background-color: transparent;
                font-variant: normal;
                font-size: 20px;
                vertical-align: 0;">
                    Actualisation du <?php echo convert_Sqldate_to_Frenchdate(date("Y-m-d")); ?>
                </div>


                <table width="100%" border="0" cellspacing="0" cellpadding="0"
                       style="padding: 0 15px;table-layout: fixed;">
                    <tr>
                        <td width="60%">
                            <div style="text-align: justify; padding-right: 20px; margin: 15px 0 30px;">
                                <p>Découvrez nos partenaires et les autres bonnes adresses référencées par notre rédaction dans les domaines du sport, du patrimoine, de la culture, du shopping et de la vie associative.</p>
                            </div>
                        </td>
                    </tr>
                </table>

                <table width="100%" border="0" cellspacing="0" cellpadding="0"
                       style="text-align:center; padding:50px;table-layout: fixed;">
                    <tr>
                        <td><a href="<?php echo site_url('front/annuaire') ?>" target="_blank" style="
                        background-color: #000000;color: #ffffff;font-size: 24px;padding: 15px 30px;-moz-user-select: none;
    border: 1px solid transparent;
    border-radius: 4px;
    cursor: pointer;
    display: inline-block;
    font-weight: 400;
    line-height: 1.42857;
    margin-bottom: 0;
    text-transform: uppercase;
    text-align: center;
    vertical-align: middle;
    text-decoration:none;
    white-space: nowrap;">Voir tous les articles</a></td>
                    </tr>
                </table>

                <table width="100%" border="0" cellspacing="0" cellpadding="0"
                       style="padding:15px; text-align: justify; margin-bottom:300px;table-layout: fixed;">
                    <tr>
                        <td>
                            <div style="text-align: center; margin-bottom: 30px;">
                                <img
                                    src="<?php echo base_url() ?>application/resources/front/images/annuaire_img_newsletter.png"
                                    style="border:none; text-align: center;"
                                    alt="Annuaire - Magazine Sortez"/>
                            </div>
                            <div style="padding: 15px; text-align: justify;">
                                <p>Vous recevez le pr&eacute;sent e-mail parce que l'adresse <a
                                        href="mailto:::email::"
                                        target="_blank">::email::</a> figure dans la
                                    liste de diffusion de Sortez.org
                                </p>
                                <p>Si vous ne souhaitez pas recevoir d'emails de ce type, vous pouvez vous d&eacute;sincrire
                                    en cliquant <a
                                        href="<?php echo site_url("front/newsletter/unsubscribe/::email::"); ?>">ici</a>.
                                </p>
                                <p>Envoy&eacute; par Priviconcept SAS sis au 18, rue des Combattants d'Afrique du Nord -
                                    06200 Nice - Siret 820 043 693 00010 - Code NAF 6201Z.</p>
                                <p>Besoin d'aide? Un commentaire &agrave; nous faire part ? N'h&eacute;sitez
                                    pas &agrave; <a
                                        href="mailto:contact@sortez.org" target="_blank">nous
                                        contacter</a>.</p>
                                <p>Visualiser le mail dans un navigateur <a
                                        href="<?php echo site_url("front/utilisateur/newsletter"); ?>"
                                        target="_blank" style="color: #ffffff;">ICI</a></p>
                            </div>
                        </td>
                    </tr>
                </table>


            </td>
        </tr>
    </table>

</div>


<?php if (isset($from_site_link_sortez) && $from_site_link_sortez == '1') { ?>
</body>
</html>
<?php } ?>

