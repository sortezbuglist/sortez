<?php $data["zTitle"] = 'Administration'; ?>

<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>



<?php 

    function SelectOption($prmValue, $prmCurrent) {

        if ($prmValue == $prmCurrent) {

            echo "selected";

        }

    }

    

    function EchoWithHighLightWords($prmValue, $prmSearchedWords) {

        $out = $prmValue;

        for ($i = 0; $i < sizeof($prmSearchedWords); $i ++) {

            $out = word_limiter(highlight_phrase($out, $prmSearchedWords[$i], "<span style='background: #FFFF00;'>", "</span>"),10) ;

        }

        echo $out;

    }

	

?>

<script type="text/javascript">

    function OnBtnNew_click() {

        document.location = "<?php echo site_url(); ?>admin/newsletters_mails/fiche/0";

    }

	

	function confirm_delete_user(IdRuser){

		if (confirm("Voulez-vous supprimer cet email ?")) {

           document.location="<?php echo site_url("admin/newsletters_mails/fiche_supprimer_user/");?>"+IdRuser;

       }

	}

</script>



    <div style="text-align: center;"><br><div class="H1-C">Liste de Prospects</div><br></div>

<?php $this->load->view('admin/newsletter/email_import',$data);?>

<hr />



<div class="content" align="center">

    <p><a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" value= "retour au menu" id = "btn"/></a></p>



    <form id="frmSearch" method="POST" action="<?php echo site_url(); ?>admin/newsletters_mails/liste/<?php echo $FilterCol ; ?>/<?php echo $FilterValue ; ?>/0">

    <center>

        Rechercher&nbsp;:&nbsp;<input type="text" name="txtSearch" id="txtSearch" value="<?php echo $SearchValue; ?>" />&nbsp;

        <input type="submit" name="btnSearch" class="CommandButton" value="GO" id="btnSearch" />

        <span class="Small">

            | <a href="<?php echo site_url(); ?>admin/newsletters_mails/liste/<?php echo $FilterCol ; ?>/<?php echo $FilterValue ; ?>/0/true">Liste compl&egrave;te</a>

        </span>

    <br>

    </center>

    <p><input type="button" class="CommandButton" value="Nouveau ..." onclick="OnBtnNew_click(); return false;" /></p>

    <p>

        <?php

        echo str_replace("&amp;per_page=","/",$PaginationLinks);

        //echo $PaginationLinks; ?>&nbsp;

        Nombre de lignes :&nbsp;

        <select name="cmbNbLignes" onchange="document.getElementById('btnSearch').click(); return false;">

            <option value="50" <?php SelectOption($NbLignes, 50); ?>>50 par page</option>

            <option value="100" <?php SelectOption($NbLignes, 100); ?>>100 par page</option>

            <option value="200" <?php SelectOption($NbLignes, 200); ?>>200 par page</option>

        </select>

        &nbsp;&nbsp;Aller &agrave; la page :&nbsp;

        <?php 

			//$base_path_system_rand = str_replace('system/', '', BASEPATH);

            //$base_path_system_rand = base_url();

            $base_path_system_rand = "";

            //echo $base_path_system_rand;

			$_SERVER['PATH_INFO'] = $base_path_system_rand.'admin/newsletters_mails';

			

            $CurrentPath = $_SERVER['PATH_INFO'];

            if (! preg_match("/\/liste\//",$CurrentPath)) {

                $CurrentPath .= "/liste/" . $FilterCol . "/" . $FilterValue . "/0";

            }

            $TabCurrentPath = explode("/", $CurrentPath);

            function GetGoToUrl($prmNumPage, $prmTabCurrentPath, $prmUriSegment) {

                $TabGoToUrl = array();

                $c = 1;

                foreach($prmTabCurrentPath as $Elt ) {

                    if ($c != $prmUriSegment + 1) {

                        $TabGoToUrl[] = $Elt;

                    } else {

                        $TabGoToUrl[] = $prmNumPage;

                    }

                    $c ++;

                }

                return site_url() . implode("/", $TabGoToUrl);

            }

        ?>

        <select id="cmbGoToPage" onchange="document.location = '' + document.getElementById('cmbGoToPage').value + ''; return false;">

            <?php $NumAffiche = 1; ?>

            <?php foreach($NumerosPages as $NumPage) { ?>

                <option value="<?php echo GetGoToUrl($NumPage, $TabCurrentPath, $UriSegment); ?>" <?php if (GetGoToUrl($NumPage, $TabCurrentPath, $UriSegment) == site_url() . $_SERVER['PATH_INFO']) { echo "selected"; }?>><?php echo $NumAffiche ?></option>

            <?php $NumAffiche ++; ?>

            <?php } ?>

        </select>

    </p>

    </form>

    <hr />



    <form id="frmSort" name="frmSort" method="POST" action="<?php echo site_url() . $_SERVER['PATH_INFO']; ?>">

        <input type="hidden" name="hdnOrder" id="hdnOrder" value="" />

    </form>

    Liste de <?php echo "<b>" . count($colUsers) . "</b> enregistrement(s) sur ". $CountAllResults . " r&eacute;sultat(s)" ?>

  <!--<table class="table_list" width="90%">-->

	<table cellpadding="1" class="tablesorter" style = "background-color: #CDCDCD;

    font-family: arial;

    font-size: 8pt;

    margin: 10px 0 15px;

    text-align: left;

    width: 99%;">

        <tr class="title" >

            <td width="5%"></td>

            <td>

                <?php if (isset($Ordre)) { ?>

                    <?php if (preg_match("/mail /", $Ordre)) { ?>

                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'mail <?php if (preg_match('/ASC/', $Ordre)) { echo 'DESC'; } else { echo 'ASC'; } ?>'; document.getElementById('frmSort').submit();">Email</a>

                        <img style="border: none;" alt="down" src="<?php echo base_url(); ?>application/resources/admin/images/<?php if (preg_match('/ASC/', $Ordre)) { echo 'up'; } else { echo 'down'; } ?>.gif" />

                    <?php } else { ?>

                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'mail ASC'; document.getElementById('frmSort').submit();">Email</a>

                    <?php } ?>

                <?php } else { ?>

                    <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'mail ASC'; document.getElementById('frmSort').submit();">Email</a>

                <?php } ?>

            </td>

            <td>

                <?php if (isset($Ordre)) { ?>

                    <?php if (preg_match("/departement_code /", $Ordre)) { ?>

                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'departement_code <?php if (preg_match('/ASC/', $Ordre)) { echo 'DESC'; } else { echo 'ASC'; } ?>'; document.getElementById('frmSort').submit();">Departement_code</a>

                        <img style="border: none;" alt="down" src="<?php echo base_url(); ?>application/resources/admin/images/<?php if (preg_match('/ASC/', $Ordre)) { echo 'up'; } else { echo 'down'; } ?>.gif" />

                    <?php } else { ?>

                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'departement_code ASC'; document.getElementById('frmSort').submit();">Departement_code</a>

                    <?php } ?>

                <?php } else { ?>

                    <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'departement_code ASC'; document.getElementById('frmSort').submit();">Departement_code</a>

                <?php } ?>

            </td>

            <td>

                <?php if (isset($Ordre)) { ?>

                    <?php if (preg_match("/name /", $Ordre)) { ?>

                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'name <?php if (preg_match('/ASC/', $Ordre)) { echo 'DESC'; } else { echo 'ASC'; } ?>'; document.getElementById('frmSort').submit();">Nom</a>

                        <img style="border: none;" alt="down" src="<?php echo base_url(); ?>application/resources/admin/images/<?php if (preg_match('/ASC/', $Ordre)) { echo 'up'; } else { echo 'down'; } ?>.gif" />

                    <?php } else { ?>

                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'name ASC'; document.getElementById('frmSort').submit();">Nom</a>

                    <?php } ?>

                <?php } else { ?>

                    <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'name ASC'; document.getElementById('frmSort').submit();">Nom</a>

                <?php } ?>

            </td>

            <td>&nbsp;</td>

            <td>&nbsp;</td>

        </tr>

        <?php

            foreach ($colUsers as $item) { 

        ?>

        <tr class="cell" <?php if ( ($DepartCount+1) %2 == 0)  { ?> style = "background-color: #E6EEEE" <?php }?> >

            <!--Surligner le mot recherche si l'utilisateur a effectue une recherche-->

            <td><a href="<?php echo site_url(); ?>admin/newsletters_mails/fiche/<?php echo $item->id; ?>"><?php echo $DepartCount + 1; ?></a></td>

            <td valign="center">

                <a href="<?php echo site_url(); ?>admin/newsletters_mails/fiche/<?php echo $item->id; ?>"><?php if ($highlight == "yes") { EchoWithHighLightWords($item->mail, $SearchedWords); } else { echo word_limiter(highlight_phrase(preg_replace("/<[^>]*>/", "",$item->mail), $SearchValue, "<span style='background: #FFFF00;'>", "</span>"),10);  } ?></a>

            </td>

            <td valign="center">

                <?php if ($highlight == "yes") { EchoWithHighLightWords($item->departement_code, $SearchedWords); } else { echo word_limiter(highlight_phrase(preg_replace("/<[^>]*>/", "",$item->departement_code), $SearchValue, "<span style='background: #FFFF00;'>", "</span>"),10);  } ?>

            </td>

            <td valign="center">

                <?php if ($highlight == "yes") { EchoWithHighLightWords($item->name, $SearchedWords); } else { echo word_limiter(highlight_phrase(preg_replace("/<[^>]*>/", "",$item->name), $SearchValue, "<span style='background: #FFFF00;'>", "</span>"),10);  } ?>

            </td>

            <td style="text-align: center">

           		<a href="<?php echo site_url(); ?>admin/newsletters_mails/fiche/<?php echo $item->id; ?>" title="Modifier"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/update_ico.png"/></a>

            </td>

            <td style="text-align: center">

           		<a href="javascript:void(0);" onclick="javascript:confirm_delete_user(<?php echo $item->id; ?>);" title="Supprimer"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/delete_ico.png"/></a>

            </td>

        </tr>

        <?php 

                $DepartCount++;

            } 

        ?>

    </table>

    <hr/>

    <p><?php echo $PaginationLinks; ?></p>

</div>

<?php $this->load->view("admin/includes/vwFooter2013"); ?>