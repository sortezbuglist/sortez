<?php $data["zTitle"] = 'Newsletter'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
    <style type="text/css">
        .admin_news_menu_content {
            padding: 15px;
            display: table;
        }

        .admin_news_menu_content a:link,
        .admin_news_menu_content a:hover,
        .admin_news_menu_content a:visited,
        .admin_news_menu_content a:focus {
            color: #ffffff;
            width: 100%;
        }
    </style>

    <div class="col-lg-12 admin_news_menu_content">
        <div class="col-xs-2">
            <a href="<?php echo site_url('admin/newsletters/category/bonplan'); ?>"
               class="btn <?php if (isset($current_categ) && $current_categ == 'bonplan') echo 'btn-success'; else echo 'btn-primary'; ?>">Bonplan</a>
        </div>
        <div class="col-xs-2">
            <a href="<?php echo site_url('admin/newsletters/category/article'); ?>"
               class="btn <?php if (isset($current_categ) && $current_categ == 'article') echo 'btn-success'; else echo 'btn-primary'; ?>">Article</a>
        </div>
        <div class="col-xs-2">
            <a href="<?php echo site_url('admin/newsletters/category/agenda'); ?>"
               class="btn <?php if (isset($current_categ) && $current_categ == 'agenda') echo 'btn-success'; else echo 'btn-primary'; ?>">Agenda</a>
        </div>
        <div class="col-xs-2">
            <a href="<?php echo site_url('admin/newsletters/category/annuaire'); ?>"
               class="btn <?php if (isset($current_categ) && $current_categ == 'annuaire') echo 'btn-success'; else echo 'btn-primary'; ?>">Annuaire</a>
        </div>
        <div class="col-xs-2">
            <a href="<?php echo site_url('admin/newsletters/category/fidelite'); ?>"
               class="btn <?php if (isset($current_categ) && $current_categ == 'fidelite') echo 'btn-success'; else echo 'btn-primary'; ?>">Fidélité</a>
        </div>
        <div class="col-xs-2">
            <a href="<?php echo site_url('admin/newsletters/category/annonce'); ?>"
               class="btn <?php if (isset($current_categ) && $current_categ == 'annonce') echo 'btn-success'; else echo 'btn-primary'; ?>">Annonce</a>
        </div>
        <div class="col-xs-4" style="padding: 15px;">
            <a href="<?php echo site_url('admin/newsletters/category/news_basic'); ?>"
               class="btn <?php if (isset($current_categ) && $current_categ == 'news_basic') echo 'btn-success'; else echo 'btn-primary'; ?>">Newsletter basic</a>
        </div>
        <div class="col-xs-4" style="padding: 15px;">
            <a href="<?php echo site_url('admin/newsletters/category/basic'); ?>"
               class="btn <?php if (isset($current_categ) && $current_categ == 'basic') echo 'btn-success'; else echo 'btn-primary'; ?>">Accès basic</a>
        </div>
        <div class="col-xs-4" style="padding: 15px;">
            <a href="<?php echo site_url('admin/newsletters/category/customnews'); ?>"
               class="btn <?php if (isset($current_categ) && $current_categ == 'customnews') echo 'btn-success'; else echo 'btn-primary'; ?>">Newsletter vide</a>
        </div>
        
    </div>


    <div id="divAdminNewsletter" class="content" align="center"
         style="font-family:Arial, Helvetica, sans-serif; font-size:12px; display: table;">
        <form id="frmNewsletter" name="frmNewsletter" method="POST"
              action="<?php echo site_url("admin/newsletters/envoyer"); ?>">

            <p><br>
            <div class="H1-C text-center">Newsletter</div>
            <br></p>
            <center>
                <table width="450px" border="0" cellspacing="0" cellpadding="0" align="center"
                       style="text-align: left;">
                    <tr>
                        <td>
                            <input name="sendallnews" type="radio" value="1"/>&nbsp;Envoyer &agrave; Tous les
                            Consommateurs Sortez
                            <div id="iDuserSelect" style="padding-left: 50px;">
                                <div>
                                    
                                    <input type="hidden" name="nom_hidden" id="nom_hidden">
                                    <input type="hidden" name="mail_content" id="mail_content">
                                    <input id="iDuserCapturedEmailcheck" type="checkbox"/>
                                    <input id="iDuserCapturedEmail" name="iDuserCapturedEmail" type="hidden" value="0"/>
                                    <label>Liste des adresses email enregistré à partir du formulaire Newsletter</label>
                                </div>
                                <div>
                                    <input id="iDuserCustomerEmailcheck" type="checkbox" value="0"/>
                                    <input id="iDuserCustomerEmail" name="iDuserCustomerEmail" type="hidden" value="0"/>
                                    <label>Liste des utilisateurs inscrits dans sortez</label>
                                </div>
                                <script type="text/javascript">
                                    jQuery(document).ready(function () {
                                        jQuery("#iDuserCapturedEmailcheck").click(function () {
                                            if (jQuery(this).is(':checked')) {
                                                jQuery("#iDuserCapturedEmail").val("1");
                                            } else {
                                                jQuery("#iDuserCapturedEmail").val("0");
                                            }
                                        });
                                        jQuery("#iDuserCustomerEmailcheck").click(function () {
                                            if (jQuery(this).is(':checked')) {
                                                jQuery("#iDuserCustomerEmail").val("1");
                                            } else {
                                                jQuery("#iDuserCustomerEmail").val("0");
                                            }
                                        });
                                        //  jQuery("#iDuserCustomerbasicCheck").click(function () {
                                        //     if (jQuery(this).is(':checked')) {
                                        //         jQuery("#iDuserCustomerBasic").val("1");
                                        //     } else {
                                        //         jQuery("#iDuserCustomerBasic").val("0");
                                        //     }
                                        // });
                                    });
                                </script>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><input name="sendallnews" type="radio" value="2" checked/>&nbsp;Envoyer &agrave; une adresse&nbsp;<input
                                name="sendonemail" id="sendonemail" value="" type="text" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                </table>
            </center>
            <div style="padding-top: 40px;">
                <div class="col-lg-12 padding0">
                    <div class="col-xs-9">
                        <?php if (isset($current_categ) && $current_categ == 'agenda'){ ?>
                        <div id="to_shox_agenda" class="p-4">
                            <label style="float: left; text-align: left; display: table; width: 100%; margin-top: 15px;">Filtre agenda:</label>
                            <select class="form-control" id="Filtre_date">
                                <option value="1">Tout</option>
                                <option value="101">Aujourd'hui</option>
                                <option value="202">Week-end</option>
                                <option value="303">La semaine</option>
                                <option value="404">Semaine prochaine</option>
                                <option value="505">Mois</option>
                            </select>
                        </div>
                        <?php } ?>
                        <?php if (isset($current_categ) && $current_categ == 'article'){ ?>
                            <div id="to_shox_article" class="p-4">
                                <label style="float: left; text-align: left; display: table; width: 100%; margin-top: 15px;">Filtre article:</label>
                                <select class="form-control" id="Filtre_date_article">
                                    <option value="1">Tout</option>
                                    <option value="101">Aujourd'hui</option>
                                    <option value="202">Week-end</option>
                                    <option value="303">La semaine</option>
                                    <option value="404">Semaine prochaine</option>
                                    <option value="505">Mois</option>
                                </select>
                            </div>
                        <?php } ?>
                        <?php if (isset($current_categ) && $current_categ == 'annonce'){ ?>
                            <div id="to_shox_annonce" class="p-4">
                                <label style="float: left; text-align: left; display: table; width: 100%; margin-top: 15px;">Filtre annonce:</label>
                                <select class="form-control" id="Filtre_date_annonce">
                                    <option value=" ">Tout</option>
                                    <option value="0">Reconditionnés</option>
                                    <option value="1">Neufs</option>
                                </select>
                            </div>
                        <?php } ?>
                        <?php if (isset($current_categ) && $current_categ == 'bonplan'){ ?>
                            <div id="to_shox_bonplan" class="p-4">
                                <label style="float: left; text-align: left; display: table; width: 100%; margin-top: 15px;">Filtre bonplan:</label>
                                <select class="form-control" id="Filtre_date_bonplan">
                                    <option value="0">Tout</option>
                                    <option value="1">Les bons plans simples</option>
                                    <option value="2">Les bons plans uniques</option>
                                    <option value="3">Les bons plans multiples</option>
                                </select>
                            </div>
                        <?php } ?>
                        <?php if (isset($current_categ) && $current_categ == 'fidelite'){ ?>
                            <div id="to_shox_fidelity" class="p-4">
                                <label style="float: left; text-align: left; display: table; width: 100%; margin-top: 15px;">Filtre fidelity:</label>
                                <select class="form-control" id="Filtre_date_fidelity">
                                    <option value="0">Tout</option>
                                    <option value="1">Les Remises directes</option>
                                    <option value="2">Les offres capitales</option>
                                    <option value="3">Les coups de tampon</option>
                                </select>
                            </div>
                        <?php } ?>
                        <script type="text/javascript">
                            jQuery("#Filtre_date").change(function () {
                                CKEDITOR.instances['txtContenu'].setData('');
                                if(jQuery('#Filtre_date').val()!='0'){
                                    let filtre=jQuery('#Filtre_date').val();
                                    data="filtre="+filtre;
                                    jQuery.ajax({
                                        url: "<?php echo site_url('admin/newsletters/get_agenda_by_date/');?>",
                                        dataType: 'html',
                                        type: 'POST',
                                        data: data,
                                        success: function (data) {
                                            CKEDITOR.instances['txtContenu'].setData(data);
                                        },
                                        error: function (data) {
                                            $("#txtContenu").html('');
                                        }
                                    });
                                }
                            });
                            jQuery("#Filtre_date_article").change(function () {
                                CKEDITOR.instances['txtContenu'].setData('');
                                if(jQuery('#Filtre_date_article').val()!='0'){
                                    let filtre=jQuery('#Filtre_date_article').val();
                                    data="filtre="+filtre;
                                    jQuery.ajax({
                                        url: "<?php echo site_url('admin/newsletters/get_article_by_date/');?>",
                                        dataType: 'html',
                                        type: 'POST',
                                        data: data,
                                        success: function (data) {
                                            CKEDITOR.instances['txtContenu'].setData(data);
                                        },
                                        error: function (data) {
                                            $("#txtContenu").html('');
                                        }
                                    });
                                }
                            });
                            jQuery("#Filtre_date_annonce").change(function () {
                                CKEDITOR.instances['txtContenu'].setData('');
                                if(jQuery('#Filtre_date_annonce').val()!=''){
                                    let filtre=jQuery('#Filtre_date_annonce').val();
                                    data="filtre="+filtre;
                                    jQuery.ajax({
                                        url: "<?php echo site_url('admin/newsletters/get_annonce_by_date/');?>",
                                        dataType: 'html',
                                        type: 'POST',
                                        data: data,
                                        success: function (data) {
                                            CKEDITOR.instances['txtContenu'].setData(data);
                                        },
                                        error: function (data) {
                                            $("#txtContenu").html('');
                                        }
                                    });
                                }
                            });
                            jQuery("#Filtre_date_bonplan").change(function () {
                                CKEDITOR.instances['txtContenu'].setData('');
                                if(jQuery('#Filtre_date_bonplan').val()!=''){
                                    let filtre=jQuery('#Filtre_date_bonplan').val();
                                    data="filtre="+filtre;
                                    jQuery.ajax({
                                        url: "<?php echo site_url('admin/newsletters/get_bonplan_by_date/');?>",
                                        dataType: 'html',
                                        type: 'POST',
                                        data: data,
                                        success: function (data) {
                                            CKEDITOR.instances['txtContenu'].setData(data);
                                        },
                                        error: function (data) {
                                            $("#txtContenu").html('');
                                        }
                                    });
                                }
                            });
                            jQuery("#Filtre_date_fidelity").change(function () {
                                CKEDITOR.instances['txtContenu'].setData('');
                                if(jQuery('#Filtre_date_fidelity').val()!=''){
                                    let filtre=jQuery('#Filtre_date_fidelity').val();
                                    data="filtre="+filtre;
                                    jQuery.ajax({
                                        url: "<?php echo site_url('admin/newsletters/get_fidelity_by_date/');?>",
                                        dataType: 'html',
                                        type: 'POST',
                                        data: data,
                                        success: function (data) {
                                            CKEDITOR.instances['txtContenu'].setData(data);
                                        },
                                        error: function (data) {
                                            $("#txtContenu").html('');
                                        }
                                    });
                                }
                            });
                        </script>
                        <div style="float: left; text-align: left; display: table; width: 100%;"><label>Objet du mail :</label></div>
                        <div style="float: left; text-align: left; display: table; width: 100%;">
                            <input type="text" id="txtSujet" name="txtSujet" class="form-control" value="<?php if (isset($txtSujet)&& $txtSujet!="") echo $txtSujet; ?>"/>
                        </div>
                        <div style="float: left; text-align: left; display: table; width: 100%; margin-top: 15px;"><label>Contenu du mail :</label></div>
                        <div style="display: table; width: 100%;">
                            <textarea id="txtContenu" style="width: 100%; height: 200px;" name="txtContenu"><?php echo $mail_content_newsletter; ?></textarea>
                            <script>

                                CKEDITOR.replace('txtContenu');

                            </script>
                        </div>
                        <div class="row" style="margin-top: 2%">
                        <div class="col-lg-6  pt-3">
                            <input type="text" id="nom" value="" placeholder="titre" style="height:35px;"><a id="save_content" class="btn btn-info pt-1 pl-1" style="border-radius: 0px 0px 0px 0px;font-size:20px;font-family: times New Roman;color:white;height:35px;width:192px;margin-bottom: 0.6%;padding: 0.01%;">Enregistrer le contenu</a>
                        </div>
                    </div>
                        <div style="padding: 30px; text-align: center;">
                            <div>
                                <input type="button" id="btnEnvoyer" value="Envoyer"
                                       style="padding: 15px 40px; font-size: 30px; border-radius: 15px;" />
                                <input type="hidden" name="current_categ" id="current_categ" value="<?php if (isset($current_categ)&& $current_categ!="") echo $current_categ; else echo "bonplan"; ?>"/>
                            </div>
                            <div id="errornewsfrm"></div>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div><label>Séléctionner les éléments :</label></div>
                        <div style="height: 600px; border: 1px solid #ccc;"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div id="div_list_save_container">
    <h2>Liste de Sauvegarde</h2>
    <table class="table">
        <thead>
        <tr>

            <th>ID</th>
            <th>Objet</th>
            <th>Nom Modification</th>
            <th>Date Modification</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
         <?php  if (isset($liste_saved_mail) AND count($liste_saved_mail) !=0){ ?>
                                    <?php foreach ($liste_saved_mail as $list) {?>
            <tr>
                <td><?php echo $list->id; ?></td>
                <td><?php echo $list->object; ?></td>
                <td><?php echo $list->nom; ?></td>
                <td><?php echo convert_Sqldate_to_Frenchdate($list->date_ajout); ?></td>
                <td>
                    <button class="btn btn-modif" id="option_change<?php echo $list->id; ?>" style="width: 75%; background-color: #265A88; color: white;" >
                        Modifier
                    </button>
                </td>
                <td>
                    <button class="btn btn-supprime" id="supprime_change<?php echo $list->id; ?>"style="width: 75%; background-color: red; color: white;"
                       >
                        Supprimer
                    </button>
                </td>
            </tr>
            <input type="hidden" name="supprime_change" id="supprime_change_value<?php echo $list->id; ?>" value="<?php echo $list->id; ?>">
            <input type="hidden" name="option_change" id="option_change_value<?php echo $list->id; ?>"value="<?php echo $list->id; ?>">
            <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/fields.check.js"></script>
    <script type="text/javascript">
        jQuery("#btnEnvoyer").click(function () {
            //CKEDITOR.instances.txtEnTete.updateElement();
            //CKEDITOR.instances.txtPiedDePage.updateElement();
            var newserror = '';
            var radiochoice = jQuery('input[type=radio][name=sendallnews]:checked').attr('value');
            if (radiochoice == 2) {
                if (jQuery("#sendonemail").val() == "") {
                    newserror = '<span style="color:#F00;">Specifier l\'adresse email pour envoyer le Newsletter !</span>';
                    jQuery("#errornewsfrm").html(newserror);
                } else if (!isEmail(jQuery("#sendonemail").val())) {
                    newserror = '<span style="color:#F00;">Veuillez ajouter un email valide !</span>';
                    jQuery("#errornewsfrm").html(newserror);
                } else newserror = '';
            } else if (radiochoice == 1) {
                newserror = '';
            }

            //alert("qsdf "+newserror);
            if (newserror == '') {
                jQuery("#frmNewsletter").submit();
            }
        });
        


        jQuery("#supprime_change<?php echo $list->id; ?>").click(function () {
            if(jQuery('#supprime_change<?php echo $list->id; ?>').val()!='0'){
                if (confirm('Valider la suppression ?')) {
                let id=jQuery('#supprime_change_value<?php echo $list->id; ?>').val();
                data="id="+id;
                jQuery.ajax({
                    url: "<?php echo site_url('admin/Newsletters/delete_newsletter_stat');?>",
                    dataType: 'html',
                    type: 'POST',
                    data: data,
                    success: function (data) {
                        document.location.reload();
                    },
                    error: function (data) {
                        $("#txtContenu").html('');
                    }
                    });
                    }
                    }
                });
                    
                    jQuery('#option_change<?php echo $list->id; ?>').click(function () {
                        $('#option_change<?php echo $list->id; ?>').html('<progress style="width:50px;"></progress>')
                        ids=$('#option_change_value<?php echo $list->id; ?>').val();
                        dataid="id="+ids;
                        jQuery.ajax({
                            url: "<?php echo site_url('admin/Newsletters/change_stat_news');?>",
                            dataType: 'html',
                            type: 'POST',
                            data: dataid,
                            success: function (data) {
                                let data_parsed=jQuery.parseJSON(data);
                                console.log(data_parsed.mail_content);

                                    CKEDITOR.instances['txtContenu'].setData(data_parsed.mail_content);
                                    $('#txtSujet').val(data_parsed.object);
                                    window.scrollTo(0, 0);
                                    $('#option_change<?php echo $list->id; ?>').html('Modifier')
                                 
                            },
                            error: function (data) {
                                console.log(data);
                                alert('Erreur de chargement');
                            }

                        });

                    });
                    
    </script>
          <?php } ?>
            <?php } ?>
        </tbody>
    </table>
</div>

    <script type="text/javascript">
        jQuery("#save_content").click(function () {  
            $("#frmNewsletter").attr("action","<?php echo site_url("admin/Newsletters/save_newsletter_stat"); ?>");
            var nom=$('#nom').val();
            $('#nom_hidden').val(nom);
            var valued =  CKEDITOR.instances['txtContenu'].getData();
            $("#mail_content").val(valued);
            $('#frmNewsletter').submit();
        });
    </script>


    <div>
        <?php
        $data['empty'] = null;
        $this->load->view('admin/newsletter/newsletter_sent_list', $data);
        ?>
    </div>
<script>
    jQuery("#save_content").click(function () {
        $("#frmNewsletter").attr("action","<?php echo site_url("admin/Newsletters/save_newsletter_stat"); ?>");
        var nom=$('#nom').val();
        $('#nom_hidden').val(nom);
        var valued =  CKEDITOR.instances['txtContenu'].getData();
        $("#mail_content").val(valued);
        $('#frmNewsletter').submit();
    });
</script>

<?php $this->load->view("admin/includes/vwFooter2013"); ?>