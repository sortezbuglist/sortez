<?php $data["zTitle"] = 'Gestion Catégorie agenda'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>
<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra'],  headers: {3: { sorter: false}, 4: {sorter: false}, 5: {sorter: false} }})
				.tablesorterPager({container: $("#pager")});
		});
		
		
	function confirm_delete_categorie(IdRubrique){
		if (confirm("Voulez-vous supprimer cette catégorie ?")) {
           document.location="<?php echo site_url("admin/categories_agenda/delete_categories_agenda/");?>/"+IdRubrique;
       }
	}
</script>
    <div id="divMesAnnonces" class="content" align="center">
        <form name="frmMesAnnonces" id="frmMesAnnonces" action="" method="POST" enctype="multipart/form-data">
                <p><a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" value= "retour au menu" id = "btn"/></a></p>
                <br>
                <div class="H1-C">Liste des catégories d'agenda</div><br/>
                <?php if (isset($mess_editcategorie)) echo $mess_editcategorie;?>
                <br/><br/>
                <a href="<?php echo site_url("admin/categories_agenda/insert_categories_agenda/"); ?>">Ajouter une catégorie d'agenda</a> </br>
				<div id="container">
					<table cellpadding="1" class="tablesorter">
						<thead>
							<tr>                    
								<th>Id</th>
								<th>Nom</th>
								<th>Type agenda</th>
<!--                                <th>Souscategorie</th>-->
								<th>Modifier</th>
                                <th>Supprimer</th>
							</tr>
						</thead>
						<tbody> 
                        <?php if (count($toListeCategorie)>0) { ?>
							<?php foreach($toListeCategorie as $oListeCategorie){ ?>
								<tr>                    
									<td><?php echo $oListeCategorie->agenda_categid ; ?></td>
									<td><?php echo $oListeCategorie->category ; ?></td>
                                    <td>
									<?php
									$this->load->model("mdl_types_agenda");
									$oagenda_typeid = $this->mdl_types_agenda->GetById($oListeCategorie->agenda_typeid) ;
									echo $oagenda_typeid->agenda_type;

                                    ?>
                                    </td>
                                    
<!--									<td>&nbsp;<a href="--><?php //echo site_url("admin/categories_agenda/souscategorie/".$oListeCategorie->agenda_categid); ?><!--">Sous catégories</a>&nbsp;</td>-->

                                  <td>&nbsp;<a href="<?php echo site_url("admin/categories_agenda/edit_categories_agenda/".$oListeCategorie->agenda_categid); ?>">Modifier</a>&nbsp;</td>

                                  <td>&nbsp;<a href="javascript:void();" onClick="confirm_delete_categorie(<?php echo $oListeCategorie->agenda_categid;?>);">Supprimer</a>&nbsp;</td>

						  </tr>
							<?php } ?>
                        <?php } ?>
						</tbody>
					</table>
					<div id="pager" class="pager">
						<img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
						<img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
						<input type="text" class="pagedisplay"/>
						<img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
						<img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
						<select class="pagesize">
							<option selected="selected"  value="10">10</option>
							<option value="20">20</option>
							<option value="30">30</option>
							<option  value="40">40</option>
						</select>
				  </div>
		  </div>
        </form>
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>