<?php $data["zTitle"] = 'Administration'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra'],  headers: {5: {sorter: false}, 6: {sorter: false} }})
				.tablesorterPager({container: $("#pager")});
		});
		
	function confirm_delete_abonnement(IdRubrique){
		if (confirm("Voulez-vous supprimer cet abonnement ?")) {
           document.location="<?php echo site_url("admin/manage_abonnement/delete_abonnement/");?>/"+IdRubrique;
       }
	}
		
</script>
    <div id="divMesAnnonces" class="content" align="center">
        <form name="frmMesAnnonces" id="frmMesAnnonces" action="" method="POST" enctype="multipart/form-data">
                <p><a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" value= "retour au menu" id = "btn"/></a></p>
                <br>
                <div class="H1-C">Liste des abonnments</div><br/>
                <?php if (isset($mess_editcategorie)) echo $mess_editcategorie;?>
                <br/>
                <a href="<?php echo site_url("admin/manage_abonnement/insert_abonnement/"); ?>">Ajouter un abonnement</a> </br>
				<div id="container">
					<table cellpadding="1" class="tablesorter">
						<thead>
							<tr>                    
								<th>Id</th>
								<th>Nom</th>
								<th>Description</th>
                                <th>Type</th>
								<th>Tarif en &euro;</th>
                                <th></th>
                                <th></th>
							</tr>
						</thead>
						<tbody> 
							
                        <?php if (isset($toAbonnement) && !empty($toAbonnement)) {?>
							<?php foreach($toAbonnement as $oListeAbonnement){ ?>
								<tr>                    
									<td><?php echo $oListeAbonnement->IdAbonnement ; ?></td>
									<td><?php echo $oListeAbonnement->Nom ; ?></td>
									<td><?php echo $oListeAbonnement->Description ; ?></td>
                                    <td><?php echo $oListeAbonnement->type ; ?></td>
                                    <td><?php echo $oListeAbonnement->tarif; ?></td>
                                  <td>&nbsp;<a href="<?php echo site_url("admin/manage_abonnement/edit_abonnement/".$oListeAbonnement->IdAbonnement); ?>">Modifier</a>&nbsp;</td>
                                  <td>&nbsp;<a href="javascript:void();" onClick="confirm_delete_abonnement(<?php echo $oListeAbonnement->IdAbonnement;?>);">Supprimer</a>&nbsp;</td>
						  </tr>
							<?php } ?>
                          <?php } ?>
						</tbody>
					</table>
					<div id="pager" class="pager">
						<img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
						<img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
						<input type="text" class="pagedisplay"/>
						<img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
						<img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
						<select class="pagesize" style="visibility:hidden">
							<option selected="selected"  value="10">10</option>
							<option value="20">20</option>
							<option value="30">30</option>
							<option  value="40">40</option>
						</select>
				  </div>
		  </div>
        </form>
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>