<?php $data["zTitle"] = 'Creation Bonplan'; ?>
<?php $this->load->view("front/includes/vwHeader", $data); ?>
<?php $this->load->view("front/includes/vwMenu"); ?>
	<script>
		$(function() {
			$( "#IdDateDebut" ).datepicker({ dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
											 monthNames: ['Janvier','Fevier','Mars','Avril','Mai','Juin','Juiller','Aout','Septembre','Octobre','Novembre','Decembre'],
											 dateFormat: 'yy-mm-dd',
											 autoSize: true});
			$( "#IdDateFin" ).datepicker({ dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
											monthNames: ['Janvier','Fevier','Mars','Avril','Mai','Juin','Juiller','Aout','Septembre','Octobre','Novembre','Decembre'],
											dateFormat: 'yy-mm-dd'});
		});
	</script>
    
    <div id="divFicheAnnonce" class="content" align="center">
	     <br>
	     <a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" value= "retour au menu" id = "btn"/> </a>
        <form name="frmCreationBonplan" id="frmCreationBonplan" action="<?php if (isset($oBonplan)) { echo site_url("admin/bonplan/modifBonplan/$idCommercant"); }else{ echo site_url("admin/bonplan/creerBonplan/$idCommercant"); } ?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="bonplan[bonplan_commercant_id]" id="IdCommercant" value="<?php echo $idCommercant ; ?>" />
		<input type="hidden" name="bonplan[bonplan_id]" id="Idbonplan_id" value="<?php if (isset($oBonplan)) { echo $oBonplan->bonplan_id ; } ?>" />
            <h1><?php echo $title ; ?></h1>
			<span><?php if (isset($msg)) { echo $msg ; } ?></span>
            <fieldset>
                <legend>Bonplan</legend>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                    </tr>
					<tr>
                        <td>
                            <label>Utilisation : </label>
                        </td>
                        <td>
                            <select name="bonplan[bon_plan_utilise_plusieurs]" id="idEtat">
                                <option value="c">-- Veuillez choisir --</option>
									<option value="0" <?php  if (isset($oBonplan) && $oBonplan->bon_plan_utilise_plusieurs == 0) { echo "selected"; } ?>>Utilisable une fois</option>
									<option value="1"  <?php if (isset($oBonplan) && $oBonplan->bon_plan_utilise_plusieurs == 1) { echo "selected"; } ?>>Toujours Utilisable</option>
                            </select>
                        </td>
                    </tr>
					<tr>
                        <td>
                            <label>Date debut : </label>
                        </td>
                        <td>
                            <input type="text" name="bonplan[bonplan_date_debut]" id="IdDateDebut" value="<?php if (isset($oBonplan)) { echo $oBonplan->bonplan_date_debut ; } ?>"  style="width:150px;"/>
                        </td>
                    </tr>
					<tr>
                        <td>
                            <label>Date fin : </label>
                        </td>
                        <td>
                            <input type="text" name="bonplan[bonplan_date_fin]" id="IdDateFin" value="<?php if (isset($oBonplan)) { echo $oBonplan->bonplan_date_fin ; } ?>" style="width:150px;"/>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Titre : </label>
                        </td>
                        <td>
							<textarea rows="2" cols="20" name="bonplan[bonplan_titre]" id="idTitre"><?php if (isset($oBonplan)) { echo $oBonplan->bonplan_titre ; } ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>D&eacute;scription : </label>
                        </td>
                        <td>
							<textarea rows="2" cols="20" name="bonplan[bonplan_texte]" id="idDescription"><?php if (isset($oBonplan)) { echo $oBonplan->bonplan_texte ; } ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Points demand&eacute;s : </label>
                        </td>
                        <td>
                            <input type="text" name="bonplan[bonplan_nombrepris]" id="idbonplan_nombrepris" value="<?php if (isset($oBonplan)) { echo $oBonplan->bonplan_nombrepris ; } ?>" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <label>Photo 01 : </label>
                        </td>
                        <td>
							<input type="hidden" name="bonplan[bonplan_photo1]" id="IdPhoto1" value="<?php if (isset($oBonplan)) { echo $oBonplan->bonplan_photo1 ; } ?>" />
                            <input type="file" name="annoncePhoto1" id="annoncePhoto1" value="" onchange="javascript:UploadImage ('<?php echo site_url("front/upload/index/");?>', 'bonplanPhoto1', '1');"/><img src="<?php echo GetImagePath("front/"); ?>/loading.gif" id="loading1" style="display:none"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Photo 02 :</label>
                        </td>
                        <td>
							<input type="hidden" name="bonplan[bonplan_photo2]" id="IdPhoto2" value="<?php if (isset($oBonplan)) { echo $oBonplan->bonplan_photo2 ; } ?>" />
                            <input type="file" name="annoncePhoto2" id="annoncePhoto2" value="" onchange="javascript:UploadImage ('<?php echo site_url("front/upload/index/");?>', 'bonplanPhoto2', '2');"/><img src="<?php echo GetImagePath("front/"); ?>/loading.gif" id="loading2" style="display:none"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Photo 03 :</label>
                        </td>
                        <td>
							<input type="hidden" name="bonplan[bonplan_photo3]" id="IdPhoto3" value="<?php if (isset($oBonplan)) { echo $oBonplan->bonplan_photo3 ; } ?>" />
                            <input type="file" name="annoncePhoto3" id="annoncePhoto3" value="" onchange="javascript:UploadImage ('<?php echo site_url("front/upload/index/");?>', 'bonplanPhoto3', '3');"/><img src="<?php echo GetImagePath("front/"); ?>/loading.gif" id="loading3" style="display:none"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Photo 04 :</label>
                        </td>
                        <td>
							<input type="hidden" name="bonplan[bonplan_photo4]" id="IdPhoto4" value="<?php if (isset($oBonplan)) { echo $oBonplan->bonplan_photo4 ; } ?>" />
                            <input type="file" name="annoncePhoto4" id="annoncePhoto4" value="" onchange="javascript:UploadImage ('<?php echo site_url("front/upload/index/");?>', 'bonplanPhoto4', '4');"/><img src="<?php echo GetImagePath("front/"); ?>/loading.gif" id="loading4" style="display:none"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Photo 05 :</label>
                        </td>
                        <td>
							<input type="hidden" name="bonplan[bonplan_photo]" id="IdPhoto" value="<?php if (isset($oBonplan)) { echo $oBonplan->bonplan_photo ; } ?>" />
                            <input type="file" name="annoncePhoto4" id="annoncePhoto4" value="" onchange="javascript:UploadImage ('<?php echo site_url("front/upload/index/");?>', 'bonplanPhoto', '5');"/><img src="<?php echo GetImagePath("front/"); ?>/loading.gif" id="loading" style="display:none"/>
                        </td>
                    </tr>
					<tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
					<tr>
                        <td></td>
                        <td align="left"><input type="button" value="Annuler" onclick="javascript:annulationAjout('<?php echo site_url("admin/bonplan/annulationAjout");?>', '<?php echo site_url("front/bonplan/listeMesBonplans/");?>', '<?php if (!isset($oBonplan)){ echo "ajout" ;}?>', '<?php echo $idCommercant ;?>');" />&nbsp;<input type="button" value="Valider" onclick="javascript:testFormBonplan();" /></td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
<?php $this->load->view("front/includes/vwFooter"); ?>