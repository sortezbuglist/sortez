<?php $data["zTitle"] = 'Gestion villes'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra']})
				.tablesorterPager({container: $("#pager")});
		});
</script>
    <div id="divAdminHome" class="content" align="center">
	     <p><a style = "color:black; text-decoration:none;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input class="btn btn-primary" type = "button" value= "retour au menu" id = "btn" onclick="document.location='<?php echo site_url("admin/home/" );?>';"/>  </a></p>
		 <p><a style = "color:black;text-decoration:none;" href = "<?php echo site_url("admin/villes/ficheVille/0") ;?>"> <input class="btn btn-success" type = "button" value= "Ajouter Ville" id = "btn"/>  </a></p>
        <br><div class="H1-C">Liste des Villes</div><br>
        
        <div style="color:#33CC33"><?php if (isset($msg)) echo $msg;?></div>
        <table cellpadding="1" class="tablesorter" style="text-align:left">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nom</th>
                    <th>Code postal</th>
                    <th>Nom simple</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($colVilles as $objVille) { ?>
                    <tr>
                        <td>
                            <a href="<?php echo site_url("admin/villes/ficheVille/".$objVille->IdVille); ?>">
                                <?php echo htmlspecialchars(stripcslashes($objVille->IdVille)); ?>
                            </a>
                        </td>
                        <td><?php echo htmlspecialchars(stripcslashes($objVille->Nom)); ?></td>
                        <td><?php echo htmlspecialchars(stripcslashes($objVille->CodePostal)); ?></td>
                        <td><?php echo htmlspecialchars(stripcslashes($objVille->NomSimple)); ?></td>
                        <td>
                            <a href="<?php echo site_url("admin/villes/ficheVille/".$objVille->IdVille); ?>">
                                Modifier
                            </a>
                        </td>
                        <td>
                            <a href="<?php echo site_url("admin/villes/delete/".$objVille->IdVille) ; ?>">
                                Supprimer
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
		<div id="pager" class="pager">
			<img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
			<img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
			<input type="text" class="pagedisplay"/>
			<img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
			<img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
			<select class="pagesize" style="visibility:hidden">
				<option selected="selected"  value="10">10</option>
				<option value="20">20</option>
				<option value="30">30</option>
				<option  value="40">40</option>
			</select>
		</div>
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>