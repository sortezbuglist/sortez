<?php $data["zTitle"] = 'Creation Type agenda'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script>
	function testFormTypeagenda(){
	var agenda_type = $("#agenda_type").val () ;
	
	var zErreur = "" ;
	
	if (agenda_type == ""){
		zErreur += "Veuillez selectionner un agenda_type\n" ;
	}
	
	if (zErreur != ""){
		alert ('Veuillez indiquer un Nom !') ;
	}else{
		document.frmCreationTypeagenda.submit();
	}
	
	return false;
}
</script>
    
    <div id="divFicheAnnonce" class="content" align="center">
	     <br>
	     <a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" value= "Retour au Menu" id = "btn"/> </a>
         
        <form name="frmCreationTypeagenda" id="frmCreationTypeagenda" action="<?php if (isset($oagenda)) { echo site_url("admin/types_agenda/modif_types_agenda/$agenda_typeid"); }else{ echo site_url("admin/types_agenda/creer_types_agenda"); } ?>" method="POST">
		<?php if (isset($oagenda->agenda_typeid)) {?><input type="hidden" name="types_agenda[agenda_typeid]" id="agenda_typeid_id" value="<?php if (isset($oagenda)) { echo $oagenda->agenda_typeid ; } ?>" /><?php  } ?>
            <h1><?php echo $title ; ?></h1>
			<span><?php if (isset($msg)) { echo $msg ; } ?></span>
            <fieldset>
                <legend>Type</legend>
                <table cellpadding="0" cellspacing="0">
                    
					<tr>
                        <td>
                            <label>Nom : </label>
                        </td>
                        <td>
                            <input type="text" name="types_agenda[agenda_type]" id="agenda_type" value="<?php if (isset($oagenda)) { echo $oagenda->agenda_type ; } ?>" />
                        </td>
                    </tr>
					<tr>
                        <td></td>
                        <td align="left">
                        <input type="button" value="Annuler" onclick="javascript:document.location='<?php echo site_url("admin/types_agenda/liste");?>';" />&nbsp;
                        <input type="button" value="Valider" onclick="javascript:testFormTypeagenda();" /></td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>


<?php $this->load->view("admin/includes/vwFooter2013"); ?>