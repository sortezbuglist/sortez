<?php $data["zTitle"] = 'Administration'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra'],  headers: {2: { sorter: false}, 3: {sorter: false} }})
				.tablesorterPager({container: $("#pager")});
		});
		
		
	function confirm_delete_souscategorie(IdSousRubrique,IdRubrique){
		if (confirm("Voulez-vous supprimer cette sous-catégorie ?")) {
           document.location="<?php echo site_url("admin/commercants/supprimersouscategorie/");?>/"+IdSousRubrique+"/"+IdRubrique;
       }
	}
		
</script>
      <br>
    <div id="divMesAnnonces" class="content" align="center">
                <p>
                <a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>">Retour au menu</a><br/>
                <a style = "color:black;" href = "<?php echo site_url("admin/commercants/categories" ) ; ?>">Retour à la liste des catégories</a>
                </p>
                <div class="H1-C">Liste des Sous-catégories de <?php echo $toCategorie->Nom; ?></div>
                <br/>
                <?php if (isset($mess_editsouscategorie)) echo $mess_editsouscategorie;?>
                <br/>
                <a href="<?php echo site_url("admin/commercants/insertsouscategorie/".$toCategorie->IdRubrique); ?>">Ajouter une sous-catégorie</a> </br>
				<div id="container">
					<table cellpadding="1" class="tablesorter">
						<thead>
							<tr>                    
								<th>Id</th>
								<th>Nom</th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody> 
                        	<?php if (count($toListeSousCategorie)!=0) { ?>
								<?php foreach($toListeSousCategorie as $oListeSousCategorie){ ?>
                                    <tr>                    
                                        <td><?php echo $oListeSousCategorie->IdSousRubrique ; ?></td>
                                        <td><?php echo $oListeSousCategorie->Nom ; ?></td>
                                        <td>&nbsp;<a href="<?php echo site_url("admin/commercants/modifiersouscategorie/".$oListeSousCategorie->IdSousRubrique."/".$toCategorie->IdRubrique); ?>">Modifier</a>&nbsp;</td>
                                        <td>&nbsp;<a href="javascript:void();" onClick="confirm_delete_souscategorie(<?php echo $oListeSousCategorie->IdSousRubrique;?>, <?php echo $toCategorie->IdRubrique;?>);">Supprimer</a>&nbsp;</td>
                                </tr>
                                <?php } ?>
                            <?php } ?>
						</tbody>
					</table>
					<div id="pager" class="pager">
						<img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
						<img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
						<input type="text" class="pagedisplay"/>
						<img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
						<img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
						<select class="pagesize" style="visibility:hidden">
							<option selected="selected"  value="10">10</option>
							<option value="20">20</option>
							<option value="30">30</option>
							<option  value="40">40</option>
						</select>
				  </div>
		  </div>
        </form>
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>