<?php $data["zTitle"] = "Administration"; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<?php 
    function SelectOption($prmValue, $prmCurrent) {
        if ($prmValue == $prmCurrent) {
            echo "selected";
        }
    }
    
    function EchoWithHighLightWords($prmValue, $prmSearchedWords) {
        $out = $prmValue;
        for ($i = 0; $i < sizeof($prmSearchedWords); $i ++) {
            $out = word_limiter(highlight_phrase($out, $prmSearchedWords[$i], "<span style='background: #FFFF00;'>", "</span>"),10) ;
        }
        echo $out;
    }
?>
<script type="text/javascript" charset="utf-8">
     function OnBtnNew_click() {
        document.location = "<?php echo site_url(); ?>front/professionnels/inscription";
    }
    function confirm_delete_user(IdRuser){
        if (confirm("Voulez-vous supprimer cet Administrateur ?")) {
           //document.location="<?php //echo site_url("admin/commercants/fiche_supprimer_user/");?>/"+IdRuser;
       }
</script>
    <div class="content" align="center">
    <table id="divAdminHome" class="content" align="center">
         <p><a style = "color:black; text-decoration:none;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" value= "retour au menu" id = "btn" onclick="document.location='<?php echo site_url("admin/home/" );?>';"/>  </a></p>
         <p><a style = "color:black;text-decoration:none;" href = "<?php echo site_url("admin/test/fiche_test/0") ;?>"> <input type = "button" value= "Ajouter test" id = "btn"/>  </a></p>
        <br><div class="H1-C">Liste des test</div><br>
         <form id="frmSearch" method="POST" action="<?php echo site_url(); ?>admin/test/liste/<?php if (isset($FilterCol)) echo $FilterCol ; ?>/<?php if (isset($FilterValue)) echo $FilterValue ; ?>/0">

     <form methode='get' action="search.php"> 
         Recherche : <input type="text" name="txtSearch" id="txtSearch" value="<?php echo $SearchValue; ?>" />&nbsp;
         <input type="submit" name="btnSearch" class="CommandButton" value="GO" id="btnSearch" />
     </form>

    <table cellpadding="1" class="table table-striped" style = "font-family: arial;
    font-size: 8pt;
    margin: 10px 0 15px;
    text-align: left;
    width: 99%;">
        <tr class="title" >
            <th width="5%"></th>
            <th>
                 <?php if (isset($Ordre)) { ?>
                    <?php if (preg_match("/id /", $Ordre)) { ?>
                        
                         
                    <?php } ?>
                
                <?php } ?>
            </th>
            <th><?php if (isset($Ordre)) { ?>
                    <?php if (preg_match("/Nom /", $Ordre)) { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Nom <?php if (preg_match('/ASC/', $Ordre)) { echo 'DESC'; } else { echo 'ASC'; } ?>'; document.getElementById('frmSort').submit();">Nom</a>
                        <img style="border: none;" alt="down" src="<?php echo base_url(); ?>application/resources/admin/images/<?php if (preg_match('/ASC/', $Ordre)) { echo 'up'; } else { echo 'down'; } ?>.gif" />
                    <?php } else { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Nom ASC'; document.getElementById('frmSort').submit();">Nom</a>
                    <?php } ?>
            
                <?php } ?>
                </th>
            <th>
                <tr>
                    <?php if (isset($Ordre)) { ?>
                    <?php if (preg_match("/id /", $Ordre)) { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'id <?php if (preg_match('/ASC/', $Ordre)) { echo 'DESC'; } else { echo 'ASC'; } ?>'; document.getElementById('frmSort').submit();">id</a>
                        <img style="border: none;" alt="down" src="<?php echo base_url(); ?>application/resources/admin/images/<?php if (preg_match('/ASC/', $Ordre)) { echo 'up'; } else { echo 'down'; } ?>.gif" />
                         <?php } else { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Id ASC'; document.getElementById('frmSort').submit();">id</a>
                    <?php } ?>
                
                <?php } ?>
            </th>
            <th><?php if (isset($Ordre)) { ?>
                    <?php if (preg_match("/id /", $Ordre)) { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'id <?php if (preg_match('/ASC/', $Ordre)) { echo 'DESC'; } else { echo 'ASC'; } ?>'; document.getElementById('frmSort').submit();">id</a>
                        <img style="border: none;" alt="down" src="<?php echo base_url(); ?>application/resources/admin/images/<?php if (preg_match('/ASC/', $Ordre)) { echo 'up'; } else { echo 'down'; } ?>.gif" />
                    <?php } else { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'id ASC'; document.getElementById('frmSort').submit();">id</a>
                    <?php } ?>
                <?php } else { ?>
                    <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'id ASC'; document.getElementById('frmSort').submit();">id</a>
                <?php } ?>
                </th>
            <th>
                <?php if (isset($Ordre)) { ?>
                    <?php if (preg_match("/Nombre /", $Ordre)) { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Nombre <?php if (preg_match('/ASC/', $Ordre)) { echo 'DESC'; } else { echo 'ASC'; } ?>'; document.getElementById('frmSort').submit();">Pr&eacute;nom</a>
                        <img style="border: none;" alt="down" src="<?php echo base_url(); ?>application/resources/admin/images/<?php if (preg_match('/ASC/', $Ordre)) { echo 'up'; } else { echo 'down'; } ?>.gif" /><?php } else { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Nombre ASC'; document.getElementById('frmSort').submit();">Pr&eacute;nom</a>
                    <?php } ?>
                <?php } else { ?>
                    <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Nombre ASC'; document.getElementById('frmSort').submit();">Nom</a>
                <?php } ?>
                <th>
                <?php if (isset($Ordre)) { ?>
                    <?php if (preg_match("/Nombre /", $Ordre)) { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Nombre <?php if (preg_match('/ASC/', $Ordre)) { echo 'DESC'; } else { echo 'ASC'; } ?>'; document.getElementById('frmSort').submit();">Nombre</a>
                        <img style="border: none;" alt="down" src="<?php echo base_url(); ?>application/resources/admin/images/<?php if (preg_match('/ASC/', $Ordre)) { echo 'up'; } else { echo 'down'; } ?>.gif" />
                    <?php } else { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Nombre ASC'; document.getElementById('frmSort').submit();">Nombre</a>
                    <?php } ?>
                <?php } else { ?>
                    <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Nombre ASC'; document.getElementById('frmSort').submit();">Nombre</a>
                <?php } ?>
            </th>
                    
                    <th>
                <?php if (isset($Ordre)) { ?>
                    <?php if (preg_match("/Option /", $Ordre)) { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Option <?php if (preg_match('/ASC/', $Ordre)) { echo 'DESC'; } else { echo 'ASC'; } ?>'; document.getElementById('frmSort').submit();">Option</a>
                        <img style="border: none;" alt="down" src="<?php echo base_url(); ?>application/resources/admin/images/<?php if (preg_match('/ASC/', $Ordre)) { echo 'up'; } else { echo 'down'; } ?>.gif" />
                    <?php } else { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Option ASC'; document.getElementById('frmSort').submit();">Option</a>
                    <?php } ?>
                <?php } else { ?>
                    <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Option ASC'; document.getElementById('frmSort').submit();">Option</a>
                <?php } ?>
            </th>
                    <th><?php if (isset($Ordre)) { ?>
                    <?php if (preg_match("/Supression /", $Ordre)) { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Supression <?php if (preg_match('/ASC/', $Ordre)) { echo 'DESC'; } else { echo 'ASC'; } ?>'; document.getElementById('frmSort').submit();">Supression</a>
                        <img style="border: none;" alt="down" src="<?php echo base_url(); ?>application/resources/admin/images/<?php if (preg_match('/ASC/', $Ordre)) { echo 'up'; } else { echo 'down'; } ?>.gif" />
                    <?php } else { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Supression ASC'; document.getElementById('frmSort').submit();">Supression;</a>
                    <?php } ?>
                <?php } else { ?>
                    <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Supression ASC'; document.getElementById('frmSort').submit();">Supression</a>
                <?php } ?>
            </th>
                </tr>
            </thead>
            <tbody>
            <?php if (count($colArticles)>0) { ?>
                <?php foreach($colArticles as $objArticle) { ?>
                    <tr>
                        <td>
                            <a href="<?php echo site_url("admin/test/fiche_test/".$objArticle->testid); ?>">
                                <?php echo $objArticle->testid; ?>
                            </a>
                        </td>
                        <td><?php if ($highlight == "yes") { EchoWithHighLightWords($objArticle->test, $SearchedWords); } else { echo word_limiter(highlight_phrase(preg_replace("/<[^>]*>/", "",$objArticle->test), $SearchValue, "<span style='background: #FFFF00;'>", "</span>"),10);  } ?></td>
                        <td>&nbsp;</td>

                        <td>
                            <?php if ($highlight == "yes") { EchoWithHighLightWords($objArticle->testid, $SearchedWords); } else { echo word_limiter(highlight_phrase(preg_replace("/<[^>]*>/", "",$objArticle->testid), $SearchValue, "<span style='background: #FFFF00;'>", "</span>"),10);  } ?>
            </td>
                            </a>
                        </td>
                        <td>
                            <a href="<?php echo site_url("admin/test/delete/".$objArticle->testid) ; ?>""  title="Supprimer"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/delete_ico.png"/>
                              
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
            </tbody>
        </table>
        <div id="pager" class="pager">
            <img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
            <img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
            <input type="text" class="pagedisplay"/>
            <img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
            <img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
            <select class="pagesize" style="visibility:hidden">
                <option selected="selected"  value="10">10</option>
                <option value="20">20</option>
                <option value="30">30</option>
                <option  value="40">40</option>
            </select>
        </div>
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>