<?php $data["zTitle"] = 'Référencement SEO';?>
<?php $this->load->view("privicarte/includes/header_mini_2_premium", $data); ?>
<?php $this->load->view("privicarte/vwFicheProfessionnel_platinum_css", $data); ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<div class="container">
    <div class="row mt-5">
        <div class="col-lg-12">
            <div class="greyed">
                <div class="col-lg-12 mb-4" style="padding:0 !important; font-family: Arial; font-size:20px; text-align:center; font-weight:bold;">
                    <div class="container-fluid">
                        <a href="<?php echo base_url('front/utilisateur/contenupro') ?>" class="btn btn-primary" style="text-decoration:none;">Retour menu</a>
                    </div>
                </div>
                <div class="col-lg-12 mb-4" style="color:#000000; font-size:20px; text-align:center; font-weight:bold;">
                    LES PACKS « SEO »
                </div>

                <div class="col-lg-12 text-center pt-4  pb-4"
                     style="font-size: 18px;font-weight: bold;">
                    <?php if(isset($message_error) && $message_error != null){ ?>
                        <p class="alert alert-danger"><?php echo $message_error; ?></p>
                    <?php }elseif(isset($message_success) && $message_success != null){ ?>
                        <p class="alert alert-success"><?php echo $message_success; ?></p>
                    <?php } ?>
                </div>

                <div class="col-lg-12 pb-4">
                    <img class="img-fluid"
                         style="width: 100%;"
                         src="<?php echo base_url('assets/img/seo.jpg') ?>">
                </div>
                <div class="col-lg-12">

                    <form action="" method="post" id="contactform">
                        <div class="div_stl_long_platinum">
                            <label style="color:#ffffff!important;"> page d'acceuil</label>

                            <select name="seo_data[check_page_acceuil]" style="float: right;margin-right: 15px" id="selectBox">
                                <option <?php if(isset($data_seo->check_page_acceuil) && $data_seo->check_page_acceuil == 0){ ?>selected="selected" <?php }?> value="0">Non</option>
                                <option <?php if(isset($data_seo->check_page_acceuil) && $data_seo->check_page_acceuil == 1){ ?>selected="selected" <?php }?> value="1">Oui</option>
                            </select>
                        </div>
                        <div  id="client_graph_form">
                            <div class="container-fluid">
                                <div class="form-group">
                                    <label for="" name="Description_1"> Description 1</label>
                                    <textarea name="seo_data[Description_1]" placeholder="Votre Description 1" class="form-control" type="text" id="textboxenom"><?php if(isset($data_seo->Description_1) && $data_seo->Description_1 != null){ echo $data_seo->Description_1; } ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="" name="Description_2"> Description 2</label>
                                    <textarea name="seo_data[Description_2]" placeholder="Description 2" class="form-control" type="text"  id="textboxedesciption"><?php if(isset($data_seo->Description_2) && $data_seo->Description_2 != null){ echo $data_seo->Description_2; } ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="" name="Description_3"> Description 3</label>
                                    <textarea name="seo_data[Description_3]" placeholder="Description 3" class="form-control" type="text"  id="textboxeadresse"><?php if(isset($data_seo->Description_3) && $data_seo->Description_3 != null){ echo $data_seo->Description_3; } ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="" name="Description_4"> Description 4</label>
                                    <textarea name="seo_data[Description_4]" placeholder="Description 4" class="form-control" type="text" id="textboxecontact"><?php if(isset($data_seo->Description_4) && $data_seo->Description_4 != null){ echo $data_seo->Description_4; } ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="" name="Description_5"> Description 5</label>
                                    <textarea name="seo_data[Description_5]" placeholder="Description 5" class="form-control" type="text" id="textboxecaracteristiques"><?php if(isset($data_seo->Description_5) && $data_seo->Description_5 != null){ echo $data_seo->Description_5; } ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="" name="jour">Jour de changement</label>
                                    <input value="<?php if(isset($data_seo->jour) && $data_seo->jour != null){ echo $data_seo->jour; } ?>" name="seo_data[jour]" placeholder="Nombre de jour de changement" class="form-control" type="text" id="textboxejour">
                                </div>
                                <input value="<?php echo date("Y-m-d"); ?>" name="seo_data[date_debut]" placeholder="Nombre de jour de changement" class="form-control" type="hidden" id="textboxedate">
                            </div>
                        </div>


                        <div class="div_stl_long_platinum">
                            <input type="hidden" value="<?php if(isset($data_seo->domaine_name_check)&& $data_seo->domaine_name_check != null){ echo $data_seo->domaine_name_check; }else{ echo 0;} ?>" id="domaine_name_check" name="seo_data[domaine_name_check]">
                            <input id="nom_de_domaine" type="checkbox"> Nom de Domaine:
                        </div>
                        <div id="domaine_name_content">
                            <div class="container-fluid">
                                <input type="hidden" value="0" id="check_to_valid">
                                <div id="alert_dommaine">

                                </div>
                                <div class="form-group">
                                    <input id="nom_domaine_check" type="text" placeholder="Votre nom de domaine" class="form-control" name="seo_data[domaine_name]" value="<?php if(isset($data_seo->domaine_name)&& $data_seo->domaine_name != null){ echo $data_seo->domaine_name; } ?>">
                                </div>
                                    <button id="check_name_domaine" type="button" class="btn btn-primary">Vérifier</button>
                            </div>
                        </div>

                        <div class="div_stl_long_platinum">
                            <label style="color:#ffffff!important;">  Adresse e-mail personnalisé </label>

                            <select name="seo_data[adresse_check]" style="float: right;margin-right: 15px" id="emailBox">
                                <option <?php if(isset($data_seo->adresse_check)&& $data_seo->adresse_check == 0){ ?>selected="selected" <?php }?> value="0">Non</option>
                                <option <?php if(isset($data_seo->adresse_check)&& $data_seo->adresse_check == 1){ ?>selected="selected" <?php }?> value="1">Oui</option>
                            </select>
                        </div>
                        <div id="showEmail" >
                            <div class="container-fluid">
                                <div class="form-group">
                                    <label for="" name="mail_1"> Adresse e-mail 1</label>
                                    <input name="seo_data[mail_1]" placeholder="Votre mail" class="form-control" type="text" id="textboxes" value="<?php if(isset($data_seo->mail_1) && $data_seo->mail_1 != null){ echo $data_seo->mail_1; } ?>">
                                </div>
                                <div class="form-group">
                                    <label for="" name="mail_2"> Adresse e-mail 2</label>
                                    <input name="seo_data[mail_2]" placeholder="Votre mail" class="form-control" type="text" id="textboxes" value="<?php if(isset($data_seo->mail_2) && $data_seo->mail_2 != null){ echo $data_seo->mail_2; } ?>">
                                </div>
                                <div class="form-group">
                                    <label for="" name="mail_3"> Adresse e-mail 3</label>
                                    <input name="seo_data[mail_3]" placeholder="Votre mail" class="form-control" type="text" id="textboxes" value="<?php if(isset($data_seo->mail_3) && $data_seo->mail_3 != null){ echo $data_seo->mail_3; } ?>">
                                </div>
                            </div>
                        </div>

                        <div class="div_stl_long_platinum">
                            <input type="hidden" value="<?php if(isset($data_seo->dofollow_check) && $data_seo->dofollow_check != null){ echo $data_seo->dofollow_check; }else{echo 0; } ?>" id="dofollow_check"  name="seo_data[dofollow_check]">
                            <input id="dofollow" type="checkbox" value="dofollow"> Liens dofollow:
                        </div>
                        <div id="dofollow_content">
                            <div class="container-fluid">
                                <input type="text" placeholder="Votre lien dofollow" class="form-control" name="seo_data[dofollow]" value="<?php if(isset($data_seo->dofollow) && $data_seo->dofollow != null){ echo $data_seo->dofollow; } ?>">
                            </div>
                        </div>

                        <div class="div_stl_long_platinum">
                            <input type="hidden" value="<?php if(isset($data_seo->account_gmail_check) && $data_seo->account_gmail_check != null){ echo $data_seo->account_gmail_check; }else{echo 0; } ?>" id="account_gmail_check" name="seo_data[account_gmail_check]">
                            <input id="gmail" type="checkbox" value="gmail"> Gmail:
                        </div>
                        <div id="gmail_content">
                            <div class="container-fluid">
                                <input value="<?php if(isset($data_seo->compte_gmail) && $data_seo->compte_gmail != null){ echo $data_seo->compte_gmail; } ?>" type="text" placeholder="Votre gmail" class="form-control" name="seo_data[compte_gmail]">
                            </div>
                        </div>


                        <div class="div_stl_long_platinum">
                            <input type="hidden" value="<?php if(isset($data_seo->analytics_check) && $data_seo->analytics_check != null){ echo $data_seo->analytics_check; }else{echo 0; } ?>"  id="analytics_check" name="seo_data[analytics_check]">
                            <input id="analytics" type="checkbox" value="analytics"> Google Analytics:
                        </div>
                        <div id="analytics_content">
                            <div class="container-fluid">
                                <input value="<?php if(isset($data_seo->analytics) && $data_seo->analytics != null){ echo $data_seo->analytics; } ?>" type="text" placeholder="Add New" class="form-control" name="seo_data[analytics]">
                            </div>
                        </div>

                        <div class="div_stl_long_platinum">
                            <input type="hidden" value="<?php if(isset($data_seo->map_check) && $data_seo->map_check != null){ echo $data_seo->map_check; }else{echo 0; } ?>" id="map_check" name="seo_data[map_check]">
                            <input id="map" type="checkbox" value="maps"> Google Maps:
                        </div>
                        <div id="map_content">
                            <div class="container-fluid">
                                <?php
                                    if (isset($oInfocommercant->Adresse1) && $oInfocommercant->Adresse1 != null){
                                        $get_adresse = str_replace(' ','+',$oInfocommercant->Adresse1);
                                        $get_adresse2 = str_replace("'","+",$get_adresse);

                                    }elseif(isset($oInfocommercant->adresse_localisation) && $oInfocommercant->adresse_localisation != null){
                                        $get_adresse = str_replace(' ','+',$oInfocommercant->adresse_localisation);
                                        $get_adresse2 = str_replace("'","+",$get_adresse);
                                    }else{
                                        $get_adresse2 = '';
                                    }
                                    if($get_adresse2 != ''){
                                        $localisation = $get_adresse2.$oInfocommercant->CodePostal;
                                    }

                                ?>
                                <div id="message_alert">

                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span id="copy_link" style="cursor: pointer" class="input-group-text">Copier</span>
                                    </div>
                                    <input
                                            type="text"
                                            placeholder="Votre lien google maps"
                                            class="form-control"
                                            name="seo_data[maps]"
                                            id="map_link"
                                            value="<iframe width='600' height='450' frameborder='0' style='border:0' src='https://www.google.com/maps/embed/v1/search?key=AIzaSyCAYWEKvmon3SQTCtoRPQJUhrAXie_DSEA&q=<?php echo $localisation; ?>&zoom=18' allowfullscreen ></iframe>"
                                            disabled="disabled"
                                    >
                                </div>
                                <iframe
                                        width="600"
                                        height="450"
                                        frameborder="0" style="border:0"
                                        src="https://www.google.com/maps/embed/v1/search?key=AIzaSyCAYWEKvmon3SQTCtoRPQJUhrAXie_DSEA&q=<?php echo $localisation; ?>&zoom=18"
                                        allowfullscreen
                                >
                                </iframe>
                            </div>
                        </div>


                        <div class="div_stl_long_platinum">
                            <input type="hidden" value="<?php if(isset($data_seo->google_search_check) && $data_seo->google_search_check != null){ echo $data_seo->google_search_check; }else{echo 0; } ?>" id="google_search_check" name="seo_data[google_search_check]">
                            <input id="google_search" type="checkbox" value="google_search"> Google search:
                        </div>
                        <div id="google_search_content">
                            <div class="container-fluid">
                                <input value="<?php if(isset($data_seo->google_search) && $data_seo->google_search != null){ echo $data_seo->google_search; } ?>" type="text" placeholder="Add New" class="form-control" name="seo_data[google_search]">
                            </div>
                        </div>

<!--                        <input id="alternatif" type="checkbox" value="alternatif">Texte altérnatif pour les images: <br>-->
<!--                        <input type="text" placeholder="Add New" class="form-control" name="alternatif">-->
                        <input type="hidden" value="<?php echo $oInfocommercant->IdCommercant; ?>" name="seo_data[IdCommercant]">
                        <div class="col-lg-12 mt-3 mb-5 text-center">
                            <input id="submit_all" class="btn btn-success w-50" type="submit" value="Valider" name="submit">
                        </div>


                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
      $("#client_graph_form").hide();
      $("#map_content").hide();
      $("#google_search_content").hide();
      $("#showEmail").hide();
      $("#dofollow_content").hide();
      $("#domaine_name_content").hide();
      $("#gmail_content").hide();
      $("#analytics_content").hide();

      /*ready function */


      if($('#selectBox').val() != 0){
          $("#client_graph_form").show();
      }else{
          $("#client_graph_form").hide();
      }
      if($('#domaine_name_check').val() != 0){
          $("#domaine_name_content").show();
          $("#nom_de_domaine").prop('checked',true);
      }else{
          $("#domaine_name_content").hide();
          $("#nom_de_domaine").prop('checked',false);
      }
      if($('#emailBox').val() != 0){
          $("#showEmail").show();
      }else{
          $("#showEmail").hide();
      }
      if($('#dofollow_check').val() != 0){
          $("#dofollow_content").show();
          $('#dofollow').prop('checked',true);
      }else{
          $("#dofollow_content").hide();
          $('#dofollow').prop('checked',false);
      }
      if($('#account_gmail_check').val() != 0){
          $("#gmail_content").show();
          $('#gmail').prop('checked',true);
      }else{
          $("#gmail_content").hide();
          $('#gmail').prop('checked',false);
      }
      if($('#analytics_check').val() != 0){
          $("#analytics_content").show();
          $('#analytics').prop('checked',true);
      }else{
          $("#analytics_content").hide();
          $('#analytics').prop('checked',false);
      }
      if($('#map_check').val() != 0){
          $("#map_content").show();
          $('#map').prop('checked',true);
      }else{
          $("#map_content").hide();
          $('#map').prop('checked',false);
      }
      if($('#google_search_check').val() != 0){
          $("#google_search_content").show();
          $('#google_search').prop('checked',true);
      }else{
          $("#google_search_content").hide();
          $('#google_search').prop('checked',false);
      }

        /*ready function */


        /*function change */
        $('#selectBox').on('change', function() {
          if ( this.value == '1')
          {
            $("#client_graph_form").show();
          }
          else
          {
            $("#client_graph_form").hide();
          }
        });
      $('#emailBox').on('change', function() {
          if ( this.value == '1')
          {
            $("#showEmail").show();
          }
          else
          {
            $("#showEmail").hide();
          }
        });
      $('#nom_de_domaine').change(function(){
          var value_check = $('#domaine_name_check').val();
            if(value_check  != 0){
                $('#domaine_name_check').val('0');
                $("#domaine_name_content").hide();
                $('#submit_all').prop('disabled',false);
            }else{
                $('#domaine_name_check').val('1');
                $("#domaine_name_content").show();
            }
      });
      $('#dofollow').change(function(){
          var value_check_dof = $('#dofollow_check').val();
            if(value_check_dof  != 0){
                $('#dofollow_check').val('0');
                $("#dofollow_content").hide();
            }else{
                $('#dofollow_check').val('1');
                $("#dofollow_content").show();
            }
      });
      $('#gmail').change(function(){
          var value_check_mail = $('#account_gmail_check').val();
            if(value_check_mail  != 0){
                $('#account_gmail_check').val('0');
                $("#gmail_content").hide();
            }else{
                $('#account_gmail_check').val('1');
                $("#gmail_content").show();
            }
      });
      $('#analytics').change(function(){
          var value_check_analytics = $('#analytics_check').val();
            if(value_check_analytics  != 0){
                $('#analytics_check').val('0');
                $("#analytics_content").hide();
            }else{
                $('#analytics_check').val('1');
                $("#analytics_content").show();
            }
      });
      $('#map').change(function(){
          var value_check_map = $('#map_check').val();
            if(value_check_map  != 0){
                $('#map_check').val('0');
                $("#map_content").hide();
            }else{
                $('#map_check').val('1');
                $("#map_content").show();
            }
      });
      $('#google_search').change(function(){
          var value_check_google_search = $('#google_search_check').val();
            if(value_check_google_search  != 0){
                $('#google_search_check').val('0');
                $("#google_search_content").hide();
            }else{
                $('#google_search_check').val('1');
                $("#google_search_content").show();
            }
      });
        /*function change */

        /*check domaine name */
      $('#check_name_domaine').click(function(){
            var domaine_name = $('#nom_domaine_check').val();
            $.ajax({
              url: '<?php echo site_url('front/utilisateur/verification_domaine') ?>',
              type: 'POST',
              data: 'domaine_name='+domaine_name,
              async: true,
              success: function (datas) {
                  if(datas != 1){
                      $('#alert_dommaine').html('<p class="alert alert-success">Votre nom de domaine est valide</p>');
                      $('#check_to_valid').val('2');
                      $('#submit_all').prop('disabled',false);
                  }else{
                      $('#alert_dommaine').html('<p class="alert alert-danger">Votre nom de domaine est déjà prise, veuillez en saisir une autre!</p>');
                      $('#check_to_valid').val('1');
                      $('#submit_all').prop('disabled', true);
                  }

              }
          });
      });
        /*check domaine name */

        /* copy clipbord */
        $("#copy_link").click(function() {
            copyToClipboard($("#map_link").val());
            $('#message_alert').html('<span class="alert alert-success">Lien copier</span>');

        })
    });
    function copyToClipboard(e) {
        var tempItem = document.createElement('input');

        tempItem.setAttribute('type','text');
        tempItem.setAttribute('display','none');

        let content = e;
        if (e instanceof HTMLElement) {
            content = e.innerHTML;
        }

        tempItem.setAttribute('value',content);
        document.body.appendChild(tempItem);

        tempItem.select();
        document.execCommand('Copy');

        tempItem.parentElement.removeChild(tempItem);
    }
    /* copy clipbord */
</script>