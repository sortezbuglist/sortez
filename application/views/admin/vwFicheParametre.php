<?php $data["zTitle"] = 'Fiche param&eacute;trage'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>


<div id="divFicheParametre" class="content" align="center" style="text-align:center;">
	     <p><br><a style = "color:black; text-decoration:none;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input class="btn btn-primary" type = "button" value= "retour au menu" id = "btn" onclick="document.location='<?php echo site_url("admin/home/");?>';"/>  </a>
		 </p>
        <form id="frmParametre" name="frmParametre" method="POST" action="<?php echo site_url("admin/parametres/modifier"); ?>" enctype="multipart/form-data">
            <table width="800" cellpadding="0" cellspacing="0" style="text-align:center;" align="center">
                <?php foreach($colParametres as $key => $objParemetre) { ?>
                    <?php if($objParemetre->Code != "PhotoHeader") { ?>
                        <tr>
                            <td>
                                <br><div class="H1-C"><?php echo htmlspecialchars($objParemetre->Nom); ?></div><br>
                                <textarea id="Parametre<?php echo $key; ?>" style="width:500px; height: 200px;" name="Parametre[<?php echo $key; ?>][Contenu]"><?php echo htmlspecialchars($objParemetre->Contenu); ?></textarea>
                                <?php echo display_ckeditor(($key == 0) ? $ckeditor0 : $ckeditor1); ?>
                                <input type="hidden" name="Parametre[<?php echo $key; ?>][IdParametre]" value="<?php echo $objParemetre->IdParametre; ?>" />
                            </td>
                        </tr>
                    <?php } else { ?>
                        <tr>
                            <td>
                                <br><div class="H1-C"><?php echo htmlspecialchars($objParemetre->Nom); ?></div><br>
                                Logo actuel : <img src="<?php echo GetImagePath("front/"); ?>/logos/<?php echo $objParemetre->Contenu; ?>" /><br/>
                                Nouveau logo:<input type="file" name="PhotoHeader" value="" />
                                <input type="hidden" name="Parametre[IdParametre]" value="<?php echo $objParemetre->IdParametre; ?>" />
                                <input type="hidden" name="Parametre[Type]" value="logo" />
                            </td>
                        </tr>
                    <?php } ?>
                <?php } ?>
            </table>
            <p><input class="btn btn-success" type="button" id="btnEnvoyer" value="Enregistrer" /></p>
        </form>
    </div>
    <script type="text/javascript">
        jQuery("#btnEnvoyer").click(function () {
            <?php if($Type != "logo") { ?>
                CKEDITOR.instances.Parametre0.updateElement();
                CKEDITOR.instances.Parametre1.updateElement();
            <?php } ?>
            jQuery("#frmParametre").submit();
        });
    </script>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>