<?php $data["zTitle"] = 'Enregistrer une Association'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script type="text/javascript" src="<?php echo GetJsPath("front/");?>/fields.check.js"></script>

    <script>
	
	function getCP(){
	      var ivilleId = jQuery('#txtVille').val();     
          jQuery.get(
            '<?php echo site_url("front/particuliers/getPostalCodeadmin"); ?>' + '/' + ivilleId,
            function (zReponse)
            {
                // alert (zReponse) ;
                jQuery('#txtCodePostal').val(zReponse) ;       
               
             
           });
	}
	
	function submit_btnSinscrire() {
		
				
				
				var txtError = "";
				
				var txtNom = $("#txtNom").val();
				if(txtNom=="") {
					txtError += "- Veuillez indiquer le Nom de l'Association <br/>";    
				}
				
				var txtCodePostal = $('#txtCodePostal').val();
				  if (txtCodePostal == "") {
					  txtError += "- Vous devez indiquer un code postal <br/>";
				  }
				  
				var ivilleId = $('#txtVille').val();
				if (ivilleId == 0) {
				  txtError += "- Vous devez selectionner une ville <br/>";
				}
				
				<?php if($prmId==0) { ?>
				var txtLogin = $("#txtLogin").val();
				if(!isEmail(txtLogin)) {
					txtError += "- Votre login doit &ecirc;tre un Email valide <br/>";    
				}
				
				var txtLogin_verif = $('#txtLogin_verif').val();
				  if (txtLogin_verif == 1) {
					  txtError += "- Votre Login existe d&eacute;j&agrave; sur notre site <br/>";
				  }
				  
				
				var txtPassword = $("#txtPassword").val();
				if(txtPassword=="") {
					txtError += "- Veuillez indiquer un Password <br/>";    
				}
				
				if($("#txtPassword").val() != $("#txtConfirmPassword").val()) {
						txtError += "- Les deux mots de passe ne sont pas identiques. <br/>";
				}
				<?php } ?>
				
				
				$("#divErrortxtInscription").html(txtError);
				//alert(txtError);
				
				if(txtError == "") {
					$("#frmModificationParticulier").submit();
				}
			
		
		}
		
		
        $(document).ready(function() {
			
			/*$("#txtDateNaissance" ).datepicker({
				dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
				dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
				monthNames: ['Janvier','F�vier','Mars','Avril','Mai','Juin','Juillet','Ao�t','Septembre','Octobre','Novembre','D�cembre'],
				dateFormat: 'DD, d MM yy',
				autoSize: true,
				changeMonth: true,
	            changeYear: true,
				yearRange: '1900:2020'
			});
			*/
			
			jQuery("#txtLogin").blur(function(){
										   //alert('cool');
										   var txtLogin = jQuery("#txtLogin").val();
										   var txtEmail = txtLogin;
										   
										   var value_result_to_show = "0";
										   
										   //alert('<?php //echo site_url("front/particuliers/verifier_login"); ?>' + '/' + txtLogin);
										   //jQuery(".EmailLoading").addClass("EmailLoading_show").removeClass("EmailLoading");
										   jQuery.post(
													'<?php echo site_url("front/particuliers/verifier_login");?>',
													{ txtLogin_var: txtLogin },
													function (zReponse)
													{
														//alert (zReponse) ;
														if (zReponse == "1") {
															value_result_to_show = "1";
														}  
														
														
														if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtLogin_').html(result_to_show);      
															$('#txtLogin_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
															$('#divErrortxtLogin_').html(result_to_show);    
															$('#txtLogin_verif').val("0");
														}
														
													 
												   });
												   
												   
											$.post(
													'<?php echo site_url("front/professionnels/verifier_login_ionauth");?>',
													{ txtLogin_var_ionauth: txtLogin },
													function (zReponse_ionauth)
													{
														//alert (zReponse) ;
														if (zReponse_ionauth == "1") {
															value_result_to_show = "1";
														} 
														
														
														if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtLogin_').html(result_to_show);       
															$('#txtLogin_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
															$('#divErrortxtLogin_').html(result_to_show);    
															$('#txtLogin_verif').val("0");
														}
													 
												   });
												   
											
											jQuery.post(
													'<?php echo site_url("front/particuliers/verifier_email");?>',
													{ txtEmail_var: txtEmail },
													function (zReponse_)
													{
														//alert (zReponse) ;
														if (zReponse_ == "1") {
															value_result_to_show = "1";
														}
														
														
														if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtLogin_').html(result_to_show);      
															$('#txtEmail_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
															$('#divErrortxtLogin_').html(result_to_show);    
															$('#txtEmail_verif').val("0");
														}
														
													 
												   });
												   
												   
											$.post(
													'<?php echo site_url("front/professionnels/verifier_email_ionauth");?>',
													{ txtEmail_var_ionauth: txtEmail },
													function (zReponse_ionauth)
													{
														//alert (zReponse) ;
														//var zReponse_html_ionauth = '';
														if (zReponse_ionauth == "1") {
															value_result_to_show = "1";
														} 
														
														
														
													 	if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtLogin_').html(result_to_show);       
															$('#txtEmail_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
															$('#divErrortxtLogin_').html(result_to_show);  
															$('#txtEmail_verif').val("0");
														}
														
														
												   });
										  
										   jQuery(".FieldError").removeClass("FieldError");
										   //jQuery(".EmailLoading").addClass("EmailLoading").removeClass("EmailLoading_show");
										   
										   });
			
						
			
        });
		
	
		
	function CP_getDepartement() {
		//alert(jQuery('#CodePostalSociete').val());
		jQuery('#departementCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
		var CodePostalSociete = jQuery('#txtCodePostal').val();
			jQuery.post(
					'<?php echo site_url("front/professionnels/departementcp_particulier"); ?>',
					{CodePostalSociete:CodePostalSociete},
					function (zReponse)
					{
						jQuery('#departementCP_container').html(zReponse);
					});
	}
	
	function CP_getVille() {
		//alert(jQuery('#CodePostalSociete').val());
		jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
		var CodePostalSociete = jQuery('#txtCodePostal').val();
			jQuery.post(
					'<?php echo site_url("front/professionnels/villecp_particulier"); ?>',
					{CodePostalSociete:CodePostalSociete},
					function (zReponse)
					{
						jQuery('#villeCP_container').html(zReponse);
					});
	}
	
	function CP_getVille_D_CP() {
		//alert(jQuery('#CodePostalSociete').val());
		jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
		var CodePostalSociete = jQuery('#txtCodePostal').val();
		var departement_id = jQuery('#departement_id').val();
			jQuery.post(
					'<?php echo site_url("front/professionnels/villecp_particulier"); ?>',
					{CodePostalSociete:CodePostalSociete, departement_id: departement_id},
					function (zReponse)
					{
						jQuery('#villeCP_container').html(zReponse);
					});
	}	
		
		
		
    </script>
    
    <script type="text/javascript">
		function return_to_liste_user() {
			document.location = "<?php echo site_url(); ?>admin/users/liste/";
		}
	</script>
    
    <div id="divInscriptionParticulier" class="content" align="center">
	      <br>
	     <a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input class="btn btn-default" type = "button" value= "retour au menu" id = "btn"/> </a>
        <h1>Enregistrer une Association</h1>
        <?php if(!empty($objUser) && $objUser->IdAssociation != "0") { ?>
            <form name="frmModificationParticulier" id="frmModificationParticulier" action="<?php echo site_url("admin/associations/fiche_modifier"); ?>" method="POST" enctype="multipart/form-data">
        <?php } else { ?>
            <form name="frmModificationParticulier" id="frmModificationParticulier" action="<?php echo site_url("admin/associations/fiche_ajouter"); ?>" method="POST" enctype="multipart/form-data">
        <?php } ?>
            <table width="75%" cellpadding="0" cellspacing="0">
                
                <tr>
                    <td>
                        <label>Nom de l'association : </label>
                    </td>
                    <td>
                        <input class="form-control" type="text" name="Particulier[Nom]" id="txtNom" value="<?php if(!empty($objUser->Nom)) echo htmlspecialchars(stripcslashes($objUser->Nom)); ?>" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>Nom du r&eacute;sponsable : </label>
                    </td>
                    <td>
                        <input class="form-control" type="text" name="Particulier[NomResp]" id="txtNom" value="<?php if(!empty($objUser->NomResp)) echo htmlspecialchars(stripcslashes($objUser->NomResp)); ?>" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>Pr&eacute;nom du r&eacute;sponsable : </label>
                    </td>
                    <td>
                        <input class="form-control" type="text" name="Particulier[PrenomResp]" id="txtPrenom" value="<?php if(!empty($objUser->PrenomResp)) echo htmlspecialchars(stripcslashes($objUser->PrenomResp)); ?>" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>Adresse : </label>
                    </td>
                    <td>
                        <textarea name="Particulier[adresse]" id="txtAdresse" class="form-control"><?php if(!empty($objUser->adresse)) echo htmlspecialchars(stripcslashes($objUser->adresse)); ?></textarea>
                    </td>
                </tr>
                
                
                
                <tr>
                    <td class="td_part_form">
                        <label>Code postal : </label>
                    </td>
                     <td id ="trReponseVille" name = "trReponseVille"  class="td_part_form">
                        <input class="form-control" type="text" name="Particulier[CodePostal]" id="txtCodePostal" style = "width:273px;" value="<?php if( isset ($objUser) && is_object($objUser)) { echo $objUser->CodePostal; }?>" onblur="javascript:CP_getDepartement();CP_getVille();"/>
                    </td>
                </tr>
                
                
                <tr>
                    <td class="td_part_form">
                        <label>Departement : </label>
                  </td>
                    <td class="td_part_form">
                        <span id="departementCP_container">
                        <select name="Particulier__departement_id" id="departement_id" class="stl_long_input_platinum form-control" disabled="disabled" onchange="javascript:CP_getVille_D_CP();" style="width:273px;">
                            <option value="0">-- Choisir --</option>
                            <?php if(sizeof($colDepartement)) { ?>
                                <?php foreach($colDepartement as $objDepartement) { ?>
                                    <option value="<?php echo $objDepartement->departement_id; ?>"><?php echo $objDepartement->departement_nom; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="td_part_form">
                        <label>Ville : </label>
                  </td>
                    <td class="td_part_form">
                        <span id="villeCP_container">
                        <input class="form-control" type="text" value="<?php if (isset($objUser->IdVille)) echo $this->mdlville->getVilleById($objUser->IdVille)->Nom;?>" name="IdVille_Nom_text" id="IdVille_Nom_text" disabled="disabled" style="width:273px;"/>
                        <input class="form-control" type="hidden" value="<?php if (isset($objUser->IdVille)) echo $objUser->IdVille;?>" name="Particulier[IdVille]" id="txtVille"/>
                        </span>
                    </td>
                </tr>
                
                
                <tr>
                    <td>
                        <label>N&deg; T&eacute;l&eacute;phone fixe : </label>
                    </td>
                    <td>
                        <input class="form-control" type="text" name="Particulier[tel]" id="txtTelephone" value="<?php if(!empty($objUser->tel)) echo htmlspecialchars(stripcslashes($objUser->tel)); ?>" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>N&deg; T&eacute;l&eacute;phone Portable : </label>
                    </td>
                    <td>
                        <input class="form-control" type="text" name="Particulier[mobile]" id="txtPortable" value="<?php if(!empty($objUser->mobile)) echo htmlspecialchars(stripcslashes($objUser->mobile)); ?>" />
                    </td>
                </tr>
                
                
				<?php if($prmId==0) { ?>
                
                <tr>
                    <td>
                        <label>Login / Email : </label>
                    </td>
                    <td>
                        <input class="form-control" type="text" name="Particulier[Email]" style = "width:273px;" id="txtLogin" value="<?php if(!empty($objUser->Email)) echo htmlspecialchars(stripcslashes($objUser->Email)); ?>" />
                        <div class="FieldError" id="divErrortxtLogin_"></div>
                        <input class="form-control" type="hidden" name="txtLogin_verif" style = "width:273px;" id="txtLogin_verif" value="0"/>
                    </td>
                </tr>
                
                
                
                <tr>
                    <td class="td_part_form">
                        <label>Password *: </label>
                    </td>
                    <td class="td_part_form">
                        <input class="form-control" type="password" name="Particulier_Password" style = "width:273px;" id="txtPassword" value="" />
                    </td>
                </tr>
                
                <tr>
                    <td class="td_part_form">
                        <label>Confirm Password *: </label>
                    </td>
                    <td class="td_part_form">
                        <input class="form-control" type="password" id="txtConfirmPassword" style = "width:273px;" value="" />
						<div class="FieldError" id="divErrortxtPassword"></div>
                    </td>
                </tr>
                <?php } ?>
                
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                     <div id="divErrortxtInscription" style="padding:0 0 15px 2px; font-family: Arial; font-size: 12px; color:#F00; margin-top:20px; text-align:left;"></div>
                    </td>
                </tr>
                
                
                <tr>
                    <td>
                        <?php if(!empty($objUser->IdAssociation) && $objUser->IdAssociation != 0) { ?>
                            <input class="form-control" type="hidden" name="Particulier[IdAssociation]" id="txtIdUser" value="<?php if(!empty($objUser->IdAssociation)) echo htmlspecialchars(stripcslashes($objUser->IdAssociation)); ?>" /></td>
                        <?php } ?>
                    <td align="left"><input class="btn btn-success" type="button" onclick="javascript:submit_btnSinscrire();" id="btnSinscrire" name="btnSinscrire" value="Enregistrer" />&nbsp;<input class="btn btn-error" type="button" value="Annuler" onclick="javascript:return_to_liste_user();"/></td>
                </tr>
                
            </table>
        </form>
    </div>
    
    
   
    
<?php $this->load->view("admin/includes/vwFooter2013"); ?>