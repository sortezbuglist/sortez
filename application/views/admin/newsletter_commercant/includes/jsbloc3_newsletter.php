<script type="text/javascript">
    <?php if (isset($objbloc_newsletter->is_activ_bloc3) AND $objbloc_newsletter->is_activ_bloc3 == 1) {  ?>
        jQuery('#type_bloc3_choose').css("display", "block");
        <?php if (isset($objbloc_newsletter->type_bloc3) AND $objbloc_newsletter->type_bloc3 == '1'){  ?>
            document.getElementById('bloc3_1').style.display = "block";
            document.getElementById('bloc3_2').style.display = "none";
            $('#type_bloc3js').val("1");
            $("#bloc3_1").removeClass("col-lg-6");
            $("#bloc3_2").removeClass("col-lg-6");
        <?php } ?>
        <?php if (isset($objbloc_newsletter->type_bloc3) AND $objbloc_newsletter->type_bloc3 == '2') {  ?>
            document.getElementById('bloc3_1').style.display = "block";
            document.getElementById('bloc3_2').style.display = "block";
            $('#type_bloc3js').val("2");
            $("#bloc3_1").addClass("col-lg-6");
            $("#bloc3_2").addClass("col-lg-6");
        <?php } ?>
        <?php if (isset($objbloc_newsletter->is_activ_btn_bloc3) && $objbloc_newsletter->is_activ_btn_bloc3 == '1') {  ?>
            document.getElementById('link_bloc3_1').style.display = "block";
        <?php } ?>
        <?php if (isset($objbloc_newsletter->is_activ_btn_bloc3_2) && $objbloc_newsletter->is_activ_btn_bloc3_2 == '1') {  ?>
            document.getElementById('link_bloc3_2').style.display = "block";
        <?php } ?>
    <?php } ?>
    $("#is_activ_bloc3").change(function () {
        if (document.getElementById('is_activ_bloc3').value == 0) {
            jQuery('#type_bloc3_choose').css("display", "none");

            jQuery('#bloc3_1').css("display", "none");

            jQuery('#bloc3_2').css("display", "none");

        }
        else if (document.getElementById('is_activ_bloc3').value == 1) {
            jQuery('#type_bloc3_choose').css("display", "block");

            jQuery('#bloc3_1').css("display", "none");

            jQuery('#bloc3_2').css("display", "none");

        }
    });
    $("#is_activ_bloc3").change(function () {
        if (document.getElementById('is_activ_bloc3').value == 0) {
            jQuery('#type_bloc3_choose').css("display", "none");

            jQuery('#bloc3_1').css("display", "none");

            jQuery('#bloc3_2').css("display", "none");

        }
        else if (document.getElementById('is_activ_bloc3').value == 1) {
            jQuery('#type_bloc3_choose').css("display", "block");

            jQuery('#bloc3_1').css("display", "none");

            jQuery('#bloc3_2').css("display", "none");

        }
    });
    $("#1b3").click(function () {
        document.getElementById('bloc3_1').style.display = "block";
        document.getElementById('bloc3_2').style.display = "none";
        $('#type_bloc3js').val("1");
        $("#bloc3_1").removeClass("col-lg-6");
        $("#bloc3_2").removeClass("col-lg-6");
    });
    $("#2b3").click(function () {
        document.getElementById('bloc3_1').style.display = "block";
        document.getElementById('bloc3_2').style.display = "block";
        $('#type_bloc3js').val("2");
        $("#bloc3_1").addClass("col-lg-6");
        $("#bloc3_2").addClass("col-lg-6");

    });
    $("#link_btn_bloc3_1").change(function () {

        if (document.getElementById('link_btn_bloc3_1').value == 1) {
            document.getElementById('link_bloc3_1').style.display = "block";

        }
        else if (document.getElementById('link_btn_bloc3_1').value == 0) {
            document.getElementById('link_bloc3_1').style.display = "none";

        }

    });
    $("#link_btn_bloc3_2").change(function () {


        if (document.getElementById('link_btn_bloc3_2').value == 1) {
            document.getElementById('link_bloc3_2').style.display = "block";

        }
        else if (document.getElementById('link_btn_bloc3_2').value == 0) {
            document.getElementById('link_bloc3_2').style.display = "none";

        }

    });

</script>