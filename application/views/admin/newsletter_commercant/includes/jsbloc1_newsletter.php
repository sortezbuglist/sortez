<script type="text/javascript">
        <?php if (isset($objbloc_newsletter->is_activ_bloc) AND $objbloc_newsletter->is_activ_bloc == 1) {  ?>
                jQuery('#type_bloc_choose').css("display", "block");
            <?php if (isset($objbloc_newsletter->type_bloc) AND $objbloc_newsletter->type_bloc == '1'){  ?>
                document.getElementById('bloc1_1').style.display = "block";
                document.getElementById('bloc1_2').style.display = "none";
                $('#type_blocjs').val("1");
                $("#bloc1_1").removeClass("col-lg-6");
                $("#bloc1_2").removeClass("col-lg-6");
            <?php } ?>
            <?php if (isset($objbloc_newsletter->type_bloc) AND $objbloc_newsletter->type_bloc == '2') {  ?>
                document.getElementById('bloc1_1').style.display = "block";
                document.getElementById('bloc1_2').style.display = "block";
                $('#type_blocjs').val("2");
                $("#bloc1_1").addClass("col-lg-6");
                $("#bloc1_2").addClass("col-lg-6");
            <?php } ?>
            <?php if (isset($objbloc_newsletter->is_activ_btn_bloc1) && $objbloc_newsletter->is_activ_btn_bloc1 == '1') {  ?>
                document.getElementById('link_bloc1_1').style.display = "block";
            <?php } ?>
            <?php if (isset($objbloc_newsletter->is_activ_btn_bloc1_2) && $objbloc_newsletter->is_activ_btn_bloc1_2 == '1') {  ?>
                document.getElementById('link_bloc1_2').style.display = "block";
            <?php } ?>
        <?php } ?>
        $("#is_activ_bloc").change(function () {
            if (document.getElementById('is_activ_bloc').value == 0) {
                jQuery('#type_bloc_choose').css("display", "none");

                jQuery('#bloc1_1').css("display", "none");

                jQuery('#bloc1_2').css("display", "none");
            }
            else if (document.getElementById('is_activ_bloc').value == 1) {
                jQuery('#type_bloc_choose').css("display", "block");
                
                jQuery('#bloc1_1').css("display", "none");

                jQuery('#bloc1_2').css("display", "none");
            }
        });
        $("#1b1").click(function () {
            document.getElementById('bloc1_1').style.display = "block";
            document.getElementById('bloc1_2').style.display = "none";
            $('#type_blocjs').val("1");
            $("#bloc1_1").removeClass("col-lg-6");
            $("#bloc1_2").removeClass("col-lg-6");

        });
        $("#2b1").click(function () {
            document.getElementById('bloc1_1').style.display = "block";
            document.getElementById('bloc1_2').style.display = "block";
            $('#type_blocjs').val("2");
            $("#bloc1_1").addClass("col-lg-6");
            $("#bloc1_2").addClass("col-lg-6");
        });

        $("#link_btn_bloc1_1").change(function () {

            if (document.getElementById('link_btn_bloc1_1').value == 1) {
                document.getElementById('link_bloc1_1').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc1_1').value == 0) {
                document.getElementById('link_bloc1_1').style.display = "none";

            }

        });
        $("#link_btn_bloc1_2").change(function () {


            if (document.getElementById('link_btn_bloc1_2').value == 1) {
                document.getElementById('link_bloc1_2').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc1_2').value == 0) {
                document.getElementById('link_bloc1_2').style.display = "none";

            }

        });
</script>