<?php $data["empty"] = null; ?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php if (isset($zTitle)) echo $zTitle; ?></title>
    <link href="<?php echo Base_Url('assets/css/bootstrap.css') ?>" rel="stylesheet">

    <style type="text/css">
        body {
            margin: 0px;
            padding: 0px;
            font-family: arial !important;
            font-size: 12px;
            line-height: 1.25em;
            margin: 0px;
            padding: 0px;
            background-image: url(https://www.sortez.org/wpimages/wpe3440087_06.jpg) !important;
            background-color: #ffffff;
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-position: center top;
            background-size: 100% 100%;
        }

        img {
            border: none;
        }

        .contener2013 {
            height: auto;
            width: 940px;
            margin-right: auto;
            margin-left: auto;
            position: relative;
            margin-top: 0px;
        }

        .maincontener2013 {
            background-color: #FFFFFF;
            float: left;
            height: auto;
            position: relative;
            width: 940px;
        }

        #table_form_inscriptionpartculier .td_part_form {
            text-align: left;
            font-weight: bold;
        }

        #table_form_inscriptionpartculier {
            text-align: center;
        }

        label {
            color: #000000 !important;
            white-space: normal !important;
        }
        @media (min-width: 992px)
            .col-lg-4 {
                -webkit-box-flex: 0;
                -webkit-flex: 0 0 33.333333%;
                -ms-flex: 0 0 33.333333%;
                flex: 0 0 33.333333%;
                max-width: 33.333333%;
            }

    </style>

        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jquery.js"></script>

    <script type="text/javascript">
        //variables globales
        gCONFIG = new Array();
        gCONFIG["BASE_URL"] = "<?php echo base_url(); ?>";
        gCONFIG["SITE_URL"] = "<?php echo site_url(); ?>";
    </script>
    <?php
    if (isset($oInfoCommercant)) {
        $thisss =& get_instance();
        $thisss->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");
        $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);
        if (isset($user_ion_auth_id)) $user_groups = $thisss->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
        if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;
        //echo $nombre_annonce_com."qsdfqsdfffff".$group_id_commercant_user;
    } else {
        $group_id_commercant_user = 0;
    }
    ?>
    <link href="<?php echo GetCssPath("bootstrap/"); ?>/bootstrap-theme.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <script type="text/javascript" src="<?php echo GetJsPath("privicarte/"); ?>/jquery-1.11.3.min.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.4.1.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript"
            src="<?php echo base_url() . "application/resources/fancybox"; ?>/lib/jquery.mousewheel-3.0.6.pack.js"></script>

    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript"
            src="<?php echo base_url() . "application/resources/fancybox"; ?>/source/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url() . "application/resources/fancybox"; ?>/source/jquery.fancybox.css?v=2.1.5"
          media="screen"/>


    <script type="text/javascript" src="<?php echo GetJsPath("privicarte/"); ?>/global.js"></script>

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script type="text/javascript" src="<?php echo GetJsPath("privicarte/"); ?>/ie-emulation-modes-warning.js"></script>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php if (isset($currentpage) && $currentpage == "publightbox") {
    } else $this->load->view("privicarte/includes/global_js", $data); ?>
    <?php $this->load->view("privicarte/includes/global_css", $data); ?>

    <link href="<?php echo GetCssPath("privicarte/"); ?>/global.css" rel="stylesheet" type="text/css">
    <link href="<?php echo GetCssPath("privicarte/"); ?>/responsive_min_768.css" rel="stylesheet" type="text/css">
    <link href="<?php echo GetCssPath("privicarte/"); ?>/responsive_min_992.css" rel="stylesheet" type="text/css">
    <link href="<?php echo GetCssPath("privicarte/"); ?>/responsive_min_1200.css" rel="stylesheet" type="text/css">


    <?php
    //LOCALDATA FILTRE
    $this_session_localdata =& get_instance();
    $this_session_localdata->load->library('session');
    $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
    $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
    //LOCALDATA FILTRE
    ?>

    <?php if ($group_id_commercant_user == 5) { ?>
        <link href="<?php echo GetCssPath("privicarte/"); ?>/navbar-fixed-top.css" rel="stylesheet" type="text/css">

        <?php
        $path_img_gallery_bg = 'application/resources/front/photoCommercant/imagesbank/' . $oInfoCommercant->user_ionauth_id . '/bg/';
        $path_img_gallery_bg_sample = 'application/resources/front/photoCommercant/imagesbank/bg_sample/';
        $path_img_gallery_old = 'application/resources/front/photoCommercant/images/';
        $filename = "";
        if (file_exists($path_img_gallery_bg . $oInfoCommercant->background_image)) {
            $background_image_to_show = base_url() . $path_img_gallery_bg . $oInfoCommercant->background_image;
            $filename = $path_img_gallery_bg . $oInfoCommercant->background_image;
        } else if (file_exists($path_img_gallery_bg_sample . $oInfoCommercant->background_image)) {
            $background_image_to_show = base_url() . $path_img_gallery_bg_sample . $oInfoCommercant->background_image;
            $filename = $path_img_gallery_bg_sample . $oInfoCommercant->background_image;
        } else {
            $background_image_to_show = base_url() . $path_img_gallery_old . $oInfoCommercant->background_image;
            $filename = $path_img_gallery_old . $oInfoCommercant->background_image;
        }

        if (isset($oInfoCommercant->background_image) &&
            $oInfoCommercant->background_image != "" &&
            $oInfoCommercant->background_image != NULL &&
            file_exists($filename) &&
            $oInfoCommercant->bg_default_image != "1") { ?>
            <style type="text/css">
                body {
                    background-image: url("<?php echo $background_image_to_show; ?>") !important;
                    background-repeat: no-repeat !important;
                    background-attachment: fixed !important;
                    background-size: 100% 100% !important;
                }
            </style>
        <?php } ?>


    <?php } else if ((isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0" && $localdata_IdVille != NULL && !isset($oInfoCommercant)) || (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0" && $localdata_IdDepartement != NULL && !isset($oInfoCommercant))) { ?>

        <style type="text/css">
            body {
                margin-top: 0 !important;
                background: #FFFFFF !important;
            }
        </style>

    <?php } ?>


    <?php
    if ((isset($oInfoCommercant->bg_default_color_container) && $oInfoCommercant->bg_default_color_container == "1" && $group_id_commercant_user == 5) || $group_id_commercant_user == 4) { ?>
        <style type="text/css">
            .bg_gris_225 {
                background-color: #E1E1E1 !important;
            }
        </style>
    <?php } ?>


    <style type="text/css">
        .link_button {
            background-color: #006699;
            border: 2px solid #003366;
            border-radius: 8px;
            color: #ffffff;
            font-size: 12px;
            padding: 10px 15px;
        }

        <?php if (!isset($oInfoCommercant) || !isset($group_id_commercant_user)) {?>
        body {
            margin-top: 50px;
        }

        <?php }  ?>

        <?php if (isset($pagecategory) && $pagecategory == 'admin_commercant') { ?>
        body {
            background-color: #f6f6f6 ;
            background-image: none !important;
        }

        <?php } ?>


        .ulmenu2013 li {
            list-style: none outside none;
            display: block;
            float: left;
            width: 155px;
            /*background-image: url(<?php echo GetImagePath("privicarte/"); ?>/background_menu_part.png);*/
            /*background-position: right top;*/
            border-right: dotted;
            background-repeat: no-repeat;
            background-size: auto 85%;
            height: auto;
            text-align: center;
            vertical-align: middle;
            margin-top:15px;
            margin-bottom:15px;
        <?php if (isset($group_id_commercant_user) && ($group_id_commercant_user == 4 || $group_id_commercant_user == 5)) { ?>
            padding: 9px;
        <?php } else { ?>
            padding-top: 13px;
        <?php } ?>
        }
        .ulmenu2013 li > a {
            color:#FFFFFF;
        }
        .navmen {
            height: 40px;
            text-align: center;
        }
        .container {
            width: 1250px !important;
        }
        .titre_glissiere1 {
            background-color: <?php if (isset($oInfoCommercant->bandeau_color) AND $oInfoCommercant->bandeau_color !=null ){echo $oInfoCommercant->bandeau_color.'!important';}else{echo "3B579D!important";} ?>;
        }

    </style>

    <meta name="google-translate-customization" content="ad114fb2f5d60b29-e2bf26e865a3d116-ga4de9321d692a0e0-29"></meta>

</head>
<body style=" background-image: url('https://www.sortez.org/wpimages/wpe3440087_06.jpg')!important">
<?php $this->load->view("privicarte/includes/facebook_login_js", $data); ?>
<div class="container">
