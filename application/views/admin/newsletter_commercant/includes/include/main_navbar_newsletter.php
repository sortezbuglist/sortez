<?php
//LOCALDATA FILTRE
$this_session_localdata =& get_instance();
$this_session_localdata->load->library('session');
$localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
$localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
if((isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && $localdata_IdVille != NULL) || (isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && $localdata_IdDepartement != NULL)) {
    ?>
    <style type="text/css">
        body { margin-top:0 !important;}
        #main_navbar_localdata { display:none !important;}
    </style>
    <?php
} //LOCALDATA FILTRE
?>

<script type="text/javascript">
    $(document).ready(function() {

        $("#contact_privicarte_nom").focusin(function() {	  if ($(this).val()=="Votre nom *") $(this).val('');	});
        $("#contact_privicarte_nom").focusout(function() {	  if ($(this).val()=="") $(this).val('Votre nom *');	});

        $("#contact_privicarte_tel").focusin(function() {	  if ($(this).val()=="Votre num\351ro de t\351l\351phone *") $(this).val('');	});
        $("#contact_privicarte_tel").focusout(function() {	  if ($(this).val()=="") $(this).val('Votre num\351ro de t\351l\351phone *');	});

        $("#contact_privicarte_mail").focusin(function() {	  if ($(this).val()=="Votre courriel *") $(this).val('');	});
        $("#contact_privicarte_mail").focusout(function() {	  if ($(this).val()=="") $(this).val('Votre courriel *');	});

        $("#contact_privicarte_msg").focusin(function() {	  if ($(this).val()=="Votre message *") $(this).val('');	});
        $("#contact_privicarte_msg").focusout(function() {	  if ($(this).val()=="") $(this).val('Votre message *');	});

        $("#contact_privicarte_reset").click(function() {
            $("#contact_privicarte_nom").val('Votre nom *');
            $("#contact_privicarte_tel").val('Votre num\351ro de t\351l\351phone *');
            $("#contact_privicarte_mail").val('Votre courriel *');
            $("#contact_privicarte_msg").val('Votre message *');
            $("#spanContactPrivicarteForm").html('* champs obligatoires');
        });

        $.noConflict();//this activate all fancybox at all page calling this navbar

        $("#contact_privicarte_send").click(function() {
            var error = 0;
            var contact_privicarte_nom = $("#contact_privicarte_nom").val();
            if (contact_privicarte_nom == '' || contact_privicarte_nom == 'Votre nom *') error = 1;
            var contact_privicarte_tel = $("#contact_privicarte_tel").val();
            if (contact_privicarte_tel == '' || contact_privicarte_tel == 'Votre num\351ro de t\351l\351phone *') error = 1;
            var contact_privicarte_mail = $("#contact_privicarte_mail").val();
            if (contact_privicarte_mail == '' || contact_privicarte_mail == 'Votre courriel *') error = 1;
            if (!validateEmail(contact_privicarte_mail)) error = 2;
            var contact_privicarte_msg = $("#contact_privicarte_msg").val();
            if (contact_privicarte_msg == '' || contact_privicarte_msg == 'Votre message *') error = 1;
            $("#spanContactPrivicarteForm").html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

            if (error == 1) {
                $("#spanContactPrivicarteForm").html('<span style="color:#FF0000;">* Les champs sont obligatoires</span>');
            } else if (error == 2) {
                $("#spanContactPrivicarteForm").html('<span style="color:#FF0000;">* Adresse email invalide</span>');
                $("#contact_privicarte_mail").css('border-color','#ff0000');
            } else {
                $.post(
                    "<?php echo site_url("front/professionnels/contact_partner_sendmail/");?>",
                    {
                        contact_partner_nom:contact_privicarte_nom,
                        contact_partner_tel:contact_privicarte_tel,
                        contact_partner_mail:contact_privicarte_mail,
                        contact_partner_msg:contact_privicarte_msg,
                        contact_partner_mailto:"contact@privicarte.fr"
                    },
                    function( data ) {
                        $("#spanContactPrivicarteForm").html(data);
                        if(typeof $.fancybox == 'function') {
                            setTimeout(function(){ window.parent.$.fancybox.close(); }, 4000);
                        } else {
                            //setTimeout(function(){ window.parent.$.fancybox.close(); }, 4000);
                        }
                    });
            }
        });


        $("#IdContactPrivicarteForm").fancybox({
            'autoScale' : false,
            'overlayOpacity'      : 0.8, // Set opacity to 0.8
            'overlayColor'        : "#000000", // Set color to Black
            'padding'         : 5,
            'width'         : 520,
            'height'        : 580,
            'transitionIn'      : 'elastic',
            'transitionOut'     : 'elastic',
            'type'          : 'iframe'
        });

        $("#IdContactPrivicarteForm_footer").fancybox({
            'autoScale' : false,
            'overlayOpacity'      : 0.8, // Set opacity to 0.8
            'overlayColor'        : "#000000", // Set color to Black
            'padding'         : 5,
            'width'         : 520,
            'height'        : 410,
            'transitionIn'      : 'elastic',
            'transitionOut'     : 'elastic'
        });

        $("#IdFacebookPrivicarteForm").fancybox({
            'autoScale' : false,
            'overlayOpacity'      : 0.8, // Set opacity to 0.8
            'overlayColor'        : "#000000", // Set color to Black
            'padding'         : 5,
            'width'         : 500,
            'height'        : 800,
            'transitionIn'      : 'elastic',
            'transitionOut'     : 'elastic',
            'type'          : 'iframe'
        });

        $("#IdTwitterPrivicarteForm").fancybox({
            'autoScale' : false,
            'overlayOpacity'      : 0.8, // Set opacity to 0.8
            'overlayColor'        : "#000000", // Set color to Black
            'padding'         : 5,
            'width'         : 500,
            'height'        : 800,
            'transitionIn'      : 'elastic',
            'transitionOut'     : 'elastic',
            'type'          : 'iframe',
        });
    });

    function validateEmail(sEmail) {
        var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            return false;
        }
    }
</script>

<style type="text/css">
    table#tableContactPartnerForm td input, table#tableContactPartnerForm td textarea { width:90% !important;}
</style>


<!-- Fixed navbar -->

<div id="main_navbar_localdata" style="background-color: rgb(229, 229, 229)" class="container">
    <div class="row pt-4 pb-4">
        <!--<div class="col-lg-4 navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span></button>
          <!--<a class="navbar-brand" href="#">Privicarte.fr</a>
        </div> !-->

        <div class="col-lg-4">
            <div class="row">
                <div class="col-lg-4">
                    <a href="javascript:void(0);" onclick="javascript:window.history.back();" class="img_nav_pvc"><img src="<?php echo GetImagePath("privicarte/") ; ?>/wp8bb34597_06.png" alt="back"></a>
                </div>
                <div>
                    <div class="col-lg-8 pt-3">
                        <span id="google_translate_element" style=""></span>
                        <script type="text/javascript">
                            function googleTranslateElementInit() {
                                new google.translate.TranslateElement({pageLanguage: 'fr', includedLanguages: 'ar,de,en,es,it,ja,nl,no,pl,pt,ru,sv,zh-CN', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false, multilanguagePage: true}, 'google_translate_element');
                            }
                        </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="row">
                <?php
                $thisss =& get_instance();
                $thisss->load->library('ion_auth');
                $this->load->model("ion_auth_used_by_club");

                $publightbox_email = $this_session_localdata->session->userdata('publightbox_email');
                ?>
                <?php if ($thisss->ion_auth->logged_in()) {
                    $user_ion_auth = $thisss->ion_auth->user()->row();
                    $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                    $iduser_commercant = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                    ?>
                    <?php if ($thisss->ion_auth->in_group(1)) { ?>
                        <div><a href='<?php echo site_url("admin/home"); ?>'>Page Admin</a></div>
                    <?php } else { ?>
                        <div class="col-lg-8 pt-3"><a href='<?php
                            if ($thisss->ion_auth->in_group(2)) echo site_url("front/utilisateur/menuconsommateurs");
                            else echo site_url("front/professionnels/fiche/".$iduser_commercant);
                            ?>'>
                                Bonjour <?php echo $user_ion_auth->first_name . " " . $user_ion_auth->last_name; ?> <img src="<?php echo GetImagePath("privicarte/") ; ?>/profile_user.png" alt="<?php echo $user_ion_auth->first_name; ?>" width="20" style="margin-top: -10px; margin-left: 10px;"/>
                            </a>
                        </div>
                    <?php } ?>
                    <div class="col-lg-4 pt-3"><a href='<?php echo site_url("connexion/sortir"); ?>'>Deconnexion</a></div>

                <?php } else if (isset($publightbox_email) && $publightbox_email != "") { ?>
                    <div><a href="<?php echo site_url("publightbox/carte");?>">Bonjour <?php echo $publightbox_email; ?> <img src="<?php echo GetImagePath("privicarte/") ; ?>/profile_user.png" alt="<?php echo $user_ion_auth->first_name; ?>" width="20" style="margin-top: -10px; margin-left: 10px;"/></a>
                    </div>

                    <div class="col-lg-6"><a href='javascript:void(0);' onclick="javascript:publightbox_deconnexion('<?php echo site_url("publightbox/deconnexion");?>');">Deconnexion</a></div>
                <?php } ?>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="row">
                <div class="col-lg-3 pr-0"><a href="<?php echo site_url();?>" class="img_nav_pvc"><img src="<?php echo GetImagePath("privicarte/") ; ?>/nav_home.png" alt="home"></a></div>
                <div class="col-lg-3 pr-0"><a href="<?php echo site_url('contact');?>" id="IdContactPrivicarteForm" class="img_nav_pvc"><img src="<?php echo GetImagePath("privicarte/") ; ?>/nav_msg.png" alt="message"></a></div><!--#divContactPrivicarteForm-->
                <div class="col-lg-3 pr--0"><a href="<?php echo site_url("front/professionnels/FacebookPrivicarteForm"); ?>" id="IdFacebookPrivicarteForm" class="img_nav_pvc"><img src="<?php echo GetImagePath("privicarte/") ; ?>/nav_fb.png" alt="facebook"></a></div>
                <div class="col-lg-3 pl-0"><a href="<?php echo site_url("front/professionnels/TwitterPrivicarteForm"); ?>" id="IdTwitterPrivicarteForm" class="img_nav_pvc"><img src="<?php echo GetImagePath("privicarte/") ; ?>/nav_twt.png" alt="twitter"></a></div>
            </div>
        </div>

    </div>
</div>




<!--Contact form contet-->
<div id="divContactPrivicarteForm" style="display:none; background-color:#FFFFFF; width:500px; height:400px;">
    <form name="formContactPrivicarteForm" id="formContactPrivicarteForm" action="#">
        <table width="100%" id="tableContactPartnerForm" border="0" cellspacing="0" cellpadding="0" style="text-align:center; width:500px; height:400px;">
            <tr>
                <td><div style="font-family:arial; font-size:24px; font-weight:bold;">Nous Contacter</div></td>
            </tr>
            <tr>
                <td><input type="text" name="contact_privicarte_nom" id="contact_privicarte_nom" value="Votre nom *"/></td>
            </tr>
            <tr>
                <td><input type="text" name="contact_privicarte_tel" id="contact_privicarte_tel" value="Votre num&eacute;ro de t&eacute;l&eacute;phone *"/></td>
            </tr>
            <tr>
                <td><input type="text" name="contact_privicarte_mail" id="contact_privicarte_mail" value="Votre courriel *"/></td>
            </tr>
            <tr>
                <td><textarea name="contact_privicarte_msg" id="contact_privicarte_msg">Votre message *</textarea></td>
            </tr>
            <tr>
                <td><span id="spanContactPrivicarteForm">* champs obligatoires</span></td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><input type="button" class="btn btn-default" name="contact_privicarte_reset" id="contact_privicarte_reset" value="Retablir"/></td>
                            <td><input type="button" class="btn btn-default" name="contact_privicarte_send" id="contact_privicarte_send" value="Envoyer"/></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</div>

