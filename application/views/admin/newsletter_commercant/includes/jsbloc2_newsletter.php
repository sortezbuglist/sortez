<script type="text/javascript">
    <?php if (isset($objbloc_newsletter->is_activ_bloc2) AND $objbloc_newsletter->is_activ_bloc2 == 1) {  ?>
        jQuery('#type_bloc2_choose').css("display", "block");
        <?php if (isset($objbloc_newsletter->type_bloc2) AND $objbloc_newsletter->type_bloc2 == '1'){  ?>
        document.getElementById('bloc2_1').style.display = "block";
        document.getElementById('bloc2_2').style.display = "none";
        $('#type_bloc2js').val("1");
        $("#bloc2_1").removeClass("col-lg-6");
        $("#bloc2_2").removeClass("col-lg-6");
        <?php } ?>
        <?php if (isset($objbloc_newsletter->type_bloc2) AND $objbloc_newsletter->type_bloc2 == '2') {  ?>
        document.getElementById('bloc2_1').style.display = "block";
        document.getElementById('bloc2_2').style.display = "block";
        $('#type_bloc2js').val("2");
        $("#bloc2_1").addClass("col-lg-6");
        $("#bloc2_2").addClass("col-lg-6");
        <?php } ?>
        <?php if (isset($objbloc_newsletter->is_activ_btn_bloc2) && $objbloc_newsletter->is_activ_btn_bloc2 == '1') {  ?>
        document.getElementById('link_bloc2_1').style.display = "block";
        <?php } ?>
        <?php if (isset($objbloc_newsletter->is_activ_btn_bloc2_2) && $objbloc_newsletter->is_activ_btn_bloc2_2 == '1') {  ?>
        document.getElementById('link_bloc2_2').style.display = "block";
        <?php } ?>
    <?php } ?>
    $("#is_activ_bloc2").change(function () {
        if (document.getElementById('is_activ_bloc2').value == 0) {
            jQuery('#type_bloc2_choose').css("display", "none");

            jQuery('#bloc2_1').css("display", "none");

            jQuery('#bloc2_2').css("display", "none");

        }
        else if (document.getElementById('is_activ_bloc2').value == 1) {
            jQuery('#type_bloc2_choose').css("display", "block");

            jQuery('#bloc2_1').css("display", "none");

            jQuery('#bloc2_2').css("display", "none");

        }
    });
    $("#1b2").click(function () {
        document.getElementById('bloc2_1').style.display = "block";
        document.getElementById('bloc2_2').style.display = "none";
        $('#type_bloc2js').val("1");
        $("#bloc2_1").removeClass("col-lg-6");
        $("#bloc2_2").removeClass("col-lg-6");

    });
    $("#2b2").click(function () {
        document.getElementById('bloc2_1').style.display = "block";
        document.getElementById('bloc2_2').style.display = "block";
        $('#type_bloc2js').val("2");
        $("#bloc2_1").addClass("col-lg-6");
        $("#bloc2_2").addClass("col-lg-6");
    });
    $("#link_btn_bloc2_1").change(function () {

        if (document.getElementById('link_btn_bloc2_1').value == 1) {
            document.getElementById('link_bloc2_1').style.display = "block";

        }
        else if (document.getElementById('link_btn_bloc2_1').value == 0) {
            document.getElementById('link_bloc2_1').style.display = "none";

        }

    });
    $("#link_btn_bloc2_2").change(function () {


        if (document.getElementById('link_btn_bloc2_2').value == 1) {
            document.getElementById('link_bloc2_2').style.display = "block";

        }
        else if (document.getElementById('link_btn_bloc2_2').value == 0) {
            document.getElementById('link_bloc2_2').style.display = "none";

        }

    });
</script>