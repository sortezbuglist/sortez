<?php $data['empty'] = null;  ?>
<?php $data["zTitle"] = 'Newsletter' ?>
<?php $this->load->view("privicarte/vwFicheProfessionnel_platinum_js", $data); ?>
<?php $this->load->view("privicarte/includes/main_header", $data); ?>
<?php $this->load->view("privicarte/includes/main_navbar", $data); ?>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<style type="text/css">
    #import_csv_btn{
        cursor: pointer;
        color: white;
        height: 25px;
        background-color: red;
        text-align: center;
        width:100px;
        font-size: 12px;
        padding: 2px;
        border-radius: 2px;
        /*line-height: 3*/
    }
    a:hover{
        text-decoration: none;
    }
    #remettre0{
        color:white;
        background-color: red;
        height: 25px;
        text-align: center;
        width: 100px;
        font-size: 12px;
        padding: 2px;
        border-radius: 2px;

    }

    /*#import_csv_btn:hover{
        cursor: pointer;
        color: white;
        background-color: deeppink;
    }*/
    body{
        background-image: url("https://www.sortez.org/wpimages/wpe3440087_06.jpg");
        margin: auto;
    }
    .btned{
        cursor: pointer;
        background-color: blue;
        color: white;
    }
    .btned:hover{
        background-color: deeppink;
    }
    .btned:active{
        background-color: green;
    }
</style>
<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
   
</script>
<body>
<div class="container" style="background-color: #f6f6f6">
    <div id="" class="content" style="">
        <form id="frmNewsletter" name="frmNewsletter" method="POST"
              action="<?php echo site_url("admin/Newsletter_commercant_backoffice/envoyer_newletter"); ?>">
            <br>
            <div class="text-center"><a class="btn btn-info" href="<?php echo site_url('auth/login')?>">Retour menu</a></div>
            <div class="p-4">
                Importer mes données:
                <select class="form-control" id="categ_to_show">
                    <option value="non">Non</option>
                    <option value="bonplan">Bonplan</option>
                    <option value="article">Article</option>
                    <option value="agenda">Agenda</option>
                    <option value="boutique">Boutique</option>
                    <option value="fidelite">Fidelite</option>
                    <option value="plat">Réservation plat du jour</option>
                </select>
            </div>
            <h3 class="text-center">Newsletter</h3>
           <?php if (isset($status)){ ?> <h4 class="text-center"><?php echo $status; ?></h4><?php } ?>
            <br>
            <div class="row" style="margin: auto">
                <table width="" border="0" cellspacing="0" cellpadding="0" align="center"
                       style="text-align: left;">
                    <tr>
                        <td>
                            <input name="sendallnews" id="sendallnews" type="radio" value="0"/>&nbsp;Envoyer &agrave; Tous les
                            clients abonnés
                            <div id="iDuserSelect" style="padding-left: 50px;">
                                <div class="row">
                                    <div class="col-lg-10">
                                    <input type="hidden" name="IdCommercant" value="<?php echo $infocom->IdCommercant;?>">
                                </div>
                                </div>
                                    <div class="row">
                                <div  class="col-lg-10">
                                    <input id="all_client_only" type="checkbox" value="0"/>
                                    <input id="all_client_only_value" name="all_client_only_value" type="hidden" value="0"/>
                                    <label>Liste des clients inscrits seulement</label>
                                    </div>
                                    <div class="col-lg-2">
                                    <a href="<?php echo site_url('admin/Newsletter_commercant_backoffice/liste_true_client/'.$infocom->IdCommercant)?>" class="btn btn-success">Voir la liste</a>
                                     </div>
                                </div>
                                <div class="row">
                                    <div  class="col-lg-10">
                                    <input id="all_fclient_only" type="checkbox" value="0"/>
                                    <input id="all_false_client_only" name="all_false_client_only_value" type="hidden" value="0"/>
                                    <label>Liste des emails du Newsletter seulement</label>
                                     </div>
                                     <div class="col-lg-2">
                                    <a href="<?php echo site_url('admin/Newsletter_commercant_backoffice/liste_false_client/'.$infocom->IdCommercant)?>" class="btn btn-success">Voir la liste</a>
                                    </div>
                                </div>
                                <div class="row">
                                             <div  class="col-lg-10">
                                                 <input id="all_commentclient_only" type="checkbox" value="0"/>
                                                 <input id="all_comment_client_only" name="all_comment_client_only_value" type="hidden" value="0"/>
                                                 <label>Liste des emails des commentaires seulement</label>
                                             </div>
                                             <div class="col-lg-2">
                                                 <a href="<?php echo site_url('admin/Newsletter_commercant_backoffice/liste_comment_client/'.$infocom->IdCommercant)?>" class="btn btn-success">Voir la liste</a>
                                             </div>
                                </div>
                                <div class="row">
                                    <div  class="col-lg-12">
                                        <input id="all_commentclientcsv_only" type="checkbox" value="0"/>
                                        <input id="all_commentclient_csv_only" name="all_csv_client_only_value" type="hidden" value="0"/>
                                        <label>Liste des emails importés depuis un CSV</label>
                                        <div style="font-size: font-variant: normal;font-size: 12px;margin-left:15px; ">
                                            <p>« Nom », « Prénom », « Date de naissance », « Adresse »,</p>
                                            <p>« Code postal », « ville », « Téléphone mobile », « e-mail ».</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 ">
                                        <a href="<?php echo site_url('admin/Newsletter_commercant_backoffice/liste_csv_client/'.$infocom->IdCommercant)?>" class="btn btn-success">Voir la liste</a>
                                    </div>
                                    <div class="col-lg-4">
                                        <a href="<?php echo site_url('admin/Newsletter_commercant_backoffice/import_csv_client/0/0/'.$infocom->IdCommercant)?>" ><div id="import_csv_btn" style="">Ajouter</div></a>
                                    </div>
                                    <div class="col-lg-4">
                                        <a href="<?php echo site_url('admin/Newsletter_commercant_backoffice/supprimer_csv/'.$infocom->IdCommercant)?>" onclick="return confirm('Are you sure you want to delete this item?');"><div id="remettre0" style="">Remettre a zero</div></a>

                                </div>
                               <!-- <div class="row text-center p-4">
                                    <div class="col-12"><a href="<?php /*echo site_url('admin/Newsletter_commercant_backoffice/import_csv_client/0/0/'.$infocom->IdCommercant)*/?>"><div id="import_csv_btn" style="">Importer une liste en CSV</div></a></div>
                                </div>-->

                        </td>
                    </tr>
                    <tr>
                        <td><input name="sendallnews" id="sendonesss" type="radio" value="2" checked/>&nbsp;Envoyer &agrave; une adresse&nbsp;<input
                                name="sendonemail" id="sendonemail" value="" type="text" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                </table>
            </div>
            <div class="row" style="margin: auto">
                <div class="col-lg-12 text-center">

                        <div style="float: left; text-align: left; display: table; width: 100%;"><label>Objet du mail :</label></div>

                            <input type="text" id="txtSujet" name="txtSujet" class="form-control" value=""/>

                    <div class="col-12 pt-3">
                        <div class="pt-4">Choisir maquette</div>
                    </div>
                    <!--<div class="row p-4">
                        <div id="col1" class="col-4 pt-4 btned" style=";height: 50px;border-right: dotted">
                            1 colonne
                        </div>
                        <div id="col2"  class="col-4 pt-4 btned" style="height: 50px;border-right: dotted">
                            2 colonnes
                        </div>
                        <div id="col3"  class="col-4 pt-4 btned" style="height: 50px;">
                            3 colonnes
                        </div>
                    </div>-->
                        <div style="float: left; text-align: left; display: table; width: 100%; margin-top: 15px;"><label>Contenu du mail :</label></div>
                        <div style="display: table; width: 100%;">
                            <textarea id="txtContenu" style="width: 100%; height: 200px;" name="txtContenu"></textarea>
                            <script>
                              var editor = CKEDITOR.replace('txtContenu');
                            </script>
                        </div>
                    <div class="row">
                        <div class="col-6 text-right pt-3">
                        <input type="text" id="nom" value="" placeholder="titre"><a id="save_content" class="btn btn-info">Enregistrer le contenu</a>
                        </div>
                        <div class="col-6 text-left pt-3">
                            <label for="content_saved">charger  contenu : </label>
                            <select id="option_change">
                                <option value="">--Choisir--</option>
                                <?php if (isset($saved_stat) AND count($saved_stat) !=0){ ?>
                                    <?php foreach ($saved_stat as $list) {?>
                                        <option id="<?php echo $list->id;?>" value="<?php echo $list->id;?>"><?php echo $list->nom;?></option>
                                <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                        <div style="padding: 30px; text-align: center;">
                            <div>
                                <input type="button" id="btnEnvoyer" value="Envoyer"
                                       style="padding: 15px 40px; font-size: 30px; border-radius: 15px;"/>
                            </div>
                            <div>
                                <span style="color: green;">Success: <span id="success_statut"></span><br></span>
                                <span style="color: red;">Erreur: <span id="error_statut"></span></span>
                            </div>
                            <div id="errornewsfrm"></div>
                        </div>
                    </div>
            </div>
            <input type="hidden" name="tosave" value="" id="tosave_hidden">
            <input type="hidden" name="nom" value="" id="nom_hidden">
            <input type="hidden" name="idcom" id="idcom_val" value="<?php echo $infocom->IdCommercant; ?>">
        </form>
   

<!--premiere bande verte-->

<?php
$this->load->view('admin/newsletter_commercant/bloc_news/news_bloc1');
?>

<!--deuxième bande verte-->

<!-- <?php 
//$this->load->view('admin/newsletter_commercant/bloc_news/news_bloc2'); 
?> 
<!--troisème bande verte-->
<!--<?php
//?> -->
<!--quatrième bande verte-->
<!--<?php
//$this->load->view('admin/newsletter_commercant/bloc_news/news_bloc4');
?>-->


</div><!--form-->


    <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/fields.check.js"></script>
    <script type="text/javascript">
        jQuery('#col1').click(function () {
            nb_grid='nbr=1';
            jQuery.ajax({
                url: "<?php echo site_url('admin/Newsletter_commercant_backoffice/load_maquette_newsletter');?>",
                dataType: 'html',
                type: 'POST',
                data: nb_grid,
                success: function (data) {
                    CKEDITOR.instances['txtContenu'].setData(data);

                },
                error: function (data) {
                    console.log(data);
                    alert('Erreur de sauvegarde');
                }

            });
        });

        jQuery('#col2').click(function () {
            nb_grid='nbr=2';
            jQuery.ajax({
                url: "<?php echo site_url('admin/Newsletter_commercant_backoffice/load_maquette_newsletter');?>",
                dataType: 'html',
                type: 'POST',
                data: nb_grid,
                success: function (data) {
                    CKEDITOR.instances['txtContenu'].setData(data);

                },
                error: function (data) {
                    console.log(data);
                    alert('Erreur de sauvegarde');
                }

            });
        });

        jQuery('#col3').click(function () {
            nb_grid='nbr=3';
            jQuery.ajax({
                url: "<?php echo site_url('admin/Newsletter_commercant_backoffice/load_maquette_newsletter');?>",
                dataType: 'html',
                type: 'POST',
                data: nb_grid,
                success: function (data) {
                    CKEDITOR.instances['txtContenu'].setData(data);
                },
                error: function (data) {
                    console.log(data);
                    alert('Erreur de sauvegarde');
                }

            });
        });

        jQuery('#option_change').change(function () {

            ids=$('#option_change').val();
            dataid="id="+ids;
            jQuery.ajax({
                url: "<?php echo site_url('admin/Newsletter_commercant_backoffice/load_stat_news');?>",
                dataType: 'html',
                type: 'POST',
                data: dataid,
                success: function (data) {
                    let data_parsed=jQuery.parseJSON(data);
                    console.log(data_parsed.content);
                        CKEDITOR.instances['txtContenu'].setData(data_parsed.content);
                        $('#txtSujet').val(data_parsed.object);
                        $('#sendonemail').val(data_parsed.send_mail_adress);
                },
                error: function (data) {
                    console.log(data);
                    alert('Erreur de sauvegarde');
                }

            });

        });

        jQuery("#save_content").click(function () {
            $("#frmNewsletter").attr("action","<?php echo site_url("admin/Newsletter_commercant_backoffice/save_stat_news"); ?>");
            var nom=$('#nom').val();
            $('#nom_hidden').val(nom);
            var valued = editor.getData();
            $("#tosave_hidden").val(valued);
            $('#frmNewsletter').submit();
        });

        function htmlspecialchars(str) {
            return str.replace('&', '&amp;').replace('"', '&quot;').replace("'", '&#039;').replace('<', '&lt;').replace('>', '&gt;');
        }

            jQuery("#all_client_only").click(function () {
                if (jQuery(this).is(':checked')) {
                    jQuery("#all_client_only_value").val("1");
                } else {
                    jQuery("#all_client_only_value").val("0");
                }
            });

            jQuery("#all_fclient_only").click(function () {
                if (jQuery(this).is(':checked')) {
                    jQuery("#all_false_client_only").val("1");
                } else {
                    jQuery("#all_false_client_only").val("0");
                }
            });

            jQuery("#all_commentclient_only").click(function () {
                if (jQuery(this).is(':checked')) {
                    jQuery("#all_comment_client_only").val("1");
                } else {
                    jQuery("#all_comment_client_only").val("0");
                }
            });

        jQuery("#all_commentclientcsv_only").click(function () {
            if (jQuery(this).is(':checked')) {
                jQuery("#all_commentclient_csv_only").val("1");
            } else {
                jQuery("#all_commentclient_csv_only").val("0");
            }
        });

        jQuery("#sendallnews").click(function () {               
                jQuery("#sendallnews").val("1");
                jQuery("#sendonesss").val("0");           
        });
        jQuery("#sendonesss").click(function () {               
                jQuery("#sendallnews").val("0");
                jQuery("#sendonesss").val("2");           
        });


        jQuery("#btnEnvoyer").click(function () {

            var nbtrue=0;
            var nbfalse=0;
            var nbcomment=0;
            var nbcsv=0;

            success=0;
            error=0;
            startexec1=1;
            startexec2=1;
            startexec4=1;

            var startexec=1;
            var newserror = '';
            var radiochoice = jQuery('input[type=radio][name=sendallnews]:checked').attr('value');

            if (radiochoice == 2) {
                if (jQuery("#sendonemail").val() == "") {
                    newserror = '<span style="color:#F00;">Specifier l\'adresse email pour envoyer le Newsletter !</span>';
                    jQuery("#errornewsfrm").html(newserror);
                } else if (!isEmail(jQuery("#sendonemail").val())) {
                    newserror = '<span style="color:#F00;">Veuillez ajouter un email valide !</span>';
                    jQuery("#errornewsfrm").html(newserror);
                } else newserror = '';
            } else if (radiochoice == 1) {
                newserror = '';
            }

            if (newserror == '') {

                var nbtrueclient=parseInt('<?php echo $nbtrueclient;?>');
                var nbfalseclient=parseInt('<?php echo $nbfalseclient;?>');
                var nbcommentclient=parseInt('<?php echo $nbcommentclient;?>');
                var nbcsvclient=parseInt('<?php echo $nbcsvclient;?>');
                var nbexec_global=0;
                //alert($("#all_client_only_value").val());
                //alert(editor.getData());
                alert($('#sendallnews').val());
                
                // data="sendallnews="+$('#sendallnews').val()+"&all_client_only_value="+$("#all_client_only_value").val()+'&all_false_client_only_value='+$('#all_false_client_only').val()+'&sendonemail='+$("#sendonemail").val()+"&all_comment_client_only_value="+$("#all_comment_client_only").val()+"&all_csv_client_only_value="+$("#all_commentclient_csv_only").val()+"&txtSujet="+$("#txtSujet").val()+"&txtContenu="+editor.getData()+"&IdCommercant="+$("#idcom_val").val();

                data={
                    "sendallnews": $('#sendallnews').val(),
                    "all_client_only_value":$("#all_client_only_value").val(),
                    "all_false_client_only_value":$('#all_false_client_only').val(),
                    "sendonemail":$("#sendonemail").val(),
                    "all_comment_client_only_value": $("#all_comment_client_only").val(),
                    "all_csv_client_only_value": $("#all_commentclient_csv_only").val(),
                    "txtSujet":$("#txtSujet").val() ,
                    "txtContenu": editor.getData(),
                    "IdCommercant": $("#idcom_val").val()                     
                }

                if($('#all_client_only_value').val()==="1"){

                    nbexec_global+=nbtrueclient;
                }
                if($('#all_client_only_value').val()==="1"){

                    nbexec_global+=nbfalseclient;
                }
                if($('#all_comment_client_only').val()==="1"){

                    nbexec_global+=nbcommentclient;
                }
                if($('#all_commentclient_csv_only').val()==="1"){

                    nbexec_global+=nbcsvclient;
                }
                if($('#sendonesss').val()==='2'){
                alert('envoyer_newletter_one');
                alert(data);
                jQuery.ajax({
                    url: "<?php echo site_url("admin/Newsletter_commercant_backoffice/envoyer_newletter_one/") ?>",
                    dataType: 'json',
                    type: 'POST',
                    data: data,
                    success: function (data) {
                        if (data=='1'){
                            success+=1;
                            $('#success_statut').html(success);
                        }else{
                            error+=1;
                            $('#error_statut').html(error);
                        }
                    },
                    error: function (data) {
                        error+=1;
                        $('#error_statut').html(error);
                    }

                });
            }
                if ($('#sendallnews').val()==='1' && $('#all_client_only_value').val()==="1"){
                    var nbexec=nbtrueclient;
                    while(startexec <= nbexec) {
                        jQuery.ajax({
                            url: "<?php echo site_url("admin/Newsletter_commercant_backoffice/envoyer_newletter_true/") ?>"+nbtrue,
                            dataType: 'html',
                            type: 'POST',
                            data: data,
                            success: function (data) {
                                if (data=='ok'){
                                    success+=1;
                                    $('#success_statut').html(success);
                                }else{
                                    error+=1;
                                    $('#error_statut').html(error);
                                }
                            },
                            error: function (data) {
                                error+=1;
                                $('#error_statut').html(error);
                            }

                        });
                        startexec+=1;
                        nbtrue++;
                    }
                }
                if ($('#sendallnews').val()==='1' && $('#all_false_client_only').val()==="1"){
                    var nbexec1=nbfalseclient;
                    while(startexec1 <= nbexec1) {

                        jQuery.ajax({
                            url: "<?php echo site_url("admin/Newsletter_commercant_backoffice/envoyer_newletter_false/"); ?>"+nbfalse,
                            dataType: 'html',
                            type: 'POST',
                            data: data,
                            success: function (data) {
                                if (data=='1'){
                                    success+=1;
                                    $('#success_statut').html(success);
                                }else{
                                    error+=1;
                                    $('#error_statut').html(error);
                                }
                            },
                            error: function (data) {
                                error+=1;
                                $('#error_statut').html(error);
                            }

                        });
                        startexec1+=1;
                        nbfalse++;
                    }
                }

                if ($('#sendallnews').val()==='1' && $('#all_comment_client_only').val()==="1"){                    
                    var nbexec2=nbcommentclient;
                    while(startexec2 <= nbexec2) {
                        jQuery.ajax({
                            url: "<?php echo site_url("admin/Newsletter_commercant_backoffice/envoyer_newletter_comment/"); ?>"+nbcomment,
                            dataType: 'html',
                            type: 'POST',
                            data: data,
                            success: function (data) {
                                if (data=='ok'){
                                    success+=1;
                                    $('#success_statut').html(success);
                                }else{
                                    error+=1;
                                    $('#error_statut').html(error);
                                }
                            },
                            error: function (data) {
                                error+=1;
                                $('#error_statut').html(error);
                            }

                        });
                        startexec2+=1;
                        nbcomment++;
                    }
                }
                if ($('#sendallnews').val()==='1' && $('#all_commentclient_csv_only').val()==="1"){
                    var nbexec4=nbcsvclient;
                    while(startexec4 <= nbexec4) {
                        jQuery.ajax({
                            url: "<?php echo site_url("admin/Newsletter_commercant_backoffice/envoyer_newletter_csv/"); ?>"+nbcsv,
                            dataType: 'html',
                            type: 'POST',
                            data: data,
                            success: function (data) {
                                if (data=='ok'){
                                    success+=1;
                                    $('#success_statut').html(success);
                                }else{
                                    error+=1;
                                    $('#error_statut').html(error);
                                }

                            },
                            error: function (data) {
                                error+=1;
                                $('#error_statut').html(error);
                            }

                        });
                        startexec4++;
                        nbcsv++;
                    }
                }

            }else{alert('df')}
        });

           jQuery("#categ_to_show").change(function () {
                CKEDITOR.instances['txtContenu'].setData('');
            if(jQuery('#categ_to_show').val()!='non'){
                let idcom="<?php echo $infocom->IdCommercant; ?>";
                let categ=jQuery('#categ_to_show').val();
                data="idcommercant="+idcom+"&categ="+categ;
                jQuery.ajax({
                    url: "<?php echo site_url('admin/Newsletter_commercant_backoffice/get_commercant_data/');?>",
                    dataType: 'html',
                    type: 'POST',
                    data: data,
                    success: function (data) {
                        CKEDITOR.instances['txtContenu'].setData(data);
                    },
                    error: function (data) {
                        $("#txtContenu").html('');
                    }
                });
            }
            });
    </script>
</div><!--container-->
</body>