<?php $data['empty'] = null;  ?>
<?php $data["zTitle"] = 'Newsletter' ?>
<?php $this->load->view("admin/newsletter_commercant/includes/include/main_header_newsletter", $data); ?>
<?php $this->load->view("admin/newsletter_commercant/includes/include/main_navbar_newsletter", $data); ?>
<?php //$this->load->view("privicarte/includes/main_header", $data); ?>
<?php //$this->load->view("privicarte/includes/main_navbar", $data); ?>
<style type="text/css">
    #import_csv_btn{
        cursor: pointer;
        color: white;
        height: 25px;
        background-color: red;
        text-align: center;
        width:100px;
        font-size: 12px;
        padding: 2px;
        border-radius: 2px;
        /*line-height: 3*/
    }
    a:hover{
        text-decoration: none;
    }
    #remettre0{
        color:white;
        background-color: red;
        height: 25px;
        text-align: center;
        width: 100px;
        font-size: 12px;
        padding: 2px;
        border-radius: 2px;

    }

    /*#import_csv_btn:hover{
        cursor: pointer;
        color: white;
        background-color: deeppink;
    }*/
    body{
        background-image: url("https://www.sortez.org/wpimages/wpe3440087_06.jpg");
        margin: auto;
    }
    .btned{
        cursor: pointer;
        background-color: blue;
        color: white;
    }
    .btned:hover{
        background-color: deeppink;
    }
    .btned:active{
        background-color: green;
    }
    .bout:hover{
        background-color: #ddd;
        border: none;
        color: black;
        padding: 16px 32px;
        text-align: center;
        font-size: 16px;
        margin: 4px 2px;
        transition: 0.3s;
        cursor:pointer;
    }
    .bout {
        background-color: green;
        color: white;
    }
</style>
<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<div class="container" style="background-color: #f6f6f6">
    <div id="" class="content" style="">
        <form id="frmNewsletter" name="frmNewsletter" method="POST"
              action="<?php echo site_url("admin/Newsletter_commercant_backoffice/envoyer_newletter"); ?>">
            <br>
            <div class="text-center"><a class="btn btn-info" href="<?php echo site_url('auth/login')?>">Retour menu</a></div>
            <div class="p-4">
                Importer mes données:
                <select class="form-control" id="categ_to_show">
                    <option value="non">Non</option>
                    <option value="bonplan">Bonplan</option>
                    <option value="article">Article</option>
                    <option value="agenda">Agenda</option>
                    <option value="boutique">Boutique</option>
                    <option value="fidelite">Fidelite</option>
                    <option value="plat">Réservation plat du jour</option>
                </select>
            </div>
            <h3 class="text-center">Newsletter</h3>
           <?php if (isset($status)){ ?> <h4 class="text-center"><?php echo $status; ?></h4><?php } ?>
            <br>
            <div class="row" style="margin: auto">
                <table width="" border="0" cellspacing="0" cellpadding="0" align="center"
                       style="text-align: left;">
                    <tr>
                        <td>
                            <input name="sendallnews" id="sendallnews" type="radio" value="1"/>&nbsp;Envoyer &agrave; Tous les
                            clients abonnés
                            <div id="iDuserSelect" style="padding-left: 50px;">
                                <div class="row">
                                <div class="col-lg-10">
                                    <input type="hidden" name="IdCommercant" value="<?php echo $IdCommercant;?>">
                                </div>
                                </div>
                                    <div class="row pt-4 pb-4">
                                        <div  class="col-lg-10">
                                            <input id="all_client_only" type="checkbox" value="0"/>
                                            <input id="all_client_only_value" name="all_client_only_value" type="hidden" value="0"/>
                                            <label>Liste des clients inscrits seulement</label>
                                        </div>
                                        <div class="col-lg-2">
                                            <a href="<?php echo site_url('admin/Newsletter_commercant_backoffice/liste_true_client/'.$IdCommercant)?>"class="btn btn-success">Voir la liste</a>
                                        </div>
                                    </div>
                                     <div class="row pb-4">
                                        <div  class="col-lg-10">
                                            <input id="all_fclient_only" type="checkbox" value="0"/>
                                            <input id="all_false_client_only" name="all_false_client_only_value" type="hidden" value="0"/>
                                            <label>Liste des emails du Newsletter seulement</label>
                                        </div>
                                        <div class="col-lg-2">
                                            <a href="<?php echo site_url('admin/Newsletter_commercant_backoffice/liste_false_client/'.$IdCommercant)?>" class="btn btn-success">Voir la liste</a>
                                        </div>
                                     </div>
                                         <div class="row pb-4">
                                             <div  class="col-lg-10">
                                                 <input id="all_commentclient_only" type="checkbox" value="0"/>
                                                 <input id="all_comment_client_only" name="all_comment_client_only_value" type="hidden" value="0"/>
                                                 <label>Liste des emails des commentaires seulement</label>
                                             </div>
                                             <div class="col-lg-2">
                                                 <a href="<?php echo site_url('admin/Newsletter_commercant_backoffice/liste_comment_client/'.$IdCommercant)?>" class="btn btn-success">Voir la liste</a>
                                             </div></div>
                                <div class="row ">
                                    <div  class="col-lg-12 pb-4">
                                        <input id="all_commentclientcsv_only" type="checkbox" value="0"/>
                                        <input id="all_commentclient_csv_only" name="all_csv_client_only_value" type="hidden" value="0"/>
                                        <label>Liste des emails importés depuis un CSV</label>
                                        <div style="font-size: font-variant: normal;font-size: 12px;margin-left:15px; ">
                                            <p>« e-mail », « Nom »,  « Prénom », « Date de naissance », </p>
                                            <p>« Adresse », « Code postal », « ville », « Téléphone mobile ».</p>
                                        </div>
                                        <div class="row  pb-4">
                                            <div class="col-lg-4 ">
                                                <a href="<?php echo site_url('admin/Newsletter_commercant_backoffice/liste_csv_client/'.$IdCommercant)?>"class="btn btn-success">Voir la liste</a>
                                            </div>
                                            <div class="col-lg-4">
                                                <a href="<?php echo site_url('admin/Newsletter_commercant_backoffice/import_csv_client/0/0/'.$IdCommercant)?>" ><div id="import_csv_btn" style="font-size:20px;font-family: times New Roman, sans-serif; padding-top: 6px; background-color:orangered;color:white;height:37px;width:120px;">Ajouter</div></a>
                                            </div>
                                            <div class="col-lg-4">
                                                <a href="<?php echo site_url('admin/Newsletter_commercant_backoffice/supprimer_csv/'.$IdCommercant)?>" onclick="return confirm('Are you sure you want to delete this item?');"><div id="remettre0" style="font-size:20px; font-family: times New Roman, sans-serif;padding-top: 6px; background-color:red;color:white;height:37px;width:140px;">Remettre a zero</div></a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                               <!-- <div class="row text-center p-4">
                                    <div class="col-12"><a href="<?php /*echo site_url('admin/Newsletter_commercant_backoffice/import_csv_client/0/0/'.$IdCommercant)*/?>"><div id="import_csv_btn" style="">Importer une liste en CSV</div></a></div>
                                </div>-->

                        </td>
                    </tr>
                    <tr>
                        <td><input name="sendallnews" id="sendonesss" type="radio" value="2" checked/>&nbsp;Envoyer &agrave; une adresse&nbsp;<input
                                name="sendonemail" id="sendonemail" value="" type="text" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                </table>
    </div>
            <div class="row" style="margin: auto">
                <div class="col-lg-12 text-center">

                        <div style="float: left; text-align: left; display: table; width: 100%;"><label>Objet du mail :</label></div>

                            <input type="text" id="txtSujet" name="txtSujet" class="form-control" value=""/>

                    <div class="col-12 pt-3">
                        <div class="pt-4">Choisir maquette</div>
                    </div>
                    <!--<div class="row p-4">
                        <div id="col1" class="col-4 pt-4 btned" style=";height: 50px;border-right: dotted">
                            1 colonne
                        </div>
                        <div id="col2"  class="col-4 pt-4 btned" style="height: 50px;border-right: dotted">
                            2 colonnes
                        </div>
                        <div id="col3"  class="col-4 pt-4 btned" style="height: 50px;">
                            3 colonnes
                        </div>
                    </div>-->
                    <div id="to_shox" class="p-4" style="display: none">
                        <label style="float: left; text-align: left; display: table; width: 100%; margin-top: 15px;">Filtre agenda:</label>
                        <select class="form-control" id="Filtre_date">
                            <option value="1">Tout</option>
                            <option value="101">Aujourd'hui</option>
                            <option value="202">Week-end</option>
                            <option value="303">La semaine</option>
                            <option value="404">Semaine prochaine</option>
                            <option value="505">Mois</option>
                        </select>
                    </div>
                    <div id="to_shox_article" class="p-4" style="display: none">
                        <label style="float: left; text-align: left; display: table; width: 100%; margin-top: 15px;">Filtre article:</label>
                        <select class="form-control" id="Filtre_date_article">
                            <option value="1">Tout</option>
                            <option value="101">Aujourd'hui</option>
                            <option value="202">Week-end</option>
                            <option value="303">La semaine</option>
                            <option value="404">Semaine prochaine</option>
                            <option value="505">Mois</option>
                        </select>
                    </div>
                    <div id="to_shox_annonce" class="p-4" style="display: none">
                        <label style="float: left; text-align: left; display: table; width: 100%; margin-top: 15px;">Filtre annonce:</label>
                        <select class="form-control" id="Filtre_date_annonce">
                            <option value=" ">Tout</option>
                            <option value="0">Reconditionnés</option>
                            <option value="1">Neufs</option>
                        </select>
                    </div>
                    <div id="to_shox_bonplan" class="p-4" style="display: none">
                        <label style="float: left; text-align: left; display: table; width: 100%; margin-top: 15px;">Filtre bonplan:</label>
                        <select class="form-control" id="Filtre_date_bonplan">
                            <option value="0">Tout</option>
                            <option value="1">Les bons plans simples</option>
                            <option value="2">Les bons plans uniques</option>
                            <option value="3">Les bons plans multiples</option>
                        </select>
                    </div>
                    <div id="to_shox_fidelity" class="p-4" style="display: none">
                        <label style="float: left; text-align: left; display: table; width: 100%; margin-top: 15px;">Filtre fidelité:</label>
                        <select class="form-control" id="Filtre_date_fidelity">
                            <option value="0">Tout</option>
                            <option value="1">Les Remises directes</option>
                            <option value="2">Les offres capitales</option>
                            <option value="3">Les coups de tampon</option>
                        </select>
                    </div>
                        <div style="float: left; text-align: left; display: table; width: 100%; margin-top: 15px;"><label>Contenu du mail :</label></div>
                        <div style="display: table; width: 100%;">
                            <textarea id="txtContenu" style="width: 100%; height: 200px;" name="txtContenu"></textarea>
                            <script>
                              var editor = CKEDITOR.replace('txtContenu');
                            </script>
                        </div>
                    <div class="row">
                        <div class="col-6 text-right pt-3">
                            <input type="text" id="nom" value="" placeholder="titre" style="height:35px;"><a id="save_content" class="btn btn-info pt-1 pl-1" style="border-radius: 0px 0px 0px 0px;font-size:20px;font-family: times New Roman;color:white;height:35px;width:192px;margin-bottom: 0.6%;padding: 0.01%;">Enregistrer le contenu</a>
                        </div>
                        <div class="col-3 text-left pt-3">
                            <label for="content_saved">charger  contenu : </label>
                            <select id="option_change" style="height:35px;">
                                <option value="">--Choisir--</option>
                                <?php if (isset($saved_stat) AND count($saved_stat) !=0){ ?>
                                    <?php foreach ($saved_stat as $list) {?>
                                        <option id="<?php echo $list->id;?>" value="<?php echo $list->id;?>"><?php echo $list->nom;?></option>
                                <?php } ?>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="col-3 text-left pt-3">
                        <label for="content_saved">supprimer : </label>
                            <select id="supprime_change" style="height:35px;">
                                <option value="">--Choisir--</option>
                                <?php if (isset($saved_stat) AND count($saved_stat) !=0){ ?>
                                    <?php foreach ($saved_stat as $list) {?>
                                        <option id="<?php echo $list->id;?>" value="<?php echo $list->id;?>"><?php echo $list->nom;?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                        <div style="padding: 30px; text-align: center;">
                            <div>
                                <input type="button" id="btnEnvoyer" value="Envoyer"
                                       style="padding: 15px 40px; font-size: 30px; border-radius: 15px;"/>
                            </div>
                            <div>
                                <span style="color: green;">Success: <span id="success_statut"></span><br></span>
                                <span style="color: red;">Erreur: <span id="error_statut"></span></span>
                            </div>
                            <div id="errornewsfrm"></div>
                        </div>
                    </div>
            </div>
            <input type="hidden" name="tosave" value="" id="tosave_hidden">
            <input type="hidden" name="nom" value="" id="nom_hidden">
            <input type="hidden" name="idcom" id="idcom_val" value="<?php echo $IdCommercant; ?>">
        </form>
    </div>
    <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/fields.check.js"></script>
    <script type="text/javascript">
        jQuery('#col1').click(function () {
            nb_grid='nbr=1';
            jQuery.ajax({
                url: "<?php echo site_url('admin/Newsletter_commercant_backoffice/load_maquette_newsletter');?>",
                dataType: 'html',
                type: 'POST',
                data: nb_grid,
                success: function (data) {
                    CKEDITOR.instances['txtContenu'].setData(data);

                },
                error: function (data) {
                    console.log(data);
                    alert('Erreur de sauvegarde');
                }

            });
        });

        jQuery('#col2').click(function () {
            nb_grid='nbr=2';
            jQuery.ajax({
                url: "<?php echo site_url('admin/Newsletter_commercant_backoffice/load_maquette_newsletter');?>",
                dataType: 'html',
                type: 'POST',
                data: nb_grid,
                success: function (data) {
                    CKEDITOR.instances['txtContenu'].setData(data);

                },
                error: function (data) {
                    console.log(data);
                    alert('Erreur de sauvegarde');
                }

            });
        });

        jQuery('#col3').click(function () {
            nb_grid='nbr=3';
            jQuery.ajax({
                url: "<?php echo site_url('admin/Newsletter_commercant_backoffice/load_maquette_newsletter');?>",
                dataType: 'html',
                type: 'POST',
                data: nb_grid,
                success: function (data) {
                    CKEDITOR.instances['txtContenu'].setData(data);
                },
                error: function (data) {
                    console.log(data);
                    alert('Erreur de sauvegarde');
                }

            });
        });

        jQuery('#option_change').change(function () {

            ids=$('#option_change').val();
            dataid="id="+ids;
            jQuery.ajax({
                url: "<?php echo site_url('admin/Newsletter_commercant_backoffice/load_stat_news');?>",
                dataType: 'html',
                type: 'POST',
                data: dataid,
                success: function (data) {
                    let data_parsed=jQuery.parseJSON(data);
                    console.log(data_parsed.content);
                        CKEDITOR.instances['txtContenu'].setData(data_parsed.content);
                        $('#txtSujet').val(data_parsed.object);
                        $('#sendonemail').val(data_parsed.send_mail_adress);
                },
                error: function (data) {
                    console.log(data);
                    alert('Erreur de sauvegarde');
                }

            });

        });

        jQuery('#supprime_change').change(function () {

            ids=$('#supprime_change').val();
            dataid="id="+ids;
            jQuery.ajax({
                url: "<?php echo site_url('admin/Newsletter_commercant_backoffice/supprimer_stat_news');?>",
                dataType: 'html',
                type: 'POST',
                data: dataid,
                success: function(data){
                  document.location.reload();
                },
                error: function(data){
                    alert('il y a une erreur de suppression');
                },
            });

        });

        jQuery("#save_content").click(function () {
            $("#frmNewsletter").attr("action","<?php echo site_url("admin/Newsletter_commercant_backoffice/save_stat_news"); ?>");
            var nom=$('#nom').val();
            $('#nom_hidden').val(nom);
            var valued = editor.getData();
            $("#tosave_hidden").val(valued);
            $('#frmNewsletter').submit();
        });
        function htmlspecialchars(str) {
            return str.replace('&', '&amp;').replace('"', '&quot;').replace("'", '&#039;').replace('<', '&lt;').replace('>', '&gt;');
        }

            jQuery("#all_client_only").click(function () {
                if (jQuery(this).is(':checked')) {
                    jQuery("#all_client_only_value").val("1");
                } else {
                    jQuery("#all_client_only_value").val("0");
                }
            });

            jQuery("#all_fclient_only").click(function () {
                if (jQuery(this).is(':checked')) {
                    jQuery("#all_false_client_only").val("1");
                } else {
                    jQuery("#all_false_client_only").val("0");
                }
            });

            jQuery("#all_commentclient_only").click(function () {
                if (jQuery(this).is(':checked')) {
                    jQuery("#all_comment_client_only").val("1");
                } else {
                    jQuery("#all_comment_client_only").val("0");
                }
            });

        jQuery("#all_commentclientcsv_only").click(function () {
            if (jQuery(this).is(':checked')) {
                jQuery("#all_commentclient_csv_only").val("1");
            } else {
                jQuery("#all_commentclient_csv_only").val("0");
            }
        });

        jQuery("#btnEnvoyer").click(function () {

            var nbtrue=0;
            var nbfalse=0;
            var nbcomment=0;
            var nbcsv=0;

            success=0;
            error=0;
            startexec1=1;
            startexec2=1;
            startexec4=1;

            var startexec=1;
            var newserror = '';
            var radiochoice = jQuery('input[type=radio][name=sendallnews]:checked').attr('value');
            if (radiochoice == 2) {
                if (jQuery("#sendonemail").val() == "") {
                    newserror = '<span style="color:#F00;">Specifier l\'adresse email pour envoyer le Newsletter !</span>';
                    jQuery("#errornewsfrm").html(newserror);
                } else if (!isEmail(jQuery("#sendonemail").val())) {
                    newserror = '<span style="color:#F00;">Veuillez ajouter un email valide !</span>';
                    jQuery("#errornewsfrm").html(newserror);
                } else newserror = '';
            } else if (radiochoice == 0) {
                newserror = '';
            }

            if (newserror == '') {
                var nbtrueclient=parseInt('<?php echo $nbtrueclient;?>');
                var nbfalseclient=parseInt('<?php echo $nbfalseclient;?>');
                var nbcommentclient=parseInt('<?php echo $nbcommentclient;?>');
                var nbcsvclient=parseInt('<?php echo $nbcsvclient;?>');
                var nbexec_global=0;
                //alert($("#all_client_only_value").val());
                //alert(editor.getData());
                // data="sendallnews="+$('#sendonesss').val()+"&all_client_only_value="+$("#all_client_only_value").val()+'&all_false_client_only_value='+$('#all_false_client_only').val()+'&sendonemail='+$("#sendonemail").val()+"&all_comment_client_only_value="+$("#all_comment_client_only").val()+"&all_csv_client_only_value="+$("#all_commentclient_csv_only").val()+"&txtSujet="+$("#txtSujet").val()+"&txtContenu="+editor.getData()+"&IdCommercant="+$("#idcom_val").val();
                data={
                    "sendallnews":$('#sendonesss').val(),
                    "all_client_only_value":$("#all_client_only_value").val(),
                    "all_false_client_only_value":$('#all_false_client_only').val(),
                    "sendonemail":$("#sendonemail").val(),
                    "all_comment_client_only_value":$("#all_comment_client_only").val(),
                    "all_csv_client_only_value":$("#all_commentclient_csv_only").val(),
                    "txtSujet":$("#txtSujet").val(),
                    "txtContenu":editor.getData(),
                    "IdCommercant":$("#idcom_val").val()
                };
                if($('#all_client_only_value').val()==="1"){

                    nbexec_global+=nbtrueclient;
                }
                if($('#all_client_only_value').val()==="1"){

                    nbexec_global+=nbfalseclient;
                }
                if($('#all_comment_client_only').val()==="1"){

                    nbexec_global+=nbcommentclient;
                }
                if($('#all_commentclient_csv_only').val()==="1"){

                    nbexec_global+=nbcsvclient;
                }
                if($('#sendonesss').val()==='2'){

                jQuery.ajax({
                    url: "<?php echo site_url("admin/Newsletter_commercant_backoffice/envoyer_newletter_one/") ?>",
                    dataType: 'json',
                    type: 'POST',
                    data: data,
                    success: function (data) {
                        if (data=='1'){
                            success+=1;
                            $('#success_statut').html(success);
                            alert('reussi!!');
                        }else{
                            error+=1;
                            $('#error_statut').html(error);
                        }
                    },
                    error: function (data) {
                        error+=1;
                        $('#error_statut').html(error);
                    }

                });
            }
                if ($('#sendallnews').val()==='1' && $('#all_client_only_value').val()==="1"){
                    var nbexec=nbtrueclient;
                    while(startexec <= nbexec) {
                        jQuery.ajax({
                            url: "<?php echo site_url("admin/Newsletter_commercant_backoffice/envoyer_newletter_true/") ?>"+nbtrue,
                            dataType: 'html',
                            type: 'POST',
                            data: data,
                            success: function (data) {
                                if (data=='ok'){
                                    success+=1;
                                    $('#success_statut').html(success);
                                    alert('reussi!!');
                                }else{
                                    error+=1;
                                    $('#error_statut').html(error);
                                }
                            },
                            error: function (data) {
                                error+=1;
                                $('#error_statut').html(error);
                            }

                        });
                        startexec+=1;
                        nbtrue++;
                    }
                }
                if ($('#sendallnews').val()==='1' && $('#all_false_client_only').val()==="1"){
                    var nbexec1=nbfalseclient;
                    while(startexec1 <= nbexec1) {

                        jQuery.ajax({
                            url: "<?php echo site_url("admin/Newsletter_commercant_backoffice/envoyer_newletter_false/"); ?>"+nbfalse,
                            dataType: 'html',
                            type: 'POST',
                            data: data,
                            success: function (data) {
                                if (data=='ok'){
                                    success+=1;
                                    $('#success_statut').html(success);
                                    alert('reussi!!');
                                }else{
                                    error+=1;
                                    $('#error_statut').html(error);
                                }
                            },
                            error: function (data) {
                                error+=1;
                                $('#error_statut').html(error);
                            }

                        });
                        startexec1+=1;
                        nbfalse++;
                    }
                }

                if ($('#sendallnews').val()==='1' && $('#all_comment_client_only').val()==="1"){
                    var nbexec2=nbcommentclient;
                    while(startexec2 <= nbexec2) {
                        jQuery.ajax({
                            url: "<?php echo site_url("admin/Newsletter_commercant_backoffice/envoyer_newletter_comment/"); ?>"+nbcomment,
                            dataType: 'html',
                            type: 'POST',
                            data: data,
                            success: function (data) {
                                if (data=='ok'){
                                    success+=1;
                                    $('#success_statut').html(success);
                                    alert('reussi!!');
                                }else{
                                    error+=1;
                                    $('#error_statut').html(error);
                                }
                            },
                            error: function (data) {
                                error+=1;
                                $('#error_statut').html(error);
                            }

                        });
                        startexec2+=1;
                        nbcomment++;
                    }
                }
                if ($('#sendallnews').val()==='1' && $('#all_commentclient_csv_only').val()==="1"){
                    var nbexec4=nbcsvclient;
                    while(startexec4 <= nbexec4) {
                        jQuery.ajax({
                            url: "<?php echo site_url("admin/Newsletter_commercant_backoffice/envoyer_newletter_csv/"); ?>"+nbcsv,
                            dataType: 'html',
                            type: 'POST',
                            data: data,
                            success: function (data) {
                                if (data=='ok'){
                                    success+=1;
                                    $('#success_statut').html(success);
                                    alert('reussi!!');
                                }else{
                                    error+=1;
                                    $('#error_statut').html(error);
                                }

                            },
                            error: function (data) {
                                error+=1;
                                $('#error_statut').html(error);
                            }

                        });
                        startexec4++;
                        nbcsv++;
                    }
                }

            }else{alert('df')}
        });

           jQuery("#categ_to_show").change(function () {
                CKEDITOR.instances['txtContenu'].setData('');
                if(jQuery("#categ_to_show").val() =='agenda'){
                    jQuery("#to_shox").css('display','block');
                    jQuery("#to_shox_article").css('display','none');
                    jQuery("#to_shox_annonce").css('display','none');
                    jQuery("#to_shox_bonplan").css('display','none');
                    jQuery("#to_shox_fidelity").css('display','none');
                }
                else if(jQuery("#categ_to_show").val() =='article'){
                    jQuery("#to_shox_article").css('display','block');
                    jQuery("#to_shox").css('display','none');
                    jQuery("#to_shox_annonce").css('display','none');
                    jQuery("#to_shox_bonplan").css('display','none');
                    jQuery("#to_shox_fidelity").css('display','none');
                }
                else if(jQuery("#categ_to_show").val() =='boutique'){
                    jQuery("#to_shox_annonce").css('display','block');
                    jQuery("#to_shox").css('display','none');
                    jQuery("#to_shox_article").css('display','none');
                    jQuery("#to_shox_bonplan").css('display','none');
                    jQuery("#to_shox_fidelity").css('display','none');
                }
                else if(jQuery("#categ_to_show").val() =='bonplan'){
                    jQuery("#to_shox_bonplan").css('display','block');
                    jQuery("#to_shox").css('display','none');
                    jQuery("#to_shox_article").css('display','none');
                    jQuery("#to_shox_annonce").css('display','none');
                    jQuery("#to_shox_fidelity").css('display','none');
                }
                else if(jQuery("#categ_to_show").val() =='fidelite'){
                    jQuery("#to_shox_bonplan").css('display','none');
                    jQuery("#to_shox").css('display','none');
                    jQuery("#to_shox_article").css('display','none');
                    jQuery("#to_shox_annonce").css('display','none');
                    jQuery("#to_shox_fidelity").css('display','block');
                }
                else{
                    jQuery("#to_shox").css('display','none');
                    jQuery("#to_shox_article").css('display','none');
                    jQuery("#to_shox_annonce").css('display','none');
                    jQuery("#to_shox_bonplan").css('display','none');
                    jQuery("#to_shox_fidelity").css('display','none');
                }
            if(jQuery('#categ_to_show').val()!='non'){
                let idcom="<?php echo $IdCommercant; ?>";
                let categ=jQuery('#categ_to_show').val();
                data="idcommercant="+idcom+"&categ="+categ;
                jQuery.ajax({
                    url: "<?php echo site_url('admin/Newsletter_commercant_backoffice/get_commercant_data/');?>",
                    dataType: 'html',
                    type: 'POST',
                    data: data,
                    success: function (data) {
                        CKEDITOR.instances['txtContenu'].setData(data);
                    },
                    error: function (data) {
                        $("#txtContenu").html('');
                    }
                });
            }
            });
        jQuery("#Filtre_date").change(function () {
            CKEDITOR.instances['txtContenu'].setData('');
            if(jQuery('#Filtre_date').val()!='0'){
                let idcom="<?php echo $IdCommercant; ?>";
                let filtre=jQuery('#Filtre_date').val();
                data="idcommercant="+idcom+"&filtre="+filtre;
                jQuery.ajax({
                    url: "<?php echo site_url('admin/Newsletter_commercant_backoffice/get_commercant_agenda/');?>",
                    dataType: 'html',
                    type: 'POST',
                    data: data,
                    success: function (data) {
                        CKEDITOR.instances['txtContenu'].setData(data);
                    },
                    error: function (data) {
                        $("#txtContenu").html('');
                    }
                });
            }
        });
        jQuery("#Filtre_date_article").change(function () {
            CKEDITOR.instances['txtContenu'].setData('');
            if(jQuery('#Filtre_date_article').val()!='0'){
                let idcom="<?php echo $IdCommercant; ?>";
                let filtre=jQuery('#Filtre_date_article').val();
                data="idcommercant="+idcom+"&filtre="+filtre;
                jQuery.ajax({
                    url: "<?php echo site_url('admin/Newsletter_commercant_backoffice/get_commercant_article/');?>",
                    dataType: 'html',
                    type: 'POST',
                    data: data,
                    success: function (data) {
                        CKEDITOR.instances['txtContenu'].setData(data);
                    },
                    error: function (data) {
                        $("#txtContenu").html('');
                    }
                });
            }
        });
        jQuery("#Filtre_date_annonce").change(function () {
            CKEDITOR.instances['txtContenu'].setData('');
            if(jQuery('#Filtre_date_annonce').val()!=''){
                let idcom="<?php echo $IdCommercant; ?>";
                let filtre=jQuery('#Filtre_date_annonce').val();
                data="idcommercant="+idcom+"&filtre="+filtre;
                jQuery.ajax({
                    url: "<?php echo site_url('admin/Newsletter_commercant_backoffice/get_commercant_annonce/');?>",
                    dataType: 'html',
                    type: 'POST',
                    data: data,
                    success: function (data) {
                        CKEDITOR.instances['txtContenu'].setData(data);
                    },
                    error: function (data) {
                        $("#txtContenu").html('');
                    }
                });
            }
        });
        jQuery("#Filtre_date_bonplan").change(function () {
            CKEDITOR.instances['txtContenu'].setData('');
            if(jQuery('#Filtre_date_bonplan').val()!=''){
                let idcom="<?php echo $IdCommercant; ?>";
                let filtre=jQuery('#Filtre_date_bonplan').val();
                data="idcommercant="+idcom+"&filtre="+filtre;
                jQuery.ajax({
                    url: "<?php echo site_url('admin/Newsletter_commercant_backoffice/get_commercant_bonplan/');?>",
                    dataType: 'html',
                    type: 'POST',
                    data: data,
                    success: function (data) {
                        CKEDITOR.instances['txtContenu'].setData(data);
                    },
                    error: function (data) {
                        $("#txtContenu").html('');
                    }
                });
            }
        });
        jQuery("#Filtre_date_fidelity").change(function () {
            CKEDITOR.instances['txtContenu'].setData('');
            if(jQuery('#Filtre_date_fidelity').val()!=''){
                let idcom="<?php echo $IdCommercant; ?>";
                let filtre=jQuery('#Filtre_date_fidelity').val();
                data="idcommercant="+idcom+"&filtre="+filtre;
                jQuery.ajax({
                    url: "<?php echo site_url('admin/Newsletter_commercant_backoffice/get_commercant_fidelite/');?>",
                    dataType: 'html',
                    type: 'POST',
                    data: data,
                    success: function (data) {
                        CKEDITOR.instances['txtContenu'].setData(data);
                    },
                    error: function (data) {
                        $("#txtContenu").html('');
                    }
                });
            }
        });
    </script>
    <script type="text/javascript">
       /* function valide(){
           $('#form_insert').submit();
        }*/
        function rajouter(){
            let activebloc1=$('#is_activ_bloc').val();
            let typebloc1=$('#type_blocjs').val();
            let imagebloc1=$('#bloc_1_image1_content').val();
            let contentbloc1=CKEDITOR.instances['bloc1_content'].getData();
            // alert(contentbloc1);
            let activeboutonbloc1=$('#link_btn_bloc1_1').val();
            let contentboutonbloc1=$('#btn_bloc1_content').val();
            let valuelinkbloc1=$('#valuelink_bloc1').val();
            let customlinkbloc1=$('#link_htt_bloc1').val();
            //bloc1_2
            let imagebloc1_2=$('#bloc_1_image2_content').val();
            let contentbloc1_2=CKEDITOR.instances['bloc1_content2'].getData();
            let activeboutonbloc1_2=$('#link_btn_bloc1_2').val();
            let contentboutonbloc1_2=$('#btn_bloc1_content2').val();
            let valuelinkbloc1_2=$('#valuelink_bloc1').val();
            let customlinkbloc1_2=$('#link_htt_bloc2').val();

            //bloc2
            let activebloc2=$('#is_activ_bloc2').val();
            let typebloc2=$('#type_bloc2js').val();
            let imagebloc2=$('#bloc_1_2_image1_content').val();
            let contentbloc2=CKEDITOR.instances['bloc1_2_content'].getData();
            let activeboutonbloc2=$('#link_btn_bloc2_1').val();
            let contentboutonbloc2=$('#btn_bloc1_2_content1').val();
            let valuelinkbloc2=$('#valuelink_2_bloc1').val();
            let customlinkbloc2=$('#link_htt_bloc1_2').val();
            //bloc2_2
            let imagebloc2_2=$('#bloc_1_2_image2_content').val();
            let contentbloc2_2=CKEDITOR.instances['bloc1_2_content2'].getData();
            let activeboutonbloc2_2=$('#link_btn_bloc2_2').val();
            let contentboutonbloc2_2=$('#btn_bloc1_2_content2').val();
            let valuelinkbloc2_2=$('#valuelink_2_bloc1').val();
            let customlinkbloc2_2=$('#link_htt_bloc2').val();

            //bloc3
            let activebloc3=$('#is_activ_bloc3').val();
            let typebloc3=$('#type_bloc3js').val();
            let imagebloc3=$('#bloc_1_3_image1_content').val();
            let contentbloc3=CKEDITOR.instances['bloc1_3_content'].getData();
            let activeboutonbloc3=$('#link_btn_bloc3_1').val();
            let contentboutonbloc3=$('#btn_bloc1_3_content1').val();
            let valuelinkbloc3=$('#valuelink_3_bloc1').val();
            let customlinkbloc3=$('#link_htt_bloc1_3').val();
            //bloc3_2
            let imagebloc3_2=$('#bloc_1_3_image2_content').val();
            let contentbloc3_2=CKEDITOR.instances['bloc1_3_content2'].getData();
            let activeboutonbloc3_2=$('#link_btn_bloc3_2').val();
            let contentboutonbloc3_2=$('#btn_bloc1_3_content2').val();
            let valuelinkbloc3_2=$('#valuelink_3_bloc1').val();
            let customlinkbloc3_2=$('#link_htt_bloc2').val();
            let activebloc4=$('#is_activ_bloc4').val();
            let typebloc4=$('#type_bloc4js').val();
            let imagebloc4=$('#bloc_1_4_image1_content').val();
            let contentbloc4=CKEDITOR.instances['bloc_1_4_content'].getData();
            let activeboutonbloc4=$('#link_btn_bloc4_1').val();
            let contentboutonbloc4=$('#btn_bloc1_4_content').val();
            let valuelinkbloc4=$('#valuelink_4_bloc1').val();
            let customlinkbloc4=$('#link_htt_bloc_1_4').val();
            let imagebloc4_2=$('#bloc_1_4_image2_content').val();
            let contentbloc4_2=CKEDITOR.instances['bloc1_4_content2'].getData();
            let activeboutonbloc4_2=$('#link_btn_bloc4_2').val();
            let contentboutonbloc4_2=$('#btn_bloc1_4_content2').val();
            let valuelinkbloc4_2=$('#valuelink_4_bloc1').val();
            let customlinkbloc4_2=$('#link_htt_bloc2').val();
            let data="activebloc1="+activebloc1+"&typebloc1="+typebloc1+"&imagebloc1="+imagebloc1+"&contentbloc1="+contentbloc1+"&activeboutonbloc1="+activeboutonbloc1+ "&contentboutonbloc1="+contentboutonbloc1+"&valuelinkbloc1="+valuelinkbloc1+ "&customlinkbloc1="+customlinkbloc1+"&imagebloc1_2="+imagebloc1_2+ "&contentbloc1_2="+contentbloc1_2+ "&activeboutonbloc1_2="+activeboutonbloc1_2+ "&contentboutonbloc1_2="+contentboutonbloc1_2+ "&valuelinkbloc1_2="+valuelinkbloc1_2+ "&customlinkbloc1_2="+customlinkbloc1_2+ "&activebloc2="+activebloc2+ "&typebloc2="+typebloc2+ "&imagebloc2="+imagebloc2+ "&contentbloc2="+contentbloc2+ "&activeboutonbloc2="+activeboutonbloc2+ "&contentboutonbloc2="+contentboutonbloc2+ "&valuelinkbloc2="+valuelinkbloc2+ "&customlinkbloc2="+customlinkbloc2+ "&imagebloc2_2="+imagebloc2_2+ "&contentbloc2_2="+contentbloc2_2+ "&activeboutonbloc2_2="+activeboutonbloc2_2+ "&contentboutonbloc2_2="+contentboutonbloc2_2+ "&valuelinkbloc2_2="+valuelinkbloc2_2+ "&customlinkbloc2_2="+customlinkbloc2_2+ "&activebloc3="+activebloc3+ "&typebloc3="+typebloc3+ "&imagebloc3="+imagebloc3+ "&contentbloc3="+contentbloc3+ "&activeboutonbloc3="+activeboutonbloc3+ "&contentboutonbloc3="+contentboutonbloc3+ "&valuelinkbloc3="+valuelinkbloc3+ "&customlinkbloc3="+customlinkbloc3+ "&imagebloc3_2="+imagebloc3_2+ "&contentbloc3_2="+contentbloc3_2+ "&activeboutonbloc3_2="+activeboutonbloc3_2+ "&contentboutonbloc3_2="+contentboutonbloc3_2+ "&valuelinkbloc3_2="+valuelinkbloc3_2+ "&customlinkbloc3_2="+customlinkbloc3_2+ "&activebloc4="+activebloc4+ "&typebloc4="+typebloc4+ "&imagebloc4="+imagebloc4+ "&contentbloc4="+contentbloc4+ "&activeboutonbloc4="+activeboutonbloc4+ "&contentboutonbloc4="+contentboutonbloc4+ "&valuelinkbloc4="+valuelinkbloc4+ "&customlinkbloc4="+customlinkbloc4+ "&imagebloc4_2="+imagebloc4_2+ "&contentbloc4_2="+contentbloc4_2+ "&activeboutonbloc4_2="+activeboutonbloc4_2+ "&contentboutonbloc4_2="+contentboutonbloc4_2+ "&valuelinkbloc4_2="+valuelinkbloc4_2+ "&customlinkbloc4_2="+customlinkbloc4_2;

            var field= CKEDITOR.instances['txtContenu'].getData();
            alert(field);
                    console.log(field);
                    jQuery.ajax({
                    url: "<?php echo site_url('admin/Newsletter_commercant_backoffice/envoyer_newletter/');?>",
                    dataType: 'html',
                    type: 'POST',
                    data: data,
                    success: function (data) {
                        CKEDITOR.instances['txtContenu'].setData(field+data);
                    },
                    error: function (data) {
                        $("#txtContenu").html('');
                    }
                });
        }
        function remplacer(){

             let activebloc1=$('#is_activ_bloc').val();
             let typebloc1=$('#type_blocjs').val();
             // let imagebloc1=$('#bloc_1_image1_content').val();
             let contentbloc1=CKEDITOR.instances['bloc1_content'].getData();
             // alert(contentbloc1);
             let activeboutonbloc1=$('#link_btn_bloc1_1').val();
             let contentboutonbloc1=$('#btn_bloc1_content').val();
             let valuelinkbloc1=$('#valuelink_bloc1').val();
             let customlinkbloc1=$('#link_htt_bloc1').val();
             //bloc1_2
             // let imagebloc1_2=$('#bloc_1_image2_content').val();
             let contentbloc1_2=CKEDITOR.instances['bloc1_content2'].getData();
             let activeboutonbloc1_2=$('#link_btn_bloc1_2').val();
             let contentboutonbloc1_2=$('#btn_bloc1_content2').val();
             let valuelinkbloc1_2=$('#valuelink_bloc1').val();
             let customlinkbloc1_2=$('#link_htt_bloc2').val();

             //bloc2
             let activebloc2=$('#is_activ_bloc2').val();
             let typebloc2=$('#type_bloc2js').val();
             // let imagebloc2=$('#bloc_1_2_image1_content').val();
             let contentbloc2=CKEDITOR.instances['bloc1_2_content'].getData();
             let activeboutonbloc2=$('#link_btn_bloc2_1').val();
             let contentboutonbloc2=$('#btn_bloc1_2_content1').val();
             let valuelinkbloc2=$('#valuelink_2_bloc1').val();
             let customlinkbloc2=$('#link_htt_bloc1_2').val();
             //bloc2_2
             // let imagebloc2_2=$('#bloc_1_2_image2_content').val();
             let contentbloc2_2=CKEDITOR.instances['bloc1_2_content2'].getData();
             let activeboutonbloc2_2=$('#link_btn_bloc2_2').val();
             let contentboutonbloc2_2=$('#btn_bloc1_2_content2').val();
             let valuelinkbloc2_2=$('#valuelink_2_bloc1').val();
             let customlinkbloc2_2=$('#link_htt_bloc2').val();            

             //bloc3
             let activebloc3=$('#is_activ_bloc3').val();
             let typebloc3=$('#type_bloc3js').val();
             // let imagebloc3=$('#bloc_1_3_image1_content').val();
             let contentbloc3=CKEDITOR.instances['bloc1_3_content'].getData();
             let activeboutonbloc3=$('#link_btn_bloc3_1').val();
             let contentboutonbloc3=$('#btn_bloc1_3_content1').val();
             let valuelinkbloc3=$('#valuelink_3_bloc1').val();
             let customlinkbloc3=$('#link_htt_bloc1_3').val();             
             //bloc3_2
             // let imagebloc3_2=$('#bloc_1_3_image2_content').val();
             let contentbloc3_2=CKEDITOR.instances['bloc1_3_content2'].getData();
             let activeboutonbloc3_2=$('#link_btn_bloc3_2').val();
             let contentboutonbloc3_2=$('#btn_bloc1_3_content2').val();
             let valuelinkbloc3_2=$('#valuelink_3_bloc1').val();
             let customlinkbloc3_2=$('#link_htt_bloc2').val();             
             let activebloc4=$('#is_activ_bloc4').val();
             let typebloc4=$('#type_bloc4js').val();
             // let imagebloc4=$('#bloc_1_4_image1_content').val();
             let contentbloc4=CKEDITOR.instances['bloc_1_4_content'].getData();
             let activeboutonbloc4=$('#link_btn_bloc4_1').val();
             let contentboutonbloc4=$('#btn_bloc1_4_content').val();
             let valuelinkbloc4=$('#valuelink_4_bloc1').val();
             let customlinkbloc4=$('#link_htt_bloc_1_4').val();            
             // let imagebloc4_2=$('#bloc_1_4_image2_content').val();
             let contentbloc4_2=CKEDITOR.instances['bloc1_4_content2'].getData();
             let activeboutonbloc4_2=$('#link_btn_bloc4_2').val();
             let contentboutonbloc4_2=$('#btn_bloc1_4_content2').val();
             let valuelinkbloc4_2=$('#valuelink_4_bloc1').val();
             let customlinkbloc4_2=$('#link_htt_bloc2').val();
             let data="activebloc1="+activebloc1+"&typebloc1="+typebloc1+"&contentbloc1="+contentbloc1+"&activeboutonbloc1="+activeboutonbloc1+ "&contentboutonbloc1="+contentboutonbloc1+"&valuelinkbloc1="+valuelinkbloc1+ "&customlinkbloc1="+customlinkbloc1+ "&contentbloc1_2="+contentbloc1_2+ "&activeboutonbloc1_2="+activeboutonbloc1_2+ "&contentboutonbloc1_2="+contentboutonbloc1_2+ "&valuelinkbloc1_2="+valuelinkbloc1_2+ "&customlinkbloc1_2="+customlinkbloc1_2+ "&activebloc2="+activebloc2+ "&typebloc2="+typebloc2+  "&contentbloc2="+contentbloc2+ "&activeboutonbloc2="+activeboutonbloc2+ "&contentboutonbloc2="+contentboutonbloc2+ "&valuelinkbloc2="+valuelinkbloc2+ "&customlinkbloc2="+customlinkbloc2+ "&contentbloc2_2="+contentbloc2_2+ "&activeboutonbloc2_2="+activeboutonbloc2_2+ "&contentboutonbloc2_2="+contentboutonbloc2_2+ "&valuelinkbloc2_2="+valuelinkbloc2_2+ "&customlinkbloc2_2="+customlinkbloc2_2+ "&activebloc3="+activebloc3+ "&typebloc3="+typebloc3+  "&contentbloc3="+contentbloc3+ "&activeboutonbloc3="+activeboutonbloc3+ "&contentboutonbloc3="+contentboutonbloc3+ "&valuelinkbloc3="+valuelinkbloc3+ "&customlinkbloc3="+customlinkbloc3+  "&contentbloc3_2="+contentbloc3_2+ "&activeboutonbloc3_2="+activeboutonbloc3_2+ "&contentboutonbloc3_2="+contentboutonbloc3_2+ "&valuelinkbloc3_2="+valuelinkbloc3_2+ "&customlinkbloc3_2="+customlinkbloc3_2+ "&activebloc4="+activebloc4+ "&typebloc4="+typebloc4+ "&contentbloc4="+contentbloc4+ "&activeboutonbloc4="+activeboutonbloc4+ "&contentboutonbloc4="+contentboutonbloc4+ "&valuelinkbloc4="+valuelinkbloc4+ "&customlinkbloc4="+customlinkbloc4+  "&contentbloc4_2="+contentbloc4_2+ "&activeboutonbloc4_2="+activeboutonbloc4_2+ "&contentboutonbloc4_2="+contentboutonbloc4_2+ "&valuelinkbloc4_2="+valuelinkbloc4_2+ "&customlinkbloc4_2="+customlinkbloc4_2;
              jQuery.ajax({
                    url: "<?php echo site_url('admin/Newsletter_commercant_backoffice/envoyer_newletter/');?>",
                    dataType: 'html',
                    type: 'POST',
                    data: data,
                    success: function (datas) {
                        CKEDITOR.instances['txtContenu'].setData(datas);
                    },
                    error: function (datas) {
                        $("#txtContenu").html('');
                    }
            });
        }
    </script>
    <div class="container">
        <div class="col-lg-12 pl-0">
            <img src="<?php echo base_url('assets/img/tt_n.png')?>" style="width:100%;">
        </div>
        <div class="col-lg-12 p-0" style="width=100%;">
            <form id ="form_insert" action="<?php echo site_url()?>/admin/Newsletter_commercant_backoffice/Ajouter_bloc" method="post">
                <div class="row">
                    <div class="col-lg-9 p-3 mt-4">
                        <!-- debut bloc -->
                        <!-- debut bloc1 -->
                        <?php $this->load->view('admin/newsletter_commercant/includes/bloc1_newsletter'); ?>
                        <?php $this->load->view('admin/newsletter_commercant/includes/jsbloc1_newsletter'); ?>
                        <!-- fin bloc1 -->
                        <!-- debut bloc2 -->
                        <?php $this->load->view('admin/newsletter_commercant/includes/bloc2_newsletter'); ?>
                        <?php $this->load->view('admin/newsletter_commercant/includes/jsbloc2_newsletter'); ?>
                        <!-- fin bloc2 -->
                        <!-- debut bloc3 -->
                        <?php $this->load->view('admin/newsletter_commercant/includes/bloc3_newsletter'); ?>
                        <?php $this->load->view('admin/newsletter_commercant/includes/jsbloc3_newsletter'); ?>
                        <!-- fin bloc3 -->
                        <!-- debut bloc4 -->
                        <?php $this->load->view('admin/newsletter_commercant/includes/bloc4_newsletter'); ?>
                        <?php $this->load->view('admin/newsletter_commercant/includes/jsbloc4_newsletter'); ?>
                        <!-- fin bloc4 -->
                        <!-- fin bloc -->
                    </div>
                    <div class="col-lg-3 p-3 mt-4">
                        <div class="row">
                           <!-- <div class="col-lg-12 pt-0 ml-5">
                                <input id="btnvalider" class="text-center" style="font-size:20px;font-family: times New Roman;height:35px;width:198px;background-color:green;color:white;cursor: pointer;" onclick="valide()" value="Enregistrer le contenu"> -->
                            </div>
                            <div class="col-lg-12 pt-3 ml-5">
                                <input type="button" id="btnvalider" class="text-center" style="font-size:20px;font-family: times New Roman;height:35px;width:198px;background-color:green;color:white;cursor: pointer;" onclick="remplacer()" value="Remplacer">
                            </div>
                            <div class="col-lg-12 pt-3 ml-5">
                                <input id="btnenvoyer" type="button" class="text-center" style="font-size:20px;font-family: times New Roman; background-color:green;color:white;height:35px;width:198px;" onclick="rajouter()" value="Rajouter">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>