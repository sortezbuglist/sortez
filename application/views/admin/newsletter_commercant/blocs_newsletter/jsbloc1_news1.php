<script type="text/javascript">
    jQuery(document).ready(function () {


        $("#is_activ_bloc_news1").change(function () {
            if (document.getElementById('is_activ_bloc_news1').value == 0) {
                jQuery('#type_bloc_news1_choose').css("display", "none");

                jQuery('#bloc1_news1').css("display", "none");

                jQuery('#bloc2_news1').css("display", "none");


            }
            else if (document.getElementById('is_activ_bloc_news1').value == 1) {
                jQuery('#type_bloc_news1_choose').css("display", "block");

                jQuery('#bloc1_news1').css("display", "none");

                jQuery('#bloc2_news1').css("display", "none");


            }
        });
        $("#1b1_1_news1").click(function () {
            document.getElementById('bloc1_news1').style.display = "block";
            document.getElementById('bloc2_news1').style.display = "none";
            document.getElementById('bonplan_config_news1').style.display = "none";
            document.getElementById('fidelity_config_news1').style.display = "none";
            $('#type_bloc_news1js').val("1");
            $("#bloc1_news1").removeClass("col-lg-6");
            $("#bloc2_news1").removeClass("col-lg-6");
        });
        $("#2b1_1_news1").click(function () {
            document.getElementById('bloc1_news1').style.display = "block";
            document.getElementById('bloc2_news1').style.display = "block";
            document.getElementById('bonplan_config_news1').style.display = "none";
            document.getElementById('fidelity_config_news1').style.display = "none";
            $('#type_bloc_news1js').val("2");
            $("#bloc1_news1").addClass("col-lg-6");
            $("#bloc2_news1").addClass("col-lg-6");
        });
        $("#1bp1_1_news1").click(function () {
            document.getElementById('bonplan_config_news1').style.display = "block";
            document.getElementById('bloc1_news1').style.display = "none";
            document.getElementById('bloc2_news1').style.display = "none";
            document.getElementById('fidelity_config_news1').style.display = "none";
            $('#type_bloc_news1js').val("1bp");
        });
        $("#1fd1_1_news1").click(function () {
            document.getElementById('fidelity_config_news1').style.display = "block";
            document.getElementById('bonplan_config_news1').style.display = "none";
            document.getElementById('bloc1_news1').style.display = "none";
            document.getElementById('bloc2_news1').style.display = "none";
            $('#type_bloc_news1js').val("1fd");
        });
        $("#link_btn_bloc1_news1").change(function () {

            if (document.getElementById('link_btn_bloc1_news1').value == 1) {
                document.getElementById('link_bloc1_news1').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc1_news1').value == 0) {
                document.getElementById('link_bloc1_news1').style.display = "none";

            }

        });
        $("#link_btn_bloc2_news1").change(function () {


            if (document.getElementById('link_btn_bloc2_news1').value == 1) {
                document.getElementById('link_bloc2_news1').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc2_news1').value == 0) {
                document.getElementById('link_bloc2_news1').style.display = "none";

            }

        });
        $("#link_btn_bp_news1").change(function () {

            if (document.getElementById('link_btn_bp_news1').value == 1) {
                document.getElementById('link_bp_news1').style.display = "block";

            }
            else if (document.getElementById('link_btn_bp_news1').value == 0) {
                document.getElementById('link_bp_news1').style.display = "none";

            }

        });
        $("#link_btn_fd_news1").change(function () {

            if (document.getElementById('link_btn_fd_news1').value == 1) {
                document.getElementById('link_fd_news1').style.display = "block";

            }
            else if (document.getElementById('link_btn_fd_news1').value == 0) {
                document.getElementById('link_fd_news1').style.display = "none";

            }

        });
    });
</script>