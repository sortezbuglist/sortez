<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" lang="fr">
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.js"></script>

</head>

<body style=" background-image: url('https://www.sortez.org/wpimages/wpe3440087_06.jpg')!important">
<div class="container" style="background-color: white;height: 100%">

    <div class="row">
        <div class="col-lg-12 text-center p-3"><a CLASS="btn btn-info" href="<?php echo site_url()?>">RETOUR SITE</a></div>
        <div class="col-lg-12 text-center p-3"><a CLASS="btn btn-info" href="<?php echo site_url('auth/login')?>">RETOUR MENU</a></div>
        <div class="col-lg-12 text-center p-3"><a CLASS="btn btn-info" href="<?php echo site_url('admin/Newsletter_commercant_backoffice')?>">RETOUR NEWSLETTER</a></div>
    </div>

    <div class="row pt-3">
        <div class="col-lg-12 text-center"><h3 style="text-decoration: underline">LISTE DES CLIENTS VIVRESAVILLES</h3></div>
    </div>
    <div class="row pt-3 text-center">
        <div class="col-lg-12 text-center">
            <table cellpadding="5" class="table text-center" style="">
                <thead class="text-center">
                <tr>
                    <th>Id</th>
                    <th>Nom</th>
                    <th>Email</th>
                    <!--<th>&nbsp;ACTION</th>!-->
                </tr>
                </thead>
                <tbody class="text-center">
                <?php if(sizeof($client)) { ?>
                    <?php foreach($client as $objClient) { ?>
                        <tr>
                            <td><?php if(isset($objClient->idclient)) echo $objClient->idclient; ?></td>

                            <td>
                                <?php if(isset($objClient->nom)) echo $objClient->nom; ?>
                            </td>
                            <td>
                                <?php if(isset($objClient->email)) echo $objClient->email; ?>
                            </td>
                            <!--<td>
                                <a class="btn btn-danger" href="javascript:void();">IdUser;?>);">
                                    Supprimer
                                </a>
                            </td>
                            !-->
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>