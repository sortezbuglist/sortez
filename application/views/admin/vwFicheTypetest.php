<?php $data["zTitle"] = 'Inscription des professionnels'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

    <script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
			jQuery('#loading_datefin_plus_un_an').hide();
			
            jQuery(".btnSinscrire").click(function(){
                var txtError = "";
				
				var RubriqueSociete = jQuery("#RubriqueSociete").val();
                if(RubriqueSociete=="0" || RubriqueSociete=="") {
                    jQuery("#divErrorRubriqueSociete").html("Un commercant doit être assigné à une rubrique.");
                    jQuery("#divErrorRubriqueSociete").show();
                    txtError += "- Un commercant doit être assigné à une rubrique.<br/>";
                } else {
                    jQuery("#divErrorRubriqueSociete").hide();
                }
				
				var SousRubriqueSociete = jQuery("#SousRubriqueSociete").val();
                if(SousRubriqueSociete=="0" || SousRubriqueSociete=="") {
                    jQuery("#divErrorSousRubriqueSociete").html("Un commercant doit être assigné à une sous-rubrique.");
                    jQuery("#divErrorSousRubriqueSociete").show();
                    txtError += "- Un commercant doit être assigné à une sous-rubrique.<br/>";
                } else {
                    jQuery("#divErrorSousRubriqueSociete").hide();
                }
				
                var EmailSociete = jQuery("#EmailSociete").val();
                if(!isEmail(EmailSociete)) {
                    jQuery("#divErrorEmailSociete").html("Ce Courriel n'est pas valide. Veuillez saisir un email valide");
                    jQuery("#divErrorEmailSociete").show();
                    txtError += "- Ce Courriel n'est pas valide. Veuillez saisir un email valide.<br/>";
                } else {
                    jQuery("#divErrorEmailSociete").hide();
                }
                
                var LoginSociete = jQuery("#LoginSociete").val();
                if(!isEmail(LoginSociete)) {
                    txtError += "- Votre login doit &ecirc;tre un email valide.<br/>";
                    jQuery("#divErrorLoginSociete").html("Votre login doit &ecirc;tre un email valide.");
                    jQuery("#divErrorLoginSociete").show();
                } else {
                    jQuery("#divErrorLoginSociete").hide();
                }
				
				
				jQuery("#total_error_admin_fiche_comm").html(txtError);
				
                if(txtError == "") {
                    jQuery("#frmInscriptionProfessionnel").submit();
                }
            });
			
			
			jQuery("#IsActifSociete").click(function(){
							var DateDebut = jQuery("#DateDebut").val();	
							var DateFin = jQuery("#DateFin").val();	
							
							if ((DateDebut=="" || DateDebut==null) && (DateFin=="" || DateFin==null)) {
								//alert("valeur null");
								DateDebut = "<?php echo convert_Sqldate_to_Frenchdate(date("Y-m-d")); ?>";
								<?php 
								$current_date = date("Y-m-d");
								$current_value = explode('-',$current_date);
								?>
								//alert("sqdfqsf "+<?php echo $current_value[0]+1;?>);
								<?php 
								$year_datefin = $current_value[0]+1; 
								$date_fin_value = $year_datefin."-".$current_value[1]."-".$current_value[2];
								?>
								DateFin = "<?php echo convert_Sqldate_to_Frenchdate($date_fin_value);?>";
								//alert(DateDebut);
								//alert(DateFin);
								jQuery("#DateDebut").val(DateDebut);
								jQuery("#DateFin").val(DateFin);
							}
												   
				});
							
			jQuery("#btn_datefin_plus_un_an").click(function(){
							var DateDebut = jQuery("#DateDebut").val();	
							
							jQuery('#loading_datefin_plus_un_an').show();
							
							jQuery.post('<?php echo site_url("admin/commercants/add_datefin_plus_un_an" ) ; ?>',
								{ DateDebut: DateDebut},
								function success(data){
									jQuery("#DateFin").val(data);
									jQuery('#loading_datefin_plus_un_an').hide();
							 });
												   
				});				
							
        })

	</script>
	<script type="text/javascript">
		$(function() {
			$("#DateDebut").datepicker({
				dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
				dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
				monthNames: ['Janvier','Fevier','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
				dateFormat: 'DD, d MM yy',
				autoSize: true
			});
			$("#DateFin").datepicker({
				dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
				dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
				monthNames: ['Janvier','Fevier','Mars','Avril','Mai','Juin','Juiller','Aout','Septembre','Octobre','Novembre','Decembre'],
				dateFormat: 'DD, d MM yy',
				autoSize: true
			});
		}); 
			function effacerPhotoAccueil(_iCommercantId){
			jQuery.post('<?php echo site_url("admin/commercants/effacerPhotoCommercant" ) ; ?>',
				{ iCommercantId: _iCommercantId},
				function success(data){ 
					jQuery("#divPhotoAccueil").hide() ;
			 });
		}
		function effacerPhoto1(_iCommercantId){
			jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto1Commercant" ) ; ?>',
				{ iCommercantId: _iCommercantId},
				function success(data){
					jQuery("#divPhoto1").hide() ;
			 });
		}
		function effacerPhoto2(_iCommercantId){
			jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto2Commercant" ) ; ?>',
				{ iCommercantId: _iCommercantId},
				function success(data){
					jQuery("#divPhoto2").hide() ;
			 });
		}
		function effacerPhoto3(_iCommercantId){
			jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto3Commercant" ) ; ?>',
				{ iCommercantId: _iCommercantId},
				function success(data){
					jQuery("#divPhoto3").hide() ;
			 });
		}
		function effacerPhoto4(_iCommercantId){
			jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto4Commercant" ) ; ?>',
				{ iCommercantId: _iCommercantId},
				function success(data){
					jQuery("#divPhoto4").hide() ;
			 });
		}
		function effacerPhoto5(_iCommercantId){
			jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto5Commercant" ) ; ?>',
				{ iCommercantId: _iCommercantId},
				function success(data){
					jQuery("#divPhoto5").hide() ;
			 });
		}
		function effacerPdf(_iCommercantId){
			jQuery.post('<?php echo site_url("admin/commercants/effacerPdfCommercant" ) ; ?>',
				{ iCommercantId: _iCommercantId},
				function success(data){
					jQuery("#divPdf").hide() ;
			 });
		}
		
		function deleteFile(_IdCommercant,_FileName) {
			jQuery.ajax({
				url: gCONFIG["SITE_URL"] + 'front/professionnels/delete_files/' + _IdCommercant + '/' + _FileName,
				dataType: 'html',
				type: 'POST',
				async: true,
				success: function(data){
					window.location.reload();
				}
			});
		}

		function listeSousRubrique(){
			 
			var irubId = jQuery('#RubriqueSociete').val();     
			jQuery.get(
				'<?php echo site_url("front/professionnels/testAjax"); ?>' + '/' + irubId,
				function (zReponse)
				{
					// alert (zReponse) ;
					jQuery('#trReponseRub').html("") ; 
					jQuery('#trReponseRub').html(zReponse) ;
				   
				 
			   });
		}
	function testtestFormTypetest(){
	var test = $("#test").val () ;
	
	var zErreur = "" ;
	
	if (test == ""){
		zErreur += "Veuillez selectionner un test\n" ;
	}
	
	if (zErreur != ""){
		alert ('Veuillez indiquer un Nom !') ;
	}else{
		document.frmCreationTest.submit();
	}
	
	return false;
}
</script>
    
    <div id="divInscriptionProfessionnel" class="content" align="center">
	     <br>
	     <a style = "color:black;text-decoration: " href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" value= "Retour au Menu" id = "btn" onclick="document.location='<?php echo site_url("admin/home/");?>';" /> </a></br>
	      <a style = "color:black;text-decoration:none;" href = "<?php echo site_url("admin/commercants/liste/" ) ; ?>"> <input type = "button" value= "retour à la liste commercants" id = "btn" onclick="document.location='<?php echo site_url("admin/commercants/liste" );?>';"/> </a><br/>
         
		 <?php if (isset($_SESSION['error_fiche_com_admin'])) { ?>
         <div style="text-align:center; color:#FF0000;"><p><?php echo $_SESSION['error_fiche_com_admin']; ?></p></div>
         <?php unset($_SESSION['error_fiche_com_admin']);} ?>
         
        <form name="frmInscriptionProfessionnel" id="frmInscriptionProfessionnel" action="<?php if (isset($test)) { echo site_url("admin/test/modif_test/$testid"); }else{ echo site_url("admin/test/creer_test"); } ?>" method="POST">
		<input type="hidden" name="test[testid]" id="testid_id" value="<?php if (isset($oArticle)) { echo $oArticle->testid;} else {echo '0';} ?>" />
            <h1><?php echo $title ; ?></h1>
			<span> <?php if (isset($msg)) { echo $msg ; } ?></span>
            <fieldset>
                <legend>fiche Profesionnel</legend>
                <table width="100%" cellpadding="0" cellspacing="0" style="text-align:left;">
                
                    
					<tr>
                        <td width="300">
                                <label class="label">Logo : </label>
                                <br/>(la largeur ne doit pas d&eacute;passer 270px)
                           
                        </td>
                        <td>
                            <input type="text" name="test" id="test" value="<?php if (isset($oArticle)) { echo $oArticle->test ; } ?>" />
                        </td>
                    </tr>
					<tr>
                        <td> 
                                <?php if(!empty($objCommercant->Logo)) { ?>
                                    <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->Logo, 100, 100,'',''); ?>
                                    <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Logo');">Supprimer</a>
                                <?php } ?><br/></td>
                        <td align="left">
                        <input type="fille" name="nomlogo" id="logonom" value="Annuler" onclick="javascript:document.location='<?php echo site_url("admin/test/liste");?>';" />&nbsp;

                    <tr>
                        <td width="300">
                            <label class="label">Lien logo : </label>
                      </td>
                        <td>
                            <input type="text" name="Societe[logo_url]" id="logo_url" value="<?php if(isset($objCommercant->logo_url)) echo htmlspecialchars($objCommercant->logo_url); else echo 'http//';?>" />
                        </td>
                    </tr>
                    <tr>
                        <td width="300">
                            <label class="label">D&eacute;signation d'activit&eacute; : </label>
                      </td>
                        <td>
                        <input type="text" name="nom[titre_entete]" id="titre_entete" value="<?php if(isset($objtest->titre_entete)) echo htmlspecialchars($objtest->titre_entete);?>"  /></td>
                    </tr>
                     <tr>
                        <td>
                            <label>Rubrique : </label>
                        </td>
                        <td>
                            <select name="AssCommercantRubrique[IdRubrique]" id="RubriqueSociete" onchange="javascript:listeSousRubrique();">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($colRubriques)) { ?>
                                    <?php foreach($colRubriques as $objRubrique) { ?>
                                        <option <?php if(isset($objAssCommercantRubrique[0]->IdRubrique) &&$objAssCommercantRubrique[0]->IdRubrique == $objRubrique->IdRubrique) { ?>selected="selected"<?php } ?> value="<?php echo $objRubrique->IdRubrique; ?>"><?php echo $objRubrique->Nom; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <div class="FieldError" id="divErrorRubriqueSociete"></div>
                        </td>
                    </tr>
                    <tr id='trReponseRub'>
                        <td>
                            <label>Sous-rubrique : </label>
                        </td>
                        <td>
                            <select name="AssCommercantSousRubrique[IdSousRubrique]" id="SousRubriqueSociete">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($colSousRubriques)) { ?>
                                    <?php foreach($colSousRubriques as $objSousRubrique) { ?>
                                        <option <?php if(isset($objAssCommercantSousRubrique[0]->IdSousRubrique) && $objAssCommercantSousRubrique[0]->IdSousRubrique == $objSousRubrique->IdSousRubrique) { ?>selected="selected"<?php } ?> value="<?php echo $objSousRubrique->IdSousRubrique; ?>"><?php echo $objSousRubrique->Nom; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                             <div class="FieldError" id="divErrorSousRubriqueSociete"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Nom ; : </label>
                        </td>
                        <td>
                </table>
            </fieldset>
        </form>
    </div>


<?php $this->load->view("admin/includes/vwFooter2013"); ?>