<?php $data["zTitle"] = 'Gestion Localisation'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra'],  headers: {5: { sorter: false}, 6: {sorter: false} }})
				.tablesorterPager({container: $("#pager")});
		});
</script>
    <div id="divAdminHome" class="content" align="center">
	     <p><a style = "color:black; text-decoration:none;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input class="btn btn-primary" type = "button" value= "retour au menu" id = "btn" onclick="document.location='<?php echo site_url("admin/home/" );?>';"/>  </a></p>
		 <p><a style = "color:black;text-decoration:none;" href = "<?php echo site_url("admin/localisations/ficheLocalisation/0") ;?>"> <input class="btn btn-success" type = "button" value= "Ajouter Localisation" id = "btn"/>  </a></p>
        <br><div class="H1-C">Liste des Localisations</div><br>
        
        <div style="color:#33CC33"><?php if (isset($msg)) echo $msg;?></div>
        <table cellpadding="1" class="tablesorter" style="text-align:left">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Lieu</th>
                    <th>Adresse</th>
                    <th>Code postal</th>
                    <th>Ville</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
            <?php if (count($colLocalisations)>0) {?>
                <?php foreach($colLocalisations as $objLocalisation) { ?>
                    <tr>
                        <td>
                            <a href="<?php echo site_url("admin/localisations/ficheLocalisation/".$objLocalisation->location_id); ?>">
                                <?php echo htmlspecialchars(stripcslashes($objLocalisation->location_id)); ?>
                            </a>
                        </td>
                        <td><?php echo htmlspecialchars(stripcslashes($objLocalisation->location)); ?></td>
                        <td><?php echo htmlspecialchars(stripcslashes($objLocalisation->location_address)); ?></td>
                        <td><?php echo htmlspecialchars(stripcslashes($objLocalisation->location_postcode)); ?></td>
                        <td>
						<?php 
						$this->load->model("mdlville");
						$oVille = $this->mdlville->getVilleById($objLocalisation->location_villeid);
        				echo $oVille->Nom;
						?>
                        </td>
                        <td>
                            <a href="<?php echo site_url("admin/localisations/ficheLocalisation/".$objLocalisation->location_id); ?>">
                                Modifier
                            </a>
                        </td>
                        <td>
                            <a href="<?php echo site_url("admin/localisations/delete/".$objLocalisation->location_id) ; ?>">
                                Supprimer
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
            </tbody>
        </table>
		<div id="pager" class="pager">
			<img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
			<img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
			<input type="text" class="pagedisplay"/>
			<img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
			<img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
			<select class="pagesize" style="visibility:hidden">
				<option selected="selected"  value="10">10</option>
				<option value="20">20</option>
				<option value="30">30</option>
				<option  value="40">40</option>
			</select>
		</div>
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>