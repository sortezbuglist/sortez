<?php $data["zTitle"] = 'Import Datatourisme'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

    <div class="text-center">
<div class="formfields" style="width: 50%; margin: auto;">
    <button style="text-align: center;border: dashed; margin-bottom: 30px;" class="btn btn-primary">Ajouter un nouveau lien RSS</button><br>
    <button id="reset_data" onclick="if(confirm('Voulez vous effacer toutes les données RSS ?')){erase();}" style="margin-bottom: 10px" class="btn btn-danger text-right">Effacer toutes les données RSS</button>
    <div style="display: none;margin-bottom: 20px" id="progress_gif"><img src="<?php echo base_url(('assets/img/loader.gif'))?>"></div>
    <form name="form" id="form" action="<?php echo site_url("admin/xml/import") ?>" method="post">
        <div class="linkfield">
        <label class="labellink">Saisir le lien</label>
        <input class="form-control" required type="text" name="links" id="links" value="<?php if (isset($links) AND $links!=""){echo $links;} ?>">
        </div>
        <div id="categorie">

                    <label>Catégorie : </label>
                    <select class="form-control" required name="categ" onchange="listeSousRubrique();" id="IdCategory">
                        <option class="form-control" value="0">-- Veuillez choisir --</option>
                        <?php if(sizeof($colCategorie)) { ?>
                            <?php foreach($colCategorie as $objcolCategorie_xx) { ?>
                                <option <?php if(isset($categid) && $categid == $objcolCategorie_xx->agenda_categid) echo 'selected="selected"';?> value="<?php echo $objcolCategorie_xx->agenda_categid; ?>"><?php echo $objcolCategorie_xx->category; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
        </div>
        <div id="ville">

            <label>Ville : </label>
            <select class="form-control" required name="IdVille"  id="IdVille">
                <option class="form-control" value="0">-- Veuillez choisir --</option>
                <?php if(sizeof($colVille)) { ?>
                    <?php foreach($colVille as $objville) { ?>
                        <option <?php if(isset($objville) && $objville == $objville->IdVille) echo 'selected="selected"';?> value="<?php echo $objville->IdVille; ?>"><?php echo $objville->ville_nom; ?></option>
                    <?php } ?>
                <?php } ?>
            </select>
        </div>
        <div>

                    <label>Type </label>
                    <select class="form-control" required name="typeof" id="typeof">
                        <option value="">-- Veuillez choisir --</option>
                        <option value="0">Agenda</option>
                        <option value="1">Article</option>
                    </select>
        </div>
        <a href="#" class="btn btn-success" onclick="show()">Valider</a>
        <a href="<?php echo site_url('admin/xml')?>" class="btn btn-danger">Réinitialiser</a>
    </form>
<!-- ------------------------------------------------------ -->

    <div id="data_import" class="btn btn-primary" style="margin-top: 20px;width: 100%;">
        Recuperer tout les articles RSS automatiquement
    </div>
    <div id="data_link" class="btn btn-success" style=" margin-top: 20px;">
        <a href=<?php echo site_url('sortez_pro/Auto_import_Rss') ?> target="_blank" style="color: white;">
            Recuperer tout les articles RSS automatiquement via lien
        </a>
    </div>

<!-- ------------------------------------------------------ -->
    <script type="text/javascript">

        function show(){
            if (document.getElementById("IdCategory").value !=0  && document.getElementById("links").value !="" && document.getElementById("IdVille").value !="0" && document.getElementById("typeof").value !="") {
                document.getElementById("form").submit();
            }else {
                alert("veuillez renseigner la catégorie , le type, le lien ou la ville");
            }

        }
    </script>
</div>
    </div>
    <?php
        $CI =& get_instance();
        $CI->load->model("xml_models");
        //var_dump($lien);die;        
        $minMax_flux = $CI->xml_models->getMinMax_flux();
        $totaDatatourisme = count($CI->xml_models->countAllDatatourisme());
        $nbrerss= $CI->xml_models->getmin();



    ?>
    <!-- script pour mettre les id dans une variable plus fonction click -->

    <script type="text/javascript">
        var list_index = [
                    <?php 
                        foreach( $allink as $link){ 
                            echo "
                                '".$link->id."',
                            ";
                        }
                    ?>
                ]

        var index = 0
        var click = 0

        function import_auto(id_btn){
            if(click == 1){
                $('#'+list_index[id_btn]).click() 
            }      
        }

        $('#data_import').click(function(){
            click = 1
            if (confirm("Voulez vous importer l'ensemble des données RSS ?")) {
                $('#data_import').html('<img src="<?php echo base_url('assets/img/rss_load.gif')?>">');
                import_auto(index)
            }  
        })
    </script>

    <!-- --------------------------------------------------------------- -->

<div style="margin-top: 50px;height:auto;border: groove">
<div>
    
    <table class="table table-bordered table-responsive">
        <thead>
        <tr>
            <th>Id</th>
            <th>Lien</th>
            <th>Catégorie</th>
            <th>Action</th>
            <th>Type</th>
            <th></th>
            <th>N°</th>

        </tr>
        </thead>
        <tbody>
            <?php $iiii = 1;?>
            <?php foreach( $allink as $link){ ?>
                <tr>
                    <td><?php echo $link->id; ?></td>
                    <td><?php echo $link->source; ?></td>
                    <td><?php $this->load->model("xml_models");
                    if (isset($link->agenda_categid)){
                        $category=$this->xml_models->getcategbycategid($link->agenda_categid);
                        echo $category->category; }?>
                    </td>
                    <td>
                            <input type="hidden" name="typeof" value="<?php echo $link->type;?>">
                            <input type="hidden" name="linkid" value="<?php echo $link->id;?>">
                            <input type="hidden" name="link" value="<?php echo $link->source;?>">
                            <input type="hidden" name="IdVille" value="<?php echo $link->IdVille;?>">
                            <button id="<?php echo $link->id;?>" onclick="get_all(0,<?php echo $link->id;?>,0,<?php echo $link->IdVille;?>,'<?php echo $link->source;?>')"  class="btn btn-info">Recuperer les articles de ce Lien</button>
                             <a href="<?php echo site_url('admin/xml/delete_link/' .$link->id); ?>" class="btn btn-danger" onclick="return confirm('Supprimer ce lien?');">Supprimer</a>
                    </td>
                    <td> <?php if ($link->type =="1") {
                                echo "Articles";
                         }else if($link->type =="0"){
                                echo "Agenda";
                         }else{
                            echo "Non specifié";
                         }
                    ?> </td>
                    <td>
                        <?php if(isset($link->getted) && $link->getted==1 && isset($link->current_date_getted) && $link->current_date_getted == date("Y-m-d")) {?>
                            <img id="icone<?php echo $link->id;?>" style="border: none;width:15px;" src="<?php echo base_url();?>application/resources/privicarte/images/activated_ico.gif">
                        <?php } else { ?>
                            <img id="icone<?php echo $link->id;?>" style="border: none;width:15px;" src="<?php echo base_url();?>application/resources/privicarte/images/deactivated_ico.png">
                        <?php } echo $link->current_date_getted." / ".date("Y-m-d"); ?>
                    </td>
                    <td>
                        <?php
                        echo $iiii; $iiii = $iiii + 1;
                        ?>
                    </td>
                </tr>
                <script type="text/javascript">
    
                    //function pour boucler le clic des boutons

                    // récuperer les élements dans le lien
                    function get_all(start,ids,cache=0,idville,source){
                        $('#'+ids).html('');
                        $('#'+ids).html('<img src="<?php echo base_url('assets/img/rss_load.gif')?>">');
                        var textBtn = $('#'+ids).val()                        //var link=ids;
                       // alert(link);

                        jQuery.ajax({
                            type: "POST",
                            url: "<?php echo site_url("admin/xml/count_data/"); ?>"+ids,
                            dataType: "json",
                            //data: 'link=' + link,
                            success: function (data) {
                                if (data != 'ko'){
                                    var end=parseInt(data)-1;
                                    var link=source;
                                    var IdVille=idville;
                                    if (end > start){
                                        jQuery.ajax({
                                            type: "POST",
                                            url: "<?php echo site_url("admin/xml/savetorsslist"); ?>",
                                            dataType: "json",
                                            data:"id="+start +"&link="+link+"&IdVille="+IdVille+"&linkid="+ids+"&cache="+cache,
                                            success: function (data) {
                                                start +=1;
                                                cache=1;
                                                //setTimeout(function(){
                                                    get_all(start,ids,cache,idville,source);
                                                console.log(start);

                                            },
                                            error: function (data) {
                                                console.log('error');
                                                start +=1;
                                                cache=1;
                                                //alert(start);
                                                //setTimeout(function(){
                                                    get_all(start,ids,cache,idville,source);
                                                  //  }, 3000);
                                            }
                                        });
                                    }else{
                                        
                                        var toFirstIdDatatourisme = parseInt(<?php echo $minMax_flux->min_id;?>);
                                        var toLastIdDatatourisme = parseInt(<?php echo $minMax_flux->max_id;?>);
                                        var lien = ids;
                                        import_datatourisme_func(toFirstIdDatatourisme, toLastIdDatatourisme,lien)
                                        $('#'+ids).html('Lien importé')
                                        $('#icone'+ids).attr('src','<?php echo base_url();?>application/resources/privicarte/images/activated_ico.gif')
                                        if(click == 1){
                                            index++
                                            if(index < list_index.length){

                                                $('#'+list_index[index]).html("Préparation de l'importation")
                                                setTimeout(function(){
                                                    import_auto(index)
                                                }, 15000)
                                            }else{
                                                $('#data_import').html('Importation de données terminé');
                                                sendMail()
                                                alert('Importation de tous les données terminée')
                                                click = 0
                                                location.reload()
                                            }
                                        }else{
                                            alert('Importation terminée et le lien est'+lien)
                                        }

                                    }
                                }
                            },
                            error: function (data) {
                                alert("L'importation a rencontré une erreur, le prochain lien va être importer automatiquement à la place")
                                $('#'+list_index[index]).html("Erreur de récupération")
                                index++
                                if(index < list_index.length){
                                    $('#'+list_index[index]).html("Préparation de l'importation")
                                    setTimeout(function(){
                                        import_auto(index)
                                    }, 30000)
                                }else{
                                    sendMail()
                                    alert('Importation de donnée terminée')
                                    location.reload()
                                }
                            }
                        });
                    }

                    function sendMail(){
                        jQuery.ajax({
                            type: "GET",
                            url: "<?php echo site_url("admin/xml/mail_report_rss_update"); ?>",
                            dataType: "json",
                            success: function (data) {
                                console.log(data)
                            },
                            error: function (data) {
                                console.log('error')
                            }
                        })
                    }

                    function data_tourism_copy(link){
                        var toFirstIdDatatourisme = parseInt(<?php echo $minMax_flux->min_id;?>);
                        var toLastIdDatatourisme = parseInt(<?php echo $minMax_flux->max_id;?>);
                        // var lien = '<?php echo $link->source; ?>'
                        var lien = link
                        import_datatourisme_func(toFirstIdDatatourisme, toLastIdDatatourisme,lien)
                    }

                    function des(is) {
                        var repl = document.getElementById(is);
                        repl.innerHTML = "<progress></progress>";
                    }
                    var first="<?php if (isset($firstid)) echo $firstid;?>";
                    var last="<?php if (isset($lastid)) echo $lastid;?>";
                    function erase(){
                        document.getElementById('progress_gif').style.display="block";
                        delete_all_e(first,last);
                    }
                    function delete_all_e(first,last) {
                        jQuery.ajax({
                            type: "POST",
                            url: "<?php echo site_url("admin/xml/reset_data"); ?>",
                            dataType: "json",
                            data: 'firstId=' + first,
                            success: function (data) {
                                console.log(data);
                                    document.getElementById('progress_gif').style.display="none";
                                    alert('toutes les données RSS sont supprimés');
                            },
                            error: function (data) {
                                console.log(data);
                                console.log(data);
                            }
                        });
                    }

                    function import_datatourisme_func(firstId, lastId,lien) {
                        jQuery.ajax({
                            type: "POST",
                            url: "<?php echo site_url("admin/xml/import_datatourisme_func"); ?>",
                            data: 'toFirstIdDatatourisme=' + firstId + '&toLastIdDatatourisme=' + lastId  + '&lienrss=' + lien,
                            dataType: "json",
                            success: function (data) {
                                //alert(data.responseText);
                                // jQuery("#item_sent_error_nb").html(data.responseText);
                                // if(Number.isInteger(data.datatourisme_imported)) datatourisme_imported = datatourisme_imported + parseInt(data.datatourisme_imported);
                                // if(Number.isInteger(data.datatourisme_exists)) datatourisme_exists = datatourisme_exists + parseInt(data.datatourisme_exists);
                                // if(Number.isInteger(data.datatourisme_skipped)) datatourisme_skipped = datatourisme_skipped + parseInt(data.datatourisme_skipped);
                                // jQuery("#item_sent_success_nb").html(datatourisme_imported);
                                // jQuery("#item_sent_exist_nb").html(datatourisme_exists);
                                // jQuery("#item_sent_error_nb").html(datatourisme_skipped);
                                // if (data.datatourisme_imported=='1') $('#send_multiple_consol').append('OK => ' + data.datatourisme_title + '<br/>');
                                // else $('#send_multiple_consol').append('KO => ' + firstId + '<br/>');
                                // scroll_bottom_consol();
                                // check_percent(firstId, lastId);
                                firstId += 1;
                                // console.log(firstId);
                                // console.log(data);


                                if (firstId <= lastId) {
                                    import_datatourisme_func(firstId, lastId,lien);
                                }
                            },
                            error: function (data) {
                                //alert(data.responseText);
                                // jQuery("#item_sent_error_nb").html(data.responseText);
                                // if(Number.isInteger(data.datatourisme_imported)) datatourisme_imported = datatourisme_imported + parseInt(data.datatourisme_imported);
                                // if(Number.isInteger(data.datatourisme_exists)) datatourisme_exists = datatourisme_exists + parseInt(data.datatourisme_exists);
                                // if(Number.isInteger(data.datatourisme_skipped)) datatourisme_skipped = datatourisme_skipped + parseInt(data.datatourisme_skipped);
                                // jQuery("#item_sent_success_nb").html(datatourisme_imported);
                                // jQuery("#item_sent_exist_nb").html(datatourisme_exists);
                                // //jQuery("#item_sent_error_nb").html(datatourisme_skipped);
                                // $('#send_multiple_consol').append('KO => ' + firstId + '<br/>');
                                // scroll_bottom_consol();
                                // check_percent(firstId, lastId);
                                firstId += 1;
                                // console.log(firstId);
                                // console.log(data);
                                if (firstId <= lastId) {
                                    import_datatourisme_func(firstId, lastId,lien);
                                }
                            }
                        });
                    }
                </script>
            <?php } ?>
        </tbody>
    </table>
</div>
</div>

<?php $this->load->view("admin/includes/vwFooter2013"); ?>