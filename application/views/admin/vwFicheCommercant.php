<?php $data["zTitle"] = 'Inscription des professionnels'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>



    <script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
			jQuery('#loading_datefin_plus_un_an').hide();
			
            jQuery(".btnSinscrire").click(function(){
                var txtError = "";
				
				var RubriqueSociete = jQuery("#RubriqueSociete").val();
                if(RubriqueSociete=="0" || RubriqueSociete=="") {
                    jQuery("#divErrorRubriqueSociete").html("Un commercant doit être assigné à une rubrique.");
                    jQuery("#divErrorRubriqueSociete").show();
                    txtError += "- Un commercant doit être assigné à une rubrique.<br/>";
                } else {
                    jQuery("#divErrorRubriqueSociete").hide();
                }
				var SousRubriqueSociete = jQuery("#SousRubriqueSociete").val();
                if(SousRubriqueSociete=="0" || SousRubriqueSociete=="") {
                    jQuery("#divErrorSousRubriqueSociete").html("Un commercant doit être assigné à une sous-rubrique.");
                    jQuery("#divErrorSousRubriqueSociete").show();
                    txtError += "- Un commercant doit être assigné à une sous-rubrique.<br/>";
                } else {
                    jQuery("#divErrorSousRubriqueSociete").hide();
                }
				
                var EmailSociete = jQuery("#EmailSociete").val();
                if(!isEmail(EmailSociete)) {
                    jQuery("#divErrorEmailSociete").html("Ce Courriel n'est pas valide. Veuillez saisir un email valide");
                    jQuery("#divErrorEmailSociete").show();
                    txtError += "- Ce Courriel n'est pas valide. Veuillez saisir un email valide.<br/>";
                } else {
                    jQuery("#divErrorEmailSociete").hide();
                }
                
                var LoginSociete = jQuery("#LoginSociete").val();
                if(!isEmail(LoginSociete)) {
                    txtError += "- Votre login doit &ecirc;tre un email valide.<br/>";
                    jQuery("#divErrorLoginSociete").html("Votre login doit &ecirc;tre un email valide.");
                    jQuery("#divErrorLoginSociete").show();
                } else {
                    jQuery("#divErrorLoginSociete").hide();
                }
				
				
				jQuery("#total_error_admin_fiche_comm").html(txtError);
				
                if(txtError == "") {
                    jQuery("#frmInscriptionProfessionnel").submit();
                }
            });
			
			
			jQuery("#IsActifSociete").click(function(){
							var DateDebut = jQuery("#DateDebut").val();	
							var DateFin = jQuery("#DateFin").val();	
							
							if ((DateDebut=="" || DateDebut==null) && (DateFin=="" || DateFin==null)) {
								//alert("valeur null");
								DateDebut = "<?php echo convert_Sqldate_to_Frenchdate(date("Y-m-d")); ?>";
								
                                <?php 
								$current_date = date("Y-m-d");
								$current_value = explode('-',$current_date);
								?>
                                
								//alert("sqdfqsf "+<?php echo $current_value[0]+1;?>);
								<?php 
								$year_datefin = $current_value[0]+1; 
								$date_fin_value = $year_datefin."-".$current_value[1]."-".$current_value[2];
								?>
								DateFin = "<?php echo convert_Sqldate_to_Frenchdate($date_fin_value);?>";
								//alert(DateDebut);
								//alert(DateFin);
								jQuery("#DateDebut").val(DateDebut);
								jQuery("#DateFin").val(DateFin);
							}
												   
				});
							
			jQuery("#btn_datefin_plus_un_an").click(function(){
							var DateDebut = jQuery("#DateDebut").val();	
							
							jQuery('#loading_datefin_plus_un_an').show();
							
							jQuery.post('<?php echo site_url("admin/commercants/add_datefin_plus_un_an" ) ; ?>',
								{ DateDebut: DateDebut},
								function success(data){
									jQuery("#DateFin").val(data);
									jQuery('#loading_datefin_plus_un_an').hide();
							 });
												   
				});				
							
        })
    </script>
	<script type="text/javascript">
		$(function() {
			$("#DateDebut").datepicker({
				dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
				dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
				monthNames: ['Janvier','Fevier','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
				dateFormat: 'DD, d MM yy',
				autoSize: true
			});
			$("#DateFin").datepicker({
				dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
				dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
				monthNames: ['Janvier','Fevier','Mars','Avril','Mai','Juin','Juiller','Aout','Septembre','Octobre','Novembre','Decembre'],
				dateFormat: 'DD, d MM yy',
				autoSize: true
			});
		}); 
		function effacerPhotoAccueil(_iCommercantId){
			jQuery.post('<?php echo site_url("admin/commercants/effacerPhotoCommercant" ) ; ?>',
				{ iCommercantId: _iCommercantId},
				function success(data){ 
					jQuery("#divPhotoAccueil").hide() ;
			 });
		}
		function effacerPhoto1(_iCommercantId){
			jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto1Commercant" ) ; ?>',
				{ iCommercantId: _iCommercantId},
				function success(data){
					jQuery("#divPhoto1").hide() ;
			 });
		}
		function effacerPhoto2(_iCommercantId){
			jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto2Commercant" ) ; ?>',
				{ iCommercantId: _iCommercantId},
				function success(data){
					jQuery("#divPhoto2").hide() ;
			 });
		}
		function effacerPhoto3(_iCommercantId){
			jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto3Commercant" ) ; ?>',
				{ iCommercantId: _iCommercantId},
				function success(data){
					jQuery("#divPhoto3").hide() ;
			 });
		}
		function effacerPhoto4(_iCommercantId){
			jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto4Commercant" ) ; ?>',
				{ iCommercantId: _iCommercantId},
				function success(data){
					jQuery("#divPhoto4").hide() ;
			 });
		}
		function effacerPhoto5(_iCommercantId){
			jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto5Commercant" ) ; ?>',
				{ iCommercantId: _iCommercantId},
				function success(data){
					jQuery("#divPhoto5").hide() ;
			 });
		}
		function effacerPdf(_iCommercantId){
			jQuery.post('<?php echo site_url("admin/commercants/effacerPdfCommercant" ) ; ?>',
				{ iCommercantId: _iCommercantId},
				function success(data){
					jQuery("#divPdf").hide() ;
			 });
		}
		
		function deleteFile(_IdCommercant,_FileName) {
			jQuery.ajax({
				url: gCONFIG["SITE_URL"] + 'front/professionnels/delete_files/' + _IdCommercant + '/' + _FileName,
				dataType: 'html',
				type: 'POST',
				async: true,
				success: function(data){
					window.location.reload();
				}
			});
		}
		
		
		function listeSousRubrique(){
			 
			var irubId = jQuery('#RubriqueSociete').val();     
			jQuery.get(
				'<?php echo site_url("front/professionnels/testAjax"); ?>' + '/' + irubId,
				function (zReponse)
				{
					// alert (zReponse) ;
					jQuery('#trReponseRub').html("") ; 
					jQuery('#trReponseRub').html(zReponse) ;
				   
				 
			   });
		}
	</script>
    <div id="divInscriptionProfessionnel" class="content" align="center">
	      <br>
	     <a style = "color:black;text-decoration:none;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" value= "retour au menu" id = "btn" onclick="document.location='<?php echo site_url("admin/home/" );?>';"/> </a><br/>
         <a style = "color:black;text-decoration:none;" href = "<?php echo site_url("admin/commercants/liste/" ) ; ?>"> <input type = "button" value= "retour à la liste commercants" id = "btn" onclick="document.location='<?php echo site_url("admin/commercants/liste" );?>';"/> </a><br/>
         
		 <?php if (isset($_SESSION['error_fiche_com_admin'])) { ?>
         <div style="text-align:center; color:#FF0000;"><p><?php echo $_SESSION['error_fiche_com_admin']; ?></p></div>
         <?php unset($_SESSION['error_fiche_com_admin']);} ?>
                 
         <form name="frmInscriptionProfessionnel" id="frmInscriptionProfessionnel" action="<?php echo site_url("admin/commercants/modifier"); ?>" method="POST" enctype="multipart/form-data">
            <h1>Fiche Professionnel</h1>
            <fieldset>
                <legend><div style="width:300px;font-size:20px;background-color:#123;color:#fff">Soci&eacute;t&eacute;</div></legend>
                <table width="100%" cellpadding="0" cellspacing="0" style="text-align:left;">
                    
                    
                    <tr>
                            <td width="300">
                                <label class="label">Logo : </label>
                                <br/>(la largeur ne doit pas d&eacute;passer 270px)
                          </td>
                            <td>
                                <?php if(!empty($objCommercant->Logo)) { ?>
                                    <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->Logo, 100, 100,'',''); ?>
                                    <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Logo');">Supprimer</a>
                                <?php } ?><br/>
                                <input type="file" name="Societelogo" id="logoSociete" value="" />
                            </td>
                        </tr>
                    <tr>
                        <td width="300">
                            <label class="label">Lien logo : </label>
                      </td>
                        <td>
                            <input type="text" name="Societe[logo_url]" id="logo_url" value="<?php if(isset($objCommercant->logo_url)) echo htmlspecialchars($objCommercant->logo_url); else echo 'http//';?>" />
                        </td>
                    </tr>
                    <tr>
                        <td width="300">
                            <label class="label">D&eacute;signation d'activit&eacute; : </label>
                      </td>
                        <td>
                            <input type="text" name="Societe[titre_entete]" id="titre_entete" value="<?php if(isset($objCommercant->titre_entete)) echo htmlspecialchars($objCommercant->titre_entete); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Rubrique : </label>
                        </td>
                        <td>
                            <select name="AssCommercantRubrique[IdRubrique]" id="RubriqueSociete" onchange="javascript:listeSousRubrique();">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($colRubriques)) { ?>
                                    <?php foreach($colRubriques as $objRubrique) { ?>
                                        <option <?php if(isset($objAssCommercantRubrique[0]->IdRubrique) &&$objAssCommercantRubrique[0]->IdRubrique == $objRubrique->IdRubrique) { ?>selected="selected"<?php } ?> value="<?php echo $objRubrique->IdRubrique; ?>"><?php echo $objRubrique->Nom; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <div class="FieldError" id="divErrorRubriqueSociete"></div>
                        </td>
                    </tr>
                    <tr id='trReponseRub'>
                        <td>
                            <label>Sous-rubrique : </label>
                        </td>
                        <td>
                            <select name="AssCommercantSousRubrique[IdSousRubrique]" id="SousRubriqueSociete">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($colSousRubriques)) { ?>
                                    <?php foreach($colSousRubriques as $objSousRubrique) { ?>
                                        <option <?php if(isset($objAssCommercantSousRubrique[0]->IdSousRubrique) && $objAssCommercantSousRubrique[0]->IdSousRubrique == $objSousRubrique->IdSousRubrique) { ?>selected="selected"<?php } ?> value="<?php echo $objSousRubrique->IdSousRubrique; ?>"><?php echo $objSousRubrique->Nom; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <div class="FieldError" id="divErrorSousRubriqueSociete"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Nom de la soci&eacute;t&eacute; : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[NomSociete]" id="NomSociete" value="<?php if(isset($objCommercant->NomSociete)) echo htmlspecialchars($objCommercant->NomSociete); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Siret : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[Siret]" id="SiretSociete" value="<?php if(isset($objCommercant->Siret)) echo htmlspecialchars($objCommercant->Siret); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Adresse de l'&eacute;tablissement : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[Adresse1]" id="Adresse1Societe" value="<?php if(isset($objCommercant->Adresse1)) echo htmlspecialchars($objCommercant->Adresse1); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Compl&eacute;ment d'Adresse : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[Adresse2]" id="Adresse2Societe" value="<?php if(isset($objCommercant->Adresse2)) echo htmlspecialchars($objCommercant->Adresse2); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Ville : </label>
                        </td>
                        <td>
                            <select name="Societe[IdVille]" id="VilleSociete">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($colVilles)) { ?>
                                    <?php foreach($colVilles as $objVille) { ?>
                                        <option <?php if(isset($objCommercant->IdVille) && $objCommercant->IdVille == $objVille->IdVille) { ?>selected="selected"<?php } ?> value="<?php echo $objVille->IdVille; ?>"><?php echo htmlentities($objVille->Nom, ENT_QUOTES, 'UTF-8'); ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Code postal : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[CodePostal]" id="CodePostalSociete" value="<?php if(isset($objCommercant->CodePostal)) echo htmlspecialchars($objCommercant->CodePostal); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>T&eacute;l&eacute;phone fixe : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[TelFixe]" id="TelFixeSociete" value="<?php if(isset($objCommercant->TelFixe)) echo htmlspecialchars($objCommercant->TelFixe); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>T&eacute;l&eacute;phone mobile : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[TelMobile]" id="TelMobileSociete" value="<?php if(isset($objCommercant->TelMobile)) echo htmlspecialchars($objCommercant->TelMobile); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Site Web : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[SiteWeb]" id="SiteWebSociete" value="<?php if(isset($objCommercant->SiteWeb)) echo htmlspecialchars($objCommercant->SiteWeb); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Courriel : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[Email]" id="EmailSociete" value="<?php if(isset($objCommercant->Email)) echo htmlspecialchars($objCommercant->Email); ?>" />
                            <div class="FieldError" id="divErrorEmailSociete"></div>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <label class="label">Label Activité 1 :</label></td>
                        <td>
                            <input type="text" name="Societe[labelactivite1]" id="labelactivite1Societe" value="<?php if(isset($objCommercant->labelactivite1)) echo htmlspecialchars($objCommercant->labelactivite1); ?>" />
                            <span style="font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#F00;">&nbsp;&nbsp;(si ce champ est vide l'onglet  activit&eacute; du commercant est masqu&eacute;)</span>
                            <div class="FieldError" id="divErrorEmailSociete"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Description Activit&eacute; 1 : </label>
                        </td>
                        <td>
                            <textarea name="Societe[activite1]" id="activite1Societe" style="width:400px; height:100px;"><?php if(isset($objCommercant->activite1)) echo htmlspecialchars($objCommercant->activite1); ?></textarea>
                            <?php echo display_ckeditor($ckeditor1); ?>
                            <div class="FieldError" id="divErroractivite1"></div>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>Label Activité 2 :</td>
                        <td>
                            <input type="text" name="Societe[labelactivite2]" id="labelactivite2Societe" value="<?php if(isset($objCommercant->labelactivite2)) echo htmlspecialchars($objCommercant->labelactivite2); ?>" /><span style="font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#F00;">&nbsp;&nbsp;(si ce champ est vide l'onglet  activit&eacute; du commercant est masqu&eacute;)</span>
                            <div class="FieldError" id="divErrorEmailSociete"></div>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <label>Description Activit&eacute; 2 : </label>
                        </td>
                        <td>
                            <textarea name="Societe[activite2]" id="activite2Societe" style="width:400px; height:100px;"><?php if(isset($objCommercant->activite2)) echo htmlspecialchars($objCommercant->activite2); ?></textarea>
                            <?php echo display_ckeditor($ckeditor2); ?>
                            <div class="FieldError" id="divErroractivite2"></div>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <fieldset>
                <legend><div style="width:300px;font-size:20px;background-color:#123;color:#fff">Param&egrave;tres Admin</div></legend>
                <table width="100%" cellpadding="0" cellspacing="0" style="text-align:left;">
                    <tr>
                        <td>
                            <label>Login : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[Login]" id="LoginSociete" value="<?php if(isset($objCommercant->Login)) echo htmlspecialchars($objCommercant->Login); ?>" style="width:300px;"/>
                            <div class="FieldError" id="divErrorLoginSociete"></div>
                        </td>
                    </tr>
                    <!--<tr>
                        <td>
                            <label>Password: </label>
                        </td>
                        <td>
                            <input type="password" name="Societe[Password]" id="PasswordSociete" value="<?php// if(isset($objCommercant->Password)) echo htmlspecialchars($objCommercant->Password); ?>" disabled/>
                        </td>
                    </tr>-->
                    <tr>
                        <td>
                            <label>Activ&eacute;: </label>
                        </td>
                        <td>
                            <input type="checkbox" name="Societe[IsActif]" id="IsActifSociete" value="1" <?php if(isset($objCommercant->IsActif) && $objCommercant->IsActif == "1") echo 'checked="checked"'; ?> />
                            <input type="hidden" name="Societe[IdCommercant]" id="IdCommercantSociete" value="<?php if(isset($objCommercant->IdCommercant)) echo htmlspecialchars($objCommercant->IdCommercant); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Vignette : </label>
                        </td>
                        <td>
                            <?php if($objCommercant->Photo5 != ""){ ?>
                                    <div id="divPhoto5">
                                    <span><?php echo $objCommercant->Photo5 ; ?></span>&nbsp;
                                    <input type="button" value="Supprimer"  onclick="if(!confirm('Voulez-vous vraiment supprimer cette photo?')){ return false;} else { effacerPhoto5('<?php echo $objCommercant->IdCommercant ; ?>') ;}"/>
                                    <br/>
                                    </div>
                                <input type="file" style="display:none" name="SocietePhoto5" id="Photo5Societe" value="" />
                            <?php } else { ?>
                                <input type="file" name="SocietePhoto5" id="Photo5Societe" value="" />
                            <?php } ?>
                        </td>
                    </tr>
                    <!-- anomalie 42 : Tafita -->
                    <tr>
                        <td valign="top">
                                <label>Photo accueil : </label>
                        </td>
                        <td>
                            <?php if($objCommercant->PhotoAccueil != ""){ ?>
                                    <div id="divPhotoAccueil">
                                    <span><?php echo $objCommercant->PhotoAccueil ; ?></span>&nbsp;
                                    <input type="button" value="Supprimer" onclick="if(!confirm('Voulez-vous vraiment supprimer cette photo?')){ return false;} else { effacerPhotoAccueil('<?php echo $objCommercant->IdCommercant ; ?>') ;}"/>
                                    <br/>
                                    </div>
                                <input type="file" style="display:none" name="SocietePhotoAccueil" id="PhotoAccueilSociete" value="" />
                            <?php } else { ?>
                            <input type="file" name="SocietePhotoAccueil" id="PhotoAccueilSociete" value="" />
                            <?php } ?>
                        </td>
                    </tr>
                    <!-- anomalie 42: Tafita -->
                    
                    
                    <tr>
                        <td>
                            <label>Module de réferencement<br/>Abonnement PLATINIUM : </label>
                        </td>
                        <td>
<script type="text/javascript">
		 $(function(){
			 $("#check_referencement").click(function(){
				 if ($('#check_referencement').attr('checked')) {
					$("#check_referencement_value").val("1");
				} else {
					$("#check_referencement_value").val("0");
				}
			 });
			   
		 })
</script>
                        	<input id="check_referencement" type="checkbox" name="check_referencement" <?php if(isset($objCommercant->referencement_annuaire) && $objCommercant->referencement_annuaire == "1") echo 'checked="checked"'; ?>/>
                            <input type="hidden" name="Societe[referencement_annuaire]" id="check_referencement_value" value="<?php if(isset($objCommercant->referencement_annuaire) && $objCommercant->referencement_annuaire == "1") echo '1'; else echo '0'; ?>"/>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <br/><label>Module AGENDA : </label><br/>
                        </td>
                        <td>
<script type="text/javascript">
		 $(function(){
			 $("#check_agenda").click(function(){
				 if ($('#check_agenda').attr('checked')) {
					$("#check_agenda_value").val("1");
				} else {
					$("#check_agenda_value").val("0");
				}
			 });
			   
		 })
</script>
                        	<input id="check_agenda" type="checkbox" name="check_agenda" <?php if(isset($objCommercant->agenda) && $objCommercant->agenda == "1") echo 'checked="checked"'; ?>/>
                            <input type="hidden" name="Societe[agenda]" id="check_agenda_value" value="<?php if(isset($objCommercant->agenda) && $objCommercant->agenda == "1") echo '1'; else echo '0'; ?>"/>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <br/><label>Module BLOG : </label><br/>
                        </td>
                        <td>
<script type="text/javascript">
		 $(function(){
			 $("#check_blog").click(function(){
				 if ($('#check_blog').attr('checked')) {
					$("#check_blog_value").val("1");
				} else {
					$("#check_blog_value").val("0");
				}
			 });
			   
		 })
</script>
                        	<input id="check_blog" type="checkbox" name="check_blog" <?php if(isset($objCommercant->blog) && $objCommercant->blog == "1") echo 'checked="checked"'; ?>/>
                            <input type="hidden" name="Societe[blog]" id="check_blog_value" value="<?php if(isset($objCommercant->blog) && $objCommercant->blog == "1") echo '1'; else echo '0'; ?>"/>
                        </td>
                    </tr>
                    
                    
                </table>
            </fieldset>
            <fieldset>
                <legend><div style="width:300px;font-size:20px;background-color:#123;color:#fff">Responsable</div></legend>
                <table width="100%" cellpadding="0" cellspacing="0" style="text-align:left;">
                    <tr>
                        <td width="300">
                            <label>Civilit&eacute; : </label>
                        </td>
                        <td>
                            <select name="Societe[Civilite]" id="CiviliteResponsableSociete">
                                <option <?php if(isset($objCommercant->Civilite) && $objCommercant->Civilite == "0") { ?>selected="selected"<?php } ?> value="0">Monsieur</option>
                                <option <?php if(isset($objCommercant->Civilite) && $objCommercant->Civilite == "1") { ?>selected="selected"<?php } ?> value="1">Madame</option>
                                <option <?php if(isset($objCommercant->Civilite) && $objCommercant->Civilite == "2") { ?>selected="selected"<?php } ?> value="2">Mademoiselle</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Nom : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[Nom]" id="NomResponsableSociete" value="<?php if(isset($objCommercant->Nom)) echo htmlspecialchars($objCommercant->Nom); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Pr&eacute;nom : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[Prenom]" id="PrenomResponsableSociete" value="<?php if(isset($objCommercant->Prenom)) echo htmlspecialchars($objCommercant->Prenom); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Fonction <span class="indication">(G&eacute;rant, Directeur ...)</span>: </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[Responsabilite]" id="ResponsabiliteResponsableSociete" value="<?php if(isset($objCommercant->Responsabilite)) echo htmlspecialchars($objCommercant->Responsabilite); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Email
                        </td>
                        <td>
                            <input type="text" name="Societe[Email_decideur]" id="Email_decideur" value="<?php if(isset($objCommercant->Email_decideur)) echo htmlspecialchars($objCommercant->Email_decideur); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>T&eacute;l&eacute;phone direct <span class="indication">(fixe ou portable)</span> : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[TelDirect]" id="TelDirectResponsableSociete" value="<?php if(isset($objCommercant->TelDirect)) echo htmlspecialchars($objCommercant->TelDirect); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Type d'abonnement : </label>
                        </td>
                        <td height="30">
						<?php 
						//echo $user_groups->id;
						/*if ($user_groups!="0") {
							if ($user_groups->id==3) echo "Basic";
							if ($user_groups->id==4) echo "Premium";
							if ($user_groups->id==5) echo "Platinum";
						}*/
						?>
                        
                        
                            <select name="Abonnement_ionauth_user" id="AbonnementSociete" style=" font-weight:bold;">
                                <option value="3" <?php if ($user_groups->id==3) echo "selected";?>>Basic</option>
                                <option value="4" <?php if ($user_groups->id==4) echo "selected";?>>Premium</option>
                                <option value="5" <?php if ($user_groups->id==5) echo "selected";?>>Platinium</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Date d&eacute;but
                        </td>
                        <td>
                            <input type="text" name="AssAbonnementCommercant[DateDebut]" id="DateDebut" value="<?php if(isset($objAssCommercantAbonnement[0]->DateDebut)) echo convert_Sqldate_to_Frenchdate($objAssCommercantAbonnement[0]->DateDebut); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Date fin
                        </td>
                        <td>
                            <input type="text" name="AssAbonnementCommercant[DateFin]" id="DateFin" value="<?php if(isset($objAssCommercantAbonnement[0]->DateFin)) echo convert_Sqldate_to_Frenchdate($objAssCommercantAbonnement[0]->DateFin); ?>" />
                            <input type="button" id="btn_datefin_plus_un_an" name="btn_datefin_plus_un_an" value="Un an de plus"/>
                            <img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...." id="loading_datefin_plus_un_an"/>
                        </td>
                    </tr>
                    
                    
                    <tr>
                        <td></td>
                        <td align="left">&nbsp;<!--<input type="button" class="btnSinscrire" value="Modifier" />--></td>
                    </tr>
                </table>
            </fieldset>
                <fieldset>
                    <legend><div style="width:300px;font-size:20px;background-color:#123;color:#fff">Offre payante</div></legend>
                    <table width="100%" cellpadding="0" cellspacing="0" style="text-align:left;">
                        <tr>
                            <td>
                                <label>Photo 1 : </label>
                            </td>
                            <td>
                                <?php if($objCommercant->Photo1 != ""){ ?>
                                        <div id="divPhoto1">
                                        <span>
										<?php //echo $objCommercant->Photo1 ; ?>
                                        <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->Photo1, 100, 100,'',''); ?>
                                        </span>&nbsp;
                                        <input type="button" value="Supprimer"  onclick="if(!confirm('Voulez-vous vraiment supprimer cette photo?')){ return false;} else { effacerPhoto1('<?php echo $objCommercant->IdCommercant ; ?>') ;}"/>
                                        <br/>
                                        </div>
                                <input type="file" style="display:none" name="SocietePhoto1" id="Photo1Societe" value="" />
                                <?php } else { ?>
                                    <input type="file" name="SocietePhoto1" id="Photo1Societe" value="" />
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Photo 2 : </label>
                            </td>
                            <td>
                            <?php if($objCommercant->Photo2 != ""){ ?>
                                    <div id="divPhoto2">
                                    <span>
									<?php //echo $objCommercant->Photo2 ; ?>
                                    <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->Photo2, 100, 100,'',''); ?>
                                    </span>&nbsp;
                                    <input type="button" value="Supprimer"  onclick="if(!confirm('Voulez-vous vraiment supprimer cette photo?')){ return false;} else { effacerPhoto2('<?php echo $objCommercant->IdCommercant ; ?>') ;}"/>
                                    <br/>
                                    </div>
                                <input type="file" style="display:none" name="SocietePhoto2" id="Photo2Societe" value="" />
                                <?php } else { ?>
                                    <input type="file" name="SocietePhoto2" id="Photo2Societe" value="" />
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Photo 3 : </label>
                            </td>
                            <td>
                            <?php if($objCommercant->Photo3 != ""){ ?>
                                    <div id="divPhoto3">
                                    <span>
									<?php //echo $objCommercant->Photo3 ; ?>
                                    <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->Photo3, 100, 100,'',''); ?>
                                    </span>&nbsp;
                                    <input type="button" value="Supprimer" onclick="if(!confirm('Voulez-vous vraiment supprimer cette photo?')){ return false;} else { effacerPhoto3('<?php echo $objCommercant->IdCommercant ; ?>') ;}"/>
                                    <br/>
                                    </div>
                                <input type="file" style="display:none" name="SocietePhoto3" id="Photo3Societe" value="" />
                                <?php } else { ?>
                                    <input type="file" name="SocietePhoto3" id="Photo3Societe" value="" />
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Photo 4 : </label>
                            </td>
                            <td>
                            <?php if($objCommercant->Photo4 != ""){ ?>
                                    <div id="divPhoto4">
                                    <span>
									<?php //echo $objCommercant->Photo4 ; ?>
                                    <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->Photo4, 100, 100,'',''); ?>
                                    </span>&nbsp;
                                    <input type="button" value="Supprimer" onclick="if(!confirm('Voulez-vous vraiment supprimer cette photo?')){ return false;} else { effacerPhoto4('<?php echo $objCommercant->IdCommercant ; ?>') ;}"/>
                                    <br/>
                                    </div>
                                <input type="file" style="display:none" name="SocietePhoto4" id="Photo4Societe" value="" />
                                <?php } else { ?>
                                    <input type="file" name="SocietePhoto4" id="Photo4Societe" value="" />
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Lien vers une vid&eacute;o : </label>
                            </td>
                            <td>
                                <input type="text" name="Societe[Video]" id="VideoSociete" value="<?php if(isset($objCommercant->Video)) echo htmlspecialchars($objCommercant->Video); ?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Horaires d'ouvertures : </label>
                            </td>
                            <td>
                                <textarea  name="Societe[Horaires]" id="HorairesSociete" ><?php if(isset($objCommercant->Horaires)) echo htmlspecialchars($objCommercant->Horaires); ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Date de fermeture annuelle : </label>
                            </td>
                            <td>
                                <textarea  name="Societe[Vacances]" id="VacancesSociete" ><?php if(isset($objCommercant->Vacances)) echo htmlspecialchars($objCommercant->Vacances); ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Description longue : </label>
                            </td>
                            <td>
                                <textarea  name="Societe[Caracteristiques]" id="CaracteristiquesSociete" ><?php if(isset($objCommercant->Caracteristiques)) echo htmlspecialchars($objCommercant->Caracteristiques); ?></textarea>
                                <?php echo display_ckeditor($ckeditor0); ?>
                            </td>
                        </tr>
                        <tr>
                        <tr>
                            <td>
                                <label>Conditions de paiement : </label>
                            </td>
                            <td>
                                <textarea  name="Societe[Conditions_paiement]" id="Conditions_paiement" ><?php if(isset($objCommercant->Conditions_paiement)) echo htmlspecialchars($objCommercant->Conditions_paiement); ?></textarea>
                            </td>
                        </tr>
                            <td>
                                <label>Lien Facebook : </label>
                            </td>
                            <td>
                                <input type="text" name="Societe[Facebook]" id="FacebookSociete" value="<?php if(isset($objCommercant->Facebook)) echo htmlspecialchars($objCommercant->Facebook); ?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Lien Google + : </label>
                            </td>
                            <td>
                                <input type="text" name="Societe[Twitter]" id="TwitterSociete" value="<?php if(isset($objCommercant->Twitter)) echo htmlspecialchars($objCommercant->Twitter); ?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>T&eacute;l&eacute;chargement d'une plaquette commerciale PDF : </label>
                            </td>
                            <td>
                            <?php if($objCommercant->Pdf != ""){ ?>
                                    <div id="divPdf">
                                    <span>
									<?php //echo $objCommercant->Pdf ; ?>
                                    
                                    <?php if ($objCommercant->Pdf != "" && $objCommercant->Pdf != NULL && file_exists("application/resources/front/photoCommercant/images/".$objCommercant->Pdf)== true) {?><a href="<?php echo base_url(); ?>application/resources/front/photoCommercant/images/<?php echo $objCommercant->Pdf ; ?>" target="_blank"><img src="<?php echo base_url(); ?>application/resources/front/images/pdf_icone.png" width="64" alt="PDF" /></a><?php } ?>
                                    
                                    
                                    </span>&nbsp;
                                    <input type="button" value="Supprimer" onclick="if(!confirm('Voulez-vous vraiment supprimer ce fichier?')){ return false;} else { effacerPdf('<?php echo $objCommercant->IdCommercant ; ?>') ;}"/>
                                    <br/>
                                    </div>
                                <input type="file" style="display:none" name="SocietePdf" id="PdfSociete" value="" />
                            <?php } else { ?>
                                <input type="file" name="SocietePdf" id="PdfSociete" value="" />
                            <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Conditions de fid&eacute;lisation : </label>
                            </td>
                            <td>
                                <textarea  name="Societe[Conditions]" id="ConditionsSociete" ><?php if(isset($objCommercant->Conditions)) echo htmlspecialchars($objCommercant->Conditions); ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td align="left">&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td align="left">
                            <div id="total_error_admin_fiche_comm" style="color:#F00;"></div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td align="left">&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td align="left"><input type="button" class="btnSinscrire" value="Modifier" /></td>
                        </tr>
                    </table>
                </fieldset>
              <input type="hidden" name="pdfAssocie" id="pdfAssocie" value="<?php if(isset($objCommercant->Pdf)) echo htmlspecialchars($objCommercant->Pdf); ?>" />
              <input type="hidden" name="photo1Associe" id="photo1Associe" value="<?php if(isset($objCommercant->Photo1)) echo htmlspecialchars($objCommercant->Photo1); ?>" />
              <input type="hidden" name="photo2Associe" id="photo2Associe" value="<?php if(isset($objCommercant->Photo2)) echo htmlspecialchars($objCommercant->Photo2); ?>" />
              <input type="hidden" name="photo3Associe" id="photo3Associe" value="<?php if(isset($objCommercant->Photo3)) echo htmlspecialchars($objCommercant->Photo3); ?>" />
              <input type="hidden" name="photo4Associe" id="photo4Associe" value="<?php if(isset($objCommercant->Photo4)) echo htmlspecialchars($objCommercant->Photo4); ?>" />
              <input type="hidden" name="photo5Associe" id="photo5Associe" value="<?php if(isset($objCommercant->Photo5)) echo htmlspecialchars($objCommercant->Photo5); ?>" />
              <input type="hidden" name="photoAccueilAssocie" id="photoAccueilAssocie" value="<?php if(isset($objCommercant->PhotoAccueil)) echo htmlspecialchars($objCommercant->PhotoAccueil); ?>" />
        </form>
    </div>
    
<?php $this->load->view("admin/includes/vwFooter2013"); ?>