<div class="row glissiere_tab">
    <form class="m-0 w-100 row d-flex">
        <div class="col-4">
            <div id="txt_title_gli6" class="text-left title_gli titre_gli_up"><?php if (isset($title_gli6->titre_glissiere)) {echo $title_gli6->titre_glissiere;}else{echo 'AJOUTER UNE GLISSIERE ';} ?></div>
        </div>
        <div class="col-4" id="activ_glissiere_activ6">
            <div class="cercle">
                <div class="text-center up d-none" id="activ_glissiere_activ6_up"><p>fermer</p></div>
                <div class="text-center down" id="activ_glissiere_activ6_down"><p>ouvrir</p></div>
            </div>
        </div>
        <div class="col-4 d-flex">
            <div class="text-right w-75 act_desact_text">Activer/désactiver</div>
            <div class="text-right w-25 titre_gli_up">
<!--                Activé:-->
<!--                <select id="title_gli6_is_activ" name="data[is_activ_gli6]">-->
<!--                    <option value="0">Non</option>-->
<!--                    <option --><?php //if (isset($title_gli6->is_activ_glissiere) AND $title_gli6->is_activ_glissiere == '1' ) echo "selected='selected'";  ?><!-- value="1">Oui</option>-->
<!--                </select>-->
                <label class="switch">
                    <input id="title_gli6_is_check" type="checkbox" <?php if (isset($title_gli6->is_activ_glissiere) AND $title_gli6->is_activ_glissiere == '1') echo 'checked'; ?> >
                    <span class="slider round"></span>
                </label>
                <input name="data[is_activ_gli6]" id="title_gli6_is_activ" type="hidden" value="<?php if (isset($title_gli6->is_activ_glissiere)) echo $title_gli6->is_activ_glissiere; ?>">
            </div>
        </div>
    </form>
</div>
<div class="row pb-5 shadowed p-2 gli_content d-none" id="activ_glissiere_activ6_content">
    <div class="row">
        <div class="col-2"></div>
        <div class="col-8 pl-2">
            <h5 class="text_label_spec pt-2">Ajouter un titre à cette glissière</h5>
            <span class="text_label_spec pr-4 float-right"><span id="char_true_title_6">20 </span>Caractères</span>
        </div>
    </div>
    <div class="row pb-2 ml-0 mr-1 bordered_bottom">
        <div class="col-2 title_gli_title text-center">Titre</div>
        <div class="col-8 pl-0 pr-4 padded_title">
            <input id="title_gli6" value="<?php if (isset($title_gli6->titre_glissiere)) echo $title_gli6->titre_glissiere ?>" onchange="chenge_title_hiddden(this)" class="form-control pr-0 textarea_style padded_title_input" type="text">
            <input id="hidden_title_gli6" value="" type="hidden">
        </div>
        <div class="col-2 pl-0 pr-2">
            <a target="_blank" href="https://fotoflexer.com/editor/"><img src="<?php echo base_url()?>assets/images/IMAGE45.webp" /></a>
            <button id="title_gli6" onclick="chenge_title(this)" class="w-100 btn btn-secondary noned_6 d-none" style="border-radius: unset!important">Valider</button>
        </div>
    </div>
    <div class="w-100">
        <div class="w-100" id="glis_content6">
            <?php if (!isset($data_gli6) OR (isset($data_gli6) AND empty($data_gli6)) ){ ?>
                <div class="pt-3 lined6" id="gliss61">
                    <div class="w-100 row d-flex">
                        <div class="col-2 pr-0" id="img_contentss61"><div class='text_label2 pt-5'>Article non enregistré</div></div>
                        <div class="col-8">
                            <div class="w-100 des_label">Titre produit ou service<span class="float-right"><span id="char_title_61">70 </span>Caractères</span></div>
                            <input type="text"  class="form-control true_title" id="true_title_art61">
                            <div class="w-100 des_label">Désignation<span class="float-right"><span id="char_61">180 </span>Caractères</span></div>
                            <textarea id="titre_art61" type="text" class="form-control textarea_style_designation" ></textarea>
                        </div>
                        <div class="col-2 price_cont_art">
                            <div class="col-12 title_txts">
                                Prix unitaire
                            </div>
                            <input id="prix_art61"  type="number" class="form-control textarea_style" />
                        </div>
                        <input type="hidden" id="id_art61" name="id_art61" value="0">
                        <div class="d-none" id="save_ind61" onclick="save_art6(1)"></div>
                    </div>
                </div>
            <?php }else{$i6=1; ?>
                <?php foreach ($data_gli6 as $gli6) {  ?>
                    <div class="pt-3 lined6" id="gliss6<?php echo $i6; ?>">
                        <div class="w-100 row d-flex">
                            <div class="col-2 pr-0" id="img_contentss6<?php echo $i6; ?>">
                                <?php if ($gli6->image !=null && is_file("application/resources/front/photoCommercant/imagesbank/".$user_ion_auth."/menugli1/".$gli6->image)){ ?>
                                    <img class="img-fluid" src="<?php echo base_url()."application/resources/front/photoCommercant/imagesbank/".$user_ion_auth."/menugli1/".$gli6->image ?>">
                                    <div class="w-100 pl-1"><div onclick="delete_image_menu(<?php echo $gli6->id; ?>,6<?php echo $i6; ?>)" class="btn_delete_img">x</div></div>
                                <?php }else{ ?>
                                    <div id="Articlephoto6<?php echo $i6; ?>_container" onclick='javascript:window.open("<?php echo site_url("media/index/".$gli6->id."-menugli1-photo6".$i6); ?>", "", "width=1045, height=675, scrollbars=yes");' href='javascript:void(0);' class="w-100 img_add text-center" style="border-radius: unset!important">
                                        <img class="img-fluid" src="<?php echo base_url()?>assets/images/download-icon-png.webp" style="height:160px">
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="col-8">
                                <div class="w-100 des_label">Titre produit ou service<span class="float-right"><span id="char_title_6<?php echo $i6; ?>">70 </span>Caractères</span></div>
                                <input type="text" id="true_title_art6<?php echo $i6; ?>" class="form-control true_title" value="<?php echo $gli6->true_title ?? '';?>">
                                <div class="w-100 des_label">Désignation<span class="float-right"><span id="char_6<?php echo $i6; ?>">180 </span>Caractères</span></div>
                                <textarea id="titre_art6<?php echo $i6; ?>" type="text" class="form-control textarea_style_designation" ><?php echo $gli6->titre;?></textarea>
                            </div>
                            <div class="col-2 price_cont_art">
                                <?php if (isset($gli6->id)){ ?>
                                    <div class="w-100" id="delete_art1<?php echo $i6; ?>">
                                        <div onclick="delete_art(<?php echo $gli6->id; ?>)" class="delete_img_div">x</div>
                                    </div>
                                <?php } ?>
                                <div class="w-100 title_txts">
                                    Prix unitaire
                                </div>
                                <input id="prix_art6<?php echo $i6; ?>" value="<?php echo $gli6->prix;?>" type="number" class="form-control textarea_style" />
                            </div>
                            <div class="d-none" id="save_ind6<?php echo $i6; ?>" onclick="save_art6(<?php echo $i6; ?>)"></div>
                            <input type="hidden" id="id_art6<?php echo $i6; ?>" name="id_art<?php echo $i6; ?>" value="<?php echo $gli6->id;?>">
                        </div>
                    </div>
                    <?php $i6++; } ?>
            <?php } ?>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center w-100 p-3" >Ajouter une information pour cette cetegorie</div>
                <textarea id="description_gli6" name="description_gli6" class="form-control"><?php if (isset($title_gli6->description_gli)) echo $title_gli6->description_gli; ?></textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-12 pt-3">
                <div class="row">
                    <div class="col-1 pl-3 pr-0">
                        <div id="add_line6" class="btn_add_line">+</div>
                    </div>
                    <div class="col-61 pl-0 pt-2 text-left">
                        <div class="add_txts">Ajouter une ligne</div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center p-2">
                <button id="save_all6" class="btn btn-secondary w-50">Validation</button>
            </div>
        </div>
        <div class="row">
            <div class="col-12 pt-3 warn_img">
                Avant d'intégrer les images, vous devez préciser les textes et les prix unitaires et valider.
            </div>
        </div>
    </div>
</div>