<script type="text/javascript">
    $(document).ready(function () {

        $("#is_actif_condition").change(function(){
            if ($('#is_actif_condition').is(":checked"))
            {
                $("#is_actif_condition_hidden").val(1);
            }else{
                $("#is_actif_condition_hidden").val(0);
            }
        });

        $("#is_activ_emp").change(function(){
            if ($('#is_activ_emp').is(":checked"))
            {
                $("#is_activ_emp_hidden").val(1);
            }else{
                $("#is_activ_emp_hidden").val(0);
            }
        });
        $("#is_activ_default_btn_btn").change(function(){
            if ($('#is_activ_default_btn_btn').is(":checked"))
            {
                $("#is_activ_default_btn").val(1);
            }else{
                $("#is_activ_default_btn").val(0);
            }
        });
        $("#is_activ_prom").change(function(){
            if ($('#is_activ_prom').is(":checked"))
            {
                $("#is_activ_prom_hidden").val(1);
            }else{
                $("#is_activ_prom_hidden").val(0);
            }
        });
        $("#is_activ_help_img").change(function(){
            if ($('#is_activ_help_img').is(":checked"))
            {
                $("#is_activ_help_img_hidden").val(1);
            }else{
                $("#is_activ_help_img_hidden").val(0);
            }
        });
        $("#is_activ_Virement_differe").change(function(){
            if ($('#is_activ_Virement_differe').is(":checked"))
            {
                $("#is_activ_Virement_differe_hidden").val(1);
            }else{
                $("#is_activ_Virement_differe_hidden").val(0);
            }
        });
        $("#is_activ_carte_differe").change(function(){
            if ($('#is_activ_carte_differe').is(":checked"))
            {
                $("#is_activ_carte_differe_hidden").val(1);
            }else{
                $("#is_activ_carte_differe_hidden").val(0);
            }
        });
        $("#is_activ_Cheque_differe").change(function(){
            if ($('#is_activ_Cheque_differe').is(":checked"))
            {
                $("#is_activ_Cheque_differe_hidden").val(1);
            }else{
                $("#is_activ_Cheque_differe_hidden").val(0);
            }
        });
        $("#is_activ_banc_type_differe").change(function(){
            if ($('#is_activ_banc_type_differe').is(":checked"))
            {
                $("#is_activ_Cheque_differe").removeAttr("disabled");
                $("#is_activ_Virement_differe").removeAttr("disabled");
                $("#is_activ_carte_differe").removeAttr("disabled");
                $("#is_activ_banc_type_differe_hidden").val(1);
            }else{
                $("#is_activ_banc_type_differe_hidden").val(0);
                $("#is_activ_Cheque_differe").attr("disabled","true");
                $("#is_activ_Virement_differe").attr("disabled","true");
                $("#is_activ_carte_differe").attr("disabled","true");
            }
        });
        $("#is_activ_facture_differe").change(function(){
            if ($('#is_activ_facture_differe').is(":checked"))
            {
                $("#is_activ_facture_differe_hidden").val(1);
            }else{
                $("#is_activ_facture_differe_hidden").val(0);
            }
        });
        $("#is_activ_pro_forma_differe").change(function(){
            if ($('#is_activ_pro_forma_differe').is(":checked"))
            {
                $("#is_activ_pro_forma_differe_hidden").val(1);
            }else{
                $("#is_activ_pro_forma_differe_hidden").val(0);
            }
        });
        $("#is_activ_devis_differe").change(function(){
            if ($('#is_activ_devis_differe').is(":checked"))
            {
                $("#is_activ_devis_differe_hidden").val(1);
            }else{
                $("#is_activ_devis_differe_hidden").val(0);
            }
        });
        $("#is_activ_fact_type_differe").change(function(){
            if ($('#is_activ_fact_type_differe').is(":checked"))
            {
                $("#is_activ_devis_differe").removeAttr("disabled");
                $("#is_activ_pro_forma_differe").removeAttr("disabled");
                $("#is_activ_facture_differe").removeAttr("disabled");
                $("#is_activ_fact_type_differe_hidden").val(1);
            }else{
                $("#is_activ_devis_differe").attr("disabled","true");
                $("#is_activ_pro_forma_differe").attr("disabled","true");
                $("#is_activ_facture_differe").attr("disabled","true");
                $("#is_activ_fact_type_differe_hidden").val(0);
            }
        });
        $("#is_activ_comment_default_differe").change(function(){
            if ($('#is_activ_comment_default_differe').is(":checked"))
            {
                $("#comment_differe_txt").val("Validez votre commande, par retour de mail, vous recevez une confirmation de réception.  Nous vous adresserons le document conforme à votre organisation (précisez ci-dessous la qualité de ce document et vos conditions de règlement) Votre règlement enregistrée, votre livraison sera activée.")
                $("#is_activ_comment_default_differe_hidden").val(1);
            }else{
                $("#is_activ_comment_default_differe_hidden").val(0);
            }
        });
        $("#is_activ_env_document").change(function(){
            if ($('#is_activ_env_document').is(":checked"))
            {
                $("#is_activ_env_document_hidden").val(1);
            }else{
                $("#is_activ_env_document_hidden").val(0);
            }
        });

        $("#is_activ_vente_differe").change(function(){
            if ($('#is_activ_vente_differe').is(":checked"))
            {
                $("#is_activ_vente_differe_hidden").val(1);
            }else{
                $("#is_activ_vente_differe_hidden").val(0);
            }
        });

        $("#is_activ_paypal_comment").change(function(){
            if ($('#is_activ_paypal_comment').is(":checked"))
            {
                $("#new_paypal_comment").val("J'accepte le règlement de cette commande avec le module Paypal en précisant le montant total ttc net a régler, valider le bouton PAYPAL qui se trouve sur paragraphe suivant.");
                $("#is_activ_paypal_comment_hidden").val(1);
            }else{
                $("#is_activ_paypal_comment_hidden").val(0);
            }
        });
        $("#is_activ_livr_paypal").change(function(){
            if ($('#is_activ_livr_paypal').is(":checked"))
            {
                $("#is_activ_livr_paypal_hidden").val(1);
            }else{
                $("#is_activ_livr_paypal_hidden").val(0);
            }
        });
        $("#is_activ_livr_a_domicile").change(function(){
            if ($('#is_activ_livr_a_domicile').is(":checked"))
            {
                $("#is_activ_livr_a_domicile_hidden").val(1);
            }else{
                $("#is_activ_livr_a_domicile_hidden").val(0);
            }
        });

        $("#is_activ_type_livr_grat").change(function(){
            if ($('#is_activ_type_livr_grat').is(":checked"))
            {
                $("#is_activ_cheque_enlev").removeAttr("disabled");
                $("#is_activ_termin_banc_enlev").removeAttr("disabled");
                $("#is_activ_espece_enlev").removeAttr("disabled");
                $("#is_activ_type_livr_grat_hidden").val(1);
            }else{
                $("#is_activ_type_livr_grat_hidden").val(0);
                $("#is_activ_cheque_enlev").attr("disabled","true");
                $("#is_activ_termin_banc_enlev").attr("disabled","true");
                $("#is_activ_espece_enlev").attr("disabled","true");
            }
        });
        $("#is_activ_type_livr").change(function(){
            if ($('#is_activ_type_livr').is(":checked"))
            {
                $("#is_activ_type_livr_hidden").val(1);
            }else{
                $("#is_activ_type_livr_hidden").val(0);
            }
        });
        $("#is_activ_espece_enlev").change(function(){
            if ($('#is_activ_espece_enlev').is(":checked"))
            {
                $("#is_activ_espece_enlev_hidden").val(1);
            }else{
                $("#is_activ_espece_enlev_hidden").val(0);
            }
        });

        $("#is_activ_livr").change(function(){
            if ($('#is_activ_livr').is(":checked"))
            {
                $("#is_activ_livr_hidden").val(1);
            }else{
                $("#is_activ_livr_hidden").val(0);
            }
        });

        $("#is_activ_comm_livr_dom").change(function(){
            if ($('#is_activ_comm_livr_dom').is(":checked"))
            {
                $("#is_activ_comm_livr_dom_hidden").val(1);
            }else{
                $("#is_activ_comm_livr_dom_hidden").val(0);
            }
        });

        $("#is_activ_comment_bottom").change(function(){
            if ($('#is_activ_comment_bottom').is(":checked"))
            {
                $("#comment_comm_spec_bottom_txt").val("Dès réception de votre commande, notre équipe vous contactera et vous précisera les conditions de livraison ou de mise à disposition. Les règlement s'effectueront avec notre terminal mobile de paiement par carte bancaire ou la remise")
                $("#is_activ_comment_bottom_hidden").val(1);
            }else{
                $("#comment_comm_spec_bottom_txt").val("");
                $("#is_activ_comment_bottom_hidden").val(0);
            }
        });

        $("#is_activ_cheque_bottom").change(function(){
            if ($('#is_activ_cheque_bottom').is(":checked"))
            {
                $("#is_activ_cheque_bottom_hidden").val(1);
            }else{
                $("#is_activ_cheque_bottom_hidden").val(0);
            }
        });

        $("#is_activ_card_bank_bottom").change(function(){
            if ($('#is_activ_card_bank_bottom').is(":checked"))
            {
                $("#is_activ_card_bank_bottom_hidden").val(1);
            }else{
                $("#is_activ_card_bank_bottom_hidden    ").val(0);
            }
        });

        $("#is_activ_com_spec").change(function(){
            if ($('#is_activ_com_spec').is(":checked"))
            {
                $("#is_activ_com_spec_hidden").val(1);
            }else{
                $("#is_activ_com_spec_hidden").val(0);
            }
        });

        $("#is_activ_termin_bank_livr").change(function(){
            if ($('#is_activ_termin_bank_livr').is(":checked"))
            {
                $("#is_activ_termin_bank_livr_hidden").val(1);
                $("#block_pay_mobile").removeClass("d-none");
                $("#block_pay_mobile").addClass("d-block");
            }else{
                $("#is_activ_termin_bank_livr_hidden").val(0);
                $("#block_pay_mobile").removeClass("d-block");
                $("#block_pay_mobile").addClass("d-none");
            }
        });

        $("#is_activ_cheque_livr").change(function(){
            if ($('#is_activ_cheque_livr').is(":checked"))
            {
                $("#is_activ_cheque_livr_hidden").val(1);
                $("#block_pay_cheque").removeClass("d-none");
                $("#block_pay_cheque").addClass("d-block");
            }else{
                $("#is_activ_cheque_livr_hidden").val(0);
                $("#block_pay_cheque").removeClass("d-block");
                $("#block_pay_cheque").addClass("d-none");
            }
        });

        $("#is_activ_termin_banc_enlev").change(function(){
            if ($('#is_activ_termin_banc_enlev').is(":checked"))
            {
                $("#is_activ_termin_banc_enlev_hidden").val(1);
                $("#block_pay_cheque").removeClass("d-none");
                $("#block_pay_cheque").addClass("d-block");
            }else{
                $("#is_activ_termin_banc_enlev_hidden").val(0);
                $("#block_pay_cheque").removeClass("d-block");
                $("#block_pay_cheque").addClass("d-none");
            }
        });

        $("#is_activ_tomm_img").change(function(){
            if ($('#is_activ_tomm_img').is(":checked"))
            {
                $("#is_activ_tomm_img_hidden").val(1);
            }else{
                $("#is_activ_tomm_img_hidden").val(0);
            }
        });

        $("#is_activ_cheque_enlev").change(function(){
            if ($('#is_activ_cheque_enlev').is(":checked"))
            {
                $("#is_activ_cheque_enlev_hidden").val(1);
                $("#block_pay_mobile").removeClass("d-none");
                $("#block_pay_mobile").addClass("d-block");
            }else{
                $("#is_activ_cheque_enlev_hidden").val(0);
                $("#block_pay_mobile").removeClass("d-block");
                $("#block_pay_mobile").addClass("d-none");
            }
        });

        $("#is_activ_comm_enlev").change(function(){
            if ($('#is_activ_comm_enlev').is(":checked"))
            {
                $("#is_activ_comm_enlev_hidden").val(1);
            }else{
                $("#is_activ_comm_enlev_hidden").val(0);
            }
        });

        $("#is_activ_paypal_livr").change(function(){
            if ($('#is_activ_paypal_livr').is(":checked"))
            {
                $("#is_activ_paypal_livr_hidden").val(1);
                $("#paypal_block_content").removeClass("d-flex");
                $("#paypal_block_content").addClass("d-block");
                $("#activ_glissiere_activ5_content").removeClass("d-none");
                $("#activ_glissiere_activ5_content").addClass("d-flex");
            }else{
                $("#is_activ_paypal_livr_hidden").val(0);
                $("#paypal_block_content").removeClass("d-flex");
                $("#paypal_block_content").addClass("d-none");
                $("#activ_glissiere_activ5_content").removeClass("d-flex");
                $("#activ_glissiere_activ5_content").addClass("d-none");
            }
        });


        $("#is_activ_webservice").change(function(){
            if ($('#is_activ_webservice').is(":checked"))
            {
                $("#is_activ_webservice_hidden").val(1);
            }else{
                $("#is_activ_webservice_hidden").val(0);
            }
        });

        $("#is_activ_visite").change(function(){
            if ($('#is_activ_visite').is(":checked"))
            {
                $("#is_activ_visite_hidden").val(1);
            }else{
                $("#is_activ_visite_hidden").val(0);
            }
        });

        $("#is_activ_del_livr").change(function(){
            if ($('#is_activ_del_livr').is(":checked"))
            {
                $("#is_activ_del_livr_hidden").val(1);
            }else{
                $("#is_activ_del_livr_hidden").val(0);
            }
        });

        $("#is_activ_prix_livr_grat").change(function(){
            if ($('#is_activ_prix_livr_grat').is(":checked"))
            {
                $("#is_activ_prix_livr_grat_hidden").val(1);
            }else{
                $("#is_activ_prix_livr_grat_hidden").val(0);
            }
        });

        $("#is_activ_glissiere").change(function(){
            if ($('#is_activ_glissiere').is(":checked"))
            {
                $("#title_gli1_is_activ").val(1);
            }else{
                $("#title_gli1_is_activ").val(0);
            }
        });
        $("#is_activ_glissiere2").change(function(){
            if ($('#is_activ_glissiere2').is(":checked"))
            {
                $("#title_gli2_is_activ").val(1);
            }else{
                $("#title_gli2_is_activ").val(0);
            }
        });
        $("#title_gli3_is_check").change(function(){
            if ($('#title_gli3_is_check').is(":checked"))
            {
                $("#title_gli3_is_activ").val(1);
            }else{
                $("#title_gli3_is_activ").val(0);
            }
        });

        $("#is_activ_comment_default_livr").change(function(){
            if ($('#is_activ_comment_default_livr').is(":checked"))
            {
                $("#livraison_comment_textarea").val("Livraison sans contact : après avoir validé votre commande, vous devez imprimer le justificatif de bonne réception que vous remettrez à notre livreur qui restera sur le pas de votre porte.");
                $("#is_activ_comment_default_livr_hidden").val(1);
            }else{
                $("#livraison_comment_textarea").val("");
                $("#is_activ_comment_default_livr_hidden").val(0);
            }
        });

        $("#title_gli4_is_check").change(function(){
            if ($('#title_gli4_is_check').is(":checked"))
            {
                $("#title_gli4_is_activ").val(1);
            }else{
                $("#title_gli4_is_activ").val(0);
            }
        });

        $("#title_gli6_is_check").change(function(){
            if ($('#title_gli6_is_check').is(":checked"))
            {
                $("#title_gli6_is_activ").val(1);
            }else{
                $("#title_gli6_is_activ").val(0);
            }
        });

        $("#title_gli7_is_check").change(function(){
            if ($('#title_gli7_is_check').is(":checked"))
            {
                $("#title_gli7_is_activ").val(1);
            }else{
                $("#title_gli7_is_activ").val(0);
            }
        });

        $("#title_gli5_is_check").change(function(){
            if ($('#title_gli5_is_check').is(":checked"))
            {
                $("#title_gli5_is_activ").val(1);
            }else{
                $("#title_gli5_is_activ").val(0);
            }
        });

        $("#is_activ_terpminal_livr_check").change(function(){
            if ($('#is_activ_terpminal_livr_check').is(":checked"))
            {
                $("#is_activ_terpminal_livr_enlev").val(1);
            }else{
                $("#is_activ_terpminal_livr_enlev").val(0);
            }
        });

        $("#is_activ_paypal_enlev").change(function(){
            if ($('#is_activ_paypal_enlev').is(":checked"))
            {
                $("#is_activ_paypal_enlev_hidden").val(1);
                $("#paypal_block_content").removeClass("d-flex");
                $("#paypal_block_content").addClass("d-block");
                $("#activ_glissiere_activ5_content").removeClass("d-none");
                $("#activ_glissiere_activ5_content").addClass("d-flex");
            }else{
                $("#is_activ_paypal_enlev_hidden").val(0);
                $("#paypal_block_content").removeClass("d-flex");
                $("#paypal_block_content").addClass("d-none");
                $("#activ_glissiere_activ5_content").removeClass("d-flex");
                $("#activ_glissiere_activ5_content").addClass("d-none");
            }
        });

        $("#is_activ_comment_default_enlev").change(function(){
            if ($('#is_activ_comment_default_enlev').is(":checked"))
            {
                $("#comment_emporter_txt").val("Nous vous demandons avant de vous déplacer de nous appeler au 04 22 56 58 54 afin d'avoir la confirmation de la finalisation de votre commande");
                $("#is_activ_comment_default_enlev_hidden").val(1);
            }else{
                $("#comment_emporter_txt").val('');
                $("#is_activ_comment_default_enlev_hidden").val(0);
            }
        });
    });
</script>
<script type="text/javascript">

    function save_art(i){
        var val_title = $("#titre_art1"+i).val();
        var val_true_title = $("#true_title_art1"+i).val();
        var val_prix = $("#prix_art1"+i).val();
        var id_art = $("#id_art1"+i).val();
        var datas = "titre="+val_title+"&prix="+val_prix+"&idCom="+"<?php echo $idcom; ?>"+"&idgli=1"+"&indexs="+i+"&id="+id_art+"&true_title="+val_true_title;
        if(val_true_title != "" && val_true_title != undefined && val_true_title != null){
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/menu/Info_menu_commercant/save_art'); ?>",
                data: datas ,
                success: function(data) {
                    if ($("#img_contentss1"+i).html() === '<div class="text_label2 pt-5">Article non enregistré</div>') {
                        var obj = JSON.parse(data);
                        $("#img_contentss1"+i).html(obj.data);
                        $("#id_art1"+i).val(obj.id);
                    }
                    // alert("OK");
                },
                error: function() {
                    // alert('Erreur');
                }
            });
        }else{
            alert("Le Champ 'Titre produit ou service' est obligatoire ");
            throw new Error("champ non rempli");
        }
    }

    $(document).ready(function() {

    });

    function upload_cgv(){
        document.getElementById("cgv_action").click();
    }
    function upload_cgv_livraison(){
        document.getElementById("cgv_action_livraison").click();
    }
    $('#cgv_action').change(function(e) {
        const fsize = e.target.files[0].size;
        const file = Math.round((fsize / 1024));
        // The size of the file.
        if (file >= 15000) {
            $('#cgv_action').val('');
            $("#content_link").val('');
            document.getElementById("error").innerHTML = "fichier trop volumineux, veuillez inserer un fichier moins de 15mb!";
            throw new Error("fichier trop volumineux");
        }else{
            var url_set = '<?php echo base_url()."application/resources/front/photoCommercant/cgv/".$infocom->user_ionauth_id."/";?>';
            var file_get = e.target.files[0].name;
            document.getElementById("error").innerHTML = "";
            $("#content_link").val(url_set+file_get);
            setTimeout(function(){
                $("#btn_cgv").html('<span class="plus_span">+</span> Téléchargement');
            }, 1);
        }
    });
    $('#cgv_action_livraison').change(function(e) {
        const fsize = e.target.files[0].size;
        const file = Math.round((fsize / 1024));
        // The size of the file.
        if (file >= 15000) {
            $('#cgv_action_livraison').val('');
            $("#content_link_livraison").val('');
            document.getElementById("error1").innerHTML = "fichier trop volumineux, veuillez inserer un fichier moins de 15mb!";
            throw new Error("fichier trop volumineux");
        }else {
            var url_set1 = '<?php echo base_url() . "application/resources/front/photoCommercant/cgv/" . $infocom->user_ionauth_id . "/";?>';
            var file_get = e.target.files[0].name;
            document.getElementById("error1").innerHTML = "";
            $("#content_link_livraison").val(url_set1 + file_get);
            setTimeout(function () {
                $("#btn_cgv_livraison").html('<span class="plus_span">+</span> Téléchargement');
            }, 1);
        }
    });
    $('#cgv_action_differe').change(function(e) {
        const fsize = e.target.files[0].size;
        const file = Math.round((fsize / 1024));
        // The size of the file.
        if (file >= 15000) {
            $('#cgv_action_differe').val('');
            $("#content_link_deffere").val('');
            document.getElementById("error2").innerHTML = "fichier trop volumineux, veuillez inserer un fichier moins de 15mb!";
            throw new Error("fichier trop volumineux");
        }else {
            var url_set2 = '<?php echo base_url() . "application/resources/front/photoCommercant/cgv/" . $infocom->user_ionauth_id . "/";?>';
            var file_get = e.target.files[0].name;
            document.getElementById("error2").innerHTML = "";
            $("#content_link_deffere").val(url_set2 + file_get);
            setTimeout(function () {
                $("#btn_cgv_differe").html('<span class="plus_span">+</span> Téléchargement');
            }, 1);
        }

    });
    $('#cgv_action_specif').change(function(e) {
        const fsize = e.target.files[0].size;
        const file = Math.round((fsize / 1024));
        // The size of the file.
        if (file >= 15000) {
            $('#cgv_action_specif').val('');
            $("#content_link_specif").val('');
            document.getElementById("error3").innerHTML = "fichier trop volumineux, veuillez inserer un fichier moins de 15mb!";
            throw new Error("fichier trop volumineux");
        }else {
            var url_set3 = '<?php echo base_url() . "application/resources/front/photoCommercant/cgv/" . $infocom->user_ionauth_id . "/";?>';
            var file_get = e.target.files[0].name;
            document.getElementById("error3").innerHTML = "";
            $("#content_link_specif").val(url_set3 + file_get);
            setTimeout(function () {
                $("#btn_cgv_specif").html('<span class="plus_span">+</span> Téléchargement');
            }, 1);
        }
    });
    function upload_cgv_differe(){
        document.getElementById("cgv_action_differe").click();
    }
    function upload_cgv_specif(){
        document.getElementById("cgv_action_specif").click();
    }
    function upload_doc(){
        document.getElementById("doc_action").click();
    }

    function delete_cgv(){
        var val_actual = $("#cgv_action").val();
        var id_com = "<?php echo $idcom; ?>";
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('admin/menu/Info_menu_commercant/delete_cgv'); ?>",
            data: "cgv="+val_actual+"&id_com="+id_com,
            success: function( data ) {
                if (data =="1"){
                    alert("Supprimé !");
                    location.reload()
                }else{
                    alert("erreur !");
                }
            },
            error: function() {
                alert('Erreur');
            }
        });
    }
    function delete_cgv_livraison(){
        var val_actual = $("#cgv_action_livraison").val();
        var id_com = "<?php echo $idcom; ?>";
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('admin/menu/Info_menu_commercant/delete_cgv_livraison'); ?>",
            data: "cgv="+val_actual+"&id_com="+id_com,
            success: function( data ) {
                if (data =="1"){
                    alert("Supprimé !");
                    location.reload()
                }else{
                    alert("erreur !");
                }
            },
            error: function() {
                alert('Erreur');
            }
        });
    }
    function delete_cgv_differe(){
        var val_actual = $("#cgv_action_differe").val();
        var id_com = "<?php echo $idcom; ?>";
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('admin/menu/Info_menu_commercant/delete_cgv_differe'); ?>",
            data: "cgv="+val_actual+"&id_com="+id_com,
            success: function( data ) {
                if (data =="1"){
                    alert("Supprimé !");
                    location.reload()
                }else{
                    alert("erreur !");
                }
            },
            error: function() {
                alert('Erreur');
            }
        });
    }
    function delete_cgv_specif(){
        var val_actual = $("#cgv_action_specif").val();
        var id_com = "<?php echo $idcom; ?>";
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('admin/menu/Info_menu_commercant/delete_cgv_specif'); ?>",
            data: "cgv="+val_actual+"&id_com="+id_com,
            success: function( data ) {
                if (data =="1"){
                    alert("Supprimé !");
                    location.reload()
                }else{
                    alert("erreur !");
                }
            },
            error: function() {
                alert('Erreur');
            }
        });
    }
    function delete_doc(){
        var val_actual = $("#doc_action").val();
        var id_com = "<?php echo $idcom; ?>";
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('admin/menu/Info_menu_commercant/delete_doc'); ?>",
            data: "doc="+val_actual+"&id_com="+id_com,
            success: function( data ) {
                if (data =="1"){
                    alert("Supprimé !");
                    location.reload()
                }else{
                    alert("erreur !");
                }
            },
            error: function() {
                alert('Erreur');
            }
        });
    }

    $(document).ready(function () {
        $("#cgv_action").change(function(){
            $("#btn_cgv").html($("#cgv_action").val())
        })
        $("#doc_action").change(function(){
            $("#btn_doc").html($("#doc_action").val())
        })
        $('.textarea_style_designation').keyup(function() {
            var textlimit = 180;
            var tlength = $(this).val().length;
            $(this).val($(this).val().substring(0, textlimit));
            var tlength = $(this).val().length;
            remain = textlimit - parseInt(tlength);
            console.log(remain);
            var id_brut = this.id;
            var id = id_brut.replace('titre_art','');
            console.log("#char_"+id);
            $("#char_"+id).html(remain+" ")
        });

        $('.true_title').keyup(function() {
            var textlimit = 70;
            var tlength = $(this).val().length;
            $(this).val($(this).val().substring(0, textlimit));
            var tlength = $(this).val().length;
            remain = textlimit - parseInt(tlength);
            console.log(remain);
            var id_brut = this.id;
            var id = id_brut.replace('true_title_art','');
            console.log("#char_"+id);
            $("#char_title_"+id).html(remain+" ")
        });
        $('.padded_title_input').keyup(function() {
            var textlimit = 20;
            var tlength = $(this).val().length;
            $(this).val($(this).val().substring(0, textlimit));
            var tlength = $(this).val().length;
            remain = textlimit - parseInt(tlength);
            console.log(remain);
            var id_brut = this.id;
            var id = id_brut.replace('title_gli','');
            console.log("#char_"+id);
            $("#char_true_title_"+id).html(remain+" ")
        });

        $("#add_line").click(function () {
            var current = document.getElementsByClassName('lined1').length;
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/menu/Info_menu_commercant/add_line'); ?>",
                data: "current="+current+"&nbgliss=1",
                success: function( data ) {
                    $("#glis_content").append(data);
                },
                error: function() {
                    alert('Erreur');
                }
            });
        });

        $("#save_all").click(function () {
            var lines = document.getElementsByClassName('lined1');
            var click_to = document.getElementsByClassName('noned_1')[0].click();
            for (i=1; i<lines.length+1;i++){
                //alert("#save_ind1"+i);
                $("#save_ind1"+i).click();
            }
            alert('Enregistré');
        })
    });
</script>



<!--gl222-->

<script type="text/javascript">

    function save_paypal_data() {

        // var is_activ = document.getElementById('title_gli5_is_activ').value;
        var txt_paypal = document.getElementById('paypal_content').value;
        var id_com = "<?php echo $idcom; ?>";
        // var pay_domicile = document.getElementById('is_activ_terpminal_livr_enlev').value;
        // var comment_livr_enlev = document.getElementById('comment_livr_enlev').value;
        var new_paypal_comment = document.getElementById('new_paypal_comment').value;

        var is_activ_comm_enlev = document.getElementById('is_activ_comm_enlev_hidden').value;
        var is_activ_com_spec = document.getElementById('is_activ_com_spec_hidden').value;
        var is_activ_comm_livr_dom = document.getElementById('is_activ_comm_livr_dom_hidden').value;
        var is_activ_comment_bottom = document.getElementById('is_activ_comment_bottom_hidden').value;
        var is_activ_card_bank_bottom = document.getElementById('is_activ_card_bank_bottom_hidden').value;
        var is_activ_cheque_bottom = document.getElementById('is_activ_cheque_bottom_hidden').value;
        var comment_comm_spec_bottom_txt = document.getElementById('comment_comm_spec_bottom_txt').value;

        $.ajax({
            type: "POST",
            url: "<?php echo site_url('admin/menu/Info_menu_commercant/save_paypal_data'); ?>",
            data: "txt_paypal="+txt_paypal+"&idcom="+id_com+"&new_paypal_comment="+new_paypal_comment+"&is_activ_com_spec="+is_activ_com_spec+"&is_activ_comm_livr_dom="+is_activ_comm_livr_dom+"&is_activ_card_bank_bottom="+is_activ_card_bank_bottom+"&is_activ_cheque_bottom="+is_activ_cheque_bottom+"&is_activ_comment_bottom="+is_activ_comment_bottom+"&is_activ_comm_enlev="+is_activ_comm_enlev+"&comment_comm_spec_bottom_txt="+comment_comm_spec_bottom_txt,
            success: function( data ) {
                alert("enregistré");
            },
            error: function() {
                alert('Erreur');
            }
        });
    }

    function delete_art(idart){
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('admin/menu/Info_menu_commercant/delete_article'); ?>",
            data: "id_art="+idart,
            success: function(data) {
                if (data == '1'){
                    location.reload();
                }
                alert('Supprimé')
            },
            error: function() {
                return 'ko';
            }
        });
    }
    function change_value(objs){
        var is_checked = objs.checked;
        var ids = objs.value;
        if (is_checked === true){
            value_to = "1";
        }else if(is_checked === false){
            value_to = "0";
        }
        document.getElementById(ids).value =value_to;
    }

    function chenge_title_hiddden(objs){
        var ids = objs.id;
        document.getElementById("hidden_"+ids).value = objs.value;
    }

    function chenge_title(objs){
        var ids = objs.id;
        var nb_title =ids.replace('title_gli','');
        var toadd = document.getElementById("hidden_"+ids).value;
        var comments = $('#description_gli'+nb_title).val();
        document.getElementById("txt_"+ids).innerHTML = toadd;
        var is_activ = document.getElementById(ids+"_is_activ").value;
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('admin/menu/Info_menu_commercant/save_gli'); ?>",
            data: "title="+toadd+"&activ="+is_activ+"&nbgli="+ids+"&idcom="+"<?php echo $idcom; ?>"+"&comment="+comments,
            success: function(data) {
                if (data == '1'){
                    return 'ok';
                }
                // alert('Enregistré')
            },
            error: function() {
                return 'ko';
            }
        });
    }

    function save_art2(i){
        var val_title = $("#titre_art2"+i).val();
        var val_true_title = $("#true_title_art2"+i).val();
        var val_prix = $("#prix_art2"+i).val();
        var id_art = $("#id_art2"+i).val();
        console.log(id_art);
        var datas = "titre="+val_title+"&prix="+val_prix+"&idCom="+"<?php echo $idcom;?>"+"&idgli=2"+"&indexs="+i+"&id="+id_art+"&true_title="+val_true_title;
        if(val_true_title != "" && val_true_title != undefined && val_true_title != null) {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/menu/Info_menu_commercant/save_art'); ?>",
                data: datas,
                success: function (data) {
                    if ($("#img_contentss2" + i).html() === '<div class="text_label2 pt-5">Article non enregistré</div>') {
                        var obj = JSON.parse(data);
                        $("#img_contentss2" + i).html(obj.data);
                        $("#id_art2" + i).val(obj.id);

                    }
                },
                error: function () {
                    alert('Erreur');
                }
            });
        }else{
            alert("Le Champ 'Titre produit ou service' est obligatoire");
            throw new Error("champ non rempli");
        }
    }

    $(document).ready(function () {
        var all = document.getElementsByClassName('input_temp');
        if(all.length !== 0){
            for(i=0;i<all.length;i++){
                var is_checked = all[i].checked;
                var ids = all[i].value;
                if (is_checked === true){
                    value_to = "1";
                }else if(is_checked === false){
                    value_to = "0";
                }
                document.getElementById(ids).value =value_to;
            }
        }

        $("#hidden_title_gli1").val($("#title_gli1").val());
        $("#hidden_title_gli2").val($("#title_gli2").val());
        $("#hidden_title_gli3").val($("#title_gli3").val());
        $("#hidden_title_gli4").val($("#title_gli4").val());
        $("#hidden_title_gli6").val($("#title_gli6").val());
        $("#hidden_title_gli7").val($("#title_gli7").val());

        $("#add_line2").click(function () {
            var current = document.getElementsByClassName('lined2').length;
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/menu/Info_menu_commercant/add_line'); ?>",
                data: "current="+current+"&nbgliss=2",
                success: function( data ) {
                    $("#glis_content2").append(data);
                },
                error: function() {
                    alert('Erreur');
                }
            });
        });
        $("#save_all2").click(function () {
            var lines = document.getElementsByClassName('lined2');
            document.getElementsByClassName('noned_2')[0].click();
            for (i=1; i<lines.length+1;i++){
                $("#save_ind2"+i).click();
            }
            alert('Enregistré');
        })
    });
</script>



<!--gl333-->

<script type="text/javascript">

    function delete_image(id_art,id_div){
        var data = "id_art="+id_art;
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('admin/menu/Info_menu_commercant/delete_image'); ?>",
            data: data ,
            success: function(data) {
                if (data == '1') {
                    var to_open = "'<?php echo site_url() ?>media/index/"+id_art+"-soutgli1-photo"+id_div+"' , '' ,'width=1045 , height=675, scrollbars=yes'"
                    var to_ch = '<div id="Articlephoto'+id_div+'_container" onclick="javascript:window.open('+to_open+');", href="javascript:void(0);" class="w-100 img_add text-center" style="border-radius: unset!important"><img class="img-fluid" src="<?php echo base_url();?>assets/images/download-icon-png.webp" style="height: 160px"></div>';
                    $("#img_contentss"+id_div).html(to_ch);
                }
            },
            error: function() {
                alert('Erreur');
            }
        });
    }

    function delete_image_menu(id_art,id_div){
        var data = "id_art="+id_art;
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('admin/menu/Info_menu_commercant/delete_image'); ?>",
            data: data ,
            success: function(data) {
                if (data == '1') {
                    var to_open = "'<?php echo site_url() ?>media/index/"+id_art+"-menuli1-photo"+id_div+"' , '' ,'width=1045 , height=675, scrollbars=yes'"
                    var to_ch = '<div id="Articlephoto'+id_div+'_container" onclick="javascript:window.open('+to_open+');", href="javascript:void(0);" class="w-100 img_add text-center" style="border-radius: unset!important"><img class="img-fluid" src="<?php echo base_url();?>assets/images/download-icon-png.webp" style="height: 160px"></div>';
                    $("#img_contentss"+id_div).html(to_ch);
                }
            },
            error: function() {
                alert('Erreur');
            }
        });
    }
    function delete_image_menu_gen(id_com){
        var data = "idcom="+id_com;
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('admin/menu/Info_menu_commercant/delete_image_menu_gen'); ?>",
            data: data ,
            success: function(data) {
                if (data == '1') {
                    var to_open = "'<?php echo site_url() ?>media/index/"+id_com+"-menu_icon-photo_menu_gen"+""+"' , '' ,'width=1045 , height=675, scrollbars=yes'"
                    var to_ch = '<div id="Menuphoto_menu_gen_container" onclick="javascript:window.open('+to_open+');", href="javascript:void(0);" class="w-100" ><img  src="<?php echo base_url();?>assets/image/download-icon-png-5.jpg" style="height: 160px"></div>';
                    $("#img_menu_gen_contents").html(to_ch);
                }
            },
            error: function() {
                alert('Erreur');
            }
        });
    }


    function save_art3(i){
        var val_title = $("#titre_art3"+i).val();
        var val_true_title = $("#true_title_art3"+i).val();
        var val_prix = $("#prix_art3"+i).val();
        var id_art = $("#id_art3"+i).val();
        var datas = "titre="+val_title+"&prix="+val_prix+"&idCom="+"<?php echo $idcom;?>"+"&idgli=3"+"&indexs="+i+"&id="+id_art+"&true_title="+val_true_title;
        if(val_true_title != "" && val_true_title != undefined && val_true_title != null) {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/menu/Info_menu_commercant/save_art'); ?>",
                data: datas,
                success: function (data) {
                    if ($("#img_contentss3" + i).html() === '<div class="text_label2 pt-5">Article non enregistré</div>') {
                        var obj = JSON.parse(data);
                        $("#img_contentss3" + i).html(obj.data);
                        $("#id_art3" + i).val(obj.id);

                    }
                },
                error: function () {
                    alert('Erreur');
                }
            });
        }else{
            alert("Le Champ 'Titre produit ou service' est obligatoire");
            throw new Error("champ non rempli");
        }
    }

    $(document).ready(function () {
        $("#add_line3").click(function () {
            var current = document.getElementsByClassName('lined3').length;
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/menu/Info_menu_commercant/add_line'); ?>",
                data: "current="+current+"&nbgliss=3",
                success: function( data ) {
                    $("#glis_content3").append(data);
                },
                error: function() {
                    alert('Erreur');
                }
            });
        });
        $("#save_all3").click(function () {
            var lines = document.getElementsByClassName('lined3');
            document.getElementsByClassName('noned_3')[0].click();
            for (i=1; i<lines.length+1;i++){
                $("#save_ind3"+i).click();
            }
            alert('Enregistré');
        })
    });
</script>




<!--gli4-->

<script type="text/javascript">

    function save_art4(i){
        var val_title = $("#titre_art4"+i).val();
        var val_true_title = $("#true_title_art4"+i).val();
        var val_prix = $("#prix_art4"+i).val();
        var id_art = $("#id_art4"+i).val();
        var datas = "titre="+val_title+"&prix="+val_prix+"&idCom="+"<?php echo $idcom;?>"+"&idgli=4"+"&indexs="+i+"&id="+id_art+"&true_title="+val_true_title;
        if(val_true_title != "" && val_true_title != undefined && val_true_title != null) {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/menu/Info_menu_commercant/save_art'); ?>",
                data: datas,
                success: function (data) {
                    if ($("#img_contentss4" + i).html() === '<div class="text_label2 pt-5">Article non enregistré</div>') {
                        var obj = JSON.parse(data);
                        $("#img_contentss4" + i).html(obj.data);
                        $("#id_art4" + i).val(obj.id);

                    }
                },
                error: function () {
                    alert('Erreur');
                }
            });
        }else{
            alert("Le Champ 'Titre produit ou service' est obligatoire");
            throw new Error("champ non rempli");
        }
    }

    $(document).ready(function () {
        $("#add_line4").click(function () {
            var current = document.getElementsByClassName('lined4').length;
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/menu/Info_menu_commercant/add_line'); ?>",
                data: "current="+current+"&nbgliss=4",
                success: function( data ) {
                    $("#glis_content4").append(data);
                },
                error: function() {
                    alert('Erreur');
                }
            });
        });
        $("#save_all4").click(function () {
            var lines = document.getElementsByClassName('lined4');
            document.getElementsByClassName('noned_4')[0].click();
            for (i=1; i<lines.length+1;i++){
                $("#save_ind4"+i).click();
            }
            alert('Enregistré');
        })
    });
</script>

<!---------- gli5 ------------->
<script type="text/javascript">

    function save_art6(i){
        var val_title = $("#titre_art6"+i).val();
        var val_true_title = $("#true_title_art6"+i).val();
        var val_prix = $("#prix_art6"+i).val();
        var id_art = $("#id_art6"+i).val();
        var datas = "titre="+val_title+"&prix="+val_prix+"&idCom="+"<?php echo $idcom;?>"+"&idgli=6"+"&indexs="+i+"&id="+id_art+"&true_title="+val_true_title;
        if(val_true_title != "" && val_true_title != undefined && val_true_title != null) {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/menu/Info_menu_commercant/save_art'); ?>",
                data: datas,
                success: function (data) {
                    if ($("#img_contentss6" + i).html() === '<div class="text_label2 pt-5">Article non enregistré</div>') {
                        var obj = JSON.parse(data);
                        $("#img_contentss6" + i).html(obj.data);
                        $("#id_art6" + i).val(obj.id);

                    }
                },
                error: function () {
                    alert('Erreur');
                }
            });
        }else{
            alert("Le champ 'Titre produit ou service' est obligatoire");
            throw new Error("champ non rempli");
        }
    }

    $(document).ready(function () {
        $("#add_line6").click(function () {
            var current = document.getElementsByClassName('lined6').length;
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/menu/Info_menu_commercant/add_line'); ?>",
                data: "current="+current+"&nbgliss=6",
                success: function( data ) {
                    $("#glis_content6").append(data);
                },
                error: function() {
                    alert('Erreur');
                }
            });
        });
        $("#save_all6").click(function () {
            var lines = document.getElementsByClassName('lined6');
            document.getElementsByClassName('noned_6')[0].click();
            for (i=1; i<lines.length+1;i++){
                $("#save_ind6"+i).click();
            }
            alert('Enregistré');
        })
    });
</script>

<!---------- gli6 ------------->
<script type="text/javascript">

    function save_art7(i){
        var val_title = $("#titre_art7"+i).val();
        var val_true_title = $("#true_title_art7"+i).val();
        var val_prix = $("#prix_art7"+i).val();
        var id_art = $("#id_art7"+i).val();
        var datas = "titre="+val_title+"&prix="+val_prix+"&idCom="+"<?php echo $idcom;?>"+"&idgli=7"+"&indexs="+i+"&id="+id_art+"&true_title="+val_true_title;
        if(val_true_title != "" && val_true_title != undefined && val_true_title != null) {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/menu/Info_menu_commercant/save_art'); ?>",
                data: datas,
                success: function (data) {
                    if ($("#img_contentss7" + i).html() === '<div class="text_label2 pt-5">Article non enregistré</div>') {
                        var obj = JSON.parse(data);
                        $("#img_contentss7" + i).html(obj.data);
                        $("#id_art7" + i).val(obj.id);

                    }
                },
                error: function () {
                    alert('Erreur');
                }
            });
        }else{
            alert("Le champ 'Titre produit ou service' est obligatoire");
            throw new Error("champ non rempli");
        }
    }

    $(document).ready(function () {
        $("#add_line7").click(function () {
            var current = document.getElementsByClassName('lined7').length;
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/menu/Info_menu_commercant/add_line'); ?>",
                data: "current="+current+"&nbgliss=7",
                success: function( data ) {
                    $("#glis_content7").append(data);
                },
                error: function() {
                    alert('Erreur');
                }
            });
        });
        $("#save_all7").click(function () {
            var lines = document.getElementsByClassName('lined7');
            document.getElementsByClassName('noned_7')[0].click();
            for (i=1; i<lines.length+1;i++){
                $("#save_ind7"+i).click();
            }
            alert('Enregistré');
        })
    });

</script>


<script type="text/javascript">
    /******script glissiere*******/
    $("#activ_glissiere_condition").click(function (){
        if($("#activ_glissiere_condition_value").val() == 0){
            $("#is_activ_condition_child").removeClass('d-none');
            $("#is_activ_condition_child").addClass('d-flex');
            $("#activ_glissiere_condition_up").removeClass('d-none');
            $("#activ_glissiere_condition_up").addClass('d-block');
            $("#activ_glissiere_condition_down").removeClass('d-block');
            $("#activ_glissiere_condition_down").addClass('d-none');
            $("#activ_glissiere_condition_value").val('1');
        }
        else{
            $("#is_activ_condition_child").removeClass('d-flex');
            $("#is_activ_condition_child").addClass('d-none');
            $("#activ_glissiere_condition_down").removeClass('d-none');
            $("#activ_glissiere_condition_down").addClass('d-block');
            $("#activ_glissiere_condition_up").removeClass('d-block');
            $("#activ_glissiere_condition_up").addClass('d-none');
            $("#activ_glissiere_condition_value").val('0');
        }
    })
    $("#activ_glissiere_emporter").click(function (){
        if($("#activ_glissiere_emporter_value").val() == 0){
            $("#is_activ_emp_child").removeClass('d-none');
            $("#is_activ_emp_child").addClass('d-block');
            $("#activ_glissiere_emporter_up").removeClass('d-none');
            $("#activ_glissiere_emporter_up").addClass('d-block');
            $("#activ_glissiere_emporter_down").removeClass('d-block');
            $("#activ_glissiere_emporter_down").addClass('d-none');
            $("#activ_glissiere_emporter_value").val('1');
        }
        else{
            $("#is_activ_emp_child").removeClass('d-block');
            $("#is_activ_emp_child").addClass('d-none');
            $("#activ_glissiere_emporter_down").removeClass('d-none');
            $("#activ_glissiere_emporter_down").addClass('d-block');
            $("#activ_glissiere_emporter_up").removeClass('d-block');
            $("#activ_glissiere_emporter_up").addClass('d-none');
            $("#activ_glissiere_emporter_value").val('0');
        }
    })
    $("#activ_glissiere_livraison").click(function (){
        if($("#activ_glissiere_livraison_value").val() == 0){
            $(".activ_glissiere_livraison_child").removeClass('d-none');
            $(".activ_glissiere_livraison_child").addClass('d-flex');
            $(".activ_glissiere_livraison_child1").removeClass('d-none');
            $(".activ_glissiere_livraison_child1").addClass('d-block');
            $("#activ_glissiere_livraison_up").removeClass('d-none');
            $("#activ_glissiere_livraison_up").addClass('d-block');
            $("#activ_glissiere_livraison_down").removeClass('d-block');
            $("#activ_glissiere_livraison_down").addClass('d-none');
            $("#activ_glissiere_livraison_value").val('1');
        }
        else{
            $(".activ_glissiere_livraison_child").removeClass('d-flex');
            $(".activ_glissiere_livraison_child").addClass('d-none');
            $(".activ_glissiere_livraison_child1").removeClass('d-block');
            $(".activ_glissiere_livraison_child1").addClass('d-none');
            $("#activ_glissiere_livraison_down").removeClass('d-none');
            $("#activ_glissiere_livraison_down").addClass('d-block');
            $("#activ_glissiere_livraison_up").removeClass('d-block');
            $("#activ_glissiere_livraison_up").addClass('d-none');
            $("#activ_glissiere_livraison_value").val('0');
        }
    })
    $("#activ_glissiere_help").click(function (){
        if($("#activ_glissiere_help_value").val() == 0){
            $(".activ_glissiere_help_child").removeClass('d-none');
            $(".activ_glissiere_help_child").addClass('d-flex');
            $("#activ_glissiere_help_up").removeClass('d-none');
            $("#activ_glissiere_help_up").addClass('d-block');
            $("#activ_glissiere_help_down").removeClass('d-block');
            $("#activ_glissiere_help_down").addClass('d-none');
            $("#activ_glissiere_help_value").val('1');
        }
        else{
            $(".activ_glissiere_help_child").removeClass('d-flex');
            $(".activ_glissiere_help_child").addClass('d-none');
            $("#activ_glissiere_help_down").removeClass('d-none');
            $("#activ_glissiere_help_down").addClass('d-block');
            $("#activ_glissiere_help_up").removeClass('d-block');
            $("#activ_glissiere_help_up").addClass('d-none');
            $("#activ_glissiere_help_value").val('0');
        }
    })
    $("#activ_glissiere_differe").click(function (){
        if($("#activ_glissiere_differe_value").val() == 0){
            $(".activ_glissiere_differe_child").removeClass('d-none');
            $(".activ_glissiere_differe_child").addClass('d-flex');
            $("#activ_glissiere_differe_up").removeClass('d-none');
            $("#activ_glissiere_differe_up").addClass('d-block');
            $("#activ_glissiere_differe_down").removeClass('d-block');
            $("#activ_glissiere_differe_down").addClass('d-none');
            $("#activ_glissiere_differe_value").val('1');
        }
        else{
            $(".activ_glissiere_differe_child").removeClass('d-flex');
            $(".activ_glissiere_differe_child").addClass('d-none');
            $("#activ_glissiere_differe_down").removeClass('d-none');
            $("#activ_glissiere_differe_down").addClass('d-block');
            $("#activ_glissiere_differe_up").removeClass('d-block');
            $("#activ_glissiere_differe_up").addClass('d-none');
            $("#activ_glissiere_differe_value").val('0');
        }
    })
    $("#activ_glissiere_prom").click(function (){
        if($("#activ_glissiere_prom_value").val() == 0){
            $(".activ_glissiere_prom_child").removeClass('d-none');
            $(".activ_glissiere_prom_child").addClass('d-flex');
            $("#activ_glissiere_prom_up").removeClass('d-none');
            $("#activ_glissiere_prom_up").addClass('d-block');
            $("#activ_glissiere_prom_down").removeClass('d-block');
            $("#activ_glissiere_prom_down").addClass('d-none');
            $("#activ_glissiere_prom_value").val('1');
        }
        else{
            $(".activ_glissiere_prom_child").removeClass('d-flex');
            $(".activ_glissiere_prom_child").addClass('d-none');
            $("#activ_glissiere_prom_down").removeClass('d-none');
            $("#activ_glissiere_prom_down").addClass('d-block');
            $("#activ_glissiere_prom_up").removeClass('d-block');
            $("#activ_glissiere_prom_up").addClass('d-none');
            $("#activ_glissiere_prom_value").val('0');
        }
    })
    $("#activ_glissiere_activ1").click(function (){
        if($("#activ_glissiere_activ1_value").val() == 0){
            $("#activ_glissiere_activ1_content").removeClass('d-none');
            $("#activ_glissiere_activ1_content").addClass('d-block');
            $("#activ_glissiere_activ1_up").removeClass('d-none');
            $("#activ_glissiere_activ1_up").addClass('d-block');
            $("#activ_glissiere_activ1_down").removeClass('d-block');
            $("#activ_glissiere_activ1_down").addClass('d-none');
            $("#activ_glissiere_activ1_value").val('1');
        }
        else{
            $("#activ_glissiere_activ1_content").removeClass('d-block');
            $("#activ_glissiere_activ1_content").addClass('d-none');
            $("#activ_glissiere_activ1_down").removeClass('d-none');
            $("#activ_glissiere_activ1_down").addClass('d-block');
            $("#activ_glissiere_activ1_up").removeClass('d-block');
            $("#activ_glissiere_activ1_up").addClass('d-none');
            $("#activ_glissiere_activ1_value").val('0');
        }
    })
    $("#activ_glissiere_activ2").click(function (){
        if($("#activ_glissiere_activ2_value").val() == 0){
            $("#activ_glissiere_activ2_content").removeClass('d-none');
            $("#activ_glissiere_activ2_content").addClass('d-block');
            $("#activ_glissiere_activ2_up").removeClass('d-none');
            $("#activ_glissiere_activ2_up").addClass('d-block');
            $("#activ_glissiere_activ2_down").removeClass('d-block');
            $("#activ_glissiere_activ2_down").addClass('d-none');
            $("#activ_glissiere_activ2_value").val('1');
        }
        else{
            $("#activ_glissiere_activ2_content").removeClass('d-block');
            $("#activ_glissiere_activ2_content").addClass('d-none');
            $("#activ_glissiere_activ2_down").removeClass('d-none');
            $("#activ_glissiere_activ2_down").addClass('d-block');
            $("#activ_glissiere_activ2_up").removeClass('d-block');
            $("#activ_glissiere_activ2_up").addClass('d-none');
            $("#activ_glissiere_activ2_value").val('0');
        }
    })
    $("#activ_glissiere_activ3").click(function (){
        if($("#activ_glissiere_activ3_value").val() == 0){
            $("#activ_glissiere_activ3_content").removeClass('d-none');
            $("#activ_glissiere_activ3_content").addClass('d-block');
            $("#activ_glissiere_activ3_up").removeClass('d-none');
            $("#activ_glissiere_activ3_up").addClass('d-block');
            $("#activ_glissiere_activ3_down").removeClass('d-block');
            $("#activ_glissiere_activ3_down").addClass('d-none');
            $("#activ_glissiere_activ3_value").val('1');
        }
        else{
            $("#activ_glissiere_activ3_content").removeClass('d-block');
            $("#activ_glissiere_activ3_content").addClass('d-none');
            $("#activ_glissiere_activ3_down").removeClass('d-none');
            $("#activ_glissiere_activ3_down").addClass('d-block');
            $("#activ_glissiere_activ3_up").removeClass('d-block');
            $("#activ_glissiere_activ3_up").addClass('d-none');
            $("#activ_glissiere_activ3_value").val('0');
        }
    })
    $("#activ_glissiere_activ4").click(function (){
        if($("#activ_glissiere_activ4_value").val() == 0){
            $("#activ_glissiere_activ4_content").removeClass('d-none');
            $("#activ_glissiere_activ4_content").addClass('d-block');
            $("#activ_glissiere_activ4_up").removeClass('d-none');
            $("#activ_glissiere_activ4_up").addClass('d-block');
            $("#activ_glissiere_activ4_down").removeClass('d-block');
            $("#activ_glissiere_activ4_down").addClass('d-none');
            $("#activ_glissiere_activ4_value").val('1');
        }
        else{
            $("#activ_glissiere_activ4_content").removeClass('d-block');
            $("#activ_glissiere_activ4_content").addClass('d-none');
            $("#activ_glissiere_activ4_down").removeClass('d-none');
            $("#activ_glissiere_activ4_down").addClass('d-block');
            $("#activ_glissiere_activ4_up").removeClass('d-block');
            $("#activ_glissiere_activ4_up").addClass('d-none');
            $("#activ_glissiere_activ4_value").val('0');
        }
    })
    $("#activ_glissiere_activ5").click(function (){
        if($("#activ_glissiere_activ5_value").val() == 0){
            $("#activ_glissiere_activ5_content").removeClass('d-none');
            $("#activ_glissiere_activ5_content").addClass('d-block');
            $("#activ_glissiere_activ5_up").removeClass('d-none');
            $("#activ_glissiere_activ5_up").addClass('d-block');
            $("#activ_glissiere_activ5_down").removeClass('d-block');
            $("#activ_glissiere_activ5_down").addClass('d-none');
            $("#activ_glissiere_activ5_value").val('1');
        }
        else{
            $("#activ_glissiere_activ5_content").removeClass('d-block');
            $("#activ_glissiere_activ5_content").addClass('d-none');
            $("#activ_glissiere_activ5_down").removeClass('d-none');
            $("#activ_glissiere_activ5_down").addClass('d-block');
            $("#activ_glissiere_activ5_up").removeClass('d-block');
            $("#activ_glissiere_activ5_up").addClass('d-none');
            $("#activ_glissiere_activ5_value").val('0');
        }
    })
    $("#activ_glissiere_activ6").click(function (){
        if($("#activ_glissiere_activ6_value").val() == 0){
            $("#activ_glissiere_activ6_content").removeClass('d-none');
            $("#activ_glissiere_activ6_content").addClass('d-block');
            $("#activ_glissiere_activ6_up").removeClass('d-none');
            $("#activ_glissiere_activ6_up").addClass('d-block');
            $("#activ_glissiere_activ6_down").removeClass('d-block');
            $("#activ_glissiere_activ6_down").addClass('d-none');
            $("#activ_glissiere_activ6_value").val('1');
        }
        else{
            $("#activ_glissiere_activ6_content").removeClass('d-block');
            $("#activ_glissiere_activ6_content").addClass('d-none');
            $("#activ_glissiere_activ6_down").removeClass('d-none');
            $("#activ_glissiere_activ6_down").addClass('d-block');
            $("#activ_glissiere_activ6_up").removeClass('d-block');
            $("#activ_glissiere_activ6_up").addClass('d-none');
            $("#activ_glissiere_activ6_value").val('0');
        }
    })
    $("#activ_glissiere_activ7").click(function (){
        if($("#activ_glissiere_activ7_value").val() == 0){
            $("#activ_glissiere_activ7_content").removeClass('d-none');
            $("#activ_glissiere_activ7_content").addClass('d-block');
            $("#activ_glissiere_activ7_up").removeClass('d-none');
            $("#activ_glissiere_activ7_up").addClass('d-block');
            $("#activ_glissiere_activ7_down").removeClass('d-block');
            $("#activ_glissiere_activ7_down").addClass('d-none');
            $("#activ_glissiere_activ7_value").val('1');
        }
        else{
            $("#activ_glissiere_activ7_content").removeClass('d-block');
            $("#activ_glissiere_activ7_content").addClass('d-none');
            $("#activ_glissiere_activ7_down").removeClass('d-none');
            $("#activ_glissiere_activ7_down").addClass('d-block');
            $("#activ_glissiere_activ7_up").removeClass('d-block');
            $("#activ_glissiere_activ7_up").addClass('d-none');
            $("#activ_glissiere_activ7_value").val('0');
        }
    })
</script>