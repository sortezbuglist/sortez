<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=utf-8" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url('application/views/admin/menu/css/style.css')?>" type="text/css" />
</head>
<body>
<form id="form_all_data" method="post" enctype="multipart/form-data" action="<?php echo site_url();?>admin/menu/Info_menu_commercant/save_info_menu_commercant">

    <input type="hidden" id="activ_glissiere_emporter_value" value="1">
    <input type="hidden" id="activ_glissiere_condition_value" value="0">
    <input type="hidden" id="activ_glissiere_livraison_value" value="0">
    <input type="hidden" id="activ_glissiere_activ1_value" value="0">
    <input type="hidden" id="activ_glissiere_help_value" value="1">
    <input type="hidden" id="activ_glissiere_differe_value" value="1">
    <input type="hidden" id="activ_glissiere_prom_value" value="1">
    <input type="hidden" id="activ_glissiere_activ2_value" value="0">
    <input type="hidden" id="activ_glissiere_activ3_value" value="0">
    <input type="hidden" id="activ_glissiere_activ4_value" value="0">
    <input type="hidden" id="activ_glissiere_activ5_value" value="0">
    <input type="hidden" id="activ_glissiere_activ6_value" value="0">
    <input type="hidden" id="activ_glissiere_activ7_value" value="0">
    <input type="hidden" name="data[id]" value="<?php if (isset($data_menu->id)){echo $data_menu->id;}else{echo "0";} ?>" />

    <style>
        .disabledbutton {
            pointer-events: none!important;
            opacity: 0.4!important;
        }
    </style>
    <div class="container cont_all">
        <div class="row pt-5">
            <div class="col-12 text-center">
                <a onclick="javascript:void(0)" href="<?php echo site_url('admin'); ?>">
                    <div class="btn btn-secondary">Menu admin</div>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-12 pt-5 text-center">
                <div class="w-50 text-center m-auto">
<!--                    <div class="row">-->
<!--                        <div class="col-12 text-center pb-3 lab">-->
<!--                            Titre du menu "Nos menus et cartes"-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="row">-->
<!--                        <div class="col-9 pr-0">-->
<!--                        </div>-->
<!--                        <div class="col-3 pl-0">-->
<!--                            <button class="btn btn-secondary w-100">Enregistrez</button>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="row">-->
<!--                        <div class="col-12 pt-3 text-center lab">-->
<!--                            " Nos menus et cartes " est affiché si le titre du menu est vide-->
<!--                        </div>-->
<!--                    </div>-->
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-12 pt-5 pb-4 text-center titre_big">
                CREATION D'UN FORMULAIRE DE VENTE EN LIGNE <br>
                "A EMPORTER" et "LIVRAISON A DOMICILE"
            </div>
            <div class="col-12 text-center">
                <div class="w-50 m-auto">
                    <ol class="font_7" style="color:rgb(0, 0, 0); font-family:futura-lt-w01-book,sans-serif; font-size:14px; font-style:normal; font-weight:400;">
                        <li>
                            <p class="font_7"><span style="font-style:normal;"><span style="font-weight:400;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Choisissez les glissières à remplir et activez les ;</span></span></span></span></span></p>
                        </li>
                        <li>
                            <p class="font_7"><span style="font-style:normal;"><span style="font-weight:400;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Choisissez et remplissez les champs préconfigurés ;</span></span></span></span></span></p>
                        </li>
                        <li>
                            <p class="font_7"><span style="font-style:normal;"><span style="font-weight:400;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:futura-lt-w01-book,sans-serif;">N'oubliez pas&nbsp;de valider chaque dépôt pour chaque glissière avec le bouton "Validation" ;</span></span></span></span></span></p>
                        </li>
                        <li>
                            <p class="font_7"><span style="font-style:normal;"><span style="font-weight:400;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Une fois que vous avez intégrer la totalité de vos données, vous pouvez visualiser le formulaire de menu en ligne des consommateurs ;</span></span></span></span></span></p>
                        </li>
                        <li>
                            <p class="font_7"><span style="font-style:normal;"><span style="font-weight:400;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Soit il est conforme et définitif ou revenez sur ce formulaire pour y apporter les modifications qui s'imposent.</span></span></span></span></span></p>
                        </li>
                    </ol>
                </div>
            </div>
        </div>

<!--        --><?php //$this->load->view('admin/menu/includes/vente_emport'); ?>
<!---->
<!--        --><?php //$this->load->view('admin/menu/includes/vente_livraison'); ?>
<!---->
<!--        --><?php //$this->load->view('admin/menu/includes/vente_differe'); ?>
<!---->
<!--        --><?php //$this->load->view('admin/menu/includes/conditions_generale'); ?>


        <div class="row glissiere_tab livraison_row">
            <div class="col-4">
                <div class="text-left titre_gli_up">AIDE TEXTES & IMAGES</div>
            </div>
            <div class="col-4" id="activ_glissiere_help">
                <div class="cercle">
                    <div class="text-center up d-block" id="activ_glissiere_help_up"><p>fermer</p></div>
                    <div class="text-center d-none down" id="activ_glissiere_help_down"><p>ouvrir</p></div>
                </div>
            </div>
            <div class="col-4 d-flex">
                <div class="text-right w-75 act_desact_text">Activer/désactiver</div>
                <div class="text-right w-25 titre_gli_up">
                    <label class="switch">
                        <input id="is_activ_help_img" type="checkbox" <?php if (isset($data_menu->is_activ_help_img) AND $data_menu->is_activ_help_img == '1') echo 'checked'; ?> >
                        <span class="slider round"></span>
                    </label>
                    <input name="data[is_activ_help_img]" id="is_activ_help_img_hidden" type="hidden" value="<?php if (isset($data_menu->is_activ_help_img)) echo $data_menu->is_activ_help_img; ?>">
                </div>
            </div>
        </div>

        <div class="row activ_glissiere_help_child gli_content ">
            <div class="col-3 pt-4">
                <div class="w-100 pt-4 text-center spec">
                    <label class="switch">
                        <input type="checkbox" disabled checked >
                        <span class="slider round"></span>
                    </label>
                </div>
                <div class="w-100 pt-4 h5 text-center ">
                    Titre,<br>
                    Textes & prix
                </div>
                <div class="w-100 pt-3">
                    <div id="fake_button" class="btn btn-secondary text-center w-100">Valider</div>
                </div>
            </div>
            <div class="col-9 pt-4 pl-0">
                <p class="font_7" style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;"><span style="font-weight:bold;">L'OUVERTURE DES GLISSIERES ET LE TRAITEMENT DES TEXTES&#8203;</span></span></span></p>
                <ol class="font_7 ml-4" style="color:rgb(0, 0, 0); font-family:futura-lt-w01-book,sans-serif; font-size:15px;">
                    <li>
                        <p class="font_7" style="font-size:15px;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Activez&nbsp;chaque glissière en poussant le bouton rouge qui deviendra vert ;</span></span></span></p>
                    </li>
                    <li>
                        <p class="font_7" style="font-size:15px;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Ouvrez une glissière ;&nbsp;</span></span></span></p>
                    </li>
                    <li>
                        <p class="font_7" style="font-size:15px;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Définissez le titre de la&nbsp;catégorie ce cette glissière ;</span></span></span></p>
                    </li>
                    <li>
                        <p class="font_7" style="font-size:15px;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Cliquez sur le bouton "Validation", l'icone ci-contre de l'image apparaît ;</span></span></span></p>
                    </li>
                    <li>
                        <p class="font_7" style="font-size:15px;"><span style="color:#000000;"><span style="font-size:15px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Intégrez le titre de votre produit ou service ;</span></span></span></p>
                    </li>
                    <li>
                        <p class="font_7" style="font-size:15px;"><span style="color:#000000;"><span style="font-size:15px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Apportez plus de précisions sur chaque produit avec le champ "Désignation" ;</span></span></span></p>
                    </li>
                    <li>
                        <p class="font_7" style="font-size:15px;"><span style="color:#000000;"><span style="font-size:15px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Précisez le prix unitaire ;</span></span></span></p>
                    </li>
                    <li>
                        <p class="font_7" style="font-size:15px;"><span style="color:#000000;"><span style="font-size:15px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Ajouter un&nbsp;ou plusieurs produits complémentaires en cliquant sur le bouton vert +.</span></span></span></p>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row activ_glissiere_help_child gli_content" id="activ_glissiere_help">
            <div class="col-3 pt-4">
                <div class="w-100 mt-5 pt-3 pb-3  text-center" style="background-color: #CCCCCC">
                    <img src="<?php echo base_url()?>assets/images/download-icon-png-5.webp">
                </div>
            </div>
            <div class="col-9 pt-4 pl-0">
                <p class="font_7" style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;"><span style="font-weight:bold;">L'INTEGRATION IMMEDIATE DE VOS IMAGES&#8203;</span></span></span></p>
                <ol class="font_7 ml-4" style="color:rgb(0, 0, 0); font-family:futura-lt-w01-book,sans-serif; font-size:15px;">
                    <li>
                        <p class="font_7" style="font-size:15px;"><span style="color:#000000;"><span style="font-size:15px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Pour intégrer les images, cliquez sur les icones ;</span></span></span></p>
                    </li>
                    <li>
                        <p class="font_7" style="font-size:15px;"><span style="color:#000000;"><span style="font-size:15px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Vous accédez à une galerie, vous cliquez sur le bouton "Choisir un fichier" ;</span></span></span></p>
                    </li>
                    <li>
                        <p class="font_7" style="font-size:15px;"><span style="color:#000000;"><span style="font-size:15px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Et vous intégrez la photo choisie sur cette galerie (maximum 500 Ko)&nbsp;</span></span></span></p>
                    </li>
                    <li>
                        <p class="font_7" style="font-size:15px;"><span style="color:#000000;"><span style="font-size:15px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Sur la galerie, sélectionnez cette photo en cliquant dessus puis appuyez sur le bouton "Attribuer" ;</span></span></span></p>
                    </li>
                    <li>
                        <p class="font_7" style="font-size:15px;"><span style="color:#000000;"><span style="font-size:15px;"><span style="font-family:futura-lt-w01-book,sans-serif;">L'image s'intègre à votre bloc.</span></span></span></p>
                    </li>
                    <li>
                        <p class="font_7" style="font-size:15px;"><span style="color:#000000;"><span style="font-size:15px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Si votre fichier est trop&nbsp;important ou si le résultatde cadrage ne vous convient pas, nous vous invitons&nbsp;à modifier votre image sur l'éditeur de photo en ligne (voir ci-dessous)</span></span></span></p>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row pb-4 activ_glissiere_help_child gli_content">
            <div class="col-3 pt-4">
                <div class="w-100 mt-5 pt-3 pb-3  text-center" style="background-color: #EAF8FF">
                    <a href="https://fotoflexer.com/editor/" target="_blank"><img style="width: 30%;height: auto" src="<?php echo base_url()?>assets/images/IMAGE45.webp"></a>
                </div>
            </div>
            <div class="col-9 pt-4 pl-0">
                <p class="font_7" style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;"><span style="font-weight:bold;">L'EDITEUR DE PHOTO EN LIGNE&#8203;</span></span></span></p>
                <ol class="font_7 ml-4" style="color:rgb(0, 0, 0); font-family:futura-lt-w01-book,sans-serif; font-size:15px;">
                    <li>
                        <p class="font_7" style="font-size:15px;"><span style="color:#000000;"><span style="font-size:15px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Pour accédez à cet éditeur, cliquez sur les icones présentes identiques à celle de droite dans les glissières produits.</span></span></span></p>
                    </li>
                    <li>
                        <p class="font_7" style="font-size:15px;"><span style="color:#000000;"><span style="font-size:15px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Importer et intégrez la photo en cliquant sur le bouton <span style="font-weight:bold;">"OPEN PHOTO"</span></span></span></span></p>
                    </li>
                    <li>
                        <p class="font_7" style="font-size:15px;"><span style="color:#000000;"><span style="font-size:15px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Recadrer la photo en cliquant sur le bouton <span style="font-weight:bold;">"CROP"</span>, appliquez le format 16/9 et validez avec le bouton <span style="font-weight:bold;">"APPLY"</span>, puis appuyez sur le bouton <span style="font-weight:bold;">"SAVE"</span> présent&nbsp;en haut à gauche, précisez le nouveau nom de votre fichier, et valider définitivement&nbsp;votre image, cette cernière sera enregistrée directement sur le fichier de téléchargement de votre ordinateur.</span></span></span></p>
                    </li>
                    <li>
                        <p class="font_7" style="font-size:15px;"><span style="color:#000000;"><span style="font-size:15px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Redimensionner la photo en cliquant sur le bouton <span style="font-weight:bold;">"RESIZE"</span>, modifiez le poids de votre image&nbsp;et validez avec le bouton <span style="font-weight:bold;">"APPLY"</span>, puis appuyez sur le bouton <span style="font-weight:bold;">"SAVE"</span> présent&nbsp;en haut à gauche, précisez le nouveau nom de votre fichier, et valid</span></span></span><span style="color:#000000;"><span style="font-size:15px;"><span style="font-family:futura-lt-w01-book,sans-serif;">er défin</span></span></span><span style="color:#000000;"><span style="font-size:15px;"><span style="font-family:futura-lt-w01-book,sans-serif;">iti</span></span></span><span style="color:#000000;"><span style="font-size:15px;"><span style="font-family:futura-lt-w01-book,sans-serif;">vement</span></span></span><span style="color:#000000;"><span style="font-size:15px;"><span style="font-family:futura-lt-w01-book,sans-serif;">&nbsp;votre image, cette dernière sera enregistrée directement sur le fichier de téléchargement de votre ordinateur.</span></span></span></p>
                    </li>
                    <li>
                        <p class="font_7" style="font-size:15px;"><span style="color:#000000;"><span style="font-size:15px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Intégrez ces images en reprenant le processus précédant.</span></span></span></p>
                    </li>
                </ol>
            </div>
        </div>
    </div>
</form>

<div class="container cont_all mb-5">
    <?php $this->load->view('admin/menu/gli_content/gli1') ?>
    <?php $this->load->view('admin/menu/gli_content/gli2') ?>
    <?php $this->load->view('admin/menu/gli_content/gli3') ?>
    <?php $this->load->view('admin/menu/gli_content/gli4') ?>
    <?php $this->load->view('admin/menu/gli_content/gli6') ?>
    <?php $this->load->view('admin/menu/gli_content/gli7') ?>




    <?php $this->load->view('admin/menu/includes/action_promo'); ?>

    <?php $this->load->view('admin/menu/includes/menu_generale'); ?>
    <?php $this->load->view('admin/menu/includes/qr_code_menu'); ?>
    <?php $this->load->view('admin/menu/includes/url_menu_bottom'); ?>
    <div class="row mt-0 mb-5">
<!--        <div class="shadowed col-12 text-center pt-2 pb-4 " >-->
<!--            <button onclick="save_paypal_data()" class="btn btn-secondary w-50" style="border-radius: unset!important">-->
<!--                Validation-->
<!--            </button>-->
<!--        </div>-->
        <div class="col-6 offset-lg-3 text-center pt-2 pb-2 mb-5" >
            <a href="<?php echo site_url(); ?><?php echo $infocom->nom_url; ?>/menus_commercants" target="_blank">
                <img src="https://www.randawilly.ovh/application/resources/privicarte/images/visualisation.png" alt="logo" title="" style="width: 180px">
            </a>
        </div>
    </div>
<!--    --><?php //$this->load->view('admin/menu/includes/commande_specif'); ?>

</div>


<input type="hidden" id="i" value="0">
<?php $this->load->view('admin/menu/js/script') ?>
</body>
</html>



