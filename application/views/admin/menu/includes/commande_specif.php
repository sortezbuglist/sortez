<div class="row glissiere_tab">
    <div class="row w-100">
        <div id="txt_title_gli1" class="txt_title_gli1_ppl text-center col-8">
            <div class="">TELECHARGEMENT D'UNE COMMANDE SPECIFIQUE</div>
        </div>
        <div class="col-3">
            <div class="text-right act_desact_text">Activer/désactiver</div>
        </div>
        <div class="text-right col-1 titre_gli_up">
            <label class="switch">
                <input id="is_activ_com_spec" type="checkbox" <?php if (isset($data_menu->is_activ_com_spec) AND $data_menu->is_activ_com_spec == '1') echo 'checked'; ?> >
                <span class="slider round"></span>
            </label>
            <input name="data[is_activ_com_spec]" id="is_activ_com_spec_hidden" type="hidden" value="<?php if (isset($data_menu->is_activ_com_spec)) echo $data_menu->is_activ_com_spec; ?>">
        </div>
    </div>
</div>
<div class="row gli_content">
    <div class="col-12">
        <p class="pt-4 lab_com">
            Avec ce module, le consommateur peut télécharger directement sur le formulaire de commande en ligne un document (commande, note, ordonnance...).  A vous de préciser vos conditions de livraison, d'enlèvement et de paiements.
            Dès validation de sa part, vous recevez les données par mail.
        </p>
    </div>
    <div class="col-3 pt-4 text-center">
        <img class="" src="<?php echo base_url() ?>assets/images/coloriage-boutique-13.webp" />
        <div class="w-100 text-center switch_term_bott">
            <label class="switch_out">
                <input id="is_activ_comm_enlev" type="checkbox" <?php if (isset($data_menu->is_activ_comm_enlev) AND $data_menu->is_activ_comm_enlev == '1') echo 'checked'; ?> >
                <span class="slider_out round_out"></span>
            </label>
            <input id="is_activ_comm_enlev_hidden" type="hidden" name="data[is_activ_comm_enlev]" value="<?php if (isset($data_menu->is_activ_comm_enlev)) echo $data_menu->is_activ_comm_enlev; ?>">
        </div>
        <div class="w-100 text-center text_label">Enlèvement*</div>
    </div>
    <div class="col-3 text-center">
        <img class="" src="<?php echo base_url() ?>assets/images/icone_livraison.webp" />
        <div class="w-100 text-center switch_term_bott">
            <label class="switch_out">
                <input id="is_activ_comm_livr_dom" type="checkbox" <?php if (isset($data_menu->is_activ_comm_livr_dom) AND $data_menu->is_activ_comm_livr_dom == '1') echo 'checked'; ?> >
                <span class="slider_out round_out"></span>
            </label>
            <input id="is_activ_comm_livr_dom_hidden" type="hidden" name="data[is_activ_comm_livr_dom]" value="<?php if (isset($data_menu->is_activ_comm_livr_dom)) echo $data_menu->is_activ_comm_livr_dom; ?>">
        </div>
        <div class="w-100 text-center text_label">Livraison à domicle*</div>
    </div>
    <div class="col-3 text-center">
        <img src="<?php echo base_url()?>assets/images/zette1.webp">
        <div class="w-100 text-center switch_term_bott">
            <label class="switch_out">
                <input id="is_activ_card_bank_bottom" type="checkbox" <?php if (isset($data_menu->is_activ_card_bank_bottom) AND $data_menu->is_activ_card_bank_bottom == '1') echo 'checked'; ?> >
                <span class="slider_out round_out"></span>
            </label>
            <input id="is_activ_card_bank_bottom_hidden" type="hidden" name="data[is_activ_card_bank_bottom]" value="<?php if (isset($data_menu->is_activ_card_bank_bottom)) echo $data_menu->is_activ_card_bank_bottom; ?>">
        </div>
        <div class="w-100 text-center text_label">Carte bancaire*</div>
    </div>
    <div class="col-3 text-center">
        <img src="<?php echo base_url()?>assets/images/cheque_1.webp">
        <div class="w-100 pt-4 text-center switch_term_bott">
            <label class="switch_out mt-1">
                <input id="is_activ_cheque_bottom" type="checkbox" <?php if (isset($data_menu->is_activ_cheque_bottom) AND $data_menu->is_activ_cheque_bottom == '1') echo 'checked'; ?> >
                <span class="slider_out round_out"></span>
            </label>
            <input id="is_activ_cheque_bottom_hidden" type="hidden" name="data[is_activ_cheque_bottom]" value="<?php if (isset($data_menu->is_activ_cheque_bottom)) echo $data_menu->is_activ_cheque_bottom; ?>">
        </div>
        <div class="w-100 text-center text_label">Carte ou chèque*</div>
    </div>
    <div class="col-12 pt-3 d-flex text-center">
        <div class="w-50 pt-2 pr-2 text-right">
            <span class="text_label" >Commentaires par défaut*</span>
        </div>
        <div class="w-50 text-left">
            <label class="switch_out">
                <input id="is_activ_comment_bottom" type="checkbox" <?php if (isset($data_menu->is_activ_comment_bottom) AND $data_menu->is_activ_comment_bottom == '1') echo 'checked'; ?> >
                <span class="slider_out round_out"></span>
            </label>
            <input id="is_activ_comment_bottom_hidden" type="hidden" name="data[is_activ_comment_bottom]" value="<?php if (isset($data_menu->is_activ_comment_bottom)) echo $data_menu->is_activ_comment_bottom; ?>">
        </div>
    </div>
    <div class="col-12">
        <textarea id="comment_comm_spec_bottom_txt" class="form-control textarea_style w-100" name="data[comment_comm_spec_bottom_txt]" placeholder=""><?php if (isset($data_menu->comment_comm_spec_bottom_txt)) echo $data_menu->comment_comm_spec_bottom_txt; ?></textarea>
    </div>
    <div class="col-12 text-center pt-3 text_label">
        * Activer ou désactiver les champs
    </div>
</div>
<div class="row mt-0 mb-5">
    <div class="shadowed col-12 text-center pt-2 pb-4 " >
        <button onclick="save_paypal_data()" class="btn btn-secondary w-50" style="border-radius: unset!important">
            Validation
        </button>
    </div>
    <div class="col-6 offset-lg-3 text-center pt-2 pb-2 mb-5" >
        <a href="<?php echo site_url(); ?><?php echo $infocom->nom_url; ?>/menus_commercants" target="_blank">
            <img src="https://www.randawilly.ovh/application/resources/privicarte/images/visualisation.png" alt="logo" title="" style="width: 180px">
        </a>
    </div>
</div>
