<div class="row glissiere_tab livraison_row">
    <div class="col-5">
        <div class="text-left titre_gli_up gli_menu_gen">GENEREZ LE QRCODE DE VOTRE PAGE MENU</div>
    </div>
    <div class="col-2" id="activ_glissiere_prom">
        <div class="cercle">
            <div class="text-center up d-block" id="activ_glissiere_prom_up"><p>fermer</p></div>
            <div class="text-center down d-none" id="activ_glissiere_prom_down"><p>ouvrir</p></div>
        </div>
    </div>
    <!--    <div class="col-5 d-flex">-->
    <!--        <div class="text-right w-75 act_desact_text">Activer/désactiver</div>-->
    <!--        <div class="text-right w-25 titre_gli_up">-->
    <!--            <label class="switch">-->
    <!--                <input id="is_activ_prom" type="checkbox" --><?php //if (isset($data_menu->is_activ_prom) AND $data_menu->is_activ_prom == '1') echo 'checked'; ?><!-- >-->
    <!--                <span class="slider round"></span>-->
    <!--            </label>-->
    <!--        </div>-->
    <!--    </div>-->
</div>
<form method="post" action="<?php echo site_url();?>admin/menu/Info_menu_commercant/generate_qr_code" class="row pb-5 shadowed p-2 gli_content d-flex">
    <input type="hidden" name="id_menu_com" value="<?php if (isset($data_menu->id)){echo $data_menu->id;}else{echo "0";} ?>" />
    <div class="col-4 text-center">
        <div class="w-100 text-center top_menu_gen_qr">
            <?php if (isset($data_menu) AND !empty($data_menu)){ ?>
            <button class="btn btn-success btn_generate_qr_codr"><?php if (isset($data_menu->url_qr_code_menu) AND is_file("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/menu_qr_code/".$data_menu->url_qr_code_menu)){echo "Regénérer le QRCODE";}else{echo 'Générer le QRCODE';} ?></button>
            <?php }elseif(empty($data_menu)){ ?>
                <p style="color: red">
                    Veuillez enregistrer vos données avant de générer votre Qr code !! !
                </p>
            <?php } ?>
        </div>
    </div>

    <div class="col-4">
        <div class="w-100 text-center">
            <?php if (isset($data_menu->url_qr_code_menu) AND is_file("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/menu_qr_code/".$data_menu->url_qr_code_menu)){ ?>
                <img src="<?php echo base_url("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/menu_qr_code/".$data_menu->url_qr_code_menu)?>">
            <?php }else{ ?>
                <img src="<?php echo base_url()?>assets/image/qrcode.jpg">
            <?php } ?>
        </div>
    </div>
    <input type="hidden" value="<?php echo site_url().$infocom->nom_url?>/menus_commercants" name="qr_code_img">
    <div class="col-4">
        <div class="w-100 top_menu_gen_qr">
            <?php if (isset($data_menu->url_qr_code_menu) AND is_file("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/menu_qr_code/".$data_menu->url_qr_code_menu)){ ?>
                <a class="btn btn-success btn_generate_qr_codr" download="qr_code_menu" href="<?php echo base_url("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/menu_qr_code/".$data_menu->url_qr_code_menu)?>" title="qr_code_menu">
                    Exportez l'image
                </a>
            <?php }elseif(!empty($data_menu)){ ?>
                <button disabled class="btn btn-success btn_generate_qr_codr">Exportez l'image</button>
            <?php } ?>
        </div>
    </div>
    <div class="w-100 text-center pt-4 pb-4">
        <input type="submit" class="btn btn-secondary  w-50" />
    </div>
</form>