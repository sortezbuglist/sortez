<div class="row glissiere_tab">
    <div class="col-4">
        <div class="text-left titre_gli_up">CONDITIONS GENERALES</div>
    </div>
    <div class="col-4" id="activ_glissiere_condition">
        <div class="cercle">
            <div class="text-center up d-none" id="activ_glissiere_condition_up"><p>fermer</p></div>
            <div class="text-center down d-block" id="activ_glissiere_condition_down"><p>ouvrir</p></div>
        </div>
    </div>
    <div class="col-4 pb-2 d-flex">
        <div class="text-right w-75 act_desact_text">Activer/désactiver</div>
        <div class="text-right w-25 titre_gli_up">
            <label class="switch">
                <input id="is_actif_condition" type="checkbox" <?php if (isset($data_menu->is_actif_condition) AND $data_menu->is_actif_condition == '1') echo 'checked'; ?> >
                <span class="slider round"></span>
            </label>
            <input name="data[is_actif_condition]" id="is_actif_condition_hidden" type="hidden" value="<?php if (isset($data_menu->is_actif_condition)) echo $data_menu->is_actif_condition; ?>">
        </div>
    </div>
    <div class=""></div>
</div>

<div class="row tab_child p-2 pb-4 d-none" id="is_activ_condition_child">
    <div class="col-lg-12 pt-5">
        <p class="h5 text-center">Mes coordonnées</p>
        <?php
        $thiss =& get_instance();
        $thiss->load->model('mdl_ville');
        $ville_data = $thiss->mdl_ville->getVilleById($infocom->IdVille);
        ?>
    </div>
    <input type="hidden" id="ville_id" name="infocom[IdVille]" value="<?php echo $infocom->IdVille; ?>">
    <input type="hidden" id="idcommercant" name="infocom[IdCommercant]" value="<?php echo $infocom->IdCommercant; ?>">
    <div class="col-lg-4 pb-2">
        <input type="text" id="nom_etablissement" class="form-control textarea_style" name="infocom[NomSociete]" placeholder="Nom de l’établissement" value="<?php if (isset($infocom->NomSociete)) echo $infocom->NomSociete; ?>">
    </div>
    <div class="col-lg-8 pb-2">
        <input type="text" id="adresse_coms" class="form-control textarea_style" name="infocom[adresse_localisation]"  placeholder="Adresse" value="<?php if (isset($infocom->adresse_localisation)) echo $infocom->adresse_localisation; ?>">
    </div>
    <div class="col-lg-4 pb-2">
        <input type="text" id="code_postal_coms" class="form-control textarea_style" name="infocom[CodePostal]" placeholder="Code postal" value="<?php if (isset($infocom->CodePostal)) echo $infocom->CodePostal; ?>">
    </div>
    <div class="col-lg-4 pb-2">
        <input type="text" id="ville_coms" class="form-control textarea_style" placeholder="Ville" value="<?php if (isset($ville_data->ville_nom)) echo $ville_data->ville_nom; ?>" disabled>
    </div>
    <div class="col-lg-4 pb-2">
        <input type="text" id="courriel_coms" class="form-control textarea_style" name="infocom[Email]" placeholder="Courriel" value="<?php if (isset($infocom->Email)) echo $infocom->Email; ?>">
    </div>
    <div class="col-lg-4 pb-2">
        <input type="text" id="numero_telephone" class="form-control textarea_style" name="infocom[TelMobile]" placeholder="Numéro de téléphone" value="<?php if (isset($infocom->TelMobile)) echo $infocom->TelMobile; ?>">
    </div>
    <div class="col-lg-4 pb-2">
        <input type="text" id="siret" class="form-control textarea_style" name="infocom[Siret]" placeholder="Siret" value="<?php if (isset($infocom->Siret)) echo $infocom->Siret; ?>">
    </div>
    <div class="col-lg-4 pb-2">
        <input type="text" id="code_ape" class="form-control textarea_style" name="infocom[code_ape]" placeholder="Code APE" value="<?php if (isset($infocom->code_ape)) echo $infocom->code_ape; ?>">
    </div>
    <div class="col-lg-4 pb-2">
        <input type="text" id="nom_responsable" class="form-control textarea_style" name="infocom[Nom]" placeholder="Nom du responsable" value="<?php if (isset($infocom->Nom)) echo $infocom->Nom; ?>">
    </div>
    <div class="col-lg-4 pb-2">
        <input type="text" id="prenom_responsable" class="form-control textarea_style" name="infocom[Prenom]" placeholder="Prénom du responsable" value="<?php if (isset($infocom->Prenom)) echo $infocom->Prenom; ?>">
    </div>
    <div class="col-lg-4 pb-2">
        <input type="text" id="titre_responsable" class="form-control textarea_style" name="infocom[Responsabilite]" placeholder="Titre du responsable" value="<?php if (isset($infocom->Responsabilite)) echo $infocom->Responsabilite; ?>">
    </div>
    <p class="pt-2 text_label w-100 text-center" for="condition_content">Conditions générales par défaut, vous pouvez modifier ces conditions <span style="color:#DB1414;"><span style="font-family:futura-lt-w01-book,sans-serif;">(Réinitialisez le texte par défaut)</span></span></p>
    <textarea id="condition_content" class="form-control textarea_style" name="data[condition_content]">
        <?php
        if(isset($data_menu->condition_content) && $data_menu->condition_content != null && $data_menu->condition_content != ""){
            echo $data_menu->condition_content;
        }
        ?>
    </textarea>
    <div id="content_default" class="d-none">
        <h2 style="text-align:center"><strong>Conditions générales de vente en ligne</strong></h2>

        <ul>
            <li>Les présentes conditions de vente sont conclues d'une part par l'établissement partenaire dont les coordonnées sont précisées en haut de page et, d'autre part, par toute personne physique ou morale souhaitant procéder à un achat via ce formulaire en ligne dénommée ci-après " l'acheteur ".</li>
        </ul>

        <h5><strong>Article 1. Objet</strong></h5>
        <ul>
            <li>L'acquisition d'un produit ou service à travers le présent site implique une acceptation sans réserve par l'acheteur des présentes conditions de vente dont l'acheteur reconna&icirc;t avoir pris connaissance préalablement à sa commande.</li>

            <li>Avant toute transaction, l'acheteur déclare d'une part que l'achat de produits sur ce formulaire est sans rapport direct avec son activité professionnelle et est limité à une utilisation strictement personnelle et d'autre part avoir la pleine capacité juridique, lui permettant de s'engager au titre des présentes conditions générales de ventes.</li>

            <li>L'établissement conserve la possibilité de modifier à tout moment ces conditions de ventes, afin de respecter toute nouvelle réglementation ou dans le but d'améliorer l'utilisation de son site. De ce fait, les conditions applicables seront celles en vigueur à la date de la commande par l'acheteur.</li>

            <li>Les présentes conditions de vente visent à définir les relations contractuelles entre le professionnel et le consommateur et les conditions applicables à tout achat effectué par le biais de ce formulaire.</li>
        </ul>

        <h5><strong>Article 2. Produits ou services</strong></h5>

        <ul>
            <li>Les produits ou services proposés sont ceux qui figurent sur ce formulaire en ligne dans la limite des stocks disponibles.</li>

            <li>Cet établissement se réserve le droit de modifier à tout moment l'assortiment de produits.</li>

            <li>Chaque produit est présenté sous forme d'un descriptif reprenant ses principales caractéristiques techniques.</li>

            <li>Les photographies sont les plus fidèles possibles mais n'engagent en rien le Vendeur.</li>
        </ul>


        <h5><strong>Article 3. Tarifs</strong></h5>

        <ul>
            <li>Les prix figurant sur les fiches produits de ce formulaire sont des prix en Euros et en toutes taxes comprises (TTC) tenant compte de la TVA applicable au jour de la commande.</li>

            <li>Tout changement du taux de la TVA pourra être répercuté sur le prix des produits.</li>

            <li>Cet établissement se réserve le droit de modifier ses prix à tout moment, étant toutefois entendu que le prix figurant au catalogue le jour de la commande sera le seul applicable à l'acheteur.</li>

            <li>Pour toute commande supérieure ou égale au montant précisé sur le formulaire les frais de port sont offerts sur les territoires référencés et présents sur le formulaire</li>

            <li>Pour toute commande inférieure un forfait de participation aux frais d'expédition pourra être facturé à l'acheteur d'un montant qui sera précisé si c'est le cas en haut de page.</li>
        </ul>

        <h5><strong>Article 4. Commande et modalités de paiement</strong></h5>

        <ul>
            <li>Avant toute commande, l'acheteur doit créer un compte avec la carte vivresaville.fr sur le site de sortez.org qui déclare accepter les conditions générales spécifiques.</li>

            <li>A chaque visite, l'acheteur, s'il souhaite commander ou consulter son compte (état des commandes, profil&hellip;), devra s'identifier à l'aide de ces informations.</li>

            <li>L'établissement propose à l'acheteur de commander et régler ses produits en plusieurs étapes, avec des options services ou de paiement au choix :</li>
        </ul>

        <h5><strong>Article 5. Vente à emporter ou livraison à domicile et service à domicile</strong></h5>

        <ul>
            <li>L'acheteur sélectionne le type de service (vente à emporter, vente en ligne avec une livraison à domicile, vente en ligne avec un paiement différé) ● L'acheteur précise la date et l'heure de livraison souhaitée.</li>

            <li>L'acheteur sélectionne les produits qu'il souhaite commander dans le &laquo; panier &raquo;, modifie si besoin (quantités, références&hellip;), vérifie l'adresse de livraison ou en renseigne une nouvelle.</li>

            <li>Ensuite, l'acheteur choisit le mode de paiement de son choix en fonction du type de service choisi.</li>

            <li>L'acheteur confirme l'acceptation des conditions générales de vente.</li>

            <li>La validation de la commande forme la conclusion définitive du contrat.</li>

            <li>Dès validation, l'acheteur reçoit un bon de commande par mail confirmant l'enregistrement de cette dernière.</li>
        </ul>
        <h5><strong>Article 6. Les modalités de paiement</strong></h5>
        <ul>
            <li><strong>Vente en ligne avec le paiement sécurisé par Paypal :</strong></li>
            <ol>
                <li>Après avoir confirmé sa commande, l'acheteur effectue le règlement par carte bancaire ;</li>

                <li>Il lui suffira de noter sur le module PAYPAL, le montant total TTC indiqué ci-dessus ;</li>

                <li>Imprimez et signez le justificatif de bonne réception ;</li>

                <li>Dès réception, nous vous contacterons pour vous remercier et vous préciser les modalités de votre livraison ;</li>

                <li>Remettez le justificatif de bonne réception à notre livreur ou dans notre établissement dans le cadre d'une vente à emporter.</li>

            </ol>
            <li><strong>Paiement à la livraison à domicile par carte bancaire avec un terminal de paiement :</strong> <p>Lors de la livraison, l'acheteur devra remettre le bon de commande après avoir daté et signé le bon de réception.</p></li>

            <li><strong>Paiement dans la vente à emporter par carte bancaire, chèque (préalablement rempli) ou espèces</strong> <p>Lors du retrait, l'acheteur devra remettre le bon de commande après avoir daté et signé le bon de réception.</p></li>
        </ul>

        <h5><strong>Article 7. Réserve de propriété</strong></h5>

        <ul>
            <li>L'établissement conserve la propriété pleine et entière des produits vendus jusqu'au parfait encaissement du prix, en principal, frais et taxes compris.</li>
        </ul>

        <h5><strong>Article 8. Rétractation</strong></h5>

        <ul>

            <li>En vertu de l'article L121-20 du Code de la consommation, l'acheteur dispose d'un délai de quatorze jours ouvrables à compter de la livraison de leur commande pour exercer son droit de rétractation et ainsi faire retour du produit au vendeur pour échange ou remboursement sans pénalité, à l'exception des frais de retour.</li>

        </ul>

        <h5><strong>Article 9. Livraison par l'établissement</strong></h5>

        <ul>
            <li>Les livraisons sont faites à l'adresse indiquée sur le bon de commande qui ne peut être que dans la zone géographique convenue.</li>

            <li>Les livraisons sont effectuées par l'établissement ou par un transporteur désigné par lui.</li>

            <li>Les dates et heures de livraison souhaitées sont précisées par le consommateur, l'établissement devra précisé son accord ou précisez dès réception les éventuelles modifications.</li>

            <li>Les heures de livraison précisées sont variables à + ou - 30 minutes pour la restauration.</li>

            <li>Lors de la livraison, l'acheteur devra remettre le bon de commande après avoir daté et signé le bon de réception.</li>

            <li>Pour les autres activités, les délais de livraison ne sont donnés qu'à titre indicatif, si ceux-ci dépassent dix jours à compter de la commande, le contrat de vente pourra être résilié et l'acheteur remboursé.</li>

        </ul>
        <h5><strong>Article 10. Livraison par un tiers</strong></h5>

        <ul>
            <li>Pour les autres produits que la restauration, l'établissement pourra fournir par e-mail à l'acheteur le numéro de suivi de son colis.</li>

            <li>En cas d'absence de l'acheteur, il recevra un avis de passage, ce qui lui permet de retirer les produits commandés au bureau de Poste le plus proche ou dans un point relais du choix.</li>

            <li>Les risques liés au transport sont à la charge de l'acquéreur à compter du moment o&ugrave; les articles quittent les locaux de l'établissement.</li>

            <li>L'acheteur est tenu de vérifier en présence du préposé de La Poste ou du livreur, l'état de l'emballage de la marchandise et son contenu à la livraison.</li>

            <li>En cas de dommage pendant le transport, toute protestation doit être effectuée auprès du transporteur dans un délai de trois jours à compter de la livraison.</li>

        </ul>
        <h5><strong>Article 11. Garantie</strong></h5>

        <ul>
            <li>Tous les produits fournis par l'établissement bénéficient de la garantie légale prévue par les articles 1641 et suivants du Code civil.</li>

            <li>En cas de non conformité d'un produit vendu, il pourra être retourné qui le reprendra, l'échangera ou le remboursera.</li>

            <li>Toutes les réclamations, demandes d'échange ou de remboursement doivent s'effectuer par voie postale à l'adresse précisée sur l'en-tête, dans un délai de trente jours après livraison.</li>
        </ul>

        <h5><strong>Article 12. Responsabilité</strong></h5>

        <ul>
            <li>L'établissement dans le processus de vente à distance, n'est tenue que par une obligation de moyens.</li>

            <li>Sa responsabilité ne pourra être engagée pour un dommage résultant de l'utilisation du réseau Internet tel que perte de données, intrusion, virus, rupture du service, ou autres problèmes involontaires.</li>

        </ul>

        <h5><strong>Article 13. Propriété intellectuelle</strong></h5>

        <ul>
            <li>Tous les éléments du site sont et restent la propriété intellectuelle et exclusive de l'établissement. Personne n'est autorisé à reproduire, exploiter, ou utiliser à quelque titre que ce soit, même partiellement, des éléments du site qu'ils soient sous forme de photo, logo, visuel ou texte.</li>
        </ul>

        <h5><strong>Article 14. Données à caractère personnel</strong></h5>

        <ul>
            <li>L'établissement s'engage à préserver la confidentialité des informations fournies par l'acheteur, qu'il serait amené à transmettre pour l'utilisation de certains services. Toute information le concernant est soumise aux dispositions de la loi n° 78-17 du 6 janvier 1978.</li>

            <li>A ce titre, l'internaute dispose d'un droit d'accès, de modification et de suppression des informations le concernant. Il peut en faire la demande à tout moment par courrier à l'adresse précisée sur l'en-tête.</li>
        </ul>

        <h5><strong>Article 15. Règlement des litiges</strong></h5>

        <ul>
            <li>Les présentes conditions de vente à distance sont soumises à la loi française. Pour tous litiges ou contentieux, le Tribunal compétent sera celui du siège social de l'établissement dont les coordonnées sont précisées sur l'en-tête.​</li>
        </ul>

    </div>
    <!--    <script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>-->
    <script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
    <script>

        CKEDITOR.config.htmlEncodeOutput = true;

        CKEDITOR.replace('condition_content');

        $(document).ready(function(){
            <?php if(isset($data_menu->condition_content) && $data_menu->condition_content != null && $data_menu->condition_content != ""){ ?>

            <?php }else{ ?>
            var content_default = $('#content_default').html();
            $('#condition_content').html(content_default);
            <?php } ?>
            $('#check_conditions').click(function(){
                var check_value = $('#conditions_accept_value').val();
                if(check_value == 0){
                    $('#conditions_accept_value').val('1');
                    document.getElementById('validation_btn_condition').disabled = false;
                }else{
                    $('#conditions_accept_value').val('0');
                    document.getElementById('validation_btn_condition').disabled = true;
                }
            });
            if(document.getElementById("check_conditions").checked == true){
                document.getElementById('validation_btn_condition').disabled = false;
            }else{
                document.getElementById('validation_btn_condition').disabled = true;
            }
        });

        $('#code_postal_coms').change(function(){
            var code_postal_coms = $("#code_postal_coms").val();
            var datas = "code_postal_coms="+code_postal_coms;
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/menu/Info_menu_commercant/get_ville_by_codepostal'); ?>",
                data: datas ,
                success: function(data) {
                    console.log(data)
                    var datass = JSON.parse(data);
                    var idville = datass.IdVille;
                    $('#ville_id').val(idville);
                    var datasss = "idville="+idville;
                    $.ajax({
                        type: "POST",
                        url: "<?php echo site_url('admin/menu/Info_menu_commercant/get_nom_by_id'); ?>",
                        data: datasss ,
                        success: function(data) {
                            console.log(data)
                            var data_ville = JSON.parse(data);
                            var ville_nom = data_ville.ville_nom;
                            $('#ville_coms').val(ville_nom);
                        },
                        error: function() {
                            // alert('Erreur');
                        }
                    });
                },
                error: function() {
                    // alert('Erreur');
                }
            });
        });
        function get_all_data(){
            var idcommercant = $('#idcommercant').val();
            var nomsociete = $('#nom_etablissement').val();
            var adresse = $('#adresse_coms').val();
            var codepostal = $('#code_postal_coms').val();
            var ville_id = $('#ville_id').val();
            var courriel_coms = $('#courriel_coms').val();
            var numero_telephone = $('#numero_telephone').val();
            var siret = $('#siret').val();
            var code_ape = $('#code_ape').val();
            var nom_responsable = $('#nom_responsable').val();
            var prenom_responsable = $('#prenom_responsable').val();
            var titre_responsable = $('#titre_responsable').val();
            var data_all = "idcommercant="+idcommercant+"&nomsociete="+nomsociete+"&adresse="+adresse+"&codepostal="+codepostal+"&ville_id="+ville_id+"&courriel_coms="+courriel_coms+"&numero_telephone="+numero_telephone+"&siret="+siret+"&code_ape="+code_ape+"&nom_responsable="+nom_responsable+"&prenom_responsable="+prenom_responsable+"&titre_responsable="+titre_responsable;
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/menu/Info_menu_commercant/get_all_data_coms'); ?>",
                data: data_all ,
                success: function(data) {
                    if(data == "ok"){
                        $('#form_all_data').submit();
                    }else{
                        alert('ko');
                    }
                },
                error: function() {
                    // alert('Erreur');
                }
            });
        }
    </script>
    <style>
        #cke_condition_content{
            width: 100%!important;
        }
        .check_new2{
            width: 55px;
            height: 55px;
        }
    </style>
    <div class="row w-100">
        <div class="col-4 pt-5 d-flex text-center m-auto">
            <input type="checkbox" class="check_new2" id="check_conditions" <?php if(isset($data_menu->is_accept_condition) && $data_menu->is_accept_condition != null && $data_menu->is_accept_condition != 0){ echo "checked"; } ?>>
            <span>Vous devez cocher et valider les conditions générales de ventes</span>
            <input type="hidden" id="conditions_accept_value" name="data[is_accept_condition]" value="<?php if(isset($data_menu->is_accept_condition) && $data_menu->is_accept_condition != null && $data_menu->is_accept_condition != ""){ echo $data_menu->is_accept_condition; }else{echo "0";} ?>">
        </div>
        <div class="col-12  pt-5 text-center m-auto">
            <button type="button" onclick="javascript:get_all_data()" class="btn btn-secondary  w-50" id="validation_btn_condition" disabled>Validation</button>
        </div>
    </div>

</div>