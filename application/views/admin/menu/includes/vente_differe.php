<div class="row glissiere_tab livraison_row">
    <div class="col-5">
        <div class="text-left titre_gli_up">VENTE EN LIGNE - PAIEMENT DIFFERE</div>
    </div>
    <div class="col-2" id="activ_glissiere_differe">
        <div class="cercle">
            <div class="text-center up d-block" id="activ_glissiere_differe_up"><p>fermer</p></div>
            <div class="text-center down d-none" id="activ_glissiere_differe_down"><p>ouvrir</p></div>
        </div>
    </div>
    <div class="col-5 d-flex">
        <div class="text-right w-75 act_desact_text">Activer/désactiver</div>
        <div class="text-right w-25 titre_gli_up">
            <label class="switch">
                <input id="is_activ_vente_differe" type="checkbox" <?php if (isset($data_menu->is_activ_vente_differe) AND $data_menu->is_activ_vente_differe == '1') echo 'checked'; ?> >
                <span class="slider round"></span>
            </label>
            <input name="data[is_activ_vente_differe]" id="is_activ_vente_differe_hidden" type="hidden" value="<?php if (isset($data_menu->is_activ_vente_differe)) echo $data_menu->is_activ_vente_differe; ?>">
        </div>
    </div>
</div>
<div class="row activ_glissiere_differe_child shadowed">
    <div class="col-12 pt-4 pb-4">
        <div class="w-100 text_label text-center">
            Jours et heures de contact
        </div>
        <div class="w-100">
            <textarea name="data[horaire_contact]" class="form-control textarea_style_paypal" id="new_paypal_comment"><?php if (isset($data_menu->horaire_contact)) echo $data_menu->horaire_contact; ?></textarea>
        </div>
    </div>
    <div class="col-8 pt-4">
        <h5>COMMANDE EN LIGNE :  ENVOIS DE DOCUMENTS (devis, factures...)
            ET PAIEMENT EN RETOUR</h5>
    </div>
    <div class="col-4 pt-4 d-flex">
        <div class="text-right w-75 act_desact_text">Activer/désactiver</div>
        <div class="text-right w-25 titre_gli_up">
            <label class="switch">
                <input id="is_activ_env_document" type="checkbox" <?php if (isset($data_menu->is_activ_env_document) AND $data_menu->is_activ_env_document == '1') echo 'checked'; ?> >
                <span class="slider round"></span>
            </label>
            <input name="data[is_activ_env_document]" id="is_activ_env_document_hidden" type="hidden" value="<?php if (isset($data_menu->is_activ_env_document)) echo $data_menu->is_activ_env_document; ?>">
        </div>
    </div>
    <div class="col-12">
        <label for="comment_emporter_txt" class="pt-2 text_label w-100 text-center">
            <div style="display: inline-block"><div class="label_switc pt-1 pr-2">Commentaires par défaut</div></div>
            <label class="switch_out">
                <input id="is_activ_comment_default_differe" type="checkbox" <?php if (isset($data_menu->is_activ_comment_default_differe) AND $data_menu->is_activ_comment_default_differe == '1') echo 'checked'; ?> >
                <span class="slider_out round_out"></span>
            </label>
        </label>
        <input id="is_activ_comment_default_differe_hidden" type="hidden" name="data[is_activ_comment_default_differe]" value="<?php if (isset($data_menu->is_activ_comment_default_differe)) echo $data_menu->is_activ_comment_default_differe; ?>" />
        <textarea id="comment_differe_txt" class="form-control textarea_style w-100" name="data[comment_differe_txt]" placeholder=""><?php if (isset($data_menu->comment_differe_txt)) echo $data_menu->comment_differe_txt; ?></textarea>
    </div>
    <div class="row mt-4 w-100">
        <div class="col-2">
            <div class="row">
                <div class="col-12 pr-0 text-right">
                    <label class="switch_out absoluted">
                        <input id="is_activ_fact_type_differe" type="checkbox" <?php if (isset($data_menu->is_activ_fact_type_differe) AND $data_menu->is_activ_fact_type_differe == '1') echo 'checked'; ?> >
                        <span class="slider_out round_out"></span>
                    </label>
                    <input name="data[is_activ_fact_type_differe]" id="is_activ_fact_type_differe_hidden" type="hidden" value="<?php if (isset($data_menu->is_activ_fact_type_differe)) echo $data_menu->is_activ_fact_type_differe; ?>" />
                </div>
            </div>
        </div>
        <div class="col-2 text-left">
            <div class="row">
                <div class="col-12 pr-0 text-left">
                    <div class="w-100 d-flex switch_term_mob">
                        <input class="check_new" id="is_activ_devis_differe" type="checkbox" <?php if (isset($data_menu->is_activ_devis_differe) AND $data_menu->is_activ_devis_differe == '1') echo 'checked'; ?> >
                        <input id="is_activ_devis_differe_hidden" type="hidden" name="data[is_activ_devis_differe]" value="<?php if (isset($data_menu->is_activ_devis_differe)) echo $data_menu->is_activ_devis_differe; ?>">
                        <div class="pl-2">Devis</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-2">
            <div class="row">
                <div class="col-12 p-0 text-left">
                    <div class="w-100 d-flex switch_term_mob">
                        <input class="check_new" id="is_activ_pro_forma_differe" type="checkbox" <?php if (isset($data_menu->is_activ_pro_forma_differe) AND $data_menu->is_activ_pro_forma_differe == '1') echo 'checked'; ?> >
                        <input id="is_activ_pro_forma_differe_hidden" type="hidden" name="data[is_activ_pro_forma_differe]" value="<?php if (isset($data_menu->is_activ_pro_forma_differe)) echo $data_menu->is_activ_pro_forma_differe; ?>">
                        <div class="pl-2">Facture pro forma</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="row">
                <div class="col-12 pr-0 text-left">
                    <div class="w-100 d-flex switch_term_mob">
                        <input class="check_new" id="is_activ_facture_differe" type="checkbox" <?php if (isset($data_menu->is_activ_facture_differe) AND $data_menu->is_activ_facture_differe == '1') echo 'checked'; ?> >
                        <input id="is_activ_facture_differe_hidden" type="hidden" name="data[is_activ_facture_differe]" value="<?php if (isset($data_menu->is_activ_facture_differe)) echo $data_menu->is_activ_facture_differe; ?>">
                        <div class="pl-2">Facture</div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="row mt-2 w-100">
        <div class="col-2">
            <div class="row">
                <div class="col-12 pr-0 text-right">
                    <label class="switch_out absoluted">
                        <input id="is_activ_banc_type_differe" type="checkbox" <?php if (isset($data_menu->is_activ_banc_type_differe) AND $data_menu->is_activ_banc_type_differe == '1') echo 'checked'; ?> >
                        <span class="slider_out round_out"></span>
                    </label>
                    <input name="data[is_activ_banc_type_differe]" id="is_activ_banc_type_differe_hidden" type="hidden" value="<?php if (isset($data_menu->is_activ_banc_type_differe)) echo $data_menu->is_activ_banc_type_differe; ?>" />
                </div>
            </div>
        </div>
        <div class="col-2 text-left">
            <div class="row">
                <div class="col-12 pr-0 text-left">
                    <div class="w-100 d-flex switch_term_mob">
                        <input class="check_new" id="is_activ_Cheque_differe" type="checkbox" <?php if (isset($data_menu->is_activ_Cheque_differe) AND $data_menu->is_activ_Cheque_differe == '1') echo 'checked'; ?> >
                        <input id="is_activ_Cheque_differe_hidden" type="hidden" name="data[is_activ_Cheque_differe]" value="<?php if (isset($data_menu->is_activ_Cheque_differe)) echo $data_menu->is_activ_Cheque_differe; ?>">
                        <div class="pl-2">Cheque</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-2">
            <div class="row">
                <div class="col-12 p-0 text-left">
                    <div class="w-100 d-flex switch_term_mob">
                        <input class="check_new" id="is_activ_Virement_differe" type="checkbox" <?php if (isset($data_menu->is_activ_Virement_differe) AND $data_menu->is_activ_Virement_differe == '1') echo 'checked'; ?> >
                        <input id="is_activ_Virement_differe_hidden" type="hidden" name="data[is_activ_Virement_differe]" value="<?php if (isset($data_menu->is_activ_Virement_differe)) echo $data_menu->is_activ_Virement_differe; ?>">
                        <div class="pl-2">Virement</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="row">
                <div class="col-12 pr-0 text-left">
                    <div class="w-100 d-flex switch_term_mob">
                        <input class="check_new" id="is_activ_carte_differe" type="checkbox" <?php if (isset($data_menu->is_activ_carte_differe) AND $data_menu->is_activ_carte_differe == '1') echo 'checked'; ?> >
                        <input id="is_activ_carte_differe_hidden" type="hidden" name="data[is_activ_carte_differe]" value="<?php if (isset($data_menu->is_activ_carte_differe)) echo $data_menu->is_activ_carte_differe; ?>">
                        <div class="pl-2">Carte bancaire</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row w-100 pb-5">
        <div class="col-12 pt-5 text-center">
            <input type="submit" class="btn btn-secondary  w-50" />
        </div>
    </div>
</div>