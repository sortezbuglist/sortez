<div class="row glissiere_tab livraison_row">
    <div class="col-5">
        <div class="text-left titre_gli_up gli_menu_gen">INTEGREZ UN BOUTON AU MENU GENERAL</div>
    </div>
    <div class="col-2" id="activ_glissiere_prom">
        <div class="cercle">
            <div class="text-center up d-block" id="activ_glissiere_prom_up"><p>fermer</p></div>
            <div class="text-center down d-none" id="activ_glissiere_prom_down"><p>ouvrir</p></div>
        </div>
    </div>
<!--    <div class="col-5 d-flex">-->
<!--        <div class="text-right w-75 act_desact_text">Activer/désactiver</div>-->
<!--        <div class="text-right w-25 titre_gli_up">-->
<!--            <label class="switch">-->
<!--                <input id="is_activ_prom" type="checkbox" --><?php //if (isset($data_menu->is_activ_prom) AND $data_menu->is_activ_prom == '1') echo 'checked'; ?><!-- >-->
<!--                <span class="slider round"></span>-->
<!--            </label>-->
<!--        </div>-->
<!--    </div>-->
</div>
<form method="post" action="<?php echo site_url();?>admin/menu/Info_menu_commercant/save_info_menu_commercant" class="row pb-5 shadowed p-2 gli_content d-flex">
    <input type="hidden" name="data[id]" value="<?php if (isset($data_menu->id)){echo $data_menu->id;}else{echo "0";} ?>" />
    <div class="col-4 text-center">
       <div class="w-100 text-center top_menu_gen">
           Je désire que ce bouton s’intègre sur le menu principal de mon site<br>
           <input <?php if (isset($data_menu->is_activ_default_btn) AND $data_menu->is_activ_default_btn == '1' ){echo 'checked';} ?> type="checkbox" class="checkbox_active_default_menu" id="is_activ_default_btn_btn" >
           <input id="is_activ_default_btn" value="<?php if (isset($data_menu->is_activ_default_btn) AND $data_menu->is_activ_default_btn == '1' ){echo '1';}else{echo '0';} ?>"  type="hidden" name="data[is_activ_default_btn]">           
       </div>
       <!--  <div class="w-100 text-center">
            <img src="<?php //echo base_url()?>/assets/image/btn_menu.png">
        </div> -->
        choisissez la couleur du bouton en cliquant sur le panel 
        <input type="color" name="data[btn_color]" style="width:150px; height: 150px"  value="<?php if (isset($data_menu->btn_color)) echo $data_menu->btn_color; ?>">
    </div>

    <div class="col-4">
        <div class="w-100 text-center top_menu_gen">
            Télécharger l'image de personnalisation, Cliquez sur l'icone !...
        </div>
        <div class="w-100 text-center" id="img_menu_gen_contents">
            <?php if (isset($data_menu->image_menu_gen) AND $data_menu->image_menu_gen !=null && is_file("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/menu_icon/".$data_menu->image_menu_gen)){ ?>
                <div class="w-100">
                    <img style="max-width: 200px" src="<?php echo base_url()?>application/resources/front/photoCommercant/imagesbank/<?php echo $infocom->user_ionauth_id; ?>/menu_icon/<?php echo $data_menu->image_menu_gen ?>">
                    <a onclick="delete_image_menu_gen(<?php echo $infocom->IdCommercant; ?>)" class="btn btn-danger mt-3">Supprimer image</a>
                </div>
            <?php }else{ ?>
                <div class="w-100" id="Menuphoto_menu_gen_container">
                    <img onclick='javascript:window.open("<?php echo site_url("media/index/".$infocom->IdCommercant."-menu_icon-photo_menu_gen"); ?>", "", "width=1045, height=675, scrollbars=yes");' src="<?php echo base_url()?>assets/image/download-icon-png-5.jpg">
                </div>
                <div id="Articleimg_menuphoto_menu_gen_container"></div>
            <?php } ?>
        </div>
    </div>

    <div class="col-4">
        <div class="w-100 top_menu_gen">
            Précisez le texte
            du bouton
        </div>
        <div class="w-100 text-center menu_txt_container">
            <input maxlength="22" value="<?php if (isset($data_menu->menu_value)) echo $data_menu->menu_value; ?>" type="text" class="form-control input_menu_txt" name="data[menu_value]">
            <?php if($error_message!=NULL):?>
           <div class="alert alert-danger mt-2" role="alert">
                <strong>Erreur ! </strong><?php print_r($error_message);?>
            </div>
            <?php endif;?>
        </div>
    </div>
    <div class="w-100 text-center pt-4 pb-4">
        <input type="submit" class="btn btn-secondary  w-50" />
    </div>
</form>