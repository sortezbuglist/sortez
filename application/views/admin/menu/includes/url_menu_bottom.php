<div class="row glissiere_tab livraison_row">
    <div class="col-5">
        <div class="text-left titre_gli_up gli_menu_gen">INTEGREZ LE MENU SUR VOTRE SITE WEB</div>
    </div>
    <div class="col-2" id="activ_glissiere_prom">
        <div class="cercle">
            <div class="text-center up d-block" id="activ_glissiere_prom_up"><p>fermer</p></div>
            <div class="text-center down d-none" id="activ_glissiere_prom_down"><p>ouvrir</p></div>
        </div>
    </div>
</div>
<form method="post" action="<?php echo site_url();?>admin/menu/Info_menu_commercant/generate_qr_code" class="row pb-5 shadowed p-2 gli_content d-flex">
    <input type="hidden" name="id_menu_com" value="<?php if (isset($data_menu->id)){echo $data_menu->id;}else{echo "0";} ?>" />
    <div class="col-12 pl-0">
        <div class="w-100">
            <p>Intégrez le formulaire en ligne sur une page de votre site web en utilisant le lien ci-dessous.
                Contacter votre webmaster :</p>
        </div>

    </div>
    <div class="w-100 text-center">
        <input class="form-control" type="text" value="<?php echo site_url().$infocom->nom_url?>/menus_commercants?content_only=1">
    </div>
</form>