<div class="row glissiere_tab livraison_row">
    <div class="col-5">
        <div class="text-left titre_gli_up">ACTION PROMOTIONNELLE !</div>
    </div>
    <div class="col-2" id="activ_glissiere_prom">
        <div class="cercle">
            <div class="text-center up d-block" id="activ_glissiere_prom_up"><p>fermer</p></div>
            <div class="text-center down d-none" id="activ_glissiere_prom_down"><p>ouvrir</p></div>
        </div>
    </div>
    <div class="col-5 d-flex">
        <div class="text-right w-75 act_desact_text">Activer/désactiver</div>
        <div class="text-right w-25 titre_gli_up">
            <label class="switch">
                <input id="is_activ_prom" type="checkbox" <?php if (isset($data_menu->is_activ_prom) AND $data_menu->is_activ_prom == '1') echo 'checked'; ?> >
                <span class="slider round"></span>
            </label>
        </div>
    </div>
</div>
<form method="post" action="<?php echo site_url();?>admin/menu/Info_menu_commercant/save_info_menu_commercant" class="row activ_glissiere_prom_child pt-4 back_prom">
    <input name="data[is_activ_prom]" id="is_activ_prom_hidden" type="hidden" value="<?php if (isset($data_menu->is_activ_prom)) echo $data_menu->is_activ_prom; ?>">
    <div class="col-3 text-center">
        <img src="<?php echo base_url()?>assets/images/cible.webp">
    </div>
    <div class="col-8">
        <div class="w-100 text-center titre_prom">
            ORGANISEZ UNE ACTION PROMOTIONNELLE !
        </div>
        <div class="w-100">
            <p class="para_prom">Avec ce module, vous organisez en temps réel une action promotionnelle qui apparaîtra sous le bouton de validation de la commande client. Cette promo peut augmenter sa potentialité.</p>
        </div>
        <div class="w-100 d-flex">
            <div class="col-6 p-0">
                <div class="row w-100">
                    <div class="col-8">
                        <div class="para_prom">
                            Précisez le montant ttc<br>
                            d'achat a dépasser
                        </div>
                    </div>
                    <div class="col-4 text-right p-0 pt-2">
                        <input value="<?php echo $data_menu->price_to_reach ?? 0; ?>" name="data[price_to_reach]" id="price_to_reach" class="form-prom" type="number">
                    </div>
                </div>
            </div>
            <?php if (isset($data_menu->id) AND $data_menu->id != null){?>
                <input type="hidden" name="data[id]" value="<?php echo $data_menu->id ?>">
            <?php } ?>
            <div class="col-6 p-0 pl-3">
                <div class="row w-100">
                    <div class="col-8">
                        <div class="para_prom">
                            Précisez le montant ttc<br>
                            de la remise immédiate
                        </div>
                    </div>
                    <div class="col-4 text-right p-0 pt-2">
                        <input value="<?php  echo $data_menu->price_to_win ?? 0; ?>" name="data[price_to_win]" id="price_to_win" class="form-prom" type="number">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="w-100 text-center pt-4 pb-4">
        <input type="submit" class="btn btn-secondary  w-50" />
    </div>
</form>