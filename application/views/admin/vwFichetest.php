<?php $data["zTitle"] = 'Creation Type Article'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script>
	function testFormTypeArticle(){
	var article_type = $("#article_type").val () ;
	
	var zErreur = "" ;
	
	if (article_type == ""){
		zErreur += "Veuillez selectionner un article_type\n" ;
	}
	
	if (zErreur != ""){
		alert ('Veuillez indiquer un Nom !') ;
	}else{
		document.frmCreationTypeArticle.submit();
	}
	
	return false;
}
</script>
    
    <div id="divFicheAnnonce" class="content" align="center">
	     <br>
	     <a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" value= "Retour au Menu" id = "btn"/> </a>
         
        <form name="frmCreationTypeArticle" id="frmCreationTypeArticle" action="<?php if (isset($oArticle)) { echo site_url("admin/types_test/modif_types_test/$testid"); }else{ echo site_url("admin/types_test/creer_types_test"); } ?>" method="POST">
		<input type="hidden" name="types_test[testid]" id="article_typeid_id" value="<?php if (isset($oArticle)) echo $oArticle->article_typeid ; else echo '0'; ?>" />
            <h1><?php echo $title ; ?></h1>
			<span><?php if (isset($msg)) { echo $msg ; } ?></span>
            <fieldset>
                <legend>Type test</legend>
                <table cellpadding="0" cellspacing="0">
                    
					<tr>
                        <td>
                            <label>Nom : </label>
                        </td>
                        <td>
                            <input type="text" name="types_test[test]" id="test" value="<?php if (isset($oArticle)) { echo $oArticle->test ; } ?>" />
                        </td>
                    </tr>
					<tr>
                        <td></td>
                        <td align="left">
                        <input type="button" value="Annuler" onclick="javascript:document.location='<?php echo site_url("admin/types_test/liste");?>';" />&nbsp;
                        <input type="button" value="Valider" onclick="javascript:testFormTypeArticle();" /></td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>


<?php $this->load->view("admin/includes/vwFooter2013"); ?>