<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SHOW THE ITEM n° <?= $datas[0]->id ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>

    <div class="container mt-3 mt-md-5">
        <div class="d-flex justify-content-between">
            <a href="<?= site_url('admin/Pictures/form') ?>">Create a new item</a>
            <a href="<?= site_url('admin/Pictures/lists') ?>">View all records</a>
        </div>
        <div class="d-flex justify-content-center">
            <h1>THE ITEM N° <?= $datas[0]->id ?></h1>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3">
                <div class="d-flex flex-column justify-content-center">
                    <h3 style="text-decoration: underline">Title :</h3>
                    <p><?= $datas[0]->title ?></p>
                </div>
                <p class="d-flex flex-column justify-content-center">
                    <h3 style="text-decoration: underline">Image :</h3>
                    <?php if(!empty($datas[0]->image)): ?>
                    <img src="<?= base_url() ?>application/images/<?= $datas[0]->image ?>" class="img-fluid">
                    <?php else : ?>
                    <span class=' fa fa-exclamation-triangle fa-5x p-3'></span>
                    <?php endif ?>
                </p>
               
                <div class="d-flex flex-column justify-content-center">
                    <h3 style="text-decoration: underline">Description :</h3>
                    <p><?= $datas[0]->description ?></p>
                </div>
                <div class="d-flex justify-content-center">
                    <a href="<?= site_url('admin/Pictures/edit/' . $datas[0]->id) ?>" class="btn btn-info">EDIT THIS ITEM</a>
                    <a href="<?= site_url('admin/Pictures/delete/' . $datas[0]->id) ?>" class="btn btn-warning">DELETE THIS ITEM</a>
                </div>
            </div>
        </div>
    </div>

</body>

</html>