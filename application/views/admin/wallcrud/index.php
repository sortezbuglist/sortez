<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <title>CREATE A NEW ITEM</title>
</head>
<body>
    
    <div class="container mt-3 mt-md-5">
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3">
                <h1 class="d-flex justify-content-center mb-3 mb-md-5"><span>New Admin CRUD</span></h1>
                
                <?php if (strlen(validation_errors()) !== 0) : ?>
                <div class="alert alert-warning">
                    <?= validation_errors(); ?>
                </div>
                <?php endif ?>
                <div class="form-group">
                    <?= form_open_multipart('admin/Pictures/store') ?>
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" id="title" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="image">Upload an image</label>
                            <input type="file" name="image" id="image" class="btn btn-primary form-control-file">
                        </div>
                        <div class="form-group d-flex justify-content-center">
                            <button type="submit" class="btn btn-success">ENVOYER</button>
                        </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
