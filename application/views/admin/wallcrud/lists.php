<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>List of all records</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
</head>
<body>
    
    <div class="container my-3 my-md-5">
        <a href="<?= site_url('admin/Pictures/form') ?>">Create a new item</a>
        <div class="d-flex justify-content-center"><h1>LIST OF ALL RECORDS</h1></div>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">TITLE</th>
                    <th scope="col">DESCRIPTION</th>
                    <th scope="col">IMAGE FILE NAME (application/images/...)</th>
                    <th scope="col">ACTIONS</th>
                </tr>
            </thead>
            <tbody>
                <?php if ($datas) : ?>
                    <?php foreach ($datas as $row) : ?>             
                    <tr>
                        <th scope="row"><?= $row->id ?></th>
                        <td><?= $row->title ?></td>
                        <td><?= $row->description ?></td>
                        <td><?= $row->image ?></td>
                        <td>
                            <a href="<?= site_url('admin/Pictures/show/'. $row->id) ?>" class="btn btn-success">
                                View
                            </a>
                            <a href="<?= site_url('admin/Pictures/edit/'. $row->id) ?>" class="btn btn-info">
                                Edit
                            </a>
                            <a href="<?= site_url('admin/Pictures/delete/'. $row->id) ?>" class="btn btn-danger">
                                Delete
                            </a>                        
                        </td>
                    </tr>
                    <?php endforeach ?> 
                <?php else : ?>
                    NO RECORD YET
                <?php endif ?>
            </tbody>
        </table>
    </div>

</body>
</html>
