<?php $data["zTitle"] = 'Creation Localisation'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script>
	function testFormLocalisation(){
		var NomLocalisation = $("#NomLocalisation").val () ;
		var AdresseLocalisation  = $("#AdresseLocalisation").val () ;
		var CpLocalisation = $("#CpLocalisation").val () ;
		var location_villeid = $("#location_villeid").val () ;
		var zErreur = "" ;
		
		if (NomLocalisation == ""){
			zErreur += "Veuillez selectionner un NomLocalisation\n" ;
		}
		if (AdresseLocalisation == ""){
			zErreur += "Veuillez selectionner l'AdresseLocalisation\n" ;
		}
		if (CpLocalisation == ""){
			zErreur += "Veuillez selectionner l'CpLocalisation\n" ;
		}
		if (location_villeid == "" || location_villeid == "0"){
			zErreur += "Veuillez indiquer la date du location_villeid\n" ;
		}
		
		
		if (zErreur != ""){
			alert ('Tout les champs sont obligatoires !') ;
		}else{
			document.frmCreationLocalisation.submit();
		}
		
		return false;
	}
	
	
	function getCP_localisation(){
        var ivilleId = jQuery('#location_villeid').val();
        jQuery.get(
                '<?php echo site_url("admin/localisations/getPostalCode_localisation"); ?>' + '/' + ivilleId,
                function (zReponse)
                {
                    jQuery('#CpLocalisation').val(zReponse);


                });
    }
	
</script>
    
    <div id="divFicheAnnonce" class="content" align="center">
	     <br>
	     <a style = "color:black; text-decoration: none;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" value= "retour au menu" id = "btn"/> </a><br/>
         <a style = "color:black; text-decoration: none;" href = "<?php echo site_url("admin/localisations/" ) ; ?>"> <input type = "button" value= "retour à la liste Localisations" id = "btn"/> </a>
         
        <form name="frmCreationLocalisation" id="frmCreationLocalisation" action="<?php if (isset($oLocalisation)) { echo site_url("admin/localisations/modifLocalisation/$location_id"); }else{ echo site_url("admin/localisations/creerLocalisation"); } ?>" method="POST">
		<input type="hidden" name="Localisation[location_id]" id="location_id_id" value="<?php if (isset($oLocalisation)) { echo $oLocalisation->location_id ; } ?>" />
            <h1><?php echo $title ; ?></h1>
			<span><?php if (isset($msg)) { echo $msg ; } ?></span>
            <fieldset>
                <legend>Localisation</legend>
                <table cellpadding="0" cellspacing="0">
                    
					<tr>
                        <td>
                            <label>Nom du local : </label>
                        </td>
                        <td>
                            <input type="text" name="Localisation[location]" id="NomLocalisation" value="<?php if (isset($oLocalisation)) { echo $oLocalisation->location ; } ?>" />
                        </td>
                    </tr>
					<tr>
                        <td>
                            <label>Adresse : </label>
                        </td>
                        <td>
                            <input type="text" name="Localisation[location_address]" id="AdresseLocalisation" value="<?php if (isset($oLocalisation)) { echo $oLocalisation->location_address ; } ?>" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <label>Ville : </label>
                        </td>
                        <td>
                            <select name="Localisation[location_villeid]" id="location_villeid" onChange="javascript:getCP_localisation();">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($colVilles)) { ?>
                                    <?php foreach($colVilles as $objVille_localisation) { ?>
                                        <option <?php if(isset($oLocalisation->location_villeid) && $oLocalisation->location_villeid == $objVille_localisation->IdVille) echo 'selected="selected"';?> value="<?php echo $objVille_localisation->IdVille; ?>"><?php echo $objVille_localisation->Nom; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Code Postal : </label>
                        </td>
                        <td>
                            <input type="text" name="Localisation[location_postcode]" id="CpLocalisation" value="<?php if (isset($oLocalisation)) { echo $oLocalisation->location_postcode ; } ?>" />
                        </td>
                    </tr>
					<tr>
                        <td></td>
                        <td align="left">
                        <input type="button" value="Annuler" onclick="javascript:document.location='<?php echo site_url("admin/localisations");?>';" />&nbsp;
                        <input type="button" value="Valider" onclick="javascript:testFormLocalisation();" /></td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>


<?php $this->load->view("admin/includes/vwFooter2013"); ?>