<?php $data["zTitle"] = "Gestion Type d'agenda"; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra'],  headers: {2: { sorter: false}, 3: {sorter: false}, 4: {sorter: false} }})
				.tablesorterPager({container: $("#pager")});
		});
</script>
    <div id="divAdminHome" class="content" align="center">
	     <p><a style = "color:black; text-decoration:none;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input class="btn btn-primary" type = "button" value= "retour au menu" id = "btn" onclick="document.location='<?php echo site_url("admin/home/" );?>';"/>  </a></p>
		 <p><a style = "color:black;text-decoration:none;" href = "<?php echo site_url("admin/types_agenda/fiche_types_agenda/0") ;?>"> <input class="btn btn-success" type = "button" value= "Ajouter Type agenda" id = "btn"/>  </a></p>
        <br><div class="H1-C">Liste des Types d'agenda</div><br>
        
        <div style="color:#33CC33"><?php if (isset($msg)) echo $msg;?></div>
        <table cellpadding="1" class="tablesorter" style="text-align:left">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nom</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
            <?php if (count($colagendas)>0) { ?>
                <?php foreach($colagendas as $objagenda) { ?>
                    <tr>
                        <td>
                            <a href="<?php echo site_url("admin/types_agenda/fiche_types_agenda/".$objagenda->agenda_typeid); ?>">
                                <?php echo $objagenda->agenda_typeid; ?>
                            </a>
                        </td>
                        <td><?php echo htmlspecialchars(stripcslashes($objagenda->agenda_type)); ?></td>
                        <td>&nbsp;</td>
                        <td>
                            <a href="<?php echo site_url("admin/types_agenda/fiche_types_agenda/".$objagenda->agenda_typeid); ?>">
                                Modifier
                            </a>
                        </td>
                        <td>
                            <a href="<?php echo site_url("admin/types_agenda/delete/".$objagenda->agenda_typeid) ; ?>">
                                Supprimer
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
            </tbody>
        </table>
		<div id="pager" class="pager">
			<img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
			<img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
			<input type="text" class="pagedisplay"/>
			<img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
			<img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
			<select class="pagesize" style="visibility:hidden">
				<option selected="selected"  value="10">10</option>
				<option value="20">20</option>
				<option value="30">30</option>
				<option  value="40">40</option>
			</select>
		</div>
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>