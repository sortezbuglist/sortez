<?php $data["zTitle"] = 'Inscription des professionnels'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra'],  headers: {0: { sorter: false}, 2: { sorter: false}, 3: { sorter: false}, 4: { sorter: false} }})
				.tablesorterPager({container: $("#pager")});
		});
</script>
    <div id="divImagespub" class="content" align="center">
        <form name="frmImagespub" id="frmImagespub" action="" method="POST" enctype="multipart/form-data">
	    <p><a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input class="btn btn-primary" type = "button" value= "retour au menu" id = "btn"/></a></p>
                <br><div class="H1-C">Images de pub (en bas)</div><br>
                    	    <p><a style = "color:black;" href = "<?php echo site_url("admin/imagespub/ficheImagespub") ;?>"> <input class="btn btn-success" type = "button" value= "Nouvelle image" id = "btn"/></a></p>
				</br>
				<div id="container">
					<table cellpadding="1" class="tablesorter">
						<thead>
							<tr>                    
								<th>Images</th>
								<th>Titre</th>
								<th>Active</th>
								<th width="60"></th>
								<th width="60"></th>
							</tr>
						</thead>
						<tbody> 
							<?php foreach($toListeImagespub as $oListeImagespub){ ?>
								<tr>                    
									<td><img src="<?php echo GetImagePath("front/")."/ico".$oListeImagespub->imagespub_image ; ?>"/></td>
									<td><?php echo $oListeImagespub->imagespub_titre ; ?></td>
									<td><input onchange="javascript:window.location.href='<?php echo site_url("admin/imagespub/active/" . $oListeImagespub->imagespub_id ) ; ?>'" type=radio name="activer" <?php if ($oListeImagespub->activer == 1){ echo "checked";} ?> value="<?php echo $oListeImagespub->imagespub_id ; ?>"></td>
									<td><a href="<?php echo site_url("admin/imagespub/ficheImagespub/" . $oListeImagespub->imagespub_id ) ; ?>">Modifier</a></td>
									<td><a href="<?php echo site_url("admin/imagespub/supprimImagespub/" . $oListeImagespub->imagespub_id ) ; ?>" onclick="if (!confirm('voulez-vous vraiment supprimer cette pub?')){ return false ; }">Supprimer</a></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
					<div id="pager" class="pager">
						<img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
						<img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
						<input type="text" class="pagedisplay"/>
						<img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
						<img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
						<select class="pagesize" style="visibility:hidden">
							<option selected="selected"  value="10">10</option>
							<option value="20">20</option>
							<option value="30">30</option>
							<option  value="40">40</option>
						</select>
					</div>
				</div>
        </form>
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>