<?php $data["zTitle"] = 'Import Datatourisme'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

    <div class="text-center">

        <?php
        $CI =& get_instance();
        $CI->load->model("Mdl_datatourisme_agenda");

        $minMax_flux = $CI->Mdl_datatourisme_agenda->getMinMax_flux();
        $totaDatatourisme = count($CI->Mdl_datatourisme_agenda->countAllDatatourisme());
        ?>
        toFirstIdDatatourisme
        <h1>Importer les données Datatourisme</h1>

        <div id="idResultMultipleSend1"></div>
        <div id="idResultMultipleSend2"></div>
        <hr>


        <div style="font-size: 20px;">Totalité des évennements enregistrés dans la base Datatourisme :
            <span style="font-weight: bold;"><?php if (isset($minMax_flux->max_id)) echo $minMax_flux->max_id; ?></span>
        </div>

        <hr>
        <div>
            <button id="import_datatourisme_go" class="btn btn-success">Proceder à l'import de la totalité des données
            </button>
            <div id="idCheckGlobalSendStatus"></div>
        </div>


        <script type="text/javascript">
            var toFirstIdDatatourisme = parseInt(<?php echo $minMax_flux->min_id;?>);
            var toLastIdDatatourisme = parseInt(<?php echo $minMax_flux->max_id;?>);
            var datatourisme_imported = 0;
            var datatourisme_exists = 0;
            var datatourisme_skipped = 0;

            jQuery(document).ready(function () {
                jQuery("#import_datatourisme_go").click(function () {
                    if (confirm("Voulez vous copier l'ensemble des données Datatourisme ?")) {
                        import_datatourisme_func(toFirstIdDatatourisme  , toLastIdDatatourisme);//21103 //toFirstIdDatatourisme
                    }
                });
            });

            function import_datatourisme_func(firstId, lastId) {
                jQuery.ajax({
                    type: "POST",
                    url: "<?php echo site_url("admin/datatourisme/import_datatourisme_func"); ?>",
                    data: 'toFirstIdDatatourisme=' + firstId + '&toLastIdDatatourisme=' + lastId,
                    dataType: "json",
                    success: function (data) {
                        //alert(data.responseText);
                        jQuery("#item_sent_error_nb").html(data.responseText);
                        if(Number.isInteger(data.datatourisme_imported)) datatourisme_imported = datatourisme_imported + parseInt(data.datatourisme_imported);
                        if(Number.isInteger(data.datatourisme_exists)) datatourisme_exists = datatourisme_exists + parseInt(data.datatourisme_exists);
                        if(Number.isInteger(data.datatourisme_skipped)) datatourisme_skipped = datatourisme_skipped + parseInt(data.datatourisme_skipped);
                        jQuery("#item_sent_success_nb").html(datatourisme_imported);
                        //jQuery("#item_sent_exist_nb").html(datatourisme_exists);
                        jQuery("#item_sent_error_nb").html(datatourisme_skipped);
                        if (data.datatourisme_imported=='1') $('#send_multiple_consol').append('OK => ' + data.datatourisme_title + '<br/>');
                        else $('#send_multiple_consol').append('KO => ' + firstId + '<br/>');
                        scroll_bottom_consol();
                        check_percent(firstId, lastId);
                        firstId += 1;
                        console.log(firstId);
                        console.log(data);
                        if (firstId <= lastId) {
                            import_datatourisme_func(firstId, lastId);
                        }
                    },
                    error: function (data) {
                        //alert(data.responseText);
                        jQuery("#item_sent_error_nb").html(data.responseText);
                        if(Number.isInteger(data.datatourisme_imported)) datatourisme_imported = datatourisme_imported + parseInt(data.datatourisme_imported);
                        if(Number.isInteger(data.datatourisme_exists)) datatourisme_exists = datatourisme_exists + parseInt(data.datatourisme_exists);
                        if(Number.isInteger(data.datatourisme_skipped)) datatourisme_skipped = datatourisme_skipped + parseInt(data.datatourisme_skipped);
                        jQuery("#item_sent_success_nb").html(datatourisme_imported);
                        jQuery("#item_sent_exist_nb").html(datatourisme_exists);
                        //jQuery("#item_sent_error_nb").html(datatourisme_skipped);
                        $('#send_multiple_consol').append('KO => ' + firstId + '<br/>');
                        scroll_bottom_consol();
                        check_percent(firstId, lastId);
                        firstId += 1;
                        console.log(firstId);
                        console.log(data);
                        if (firstId <= lastId) {
                            import_datatourisme_func(firstId, lastId);
                        }
                    }
                });
            }

            function check_percent(current, last) {
                //alert('current : '+current+" / last : "+last);
                var percent_value = parseInt(current) * 100 / parseInt(last);
                percent_value = percent_value.toFixed(1);
                //alert(percent_value);
                jQuery("#newsletter_percent_value").css("width", String(percent_value) + "%");
                jQuery("#newsletter_percent_value").html(String(percent_value) + "%");
            }

            function scroll_bottom_consol(){
                var wtf    = jQuery('#send_multiple_consol');
                var height = wtf[0].scrollHeight;
                wtf.scrollTop(height);
            }

        </script>


        <hr>
        <div style="text-align: left">Copie des données Datatourisme</div>
        <div>
            <div class="progress">
                <div id="newsletter_percent_value" class="progress-bar" role="progressbar" aria-valuenow="70"
                     aria-valuemin="0" aria-valuemax="100" style="width:0.9%">
                    0.9%
                </div>
            </div>
        </div>

        <hr>

        <div>
            Article importé avec succès : <span id="item_sent_success_nb" style="color: #1dc116;"></span>
        </div>
        <div>
            Article déjà existant : <span id="item_sent_exist_nb" style="color: #FF0000;"></span>
        </div>
        <div>
            Article non importé : <span id="item_sent_error_nb" style="color: #FF0000;"></span>
        </div>


        <div style="font-weight: bold; color: #FF0000; padding-top: 50px; font-size: 20px;">Note: Ne pas recharger la
            page durant l'envoi !
        </div>
        <hr>

        <div>
            <div id="send_multiple_consol"
                 style="background-color: #000000; color: #ffffff; width: 100%; height: 350px; overflow: scroll; text-align: left;">

            </div>
        </div>
    </div>

<?php $this->load->view("admin/includes/vwFooter2013"); ?>