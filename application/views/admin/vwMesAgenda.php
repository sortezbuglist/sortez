<?php $data["zTitle"] = 'Agenda'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>


    <script type="text/javascript" charset="utf-8">
        $(function () {
            $(".tablesorter")
                .tablesorter({widthFixed: true, widgets: ['zebra'], headers: {7: {sorter: false}, 8: {sorter: false}}})
                .tablesorterPager({container: $("#pager")});
        });
        function confirm_delete_agenda(IdRubrique, IdCommercant){
            if (confirm("Voulez-vous supprimer cet événement définitivement ?")) {
                document.location="<?php echo site_url("admin/agenda/deleteAgenda/");?>/"+IdRubrique+"/"+IdCommercant;
            }
        }
    </script>
    <div id="divMesAnnonces" class="content" align="center">
        <form name="frmMesAnnonces" id="frmMesAnnonces" action="" method="POST" enctype="multipart/form-data">


            <p><span align="left"><a style="text-decoration:none;" href="<?php echo site_url("admin/home"); ?>"><input
                                type="button" id="btnnew" value="retour au menu" class="btn btn-primary"/></a></span></p>


            <?php if (isset($limit_annonce_add) && $limit_annonce_add == 1) {
                echo "Vous avez atteint la limite de 20 événements.";
            } else { ?>
                <p><span align="left">
            <a style="text-decoration:none;" href="<?php echo site_url("admin/agenda/ficheAgenda/"); ?>">
				<input type="button" id="btnnew" value="Ajouter une nouvel événement"
                       onclick="document.location='<?php echo site_url("admin/agenda/ficheAgenda/"); ?>';" class="btn btn-success"/>
				</a></span></p>
            <?php } ?>


            <h1>Admin Agenda</h1>

            <div id="container">
                <table cellpadding="1" class="tablesorter">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Titre</th>
                        <th>Description</th>
                        <th>Date ajout</th>
                        <th>A la Une</th>
                        <th>Etat</th>
                        <th width="15"></th>
                        <th width="15"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (count($toListeAgenda) > 0) { ?>
                        <?php foreach ($toListeAgenda as $oListeAgenda) { ?>
                            <tr>
                                <td><?php echo $oListeAgenda->id; ?></td>
                                <td><?php echo $oListeAgenda->nom_manifestation; ?></td>
                                <td><?php echo truncate(strip_tags($oListeAgenda->description), 80, " ..."); ?></td>

                                <td><?php
                                    if (convertDateWithSlashes($oListeAgenda->date_depot) != "00/00/0000")
                                        echo convert_Sqldate_to_Frenchdate($oListeAgenda->date_depot); ?></td>
                                <td><?php
                                    //echo $oListeAgenda->alaune;
                                    if ($oListeAgenda->alaune != "1") echo "Non"; else echo "Oui";
                                    ?></td>
                                <td><?php
                                    //echo $oListeAgenda->IsActif;
                                    if ($oListeAgenda->IsActif == "1") echo "Activé";
                                    if ($oListeAgenda->IsActif == "0") echo "A valider";
                                    if ($oListeAgenda->IsActif == "2") echo "Annulé";
                                    if ($oListeAgenda->IsActif == "3") echo "Supprimé";
                                    ?></td>
                                <td>
                                    <a href="#<?php //echo site_url("admin/agenda/ficheAgenda/" . $oListeAgenda->IdCommercant . "/" . $oListeAgenda->id); ?>">Modifier</a>
                                </td>
                                <td>
                                    <a href="javascript:void();"
                                       onClick="confirm_delete_agenda(<?php if (isset($oListeAgenda->id)) echo $oListeAgenda->id; else echo '0'; ?>,<?php if (isset($oListeAgenda->IdCommercant)) echo $oListeAgenda->IdCommercant; else echo '0'; ?>);">
                                        Supprimer
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                    </tbody>
                </table>
                <div id="pager" class="pager" style="text-align:center;">
                    <img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
                    <img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
                    <input type="text" class="pagedisplay"/>
                    <img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
                    <img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
                    <select class="pagesize" style="visibility:hidden">
                        <option selected="selected" value="10">10</option>
                        <option value="20">20</option>
                        <option value="30">30</option>
                        <option value="40">40</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>