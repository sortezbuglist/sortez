<?php $data["zTitle"] = "Gestion Pack article Payement"; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>
<?php $this->load->view("packarticle/main_menu", $data); ?>

<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra'],  headers: {2: { sorter: false}, 3: {sorter: false}, 4: {sorter: false} }})
				.tablesorterPager({container: $("#pager")});
		});
</script>
    <div id="divAdminHome" class="content" align="center">
	     <p><a style = "color:black; text-decoration:none;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" value= "retour au menu" id = "btn" onclick="document.location='<?php echo site_url("admin/home/" );?>';"/>  </a></p>
		 <p><a style = "color:black;text-decoration:none;" href = "<?php echo site_url("admin/payement/fiche_payement/0") ;?>"> <input type = "button" value= "Ajouter Payement" id = "btn"/>  </a></p>
        <br><div class="H1-C">Liste des payements</div><br>
        
        <div style="color:#33CC33"><?php if (isset($msg)) echo $msg;?></div>
        <table cellpadding="1" class="tablesorter" style="text-align:left">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>title</th>
                    <th>desc</th>
                    <th>Modification</th>
                    <th>Supréssion</th>
                </tr>
            </thead>
            <tbody>
            <?php if (count($colPaye)>0) { ?>
                <?php foreach($colPaye as $objPaye) { ?>
                    <tr>
                        <td>
                            <a href="<?php  echo site_url("admin/payement/fiche_payement/".$objPaye->id); ?>">
                                <?php echo $objPaye->id; ?>
                            </a>
                        </td>
                        <td> 
                    <?php echo htmlspecialchars(stripcslashes ($objPaye->title)); ?></td>
                        <td><?php echo htmlspecialchars(stripcslashes($objPaye->desc)); ?></td>
                        <td>
                            <a href="<?php if (isset($objPaye->id)) echo site_url("admin/payement/fiche_payement/".$objPaye->id); ?>">
                                Modifier
                            </a>
                        </td>
                        <td>
                            <a href="<?php if(isset($objPaye->id)) echo site_url("admin/payement/delete/".$objPaye->id) ; ?>">
                                Supprimer
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
            </tbody>
        </table>
		<div id="pager" class="pager">
			<img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
			<img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
			<input type="text" class="pagedisplay"/>
			<img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
			<img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
			<select class="pagesize" style="visibility:hidden">
				<option selected="selected"  value="10">10</option>
				<option value="20">20</option>
				<option value="30">30</option>
				<option  value="40">40</option>
			</select>
		</div>
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>