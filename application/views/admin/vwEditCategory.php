<?php $data["zTitle"] = 'Administration'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script type="text/javascript" charset="utf-8">
	$(function() {
				$("#btnsubmit_editcategorie").click(function(){
					
					var txtError = "";
					
					//verify input content
					var inputcateg_editcategorie = $("#inputcateg_editcategorie").val();
					if(inputcateg_editcategorie=="") {
						
						txtError += "- Veuillez indiquer Votre inputcateg_editcategorie <br/>"; 
						//alert("Veuillez indiquer Votre nom");
						$("#inputcateg_editcategorie").css('border-color', 'red');
					} else {
						$("#inputcateg_editcategorie").css('border-color', '#E3E1E2');
					}
					
					
					//alert(txtError);
					if(txtError == "") {
						//alert('ok');
						$("#form_editcategorie").submit();
						//$("#qsdf_x").click();
					}
					
				
				
			});
		});
</script>
      <br>
     
    <div id="divMesAnnonces" class="content" align="center">
                <a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>">Retour au menu</a><br/><br/>
                <a style = "color:black;" href = "<?php echo site_url("admin/commercants/categories" ) ; ?>">Retour à la liste des catégories</a><br/>
                <br>
                <p><div class="H1-C">Editer catégorie</div></p>
				
					
<form action="<?php echo site_url("admin/commercants/savecategorie"); ?>" method="post" enctype="multipart/form-data" id="form_editcategorie" name="form_editcategorie">
<table width="400" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>IdRubrique</td>
    <td>
    <input name="idrubrique_editcategorie" id="idrubrique_editcategorie" type="hidden" value="<?php if (isset($oCategorie) && $oCategorie->IdRubrique != 0)  echo $oCategorie->IdRubrique; else echo 0;?>">
	<?php if (isset($oCategorie) && $oCategorie->IdRubrique != 0)  echo $oCategorie->IdRubrique; else echo 0;?>
    </td>
  </tr>
  <tr>
    <td>Nom</td>
    <td><input name="inputcateg_editcategorie" id="inputcateg_editcategorie" type="text" style="width:300px;" value="<?php if (isset($oCategorie) && $oCategorie->Nom != NULL)  echo $oCategorie->Nom;?>"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
    <a style = "color:black;" href = "<?php echo site_url("admin/commercants/categories" ) ; ?>">
    <input name="annuler_editcategorie" id="annuler_editcategorie" type="button" value="Annuler"></a>&nbsp;
    <input name="btnsubmit_editcategorie" id="btnsubmit_editcategorie" type="button" value="Enregistrer">
    </td>
  </tr>
</table>
</form>
                    
		  
       
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>