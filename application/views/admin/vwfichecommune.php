<?php $data["zTitle"] = 'Creation annonce'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script>
	function testFormCommune(){
	var NomCommune = $("#NomCommune").val () ;
	var CpCommune = $("#CpCommune").val () ;
	var NomsimpleCommune = $("#NomsimpleCommune").val () ;
	var zErreur = "" ;
	
	if (NomCommune == ""){
		zErreur += "Veuillez selectionner un NomCommune\n" ;
	}
	if (CpCommune == ""){
		zErreur += "Veuillez selectionner l'CpCommune\n" ;
	}
	if (NomsimpleCommune == ""){
		zErreur += "Veuillez indiquer la date du NomsimpleCommune\n" ;
	}
	
	
	
	if (zErreur != ""){
		alert ('Tout les champs sont obligatoires !') ;
	}else{
		document.frmCreationCommune.submit();
	}
	
	return false;
}
</script>
    
    <div id="divFicheAnnonce" class="content" align="center">
	     <br>
	     <a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" value= "retour au menu" id = "btn"/> </a>
        <form name="frmCreationCommune" id="frmCreationCommune" action="<?php if (isset($oCommune)) { echo site_url("admin/commune/modifCommune/$id_commune"); }else{ echo site_url("admin/commune"); } ?>" method="POST">
		<input type="hidden" name="commune[id_commune]" id="id_commune" value="<?php if (isset($oCommune)) { echo $oCommune->id_commune ; } ?>" />
            <h1><?php echo $title ; ?></h1>
			<span><?php if (isset($msg)) { echo $msg ; } ?></span>
            <fieldset>
                <legend>Commune</legend>
                <table cellpadding="0" cellspacing="0">
                    
					<tr>
                        <td>
                            <label>Nom Commune : </label>
                        </td>
                        <td>
                            <input type="text" name="commune[N_Com_min]" id="NomCommune" value="<?php if (isset($oCommune)) { echo $oCommune->N_Com_min ; } ?>" />
                        </td>
                    </tr>

					<tr>
                        <td></td>
                        <td align="left">
                        <input type="button" value="Annuler" onclick="javascript:document.location='<?php echo site_url("admin/commune/liste");?>';" />&nbsp;
                        <input type="button" value="Valider" onclick="javascript:testFormCommune();" /></td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>


<?php $this->load->view("admin/includes/vwFooter2013"); ?>