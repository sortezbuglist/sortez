<ul id="menu-accordeon">
    <li><a href="#">Sortez Admin Menu</a>
        <ul>            
            <li><a href="<?php echo site_url("admin/xml/"); ?>">Gestion des Flux RSS</a></li>
            <li><a href="<?php echo site_url("front/webscrapping") ?>">Webscrapping</a></li>
            <li><a href="<?php echo site_url("admin/liste_demande") ?>">Liste des demandes</a></li>
            <li><a href="<?php echo site_url("admin/annuaire/personnalisation"); ?>">Export annuaire</a></li>
            <li><a href="<?php echo site_url("admin/usersadmin/liste"); ?>">Gestion des Administrateurs</a></li>
            <li><a href="<?php echo site_url("admin/associations/liste"); ?>">Gestion des Associations</a></li>
            <li><a href="<?php echo site_url("admin/commercants/liste"); ?>">Gestion des Commer&ccedil;ants</a></li>
            <li><a href="<?php echo site_url("admin/commercants/listederniereinscrit"); ?>">Liste des derniers inscriptions non validés</a></li>
            <li><a href="<?php echo site_url("admin/vivresaville"); ?>">Gestion Vivresaville.fr</a></li>
            <li><a href="<?php echo site_url("admin/users/liste"); ?>">Gestion des Particuliers</a></li>
            <li><a href="<?php echo site_url("admin/cadeau/liste"); ?>">Gestion des cadeaux</a></li>
            <li><a href="<?php echo site_url("admin/parametres/fiche/particulier"); ?>">Confirmation d'inscription particulier</a></li>
            <li><a href="<?php echo site_url("admin/parametres/fiche/pro"); ?>">Confirmation d'inscription pro</a></li>
            <li><a href="<?php echo site_url("admin/newsletters"); ?>">Newsletter</a></li>
            <li><a href="<?php echo site_url("admin/newsletters_mails"); ?>">Liste de Prospects</a></li>
            <li><a href="<?php echo site_url("admin/annonce"); ?>">CRUD Annonces</a></li>
            <!--<li><a href="<?php echo site_url("admin/bonplan"); ?>">VALIDATION BonPlans</a></li>-->
            <li><a href="<?php echo site_url("admin/mail_activation_bon_plan"); ?>">Modification mail validation Bon plan</a></li>
            <li><a href="<?php echo site_url("admin/imagespub"); ?>">CRUD images pub</a></li>
            <li><a href="<?php echo site_url("admin/parametres/fiche/logo"); ?>">Gestion du logo dans le header</a></li>
            <li><a href="<?php echo site_url("admin/villes/liste"); ?>">CRUD Villes</a></li>
            <li><a href="<?php echo site_url("admin/commercants/categories"); ?>">CRUD Catégories partenaires</a></li>
            <li><a href="<?php echo site_url("admin/manage_abonnement/"); ?>">Offre Abonnement & Tarifs</a></li>
            <li><a href="<?php echo site_url("admin/manage_abonnement/validation"); ?>">Validation Abonnement Commercant</a></li>
            <li><a href="<?php echo site_url("admin/pack_objet"); ?>">CRUD Pack articles object</a></li>
            <li><a href="<?php echo site_url("admin/payement"); ?>">CRUD Pack articles payment</a></li>
            <li><a href="<?php echo site_url("admin/planing"); ?>">CRUD Pack articles planing</a></li>
            <!--<li><a href="<?php echo site_url("admin/categories_article"); ?>">CRUD Catégories d'articles</a></li>-->
            <li><a href="<?php echo site_url("admin/articles/index"); ?>">CRUD Articles</a></li>
            <li><a href="<?php echo site_url("admin/localisations"); ?>">CRUD Localisation</a></li>
            <li><a href="<?php echo site_url("admin/agenda/index"); ?>">CRUD AGENDA</a></li>
            <li><a href="<?php echo site_url("admin/categories_agenda"); ?>">CRUD Catégories articles - agenda</a></li>
            <li><a href="<?php echo site_url("admin/types_agenda"); ?>">CRUD Type d'agenda/d'article/revue</a></li>
            <!-- <li><a href="#">CRUD Modules Professionnels</a></li> -->
            <li><a href="<?php echo site_url("admin/packarticle"); ?>">PACKARTICLE</a></li>
            <!-- <li><a href="<?php //echo site_url("admin/csv"); ?>">IMPORT CSV TO COMMERCANT  </a></li> -->
            <!--<li><a  href="<?php// echo site_url("admin/test"); ?>">Mazo</a></li>!-->
            <!--<li><a  href="<?php //echo site_url("admin/manokana"); ?>">Nomena</a>!-->
           <li><a href="javascript:void(0);" onclick="document.location = '<?php echo site_url("connexion/sortir"); ?>';">Déconnexion</a></li>
        </ul>
    </li>
</ul>
<style type="text/css">
    #menu-accordeon {
        padding:0 0 0 15px;
        margin:15px 0 0 0;
        list-style:none;
        text-align: center;
        width: 350px;
    }
    #menu-accordeon ul {
        padding:0;
        margin:0;
        list-style:none;
        text-align: center;
    }
    #menu-accordeon li {
        background-color:#729EBF;
        background-image:-webkit-linear-gradient(top, #729EBF 0%, #333A40 100%);
        background-image: linear-gradient(to bottom, #729EBF 0%, #333A40 100%);
        border-radius: 6px;
        margin-bottom:2px;
        box-shadow: 3px 3px 3px #999;
        border:solid 1px #333A40
    }
    #menu-accordeon li li {
        max-height:0;
        overflow: hidden;
        transition: all .5s;
        border-radius:0;
        background: #444;
        box-shadow: none;
        border:none;
        margin:0
    }
    #menu-accordeon a {
        display:block;
        text-decoration: none;
        color: #fff;
        padding: 8px 0;
        font-family: verdana;
        font-size:1.2em
    }
    #menu-accordeon ul li a, #menu-accordeon li:hover li a {
        font-size:1em
    }
    #menu-accordeon li:hover {
        background: #729EBF
    }
    #menu-accordeon li li:hover {
        background: #999;
    }
    #menu-accordeon ul li:last-child {
        border-radius: 0 0 6px 6px;
        border:none;
    }
    #menu-accordeon li:hover li {
        max-height: 15em;
    }
</style>


    
