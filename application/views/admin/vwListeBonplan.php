<?php $data["zTitle"] = 'Liste BonPlan'; ?>
<?php $this->load->view("front/includes/vwHeader", $data); ?>
<?php $this->load->view("front/includes/vwMenu"); ?>
<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra'],  headers: {7: { sorter: false}, 8: {sorter: false} }})
				.tablesorterPager({container: $("#pager")});
		});
</script>
      <br>
     <a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" value= "retour au menu" id = "btn"/> </a>
    <div id="divMesAnnonces" class="content" align="center">
        <form name="frmMesAnnonces" id="frmMesAnnonces" action="" method="POST" enctype="multipart/form-data">
                                 <a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" value= "retour au menu" id = "btn"/></a>
                <br>
                <div class="H1-C">Liste des BonPlans</div><br>
				</br>
                                 <br><a style = "color:black;" href = "<?php echo site_url("admin/bonplan/ficheBonplan/0") ;?>"> <input type = "button" value= "Nouveau Bonplan" id = "btn"/></a>
				<div id="container">
					<table cellpadding="1" class="tablesorter">
						<thead>
							<tr>                    
								<th>Titre</th>
								<th>Description long</th>
								<th>Commercant</th>
								<th>Utilisation</th>
								<th></th>
								<th></th>
                                <th>PUBLICATION</th>
							</tr>
						</thead>
						<tbody> 
							<?php foreach($toListeMesBonplan as $oListeMesBonplan){ ?>
								<tr>                    
									<td><?php echo $oListeMesBonplan->bonplan_titre ; ?></td>
									<td><?php echo truncate($oListeMesBonplan->bonplan_texte,50,$etc = " ...") ; ?></td>
									<td><?php echo $oListeMesBonplan->NomSociete ; ?></td>
									<td><?php if ($oListeMesBonplan->bon_plan_utilise_plusieurs == 0) { echo "Utilisable une fois" ; }else { echo "Toujours utilisable" ;} ?></td>
									<td><a href="<?php echo site_url("admin/bonplan/ficheBonplan/" . $oListeMesBonplan->bonplan_commercant_id . "/" . $oListeMesBonplan->bonplan_id ) ; ?>">Modifier</a></td>
									<td><a href="<?php echo site_url("admin/bonplan/supprimBonplan/" . $oListeMesBonplan->bonplan_id . "/" . $oListeMesBonplan->bonplan_commercant_id) ; ?>" onclick="if (!confirm('voulez-vous vraiment supprimer cette annoce?')){ return false ; }">Supprimer</a></td>
                                    <td align="right"><?php if ($oListeMesBonplan->publication == 0) { echo '<span style="color:#F00;">A VALIDER</span>' ; }else { echo '<span style="color:#090;">Valid&eacute;</span>' ;} ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
					<div id="pager" class="pager">
						<img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
						<img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
						<input type="text" class="pagedisplay"/>
						<img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
						<img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
						<select class="pagesize" style="visibility:hidden">
							<option selected="selected"  value="10">10</option>
							<option value="20">20</option>
							<option value="30">30</option>
							<option  value="40">40</option>
						</select>
					</div>
				</div>
        </form>
    </div>
<?php $this->load->view("front/includes/vwFooter"); ?>