<?php $data["zTitle"] = 'Gestion des cadeaux'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script>
		$(function() {
			$( "#txtDateDebut" ).datepicker({ dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
											 monthNames: ['Janvier','Fevier','Mars','Avril','Mai','Juin','Juiller','Aout','Septembre','Octobre','Novembre','Decembre'],
											 dateFormat: 'yy-mm-dd',
											 autoSize: true});
			$( "#txtDateFin" ).datepicker({ dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
											monthNames: ['Janvier','Fevier','Mars','Avril','Mai','Juin','Juiller','Aout','Septembre','Octobre','Novembre','Decembre'],
											dateFormat: 'yy-mm-dd'});
		});
	</script>

<form id="frmCadeau" name="frmCadeau" enctype="multipart/form-data" method="post" action= "<?php echo site_url("admin/cadeau/save/" ) ;?> ">
    <div id="divAdminHome" class="content" align="center">
	    <br>
	    <a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" value= "retour au menu" id = "btn" onclick="document.location='<?php echo site_url("admin/home/" ) ; ?>'"/> </a>
        <h1>GESTION DES CADEAUX </h1>
        

  <table style="text-align:left">
   <tr> 
    <td><label> Nom de l'offre :</label> </td>
    <td> 
	
	<input type = "hidden" id = "Idcadeau" name = "Idcadeau" value = "<?php if(isset($cadeau)) echo $cadeau->Idcadeau ;?>" /> 
	<input type = "text" id = "txtNom" name = "txtNom" value = "<?php if(isset($cadeau)) echo $cadeau->Nomoffre ;?>" /> </td>
   </tr>
   
    <tr> 
    <td> <label> Commentaire :</label> </td>
    <td> <input type = "text" id = "txtComment" name = "txtComment" value = "<?php if(isset($cadeau)) echo $cadeau->Commentaire ;?>" /> </td>
   </tr>
   
    <tr> 
    <td><label> Nombre de points ce cadeau :</label> </td>
    <td> <input type = "text" id = "txtPoint" name = "txtPoint" value = "<?php if(isset($cadeau)) echo $cadeau->valeurpoints ;?>" /> </td>
   </tr>
   
    <tr> 
    <td><label>Date de d&eacute;but de validité : </label> </td>
    <td> <input type = "text" id = "txtDateDebut" name = "txtDateDebut" value = "<?php if(isset($cadeau)) echo $cadeau->Valabledebut ;?>" /> </td>
   </tr>
   
    <tr> 
    <td><label> Date de fin de validité : </label> </td>
    <td> <input type = "text" id = "txtDateFin" name = "txtDateFin" value = "<?php if(isset($cadeau)) echo $cadeau->Valablefin ;?>" /> </td>
   </tr>
   
    <tr> 
    <td><label> Nombre de cadeaux disponibles : </label> </td>
    <td> <input type = "text" id = "txtNbDispo" name = "txtNbDispo" value = "<?php if(isset($cadeau)) echo $cadeau->Qtedisponible ;?>" /> </td>
   </tr>
   
    <tr> 
    <td><label>Image du cadeau : </label> </td>
    <td> <input type = "file" id = "txtImage" name = "txtImage" />
	      
	     <input type = "hidden" id = "hidImage" name = "hidImage"  value="<?php if(isset($cadeau)) { echo $cadeau->Imagecadeau ; }?>"/> 
		 <div> <img src = "<?php if(isset($cadeau))  { echo GetImagePath("front/"); ?>/cadeaux/<?php echo $cadeau->Imagecadeau ; }?> " alt = ""> </div>
	     
	</td>
   </tr>
   
    <tr> 
    <td><label>Date de cr&eacute;ation de la fiche </label> </td>
    <td> <input type = "text" id = "" readonly name = "txtDateCrea" value = "<?php if(isset($cadeau)) { echo $cadeau->Ajoutele; } else { echo date("Y-m-d"); } ?>" /> </td>
   </tr>
   <br><br>
  </table>
  <input type = "submit" name= "Valider" id="Valider" value="Valider"/>
                          
 <input type="button" value="D&eacute;connexion" onclick="document.location = '<?php echo site_url("connexion/sortir"); ?>';" />
    </div>
</form>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>