<?php $data["zTitle"] = "Gestion Type d'Article"; ?>
<?php $this->load->view("privicarte/includes/main_header", $data); ?>
<?php $this->load->view("privicarte/includes/main_navbar", $data); ?>

<style type="text/css">
    .container{
        background-color: white;
    }
    body{
        margin-top: 0px;
    }
</style>
<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra'],  headers: {2: { sorter: false}, 3: {sorter: false}, 4: {sorter: false} }})
				.tablesorterPager({container: $("#pager")});
		});
</script>
    <div id="divAdminHome" class="content pt-5" align="center">
	     <p><a style = "color:black; text-decoration:none;" href = "<?php echo site_url('auth/login')?>"> <input class="btn btn-success" type = "button" value= "retour au menu" id = "btn"/>  </a></p>

        <br><div class="h3 text-capitalize" style="text-decoration: underline">Liste des commentaire</div><br>
        <div class="content" align="center">
      <br>
            <div class="row p-4">

               <div class="col-3">
                        <label for="is_actif_livre_or">Activer le system Livre d'or:</label>
               </div>
            <div class="col-4">
                <form method="post" action="<?php echo site_url('front/Gestion_livre_dor/update_livre_actif')?>">
                    <input type="hidden" value="<?php echo $infocom->IdCommercant; ?>" name="gli[IdCommercant]">
                        <select  class="form-control" name="gli[is_actif_livre_or]" id="is_actif_livre_or">
                            <option value="0">Non</option>
                            <option value="1" <?php if (isset($glis->is_actif_livre_or) AND $glis->is_actif_livre_or==1 ){echo "selected"; } ?>>Oui</option>
                        </select>
                    <button class="btn btn-info mt-4">Valider</button>
                </form>
            </div>
            </div>
         <div style="color:#33CC33"><?php if (isset($msg)) echo $msg;?></div>
        <table  class="table">
            <thead class="thead-default">
                <tr>
                    <th>Id</th>
                    <th>Nom</th>
                    <th>Email</th>
                    <th>Message</th>
                    <th>Modifier</th>
                    <th>Supprimer</th>


                </tr>
            </thead>
            <tbody class="text-body">
            <?php if (count($collivredor)>0) { ?>
                <?php foreach($collivredor as $objlivredor) { ?>
                    <tr>
                        <td>
                            <a href="<?php echo site_url("front/Gestion_livre_dor//".$objlivredor->id); ?>">
                                <?php echo $objlivredor->id; ?>
                            </a>
                        </td>
                        <td><?php echo htmlspecialchars(stripcslashes($objlivredor->nom)); ?></td>
                        <td><?php echo $objlivredor->mail; ?></td>
                        <td><?php echo $objlivredor->message; ?></td>
                        <td>
                            <a class="btn btn-info" href="<?php echo site_url("front/Gestion_livre_dor/fiche_commentaire/".$objlivredor->id); ?>">
                                Modifier
                            </a>
                        </td>
                        <td>
                            <a class="btn btn-danger" href="<?php echo site_url("front/Gestion_livre_dor/delete/".$objlivredor->id.'/'.$idcom) ; ?>">
                                Supprimer
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
            </tbody>
        </table>
		<div id="pager" class="pager">
			<img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
			<img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
			<input type="text" class="pagedisplay"/>
			<img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
			<img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
			<select class="pagesize" style="visibility:hidden">
				<option selected="selected"  value="10">10</option>
				<option value="20">20</option>
				<option value="30">30</option>
				<option  value="40">40</option>
			</select>
		</div>
    </div>
    </div>
</div>
</div>
