<?php $data["zTitle"] = "Gestion Type d'Article"; ?>
<?php $this->load->view("privicarte/includes/main_header", $data); ?>
<?php $this->load->view("privicarte/includes/main_navbar", $data); ?>

<style type="text/css">
    .container{
        background-color: white;
        height: 100%!important;
    }
    body{
        margin-top: 0px;
    }
</style>
<script type="text/javascript" charset="utf-8">
    $(function() {
        $(".tablesorter")
            .tablesorter({widthFixed: true, widgets: ['zebra'],  headers: {2: { sorter: false}, 3: {sorter: false}, 4: {sorter: false} }})
            .tablesorterPager({container: $("#pager")});
    });
</script>
<div id="divAdminHome" class="content pt-5" align="center" style="height: 500px">
    <p><a style = "color:black; text-decoration:none;" href = "<?php echo site_url('auth/login')?>"> <input class="btn btn-success" type = "button" value= "retour au menu" id = "btn"/>  </a></p>

    <br><div class="h3 text-capitalize" style="text-decoration: underline">Liste des commentaire</div><br>
    <form method="post" action="<?php echo site_url("front/Gestion_livre_dor/save_actif_not_comment")?>">
        <div class="row">
            <div class="col-3"></div>
            <label class="col-3 text-info" for="select_comment_google">Activer commentaire google:</label>
            <input type="hidden" name="com_google[idcommercant]" value="<?php echo $infocom->IdCommercant ?? 0 ?>">
            <select class="col-3 form-control pr-4" id="select_comment_google" name="com_google[IsActifCommentGoogle]">
                <option value="0" class="text-info">Non</option>
                <option <?php if (isset($infocom->IsActifCommentGoogle) AND $infocom->IsActifCommentGoogle=='1')echo 'selected'?> value="1" class="text-info">Oui</option>
            </select>
            <div class="col-3"></div>
        </div>
        <div class="row p-4">
            <div class="col-3"></div>
            <div class="col-3 text-center text-info"><label for="url_comment">Saisir le lien complet</label></div>

            <input class="col-3 form-control" id="url_comment" type="url" name="com_google[link_comment_google]" value="<?php if (isset($infocom->link_comment_google))echo $infocom->link_comment_google; ?>">

            <div class="col-3"></div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center text-info"><input type="submit" class="btn btn-success" value="Valider"></div>
        </div>
    </form>
</div>
</div>
</div>
