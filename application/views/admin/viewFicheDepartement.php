<!--test modif-->
<!--test modif-->
<?php $data["zTitle"] = 'Creation annonce'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script>
	function testFormDepartement(){
	var NomDepartement = $("#NomDepartement").val () ;
	var CpDepartement = $("#CpDepartement").val () ;
	var NomsimpleDepartement = $("#NomsimpleDepartement").val () ;
	var zErreur = "" ;
	
	if (NomDepartement == ""){
		zErreur += "Veuillez selectionner un NomDepartement\n" ;
	}
	if (CpDepartement == ""){
		zErreur += "Veuillez selectionner l'CpDepartement\n" ;
	}
	if (NomsimpleDepartement == ""){
		zErreur += "Veuillez indiquer la date du NomsimpleDepartement\n" ;
	}
	
	
	
	if (zErreur != ""){
		alert ('Tout les champs sont obligatoires !') ;
	}else{
		document.frmCreationDepartement.submit();
	}
	
	return false;
}
</script>
    
    <div id="divFicheAnnonce" class="content" align="center">
	     <br>
	     <a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" value= "retour au menu" id = "btn"/> </a>
        <form name="frmCreationDepartement" id="frmCreationDepartement" action="<?php if (isset($oDepartement)) { echo site_url("admin/departement/modifDepartement/$departement_id"); }else{ echo site_url("admin/departement"); } ?>" method="POST">
		<input type="hidden" name="departement[departement_id]" id="departement_id" value="<?php if (isset($oDepartement)) { echo $oDepartement->departement_id ; } ?>" />
            <h1><?php echo $title ; ?></h1>
			<span><?php if (isset($msg)) { echo $msg ; } ?></span>
            <fieldset>
                <legend>Departement</legend>
                <table cellpadding="0" cellspacing="0">
                    
					<tr>
                        <td>
                            <label>Nom Departement : </label>
                        </td>
                        <td>
                            <input type="text" name="departement[departement_nom]" id="NomDepartement" value="<?php if (isset($oDepartement)) { echo $oDepartement->departement_nom ; } ?>" />
                        </td>
                    </tr>

					<tr>
                        <td></td>
                        <td align="left">
                        <input type="button" value="Annuler" onclick="javascript:document.location='<?php echo site_url("admin/departement/liste");?>';" />&nbsp;
                        <input type="button" value="Valider" onclick="javascript:testFormDepartement();" /></td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>


<?php $this->load->view("admin/includes/vwFooter2013"); ?>