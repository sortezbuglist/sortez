<?php $data["zTitle"] = "Gestion Pack article Objets"; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>
<?php $this->load->view("packarticle/main_menu", $data); ?>

<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra'],  headers: {2: { sorter: false}, 3: {sorter: false}, 4: {sorter: false} }})
				.tablesorterPager({container: $("#pager")});
		});
</script>
    <div id="divAdminHome" class="content" align="center">
	     <p><a style = "color:black; text-decoration:none;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" value= "retour au menu" id = "btn" onclick="document.location='<?php echo site_url("admin/home/" );?>';"/>  </a></p>
		 <p><a style = "color:black;text-decoration:none;" href = "<?php echo site_url("admin/pack_objet/fiche_pack_objet/0") ;?>"> <input type = "button" value= "Ajouter Objets" id = "btn"/>  </a></p>
        <br><div class="H1-C">Liste des Objets</div><br>
        
        <div style="color:#33CC33"><?php if (isset($msg)) echo $msg;?></div>
        <table cellpadding="1" class="tablesorter" style="text-align:left">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>title</th>
                    <th>desc</th>
                    <th>Modification</th>
                    <th>Supréssion</th>
                </tr>
            </thead>
            <tbody>
            <?php if (count($colObjet)>0) { ?>
                <?php foreach($colObjet as $objObjet) { ?>
                    <tr>
                        <td>
                            <a href="<?php  echo site_url("admin/pack_objet/fiche_pack_objet/".$objObjet->id); ?>">
                                <?php echo $objObjet->id; ?>
                            </a>
                        </td>
                        <td> 
                    <?php echo htmlspecialchars(stripcslashes ($objObjet->title)); ?></td>
                        <td><?php echo htmlspecialchars(stripcslashes($objObjet->desc)); ?></td>
                        <td>
                            <a href="<?php if (isset($objObjet->id)) echo site_url("admin/pack_objet/fiche_pack_objet/".$objObjet->id); ?>">
                                Modifier
                            </a>
                        </td>
                        <td>
                            <a href="<?php if(isset($objObjet->id)) echo site_url("admin/pack_objet/delete/".$objObjet->id) ; ?>">
                                Supprimer
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
            </tbody>
        </table>
		<div id="pager" class="pager">
			<img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
			<img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
			<input type="text" class="pagedisplay"/>
			<img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
			<img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
			<select class="pagesize" style="visibility:hidden">
				<option selected="selected"  value="10">10</option>
				<option value="20">20</option>
				<option value="30">30</option>
				<option  value="40">40</option>
			</select>
		</div>
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>