<?php $data["zTitle"] = 'Inscription des professionnels'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>


<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra'],  headers: {7: { sorter: false}, 8: {sorter: false} }})
				.tablesorterPager({container: $("#pager")});
		});
</script>
    <div id="divMesAnnonces" class="content" align="center">
        <form name="frmMesAnnonces" id="frmMesAnnonces" action="" method="POST" enctype="multipart/form-data">
                <p><a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input class="btn btn-primary" type = "button" value= "retour au menu" id = "btn"/></a></p>
                <br><div class="H1-C">Liste des annonces</div><br>
				</br>
                                 <br><a style = "color:black;" href = "<?php echo site_url("admin/annonce/ficheAnnonce/0") ;?>"> <input class="btn btn-success" type = "button" value= "Nouvelle annonce" id = "btn"/></a>
				<div id="container">
					<table cellpadding="1" class="tablesorter">
						<thead>
							<tr>                    
								<th>Description court</th>
								<th>Description long</th>
								<th>Date debut</th>
								<th>Date fin</th>
								<th>Prix neuf</th>
								<th>Prix solde</th>
								<th>Etat</th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody> 
							<?php foreach($toListeMesAnnonce as $oListeMesAnnonce){ ?>
								<tr>                    
									<td><?php echo $oListeMesAnnonce->annonce_description_courte ; ?></td>
                                    <td><?php echo truncate(strip_tags($oListeMesAnnonce->annonce_description_longue),100," ...") ; ?></td>
									<td><?php echo convert_Sqldate_to_Frenchdate($oListeMesAnnonce->annonce_date_debut) ; ?></td>
									<td><?php echo convert_Sqldate_to_Frenchdate($oListeMesAnnonce->annonce_date_fin) ; ?></td>
									<td><?php echo $oListeMesAnnonce->annonce_prix_neuf ; ?></td>
									<td><?php echo $oListeMesAnnonce->annonce_prix_solde ; ?></td>
									<td><?php if ($oListeMesAnnonce->annonce_etat == 0) { echo "Occasion" ; }else { echo "Occasion" ;} ?></td>
									<td><a href="<?php echo site_url("admin/annonce/ficheAnnonce/" . $oListeMesAnnonce->annonce_commercant_id . "/" . $oListeMesAnnonce->annonce_id ) ; ?>">Modifier</a></td>
									<td><a href="<?php echo site_url("admin/annonce/supprimAnnonce/" . $oListeMesAnnonce->annonce_id . "/" . $oListeMesAnnonce->annonce_commercant_id) ; ?>" onclick="if (!confirm('voulez-vous vraiment supprimer cette annoce?')){ return false ; }">Supprimer</a></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
					<div id="pager" class="pager">
						<img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
						<img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
						<input type="text" class="pagedisplay"/>
						<img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
						<img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
						<select class="pagesize" style="visibility:hidden">
							<option selected="selected"  value="10">10</option>
							<option value="20">20</option>
							<option value="30">30</option>
							<option  value="40">40</option>
						</select>
					</div>
				</div>
        </form>
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>