<?php $data["zTitle"] = 'Creation imagespub'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>  
    <div id="divFicheimagespub" class="content" align="center">
	     <br>
	     <a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" value= "retour au menu" id = "btn"/> </a>
        <form name="frmCreationimagespub" id="frmCreationimagespub" action="<?php if (isset($oImagespub)) { echo site_url("admin/imagespub/modifImagespub"); }else{ echo site_url("admin/imagespub/creerImagespub"); } ?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="imagespub[imagespub_id]" id="Idimagespub_id" value="<?php if (isset($oImagespub)) { echo $oImagespub->imagespub_id ; } ?>" />
            <h1><?php echo $title ; ?></h1>
			<span><?php if (isset($msg)) { echo $msg ; } ?></span>
            <fieldset>
                <legend>Images pub</legend>
                <table width="100%" cellpadding="0" cellspacing="0">
					<tr>
                        <td>
                            <label>Titre : </label>
                        </td>
                        <td>
                            <input type="text" name="imagespub[imagespub_titre]" id="IdTitrePub" value="<?php if (isset($oImagespub)) { echo $oImagespub->imagespub_titre ; } ?>" />
                        </td>
                    </tr>
					
                    <tr>
                        <td>
                            <label>Image : </label>
                        </td>
                        <td>
							<input type="hidden" name="imagespub[imagespub_image]" id="IdPhoto1" value="<?php if (isset($oImagespub)) { echo $oImagespub->imagespub_image ; } ?>" />
                            <input type="file" name="imagespubPhoto1" id="imagespubPhoto1" value="" onchange="javascript:UploadImage ('<?php echo site_url("admin/imagespub/uploadPub/");?>', 'imagespubPhoto1', '1');"/><img src="<?php echo GetImagePath("front/"); ?>/loading.gif" id="loading1" style="display:none"/>
                        </td>
                    </tr>
					<!--tr>
						<td>&nbsp;</td>
                        <td><img src="<?php echo GetImagePath("front/"); ?>/<?php if (isset($oImagespub) && $oImagespub->imagespub_image != ""){ echo $oImagespub->imagespub_image ; }else{ echo "no-image.jpg" ; } ?>" id="img01" style="border: 1px solid #e1e1e1; padding: 3px; margin: 0px 20px 2px 0px;" />
						</td>
					</tr-->
					<tr>
                        <td></td>
                        <td align="left"><input type="submit" value="Annuler" name="cmdCancel"/>&nbsp;<input type="button" value="Valider" onclick="javascript:testFormImagespub();" /></td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>