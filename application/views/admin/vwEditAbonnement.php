<?php $data["zTitle"] = 'Administration'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.js"></script>

<script type="text/javascript" charset="utf-8">
	$(function() {
				$("#btnsubmit_editabonnement").click(function(){
					
					var txtError = "";
					
					//verify input content
					var inputcateg_editabonnement = $("#inputcateg_editabonnement").val();
					if(inputcateg_editabonnement=="") {
						
						txtError += "- Veuillez indiquer Votre inputcateg_editabonnement <br/>"; 
						//alert("Veuillez indiquer Votre nom");
						$("#inputcateg_editabonnement").css('border-color', 'red');
					} else {
						$("#inputcateg_editabonnement").css('border-color', '#E3E1E2');
					}
					
					var inputtarif_editabonnement = $("#inputtarif_editabonnement").val();
					if(inputtarif_editabonnement=="" || isPositiveInteger(inputtarif_editabonnement)==false) {
						
						txtError += "- Veuillez indiquer Votre inputtarif_editabonnement <br/>"; 
						//alert("Veuillez indiquer Votre nom");
						$("#inputtarif_editabonnement").css('border-color', 'red');
					} else {
						$("#inputtarif_editabonnement").css('border-color', '#E3E1E2');
					}
					
					
					//alert(txtError);
					if(txtError == "") {
						//alert('ok');
						$("#form_editabonnement").submit();
						//$("#qsdf_x").click();
					}
					
				
				
			});
		});
    jQuery(document).ready(function () {

   $("#delete_img_module").click(function () {
       var id=$("#idrubrique_editabonnement").val();
       if (id != 0){
        var datas='id='+id+'&image_name='+'<?php echo $toAbonnement->image ?? ''; ?>';
           jQuery.ajax({
               url: '<?php echo base_url();?>admin/manage_abonnement/delete_image_module/',
               dataType: 'text',
               type: 'POST',
               async: true,
               data:datas,
               success: function (data) {
                   if (data ==='ok'){
                       alert('Image supprimé');
                       window.location.reload();
                   }else if (data ==='ok'){
                       alert('Une erreur s\'est produite');
                       console.log(data);
                   }

               },
               error: function (data) {
                  alert('Une erreur s\'est produite');
                   console.log(data);
               }
           });
       }
   });

    });



</script>
      <br>
     
    <div id="divMesAnnonces" class="content" align="center">
                <a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>">Retour au menu</a><br/>
                <a style = "color:black;" href = "<?php echo site_url("admin/manage_abonnement/" ) ; ?>">Retour à la liste des abonnements</a>
                <p><div class="H1-C">Editer Abonnement</div><br/><br/></p>
				</br>
				
					
<form action="<?php echo site_url("admin/manage_abonnement/save_abonnement"); ?>" method="post"  enctype="multipart/form-data" id="form_editabonnement" name="form_editabonnement">
<table width="400" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Id</td>
    <td>
    <input name="idrubrique_editabonnement" id="idrubrique_editabonnement" type="hidden" value="<?php if (isset($toAbonnement) && $toAbonnement->IdAbonnement != 0)  echo $toAbonnement->IdAbonnement; else echo 0;?>">
	<?php if (isset($toAbonnement) && $toAbonnement->IdAbonnement != 0)  echo $toAbonnement->IdAbonnement; else echo 0;?>
    </td>
  </tr>
  <tr>
    <td>Nom</td>
    <td><input name="inputcateg_editabonnement" id="inputcateg_editabonnement" type="text" style="width:300px;" value="<?php if (isset($toAbonnement) && $toAbonnement->Nom != NULL)  echo $toAbonnement->Nom;?>"></td>
  </tr>
  <tr>
    <td>Tarif en &euro;</td>
    <td><input name="inputtarif_editabonnement" id="inputtarif_editabonnement" type="text" style="width:300px;" value="<?php if (isset($toAbonnement) && $toAbonnement->tarif != NULL)  echo $toAbonnement->tarif?>"></td>
  </tr>
  <tr>
    <td>Description</td>
    <td>
    <textarea name="inputdesc_editabonnement" id="inputdesc_editabonnement" type="text" style="width:300px;"><?php if (isset($toAbonnement) && $toAbonnement->Description != NULL)  echo $toAbonnement->Description;?></textarea>
    </td>
  </tr>
   <tr>
    <td>Type</td>
    <td>
    <select name="inputtype_editabonnement" id="inputtype_editabonnement">
    	<option value="gratuit" <?php if (isset($toAbonnement) && $toAbonnement->type == "gratuit") { ?>selected <?php }?> >gratuit</option>
        <option value="premium" <?php if (isset($toAbonnement) && $toAbonnement->type == "premium") { ?>selected <?php }?> >premium</option>
        <option value="platinum" <?php if (isset($toAbonnement) && $toAbonnement->type == "platinum") { ?>selected <?php }?> >platinum</option>
    </select>
    </td>
  </tr>
    <tr>
        <td>Image</td>
        <td>
            <?php if (isset($toAbonnement->image) AND file_exists('application/resources/front/images/image_module/'.$toAbonnement->image.'.png')){ ?>
            <img src="<?php echo base_url('application/resources/front/images/image_module/'.$toAbonnement->image.'.png')?>" width="200px" height="auto"><a href="#" class="btn btn-danger" id="delete_img_module">Supprimer</a>
            <?php }elseif(isset($toAbonnement->image) AND file_exists('application/resources/front/images/image_module/'.$toAbonnement->image.'.jpg')){?>

                <img src="<?php echo base_url('application/resources/front/images/image_module/'.$toAbonnement->image.'.jpg')?>" width="200px" height="auto"><a href="#" class="btn btn-danger" id="delete_img_module">Supprimer</a>

            <?php }else{?>
            <input type="file" name="image" id="image" value=""/><img src="<?php echo GetImagePath("front/"); ?>/loading.gif" id="loading1" style="display:none"/>
            <?php }?>
        </td>
    </tr>
  <tr>
    <td>
    </td>
    <td>
    <a style = "color:black;" href = "<?php echo site_url("admin/manage_abonnement/" ) ; ?>">
    <input name="annuler_editabonnement" id="annuler_editabonnement" type="button" value="Annuler"></a>&nbsp;
    <input name="btnsubmit_editabonnement" id="btnsubmit_editabonnement" type="button" value="Enregistrer">
    </td>
  </tr>
</table>
</form>
                    
		  
       
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>