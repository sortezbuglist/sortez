<?php $data["zTitle"] = "Gestion vivre sa ville"; ?>
<?php $this->load->view("admin/vivresaville/includes/main_header", $data); ?>

    <link href="css/global.css" rel="stylesheet" type="text/css">
    <div class="row justify-content-center text-center">
        <div class="col-12 p-0">
            <h1><?php if (isset($oville->name_ville)) echo $oville->name_ville; else { ?>LA GESTION DE VIVRESAVILLE.FR<?php } ?></h1>
        </div>
        <div class="col-12 p-0">
            <a href="<?php echo site_url("admin/vivresaville"); ?>" class="btn vsv_color mt-3">Retour à la liste des
                villes</a>
        </div>
    </div>


    <div class="row justify-content-center text-center">
        <div class="col-8 p-3">
            <a href="<?php echo site_url("admin/vivresaville/home_mgt/" . $obj_item->id); ?>" class="btn btn-success">Gestion
                de la page d'accueil</a>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-8 p-0">


            <form id="vsv_form_ville" name="vsv_form_ville" method="POST" enctype="multipart/form-data">

                <div class="col vsv_title_bar vsv_color">Contenus de la page d'accueil</div>

                <div class="col p-0 vsv_image_rubrique_block pt-5">
                    <div class="row">
                        <div class="col-12 mb-3 border">
                            <div class="col-12">IMAGE BANNIERE ANNUAIRE</div>
                            <div class="col-12">
                                <?php
                                if (isset($oville->home_photo_banner) && $oville->home_photo_banner != "0" && $oville->home_photo_banner != "" && file_exists("application/resources/front/images/vivresaville/" . $oville->home_photo_banner)) { ?>
                                    <img
                                        src="<?php echo base_url(); ?>application/resources/front/images/vivresaville/<?php echo $oville->home_photo_banner; ?>"
                                        style="width: 100%" class="mt-3"/>
                                    <input type="button" id="btn_delete_home_photo_banner" class="btn btn-danger d-block mt-3"
                                           onClick="javascript:remove_vsv_home_photo_banner(<?php echo $oville->id; ?>);"
                                           value="Supprimer"/>
                                <?php } else { ?>
                                    <input type="file" id="vsv_home_photo_banner_file" name="vsv_home_photo_banner_file"
                                           class="form-control-file"/>
                                <?php } ?>
                                <input type="hidden" name="vsv_object[home_photo_banner]" id="vsv_home_photo_banner_associe"
                                       value="<?php if (isset($oville->home_photo_banner)) echo $oville->home_photo_banner; ?>"/>
                                <span id="vsv_home_photo_banner_file_associe_span"></span>
                            </div>
                        </div>
                        <div class="col-4 mb-3 border">
                            <div class="col-12">IMAGE ARTICLE</div>
                            <div class="col-12">
                                <?php
                                if (isset($oville->home_photo_article) && $oville->home_photo_article != "0" && $oville->home_photo_article != "" && file_exists("application/resources/front/images/vivresaville/" . $oville->home_photo_article)) { ?>
                                    <img
                                        src="<?php echo base_url(); ?>application/resources/front/images/vivresaville/<?php echo $oville->home_photo_article; ?>"
                                        style="width: 100%" class="mt-3"/>
                                    <input type="button" id="btn_delete_home_photo_article" class="btn btn-danger d-block mt-3"
                                           onClick="javascript:remove_vsv_home_photo_article(<?php echo $oville->id; ?>);"
                                           value="Supprimer"/>
                                <?php } else { ?>
                                    <input type="file" id="vsv_home_photo_article_file" name="vsv_home_photo_article_file"
                                           class="form-control-file"/>
                                <?php } ?>
                                <input type="hidden" name="vsv_object[home_photo_article]" id="vsv_home_photo_article_file_associe"
                                       value="<?php if (isset($oville->home_photo_article)) echo $oville->home_photo_article; ?>"/>
                                <span id="vsv_home_photo_article_file_associe_span"></span>
                            </div>
                        </div>
                        <div class="col-4 border">
                            <div class="col-12">IMAGE AGENDA</div>
                            <div class="col-12">
                                <?php if (isset($oville->home_photo_agenda) && $oville->home_photo_agenda != "0" && $oville->home_photo_agenda != "" && file_exists("application/resources/front/images/vivresaville/" . $oville->home_photo_agenda)) { ?>
                                    <img
                                        src="<?php echo base_url(); ?>application/resources/front/images/vivresaville/<?php echo $oville->home_photo_agenda; ?>"
                                        style="width: 100%" class="mt-3"/>
                                    <input type="button" id="btn_delete_home_photo_agenda" class="btn btn-danger d-block mt-3"
                                           onClick="javascript:remove_vsv_home_photo_agenda(<?php echo $oville->id; ?>);"
                                           value="Supprimer"/>
                                <?php } else { ?>
                                    <input type="file" id="vsv_home_photo_agenda_file" name="vsv_home_photo_agenda_file" class="form-control-file"/>
                                <?php } ?>
                                <input type="hidden" name="vsv_object[home_photo_agenda]" id="vsv_home_photo_agenda_file_associe"
                                       value="<?php if (isset($oville->home_photo_agenda)) echo $oville->home_photo_agenda; ?>"/>
                                <span id="vsv_home_photo_agenda_file_associe_span"></span>
                            </div>
                        </div>
                        <div class="col-4 border">
                            <div class="col-12">IMAGE ANNONCE</div>
                            <div class="col-12">
                                <?php if (isset($oville->home_photo_annonce) && $oville->home_photo_annonce != "0" && $oville->home_photo_annonce != "" && file_exists("application/resources/front/images/vivresaville/" . $oville->home_photo_annonce)) { ?>
                                    <img
                                        src="<?php echo base_url(); ?>application/resources/front/images/vivresaville/<?php echo $oville->home_photo_annonce; ?>"
                                        style="width: 100%" class="mt-3"/>
                                    <input type="button" id="btn_delete_home_photo_annonce" class="btn btn-danger d-block mt-3"
                                           onClick="javascript:remove_vsv_home_photo_annonce(<?php echo $oville->id; ?>);"
                                           value="Supprimer"/>
                                <?php } else { ?>
                                    <input type="file" id="vsv_home_photo_annonce_file" name="vsv_home_photo_annonce_file" class="form-control-file"/>
                                <?php } ?>
                                <input type="hidden" name="vsv_object[home_photo_annonce]" id="vsv_home_photo_annonce_file_associe"
                                       value="<?php if (isset($oville->home_photo_annonce)) echo $oville->home_photo_annonce; ?>"/>
                                <span id="vsv_home_photo_annonce_file_associe_span"></span>
                            </div>
                        </div>
                        <div class="col-6 border mb-3 mt-3">
                            <div class="col-12">IMAGE BONPLAN</div>
                            <div class="col-12">
                                <?php if (isset($oville->home_photo_bonplan) && $oville->home_photo_bonplan != "0" && $oville->home_photo_bonplan != "" && file_exists("application/resources/front/images/vivresaville/" . $oville->home_photo_bonplan)) { ?>
                                    <img
                                        src="<?php echo base_url(); ?>application/resources/front/images/vivresaville/<?php echo $oville->home_photo_bonplan; ?>"
                                        style="width: 100%" class="mt-3"/>
                                    <input type="button" id="btn_delete_home_photo_bonplan" class="btn btn-danger d-block mt-3"
                                           onClick="javascript:remove_vsv_home_photo_bonplan(<?php echo $oville->id; ?>);"
                                           value="Supprimer"/>
                                <?php } else { ?>
                                    <input type="file" id="vsv_home_photo_bonplan_file" name="vsv_home_photo_bonplan_file" class="form-control-file"/>
                                <?php } ?>
                                <input type="hidden" name="vsv_object[home_photo_bonplan]" id="vsv_home_photo_bonplan_file_associe"
                                       value="<?php if (isset($oville->home_photo_bonplan)) echo $oville->home_photo_bonplan; ?>"/>
                                <span id="vsv_home_photo_bonplan_file_associe_span"></span>
                            </div>
                        </div>
                        <div class="col-6 border mb-3 mt-3">
                            <div class="col-12">IMAGE FIDELITE</div>
                            <div class="col-12">
                                <?php if (isset($oville->home_photo_fidelite) && $oville->home_photo_fidelite != "0" && $oville->home_photo_fidelite != "" && file_exists("application/resources/front/images/vivresaville/" . $oville->home_photo_fidelite)) { ?>
                                    <img
                                        src="<?php echo base_url(); ?>application/resources/front/images/vivresaville/<?php echo $oville->home_photo_fidelite; ?>"
                                        style="width: 100%" class="mt-3"/>
                                    <input type="button" id="btn_delete_home_photo_fidelite" class="btn btn-danger d-block mt-3"
                                           onClick="javascript:remove_vsv_home_photo_fidelite(<?php echo $oville->id; ?>);"
                                           value="Supprimer"/>
                                <?php } else { ?>
                                    <input type="file" id="vsv_home_photo_fidelite_file" name="vsv_home_photo_fidelite_file" class="form-control-file"/>
                                <?php } ?>
                                <input type="hidden" name="vsv_object[home_photo_fidelite]" id="vsv_home_photo_fidelite_file_associe"
                                       value="<?php if (isset($oville->home_photo_fidelite)) echo $oville->home_photo_fidelite; ?>"/>
                                <span id="vsv_home_photo_fidelite_file_associe_span"></span>
                            </div>
                        </div>
                        <div class="col-12 border mb-3 mt-3">
                            <div class="col-12">IMAGE BANIERE DU BAS</div>
                            <div class="col-12">
                                <?php if (isset($oville->home_photo_annuaire) && $oville->home_photo_annuaire != "0" && $oville->home_photo_annuaire != "" && file_exists("application/resources/front/images/vivresaville/" . $oville->home_photo_annuaire)) { ?>
                                    <img
                                        src="<?php echo base_url(); ?>application/resources/front/images/vivresaville/<?php echo $oville->home_photo_annuaire; ?>"
                                        style="width: 100%" class="mt-3"/>
                                    <input type="button" id="btn_delete_home_photo_annuaire" class="btn btn-danger d-block mt-3"
                                           onClick="javascript:remove_vsv_home_photo_annuaire(<?php echo $oville->id; ?>);"
                                           value="Supprimer"/>
                                <?php } else { ?>
                                    <input type="file" id="vsv_home_photo_annuaire_file" name="vsv_home_photo_annuaire_file" class="form-control-file"/>
                                <?php } ?>
                                <input type="hidden" name="vsv_object[home_photo_annuaire]" id="vsv_home_photo_annuaire_file_associe"
                                       value="<?php if (isset($oville->home_photo_annuaire)) echo $oville->home_photo_annuaire; ?>"/>
                                <span id="vsv_home_photo_annuaire_file_associe_span"></span>
                            </div>
                        </div>
                        <div class="col-12 form-group">
                            <label for="vsv_description">Desctiption : </label>
                    <textarea class="form-control" id="vsv_home_description" name="vsv_object[home_description]"
                              rows="3"><?php if (isset($oville->home_description)) echo $oville->home_description; ?></textarea>
                        </div>
                    </div>
                </div>

                <div class="col p-0 vsv_submit_btn_block pt-5 pb-5">
                    <div class="row justify-content-center">
                        <div class="col-8 pb-5">
                            <a href="javascript:void(0);" id="vsv_submit_home_btn" onclick="javascript:vsv_form_ville_home_fn();"
                               class="btn w-100 btn-success">ENREGISTRER</a>
                            <input type="hidden" id="vsv_id" name="vsv_object[id]"
                                   value="<?php if (isset($oville->id)) echo $oville->id; else echo "0"; ?>">
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-8 pb-5" id="vsv_form_submit_error">

                        </div>
                    </div>
                </div>


            </form>


        </div>
    </div>

<?php $this->load->view("admin/vivresaville/includes/main_footer"); ?>