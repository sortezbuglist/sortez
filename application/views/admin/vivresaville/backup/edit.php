<?php $data["zTitle"] = "Gestion vivre sa ville"; ?>
<?php $this->load->view("admin/vivresaville/includes/main_header", $data); ?>
    <link href="css/global.css" rel="stylesheet" type="text/css">
    <div class="row justify-content-center text-center">
        <div class="col-12 p-0">
            <h1><?php if (isset($oville->name_ville)) echo $oville->name_ville; else { ?>LA GESTION DE VIVRESAVILLE.FR<?php } ?></h1>
        </div>
        <div class="col-12 p-0">
            <a href="<?php echo site_url("admin/vivresaville");?>" class="btn vsv_color mt-3">Retour à la liste des villes</a>
        </div>
    </div>


    <div class="row justify-content-center text-center">
        <div class="col-8 p-3">
            <a href="<?php echo site_url("admin/vivresaville/home_mgt/".$oville->id);?>" class="btn btn-success">Gestion de la page d'accueil</a>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-8 p-0">


            <form id="vsv_form_ville" name="vsv_form_ville" method="POST" enctype="multipart/form-data">

                <div class="col vsv_title_bar vsv_color">Création et modification d'une ville</div>
            <div class="col p-0">
                <div class="row">
                    <div class="col-6">Nom de la ville</div>
                    <div class="col-6"><input type="text" id="vsv_name_ville" name="vsv_object[name_ville]"
                                              class="form-control"
                                              value="<?php if (isset($oville->name_ville)) echo $oville->name_ville; ?>"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">Code postal de référence</div>
                    <div class="col-6"><input type="text" id="vsv_postalcode_main" name="vsv_object[postalcode_main]"
                                              class="form-control"
                                              value="<?php if (isset($oville->postalcode_main)) echo $oville->postalcode_main; ?>"
                                              onkeyup="javascript:vsv_getDepartement_list();vsv_getVille_list();"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">Département</div>
                    <div class="col-6" id="vsv_departementCP_container">
                        <select id="vsv_id_department" name="vsv_object[id_department]" class="form-control p-1"
                                onchange="javascript:vsv_getVille_list();">
                            <option value="0">Choisir le département</option>
                            <?php if (sizeof($colDepartement)) { ?>
                                <?php foreach ($colDepartement as $objDepartement) { ?>
                                    <option
                                        <?php if (isset($oville->id_department) && $oville->id_department == $objDepartement->departement_id) echo "selected"; ?>
                                        value="<?php echo $objDepartement->departement_id; ?>"><?php echo $objDepartement->departement_nom; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">Ville de référence</div>
                    <div class="col-6" id="vsv_villeCP_container">
                        <input type="text"
                               value="<?php if (isset($oville->id_ville)) echo $this->mdlville->getVilleById($oville->id_ville)->Nom; ?>"
                               name="IdVille_Nom_text" id="IdVille_Nom_text" disabled="disabled"
                               class="form-control"/>
                        <input type="hidden"
                               value="<?php if (isset($oville->id_ville)) echo $oville->id_ville; ?>"
                               name="vsv_object[id_ville]" id="vsv_id_ville"/>
                    </div>
                </div>

                <!--
                <div class="row">Téritoire additionnel</div>
                <div class="row">
                    <div class="col-4">
                        <div class="col p-0">Code postal</div>
                        <div class="col p-0">
                            <input type="text" id="vsv_postalcode_<?php //echo $ii;?>" name="vsv_object[postalcode_<?php //echo $ii;?>]"
                                   class="form-control"
                                   value="<?php //if (isset($oville->postalcode_1)) echo $oville->postalcode_1; ?>"/>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="row">
                            <div class="col p-0">Ville de référence</div>
                            <div class="col p-0" id="vsv_villeCP_container">
                                <input type="text"
                                       value="<?php //if (isset($oville->id_ville)) echo $this->mdlville->getVilleById($oville->id_ville)->Nom; ?>"
                                       name="IdVille_Nom_text" id="IdVille_Nom_text" disabled="disabled"
                                       class="form-control"/>
                                <input type="hidden"
                                       value="<?php //if (isset($oville->id_ville)) echo $oville->id_ville; ?>"
                                       name="vsv_object[id_ville]" id="vsv_id_ville"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="col p-0">Identification</div>
                        <div class="col p-0">
                            <input type="text" id="vsv_postalcode1" name="vsv_object[postalcode1]"
                                   class="form-control"
                                   value="<?php //if (isset($oville->postalcode1)) echo $oville->postalcode1; ?>"/>
                        </div>
                    </div>
                </div>
                -->

                <div class="row pl-3 mt-5">
                    <div class="col-6 vsv_modal_container">
                        <a href="javascript:void(0);" id="vsv_add_territoire" attachmentid="<?php echo base_url();?>vivresaville/page/edit_autre_ville/<?php if (isset($oville->id)) echo $oville->id; ?>/0" class="btn btn-primary vsv_modal_link">Ajouter un térritoire additionnel</a>
                    </div>
                    <div class="col-6 text-right">
                        <a href="javascript:void(0);" id="vsv_refresh_territoire" class="btn btn-primary">Actualiser la liste</a>
                    </div>
                </div>

                <div class="row pb-5" id="vsv_ville_other_container">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Code postal</th>
                            <th>Identification</th>
                            <th>Ville référencée</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 1;
                        if (isset($ville_data)) {
                            foreach ($ville_data as $item) { ?>
                                <tr>
                                    <th scope="row"><?php echo $i; ?></th>
                                    <td><?php echo $item->postal_code; ?></td>
                                    <td><?php echo $item->name; ?></td>
                                    <td></td>
                                    <td class="">
                                        <div class="vsv_modal_container">
                                            <a href="javascript:void(0);"
                                               id="edit_autre_ville_<?php echo $i;?>"
                                               attachmentid="<?php echo base_url(); ?>vivresaville/page/edit_autre_ville/<?php if (isset($item->id_vsv)) echo $item->id_vsv; else echo "0"; ?>/<?php if (isset($item->id)) echo $item->id; else echo "0"; ?>"
                                               class="btn btn-primary vsv_modal_link">Modifier</a>
                                        </div>
                                    </td>
                                    <td class="">
                                        <div class="vsv_modal_container">
                                            <a href="javascript:void(0);"
                                               id="delete_autre_ville_<?php echo $i;?>"
                                               attachmentid="<?php echo base_url(); ?>vivresaville/page/delete_autre_ville/<?php if (isset($item->id_vsv)) echo $item->id_vsv; else echo "0"; ?>/<?php if (isset($item->id)) echo $item->id; else echo "0"; ?>"
                                               class="btn btn-primary vsv_modal_link">Supprimer</a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                        } ?>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="col p-0 vsv_subdomain_block">
                <div class="col vsv_title_bar grey_bg_color">Création et modification d'une ville</div>
                <div class="col p-0">
                    <div class="row">
                        <div class="col-2">titre du site</div>
                        <div class="col-10"><input type="txt" id="vsv_site_title" name="vsv_object[site_title]"
                                                   class="form-control"
                                                   value="<?php if (isset($oville->site_title)) echo $oville->site_title; ?>"/>
                        </div>
                        <div class="col-12 pt-3">Le titre apparait dans les résultats de recherche et dans la basse de titre du navigateur</div>
                        <div class="col-2 pt-3">URL Vivresaville.fr</div>
                        <div class="col-10 pt-3 input-group">
                            <input type="text" id="vsv_subdomain" name="vsv_object[subdomain]" class="form-control"
                                   placeholder="votre-nom-de-domaine" aria-describedby="basic-addon2"
                                   value="<?php if (isset($oville->subdomain)) echo $oville->subdomain; ?>"/>
                            <span class="input-group-addon" id="basic-addon2">.vivresaville.fr</span>
                        </div>
                        <div class="col-12 pt-3">Indiquer une URL Vivresaville.fr et envoyer le nous pour enregistrement.</div>
                        <div class="col-6 pt-3">
                            <a href="javascript:void(0);" class="btn w-100 vsv_color">Vérrfication</a>
                        </div>
                        <div class="col-6 pt-3">
                            <a href="javascript:void(0);" class="btn w-100 vsv_color">Envoyer pour enregistrement</a>
                        </div>
                    </div>
                </div>
            </div>
                <div class="col p-0 vsv_meta_contents pt-5 pb-5">
                <div class="form-group">
                    <label for="vsv_description">Desctiption du site : Quelques phrase décrivant votre page. Elles sont
                        aussi utilisées lorsque votre page est partagée sur Facebook</label>
                    <textarea class="form-control" id="vsv_description" name="vsv_object[description]"
                              rows="3"><?php if (isset($oville->description)) echo $oville->description; ?></textarea>
                </div>
                <div class="form-group">
                    <label for="vsv_keywords">Mots-clés du site : Une liste de mots-clés séparés par des virgules pour
                        votre page. Que doit-on chercher pour trouver cette page ?</label>
                    <textarea class="form-control" id="vsv_keywords" name="vsv_object[keywords]"
                              rows="3"><?php if (isset($oville->keywords)) echo $oville->keywords; ?></textarea>
                </div>
            </div>

                <div class="row">
                    <div class="col pb-4">
                        <label>
                        Activation de la Ville :
                        <input type="checkbox" id="vsv_admin_isactive_check" name="vsv_admin_isactive_check" onchange="javascript:vsv_admin_isactive_check_func();" <?php if (isset($oville->isactive) && $oville->isactive=="1") { ?>checked<?php } ?> data-toggle="toggle" data-size="large">
                        </label>
                        <input type="hidden" id="vsv_admin_isactive" name="vsv_object[isactive]" value="<?php if (isset($oville->isactive)) echo $oville->isactive; ?>">
                    </div>
                </div>

                <div class="col p-0 vsv_qrcode_content grey_bg_color pb-5 mt-3 mb-3">
                    <div class="col vsv_title_bar grey_bg_color">Création d'un QRCODE</div>
                <div class="row">
                    <div class="col-4 text-center">
                        <img src="<?php echo base_url(); ?>assets/ville-test/wpimages/vsv_qrcode.png" width="131"
                             height="131" alt=""/></div>
                    <div class="col-8">
                        <div class="col-12">Préciser l'URL de votre page</div>
                        <div class="col-12"><input type="txt" id="vsv_subdomain_url" class="form-control"
                                                   value="<?php if (isset($oville->subdomain_url)) echo $oville->subdomain_url; ?>"
                                                   name="vsv_object[subdomain_url]"/></div>
                        <div class="col-12">
                            <p>
                                Par défaut l'url correspondante à votre page d'accueil est enregistrée. Le QRCODE est automatiquement généré et se retrouve sur l'ensemble de vos pages "Nos infos sur mobile".<br/>
                                Si vous optez pour un sous-dmaine ou une url personnalisée, n'oubliez pas d'intégrer l'adresse et de valider le nouveau QRCODE
                            </p>
                        </div>
                        <div class="col-12">
                            <a href="#" class="btn w-100 vsv_color">Créer le QRCODE</a>
                        </div>
                    </div>
                </div>
            </div>
                <div class="col p-0 vsv_image_rubrique_block pt-5">
                    <div class="row">
                    <div class="col-6 mb-3">
                        <div class="col-12">IMPORT DU LOGO DE LA VILLE</div>
                        <div class="col-12">
                            <?php
                            if (isset($oville->logo) && $oville->logo!="0" && $oville->logo!="" && file_exists("application/resources/front/images/vivresaville/".$oville->logo)) { ?>
                                    <img src="<?php echo base_url();?>application/resources/front/images/vivresaville/<?php echo $oville->logo;?>" style="width: 150px" class="mt-3"/>
                                    <input type="button" id="btn_delete_logo" class="btn btn-danger d-block mt-3" onClick="javascript:remove_vsv_logo(<?php echo $oville->id;?>);" value="Supprimer"/>
                            <?php } else { ?>
                                <input type="file" id="vsv_logo_file" name="vsv_logo_file" class="form-control-file"/>
                            <?php } ?>
                            <input type="hidden" name="vsv_object[logo]" id="vsv_logo_file_associe"
                                   value="<?php if (isset($oville->logo)) echo $oville->logo; ?>"/>
                            <span id="vsv_logo_file_associe_span"></span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="col-12">IMPORT DE L'IMAGE DE FOND</div>
                        <div class="col-12">
                            <?php if (isset($oville->background) && $oville->background!="0" && $oville->background!="" && file_exists("application/resources/front/images/vivresaville/".$oville->background)) { ?>
                                    <img src="<?php echo base_url();?>application/resources/front/images/vivresaville/<?php echo $oville->background;?>" style="width: 150px" class="mt-3"/>
                                    <input type="button" id="btn_delete_background" class="btn btn-danger d-block mt-3" onClick="javascript:remove_vsv_background(<?php echo $oville->id;?>);" value="Supprimer"/>
                            <?php } else { ?>
                                <input type="file" id="vsv_bg_file" name="vsv_bg_file" class="form-control-file"/>
                            <?php } ?>
                            <input type="hidden" name="vsv_object[background]" id="vsv_bg_file_associe"
                                   value="<?php if (isset($oville->background)) echo $oville->background; ?>"/>
                            <span id="vsv_background_file_associe_span"></span>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col p-0 vsv_image_rubrique_block pt-5">
                    <div class="row">
                        <div class="col-6">Lien actualité Mairie</div>
                        <div class="col-6"><input type="text" id="vsv_link_actu_mairie"
                                                  name="vsv_object[link_actu_mairie]" class="form-control" placeholder="http://"
                                                  value="<?php if (isset($oville->link_actu_mairie)) echo $oville->link_actu_mairie; ?>"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">Lien agenda Mairie</div>
                        <div class="col-6"><input type="text" id="vsv_link_agenda_mairie"
                                                  name="vsv_object[link_agenda_mairie]" class="form-control" placeholder="http://"
                                                  value="<?php if (isset($oville->link_agenda_mairie)) echo $oville->link_agenda_mairie; ?>"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">Lien page accueil</div>
                        <div class="col-6"><input type="text" id="vsv_link_home" name="vsv_object[link_home]"
                                                  class="form-control" placeholder="http://"
                                                  value="<?php if (isset($oville->link_home)) echo $oville->link_home; ?>"/>
                        </div>
                    </div>
                    <div class="row pt-3">
                        <div class="col-6">Lien publicité</div>
                        <div class="col-6"><input type="text" id="vsv_pub_link" name="vsv_object[pub_link]"
                                                  class="form-control" placeholder="http://"
                                                  value="<?php if (isset($oville->pub_link)) echo $oville->pub_link; ?>"/>
                        </div>
                    </div>
                </div>
                <div class="col p-0 vsv_submit_btn_block pt-5 pb-5">
                    <div class="row justify-content-center">
                        <div class="col-8 pb-5">
                            <a href="javascript:void(0);" id="vsv_submit_btn" onclick="javascript:vsv_form_ville_fn();"
                               class="btn w-100 btn-success">ENREGISTRER</a>
                            <input type="hidden" id="vsv_id" name="vsv_object[id]"
                                   value="<?php if (isset($oville->id)) echo $oville->id; else echo "0"; ?>">
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-8 pb-5" id="vsv_form_submit_error">

                        </div>
                    </div>
            </div>


            </form>





        </div>
    </div>

<?php $this->load->view("admin/vivresaville/includes/modal_window"); ?>

<script type="application/javascript">
    $(document).ready(function (){
        $( "#vsv_refresh_territoire" ).click(function() {
            $("#vsv_ville_other_container").html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            location.reload();
            /*$.post(
                '<?php echo base_url();?>vivresaville/page/liste_autre_ville/',
                {
                    id_vivresaville:'<?php if (isset($oville->id)) echo $oville->id; else echo 0; ?>'
                }
                ,
                function (zReponse)
                {
                    $("#vsv_ville_other_container").html(zReponse);
                }
            );*/
        });
    });
</script>

<?php $this->load->view("admin/vivresaville/includes/main_footer"); ?>