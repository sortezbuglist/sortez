jQuery( document ).ready(function() {

});
function vsv_admin_isactive_check_func(){
    if ($("#vsv_admin_isactive_check").is(":checked")) {
        $("#vsv_admin_isactive").val('1');
    } else {
        $("#vsv_admin_isactive").val('0');
    }
}
function vsv_form_ville_fn(){
    var txtError = "";

    var vsv_name = jQuery("#vsv_name_ville").val();
    //alert(vsv_name);
    if(vsv_name == "") {
        txtError += "- Veuillez indiquer le nom de la Ville.<br/>";
        $("#vsv_name_ville").css('border-color', 'red');
        $("#vsv_name_ville").css('color', 'red');
    } else {
        $("#vsv_name_ville").css('border-color', '#E3E1E2');
        $("#vsv_name_ville").css('color', '#495057');
    }

    var vsv_postalcode_main = jQuery("#vsv_postalcode_main").val();
    if(vsv_postalcode_main == "") {
        txtError += "- Veuillez indiquer le code postal de référence.<br/>";
        $("#vsv_postalcode_main").css('border-color', 'red');
        $("#vsv_postalcode_main").css('color', 'red');
    } else {
        $("#vsv_postalcode_main").css('border-color', '#E3E1E2');
        $("#vsv_postalcode_main").css('color', '#495057');
    }

    var vsv_id_department = jQuery("#vsv_id_department").val();
    if(vsv_id_department == "" || vsv_id_department == "0") {
        txtError += "- Veuillez indiquer le département.<br/>";
        $("#vsv_id_department").css('border-color', 'red');
        $("#vsv_id_department").css('color', 'red');
    } else {
        $("#vsv_id_department").css('border-color', '#E3E1E2');
        $("#vsv_id_department").css('color', '#495057');
    }

    var vsv_id_ville = jQuery("#vsv_id_ville").val();
    if(vsv_id_ville == "" || vsv_id_ville == "0") {
        txtError += "- Veuillez indiquer la ville de référence.<br/>";
        $("#vsv_id_ville").css('border-color', 'red');
        $("#vsv_id_ville").css('color', 'red');
    } else {
        $("#vsv_id_ville").css('border-color', '#E3E1E2');
        $("#vsv_id_ville").css('color', '#495057');
    }

    var vsv_site_title = jQuery("#vsv_site_title").val();
    if(vsv_site_title == "" || vsv_site_title == "0") {
        txtError += "- Veuillez indiquer un titre au site.<br/>";
        $("#vsv_site_title").css('border-color', 'red');
        $("#vsv_site_title").css('color', 'red');
    } else {
        $("#vsv_site_title").css('border-color', '#E3E1E2');
        $("#vsv_site_title").css('color', '#495057');
    }

    if(txtError == "") {
        jQuery("#vsv_form_submit_error").hide();
        jQuery("#vsv_form_ville").submit();
    } else {
        jQuery("#vsv_form_submit_error").html(txtError);
        window.location.hash = '#vsv_form_submit_error';
    }


}

function vsv_form_ville_home_fn(){
    jQuery("#vsv_form_submit_error").hide();
    jQuery("#vsv_form_ville").submit();
}
