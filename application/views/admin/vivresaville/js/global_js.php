<script type="text/javascript">

    function vsv_getDepartement_list() {
        jQuery('#vsv_departementCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        var vsv_postalcode_main = jQuery('#vsv_postalcode_main').val();
        jQuery.post(
            '<?php echo site_url("admin/vivresaville/getDepartmentList"); ?>',
            {vsv_postalcode_main:vsv_postalcode_main},
            function (zReponse)
            {
                jQuery('#vsv_departementCP_container').html(zReponse);
            });
    }

    function vsv_getVille_list() {
        jQuery('#vsv_villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        var vsv_postalcode_main = jQuery('#vsv_postalcode_main').val();
        var vsv_id_department = jQuery('#vsv_id_department').val();
        jQuery.post(
            '<?php echo site_url("admin/vivresaville/getVIlleList"); ?>',
            {
                vsv_postalcode_main:vsv_postalcode_main,
                vsv_id_department:vsv_id_department
            },
            function (zReponse)
            {
                jQuery('#vsv_villeCP_container').html(zReponse);
            });
    }

    function remove_vsv_logo(vsv_id=0) {
        jQuery('#vsv_logo_file_associe_span').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        jQuery.post(
            '<?php echo site_url("admin/vivresaville/remove_vsv_logo"); ?>',
            {vsv_id:vsv_id},
            function (zReponse)
            {
                if (zReponse=="error")
                jQuery('#vsv_logo_file_associe_span').html("Une erreur est surenue, veuillez refaire l'opération !");
                else location.reload();
            });
    }
    function remove_vsv_logo_droite(vsv_id=0) {
        jQuery('#vsv_logo_droite_file_associe_span').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        jQuery.post(
            '<?php echo site_url("admin/vivresaville/remove_vsv_logo_droite"); ?>',
            {vsv_id:vsv_id},
            function (zReponse)
            {
                if (zReponse=="error")
                jQuery('#vsv_logo_file_associe_span').html("Une erreur est surenue, veuillez refaire l'opération !");
                else location.reload();
            });
    }
    function remove_vsv_home_photo_site_web(vsv_id=0) {
        jQuery('#vsv_home_photo_site_web_file_associe_span').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        jQuery.post(
            '<?php echo site_url("admin/vivresaville/remove_vsv_home_photo_site_web"); ?>',
            {vsv_id:vsv_id},
            function (zReponse)
            {
                if (zReponse=="error")
                jQuery('#vsv_home_photo_site_web_file_associe_span').html("Une erreur est surenue, veuillez refaire l'opération !");
                else location.reload();
            });
    }
    function remove_vsv_home_photo_articleb(vsv_id=0) {
        jQuery('#vsv_home_photo_articleb_file_associe_span').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        jQuery.post(
            '<?php echo site_url("admin/vivresaville/remove_vsv_home_photo_articleb"); ?>',
            {vsv_id:vsv_id},
            function (zReponse)
            {
                if (zReponse=="error")
                jQuery('#vsv_home_photo_articleb_file_associe_span').html("Une erreur est surenue, veuillez refaire l'opération !");
                else location.reload();
            });
    }
    function remove_vsv_home_photo_revue(vsv_id=0) {
        jQuery('#vsv_home_photo_revue_file_associe_span').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        jQuery.post(
            '<?php echo site_url("admin/vivresaville/remove_vsv_home_photo_revue"); ?>',
            {vsv_id:vsv_id},
            function (zReponse)
            {
                if (zReponse=="error")
                jQuery('#vsv_home_photo_revue_file_associe_span').html("Une erreur est surenue, veuillez refaire l'opération !");
                else location.reload();
            });
    }
    function remove_vsv_home_photo_bonnes_adresses(vsv_id=0) {
        jQuery('#vsv_home_photo_bonnes_adresses_file_associe_span').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        jQuery.post(
            '<?php echo site_url("admin/vivresaville/remove_vsv_home_photo_bonnes_adresses"); ?>',
            {vsv_id:vsv_id},
            function (zReponse)
            {
                if (zReponse=="error")
                jQuery('#vsv_home_photo_bonnes_adresses_file_associe_span').html("Une erreur est surenue, veuillez refaire l'opération !");
                else location.reload();
            });
    }
    function remove_vsv_home_photo_commerce(vsv_id=0) {
        jQuery('#vsv_home_photo_commerce_file_associe_span').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        jQuery.post(
            '<?php echo site_url("admin/vivresaville/remove_vsv_home_photo_commerce"); ?>',
            {vsv_id:vsv_id},
            function (zReponse)
            {
                if (zReponse=="error")
                jQuery('#vsv_home_photo_commerce_file_associe_span').html("Une erreur est surenue, veuillez refaire l'opération !");
                else location.reload();
            });
    }
    function remove_vsv_home_photo_form(vsv_id=0) {
        jQuery('#vsv_home_photo_form_file_associe_span').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        jQuery.post(
            '<?php echo site_url("admin/vivresaville/remove_vsv_home_photo_form"); ?>',
            {vsv_id:vsv_id},
            function (zReponse)
            {
                if (zReponse=="error")
                jQuery('#vsv_home_photo_form_file_associe_span').html("Une erreur est surenue, veuillez refaire l'opération !");
                else location.reload();
            });
    }
    function remove_vsv_home_photo_deal(vsv_id=0) {
        jQuery('#vsv_home_photo_deal_file_associe_span').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        jQuery.post(
            '<?php echo site_url("admin/vivresaville/remove_vsv_home_photo_deal"); ?>',
            {vsv_id:vsv_id},
            function (zReponse)
            {
                if (zReponse=="error")
                jQuery('#vsv_home_photo_deal_file_associe_span').html("Une erreur est surenue, veuillez refaire l'opération !");
                else location.reload();
            });
    }
    function remove_vsv_home_photo_carte(vsv_id=0) {
        jQuery('#vsv_home_photo_carte_file_associe_span').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        jQuery.post(
            '<?php echo site_url("admin/vivresaville/remove_vsv_home_photo_carte"); ?>',
            {vsv_id:vsv_id},
            function (zReponse)
            {
                if (zReponse=="error")
                jQuery('#vsv_home_photo_carte_file_associe_span').html("Une erreur est surenue, veuillez refaire l'opération !");
                else location.reload();
            });
    }

    function remove_vsv_background(vsv_id=0) {
        jQuery('#vsv_background_file_associe_span').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        jQuery.post(
            '<?php echo site_url("admin/vivresaville/remove_vsv_background"); ?>',
            {vsv_id:vsv_id},
            function (zReponse)
            {
                if (zReponse=="error")
                    jQuery('#vsv_background_file_associe_span').html("Une erreur est surenue, veuillez refaire l'opération !");
                else location.reload();
            });
    }


    function remove_vsv_home_photo_banner(vsv_id=0) {
        jQuery('#vsv_home_photo_banner_file_associe_span').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        jQuery.post(
            '<?php echo site_url("admin/vivresaville/remove_vsv_home_photo_banner"); ?>',
            {vsv_id:vsv_id},
            function (zReponse)
            {
                if (zReponse=="error")
                    jQuery('#vsv_home_photo_banner_file_associe_span').html("Une erreur est surenue, veuillez refaire l'opération !");
                else location.reload();
            });
    }

    function remove_vsv_home_photo_article(vsv_id=0) {
        jQuery('#vsv_home_photo_article_file_associe_span').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        jQuery.post(
            '<?php echo site_url("admin/vivresaville/remove_vsv_home_photo_article"); ?>',
            {vsv_id:vsv_id},
            function (zReponse)
            {
                if (zReponse=="error")
                    jQuery('#vsv_home_photo_article_file_associe_span').html("Une erreur est surenue, veuillez refaire l'opération !");
                else location.reload();
            });
    }

    function remove_vsv_home_photo_agenda(vsv_id=0) {
        jQuery('#vsv_home_photo_agenda_file_associe_span').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        jQuery.post(
            '<?php echo site_url("admin/vivresaville/remove_vsv_home_photo_agenda"); ?>',
            {vsv_id:vsv_id},
            function (zReponse)
            {
                if (zReponse=="error")
                    jQuery('#vsv_home_photo_agenda_file_associe_span').html("Une erreur est surenue, veuillez refaire l'opération !");
                else location.reload();
            });
    }

    function remove_vsv_home_photo_annonceb(vsv_id=0) {
        jQuery('#vsv_home_photo_annonceb_file_associe_span').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        jQuery.post(
            '<?php echo site_url("admin/vivresaville/remove_vsv_home_photo_annonce"); ?>',
            {vsv_id:vsv_id},
            function (zReponse)
            {
                if (zReponse=="error")
                    jQuery('#vsv_home_photo_annonceb_file_associe_span').html("Une erreur est surenue, veuillez refaire l'opération !");
                else location.reload();
            });
    }

    function remove_vsv_home_photo_annuaire(vsv_id=0) {
        jQuery('#vsv_home_photo_annuaire_file_associe_span').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        jQuery.post(
            '<?php echo site_url("admin/vivresaville/remove_vsv_home_photo_annuaire"); ?>',
            {vsv_id:vsv_id},
            function (zReponse)
            {
                if (zReponse=="error")
                    jQuery('#vsv_home_photo_annuaire_file_associe_span').html("Une erreur est surenue, veuillez refaire l'opération !");
                else location.reload();
            });
    }

    function remove_vsv_home_photo_fidelite(vsv_id=0) {
        jQuery('#vsv_home_photo_fidelite_file_associe_span').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        jQuery.post(
            '<?php echo site_url("admin/vivresaville/remove_vsv_home_photo_fidelite"); ?>',
            {vsv_id:vsv_id},
            function (zReponse)
            {
                if (zReponse=="error")
                    jQuery('#vsv_home_photo_fidelite_file_associe_span').html("Une erreur est surenue, veuillez refaire l'opération !");
                else location.reload();
            });
    }

    function remove_vsv_home_photo_bonplan(vsv_id=0) {
        jQuery('#vsv_home_photo_bonplan_file_associe_span').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        jQuery.post(
            '<?php echo site_url("admin/vivresaville/remove_vsv_home_photo_bonplan"); ?>',
            {vsv_id:vsv_id},
            function (zReponse)
            {
                if (zReponse=="error")
                    jQuery('#vsv_home_photo_bonplan_file_associe_span').html("Une erreur est surenue, veuillez refaire l'opération !");
                else location.reload();
            });
    }
    function remove_vsv_home_photo_newsletter(vsv_id=0) {
        jQuery('#vsv_home_photo_newsletter_file_associe_span').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        jQuery.post(
            '<?php echo site_url("admin/vivresaville/remove_vsv_home_photo_newsletter"); ?>',
            {vsv_id:vsv_id},
            function (zReponse)
            {
                if (zReponse=="error")
                    jQuery('#vsv_home_photo_newsletter_file_associe_span').html("Une erreur est surenue, veuillez refaire l'opération !");
                else location.reload();
            });
    }
    function remove_vsv_home_photo_gestion(vsv_id=0) {
        jQuery('#vsv_home_photo_gestion_file_associe_span').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        jQuery.post(
            '<?php echo site_url("admin/vivresaville/remove_vsv_home_photo_gestion"); ?>',
            {vsv_id:vsv_id},
            function (zReponse)
            {
                if (zReponse=="error")
                jQuery('#vsv_home_photo_gestion_file_associe_span').html("Une erreur est surenue, veuillez refaire l'opération !");
                else location.reload();
            });
    }


</script>