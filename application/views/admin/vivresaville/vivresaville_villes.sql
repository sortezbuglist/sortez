/*
Navicat MySQL Data Transfer

Source Server         : MySQL localhost
Source Server Version : 50717
Source Host           : 127.0.0.1:3306
Source Database       : sortez5

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2017-08-25 10:13:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for vivresaville_villes
-- ----------------------------
DROP TABLE IF EXISTS `vivresaville_villes`;
CREATE TABLE `vivresaville_villes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `id_ville` int(11) DEFAULT NULL,
  `id_department` int(11) DEFAULT NULL,
  `postalcode1` int(11) DEFAULT NULL,
  `postalcode2` int(11) DEFAULT NULL,
  `postalcode3` int(11) DEFAULT NULL,
  `subdomain` varchar(150) DEFAULT NULL,
  `site_title` varchar(150) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `subdomain_url` varchar(150) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `background` varchar(255) DEFAULT NULL,
  `photo_annuaire` varchar(255) DEFAULT NULL,
  `photo_agenda` varchar(255) DEFAULT NULL,
  `photo_article` varchar(255) DEFAULT NULL,
  `photo_bonplan` varchar(255) DEFAULT NULL,
  `photo_fidelite` varchar(255) DEFAULT NULL,
  `photo_annonce` varchar(255) DEFAULT NULL,
  `link_actu_mairie` varchar(255) DEFAULT NULL,
  `link_agenda_mairie` varchar(255) DEFAULT NULL,
  `link_home` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vivresaville_villes
-- ----------------------------
