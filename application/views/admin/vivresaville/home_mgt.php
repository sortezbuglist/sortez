<?php $data["zTitle"] = "Gestion vivre sa ville"; ?>

<?php $this->load->view("admin/vivresaville/includes/main_header", $data); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>application/resources/ckeditor/ckeditor.js"></script>

    <link href="css/global.css" rel="stylesheet" type="text/css">

    <div class="row justify-content-center text-center">

        <div class="col-sm-12 p-0">

            <h1><?php if (isset($oville->name_ville)) echo $oville->name_ville; else { ?>LA GESTION DE VIVRESAVILLE.FR<?php } ?></h1>

        </div>

        <div class="col-sm-12 p-0">

            <a href="<?php echo site_url("admin/vivresaville"); ?>" class="btn vsv_color mt-3">Retour à la liste des

                villes</a>

        </div>

    </div>





<!--    <div class="row justify-content-center text-center">-->

<!--        <div class="col-8 p-3">-->

<!--            <a href="--><?php //echo site_url("admin/vivresaville/home_mgt/" . $obj_item->id); ?><!--" class="btn btn-success">Gestion-->

<!--                de la page d'accueil</a>-->

<!--        </div>-->

<!--    </div>-->



    <div class="row justify-content-center">

        <div class="container container_select_ville p-0">





            <form id="vsv_form_ville" name="vsv_form_ville" method="POST" enctype="multipart/form-data">



<!--                <div class="col vsv_title_bar vsv_color">Contenus de la page d'accueil</div>-->



                <div class="col p-0 vsv_image_rubrique_block pt-5">

                    <div class="row">

                        <div class="col-sm-12 vsv_title_bar vsv_color">

                            <p style="text-transform: capitalize;font-size:17px;color:#FFFFFF">Image logo</p>

                        </div>

                        <div class="col-6 mb-3">

                            <div class="col-sm-12 text-center">

                                <p class="info_message">

                                    Import du logo de la ville

                                </p>

                            </div>

                            <div class="col-sm-12 text-center">

                                <?php

                                if (isset($oville->logo) && $oville->logo!="0" && $oville->logo!="" && file_exists("application/resources/front/images/vivresaville/".$oville->logo)) { ?>

                                    <img src="<?php echo base_url();?>application/resources/front/images/vivresaville/<?php echo $oville->logo;?>" style="width: 150px" class="mt-3"/>

                                    <div class="row">

                                        <div class="col-6">

                                            <a href="javascript:void(0)" class="border_rad_none btn btn-primary d-block mt-3">

                                                Télécharger image

                                            </a>

                                        </div>

                                        <div class="col-6">

                                            <a href="javascript:void(0)" onClick="javascript:remove_vsv_logo(<?php echo $oville->id;?>);" class=" border_rad_none btn btn-danger d-block mt-3">

                                                Supprimer image

                                            </a>

                                        </div>

                                    </div>

                                <?php } else { ?>

                                    <img src="<?php echo base_url();?>assets/soutenons/logo_vivresaville.png" style="width: 150px" class="mt-3"/>

                                    <p class="info_message">Image par défaut : la carte vivresaville</p>

                                    <div class="row">

                                        <div class="col-6">

                                            <a href="javascript:void(0)" onclick="javascript:add_image('logo_ville')" class=" border_rad_none btn btn-primary d-block mt-3">

                                                Télécharger image

                                            </a>

                                        </div>

                                        <div class="col-6">

                                            <a href="javascript:void(0)" class="border_rad_none btn btn-danger d-block mt-3">

                                                Supprimer image

                                            </a>

                                        </div>

                                    </div>

                                    <input type="file" id="vsv_logo_file" name="vsv_logo_file" class="form-control-file d-none"/>

                                <?php } ?>

                                <input type="hidden" name="vsv_object[logo]" id="vsv_logo_file_associe"

                                       value="<?php if (isset($oville->logo)) echo $oville->logo; ?>"/>

                                <span id="vsv_logo_file_associe_span"></span>

                            </div>

                        </div>

                        <div class="col-6 mb-3">

                            <div class="col-sm-12 text-center">

                                <p class="info_message">

                                    Import du logo de droite 

                                </p>

                            </div>

                            <div class="col-sm-12 text-center">

                                <?php

                                if (isset($oville->logo_droite) && $oville->logo_droite!="0" && $oville->logo_droite!="" && file_exists("application/resources/front/images/vivresaville/".$oville->logo_droite)) { ?>

                                    <img src="<?php echo base_url();?>application/resources/front/images/vivresaville/<?php echo $oville->logo_droite;?>" style="width: 150px" class="mt-3"/>

                                    <div class="row">

                                        <div class="col-6">
                                            <a href="javascript:void(0)" class="border_rad_none btn btn-primary d-block mt-3">

                                                Télécharger image

                                            </a>

                                        </div>

                                        <div class="col-6">

                                            <a href="javascript:void(0)" onClick="javascript:remove_vsv_logo_droite(<?php echo $oville->id;?>);" class="border_rad_none btn btn-danger d-block mt-3">

                                                Supprimer image

                                            </a>

                                        </div>

                                    </div>

<!--                                    <input type="button" id="btn_load_logo" class="btn btn-primary d-block mt-3" onClick="javascript:void(0)" value="Telecharger image"/>-->

<!--                                    <input type="button" id="btn_delete_logo" class="btn btn-danger d-block mt-3" onClick="javascript:remove_vsv_logo_droite(<?php //echo $oville->id;?>);" value="Supprimer"/> -->

                                <?php } else { ?>

                                    <img src="<?php echo base_url();?>assets/soutenons/logo_vivresaville.png" style="width: 150px" class="mt-3"/>

                                    <div class="row">

                                        <div class="col-6">

                                            <a href="javascript:void(0)" onclick="javascript:add_image('logo_droite')" class="border_rad_none btn btn-primary d-block mt-3">

                                                Télécharger image

                                            </a>

                                        </div>

                                        <div class="col-6">

                                            <a href="javascript:void(0)" class="border_rad_none btn btn-danger d-block mt-3">

                                                Supprimer image

                                            </a>

                                        </div>

                                    </div>

                                    <input type="file" id="vsv_logo_droite_file" name="vsv_logo_droite_file" class="form-control-file d-none"/>

                                <?php } ?>

                                <input type="hidden" name="vsv_object[logo_droite]" id="vsv_logo_droite_file_associe"

                                       value="<?php if (isset($oville->logo_droite)) echo $oville->logo_droite; ?>"/>

                                <span id="vsv_logo_droite_file_associe_span"></span>

                            </div>

                        </div>

                        <div class="col-sm-12 vsv_title_bar vsv_color">

                            <p style="text-transform: capitalize;font-size:17px;color:#FFFFFF">Image bannière</p>

                        </div>

                        <div class="col-sm-12 mb-3">

                            <div class="col-sm-12">

                                <?php if (isset($oville->home_photo_banner) && $oville->home_photo_banner!="0" && $oville->home_photo_banner!="" && file_exists("application/resources/front/images/vivresaville/".$oville->home_photo_banner)) { ?>

                                    <img src="<?php echo base_url();?>application/resources/front/images/vivresaville/<?php echo $oville->home_photo_banner;?>" style="width: 100%" class="mt-3"/>

                                    <div class="col-8">

                                        <div class="row">

                                            <div class="col-6">

                                                <a href="javascript:void(0)" class="border_rad_none btn btn-primary d-block mt-3">

                                                    Télécharger image

                                                </a>

                                            </div>

                                            <div class="col-6">

                                                <a href="javascript:void(0)" onClick="javascript:remove_vsv_home_photo_banner(<?php echo $oville->id;?>);" class="border_rad_none btn btn-danger d-block mt-3">

                                                    Supprimer image

                                                </a>

                                            </div>

                                        </div>

                                    </div>

<!--                                    <input type="button" id="btn_delete_background" class="btn btn-danger d-block mt-3" onClick="javascript:remove_vsv_background(<?php //echo $oville->id;?>);" value="Supprimer"/> -->

                                <?php } else { ?>

                                    <p class="info_message">Dimension optimisée : 1200 x 400 pixels</p>

                                    <div class="col-8">

                                        <div class="row">

                                            <div class="col-6">

                                                <a href="javascript:void(0)" onclick="javascript:add_image('image_banniere')" class="border_rad_none btn btn-primary d-block mt-3">

                                                    Télécharger image

                                                </a>

                                            </div>

                                            <div class="col-6">

                                                <a href="javascript:void(0)" class="border_rad_none btn btn-danger d-block mt-3">

                                                    Supprimer image

                                                </a>

                                            </div>

                                        </div>

                                    </div>

                                    <input type="file" id="vsv_bg_file" name="home_photo_banner" class="form-control-file d-none"/>

                                <?php } ?>

                                <input type="hidden" name="vsv_object[home_photo_banner]" id="vsv_bg_file_associe"

                                       value="<?php if (isset($oville->home_photo_banner)) echo $oville->home_photo_banner; ?>"/>

                                <span id="vsv_background_file_associe_span"></span>

                            </div>

                        </div>

                    </div>

                </div>



                <div class="col p-0 vsv_image_rubrique_block pt-5">

                    <div class="row">

               <div class="col-sm-12 vsv_title_bar vsv_color">
                            <p style="text-transform: capitalize;font-size:17px;color:#FFFFFF">Texte de Presentation</p>
                        </div>
                            <div class="col-sm-12">
                                <textarea  id="presentation" name="vsv_object[text_top]"
                                       class="border_rad_none form-control" placeholder="Integration du text d'entête"
                                       value=""> <?php if (isset($oville->text_top)) echo $oville->text_top; ?> </textarea>
                            </div>
                            <script>
                                 CKEDITOR.config.uiColor = '#E80EAE';  
                CKEDITOR.replace('presentation');

            </script>

                            </div>

                            </div>

                        

                <?php  //var_dump($oville);

                $data['h'] = $oville->id;

                $data['h']=$this->vivresaville_villes->show_list($oville->id);

               

               

                $this->load->view('admin/vivresaville/bloc_menu'); 

                 $this->load->view('admin/vivresaville/bloc_menu2',$data); ?>



                <div class="col p-0 vsv_image_rubrique_block pt-5">

                    <div class="row">

                        <div class="col-sm-12 vsv_title_bar vsv_color">

                            <p style="text-transform: capitalize;font-size:17px;color:#FFFFFF">Intégration slide publicitaire</p>

                        </div>

                        <div class="row pt-3">

                            <div class="col-sm-12">

                                <p class="info_message">

                                    Dimension optimisée : 980 x 300 pixels

                                </p>

                            </div>

                            <div class="col-sm-12">

                                <input type="text" id="vsv_slide_link" name="vsv_object[slide_link]"

                                       class="border_rad_none form-control" placeholder="intégration de l'URL"

                                       value="<?php if (isset($oville->slide_link)) echo $oville->slide_link; ?>"/>

                            </div>

                        </div>

                    </div>

                </div>



                <div class="col p-0 vsv_submit_btn_block pt-5 pb-5">

                    <div class="row justify-content-center">

                        <div class="col-8 pb-5">

                            <a href="javascript:void(0);" id="vsv_submit_home_btn" onclick="javascript:vsv_form_ville_home_fn();"

                               class="btn w-100 btn-success">ENREGISTRER</a>

                               

                            <input type="hidden" id="vsv_id" name="vsv_object[id]"

                                   value="<?php if (isset($oville->id)) echo $oville->id; else echo "0"; ?>">

                        </div>

                    </div>

                    <div class="row justify-content-center">

                        <div class="col-8 pb-5" id="vsv_form_submit_error">



                        </div>

                    </div>

                </div>





            </form>





        </div>

    </div>

<script type="text/javascript">

    function add_image(image_to_add){

        if(image_to_add == "logo_ville"){

            $('#vsv_logo_file').click();

        }else if(image_to_add == "logo_droite"){

            $('#vsv_logo_droite_file').click();

        }else if(image_to_add == "image_banniere"){

            $('#vsv_bg_file').click();

        }else if(image_to_add == "image_site"){

            $('#vsv_home_photo_site_web_file').click();

        }

    }

</script>

<style>

    .border_rad_none{

        border-radius: unset;

    }

    .info_message{

        font-size:15px;

        font-family:Futura-LT-Book, Sans-Serif;

        color:#000000

    }

    .border_shadow{

        box-shadow: 3px 3px 5px 2px #ccc;

    }

    .text_default{

        font-size:17px;

        font-family: Libre-Baskerville, Serif;

        color:#E80EAE;

        text-align: center;

    }

    /* The container */

    .content_checkmark {

        display: block;

        position: relative;

        padding-left: 35px;

        margin-bottom: 12px;

        cursor: pointer;

        -webkit-user-select: none;

        -moz-user-select: none;

        -ms-user-select: none;

        user-select: none;

    }



    /* Hide the browser's default checkbox */

    .content_checkmark input {

        position: absolute;

        opacity: 0;

        cursor: pointer;

        height: 0;

        width: 0;

    }



    /* Create a custom checkbox */

    .checkmark {

        position: absolute;

        top: 3px;

        left: 35px;

        height: 17px!important;

        width: 17px!important;

        border: 2px solid #a9a9a9;

        border-radius: unset!important;

        background-color: #ffffff!important;

    }



    /* On mouse-over, add a grey background color */

    .content_checkmark:hover input ~ .checkmark {

        background-color: #ffffff;

    }



    /* When the checkbox is checked, add a blue background */

    .content_checkmark input:checked ~ .checkmark {

        background-color: #ffffff;

        border: 2px solid #a9a9a9;

        border-radius: unset;

    }



    /* Create the checkmark/indicator (hidden when not checked) */

    .checkmark:after {

        content: "";

        position: absolute;

        display: none;

    }



    /* Show the checkmark when checked */

    .content_checkmark input:checked ~ .checkmark:after {

        display: block;

    }



    /* Style the checkmark/indicator */

    .content_checkmark .checkmark:after {

        left: 3px;

        top: 0px;

        width: 7px;

        height: 10px;

        border: solid #a9a9a9;

        border-width: 0 3px 3px 0;

        -webkit-transform: rotate(45deg);

        -ms-transform: rotate(45deg);

        transform: rotate(45deg);

    }

    .border_rad_none.btn.btn-primary.d-block.mt-3,.border_rad_none.btn.btn-primary.d-block.mt-1{

        background-color: #E80EAE;

        border: none;

    }

    .border_rad_none.btn.btn-danger.d-block.mt-3,

    .border_rad_none.btn.btn-danger.d-block.mt-1{

        background-color: #e81a0e;

    }

    .border_rad_none.btn.btn-primary.d-block.mt-3:hover,

    .border_rad_none.btn.btn-primary.d-block.mt-1:hover{

        background-color: #4f44da;

    }

    .border_rad_none.btn.btn-danger.d-block.mt-3:hover,

    .border_rad_none.btn.btn-danger.d-block.mt-1:hover{

        background-color: #4f44da;

    }

    .btn_block_categ_other {

        border: 2px solid #ffffff;

        background-color: #E80EAE;

        color: #ffffff;

        border-radius: unset;

        margin-bottom: 15px;

        padding-left: 50px;

        padding-right: 50px;

    }

    .border_rad_none.form-control,

    .form-control.border_rad_none{

        border: 2px solid #a9a9a9;

    }

    p.info_message.w-100.text-center {

        margin-top: 15px;

    }

    p.text_default {

        margin-top: 15px;

    }

    .container_select_ville {

        width: 925px;

    }

    .vsv_title_bar p{

        font-family: Futura-LT-Book, Sans-Serif;

    }

</style>



<?php $this->load->view("admin/vivresaville/includes/main_footer"); ?>