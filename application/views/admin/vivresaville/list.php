<link href="<?php echo base_url();?>application/views/admin/vivresaville/css/global.css" rel="stylesheet" type="text/css">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-8 p-0">
            <div class="col p-0 vsv_ville_list_block">
                <div class="row justify-content-center text-center">
                    <div class="col-12 p-0">
                        <h1>LA GESTION DE VIVRESAVILLE.FR</h1>
                    </div>
                    <div class="col-12 p-0">
                    <img src="<?php echo base_url();?>assets/ville-test/wpimages/vsv_img_title_admin.png" width="283" height="237" alt=""/> </div>
                </div>
                <div class="col vsv_title_bar vsv_color">Les villes en cours</div>
                <?php if (isset($objlist) && count($objlist) > 0) { ?>
                <ul class="list-group">
                    <?php foreach ($objlist as $obj_item) { ?>
                    <li class="list-group-item">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-6">
                                    <?php echo $obj_item->name_ville; ?>
                                </div>
                                <div class="col-2 text-right">
                                    <?php if($obj_item->isactive=='1') {?>
                                        <img style="border: none;width:15px;" src="<?php echo base_url();?>application/resources/privicarte/images/activated_ico.gif">
                                    <?php } else { ?>
                                        <img style="border: none;width:15px;" src="<?php echo base_url();?>application/resources/privicarte/images/deactivated_ico.png">
                                    <?php } ?>
                                </div>
                                <div class="col-4 text-center">
                                    <a href="<?php echo site_url("admin/vivresaville/edit/".$obj_item->id);?>" class="btn w-100 vsv_color">Modifier</a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
                <?php } ?>
                <div class="row justify-content-center">
                    <div class="col-6 p-0">
                        <a href="<?php echo site_url("admin/vivresaville/edit");?>" class="btn w-100 vsv_color mt-5">Créer une ville</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>