<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Administration - Sortez</title>


    <!-- bootstrap core -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="<?php echo base_url(); ?>application/views/vivresaville/bootstrap/css/bootstrap-datepicker.standalone.min.css"
          rel="stylesheet" type="text/css">


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url(); ?>application/views/vivresaville/jquery/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url(); ?>application/views/vivresaville/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>application/views/vivresaville/bootstrap/js/bootstrap.min.js"></script>

    <link href="<?php echo base_url(); ?>application/views/admin/vivresaville/bootstrap/css/bootstrap-toggle.min.css"
          rel="stylesheet">
    <script src="<?php echo base_url(); ?>application/views/admin/vivresaville/bootstrap/js/bootstrap-toggle.min.js"></script>

    <script src="<?php echo base_url(); ?>application/views/vivresaville/jquery/jquery-migrate-1.4.1.min.js"></script>

    <!--validation plugin-->
    <!--<link href="<?php echo base_url(); ?>application/views/admin/vivresaville/css/jquery.validation.css" rel="stylesheet" type="text/css">-->
    <script type="text/javascript"
            src="<?php echo base_url(); ?>application/views/admin/vivresaville/js/jquery.validate.js"></script>


    <?php $data['umpty'] = null; ?>

    <?php $this->load->view("admin/vivresaville/js/global_js", $data); ?>
    <script type="text/javascript"
            src="<?php echo base_url(); ?>application/views/admin/vivresaville/js/global.js"></script>

    <?php $this->load->view("admin/vivresaville/css/global_css", $data); ?>
    <link href="<?php echo base_url(); ?>application/views/admin/vivresaville/css/global.css" rel="stylesheet"
          type="text/css">
    <link href="../css/global.css" rel="stylesheet" type="text/css">

                            <!-- uiJquery     -->
                            <link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
					<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
  					<script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
	<script>
		$(document).ready(function(){
			// $('.reorder_link').on('click',function(){
				$("tbody.reorder-photos-list").sortable({ tolerance: 'pointer' });
				$('.reorder_link').html('save reordering');
				$('.reorder_link').attr("id","saveReorder");
				$('#reorderHelper').slideDown('slow');
				$('.image_link').attr("href","javascript:void(0);");
				$('.image_link').css("cursor","move");
				$("#saveReorder").click(function( e ){
					if( !$("#saveReorder i").length ){
						$("tbody.reorder-photos-list").sortable('destroy');
						$("#reorderHelper").html("en cours de sauvegarde - juste un instant. ne quitter pas cette page.").removeClass('light_box').addClass('notice notice_error');
			
						var h = [];
						$("tbody.reorder-photos-list tr").each(function() {
							h.push($(this).attr('id').substr(9));
						});
						$.ajax({
							type: "POST",
							url: "<?php echo base_url('admin/vivresaville/orderUpdate'); ?>",
							data: {ids: " " + h + ""},
							success: function(data){
                                console.log(data)
								 window.location.reload();
							}
						});	
						return false;
					}	
					e.preventDefault();		
				});
			// });
		});
	</script>

</head>
<body>
<div class="row justify-content-center">
    <div class="col-11 p-0">

        <div class="row">
            <div class="col admin_main_banner">
                <div class="col-lg-7 padding0" style="text-align:left; width:58.3333%; float:left;"><img
                            src="<?php echo GetImagePath("privicarte/"); ?>/logo_mini.png" alt="logo"></div>
                <div class="col-lg-5 padding0" style="text-align:right; width:41.6667%; float:left;">

                    <div class="col-lg-12 padding0"><a href="<?php echo site_url(); ?>"><img
                                    src="<?php echo GetImagePath("privicarte/"); ?>/btn_back.png" alt="back"></a></div>
                </div>
            </div>
        </div>

        <div class="row main_admin_container">


            <div class="admin_main_menu_left p-0" style="position: absolute; z-index: 1;">
                <?php $this->load->view("admin/includes/vwMainMenu", $data); ?>
            </div>

            <div class="col-12 col-xs-12 admin_main_container_right">
  