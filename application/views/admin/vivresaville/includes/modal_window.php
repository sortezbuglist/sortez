<div class="modal fade" id="vsv_modal_main_menu" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe id="vsv_modal_main_iframe" class="embed-responsive-item" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>


<script type="application/javascript">
    $(document).ready(function () {
        /*setTimeout(function(){
         $('#google_translate_element .skiptranslate .goog-te-gadget-simple a.goog-te-menu-value span:first-child').html("LANGUE");
         }, 2500);*/

        $(".vsv_modal_container a.vsv_modal_link").each(function () {
            var id = $(this).attr("id");
            $(".vsv_modal_container").on('click', "#" + id, function () {
                if (id == "vsv_main_link_fb" || id == "vsv_main_link_twt") {
                    $('#vsv_modal_main_menu .modal-body .embed-responsive').css("height", "700px");
                } else {
                    $('#vsv_modal_main_menu .modal-body .embed-responsive').css("height", "auto");
                }
                var src = $(this).attr('attachmentid');
                $("#vsv_modal_main_iframe").attr('src', src);
                $('#vsv_modal_main_menu').modal('show');
                return false;
            });
        });
    });
</script>
