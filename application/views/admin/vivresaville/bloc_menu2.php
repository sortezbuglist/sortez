
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
    .titre{
        width: 10% !important;
    }
    .img{
      width: 20% !important;
    }
    .text{
        width: 45%;
    }
    .action{
        width: 15%;
    }
    .div_action{
            justify-content: space-between;
                display: flex;
    }
    .fa-pencil{
        font-size: 25px;
    color: yellowgreen;
    }
    .fa-trash{
        font-size: 25px;
        color: crimson;
    }

</style>
<div class="col-sm-12 vsv_title_bar vsv_color">
            <p style="text-transform: capitalize;font-size:17px;color:#FFFFFF">Liste des blocs</p>
        </div>
        <a href="javascript:void(0);" class="reorder_link" id="saveReorder">mettre les cartes en ordre</a>
<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
            <tr >
                    <!-- <td>Id bloc menu</td>
                    <td>Id vivresaville ville</td> -->
                    <td>Titre</td>
                    <td>Photo</td>
                    <td>Lien</td>
                    <td>Texte</td>
            </tr>
        </thead>
        <tbody  class="reorder_ul reorder-photos-list">
            <?php foreach($h as $row){ ?>
                <a href="javascript:void(0);" style="float:none;" class="image_link">    
                    <tr id="image_li_<?php echo $row->id_bloc_menu ; ?>" class="reorder_ul reorder-photos-list">
                                <!-- <td><?php //echo $row->id_bloc_menu  ?></td>
                                <td><?php //echo $row->id_vivresaville_villes  ?></td> -->
                                <td class="titre"><?php  echo $row->titre ?></td>
                                <td class="img">

                                    <? if(isset($row->home_photo_site_web) && $row->home_photo_site_web!=null){?>
                                    <img src="<?php echo base_url('application/resources/front/images/vivresaville/'.$row->home_photo_site_web);?>" style="width: 120px;"> 
                                    <?}?>
                                        
                                    </td> 
                                <td class="link"><?php echo $row->link  ?></td>
                                <td class="text"><?php echo $row->bloc_text  ?></td>
                            <td class="action">
                                <div class="div_action"><a href="<?php echo base_url('admin/vivresaville/home_mgt/'.$row->id_vivresaville_villes.'?id='.$row->id_bloc_menu) ?>" title="Modiffier cette bloc"><i class="fa fa-pencil"></i></a>
                            
                                <a href="<?php echo base_url('admin/vivresaville/clear/'.$row->id_bloc_menu.'/'.$row->id_vivresaville_villes) ?>" title="Supprimer cette bloc" onclick="javascript: return confirm('Êtes-vous sur de vouloir supprimer cette bloc ?');"><i class="fa fa-trash"></i></a></div>
                            </td>
                    </tr>
                </a>
            <?php  } ?>
        </tbody>




    </table>
</div>




