<?php $data["zTitle"] = 'Administration'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script type="text/javascript" charset="utf-8">
	$(function() {
				$("#btnsubmit_editsouscategorie").click(function(){
					
					var txtError = "";
					
					//verify input content
					var inputcateg_editsouscategorie = $("#inputcateg_editsouscategorie").val();
					if(inputcateg_editsouscategorie=="") {
						
						txtError += "- Veuillez indiquer Votre inputcateg_editsouscategorie <br/>"; 
						//alert("Veuillez indiquer Votre nom");
						$("#inputcateg_editsouscategorie").css('border-color', 'red');
					} else {
						$("#inputcateg_editsouscategorie").css('border-color', '#E3E1E2');
					}
					
					
					//alert(txtError);
					if(txtError == "") {
						//alert('ok');
						$("#form_editsouscategorie").submit();
						//$("#qsdf_x").click();
					}
					
				
				
			});
		});
</script>
      <br>
     
    <div id="divMesAnnonces" class="content" align="center">
				<p><a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>">Retour au menu</a><br/>
                <a style = "color:black;" href = "<?php echo site_url("admin/commercants/categories" ) ; ?>">Retour à la liste des catégories</a><br/>
                <a style = "color:black;" href = "<?php echo site_url("admin/commercants/souscategorie/".$toCategorie->IdRubrique) ; ?>">Retour à la liste des sous-catégories de <?php echo $toCategorie->Nom;?></a>
				</p>
                <div class="H1-C">Editer sous-catégorie</div>
                <br/><br/>
				</br>
				
					
<form action="<?php echo site_url("admin/commercants/savesouscategorie"); ?>" method="post" enctype="multipart/form-data" id="form_editsouscategorie" name="form_editsouscategorie">
<table width="400" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>IdRubrique</td>
    <td>
    <input name="idrubrique_editcategorie" id="idrubrique_editcategorie" type="hidden" value="<?php if (isset($toCategorie) && $toCategorie->IdRubrique != 0)  echo $toCategorie->IdRubrique; else echo 0;?>">
    <input name="idrubrique_editsouscategorie" id="idrubrique_editsouscategorie" type="hidden" value="<?php if (isset($oSousCategorie) && $oSousCategorie->IdSousRubrique != 0)  echo $oSousCategorie->IdSousRubrique; else echo 0;?>">
	<?php if (isset($oSousCategorie) && $oSousCategorie->IdSousRubrique != 0)  echo $oSousCategorie->IdSousRubrique; else echo 0;?>
    </td>
  </tr>
  <tr>
    <td>Nom</td>
    <td>
    <input name="inputcateg_editsouscategorie" id="inputcateg_editsouscategorie" type="text" style="width:300px;" value="<?php if (isset($oSousCategorie) && $oSousCategorie->Nom != NULL)  echo $oSousCategorie->Nom;?>">
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
    <a style = "color:black;" href = "<?php echo site_url("admin/commercants/souscategorie/".$toCategorie->IdRubrique) ; ?>">
    <input name="annuler_editsouscategorie" id="annuler_editsouscategorie" type="button" value="Annuler"></a>&nbsp;
    <input name="btnsubmit_editsouscategorie" id="btnsubmit_editsouscategorie" type="button" value="Enregistrer">
    </td>
  </tr>
</table>
</form>
                    
		  
       
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>