<?php $data["zTitle"] = 'Creation annonce'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script>
	function testFormVille(){
	var NomVille = $("#NomVille").val () ;
	var CpVille = $("#CpVille").val () ;
	var NomsimpleVille = $("#NomsimpleVille").val () ;
	var zErreur = "" ;
	
	if (NomVille == ""){
		zErreur += "Veuillez selectionner un NomVille\n" ;
	}
	if (CpVille == ""){
		zErreur += "Veuillez selectionner l'CpVille\n" ;
	}
	if (NomsimpleVille == ""){
		zErreur += "Veuillez indiquer la date du NomsimpleVille\n" ;
	}
	
	
	
	if (zErreur != ""){
		alert ('Tout les champs sont obligatoires !') ;
	}else{
		document.frmCreationVille.submit();
	}
	
	return false;
}
</script>
    
    <div id="divFicheAnnonce" class="content" align="center">
	     <br>
	     <a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input class="btn btn-primary" type = "button" value= "retour au menu" id = "btn"/> </a>
        <form name="frmCreationVille" id="frmCreationVille" action="<?php if (isset($oVille)) { echo site_url("admin/ville/modifVille/$IdVille"); }else{ echo site_url("admin/ville/creerVille"); } ?>" method="POST">
		<input type="hidden" name="ville[IdVille]" id="IdVille_id" value="<?php if (isset($oVille)) { echo $oVille->IdVille ; } ?>" />
            <h1><?php echo $title ; ?></h1>
			<span><?php if (isset($msg)) { echo $msg ; } ?></span>
            <fieldset>
                <legend>Ville</legend>
                <table cellpadding="0" cellspacing="0">
                    
					<tr>
                        <td>
                            <label>Nom Ville : </label>
                        </td>
                        <td>
                            <input type="text" name="ville[Nom]" id="NomVille" value="<?php if (isset($oVille)) { echo $oVille->Nom ; } ?>" />
                        </td>
                    </tr>

					<tr>
                        <td></td>
                        <td align="left">
                        <input type="button" value="Annuler" onclick="javascript:document.location='<?php echo site_url("admin/ville/liste");?>';" />&nbsp;
                        <input type="button" value="Valider" onclick="javascript:testFormVille();" /></td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>


<?php $this->load->view("admin/includes/vwFooter2013"); ?>