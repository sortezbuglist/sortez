<?php $data["zTitle"] = 'Gestion villes'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra']})
				.tablesorterPager({container: $("#pager")});
		});
</script>
    <div id="divAdminHome" class="content" align="center">
	     <p><a style = "color:black; text-decoration:none;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" value= "retour au menu" id = "btn" onclick="document.location='<?php echo site_url("admin/home/" );?>';"/>  </a></p>
		 
        <br><a style = "color:black;text-decoration:none;" href="<?php echo site_url("admin/ville"); ?>"><div class="H1-C">Liste des Villes</div></a><br>
        <br><a style = "color:black;text-decoration:none;" href="<?php echo site_url("admin/commune"); ?>"><div class="H1-C">Liste des Communes</div></a><br>
		<br><a style = "color:black;text-decoration:none;" href="<?php echo site_url("admin/departement"); ?>"><div class="H1-C">Liste des Départements</div></a><br>
        <div style="color:#33CC33"><?php if (isset($msg)) echo $msg;?></div>
      <fieldset>
                <legend>Liste des Communes</legend>
		<table cellpadding="1" class="tablesorter" style="text-align:left">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nom</th>
                   
                    <th>&nbsp;</th>
                 
                </tr>
            </thead>
            <tbody>
                <?php foreach($colCommune as $objCommune) { ?>
                    <tr>
                        <td>
                            <a href="<?php echo site_url("admin/commune/ficheCommune/".$objCommune->id_commune); ?>">
                                <?php echo htmlspecialchars(stripcslashes($objCommune->id_commune)); ?>
                            </a>
                        </td>
                        <td><?php echo htmlspecialchars(stripcslashes($objCommune->N_Com_min)); ?></td>
                      
                        <td>
                            <a href="<?php echo site_url("admin/commune/ficheCommune/".$objCommune->id_commune); ?>">
                                Modifier
                            </a>
                        </td>
                      
                    </tr>
                <?php } ?>
            </tbody>
        </table>
	  </fieldset>
					<div id="pager" class="pager">
						<img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
						<img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
						<input type="text" class="pagedisplay"/>
						<img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
						<img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
						<select class="pagesize" style="visibility:hidden">
							<option selected="selected"  value="10">10</option>
							<option value="20">20</option>
							<option value="30">30</option>
							<option  value="40">40</option>
						</select>
					</div>
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>