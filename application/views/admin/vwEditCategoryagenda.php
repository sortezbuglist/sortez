<?php $data["zTitle"] = 'Gestion Categorie agenda'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script type="text/javascript" charset="utf-8">
	$(function() {
				$("#btnsubmit_editcategorie").click(function(){
					
					var txtError = "";
					
					//verify input content
					var inputcateg_editcategorie = $("#inputcateg_editcategorie").val();
					if(inputcateg_editcategorie=="") {
						txtError += "- Veuillez indiquer Votre inputcateg_editcategorie <br/>"; 
						$("#inputcateg_editcategorie").css('border-color', 'red');
					} else {
						$("#inputcateg_editcategorie").css('border-color', '#E3E1E2');
					}
					
					
					var agenda_typeid = $("#agenda_typeid").val();
					if(agenda_typeid=="" || agenda_typeid=="0") {
						txtError += "- Veuillez indiquer Votre agenda_typeid <br/>"; 
						$("#agenda_typeid").css('border-color', 'red');
					} else {
						$("#agenda_typeid").css('border-color', '#E3E1E2');
					}
					
					
					//alert(txtError);
					if(txtError == "") {
						$("#form_editcategorie").submit();
					}
					
				
				
			});
		});
		
		function deleteFile(_IdAgenda,_FileName) {
				jQuery.ajax({
					url: '<?php echo base_url();?>agenda/delete_files_category/' + _IdAgenda + '/' + _FileName,
					dataType: 'html',
					type: 'POST',
					async: true,
					success: function(data){
						window.location.reload();
					}
				});
				
			}
</script>
      <br>
     
    <div id="divMesAnnonces" class="content" align="center">
                <a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>">Retour au menu</a><br/><br/>
                <a style = "color:black;" href = "<?php echo site_url("admin/categories_agenda" ) ; ?>">Retour à la liste des catégories</a><br/>
                <br>
                <p><div class="H1-C">Editer catégorie agenda</div></p>
				
					
<form action="<?php echo site_url("admin/categories_agenda/savecategorie"); ?>" method="post" enctype="multipart/form-data" id="form_editcategorie" name="form_editcategorie">
<table width="400" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Id : </td>
    <td>
    <input name="idrubrique_editcategorie" id="idrubrique_editcategorie" type="hidden" value="<?php if (isset($oCategorie) && $oCategorie->agenda_categid != 0)  echo $oCategorie->agenda_categid; else echo 0;?>">
	<?php if (isset($oCategorie) && $oCategorie->agenda_categid != 0)  echo $oCategorie->agenda_categid; else echo 0;?>
    </td>
  </tr>
  <tr>
    <td>Type agenda : </td>
    <td>
    	<select name="agenda_typeid" id="agenda_typeid">
            <?php if(sizeof($oTypeagenda)) { ?>
                <?php foreach($oTypeagenda as $objTypeagenda) { ?>
                    <option <?php if(isset($oCategorie->agenda_typeid) && $objTypeagenda->agenda_typeid == $oCategorie->agenda_typeid) echo 'selected="selected"';?> value="<?php echo $objTypeagenda->agenda_typeid; ?>"><?php echo $objTypeagenda->agenda_type; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
    </td>
  </tr>
  <tr>
    <td>Catégorie : </td>
    <td><input name="inputcateg_editcategorie" id="inputcateg_editcategorie" type="text" style="width:300px;" value="<?php if (isset($oCategorie) && $oCategorie->category != NULL)  echo $oCategorie->category;?>"></td>
  </tr>

 
  <tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Image  : </label>
        <input type="hidden" name="imagesAssocie" id="imagesAssocie" value="<?php if(isset($oCategorie->images)) echo $oCategorie->images; ?>" />
  </td>
    <td>
        <?php if(!empty($oCategorie->images)) { ?>
            <?php 
			$img_doc_affiche_split_array = explode('.',$oCategorie->images);
			$img_doc_affiche_path = "application/resources/front/images/agenda/category/".$img_doc_affiche_split_array[0]."_thumb_200_200.".$img_doc_affiche_split_array[1];
			//echo $img_doc_affiche_path;
			if (file_exists($img_doc_affiche_path)== true) {
				echo '<img  src="'.GetImagePath("front/").'/agenda/category/'.$img_doc_affiche_split_array[0].'_thumb_200_200.'.$img_doc_affiche_split_array[1].'"/>';
			} else {
				echo image_thumb("application/resources/front/images/agenda/category/" . $oCategorie->images, 200, 200,'',''); 
			}
			?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $oCategorie->agenda_categid; ?>','images');">Supprimer</a>
        <?php } else { ?>
        <input type="file" name="inputcateg_images" id="inputcateg_images" value=""/>
        <?php } ?>
        
    </td>
</tr>
  
  <tr>
    <td>&nbsp;</td>
    <td>
    <a style = "color:black;" href = "<?php echo site_url("admin/categories_agenda" ) ; ?>">
    <input name="annuler_editcategorie" id="annuler_editcategorie" type="button" value="Annuler"></a>&nbsp;
    <input name="btnsubmit_editcategorie" id="btnsubmit_editcategorie" type="button" value="Enregistrer">
    </td>
  </tr>
</table>
</form>
                    
		  
       
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>