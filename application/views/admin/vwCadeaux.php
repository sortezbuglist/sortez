<?php $data["zTitle"] = 'Listes des cadeaux'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>


<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra'], headers: { 3: { sorter: false}, 4: {sorter: false} }})
				.tablesorterPager({container: $("#pager")});
		});
</script>
    <div id="divAdminHome" class="content" align="center">
	       
	       <p><a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"><input class="btn btn-primary" type = "button" value= "retour au menu" id = "btn"/></a>
            </p>
                
                <div class="H1-C">Liste des cadeaux</div>
		 <p><a class="btn btn-success" href="<?php echo site_url("admin/cadeau/edit/0"); ?>">
                                Ajouter un cadeau
         </a></p>
        <table cellpadding="1" class="tablesorter">
            <thead>
				<tr>
                    <th>Nom de l'offre</th>  
                    <th>Date de debut de validit&eacute;</th>
                    <th>Date de fin de validit&eacute;</th>
                    <th width="10">&nbsp;</th>
					<th width="10">&nbsp;</th>				
				</tr>
			</thead>
			<tbody>
                <?php foreach($lstcadeau as $objCadeau) { ?>
                    <tr>
                        <td>
                            <?php echo htmlspecialchars(stripcslashes($objCadeau->Nomoffre )); ?>
                            
                        </td>
						<td>
                            <?php echo ($objCadeau->Valabledebut ); ?> 
                        </td>
						<td>
                            <?php echo ($objCadeau->Valablefin ); ?> 
                        </td>
						<td>
                            <a href="<?php echo site_url("admin/cadeau/edit/" . $objCadeau->Idcadeau); ?>">
                                Modifier
                            </a>
                        </td>
						<td>
                            <a href="<?php echo site_url("admin/cadeau/delete/" . $objCadeau->Idcadeau); ?>">
                               Supprimer
                            </a>
                        </td>
                        
                    </tr>
                <?php } ?>
			</tbody>
        </table>
		<div id="pager" class="pager">
			<img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
			<img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
			<input type="text" class="pagedisplay"/>
			<img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
			<img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
			<select class="pagesize" style="visibility:hidden">
				<option selected="selected"  value="10">10</option>
				<option value="20">20</option>
				<option value="30">30</option>
				<option  value="40">40</option>
			</select>
		</div>		
		<br><br>
		
        <input type="button" value="D&eacute;connexion" onclick="document.location = '<?php echo site_url("connexion/sortir"); ?>';" />
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>