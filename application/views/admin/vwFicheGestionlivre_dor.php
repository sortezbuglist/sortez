<?php $data["zTitle"] = 'Creation Type Article'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script>
	function testFormTypeArticle(){
	var article_type = $("#article_type").val () ;

	var zErreur = "" ;

	if (article_type == ""){
		zErreur += "Veuillez selectionner un article_type\n" ;
	}

	if (zErreur != ""){
		alert ('Veuillez indiquer un Nom !') ;
	}else{
		document.frmCreationTypeArticle.submit();
	}

	return false;
}
</script>

    <div id="divFicheAnnonce" class="content" align="center">
	     <br>
	     <a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" value= "Retour au Menu" id = "btn"/> </a>

        <form name="frmCreationTypeArticle" id="frmCreationTypeArticle" action="<?php if (isset($oCom)) { echo site_url("front/Gestion_livre_dor/modif_commentaire/".$oCom->idcommercant); } ?>" method="POST">
		<input type="hidden" name="Gestion_livre_dor[id]" id="id" value="<?php if (isset($oCom)) echo $oCom->id ; else echo '0'; ?>" />
            <h1><?php echo $title ; ?></h1>
			<span><?php if (isset($msg)) { echo $msg ; } ?></span>
            <fieldset>
                <legend>Commentaire</legend>
                <table cellpadding="0" cellspacing="0">

					<tr>
                        <td>
                            <label>Nom : </label>
                        </td>
                        <td>
                            <input type="text" name="Gestion_livre_dor[nom]" disabled id="nom" value="<?php if (isset($oCom)) { echo $oCom->nom ; } ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Message : </label>
                        </td>
                        <td>
                           <textarea style="width:250px; margin-top: 5px;" name="Gestion_livre_dor[message]" id="message"> <?php if (isset($oCom)) { echo $oCom->message ; } ?>  </textarea>
                        </td>
                    </tr>
					<tr>
                        <td></td>
                        <td align="left">
                        <input type="button" value="Annuler" onclick="javascript:document.location='<?php echo site_url("front/Gestion_livre_dor/liste/".$oCom->idcommercant);?>';" />&nbsp;
                        <input type="submit" value="Valider" onclick="javascript:testFormTypeArticle();" /></td>
                    </tr>
        </form>
                </table>
            </fieldset>
    </div>


<?php $this->load->view("admin/includes/vwFooter2013"); ?>
