<?php $data["zTitle"] = 'Modification'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>


<?php //var_dump($oUserIonauth);
if (isset($oUserIonauth) && is_object($oUserIonauth) && $oUserIonauth->active=='0' && $oUserIonauth->activation_code!=''){
    ?>
    <div class="row">
        <div class="col-12">
            <div class="alert alert-danger" role="alert">
                <strong>Alert ! </strong>Ce compte n'est pas encore activé dans le système, pour l'activer cliquer
                <span><a href="<?php echo base_url().'auth/activate/'.$oUserIonauth->id.'/'.$oUserIonauth->activation_code;?>" target="_blank" class="btn btn-success">ICI</a></span>
            </div>
        </div>
    </div>
    <?php
}
?>

<script type="text/javascript" src="<?php echo GetJsPath("front/");?>/fields.check.js"></script>

    <script>
	
	function getCP(){
	      var ivilleId = jQuery('#txtVille').val();     
          jQuery.get(
            '<?php echo site_url("front/particuliers/getPostalCodeadmin"); ?>' + '/' + ivilleId,
            function (zReponse)
            {
                // alert (zReponse) ;
                jQuery('#txtCodePostal').val(zReponse) ;       
               
             
           });
	}
	
	function submit_btnSinscrire() {
		
				
				
				var txtError = "";
				
				var txtNom = $("#txtNom").val();
				if(txtNom=="") {
					txtError += "- Veuillez indiquer Votre nom <br/>";    
				}
				
				  var ivilleId = $('#txtVille').val();
				  if (ivilleId == 0) {
					  txtError += "- Vous devez sélectionner une ville <br/>";
				  }
				  
				  var txtCodePostal = $('#txtCodePostal').val();
				  if (txtCodePostal == "") {
					  txtError += "- Vous devez indiquer un code postal <br/>";
				  }
				
				var txtLogin = $("#txtLogin").val();
				if(!isEmail(txtLogin)) {
					txtError += "- Votre login doit &ecirc;tre un email valide <br/>";    
				}
				
				var txtLogin_verif = $('#txtLogin_verif').val();
				  if (txtLogin_verif == 1) {
					  txtError += "- Votre Login existe déjà sur notre site <br/>";
				  }
				  
				<?php if($prmId==0) { ?>
				var txtPassword = $("#txtPassword").val();
				if(txtPassword=="") {
					txtError += "- Veuillez indiquer un Password <br/>";    
				}
				
				if($("#txtPassword").val() != $("#txtConfirmPassword").val()) {
						txtError += "- Les deux mots de passe ne sont pas identiques. <br/>";
				}
				<?php } ?>
				
				
				$("#divErrortxtInscription").html(txtError);
				//alert(txtError);
				
				if(txtError == "") {
					$("#frmModificationParticulier").submit();
				}
			
		
		}
		
		
        $(document).ready(function() {
			
			$("#txtDateNaissance" ).datepicker({
				dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
				dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
				monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
				dateFormat: 'DD, d MM yy',
				autoSize: true,
				changeMonth: true,
	            changeYear: true,
				yearRange: '1900:2020'
			});
			
			
			
			jQuery("#txtLogin").blur(function(){
										   //alert('cool');
										   var txtLogin = jQuery("#txtLogin").val();
										   var txtEmail = txtLogin;
										   
										   var value_result_to_show = "0";
										   
										   //alert('<?php //echo site_url("front/particuliers/verifier_login"); ?>' + '/' + txtLogin);
										   //jQuery(".EmailLoading").addClass("EmailLoading_show").removeClass("EmailLoading");
										   jQuery.post(
													'<?php echo site_url("front/particuliers/verifier_login");?>',
													{ txtLogin_var: txtLogin },
													function (zReponse)
													{
														//alert (zReponse) ;
														if (zReponse == "1") {
															value_result_to_show = "1";
														}  
														
														
														if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtLogin_').html(result_to_show);      
															$('#txtLogin_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
															$('#divErrortxtLogin_').html(result_to_show);    
															$('#txtLogin_verif').val("0");
														}
														
													 
												   });
												   
												   
											$.post(
													'<?php echo site_url("front/professionnels/verifier_login_ionauth");?>',
													{ txtLogin_var_ionauth: txtLogin },
													function (zReponse_ionauth)
													{
														//alert (zReponse) ;
														if (zReponse_ionauth == "1") {
															value_result_to_show = "1";
														} 
														
														
														if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtLogin_').html(result_to_show);       
															$('#txtLogin_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
															$('#divErrortxtLogin_').html(result_to_show);    
															$('#txtLogin_verif').val("0");
														}
													 
												   });
												   
											
											jQuery.post(
													'<?php echo site_url("front/particuliers/verifier_email");?>',
													{ txtEmail_var: txtEmail },
													function (zReponse_)
													{
														//alert (zReponse) ;
														if (zReponse_ == "1") {
															value_result_to_show = "1";
														}
														
														
														if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtLogin_').html(result_to_show);      
															$('#txtEmail_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
															$('#divErrortxtLogin_').html(result_to_show);    
															$('#txtEmail_verif').val("0");
														}
														
													 
												   });
												   
												   
											$.post(
													'<?php echo site_url("front/professionnels/verifier_email_ionauth");?>',
													{ txtEmail_var_ionauth: txtEmail },
													function (zReponse_ionauth)
													{
														//alert (zReponse) ;
														//var zReponse_html_ionauth = '';
														if (zReponse_ionauth == "1") {
															value_result_to_show = "1";
														} 
														
														
														
													 	if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtLogin_').html(result_to_show);       
															$('#txtEmail_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
															$('#divErrortxtLogin_').html(result_to_show);  
															$('#txtEmail_verif').val("0");
														}
														
														
												   });
										  
										   jQuery(".FieldError").removeClass("FieldError");
										   //jQuery(".EmailLoading").addClass("EmailLoading").removeClass("EmailLoading_show");
										   
										   });
			
						
			
        });
		
	
		
	function CP_getDepartement() {
		//alert(jQuery('#CodePostalSociete').val());
		jQuery('#departementCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
		var CodePostalSociete = jQuery('#txtCodePostal').val();
			jQuery.post(
					'<?php echo site_url("front/professionnels/departementcp_particulier"); ?>',
					{CodePostalSociete:CodePostalSociete},
					function (zReponse)
					{
						jQuery('#departementCP_container').html(zReponse);
					});
	}
	
	function CP_getVille() {
		//alert(jQuery('#CodePostalSociete').val());
		jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
		var CodePostalSociete = jQuery('#txtCodePostal').val();
			jQuery.post(
					'<?php echo site_url("front/professionnels/villecp_particulier"); ?>',
					{CodePostalSociete:CodePostalSociete},
					function (zReponse)
					{
						jQuery('#villeCP_container').html(zReponse);
					});
	}
	
	function CP_getVille_D_CP() {
		//alert(jQuery('#CodePostalSociete').val());
		jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
		var CodePostalSociete = jQuery('#txtCodePostal').val();
		var departement_id = jQuery('#departement_id').val();
			jQuery.post(
					'<?php echo site_url("front/professionnels/villecp_particulier"); ?>',
					{CodePostalSociete:CodePostalSociete, departement_id: departement_id},
					function (zReponse)
					{
						jQuery('#villeCP_container').html(zReponse);
					});
	}	
		
		
		
    </script>
    
    <script type="text/javascript">
		function return_to_liste_user() {
			document.location = "<?php echo site_url(); ?>admin/users/liste/";
		}
	</script>
    
    <div id="divInscriptionParticulier" class="content" align="center">
	      <br>
	     <a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" class="btn btn-primary" value= "retour au menu" id = "btn"/> </a>
        <h1>Modification</h1>
        <?php if(!empty($objUser) && $objUser->IdUser != "0") { ?>
            <form name="frmModificationParticulier" id="frmModificationParticulier" action="<?php echo site_url("admin/users/fiche_modifier"); ?>" method="POST" enctype="multipart/form-data">
        <?php } else { ?>
            <form name="frmModificationParticulier" id="frmModificationParticulier" action="<?php echo site_url("admin/users/fiche_ajouter"); ?>" method="POST" enctype="multipart/form-data">
        <?php } ?>
            <table width="75%" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <label>Date de naissance : </label>
                    </td>
                    <td>
                        <input type="text" name="Particulier[DateNaissance]" class="form-control" id="txtDateNaissance" value="<?php if(!empty($objUser->DateNaissance)) echo convert_Sqldate_to_Frenchdate($objUser->DateNaissance); ?>" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>Civilit&eacute; : </label>
                    </td>
                    <td>
                        <select name="Particulier[Civilite]" id="txtParticulier" class="form-control">
                            <option <?php if(isset($objUser->IdVille) && $objUser->IdVille == "0") echo "selected='selected'"; ?> value="0">Monsieur</option>
                            <option <?php if(isset($objUser->IdVille) && $objUser->IdVille == "1") echo "selected='selected'"; ?> value="1">Madame</option>
                            <option <?php if(isset($objUser->IdVille) && $objUser->IdVille == "2") echo "selected='selected'"; ?> value="2">Mademoiselle</option>
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>Nom : </label>
                    </td>
                    <td>
                        <input type="text" name="Particulier[Nom]" class="form-control" id="txtNom" value="<?php if(!empty($objUser->Nom)) echo htmlspecialchars(stripcslashes($objUser->Nom)); ?>" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>Pr&eacute;nom : </label>
                    </td>
                    <td>
                        <input type="text" name="Particulier[Prenom]" class="form-control" id="txtPrenom" value="<?php if(!empty($objUser->Prenom)) echo htmlspecialchars(stripcslashes($objUser->Prenom)); ?>" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>Profession : </label>
                    </td>
                    <td>
                        <input type="text" name="Particulier[Profession]" class="form-control" id="txtProfession" value="<?php if(!empty($objUser->Profession)) echo htmlspecialchars(stripcslashes($objUser->Profession)); ?>" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>Adresse : </label>
                    </td>
                    <td>
                        <textarea name="Particulier[Adresse]" class="form-control" id="txtAdresse"><?php if(!empty($objUser->Adresse)) echo htmlspecialchars(stripcslashes($objUser->Adresse)); ?></textarea>
                    </td>
                </tr>
                
                
                
                <tr>
                    <td class="td_part_form">
                        <label>Code postal : </label>
                    </td>
                     <td id ="trReponseVille" name = "trReponseVille"  class="td_part_form">
                        <input type="text" name="Particulier[CodePostal]" class="form-control" id="txtCodePostal" style = "width:273px;" value="<?php if( isset ($objUser) && is_object($objUser)) { echo $objUser->CodePostal; }?>" onblur="javascript:CP_getDepartement();CP_getVille();"/>
                    </td>
                </tr>
                
                
                <tr>
                    <td class="td_part_form">
                        <label>Departement : </label>
                  </td>
                    <td class="td_part_form">
                        <span id="departementCP_container">
                        <select name="Particulier__departement_id" id="departement_id" class="stl_long_input_platinu form-control" disabled="disabled" onchange="javascript:CP_getVille_D_CP();" style="width:273px;">
                            <option value="0">-- Choisir --</option>
                            <?php if(sizeof($colDepartement)) { ?>
                                <?php foreach($colDepartement as $objDepartement) { ?>
                                    <option value="<?php echo $objDepartement->departement_id; ?>"><?php echo $objDepartement->departement_nom; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="td_part_form">
                        <label>Ville : </label>
                  </td>
                    <td class="td_part_form">
                        <span id="villeCP_container">
                        <input type="text" class="form-control" value="<?php if (isset($objUser->IdVille)) echo $this->mdlville->getVilleById($objUser->IdVille)->Nom;?>" name="IdVille_Nom_text" id="IdVille_Nom_text" disabled="disabled" style="width:273px;"/>
                        <input type="hidden" value="<?php if (isset($objUser->IdVille)) echo $objUser->IdVille;?>" name="Particulier[IdVille]" id="txtVille"/>
                        </span>
                    </td>
                </tr>
                
                
                
                <!--<tr>
                    <td>
                        <label>Ville : </label>
                    </td>
                    <td>
                        <select name="Particulier[IdVille]" id="txtVille" onchange ="getCP();">
                            <option value="0">-- Veuillez choisir --</option>
                            <?php //if(sizeof($colVilles)) { ?>
                                <?php //foreach($colVilles as $objVille) { ?>
                                    <option <?php //if(isset($objUser->IdVille) && $objUser->IdVille == $objVille->IdVille) echo "selected='selected'"; ?> value="<?php //echo $objVille->IdVille; ?>"><?php //echo htmlentities($objVille->Nom); ?></option>
                                <?php //} ?>
                            <?php //} ?>
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>Code postal : </label>
                    </td>
                    <td>
                        <input type="text" name="Particulier[CodePostal]" id="txtCodePostal" value="<?php //if(!empty($objUser->CodePostal)) echo htmlspecialchars(stripcslashes($objUser->CodePostal)); ?>" />
                    </td>
                </tr>-->
                
                <tr>
                    <td>
                        <label>N° T&eacute;l&eacute;phone fixe : </label>
                    </td>
                    <td>
                        <input type="text" name="Particulier[Telephone]" class="form-control" id="txtTelephone" value="<?php if(!empty($objUser->Telephone)) echo htmlspecialchars(stripcslashes($objUser->Telephone)); ?>" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>N° T&eacute;l&eacute;phone Portable : </label>
                    </td>
                    <td>
                        <input type="text" name="Particulier[Portable]" class="form-control" id="txtPortable" value="<?php if(!empty($objUser->Portable)) echo htmlspecialchars(stripcslashes($objUser->Portable)); ?>" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>N° de Fax : </label>
                    </td>
                    <td>
                        <input type="text" name="Particulier[Fax]" class="form-control" id="txtFax" value="<?php if(!empty($objUser->Fax)) echo htmlspecialchars(stripcslashes($objUser->Fax)); ?>" />
                    </td>
                </tr>
                
                <!--<tr>
                    <td>
                        <label>Email : </label>
                    </td>
                    <td>
                        <input type="text" name="Particulier[Email]" id="txtEmail" value="<?php //if(!empty($objUser->Email)) echo htmlspecialchars(stripcslashes($objUser->Email)); ?>"/>
                    </td>
                </tr>-->
                
                <tr>
                    <td>
                        <label>Login : </label>
                    </td>
                    <td>
                        <input type="text" name="Particulier[Login]" style = "width:273px;" class="form-control" id="txtLogin" value="<?php if(!empty($objUser->Login)) echo htmlspecialchars(stripcslashes($objUser->Login)); ?>" />
                        <div class="FieldError" id="divErrortxtLogin_"></div>
                        <input type="hidden" name="txtLogin_verif" style = "width:273px;" id="txtLogin_verif" value="0"/>
                    </td>
                </tr>
                
                <!--<tr>
                    <td>
                        <label>R&ocirc;le : </label>
                    </td>
                    <td>
                        <select name="Particulier[UserRole]" id="txtParticulier">
                            <option <?php //if(isset($objUser->UserRole) && $objUser->UserRole == "0") echo "selected='selected'"; ?> value="0">Visiteur</option>
                            <option <?php //if(isset($objUser->UserRole) && $objUser->UserRole == "1") echo "selected='selected'"; ?> value="1">Administrateur</option>
                        </select>
                    </td>
                </tr>-->
                
                <?php if($prmId==0) { ?>
                <tr>
                    <td class="td_part_form">
                        <label>Password *: </label>
                    </td>
                    <td class="td_part_form">
                        <input type="password" class="form-control" name="Particulier_Password" style = "width:273px;" id="txtPassword" value="" />
                    </td>
                </tr>
                
                <tr>
                    <td class="td_part_form">
                        <label>Confirm Password *: </label>
                    </td>
                    <td class="td_part_form">
                        <input type="password" class="form-control" id="txtConfirmPassword" style = "width:273px;" value="" />
						<div class="FieldError" id="divErrortxtPassword"></div>
                    </td>
                </tr>
                <?php } ?>
                
                
                <tr>
                    <td>
                        <?php if(!empty($objUser->IdUser) && $objUser->IdUser != 0) { ?>
                            <input type="hidden" name="Particulier[IdUser]" id="txtIdUser" value="<?php if(!empty($objUser->IdUser)) echo htmlspecialchars(stripcslashes($objUser->IdUser)); ?>" /></td>
                        <?php } ?>
                    <td align="left"><input type="button" onclick="javascript:submit_btnSinscrire();" class="btn btn-success" id="btnSinscrire" name="btnSinscrire" value="Enregistrer" />&nbsp;<input type="button" value="Annuler" class="btn btn-danger" onclick="javascript:return_to_liste_user();"/></td>
                </tr>
                
            </table>
        </form>
    </div>
    
    
    <div id="divErrortxtInscription" style="padding:0px 0px 0px 2px; font-family: Arial; font-size: 12px; color:#F00; margin-top:20px; text-align:center;"></div>
    
<?php $this->load->view("admin/includes/vwFooter2013"); ?>