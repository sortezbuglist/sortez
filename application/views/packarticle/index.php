<?php $data["zTitle"] = 'Packarticle'; ?>
<?php $this->load->view("sortez_vsv/includes/main_header", $data); ?>

<?php //$this->load->view("packarticle/includes/main_modal", $data); ?>

<style type="text/css">
    body {
        background-image: none !important;
    }
    .quota_status_nb {
        font-size: 100px;
        font-weight: bold;
        background-color: #1179AC;
        color: #fff;
        border-radius: 50%;
        line-height: 190%;
        margin-top: 15px;
    }
    .quota_status_txt {
        text-transform: uppercase;
        font-size: 20px;
        font-weight: bold;
        margin: 15px 0;
    }
</style>

<div class="admincontainer_main mt-0 mb-0 ml-auto mr-auto text-center" style="width: 650px;">
    <div class="col-12 title text-center mt-3">
        <div class="vsv_modal_container">
            <a onclick="javascript:window.open('<?php echo base_url(); ?>contact', 'Details annonce', 'width=575, height=600, scrollbars=yes');" href="javascript:void(0);" title="Contact">
                <img src="<?php echo base_url();?>application/resources/sortez/images/packarticle_banner.jpg" class="img-fluid"/>
            </a>
        </div>
    </div>
    <div class="col-12 title text-center mt-3 mb-5">
        <h1>LA GESTION DE VOTRE PACK ARTICLES</h1>
    </div>
    <div class="row mb-5">
        <div class="col text-center">
            <a href="<?php echo site_url('front/utilisateur/contenupro'); ?>" class="btn btn-danger">RETOUR MENU</a>
        </div>
    </div>
    <div class="row account_status">
        <div class="col-12 label_title text-left btn btn-primary">
            <label class="text-uppercase">l'état de votre compte</label>
        </div>
        <div class="col-12" style="min-height: 250px;">
            <?php $this->load->view("packarticle/article_status", $data); ?>
        </div>
    </div>
    <div class="row articles_planing">
        <div class="col-12 label_title text-left btn btn-primary mt-5">
            <label class="text-uppercase">la planification de vos articles</label>
        </div>
        <div class="col-12 pt-3" style="min-height: 250px;">
            <?php $this->load->view("packarticle/article_planing", $data); ?>
        </div>
    </div>
    <div class="row quota_order">
        <div class="col-12 label_title text-left btn btn-primary mt-5">
            <label class="text-uppercase">Je commande un quota complémentaire</label>
        </div>
        <?php $this->load->view("packarticle/order_form", $data); ?>
    </div>
</div>

<?php $this->load->view("sortez_vsv/includes/main_footer"); ?>
