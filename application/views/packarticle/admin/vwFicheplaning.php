<?php $data["zTitle"] = 'Creation Planing'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script>
	function testFormPlaning(){
	var Nommonth = $("#Nommonth").val () ;
	var Nomyear = $("#Nomyear").val ();
    var Nomname = $("#Nomname").val ();
	var zErreur = "" ;
	
	if (Nommonth == ""){
		zErreur += "Veuillez selectionner un Nommonth\n" ;
	}
	if (Nomyear == ""){
		zErreur += "Veuillez selectionner un Nomyear\n" ;
	}
    if (Nomname == ""){
        zErreur += "Veuillez selectionner un Nomname\n" ;
    }
	
	
	if (zErreur != ""){
		alert ('Veuillez indiquer un Nom !') ;
	}else{
		document.frmCreationplaning.submit();
	}
	
	return false;
}
</script>
    
    <div id="divFicheAnnonce" class="content" align="center">
	     <br>
	     <a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" class ="btn btn-primary" value= "Retour au Menu" id = "btn"/> </a><br/>
         <a style = "color:black; text-decoration: none;" href = "<?php echo site_url("admin/palaning/" ) ; ?>"> <input type = "button" class ="btn btn-primary" value= "retour à la liste planing" id = "btn"/> </a>
         
        <form name="frmCreationplaning" id="frmCreationplaning" action="<?php if (isset($oPlan)) { echo site_url("admin/planing/modif_planing/$id"); }else{ echo site_url("admin/planing/creer_planing"); } ?>" method="POST">
		<input type="hidden" name="planing[id]" id="id_id" value="<?php if (isset($oPlan)) echo $oPlan->id ; else echo '0'; ?>" />
            <h1><?php echo $title ; ?></h1>
			<span><?php if (isset($msg)) { echo $msg ; } ?></span>
            <fieldset>
                <legend>Planing</legend>
                <table cellpadding="0" cellspacing="0">
                    
					<tr>
                        <td>
                            <label>Month:</label>
                        </td>
                        <td>
                            <input type="text" name="planing[month]" id="Nommonth" class="form-control" value="<?php if (isset($oPlan)) { echo $oPlan->month ; } ?>" />
                        </td>
                    </tr>
                    <tr>

                        <td>
                            <label>Year:</label>
                        </td>
                        	<td>
                            <input type="text" name="planing[year]" id="Nomyear" class="form-control" value="<?php if (isset($oPlan)) { echo $oPlan->year ; } ?>" />
                        
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Name:</label>
                        </td>
                            <td>
                            <input type="text" name="planing[name]" id="Nomname" class="form-control" value="<?php if (isset($oPlan)) { echo $oPlan->name ; } ?>" />
         
                        </td>
                    </tr>
					<tr>
                        
                        <td align="left">
                        <input type="button" class ="btn btn-danger "  value="Annuler" onclick="javascript:document.location='<?php echo site_url("admin/planing");?>';" />&nbsp;
                        <input type="button" class ="btn btn-success" value="Valider" onclick="javascript:testFormPlaning();" /></td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>


<?php $this->load->view("admin/includes/vwFooter2013"); ?>