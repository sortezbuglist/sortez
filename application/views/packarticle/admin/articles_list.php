<?php $data['empty'] = null; ?>
<?php $this->load->view("admin/vivresaville/includes/main_header", $data); ?>
<?php $this->load->view("packarticle/main_menu", $data); ?>

<h2>Détails du Quota Packarticle</h2>
<div class="col-12">
    <table class="table">
        <tr>
            <th>Professionnel - Commercant</th>
            <td>
                <div class="col-12">
                    <div class="row">
                        <div class="col-12">
                            Nom
                            : <?php if (isset($oPack_order->user_id)) echo $this->Commercant->GetWhere(" user_ionauth_id=" . $oPack_order->user_id . " limit 1")->NomSociete; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            Email
                            : <?php if (isset($oPack_order->user_id)) echo $this->Commercant->GetWhere(" user_ionauth_id=" . $oPack_order->user_id . " limit 1")->Email; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            Contact
                            : <?php if (isset($oPack_order->user_id)) echo $this->Commercant->GetWhere(" user_ionauth_id=" . $oPack_order->user_id . " limit 1")->TelFixe; ?>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <th>Quota - Packarticle demandé</th>
            <td>
                <div class="col-12">
                    <div class="row">
                        <div class="col-12">
                            Détails
                            : <?php if (isset($oPack_order->pack_id)) echo $this->packarticle_pack->getById($oPack_order->pack_id)->title; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            Quota
                            : <?php if (isset($oPack_order->pack_id)) echo $this->packarticle_quota->getById($this->packarticle_pack->getById($oPack_order->pack_id)->quota_id)->quota; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            Prix
                            : <?php if (isset($oPack_order->pack_id)) echo $this->packarticle_pack->getById($oPack_order->pack_id)->price_min; ?>
                            € -
                            <?php if (isset($oPack_order->pack_id)) echo $this->packarticle_pack->getById($oPack_order->pack_id)->price_max; ?>
                            €
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>
<?php //var_dump($packarticle_article); ?>
<hr align="center" width="100%" color="blue" size="8">
<div class="col-12">
    <table class="table">

        <tr>
            <th>Date de création:</th>
            <td>
                <div class="col-12">
                    <div class="row">
                        <div class="col-12">
                            Création
                            : <?php if (isset($oPack_order->creation_date)) echo convert_Sqldate_to_Frenchdate($oPack_order->creation_date); ?>
                        </div>
                    </div>
                </div>
            </td>
        </tr>

        <tr>

            <th>Date de validation:</th>
            <td>
                <div class="col-12">
                    <div class="row">
                        <div class="col-12">
                            Validation
                            : <?php if (isset($oPack_order->validation_date)) echo convert_Sqldate_to_Frenchdate($oPack_order->validation_date); ?>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>
<hr align="center" width="100%" color="blue" size="8">
<h3>Liste des articles</h3>

<?php if (isset($mssg_confirm_update) && $mssg_confirm_update == '1') { ?>
    <div class="alert alert-success">
        <strong>Succèss !</strong> Article mis à jour.
    </div>
<?php } ?>

<script type="application/javascript">
    function get_article_into_sortez(article_id='0') {
        var url = '<?php echo base_url();?>admin/commercants/fiche/'+article_id;
        var win = window.open(url, '_blank');
        win.focus();
    }
</script>

<div class="p-3">
    <div class="row">
        <div class="col-3">
            <div class="row">
                <div class="col-3 p-0">QUOTA</div>
                <div class="col-9">RESERVATION DE L'EDITION</div>
            </div>
        </div>
        <div class="col-4">TITRE DE L'ARTICLE</div>
        <div class="col-5">ETAT</div>
    </div>
    <?php
    /**
     * Created by PhpStorm.
     * User: rand
     * Date: 05/02/2018
     * Time: 12:13
     */
    //var_dump(count($packarticle_order_value));
    $iii = 1;

    if (isset($packarticle_article) && count($packarticle_article) > 0) {
        foreach ($packarticle_article as $item_article) {
            ?>
            <form id="packarticle_article_form_<?php echo $item_article->id; ?>" name="packarticle_article_form"
                  action="" class="" method="post">
                <div class="row pb-2">
                    <div class="col-3">
                        <div class="row">
                            <div class="col-3 p-0">
                                <span class="quota_number form-control"><?php echo $iii; ?></span>
                            </div>
                            <div class="col-9">
                                <?php if (isset($packarticle_planing) && count($packarticle_planing) > 0) { ?>
                                    <select id="packarticle_planing_id_<?php echo $item_article->id; ?>"
                                            name="packarticle_planing_id"
                                            class="form-control">
                                        <option value="0">Réserver l'article</option>
                                        <?php foreach ($packarticle_planing as $item_planing) { ?>
                                            <option value="<?php echo $item_planing->id; ?>" <?php if (isset($item_article->planing_id) && $item_article->planing_id == $item_planing->id) echo 'selected'; ?>><?php echo $item_planing->name; ?></option>
                                        <?php } ?>
                                    </select>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <input type="text" id="packarticle_article_title_<?php echo $item_article->id; ?>"
                               name="packarticle_article_title"
                               value="<?php if (isset($item_article->title)) echo $item_article->title; ?>"
                               class="form-control"/>
                    </div>
                    <div class="col-5">
                        <div class="row">
                            <div class="col-5">
                                <?php if (isset($item_article->in_sortez) && $item_article->in_sortez == '1') { ?>
                                    <button class="btn btn-success"
                                            id="add_into_article_<?php echo $item_article->id; ?>"
                                            name="add_into_article"
                                            title="Déjà créé dans la gestion des articles Sortez.org"
                                            onclick="return false;">Enregistré
                                    </button>
                                <?php } else { ?>
                                    <button class="btn btn-primary"
                                            id="add_into_article_<?php echo $item_article->id; ?>"
                                            name="add_into_article"
                                            title="Créer l'article dans la gestion des articles Sortez.org"
                                            <?php if ($item_article->title=="") {?>disabled<?php } ?>
                                            onclick="javascript:get_article_into_sortez('<?php if (isset($oPack_order->user_id)) echo $this->Commercant->GetWhere(" user_ionauth_id=" . $oPack_order->user_id . " limit 1")->IdCommercant;?>');return false;">A Créer
                                    </button>
                                <?php } ?>
                            </div>
                            <div class="col-4">
                                <?php
                                if (isset($item_article->status_id) && $item_article->status_id == '0') $class_status_id = 'danger';
                                elseif (isset($item_article->status_id) && $item_article->status_id == '1') $class_status_id = 'warning';
                                elseif (isset($item_article->status_id) && $item_article->status_id == '2') $class_status_id = 'info';
                                elseif (isset($item_article->status_id) && $item_article->status_id == '3') $class_status_id = 'success';
                                else $class_status_id = 'default';
                                ?>
                                <select id="packarticle_article_status_<?php echo $item_article->id; ?>"
                                        name="packarticle_article_status"
                                        class="btn btn-<?php echo $class_status_id;?> form-control">
                                    <option class="btn btn-danger" value="0" <?php if (isset($item_article->status_id) && $item_article->status_id == '0') echo 'selected'; ?>>
                                        En attente
                                    </option>
                                    <option class="btn btn-warning" value="1" <?php if (isset($item_article->status_id) && $item_article->status_id == '1') echo 'selected'; ?>>
                                        Réservé
                                    </option>
                                    <option class="btn btn-info" value="2" <?php if (isset($item_article->status_id) && $item_article->status_id == '2') echo 'selected'; ?>>
                                        Enregistré
                                    </option>
                                    <option class="btn btn-success" value="3" <?php if (isset($item_article->status_id) && $item_article->status_id == '3') echo 'selected'; ?>>
                                        Edité
                                    </option>
                                </select>
                            </div>
                            <div class="col-3">
                                <button class="btn btn-info"
                                        type="submit"
                                        id="packarticle_article_submit_<?php echo $item_article->id; ?>"
                                        name="packarticle_article_submit"
                                        value="Enregistrer"
                                        title="Enregistrer l'article">Enregistrer
                                </button>
                                <input type="hidden" value="<?php echo $item_article->id; ?>"
                                       id="packarticle_article_id_<?php echo $item_article->id; ?>"
                                       name="packarticle_article_id"/>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <?php
            ++$iii;
        }
    }
    ?>
</div>

<?php $this->load->view("admin/vivresaville/includes/main_footer", $data); ?>
