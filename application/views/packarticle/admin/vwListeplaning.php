<?php $data["zTitle"] = "Gestion de planing"; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>
<?php $this->load->view("packarticle/main_menu", $data); ?>

<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra'],  headers: {2: { sorter: false}, 3: {sorter: false}, 4: {sorter: false} }})
				.tablesorterPager({container: $("#pager")});
		});
        });
    function confirm_delete_user(IdRuser){
        if (confirm("Voulez-vous supprimer cet Administrateur ?")) {
           document.location="<?php if(isset($objPlan->id)) echo site_url('admin/planing/delete/'.$objPlan->id) ; ?>/"+IdRuser;
       }
    }
</script>
    <div id="divAdminHome" class="content" align="center">
	     <p><a style = "color:black; text-decoration:none;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" class ="btn btn-primary" value= "retour au menu" id = "btn" onclick="document.location='<?php echo site_url("admin/home/" );?>';"/>  </a></p>
		 <p><a style = "color:black;text-decoration:none;" href = "<?php echo site_url("admin/planing/fiche_planing/0") ;?>"> <input type = "button" class ="btn btn-primary" value= "Ajouter planing" id = "btn"/>  </a></p>
        <br><div class="H1-C">Liste de planing</div><br>
         Nombre de lignes :&nbsp;
        <select name="cmbNbLignes" onchange="document.getElementById('btnSearch').click(); return false;">
            <option value="50" <?php if (isset($NbLignes)) echo SelectOption($NbLignes, 50); ?>>50 par page</option>
            <option value="100" <?php if (isset($NbLignes)) echo SelectOption($NbLignes, 100); ?>>100 par page</option>
            <option value="200" <?php if (isset($NbLignes)) echo SelectOption($NbLignes, 200); ?>>200 par page</option>
        </select>
        
        <div style="color:#33CC33"><?php if (isset($msg)) echo $msg;?></div>
        <table cellpadding="1" class="tablesorter" style="text-align:left">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>month</th>
                    <th>year</th>
                    <th>name</th>
                    <th>Modification</th>
                    <th>Supréssion</th>
                </tr>
            </thead>
            <tbody>
            <?php if (count($colPlan)>0) { ?>
                <?php foreach($colPlan as $objPlan) { ?>
                    <tr>
                        <td>
                            <a href="<?php  echo site_url("admin/planing/fiche_planing/".$objPlan->id); ?>">
                                <?php echo $objPlan->id; ?>
                            </a>
                        </td>
                        <td> 
                    <?php echo htmlspecialchars(stripcslashes ($objPlan->month)); ?></td>
                        <td><?php echo htmlspecialchars(stripcslashes($objPlan->year)); ?></td>
                         <td> 
                    <?php echo htmlspecialchars(stripcslashes ($objPlan->name)); ?></td>
                        <td>
                            <a href="<?php if (isset($objPlan->id)) echo site_url("admin/planing/fiche_planing/".$objPlan->id); ?>">
                                Modifier
                            </a>
                        </td>
                        <td>
                             <a href="javascript:void(0);" onclick="javascript:confirm_delete_user(<?php echo($objPlan->id); ?>);">
                                Supprimer
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
            </tbody>
        </table>
		<div id="pager" class="pager">
			<img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
			<img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
			<input type="text" class="pagedisplay"/>
			<img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
			<img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
			<select class="pagesize" style="visibility:hidden">
				<option selected="selected"  value="10">10</option>
				<option value="20">20</option>
				<option value="30">30</option>
				<option  value="40">40</option>
			</select>
		</div>
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>