
<?php $data['empty'] = null; ?>
<?php $this->load->view("admin/vivresaville/includes/main_header", $data); ?>
<?php $this->load->view("packarticle/main_menu", $data); ?>

<h2>Validation des commandes</h2>
<div class="col-12">
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Nom du Commercant</th>
            <th>Quota</th>
            <th>Détails du pack</th>
            <th>Info sur l'etat des articles</th>
            <th>Etat</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        <?php if (isset($objOrder) && count($objOrder) > 0) { ?>
            <?php foreach ($objOrder as $item) { ?>
                <tr>
                    <td><?php if (isset($item->id)) echo $item->id; ?></td>
                    <td><?php if (isset($item->user_id)) echo $this->Commercant->GetWhere(" user_ionauth_id=".$item->user_id." limit 1")->NomSociete; ?></td>
                    <td><?php if (isset($item->pack_id)) echo $this->packarticle_quota->getById($this->packarticle_pack->getById($item->pack_id)->quota_id)->quota;?></td>
                    <td><?php if (isset($item->pack_id)) echo $this->packarticle_pack->getById($item->pack_id)->title; ?></td>
                    <td>
                        <div><span class="text-danger">En attente : </span><?php echo count($this->packarticle_article->getWhere(" order_id=".$item->id." and status_id=0 "));?></div>
                        <div><span class="text-primary">Planifié : </span><?php echo count($this->packarticle_article->getWhere(" order_id=".$item->id." and status_id=1 "));?></div>
                        <div><span class="text-warning">Créé : </span><?php echo count($this->packarticle_article->getWhere(" order_id=".$item->id." and status_id=2 "));?></div>
                        <div><span class="text-success">Edité : </span><?php echo count($this->packarticle_article->getWhere(" order_id=".$item->id." and status_id=3 "));?></div>
                    <td>

                        <?php
                        if (isset($item->status_id) && $item->status_id==1) echo '<div class="alert alert-danger" role="alert">En attente de validation</div>';
                        if (isset($item->status_id) && $item->status_id==2) echo '<div class="alert alert-success" role="alert">Validé</div>';
                        if (isset($item->status_id) && $item->status_id==3) echo '<div class="alert alert-warning" role="alert">Expiré</div>';
                        ?>
                    </td>
                    <td><?php if (isset($item->status_id) && $item->status_id==1) echo '<a href="'.site_url("admin/packarticle/fiche_packarticle_order/").$item->id.'" class="btn btn-success">Valider</a>';?></td>
                    <td><?php if (isset($item->status_id) && $item->status_id==2) echo '<a href="'.site_url("admin/packarticle/details_packarticle_order/").$item->id.'" class="btn btn-primary">Détails</a>';?></td>
                    <td>
                        <button class="btn btn-success"
                                id="btn_delete_pack_order"
                                name="btn_delete_pack_order"
                                title="Supprimer"
                                onclick="confirm_delete_order_pack(<?php echo $item->id;?>);"
                        >Supprimer</button>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
    </table>
</div>

<script type="application/javascript">
    function confirm_delete_order_pack(order_id=0){
        if (confirm("Voulez vous supprimer cette commande avec ces contenus ?")) {
            $.post(
                '<?php echo base_url();?>admin/packarticle/delete_article_order',
                {
                    order_id: order_id
                }
                ,
                function (zReponse)
                {
                    alert(zReponse);
                    location.reload();
                }
            );
        }
    }
</script>

<?php $this->load->view("admin/vivresaville/includes/main_footer", $data); ?>
