<?php $data['empty'] = null; ?>
<?php $this->load->view("admin/vivresaville/includes/main_header", $data); ?>
<?php $this->load->view("packarticle/main_menu", $data); ?>
<?php
class pack_objet extends CI_Controller{

    function __construct() {
        parent::__construct();

        $this->load->library('session');
        $this->load->library('ion_auth');
        $this->load->model("packarticle_object");
        $this->load->model("ion_auth_used_by_club");

        if (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('domain_from', '1');
            redirect("connexion");
        }
    }

    function index(){
        $this->liste();
    }

     function liste() {
        $objObjet = $this->packarticle_object->GetAll();
        $data["colObjet"] = $objObjet;

        if($this->session->flashdata('mess_editPackObjet')=='1') $data['msg'] = '<strong style="color:#060">Type enregistré !</strong>';
        if($this->session->flashdata(' mess_editPackObjet')=='2') $data['msg'] = '<strong style="color:#F00">Cet objet ne peut être supprimé ! Objet lié à une catégorie !</strong>';
        if($this->session->flashdata(' mess_editPackObjet')=='3') $data['msg'] = '<strong style="color:#060">Objet supprimé !</strong>';

        $this->load->view("admin/vwListePackObjet",$data);
    }

    function fiche_pack_objet($IdPackObjet) {

        if ($IdPackObjet != 0){
            $data["title"] = "Modification  Objet" ;
            $data["oObjet"] = $this->packarticle_object->getById($IdPackObjet) ;
            $data["id"] = $IdPackObjet;
            $this->load->view('admin/vwFichePackObjet', $data) ;

        }else{
            $data["title"] = "Creer un Objet" ;
            $this->load->view('admin/vwFichePackObjet', $data) ;
        }
    }

    function creer_pack_objet(){
        $oObjet = $this->input->post("pack_objet") ;
        $this->packarticle_object->insertpackarticle_object($oObjet);
        $data["msg"] = "Objet ajouté" ;

        $objObjet = $this->packarticle_object->GetAll();
        $data["colObjet"] = $objObjet;
        $this->load->view("admin/vwListePackObjet",$data);
    }

    function modif_pack_objet($IdPackObjet){
        $oObjet = $this->input->post("pack_objet") ;
        $this->packarticle_object->updatepackarticle_objet($oObjet);
        $data["msg"] = "Objet enregistré" ;

        $objObjet = $this->packarticle_object->GetAll();
        $data["colObjet"] = $objObjet;
        $this->load->view("admin/vwListePackObjet",$data);
    }

    
    function delete($prmId) {

        if(is_numeric(trim($prmId))) {
            //verify if category contain active article********
            $oCategorie = $this->packarticle_object->verifier_pack_objet($prmId) ;
            //$oCategorie = array();
            if (count($oCategorie)==0){
                $this->packarticle_object->supprimepackarticle_objet($prmId);
                $this->session->set_flashdata('mess_editPackObjet', '3');
            } else {
                $this->session->set_flashdata('mess_editPackObjet', '2');
            }

            //$this->firephp->log($oCategorie, 'oCategorie');

            redirect("admin/pack_objet");
        }
        else {
            redirect("admin/pack_objet");
        }

    }


}
    

