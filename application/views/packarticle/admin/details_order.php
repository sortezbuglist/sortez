<?php $data['empty'] = null; ?>
<?php $this->load->view("admin/vivresaville/includes/main_header", $data); ?>
<?php $this->load->view("packarticle/main_menu", $data); ?>

<h2>Validation des commandes</h2>
<form method="post" action="">
<div class="col-12">
    <table class="table">
        <tr>
            <th>Professionnel - Commercant</th>
            <td>
                <div class="col-12">
                    <div class="row">
                        <div class="col-12">
                            Nom
                            : <?php if (isset($oPack_order->user_id)) echo $this->Commercant->GetWhere(" user_ionauth_id=" . $oPack_order->user_id . " limit 1")->NomSociete; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            Email
                            : <?php if (isset($oPack_order->user_id)) echo $this->Commercant->GetWhere(" user_ionauth_id=" . $oPack_order->user_id . " limit 1")->Email; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            Contact
                            : <?php if (isset($oPack_order->user_id)) echo $this->Commercant->GetWhere(" user_ionauth_id=" . $oPack_order->user_id . " limit 1")->TelFixe; ?>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <th>Quota - Packarticle demandé</th>
            <td>
                <div class="col-12">
                    <div class="row">
                        <div class="col-12">
                            Détails
                            : <?php if (isset($oPack_order->pack_id)) echo $this->packarticle_pack->getById($oPack_order->pack_id)->title; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            Quota
                            : <?php //if (isset($oPack_order->pack_id)) echo $this->packarticle_quota->getById($this->packarticle_pack->getById($oPack_order->pack_id)->quota_id)->quota; ?>
                            <select id="quota_id_validation_value" name="quota_id_validation_value" class="btn btn-info">
                                <?php foreach ($all_quota as $item) {?>
                                    <option value="<?php echo $item->id;?>" <?php if ($this->packarticle_pack->getById($oPack_order->pack_id)->quota_id == $item->id) echo 'selected';?>><?php echo $item->quota;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            Prix
                            : <?php if (isset($oPack_order->pack_id)) echo $this->packarticle_pack->getById($oPack_order->pack_id)->price_min; ?>
                            € -
                            <?php if (isset($oPack_order->pack_id)) echo $this->packarticle_pack->getById($oPack_order->pack_id)->price_max; ?>
                            €
                        </div>
                    </div>
                </div>
            </td>
        </tr>

    </table>
</div>
<?php if(isset($oPack_order->status_id) && $oPack_order->id!='2') {?>
<div class="col-12">
    <fieldset>

            <div class="row">
                <div class="col-6">
                    <input type="submit" value="VALIDER LA COMMANDE" id="packarticle_pack_submit"
                           name="packarticle_pack_submit" class="btn btn-success w-100">
                    <input type="hidden" value="2" id="packarticle_order_status_id" name="packarticle_order_status_id"/>
                </div>
                <div class="col-6">
                    <input type="hidden" value="<?php if(isset($oPack_order->id) && $oPack_order->id!='') echo $oPack_order->id;?>" id="packarticle_order_id" name="packarticle_order_id">
                    <input type="hidden" value="<?php if (isset($oPack_order->pack_id)) echo $this->packarticle_quota->getById($this->packarticle_pack->getById($oPack_order->pack_id)->quota_id)->quota; ?>" id="packarticle_order_quota" name="packarticle_order_quota">
                </div>
                <div class="col-6 mt-3">
                    <?php if (isset($mssg_confirm_update) && $mssg_confirm_update=='1') {?>
                        <div class="alert alert-success" role="alert">
                            <strong>Succès !</strong> La commande de quota a bien été modifiée.
                        </div>
                    <?php } ?>
                </div>
            </div>

    </fieldset>
</div>
<?php } ?>
</form>

<?php $this->load->view("admin/vivresaville/includes/main_footer", $data); ?>
