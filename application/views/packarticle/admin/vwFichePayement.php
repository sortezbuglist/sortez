<?php $data["zTitle"] = 'Creation Pack article payement'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script>
	function testFormPayement(){
	var Nomtitle = $("#Nomtitle").val () ;
	var Nomdesc = $("#Nomdesc").val ()
	var zErreur = "" ;
	
	if (Nomtitle == ""){
		zErreur += "Veuillez selectionner un Nomtitle\n" ;
	}
	if (Nomdesc == ""){
		zErreur += "Veuillez selectionner un Nomdesc\n" ;
	}
	
	
	if (zErreur != ""){
		alert ('Veuillez indiquer un Nom !') ;
	}else{
		document.frmCreationPayement.submit();
	}
	
	return false;
}
</script>
    
    <div id="divFicheAnnonce" class="content" align="center">
	     <br>
	     <a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" class ="btn btn-primary" value= "Retour au Menu" id = "btn"/> </a><br/>
         <a style = "color:black; text-decoration: none;" href = "<?php echo site_url("admin/payement/" ) ; ?>"> <input type = "button" class ="btn btn-primary" value= "retour à la liste de payement" id = "btn"/> </a>
         
        <form name="frmCreationPayement" id="frmCreationPayement" action="<?php if (isset($oPaye)) { echo site_url("admin/payement/modif_payement/$id"); }else{ echo site_url("admin/payement/creer_payement"); } ?>" method="POST">
		<input type="hidden" name="payement[id]" id="id_id" value="<?php if (isset($oPaye)) echo $oPaye->id ; else echo '0'; ?>" />
            <h1><?php echo $title ; ?></h1>
			<span><?php if (isset($msg)) { echo $msg ; } ?></span>
            <fieldset>
                <legend>Pack article payement</legend>
                <table cellpadding="0" cellspacing="0">
                    
					<tr>
                        <td>
                            <label>title:</label>
                        </td>
                        <td>
                            <input type="text" name="payement[title]" id="Nomtitle" class="form-control" value="<?php if (isset($oPaye)) { echo $oPaye->title ; } ?>" />
                        </td>
                    </tr>
                    <tr>

                        <td>
                            <label>desc:</label>
                        </td>
                        	<td>
                            <input type="text" name="payement[desc]" id="Nomdesc" class="form-control" value="<?php if (isset($oPaye)) { echo $oPaye->desc ; } ?>" />
         
                        </td>
                    </tr>
					<tr>
                        
                        <td align="left">
                        <input type="button" class ="btn btn-danger "  value="Annuler" onclick="javascript:document.location='<?php echo site_url("admin/payement");?>';" />&nbsp;
                        <input type="button" class ="btn btn-success" value="Valider" onclick="javascript:testFormPayement();" /></td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>


<?php $this->load->view("admin/includes/vwFooter2013"); ?>