<?php $data["zTitle"] = 'Creation Pack article Objet'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script>
	function testFormPackObject(){
	var Nomtitle = $("#Nomtitle").val () ;
	var Nomdesc = $("#Nomdesc").val ()
	var zErreur = "" ;
	
	if (Nomtitle == ""){
		zErreur += "Veuillez selectionner un Nomtitle\n" ;
	}
	if (Nomdesc == ""){
		zErreur += "Veuillez selectionner un Nomdesc\n" ;
	}
	
	
	if (zErreur != ""){
		alert ('Veuillez indiquer un Nom !') ;
	}else{
		document.frmCreationpackObjet.submit();
	}
	
	return false;
}
</script>
    
    <div id="divFicheAnnonce" class="content" align="center">
	     <br>
	     <a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" class ="btn btn-primary" value= "Retour au Menu" id = "btn"/> </a><br/>
         <a style = "color:black; text-decoration: none;" href = "<?php echo site_url("admin/pack_objet/" ) ; ?>"> <input type = "button" class ="btn btn-primary" value= "retour à la liste pack_objet" id = "btn"/> </a>
         
        <form name="frmCreationpackObjet" id="frmCreationpackObjet" action="<?php if (isset($oObjet)) { echo site_url("admin/pack_objet/modif_pack_objet/$id"); }else{ echo site_url("admin/pack_objet/creer_pack_objet"); } ?>" method="POST">
		<input type="hidden" name="pack_objet[id]" id="id_id" value="<?php if (isset($oObjet)) echo $oObjet->id ; else echo '0'; ?>" />
            <h1><?php echo $title ; ?></h1>
			<span><?php if (isset($msg)) { echo $msg ; } ?></span>
            <fieldset>
                <legend>Pack article objet</legend>
                <table cellpadding="0" cellspacing="0">
                    
					<tr>
                        <td>
                            <label>title:</label>
                        </td>
                        <td>
                            <input type="text" name="pack_objet[title]" id="Nomtitle" class="form-control" value="<?php if (isset($oObjet)) { echo $oObjet->title ; } ?>" />
                        </td>
                    </tr>
                    <tr>

                        <td>
                            <label>desc:</label>
                        </td>
                        	<td>
                            <input type="text" name="pack_objet[desc]" id="Nomdesc" class="form-control" value="<?php if (isset($oObjet)) { echo $oObjet->desc ; } ?>" />
         
                        </td>
                    </tr>
					<tr>
                        
                        <td align="left">
                        <input type="button"  class ="btn btn-danger "  value="Annuler" onclick="javascript:document.location='<?php echo site_url("admin/pack_objet");?>';" />&nbsp;
                        <input type="button" class ="btn btn-success" value="Valider" onclick="javascript:testFormPackObject();" /></td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>


<?php $this->load->view("admin/includes/vwFooter2013"); ?>