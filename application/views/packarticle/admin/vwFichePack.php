<?php $data["zTitle"] = 'Creation Pack '; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script>
	function testFormPack(){
	var Nomtitle = $("#Nomtitle").val () ;
	var Nomdesc = $("#Nomdesc").val ();
    var Nomprice_min = $("#Nomprice_min").val () ;
    var Nomprice_max = $("#Nomprice_max").val ();
	var zErreur = "" ;
	
	if (Nomtitle == ""){
		zErreur += "Veuillez selectionner un Nomtitle\n" ;
	}
	if (Nomdesc == ""){
		zErreur += "Veuillez selectionner un Nomdesc\n" ;
	}
    if (Nomprice_min == ""){
        zErreur += "Veuillez selectionner un Nomprice_min\n" ;
    }
    if (Nomprice_max == ""){
        zErreur += "Veuillez selectionner un Nomprice_max\n" ;
    }
	
	
	if (zErreur != ""){
		alert ('Veuillez indiquer un Nom !') ;
	}else{
		document.frmCreationPack.submit();
	}
	
	return false;
}
</script>
    
    <div id="divFicheAnnonce" class="content" align="center">
	     <br>
	     <a style = "color:black;" href = "<?php echo site_url("admin/home/" ) ; ?>"> <input type = "button" class ="btn btn-primary" value= "Retour au Menu" id = "btn"/> </a><br/>
         <a style = "color:black; text-decoration: none;" href = "<?php echo site_url("admin/pack/" ) ; ?>"> <input type = "button" class ="btn btn-primary" value= "retour à la liste pack" id = "btn"/> </a>
         
        <form name="frmCreationPack" id="frmCreationPack" action="<?php if (isset($oPack)) { echo site_url("admin/pack/modif_pack/$id"); }else{ echo site_url("admin/pack/creer_pack"); } ?>" method="POST">
		<input type="hidden" name="pack[id]" id="id_id" value="<?php if (isset($oPack)) echo $oPack->id ; else echo '0'; ?>" />
            <h1><?php echo $title ; ?></h1>
			<span><?php if (isset($msg)) { echo $msg ; } ?></span>
            <fieldset>
                <legend>Pack article pack</legend>
                 <table cellpadding="0" cellspacing="0">
                    
					<tr>
                        <td>
                            <label>title:</label>
                        </td>
                        <td>
                            <input type="text" name="pack[title]" id="Nomtitle" class="form-control" value="<?php if (isset($oPack)) { echo $oPack->title ; } ?>" />
                        </td>
                    </tr>
                    <tr>

                        <td>
                            <label>desc:</label>
                        </td>
                        	<td>
                            <input type="text" name="pack[desc]" id="Nomdesc" class="form-control" value="<?php if (isset($oPack)) { echo $oPack->desc ; } ?>" />
         
                        </td>
                    </tr>
                <tr>
                    <td>
                        <label>quota : </label>
                    </td>
                    <td>
                        <select name="Particulier[quota]" id="txtParticulier" class="form-control">
                            <option <?php if(isset($oquota->quota_id) &&  $oquota->quota_id == "1") echo "selected='selected'"; ?> value="1">5</option>
                            <option <?php if(isset($oquota->quota_id) && $oquota->quota_id == "2") echo "selected='selected'"; ?> value="2">10</option>
                            <option <?php if(isset($oquota->quota_id) && $oquota->quota_id == "3") echo "selected='selected'"; ?> value="3">15</option>
                            <option <?php if(isset($oquota->quota_id) && $oquota->quota_id == "4") echo "selected='selected'"; ?> value="4">20</option>
                            <option <?php if(isset($oquota->quota_id) && $oquota->quota_id == "5") echo "selected='selected'"; ?> value="5">25</option>
                        </select>
                    </td>
                </tr>
                <tr>

                    <td>
                        <label>type : </label>
                    </td>
                    <td>
                        <select name="Particulier[type]" id="txtParticulier" class="form-control">
                            <option <?php if(isset($otype->type_id) && $otype->type_id== "1") echo "selected='selected'"; ?> value="1">article</option>

                        </select>
                    </td>
                </tr>
                    <tr>
                        <td>
                            <label>price_min:</label>
                        </td>
                        <td>
                            <input type="text" name="pack[price_min]" id="Nomprice_min" class="form-control" value="<?php if (isset($oPack)) { echo $oPack->price_min ; } ?>" />
                        </td>
                    </tr>
                    <tr>

                        <td>
                            <label>price_max:</label>
                        </td>
                            <td>
                            <input type="text" name="pack[price_max]" id="Nomprice_max" class="form-control" value="<?php if (isset($oPack)) { echo $oPack->price_max ; } ?>" />
         
                        </td>
                    </tr>
					<tr>
                        
                        <td align="left" >
                        <input type="button" class ="btn btn-danger "  value="Annuler" onclick="javascript:document.location='<?php echo site_url("admin/pack");?>';" />&nbsp;
                        <input type="button" class ="btn btn-success" value="Valider" onclick="javascript:testFormPack();" /></td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>


<?php $this->load->view("admin/includes/vwFooter2013"); ?>