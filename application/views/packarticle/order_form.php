<?php $data['empty']=null; ?>
<div class="col-12 p-3">
    <form method="post" action="" name="packarticle_form" id="packarticle_form">
        <div class="row">
            <div class="col-12">
                <?php if (isset($packarticle_pack_allpack) && count($packarticle_pack_allpack) > 0) { ?>
                    <select id="packarticle_pack_id" name="packarticle_pack_id" class="form-control">
                        <?php foreach ($packarticle_pack_allpack as $packarticle_pack_item) { ?>
                            <option value="<?php echo $packarticle_pack_item->id ?>"><?php echo $packarticle_pack_item->title ?></option>
                        <?php } ?>
                    </select>
                <?php } ?>
            </div>
            <div class="col-12">
                <?php if (isset($packarticle_payment) && count($packarticle_payment) > 0) { ?>
                    <select id="packarticle_payment_id" name="packarticle_payment_id" class="form-control">
                        <?php foreach ($packarticle_payment as $packarticle_payment_item) { ?>
                            <option value="<?php echo $packarticle_payment_item->id ?>"><?php echo $packarticle_payment_item->title ?></option>
                        <?php } ?>
                    </select>
                <?php } ?>
            </div>
            <div class="col-12">
                <?php if (isset($packarticle_object) && count($packarticle_object) > 0) { ?>
                    <select id="packarticle_object_id" name="packarticle_object_id" class="form-control">
                        <?php foreach ($packarticle_object as $packarticle_object_item) { ?>
                            <option value="<?php echo $packarticle_object_item->id ?>"><?php echo $packarticle_object_item->title ?></option>
                        <?php } ?>
                    </select>
                <?php } ?>
            </div>
            <div class="col-12 pt-3">
                <a href="javascript:void(0);" id="packarticle_order_submit" class="btn btn-success">Je valide ma commande</a>
            </div>
            <div class="col-12" id="packarticle_order_result"></div>

        </div>
    </form>
</div>
<script type="application/javascript">
    jQuery(document).ready(function () {
        jQuery('#packarticle_order_submit').click(function() {
            if (confirm("Valider votre commande Packarticle !")) {
                jQuery("#packarticle_order_result").html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...."/>');
                var packarticle_pack_id = jQuery('#packarticle_pack_id').val();
                var packarticle_payment_id = jQuery('#packarticle_payment_id').val();
                var packarticle_object_id = jQuery('#packarticle_object_id').val();
                jQuery.post(
                    '<?php echo base_url();?>front/packarticle/saveorder/',
                    {
                        packarticle_pack_id: packarticle_pack_id,
                        packarticle_payment_id: packarticle_payment_id,
                        packarticle_object_id: packarticle_object_id
                    },
                    function (zReponse) {
                        jQuery("#packarticle_order_result").html(zReponse);
                        jQuery("#packarticle_order_list").html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...."/>');
                        jQuery.post(
                            '<?php echo base_url();?>front/packarticle/order_list/',
                            {
                                packarticle_object_id: packarticle_object_id
                            },
                            function (zReponse2) {
                                jQuery("#packarticle_order_list").html(zReponse2);
                            }
                        );
                    }
                );
            }
        });
    });
</script>
<div class="col-12 mt-5">
    <div class="row">
        <div class="col-12" id="packarticle_order_list">
            <?php $this->load->view('packarticle/order_list_vw', $data);?>
        </div>
    </div>
</div>
