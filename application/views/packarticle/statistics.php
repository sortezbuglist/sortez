<?php $data['empty'] = null; ?>
<?php $this->load->view("admin/vivresaville/includes/main_header", $data); ?>
<?php $this->load->view("packarticle/main_menu", $data); ?>
<script type="text/javascript">function validerForm() {
        document.filtre.submit();
    }
</script>
<div class="col p-3" style="background-color: #ccc;">
    <center><h4 style="color: blue;text-decoration: underline">Filtrer les articles:</h4></center>
    <center>
        <div class="col">
            <form action="<?php echo site_url('admin/packarticle/statistics/'); ?>" name="filtre" method="post" id="">
                <div class="row">
                    <div class="col-4">
                        <label for="username">
                            <strong style="color: green;text-decoration: underline">Contact commercant:</strong>
                        </label>
                        <select class="selectpicker show-tick form-control" name="user_id"
                                onchange="javascript:validerForm();">
                            <option value="0">--- choisir ---</option>
                            <?php foreach ($username as $name) { ?>
                                <option <?php if (isset($selected_user_id) && $selected_user_id == $name->id) echo 'selected="selected"'; ?>
                                        value="<?php echo $name->id; ?>"><?php echo $name->first_name; ?> / <?php echo $name->username; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-4">
                        <label for="edition">
                            <strong style="color: green;text-decoration: underline">Edition:</strong>
                        </label>
                        <br>
                        <select class="selectpicker show-tick form-control" name="id_planing"
                                onchange="javascript:validerForm();">
                            <option value="0">--- choisir ---</option>
                            <?php foreach ($edition as $editions) { ?> ?>
                                <option <?php if (isset($selected_id_planing) && $selected_id_planing == $editions->id) echo 'selected="selected"'; ?>
                                        value="<?php echo $editions->id; ?>"><?php echo $editions->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-4">
                        <label for="etat">
                            <strong style="color: green;text-decoration: underline">Etat:</strong>
                        </label>
                        <br>
                        <select class="selectpicker show-tick form-control" name="id_status"
                                onchange="javascript:validerForm();">
                            <option value="0">--- choisir ---</option>
                            <option <?php if (isset($selected_id_status) && $selected_id_status == 0) echo 'selected="selected"'; ?>
                                    value="0">
                                En attente
                            </option>
                            <option <?php if (isset($selected_id_status) && $selected_id_status == 1) echo 'selected="selected"'; ?>
                                    value="1">
                                Planifié
                            </option>
                            <option <?php if (isset($selected_id_status) && $selected_id_status == 2) echo 'selected="selected"'; ?>
                                    value="2">
                                Enregistré
                            </option>
                            <option <?php if (isset($selected_id_status) && $selected_id_status == 3) echo 'selected="selected"'; ?>
                                    value="3">
                                Edité
                            </option>
                        </select>
                    </div>
                    <div class="col"></div>
                    <div class="col"></div>
                </div>
            </form>
        </div>
    </center>
</div>
<center><h1>Liste des articles dans Packarticle</h1></center>
<table class="table table-striped">
    <tr class="font-weight-bold">
        <th>ID</th>
        <th>Titre</th>
        <th>Contact commercant</th>
        <th>Edition</th>
        <th>Etat</th>
        <th></th>
    </tr>
    <?php
    $thisss =& get_instance();
    $thisss->load->library('ion_auth');
    $iii=1;
    foreach ($objArticle as $item) {
        if (isset($item->title) && $item->title != '') {
            ?>
            <tr>
                <td><?php echo $iii;//echo $item->id; ?></td>
                <td><?php echo $item->title; ?></td>
                <td>
                    <?php if(isset($item->user_id)&&$item->user_id!=''&&$item->user_id!='0') echo $thisss->ion_auth->user($item->user_id)->row()->first_name;?> /
                    <?php if(isset($item->user_id)&&$item->user_id!=''&&$item->user_id!='0') echo $thisss->ion_auth->user($item->user_id)->row()->username;?>
                </td>
                <td><?php if(isset($item->planing_id) && $item->planing_id!='' && $item->planing_id!='0') {
                    echo $thisss->packarticle_planing->getById($item->planing_id)->name;//var_dump($result);
                    }?></td>
                <td>
                    <?php if (isset($item->status_id) && $item->status_id == '0') echo 'En attente'; ?>
                    <?php if (isset($item->status_id) && $item->status_id == '1') echo 'Planifié'; ?>
                    <?php if (isset($item->status_id) && $item->status_id == '2') echo 'Enregistré'; ?>
                    <?php if (isset($item->status_id) && $item->status_id == '3') echo 'Edité'; ?>
                </td>
                <td>
                    <a href="<?php echo site_url('admin/packarticle/details_packarticle_order/' . $item->order_id); ?>"
                       class="btn btn-primary">Détails</a>
                </td>
            </tr>
            <?php $iii++;?>
        <?php } ?>
    <?php } ?></table>


<?php //var_dump($selected_user_id); ?>


<?php $this->load->view("admin/vivresaville/includes/main_footer", $data); ?>
