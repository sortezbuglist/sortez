<h3>Demande quota déjà effectuée</h3>
<div class="col-12">
    <table class="table">
        <tr>
            <th>ID:</th>
            <th>Titre</th>
            <th>Création</th>
            <th>Etat</th>
        </tr>
        <?php
        if (count($packarticle_order_value)>0) {
            foreach($packarticle_order_value as $value){
                ?>
                <tr>
                    <td><?php if (isset($value->id)) echo $value->id;?></td>
                    <td><?php if (isset($value->title)) echo $value->title;?></td>
                    <td><?php if (isset($value->creation_date)) echo convertDateWithSlashes($value->creation_date);?></td>
                    <td>
                        <?php if (isset($value->status_id) && $value->status_id == '1') echo "En attente de validation";?>
                        <?php if (isset($value->status_id) && $value->status_id == '2') echo "Validé";?>
                        <?php if (isset($value->status_id) && $value->status_id == '3') echo "Expiré";?>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </table>
</div>
