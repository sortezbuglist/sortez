<?php if (isset($mssg_confirm_update) && $mssg_confirm_update == '1') { ?>
    <div class="alert alert-success">
        <strong>Succèss !</strong> Article mis à jour.
    </div>
<?php } ?>

    <div class="row">
        <div class="col-4">
            <div class="row">
                <div class="col-3 p-0">QUOTA</div>
                <div class="col-9">RESERVATION DE L'EDITION</div>
            </div>
        </div>
        <div class="col-4">TITRE DE L'ARTICLE</div>
        <div class="col-4">ETAT</div>
    </div>
    <script type="application/javascript">
        function add_article_into_sortez(article_id=0) {
            var article_selector = "#packarticle_article_title_" + article_id;
            //alert(article_selector);
            var article_title = jQuery(article_selector).val();
            if (article_title == "") {
                alert('Veuillez indiquer un titre valide pour l\'article');
            } else {
                if (confirm('Voulez-vous créer cet article dans les registres de Sortez.org ?')) {
                    jQuery.ajax({
                        url: '<?php echo site_url("front/packarticle/add_article_into_sortez"); ?>',
                        type: "post",
                        data: {packarticle_article_id: article_id, packarticle_article_title: article_title},
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);
                            if (data.error == null) {
                                get_article_into_sortez(article_id);
                            } else alert(data.error);
                        }
                    });
                }
            }
        }

        function get_article_into_sortez(article_id=0) {
            $.post(
                '<?php echo base_url();?>front/packarticle/get_article_into_sortez/',
                {
                    article_id: article_id
                }
                ,
                function (zReponse) {

                    if (zReponse != "error") {
                        location.reload();
                        //var win = window.open(zReponse);
                        top.location.href=zReponse;
                        win.focus();
                    }else{
                        alert(zReponse);
                    }
                }
            );
        }
    </script>
<?php
/**
 * Created by PhpStorm.
 * User: rand
 * Date: 05/02/2018
 * Time: 12:13
 */
//var_dump(count($packarticle_order_value));

if (isset($packarticle_order_value) && count($packarticle_order_value) > 0) {
    $iii = 1;
    foreach ($packarticle_order_value as $item_order) {
        if (isset($item_order->id)) {
            $packarticle_article_value = $this->packarticle_article->getWhere(" order_id=" . $item_order->id . " order by id asc");
            //var_dump(count($packarticle_article_value));
            if (isset($packarticle_article_value) && count($packarticle_article_value) > 0) {
                foreach ($packarticle_article_value as $item_article) {
                    ?>
                    <form id="packarticle_article_form_<?php echo $item_article->id; ?>" name="packarticle_article_form"
                          action="" class="" method="post">
                        <div class="row pb-2">
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-3 p-0">
                                        <span class="quota_number form-control"><?php echo $iii; ?></span>
                                    </div>
                                    <div class="col-9 pr-0">
                                        <?php if (isset($packarticle_planing) && count($packarticle_planing) > 0) { ?>
                                            <select id="packarticle_planing_id_<?php echo $item_article->id; ?>"
                                                    name="packarticle_planing_id"
                                                    class="form-control">
                                                <option value="0">Réserver l'article</option>
                                                <?php foreach ($packarticle_planing as $item_planing) { ?>
                                                    <option value="<?php echo $item_planing->id; ?>" <?php if (isset($item_article->planing_id) && $item_article->planing_id == $item_planing->id) echo 'selected'; ?>><?php echo $item_planing->name; ?></option>
                                                <?php } ?>
                                            </select>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4 pr-0">
                                <input type="text" id="packarticle_article_title_<?php echo $item_article->id; ?>"
                                       name="packarticle_article_title"
                                       value="<?php if (isset($item_article->title)) echo $item_article->title; ?>"
                                       class="form-control"/>
                            </div>
                            <div class="col-5 pr-0">
                                <div class="row">
                                    <div class="col-3 p-0">
                                        <button class="btn btn-success"
                                                type="submit"
                                                id="packarticle_article_submit_<?php echo $item_article->id; ?>"
                                                name="packarticle_article_submit"
                                                value="Enregistrer"
                                                title="Enregistrer l'article">
                                            <img style="border: none;width:15px;"
                                                 src="<?php echo base_url(); ?>application/resources/privicarte/images/activated_ico.gif">
                                        </button>
                                        <input type="hidden" value="<?php echo $item_article->id; ?>"
                                               id="packarticle_article_id_<?php echo $item_article->id; ?>"
                                               name="packarticle_article_id"/>
                                    </div>
                                    <div class="col-3 p-0">
                                        <?php if (isset($item_article->in_sortez) && $item_article->in_sortez == '1' && count($this->mdlarticle->getById($item_article->article_sortez_id)) > 0) { ?>
                                            <button type="button" class="btn btn-success"
                                                    id="add_into_article_<?php echo $item_article->id; ?>"
                                                    name="add_into_article"
                                                    title="Article existant dans la gestion des articles Sortez.org"
                                                    <?php if ($item_article->title == "") { ?>disabled<?php } ?>
                                                    onclick="get_article_into_sortez(<?php echo $item_article->id; ?>);return false;">
                                                <img style="border: none;width:15px;"
                                                     src="<?php echo base_url(); ?>application/resources/privicarte/images/update_ico.png">
                                            </button>
                                        <?php } else { ?>
                                            <button class="btn btn-primary"
                                                    type="button"
                                                    id="add_into_article_<?php echo $item_article->id; ?>"
                                                    name="add_into_article"
                                                    title="Créer l'article dans la gestion des articles"
                                                    <?php if ($item_article->title == "") { ?>disabled<?php } ?>
                                                    onclick="add_article_into_sortez(<?php echo $item_article->id; ?>);return false;">
                                                <img style="border: none;width:15px;"
                                                     src="<?php echo base_url(); ?>application/resources/privicarte/images/update_ico.png">
                                            </button>
                                        <?php } ?>
                                    </div>
                                    <div class="col-3 p-0">
                                        <button class="btn btn-primary"
                                                type="button"
                                                id="check_into_article_<?php echo $item_article->id; ?>"
                                                name="check_into_article"
                                                title="Voir l'article dans la gestion des articles"
                                            <?php if (!isset($item_article->in_sortez) || $item_article->in_sortez != '1' || count($this->mdlarticle->getById($item_article->article_sortez_id)) == 0 || $item_article->title == "") { ?>
                                                disabled
                                            <?php } ?>
                                                onclick="get_article_into_sortez(<?php echo $item_article->id; ?>);return false;">
                                            <img style="border: none;width:15px;"
                                                 src="<?php echo base_url(); ?>application/resources/privicarte/images/delete_ico.png">
                                        </button>
                                    </div>
                                    <div class="col-3 p-0">
                                        <?php
                                        if (isset($item_article->status_id) && $item_article->status_id == '0') {
                                            $class_status_id = 'danger';
                                            $result_status_id = 'En attente';
                                            $result_status_id_title = 'En attente d\'édition dans Sortez.org';
                                        } elseif (isset($item_article->status_id) && $item_article->status_id == '1') {
                                            $class_status_id = 'warning';
                                            $result_status_id = 'Réservé';
                                            $result_status_id_title = 'Réservé en publication chez Sortez.org';
                                        } elseif (isset($item_article->status_id) && $item_article->status_id == '2') {
                                            $class_status_id = 'info';
                                            $result_status_id = 'Enregistré';
                                            $result_status_id_title = 'Enregistré dans les articles de Sortez.org';
                                        } elseif (isset($item_article->status_id) && $item_article->status_id == '3') {
                                            $class_status_id = 'success';
                                            $result_status_id = 'Edité';
                                            $result_status_id_title = 'Edité dans Sortez Magazine';
                                        } else {
                                            $class_status_id = 'default';
                                            $result_status_id = 'En attente';
                                            $result_status_id_title = 'En attente d\'édition dans Sortez.org';
                                        }
                                        ?>
                                        <div class="btn btn-<?php echo $class_status_id; ?>"
                                             title="<?php echo $result_status_id_title; ?>">
                                            <img style="border: none;width:15px;"
                                                 src="<?php echo base_url(); ?>application/resources/privicarte/images/deactivated_ico.png">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                    <?php
                    ++$iii;
                }
            }
        }
    }
} else {
    echo "Vous n'avez pas encore d'article à planifier, veuillez passer commande !";
}
