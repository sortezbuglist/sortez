<div class="text-center mt-3" style="margin-top: 15px;display: table;width: 100%;">
    <a href="<?php echo site_url('admin/packarticle')?>" class="btn btn-primary col-lg-4 col-4"><h1>Packarticle</h1></a>
</div>
<div class="col-12 mt-2 mb-2" style="margin: 0px 0; padding: 15px;">
    <div class="row">
        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
            <a href="<?php echo site_url('admin/packarticle')?>" class="btn btn-primary">Validation des commandes</a>
        </div>
        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
            <a href="<?php echo site_url('admin/quota')?>" class="btn btn-primary">Gestion des Quota</a>
        </div>
        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
            <a href="<?php echo site_url('admin/planing')?>" class="btn btn-primary">Gestion des planning</a>
        </div>
        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
            <a href="<?php echo site_url('admin/payement')?>" class="btn btn-primary">Gestion des payement</a>
        </div>
        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
            <a href="<?php echo site_url('admin/pack')?>" class="btn btn-primary">Gestion des pack</a>
        </div>
        
        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
            <a href="<?php echo site_url('admin/pack_objet')?>" class="btn btn-primary">Objet de demande client</a>
        </div>
    </div>
    <div class="row">
        <div class="col pt-3">
            <a href="<?php echo site_url('admin/packarticle/statistics')?>" class="btn btn-success col">Statistiques des articles</a>
        </div>
    </div>
</div>
<style type="text/css">
.col.adminmenupackarticle {
    width: 20%;
    float: left;
}
</style>