<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="fr"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Clubproximite Identifiant</title>
<meta name="Generator" content="Serif WebPlus X6">
<meta name="viewport" content="width=320">
<style type="text/css">
body{margin:0;padding:0;}
.Normal-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:center; font-weight:400;
}
.Normal-P-P0
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:justify; font-weight:400;
}
.Corps-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:justify; font-weight:400;
}
.Corps-P-P0
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:center; font-weight:400;
}
.Corps-P-P1
{
    margin:0.0px 0.0px 12.0px 0.0px; text-align:justify; font-weight:400;
}
.Normal-C
{
    font-family:"Arial", sans-serif; font-weight:700; color:#b30000; font-size:17.0px; line-height:1.18em;
}
.Normal-C-C0
{
    font-family:"Arial", sans-serif; font-size:8.0px; line-height:1.25em;
}
.Normal-C-C1
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
.Normal-C-C2
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:12.0px; line-height:1.25em;
}
.Strong-C
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
.Normal-C-C3
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:8.0px; line-height:1.25em;
}
.Strong-C-C0
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:12.0px; line-height:1.25em;
}
.Corps-C
{
    font-family:"Arial", sans-serif; font-size:8.0px; line-height:1.25em;
}
.Corps-C-C0
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C1
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C2
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:11.0px; line-height:12.0px;
}
.Corps-C-C3
{
    font-family:"Arial", sans-serif; font-size:11.0px; line-height:1.27em;
}
.Corps-C-C4
{
    font-family:"Verdana", sans-serif; font-size:11.0px; line-height:1.18em;
}
.Corps-artistique-C
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:12.0px; line-height:1.25em;
}
</style>
<link rel="stylesheet" href="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpstyles.css" type="text/css">

<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/wpscripts2013/jquery-1.8.3.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>
<script type="text/javascript">

$(function(){
     
    $("#btn_connexion_submit").click(function(){
            
            var txtError = "";
			var txtError_js = "";
            
            
            var user_login_mobile = $("#user_login_mobile").val();
            if(user_login_mobile=="" || user_login_mobile=="Précisez votre courriel") {
                
                txtError += "- Veuillez indiquer Votre courriel <br/>"; 
				txtError_js += "- Veuillez indiquer Votre courriel \n";
                
                $("#user_login_mobile").css('border-color', 'red');
            } else {
                $("#user_login_mobile").css('border-color', '#E3E1E2');
            }
            
            var user_pass_mobile = $("#user_pass_mobile").val();
            if(user_pass_mobile=="" || user_pass_mobile=="Précisez votre mot de passe") {
                
                txtError += "- Veuillez indiquer Votre mot de passe <br/>"; 
				txtError_js += "- Veuillez indiquer Votre mot de passe \n";
                
                $("#user_pass_mobile").css('border-color', 'red');
            } else {
                $("#user_pass_mobile").css('border-color', '#E3E1E2');
            }
            
			$("#mess_connexion_mobile").html("<blink>"+txtError+"</blink>");
			
            
            if(txtError == "") {
               
                $("#frmConnexion_mobile").submit();
                
            } else {
				//alert(txtError_js);
			}
            
        });
        
        
        
        $('#user_login_mobile').focusin(function(){
            if($(this).val()=="Précisez votre courriel") {
                $(this).val("");
            }
        });
        $('#user_login_mobile').focusout(function(){
            if($(this).val()=="") {
                $(this).val("Précisez votre courriel");
            }
        });
        $('#user_pass_mobile').focusin(function(){
            if($(this).val()=="Précisez votre mot de passe") {
                $(this).val("");
            }
        });
        $('#user_pass_mobile').focusout(function(){
            if($(this).val()=="") {
                $(this).val("Précisez votre mot de passe");
            }
        });
        
    
   
});

   
</script>


</head>

<body style="background-color:#003566;height:520px;" text="#000000">
<div style="background-color:transparent;margin-left:auto;margin-right:auto;position:relative;width:320px;height:520px;">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/head_mobile_court_nv.png" alt="" style="position:absolute;left:0px;top:0px;" border="0" width="320" height="68">
<map id="map0" name="map0">
    <area shape="rect" coords="4,8,291,60" href="<?php echo site_url("front/particuliers/inscription/");?>" alt="">
</map>


<a href="<?php echo site_url("front/mobile/fonctionnement_club");?>"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/FONCTIONNEMENTCLUB.PNG" alt="" style="position:absolute; left:12px; top:95px;text-align:center; width:300px; height:63px;"></a>


<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp05d7e3b3_06.png" alt="" usemap="#map0" style="position:absolute; left:14px; top:164px;" border="0" height="74" width="294">
<div style='color: #ffffff;
    font-family: "Vladimir Script",cursive;
    font-size: 32px;
    line-height: 47px; position:absolute; left:50px; top:225px;'>
    
    <img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/je-suis-deja-inscrit.PNG" alt="" border="0"/>
    </div>

<form id="frmConnexion_mobile" name="frmConnexion_mobile" action="<?php echo site_url("auth/login"); ?>" method="post" accept-charset="utf-8"> 
    <input name="identity" id="user_login_mobile" value="Précisez votre courriel" style="position:absolute; left:17px; top:281px; width:292px;" type="text">
    <input name="password" id="user_pass_mobile" value="Précisez votre mot de passe" style="position:absolute; left:17px; top:312px; width:292px;" type="password">
    <a href="Javascript:void();" id="btn_connexion_submit">
    <img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpcd5cd45f_06.png" title="" alt="index2" usemap="#map1" style="position:absolute; left:15px; top:347px;" border="0" height="63" width="294">
    </a>
</form>

<div style="position:absolute; left:14px; top:420px; font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#FFFFFF; text-align:center; width:300px" id="mess_connexion_mobile"><blink><?php if (isset($message)) echo $message; ?></blink></div>

<div style="position:absolute; left:0px; top:466px;">
<?php 
$data["zTitle"] = 'Identification';
$this->load->view("front2013/includes/mobile_footer.php", $data);
?>
</div>

</div>

</body></html>