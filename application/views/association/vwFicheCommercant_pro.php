<?php $this->load->view("association/includes/header", $data); ?>

<style type="text/css">
#frmInscriptionProfessionnel fieldset {
	width:100%;
}
#frmInscriptionProfessionnel .inputadmin { width:400px;}
</style>
    
    <div id="divInscriptionProfessionnel" class="content" align="center" style="padding:0 100px;">
        
        <h1 style="margin:0px; padding-top:20px;">Fiche Professionnel</h1>
        <?php if(isset($objCommercant->NomSociete)) echo "<h2 style='margin:0px;'>".htmlspecialchars($objCommercant->NomSociete)."</h2>"; ?> 
        
        
        <?php $data['umpty'] = ''; $this->load->view("association/vwFicheCommercant_contact", $data); ?>  
            
            <fieldset style="margin-top:30px;">
                <div class="col-lg-12" style="font-size:20px;background-color:#000;color:#fff; height:40px; padding-top:12px; text-align:left;">Les coordonnées de l'établissement</div>
                <div class="col-lg-12" style="background:#E1E1E1; padding-top:15px; padding-bottom:15px;"> 
                <table width="100%" cellpadding="0" cellspacing="0" style="text-align:left;">
                    
                    <tr>
                        <td>
                            <label>Rubrique : </label>
                        </td>
                        <td>
                                <?php if(sizeof($colRubriques)) { ?>
                                    <?php foreach($colRubriques as $objRubrique) { ?>
                                        <?php if (isset($objAssCommercantRubrique[0]->IdRubrique) && $objAssCommercantRubrique[0]->IdRubrique == $objRubrique->IdRubrique) { echo $objRubrique->Nom; } ?>
                                    <?php } ?>
                                <?php } ?>
                        </td>
                    </tr>
                    <tr id='trReponseRub'>
                        <td>
                            <label>Sous-rubrique : </label>
                        </td>
                        <td>
                                <?php if(sizeof($colSousRubriques)) { ?>
                                    <?php foreach($colSousRubriques as $objSousRubrique) { ?>
                                        <?php if(isset($objAssCommercantSousRubrique[0]->IdSousRubrique) && $objAssCommercantSousRubrique[0]->IdSousRubrique == $objSousRubrique->IdSousRubrique) { echo $objSousRubrique->Nom; } ?>
                                    <?php } ?>
                                <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Nom de la soci&eacute;t&eacute; : </label>
                        </td>
                        <td>
                            <?php if(isset($objCommercant->NomSociete)) echo htmlspecialchars($objCommercant->NomSociete); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Siret : </label>
                        </td>
                        <td>
                            <?php if(isset($objCommercant->Siret)) echo htmlspecialchars($objCommercant->Siret); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Adresse de l'&eacute;tablissement : </label>
                        </td>
                        <td>
                            <?php if(isset($objCommercant->Adresse1)) echo htmlspecialchars($objCommercant->Adresse1); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Compl&eacute;ment d'Adresse : </label>
                        </td>
                        <td>
                            <?php if(isset($objCommercant->Adresse2)) echo htmlspecialchars($objCommercant->Adresse2); ?>
                        </td>
                    </tr>
                    
                    
                    
                    
                    
                   
                    
                      <tr>
                        <td>Code Postal *</td>
                        <td>
                            <?php if(isset($objCommercant->CodePostal)) echo htmlspecialchars($objCommercant->CodePostal); ?>
                        </td>
                      </tr>
                    
                    
                    <tr>
                        <td>Ville *</td>
                        <td>
                            <?php if (isset($objCommercant->IdVille)) echo $this->mdlville->getVilleById($objCommercant->IdVille)->Nom;?>
                        </td>
                    </tr>
                    
                    
                    
                    
                    
                   
                    <tr>
                        <td>
                            <label>T&eacute;l&eacute;phone fixe : </label>
                        </td>
                        <td>
                            <?php if(isset($objCommercant->TelFixe)) echo htmlspecialchars($objCommercant->TelFixe); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>T&eacute;l&eacute;phone mobile : </label>
                        </td>
                        <td>
                            <?php if(isset($objCommercant->TelMobile)) echo htmlspecialchars($objCommercant->TelMobile); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Site Web : </label>
                        </td>
                        <td>
                            <?php if(isset($objCommercant->SiteWeb)) echo htmlspecialchars($objCommercant->SiteWeb); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Courriel : </label>
                        </td>
                        <td>
                            <?php if(isset($objCommercant->Email)) echo htmlspecialchars($objCommercant->Email); ?>
                        </td>
                    </tr>
                    
                    
                </table>
                </div>
            </fieldset>
            <fieldset>
                <div class="col-lg-12" style="font-size:20px;background-color:#000;color:#fff;height:40px; padding-top:12px; text-align:left;">Les coordonnées du décideur</div>
                <div class="col-lg-12" style="padding-top:15px; padding-bottom:15px;">
                
                <table width="100%" cellpadding="0" cellspacing="0" style="text-align:left;">
                    <tr>
                        <td width="300">
                            <label>Civilit&eacute; : </label>
                        </td>
                        <td>
                                <?php if(isset($objCommercant->Civilite) && $objCommercant->Civilite == "0") { ?>Monsieur<?php } ?>
                                <?php if(isset($objCommercant->Civilite) && $objCommercant->Civilite == "1") { ?>Madamme<?php } ?>
                                <?php if(isset($objCommercant->Civilite) && $objCommercant->Civilite == "2") { ?>Mademoiselle<?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Nom : </label>
                        </td>
                        <td>
                            <?php if(isset($objCommercant->Nom)) echo htmlspecialchars($objCommercant->Nom); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Pr&eacute;nom : </label>
                        </td>
                        <td>
                            <?php if(isset($objCommercant->Prenom)) echo htmlspecialchars($objCommercant->Prenom); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Fonction <span class="indication">(G&eacute;rant, Directeur ...)</span>: </label>
                        </td>
                        <td>
                            <?php if(isset($objCommercant->Responsabilite)) echo htmlspecialchars($objCommercant->Responsabilite); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Email
                        </td>
                        <td>
                            <?php if(isset($objCommercant->Email_decideur)) echo htmlspecialchars($objCommercant->Email_decideur); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>T&eacute;l&eacute;phone direct <span class="indication">(fixe ou portable)</span> : </label>
                        </td>
                        <td>
                            <?php if(isset($objCommercant->TelDirect)) echo htmlspecialchars($objCommercant->TelDirect); ?>
                        </td>
                    </tr>
                    
                </table>
                 
                
                </div>
            </fieldset>
            <fieldset>
                <div class="col-lg-12" style="font-size:20px;background-color:#000;color:#fff;height:40px; padding-top:12px; text-align:left;">Parametres de l'abonnement</div>
                <div class="col-lg-12" style="background:#E1E1E1;padding-top:15px; padding-bottom:15px;">
                
                <table width="100%" cellpadding="0" cellspacing="0" style="text-align:left;">
                    
                    
                    <tr>
                        <td style="width:200px;height:50px; vertical-align: top;">
                            <label>Abonnement activ&eacute; : </label>
                        </td>
                        <td style="vertical-align: top;">
                            <?php if(isset($objCommercant->IsActif) && $objCommercant->IsActif == "1") echo 'Actif'; else echo 'Non'; ?>
                        </td>
                    </tr>
                    
                    </table>
                  
                
                <table width="100%" cellpadding="0" cellspacing="0" style="text-align:left;">
                    
                    <tr>
                        <td>
                            Date d&eacute;but
                        </td>
                        <td>
                        <?php if(isset($objAssCommercantAbonnement[0]->DateDebut)) echo convertDateWithSlashes($objAssCommercantAbonnement[0]->DateDebut); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Date fin
                        </td>
                        <td>
                        <?php if(isset($objAssCommercantAbonnement[0]->DateFin)) echo convertDateWithSlashes($objAssCommercantAbonnement[0]->DateFin); ?>
                        </td>
                    </tr>
                    
                    
                    <tr>
                        <td></td>
                        <td align="left">&nbsp;<!--<input type="button" class="btnSinscrire" value="Modifier" />--></td>
                    </tr>
                </table>
                </div>
            </fieldset>
            
            
            
                
    </div>
    
<?php $this->load->view("association/includes/footer", $data); ?>