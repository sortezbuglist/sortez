<?php $this->load->view("association/includes/header", $data); ?>

<style type="text/css">
#frmInscriptionProfessionnel fieldset {
	width:100%;
}
#frmInscriptionProfessionnel .inputadmin { width:400px;}
</style>


    <script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
			jQuery('#loading_datefin_plus_un_an').hide();
			
            jQuery(".btnSinscrire").click(function(){
                var txtError = "";
				
				var RubriqueSociete = jQuery("#RubriqueSociete").val();
                if(RubriqueSociete=="0" || RubriqueSociete=="") {
                    jQuery("#divErrorRubriqueSociete").html("Un commercant doit être assigné à une rubrique.");
                    jQuery("#divErrorRubriqueSociete").show();
                    txtError += "- Un commercant doit être assigné à une rubrique.<br/>";
                } else {
                    jQuery("#divErrorRubriqueSociete").hide();
                }
				
				var SousRubriqueSociete = jQuery("#SousRubriqueSociete").val();
                if(SousRubriqueSociete=="0" || SousRubriqueSociete=="") {
                    jQuery("#divErrorSousRubriqueSociete").html("Un commercant doit être assigné à une sous-rubrique.");
                    jQuery("#divErrorSousRubriqueSociete").show();
                    txtError += "- Un commercant doit être assigné à une sous-rubrique.<br/>";
                } else {
                    jQuery("#divErrorSousRubriqueSociete").hide();
                }
				
                var EmailSociete = jQuery("#EmailSociete").val();
                if(!isEmail(EmailSociete)) {
                    jQuery("#divErrorEmailSociete").html("Ce Courriel n'est pas valide. Veuillez saisir un email valide");
                    jQuery("#divErrorEmailSociete").show();
                    txtError += "- Ce Courriel n'est pas valide. Veuillez saisir un email valide.<br/>";
                } else {
                    jQuery("#divErrorEmailSociete").hide();
                }
                
                var LoginSociete = jQuery("#LoginSociete").val();
                if(!isEmail(LoginSociete)) {
                    txtError += "- Votre login doit &ecirc;tre un email valide.<br/>";
                    jQuery("#divErrorLoginSociete").html("Votre login doit &ecirc;tre un email valide.");
                    jQuery("#divErrorLoginSociete").show();
                } else {
                    jQuery("#divErrorLoginSociete").hide();
                }
				
				
				jQuery("#total_error_admin_fiche_comm").html(txtError);
				
                if(txtError == "") {
                    jQuery("#frmInscriptionProfessionnel").submit();
                }
            });
			
			
			jQuery("#IsActifSociete").click(function(){
							var DateDebut = jQuery("#DateDebut").val();	
							var DateFin = jQuery("#DateFin").val();	
							
							if ((DateDebut=="" || DateDebut==null) && (DateFin=="" || DateFin==null)) {
								//alert("valeur null");
								DateDebut = "<?php echo convert_Sqldate_to_Frenchdate(date("Y-m-d")); ?>";
								<?php 
								$current_date = date("Y-m-d");
								$current_value = explode('-',$current_date);
								?>
								//alert("sqdfqsf "+<?php echo $current_value[0]+1;?>);
								<?php 
								$year_datefin = $current_value[0]+1; 
								$date_fin_value = $year_datefin."-".$current_value[1]."-".$current_value[2];
								?>
								DateFin = "<?php echo convert_Sqldate_to_Frenchdate($date_fin_value);?>";
								//alert(DateDebut);
								//alert(DateFin);
								jQuery("#DateDebut").val(DateDebut);
								jQuery("#DateFin").val(DateFin);
							}
												   
				});
							
			jQuery("#btn_datefin_plus_un_an").click(function(){
							var DateDebut = jQuery("#DateDebut").val();	
							
							//alert(DateDebut);
							jQuery('#loading_datefin_plus_un_an').show();
							
							jQuery.post('<?php echo site_url("association/commercants/add_datefin_plus_un_an" ) ; ?>',
								{ DateDebut: DateDebut},
								function success(data){
									jQuery("#DateFin").val(data);
									jQuery('#loading_datefin_plus_un_an').hide();
							 });
												   
				});				
							
        })
    </script>
	<script type="text/javascript">
		
		
		
		<?php
		$user_agent_browser_mda = $_SERVER['HTTP_USER_AGENT'];
		if (strpos(strtoupper($user_agent_browser_mda), 'FIREFOX') !== false) {
		?>
		
		$(function() {
			$( "#DateDebut" ).datepicker({ dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
												dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
												monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
												dateFormat: 'dd/mm/yy',
												 autoSize: true,
												changeMonth: true,
												changeYear: true,
												yearRange: '1900:2020'});	
		$( "#DateFin" ).datepicker({ dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
												dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
												monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
												dateFormat: 'dd/mm/yy',
												 autoSize: true,
												changeMonth: true,
												changeYear: true,
												yearRange: '1900:2020'});													
		});
		<?php } ?>

		
		
		
		function effacerPhotoAccueil(_iCommercantId){
			jQuery.post('<?php echo site_url("admin/commercants/effacerPhotoCommercant" ) ; ?>',
				{ iCommercantId: _iCommercantId},
				function success(data){ 
					jQuery("#divPhotoAccueil").hide() ;
			 });
		}
		function effacerPhoto1(_iCommercantId){
			jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto1Commercant" ) ; ?>',
				{ iCommercantId: _iCommercantId},
				function success(data){
					jQuery("#divPhoto1").hide() ;
			 });
		}
		function effacerPhoto2(_iCommercantId){
			jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto2Commercant" ) ; ?>',
				{ iCommercantId: _iCommercantId},
				function success(data){
					jQuery("#divPhoto2").hide() ;
			 });
		}
		function effacerPhoto3(_iCommercantId){
			jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto3Commercant" ) ; ?>',
				{ iCommercantId: _iCommercantId},
				function success(data){
					jQuery("#divPhoto3").hide() ;
			 });
		}
		function effacerPhoto4(_iCommercantId){
			jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto4Commercant" ) ; ?>',
				{ iCommercantId: _iCommercantId},
				function success(data){
					jQuery("#divPhoto4").hide() ;
			 });
		}
		function effacerPhoto5(_iCommercantId){
			jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto5Commercant" ) ; ?>',
				{ iCommercantId: _iCommercantId},
				function success(data){
					jQuery("#divPhoto5").hide() ;
			 });
		}
		function effacerPdf(_iCommercantId){
			jQuery.post('<?php echo site_url("admin/commercants/effacerPdfCommercant" ) ; ?>',
				{ iCommercantId: _iCommercantId},
				function success(data){
					jQuery("#divPdf").hide() ;
			 });
		}
		
		function deleteFile(_IdCommercant,_FileName) {
			jQuery.ajax({
				url: gCONFIG["SITE_URL"] + 'front/professionnels/delete_files/' + _IdCommercant + '/' + _FileName,
				dataType: 'html',
				type: 'POST',
				async: true,
				success: function(data){
					window.location.reload();
				}
			});
		}
		
		
		function listeSousRubrique(){
			 
			var irubId = jQuery('#RubriqueSociete').val();     
			jQuery.get(
				'<?php echo site_url("front/professionnels/testAjax"); ?>' + '/' + irubId,
				function (zReponse)
				{
					// alert (zReponse) ;
					jQuery('#trReponseRub').html("") ; 
					jQuery('#trReponseRub').html(zReponse) ;
				   
				 
			   });
		}
		
		
		
		function CP_getDepartement() {
		   //alert(jQuery('#CodePostalSociete').val());
		   jQuery('#departementCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
			var CodePostalSociete = jQuery('#CodePostalSociete').val();
				jQuery.post(
						'<?php echo site_url("front/professionnels/departementcp"); ?>',
						{CodePostalSociete:CodePostalSociete},
						function (zReponse)
						{
							jQuery('#departementCP_container').html(zReponse);
						});
		}
		
		function CP_getVille() {
		   //alert(jQuery('#CodePostalSociete').val());
		   jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
			var CodePostalSociete = jQuery('#CodePostalSociete').val();
				jQuery.post(
						'<?php echo site_url("front/professionnels/villecp"); ?>',
						{CodePostalSociete:CodePostalSociete},
						function (zReponse)
						{
							jQuery('#villeCP_container').html(zReponse);
						});
		}
		
		function CP_getVille_D_CP() {
			//alert(jQuery('#CodePostalSociete').val());
			jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
			var CodePostalSociete = jQuery('#CodePostalSociete').val();
			var departement_id = jQuery('#departement_id').val();
				jQuery.post(
						'<?php echo site_url("front/professionnels/villecp"); ?>',
						{CodePostalSociete:CodePostalSociete, departement_id: departement_id},
						function (zReponse)
						{
							jQuery('#villeCP_container').html(zReponse);
						});
		}
	</script>
    
    
    <div id="divInscriptionProfessionnel" class="content" align="center" style="padding:0 100px;">
        
        <h1 style="margin:0px; padding-top:20px;">Fiche Professionnel</h1>
        <h2 style="margin:0px;">Validation abonnement</h2>
        <?php if(isset($objCommercant->NomSociete)) echo "<h3 style='margin:0px;'>".htmlspecialchars($objCommercant->NomSociete)."</h3>"; ?> 
        
        
        <?php $data['umpty'] = ''; $this->load->view("association/vwFicheCommercant_contact", $data); ?>  
          
         
		 <?php if (isset($_SESSION['error_fiche_com_admin'])) { ?>
         <div style="text-align:center; color:#FF0000;"><p><?php echo $_SESSION['error_fiche_com_admin']; ?></p></div>
         <?php unset($_SESSION['error_fiche_com_admin']);} ?>
                 
         <form name="frmInscriptionProfessionnel" id="frmInscriptionProfessionnel" action="<?php echo site_url("association/commercants/modifier"); ?>" method="POST" enctype="multipart/form-data">
            
            <fieldset style="margin-top:30px;">
                <div class="col-lg-12" style="font-size:20px;background-color:#000;color:#fff; height:40px; padding-top:12px; text-align:left;">Les coordonnées de l'établissement</div>
                <div class="col-lg-12" style="background:#E1E1E1; padding-top:15px; padding-bottom:15px;"> 
                <table width="100%" cellpadding="0" cellspacing="0" style="text-align:left;">
                    
                    <tr>
                        <td>
                            <label>Rubrique : </label>
                        </td>
                        <td>
                            <select class="inputadmin" name="AssCommercantRubrique[IdRubrique]" id="RubriqueSociete" onchange="javascript:listeSousRubrique();">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($colRubriques)) { ?>
                                    <?php foreach($colRubriques as $objRubrique) { ?>
                                        <option <?php if(isset($objAssCommercantRubrique[0]->IdRubrique) &&$objAssCommercantRubrique[0]->IdRubrique == $objRubrique->IdRubrique) { ?>selected="selected"<?php } ?> value="<?php echo $objRubrique->IdRubrique; ?>"><?php echo $objRubrique->Nom; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <div class="FieldError" id="divErrorRubriqueSociete"></div>
                        </td>
                    </tr>
                    <tr id='trReponseRub'>
                        <td>
                            <label>Sous-rubrique : </label>
                        </td>
                        <td>
                            <select class="inputadmin" name="AssCommercantSousRubrique[IdSousRubrique]" id="SousRubriqueSociete">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($colSousRubriques)) { ?>
                                    <?php foreach($colSousRubriques as $objSousRubrique) { ?>
                                        <option <?php if(isset($objAssCommercantSousRubrique[0]->IdSousRubrique) && $objAssCommercantSousRubrique[0]->IdSousRubrique == $objSousRubrique->IdSousRubrique) { ?>selected="selected"<?php } ?> value="<?php echo $objSousRubrique->IdSousRubrique; ?>"><?php echo $objSousRubrique->Nom; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <div class="FieldError" id="divErrorSousRubriqueSociete"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Nom de la soci&eacute;t&eacute; : </label>
                        </td>
                        <td>
                            <input class="inputadmin" type="text" name="Societe[NomSociete]" id="NomSociete" value="<?php if(isset($objCommercant->NomSociete)) echo htmlspecialchars($objCommercant->NomSociete); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Siret : </label>
                        </td>
                        <td>
                            <input class="inputadmin" type="text" name="Societe[Siret]" id="SiretSociete" value="<?php if(isset($objCommercant->Siret)) echo htmlspecialchars($objCommercant->Siret); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Adresse de l'&eacute;tablissement : </label>
                        </td>
                        <td>
                            <input class="inputadmin" type="text" name="Societe[Adresse1]" id="Adresse1Societe" value="<?php if(isset($objCommercant->Adresse1)) echo htmlspecialchars($objCommercant->Adresse1); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Compl&eacute;ment d'Adresse : </label>
                        </td>
                        <td>
                            <input class="inputadmin" type="text" name="Societe[Adresse2]" id="Adresse2Societe" value="<?php if(isset($objCommercant->Adresse2)) echo htmlspecialchars($objCommercant->Adresse2); ?>" />
                        </td>
                    </tr>
                    
                    
                    
                    
                    
                   
                    
                      <tr>
                        <td>Code Postal *</td>
                        <td>
                            <input class="inputadmin" type="text" name="Societe[CodePostal]" id="CodePostalSociete" value="<?php if(isset($objCommercant->CodePostal)) echo htmlspecialchars($objCommercant->CodePostal); ?>" onblur="javascript:CP_getDepartement();CP_getVille();"/>
                        </td>
                      </tr>
                    
                    <tr>
                        <td>Departement : </td>
                        <td>
                            <span id="departementCP_container">
                            <select name="Societe[departement_id]" id="departement_id" disabled="disabled" class="input_width inputadmin" onchange="javascript:CP_getVille_D_CP();">
                                <option value="0">-- Choisir --</option>
                                <?php if(sizeof($colDepartement)) { ?>
                                    <?php foreach($colDepartement as $objDepartement) { ?>
                                        <option value="<?php echo $objDepartement->departement_id; ?>"><?php echo $objDepartement->departement_nom; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            </span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>Ville *</td>
                        <td>
                            <span id="villeCP_container">
                            <input type="text" value="<?php if (isset($objCommercant->IdVille)) echo $this->mdlville->getVilleById($objCommercant->IdVille)->Nom;?>" name="IdVille_Nom_text" id="IdVille_Nom_text" disabled="disabled" class="input_width inputadmin"/>
                            <input type="hidden" value="<?php if(isset($objCommercant->IdVille)) echo $objCommercant->IdVille; ?>" name="Societe[IdVille]" id="VilleSociete"/>
                            </span>
                        </td>
                    </tr>
                    
                    
  
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                   
                    <tr>
                        <td>
                            <label>T&eacute;l&eacute;phone fixe : </label>
                        </td>
                        <td>
                            <input type="text" class="inputadmin" name="Societe[TelFixe]" id="TelFixeSociete" value="<?php if(isset($objCommercant->TelFixe)) echo htmlspecialchars($objCommercant->TelFixe); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>T&eacute;l&eacute;phone mobile : </label>
                        </td>
                        <td>
                            <input type="text" class="inputadmin" name="Societe[TelMobile]" id="TelMobileSociete" value="<?php if(isset($objCommercant->TelMobile)) echo htmlspecialchars($objCommercant->TelMobile); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Site Web : </label>
                        </td>
                        <td>
                            <input type="text" class="inputadmin" name="Societe[SiteWeb]" id="SiteWebSociete" value="<?php if(isset($objCommercant->SiteWeb)) echo htmlspecialchars($objCommercant->SiteWeb); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Courriel : </label>
                        </td>
                        <td>
                            <input type="text" class="inputadmin" name="Societe[Email]" id="EmailSociete" value="<?php if(isset($objCommercant->Email)) echo htmlspecialchars($objCommercant->Email); ?>" />
                            <div class="FieldError" id="divErrorEmailSociete"></div>
                        </td>
                    </tr>
                    
                    
                </table>
                </div>
            </fieldset>
            <fieldset>
                <div class="col-lg-12" style="font-size:20px;background-color:#000;color:#fff;height:40px; padding-top:12px; text-align:left;">Les coordonnées du décideur</div>
                <div class="col-lg-12" style="padding-top:15px; padding-bottom:15px;">
                
                <table width="100%" cellpadding="0" cellspacing="0" style="text-align:left;">
                    <tr>
                        <td width="300">
                            <label>Civilit&eacute; : </label>
                        </td>
                        <td>
                            <select name="Societe[Civilite]" id="CiviliteResponsableSociete" class="inputadmin">
                                <option <?php if(isset($objCommercant->Civilite) && $objCommercant->Civilite == "0") { ?>selected="selected"<?php } ?> value="0">Monsieur</option>
                                <option <?php if(isset($objCommercant->Civilite) && $objCommercant->Civilite == "1") { ?>selected="selected"<?php } ?> value="1">Madame</option>
                                <option <?php if(isset($objCommercant->Civilite) && $objCommercant->Civilite == "2") { ?>selected="selected"<?php } ?> value="2">Mademoiselle</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Nom : </label>
                        </td>
                        <td>
                            <input type="text" class="inputadmin" name="Societe[Nom]" id="NomResponsableSociete" value="<?php if(isset($objCommercant->Nom)) echo htmlspecialchars($objCommercant->Nom); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Pr&eacute;nom : </label>
                        </td>
                        <td>
                            <input type="text" class="inputadmin" name="Societe[Prenom]" id="PrenomResponsableSociete" value="<?php if(isset($objCommercant->Prenom)) echo htmlspecialchars($objCommercant->Prenom); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Fonction <span class="indication">(G&eacute;rant, Directeur ...)</span>: </label>
                        </td>
                        <td>
                            <input type="text" class="inputadmin" name="Societe[Responsabilite]" id="ResponsabiliteResponsableSociete" value="<?php if(isset($objCommercant->Responsabilite)) echo htmlspecialchars($objCommercant->Responsabilite); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Email
                        </td>
                        <td>
                            <input type="text" class="inputadmin" name="Societe[Email_decideur]" id="Email_decideur" value="<?php if(isset($objCommercant->Email_decideur)) echo htmlspecialchars($objCommercant->Email_decideur); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>T&eacute;l&eacute;phone direct <span class="indication">(fixe ou portable)</span> : </label>
                        </td>
                        <td>
                            <input type="text" class="inputadmin" name="Societe[TelDirect]" id="TelDirectResponsableSociete" value="<?php if(isset($objCommercant->TelDirect)) echo htmlspecialchars($objCommercant->TelDirect); ?>" />
                        </td>
                    </tr>
                    
                </table>
                 
                
                </div>
            </fieldset>
            <fieldset>
                <div class="col-lg-12" style="font-size:20px;background-color:#000;color:#fff;height:40px; padding-top:12px; text-align:left;">Parametres de l'abonnement</div>
                <div class="col-lg-12" style="background:#E1E1E1;padding-top:15px; padding-bottom:15px;">
                
                <table width="100%" cellpadding="0" cellspacing="0" style="text-align:left;">
                    
                    
                    <tr>
                        <td style="width:200px;height:50px; vertical-align: top;">
                            <label>Abonnement activ&eacute; : </label>
                        </td>
                        <td style="vertical-align: top;">
                            <input type="checkbox" name="Societe[IsActif]" id="IsActifSociete" value="1" <?php if(isset($objCommercant->IsActif) && $objCommercant->IsActif == "1") echo 'checked="checked"'; ?> />
                            <input type="hidden" name="Societe[IdCommercant]" id="IdCommercantSociete" value="<?php if(isset($objCommercant->IdCommercant)) echo htmlspecialchars($objCommercant->IdCommercant); ?>" />
                        </td>
                    </tr>
                    
                    </table>
                  
                <input type="hidden" name="Abonnement_ionauth_user" id="AbonnementSociete" value="3"/>
                
                <table width="100%" cellpadding="0" cellspacing="0" style="text-align:left;">
                    <!--<tr>
                        <td style="width:200px;">
                            <label>Type d'abonnement : </label>
                        </td>
                        <td height="30">
                            <select name="Abonnement_ionauth_user" id="AbonnementSociete" style=" font-weight:bold;" class="inputadmin">
                                <option value="3" <?php //if ($user_groups->id==3) echo "selected";?>>Basic</option>
                                <option value="4" <?php //if ($user_groups->id==4) echo "selected";?>>Premium</option>
                                <option value="5" <?php //if ($user_groups->id==5) echo "selected";?>>Platinium</option>
                            </select>
                        </td>
                    </tr>-->
                    
                    <tr>
                        <td>
                            Date d&eacute;but
                        </td>
                        <td>
                        <?php
						$user_agent_browser_mda = $_SERVER['HTTP_USER_AGENT'];
						if (strpos(strtoupper($user_agent_browser_mda), 'FIREFOX') !== false) {
						?>
                            <input type="text" class="inputadmin" name="AssAbonnementCommercant[DateDebut]" id="DateDebut" value="<?php if(isset($objAssCommercantAbonnement[0]->DateDebut)) echo convertDateWithSlashes($objAssCommercantAbonnement[0]->DateDebut); ?>" />
						<?php
                        } else {
                        ?>
                        <input type="date" class="inputadmin" name="AssAbonnementCommercant[DateDebut]" id="DateDebut" value="<?php if(isset($objAssCommercantAbonnement[0]->DateDebut)) echo $objAssCommercantAbonnement[0]->DateDebut; ?>" />
                        <?php
                        } 
                        ?>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            Date fin
                        </td>
                        <td>
                        <?php
						$user_agent_browser_mda = $_SERVER['HTTP_USER_AGENT'];
						if (strpos(strtoupper($user_agent_browser_mda), 'FIREFOX') !== false) {
						?>
                            <input type="text" class="inputadmin" name="AssAbonnementCommercant[DateFin]" id="DateFin" value="<?php if(isset($objAssCommercantAbonnement[0]->DateFin)) echo convertDateWithSlashes($objAssCommercantAbonnement[0]->DateFin); ?>" />
                            
                        <?php
                        } else {
                        ?>
                        	<input type="date" class="inputadmin" name="AssAbonnementCommercant[DateFin]" id="DateFin" value="<?php if(isset($objAssCommercantAbonnement[0]->DateFin)) echo $objAssCommercantAbonnement[0]->DateFin; ?>" />
                        <?php
                        } 
                        ?>
                            <input type="button" id="btn_datefin_plus_un_an" name="btn_datefin_plus_un_an" value="Ajouter une ann&eacute;e"/>
                            <img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...." id="loading_datefin_plus_un_an"/>
                        </td>
                    </tr>
                    
                    
                    <tr>
                        <td></td>
                        <td align="left">&nbsp;<!--<input type="button" class="btnSinscrire" value="Modifier" />--></td>
                    </tr>
                </table>
                </div>
            </fieldset>
            
            
            <div class="col-lg-12" style="margin:30px 0; text-align:center;"><button class="btn btn-primary" style="background:#000000;">Enregistrer</button></div>
                
              <input type="hidden" name="pdfAssocie" id="pdfAssocie" value="<?php if(isset($objCommercant->Pdf)) echo htmlspecialchars($objCommercant->Pdf); ?>" />
              <input type="hidden" name="photo1Associe" id="photo1Associe" value="<?php if(isset($objCommercant->Photo1)) echo htmlspecialchars($objCommercant->Photo1); ?>" />
              <input type="hidden" name="photo2Associe" id="photo2Associe" value="<?php if(isset($objCommercant->Photo2)) echo htmlspecialchars($objCommercant->Photo2); ?>" />
              <input type="hidden" name="photo3Associe" id="photo3Associe" value="<?php if(isset($objCommercant->Photo3)) echo htmlspecialchars($objCommercant->Photo3); ?>" />
              <input type="hidden" name="photo4Associe" id="photo4Associe" value="<?php if(isset($objCommercant->Photo4)) echo htmlspecialchars($objCommercant->Photo4); ?>" />
              <input type="hidden" name="photo5Associe" id="photo5Associe" value="<?php if(isset($objCommercant->Photo5)) echo htmlspecialchars($objCommercant->Photo5); ?>" />
              <input type="hidden" name="photoAccueilAssocie" id="photoAccueilAssocie" value="<?php if(isset($objCommercant->PhotoAccueil)) echo htmlspecialchars($objCommercant->PhotoAccueil); ?>" />
        </form>
    </div>
    
<?php $this->load->view("association/includes/footer", $data); ?>