<?php $this->load->view("association/includes/header", $data); ?>

<?php 
    function SelectOption($prmValue, $prmCurrent) {
        if ($prmValue == $prmCurrent) {
            echo "selected";
        }
    }
    
    function EchoWithHighLightWords($prmValue, $prmSearchedWords) {
        $out = $prmValue;
        for ($i = 0; $i < sizeof($prmSearchedWords); $i ++) {
            $out = word_limiter(highlight_phrase($out, $prmSearchedWords[$i], "<span style='background: #FFFF00;'>", "</span>"),10) ;
        }
        echo $out;
    }
	
?>
<script type="text/javascript">
    function OnBtnNew_click() {
        document.location = "<?php echo site_url(); ?>association/users/fiche/0";
    }
	
	function confirm_delete_user(IdRuser){
		if (confirm("Voulez-vous supprimer cet utilisateur ?")) {
           document.location="<?php echo site_url("association/users/fiche_supprimer_user/");?>/"+IdRuser;
       }
	}

</script>

<div class="content" align="center">
      
    <div class="H1-C" style="margin-bottom:30px;">Liste des Utilisateurs</div>
    <form id="frmSearch" method="POST" action="<?php echo site_url(); ?>association/users/liste/<?php echo $FilterCol ; ?>/<?php echo $FilterValue ; ?>/0">
    <center>
        Rechercher&nbsp;:&nbsp;<input type="text" name="txtSearch" id="txtSearch" value="<?php echo $SearchValue; ?>" />&nbsp;
        <input type="submit" name="btnSearch" class="CommandButton btn btn-default" value="GO" id="btnSearch" />
        <span class="Small">
            | <a href="<?php echo site_url(); ?>association/users/liste/<?php echo $FilterCol ; ?>/<?php echo $FilterValue ; ?>/0/true">Liste compl&egrave;te</a>
        </span>
    <br>
    </center>
    <p><input type="button" class="CommandButton btn btn-default" value="Nouveau ..." onclick="OnBtnNew_click(); return false;" /></p>
    <p>
        <?php echo $PaginationLinks; ?>&nbsp;
        Nombre de lignes :&nbsp;
        <select name="cmbNbLignes" onchange="document.getElementById('btnSearch').click(); return false;">
            <option value="50" <?php SelectOption($NbLignes, 50); ?>>50 par page</option>
            <option value="100" <?php SelectOption($NbLignes, 100); ?>>100 par page</option>
            <option value="200" <?php SelectOption($NbLignes, 200); ?>>200 par page</option>
        </select>
        &nbsp;&nbsp;Aller &agrave; la page :&nbsp;
        <?php 
			$base_path_system_rand = str_replace('system/', '', BASEPATH);
			//echo $base_path_system_rand;
			$_SERVER['PATH_INFO'] = $base_path_system_rand.'/admin/users';
			
            $CurrentPath = $_SERVER['PATH_INFO'];
            if (! preg_match("/\/liste\//",$CurrentPath)) {
                $CurrentPath .= "/liste/" . $FilterCol . "/" . $FilterValue . "/0";
            }
            $TabCurrentPath = explode("/", $CurrentPath);
            function GetGoToUrl($prmNumPage, $prmTabCurrentPath, $prmUriSegment) {
                $TabGoToUrl = array();
                $c = 1;
                foreach($prmTabCurrentPath as $Elt ) {
                    if ($c != $prmUriSegment + 1) {
                        $TabGoToUrl[] = $Elt;
                    } else {
                        $TabGoToUrl[] = $prmNumPage;
                    }
                    $c ++;
                }
                return site_url() . implode("/", $TabGoToUrl);
            }
        ?>
        <select id="cmbGoToPage" onchange="document.location = '' + document.getElementById('cmbGoToPage').value + ''; return false;">
            <?php $NumAffiche = 1; ?>
            <?php foreach($NumerosPages as $NumPage) { ?>
                <option value="<?php echo GetGoToUrl($NumPage, $TabCurrentPath, $UriSegment); ?>" <?php if (GetGoToUrl($NumPage, $TabCurrentPath, $UriSegment) == site_url() . $_SERVER['PATH_INFO']) { echo "selected"; }?>><?php echo $NumAffiche ?></option>
            <?php $NumAffiche ++; ?>
            <?php } ?>
        </select>
    </p>
    </form>
    <hr />
    <form id="frmSort" name="frmSort" method="POST" action="<?php echo site_url() . $_SERVER['PATH_INFO']; ?>">
        <input type="hidden" name="hdnOrder" id="hdnOrder" value="" />
    </form>
    Liste de <?php echo "<b>" . count($colUsers) . "</b> enregistrement(s) sur ". $CountAllResults . " r&eacute;sultat(s)" ?>
  <!--<table class="table_list" width="90%">-->
	<table cellpadding="1" class="tablesorter" style = "border-collapse: separate; border-spacing: 10px;
    font-family: arial;
    font-size: 8pt;
    margin: 10px 0 15px;
    text-align: left;
    width: 99%;">
        <tr class="title" >
            <td width="5%"></td>
            <td>
                <?php if (isset($Ordre)) { ?>
                    <?php if (preg_match("/Nom /", $Ordre)) { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Nom <?php if (preg_match('/ASC/', $Ordre)) { echo 'DESC'; } else { echo 'ASC'; } ?>'; document.getElementById('frmSort').submit();">Nom</a>
                        <img style="border: none;" alt="down" src="<?php echo base_url(); ?>application/resources/admin/images/<?php if (preg_match('/ASC/', $Ordre)) { echo 'up'; } else { echo 'down'; } ?>.gif" />
                    <?php } else { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Nom ASC'; document.getElementById('frmSort').submit();">Nom</a>
                    <?php } ?>
                <?php } else { ?>
                    <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Nom ASC'; document.getElementById('frmSort').submit();">Nom</a>
                <?php } ?>
            </td>
            <td>
                <?php if (isset($Ordre)) { ?>
                    <?php if (preg_match("/Prenom /", $Ordre)) { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Prenom <?php if (preg_match('/ASC/', $Ordre)) { echo 'DESC'; } else { echo 'ASC'; } ?>'; document.getElementById('frmSort').submit();">Pr&eacute;nom</a>
                        <img style="border: none;" alt="down" src="<?php echo base_url(); ?>application/resources/admin/images/<?php if (preg_match('/ASC/', $Ordre)) { echo 'up'; } else { echo 'down'; } ?>.gif" />
                    <?php } else { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Prenom ASC'; document.getElementById('frmSort').submit();">Pr&eacute;nom</a>
                    <?php } ?>
                <?php } else { ?>
                    <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Prenom ASC'; document.getElementById('frmSort').submit();">Pr&eacute;nom</a>
                <?php } ?>
            </td>
            <td>
                <?php if (isset($Ordre)) { ?>
                    <?php if (preg_match("/Email /", $Ordre)) { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Email <?php if (preg_match('/ASC/', $Ordre)) { echo 'DESC'; } else { echo 'ASC'; } ?>'; document.getElementById('frmSort').submit();">Email</a>
                        <img style="border: none;" alt="down" src="<?php echo base_url(); ?>application/resources/admin/images/<?php if (preg_match('/ASC/', $Ordre)) { echo 'up'; } else { echo 'down'; } ?>.gif" />
                    <?php } else { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Email ASC'; document.getElementById('frmSort').submit();">Email</a>
                    <?php } ?>
                <?php } else { ?>
                    <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Email ASC'; document.getElementById('frmSort').submit();">Email</a>
                <?php } ?>
            </td>
            <td>
                <?php if (isset($Ordre)) { ?>
                    <?php if (preg_match("/UserRole /", $Ordre)) { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'UserRole <?php if (preg_match('/ASC/', $Ordre)) { echo 'DESC'; } else { echo 'ASC'; } ?>'; document.getElementById('frmSort').submit();">R&ocirc;le</a>
                        <img style="border: none;" alt="down" src="<?php echo base_url(); ?>application/resources/admin/images/<?php if (preg_match('/ASC/', $Ordre)) { echo 'up'; } else { echo 'down'; } ?>.gif" />
                    <?php } else { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'UserRole ASC'; document.getElementById('frmSort').submit();">R&ocirc;le</a>
                    <?php } ?>
                <?php } else { ?>
                    <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'UserRole ASC'; document.getElementById('frmSort').submit();">R&ocirc;le</a>
                <?php } ?>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <?php 
            foreach ($colUsers as $item) { 
        ?>
        <tr class="cell" <?php if ( ($DepartCount+1) %2 == 0)  { ?> style = "background-color: #E6EEEE" <?php }?> >
            <!--Surligner le mot recherche si l'utilisateur a effectue une recherche-->
            <td><a href="javascript:void(0);"><?php echo $DepartCount + 1; ?></a></td>
            <td valign="center">
                <a href="javascript:void(0);"><?php if ($highlight == "yes") { EchoWithHighLightWords($item->Nom, $SearchedWords); } else { echo word_limiter(highlight_phrase(preg_replace("/<[^>]*>/", "",$item->Nom), $SearchValue, "<span style='background: #FFFF00;'>", "</span>"),10);  } ?></a>
            </td>
            <td valign="center">
                <?php if ($highlight == "yes") { EchoWithHighLightWords($item->Email, $SearchedWords); } else { echo word_limiter(highlight_phrase(preg_replace("/<[^>]*>/", "",$item->Prenom), $SearchValue, "<span style='background: #FFFF00;'>", "</span>"),10);  } ?>
            </td>
            <td valign="center">
                <?php if ($highlight == "yes") { EchoWithHighLightWords($item->Email, $SearchedWords); } else { echo word_limiter(highlight_phrase(preg_replace("/<[^>]*>/", "",$item->Email), $SearchValue, "<span style='background: #FFFF00;'>", "</span>"),10);  } ?>
            </td>
            <td valign="center">
                <?php if ($item->UserRole == "1") { echo "Administrateur"; } else { echo "Visiteur";  } ?>
            </td>
            <td style="text-align: center">
           		<!--<a href="<?php echo site_url(); ?>association/users/fiche/<?php echo $item->IdUser; ?>" title="Modifier"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/update_ico.png"/></a>-->
                <a href="javascript:void(0);" title="Modifier"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/update_ico.png"/></a>
            </td>
            <td style="text-align: center">
           		<!--<a href="javascript:void(0);" onclick="javascript:confirm_delete_user(<?php echo $item->IdUser; ?>);" title="Supprimer"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/delete_ico.png"/></a>-->
                <a href="javascript:void(0);" title="Supprimer"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/delete_ico.png"/></a>
            </td>
        </tr>
        <?php 
                $DepartCount++;
            } 
        ?>
    </table>
    <hr/>
    <p><?php echo $PaginationLinks; ?></p>
</div>
<?php $this->load->view("association/includes/footer", $data); ?>