
<script type="text/javascript">

$(document).ready(function() {

	$("#contact_privicarte_nom_association").focusin(function() {	  if ($(this).val()=="Votre nom *") $(this).val('');	});
	$("#contact_privicarte_nom_association").focusout(function() {	  if ($(this).val()=="") $(this).val('Votre nom *');	});
	
	$("#contact_privicarte_tel_association").focusin(function() {	  if ($(this).val()=="Votre num\351ro de t\351l\351phone *") $(this).val('');	});
	$("#contact_privicarte_tel_association").focusout(function() {	  if ($(this).val()=="") $(this).val('Votre num\351ro de t\351l\351phone *');	});
	
	$("#contact_privicarte_mail_association").focusin(function() {	  if ($(this).val()=="Votre courriel *") $(this).val('');	});
	$("#contact_privicarte_mail_association").focusout(function() {	  if ($(this).val()=="") $(this).val('Votre courriel *');	});
	
	$("#contact_privicarte_msg_association").focusin(function() {	  if ($(this).val()=="Votre message *") $(this).val('');	});
	$("#contact_privicarte_msg_association").focusout(function() {	  if ($(this).val()=="") $(this).val('Votre message *');	});
	
	$("#contact_privicarte_reset_association").click(function() {
	  $("#contact_privicarte_nom_association").val('Votre nom *');
	  $("#contact_privicarte_tel_association").val('Votre num\351ro de t\351l\351phone *');
	  $("#contact_privicarte_mail_association").val('Votre courriel *');
	  $("#contact_privicarte_msg_association").val('Votre message *');
	  $("#spanContactPrivicarteForm_association").html('* champs obligatoires');
	});
	
	$("#contact_privicarte_send_association").click(function() {
		var error = 0;
		var contact_privicarte_nom_association = $("#contact_privicarte_nom_association").val();
		if (contact_privicarte_nom_association == '' || contact_privicarte_nom_association == 'Votre nom *') error = 1;
		var contact_privicarte_tel_association = $("#contact_privicarte_tel_association").val();
		if (contact_privicarte_tel_association == '' || contact_privicarte_tel_association == 'Votre num\351ro de t\351l\351phone *') error = 1;
		var contact_privicarte_mail_association = $("#contact_privicarte_mail_association").val();
		if (contact_privicarte_mail_association == '' || contact_privicarte_mail_association == 'Votre courriel *') error = 1;
		if (!validateEmail(contact_privicarte_mail_association)) error = 2;
		var contact_privicarte_msg_association = $("#contact_privicarte_msg_association").val();
		if (contact_privicarte_msg_association == '' || contact_privicarte_msg_association == 'Votre message *') error = 1;
		$("#spanContactPrivicarteForm_association").html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
		
		if (error == 1) {
			$("#spanContactPrivicarteForm_association").html('<span style="color:#FF0000;">* Les champs sont obligatoires</span>');
		} else if (error == 2) {
			$("#spanContactPrivicarteForm_association").html('<span style="color:#FF0000;">* Adresse email invalide</span>');
			$("#contact_privicarte_mail_association").css('border-color','#ff0000'); 
		} else {
			$.post(
				"<?php echo site_url("front/professionnels/contact_partner_sendmail/");?>", 
				{
					contact_partner_nom:contact_privicarte_nom_association,
					contact_partner_tel:contact_privicarte_tel_association,
					contact_partner_mail:contact_privicarte_mail_association,
					contact_partner_msg:contact_privicarte_msg_association,
					contact_partner_mailto:"<?php if(isset($objCommercant->Email)) echo htmlspecialchars($objCommercant->Email); else echo "webmaster@privicarte.fr";?>"
				},
				function( data ) {
					$("#spanContactPrivicarteForm_association").html(data);
					if(typeof $.fancybox == 'function') {
						 setTimeout(function(){ window.parent.$.fancybox.close(); }, 4000);
					} else {
						 //setTimeout(function(){ window.parent.$.fancybox.close(); }, 4000);
					}
				});
			}
	});
	
	$("#IdContactSortezAssociation").fancybox({
		'autoScale' : false,
		'overlayOpacity'      : 0.8, // Set opacity to 0.8
		'overlayColor'        : "#000000", // Set color to Black
		'padding'         : 5,
		'width'         : 650,
		'height'        : 500,
		'transitionIn'      : 'elastic',
		'transitionOut'     : 'elastic'
	});
	
});

function validateEmail(sEmail) {
	var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if (filter.test(sEmail)) {
		return true;
	}
	else {
		return false;
	}
}
</script>

<div style="display:block; padding:15px;">
<a href="#divContactSortezAssociation" class="btn btn-success" id="IdContactSortezAssociation" title="Contacter ce membre">Contacter ce membre !</a>
</div>

<!--Contact form contet-->
<div id="divContactSortezAssociation" style="display:none; background-color:#FFFFFF; padding-top:40px;">
    <form name="formContactPrivicarteForm_ssociation" id="formContactPrivicarteForm_ssociation" action="#">
        <table width="100%" id="tableContactPartnerForm_ssociation" border="0" cellspacing="0" cellpadding="0" style="text-align:center; width:500px; height:400px;">
          <tr>
            <td><div style="font-family:arial; font-size:20px; font-weight:bold;">Envoyer un e-mail &agrave; <?php if(isset($objCommercant->NomSociete)) echo htmlspecialchars($objCommercant->NomSociete); ?></div></td>
          </tr>
          <tr>
            <td><input type="text" name="contact_privicarte_nom_association" id="contact_privicarte_nom_association" value="Votre nom *" class="form-control"/></td>
          </tr>
          <tr>
            <td><input type="text" name="contact_privicarte_tel_association" id="contact_privicarte_tel_association" value="Votre num&eacute;ro de t&eacute;l&eacute;phone *"  class="form-control"/></td>
          </tr>
          <tr>
            <td><input type="text" name="contact_privicarte_mail_association" id="contact_privicarte_mail_association" value="Votre courriel *"  class="form-control"/></td>
          </tr>
          <tr>
            <td><textarea name="contact_privicarte_msg_association" id="contact_privicarte_msg_association"  class="form-control" style="height:125px;">Votre message *</textarea></td>
          </tr>
          <tr>
            <td><span id="spanContactPrivicarteForm_association">* champs obligatoires</span></td>
          </tr>
          <tr>
            <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td style="text-align: right; padding-right: 15px;"><input type="button" class="btn btn-default" name="contact_privicarte_reset_association" id="contact_privicarte_reset_association" value="Retablir"/></td>
                <td><input type="button" class="btn btn-default" name="contact_privicarte_send_association" id="contact_privicarte_send_association" value="Envoyer"/></td>
              </tr>
            </table>
            </td>
          </tr>
        </table>
    </form>
</div>


