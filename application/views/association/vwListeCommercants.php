<?php $this->load->view("association/includes/header", $data); ?>

<?php 
    function SelectOption($prmValue, $prmCurrent) {
        if ($prmValue == $prmCurrent) {
            echo "selected";
        }
    }
    
    function EchoWithHighLightWords($prmValue, $prmSearchedWords) {
        $out = $prmValue;
        for ($i = 0; $i < sizeof($prmSearchedWords); $i ++) {
            $out = word_limiter(highlight_phrase($out, $prmSearchedWords[$i], "<span style='background: #FFFF00;'>", "</span>"),10) ;
        }
        echo $out;
    }
?>
<script type="text/javascript">
    function OnBtnNew_click() {
        document.location = "<?php echo site_url(); ?>association/professionnels/inscription";
    }
	
	function confirm_delete_user(IdRuser){
		if (confirm("Voulez-vous supprimer cet Administrateur ?")) {
           //document.location="<?php //echo site_url("admin/commercants/fiche_supprimer_user/");?>/"+IdRuser;
       }
	}

</script>

<script type="text/javascript">
	function IdAssociationMembreList_click() {
		//alert("ok");
		var divContents = jQuery("#IdAssociationMembreList_content").html();
		var printWindow = window.open('', '', 'height=768,width=800,scrollbars=yes');
		printWindow.document.write('<html><head><title>Liste membres</title>');
		printWindow.document.write('</head><body >');
		printWindow.document.write(divContents);
		printWindow.document.write('</body></html>');
		printWindow.document.close();
		printWindow.print();
	}
</script>

<div class="content" align="center">
      <div class="H1-C" style="margin-bottom:30px;">Liste des Commercants membres</div>
    <form id="frmSearch" method="POST" action="<?php echo site_url(); ?>association/commercants/liste/<?php echo $FilterCol ; ?>/<?php echo $FilterValue ; ?>/0">
    <center>
        Rechercher&nbsp;:&nbsp;<input type="text" name="txtSearch" id="txtSearch" value="<?php echo $SearchValue; ?>" />&nbsp;
        <input type="submit" name="btnSearch" class="CommandButton btn btn-default" value="GO" id="btnSearch" />
        <span class="Small">
            | <a href="<?php echo site_url(); ?>association/commercants/liste/<?php echo $FilterCol ; ?>/<?php echo $FilterValue ; ?>/0/true">Liste compl&egrave;te</a>
        </span>
    <br>
    </center>
    <p><input type="button" class="CommandButton btn btn-default" value="Nouveau ..." onclick="OnBtnNew_click(); return false;" /></p>
    <p>
        <?php echo $PaginationLinks; ?>&nbsp;
        Nombre de lignes :&nbsp;
        <select name="cmbNbLignes" onchange="document.getElementById('btnSearch').click(); return false;">
            <option value="50" <?php SelectOption($NbLignes, 50); ?>>50 par page</option>
            <option value="100" <?php SelectOption($NbLignes, 100); ?>>100 par page</option>
            <option value="200" <?php SelectOption($NbLignes, 200); ?>>200 par page</option>
        </select>
        &nbsp;&nbsp;Aller &agrave; la page :&nbsp;
        <?php 
			$base_path_system_rand = str_replace('system/', '', BASEPATH);
			//echo $base_path_system_rand;
			$_SERVER['PATH_INFO'] = $base_path_system_rand.'/admin/commercants';
			
            $CurrentPath = $_SERVER['PATH_INFO'];
            if (! preg_match("/\/liste\//",$CurrentPath)) {
                $CurrentPath .= "/liste/" . $FilterCol . "/" . $FilterValue . "/0";
            }
            $TabCurrentPath = explode("/", $CurrentPath);
            function GetGoToUrl($prmNumPage, $prmTabCurrentPath, $prmUriSegment) {
                $TabGoToUrl = array();
                $c = 1;
                foreach($prmTabCurrentPath as $Elt ) {
                    if ($c != $prmUriSegment + 1) {
                        $TabGoToUrl[] = $Elt;
                    } else {
                        $TabGoToUrl[] = $prmNumPage;
                    }
                    $c ++;
                }
                return site_url() . implode("/", $TabGoToUrl);
            }
        ?>
        <select id="cmbGoToPage" onchange="document.location = '' + document.getElementById('cmbGoToPage').value + ''; return false;">
            <?php $NumAffiche = 1; ?>
            <?php foreach($NumerosPages as $NumPage) { ?>
                <option value="<?php echo GetGoToUrl($NumPage, $TabCurrentPath, $UriSegment); ?>" <?php if (GetGoToUrl($NumPage, $TabCurrentPath, $UriSegment) == site_url() . $_SERVER['PATH_INFO']) { echo "selected"; }?>><?php echo $NumAffiche ?></option>
            <?php $NumAffiche ++; ?>
            <?php } ?>
        </select>
    </p>
    </form>
    <hr />
    <?php $data['umpty'] = ''; $this->load->view("association/vwListeCommercants_contact", $data); ?>  
    <hr />
    
    
    
    <form id="frmSort" name="frmSort" method="POST" action="<?php echo site_url() . $_SERVER['PATH_INFO']; ?>">
        <input type="hidden" name="hdnOrder" id="hdnOrder" value="" />
    </form>
    <div id="IdAssociationMembreList_content">
    
    <div class="col-xs-12" style="margin-bottom: 30px;">
    	Liste de <?php echo "<b>" . count($colUsers) . "</b> enregistrement(s) sur ". $CountAllResults . " r&eacute;sultat(s)" ?>
        <a href="#" title="Imprimer la liste" id="IdAssociationMembreList_btnPrint" style="padding-right:15px; padding-left:15px;" onclick="javascript:IdAssociationMembreList_click();"><img style="border: none;width:25px;" src="<?php echo GetImagePath("privicarte/"); ?>/print_association.png"/></a>
        <a href="javascript:void(0);" title="Exporter la liste"><img style="border: none;width:25px;" src="<?php echo GetImagePath("privicarte/"); ?>/export_association.png"/></a>
    </div>
    
  <!--<table class="table_list" width="90%">-->
	<table cellpadding="1" class="tablesorter" style = "border-collapse: separate; border-spacing: 10px;
    font-family: arial;
    font-size: 8pt;
    margin: 10px 0 15px;
    text-align: left;
    width: 99%;">
        <tr class="title" >
            <td width="5%"></td>
            <td>
                <?php if (isset($Ordre)) { ?>
                    <?php if (preg_match("/NomSociete /", $Ordre)) { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'NomSociete <?php if (preg_match('/ASC/', $Ordre)) { echo 'DESC'; } else { echo 'ASC'; } ?>'; document.getElementById('frmSort').submit();">NomSociete</a>
                        <img style="border: none;" alt="down" src="<?php echo base_url(); ?>application/resources/admin/images/<?php if (preg_match('/ASC/', $Ordre)) { echo 'up'; } else { echo 'down'; } ?>.gif" />
                    <?php } else { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'NomSociete ASC'; document.getElementById('frmSort').submit();">NomSociete</a>
                    <?php } ?>
                <?php } else { ?>
                    <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'NomSociete ASC'; document.getElementById('frmSort').submit();">NomSociete</a>
                <?php } ?>
            </td>
            <td>
                <?php if (isset($Ordre)) { ?>
                    <?php if (preg_match("/Adresse1 /", $Ordre)) { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Adresse1 <?php if (preg_match('/ASC/', $Ordre)) { echo 'DESC'; } else { echo 'ASC'; } ?>'; document.getElementById('frmSort').submit();">Adresse</a>
                        <img style="border: none;" alt="down" src="<?php echo base_url(); ?>application/resources/admin/images/<?php if (preg_match('/ASC/', $Ordre)) { echo 'up'; } else { echo 'down'; } ?>.gif" />
                    <?php } else { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Adresse1 ASC'; document.getElementById('frmSort').submit();">Adresse</a>
                    <?php } ?>
                <?php } else { ?>
                    <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Adresse1 ASC'; document.getElementById('frmSort').submit();">Adresse</a>
                <?php } ?>
            </td>
            <td>
                <?php if (isset($Ordre)) { ?>
                    <?php if (preg_match("/TelFixe /", $Ordre)) { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'TelFixe <?php if (preg_match('/ASC/', $Ordre)) { echo 'DESC'; } else { echo 'ASC'; } ?>'; document.getElementById('frmSort').submit();">Tel</a>
                        <img style="border: none;" alt="down" src="<?php echo base_url(); ?>application/resources/admin/images/<?php if (preg_match('/ASC/', $Ordre)) { echo 'up'; } else { echo 'down'; } ?>.gif" />
                    <?php } else { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'TelFixe ASC'; document.getElementById('frmSort').submit();">Tel</a>
                    <?php } ?>
                <?php } else { ?>
                    <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'TelFixe ASC'; document.getElementById('frmSort').submit();">Tel</a>
                <?php } ?>
            </td>
            <td>
                <?php if (isset($Ordre)) { ?>
                    <?php if (preg_match("/Email /", $Ordre)) { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Email <?php if (preg_match('/ASC/', $Ordre)) { echo 'DESC'; } else { echo 'ASC'; } ?>'; document.getElementById('frmSort').submit();">Email</a>
                        <img style="border: none;" alt="down" src="<?php echo base_url(); ?>application/resources/admin/images/<?php if (preg_match('/ASC/', $Ordre)) { echo 'up'; } else { echo 'down'; } ?>.gif" />
                    <?php } else { ?>
                        <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Email ASC'; document.getElementById('frmSort').submit();">Email</a>
                    <?php } ?>
                <?php } else { ?>
                    <a href="javascript: void(0);" onclick="document.getElementById('hdnOrder').value = 'Email ASC'; document.getElementById('frmSort').submit();">Email</a>
                <?php } ?>
            </td>
            <td>
                Compte
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
			<td>&nbsp;</td>
        </tr>
        <?php 
            foreach ($colUsers as $item) { 
				$user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($item->IdCommercant);
				if (isset($user_ion_auth_id)) $user_groups = $this->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
				$data["user_groups"] = $user_groups[0];
        ?>
        <tr class="cell" <?php if ( ($DepartCount+1) %2 == 0)  { ?> style = "background-color: #E6EEEE" <?php }?> >
            <!--Surligner le mot recherche si l'utilisateur a effectue une recherche-->
            <td>
            	<a href="<?php echo site_url(); ?>association/commercants/fiche/<?php echo $item->IdCommercant; ?>"><?php echo $DepartCount + 1; ?></a>
            </td>
            <td valign="center">
                <a href="<?php echo site_url(); ?>association/commercants/fiche/<?php echo $item->IdCommercant; ?>"><?php if ($highlight == "yes") { EchoWithHighLightWords($item->NomSociete, $SearchedWords); } else { echo word_limiter(highlight_phrase(preg_replace("/<[^>]*>/", "",$item->NomSociete), $SearchValue, "<span style='background: #FFFF00;'>", "</span>"),10);  } ?></a>
            </td>
            <td valign="center">
                <a href="<?php echo site_url(); ?>association/commercants/fiche/<?php echo $item->IdCommercant; ?>"><?php if ($highlight == "yes") { EchoWithHighLightWords($item->Adresse1, $SearchedWords); } else { echo word_limiter(highlight_phrase(preg_replace("/<[^>]*>/", "",$item->Adresse1), $SearchValue, "<span style='background: #FFFF00;'>", "</span>"),10);  } ?></a>
            </td>
            <td valign="center">
                <?php if ($highlight == "yes") { EchoWithHighLightWords($item->TelFixe, $SearchedWords); } else { echo word_limiter(highlight_phrase(preg_replace("/<[^>]*>/", "",$item->TelFixe), $SearchValue, "<span style='background: #FFFF00;'>", "</span>"),10);  } ?>
            </td>
            <td valign="center">
                <?php if ($highlight == "yes") { EchoWithHighLightWords($item->Email, $SearchedWords); } else { echo word_limiter(highlight_phrase(preg_replace("/<[^>]*>/", "",$item->Email), $SearchValue, "<span style='background: #FFFF00;'>", "</span>"),10);  } ?>
            </td>
            <td valign="center">
                <?php 
				if (isset($user_groups[0]) && $user_groups[0]->id=='5') echo '<span style="color: #660000; font-weight:bold;">Platinium</span>';
				else if (isset($user_groups[0]) && $user_groups[0]->id=='4' ) echo '<span style="color: #336600; font-weight:bold;">Premium</span>';
				else if (isset($user_groups[0]) && $user_groups[0]->id=='3' ) echo '<span style="color: #FFCC00; font-weight:bold;">Basique</span>';
				else echo '<span style="color: #FF0000; font-weight:bold;">Non valide</span>';
				?>
            </td>
            <td style="text-align: center">
                <?php if ($item->IsActif == "1") { ?>
                <a href="<?php echo site_url(); ?>association/commercants/fiche/<?php echo $item->IdCommercant; ?>" title="Actif"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/activated_ico.gif"/></a>
                <?php } else { ?>
                <a href="<?php echo site_url(); ?>association/commercants/fiche/<?php echo $item->IdCommercant; ?>" title="D&eacute;sactiv&eacute;"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/deactivated_ico.png"/></a>
                <?php } ?>
            </td>
            <td style="text-align: center">
                <?php if (isset($user_groups[0]) && $user_groups[0]->id=='3' ) { ?>
                <a href="<?php echo site_url(); ?>association/commercants/fiche/<?php echo $item->IdCommercant; ?>" title="Modifier"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/update_ico.png"/></a>
                <?php } else { ?>
                <a href="<?php echo site_url(); ?>association/commercants/fiche/<?php echo $item->IdCommercant; ?>" title="Modifier"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/view_ico.png"/></a>
                <?php } ?>
            </td>
            <td style="text-align: center">
           		<?php if (isset($user_groups[0]) && $user_groups[0]->id=='3' ) { ?>
                <a href="<?php echo site_url(); ?>association/commercants/supprimer/<?php echo $item->IdCommercant; ?>" title="Supprimer"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/delete_ico.png"/></a>
                <?php } ?>
            </td>
        </tr>
        <?php 
                $DepartCount++;
            } 
        ?>
    </table>
    </div>
    <hr/>
    <p><?php echo $PaginationLinks; ?></p>
</div>
<?php $this->load->view("association/includes/footer", $data); ?>