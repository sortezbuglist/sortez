<?php $this->load->view("association/includes/header", $data); ?>

<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>


	<script type="text/javascript">
	
		 
		 
		 // Use jQuery via $(...)
		 $(document).ready(function(){//debut ready fonction
			   
			   
			   //To show sousrubrique corresponding to rubrique
			   $("#RubriqueSociete").change(function(){
			   					$('#trReponseRub').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
                                var irubId = $("#RubriqueSociete").val();
                                //alert(irubId);
								$.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>association/professionnels/testAjax/"+irubId,
                                    success: function(msg){
                                        //alert(msg);
                                        var numero_reponse = msg;
                                        $("#trReponseRub").html(numero_reponse);
                                        //alert(numero_reponse);
                                    }
                                    });
                                    //alert ("test "+$("#RubriqueSociete").val());
                                                                    
                           });
			   
			   
			   $("#EmailSociete").blur(function(){
										   //alert('cool');
										   //var result_to_show = "";
										   var value_result_to_show = "0";
										   
										   var txtEmail = $("#EmailSociete").val();
										   //alert('<?php //echo site_url("association/professionnels/verifier_email"); ?>' + '/' + txtEmail);
										   //jQuery(".EmailLoading").addClass("EmailLoading_show").removeClass("EmailLoading");
										   $.post(
													'<?php echo site_url("association/professionnels/verifier_email");?>',
													{ txtEmail_var: txtEmail },
													function (zReponse)
													{
														//alert (zReponse) ;
														//var zReponse_html = '';
														if (zReponse == "1") {
															value_result_to_show = "1";
														}
														
														
														if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce mail est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtEmail_').html(result_to_show);       
															$('#txtEmail_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce mail est disponible</span>';
															$('#divErrortxtEmail_').html(result_to_show);    
															$('#txtEmail_verif').val("0");
														}
													 
												   });
											
											$.post(
													'<?php echo site_url("association/professionnels/verifier_email_ionauth");?>',
													{ txtEmail_var_ionauth: txtEmail },
													function (zReponse_ionauth)
													{
														//alert (zReponse) ;
														//var zReponse_html_ionauth = '';
														if (zReponse_ionauth == "1") {
															value_result_to_show = "1";
														} 
														
														
														
													 	if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce mail est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtEmail_').html(result_to_show);       
															$('#txtEmail_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce mail est disponible</span>';
															$('#divErrortxtEmail_').html(result_to_show);    
															$('#txtEmail_verif').val("0");
														}
														
														
												   });
												   
												   
											
											
												   
										   
										   //jQuery(".FieldError").removeClass("FieldError");
										   //jQuery(".EmailLoading").addClass("EmailLoading").removeClass("EmailLoading_show");
										   
										   });
			   
			   
			   $("#txtLogin").blur(function(){
										   //alert('cool');
										   var txtLogin = $("#txtLogin").val();
										   
										   var value_result_to_show = "0";
										   
										   //alert('<?php //echo site_url("association/professionnels/verifier_login"); ?>' + '/' + txtEmail);
										   //jQuery(".EmailLoading").addClass("EmailLoading_show").removeClass("EmailLoading");
										   $.post(
													'<?php echo site_url("association/professionnels/verifier_login");?>',
													{ txtLogin_var: txtLogin },
													function (zReponse)
													{
														//alert (zReponse) ;
														var zReponse_html = '';
														if (zReponse == "1") {
															value_result_to_show = "1";
														} 
														
														
														if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtLogin_').html(result_to_show);       
															$('#txtLogin_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
															$('#divErrortxtLogin_').html(result_to_show);    
															$('#txtLogin_verif').val("0");
														}
													 
												   });
										   
										   
										   $.post(
													'<?php echo site_url("association/professionnels/verifier_login_ionauth");?>',
													{ txtLogin_var_ionauth: txtLogin },
													function (zReponse_ionauth)
													{
														//alert (zReponse) ;
														var zReponse_html_ionauth = '';
														if (zReponse_ionauth == "1") {
															value_result_to_show = "1";
														} 
														
														
														if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtLogin_').html(result_to_show);       
															$('#txtLogin_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
															$('#divErrortxtLogin_').html(result_to_show);    
															$('#txtLogin_verif').val("0");
														}
													 
												   });
										   
										   
										   //jQuery(".FieldError").removeClass("FieldError");
										   //jQuery(".EmailLoading").addClass("EmailLoading").removeClass("EmailLoading_show");
										   
										   });
			
			   
			   //To show postal code automatically
			   $("#VilleSociete").change(function(){
								var irubId = $("#VilleSociete").val();
                                //alert(irubId);
								$.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>association/professionnels/getPostalCode/"+irubId,
                                    success: function(msg){
                                        //alert(msg);
                                        var numero_reponse = msg;
                                        $("#CodePostalSociete").val(numero_reponse);
                                        //alert(numero_reponse);
                                    }
                                    });
                                    //alert ("test "+$("#VilleSociete").val());
				});
		
			   
		
		var valabonnementht = 0;
		var valmoduleht = 0;
		
		
		
		<?php foreach ($colAbonnements as $objAbonnement) { ?>
		$("#check_abonnement_<?php echo $objAbonnement->IdAbonnement;?>").click(function(){
            if ($(this).attr('checked')) {
				$("#check_div_abonnement_<?php echo $objAbonnement->IdAbonnement;?>").html("<?php echo $objAbonnement->tarif;?> &euro;");
				calcmontantht();
            } else {
				$("#check_div_abonnement_<?php echo $objAbonnement->IdAbonnement;?>").html("");
				calcmontantht();
			}
        });
		<?php } ?>
		
		
		
		
		function calcmontantht(){
		
			var totalmontantht = 0;
			var montanttva = 0;
			var valtva = 0.20;
			var montantttc = 0;
		
			var $check_abonnement_list = new Array();
			<?php foreach ($colAbonnements as $objAbonnement) { ?>
				if ($('#check_abonnement_<?php echo $objAbonnement->IdAbonnement;?>').attr('checked'))  totalmontantht += parseInt("<?php echo $objAbonnement->tarif;?>");
			<?php } ?>
			
			//totalmontantht = check_358_premium_value + check_358_platinium_value + check_358_agenda_plus_value + check_358_web_ref1_value + check_358_web_ref_n_value + check_358_restauration_value;
			//alert(totalmontantht);
			$("#divMontantHT").html(totalmontantht+"€");
			$("#hidemontantht").val(totalmontantht+"€");
			//calcmontanttva (parseInt(totalmontantht), valtva);
			//calcmontantttc (parseInt(totalmontantht), montanttva);
			montanttva = totalmontantht * valtva;
			$("#divMontantTVA").html(_roundNumber(montanttva,2)+"€");
			$("#hidemontanttva").val(_roundNumber(montanttva,2)+"€");
			montantttc = totalmontantht + montanttva;
			$("#divMontantTTC").html(montantttc+"€");
			$("#hidemontantttc").val(montantttc+"€");
			$("#montantttcvalue_abonnement").val(montantttc);
		}
		
		
		function calcmontanttva (totalmontantht, valtva){
			montanttva = totalmontantht * valtva;
			$("#divMontantTVA").html(_roundNumber(montanttva,2)+"€");
		}
		
		//limit decimal
		function _roundNumber(num,dec) {
			
			return (parseFloat(num)).toFixed(dec);
		}
		
		function calcmontantttc (totalmontantht, montanttva){
			montantttc = totalmontantht + montanttva;
			$("#divMontantTTC").html(montantttc+"€");
			$("#montantttcvalue_abonnement").val(montantttc);
		}
		
			   
		//verify field form value 
		$("#btnSinscrire").click(function(){
										  
				var txtError = "";
				/*var EmailSociete = $("#EmailSociete").val();
				if(!isEmail(EmailSociete)) {
					$("#divErrorEmailSociete").html("Cet email n'est pas valide. Veuillez saisir un email valide");
					$("#divErrorEmailSociete").show();
					txtError += "1";
				} else {
					$("#divErrorEmailSociete").hide();
				}*/
				// :Check if a city has been selected before validating
				
				var RubriqueSociete = $('#RubriqueSociete').val();
				  if (RubriqueSociete == "") {
					txtError += "- Vous devez préciser Activité<br/>";
				  }  
				
				var SousRubriqueSociete = $('#SousRubriqueSociete').val();
				  if (SousRubriqueSociete == "0") {
					txtError += "- Vous devez préciser une sous-rubrique<br/>";
				  } 
				
				var NomSociete = $('#NomSociete').val();
				  if (NomSociete == "") {
					txtError += "- Vous devez préciser le Nom ou enseigne<br/>";
				  }
				
				var ivilleId = $('#VilleSociete').val();
				  if (ivilleId == 0) {
					txtError += "- Vous devez sélectionner une ville<br/>";
				  }
				
				var CodePostalSociete = $('#CodePostalSociete').val();
				  if (CodePostalSociete == "") {
					txtError += "- Vous devez préciser le code postal<br/>";
				  }
				
				var EmailSociete = $("#EmailSociete").val();
				if(!isEmail(EmailSociete)) {
					txtError += "- Veuillez indiquer un email valide.<br/>";
				} 
				
				var txtEmail_verif = $("#txtEmail_verif").val();
				if(txtEmail_verif==1) {
					txtError += "- Cet Email existe déjà sur notre site <br/>";
				} 
				
				
				var NomResponsableSociete = $('#NomResponsableSociete').val();
				  if (NomResponsableSociete == "") {
					txtError += "- Vous devez préciser le Nom du Decideur <br/>";
				  }
				
				var PrenomResponsableSociete = $('#PrenomResponsableSociete').val();
				  if (PrenomResponsableSociete == "") {
					txtError += "- Vous devez préciser le Prenom du Decideur <br/>";
				  }
				  
				var ResponsabiliteResponsableSociete = $('#ResponsabiliteResponsableSociete').val();
				  if (ResponsabiliteResponsableSociete == "") {
					txtError += "- Vous devez préciser la responsabilité du Decideur <br/>";
				  }
				
				var TelDirectResponsableSociete = $('#TelDirectResponsableSociete').val();
				  if (TelDirectResponsableSociete == "") {
					txtError += "- Vous devez préciser le numero de téléphone du Decideur <br/>";
				  }
				
				var Email_decideurResponsableSociete = $("#Email_decideurResponsableSociete").val();
				if(!isEmail(Email_decideurResponsableSociete)) {
					txtError += "- Veuillez indiquer un email valide pour le decideur.<br/>";
				} 
				
				
				var AbonnementSociete = $('#AbonnementSociete').val();
				  if (AbonnementSociete == "0") {
					txtError += "- Vous devez choisir votre abonnement<br/>";
				  }  
				
				var activite1Societe = $('#activite1Societe').val();
				  if (activite1Societe == "") {
					txtError += "- Vous devez décrire votre activité<br/>";
				  }
				
				var txtLogin = $("#txtLogin").val();
				if(!isEmail(txtLogin)) {
					txtError += "- Votre login doit &ecirc;tre un email valide.<br/>";
				} 
				
				
				var txtLogin_verif = $("#txtLogin_verif").val();
				if(txtLogin_verif==1) {
					txtError += "- Votre Login existe déjà sur notre site <br/>";
				}
				
				var passs = $('#txtPassword').val();
				  if (passs == "") {
					txtError += "- Vous devez spécifier un mot de passe<br/>";
				  }
				
				if($("#txtPassword").val() != $("#txtConfirmPassword").val()) {
					txtError += "- Les deux mots de passe ne sont pas identiques.<br/>";
				}
				
				var validationabonnement = $('#validationabonnement').val();
				//alert("coche "+validationabonnement);
				if ($('#validationabonnement').is(":checked")) {} else {
					txtError += "- Vous devez valider les conditions générales<br/>";
				}
				
				
				if ($('#idreferencement0').is(":checked")) {
					$("#idreferencement").val("1");
				} else {
					$("#idreferencement").val("0");
				}
				//alert ("test "+$('#idreferencement').val());
				
				//verify captcha
				var captcha = $('#captcha').val();
				  if (captcha == "") {
					txtError += "- Vous devez remplir le captcha<br/>";
				  } else {
					  $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>association/professionnels/verify_captcha/"+captcha,
                                    success: function(msg){
                                        //alert(msg);
                                        var numero_reponse = msg;
                                        //alert(numero_reponse);
										$("#divCaptchavalueverify").val(numero_reponse);
                                    }
                             });
                             
				  }
				  
				 var divCaptchavalueverify = $('#divCaptchavalueverify').val();
				  if (divCaptchavalueverify == "0") {
					txtError += "- Les Textes que vous avez entré ne sont pas valides.<br/>";
				  } 
				// end verify captcha  
				  
				
				//final verification of input error
				if(txtError == "") {
					$("#frmInscriptionProfessionnel").submit();
				} else {
					$("#divErrorFrmInscriptionProfessionnel").html(txtError);
				}
			})
		
		
		
		
		
    })
		 
		 
    </script>

<script type="text/javascript">var blankSrc = "wpscripts/blank.gif";
</script>

<script type="text/javascript">
	function btn_login_page_avantagepro() {
            //alert('qsdfgqsdf');
            var txtError = "";
            
            var user_login = $("#user_login").val();
            if(user_login=="" || user_login=="Préciser votre courriel") {
                txtError += "<br/>- Veuillez indiquer Votre login !"; 
                $("#user_login").css('border-color', 'red');
            }
            
            var user_pass = $("#user_pass").val();
            if(user_pass=="" || user_pass=="Préciser votre mot de passe") {
                txtError += "<br/>- Veuillez indiquer Votre mot de passe !";
                $("#user_pass").css('border-color', 'red');
            } 
	
            if(txtError == "") {
                $("#frmConnexion").submit();
            }
        }
	
        $(function(){    
            $("#user_login").focusin(function(){
                if($(this).val()=="Préciser votre courriel") {
                    $(this).val("");
                }
            });
            $("#user_login").focusout(function(){
                if($(this).val()=="") {
                    $(this).val("Préciser votre courriel");
                }
            });
            $("#user_pass").focusin(function(){
                if($(this).val()=="Préciser votre mot de passe") {
                    $(this).val("");
                }
            });
            $("#user_pass").focusout(function(){
                if($(this).val()=="") {
                    $(this).val("Préciser votre mot de passe");
                }
            });
        });	
		
		
		
		function CP_getDepartement() {
		   //alert(jQuery('#CodePostalSociete').val());
		   jQuery('#departementCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
			var CodePostalSociete = jQuery('#CodePostalSociete').val();
				jQuery.post(
						'<?php echo site_url("association/professionnels/departementcp"); ?>',
						{CodePostalSociete:CodePostalSociete},
						function (zReponse)
						{
							jQuery('#departementCP_container').html(zReponse);
						});
		}
		
		function CP_getVille() {
		   //alert(jQuery('#CodePostalSociete').val());
		   jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
			var CodePostalSociete = jQuery('#CodePostalSociete').val();
				jQuery.post(
						'<?php echo site_url("association/professionnels/villecp"); ?>',
						{CodePostalSociete:CodePostalSociete},
						function (zReponse)
						{
							jQuery('#villeCP_container').html(zReponse);
						});
		}
		
		function CP_getVille_D_CP() {
			//alert(jQuery('#CodePostalSociete').val());
			jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
			var CodePostalSociete = jQuery('#CodePostalSociete').val();
			var departement_id = jQuery('#departement_id').val();
				jQuery.post(
						'<?php echo site_url("association/professionnels/villecp"); ?>',
						{CodePostalSociete:CodePostalSociete, departement_id: departement_id},
						function (zReponse)
						{
							jQuery('#villeCP_container').html(zReponse);
						});
		}
				
</script>




<style type="text/css">
.Normal-C-C1 {
    color: #000054;
    font-family: "Vladimir Script",cursive;
    font-size: 48px;
    line-height: 47px;
}
.contect_all_data_pro_subscription {
	 margin-left:15px; margin-right:15px;
}
.title_sub_pro {
	color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 15px;
	padding-bottom: 5px;
    padding-top: 5px;
    font-weight: 700;
    line-height: 1.19em;
	text-align:center;
	margin-top:15px;
	margin-bottom:15px;
	background-color:#000000;
	}
.bloc_sub_pro {
	/*background-color:#3653A2;*/
	padding-bottom:0px;
	padding-top:5px;
}	
.space_sub_pro {
	height:20px;}
.table_sub_pro {
	color: #000000;
    font-family: "Arial",sans-serif;
    font-size: 13px;
    line-height: 1.23em;
	}
.table_sub_pro tr {
	height:35px;}
.table_sub_pro_abonnement td {
	border: 2px solid #000000;
	}		
.input_width {
	width:400px;
}
.td_color_1 {
	background-color:#F4F4F4;
}
.td_color_2 {
	background-color:#E5E5E5;
}
.td_color_3 {
	background-color:#B6B6B6;
}
</style>


<div style="text-align:center;">
<h1>Souscription d'un abonnement professionnel Basique</h1>
<h4>* Champs obligatoires</h4>
</div>


<div id="contect_all_data_pro_subscription" class="contect_all_data_pro_subscription">





<form name="frmInscriptionProfessionnel" id="frmInscriptionProfessionnel" action="<?php echo site_url("association/professionnels/ajouter"); ?>" method="POST" accept-charset="UTF-8" target="_self" enctype="multipart/form-data" style="margin:0px;">

<div class="bloc_sub_pro">
<div class="title_sub_pro">Activité</div>
<div class="table_sub_pro">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="202px">Préciser votre activité *</td>
    <td>
    <select name="AssCommercantRubrique[IdRubrique]" id="RubriqueSociete" tabindex="1" class="input_width form-control">
        <option value="">-- Veuillez choisir --</option>
        <?php if(sizeof($colRubriques)) { ?>
            <?php foreach($colRubriques as $objRubrique) { ?>
                <option value="<?php echo $objRubrique->IdRubrique; ?>"><?php echo $objRubrique->Nom; ?></option>
            <?php } ?>
        <?php } ?>
    </select>
    </td>
  </tr>
  <tr id='trReponseRub'  style="height:auto;">
    
  </tr>
  <tr>
    <td>Statut</td>
    <td>
    <select name="Societe[idstatut]" size="1" class="input_width form-control" tabindex="3">
        <option selected="selected" value="">Choisir&nbsp;votre&nbsp;statut</option>
        <?php if(sizeof($colStatut)) { ?>
            <?php foreach($colStatut as $objStatut) { ?>
                <option value="<?php echo $objStatut->id; ?>"><?php echo stripcslashes($objStatut->Nom); ?></option>
            <?php } ?>
        <?php } ?>
    </select>
    </td>
  </tr>
  <tr>
    <td>Autre, préciser</td>
    <td>
    <input name="Autre"  class="input_width form-control" type="text" tabindex="4">
    </td>
  </tr>
  <tr>
    <td>Nom ou enseigne *</td>
    <td>
    <input type="text" name="Societe[NomSociete]" id="NomSociete" value=""  class="input_width form-control" tabindex="5"/>
    </td>
  </tr>
  <tr>
    <td>Adresse 1</td>
    <td>
    <input type="text" name="Societe[Adresse1]" id="Adresse1Societe" value=""  class="input_width form-control" tabindex="6"/>
    </td>
  </tr>
  <tr>
    <td>Adresse 2</td>
    <td>
    <input type="text" name="Societe[Adresse2]" id="Adresse2Societe" value=""  class="input_width form-control" tabindex="7"/>
    </td>
  </tr>
    
  
  <tr>
    <td>Code Postal *</td>
    <td>
        <input type="text" name="Societe[CodePostal]" id="CodePostalSociete" value="" class="input_width form-control"  tabindex="9" onblur="javascript:CP_getDepartement();CP_getVille();"/>
    </td>
  </tr>

<tr>
    <td>Departement : </td>
    <td>
    	<span id="departementCP_container">
        <select name="Societe[departement_id]" id="departement_id" disabled="disabled" class="input_width form-control" onchange="javascript:CP_getVille_D_CP();">
            <option value="0">-- Choisir --</option>
            <?php if(sizeof($colDepartement)) { ?>
                <?php foreach($colDepartement as $objDepartement) { ?>
                    <option value="<?php echo $objDepartement->departement_id; ?>"><?php echo $objDepartement->departement_nom; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
        </span>
    </td>
</tr>

<tr>
    <td>Ville *</td>
    <td>
    	<span id="villeCP_container">
        <input type="text" value="" name="IdVille_Nom_text" id="IdVille_Nom_text" disabled="disabled" class="input_width form-control"/>
        <input type="hidden" value="" name="Societe[IdVille]" id="VilleSociete"/>
        </span>
    </td>
</tr>
  
  
  
  
  <tr>
    <td>Téléphone direct</td>
    <td>
    <input type="text" name="Societe[TelFixe]" id="TelFixeSociete" value=""  class="input_width form-control" tabindex="10"/>
    </td>
  </tr>
  <tr>
    <td>Téléphone mobile</td>
    <td><input type="text" name="Societe[TelMobile]" id="TelMobileSociete" value=""  class="input_width form-control" tabindex="11"/></td>
  </tr>
  <tr>
    <td>Email *</td>
    <td>
    <input type="text" name="Societe[Email]" id="EmailSociete" value=""  class="input_width form-control" tabindex="12"/>
    <div id="divErrortxtEmail_" style="width:152px; height:20px; overflow:hidden; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;"></div>
    <input type="hidden" name="txtEmail_verif" id="txtEmail_verif" value="0"  class="input_width form-control" tabindex="18"/>
    </td>
  </tr>
</table>
</div>
</div>
<div class="space_sub_pro">&nbsp;</div>



<div class="bloc_sub_pro">
<div class="title_sub_pro">Les coordonnées du décideur</div>
<div class="table_sub_pro">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2">
    Le soussigné déclare avoir la faculté 
d’engager en son nom sa structure (commerce, entreprise, collectivité…) 
dont les coordonnées sont précisées ci-<wbr>dessus.
    </td>
  </tr>
  <tr>
    <td width="202px"s>Civilité *</td>
    <td>
    <select name="Societe[Civilite]" id="CiviliteResponsableSociete"  class="input_width form-control" tabindex="13">
        <option value="0">Monsieur</option>
        <option value="1">Madame</option>
        <option value="2">Mademoiselle</option>
    </select>
    </td>
  </tr>
  <tr>
    <td>Nom responsable *</td>
    <td><input type="text" name="Societe[Nom]" id="NomResponsableSociete" value=""  class="input_width form-control" tabindex="14"/></td>
  </tr>
  <tr>
    <td>Prénom responsable *</td>
    <td><input type="text" name="Societe[Prenom]" id="PrenomResponsableSociete" value=""  class="input_width form-control" tabindex="15"/></td>
  </tr>
  <tr>
    <td>Fonction responsable *</td>
    <td><input type="text" name="Societe[Responsabilite]" id="ResponsabiliteResponsableSociete" value=""  class="input_width form-control" tabindex="16"/></td>
  </tr>
  <tr>
    <td>Téléphone direct *</td>
    <td><input type="text" name="Societe[TelDirect]" id="TelDirectResponsableSociete" value=""  class="input_width form-control" tabindex="17"/></td>
  </tr>
  <tr>
    <td>Email *</td>
    <td><input type="text" name="Societe[Email_decideur]" id="Email_decideurResponsableSociete" value=""  class="input_width form-control" tabindex="18"/></td>
  </tr>
</table>
</div>
</div>
<div class="space_sub_pro">&nbsp;</div>


<div class="bloc_sub_pro">
<div class="title_sub_pro">Votre identifiant et mot de passe</div>
<div class="table_sub_pro">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="202px">Identifiant *</td>
    <td>
    <input type="text" name="Societe[Login]" id="txtLogin" value=""  class="input_width form-control" tabindex="20"/>
    <div id="divErrortxtLogin_" style="width:152px; overflow:hidden; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;"></div>
    <div id="inputMontantTTC">
       <input type="hidden" name="montantttcvalue_abonnement" id="montantttcvalue_abonnement"/>
       <input type="hidden" name="txtLogin_verif" id="txtLogin_verif" value="0" />
    </div>
    </td>
  </tr>
  <tr>
    <td>Mot de passe *</td>
    <td><input type="password" name="Societe_Password" id="txtPassword" value=""  class="input_width form-control" tabindex="21"/></td>
  </tr>
  <tr>
    <td>Confirmation du mot de passe</td>
    <td><input type="password" id="txtConfirmPassword" value=""  class="input_width form-control" tabindex="22"/></td>
  </tr>
</table>
</div>
</div>
<div class="space_sub_pro">&nbsp;</div>

<?php 
$this->load->Model("Abonnement");
$obj_abonnement_gratuit = $this->Abonnement->GetWhere(" type='gratuit' ");
$obj_abonnement_premium = $this->Abonnement->GetWhere(" type='premium' ");
$obj_abonnement_platinum = $this->Abonnement->GetWhere(" type='platinum' ");

if (isset($obj_abonnement_gratuit) && $type=="basic") $value_abonnement_sub_pro = $obj_abonnement_gratuit->IdAbonnement;
else if (isset($obj_abonnement_premium) && $type=="premium") $value_abonnement_sub_pro = $obj_abonnement_premium->IdAbonnement;
else if (isset($obj_abonnement_platinum) && ($type=="platinium" || $type=="platinum")) $value_abonnement_sub_pro = $obj_abonnement_platinum->IdAbonnement;
else $value_abonnement_sub_pro = '1';
?>
<input type="hidden" name="AssAbonnementCommercant[IdAbonnement]" id="AbonnementSociete" value="<?php echo $value_abonnement_sub_pro;?>"/>




<div class="space_sub_pro">&nbsp;</div>


<div style="margin:15px; padding:15px; font-weight:bold;">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top"><input name="validationabonnement" id="validationabonnement" value="1" type="checkbox" tabindex="24"></td>
    <td>
    Je confirme* ma demande d'inscription et la validation des conditions générales.
    </td>
  </tr>
</table>
<p>(<a href="http://privicarte.fr/pages/conditions-professionnels.html" target="_blank" style="font-weight:normal;">cliquez ici pour consulter les conditions générales</a>)</p>
</div>


<div style="text-align:center;">
<?php echo $captcha['image']; ?><br />
    <input name="captcha" value="" id="captcha" type="text" tabindex="25">
    <input name="divCaptchavalueverify" id="divCaptchavalueverify" value="" type="hidden" />
</div>


<div style="text-align:center; margin-top:20px; margin-bottom:20px;">
<input id="btnSinscrire" class="btn btn-success" style="width: 227px; height: 30px;" name="envoyer" value="Je valide ma commande" type="button" tabindex="26">
</div>

<div class="FieldError" id="divErrorFrmInscriptionProfessionnel" style="height: auto; color: red; font-family: arial; font-size: 12px; text-align:center;"></div>


<!--<div style="text-align:center;">
<img src="<?php //echo GetImagePath("front/"); ?>/wpimages2013/img_sb_pro_bottom.png">
</div>-->





</form>

</div>




<?php $this->load->view("association/includes/footer", $data); ?>