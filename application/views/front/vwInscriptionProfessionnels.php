<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="fr"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="Generator" content="Serif WebPlus X5">
<meta http-equiv="X-UA-Compatible" content="IE=EmulationIE8">
<title>Inscription des professionnels</title>
<!--<script type="text/javascript">
/*function validate_form_1( form )
{
    if( form.elements['confirmationclub'].checked=="" ) { alert("confirmation référencement club obligatoire"); form.elements['confirmationclub'].focus(); return false; }
    if( ltrim(rtrim(form.elements['abonnement'].value,' '),' ')=="" ) { alert("sélectionner votre abonnement"); form.elements['abonnement'].focus(); return false; }
    return true;
}*/
</script>-->

<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.6.4.js"></script>

	<script type="text/javascript">
	
		 
		 
		 // Use jQuery via $(...)
		 $(document).ready(function(){//debut ready fonction
			   
			   
			   //To show sousrubrique corresponding to rubrique
			   $("#RubriqueSociete").change(function(){
                                var irubId = $("#RubriqueSociete").val();
                                //alert(irubId);
								$.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>front/professionnels/testAjax/"+irubId,
                                    success: function(msg){
                                        //alert(msg);
                                        var numero_reponse = msg;
                                        $("#trReponseRub").html(numero_reponse);
                                        //alert(numero_reponse);
                                    }
                                    });
                                    //alert ("test "+$("#RubriqueSociete").val());
                                                                    
                           });
			   
			   
			   $("#EmailSociete").blur(function(){
										   //alert('cool');
										   var txtEmail = $("#EmailSociete").val();
										   //alert('<?php echo site_url("front/professionnels/verifier_email"); ?>' + '/' + txtEmail);
										   //jQuery(".EmailLoading").addClass("EmailLoading_show").removeClass("EmailLoading");
										   $.post(
													'<?php echo site_url("front/professionnels/verifier_email");?>',
													{ txtEmail_var: txtEmail },
													function (zReponse)
													{
														//alert (zReponse) ;
														var zReponse_html = '';
														if (zReponse == "1") {
															zReponse_html = '<span style="color:#F00; font-weight:bold;">Ce mail est d&eacute;j&agrave; utilis&eacute;</span>';
														} else { 
															zReponse_html = '<span style="color:#3C0; font-weight:bold;">Ce mail est disponible</span>';
														} 
														
														
														if (txtEmail != "") {$('#divErrortxtEmail_').html(zReponse_html);}       
													    $('#txtEmail_verif').val(zReponse);
													 
												   });
										   
										   //jQuery(".FieldError").removeClass("FieldError");
										   //jQuery(".EmailLoading").addClass("EmailLoading").removeClass("EmailLoading_show");
										   
										   });
			   
			   
			   $("#txtLogin").blur(function(){
										   //alert('cool');
										   var txtLogin = $("#txtLogin").val();
										   //alert('<?php echo site_url("front/professionnels/verifier_login"); ?>' + '/' + txtEmail);
										   //jQuery(".EmailLoading").addClass("EmailLoading_show").removeClass("EmailLoading");
										   $.post(
													'<?php echo site_url("front/professionnels/verifier_login");?>',
													{ txtLogin_var: txtLogin },
													function (zReponse)
													{
														//alert (zReponse) ;
														var zReponse_html = '';
														if (zReponse == "1") {
															zReponse_html = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
														} else { 
															zReponse_html = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
														} 
														
														
														if (txtLogin != "") {$('#divErrortxtLogin_').html(zReponse_html);}       
													    $('#txtLogin_verif').val(zReponse);
													 
												   });
										   
										   //jQuery(".FieldError").removeClass("FieldError");
										   //jQuery(".EmailLoading").addClass("EmailLoading").removeClass("EmailLoading_show");
										   
										   });
			
			   
			   //To show postal code automatically
			   $("#VilleSociete").change(function(){
								var irubId = $("#VilleSociete").val();
                                //alert(irubId);
								$.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>front/professionnels/getPostalCode/"+irubId,
                                    success: function(msg){
                                        //alert(msg);
                                        var numero_reponse = msg;
                                        $("#CodePostalSociete").val(numero_reponse);
                                        //alert(numero_reponse);
                                    }
                                    });
                                    //alert ("test "+$("#VilleSociete").val());
				});
			   
		
		var valabonnementht = 0;
		var valmoduleht = 0;
		var totalmontantht = 0;
		var montanttva = 0;
		var valtva = 0.196;
		var montantttc = 0;
		
		function calcmontantht(valabonnementht, valmoduleht){
			totalmontantht = valabonnementht + valmoduleht;
			$("#divMontantHT").html(totalmontantht+"€");
			calcmontanttva (totalmontantht, valtva);
			calcmontantttc (totalmontantht, montanttva);
			}
		
		function calcmontanttva (totalmontantht, valtva){
			montanttva = totalmontantht * valtva;
			$("#divMontantTVA").html(_roundNumber(montanttva,2)+"€");
		}
		
		//limit decimal
		function _roundNumber(num,dec) {
			
			return (parseFloat(num)).toFixed(dec);
		}
		
		function calcmontantttc (totalmontantht, montanttva){
			montantttc = totalmontantht + montanttva;
			$("#divMontantTTC").html(montantttc+"€");
			$("#montantttcvalue_abonnement").val(montantttc);
		}
		
		//show subscription value and label
		   $("#AbonnementSociete").change(function(){
							var irubId = $("#AbonnementSociete").val();
							//alert(irubId);
							$("#divAbonnementdesc").html($(this).find("option:selected").text());
							
                                                        $.post(
                                                        '<?php echo base_url();?>front/utilisateur/get_tarif_abonnement/',
                                                            { 
                                                                IdAbonnement: irubId
                                                            },
                                                            function (zReponse)
                                                            {
                                                                valabonnementht = parseInt(zReponse);
                                                                calcmontantht(valabonnementht, valmoduleht);
								$("#divAbonnementmontant").html(zReponse+"€");
                                                            });
                                                        
			}); 
		
		//show module subscription value and label
		   /*$("#nb_module_nb_page").change(function(){
							var irubId = $("#nb_module_nb_page").val();
							//alert(irubId);
							$("#divModuledesc").html($(this).find("option:selected").text());
							if (irubId == 0) {
								valmoduleht = 0;
								calcmontantht(valabonnementht, valmoduleht);
								$("#divModulemontant").html("");
								$("#divModuledesc").html("");
							}
							if (irubId == 1) {
								valmoduleht = 400;
								calcmontantht(valabonnementht, valmoduleht);
								$("#divModulemontant").html("400€");
							}
							if (irubId == 2) {
								valmoduleht = 800;
								calcmontantht(valabonnementht, valmoduleht);
								$("#divModulemontant").html("800€");
							}
							if (irubId == 3) {
								valmoduleht = 1200;
								calcmontantht(valabonnementht, valmoduleht);
								$("#divModulemontant").html("1200€");
							}
							if (irubId == 4) {
								valmoduleht = 1600;
								calcmontantht(valabonnementht, valmoduleht);
								$("#divModulemontant").html("1600€");
							}
							if (irubId == 5) {
								valmoduleht = 2000;
								calcmontantht(valabonnementht, valmoduleht);
								$("#divModulemontant").html("2000€");
							}
							
			});*/ 
			   
			   
			   
		//verify field form value 
		$("#btnSinscrire").click(function(){
										  
				var txtError = "";
				/*var EmailSociete = $("#EmailSociete").val();
				if(!isEmail(EmailSociete)) {
					$("#divErrorEmailSociete").html("Cet email n'est pas valide. Veuillez saisir un email valide");
					$("#divErrorEmailSociete").show();
					txtError += "1";
				} else {
					$("#divErrorEmailSociete").hide();
				}*/
				// :Check if a city has been selected before validating
				
				var RubriqueSociete = $('#RubriqueSociete').val();
				  if (RubriqueSociete == "") {
					txtError += "- Vous devez préciser votre activité<br/>";
				  }  
				
				var SousRubriqueSociete = $('#SousRubriqueSociete').val();
				  if (SousRubriqueSociete == "0") {
					txtError += "- Vous devez préciser une sous-rubrique<br/>";
				  } 
				
				var NomSociete = $('#NomSociete').val();
				  if (NomSociete == "") {
					txtError += "- Vous devez préciser le Nom ou enseigne<br/>";
				  }
				
				var ivilleId = $('#VilleSociete').val();
				  if (ivilleId == 0) {
					txtError += "- Vous devez sélectionner une ville<br/>";
				  }
				
				var CodePostalSociete = $('#CodePostalSociete').val();
				  if (CodePostalSociete == "") {
					txtError += "- Vous devez préciser le code postal<br/>";
				  }
				
				var EmailSociete = $("#EmailSociete").val();
				if(!isEmail(EmailSociete)) {
					txtError += "- Veuillez indiquer un email valide.<br/>";
				} 
				
				var txtEmail_verif = $("#txtEmail_verif").val();
				if(txtEmail_verif==1) {
					txtError += "- Votre Email existe déjà sur notre site <br/>";
				} 
				
				
				var NomResponsableSociete = $('#NomResponsableSociete').val();
				  if (NomResponsableSociete == "") {
					txtError += "- Vous devez préciser le Nom du Decideur <br/>";
				  }
				
				var PrenomResponsableSociete = $('#PrenomResponsableSociete').val();
				  if (PrenomResponsableSociete == "") {
					txtError += "- Vous devez préciser le Prenom du Decideur <br/>";
				  }
				  
				var ResponsabiliteResponsableSociete = $('#ResponsabiliteResponsableSociete').val();
				  if (ResponsabiliteResponsableSociete == "") {
					txtError += "- Vous devez préciser la responsabilité du Decideur <br/>";
				  }
				
				var TelDirectResponsableSociete = $('#TelDirectResponsableSociete').val();
				  if (TelDirectResponsableSociete == "") {
					txtError += "- Vous devez préciser le numero de téléphone du Decideur <br/>";
				  }
				
				var Email_decideurResponsableSociete = $("#Email_decideurResponsableSociete").val();
				if(!isEmail(Email_decideurResponsableSociete)) {
					txtError += "- Veuillez indiquer un email valide pour le decideur.<br/>";
				} 
				
				
				var AbonnementSociete = $('#AbonnementSociete').val();
				  if (AbonnementSociete == "0") {
					txtError += "- Vous devez choisir votre abonnement<br/>";
				  }  
				
				var activite1Societe = $('#activite1Societe').val();
				  if (activite1Societe == "") {
					txtError += "- Vous devez décrire votre activité<br/>";
				  }
				
				var txtLogin = $("#txtLogin").val();
				if(!isEmail(txtLogin)) {
					txtError += "- Votre login doit &ecirc;tre un email valide.<br/>";
				} 
				
				
				var txtLogin_verif = $("#txtLogin_verif").val();
				if(txtLogin_verif==1) {
					txtError += "- Votre Login existe déjà sur notre site <br/>";
				}
				
				var passs = $('#txtPassword').val();
				  if (passs == "") {
					txtError += "- Vous devez spécifier un mot de passe<br/>";
				  }
				
				if($("#txtPassword").val() != $("#txtConfirmPassword").val()) {
					txtError += "- Les deux mots de passe ne sont pas identiques.<br/>";
				}
				
				var validationabonnement = $('#validationabonnement').val();
				//alert("coche "+validationabonnement);
				if ($('#validationabonnement').is(":checked")) {} else {
					txtError += "- Vous devez valider les conditions générales<br/>";
				}
				
				
				if ($('#idreferencement0').is(":checked")) {
					$("#idreferencement").val("1");
				} else {
					$("#idreferencement").val("0");
				}
				//alert ("test "+$('#idreferencement').val());
				
				//verify captcha
				var captcha = $('#captcha').val();
				  if (captcha == "") {
					txtError += "- Vous devez remplir le captcha<br/>";
				  } else {
					  $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>front/professionnels/verify_captcha/"+captcha,
                                    success: function(msg){
                                        //alert(msg);
                                        var numero_reponse = msg;
                                        //alert(numero_reponse);
										$("#divCaptchavalueverify").val(numero_reponse);
                                    }
                             });
                             
				  }
				  
				 var divCaptchavalueverify = $('#divCaptchavalueverify').val();
				  if (divCaptchavalueverify == "0") {
					txtError += "- Les Textes que vous avez entré ne sont pas valides.<br/>";
				  } 
				// end verify captcha  
				  
				
				//final verification of input error
				if(txtError == "") {
					$("#frmInscriptionProfessionnel").submit();
				} else {
					$("#divErrorFrmInscriptionProfessionnel").html(txtError);
				}
			})
		
		
		
		
		
    })
		 
		
    </script>

<style type="text/css">
body {margin: 0px; padding: 0px;}
.Corps-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:left; font-weight:400;
}
.Corps-C
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C0
{
    font-family:"Arial", sans-serif; font-weight:700; color:#3653a2; font-size:21.0px; line-height:1.19em;
}
.Corps-artistique-C
{
    font-family:"Rockwell", serif; font-weight:700; color:#ffffff; font-size:24.0px; line-height:1.21em;
}
.Normal-C
{
    font-family:"Arial", sans-serif; font-weight:700; color:#3653a2; font-size:21.0px; line-height:15.0px;
}
.Normal-C-C0
{
    font-family:"Arial", sans-serif; font-weight:700; color:#3653a2; font-size:13.0px; line-height:15.0px;
}
.Normal-C-C1
{
    font-family:"Arial", sans-serif; font-weight:700; color:#3653a2; font-size:19.0px; line-height:15.0px;
}
.Corps-artistique-C-C0
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
.Corps-artistique-C-C1
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C1
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:12.0px; line-height:1.25em;
}
.Normal-C-C2
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:13.0px; line-height:16.0px;
}
.Normal-C-C3
{
    font-family:"Arial", sans-serif; color:#ffffff; font-size:13.0px; line-height:16.0px;
}
.Hyperlink-C
{
    font-family:"Arial", sans-serif; color:#ffffff; text-decoration:underline; font-size:13.0px; line-height:16.0px;
}
.Normal-C-C4
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:13.0px; line-height:15.0px;
}
.Corps-C-C2
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:29.0px; line-height:1.21em;
}
.Corps-C-C3
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C4
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:15.0px; line-height:1.20em;
}
.Hyperlink-C-C0
{
    font-family:"Arial", sans-serif; font-weight:700; text-decoration:underline; font-size:12.0px; line-height:1.25em;
}
</style>
<!--<script type="text/javascript" src="<?php// echo GetImagePath("front/"); ?>/jspngfix.js"></script>-->
<link rel="stylesheet" href="inscription_fichiers/wpstyles.css" type="text/css">
<script type="text/javascript">var blankSrc = "wpscripts/blank.gif";
</script>
</head>

<body style="background-color: rgb(255, 255, 255); background-image: url(&quot;<?php echo GetImagePath("front/"); ?>/wp8da8cbec_06.png&quot;); background-repeat: repeat; background-position: center top; background-attachment: scroll; text-align: center; height: 3000px;" text="#000000">
<form name="frmInscriptionProfessionnel" id="frmInscriptionProfessionnel" action="<?php echo site_url("front/professionnels/ajouter"); ?>" method="POST" accept-charset="UTF-8" target="_self" enctype="multipart/form-data" style="margin:0px;">

<div style="background-color:#ffffff;text-align:left;margin-left:auto;margin-right:auto;position:relative;width:1024px;height:3000px;"><img src="<?php echo GetImagePath("front/"); ?>/wpc2bffdba_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 0px; top: 0px;" height="239" border="0" width="1023"><img src="<?php echo GetImagePath("front/"); ?>/wpa72875e4_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 213px; top: 2076px;" height="140" border="0" width="582">
<img src="<?php echo GetImagePath("front/"); ?>/wp335f96b6_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 222px; top: 1083px;" height="530" border="0" width="582">
<img src="<?php echo GetImagePath("front/"); ?>/wp03bf05c4_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 221px; top: 344px;" height="410" border="0" width="582">
<img src="<?php echo GetImagePath("front/"); ?>/wp28acf631_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 220px; top: 806px;" height="229" border="0" width="582">
<div style="position:absolute; left:234px; top:817px; width:575px; height:58px;">
  <div class="Wp-Corps-P">
        <span class="Corps-C">Le soussigné déclare avoir la faculté 
d’engager en son nom sa structure (commerce, entreprise, collectivité…) 
dont les coordonnées sont précisées ci-<wbr>dessus.</span></div>
</div>
<div style="position:absolute;left:230px;top:310px;width:590px;height:38px;">
    <div class="Wp-Corps-P">
        <span class="Corps-C-C0">Votre activité</span></div>
</div>
<a href="http://www.proximite-magazine.com/"><img src="<?php echo GetImagePath("front/"); ?>/wp0e0ba6c7_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 824px; top: 260px;" height="400" border="0" width="187"></a>
<map id="map0" name="map0">
    <area shape="rect" coords="1,0,188,5" href="http://www.dossiersdepresse-paca.com/" alt="">
    <area shape="rect" coords="1,4,188,406" href="http://www.dossiersdepresse-paca.com/" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wpf7bcf651_06.png" alt="" onload="OnLoadPngFix()" usemap="#map0" style="position: absolute; left: 15px; top: 260px;" height="400" border="0" width="192">
<img src="<?php echo GetImagePath("front/"); ?>/wp60012998_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 221px; top: 261px;" height="35" border="0" width="582">
<div style="position:absolute;left:439px;top:261px;width:149px;height:30px;">
    <div class="Wp-Corps-artistique-P">
        <span class="Corps-artistique-C">Inscription</span></div>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wp44060085_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 216px; top: 1917px;" height="87" border="0" width="582">
<div style="position:absolute; left:217px; top:1853px; width:575px; height:38px;">
    <div class="Wp-Corps-P">
      <span class="Corps-C-C0">Le Club Proximité (abonnement annuel gratuit)</span></div>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wp7f29e1b4_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 219px; top: 1696px;" height="120" border="0" width="582">

<div id="txt_601" style="position:absolute; left:230px; top:406px; width:149px; height:20px; overflow:hidden;">
<label for="combo_30"><span class="Corps-C">Statut</span></label>
</div>
<select name="Societe[idstatut]" size="1" style="position:absolute; left:419px; top:406px;width: 202px;" tabindex="3">
    <option selected="selected" value="">Choisir&nbsp;votre&nbsp;statut</option>
    <?php if(sizeof($colStatut)) { ?>
        <?php foreach($colStatut as $objStatut) { ?>
            <option value="<?php echo $objStatut->id; ?>"><?php echo htmlspecialchars(stripcslashes($objStatut->Nom)); ?></option>
        <?php } ?>
    <?php } ?>
</select>
<div id="txt_603" style="position:absolute; left:230px; top:351px; width:149px; height:20px; overflow:hidden;">
<span class="Corps-C">Préciser votre activité *</span>
</div>
<div id="trReponseRub_1" style="position:absolute; left:230px; top:378px; width:392px; height:20px;">
<span class="Corps-C">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr id='trReponseRub'>
                        
  </tr>
</table>
</span>
</div>
<div id="txt_604" style="position:absolute;left:230px;top:473px;width:149px;height:20px;overflow:hidden;">
<span class="Corps-C">Nom ou enseigne *</span>
</div>
<div id="txt_605" style="position:absolute;left:230px;top:510px;width:87px;height:20px;overflow:hidden;">
<span class="Corps-C">Adresse 1</span>
</div>
<div id="txt_606" style="position:absolute;left:230px;top:544px;width:87px;height:20px;overflow:hidden;">
<span class="Corps-C">Adresse 2</span>
</div>
<div id="txt_607" style="position:absolute; left:230px; top:616px; width:104px; height:20px; overflow:hidden;">
<span class="Corps-C">Code Postal *</span>
</div>
<div id="txt_608" style="position:absolute;left:230px;top:651px;width:143px;height:20px;overflow:hidden;">
<span class="Corps-C">Téléphone direct</span>
</div>
<div id="txt_609" style="position:absolute;left:230px;top:686px;width:152px;height:20px;overflow:hidden;">
<span class="Corps-C">Téléphone mobile</span>
</div>
<div id="txt_609" style="position:absolute; left:229px; top:719px; width:152px; height:20px; overflow:hidden;">
<span class="Corps-C">Email *</span>
</div>

<div id="divErrortxtEmail_" style="position:absolute; left:633px; top:719px; width:152px; height:20px; overflow:hidden; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;"></div>

<div id="txt_610" style="position:absolute; left:230px; top:582px; width:104px; height:20px; overflow:hidden;">
<label for="combo_28"><span class="Corps-C">Ville *</span></label>
</div>

<select name="Societe[Civilite]" id="CiviliteResponsableSociete" style="position:absolute; left:428px; top:857px; width: 202px;" tabindex="13">
    <option value="0">Monsieur</option>
    <option value="1">Madame</option>
    <option value="2">Mademoiselle</option>
</select>

<div id="txt_614" style="position:absolute; left:237px; top:885px; width:138px; height:20px; overflow:hidden;">
<span class="Corps-C">Nom responsable *</span>
</div>
<div id="txt_615" style="position:absolute; left:236px; top:916px; width:172px; height:20px; overflow:hidden;">
<span class="Corps-C">Prénom responsable *</span>
</div>
<div id="txt_616" style="position:absolute; left:235px; top:946px; width:180px; height:20px; overflow:hidden;">
<span class="Corps-C">Fonction responsable *</span>
</div>
<div id="txt_617" style="position:absolute; left:236px; top:976px; width:143px; height:20px; overflow:hidden;">
<span class="Corps-C">Téléphone direct *</span>
</div>
<div id="txt_617" style="position:absolute; left:236px; top:1007px; width:143px; height:20px; overflow:hidden;">
<span class="Corps-C">Email *</span>
</div>
<div id="txt_618" style="position:absolute; left:236px; top:857px; width:138px; height:20px; overflow:hidden;">
<span class="Corps-C">Civilité *</span>
</div>
<div id="txt_619" style="position:absolute; left:235px; top:1100px; width:159px; height:39px; overflow:hidden;">
<span class="Corps-C">Le Choix de votre abonnement *</span>
</div>
<div id="txt_620" style="position:absolute; left:233px; top:1706px; width:93px; height:20px; overflow:hidden;">
<span class="Corps-C">Identifiant *</span></p>
</div>

<div id="divErrortxtLogin_" style="position:absolute; left:588px; top:1706px; width:152px; height:20px; overflow:hidden; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;"></div>

<div id="txt_621" style="position:absolute; left:233px; top:1734px; width:116px; height:20px; overflow:hidden;">
<span class="Corps-C">Mot de passe *</span></p>
</div>
<div id="txt_622" style="position:absolute; left:234px; top:1768px; width:229px; height:39px; overflow:hidden;">
<p class="Corps-P"><label for="check_2"><span class="Corps-C">Confirmation du</span></label></p>
<p class="Corps-P"><label for="check_2"><span class="Corps-C">Mot de passe *</span></label></p>
</div>
<input name="idreferencement0" id="idreferencement0" value="1" style="position: absolute; left: 225px; top: 1949px;" type="checkbox" tabindex="23">
<input type="hidden" name="Societe[idreferencement]" id="idreferencement" value=""/>
<div style="position:absolute; left:229px; top:1053px; width:375px; height:38px;">
    <div class="Wp-Corps-P">
        <span class="Corps-C-C0">Votre abonnement</span></div>
</div>


<div id="txt_631" style="position:absolute; left:415px; top:1101px; width:346px; height:189px; overflow:hidden;">
<span class="Corps-C">
<table width="100%" border="0" cellspacing="3" cellpadding="3">
<?php if(sizeof($colAbonnements)) { ?>
<?php foreach($colAbonnements as $objAbonnements) { ?>
  <tr>
    <td><?php echo $objAbonnements->Nom;?><br/><?php echo $objAbonnements->Description;?></td>
    <td width="75">= <?php echo $objAbonnements->tarif;?> &euro;</td>
  </tr>
<?php } ?>  
<?php } ?>  
</table>
</span></p>
</div>

<select name="Societe[IdVille]" id="VilleSociete" style="position: absolute; left: 419px; top: 580px; width: 202px;" tabindex="8">
  <option value="0">-- Veuillez choisir --</option>
    <?php if(sizeof($colVilles)) { ?>
        <?php foreach($colVilles as $objVille) { ?>
            <option value="<?php echo $objVille->IdVille; ?>"><?php echo htmlspecialchars(stripcslashes($objVille->Nom)); ?></option>
        <?php } ?>
    <?php } ?>
</select>

<div id="txt_645" style="position:absolute;left:230px;top:438px;width:149px;height:20px;overflow:hidden;">
<label for="edit_42"><span class="Corps-C">Autre, préciser</span></label>
</div>

<input type="text" name="Societe[CodePostal]" id="CodePostalSociete" value="" style="position: absolute; left: 419px; top: 614px; width: 202px;" tabindex="9"/>

<input type="text" name="Societe[Adresse1]" id="Adresse1Societe" value="" style="position: absolute; left: 419px; top: 507px; width: 202px;" tabindex="6"/>

<input type="text" name="Societe[Adresse2]" id="Adresse2Societe" value="" style="position: absolute; left: 419px; top: 542px; width: 202px;" tabindex="7"/>

<input type="text" name="Societe[NomSociete]" id="NomSociete" value="" style="position: absolute; left: 419px; top: 472px; width: 202px;" tabindex="5"/>

<input name="Autre" style="position: absolute; left: 419px; top: 437px; width: 202px;" type="text" tabindex="4">

<input type="text" name="Societe[TelFixe]" id="TelFixeSociete" value="" style="position: absolute; left: 419px; top: 647px; width: 202px;" tabindex="10"/>

<input type="text" name="Societe[TelMobile]" id="TelMobileSociete" value="" style="position: absolute; left: 419px; top: 682px; width: 202px;" tabindex="11"/>

<input type="text" name="Societe[Email]" id="EmailSociete" value="" style="position: absolute; left: 419px; top: 717px; width: 202px;" tabindex="12"/>

<!--<input name="Activité" style="position: absolute; left: 419px; top: 367px; width: 202px;" type="text">-->
<select name="AssCommercantRubrique[IdRubrique]" onChange="javascript:listeSousRubrique();" id="RubriqueSociete" tabindex="1" style="position: absolute; left: 419px; top: 350px; width: 202px;">
    <option value="">-- Veuillez choisir --</option>
    <?php if(sizeof($colRubriques)) { ?>
        <?php foreach($colRubriques as $objRubrique) { ?>
            <option value="<?php echo $objRubrique->IdRubrique; ?>"><?php echo htmlentities($objRubrique->Nom); ?></option>
        <?php } ?>
    <?php } ?>
</select>


<input type="text" name="Societe[Nom]" id="NomResponsableSociete" value="" style="position: absolute; left: 427px; top: 885px; width: 202px;" tabindex="14"/>

<input type="text" name="Societe[TelDirect]" id="TelDirectResponsableSociete" value="" style="position: absolute; left: 426px; top: 975px; width: 202px;" tabindex="17"/>

<input type="text" name="Societe[Email_decideur]" id="Email_decideurResponsableSociete" value="" style="position: absolute; left: 426px; top: 1005px; width: 202px;" tabindex="18"/>
<input type="hidden" name="txtEmail_verif" id="txtEmail_verif" value="0" style="position: absolute; left: 630px; top: 746px; width: 202px;" tabindex="18"/>

<input type="text" name="Societe[Responsabilite]" id="ResponsabiliteResponsableSociete" value="" style="position: absolute; left: 426px; top: 944px; width: 202px;" tabindex="16"/>

<input type="text" name="Societe[Prenom]" id="PrenomResponsableSociete" value="" style="position: absolute; left: 426px; top: 914px; width: 202px;" tabindex="15"/>

<input type="text" name="Societe[Login]" id="txtLogin" value="" style="position: absolute; left: 372px; top: 1705px; width: 202px;" tabindex="20"/>
<input type="hidden" name="txtLogin_verif" id="txtLogin_verif" value="0" style="position: absolute; left: 613px; top: 1730px; width: 202px;" />

<input type="password" name="Societe_Password" id="txtPassword" value="" style="position: absolute; left: 372px; top: 1734px; width: 202px;" tabindex="21"/>

<input type="password" id="txtConfirmPassword" value="" style="position: absolute; left: 374px; top: 1769px; width: 202px;" tabindex="22"/>


<select name="AssAbonnementCommercant[IdAbonnement]" id="AbonnementSociete" style="position:absolute; left:423px; top:1302px;" tabindex="19">
    <option value="0" selected="selected">-- Veuillez choisir&nbsp;votre&nbsp;abonnement --</option>
    <?php if(sizeof($colAbonnements)) { ?>
        <?php foreach($colAbonnements as $objAbonnement) { ?>
            <option value="<?php echo $objAbonnement->IdAbonnement; ?>" title="<?php echo htmlentities($objAbonnement->Description); ?>"><?php echo htmlentities($objAbonnement->Nom); ?></option>
        <?php } ?>
    <?php } ?>
</select>
<div style="position:absolute; left:217px; top:2043px; width:564px; height:22px;">
  <div class="Wp-Normal-P">
        <span class="Normal-C">L'ouverture de l’abonnement</span><span class="Normal-C-C0"></span></div>
</div>
<!--<div id="txt_687" style="position:absolute;left:422px;top:1273px;width:181px;height:42px;overflow:hidden;">
<p class="Corps-P"><span class="Corps-C">Abonnement par module de 5 pages</span></p>
</div>-->
<!--<div id="txt_688" style="position:absolute;left:622px;top:1283px;width:174px;height:19px;overflow:hidden;">
<label for="combo_32"><span class="Corps-C">= 400€ ht et 478.40€ TTC</span></label></p>
</div>-->
<!--<select name="nb_module_nb_page" id="nb_module_nb_page" size="1" style="position:absolute; left:422px; top:1333px;">
    <option selected="selected" value="0">Veuillez&nbsp;choisir&nbsp;le&nbsp;nombre&nbsp;de&nbsp;module</option>
    <option value="1">1&nbsp;module&nbsp;de&nbsp;5&nbsp;pages</option>
    <option value="2">2&nbsp;modules&nbsp;de&nbsp;5&nbsp;pages</option>
    <option value="3">3&nbsp;modules&nbsp;de&nbsp;5&nbsp;pages</option>
    <option value="4">4&nbsp;modules&nbsp;de&nbsp;5&nbsp;pages</option>
    <option value="5">5&nbsp;modules&nbsp;de&nbsp;5&nbsp;pages</option>
</select>-->
<img src="<?php echo GetImagePath("front/"); ?>/wp9e973d07_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 422px; top: 1383px;" height="199" border="0" width="369">
<div id="txt_698" style="position:absolute;left:230px;top:1385px;width:147px;height:22px;overflow:hidden;">
<span class="Corps-C">Votre commande :</span></p>
</div>
<div style="position:absolute;left:526px;top:1386px;width:83px;height:15px;">
  <div class="Wp-Corps-artistique-P">
      <span class="Corps-artistique-C-C0">Description</span></div>
</div>

<div id="divAbonnementdesc" style="position:absolute; left:430px; top:1409px; width:248px; height:34px;">
   
</div>
<div id="divAbonnementmontant" style="position:absolute; left:691px; top:1410px; width:82px; height:34px;">
   
</div>
<div id="divModuledesc" style="position:absolute; left:431px; top:1446px; width:248px; height:31px;">
   
</div>
<div id="divModulemontant" style="position:absolute; left:689px; top:1448px; width:84px; height:31px;">
   
</div>
<div style="position:absolute;left:704px;top:1389px;width:76px;height:15px;">
    <div class="Wp-Corps-artistique-P">
        <span class="Corps-artistique-C-C0">Montant ht</span></div>
</div>
<div id="divMontantHT" style="position:absolute; left:690px; top:1483px; width:84px; height:31px;">
   
</div>
<div id="divMontantTVA" style="position:absolute; left:691px; top:1516px; width:84px; height:31px;">
   
</div>
<div id="divMontantTTC" style="position:absolute; left:692px; top:1551px; width:84px; height:31px;">
   
</div>
<div id="inputMontantTTC" style="position:absolute; left:619px; top:1716px; width:84px; height:31px;">
   <input type="hidden" name="montantttcvalue_abonnement" id="montantttcvalue_abonnement"/>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wp37e7f62c_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 422px; top: 1406px;" height="3" border="0" width="369">
<img src="<?php echo GetImagePath("front/"); ?>/wpfc576a5e_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 681px; top: 1384px;" height="204" border="0" width="4">
<img src="<?php echo GetImagePath("front/"); ?>/wp383251bd_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 422px; top: 1480px;" height="4" border="0" width="368">
<img src="<?php echo GetImagePath("front/"); ?>/wp704a291a_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 422px; top: 1442px;" height="3" border="0" width="368">
<div style="position:absolute;left:430px;top:1492px;width:102px;height:15px;">
    <div class="Wp-Corps-artistique-P">
        <span class="Corps-artistique-C-C0">Montant total ht</span></div>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpfdf97252_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 422px; top: 1512px;" height="2" border="0" width="368">
<img src="<?php echo GetImagePath("front/"); ?>/wp704a291a_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 422px; top: 1543px;" height="3" border="0" width="368">
<div style="position:absolute;left:431px;top:1520px;width:86px;height:15px;">
    <div class="Wp-Corps-artistique-P">
        <span class="Corps-artistique-C-C0">TVA 19,60%</span></div>
</div>
<div style="position:absolute;left:430px;top:1553px;width:180px;height:15px;">
    <div class="Wp-Corps-artistique-P">
        <span class="Corps-artistique-C-C0">Montant total TTC net à régler</span></div>
</div>
<!--<div id="txt_689" style="position:absolute;left:230px;top:1278px;width:154px;height:22px;overflow:hidden;">
  <p class="Corps-P"><span class="Corps-C"> Tags Webmobile</span></p>
<p class="Corps-P"><span class="Corps-C">&nbsp;</span></p>
</div>-->
<div style="position:absolute; left:230px; top:773px; width:324px; height:38px;">
  <div class="Wp-Corps-P"> <span class="Corps-C-C0">Les coordonnées du décideur</span></div>
</div>
<map id="map1" name="map1">
  <area shape="poly" coords="22,26,18,26,18,19,14,19,14,26,10,26,10,17,16,10,22,17" href="http://www.creezvotresiteweb.fr/" alt="">
    <area shape="poly" coords="24,17,16,9,8,17,7,16,10,13,10,8,12,8,12,11,16,7,25,16" href="http://www.creezvotresiteweb.fr/" alt="">
    <area shape="poly" coords="31,31,32,29,32,2,31,1,1,1,1,31,2,32,31,32" href="http://www.creezvotresiteweb.fr/" alt="">
    <area shape="poly" coords="22,17,16,10,10,17,10,26,14,26,14,19,18,19,18,26,22,26" href="http://www.creezvotresiteweb.fr/" alt="">
    <area shape="poly" coords="25,16,16,7,12,11,12,13,10,13,7,16,8,17,16,9,24,17" href="http://www.creezvotresiteweb.fr/" alt="">
    <area shape="rect" coords="10,8,13,14" href="http://www.creezvotresiteweb.fr/" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wp204bb0c4_06.png" alt="" onload="OnLoadPngFix()" usemap="#map1" style="position: absolute; left: 930px; top: 157px;" height="32" border="0" width="32">
<map id="map2" name="map2">
    <area shape="poly" coords="15,24,5,16,15,9,15,13,26,13,26,20,15,20" href="javascript:history.back()" alt="">
    <area shape="poly" coords="31,31,32,29,32,2,31,1,1,1,1,31,2,32,31,32" href="javascript:history.back()" alt="">
    <area shape="poly" coords="15,20,26,20,26,13,15,13,15,9,5,16,15,24" href="javascript:history.back()" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wpb9e0da9a_06.png" alt="" onload="OnLoadPngFix()" usemap="#map2" style="position: absolute; left: 967px; top: 157px;" height="32" border="0" width="32">
<div style="position:absolute; left:257px; top:1927px; width:541px; height:71px;">
    <div class="Wp-Normal-P">
        <span class="Normal-C-C2">J’autorise le référencement de mes 
données au Club Proximité &nbsp;(site de référencement gratuit pour tous
 les abonnés à notre site «&nbsp;creezvotresiteweb.fr&nbsp;» et 
j’accepte les conditions générales d’utilisation du Club Proximité.<br></span>
        <span class="Normal-C-C3"> </span><span class="Hyperlink-C">(cliquez ici pour consulter les conditions générales)</span><span class="Normal-C-C3"> </span></div>
</div>
<div id="txt_644" style="position:absolute; left:217px; top:2083px; width:565px; height:124px; overflow:hidden;">
<span class="Normal-C-C2">Pour les entreprises, commerçants : après réception de leur chèque par courrier,
    nous ouvrirons leur compte, vous serez avertis par courriel de son ouverture. Une
    facture sera adressée dès réception du règlement.</span><br/>
<span class="Normal-C-C2"><br>Pour les collectivités : après réception ou confirmation de leur commande par courrier,
    télécopie ou courriel, nous ouvrirons leur compte, elles seront avertis par courriel
    de l’ouverture de leurs droits.<br>&nbsp;</span>
<p class="Wp-Normal-P"><span class="Normal-C-C4">&nbsp;</span></p>
<span class="Corps-C-C2">&nbsp;</span></p>
</div>
<div style="position:absolute; left:220px; top:1641px; width:373px; height:38px;">
  <div class="Wp-Corps-P">
        <span class="Corps-C-C0">Votre identifiant et mot de passe</span></div>
</div>
<div id="txt_641" style="position:absolute; left:285px; top:2247px; width:545px; height:40px; overflow:hidden;">
<span class="Corps-C-C3">Je confirme ma demande d'inscription et la validation des conditions générales</span><span class="Corps-C-C4"><br></span><span class="Hyperlink-C-C0"><a href="http://www.creezvotresiteweb.fr/conditions.html" style="text-decoration: underline;">(cliquez
    ici pour consulter les conditions générales)</a></span></p>
</div>
<div style="position:absolute; left:444px; top:2310px; width:190px; height:69px;">
    <!--<img src="<?php echo GetImagePath("front/"); ?>/img_verify.png"><br>
    <a title="Ecouter un CAPTCHA audio" href="http://www.serifwebresources.com/util/audio/audio.php?lang="><img alt="Ecouter un CAPTCHA audio" style="vertical-align: middle;" src="<?php echo GetImagePath("front/"); ?>/audio-desc.png" border="0"></a>-->
    <p></p>
    <?php echo $captcha['image']; ?><br />
    <input name="captcha" value="" id="captcha" type="text" tabindex="25">
    <input name="divCaptchavalueverify" id="divCaptchavalueverify" value="" type="hidden" />
    </div>
<input name="validationabonnement" id="validationabonnement" value="1" style="position: absolute; left: 246px; top: 2259px;" type="checkbox" tabindex="24">
<div style="position:absolute; left:514px; top:2314px; width:118px; height:15px;">
    <div class="Wp-Corps-artistique-P">
        <span class="Corps-artistique-C-C1">Saisie obligatoire</span></div>
</div>
<input id="btnSinscrire" style="position: absolute; left: 430px; top: 2439px; width: 227px; height: 30px;" name="envoyer" value="Validation" type="button" tabindex="26">
<!--<script type="text/javascript" src="<?php// echo GetImagePath("front/"); ?>/jsValidation.js"></script>-->

<div class="FieldError" id="divErrorFrmInscriptionProfessionnel" style="position: absolute; left: 446px; top: 2495px; width: 527px; height: auto; color: red; font-family: arial; font-size: 12px;"></div>

</div>
</form>

<img src="<?php echo GetImagePath("front/"); ?>/wpd1658a53_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 330px; top: 2692px;" border="0" height="308" width="259">
<img src="<?php echo GetImagePath("front/"); ?>/wp29327fd8_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 653px; top: 2785px;" border="0" height="51" width="50">
<img src="<?php echo GetImagePath("front/"); ?>/wpb21ff241_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 655px; top: 2855px;" border="0" height="47" width="50">
<img src="<?php echo GetImagePath("front/"); ?>/wp3ab2542d_06.png" title="" alt="06 72 05 59 35" onload="OnLoadPngFix()" style="position: absolute; left: 722px; top: 2797px;" border="0" height="21" width="117">
<img src="<?php echo GetImagePath("front/"); ?>/wpc041b622_06.png" title="" alt="04 93 73 84 51" onload="OnLoadPngFix()" style="position: absolute; left: 722px; top: 2863px;" border="0" height="21" width="117">


<?php $this->load->view("front/includes/vwFooter_nv"); ?>