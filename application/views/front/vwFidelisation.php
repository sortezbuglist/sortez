<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="fr"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="Generator" content="Serif WebPlus X5">
<meta http-equiv="X-UA-Compatible" content="IE=EmulationIE8">
<?php $data["zTitle"] = 'Les offres de fidélisation du Club :' ?>
<title>Les offres de fidélisation du Club :</title>
<style type="text/css">
body {margin: 0px; padding: 0px;}
.Normal-P
{
    margin:0.0px 0.0px 13.0px 0.0px; text-align:left; font-weight:400;
}
.Normal-P-P0
{
    margin:7.0px 0.0px 7.0px 0.0px; text-align:left; font-weight:400;
}
.Normal-P-P1
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:justify; font-weight:400;
}
.Corps-P
{
    margin:0.0px 0.0px 12.0px 0.0px; text-align:justify; font-weight:400;
}
.Corps-P-P0
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:left; font-weight:400;
}
.Normal-C
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
.Normal-C-C0
{
    font-family:"Verdana", sans-serif; font-size:16.0px; line-height:1.13em;
}
.Corps-C
{
    font-family:"Verdana", sans-serif; font-size:16.0px; line-height:1.13em;
}
.Normal-C-C1
{
    font-family:"Arial", sans-serif; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C0
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C1
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:21.0px; line-height:1.19em;
}
.Corps-C-C2
{
    font-family:"Verdana", sans-serif; font-size:19.0px; line-height:1.21em;
}
.Normal-C-C2
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:16.0px; line-height:1.25em;
}
.Normal-C-C3
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:24.0px; line-height:1.25em;
}
.Normal-C-C4
{
    font-family:"Verdana", sans-serif; color:#ffffff; font-size:16.0px; line-height:1.13em;
}
.Normal-C-C5
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:16.0px; line-height:1.25em;
}
.Normal-C-C6
{
    font-family:"Arial", sans-serif; font-size:24.0px; line-height:1.17em;
}

#divcontentmainbody {
	position: absolute;
	left: 17px;
	top: 230px;
	width:605px;
	height:690px;
	border:none;
	background-image: url(<?php echo GetImagePath("front/"); ?>/wpf861f3c9_06.png);
	background-repeat:no-repeat;
	padding-top: 300px;
	padding-left: 15px;
}
</style>
<script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/jspngfix.js"></script>
<link rel="stylesheet" href="fidelisation_fichiers/wpstyles.css" type="text/css">
<script type="text/javascript">var blankSrc = "wpscripts/blank.gif";
</script>
<SCRIPT type="text/javascript" src="<?php echo GetCssPath("front/") ; ?>/validation_cadeaux_files/lmpres80.js"></SCRIPT>
	<SCRIPT type="text/javascript" src="<?php echo GetCssPath("front/") ; ?>/validation_cadeaux_files/validation-cadeaux.js"></SCRIPT>
    <SCRIPT type="text/javascript">
	  function submiter(){
	 
	  fcommandeadd.submit();
	  }
	  
	</SCRIPT>
    
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.6.4.js"></script>
	<script type="text/javascript">
	
		 var $j = jQuery.noConflict();
		 
		 // Use jQuery via $j(...)
		 $j(document).ready(function(){//debut ready fonction
			   
			   
			   $j("#valider_form_code").click(function(){
								var code_cadeaux_client = $j("#code_cadeaux_client").val();
								var code_cadeaux_commercant = $j("#code_cadeaux_commercant").val();
								
								if (code_cadeaux_client == "" || code_cadeaux_commercant == "") {
									//alert("Veuillez indiquer les deux codes demandés !");
									$j("#result_codes_validation").html("<p>Veuillez indiquer les deux codes demandés !</p>");
								} else  {
									$j("#code_cadeaux_form_validation").submit();
								}
				})
			   
			   
			   $j("#commander_cadeau").click(function(){
								var x_designation = $j("#x_designation").val();
								var x_quantite = $j("#x_quantite").val();
								
								var x_error = "";
								if (x_designation == "") {
									x_error = x_error + '<span style="color:#F00">Veuillez sélectionner le cadeau !<br/></span>';
								} 
								if (x_quantite == "" || isNaN(x_quantite) == true) {
									x_error = x_error + '<span style="color:#F00">Veuillez indiquer une valeur exacte de la quantité du cadeau !<br/></span>';
								}
								
								
								if (x_error == "")  {
									$j("#fcommandeadd").submit();
								} else {
									$j("#result_verif_pt_cadeau").html(x_error);	
								}
				})
			   
			   //To sho chat windows
			   $j("#x_designation").change(function(){
                                var id_cadeau = $j("#x_designation").val();
                                var id_user = <?php echo ($IdUser);?>;
                                $j.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url(); ?>front/fidelisation/verification_point_cadeau/"+id_cadeau+"/"+id_user,
                                    success: function(msg){
                                        //alert(msg);
                                        var numero_famille = msg;
                                        $j("#result_verif_pt_cadeau").html(numero_famille);
                                        //alert(numero_famille);
                                    }
                                    });
                                    //alert ("test "+$j("#x_designation").val());
                                                                    
                           });
			   
			   
			   <?php if (isset($Idcadeau)) { ?>
			   
				var id_cadeau_1 = <?php echo ($Idcadeau);?>;
				var id_user_1 = <?php echo ($IdUser);?>;
				$j.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>front/fidelisation/verification_point_cadeau/"+id_cadeau_1+"/"+id_user_1,
					success: function(msg){
						//alert(msg);
						var numero_famille_1 = msg;
						$j("#result_verif_pt_cadeau").html(numero_famille_1);
						//alert(numero_famille);
					}
					});
					//alert ("test <?php echo ($Idcadeau);?>");
													
		        <?php } ?>
			   
			   
		 });
		
	</script>


</head>

<body style="background-color: transparent; background-image: url(&quot;<?php echo GetImagePath("front/"); ?>/wp007ff600_06.png&quot;); background-repeat: repeat; background-position: center top; background-attachment: scroll; text-align: center; height: 2000px;" text="#000000">
<div style="background-color:#cccccc;text-align:left;margin-left:auto;margin-right:auto;position:relative;width:650px;height:2000px;">
<map id="map0" name="map0">
    <area shape="rect" coords="11,121,75,193" href="<?php echo site_url();?>" salt="">
    <area shape="rect" coords="89,121,153,194" href="javascript:history.back()" alt="">
    <area shape="rect" coords="169,121,233,194" href="http://www.proximite-magazine.com/clubnv/fonctionnementclub.html" alt="">
    <area shape="rect" coords="246,121,310,194" href="<?php echo site_url("front/contact");?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wpd22efe04_06.png" id="hs_4" title="" alt="contact" onload="OnLoadPngFix()" usemap="#map0" style="position: absolute; left: 0px; top: 0px;" height="200" border="0" width="650">





<div id="divcontentmainbody">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <div id="result_codes_validation" style="color:#F00; width:100%; text-align:center; font-family:Arial, Helvetica, sans-serif;">
    <?php if (isset($zResultvalidation)) echo $zResultvalidation; ?>
    </div>
    </td>
  </tr>
</table>




<span class="Corps-C-C0"><?php  if ($UserCivilite == 0 ) {	
                $nomCivilite = "Mr";
           } 
		   if ($UserCivilite == 1 ) {	
                $nomCivilite = "Mme";
           } 
		   if ($UserCivilite == 2 ) {	
                $nomCivilite = "Mlle";
           } 
	    echo  $nomCivilite; ?> 

<?php echo $UserPrenom ; ?>, votre capital disponible est de <span class="Corps-C-C1"><?php echo $NbrPoints ; ?></span> points</span><br/><br/>
<span class="Corps-C-C0">Ajouter des points à votre capital en validant la concrétisation d'un bon plan</span><br/>


<form name="code_cadeaux_form_validation" id="code_cadeaux_form_validation" method="post" action="<?php echo site_url("front/fidelisation/verification_code_cadeaux/");?>">
<input name="IdUser" id="IdUser" value="<?php echo ($IdUser) ;?>" type="hidden">
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td><p class="Wp-Normal-P"><span class="Normal-C-C1">Précisez le code que vous avez reçu de Club-Proximité ...</span></p></td>
    <td><input id="code_cadeaux_client" name="code_cadeaux_client" style="width: 188px;" type="text"></td>
  </tr>
  <tr>
    <td><p class="Normal-P-P1"><span class="Normal-C-C1">Précisez le code de validation qui vous a été remis</span></p>
<p class="Normal-P-P1"><span class="Normal-C-C1">par notre Partenaire ...</span></p></td>
    <td><input id="code_cadeaux_commercant" name="code_cadeaux_commercant" style="width: 188px;" type="text"></td>
  </tr>
  
  
  <tr>
    <td colspan="2">
   <p class="Corps-P-P0"><span class="Corps-C-C0">Ces informations étant reconnues et validées, votre capital points sera augmenté</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C0">de 10 points.</span></p>
    </td>
  </tr>
  
  <tr>
    <td colspan="2">
    <div style="text-align:right;">
    <button type="button" name="valider_form_code" id="valider_form_code" style="border: none; background-color:#FFF;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpc5451f16_06.png" id="pic_1598" alt="" onload="OnLoadPngFix()" height="30" border="0" width="106">
    </button>
    </div>
    </td>
  </tr>
</table>
</form>



<form name="fcommandeadd" id="fcommandeadd" action="<?php echo site_url("front/fidelisation/commande/");?>" method="post"> <!--onSubmit="return commande_add.ValidateForm(this);"-->
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td colspan="2">
    <div style="background-image:url(<?php echo GetImagePath("front/"); ?>/wp2ddc8e98_06.png); background-repeat:no-repeat; text-align:center">
    <span class="Normal-C-C2">Votre nouveau capital disponible &nbsp;est &nbsp;de &nbsp;</span><span class="Normal-C-C3"><?php echo $NbrPoints ; ?> points</span>
    </div>
    </td>
  </tr>
  <tr>
    <td><span class="Corps-C-C0">Choisissez votre cadeau </span></td>
    <td>
    <div style="text-align:right"><select id="x_designation" name="x_designation" >
        <option value="" selected="selected">
        Choisissez le cadeau</option>
        <?php if(count($listCadeau)>0) { foreach ($listCadeau as $oCadeau) {?>

        <option value="<?php echo ($oCadeau->Idcadeau);?>" <?php if (isset($Idcadeau) && $Idcadeau == $oCadeau->Idcadeau) {?> selected="selected" <?php }?>>
        <?php echo ($oCadeau->Nomoffre) ; ?></option>    <?php } }?>
    </select>	
    <input name="IdUser" id="IdUser" value="<?php echo ($IdUser) ;?>" type="hidden"></div>
    </td>
  </tr>
  <tr>
    <td><span class="Corps-C-C0">Nombre de cadeau</span></td>
    <td>
    <div style="text-align:right"><input name="x_quantite" id="x_quantite" type="text"></div>
    </td>
  </tr>
  <tr>
    <td><div id="result_verif_pt_cadeau" style="font-family:Verdana, Geneva, sans-serif; font-size:12px;"></div></td>
    <td>
    <div style="text-align:right;">
    <button type="button" name="commander_cadeau" id="commander_cadeau" style="border:none; background-color:#FFF;"> 
    <img src="<?php echo GetImagePath("front/"); ?>/wpc5451f16_06.png" alt="" name="pic_1596" width="106" height="30" border="0" id="pic_1596" onload="OnLoadPngFix()">
    </button>
    </div>
    </td>
  </tr>
  <tr>
    <td colspan="2"><p class="Normal-P-P0" style="margin-top:0px"><span class="Normal-C-C1">Votre commande vous est confirmée immédiatement par email.</span></p>
<p class="Normal-P-P0"><span class="Normal-C-C1">Sous 8 jours vous recevrez par courrier à votre domicile le ou les cadeaux validés.</span></p></td>
  </tr>
</table>
</form>

<!--<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td colspan="2">
    <div style="background-image:url(<?php echo GetImagePath("front/"); ?>/wp2ddc8e98_06.png); background-repeat:no-repeat; text-align:center">
    <span class="Normal-C-C2">Le solde de votre capital disponible &nbsp;est &nbsp;de &nbsp;</span><span class="Normal-C-C3">0 points</span>
    </div>
    </td>
  </tr>
  <tr>
    <td><span class="Normal-C-C5">Visualiser mes archives</span></td>
    <td><div style="text-align:right;"><img src="<?php echo GetImagePath("front/"); ?>/wpc5451f16_06.png" id="pic_" alt="" onload="OnLoadPngFix()" height="30" border="0" width="106"></div></td>
  </tr>
  
</table>-->

</div>


</div>

</body></html>