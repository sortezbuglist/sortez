<?php $data["zTitle"] = 'Inscription des professionnels'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>


<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra'], headers: {7: { sorter: false}, 8: {sorter: false} }})
				.tablesorterPager({container: $("#pager")});
		});
</script>    <div id="divMesAnnonces" class="content" align="center">
        <form name="frmMesAnnonces" id="frmMesAnnonces" action="" method="POST" enctype="multipart/form-data">
			
            
            <p><span align="left"><a style = "text-decoration:none;" href="<?php echo site_url("front/utilisateur/contenupro") ;?>"><input type = "button" id = "btnnew" value ="retour au menu" onclick="document.location='<?php echo site_url("front/professionnels/fiche/$idCommercant") ;?>';"/></a></span></p>
            
            
            <?php if (isset($limit_annonce_add) && $limit_annonce_add==1) { echo "Vous avez atteint la limite de 20 annonces.";} else {?>
            <p><span align="left">
            <a style = "text-decoration:none;" href="<?php echo site_url("front/annonce/ficheAnnonce/$idCommercant") ;?>">
				<input type = "button" id = "btnnew" value ="Ajouter une nouvelle annonce" onclick="document.location='<?php echo site_url("front/annonce/ficheAnnonce/$idCommercant") ;?>';"/>
				</a></span></p>
                <?php }?>
                
				
				
            <h1>Liste de mes annonces</h1>
				
				<div id="container">
					<table cellpadding="1" class="tablesorter">
						<thead>
							<tr>                    
								<th>Description courte</th>
								<th width="300">Description longue</th>
								<th>Date debut</th>
								<th>Date fin</th>
								<th>Prix neuf</th>
								<th>Prix solde</th>
								<th>Etat</th>
								<th width="10"></th>
								<th width="10"></th>
							</tr>
						</thead>
						<tbody> 
							<?php foreach($toListeMesAnnonce as $oListeMesAnnonce){ ?>
								<tr>                    
									<td><?php echo $oListeMesAnnonce->annonce_description_courte ; ?></td>
									<td><?php echo truncate(strip_tags($oListeMesAnnonce->annonce_description_longue),100," ...") ; ?></td>
									<td><?php 
									if (convertDateWithSlashes($oListeMesAnnonce->annonce_date_debut)!="00/00/0000")
									echo convert_Sqldate_to_Frenchdate($oListeMesAnnonce->annonce_date_debut) ; ?></td>
									<td><?php 
									if (convertDateWithSlashes($oListeMesAnnonce->annonce_date_fin)!="00/00/0000")
									echo convert_Sqldate_to_Frenchdate($oListeMesAnnonce->annonce_date_fin) ; ?></td>
									<td><?php 
									if ($oListeMesAnnonce->annonce_prix_neuf!=0)
									echo $oListeMesAnnonce->annonce_prix_neuf ; ?></td>
									<td><?php 
									if ($oListeMesAnnonce->annonce_prix_solde!=0)
									echo $oListeMesAnnonce->annonce_prix_solde ; ?></td>
									<td><?php if ($oListeMesAnnonce->annonce_etat == 0) { echo "Occasion" ; }else { echo "Occasion" ;} ?></td>
									<td><a href="<?php echo site_url("front/annonce/ficheAnnonce/" . $oListeMesAnnonce->annonce_commercant_id . "/" . $oListeMesAnnonce->annonce_id ) ; ?>">Modifier</a></td>
									<td><a href="<?php echo site_url("front/annonce/supprimAnnonce/" . $oListeMesAnnonce->annonce_id . "/" . $oListeMesAnnonce->annonce_commercant_id) ; ?>" onclick="if (!confirm('voulez-vous vraiment supprimer cette annoce?')){ return false ; }">Supprimer</a></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
					<div id="pager" class="pager">
							<img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
							<img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
							<input type="text" class="pagedisplay"/>
							<img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
							<img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
							<select class="pagesize" style="visibility:hidden">
								<option selected="selected"  value="10">10</option>
								<option value="20">20</option>
								<option value="30">30</option>
								<option  value="40">40</option>
							</select>
					</div>
				</div>
        </form>
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>