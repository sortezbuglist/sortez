<?php $data["zTitle"] = 'Inscription des professionnels'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="fr"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="Generator" content="Serif WebPlus X5">
<meta http-equiv="X-UA-Compatible" content="IE=EmulationIE8">
<title>Inscription</title>
<style type="text/css">
#ErrorMessage {
    color: red;
    font-style: italic;
    text-align: center;
}
body {margin: 0px; padding: 0px;}
.Normal-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:center; font-weight:400;
}
.Normal-C
{
    font-family:"Rockwell", serif; font-weight:700; color:#ffffff; font-size:24.0px; line-height:1.21em;
}
.Corps-C
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:15.0px; line-height:1.20em;
}
.Normal-C-C0
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:13.0px; line-height:1.23em;
}
.Normal-C-C1
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:12.0px; line-height:1.25em;
}
.Button1,.Button1:link,.Button1:visited{background-position:0px 0px;text-decoration:none;display:block;position:absolute;background-image:url(<?php echo GetImagePath("front/"); ?>/wp510a01e1_06.png);}
.Button1:focus{outline-style:none;}
.Button1:hover{background-position:0px -60px;}
.Button1:active{background-position:0px -30px;}
.Button1 span,.Button1:link span,.Button1:visited span{color:#000000;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:none;font-style:normal;left:22px;top:7px;width:267px;height:15px;font-size:12px;display:block;position:absolute;cursor:pointer;}
.Button2,.Button2:link,.Button2:visited{background-position:0px 0px;text-decoration:none;display:block;position:absolute;background-image:url(<?php echo GetImagePath("front/"); ?>/wp6926c6e5_06.png);}
.Button2:focus{outline-style:none;}
.Button2:hover{background-position:0px -60px;}
.Button2:active{background-position:0px -30px;}
.Button2 span,.Button2:link span,.Button2:visited span{color:#000000;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:none;font-style:normal;left:22px;top:7px;width:267px;height:15px;font-size:12px;display:block;position:absolute;cursor:pointer;}
.Button3,.Button3:link,.Button3:visited{background-position:0px 0px;text-decoration:none;display:block;position:absolute;background-image:url(<?php echo GetImagePath("front/"); ?>/wp6c154bd0_06.png);}
.Button3:focus{outline-style:none;}
.Button3:hover{background-position:0px -60px;}
.Button3:active{background-position:0px -30px;}
.Button3 span,.Button3:link span,.Button3:visited span{color:#000000;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:none;font-style:normal;left:22px;top:7px;width:104px;height:15px;font-size:12px;display:block;position:absolute;cursor:pointer;}
.Button4,.Button4:link,.Button4:visited{background-position:0px 0px;text-decoration:none;display:block;position:absolute;background-image:url(<?php echo GetImagePath("front/"); ?>/wpcc0931d4_06.png);}
.Button4:focus{outline-style:none;}
.Button4:hover{background-position:0px -60px;}
.Button4:active{background-position:0px -30px;}
.Button4 span,.Button4:link span,.Button4:visited span{color:#000000;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:none;font-style:normal;left:22px;top:7px;width:89px;height:15px;font-size:12px;display:block;position:absolute;cursor:pointer;}
</style>
<script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/jspngfix.js"></script>
<link rel="stylesheet" href="<?php echo GetImagePath("front/"); ?>/wpstyles.css" type="text/css">
<script type="text/javascript">var blankSrc = "<?php echo GetImagePath("front/"); ?>/blank.gif";
</script>
</head>

<body style="background-color: rgb(255, 255, 255); background-image: url(<?php echo GetImagePath("front/"); ?>/wp8da8cbec_06.png); background-repeat: repeat; background-position: center top; background-attachment: scroll; text-align: center; height: 1300px;" text="#000000">
<div style="background-color:#ffffff;text-align:left;margin-left:auto;margin-right:auto;position:relative;width:1024px;height:1300px;">
<img src="<?php echo GetImagePath("front/"); ?>/wp17345c2a_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 0px; top: 0px;" border="0" height="239" width="1023">
<img src="<?php echo GetImagePath("front/"); ?>/wp23fa3ca3_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 224px; top: 260px;" border="0" height="35" width="578">
<div style="position:absolute; left:224px; top:262px; width:561px; height:30px;">
    <div class="Normal-P">
        <span class="Normal-C">Confirmation de votre demande d’Inscription</span></div>
</div>
<map id="map0" name="map0">
    <area shape="rect" coords="10,8,13,14" href="http://www.creezvotresiteweb.fr/index.html" alt="">
    <area shape="poly" coords="25,16,16,7,7,16,8,17,16,9,24,17" href="http://www.club-proximite.com/" alt="">
    <area shape="poly" coords="22,10,16,4,10,10,10,19,14,19,14,13,18,13,18,19,22,19" href="http://www.creezvotresiteweb.fr/index.html" alt="">
    <area shape="poly" coords="31,30,31,1,30,0,1,0,0,3,0,30,1,31,31,31" href="http://www.creezvotresiteweb.fr/index.html" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wp204bb0c4_06.png" alt="" onload="OnLoadPngFix()" usemap="#map0" style="position: absolute; left: 930px; top: 157px;" border="0" height="32" width="32">
<map id="map1" name="map1">
    <area shape="poly" coords="15,20,26,20,26,13,15,13,15,9,5,16,15,24" href="javascript:history.back()" alt="">
    <area shape="poly" coords="31,30,31,1,30,0,1,0,0,3,0,30,1,31,31,31" href="javascript:history.back()" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wpb9e0da9a_06.png" alt="" onload="OnLoadPngFix()" usemap="#map1" style="position: absolute; left: 967px; top: 157px;" border="0" height="32" width="32">


<div id="txt_744" style="position:absolute; left:300px; top:676px; width:458px; height: auto; overflow:hidden; font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight:bold; text-align:center;" align="center">
<p>L’équipe de www.creezvotresiteweb.fr vous remercie de votre confiance.</p>


<p>Votre demande d’adhésion est enregistrée.</p>

<p>Vous recevrez sous peu une facture correspondante.</p>

<p>Dès réception de votre règlement, votre compte sera activé,

votre identifiant et mot de passe vous seront envoyés…</p>

</div>




<img src="<?php echo GetImagePath("front/"); ?>/wp3994bdfc_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 817px; top: 260px;" border="0" height="400" width="192">
<a href="http://www.proximite-magazine.com/" id="btn_135" class="Button3" style="position: absolute; left: 846px; top: 619px; width: 147px; height: 30px;"><span>Accès </span></a>
<img src="<?php echo GetImagePath("front/"); ?>/wp753642b8_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 15px; top: 260px;" border="0" height="400" width="192">
<a href="http://www.dossiersdepresse-paca.com/" id="btn_136" class="Button4" style="position: absolute; left: 68px; top: 627px; width: 133px; height: 30px;"><span>Accès </span></a>
<img src="<?php echo GetImagePath("front/"); ?>/wpa7692879_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 234px; top: 303px;" border="0" height="308" width="545">
<img src="<?php echo GetImagePath("front/"); ?>/wp07aa8538_06.png" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 0px; top: 1258px;" border="0" height="124" width="1024">
<div style="position:absolute;left:29px;top:1281px;width:978px;height:93px;">
    <div class="Normal-P">
        <span class="Normal-C-C0">Creezvotresiteweb.fr est une marque déposée<br></span>
        <span class="Normal-C-C1"> Editeur : Phase SARL 60 avenue de Nice 06800 Cagnes-<wbr>sur-<wbr>Mer<br></span>
        <span class="Normal-C-C1">Sarl au capital de 7265 € -<wbr> RCS Antibes 412.071.466<br></span><span class="Normal-C-C0">Téléphone : 00 33 (0)4 93 73 84 51 -<wbr> Mobile : 00 33 (0)6 72 05 59 35 -<wbr> proximite-<wbr>magazine@orange.fr</span></div>
</div>
</div>

</body></html>