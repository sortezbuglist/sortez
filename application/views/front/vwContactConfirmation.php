<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="fr"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="Generator" content="Serif WebPlus X5">
<meta http-equiv="X-UA-Compatible" content="IE=EmulationIE8">
<title>Contact</title>

<style type="text/css">
body {margin: 0px; padding: 0px;}
.Corps-artistique-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:left; font-weight:400;
}
.Corps-P
{
    margin:0.0px 0.0px 12.0px 0.0px; text-align:justify; font-weight:400;
}
.Corps-P-P0
{
    margin:0.0px 0.0px 1.0px 0.0px; text-align:left; font-weight:400;
}
.Normal-P
{
    margin:0.0px 0.0px 1.0px 0.0px; text-align:left; font-weight:400;
}
.Corps-artistique-C
{
    font-family:"Verdana", sans-serif; font-size:12.0px; line-height:1.17em;
}
.Corps-C
{
    font-family:"Verdana", sans-serif; font-size:12.0px; line-height:1.17em;
}
.Corps-C-C0
{
    font-family:"Verdana", sans-serif; font-size:11.0px; line-height:1.18em;
}
.Corps-C-C1
{
    font-family:"Verdana", sans-serif; font-size:9.0px; line-height:1.33em;
}
.Corps-C-C2
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C3
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:12.0px; line-height:1.25em;
}
.Normal-C
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:12.0px; line-height:1.25em;
}
.Normal-C-C0
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
</style>
<script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/jspngfix.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>
<link rel="stylesheet" href="contact_fichiers/wpstyles.css" type="text/css">
<script type="text/javascript">var blankSrc = "wpscripts/blank.gif";
</script>




</head>

<body style="background-color: transparent; background-image: url(&quot;<?php echo GetImagePath("front/"); ?>/wp007ff600_06.png&quot;); background-repeat: repeat; background-position: center top; background-attachment: scroll; text-align: center; height: 1500px;" text="#000000">
<div style="background-color:#cccccc;text-align:left;margin-left:auto;margin-right:auto;position:relative;width:650px;height:1500px;">
<map id="map0" name="map0">
    <area shape="rect" coords="11,121,75,193" href="<?php echo site_url();?>" alt="">
    <area shape="rect" coords="89,121,153,194" href="javascript:history.back()" alt="">
    <area shape="rect" coords="169,121,233,194" alt="">
    <area shape="rect" coords="246,121,310,194" href="<?php echo site_url("front/contact");?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wpd22efe04_06.png" id="hs_4" title="" alt="contact" onload="OnLoadPngFix()" usemap="#map0" style="position: absolute; left: 0px; top: 0px;" height="200" border="0" width="650">
<img src="<?php echo GetImagePath("front/"); ?>/wp4815d3de_06.png" id="pic_1579" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 15px; top: 217px;" height="520" border="0" width="620">
<div id="art_73" style="position:absolute; left:72px; top:297px; width:483px; height:49px;">
    <div class="Corps-artistique-P">
        <span class="Corps-artistique-C">
        <?php if (isset($IdInsertedContact)) {?>
        Votre demande de contact a bien été envoyé. Merci.
        <?php } else {?>
        une erreur est survenue. Veuillez refaire  l'opération.
        <?php } ?>
        </span></div>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/2966-istock-000009805936xsmall-s-.png" id="pic_1579" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 134px; top: 390px; z-index:1;" height="299" border="0" width="401">

<!--<div id="txt_45" style="position:absolute; left:67px; top:598px; width:251px; height:40px; overflow:hidden;">
<span class="Corps-C">Merci de saisir exactement les caractères qui s’affichent dans l’image</span>
</div>-->
<img src="<?php echo GetImagePath("front/"); ?>/wp999f4cb7_06.png" id="pic_1581" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 15px; top: 758px;" height="301" border="0" width="620">
<div id="txt_49" style="position:absolute;left:40px;top:826px;width:512px;height:205px;overflow:hidden;">
<p class="Corps-P-P0"><span class="Corps-C-C2">La SARL Phase est l’éditeur de ce site</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C2">L'hébergement est assuré par la SARL Phase</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C3">Adresse :</span><span class="Corps-C-C2"> 60 avenue de Nice 06800 Cagnes sur Mer</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C3">Directeur de publication : </span><span class="Corps-C-C2">Jean Pierre Woignier</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C3">Téléphone direct : </span><span class="Corps-C-C2">04.93.73.84.51</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C3">Téléphone mobile :</span><span class="Corps-C-C2"> 06.72.05.59.35</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C3">Courrier :</span><span class="Corps-C-C2"> proximite-<wbr>magazine@orange.fr</span></p>
<p class="Normal-P"><span class="Normal-C">Statut :</span><span class="Normal-C-C0"> SARL au capital de 7265€ </span></p>
<p class="Normal-P"><span class="Normal-C">N° Registre de commerce :</span><span class="Normal-C-C0"> RCS Antibes 412.071.466</span></p>
<p class="Normal-P"><span class="Normal-C">SIRET :</span><span class="Normal-C-C0"> 412071466 00012</span></p>
<p class="Normal-P"><span class="Normal-C">CODE NAF :</span><span class="Normal-C-C0"> 7022Z</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C3">IBAN :</span><span class="Corps-C-C2"> FR76 3007 6023 5110 6081 0020 072</span></p>
</div>


</div>

</body></html>