<?php $data["zTitle"] = "Mentions legales";?>

<?php 
$base_path_system = str_replace('system/', '', BASEPATH);
include($base_path_system.'application/views/front2013/includes/url_var_definition.php');
//$this->load->view("front2013/includes/url_var_definition", $data); 
?>

<?php $this->load->view("front2013/includes/mobile_header_page_partner", $data); ?>



<div id="txt_619" style=" padding-top:15px; padding-bottom:20px; padding-left:15px; padding-right:15px; background-color:#FFFFFF;">
<p class="Wp-Normal-P"><span class="Strong-C">Le dépositaire est l’éditeur de ce site</span></p>
<p class="Wp-Normal-P"><span class="Strong-C">La SARL &nbsp;Phase est le prestataire technique.</span><span class="Normal-C"><br>&nbsp;</span></p>
<p class="Corps-P"><span class="Corps-C">Nom ou Raison sociale :</span><span class="Corps-C_val"><?php if ($oInfoCommercant->NomSociete!="") echo ' '.$oInfoCommercant->NomSociete ; ?></span></p>
<p class="Corps-P"><span class="Corps-C">Adresse : </span><span class="Corps-C_val"><?php echo ' '.$oInfoCommercant->Adresse1 ; ?><?php if ($oInfoCommercant->Adresse2 != "") echo " - ".$oInfoCommercant->Adresse2 ; ?></span></p>
<p class="Corps-P"><span class="Corps-C">Adresse électronique : </span><span class="Corps-C_val"><a href="mailto:<?php echo $oInfoCommercant->Email;?>"><?php echo ' '.$oInfoCommercant->Email ; ?></a></span></p>
<p class="Corps-P"><span class="Corps-C">Téléphone : </span><span class="Corps-C_val"><?php if ($oInfoCommercant->TelFixe!="") echo ' '.$oInfoCommercant->TelFixe ; ?></span></p>
<p class="Corps-P"><span class="Corps-C">N° Registre Commerce :</span><span class="Corps-C">&nbsp;</span></p>
<p class="Corps-P"><span class="Corps-C">Mentions RCS : </span><span class="Corps-C">&nbsp;</span></p>
<p class="Corps-P"><span class="Corps-C">Capital social : </span><span class="Corps-C">&nbsp;</span></p>
<p class="Corps-P"><span class="Corps-C">Adresse siège social :</span><span class="Corps-C">&nbsp;</span></p>
<p class="Corps-P"><span class="Corps-C-C0">Siret :</span><span class="Corps-C-C0">&nbsp;</span></p>
<p class="Corps-P"><span class="Corps-C-C0">N° d'assujetissement à la TVA :</span><span class="Corps-C-C0">&nbsp;</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C0">Directeur de rédaction et de publication </span></p>
<p class="Normal-P">&nbsp;</p>
<p class="Normal-P">&nbsp;</p>
<p class="Normal-P"><span class="Normal-C-C0">L’hébergement est assuré par :</span></p>
<p class="Normal-P"><span class="Normal-C-C0">SARL PHASE</span></p>
<p class="Normal-P"><span class="Normal-C-C0">60 avenue de Nice</span></p>
<p class="Normal-P"><span class="Normal-C-C0">06800 Cagnes sur Mer</span></p>
<p class="Normal-P"><span class="Normal-C">Tél. : 04 93 73 84 51</span></p>
<p class="Normal-P"><span class="Normal-C">Mobile : 06 72 05 59 35</span></p>
<p class="Normal-P"><span class="Normal-C">SARL au capital de 7265€ </span></p>
<p class="Normal-P"><span class="Normal-C">RCS Antibes 412.071.466</span></p>
<p class="Normal-P"><span class="Normal-C">SIRET : 412071466 00012</span></p>
<p class="Normal-P"><span class="Normal-C">CODE NAF : 7022Z</span></p>
<p class="Corps-P-P1"><span class="Corps-C-C0">IBAN : FR76 3007 6023 5110 6081 0020 072</span></p>
</div>


<?php $this->load->view("front2013/includes/mobile_footer_page_partner", $data); ?>