<?php $data["zTitle"] = 'Inscription des professionnels'; ?>
<?php $this->load->view("front/includes/vwHeader_nv", $data); ?>
<?php $this->load->view("front/includes/vwMenu"); ?>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>
<script type="text/javascript">
function listeSousRubrique(){
         
        var irubId = jQuery('#RubriqueSociete').val();     
        jQuery.get(
            '<?php echo site_url("front/professionnels/testAjax"); ?>' + '/' + irubId,
            function (zReponse)
            {
                // alert (zReponse) ;
                jQuery('#trReponseRub').html("") ; 
                jQuery('#trReponseRub').html(zReponse) ;
               
             
           });
    }
	function getCP(){
	      var ivilleId = jQuery('#VilleSociete').val();
          jQuery.get(
            '<?php echo site_url("front/professionnels/getPostalCode"); ?>' + '/' + ivilleId,
            function (zReponse)
            {
                jQuery('#trReponseVille').html(zReponse);
               
             
           });
	}
    
    function deleteFile(_IdCommercant,_FileName) {
        jQuery.ajax({
            url: gCONFIG["SITE_URL"] + 'front/professionnels/delete_files/' + _IdCommercant + '/' + _FileName,
            dataType: 'html',
            type: 'POST',
            async: true,
            success: function(data){
                window.location.reload();
            }
        });
    }
    
    jQuery(document).ready(function() {
        jQuery(".btnSinscrire").click(function(){
            var txtError = "";
            var EmailSociete = jQuery("#EmailSociete").val();
            if(!isEmail(EmailSociete)) {
                jQuery("#divErrorEmailSociete").html("Cet email n'est pas valide. Veuillez saisir un email valide");
                jQuery("#divErrorEmailSociete").show();
                txtError += "1";
            } else {
                jQuery("#divErrorEmailSociete").hide();
            }
            /*
            var LoginSociete = jQuery("#LoginSociete").val();
            if(!isEmail(LoginSociete)) {
                txtError += "1";
                jQuery("#divErrorLoginSociete").html("Votre login doit &ecirc;tre un email valide.");
                jQuery("#divErrorLoginSociete").show();
            } else {
                jQuery("#divErrorLoginSociete").hide();
            }
            */
            if(txtError == "") {
                jQuery("#frmInscriptionProfessionnel").submit();
            }
        })
		
		
		//$('#bandeau_colorSociete').colorPicker();
    })
</script>
    
    <a href="<?php echo site_url($objCommercant->nom_url);?>" target="_blank" onClick="var w = window.open(this.href,'_blank','width=950,height=1200,menubar=no,resizable=no,scrollbars=yes,status=no,toolbar=no'); if( w != null ){ w.focus(); }; return false;" alt="show"><img src="<?php echo GetImagePath("front/");?>/visualisezsite.png" width="206" height="166" alt="logo" title="" style="position:absolute; left:807px; top:245px; z-index:1;"/></a>
    
    <a href="http://www.creezvotresiteweb.fr/aideinscription.html" target="_blank" alt="help"><img src="<?php echo GetImagePath("front/");?>/iconeaide.png" width="189" height="183" alt="logo" title="" style="position:absolute; left:31px; top:245px; z-index:1;"/></a>
    
    
    <div id="divInscriptionProfessionnel" class="content" align="center" style="background-color: #fff;">
<h1>Fiche Professionnel</h1>
		<br>
        <?php if (isset($mssg) && $mssg==1) {?> <div align="center" style="text-align:center; font-style:italic;color:#0C0;">Les modifications ont &eacute;t&eacute; enregistr&eacute;es</div><br><?php }?>
            <?php if(isset($objCommercant)) {  ?>
                       <table>
                       		<?php if(isset($show_annonce_btn) && ($show_annonce_btn==1)) { ?>
                            <tr>
                                <td></td>
                                <td align="left"><a style="text-decoration:none;" href="<?php echo site_url("front/annonce/listeMesAnnonces/$objCommercant->IdCommercant");?>"><input type ="button" style="width:200px;"id="btnannonce" value="gestions des annonces" onclick="document.location='<?php echo site_url("front/annonce/listeMesAnnonces/$objCommercant->IdCommercant");?>';"/>  </a></td>
                            </tr>
                            <?php } ?>
                            <?php if(isset($show_bonplan_btn) && ($show_bonplan_btn==1)) { ?>
                             <tr>
                                <td></td>
                                <td align="left"><a  href="<?php echo site_url("front/bonplan/listeMesBonplans/$objCommercant->IdCommercant");?>"><input type ="button" style="width:200px;" id="btnbonplan" value="gestions des bons plans" onclick="document.location='<?php echo site_url("front/bonplan/listeMesBonplans/$objCommercant->IdCommercant");?>';"/> </a></td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td></td><td></td>
                            </tr>
                        </table>
                    <?php } ?>
					<div>
	     <?php if(isset($objCommercant)) { ?>
		<!--<form action ="<?php// echo site_url("front/professionnels/activationBonPlan/$objCommercant->IdCommercant");?>" method="POST">
		  
		<div style="clear:both; padding:10px ">
                <fieldset>
                    <legend><div style="width:300px;font-size:20px;background-color:#123;color:#fff">Validation d'un Bon Plan</div></legend>
                    <table width="100%" cellpadding="2" cellspacing="2">
                        <tr>
                            <td>
                               <label class="label">&nbsp;&nbsp;    Entrer le numero du bon plan :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>
                               <input type="text" name="bonPlan" id="bonPlan" value="" style="width:140px;"/>

                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                               <input id = "btnBonPlan" type = "submit" value= "Valider le bon plan" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
                </div>
		</form>-->
		  <?php } ?>
		  </div>
		  
        <form name="frmInscriptionProfessionnel" id="frmInscriptionProfessionnel" action="<?php echo site_url("front/professionnels/modifier"); ?>" method="POST" enctype="multipart/form-data">
            
		<div style="clear:both; padding:10px ">
            <fieldset>
                <legend><div style="width:300px;font-size:20px;background-color:#123;color:#fff">Vos coordonn&eacute;es</div></legend>
                <table width="100%" cellpadding="2" cellspacing="2" style="float:left; text-align:left;" align="left">
                    
                    <tr>
                            <td width="300">
                                <label class="label">Logo : </label>
                                <br/>(la largeur ne doit pas d&eacute;passer 270px)
                          </td>
                            <td>
                                <?php if(!empty($objCommercant->Logo)) { ?>
                                    <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->Logo, 100, 100,'',''); ?>
                                    <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Logo');">Supprimer</a>
                                <?php } ?><br/>
                                <input type="file" name="Societelogo" id="logoSociete" value="" />
                            </td>
                        </tr>
                        <tr>
                        <td width="300">
                            <label class="label">Lien logo : </label>
                      </td>
                        <td>
                            <input type="text" name="Societe[logo_url]" id="logo_url" value="<?php if(isset($objCommercant->logo_url)) echo htmlspecialchars($objCommercant->logo_url); else echo 'http//';?>" />
                        </td>
                    </tr>
                    <tr>
                        <td width="300">
                            <label class="label">D&eacute;signation de votre activit&eacute; : </label>
                      </td>
                        <td>
                            <input type="text" name="Societe[titre_entete]" id="titre_entete" value="<?php if(isset($objCommercant->titre_entete)) echo htmlspecialchars($objCommercant->titre_entete); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td width="300">
                            <label class="label">Rubrique : </label>
                      </td>
                        <td>
                            <select name="AssCommercantRubrique[IdRubrique]" onchange="javascript:listeSousRubrique();" id="RubriqueSociete">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($colRubriques)) { ?>
                                    <?php foreach($colRubriques as $objRubrique) { ?>
                                        <option <?php if(isset($objAssCommercantRubrique[0]->IdRubrique) &&$objAssCommercantRubrique[0]->IdRubrique == $objRubrique->IdRubrique) { ?>selected="selected"<?php } ?> value="<?php echo $objRubrique->IdRubrique; ?>"><?php echo htmlentities($objRubrique->Nom); ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr id='trReponseRub'>
                        <td width="300">
                            <label class="label">Sous-rubrique : </label>
                      </td>
                        <td>
                            <select name="AssCommercantSousRubrique[IdSousRubrique]" id="SousRubriqueSociete">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($colSousRubriques)) { ?>
                                    <?php foreach($colSousRubriques as $objSousRubrique) { ?>
                                        <option <?php if(isset($objAssCommercantSousRubrique[0]->IdSousRubrique) && $objAssCommercantSousRubrique[0]->IdSousRubrique == $objSousRubrique->IdSousRubrique) { ?>selected="selected"<?php } ?>  value="<?php echo $objSousRubrique->IdSousRubrique; ?>"><?php echo htmlentities($objSousRubrique->Nom); ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <?php   /**?>
                    <tr>
                        <td width="300">
                            <label class="label">Sous-rubrique : </label>
                      </td>
                        <td>
                            <select name="AssCommercantSousRubrique[IdSousRubrique]" id="SousRubriqueSociete">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($colSousRubriques)) { ?>
                                    <?php foreach($colSousRubriques as $objSousRubrique) { ?>
                                        <option <?php if(isset($objAssCommercantSousRubrique[0]->IdSousRubrique) && $objAssCommercantSousRubrique[0]->IdSousRubrique == $objSousRubrique->IdSousRubrique) { ?>selected="selected"<?php } ?> value="<?php echo $objSousRubrique->IdSousRubrique; ?>"><?php echo htmlentities($objSousRubrique->Nom); ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <?php   */ ?>
                    <tr>
                        <td width="300">
                            <label class="label">Nom de la soci&eacute;t&eacute; : </label>
                      </td>
                        <td>
                            <input type="text" name="Societe[NomSociete]" id="NomSociete" value="<?php if(isset($objCommercant->NomSociete)) echo htmlspecialchars($objCommercant->NomSociete); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td width="300">
                            <label class="label">Siret : </label>
                      </td>
                        <td>
                            <input type="text" name="Societe[Siret]" id="SiretSociete" value="<?php if(isset($objCommercant->Siret)) echo htmlspecialchars($objCommercant->Siret); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td width="300">
                            <label class="label">Adresse de l'&eacute;tablissement : </label>
                      </td>
                        <td>
                            <input type="text" name="Societe[Adresse1]" id="Adresse1Societe" value="<?php if(isset($objCommercant->Adresse1)) echo htmlspecialchars($objCommercant->Adresse1); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td width="300">
                            <label class="label">Compl&eacute;ment d'Adresse : </label>
                      </td>
                        <td>
                            <input type="text" name="Societe[Adresse2]" id="Adresse2Societe" value="<?php if(isset($objCommercant->Adresse2)) echo htmlspecialchars($objCommercant->Adresse2); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td width="300">
                            <label class="label">Ville : </label>
                      </td>
                        <td>
                            <select name="Societe[IdVille]" id="VilleSociete" onchange = "getCP();">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($colVilles)) { ?>
                                    <?php foreach($colVilles as $objVille) { ?>
                                        <option <?php if(isset($objCommercant->IdVille) && $objCommercant->IdVille == $objVille->IdVille) { ?>selected="selected"<?php } ?> value="<?php echo $objVille->IdVille; ?>"><?php echo $objVille->Nom; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="300">
                            <label class="label">Code postal : </label>
                      </td>
                        <td id="trReponseVille">
                            <input type="text" name="Societe[CodePostal]" id="CodePostalSociete" value="<?php if(isset($objCommercant->CodePostal)) echo htmlspecialchars($objCommercant->CodePostal); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td width="300">
                            <label class="label">T&eacute;l&eacute;phone fixe : </label>
                      </td>
                        <td>
                            <input type="text" name="Societe[TelFixe]" id="TelFixeSociete" value="<?php if(isset($objCommercant->TelFixe)) echo htmlspecialchars($objCommercant->TelFixe); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td width="300">
                            <label class="label">T&eacute;l&eacute;phone mobile : </label>
                      </td>
                        <td>
                            <input type="text" name="Societe[TelMobile]" id="TelMobileSociete" value="<?php if(isset($objCommercant->TelMobile)) echo htmlspecialchars($objCommercant->TelMobile); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td width="300">
                            <label class="label">Fax : </label>
                      </td>
                        <td>
                            <input type="text" name="Societe[fax]" id="faxSociete" value="<?php if(isset($objCommercant->fax)) echo htmlspecialchars($objCommercant->fax); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td width="300">
                            <label class="label">Courriel : </label>
                      </td>
                        <td>
                            <input type="text" name="Societe[Email]" id="EmailSociete" value="<?php if(isset($objCommercant->Email)) echo htmlspecialchars($objCommercant->Email); ?>" />
                            <div class="FieldError" id="divErrorEmailSociete"></div>
                        </td>
                    </tr>
                    
                  
                    <!--activité 1 et activité 2 deleted from here-->
                    
            </table>
            </fieldset>
			</div>
			<div style="clear:both; padding:10px ">
            <fieldset>
                <legend><div style="width:300px;font-size:20px;background-color:#123;color:#fff">Nom D&eacute;cideur</div></legend>
                <table width="100%" cellpadding="2" cellspacing="2"  style="float:left; text-align:left;" align="left">
                    <tr>
                        <td width="300">
                            <label class="label">Civilit&eacute; : </label>
                      </td>
                        <td>
                            <select name="Societe[Civilite]" id="CiviliteResponsableSociete">
                                <option <?php if(isset($objCommercant->Civilite) && $objCommercant->Civilite == "0") { ?>selected="selected"<?php } ?> value="0">Monsieur</option>
                                <option <?php if(isset($objCommercant->Civilite) && $objCommercant->Civilite == "1") { ?>selected="selected"<?php } ?> value="1">Madame</option>
                                <option <?php if(isset($objCommercant->Civilite) && $objCommercant->Civilite == "2") { ?>selected="selected"<?php } ?> value="2">Mademoiselle</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="300">
                            <label class="label">Nom : </label>
                      </td>
                        <td>
                            <input type="text" name="Societe[Nom]" id="NomResponsableSociete" value="<?php if(isset($objCommercant->Nom)) echo htmlspecialchars($objCommercant->Nom); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td width="300">
                            <label class="label">Pr&eacute;nom : </label>
                      </td>
                        <td>
                            <input type="text" name="Societe[Prenom]" id="PrenomResponsableSociete" value="<?php if(isset($objCommercant->Prenom)) echo htmlspecialchars($objCommercant->Prenom); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td width="300">
                            <label class="label">Fonction <span class="indication">(G&eacute;rant, Directeur ...)</span>: </label>
                      </td>
                        <td>
                            <input type="text" name="Societe[Responsabilite]" id="ResponsabiliteResponsableSociete" value="<?php if(isset($objCommercant->Responsabilite)) echo htmlspecialchars($objCommercant->Responsabilite); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td width="300">
                            <label class="label">T&eacute;l&eacute;phone direct <span class="indication">(fixe ou portable)</span> : </label>
                      </td>
                        <td>
                            <input type="text" name="Societe[TelDirect]" id="TelDirectResponsableSociete" value="<?php if(isset($objCommercant->TelDirect)) echo htmlspecialchars($objCommercant->TelDirect); ?>" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td width="300">
                            <label class="label">Email : </label>
                      </td>
                        <td>
                            <input type="text" name="Societe[Email_decideur]" id="Email_decideurSociete" value="<?php if(isset($objCommercant->Email_decideur)) echo htmlspecialchars($objCommercant->Email_decideur); ?>" />
                        </td>
                    </tr>
                    
                    <?php //if ($userType == _USER_TYPE_ADMIN_) { ?>
                    <tr>
                        <td width="300">
                            <label class="label">Choix de votre abonnement : </label>
                      </td>
                        <td>
                            <!--<select name="AssAbonnementCommercant[IdAbonnement]" id="AbonnementSociete">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($colAbonnements)) { ?>
                                    <?php foreach($colAbonnements as $objAbonnement) { ?>
                                        <option <?php if(isset($objAssCommercantAbonnement[0]->IdAbonnement) && $objAssCommercantAbonnement[0]->IdAbonnement == $objAbonnement->IdAbonnement) { ?>selected="selected"<?php } ?> value="<?php echo $objAbonnement->IdAbonnement; ?>" title="<?php echo htmlentities($objAbonnement->Description); ?>"><?php echo htmlentities($objAbonnement->Nom); ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>-->
                            
                            <?php 
							if(sizeof($colAbonnements)) { 
								 foreach($colAbonnements as $objAbonnement) { 
								 	if(isset($objAssCommercantAbonnement[0]->IdAbonnement) && $objAssCommercantAbonnement[0]->IdAbonnement == $objAbonnement->IdAbonnement) {
											echo htmlentities($objAbonnement->Nom);
										}
								 }
							}
							?>
                            
                        </td>
                    </tr>
                    
                    <?php //echo $objAbonnement->IdAbonnement; ?>
                    <?php if($objAbonnement->IdAbonnement == 1) { ?>
                        <tr>
                            <td width="300"></td>
                            <td align="left"><input type="submit" value="Modifier" /></td>
                        </tr>
                    <?php } ?>
                    
                    <?php //} ?>
            </table>
            </fieldset>
			</div>
            <?php if($objAbonnement->IdAbonnement != 1) { ?>
			<div style="clear:both; padding:10px ">
                <fieldset>
                    <legend><div style="width:300px;font-size:20px;background-color:#123;color:#fff">Vos autres donn&eacute;es</div></legend>
                    <table width="100%" cellpadding="2" cellspacing="2" style="float:left; text-align:left;" align="left">
                        
                        <tr>
                            <td>
                                <label class="label">Couleur de fond du bandeau : </label>
                          </td>
                            <td>
                            
                                <link rel="stylesheet" href="<?php echo GetJsPath("front/") ; ?>/colorPicker/colorpicker.css" type="text/css" />
                                <!--<link rel="stylesheet" media="screen" type="text/css" href="<?php// echo GetJsPath("front/") ; ?>/colorPicker/layout.css" />-->
                                <style type="text/css">
								#colorSelector2 {
									position: relative;
									width: 36px;
									height: 36px;
									/*background: url(<?php// echo GetJsPath("front/") ; ?>/colorPicker/select2.png);*/
								}
								#colorSelector2 div {
									position: relative;
									width: 28px;
									height: 28px;
									background: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/select2.png) center;
								}
								#colorpickerHolder2 {
									width: 356px;
									height: 0;
									overflow: hidden;
									position: relative;
								}
								#colorpickerHolder2 .colorpicker {
									background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_background.png);
									position: relative;
									bottom: 0;
									left: 0;
								}
								#colorpickerHolder2 .colorpicker_hue div {
									background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_indic.gif);
								}
								#colorpickerHolder2 .colorpicker_hex {
									background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_hex.png);
								}
								#colorpickerHolder2 .colorpicker_rgb_r {
									background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_rgb_r.png);
								}
								#colorpickerHolder2 .colorpicker_rgb_g {
									background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_rgb_g.png);
								}
								#colorpickerHolder2 .colorpicker_rgb_b {
									background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_rgb_b.png);
								}
								#colorpickerHolder2 .colorpicker_hsb_s {
									background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_hsb_s.png);
									display: none;
								}
								#colorpickerHolder2 .colorpicker_hsb_h {
									background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_hsb_h.png);
									display: none;
								}
								#colorpickerHolder2 .colorpicker_hsb_b {
									background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_hsb_b.png);
									display: none;
								}
								#colorpickerHolder2 .colorpicker_submit {
									background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_submit.png);
								}
								#colorpickerHolder2 .colorpicker input {
									color: #778398;
								}
								#customWidget {
									position: relative;
									height: 36px;
								}

								</style>
                                <script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/colorPicker/colorpicker.js"></script>
                                <script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/colorPicker/eye.js"></script>
                                <script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/colorPicker/layout.js?ver=1.0.2"></script>
                            
                                <input type="hidden" name="Societe[bandeau_color]" id="bandeau_colorSociete" value="<?php if(isset($objCommercant->bandeau_color)) echo $objCommercant->bandeau_color; else echo '#004DA3'; ?>" />
                                <div id="colorSelector2"><div style="background-color: <?php if(isset($objCommercant->bandeau_color)) echo $objCommercant->bandeau_color; else echo '#004DA3'; ?>"></div></div>
                                <div id="colorpickerHolder2">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td width="300">
                                <label class="label">Photo du bandeau du haut (900x420) : </label>
                                  <input type="hidden" name="pdfAssocie" id="pdfAssocie" value="<?php if(isset($objCommercant->Pdf)) echo htmlspecialchars($objCommercant->Pdf); ?>" />
                                  <input type="hidden" name="photologoAssocie" id="photologoAssocie" value="<?php if(isset($objCommercant->Logo)) echo htmlspecialchars($objCommercant->Logo); ?>" />                                  
                                  <input type="hidden" name="photo1Associe" id="photo1Associe" value="<?php if(isset($objCommercant->Photo1)) echo htmlspecialchars($objCommercant->Photo1); ?>" />
                                  <input type="hidden" name="photo2Associe" id="photo2Associe" value="<?php if(isset($objCommercant->Photo2)) echo htmlspecialchars($objCommercant->Photo2); ?>" />
                                  <input type="hidden" name="photo3Associe" id="photo3Associe" value="<?php if(isset($objCommercant->Photo3)) echo htmlspecialchars($objCommercant->Photo3); ?>" />
                                  <input type="hidden" name="photo4Associe" id="photo4Associe" value="<?php if(isset($objCommercant->Photo4)) echo htmlspecialchars($objCommercant->Photo4); ?>" />
                                  <input type="hidden" name="photo5Associe" id="photo5Associe" value="<?php if(isset($objCommercant->Photo5)) echo htmlspecialchars($objCommercant->Photo5); ?>" />
                                  <input type="hidden" name="bandeau_topAssocie" id="bandeau_topAssocie" value="<?php if(isset($objCommercant->bandeau_top)) echo htmlspecialchars($objCommercant->bandeau_top); ?>" />
                          </td>
                            <td>
                                <?php if(!empty($objCommercant->bandeau_top)) { ?>
                                    <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->bandeau_top, 100, 100,'',''); ?>
                                    <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','bandeau_top');">Supprimer</a>
                                <?php } ?><br/>
                                <input type="file" name="Societebandeau_top" id="bandeau_topSociete" value="" />
                            </td>
                        </tr>
                        <tr>
                            <td width="300">
                                <label class="label">Photo 1 : </label>
                          </td>
                            <td>
                                <?php if(!empty($objCommercant->Photo1)) { ?>
                                    <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->Photo1, 100, 100,'',''); ?>
                                    <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Photo1');">Supprimer</a>
                                <?php } ?><br/>
                                <input type="file" name="SocietePhoto1" id="Photo1Societe" value="" />
                            </td>
                        </tr>
                        <tr>
                            <td width="300">
                                <label class="label">Photo 2 : </label>
                          </td>
                            <td>
                                <?php if(!empty($objCommercant->Photo2)) { ?>
                                    <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->Photo2, 100, 100,'',''); ?>
                                    <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Photo2');">Supprimer</a>
                                <?php } ?><br/>
                                <input type="file" name="SocietePhoto2" id="Photo2Societe" value="" />
                            </td>
                        </tr>
                        <tr>
                            <td width="300">
                                <label class="label">Photo 3 : </label>
                          </td>
                            <td>
                                <?php if(!empty($objCommercant->Photo3)) { ?>
                                    <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->Photo3, 100, 100,'',''); ?>
                                    <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Photo3');">Supprimer</a>
                                <?php } ?><br/>
                                <input type="file" name="SocietePhoto3" id="Photo3Societe" value="" />
                            </td>
                        </tr>
                        <tr>
                            <td width="300">
                                <label class="label">Photo 4 : </label>
                          </td>
                            <td>
                                <?php if(!empty($objCommercant->Photo4)) { ?>
                                    <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->Photo4, 100, 100,'',''); ?>
                                    <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Photo4');">Supprimer</a>
                                <?php } ?><br/>
                                <input type="file" name="SocietePhoto4" id="Photo4Societe" value="" />
                            </td>
                        </tr>
                        <tr>
                            <td width="300">
                                <label class="label">Photo 5 : </label>
                          </td>
                            <td>
                                <?php if(!empty($objCommercant->Photo5)) { ?>
                                    <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->Photo5, 100, 100,'',''); ?>
                                    <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Photo5');">Supprimer</a>
                                <?php } ?><br/>
                                <input type="file" name="SocietePhoto5" id="Photo5Societe" value="" />
                            </td>
                        </tr>
                        <tr>
                            <td width="300">
                                <label class="label">Lien vers une vid&eacute;o : </label>
                          </td>
                            <td>
                                <input type="text" name="Societe[Video]" id="VideoSociete" value="<?php if(isset($objCommercant->Video)) echo htmlspecialchars($objCommercant->Video); ?>" /><span style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00;">&nbsp;&nbsp;&nbsp;Format : http://www.youtube.com/watch?v=gvGymDhY49E </span>
                            </td>
                        </tr>
                        <tr>
                            <td width="300">
                                <label class="label">Horaires d'ouvertures : </label>
                          </td>
                            <td>
                                <textarea  name="Societe[Horaires]" id="HorairesSociete" ><?php if(isset($objCommercant->Horaires)) echo htmlspecialchars($objCommercant->Horaires); ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td width="300">
                                <label class="label">Fermeture : </label>
                          </td>
                            <td>
                                <textarea  name="Societe[Vacances]" id="VacancesSociete" ><?php if(isset($objCommercant->Vacances)) echo htmlspecialchars($objCommercant->Vacances); ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td width="300">
                                <label class="label">Description longue : </label>
                          </td>
                            <td height="200">
                                <textarea name="Societe[Caracteristiques]" id="CaracteristiquesSociete" ><?php if(isset($objCommercant->Caracteristiques)) echo htmlspecialchars($objCommercant->Caracteristiques); ?></textarea>
                                <?php echo display_ckeditor($ckeditor0); ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="300">
                                <label class="label">Lien Facebook : </label>
                          </td>
                            <td>
                                <input type="text" name="Societe[Facebook]" id="FacebookSociete" value="<?php if(isset($objCommercant->Facebook)) echo htmlspecialchars($objCommercant->Facebook); ?>" />
                            </td>
                        </tr>
                        <tr valign="middle">
                            <td width="300" height="35">
                                <label class="label">Lien Google + : </label>
                          </td>
                            <td width="300" height="35">
                                <input type="text" name="Societe[Twitter]" id="TwitterSociete" value="<?php if(isset($objCommercant->Twitter)) echo htmlspecialchars($objCommercant->Twitter); ?>" />
                          </td>
                        </tr>
                        <tr valign="middle">
                            <td width="300"valign="top">
                                <label class="label">T&eacute;l&eacute;chargement d’une plaquette commerciale PDF : </label>
                          </td>
                            <td  valign="top">
                                <?php if(!empty($objCommercant->Pdf)) { ?>
                                    <?php //echo $objCommercant->Pdf; ?>
                                    
                                    <?php if ($objCommercant->Pdf != "" && $objCommercant->Pdf != NULL && file_exists("application/resources/front/photoCommercant/images/".$objCommercant->Pdf)== true) {?><a href="<?php echo base_url(); ?>application/resources/front/photoCommercant/images/<?php echo $objCommercant->Pdf ; ?>" target="_blank"><img src="<?php echo base_url(); ?>application/resources/front/images/pdf_icone.png" width="64" alt="PDF" /></a><?php } ?>
                                    
                                    
                                    <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Pdf');">Supprimer</a><br/>
                                
                                <input type="file" name="SocietePdf" id="PdfSociete" value="" style="display:none;" />
                            <?php } else { ?>
                                <input type="file" name="SocietePdf" id="PdfSociete" value="" />
                            <?php } ?>
                          </td>
                      </tr>
                        <tr>
                            <td width="300">
                                <label class="label">Conditions de fid&eacute;lisation : </label>
                          </td>
                            <td>
                                <textarea  name="Societe[Conditions]" id="ConditionsSociete" style="width:400px;"><?php if(isset($objCommercant->Conditions)) echo htmlspecialchars($objCommercant->Conditions); ?></textarea>
                            </td>
                        </tr>
						<tr>
                            <td width="300">
                                <label class="label">Moyens et conditions de paiement : </label>
                          </td>
                            <td>
                                <textarea  name="Societe[Conditions_paiement]" id="Conditions_paiement" style="width:400px;"><?php if(isset($objCommercant->Conditions_paiement)) echo htmlspecialchars($objCommercant->Conditions_paiement); ?></textarea>
                            </td>
                        </tr>
						<tr>
                            <td width="300">
                                <label class="label">Site Web : </label>
                          </td>
                            <td>
                                <textarea  name="Societe[SiteWeb]" id="SiteWeb" style="width:400px; height:30px;"><?php if(isset($objCommercant->SiteWeb)) echo htmlspecialchars($objCommercant->SiteWeb); ?></textarea>
                            </td>
                    </tr>
                    
                    	<tr>
                            <td width="300">
                                <label class="label">Mots cl&eacute;s :<br />(&agrave; s&eacute;parer par des virgules) </label>
                          </td>
                            <td>
                                <textarea  name="Societe[metatag]" id="metatag" style="width:400px; height:150px;"><?php if(isset($objCommercant->metatag)) echo htmlspecialchars($objCommercant->metatag); ?></textarea>
                            </td>
                    </tr>
                      
                        
                        <tr>
                            <td width="300"></td>
                            <td align="left"><input type="button" class="btnSinscrire" value="Modifier" /></td>
                        </tr>
                </table>
                </fieldset>
                </div>
            <?php } ?>
        </form>
    </div>
<?php $this->load->view("front/includes/vwFooter_nv"); ?>