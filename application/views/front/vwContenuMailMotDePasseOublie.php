
<table cellpadding="5" cellspacing="5" style="font-family:Arial, Helvetica, sans-serif;">
	<tr>
		<td>
			Bonjour
		</td>
	</tr>
    <tr>
		<td>
			Vous nous avez demand&eacute; de vous rappeler votre mot de passe.
		</td>
	</tr>
    <tr>
		<td>
			Cette demande nous est parvenue le <?php echo date_french();?> &agrave; <?php echo date('H:i:s');?>
		</td>
	</tr>
    <tr>
		<td>
			Votre compte :
		</td>
	</tr>
	<tr>
		<td>
                    
            <table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><label>Login : </label></td>
                <td>&nbsp;<?php echo $zLogin ; ?></td>
              </tr>
              <tr>
                <td><label>Mot de passe : </label></td>
                <td>&nbsp;<?php echo $zMotDePasse ; ?></td>
              </tr>
            </table>
            
		</td>
	</tr>
	
	<tr>
		<td>
			Cordialement<br />
            L'&eacute;quipe du Club-Proximit&eacute;
		</td>
	</tr>
	
</table>