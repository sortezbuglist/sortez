<?php $data["zTitle"] = 'Acces compte' ?>
<?php $this->load->view("front/includes/vwHeader", $data); ?>
<?php $this->load->view("front/includes/vwMenu"); ?>
<?php
$userType ="";
if(isset($GLOBALS["gCurrentUser"])){
  $userType = $GLOBALS["gCurrentUser"]->UserType() ;  
 }
// echo $userType;exit();
?>

    <div id="divMenuProfessionnel">
            <div id="divMenuProfessionnelLeft">
                <h1>
                    Vous &ecirc;tes <br/>un particulier ?
                    <img src="<?php echo GetImagePath("front/"); ?>/wpc014a069.png" />
                </h1>
                <h3>Je suis un particulier et je d&eacute;sire b&eacute;n&eacute;ficier des &laquo; Bons Plans &raquo;  et &laquo; Points Cadeaux &raquo;conc&eacute;d&eacute;s par les partenaires du Club.</h3>
                <ul>
                    <li>
                        <a href="<?php echo site_url("front/particuliers/fonctionnement"); ?>">
                            Le fonctionnement des &laquo; Bons Plans &raquo;, Points Cadeaux &raquo; et  annonces professionnelles
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url("front/conditionsgenerales/"); ?>">
                            Les conditions g&eacute;n&eacute;rales
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url("front/particuliers/inscription/") ?>">
                            Je m'inscris en ligne
                        </a>
                    </li>
                    <li>
                        <a href="<?php  if($userType == '0') { echo site_url("front/particuliers/inscription/" . $GLOBALS["gCurrentUser"]->UserId());  } else { echo'#' ; }?> ">
                            J'acc&egrave;de &agrave; mon compte
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url("connexion/") ?>">
                            Je m'identifie pour acc&eacute;der aux offres
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url("front/fidelisation/") ?>">
                            Je valide les points cadeaux
                        </a>
                    </li>
                </ul>
            </div>
            
            <div id="divMenuProfessionnelRight">
                <h1>
                    Vous &ecirc;tes <br/>professionnel ?
                    <img src="<?php echo GetImagePath("front/"); ?>/wp2ce7c608.png" />
                </h1>
                <h3>Je suis un professionnel et je d&eacute;sire adh&eacute;rer &agrave; l’annuaire de base ou en qualit&eacute; de Partenaire du Club Proximit&eacute;.</h3>
                <ul>
                    <li>
                        <a href="<?php echo site_url("front/menuprofessionnels/fonctionnement"); ?>">
                            Le fonctionnement des &laquo; Bons Plans &raquo;, Points Cadeaux &raquo; et  annonces professionnelles
                        </a>
                    </li>
                    <li>
                         <a href="<?php echo site_url("front/conditionsgenerales/"); ?>">
                            Les conditions g&eacute;n&eacute;rales
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url("front/menuprofessionnels/abonnements"); ?>">
                            Les diff&eacute;rents abonnements (Informations)
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url("front/professionnels/inscription"); ?>">
                            Je m'inscris
                        </a>
                    </li>
                    <?php if($GLOBALS["gCurrentUser"]->IsConnected() && $userType == 'COMMERCANT') { ?>
                        <li>
                            <a href="<?php  if($userType == 'COMMERCANT') { echo site_url("front/professionnels/fiche/" . $GLOBALS["gCurrentUser"]->UserId());  } else { echo site_url("connexion") ; }?> ">
                                J'acc&egrave;de &agrave; mon compte
                            </a>
                        </li>
                    <?php } elseif(!$GLOBALS["gCurrentUser"]->IsConnected()) { ?>
                        <li>
                            <a href="<?php echo site_url("connexion"); ?>">
                                J'acc&egrave;de &agrave; mon compte
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            
        </div>
    </div>
<?php $this->load->view("front/includes/vwFooter"); ?>