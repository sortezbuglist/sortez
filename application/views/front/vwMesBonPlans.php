<?php $data["zTitle"] = 'Liste de mes Bon plans'; ?>
<?php $this->load->view("front/includes/vwHeader", $data); ?>
<?php $this->load->view("front/includes/vwMenu"); ?>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
				oTable = $('#iDTablebnplan').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"aLengthMenu": [],
					"iDisplayLength": 2
				});
			} );
	
</script>
    <div id="divMesBnplan" class="content" align="center">
        <form name="frmMesAnnonces" id="frmMesAnnonces" action="" method="POST" enctype="multipart/form-data">
		    <br>
		    <span align="left"><a href="<?php echo site_url("front/bonplan/ficheBonplan/$idCommercant") ;?>"><input type = "button" id = "btnnew" value ="Ajouter un nouveau" /></a></span>
				<br><span align="left"><a href="<?php echo site_url("front/menuprofessionnels") ;?>"><input type = "button" id = "btnnew" value ="retour au menu" /></a></span>
            <h1>Liste de mes Bon plans</h1>
				
				<div id="container">
					<table cellpadding="0" cellspacing="0" border="0" class="display" id="iDTablebnplan">
						<thead>
							<tr>
								<th>Titre</th>
								<th>Description</th>
								<th>Categorie</th>
								<th>Ville</th>
								<th>Nbr pris</th>
								<th>Date debut</th>
								<th>Date fin</th>
								<th>&nbsp;</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody> 
							<?php $iX = 0 ; foreach($toListeMesBonplans as $oListeMesBonplans){ ?>
								<tr class="<?php if ($iX = 0){ echo "gradeX"; }else{ echo "gradeA"; }?>">                    
									<td><?php echo $oListeMesBonplans->bonplan_titre ; ?></td>
									<td><?php echo $oListeMesBonplans->bonplan_texte ; ?></td>
									<td><?php echo $oListeMesBonplans->categorie_nom ; ?></td>
									<td><?php echo $oListeMesBonplans->Nom  ; ?></td>
									<td class="center"><?php echo $oListeMesBonplans->bonplan_nombrepris  ; ?></td>
									<td class="center"><?php echo $oListeMesBonplans->bonplan_date_debut ; ?></td>
									<td class="center"><?php echo $oListeMesBonplans->bonplan_date_fin ; ?></td>
									<td><a href="<?php echo site_url("front/bonplan/ficheBonplan/" . $oListeMesBonplans->bonplan_commercant_id . "/" . $oListeMesBonplans->bonplan_id ) ; ?>">Modifier</a></td>
									<td><a href="<?php echo site_url("front/bonplan/supprimBonplan/" . $oListeMesBonplans->bonplan_id . "/" . $oListeMesBonplans->bonplan_commercant_id) ; ?>" onclick="if (!confirm('voulez-vous vraiment supprimer cette annoce?')){ return false ; }">Supprimer</a></td>
								</tr>
							<?php $iX ++ ; } ?>
						</tbody>
					</table>
				</div>
        </form>
    </div>
<?php $this->load->view("front/includes/vwFooter"); ?>