<?php $data["zTitle"] = 'Accueil' ?>

<?php 
//$this->load->model("mdlville") ;
//$commercant_url_ville = $this->mdlville->getVilleById($oInfoCommercant->IdVille);
//$commercant_url_nom_com = RemoveExtendedChars2($oInfoCommercant->NomSociete);
//$commercant_url_nom_ville = RemoveExtendedChars2($commercant_url_ville->Nom);
//$commercant_url_nom = $commercant_url_nom_com."_".$commercant_url_nom_ville;
$commercant_url_nom = $oInfoCommercant->nom_url;
$commercant_url_home = $commercant_url_nom;
$commercant_url_presentation = $commercant_url_nom."/presentation";
$commercant_url_infos = $commercant_url_nom."/infos";
$commercant_url_autresinfos = $commercant_url_nom."/autresinfos";
$commercant_url_annonces = $commercant_url_nom."/annonces";
$commercant_url_notre_bonplan = $commercant_url_nom."/notre_bonplan";
$commercant_url_noussituer = $commercant_url_nom."/nous_situer";
$commercant_url_nous_contacter = $commercant_url_nom."/nous_contacter";

$commercant_url_photos = $commercant_url_nom."/photos";
$commercant_url_video = $commercant_url_nom."/video";
$commercant_url_menu_mobile = $commercant_url_nom."/menu_mobile";
$commercant_url_mentions_legales = $commercant_url_nom."/mentions_legales";
$commercant_url_coordonnees_horaires = $commercant_url_nom."/coordonnees_horaires";

?>

<?php if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) { 

?>

<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN" "http://www.openmobilealliance.org/tech/DTD/xhtml-mobile11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="Generator" content="Serif WebPlus X5">
<meta http-equiv="X-UA-Compatible" content="IE=EmulationIE8">
<title>Menu mobile</title>
<meta content="minimum-scale=1.0, width=device-width" name="viewport">
 <!--Master Page Head-->
<style type="text/css">
body {margin: 0px; padding: 0px;}
.Corps-artistique-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:left; font-weight:400;
}
.Corps-P
{
    margin:0.0px 0.0px 4.0px 0.0px; text-align:center; font-weight:400;
}
.Corps-artistique-C
{
    font-family:"Arial", sans-serif; font-weight:700; color:#3653c1; font-size:19.0px; line-height:1.21em;
}
.Corps-artistique-C-C0
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:13.0px; line-height:1.23em;
}
.Corps-artistique-C-C1
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:12.0px; line-height:1.25em;
}
.Corps-artistique-C-C2
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:16.0px; line-height:1.25em;
}
.Corps-artistique-C-C3
{
    font-family:"Arial", sans-serif; font-size:15.0px; line-height:1.13em;
}
.Corps-artistique-C-C4
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:15.0px; line-height:1.20em;
}
.Normal-C
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:12.0px; line-height:1.25em;
}
.Corps-C
{
    font-family:"Arial", sans-serif; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C0
{
    font-family:"Arial", sans-serif; text-decoration:underline; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C1
{
    font-family:"Arial", sans-serif; text-decoration:underline; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C2
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
.Corps-P-P1
{
    margin:0.0px 0.0px 4.0px 0.0px; text-align:center; font-weight:400;
}
.Corps-C-C3
{
    font-family:"Arial", sans-serif; text-decoration:underline; font-size:13.0px; line-height:1.23em;
}
.Button1,.Button1:link,.Button1:visited{background-position:0px 0px;text-decoration:none;display:block;position:absolute;background-image:url(<?php echo GetImagePath("front/"); ?>/wpe3ad0087_06.png);}
.Button1:focus{outline-style:none;}
.Button1:hover{background-position:0px -70px;}
.Button1:active{background-position:0px -35px;}
.Button1 span,.Button1:link span,.Button1:visited span{color:#333333;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:left;text-transform:none;font-style:normal;left:39px;top:38px;width:218px;height:22px;font-size:17px;display:block;position:absolute;cursor:pointer;}
.Button1:hover span{color:#ffffff;}
</style>
<script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/jspngfix.js"></script>
<link rel="stylesheet" href="menu_fichiers/wpstyles.css" type="text/css">
<script type="text/javascript">var blankSrc = "wpscripts/blank.gif";
</script>
</head>

<body style="background-color: transparent; text-align: center; height: 2000px;" text="#000000">
<div style="background-color:#cccccc;text-align:left;margin-left:auto;margin-right:auto;position:relative;width:320px;height:2000px;">

<!--start main info menu-->
<?php $data["zTitle"] = 'Accueil' ;?>
<?php $this->load->view("front/vwTitreinfocommercantMobile", $data);?>
<!--end main info menu-->

<img src="<?php echo GetImagePath("front/"); ?>/wp51f75006_06.png" id="qs_1664" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 0px; top: 95px;" height="5" border="0" width="320">

<!--start main img-->
<?php $this->load->view("front/vwSlidePartenaireMobile", $data);?>
<!--end main img-->



<!--start main icone menu-->
<?php $this->load->view("front/vwMenumainiconeMobile", $data);?>
<!--end main icone menu-->


<!--start main logo mobile-->
<?php $this->load->view("front/vwMainLogoMobile", $data);?>
<!--end main logo mobile-->


<img src="<?php echo GetImagePath("front/"); ?>/wp2aebdbd9_06.png" id="qs_1180" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 15px; top: 455px;" height="53" border="0" width="290">
<div id="art_277" style="position:absolute;left:28px;top:465px;width:120px;height:21px;">
    <div class="Corps-artistique-P">
        <span class="Corps-artistique-C-C2">Notre activité</span></div>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wp4f089b98_06.png" id="qs_1183" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 15px; top: 455px;" height="296" border="0" width="290">


<!--btn coordonnees-->
<a href="<?php echo site_url($commercant_url_coordonnees_horaires);?>" id="btn_127" class="Button1" style="position: absolute; left: 28px; top: 504px; width: 263px; height: 35px;"><span></span></a>
<div id="art_186" style="position:absolute;left:79px;top:514px;width:181px;height:18px;">
    <div class="Corps-artistique-P">
        <span class="Corps-artistique-C-C3"><a href="<?php echo site_url($commercant_url_coordonnees_horaires);?>" style="color: rgb(0, 0, 0); text-decoration: none;">Coordonnées et horaires</a></span></div>
</div>
<!--btn coordonnees-->


<!--btn presentation-->
<a href="<?php echo site_url($commercant_url_presentation);?>" id="btn_128" class="Button1" style="position: absolute; left: 28px; top: 549px; width: 263px; height: 35px;"><span></span></a>
<div id="art_1203" style="position:absolute;left:81px;top:558px;width:177px;height:18px;">
    <div class="Corps-artistique-P">
        <span class="Corps-artistique-C-C3"><a href="<?php echo site_url($commercant_url_presentation);?>" style="color: rgb(0, 0, 0); text-decoration: none;">Présentation de l’activité</a></span></div>
</div>
<!--btn presentation-->



<!--btn activite 1-->
<?php if ($oInfoCommercant->labelactivite1 != "" && $oInfoCommercant->labelactivite1 != NULL) { ?>
<a href="<?php echo site_url($commercant_url_infos);?>" id="btn_129" class="Button1" style="position: absolute; left: 28px; top: 595px; width: 263px; height: 35px;"><span></span></a>
<div id="art_1204" style="position:absolute; left:88px; top:603px; width:179px; height:18px; text-align:center;">
<div class="Corps-artistique-P">
        <span class="Corps-artistique-C-C3"><a href="<?php echo site_url($commercant_url_infos);?>" style="color: rgb(0, 0, 0); text-decoration: none;"><?php echo $oInfoCommercant->labelactivite1; ?></a></span></div>
</div>
<?php } ?>
<!--btn activite 1-->


<!--btn activite 2-->
<?php if ($oInfoCommercant->labelactivite2 != "" && $oInfoCommercant->labelactivite2 != NULL) { ?>
<a href="<?php echo site_url($commercant_url_autresinfos);?>" id="btn_130" class="Button1" style="position: absolute; left: 28px; top: 643px; width: 263px; height: 35px;"><span></span></a>
<div id="art_1206" style="position:absolute; left:89px; top:652px; width:179px; height:18px;">
<div class="Corps-artistique-P">
        <span class="Corps-artistique-C-C3"><a href="<?php echo site_url($commercant_url_autresinfos);?>" style="color: rgb(0, 0, 0); text-decoration: none;"><?php echo $oInfoCommercant->labelactivite2; ?></a></span></div>
</div>
<?php } ?>
<!--btn activite 2-->


<!--btn mention legale-->
<a href="<?php echo site_url($commercant_url_mentions_legales);?>" id="btn_142" class="Button1" style="position: absolute; left: 28px; top: 693px; width: 263px; height: 35px;"><span></span></a>
<div id="art_1220" style="position:absolute; left:87px; top:703px; width:139px; height:18px;">
    <div class="Corps-artistique-P">
        <span class="Corps-artistique-C-C3"><a href="<?php echo site_url($commercant_url_mentions_legales);?>" style="color: rgb(0, 0, 0); text-decoration: none;">Mentions légales</a></span></div>
</div>
<!--btn mention legale-->



<!--<img src="<?php// echo GetImagePath("front/"); ?>/wp8a638844_06.png" id="qs_872" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 15px; top: 969px;" height="156" border="0" width="290">
<div id="art_151" style="position:absolute;left:28px;top:981px;width:128px;height:19px;">
<div class="Corps-artistique-P">
        <span class="Corps-artistique-C-C4"><a href="<?php// echo site_url("front/commercant/listeBonPlanParCommercant/");?>/<?php// echo $oInfoCommercant->IdCommercant ; ?>" style="color: rgb(255, 255, 255); text-decoration: none;">Notre Bon Plan</a></span></div>
</div>-->
<!--<map id="map1" name="map1">
    <area shape="poly" coords="290,37,290,12,285,4,278,0,12,0,4,5,0,13,0,38,1,38,1,13,6,5,13,1,277,1,285,6,289,14,289,38" href="http://www.proximite-magazine.com/clubnv/bonplanmobile.html" alt="">
    <area shape="poly" coords="290,38,289,38,289,134,290,135" href="http://www.proximite-magazine.com/clubnv/bonplanmobile.html" alt="">
    <area shape="poly" coords="1,134,1,38,0,38,0,135" href="http://www.proximite-magazine.com/clubnv/bonplanmobile.html" alt="">
    <area shape="poly" coords="279,173,278,173,277,174,279,174" href="http://www.proximite-magazine.com/clubnv/bonplanmobile.html" alt="">
    <area shape="poly" coords="12,173,11,173,11,174,13,174" href="http://www.proximite-magazine.com/clubnv/bonplanmobile.html" alt="">
    <area shape="poly" coords="282,172,280,172,280,173,281,173" href="http://www.proximite-magazine.com/clubnv/bonplanmobile.html" alt="">
    <area shape="poly" coords="10,172,8,172,9,173,10,173" href="http://www.proximite-magazine.com/clubnv/bonplanmobile.html" alt="">
    <area shape="poly" coords="8,171,7,171,6,170,4,168,3,167,3,168,4,169,6,171,7,172" href="http://www.proximite-magazine.com/clubnv/bonplanmobile.html" alt="">
    <area shape="poly" coords="3,166,2,165,2,163,1,162,1,134,0,135,0,163,1,164,1,165,2,166,3,167" href="http://www.proximite-magazine.com/clubnv/bonplanmobile.html" alt="">
    <area shape="poly" coords="283,171,285,170,287,168,289,165,290,163,290,135,289,134,289,162,288,164,287,166,285,169,283,171,282,172" href="http://www.proximite-magazine.com/clubnv/bonplanmobile.html" alt="">
</map>



<map id="map2" name="map2">
    <area shape="poly" coords="293,17,293,13,292,12,292,10,291,10,291,12,292,13,292,36,293,36" href="http://www.proximite-magazine.com/clubnv/annoncesmobile.html" alt="">
    <area shape="poly" coords="291,9,290,8,289,7,288,6,289,7,290,8,290,10" href="http://www.proximite-magazine.com/clubnv/annoncesmobile.html" alt="">
    <area shape="poly" coords="4,17,4,13,5,12,5,10,6,9,7,8,7,7,8,6,7,7,5,9,4,10,4,12,3,13,3,36,4,36" href="http://www.proximite-magazine.com/clubnv/annoncesmobile.html" alt="">
    <area shape="poly" coords="288,5,287,5,287,4,286,4,287,5,287,6,288,6" href="http://www.proximite-magazine.com/clubnv/annoncesmobile.html" alt="">
    <area shape="poly" coords="285,3,285,4,286,4" href="http://www.proximite-magazine.com/clubnv/annoncesmobile.html" alt="">
    <area shape="poly" coords="9,5,10,4,11,3,10,4,9,5,8,6" href="http://www.proximite-magazine.com/clubnv/annoncesmobile.html" alt="">
    <area shape="rect" coords="283,2,285,4" href="http://www.proximite-magazine.com/clubnv/annoncesmobile.html" alt="">
    <area shape="rect" coords="12,2,14,4" href="http://www.proximite-magazine.com/clubnv/annoncesmobile.html" alt="">
    <area shape="poly" coords="281,1,280,1,281,2,282,2" href="http://www.proximite-magazine.com/clubnv/annoncesmobile.html" alt="">
    <area shape="poly" coords="16,1,15,1,14,2,15,2" href="http://www.proximite-magazine.com/clubnv/annoncesmobile.html" alt="">
    <area shape="poly" coords="291,165,290,165,290,166" href="http://www.proximite-magazine.com/clubnv/annoncesmobile.html" alt="">
    <area shape="poly" coords="292,163,292,162,293,161,293,160,292,160,292,162,291,163,291,164,292,164" href="http://www.proximite-magazine.com/clubnv/annoncesmobile.html" alt="">
    <area shape="rect" coords="292,36,294,161" href="http://www.proximite-magazine.com/clubnv/annoncesmobile.html" alt="">
    <area shape="poly" coords="4,159,4,36,1,36,1,160,4,160" href="http://www.proximite-magazine.com/clubnv/annoncesmobile.html" alt="">
    <area shape="poly" coords="281,173,292,158,292,13,280,1,16,1,4,17,4,162,16,174,280,174" href="<?php// echo site_url("front/annonce/menuannonceCommercant/");?>/<?php// echo $oInfoCommercant->IdCommercant ; ?>" alt="">
</map>-->


<div style="position:absolute;left:12px;top:775px;width:293px;height:auto;">
	<!--affichage annnonce-->
    <?php if ($nombre_annonce_com != 0) { ?>
<div style="width:293px;height:175px; background-image:url(<?php echo GetImagePath("front/"); ?>/wp926e5481_06.png); background-repeat:no-repeat; background-position:left;">
        <div class="Corps-artistique-P">
            <span style="margin-left:20px; margin-top:0px; font-family:Arial, Helvetica, sans-serif; color:#FFF;font-size: 15px;font-weight: 700;line-height: 2.5em;"><a href="<?php echo site_url($commercant_url_annonces);?>" style=" color:#FFF; text-decoration: none;">Nos annonces (<?php echo $nombre_annonce_com; ?>)</a></span>
            </div>
            <div style="height:100px;"><center><a href="<?php echo site_url($commercant_url_annonces);?>" style=" color:#FFF; text-decoration: none;"><img src="<?php echo GetImagePath("front/"); ?>/noneimage.png" width="290" height="125" alt="slide" title=""></a></center></div>
    </div>
    <div style=" width:293px; height:20px;"></div>
    <?php } ?>
	
    
    <?php if (isset($oLastbonplanCom)) {?>
<div style="height: 125px;    left: 15px;    padding: 45px 15px 30px 115px;  width: 160px; background-image:url(<?php echo GetImagePath("front/"); ?>/wp8a638844_06_mbl.png); background-repeat:no-repeat;">
<table width="100%" height="123" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="middle">
    <a href="<?php echo site_url($commercant_url_notre_bonplan);?>" style=" font-family: Arial, Helvetica, sans-serif; color:#FFF; font-weight:bold; text-decoration:none;">
	<?php if (isset($oLastbonplanCom)) {
            echo truncate($oLastbonplanCom->bonplan_titre,80,"...");
           //echo '<span style="font-size:9px;">'.truncate($oLastbonplanCom->bonplan_texte,100,"...")."</span>";
        }?>
    </a> 
    </td>
  </tr>
</table>
    </div>
    <?php } ?>
    
    
    
	<div id="art_1114" style="height: 40px;    padding-left: 100px;    padding-top: 14px;    width: 193px; background-image:url(<?php echo GetImagePath("front/"); ?>/fond_ami.png);background-repeat: no-repeat; background-position:center;">
        <div class="Wp-Normal-P">
            <span class="Normal-C"><a href="mailto:<?php echo $oInfoCommercant->Email ; ?>" style="color: rgb(255, 255, 255); text-decoration: none;">Recommander à un ami</a></span>
        </div>
	</div>
    
    <div style=" width:293px; height:20px;"></div>
    
    <div id="art_1202" style="height: 40px;    padding-left: 100px;    padding-top: 14px;    width: 193px;background-image:url(<?php echo GetImagePath("front/"); ?>/fond_alert.png);background-repeat: no-repeat; background-position:center;">
        <div class="Wp-Normal-P">
            <span class="Normal-C"><a href="<?php 
                  if ($GLOBALS["gCurrentUser"]->UserId()) {
                      echo site_url("front/alertes/add/" . $GLOBALS["gCurrentUser"]->UserId() . "/" . $oInfoCommercant->IdCommercant);
                  } else echo 'Javascript:void();';
                  ?>" style="color: rgb(255, 255, 255); text-decoration: none;">Créez une alerte</a></span>
         </div>
	</div>

	
    <div id="txt_632" style="position:width:293px;height: auto;overflow:hidden; text-align:center; font-size:13.0px;">
    <!--start main footer menu-->
    <?php $this->load->view("front/vwMenumainfooterMobile", $data);?>
    <!--end main footer menu-->
    </div>
    

</div>









</div>

</body></html>

<?php
} else { 


 
?>



<?php $this->load->view("front/includes/vwHeaderPartenaire", $data); ?>

<div id="main_body">
    <div id="main_left">
    	<?php $this->load->view("front/vwContactPartenaire", $data); ?> 
    </div>
    <div id="main_right">
      <?php $this->load->view("front/vwSlidePartenaire", $data); ?> 
      <div id="main_data">
        <div id="main_data_head"><?php echo $oInfoCommercant->NomSociete ; ?></div>
        <div id="main_data_body">
        <?php //if ($is_mobile == false) echo " wsdqfd<br/>"; else echo "is_mobile<br/>"; ?>
        <?php //if ($is_robot == false) echo " sdgdfs<br/>"; else echo "is_robot<br/>";?>
        <?php //if ($is_browser == false) echo " sdgdfs<br/>"; else echo "is_browser<br/>";?>
         <?php //echo $is_platform."<br/><br/><br/>";?>
        <br/><?php echo $oInfoCommercant->Caracteristiques; ?></div>
        <div id="main_data_footer"></div>
      </div>
    </div>
  </div>

<?php $this->load->view("front/includes/vwFooterPartenaire"); ?>

<?php 

}
?>