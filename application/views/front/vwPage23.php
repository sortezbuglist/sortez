<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="Generator" content="Serif WebPlus X4">
<title>club fonctionnement points</title>
<style type="text/css">
<!--
body {margin: 0px; padding: 0px;}
a:link {color: #3e550e;}
a:visited {color: #8e9165;}
a:hover {color: #3e550e;}
a:active {color: #3e550e;}
.Normal-P
        {
        margin:0.0px 0.0px 0.0px 0.0px; text-align:justify; font-weight:400; 
        }
.Corps-P
        {
        margin:0.0px 0.0px 12.0px 0.0px; text-align:left; font-weight:400; 
        }
.Normal-P0
        {
        margin:0.0px 0.0px 0.0px 0.0px; text-align:left; font-weight:400; 
        }
.Normal-C
        {
        font-family:"Arial", sans-serif; font-size:24.0px; line-height:1.17em; 
        color:#ffffff; 
        }
.Corps-C
        {
        font-family:"Verdana", sans-serif; font-size:16.0px; 
        line-height:1.13em; 
        }
.Normal-C0
        {
        font-family:"Arial", sans-serif; font-weight:700; font-size:29.0px; 
        line-height:1.21em; color:#ffffff; 
        }
.Normal-C1
        {
        font-family:"Arial", sans-serif; font-weight:700; font-size:27.0px; 
        line-height:1.22em; color:#ffffff; 
        }
-->
</style>
<script type="text/javascript" src="wpscripts/jspngfix.js"></script>
<script type="text/javascript">
var blankSrc = "wpscripts/blank.gif";
</script>
</head>

<body text="#000000" style="background-color:#000000; text-align:center; height:3000px;">
<div style="background-color:#000000;text-align:left;margin-left:auto;margin-right:auto;position:relative;width:800px;height:3000px;">
<div style="position:absolute; left:0px; top:0px; width:621px; height:567px;">
    <map id="map0" name="map0">
        <area shape="poly" coords="401,471,373,471,377,477,380,484,382,492,383,499,383,508,381,515,379,522,375,528,371,534,369,536,401,536" href="javascript:history.back()" alt="">
        <area shape="poly" coords="293,535,285,522,281,508,280,496,284,482,290,471,26,471,26,536,295,536" href="javascript:history.back()" alt="">
        <area shape="poly" coords="324,522,313,517,309,506,313,505,320,514,328,507,329,505,318,502,308,493,307,481,317,470,326,467,338,468,349,472,356,482,357,486,352,497,343,503,332,513" href="javascript:history.back()" alt="">
        <area shape="poly" coords="339,537,356,530,366,515,369,497,363,480,349,468,338,464,320,466,305,475,296,489,294,506,301,523,315,535,325,538" href="javascript:history.back()" alt="">
        <area shape="poly" coords="338,538,320,536,305,527,296,513,294,496,301,479,315,467,325,464,343,466,359,475,368,491,369,507,362,523,348,535" href="javascript:history.back()" alt="">
        <area shape="poly" coords="341,551,363,541,379,522,383,497,375,474,356,456,339,450,314,453,293,467,282,489,280,506,288,528,307,546,324,552" href="javascript:history.back()" alt="">
        <area shape="poly" coords="324,521,327,518,331,514,335,510,337,506,339,504,335,505,329,505,326,509,323,513,320,514,316,509,313,506,310,505,309,509,311,513,313,517,316,520,319,522,324,522" href="javascript:history.back()" alt="">
        <area shape="poly" coords="336,504,344,502,352,497,356,489,357,486,354,477,346,470,338,467,326,467,317,470,310,476,307,485,309,494,316,500,323,504,328,505" href="javascript:history.back()" alt="">
    </map>
    <img src="<?php echo GetImagePath("front/"); ?>/wp820703ee.png" width="621" height="567" border="0" id="qs_234" name="qs_234" title="" alt="" onload="OnLoadPngFix()" usemap="#map0"></div>
<div id="txt_477" style="position:absolute; left:20px; top:1040px; width:760px; height:697px;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box; overflow:hidden;">
<p class="Normal-P"><span class="Normal-C">Avec les points cadeaux, Le club proximité aide ses partenaires à dynamiser leur
    activité et à fidéliser les consommateurs adhérents.</span></p>
<p class="Normal-P"><span class="Normal-C">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C">Le Club propose à ces derniers de gagner des point cadeaux à chaque concrétisation
    d’une offre “bon plan”. </span></p>
<p class="Normal-P"><span class="Normal-C">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C">Ces cadeaux sont regroupés au sein d&#39;un catalogue.</span></p>
<p class="Normal-P"><span class="Normal-C">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C">Le consommateur note le numéro du courriel qu’il a reçu lors de la confirmation de
    sa demande.</span></p>
<p class="Normal-P"><span class="Normal-C">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C">Lors de son achat chez le partenaire, il lui demande lui le numéro du courriel qu’il
    a reçu.</span></p>
<p class="Normal-P"><span class="Normal-C">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C">Muni de ces deux codes, le consommateur se connecte sur son compte, il les enregistre
    sur les champs correspondant, en validant notre système reconnaîtra les numéros et
    son compte sera crédité de 10 points.</span></p>
<p class="Normal-P"><span class="Normal-C">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C">Le consommateur cumule les points, et les transforme &nbsp;en cadeaux quand il le souhaite
    ... </span></p>
<p class="Corps-P"><span class="Corps-C">&nbsp;</span></p>
</div>
<div style="position:absolute; left:603px; top:385px; width:23px; height:19px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpc705ac5c.png" width="23" height="19" border="0" id="pcrv_155" name="pcrv_155" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:624px; top:383px; width:25px; height:23px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpfd08d9a4.png" width="25" height="23" border="0" id="pcrv_156" name="pcrv_156" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:648px; top:371px; width:24px; height:34px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wp5c1e3fcc.png" width="24" height="34" border="0" id="pcrv_157" name="pcrv_157" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:674px; top:383px; width:47px; height:23px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wp89cc115e.png" width="47" height="23" border="0" id="pcrv_159" name="pcrv_159" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:720px; top:388px; width:17px; height:17px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wp4bbcfc9b.png" width="17" height="17" border="0" id="pcrv_160" name="pcrv_160" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:739px; top:388px; width:21px; height:18px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpb8847a25.png" width="21" height="18" border="0" id="pcrv_161" name="pcrv_161" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:20px; top:1704px; width:760px; height:65px;">
    <a href="<?php echo site_url("front/cadeau/"); ?>"><img src="<?php echo GetImagePath("front/"); ?>/wpc2839093.png" width="760" height="65" border="0" id="qs_91" name="qs_91" title="" alt="" onload="OnLoadPngFix()"></a></div>
<div style="position:absolute; left:650px; top:1683px; width:111px; height:110px;">
    <map id="map1" name="map1">
        <area shape="poly" coords="111,21,94,21,98,26,100,30,103,34,104,38,105,41,106,44,106,48,107,51,107,60,106,64,105,67,104,71,103,74,101,79,98,84,96,86,111,86" href="<?php echo site_url("front/cadeau/"); ?>" alt="">
        <area shape="poly" coords="14,85,11,80,8,76,7,72,5,67,5,64,4,60,4,48,5,44,6,41,8,36,10,31,13,26,15,24,17,21,0,21,0,86,15,86" href="<?php echo site_url("front/cadeau/"); ?>" alt="">
        <area shape="poly" coords="48,76,37,71,33,60,37,59,44,68,52,61,53,59,42,56,32,47,31,35,39,26,49,21,61,21,72,26,80,35,79,47,69,56,59,63,51,72" href="<?php echo site_url("front/cadeau/"); ?>" alt="">
        <area shape="poly" coords="63,91,80,84,90,69,93,51,87,34,73,22,62,18,44,20,28,29,19,45,18,61,25,77,39,89,49,92" href="<?php echo site_url("front/cadeau/"); ?>" alt="">
        <area shape="poly" coords="62,92,44,90,28,81,19,65,18,49,25,33,39,21,49,18,67,20,83,29,92,45,93,61,86,77,72,89" href="<?php echo site_url("front/cadeau/"); ?>" alt="">
        <area shape="poly" coords="64,105,87,95,103,76,107,51,99,28,80,10,63,4,38,7,17,21,5,43,4,62,14,85,33,101,48,106" href="<?php echo site_url("front/cadeau/"); ?>" alt="">
        <area shape="poly" coords="48,75,51,72,58,65,61,61,63,58,59,59,53,59,50,63,47,66,44,68,40,63,37,60,34,59,33,63,35,67,37,71,40,74,43,76,48,76" href="<?php echo site_url("front/cadeau/"); ?>" alt="">
        <area shape="poly" coords="60,58,69,56,76,50,80,42,79,33,73,26,66,22,61,21,49,21,41,24,33,31,31,40,34,49,42,56,51,58,59,59" href="<?php echo site_url("front/cadeau/"); ?>" alt="">
    </map>
    <img src="<?php echo GetImagePath("front/"); ?>/wp72433f35.png" width="111" height="110" border="0" id="qs_155" name="qs_155" title="" alt="" onload="OnLoadPngFix()" usemap="#map1"></div>
<div style="position:absolute; left:36px; top:1716px; width:289px; height:37px;">
    <div class="Normal-P0">
        <span class="Normal-C0"><a href="<?php echo site_url("front/cadeau/"); ?>" style="color:#ffffff;text-decoration:none;">Accès au catalogue</a></span></div>
</div>
<div style="position:absolute; left:36px; top:485px; width:239px; height:35px;">
    <div class="Normal-P0">
        <span class="Normal-C1"><a href="javascript:history.back()" style="color:#ffffff;text-decoration:none;">Page précédente</a></span></div>
</div>
<div style="position:absolute; left:405px; top:447px; width:375px; height:110px;">
    <map id="map2" name="map2">
        <area shape="poly" coords="375,25,348,25,352,31,355,38,357,46,358,53,358,60,356,67,354,74,349,84,344,90,375,90" href="<?php echo site_url("front/page22/"); ?>" alt="">
        <area shape="poly" coords="268,89,259,76,255,60,256,44,262,30,265,25,0,25,0,90,269,90" href="<?php echo site_url("front/page22/"); ?>" alt="">
        <area shape="poly" coords="299,76,288,71,284,60,288,59,295,68,303,61,304,59,293,56,283,47,282,35,290,26,300,21,313,21,324,26,331,36,329,48,320,56,310,63,302,72" href="<?php echo site_url("front/page22/"); ?>" alt="">
        <area shape="poly" coords="314,91,331,84,341,69,344,51,338,34,324,22,313,18,295,20,279,29,270,45,269,61,276,77,290,89,300,92" href="<?php echo site_url("front/page22/"); ?>" alt="">
        <area shape="poly" coords="313,92,295,90,279,81,270,65,269,49,276,33,290,21,300,18,318,20,334,29,343,45,344,61,337,77,323,89" href="<?php echo site_url("front/page22/"); ?>" alt="">
        <area shape="poly" coords="315,105,338,95,354,76,358,51,350,28,331,10,314,4,289,7,268,21,257,43,255,60,263,82,282,100,299,106" href="<?php echo site_url("front/page22/"); ?>" alt="">
        <area shape="poly" coords="299,75,302,72,306,68,309,65,312,61,314,58,310,59,304,59,301,63,298,66,295,68,291,63,288,60,285,59,284,63,286,67,288,71,291,74,294,76,299,76" href="<?php echo site_url("front/page22/"); ?>" alt="">
        <area shape="poly" coords="311,58,320,56,327,50,331,42,330,33,324,26,317,22,313,21,300,21,292,24,285,30,282,39,284,48,291,55,298,58,303,59" href="<?php echo site_url("front/page22/"); ?>" alt="">
    </map>
    <img src="<?php echo GetImagePath("front/"); ?>/wpa37c8097.png" width="375" height="110" border="0" id="qs_162" name="qs_162" title="" alt="" onload="OnLoadPngFix()" usemap="#map2"></div>
<div style="position:absolute; left:426px; top:487px; width:186px; height:35px;">
    <div class="Normal-P0">
        <span class="Normal-C1"><a href="<?php echo site_url("front/annonce/ficheCommercantAnnonce/");?>/<?php echo $oInfoCommercant->IdCommercant ; ?>" style="color:#ffffff;text-decoration:none;">Page accueil</a></span></div>
</div>
<div style="position:absolute; left:20px; top:560px; width:760px; height:452px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wp970e70b2.png" width="760" height="452" border="0" id="qs_329" name="qs_329" title="" alt="" onload="OnLoadPngFix()"></div>
</div>
</body>
</html>