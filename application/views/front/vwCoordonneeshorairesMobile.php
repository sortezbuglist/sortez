<?php $data["zTitle"] = 'Accueil' ?>


<?php if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) { 

?>

<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN" "http://www.openmobilealliance.org/tech/DTD/xhtml-mobile11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="Generator" content="Serif WebPlus X5">
<meta http-equiv="X-UA-Compatible" content="IE=EmulationIE8">
<title>Coordonnées - horaires</title>
<meta content="minimum-scale=1.0, width=device-width" name="viewport">
 <!--Master Page Head-->
<style type="text/css">
body {margin: 0px; padding: 0px;}
.Corps-artistique-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:left; font-weight:400;
}
.Corps-P
{
    margin:0.0px 0.0px 12.0px 0.0px; text-align:justify; font-weight:400;
}
.Corps-P-P0
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:left; font-weight:400;
}
.Corps-P-P1
{
    margin:0.0px 0.0px 3.0px 0.0px; text-align: center; font-weight:400;
}
.Corps-P-P12
{
    margin:0.0px 0.0px 3.0px 0.0px; text-align: left; font-weight:400;
}
.Corps-P-P2
{
    margin:0.0px 0.0px 5.0px 0.0px; text-align:left; font-weight:400;
}
.Corps-P-P3
{
    margin:0.0px 0.0px 4.0px 0.0px; text-align:center; font-weight:400;
}
.Corps-artistique-C_coord
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:15.0px; line-height:1.20em;
}
.Corps-C
{
    font-family:"Arial", sans-serif; font-weight:700; color:#3653a2; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C0
{
    font-family:"Arial", sans-serif; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C1
{
    font-family:"Verdana", sans-serif; font-size:15.0px; line-height:1.20em;
}
.Corps-artistique-C {
font-family: "Arial", sans-serif;
font-weight: 700;
color: #3653C1;
font-size: 19.0px;
line-height: 1.21em;
}
.Corps-artistique-C-C0 {
font-family: "Arial", sans-serif;
font-weight: 700;
font-size: 13.0px;
line-height: 1.23em;
}
.Corps-artistique-C-C1
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:13.0px; line-height:1.23em;
}
.Corps-artistique-C-C2
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C2
{
    font-family:"Verdana", sans-serif; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C3
{
    font-family:"Arial", sans-serif; font-size:13.0px; line-height:1.23em;
}
.Corps-artistique-C-C3
{
    font-family:"Arial", sans-serif; font-size:15.0px; line-height:1.13em;
}
.Corps-C-C4
{
    font-family:"Arial", sans-serif; text-decoration:underline; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C5
{
    font-family:"Arial", sans-serif; text-decoration:underline; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C6
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
.Button1,.Button1:link,.Button1:visited{background-position:0px 0px;text-decoration:none;display:block;position:absolute;background-image:url(<?php echo GetImagePath("front/"); ?>/wpe3ad0087_06.png);}
.Button1:focus{outline-style:none;}
.Button1:hover{background-position:0px -70px;}
.Button1:active{background-position:0px -35px;}
.Button1 span,.Button1:link span,.Button1:visited span{color:#333333;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:left;text-transform:none;font-style:normal;left:39px;top:38px;width:218px;height:22px;font-size:17px;display:block;position:absolute;cursor:pointer;}
.Button1:hover span{color:#ffffff;}
.Button2,.Button2:link,.Button2:visited{background-position:0px 0px;text-decoration:none;display:block;position:absolute;background-image:url(<?php echo GetImagePath("front/"); ?>/wpaab9d714_06.png);}
.Button2:focus{outline-style:none;}
.Button2:hover{background-position:0px -70px;}
.Button2:active{background-position:0px -35px;}
.Button2 span,.Button2:link span,.Button2:visited span{color:#333333;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:left;text-transform:none;font-style:normal;left:39px;top:38px;width:216px;height:22px;font-size:17px;display:block;position:absolute;cursor:pointer;}
.Button2:hover span{color:#ffffff;}
</style>
<script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/jspngfix.js"></script>
<link rel="stylesheet" href="presentationmobile_fichiers/wpstyles.css" type="text/css">
<script type="text/javascript">var blankSrc = "wpscripts/blank.gif";
</script>
</head>

<body style="background-color: transparent; text-align: center; height: 1050px;" text="#000000">
<div style="background-color:#cccccc;text-align:left;margin-left:auto;margin-right:auto;position:relative;width:320px;height:1050px;">
  
<!--start main info menu-->
<?php $data["zTitle"] = 'Accueil' ;?>
<?php $this->load->view("front/vwTitreinfocommercantMobile", $data);?>
<!--end main info menu-->


<!--start main icone menu 2-->
<?php $this->load->view("front/vwMenumainiconeMobile_noslide", $data);?>
<!--end main icone menu 2-->

  
<img src="<?php echo GetImagePath("front/"); ?>/wp51f75006_06.png" id="qs_1664" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 0px; top: 100px;" height="5" border="0" width="320">


<!--start main logo mobile-->
<?php $this->load->view("front/vwMainLogoMobile", $data);?>
<!--end main logo mobile-->


<img src="<?php echo GetImagePath("front/"); ?>/wp8c8f8ab8_06.png" id="qs_1185" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 15px; top: 291px;" height="247" border="0" width="290">
<div id="art_279" style="position:absolute;left:31px;top:301px;width:198px;height:19px;">
    <div class="Corps-artistique-P">
        <span class="Corps-artistique-C_coord">Coordonnées et horaires</span></div>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpe7b90467_06.png" id="qs_1187" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 15px; top: 291px;" height="271" border="0" width="290">

<div id="txt_605" style="position:absolute; left:26px; top:335px; width:263px; height:213px; overflow:hidden;">
<p class="Corps-P-P0">
<span class="Corps-C-C0">
<?php echo $oInfoCommercant->Adresse1 ; ?><?php if ($oInfoCommercant->Adresse2!="") echo " - ".$oInfoCommercant->Adresse2 ; ?>
<?php if ($oInfoCommercant->CodePostal!="") echo " - ".$oInfoCommercant->CodePostal ; ?><?php if ($oInfoCommercant->ville != "") echo " - ".$oInfoCommercant->ville ; ?>
</span></p>
<p class="Corps-P-P12"><span class="Corps-C-C3"><strong>Tél :</strong></span><span class="Corps-C-C0"> <?php echo '<a href="tel:'.supprime_espace($oInfoCommercant->TelFixe).'">'.$oInfoCommercant->TelFixe.'</a>' ; ?></span></p>
<p class="Corps-P-P12"><span class="Corps-C-C3"><strong>Directe :</strong></span><span class="Corps-C-C0"> <?php echo '<a href="tel:'.supprime_espace($oInfoCommercant->TelDirect).'">'.$oInfoCommercant->TelDirect.'</a>' ; ?></span></p>
<p class="Corps-P-P12"><span class="Corps-C-C3"><strong>Mobile :</strong></span><span class="Corps-C-C0"> <?php echo '<a href="tel:'.supprime_espace($oInfoCommercant->TelMobile).'">'.$oInfoCommercant->TelMobile.'</a>' ; ?></span></p>
<p class="Corps-P-P12"><span class="Corps-C-C3"><strong>Email :</strong></span><span class="Corps-C-C0"> <?php echo '<a href="mailto:'.$oInfoCommercant->Email.'">'.$oInfoCommercant->Email.'</a>' ; ?></span></p>
<p class="Corps-P-P2"><span class="Corps-C-C0"><?php echo $oInfoCommercant->Horaires ; ?> </span></p>
</div>

<div id="txt_632" style="position:absolute; left:18px; top:613px; width:290px; height: auto; overflow:hidden; font-size:13px; text-align:center;">
  <center>
<!--start main footer menu-->
<?php $this->load->view("front/vwMenumainfooterMobile", $data);?>
<!--end main footer menu-->
</center>
</div>


</div>

</body></html>


<?php
} else { 


 
?>



<?php $this->load->view("front/includes/vwHeaderPartenaire", $data); ?>

<div id="main_body">
    <div id="main_left">
    	<?php $this->load->view("front/vwContactPartenaire", $data); ?> 
    </div>
    <div id="main_right">
      <?php $this->load->view("front/vwSlidePartenaire", $data); ?> 
      <div id="main_data">
        <div id="main_data_head"><?php echo $oInfoCommercant->NomSociete ; ?></div>
        <div id="main_data_body">
        <?php //if ($is_mobile == false) echo " wsdqfd<br/>"; else echo "is_mobile<br/>"; ?>
        <?php //if ($is_robot == false) echo " sdgdfs<br/>"; else echo "is_robot<br/>";?>
        <?php //if ($is_browser == false) echo " sdgdfs<br/>"; else echo "is_browser<br/>";?>
         <?php //echo $is_platform."<br/><br/><br/>";?>
        <br/><?php echo $oInfoCommercant->Caracteristiques; ?></div>
        <div id="main_data_footer"></div>
      </div>
    </div>
  </div>

<?php $this->load->view("front/includes/vwFooterPartenaire"); ?>

<?php 

}
?>