<?php $data["zTitle"] = 'Inscription des professionnels'; ?>
<?php $this->load->view("sortez_vsv/includes/main_header", $data); ?>
<div class="container">
<?php $this->load->view("sortez_vsv/includes/header_top", $data); ?>
<?php $this->load->view("sortez_vsv/includes/header_right", $data); ?>
<div class="row text-center" >
    <div class="col-lg-12 text-center"><a class="btn btn-info" href="<?php site_url('front/utilisateur/contenupro') ?>">Retour</a></div>
<div class="col-lg-12 text-center" style="color:white;width:750px; text-align:center; font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight: bold;">Votre abonnement a expir&eacute;.<br /> veuillez contacter l'administrateur du Sortez.org pour renouveller votre abonnement.<br />
<a style="color: deeppink" href="<?php echo site_url("front/contact/");?>">ici</a>.<br /></div>
<div style="height: 400px" class="col-lg-12 text-center"><img src="<?php echo GetImagePath("front/");?>/expire.gif" /></div>
</div>
    <?php $this->load->view("sortez_vsv/includes/main_footer_link", $data); ?>
    <?php $this->load->view("privicarte/includes/main_footer_copyright_pro", $data); ?>
    <?php $this->load->view("privicarte/includes/main_footer", $data); ?>
</div>