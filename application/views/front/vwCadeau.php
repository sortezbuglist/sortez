<?php $data["zTitle"] = 'Cadeaux' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="fr"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="Generator" content="Serif WebPlus X5">
<meta http-equiv="X-UA-Compatible" content="IE=EmulationIE8">
<!--<meta name="viewport" content="width=800" />-->
<meta name="Viewport" content="width=device-width">
<title>Cadeaux</title>
<style type="text/css">
body {margin: 0px; padding: 0px;}
.Normal-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:justify; font-weight:400;
}
.Normal-P-P0
{
    margin:7.0px 0.0px 0.0px 0.0px; text-align:left; font-weight:400;
}
.Corps-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:left; font-weight:400;
}
.Normal-C
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:16.0px; line-height:1.25em;
}
.Normal-C-C0
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:13.0px; line-height:1.23em;
}
.Normal-C-C1
{
    font-family:"Arial", sans-serif; font-size:13.0px; line-height:1.23em;
}
.Corps-C
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C0
{
    font-family:"Arial", sans-serif; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C1
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:21.0px; line-height:1.19em;
}
.Normal-C-C2
{
    font-family:"Arial", sans-serif; font-size:5.0px; line-height:1.20em;
}
.Normal-C-C3
{
    font-family:"Arial", sans-serif; font-weight:700; color:#990000; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C2
{
    font-family:"Verdana", sans-serif; font-size:13.0px; line-height:1.23em;
}
#id_mainbody_head {
	background-image: url(<?php echo GetImagePath("front/"); ?>/fond_mainbody_head.png);
	background-repeat: no-repeat;
	float: left;
	height: 297px;
	width: 620px;
	position: relative;
}
#id_mainbody_main {
	float: left;
	width: 620px;
	position: relative;
	background-image: url(<?php echo GetImagePath("front/"); ?>/fond_mainbody_main.png);
	background-repeat: repeat-y;
}
#id_mainbody_footer {
	background-image: url(<?php echo GetImagePath("front/"); ?>/fond_mainbody_footer.png);
	background-repeat: no-repeat;
	float: left;
	height: 31px;
	width: 620px;
	position: relative;
}
#id_mainbody_content {
	background-image: url(<?php echo GetImagePath("front/"); ?>/fond_mainbody_content.png);
	background-repeat: no-repeat;
	background-position: left bottom;
	float: left;
	width: 610px;
	position: relative;
	padding-right: 5px;
	padding-left: 5px;
	height: auto;
	padding-bottom: 10px;
}
</style>
<script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/jspngfix.js"></script>
<link rel="stylesheet" href="fonctionnementpointscadeaux_fichiers/wpstyles.css" type="text/css">
<script type="text/javascript">var blankSrc = "wpscripts/blank.gif";
</script>
</head>

<body style="background-color: transparent; background-image: url(&quot;<?php echo GetImagePath("front/"); ?>/wp007ff600_06.png&quot;); background-repeat: repeat; background-position: center top; background-attachment: scroll; text-align: center; height: 2500px;" text="#000000">
<div style="background-color:#cccccc;text-align:left;margin-left:auto;margin-right:auto;position:relative;width:650px;height:2500px;">
<map id="map0" name="map0">
    <area shape="rect" coords="11,121,75,193" href="<?php echo site_url();?>" alt="">
    <area shape="rect" coords="89,121,153,194" href="javascript:history.back();" alt="">
    <area shape="rect" coords="169,121,233,194" href="javascript:history.void();" alt="">
    <area shape="rect" coords="246,121,310,194" href="<?php echo site_url("front/contact");?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wpd22efe04_06.png" id="hs_4" title="" alt="contact" onload="OnLoadPngFix()" usemap="#map0" style="position: absolute; left: 0px; top: 0px;" height="200" border="0" width="650">



<div style="position: absolute; left: 15px; top: 880px; width:620px; height:auto;">
  <div id="id_mainbody_head"></div>
  <div id="id_mainbody_main">
  
  <?php foreach ($listCadeau as $ocadeau) {?>
  <div id="id_mainbody_content">
  
	<table cellspacing="3" style="width:100%;"> 
		<tr> 
		  <td style="width:213px ; height:221px ;" rowspan="2"> 
          <?php if ($ocadeau->Imagecadeau != "" && $ocadeau->Imagecadeau != NULL) { ?>
		  <IMG alt="" style="width:213px ;" id="LMImageImg3" src="<?php echo GetImagePath("front/"); ?>/cadeaux/<?php echo $ocadeau->Imagecadeau ;?>">
          <?php } else { ?>
          <IMG alt="cadeau" style="width:213px ; " id="LMImageImgcadeau" src="<?php echo GetImagePath("front/"); ?>/cadeau_bleu.jpg">
          <?php } ?>

		  </td>
		  <td valign="top"> 
          <div style="margin-left:20px;">
          <br/>
		  <FONT face="Arial" color="#000000"><SPAN style="font-size:16pt; font-weight:700;"><?php echo utf8_encode($ocadeau->Nomoffre) ;?><BR></SPAN></FONT>
          <FONT face="Arial" color="#000000"><SPAN style="font-size:10pt;"><?php echo $ocadeau->Commentaire ;?></SPAN></FONT>
          </div>

		  </td>
          
          <?php if (isset($id_client) && isset($type_client) && $type_client != "COMMERCANT") {?>
          <td align="right">
          <form name="validation_point_cadeaux" id="validation_point_cadeaux" action="<?php echo site_url("front/fidelisation/");?>" method="post">
          <input name="Idcadeau" id="Idcadeau" type="hidden" value="<?php echo $ocadeau->Idcadeau ;?>">
          <button type="submit" style="border:none; background-color:#FFF;">
          <img src="<?php echo GetImagePath("front/"); ?>/wpc5451f16_06.png" alt="" name="pic_1598" width="106" height="30" border="0" id="pic_1598">
          </button>
          </form>
          </td>
          <?php } ?>
          
		</tr>
        <tr> 
		  
		  <td valign="bottom"> 
		  <div style="margin-left:20px;">
          
          <br />
          <FONT face="Arial" color="#000000"><SPAN style="font-size:13px; font-weight:700;"><B>Validit&eacute; : </B></SPAN></FONT>
          <FONT face="Arial" color="#000000"><SPAN style="font-size:13px;"><?php echo $ocadeau->Valabledebut; if ($ocadeau->Valablefin){echo " au ".$ocadeau->Valablefin;}?></SPAN></FONT>
          <br />
          <FONT face="Arial" color="#000000"><SPAN style="font-size:13px; font-weight:700;"><B>Quantit&eacute; disponible : </B></SPAN></FONT>
          <FONT face="Arial" color="#000000"><SPAN style="font-size:13px;"><?php echo utf8_encode($ocadeau->Qtedisponible) ;?></SPAN></FONT>
          <br />
          <FONT face="Arial" color="#000000"><SPAN style="font-size:13px; font-weight:700;"><B>Valeur :</B></SPAN></FONT>
          <FONT face="Arial" color="#00000"><SPAN style="font-size:21pt; font-weight:700;line-height: 1.19em;"> <?php echo utf8_encode($ocadeau->valeurpoints) ;?> points</SPAN></FONT>
			</div>
		  </td>
		</tr>
	</table>
  </div>
<?php }?>
  
  </div>
  <div id="id_mainbody_footer"></div>
</div>


<img src="<?php echo GetImagePath("front/"); ?>/wp6cb847ad_06.png" id="pic_1593" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 15px; top: 216px;" height="643" border="0" width="620">
<div id="txt_477" style="position:absolute;left:25px;top:599px;width:598px;height:206px;overflow:hidden;">


<?php if (isset($argValidate) && $argValidate == 0){ ?>
<P style="text-align:left"><FONT face="Verdana" color="#000000"><SPAN style="font-size:16pt;line-height:24px;"><B><blink>&nbsp;&nbsp;Votre commande est invalide.</blink></B></SPAN></FONT></P>
<P style="text-align:left"><FONT face="Verdana" color="#000000"><SPAN style="font-size:16pt;line-height:24px;"><B><blink>&nbsp;&nbsp;Verifier la validité de vos points cadeau.</blink></B></SPAN></FONT></P>
<?php } else if (isset($argValidate) && $argValidate == 1){ ?>
<P style="text-align:left"><FONT face="Verdana" color="#000000"><SPAN style="font-size:16pt;line-height:24px;"><B><blink>&nbsp;Votre commande a bien été enregistrée.</blink></B></SPAN></FONT></P>
<P style="text-align:left"><FONT face="Verdana" color="#000000"><SPAN style="font-size:16pt;line-height:24px;"><B><blink>&nbsp;Vous allez recevoir sur votre adresse mail </blink></B></SPAN></FONT></P>
<P style="text-align:left"><FONT face="Verdana" color="#000000"><SPAN style="font-size:16pt;line-height:24px;"><B><blink>&nbsp;la confirmation de ce BonPlan</blink></B></SPAN></FONT></P>

<?php } else { ?>

<p class="Normal-P"><span class="Normal-C-C1">Avec les points cadeaux, Le club proximité aide ses partenaires à dynamiser leur
    activité et à fidéliser les consommateurs adhérents.</span></p>
<p class="Normal-P"><span class="Normal-C-C2">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C-C1">Le Club propose à ces derniers de gagner des point cadeaux à chaque concrétisation
    d’une offre “bon plan”. </span></p>
<p class="Normal-P"><span class="Normal-C-C2">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C-C3">Ces cadeaux sont regroupés au sein d'un catalogue (voir ci-<wbr>dessous)</span></p>
<p class="Normal-P"><span class="Normal-C-C2">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C-C1">Le consommateur note le numéro du courriel qu’il a reçu lors de la confirmation de
    sa demande.</span></p>
<p class="Normal-P"><span class="Normal-C-C2">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C-C1">Lors de son achat chez le partenaire, il lui demande lui le numéro du courriel qu’il
    a reçu.</span></p>
<p class="Normal-P"><span class="Normal-C-C2">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C-C1">Muni de ces deux codes, le consommateur se connecte sur son compte, il les enregistre
    sur les champs correspondant, en validant notre système reconnaîtra les numéros et
    son compte sera crédité de 10 points.</span></p>
<p class="Normal-P"><span class="Normal-C-C2">&nbsp;</span></p>
<p class="Wp-Corps-P"><span class="Corps-C-C2">&nbsp;</span></p>

<?php } ?>
</div>

<?php if (!isset($id_client)) { // && $type_client != "COMMERCANT"?>
<map id="map1" name="map1">
    <area shape="poly" coords="298,34,303,28,303,6,297,0,6,0,0,7,0,29,6,35,297,35" href="<?php echo site_url("front/particuliers/inscription/"); ?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wp049b6fab_06.png" id="pic_1588" title="" alt="inscriptionparticuliers" onload="OnLoadPngFix()" usemap="#map1" style="position: absolute; left: 171px; top: 800px;" height="35" border="0" width="303">
<?php }?>

</div>

</body></html>