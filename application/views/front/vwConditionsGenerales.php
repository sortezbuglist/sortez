<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="Generator" content="Serif WebPlus X4">
<title>club conditions générales</title>
<style type="text/css">
<!--
body {margin: 0px; padding: 0px;}
a:link {color: #3e550e;}
a:visited {color: #8e9165;}
a:hover {color: #3e550e;}
a:active {color: #3e550e;}
.Normal-P
        {
        margin:0.0px 0.0px 0.0px 0.0px; text-align:justify; font-weight:400; 
        }
.Corps-P
        {
        margin:0.0px 0.0px 12.0px 0.0px; text-align:left; font-weight:400; 
        }
.Normal-P0
        {
        margin:0.0px 0.0px 0.0px 0.0px; text-align:left; font-weight:400; 
        }
.Normal-C
        {
        font-family:"Arial", sans-serif; font-weight:700; font-size:27.0px; 
        line-height:1.22em; color:#fcc73c; 
        }
.Normal-C0
        {
        font-family:"Arial", sans-serif; font-size:21.0px; line-height:1.19em; 
        color:#ffffff; 
        }
.Normal-C1
        {
        font-family:"Arial", sans-serif; font-weight:700; font-size:21.0px; 
        line-height:1.19em; color:#ffffff; 
        }
.Normal-C2
        {
        font-family:"Arial", sans-serif; font-weight:700; font-size:24.0px; 
        line-height:1.25em; color:#fcc73c; 
        }
.Normal-C3
        {
        font-family:"Arial", sans-serif; font-size:24.0px; line-height:1.17em; 
        color:#fcc73c; 
        }
.Normal-C4
        {
        font-family:"Arial", sans-serif; font-style:italic; font-size:21.0px; 
        line-height:1.19em; color:#ffffff; 
        }
.Normal-C5
        {
        font-family:"Arial", sans-serif; font-weight:700; font-size:21.0px; 
        line-height:1.19em; color:#fcc73c; 
        }
.Normal-C6
        {
        font-family:"Arial", sans-serif; font-weight:700; font-size:24.0px; 
        line-height:1.25em; color:#edd112; 
        }
.Corps-C
        {
        font-family:"Arial", sans-serif; font-size:16.0px; line-height:1.19em; 
        }
.Normal-C7
        {
        font-family:"Arial", sans-serif; font-weight:700; font-size:27.0px; 
        line-height:1.22em; color:#ffffff; 
        }
-->
</style>
<script type="text/javascript" src="wpscripts/jspngfix.js"></script>
<script type="text/javascript">
var blankSrc = "wpscripts/blank.gif";
</script>
</head>

<body text="#000000" style="background-color:#000000; text-align:center; height:7000px;">
<div style="background-color:#000000;text-align:left;margin-left:auto;margin-right:auto;position:relative;width:800px;height:7000px;">
<div style="position:absolute; left:0px; top:0px; width:621px; height:567px;">
    <map id="map0" name="map0">
        <area shape="poly" coords="401,471,373,471,377,477,380,484,382,492,383,499,383,508,381,515,379,522,375,528,371,534,369,536,401,536" href="javascript:history.back()" alt="">
        <area shape="poly" coords="293,535,285,522,281,508,280,496,284,482,290,471,26,471,26,536,295,536" href="javascript:history.back()" alt="">
        <area shape="poly" coords="324,522,313,517,309,506,313,505,320,514,328,507,329,505,318,502,308,493,307,481,317,470,326,467,338,468,349,472,356,482,357,486,352,497,343,503,332,513" href="javascript:history.back()" alt="">
        <area shape="poly" coords="339,537,356,530,366,515,369,497,363,480,349,468,338,464,320,466,305,475,296,489,294,506,301,523,315,535,325,538" href="javascript:history.back()" alt="">
        <area shape="poly" coords="338,538,320,536,305,527,296,513,294,496,301,479,315,467,325,464,343,466,359,475,368,491,369,507,362,523,348,535" href="javascript:history.back()" alt="">
        <area shape="poly" coords="341,551,363,541,379,522,383,497,375,474,356,456,339,450,314,453,293,467,282,489,280,506,288,528,307,546,324,552" href="javascript:history.back()" alt="">
        <area shape="poly" coords="324,521,327,518,331,514,335,510,337,506,339,504,335,505,329,505,326,509,323,513,320,514,316,509,313,506,310,505,309,509,311,513,313,517,316,520,319,522,324,522" href="javascript:history.back()" alt="">
        <area shape="poly" coords="336,504,344,502,352,497,356,489,357,486,354,477,346,470,338,467,326,467,317,470,310,476,307,485,309,494,316,500,323,504,328,505" href="javascript:history.back()" alt="">
    </map>
    <img src="<?php echo GetImagePath("front/"); ?>/wpimages/wpd8bafb35.png" width="621" height="567" border="0" id="qs_155" name="qs_155" title="" alt="" onload="OnLoadPngFix()" usemap="#map0"></div>
<div style="position:absolute; left:616px; top:382px; width:21px; height:21px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpimages/wp22fb5c2b.png" width="21" height="21" border="0" id="pcrv_241" name="pcrv_241" title="" alt="" onload="OnLoadPngFix()"></div>
<div id="txt_492" style="position:absolute; left:20px; top:581px; width:760px; height:5595px;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box; overflow:hidden;">
<p class="Normal-P"><span class="Normal-C">L&#39;adhésion du Consommateur</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C1">Afin de bénéficier des avantages du Club Proximité :</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Le consommateur doit s&#39;identifier et s&#39;inscrire uniquement en ligne sur www.proximite-<wbr>magazine.com
    ou sur nos applications mobiles.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Le consommateur doit confirmer qu&#39;il est majeur.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Le consommateur doit accepter sans réserve les conditions générales du Club Proximité.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Le consommateur doit valider sa demande d&#39;inscription.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Après validation de sa demande il reçoit par retour de mail une confirmation d&#39;enregistrement
    avec la présence de son identifiant et mot de passe qu&#39;il doit confirmer.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Le consommateur gère lui-<wbr>même sa fiche, il peut la modifier à volonté.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Le consommateur s&#39;engage sur l&#39;honneur à n&#39;ouvrir qu&#39;un seul compte adhérent et de
    ne s&#39;enregistrer qu&#39;une seule fois en qualité de consommateur.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Le consommateur qui désire se désinscrire du Club devra nous adresser par mail sa
    demande de retrait.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Le consommateur ne reçoit pas de carte d&#39;adhérent, seule la présentation du mail
    de confirmation imprimé ou présent sur son Smartphone lui permettra de bénéficier
    de l&#39;offre « Bons Plans » du diffuseur. </span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C">L&#39;adhésion du Professionnel</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Le professionnel choisi l&#39;abonnement le plus approprié à ses objectifs. </span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Lors de son inscription il reçoit après paiement de son abonnement un identifiant
    et mot de passe, avec ses données il se connecte à une page d&#39;administration. Il
    peut ainsi créer, modifier et supprimer à volonté et en temps réel ses données. </span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C1">Le règlement :</span><span class="Normal-C0"> </span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Le compte professionnel est activé à réception du chèque ou virement &nbsp;si le règlement
    est effectué en carte bancaire par l&#39;intermédiaire de PAYPAL, le compte sera ouvert
    automatiquement dès la validation par cet organisme.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C2">L&#39;abonnement de base : (Inscription gratuite -<wbr> à renouveler au bout de &nbsp;12 mois)</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Cet abonnement donne droit au professionnel de se connecter sur une page administration
    sur la quelle il peut remplir, modifier ou supprimer à volonté les différents champs
    :</span></p>
<p class="Normal-P"><span class="Normal-C0">Enseigne, adresse, ville, téléphone, code postal, téléphone fixe, téléphone mobile,
    courriel, site internet, intégration d&#39;une photo, plan de localisation appartenance
    à une association, détail de l&#39;activité, présence d&#39;un petit article de présentation.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C2">L&#39;abonnement Partenaire du Club &nbsp;(Inscription payante -<wbr> abonnement de 12 mois) </span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Cet abonnement donne droit au professionnel de se connecter sur une page administration
    sur la quelle il peut remplir, modifier ou supprimer à volonté les différents champs
    :</span></p>
<p class="Normal-P"><span class="Normal-C0">Enseigne, adresse, ville, téléphone, code postal, téléphone fixe, téléphone mobile,
    courriel, site internet, intégration d&#39;une photo, plan de localisation appartenance
    à une association, Une galerie avec 4 photos au maximum, un lien vidéo You Tube,
    un article informatif, vos horaires, vos dates de fermeture pour congés, vos éventuelles
    conditions de fidélisation, le téléchargement d&#39;un document commercial en PDF, les
    liens face book, twitter, myspace.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C2">L&#39;offre de fidélisation :</span><span class="Normal-C3"> </span><span class="Normal-C0">Le professionnel peut concéder une offre de fidélisation
    identique à celle qu&#39;il concède à sa clientèle traditionnelle, il peut préciser les
    modalités de cette offre, cette offre de fidélisation n&#39;est pas cumulable avec l&#39;offre
    « Bons Plans » et les soldes et promotions en cours sauf avis contraire du professionnel.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C2">Les Options de l&#39;abonnement partenaire</span></p>
<p class="Normal-P"><span class="Normal-C3">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C2">Les Bons plans</span><span class="Normal-C3">, </span><span class="Normal-C2">le Principe :</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Avec cette option, nos partenaires concèdent des offres exceptionnelles pour motiver
    les consommateurs à leur rendre visite.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Dès réception de leur règlement, leur compte &nbsp;&nbsp;&quot;Bons Plans&quot; sera ouvert.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">L&#39;offre &quot;Bons Plans&quot; &nbsp;peut être unique, exceptionnelle ou annuelle. </span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">La qualité de l&#39;offre sera précisée sur chacune d&#39;elles :</span></p>
<p class="Normal-P"><span class="Normal-C0">Celle ci peut-<wbr>être validée qu&#39;une seule fois :</span></p>
<p class="Normal-P"><span class="Normal-C4">Exemple pour un restaurant (1 menu acheté = 1 menu offert) L&#39;adhérent &nbsp;identifié
    ne peut la valider qu&#39;une seule fois. Si par nature il désire valider une promotion
    déjà utilisé, notre système lui précisera son impossibilité de le faire.</span></p>
<p class="Normal-P"><span class="Normal-C4">Cette offre est exceptionnelle et utilisable qu&#39;une seule fois, une fois le consommateur
    identifié, il valide son offre, si celle-<wbr>ci a déjà été utilisée par ce même consommateur,
    le bouton change de couleur et le message devient « Cette offre a déjà été utilisée
    ».</span></p>
<p class="Normal-P"><span class="Normal-C4">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C4">Si un consommateur ayant déjà profité de l&#39;offre découverte s&#39;inscrit sous un autre
    nom et/ou &nbsp;avec un autre e-<wbr>mail pour pouvoir bénéficier une seconde fois de l&#39;offre
    découverte chez le même professionnel, ce consommateur verra son compte supprimé
    du Club Proximité.</span></p>
<p class="Normal-P"><span class="Normal-C4">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C4">Si le diffuseur reconnaît ce consommateur, il est en droit de refuser à concéder
    son offre découverte et à avertir le club de la mauvaise foi de ce consommateur.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C2">Ou bien les partenaires peuvent choisir qu&#39;elle soit permanente :</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Pendant la totalité de leur abonnement ou pendant une période précisée. L&#39;adhérent
    &nbsp;identifié peut valider votre offre autant de fois qu&#39;il le désire.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C">Le Fonctionnement des “Bons Plans”</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Les offres « Bons Plans » sont valables uniquement pendant la période de l&#39;abonnement
    du partenaire.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">La date de fin de cet abonnement est précisée sur l&#39;offre du professionnel.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Avant de valider l&#39;offre « Bons Plans », le consommateur doit s&#39;identifier (identifiant
    et mot de passe).</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Puis il valide en cliquant sur le bouton « Validation de ce Bon Plan ».</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Dès validation de cette offre, le consommateur reçoit un mail personnalisé qui lui
    confirme son choix, il imprime ce bon et le présente chez le diffuseur pour en bénéficier
    ou présente le courriel sur son Smartphone.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">L&#39;offre « Bons Plans » n&#39;est pas cumulable avec les offres de fidélisation, soldes
    ou promotions en cours déjà concédés par le diffuseur.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Parallèlement, ce même diffuseur reçoit un double de la demande, précisant les coordonnées
    du consommateur par courriel.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Le consommateur autorise ce diffuseur à prendre contact avec lui afin de préciser
    un jour et une heure de rendez vous.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">L&#39;offre découverte est valable 15 jours après son émission, passé ce délai, le diffuseur
    peut refuser de concéder cette offre au consommateur.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C">Les Annonces Professionnelles</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Dès réception de leur règlement, leur compte « Annonces Pro » sera ouvert.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Seuls les partenaires du Club abonnés peuvent souscrire à cette option.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Ils choisissent un ou plusieurs modules de 20 annonces, ils déposent en temps réel
    leurs produits ou services (déstockage, soldes, promotions etc...)</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Depuis leur page admin, ils peuvent dans l&#39;année de leur abonnement, gérer ce capital
    de 20 annonces (création, modification, suppression).</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Ces annonces professionnelles sont &nbsp;présentes sur notre site internet et sur nos
    applications mobiles. </span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C2">Engagement des Partenaires du Club Proximité :</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Le Club propose sur sa base de données hébergée sur le site internet www.proximite-<wbr>magazine.com
    et ses applications mobiles des infor-<wbr>mations et des offres des commerçants et artisans
    qui ont adhérés en qualité de parte-<wbr>naires.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C5">Les diffuseurs commerçants, artisans, PME, PMI s&#39;engagent pendant la période de leur
    abonnement à :</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Respecter strictement leurs offres « Bons Plans » . Assumer seuls la responsabilité
    de la régularité des informations précisées sur leur page informative et de l&#39;annonce
    de leurs offres. </span></p>
<p class="Normal-P"><span class="Normal-C0">Les offres réservées aux consommateurs abonnés du « Club Proximité » sont proposées
    par les diffuseurs sous leur entière responsabilité.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C">Engagement du Club Proximité :</span></p>
<p class="Normal-P"><span class="Normal-C0">Le Club fourni simplement un outil complet de communication :</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Avec une base de données relationnelle hébergée sur le site internet www.proximite-<wbr>magazine.com.</span></p>
<p class="Normal-P"><span class="Normal-C0"> </span></p>
<p class="Normal-P"><span class="Normal-C0">Avec la présence d&#39;insertions publicitaires sur l&#39;ensemble des éditions du Magazine
    Proximité.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Ainsi que tous autres moyens de communication que le Club jugera utile. </span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C">La page d&#39;accueil du Club Proximité :</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Sur cette page : http://www.proximite-<wbr>magazine.com/bondereduction/club-<wbr>proximite/Nos-<wbr>Bons.php,
    le consommateur découvre l&#39;ensemble des professionnels et diffuseurs ainsi que leurs
    offres « Bons Plans » concédées. </span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">A l&#39;aide de recherches multicritères, il navigue et choisit le ou les offres pouvant
    l&#39;intéresser.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C6">La Charte de Confidentialité</span></p>
<p class="Normal-P"><span class="Normal-C0"> </span></p>
<p class="Normal-P"><span class="Normal-C0">La Carte Club Proximité s&#39;engage à ce que les données personnelles qui sont recueillies
    au moment de votre inscription au Club restent totalement confidentielles et ne soient
    transmises à aucun tiers.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Toutefois, en adhérant au club vous indiquez que vous souhaitez recevoir des informations
    régulières concernant la vie du club et du magazine Proximité.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C2">Informatique et libertés</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Conformément à la Loi numéro 78-<wbr>17 du 6 janvier 1978, dite &quot;Loi Informatique et Libertés&quot;,
    vous avez un droit d&#39;accès et de rectification des données nominatives que vous fournissez
    dans le cadre de votre adhésion au Club.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Si, à un moment quelconque, vous souhaitez exercer votre droit d&#39;accès et de rectification
    aux informations nominatives vous concernant, merci d&#39;adresser votre demande avec
    votre identifiant et votre mot de passe par mail à proximite-<wbr>magazine@orange.fr.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">Une copie confidentielle des informations nominatives spécifiques à chaque membre
    vous sera adressée. Par mesure de confidentialité, toute demande ne contenant pas
    l&#39;identifiant et le mot de passe du membre sera systématiquement rejetée.</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C2">Mentions Légales</span></p>
<p class="Normal-P"><span class="Normal-C0">Editeur du site Internet :</span></p>
<p class="Normal-P"><span class="Normal-C0">SARL PHASE</span></p>
<p class="Normal-P"><span class="Normal-C0">60 avenue de Nice</span></p>
<p class="Normal-P"><span class="Normal-C0">06800 Cagnes sur Mer</span></p>
<p class="Normal-P"><span class="Normal-C0">Tél. : 04 93 73 84 51</span></p>
<p class="Normal-P"><span class="Normal-C0">Mobile : 06 72 05 59 35</span></p>
<p class="Normal-P"><span class="Normal-C0">SARL au capital de 7265€ </span></p>
<p class="Normal-P"><span class="Normal-C0">RCS Antibes 412.071.466</span></p>
<p class="Normal-P"><span class="Normal-C0">SIRET : 412071466 00012</span></p>
<p class="Normal-P"><span class="Normal-C0">CODE NAF : 7022Z</span></p>
<p class="Normal-P"><span class="Normal-C0">IBAN : FR76 3007 6023 5110 6081 0020 072</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Normal-P"><span class="Normal-C0">&nbsp;</span></p>
<p class="Corps-P"><span class="Corps-C">&nbsp;</span></p>
</div>
<div style="position:absolute; left:36px; top:485px; width:239px; height:35px;">
    <div class="Normal-P0">
        <span class="Normal-C7"><a href="javascript:history.back()" style="color:#ffffff;text-decoration:none;">Page précédente</a></span></div>
</div>
<div style="position:absolute; left:405px; top:447px; width:375px; height:110px;">
    <map id="map1" name="map1">
        <area shape="poly" coords="375,25,348,25,352,31,355,38,357,46,358,53,358,60,356,67,354,74,349,84,344,90,375,90" href="<?php echo site_url("front/page22/"); ?>" alt="">
        <area shape="poly" coords="268,89,259,76,255,60,256,44,262,30,265,25,0,25,0,90,269,90" href="<?php echo site_url("front/page22/"); ?>" alt="">
        <area shape="poly" coords="299,76,288,71,284,60,288,59,295,68,303,61,304,59,293,56,283,47,282,35,290,26,300,21,313,21,324,26,331,36,329,48,320,56,310,63,302,72" href="<?php echo site_url("front/page22/"); ?>" alt="">
        <area shape="poly" coords="314,91,331,84,341,69,344,51,338,34,324,22,313,18,295,20,279,29,270,45,269,61,276,77,290,89,300,92" href="<?php echo site_url("front/page22/"); ?>" alt="">
        <area shape="poly" coords="313,92,295,90,279,81,270,65,269,49,276,33,290,21,300,18,318,20,334,29,343,45,344,61,337,77,323,89" href="<?php echo site_url("front/page22/"); ?>" alt="">
        <area shape="poly" coords="315,105,338,95,354,76,358,51,350,28,331,10,314,4,289,7,268,21,257,43,255,60,263,82,282,100,299,106" href="<?php echo site_url("front/page22/"); ?>" alt="">
        <area shape="poly" coords="299,75,302,72,306,68,309,65,312,61,314,58,310,59,304,59,301,63,298,66,295,68,291,63,288,60,285,59,284,63,286,67,288,71,291,74,294,76,299,76" href="<?php echo site_url("front/page22/"); ?>" alt="">
        <area shape="poly" coords="311,58,320,56,327,50,331,42,330,33,324,26,317,22,313,21,300,21,292,24,285,30,282,39,284,48,291,55,298,58,303,59" href="<?php echo site_url("front/page22/"); ?>" alt="">
    </map>
    <img src="<?php echo GetImagePath("front/"); ?>/wpimages/wpa37c8097.png" width="375" height="110" border="0" id="qs_162" name="qs_162" title="" alt="" onload="OnLoadPngFix()" usemap="#map1"></div>
<div style="position:absolute; left:426px; top:487px; width:186px; height:35px;">
    <div class="Normal-P0">
        <span class="Normal-C7"><a href="<?php echo site_url("front/page22/"); ?>" style="color:#ffffff;text-decoration:none;">Page accueil</a></span></div>
</div>
</div>
</body>
</html>