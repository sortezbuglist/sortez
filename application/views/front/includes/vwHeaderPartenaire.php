<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="<?php if (isset($oInfoCommercant->metatag)) echo $oInfoCommercant->metatag;?>" />

<title><?php echo "".$oInfoCommercant->NomSociete ; ?></title>

<link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/") ; ?>/css_partenaire.css" />

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.6.4.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.js"></script>
<script type="text/javascript">

var $jv = jQuery.noConflict();

$jv(document).ready(function() {
	// validate form and submit
	$jv("#frmNousContacter").validate({
		rules: {
			zNom: "required",
			zTelephone: "required",
			zEmail: {
				required: true,
				email: true
			},
			zCommentaire: "required"
		},
		messages: {
			zNom: "Veuillez indiquer votre nom.",
			zTelephone: "Veuillez indiquer votre contact téléphonique",
			zEmail: "Veuillez entrer un adresse mail valide.",
			zCommentaire: "Veuillez iniquer votre message."
		}
		
		
	});
	
	
	
	$jv("#frmRecomanderAmi").validate({
		rules: {
			zNom:  {
				required: true,
				email: true
			},
			zTelephone: "required",
			zEmail: {
				required: true,
				email: true
			},
			zCommentaire: "required"
		},
		messages: {
			zNom: "Veuillez entrer un adresse mail valide.",
			zTelephone: "Veuillez indiquer votre Nom",
			zEmail: "Veuillez entrer un adresse mail valide.",
			zCommentaire: "Veuillez iniquer votre message."
		}
		
		
	});
	
	
		var logo_url_to_show = $jv("#logo_url_to_show").attr('href');
        //alert(logo_url_to_show);
        /*var logo_url_to_show_array = logo_url_to_show.split("http//");
        //alert(logo_url_to_show_array.length);
        var logo_url_to_show_array_length = logo_url_to_show_array.length;
        if (logo_url_to_show_array_length == 2) {
            //alert(logo_url_to_show_array[1]);
			
            $jv("#logo_url_to_show").attr('href',"http://"+logo_url_to_show_array[1].replace("/",""));
        }
		alert("lien1 : "+logo_url_to_show_array[0]+"  lien2 :"+logo_url_to_show_array[1]);
		//alert(logo_url_to_show.substring(1));*/
		$jv("#logo_url_to_show").attr('href',logo_url_to_show.substring(1));
});

</script>

</head>

<body>
<div id="conteneur">
  <div id="header_menu">
<?php 
	//$this->load->model("mdlville") ;
	//$commercant_url_ville = $this->mdlville->getVilleById($oInfoCommercant->IdVille);
	//$commercant_url_nom_com = RemoveExtendedChars2($oInfoCommercant->NomSociete);
	//$commercant_url_nom_ville = RemoveExtendedChars2($commercant_url_ville->Nom);
	//$commercant_url_nom = $commercant_url_nom_com."_".$commercant_url_nom_ville;
	$commercant_url_nom = $oInfoCommercant->nom_url;
	$commercant_url_home = $commercant_url_nom;
	$commercant_url_presentation = $commercant_url_nom."/presentation";
	$commercant_url_infos = $commercant_url_nom."/infos";
	$commercant_url_autresinfos = $commercant_url_nom."/autresinfos";
	$commercant_url_annonces = $commercant_url_nom."/annonces";
	$commercant_url_notre_bonplan = $commercant_url_nom."/notre_bonplan";
	$commercant_url_noussituer = $commercant_url_nom."/nous_situer";
	$commercant_url_nous_contacter = $commercant_url_nom."/nous_contacter";
	?>
  <a href="<?php echo site_url($commercant_url_home);?>" <?php if ($active_link == "accueil") {?> class="header_menu_link_active" <?php } else {?> class="header_menu_link" <?php }?>>Accueil</a> <span class="menu_separator"></span>
  <a href="<?php echo site_url($commercant_url_presentation);?>" <?php if ($active_link == "presentation") {?> class="header_menu_link_active" <?php } else {?> class="header_menu_link" <?php }?>>Présentation</a> <span class="menu_separator"></span>
  
  <?php if ($oInfoCommercant->labelactivite1 != "" && $oInfoCommercant->labelactivite1 != NULL) { ?>
  <a href="<?php echo site_url($commercant_url_infos);?>" <?php if ($active_link == "activite1") {?> class="header_menu_link_active" <?php } else {?> class="header_menu_link" <?php }?>><?php echo $oInfoCommercant->labelactivite1; ?></a> <span class="menu_separator"></span>
  <?php } ?>
  
  <?php if ($oInfoCommercant->labelactivite2 != "" && $oInfoCommercant->labelactivite2 != NULL) { ?>
  <a href="<?php echo site_url($commercant_url_autresinfos);?>" <?php if ($active_link == "activite2") {?> class="header_menu_link_active" <?php } else {?> class="header_menu_link" <?php }?>><?php echo $oInfoCommercant->labelactivite2; ?></a> <span class="menu_separator"></span>
  <?php } ?>
  
  <?php if ($nombre_annonce_com != '0') { ?>
  <a href="<?php echo site_url($commercant_url_annonces);?>" <?php if ($active_link == "annonces") {?> class="header_menu_link_active" <?php } else {?> class="header_menu_link" <?php }?>>Nos annonces</a> <span class="menu_separator"></span>
  <?php } ?>
  
  <?php if (isset($oLastbonplanCom)) { ?>
  <a href="<?php echo site_url($commercant_url_notre_bonplan);?>" <?php if ($active_link == "bonplans") {?> class="header_menu_link_active" <?php } else {?> class="header_menu_link" <?php }?>>Notre Bon &nbsp;Plan</a> <span class="menu_separator"></span>
  <?php } ?>
  
  
  <a href="<?php echo site_url($commercant_url_noussituer);?>" <?php if ($active_link == "situer") {?> class="header_menu_link_active" <?php } else {?> class="header_menu_link" <?php }?>>Nous situer</a> <span class="menu_separator"></span>
  <a href="<?php echo site_url($commercant_url_nous_contacter);?>" <?php if ($active_link == "contacter") {?> class="header_menu_link_active" <?php } else {?> class="header_menu_link" <?php }?>>Nous contacter</a><span class="menu_separator"></span>

  </div>
  <div id="head_main"></div>
  <div id="main_main">
  <div id="head_info">
  	<div id="info_partenaire_name">
      	<span class="Corps-artistique-C-C1"><?php echo $oInfoCommercant->NomSociete ; ?></span><br/>
        <span class="Corps-artistique-C-C2"><?php if (isset($oInfoCommercant->titre_entete)) echo $oInfoCommercant->titre_entete ; //if (isset($oRubCom)) echo $oRubCom->Nom ; ?></span><br/>
        <span class="Corps-artistique-C-C3"><?php echo $oInfoCommercant->Adresse1 ; ?><?php if ($oInfoCommercant->Adresse2 != "") echo " - ".$oInfoCommercant->Adresse2 ; ?><?php if ($oInfoCommercant->CodePostal != "") echo " - ".$oInfoCommercant->CodePostal ; ?><?php if ($oInfoCommercant->ville != "") echo " - ".$oInfoCommercant->ville ; ?></span>
    </div>
  	<div id="social_link">
    
    <table width="100px" align="right" style="float:right; text-align:right;" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="right" style="text-align:right;">
<!-- Begin TranslateThis Button -->

<div id="translate-this"><a style="width:180px;height:18px;display:block;" class="translate-this-button" href="http://www.translatecompany.com/"><!--website translation-->&nbsp;</a></div>

<script type="text/javascript" src="http://x.translateth.is/translate-this.js"></script>
<script type="text/javascript">
TranslateThis();
</script>

<!-- End TranslateThis Button -->

        </td>
      </tr>
    </table>

    
    <table border="0" cellspacing="3" cellpadding="3" align="right">
  <tr>
    <td valign="top"><a href="mailto:<?php echo $oInfoCommercant->Login ; ?>"><img src="<?php echo GetImagePath("front/"); ?>/wp1fa8a831_06.png" width="37" height="37" alt="contact" /></a></td>
    <td>
    <?php if ($active_link == "accueil") { ?>
    	<a href="Javascript:void();">
	<?php } ?>
    <?php if ($active_link == "presentation") { ?>
    	<a href="<?php echo site_url($commercant_url_home);?>">
	<?php } ?>
    <?php if ($active_link == "activite1") { ?>
    	<a href="<?php echo site_url($commercant_url_presentation);?>">
	<?php } ?>
    <?php if ($active_link == "activite2") { ?>
		<?php if ($oInfoCommercant->labelactivite1 != "" && $oInfoCommercant->labelactivite1 != NULL) { ?>
        	<a href="<?php echo site_url($commercant_url_infos);?>">
        <?php } else {?>
        	<a href="<?php echo site_url($commercant_url_presentation);?>">
        <?php } ?>
	<?php } ?>
    <?php if ($active_link == "annonces") { ?>
        <?php if ($oInfoCommercant->labelactivite2 != "" && $oInfoCommercant->labelactivite2 != NULL) { ?>
        	<a href="<?php echo site_url($commercant_url_autresinfos);?>">
		<?php } else if ($oInfoCommercant->labelactivite1 != "" && $oInfoCommercant->labelactivite1 != NULL) { ?>
        	<a href="<?php echo site_url($commercant_url_infos);?>">
        <?php } else {?>
        	<a href="<?php echo site_url($commercant_url_presentation);?>">
        <?php } ?>
	<?php } ?>
    <?php if ($active_link == "bonplans") { ?>
        <?php if ($nombre_annonce_com != '0') { ?>
            <a href="<?php echo site_url($commercant_url_annonces);?>">
        <?php } else if ($oInfoCommercant->labelactivite2 != "" && $oInfoCommercant->labelactivite2 != NULL) { ?>
        	<a href="<?php echo site_url($commercant_url_autresinfos);?>">
		<?php } else if ($oInfoCommercant->labelactivite1 != "" && $oInfoCommercant->labelactivite1 != NULL) { ?>
        	<a href="<?php echo site_url($commercant_url_infos);?>">
        <?php } else {?>
        	<a href="<?php echo site_url($commercant_url_presentation);?>">
        <?php } ?>
	<?php } ?>
    <?php if ($active_link == "situer") { ?>
        <?php if (isset($oLastbonplanCom)) { ?>
        	<a href="<?php echo site_url($commercant_url_notre_bonplan);?>">
        <?php } else if ($nombre_annonce_com != '0') { ?>
            <a href="<?php echo site_url($commercant_url_annonces);?>">
        <?php } else if ($oInfoCommercant->labelactivite2 != "" && $oInfoCommercant->labelactivite2 != NULL) { ?>
        	<a href="<?php echo site_url($commercant_url_autresinfos);?>">
		<?php } else if ($oInfoCommercant->labelactivite1 != "" && $oInfoCommercant->labelactivite1 != NULL) { ?>
        	<a href="<?php echo site_url($commercant_url_infos);?>">
        <?php } else {?>
        	<a href="<?php echo site_url($commercant_url_presentation);?>">
        <?php } ?>
	<?php } ?>
    <?php if ($active_link == "contacter") { ?>
    	<a href="<?php echo site_url($commercant_url_noussituer);?>">
	<?php } ?>
    <img src="<?php echo GetImagePath("front/"); ?>/wp7265eaec_06.png" width="37" height="37" alt="link_back" />
    </a>
    </td>
    <td>
    <?php if ($active_link == "accueil") { ?>
    	<a href="<?php echo site_url($commercant_url_presentation);?>">
	<?php } ?>
    <?php if ($active_link == "presentation") { ?>
    	<?php if ($oInfoCommercant->labelactivite1 != "" && $oInfoCommercant->labelactivite1 != NULL) { ?>
        	<a href="<?php echo site_url($commercant_url_infos);?>">
        <?php } else if ($oInfoCommercant->labelactivite2 != "" && $oInfoCommercant->labelactivite2 != NULL) { ?>
        	<a href="<?php echo site_url($commercant_url_autresinfos);?>">
        <?php } else if ($nombre_annonce_com != '0') { ?>
            <a href="<?php echo site_url($commercant_url_annonces);?>">
        <?php } else if (isset($oLastbonplanCom)) { ?>
        	<a href="<?php echo site_url($commercant_url_notre_bonplan);?>">
        <?php } else {?>
        	<a href="<?php echo site_url($commercant_url_noussituer);?>">
        <?php } ?>
	<?php } ?>
    <?php if ($active_link == "activite1") { ?>
        <?php if ($oInfoCommercant->labelactivite2 != "" && $oInfoCommercant->labelactivite2 != NULL) { ?>
        	<a href="<?php echo site_url($commercant_url_autresinfos);?>">
        <?php } else if ($nombre_annonce_com != '0') { ?>
            <a href="<?php echo site_url($commercant_url_annonces);?>">
        <?php } else if (isset($oLastbonplanCom)) { ?>
        	<a href="<?php echo site_url($commercant_url_notre_bonplan);?>">
        <?php } else {?>
        	<a href="<?php echo site_url($commercant_url_noussituer);?>">
        <?php } ?>
	<?php } ?>
    <?php if ($active_link == "activite2") { ?>
        <?php if ($nombre_annonce_com != '0') { ?>
            <a href="<?php echo site_url($commercant_url_annonces);?>">
        <?php } else if (isset($oLastbonplanCom)) { ?>
        	<a href="<?php echo site_url($commercant_url_notre_bonplan);?>">
        <?php } else {?>
        	<a href="<?php echo site_url($commercant_url_noussituer);?>">
        <?php } ?>
	<?php } ?>
    <?php if ($active_link == "annonces") { ?>
        <?php if (isset($oLastbonplanCom)) { ?>
        	<a href="<?php echo site_url($commercant_url_notre_bonplan);?>">
        <?php } else {?>
        	<a href="<?php echo site_url($commercant_url_noussituer);?>">
        <?php } ?>
	<?php } ?>
    <?php if ($active_link == "bonplans") { ?>
    	<a href="<?php echo site_url($commercant_url_noussituer);?>">
	<?php } ?>
    <?php if ($active_link == "situer") { ?>
    	<a href="<?php echo site_url($commercant_url_nous_contacter);?>">
	<?php } ?>
    <?php if ($active_link == "contacter") { ?>
    	<a href="Javascript:void();">
	<?php } ?>
    <img src="<?php echo GetImagePath("front/"); ?>/wp0349ccd6_06.png" width="37" height="37" alt="link_next" />
    </a>
    </td>
    <?php if ($oInfoCommercant->Facebook != "" && $oInfoCommercant->Facebook != NULL) {?>
    <td valign="top"><a href="<?php echo $oInfoCommercant->Facebook ; ?>" target="_blank"><img src="<?php echo GetImagePath("front/"); ?>/wpd01d06ac_06.png" width="37" height="37" alt="link_facebook" /></a></td>
    <?php } else {}?>
    <?php if ($oInfoCommercant->Twitter != "" && $oInfoCommercant->Twitter != NULL) {?>
    <td valign="top"><a href="<?php echo $oInfoCommercant->Twitter ; ?>" target="_blank"><img src="<?php echo GetImagePath("front/"); ?>/wp447ecec3_06.png" width="37" height="37" alt="link_twet" /></a></td>
    <?php } else {}?>
  </tr>
</table>

    </div>
  </div>