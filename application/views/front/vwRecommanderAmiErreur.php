<?php $data["zTitle"] = 'Accueil' ?>




<?php if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) { 

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN" "http://www.openmobilealliance.org/tech/DTD/xhtml-mobile11.dtd">-->
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="Generator" content="Serif WebPlus X5">
<meta http-equiv="X-UA-Compatible" content="IE=EmulationIE8">
<title>Formulaire mobile</title>
<meta content="minimum-scale=1.0, width=device-width" name="viewport">
 <!--Master Page Head-->
<style type="text/css">
body {margin: 0px; padding: 0px;}
.Corps-artistique-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:left; font-weight:400;
}
.Corps-P
{
    margin:0.0px 0.0px 12.0px 0.0px; text-align:justify; font-weight:400;
}
.Corps-P-P0
{
    margin:0.0px 0.0px 4.0px 0.0px; text-align:center; font-weight:400;
}
.Corps-artistique-C
{
    font-family:"Arial", sans-serif; font-weight:700; color:#3653c1; font-size:19.0px; line-height:1.21em;
}
.Corps-artistique-C-C0
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:13.0px; line-height:1.23em;
}
.Corps-artistique-C-C1
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:12.0px; line-height:1.25em;
}
.Corps-C
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
.Corps-artistique-C-C2
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C0
{
    font-family:"Verdana", sans-serif; font-size:16.0px; line-height:1.13em;
}
.Corps-C-C1
{
    font-family:"Arial", sans-serif; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C2
{
    font-family:"Arial", sans-serif; text-decoration:underline; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C3
{
    font-family:"Arial", sans-serif; text-decoration:underline; font-size:12.0px; line-height:1.25em;
}
</style>
<script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/jspngfix.js"></script>
<link rel="stylesheet" href="formulairemobile_fichiers/wpstyles.css" type="text/css">
<script type="text/javascript">var blankSrc = "wpscripts/blank.gif";
</script>

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.6.4.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery("#submitbtn").click(function(){
            var txtError = "";
            
            var zNom = jQuery("#zNom").val();
            if(zNom=="" || zNom=="Votre nom*") {
                txtError += "- Veuillez indiquer Votre nom <br/>";    
            }
            
            var zEmail = jQuery("#zEmail").val();
            if(!isEmail(zEmail)) {
                txtError += "- Veuillez saisir un email valide <br/>";
            } 
                        
            var zTelephone = jQuery("#zTelephone").val();
            if(zTelephone=="" || zTelephone=="Votre numéro de téléphone*") {
                txtError += "- Veuillez indiquer votre numéro de téléphone <br/>";    
            }
            
            var zCommentaire = jQuery("#zCommentaire").val();
            if(zCommentaire=="" || zCommentaire=="Votre message*") {
                txtError += "- Veuillez indiquer votre message <br/>";    
            }
            
            
            jQuery("#divErrortxtmobile").html(txtError);
            
            if(txtError == "") {
                jQuery("#frmNousContacter").submit();
            }
        });
		
    })
</script>

</head>

<body style="background-color: transparent; text-align: center; height: 2000px;" text="#000000">
<div style="background-color:#cccccc;text-align:left;margin-left:auto;margin-right:auto;position:relative;width:320px;height:2000px;">
<img src="<?php echo GetImagePath("front/"); ?>/wp9b4ac3b7_06.png" id="qs_880" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 15px; top: 290px;" height="770" border="0" width="290">

<!--start main info menu-->
<?php $this->load->view("front/vwTitreinfocommercantMobile", $data);?>
<!--end main info menu-->

<!--start main icone menu 2-->
<?php $this->load->view("front/vwMenumainiconeMobile_noslide", $data);?>
<!--end main icone menu 2-->



<img src="<?php echo GetImagePath("front/"); ?>/wp51f75006_06.png" id="qs_1664" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 0px; top: 95px;" height="5" border="0" width="320">
<img src="<?php echo GetImagePath("front/"); ?>/wp67b9ee2f_06.png" id="grp_857" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 249px; top: 12px;" height="78" border="0" width="57">



<div id="txt_761" style="position:absolute;left:30px;top:339px;width:265px;height: auto;overflow:hidden; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height: 1.25em;">
  <p class="Corps-artistique-P"><span class="Corps-artistique-C-C2">Merci de bien vouloir préciser vos coordonnées et votre demande</span></p>
  <p class="Corps-P"><span class="Corps-C">* champs obligatoires</span></p>

  <form name="frmNousContacter" id="frmNousContacter" action="<?php echo site_url("front/annonce/envoiMailNousContacter"); ?>" method="post" enctype="multipart/form-data"> 
  <table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td colspan="2"><input type="text" name="zNom" id="zNom" style="width:98%;" value="Votre nom*" onfocus="if (this.value=='Votre nom*') this.value='';" onblur="if (this.value=='') this.value='Votre nom*';"/>
		<input type="hidden" name="zMailTo" id="zMailTo" value="<?php echo $oInfoCommercant->Email ; ?>"/></td>
  </tr>
  <tr>
    <td colspan="2"><input type="text" name="zEmail" id="zEmail" style="width:98%;" value="Votre courriel*" onfocus="if (this.value=='Votre courriel*') this.value='';" onblur="if (this.value=='') this.value='Votre courriel*';"/></td>
  </tr>
  <tr>
    <td colspan="2"><input type="text" name="zTelephone" id="zTelephone" style="width:98%;" value="Votre numéro de téléphone*" onfocus="if (this.value=='Votre numéro de téléphone*') this.value='';" onblur="if (this.value=='') this.value='Votre numéro de téléphone*';"/></td>
  </tr>
  <tr>
    <td colspan="2"><textarea name="zCommentaire" id="zCommentaire" cols="45" rows="5" style="width:98%;" onfocus="if (this.value=='Votre message*') this.value='';" onblur="if (this.value=='') this.value='Votre message*';">Votre message*</textarea></td>
  </tr>
  <tr>
    <td colspan="2"><input name="checkmail" type="checkbox" value="" /> J'accepte de recevoir par courriel des informations provenant de cet &eacute;tablis-sement.</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><input name="resetbtn" id="resetbtn" type="reset" value="R&eacute;tablir" /></td>
    <td align="right"><input name="submitbtn" id="submitbtn" type="button" value="Envoyer" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
 
<div id="divErrortxtmobile" style="width:290px;height: auto; text-align:left; color:#F00;"></div> 
</div>



<div id="txt_632" style="position:absolute;left:15px;top:1080px;width:290px;height:550px;overflow:hidden; text-align:center">
  <!--start main footer menu-->
  <?php $this->load->view("front/vwMenumainfooterMobile", $data);?>
  <!--end main footer menu-->
</div>


</div>

</body></html>



<?php
} else { 

?>


<?php $this->load->view("front/includes/vwHeaderPartenaire", $data); ?>

<style type="text/css">

#frmNousContacter label.error {
	margin-left: 10px;
	width: auto;
	display: inline;
	color:#F00;
}

</style>

<div id="main_body">
    <div id="main_left">
    	<?php $this->load->view("front/vwContactPartenaire", $data); ?> 
                
        <?php $this->load->view("front/vwLiendonneesPartenaire", $data); ?>
        <?php $this->load->view("front/vwLienannoncePartenaire", $data); ?>
        <div id="separator_left"></div>
        <?php $this->load->view("front/vwLienbonplanPartenaire", $data); ?>
        
    </div>
    <div id="main_right">
      <?php $this->load->view("front/vwSlidePartenaire", $data); ?> 
      <div id="main_data">
        <div id="main_data_head">Recommander &agrave; un amis</div>
        <div id="main_data_body">
        	<br/>
            Un probl&egrave;me est survenu, veuillez renvoyer le mail !
            
        
        
        </div>
        <div id="main_data_footer"></div>
      </div>
    </div>
  </div>



<?php $this->load->view("front/includes/vwFooterPartenaire"); ?>


<?php 

}
?>