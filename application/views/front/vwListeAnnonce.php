<?php $data["zTitle"] = 'Annonces' ?>
<?php $this->load->view("front/includes/vwHeader", $data); ?>
<?php $this->load->view("front/includes/vwMenu"); ?>
<script type="text/javascript">
	function lookup(inputString) {
		if(inputString.length == 0) {
			// Hide the suggestion box.
			jQuery('#suggestions').hide();
		} else {		
			jQuery.post("<?php echo site_url('front/autocomplete/indexlisteCategorie/'); ?>/"+inputString+" " , {queryString: ""+inputString+""}, function(data){				
				if(data.length >0) { 
					jQuery('#suggestions').show();
					jQuery('#autoSuggestionsList').html(data);
				}
			});
		}
	} 
		
	function fill(thisValue,id) {
		jQuery('#inputString').val(thisValue);		
		jQuery('#inputStringHidden').val(id);	
							
		setTimeout("jQuery('#suggestions').hide();", 500);
	}
	function lookupCommercant(inputString) {
		if(inputString.length == 0) {
			// Hide the suggestion box.
			jQuery('#suggestionsCommercant').hide();
		} else {		
			jQuery.post("<?php echo site_url('front/autocomplete/indexlisteCommercant/'); ?>/"+inputString+" " , {queryString: ""+inputString+""}, function(data){				
				if(data.length >0) { 
					jQuery('#suggestionsCommercant').show();
					jQuery('#autoSuggestionsListCommercant').html(data);
				}
			});
		}
	} 
		
	function fillCommercant(thisValue,id) {
		jQuery('#inputStringCommercant').val(thisValue);		
		jQuery('#inputStringCommercantHidden').val(id);	
							
		setTimeout("jQuery('#suggestionsCommercant').hide();", 500);
	}
</script>

<style type="text/css">
<!--
#mainbody_header {
	background-image: url(<?php echo GetImagePath("front/"); ?>/bg_mainbody_header.png);
	background-repeat: no-repeat;
	background-position: left;
	float: left;
	height: 133px;
	width: 100%;
}
#id_mainbody_main {
	background-image: url(<?php echo GetImagePath("front/"); ?>/bg_mainbody_main.png);
	background-repeat: repeat-y;
	float: left;
	width: 100%;
	position: relative;
}
#id_mainbody_footer {
	background-image: url(<?php echo GetImagePath("front/"); ?>/bg_mainbody_footer.png);
	background-repeat: no-repeat;
	float: left;
	height: 24px;
	width: 100%;
	position: relative;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	padding-left: 30px;
}
#id_mainbody_mainrepete {
	background-image: url(<?php echo GetImagePath("front/"); ?>/bg_mainbody_separator.png);
	background-repeat: no-repeat;
	background-position: left bottom;
	float: left;
	width: 100%;
	position: relative;
	font-family: "Arial",sans-serif;
    font-size: 12px;
}
#id_bp_minitext {
	background-repeat: no-repeat;
	background-position: center;
	float: left;
	width: 185px;
	position: relative;
	height: 109px;
	background-image: url(<?php echo GetImagePath("front/"); ?>/wpcda8dfe6_06.png);
	color: #000;
	font-family: "Arial", sans-serif;
	font-size: 12px;
	font-weight: normal;
	padding-top: 20px;
	padding-left: 35px;
	padding-right: 10px;
}

.status_btn {
	height: 43px;
	padding-top: 50px;
}
-->
</style>

<!--start main menu bloc-->
<map id="map0" name="map0">
    <area shape="rect" coords="24,172,89,245" href="<?php echo site_url();?>" alt="">
    <area shape="rect" coords="102,173,166,245" href="javascript:history.back()" alt="">
    <area shape="rect" coords="182,173,246,245" href="<?php echo site_url('front/bonplan/'); ?>" alt="">
    <area shape="rect" coords="259,173,323,245" href="<?php echo site_url("front/contact");?>" alt="">
</map>
<!--end main menu bloc-->

<!--start menu bloc for home-->
<a href="<?php echo site_url();?>"><img src="<?php echo GetImagePath("front/"); ?>/wpb2ca9cf5_06.png" id="pic_1575" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 15px; top: 265px; z-index:1000;" width="187" border="0" height="273"></a>

<img src="<?php echo GetImagePath("front/"); ?>/wpf72bcd47_06.png" id="pic_1573" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 217px; top: 264px;" height="273" border="0" width="187">

<map id="map1" name="map1">
    <area shape="poly" coords="181,272,187,263,187,43,175,0,7,0,0,18,0,230,12,273,155,273" href="<?php echo site_url('front/bonplan/'); ?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wp9326ed95_06.png" id="pic_1506" title="" alt="bons plans" onload="OnLoadPngFix()" usemap="#map1" style="position: absolute; left: 418px; top: 265px;" height="273" border="0" width="187">

<map id="map4" name="map4">
  <area shape="poly" coords="182,272,188,263,188,43,176,0,8,0,1,18,1,230,13,273,156,273" href="<?php echo site_url("front/cadeau");?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wpc783aaac_06.png" id="pic_1510" title="" alt="fonctionnementpointscadeaux" onload="OnLoadPngFix()" usemap="#map4" style="position: absolute; left: 620px; top: 265px;" height="273" border="0" width="187">

<!--end menu bloc for home-->


<!--start main body bloc-->

<div id="main_body_bloc" style="position: absolute; left: 15px; top: 560px; height:auto; width:792px" border="0">
  <form name="frmRechercheAnnonce" method="post" action="<?php echo site_url('front/annonce/index/'); ?>">
  <div id="mainbody_header">
  	<div style="margin-left:10px; margin-top:15px;">
    <table width="100%" border="0" cellspacing="5" cellpadding="0">
      <tr>
        <td><span class="Normal-C">Recherche par th&egrave;mes</span></td>
        <td><span class="Normal-C">Recherche par d&eacute;positaires</span></td>
      </tr>
      <tr>
        <td>
        	<select type="inputStringHidden" name="inputStringHidden" style="width:366px">
                <option value="0">Selectionner cat&eacute;gorie</option>
                <?php foreach($toSousRubrique as $oSousRubrique){ ?>
                    <option value="<?php echo $oSousRubrique->IdSousRubrique ; ?>" <?php if(isset($iCategorieId) && $iCategorieId == $oSousRubrique->IdSousRubrique) echo 'selected="selected"'; ?>><?php echo $oSousRubrique->Nom ." (".$oSousRubrique->nb_annonce.")"; ?></option>
                <?php } ?>
            </select>
        </td>
        <td>
        	<select type="inputStringCommercantHidden" name="inputStringCommercantHidden" style="width:366px">
                <option value="0">Selectionner diffuseur</option>
                <?php foreach($toCommercant as $oCommercant){ ?>
                    <option value="<?php echo $oCommercant->IdCommercant ; ?>" <?php if(isset($iCommercantId) && $iCommercantId == $oCommercant->IdCommercant) echo 'selected="selected"'; ?>><?php echo $oCommercant->NomSociete." (".$oCommercant->annonce_nb.")" ; ?></option>
                <?php } ?>
            </select>
        </td>
      </tr>
      <tr>
        <td><span class="Normal-C">Recherche par mots cl&eacute;s</span></td>
        <td rowspan="2">
        	<div style="text-align:right; margin-top:10px; margin-right:10px;">
            <span class="Normal-C" style=" float:left; margin-top:10px;">Etat&nbsp;
            <select name="iEtat" id="iEtat">
                <option value="" <?php if(isset($iEtat) && $iEtat == "") echo 'selected="selected"'; ?>> -------- </option>
                <option value="0" <?php if(isset($iEtat) && $iEtat == "0") echo 'selected="selected"'; ?>>Revente</option>
                <option value="1" <?php if(isset($iEtat) && $iEtat == "1") echo 'selected="selected"'; ?>>Neuf</option>
                <option value="2" <?php if(isset($iEtat) && $iEtat == "2") echo 'selected="selected"'; ?>>Service</option>
            </select>
            </span>
            
           <a href="Javascript:void();" onclick="document.frmRechercheAnnonce.submit();"><img src="<?php echo GetImagePath("front/"); ?>/wpa9e8bbcd_06.png" alt="" name="pic_1504" width="106" height="30" border="0" id="pic_1504" style="" onload="OnLoadPngFix()"></a>
		  <a href="Javascript:void();" onclick="document.location='<?php echo site_url("front/annonce/index/"); ?>' ;"><img src="<?php echo GetImagePath("front/"); ?>/wpc625ec40_06.png" alt="" name="pic_1505" width="106" height="30" border="0" id="pic_1505" style="" onload="OnLoadPngFix()"></a>
        </div>
            
        </td>
      </tr>
      <tr>
        <td>
        	<input type="text" name="zMotCle" id="zMotCle" style="width:362px;" value="<?php if(isset($zMotCle)) echo $zMotCle; ?>"/>
        </td>
       
      </tr>
    </table>
	</div>
  </div>
  </form>
  
  <div id="id_mainbody_main">
  	<?php foreach($toListeAnnonce as $oListeAnnonce){ ?>
    <?php 
	if ($oListeAnnonce->etat == 1) $zEtat = "Neuf";
	else if ($oListeAnnonce->etat == 0) $zEtat = "Revente";
	else if ($oListeAnnonce->etat == 2) $zEtat = "Service";
	else $zEtat = "";
	?>
    <div id="id_mainbody_mainrepete">
		    
<table width="100%" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td style="width:210px">
            <?php if($oListeAnnonce->photo != "" && file_exists("application/resources/front/images/".$oListeAnnonce->photo)== true) { 
				echo image_thumb("application/resources/front/images/".$oListeAnnonce->photo, 210, 210,'','');
			} else {
				 ?>
             <img src="<?php echo GetImagePath("front/") . "/wp71b211d2_06.png" ;?>" width="210" border="0" id="pic_965" name="pic_965" title="" alt="">    
              <?php } ?>   
            </td>
            <td>
                <div style="margin-left:10px;">
                <span style="font-size:13px;"><strong>
                <?php echo $oListeAnnonce->rubrique ;?><br /></strong>
                <?php echo $oListeAnnonce->texte_courte ; ?><br /></span>
                <?php if ($oListeAnnonce->prix_vente!=0) {?><strong><p>Prix de vente : <?php echo $oListeAnnonce->prix_vente ; ?> &euro;<br /><?php }?>
                <?php if ($zEtat!="") {?>Etat : <?php echo $zEtat ; ?><?php } ?></p></strong>
                </div>
            </td>
            <td style="width:230px">
              <div id="id_bp_minitext">
                  <strong><?php echo $oListeAnnonce->NomSociete ; ?></strong><br />
                  <?php echo $oListeAnnonce->rue ; ?>&nbsp;<?php echo $oListeAnnonce->quartier ; ?><br />
                  <?php echo $oListeAnnonce->ville ; ?><br />
                  <?php
                        $iNombreAnnonceParDiffuseur = 0 ;
                        if(isset($oListeAnnonce->IdCommercant) ){
                            if($oListeAnnonce->IdCommercant != null && $oListeAnnonce->IdCommercant != 0  &&   $oListeAnnonce->IdCommercant != ""){
                            $iNombreAnnonceParDiffuseur = $mdlannonce->nombreAnnonceParDiffuseur($oListeAnnonce->IdCommercant) ;
                            }
                        }
                    ?>
                  <?php echo $iNombreAnnonceParDiffuseur ; ?> annonces ont &eacute;t&eacute; d&eacute;pos&eacute;es &nbsp;par ce diffuseur<br />
                </div>
            </td>
            <td align="center" style="width:70px;">
            <p class="status_btn"><a href="<?php echo site_url("front/annonce/detailAnnonce/" . $oListeAnnonce->annonce_id);?>"><img src="<?php echo GetImagePath("front/"); ?>/wp68233b54_06.png" id="pic_1526" alt="" onload="OnLoadPngFix()" height="50" border="0" width="50" style="text-align:center"></a></p>
            </td>
          </tr>
          
          
        </table>

    </div>
    <?php  } ?>
  </div>
  <div id="id_mainbody_footer">
  	<div>
  	<?php
        if($iNombreLiens != 1){
            ?>Pages : <?php
            $iPageCourante = $argOffset ;
            for($i=1; $i<=$iNombreLiens; $i++){
                $argOffset = ($i - 1 ) * $PerPage ;
                ?>
                <?php if($iPageCourante == $argOffset){ ?>
                    <span style="cursor:pointer; color:red; font-weight: bold;" onclick="document.location='<?php echo site_url("front/annonce/redirection/". $argOffset) ; ?>' ;">
                            <?php echo $i ; ?>
                    </span>&nbsp;
                <?php }else{?>
                    <span style="cursor:pointer;text-decoration: underline" onclick="document.location='<?php echo site_url("front/annonce/redirection/". $argOffset) ; ?>' ;">
                            <?php echo $i ; ?>
                    </span>&nbsp;
                <?php }?>
                <?php
            }
        }
    ?>
  </div>
  </div>
  
  
  
  
  
  <div id="id_mainbody_copyrig" style="width:100%;height:73px; position:relative; float:left; margin-top:40px;">
    <div class="Normal-P-P0">
        <span class="Normal-C-C0">Le &nbsp;Club Proximit&eacute; est une marque d&eacute;pos&eacute;e et une r&eacute;alisation du Magazine Proximit&eacute;<br></span></div>
    <div class="Corps-P-P0">
        <span class="Corps-C-C6"> Editeur : Phase SARL 60 avenue de Nice 06800 Cagnes-<wbr>sur-<wbr>Mer &nbsp;</span><span class="Corps-C-C7">Sarl au capital de 7265 &euro; -<wbr> RCS Antibes 412.071.466<br></span><span class="Corps-C">T&eacute;l&eacute;phone : 00 33 (0)4 93 73 84 51 -<wbr> Mobile : 00 33 (0)6 72 05 59 35 -<wbr> proximite-<wbr>magazine@orange.fr</span></div>
</div>
  
  
</div>
<!--end main body bloc-->



<?php $this->load->view("front/includes/vwFooter"); ?>
