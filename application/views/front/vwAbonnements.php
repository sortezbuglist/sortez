<?php $data["zTitle"] = 'Abonnements' ?>
<?php $this->load->view("front/includes/vwHeader", $data); ?>
<?php $this->load->view("front/includes/vwMenu"); ?>
<style type="text/css">
<!--
body {margin: 0px; padding: 0px;}
a:link {color: #3e550e;}
a:visited {color: #8e9165;}
a:hover {color: #3e550e;}
a:active {color: #3e550e;}
.Corps-P
        {
        margin:0.0px 0.0px 5.0px 0.0px; text-align:justify; font-weight:400; 
        }
.Corps-P0
        {
        margin:0.0px 0.0px 0.0px 0.0px; text-align:left; font-weight:400; 
        }
.Corps-P1
        {
        margin:0.0px 0.0px 0.0px 0.0px; text-align:justify; font-weight:400; 
        }
.Corps-P2
        {
        margin:0.0px 0.0px 12.0px 0.0px; text-align:left; font-weight:400; 
        }
.Corps-C
        {
        font-family:"Arial", sans-serif; font-weight:700; font-size:15.0px; 
        line-height:1.20em; 
        }
.Corps-C0
        {
        font-family:"Arial", sans-serif; font-size:13.0px; line-height:1.23em; 
        }
.Corps-C1
        {
        font-family:"Capture it", serif; font-size:40.0px; line-height:1.13em; 
        }
.Normal-C
        {
        font-family:"Capture it", serif; font-size:21.0px; line-height:1.14em; 
        color:#ffffff; 
        }
.Corps-C2
        {
        font-family:"Arial", sans-serif; font-weight:700; font-size:5.0px; 
        line-height:1.20em; 
        }
.Corps-C3
        {
        font-family:"Arial", sans-serif; font-weight:700; font-size:19.0px; 
        line-height:1.21em; color:#a60000; 
        }
.Corps-C4
        {
        font-family:"Arial", sans-serif; font-size:5.0px; line-height:1.20em; 
        }
.Corps-C5
        {
        font-family:"Arial", sans-serif; font-weight:700; font-size:13.0px; 
        line-height:1.23em; color:#a60000; 
        }
.Corps-C6
        {
        font-family:"Arial", sans-serif; font-weight:700; font-size:21.0px; 
        line-height:1.19em; 
        }
.Corps-C7
        {
        font-family:"Arial", sans-serif; font-weight:700; font-size:16.0px; 
        line-height:1.25em; 
        }
.Corps-C8
        {
        font-family:"Arial", sans-serif; font-weight:700; font-size:13.0px; 
        line-height:1.23em; 
        }
.Corps-C9
        {
        font-family:"Verdana", sans-serif; font-size:16.0px; 
        line-height:1.13em; 
        }
.Corps-C10
        {
        font-family:"Arial", sans-serif; font-style:italic; font-size:12.0px; 
        line-height:1.25em; 
        }
.Normal-C0
        {
        font-family:"Capture it", serif; font-size:24.0px; line-height:1.17em; 
        color:#ffffff; 
        }
.Normal-C1
        {
        font-family:"Arial", sans-serif; font-weight:700; font-size:16.0px; 
        line-height:1.25em; color:#ffffff; 
        }
-->
</style>
<div style="background-color:#ffffff;width:1024px;height:3500px;">
<div style="position:absolute; left:0px; top:241px; width:246px; height:34px;">
    <a href="http://www.proximite-magazine.com"><img src="<?php echo GetImagePath("front/"); ?>/wp5533b116.gif" width="246" height="34" border="0" id="hs_10" name="hs_10" title="" alt=""></a></div>
<div style="position:absolute; left:20px; top:2604px; width:231px; height:246px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpdcc8f2fc.png" width="231" height="246" border="0" id="pic_7" name="pic_7" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:20px; top:1346px; width:220px; height:280px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wp05442c65.png" width="220" height="280" border="0" id="pic_86" name="pic_86" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:11px; top:321px; width:789px; height:524px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wp9d0400a3.png" width="789" height="524" border="0" id="qs_1" name="qs_1" title="" alt="" onload="OnLoadPngFix()"></div>
<div id="txt_1" style="position:absolute; left:250px; top:450px; width:540px; height:301px;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box; overflow:hidden;">
<p class="Corps-P"><span class="Corps-C">Créer, modifier gratuitement votre fiche : </span></p>
<p class="Corps-P"><span class="Corps-C0">Enseigne, adresse, ville, téléphone, code postal, téléphone fixe, téléphone mobile,
    courriel, site internet, intégration d’une photo, plan de localisation appartenance
    à une association, présence du détail de leur activité (mots clés et petit article
    de présentation).</span></p>
<p class="Corps-P"><span class="Corps-C">Pour en bénéficier, vous devez souscrire un abonnement annuel “Annuaire de base”.</span></p>
<p class="Corps-P"><span class="Corps-C0">Il vous suffit de vous inscrire gratuitement en ligne en remplissant le formulaire
    (voir lien ci-<wbr>dessous)</span></p>
<p class="Corps-P"><span class="Corps-C0">Vous recevrez immédiatement vos identifiants par mail.</span></p>
<p class="Corps-P"><span class="Corps-C0">Vous accédez alors à votre compte avec la mise à disposition d’une page admin personnalisée
    &nbsp;: vous créez, modifiez et supprimez vos données quand vous le désirez.</span></p>
<p class="Corps-P"><span class="Corps-C">Votre abonnement gratuit est valable 12 mois.</span></p>
<p class="Corps-P"><span class="Corps-C">votre présence &nbsp;sur nos applications mobiles Iphone et Android</span></p>
</div>
<div id="txt_33" style="position:absolute; left:318px; top:328px; width:482px; height:106px;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box; overflow:hidden;">
<p class="Corps-P0"><span class="Corps-C1">L’annuaire</span></p>
<p class="Corps-P0"><span class="Corps-C1">de IBase</span></p>
</div>
<div style="position:absolute; left:10px; top:264px; width:600px; height:49px;">
    <div class="Corps-P0">
        <span class="Corps-C1">Les différents aibonnements</span></div>
</div>
<div style="position:absolute; left:250px; top:719px; width:540px; height:36px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wp33083e9e.png" width="540" height="36" border="0" id="qs_35" name="qs_35" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:274px; top:725px; width:301px; height:26px;">
    <div class="Corps-P0">
        <span class="Normal-C">LES &nbsp;CONDITIONS GéNéRALES</span></div>
</div>
<div style="position:absolute; left:759px; top:719px; width:31px; height:63px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpc7f3746c.png" width="31" height="63" border="0" id="art_38" name="art_38" title="" alt="^" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:810px; top:532px; width:203px; height:590px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpaafd1efe.gif" width="203" height="590" border="0" id="agif2" name="agif2" title="" alt=""></div>
<div style="position:absolute; left:808px; top:1134px; width:203px; height:500px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpa50c3c2b.png" width="203" height="500" border="0" id="pic_44" name="pic_44" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:250px; top:299px; width:103px; height:170px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpc0db4475.png" width="103" height="170" border="0" id="art_23" name="art_23" title="" alt="1" onload="OnLoadPngFix()"></div>
<div id="txt_35" style="position:absolute; left:318px; top:879px; width:405px; height:124px;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box; overflow:hidden;">
<p class="Corps-P0"><span class="Corps-C1">LES PARTENAIRES</span></p>
<p class="Corps-P0"><span class="Corps-C1">DU CLUIB</span></p>
</div>
<div style="position:absolute; left:10px; top:847px; width:790px; height:2390px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wp7be3e5ae.png" width="790" height="2390" border="0" id="art_24" name="art_24" title="" alt="2" onload="OnLoadPngFix()"></div>
<div id="txt_36" style="position:absolute; left:250px; top:1006px; width:540px; height:1816px;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box; overflow:hidden;">
<p class="Corps-P"><span class="Corps-C">Les professionnels qui désirent développer davantage leur présence sur notre annuaire
    professionnel adhérerent au Club en qualité de partenaire.</span></p>
<p class="Corps-P"><span class="Corps-C2">&nbsp;</span></p>
<p class="Corps-P1"><span class="Corps-C3">Premier avantage : </span></p>
<p class="Corps-P1"><span class="Corps-C3">une information plus précise et détaillée </span></p>
<p class="Corps-P1"><span class="Corps-C4">&nbsp;</span></p>
<p class="Corps-P1"><span class="Corps-C0">Vous complétez les informations de l’annuaire de base par une galerie photos, une
    vidéo, un article informatif plus important, vos horaires d’ouverture, vos dates
    de fermeture pour congés, vos éventuelles conditions de fidélisation pratiquées dans
    votre magasin ou structure, le téléchargement d’un document commercial en PDF, vos
    liens facebook, twitter ou myspace.</span></p>
<p class="Corps-P1"><span class="Corps-C5">&nbsp;</span></p>
<p class="Corps-P1"><span class="Corps-C3">&nbsp;</span></p>
<p class="Corps-P1"><span class="Corps-C3">&nbsp;</span></p>
<p class="Corps-P1"><span class="Corps-C3">&nbsp;</span></p>
<p class="Corps-P1"><span class="Corps-C3">&nbsp;</span></p>
<p class="Corps-P1"><span class="Corps-C3">&nbsp;</span></p>
<p class="Corps-P1"><span class="Corps-C3">&nbsp;</span></p>
<p class="Corps-P1"><span class="Corps-C3">Deuxième avantage : un positionnement privilégié sur l’annuaire professionnel</span></p>
<p class="Corps-P"><span class="Corps-C0">Sur la page de recherche de notre annuaire professionnel sur notre site web et sur
    nos applications mobiles, les fiches des partenaires du club sont positionnés en
    premier, suivront les fiches de base.</span></p>
<p class="Corps-P"><span class="Corps-C4">&nbsp;</span></p>
<p class="Corps-P"><span class="Corps-C3">Troisième avantage : une présence sur nos applications mobiles</span></p>
<p class="Corps-P"><span class="Corps-C0">L’ensemble de vos coordonnées se retrouve également sur nos applications mobiles
    (Iphone et Android)</span></p>
<p class="Corps-P"><span class="Corps-C4">&nbsp;</span></p>
<p class="Corps-P"><span class="Corps-C3">Quatrième avantage : une communication cohérente</span></p>
<p class="Corps-P"><span class="Corps-C0">En qualité de partenaire, ils bénéficient de l’envois newsletters du Club auprès
    des adhérents du Club.</span></p>
<p class="Corps-P"><span class="Corps-C0">En qualité de partenaire, ils bénéficient de la promotion du Club sur les éditions
    du Magazine proximité et sur tout les autres supports et actions.</span></p>
<p class="Corps-P"><span class="Corps-C2">&nbsp;</span></p>
<p class="Corps-P"><span class="Corps-C3">Cinquième avantage : Lors de votre adhésion, vous pou-<wbr>vez souscrire à deux options
    qui vous aideront à booster votre activité.</span></p>
</div>
<div style="position:absolute; left:10px; top:2881px; width:790px; height:36px;">
    <a href="page16.html"><img src="<?php echo GetImagePath("front/"); ?>/wpeab18809.png" width="790" height="36" border="0" id="qs_32" name="qs_32" title="" alt="Page 15" onload="OnLoadPngFix()"></a></div>
<div style="position:absolute; left:10px; top:1739px; width:790px; height:36px;">
    <a href="page16.html"><img src="<?php echo GetImagePath("front/"); ?>/wpeab18809.png" width="790" height="36" border="0" id="qs_32" name="qs_32" title="" alt="Page 15" onload="OnLoadPngFix()"></a></div>
<div style="position:absolute; left:10px; top:2024px; width:790px; height:36px;">
    <a href="page16.html"><img src="<?php echo GetImagePath("front/"); ?>/wpeab18809.png" width="790" height="36" border="0" id="qs_30" name="qs_30" title="" alt="Page 15" onload="OnLoadPngFix()"></a></div>
<div style="position:absolute; left:249px; top:719px; width:542px; height:34px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wp5533b116.gif" width="542" height="34" border="0" id="hs_2" name="hs_2" title="" alt=""></div>
<div style="position:absolute; left:251px; top:1195px; width:539px; height:111px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpf7e582ca.png" width="539" height="111" border="0" id="pic_1" name="pic_1" title="" alt="" onload="OnLoadPngFix()"></div>
<div id="txt_1" style="position:absolute; left:367px; top:1209px; width:412px; height:85px;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box; overflow:hidden;">
<p class="Corps-P1"><span class="Corps-C6">Votre fiche clés en mains... </span></p>
<p class="Corps-P1"><span class="Corps-C7">Nous pouvons nous occuper de la réalisation de votre fiche, photos, article, vidéo....</span></p>
</div>
<div style="position:absolute; left:584px; top:1788px; width:216px; height:214px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wp0f14eefe_05.jpg" width="216" height="214" border="0" id="pic_102" name="pic_102" title="" alt=""></div>
<div id="txt_20" style="position:absolute; left:250px; top:1809px; width:318px; height:194px;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box; overflow:hidden;">
<p class="Corps-P"><span class="Corps-C8">Vous choisissez un ou plusieurs modules de 20 annonces</span><span class="Corps-C0">, vous déposez en temps réel
    vos produits ou services (destockage, soldes etc...)</span></p>
<p class="Corps-P"><span class="Corps-C0">Depuis votre page d’administration, vous gérer ce capital de 20 annonces (création,
    modification, suppression) dans l’année de votre abonnement.</span></p>
<p class="Corps-P"><span class="Corps-C8">Les annonces professionnelles sont également présentes sur nos applications mobiles
    (Iphone, Android).</span></p>
<p class="Corps-P"><span class="Corps-C">&nbsp;</span></p>
<p class="Corps-P2"><span class="Corps-C9">&nbsp;</span></p>
</div>
<div id="txt_19" style="position:absolute; left:250px; top:2085px; width:540px; height:174px;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box; overflow:hidden;">
<p class="Corps-P"><span class="Corps-C0">Avec cette option, vous motivez les consommateurs à vous rendre visite. </span></p>
<p class="Corps-P"><span class="Corps-C0">Nos partenaires concèdent des offres exceptionnelles aux consommateurs inscrits au
    Club.</span></p>
<p class="Corps-P"><span class="Corps-C8">Celle ci peut-<wbr>être validée qu’une seule fois :</span></p>
<p class="Corps-P"><span class="Corps-C10">Exemple pour un restaurant (1 menu acheté = 1 menu offert) L’adhérent &nbsp;identifié
    ne peut la valider qu’une seule fois. Si par nature il désire valider une promotion
    déjà utilisé, notre système lui précisera son impossibilité de le faire.</span></p>
<p class="Corps-P"><span class="Corps-C8">Ou bien les partenaires peuvent choisir qu’elle soit permanente :</span></p>
<p class="Corps-P"><span class="Corps-C0">pendant la totalité de leur abonnement ou pendant une période précisée. L’adhérent
    &nbsp;identifié peut valider votre offre autant de fois qu’il le désire.</span></p>
</div>
<div style="position:absolute; left:250px; top:2309px; width:540px; height:36px;">
    <a href="page16.html"><img src="<?php echo GetImagePath("front/"); ?>/wp33083e9e.png" width="540" height="36" border="0" id="qs_31" name="qs_31" title="" alt="Page 15" onload="OnLoadPngFix()"></a></div>
<div style="position:absolute; left:259px; top:2315px; width:408px; height:26px;">
    <div class="Corps-P0">
        <span class="Normal-C">Les Ibons plans : LE fonctionnement</span></div>
</div>
<div style="position:absolute; left:732px; top:2321px; width:49px; height:17px;">
    <map id="map1" name="map1">
        <area shape="poly" coords="49,0,24,17,49,17" href="page16.html" alt="">
        <area shape="poly" coords="32,0,17,0,24,6" href="page16.html" alt="">
        <area shape="poly" coords="0,0,0,17,24,17" href="page16.html" alt="">
    </map>
    <img src="<?php echo GetImagePath("front/"); ?>/wp4ba70da3.png" width="49" height="17" border="0" id="pcrv_2" name="pcrv_2" title="" alt="" onload="OnLoadPngFix()" usemap="#map1"></div>
<div style="position:absolute; left:20px; top:1742px; width:631px; height:30px;">
    <div class="Corps-P0">
        <span class="Normal-C0">Option 1 : l’accès aux annonces professionnelles</span></div>
</div>
<div style="position:absolute; left:20px; top:2027px; width:427px; height:30px;">
    <div class="Corps-P0">
        <span class="Normal-C0">Option 2 : l’accès aux ibons plans</span></div>
</div>
<div style="position:absolute; left:20px; top:2348px; width:220px; height:240px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wp9c58571f.png" width="220" height="240" border="0" id="pic_88" name="pic_88" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:250px; top:2601px; width:540px; height:36px;">
    <a href="page16.html"><img src="<?php echo GetImagePath("front/"); ?>/wp33083e9e.png" width="540" height="36" border="0" id="qs_30" name="qs_30" title="" alt="Page 15" onload="OnLoadPngFix()"></a></div>
<div style="position:absolute; left:257px; top:2607px; width:411px; height:26px;">
    <div class="Corps-P0">
        <span class="Normal-C">Les Ibons plans : LES &nbsp;POINTS CADEAUX</span></div>
</div>
<div id="txt_21" style="position:absolute; left:250px; top:2651px; width:540px; height:214px;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box; overflow:hidden;">
<p class="Corps-P"><span class="Corps-C8">Avec les points cadeaux, Le club proximité aide ses partenaires à dynamiser leur
    activité et à fidéliser les consommateurs adhérents.</span></p>
<p class="Corps-P"><span class="Corps-C0">Le Club propose à ces derniers de gagner des point cadeaux à chaque concrétisation
    d’une offre “bon plan”.</span></p>
<p class="Corps-P"><span class="Corps-C0">Le consommateur </span><span class="Corps-C8">note le numéro du courriel </span><span class="Corps-C0">qu’il a reçu lors de la confirmation de
    sa demande.</span></p>
<p class="Corps-P"><span class="Corps-C0">Lors de son achat chez le partenaire, il lui demande lui </span><span class="Corps-C8">le numéro du courriel qu’il
    a reçu.</span></p>
<p class="Corps-P"><span class="Corps-C8">Muni de ces deux codes, le consommateur se connecte sur son compte, il les enregistre
    sur les champs correspondant, en validant notre système reconnaîtra les numéros et
    son compte sera crédité de 10 points.</span></p>
<p class="Corps-P"><span class="Corps-C0">Le consommateur cumule les points, et les </span><span class="Corps-C8">transforme &nbsp;en cadeaux quand il le souhaite.</span><span class="Corps-C0">
    </span></p>
<p class="Corps-P2"><span class="Corps-C9">&nbsp;</span></p>
<p class="Corps-P2"><span class="Corps-C9">&nbsp;</span></p>
</div>
<div id="txt_31" style="position:absolute; left:250px; top:2384px; width:540px; height:216px;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box; overflow:hidden;">
<p class="Corps-P"><span class="Corps-C8">Le consommateur choisit le bon plan qui lui convient parmi les offres des partenaires
    du Club.</span></p>
<p class="Corps-P"><span class="Corps-C4">&nbsp;</span></p>
<p class="Corps-P"><span class="Corps-C0">Avant de le valider , il faut que ce dernier soit </span><span class="Corps-C8">inscrit</span><span class="Corps-C0"> et</span><span class="Corps-C8"> identifié</span><span class="Corps-C0"> en qualité
    d’adhérent du Club</span></p>
<p class="Corps-P"><span class="Corps-C0">Le consommateur identifié peut alors </span><span class="Corps-C8">valider la promotion</span><span class="Corps-C0"> qui lui convient.</span></p>
<p class="Corps-P"><span class="Corps-C8">Il reçoit une confirmation numérotée par mail lui rappelant, ses coordonnées, ceux
    du partenaire et les caractéristiques de la promotion.</span></p>
<p class="Corps-P"><span class="Corps-C0">Parallèlement, le professionnel reçoit un </span><span class="Corps-C8">double numéroté</span><span class="Corps-C0">.</span></p>
<p class="Corps-P"><span class="Corps-C0">L’adhérent </span><span class="Corps-C8">se déplace </span><span class="Corps-C0">chez notre partenaire, </span><span class="Corps-C8">s’identifie</span><span class="Corps-C0"> en lui montrant </span><span class="Corps-C8">le mail
    imprimé</span><span class="Corps-C0"> ou </span><span class="Corps-C8">&nbsp;présent sur son Smartphone</span><span class="Corps-C0">.</span></p>
<p class="Corps-P2"><span class="Corps-C0">Il bénéfice alors de cette promotion</span></p>
</div>
<div style="position:absolute; left:730px; top:2612px; width:49px; height:17px;">
    <map id="map2" name="map2">
        <area shape="poly" coords="49,0,24,17,49,17" href="page16.html" alt="">
        <area shape="poly" coords="32,0,17,0,24,6" href="page16.html" alt="">
        <area shape="poly" coords="0,0,0,17,24,17" href="page16.html" alt="">
    </map>
    <img src="<?php echo GetImagePath("front/"); ?>/wp4ba70da3.png" width="49" height="17" border="0" id="pcrv_3" name="pcrv_3" title="" alt="" onload="OnLoadPngFix()" usemap="#map2"></div>
<div style="position:absolute; left:252px; top:3106px; width:540px; height:36px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wp33083e9e.png" width="540" height="36" border="0" id="qs_35" name="qs_35" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:266px; top:3112px; width:301px; height:26px;">
    <div class="Corps-P0">
        <span class="Normal-C">LES &nbsp;CONDITIONS GéNéRALES</span></div>
</div>
<div style="position:absolute; left:250px; top:3107px; width:542px; height:34px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wp5533b116.gif" width="542" height="34" border="0" id="hs_3" name="hs_3" title="" alt=""></div>
<div id="txt_29" style="position:absolute; left:250px; top:2930px; width:540px; height:173px;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box; overflow:hidden;">
<p class="Corps-P"><span class="Corps-C8">Pour bénéficier de tous ces avantages et options, les professionnels doivent souscrire
    un abonnement annuel “Partenaire Club”.</span></p>
<p class="Corps-P"><span class="Corps-C0">Il suffit de remplir le formulaire (voir lien ci-<wbr>dessous)</span></p>
<p class="Corps-P"><span class="Corps-C0">Dès votre règlement enregistré, Vous recevez immédiatement vos identifiants par mail.</span></p>
<p class="Corps-P"><span class="Corps-C8">le professionnel accède à son compte; &nbsp;il créé, modifie et supprime ses données quand
    il le désire.</span></p>
<p class="Corps-P"><span class="Corps-C8">L’abonnement &nbsp;est valable 12 mois,</span></p>
<p class="Corps-P"><span class="Corps-C0">Les données du partenaire sont présentes sur nos applications mobiles Iphone et Android</span></p>
<p class="Corps-P"><span class="Corps-C">&nbsp;</span></p>
<p class="Corps-P2"><span class="Corps-C9">&nbsp;</span></p>
</div>
<div style="position:absolute; left:20px; top:328px; width:204px; height:242px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpa2aa549e.png" width="204" height="242" border="0" id="pic_1" name="pic_1" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:111px; top:607px; width:111px; height:112px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpa12d8626.png" width="111" height="112" border="0" id="pic_2" name="pic_2" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:20px; top:878px; width:204px; height:234px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wp09b5b079.png" width="204" height="234" border="0" id="pic_3" name="pic_3" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:105px; top:1147px; width:111px; height:112px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpa12d8626.png" width="111" height="112" border="0" id="pic_4" name="pic_4" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:20px; top:1782px; width:204px; height:230px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wp068220e1.png" width="204" height="230" border="0" id="pic_5" name="pic_5" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:20px; top:2076px; width:204px; height:236px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wp51a07b7d.png" width="204" height="236" border="0" id="pic_6" name="pic_6" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:809px; top:269px; width:204px; height:255px;">
    <map id="map3" name="map3">
        <area shape="rect" coords="203,0,205,7" href="http://www.proximite-magazine.com/games/games1/cadeaux.html" target="_blank" onclick="window.open(this.href,'_blank','width=820,height=2000,menubar=yes,resizable=no,scrollbars=no,status=no,toolbar=no').focus(); return false;" alt="">
    </map>
    <a href="page21.html"><img src="<?php echo GetImagePath("front/"); ?>/wpaf9f6326.png" width="204" height="255" border="0" id="pic_13" name="pic_13" title="" alt="ACCESCOMPTE" onload="OnLoadPngFix()" usemap="#map3"></a></div>
<div style="position:absolute; left:300px; top:781px; width:432px; height:29px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wp0505ae60.png" width="432" height="29" border="0" id="qs_7" name="qs_7" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:366px; top:786px; width:331px; height:21px;">
    <div class="Corps-P0">
        <span class="Normal-C1">Je &nbsp;me connecte sur la page d’inscription</span></div>
</div>
<div style="position:absolute; left:295px; top:3177px; width:432px; height:29px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wp0505ae60.png" width="432" height="29" border="0" id="qs_1" name="qs_1" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:361px; top:3182px; width:331px; height:21px;">
    <div class="Corps-P0">
        <span class="Normal-C1">Je &nbsp;me connecte sur la page d’inscription</span></div>
</div>
</div>
<?php $this->load->view("front/includes/vwFooter"); ?>