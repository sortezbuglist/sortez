<?php $data["zTitle"] = 'Bon plans' ?>
<?php $this->load->view("front/includes/vwHeader", $data); ?>
<?php $this->load->view("front/includes/vwMenu"); ?>

<script type="text/javascript">
    function lookup(inputString) {
        if(inputString.length == 0) {
            // Hide the suggestion box.
            jQuery('#suggestions').hide();
        } else {        
            jQuery.post("<?php echo site_url('front/autocomplete/indexlisteCategorie/'); ?>/"+inputString+" " , {queryString: ""+inputString+""}, function(data){                
                if(data.length >0) { 
                    jQuery('#suggestions').show();
                    jQuery('#autoSuggestionsList').html(data);
                }
            });
        }
    } 
        
    function fill(thisValue,id) {
        jQuery('#inputString').val(thisValue);        
        jQuery('#inputStringHidden').val(id);    
                            
        setTimeout("jQuery('#suggestions').hide();", 500);
    }
    function lookupVille(inputString) {
        if(inputString.length == 0) {
            // Hide the suggestion box.
            jQuery('#suggestionsVille').hide();
        } else {        
            jQuery.post("<?php echo site_url('front/autocomplete/indexlisteVille/'); ?>/"+inputString+" " , {queryString: ""+inputString+""}, function(data){                
                if(data.length >0) { 
                    jQuery('#suggestionsVille').show();
                    jQuery('#autoSuggestionsListVille').html(data);
                }
            });
        }
    } 
        
    function fillVille(thisValue,id) {
        jQuery('#inputStringVille').val(thisValue);        
        jQuery('#inputStringVilleHidden').val(id);    
                            
        setTimeout("jQuery('#suggestionsVille').hide();", 500);
    }
    
    function delete_all_session_bonplan(){
        <?php
        /*unset($_SESSION['iCategorieId']);
        unset($_SESSION['iVilleId']);
        unset($_SESSION['zMotCle']);*/
        ?>
    }
</script>

<style type="text/css">
<!--
#mainbody_header {
	background-image: url(<?php echo GetImagePath("front/"); ?>/bg_mainbody_header.png);
	background-repeat: no-repeat;
	background-position: left;
	float: left;
	height: 133px;
	width: 100%;
}
#id_mainbody_main {
	background-image: url(<?php echo GetImagePath("front/"); ?>/bg_mainbody_main.png);
	background-repeat: repeat-y;
	float: left;
	width: 100%;
	position: relative;
}
#id_mainbody_footer {
	background-image: url(<?php echo GetImagePath("front/"); ?>/bg_mainbody_footer.png);
	background-repeat: no-repeat;
	float: left;
	height: 24px;
	width: 100%;
	position: relative;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	padding-left: 30px;
}
#id_mainbody_mainrepete {
	background-image: url(<?php echo GetImagePath("front/"); ?>/bg_mainbody_separator.png);
	background-repeat: no-repeat;
	background-position: left bottom;
	float: left;
	width: 100%;
	position: relative;
	font-family: "Arial",sans-serif;
    font-size: 12px;
}
#id_bp_minitext {
	background-repeat: no-repeat;
	background-position: center;
	float: left;
	width: 150px;
	position: relative;
	height: 96px;
	background-image: url(<?php echo GetImagePath("front/"); ?>/wpd9b44e6d_06.png);
	padding: 25px 15px 20px 118px;
	color: #FFFFFF;
	font-family: "Arial",sans-serif;
	font-size: 15px;
	font-weight: 700;
	line-height: 1.2em;
	text-align:center;
}

.status_btn {
	height: 43px;
	margin-top:80px;
}
-->
</style>

<!--start main menu bloc-->
<map id="map0" name="map0">
    <area shape="rect" coords="24,172,89,245" href="<?php echo site_url();?>" alt="">
    <area shape="rect" coords="102,173,166,245" href="javascript:history.back()" alt="">
    <area shape="rect" coords="182,173,246,245" href="<?php echo site_url("front/cadeau");?>" alt="">
    <area shape="rect" coords="259,173,323,245" href="<?php echo site_url("front/contact");?>" alt="">
</map>
<!--end main menu bloc-->


<!--start menu bloc for home-->
<a href="<?php echo site_url();?>"><img src="<?php echo GetImagePath("front/"); ?>/wpb2ca9cf5_06.png" id="pic_1575" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 15px; top: 265px; z-index:1000;" width="187" border="0" height="273"></a>

<map id="map3" name="map3">
  <area shape="poly" coords="181,272,187,263,187,43,175,0,7,0,0,18,0,230,12,273,155,273" href="<?php echo site_url("front/annonce");?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wpb284a532_06.png" id="pic_1509" title="" alt="annonces" onload="OnLoadPngFix()" usemap="#map3" style="position: absolute; left: 217px; top: 265px;" height="273" border="0" width="187">

<img src="<?php echo GetImagePath("front/"); ?>/wpc863625d_06.png" id="pic_1576" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 418px; top: 265px;" width="187" border="0" height="273">

<map id="map4" name="map4">
  <area shape="poly" coords="182,272,188,263,188,43,176,0,8,0,1,18,1,230,13,273,156,273" href="<?php echo site_url("front/cadeau");?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wpc783aaac_06.png" id="pic_1510" title="" alt="fonctionnementpointscadeaux" onload="OnLoadPngFix()" usemap="#map4" style="position: absolute; left: 620px; top: 265px;" height="273" border="0" width="187">

<!--end menu bloc for home-->



<!--start main body bloc-->

<div id="main_body_bloc" style="position: absolute; left: 15px; top: 560px; height:auto; width:792px" border="0">
  <div id="mainbody_header">
  <form name="frmRechercheAnnonce" id="frmRechercheAnnonce" method="post" action="<?php echo site_url('front/bonplan/index/'); ?>">
  		<input type="hidden" name="hdnFavoris" id="hdnFavoris" value="<?php echo $iFavoris; ?>" />
  	<div style="margin-left:10px; margin-top:15px;">
    <table width="100%" border="0" cellspacing="5" cellpadding="0">
      <tr>
        <td><span class="Normal-C">Recherche par th&egrave;mes</span></td>
        <td><span class="Normal-C">Recherche par d&eacute;positaires</span></td>
      </tr>
      <tr>
        <td>
        <select type="inputStringHidden" name="inputStringHidden" style="width:366px">
            <option value="0">-- Veuillez choisir --</option>
            <?php foreach($toCategorie as $oCategorie){ ?>
                <option value="<?php echo $oCategorie->IdSousRubrique ; ?>" <?php if(isset($iCategorieId) && $iCategorieId == $oCategorie->IdSousRubrique) echo 'selected="selected"'; ?>><?php echo $oCategorie->Nom." (".$oCategorie->nb_bonplan.")" ; ?></option>
            <?php } ?>
        </select>
        </td>
        <td>
        <!--<select type="inputStringVilleHidden" name="inputStringVilleHidden" style="width:366px">
            <option value="0">-- Veuillez choisir --</option>
            <?php// foreach($toVille as $oVille){ ?>
                <option value="<?php// echo $oVille->IdVille ; ?>" <?php// if(isset($iVilleId) && $iVilleId == $oVille->IdVille) echo 'selected="selected"'; ?>><?php// echo $oVille->commercant."&nbsp;(".$oVille->Nom.")" ; ?></option>
            <?php// } ?>
        </select>-->
        <select type="inputStringCommercantHidden" name="inputStringCommercantHidden" style="width:366px">
            <option value="0">Selectionner diffuseur</option>
            <?php foreach($toCommercant as $oCommercant){ ?>
                <option value="<?php echo $oCommercant->IdCommercant ; ?>" <?php if(isset($iCommercantId) && $iCommercantId == $oCommercant->IdCommercant) echo 'selected="selected"'; ?>><?php echo $oCommercant->NomSociete; ?></option>
            <?php } ?>
        </select>
        </td>
      </tr>
      <tr>
        <td><span class="Normal-C">Recherche par mots cl&eacute;s</span></td>
        <td rowspan="2">
        <div style="text-align:right; margin-top:10px; margin-right:10px;"> <a href="Javascript:void();" onclick="document.frmRechercheAnnonce.submit();"><img src="<?php echo GetImagePath("front/"); ?>/wpa9e8bbcd_06.png" alt="" name="pic_1504" width="106" height="30" border="0" id="pic_1504" style="" onload="OnLoadPngFix()"></a>
		  <a href="Javascript:void();" onclick="document.location='<?php echo site_url("front/bonplan/index/"); ?>' ;"><img src="<?php echo GetImagePath("front/"); ?>/wpc625ec40_06.png" alt="" name="pic_1505" width="106" height="30" border="0" id="pic_1505" style="" onload="OnLoadPngFix()"></a>
        </div>
        </td>
      </tr>
      <tr>
        <td><input type="text" name="zMotCle" id="zMotCle" style="width:362px;" value="<?php if(isset($zMotCle)) echo $zMotCle; ?>"/></td>
       
      </tr>
    </table>
	</div>
    </form>
  </div>
  <div id="id_mainbody_main">
  <?php 
  /*echo "<br/>".$iNombreLiens;
  echo "<br/>".$PerPage;  
  echo "<br/>".$TotalRows;  */
  ?>
  
  	<?php foreach($toListeBonPlan as $oListeBonPlan){ ?>
    <div id="id_mainbody_mainrepete">
		<?php 
        $objasscommrubr = $this->mdlcommercant->GetRubriqueId($oListeBonPlan->bonplan_commercant_id);
        if ($objasscommrubr) $objrubr = $this->mdlcategorie->GetById($objasscommrubr->IdRubrique);
        ?>
    
<table width="100%" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td style="width:215px">
            <img src="<?php if ($oListeBonPlan->bonplan_photo1 != "" && file_exists("application/resources/front/images/".$oListeBonPlan->bonplan_photo1)==true){ echo base_url()."application/resources/front/images/".$oListeBonPlan->bonplan_photo1 ; }else{echo GetImagePath("front/")."/wp71b211d2_06.png";}?>" width="215" border="0" id="pic_965" name="pic_965" title="" alt="">
            </td>
            <td style="width:170px">
                <span style="font-size:13px;"><strong>
                <?php //if ($objasscommrubr) echo $objrubr->Nom ;?><br />
                <?php echo $oListeBonPlan->Nom;?><br />
                <?php echo $oListeBonPlan->NomSociete ; ?><br /></strong></span>
                <?php echo $oListeBonPlan->ville ; ?>, <?php echo $oListeBonPlan->rue ; ?>, <?php echo $oListeBonPlan->quartier ; ?>
            </td>
            <td style="width:283px">
                <div id="id_bp_minitext"><?php echo $oListeBonPlan->bonplan_titre;//truncate($oListeBonPlan->bonplan_texte,40,"...");?></div><!--."<br />".$oListeBonPlan->bonplan_titre-->
            </td>
            <td align="center">
            <p class="status_btn"><a  target='_blank' href="<?php echo site_url($oListeBonPlan->nom_url."/notre_bonplan");?>"><img src="<?php echo GetImagePath("front/"); ?>/wp68233b54_06.png" id="pic_1526" alt="" onload="OnLoadPngFix()" height="50" border="0" width="50" style="text-align:center"></a></p>
            </td>
          </tr>
          
          
        </table>

    </div>
    <?php  } ?>
  </div>
  <div id="id_mainbody_footer">
  	<?php
        if($iNombreLiens != 1){
            ?>Pages : <?php
            $iPageCourante = $argOffset ;
            for($i=1; $i<=$iNombreLiens; $i++){
                $argOffset = ($i - 1 ) * $PerPage ;
                ?>
                <?php if($iPageCourante == $argOffset){ ?>
                    <span style="cursor:pointer; color:red; font-weight: bold;" onclick="document.location='<?php echo site_url("front/bonplan/redirection/". $argOffset) ; ?>' ;">
                            <?php echo $i ; ?>
                    </span>&nbsp;
                <?php }else{?>
                    <span style="cursor:pointer;text-decoration: underline" onclick="document.location='<?php echo site_url("front/bonplan/redirection/". $argOffset) ; ?>' ;">
                            <?php echo $i ; ?>
                    </span>&nbsp;
                <?php }?>
                <?php
            }
        }
    ?>
  </div>
  
  
  
  
  
  <div id="id_mainbody_copyrig" style="width:100%;height:73px; position:relative; float:left; margin-top:40px;">
    <div class="Normal-P-P0">
        <span class="Normal-C-C0">Le &nbsp;Club Proximit&eacute; est une marque d&eacute;pos&eacute;e et une r&eacute;alisation du Magazine Proximit&eacute;<br></span></div>
    <div class="Corps-P-P0">
        <span class="Corps-C-C6"> Editeur : Phase SARL 60 avenue de Nice 06800 Cagnes-<wbr>sur-<wbr>Mer &nbsp;</span><span class="Corps-C-C7">Sarl au capital de 7265 &euro; -<wbr> RCS Antibes 412.071.466<br></span><span class="Corps-C">T&eacute;l&eacute;phone : 00 33 (0)4 93 73 84 51 -<wbr> Mobile : 00 33 (0)6 72 05 59 35 -<wbr> proximite-<wbr>magazine@orange.fr</span></div>
</div>
  
  
</div>
<!--end main body bloc-->



<?php $this->load->view("front/includes/vwFooter"); ?>