<?php $data["zTitle"] = "Inscription d'un particulier"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="fr"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="Generator" content="Serif WebPlus X5">
<meta http-equiv="X-UA-Compatible" content="IE=EmulationIE8">
<title>Inscription d'un particulier</title>
<style type="text/css">
body {margin: 0px; padding: 0px;}
.Normal-P
{
    margin:0.0px 0.0px 13.0px 0.0px; text-align:left; font-weight:400;
}
.Corps-P
{
    margin:0.0px 0.0px 12.0px 0.0px; text-align:justify; font-weight:400;
}
.Normal-C
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
.Normal-C-C0
{
    font-family:"Verdana", sans-serif; font-size:16.0px; line-height:1.13em;
}
.Corps-C
{
    font-family:"Verdana", sans-serif; font-size:16.0px; line-height:1.13em;
}
.Corps-C-C0
{
    font-family:"Arial", sans-serif; font-size:11.0px; line-height:1.27em;
}
.EmailLoading, .LoginLoading {
	visibility:hidden;
}
.EmailLoading_show, .LoginLoading_show {
	visibility:visible;
}
</style>
<script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/jspngfix.js"></script>
<link rel="stylesheet" href="inscriptionparticuliers_fichiers/wpstyles.css" type="text/css">
<script type="text/javascript">var blankSrc = "wpscripts/blank.gif";
</script>

<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>


<link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/") ; ?>/style.css" />
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/") ; ?>/blue/style.css" />
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/jquery.ui.core.js"></script>
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/") ; ?>/jquery-ui-1.8.4.custom.css" />
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/") ; ?>/demo_table_jui.css" />

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery("#btnSinscrire").click(function(){
            var txtError = "";
            
            var txtNom = jQuery("#txtNom").val();
            if(txtNom=="") {
                txtError += "- Veuillez indiquer Votre nom <br/>";    
            }
            
            var txtEmail = jQuery("#txtEmail").val();
            if(!isEmail(txtEmail)) {
                txtError += "- L'adresse mail n'est pas valide. Veuillez saisir un email valide <br/>";
            } 
			
			 var txtEmail_verif = jQuery('#txtEmail_verif').val();
			  if (txtEmail_verif == 1) {
				  txtError += "- Votre Email existe déjà sur notre site <br/>";
			  }
			
            // :Check if a city has been selected before validating
              var ivilleId = jQuery('#txtVille').val();
              if (ivilleId == 0) {
                  txtError += "- Vous devez sélectionner une ville <br/>";
              }
            
            var txtLogin = jQuery("#txtLogin").val();
            if(!isEmail(txtLogin)) {
                txtError += "- Votre login doit &ecirc;tre un email valide <br/>";    
            }
			
			var txtLogin_verif = jQuery('#txtLogin_verif').val();
			  if (txtLogin_verif == 1) {
				  txtError += "- Votre Login existe déjà sur notre site <br/>";
			  }
            
            var txtPassword = jQuery("#txtPassword").val();
            if(txtPassword=="") {
                txtError += "- Veuillez indiquer un Password <br/>";    
            }
            
            if(jQuery("#txtPassword").val() != jQuery("#txtConfirmPassword").val()) {
                    txtError += "- Les deux mots de passe ne sont pas identiques. <br/>";
            }
            
            var validationabonnement = $('#validationabonnement').val();
            //alert("coche "+validationabonnement);
            if ($('#validationabonnement').is(":checked")) {} else {
                    txtError += "- Vous devez valider les conditions générales<br/>";
            }
            
            jQuery("#divErrortxtInscription").html(txtError);
            
            if(txtError == "") {
                jQuery("#frmInscriptionParticulier").submit();
            }
        });
		
		
		jQuery("#txtEmail").blur(function(){
										   //alert('cool');
										   var txtEmail = jQuery("#txtEmail").val();
										   //alert('<?php echo site_url("front/particuliers/verifier_email"); ?>' + '/' + txtEmail);
										   //jQuery(".EmailLoading").addClass("EmailLoading_show").removeClass("EmailLoading");
										   jQuery.post(
													'<?php echo site_url("front/particuliers/verifier_email");?>',
													{ txtEmail_var: txtEmail },
													function (zReponse)
													{
														//alert (zReponse) ;
														var zReponse_html = '';
														if (zReponse == "1") {
															zReponse_html = '<span style="color:#F00; font-weight:bold;">Ce mail est d&eacute;j&agrave; utilis&eacute;</span>';
														} else { 
															zReponse_html = '<span style="color:#3C0; font-weight:bold;">Ce mail est disponible</span>';
														} 
														
														
														if (txtEmail != "") {jQuery('#divErrortxtEmail_').html(zReponse_html);}       
													    jQuery('#txtEmail_verif').val(zReponse);
													 
												   });
										   
										   jQuery(".FieldError").removeClass("FieldError");
										   //jQuery(".EmailLoading").addClass("EmailLoading").removeClass("EmailLoading_show");
										   
										   });
		
		
		
		
		jQuery("#txtLogin").blur(function(){
										   //alert('cool');
										   var txtLogin = jQuery("#txtLogin").val();
										   //alert('<?php echo site_url("front/particuliers/verifier_login"); ?>' + '/' + txtLogin);
										   //jQuery(".EmailLoading").addClass("EmailLoading_show").removeClass("EmailLoading");
										   jQuery.post(
													'<?php echo site_url("front/particuliers/verifier_login");?>',
													{ txtLogin_var: txtLogin },
													function (zReponse)
													{
														//alert (zReponse) ;
														var zReponse_html = '';
														if (zReponse == "1") {
															zReponse_html = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
														} else { 
															zReponse_html = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
														} 
														
														
														if (txtLogin != "") {jQuery('#divErrortxtLogin_').html(zReponse_html);}       
													    jQuery('#txtLogin_verif').val(zReponse);
													 
												   });
										  
										   jQuery(".FieldError").removeClass("FieldError");
										   //jQuery(".EmailLoading").addClass("EmailLoading").removeClass("EmailLoading_show");
										   
										   });
		
		
		jQuery( "#txtDateNaissance" ).datepicker({
				dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
				monthNames: ['Janvier','Fevier','Mars','Avril','Mai','Juin','Juiller','Aout','Septembre','Octobre','Novembre','Decembre'],
				dateFormat: 'yy-mm-dd',
				autoSize: true
			});
		
    })
	function getCP(){
	      var ivilleId = jQuery('#txtVille').val();     
          jQuery.get(
            '<?php echo site_url("front/particuliers/getPostalCode"); ?>' + '/' + ivilleId,
            function (zReponse)
            {
                // alert (zReponse) ;
                jQuery('#trReponseVille').html(zReponse) ;       
               
             
           });
	}
	 
</script>

</head>

<body style="background-color: transparent; background-image: url(&quot;<?php echo GetImagePath("front/"); ?>/wp007ff600_06.png&quot;); background-repeat: repeat; background-position: center top; background-attachment: scroll; text-align: center; height: 2000px;" text="#000000">
<form name="frmInscriptionParticulier" id="frmInscriptionParticulier" action="<?php echo site_url("front/particuliers/ajouter"); ?>" method="POST" enctype="multipart/form-data">
<div style="background-color:#cccccc;text-align:left;margin-left:auto;margin-right:auto;position:relative;width:650px;height:2000px;">
<map id="map0" name="map0">
    <area shape="rect" coords="11,121,75,193" href="<?php echo site_url();?>" alt="">
    <area shape="rect" coords="89,121,153,194" href="javascript:history.back()" alt="">
    <area shape="rect" coords="169,121,233,194" href="http://www.proximite-magazine.com/clubnv/newsletter.html" alt="">
    <area shape="rect" coords="246,121,310,194" href="<?php echo site_url("front/contact");?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wpd22efe04_06.png" id="hs_4" title="" alt="contact" onload="OnLoadPngFix()" usemap="#map0" style="position: absolute; left: 0px; top: 0px;" width="650" border="0" height="200">
<img src="<?php echo GetImagePath("front/"); ?>/wpc1d7ef6a_06.png" id="pic_1603" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 15px; top: 219px;" width="620" border="0" height="1039">
<div id="txt_586" style="position:absolute; left:83px; top:479px; width:475px; height:518px; overflow:hidden; font-family: Arial; font-size: 12px; line-height: 1.25em;">


			<table width="100%" cellpadding="0" cellspacing="5">
                
                <?php if($prmId!=0) { 
                echo '<tr>
                    <td colspan="2">
                    <p>
                        <span style="font-family: Arial;    font-size: 16px;    font-weight: 700;    line-height: 1.25em;">Bienvenue </span>';
						
						if ($oParticulier->Nom == 0) echo "Mr "; 
						if ($oParticulier->Nom == 1) echo "Mme ";
						if ($oParticulier->Nom == 2) echo "Mlle ";
						echo $oParticulier->Nom.", ";
                        echo $oParticulier->Adresse.", ";
                        echo $oParticulier->CodePostal.", ";
						$obj_ville = $this->mdlville->getVilleById($oParticulier->IdVille);
                        if ($obj_ville) echo $obj_ville->Nom; 
                   echo '</p>
                    </td>
                </tr>';
				
				} 
				?>
                
                <tr>
                    <td>
                        <label>Nom *: </label>
                    </td>
                    <td>
                        <input type="text" name="Particulier[Nom]" style = "width:273px;" id="txtNom" value="<?php if( isset ($oParticulier)) { echo $oParticulier->Nom; }?>" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>Pr&eacute;nom : </label>
                    </td>
                    <td>
                        <input type="text" name="Particulier[Prenom]" style = "width:273px;" id="txtPrenom" value="<?php if( isset ($oParticulier)) { echo $oParticulier->Prenom; }?>" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>Date de naissance : </label>
                    </td>
                    <td>
                        <input style = "width:273px;" type="text" name="Particulier[DateNaissance]" id="txtDateNaissance" value="<?php if( isset ($oParticulier)) { echo $oParticulier->DateNaissance; }?>" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>Civilit&eacute; : </label>
                    </td>
                    <td>
                        <select style = "width:280px;" name="Particulier[Civilite]" id="txtParticulier">
                            <option value="0">Monsieur</option>
                            <option value="1">Madame</option>
                            <option value="2">Mademoiselle</option>
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>Profession : </label>
                    </td>
                    <td>
                        <input type="text" name="Particulier[Profession]" style = "width:273px;" id="txtProfession" value="<?php if( isset ($oParticulier)) { echo $oParticulier->Profession; }?>" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>Adresse : </label>
                    </td>
                    <td>
                        <textarea name="Particulier[Adresse]" style = "width:273px;" id="txtAdresse"><?php if( isset ($oParticulier)) { echo $oParticulier->Adresse; }?></textarea>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>Ville *: </label>
                    </td>
                    <td>
                        <select name="Particulier[IdVille]" id="txtVille" style = "width:280px;" onchange ="getCP();">
                            <option value="0">-- Veuillez choisir --</option>
                            <?php if(sizeof($colVilles)) { ?>
                                <?php foreach($colVilles as $objVille) { ?>
                                    <option value="<?php echo $objVille->IdVille; ?>" <?php if( isset ($oParticulier) && $oParticulier->IdVille == $objVille->IdVille) { ?>selected="selected"<?php } ?>><?php echo htmlspecialchars(stripcslashes($objVille->Nom)); ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        <div class="FieldError" id="divErrorCity"></div>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>Code postal : </label>
                    </td>
                     <td id ="trReponseVille" name = "trReponseVille">
                        <input type="text" name="Particulier[CodePostal]" id="txtCodePostal" style = "width:273px;" value="<?php if( isset ($oParticulier)) { echo $oParticulier->CodePostal; }?>" />
                    </td>
                </tr>
				
                
                <tr>
                    <td>
                        <label>N° T&eacute;l&eacute;phone fixe : </label>
                    </td>
                    <td>
                        <input type="text" name="Particulier[Telephone]" id="txtTelephone" style = "width:273px;" value="<?php if( isset ($oParticulier)) { echo $oParticulier->Telephone; }?>" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>N° T&eacute;l&eacute;phone Portable : </label>
                    </td>
                    <td>
                        <input type="text" name="Particulier[Portable]" style = "width:273px;" id="txtPortable" value="<?php if( isset ($oParticulier)) { echo $oParticulier->Portable; }?>" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>N° de Fax : </label>
                    </td>
                    <td>
                        <input type="text" name="Particulier[Fax]" style = "width:273px;" id="txtFax" value="<?php if( isset ($oParticulier)) { echo $oParticulier->Fax; }?>" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>Email *: </label>
                    </td>
                    <td>
                        <input type="text" name="Particulier[Email]" style = "width:273px;" id="txtEmail" value="<?php if( isset ($oParticulier)) { echo $oParticulier->Email; }?>"/>
                        <div class="FieldError" id="divErrortxtEmail_"></div>
                        <!--<img src="<?php echo GetImagePath("front/"); ?>/loading.gif" class="EmailLoading" title="" alt="loading" onload="OnLoadPngFix()"/>-->
                        <input type="hidden" name="txtEmail_verif" style = "width:273px;" id="txtEmail_verif" value="0"/>
					</td>
                </tr>
                
                <tr>
                    <td>
                        <label>Login *: </label>
                    </td>
                    <td>
                        <input type="text" name="Particulier[Login]" style = "width:273px;" id="txtLogin" value="<?php if( isset ($oParticulier)) { echo $oParticulier->Login; }?>" />
                        <div class="FieldError" id="divErrortxtLogin_"></div>
                        <!--<img src="<?php echo GetImagePath("front/"); ?>/loading.gif" class="LoginLoading" title="" alt="loading" onload="OnLoadPngFix()"/>-->
                        <input type="hidden" name="txtLogin_verif" style = "width:273px;" id="txtLogin_verif" value="0"/>
					</td>
                </tr>
                
                <tr>
                    <td>
                        <label>Password *: </label>
                    </td>
                    <td>
                        <input type="password" name="Particulier[Password]" style = "width:273px;" id="txtPassword" value="<?php if( isset ($oParticulier)) { echo $oParticulier->Password; }?>" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>Confirm Password *: </label>
                    </td>
                    <td>
                        <input type="password" id="txtConfirmPassword" style = "width:273px;" value="<?php if( isset ($oParticulier)) { echo $oParticulier->Password; }?>" />
						<div class="FieldError" id="divErrortxtPassword"></div>
                    </td>
                </tr>
                <input type="hidden" name="Particulier[IdUser]" style = "width:273px;" id="IdUser" value="<?php if( isset ($oParticulier)) { echo $oParticulier->IdUser; } else echo "0";?>" />
                <!--<tr>
                    <td></td>
                    <td align="left"><?php if(!isset($oParticulier) || $oParticulier->IdUser == 0) { ?><input type="button" id="btnSinscrire"  value="S'inscrire" /><?php } ?>  </td>
                </tr>-->
                
            </table>
        



</div>

<?php if($prmId==0) { ?>
<div id="txt_587" style="position:absolute; left:290px; top:991px; width:242px; height:157px; overflow:hidden; padding:0px 0px 0px 2px;">
<p class="Corps-P"><span class="Corps-C-C0">Je déclare être majeur et accepter les conditions générales du Club Proximité.</span></p>
<p class="Corps-P"><span class="Corps-C-C0">Vous recevez par retour de mail, la confirmation de votre adhésion ainsi que la validation
    de votre identifiant et mot de passe.</span></p>
<p class="Corps-P"><span class="Corps-C-C0">L’abonnement à notre newsletter est automatique.</span></p>
</div>
<?php } ?>

<input id="validationabonnement" name="check_3" value="1" style="position: absolute; left: 260px; top: 990px; <?php if($prmId!=0) echo 'visibility:hidden;';?>" type="checkbox" <?php if($prmId!=0) echo 'checked="checked"'; ?>  />

<?php if($prmId==0) { ?>
<map id="map1" name="map1">
    <area shape="poly" coords="165,29,168,25,168,4,164,1,4,1,0,6,0,27,4,30,164,30" href="<?php echo site_url();?>staticpages/conditionsgenerales.html" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wp699c9025_06.png" id="pic_1604" title="" alt="conditionsgenerales" onload="OnLoadPngFix()" usemap="#map1" style="position: absolute; left: 366px; top: 1171px;" width="168" border="0" height="29">
<?php } ?>

<a href="Javascript:void();" id="btnSinscrire"><img src="<?php echo GetImagePath("front/"); ?>/<?php if($prmId!=0) echo 'wp613c7bec_06_modif.png'; else echo 'wp613c7bec_06.png';?>" id="pic_1605" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 366px; top: <?php if($prmId!=0) echo '1000px;'; else echo '1129px;';?>" 
<?php if($prmId!=0) { ?>width="115" height="31" <?php } else  { ?>width="168" height="29" <?php }?>
border="0" ></a>

<?php if($prmId==0) { ?>
<a href="http://www.proximite-magazine.com/clubnv/newsletter.html"><img src="<?php echo GetImagePath("front/"); ?>/wp42dd878d_05_06.jpg" id="pic_1314" title="" alt="newsletter" style="position: absolute; left: 51px; top: 988px;" width="161" border="0" height="96"></a>
<?php } ?>

<div id="divErrortxtInscription" style="position:absolute; left:42px; top:1086px; width:242px; height:137px; overflow:hidden; padding:0px 0px 0px 2px; font-family: Arial; font-size: 12px; color:#F00;">
</div>

</div>
</form>
</body></html>