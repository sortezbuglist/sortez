<?php $data["zTitle"] = 'Fonctionnement' ?>
<?php $this->load->view("front/includes/vwHeader", $data); ?>
<?php $this->load->view("front/includes/vwMenu"); ?>
<style type="text/css">
<!--
body {margin: 0px; padding: 0px;}
a:link {color: #3e550e;}
a:visited {color: #8e9165;}
a:hover {color: #3e550e;}
a:active {color: #3e550e;}
.Normal-P
        {
        margin:0.0px 0.0px 0.0px 0.0px; text-align:left; font-weight:400; 
        }
.Corps-P
        {
        margin:0.0px 0.0px 5.0px 0.0px; text-align:justify; font-weight:400; 
        }
.Normal-P0
        {
        margin:0.0px 0.0px 0.0px 0.0px; text-align:justify; font-weight:400; 
        }
.Corps-P0
        {
        margin:0.0px 0.0px 5.0px 0.0px; text-align:left; font-weight:400; 
        }
.Corps-P1
        {
        margin:0.0px 0.0px 12.0px 0.0px; text-align:justify; 
        font-weight:400; 
        }
.Normal-C
        {
        font-family:"Capture it", serif; font-size:32.0px; line-height:1.16em; 
        }
.Corps-C
        {
        font-family:"Arial", sans-serif; font-weight:700; font-size:13.0px; 
        line-height:1.23em; 
        }
.Corps-C0
        {
        font-family:"Arial", sans-serif; font-size:13.0px; line-height:1.23em; 
        }
.Normal-C0
        {
        font-family:"Arial", sans-serif; font-size:13.0px; line-height:1.23em; 
        color:#990000; 
        }
.Normal-C1
        {
        font-family:"Arial Black", sans-serif; font-size:47.0px; 
        line-height:1.43em; 
        }
.Corps-C1
        {
        font-family:"Arial", sans-serif; font-weight:700; font-size:19.0px; 
        line-height:1.21em; 
        }
.Normal-C2
        {
        font-family:"Capture it", serif; font-size:40.0px; line-height:1.13em; 
        }
-->
</style>
<div style="position:absolute; left:0px; top:241px; width:246px; height:34px;">
    <a href="http://www.proximite-magazine.com"><img src="<?php echo GetImagePath("front/"); ?>/wp5533b116.gif" width="246" height="34" border="0" id="hs_10" name="hs_10" title="" alt=""></a></div>
<div style="position:absolute; left:11px; top:1529px; width:239px; height:237px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wp5129ac39_05.jpg" width="239" height="237" border="0" id="pic_102" name="pic_102" title="" alt=""></div>
<div style="position:absolute; left:20px; top:1003px; width:203px; height:189px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpfa897b0b.png" width="203" height="189" border="0" id="pcrv_4" name="pcrv_4" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:10px; top:370px; width:790px; height:465px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpa9552349.png" width="790" height="465" border="0" id="qs_25" name="qs_25" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:250px; top:382px; width:329px; height:77px;">
    <div class="Normal-P">
        <span style="font-size: 28px;">Le FONCTIONNEMENT DES BONS PLANS</span>
	</div>
</div>
<div style="position:absolute; left:11px; top:287px; width:197px; height:74px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpb5f7cf11_05.jpg" width="197" height="74" border="0" id="pic_70" name="pic_70" title="" alt=""></div>
<div id="txt_38" style="position:absolute; left:250px; top:472px; width:540px; height:357px;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box; overflow:hidden;">
<p class="Corps-P"><span class="Corps-C">Le consommateur choisit le bon plan qui lui convient parmi les offres des partenaires
    du Club.</span></p>
<p class="Corps-P"><span class="Corps-C0">Avant de le valider , il faut que ce dernier soit </span><span class="Corps-C">inscrit</span><span class="Corps-C0"> et</span><span class="Corps-C"> identifié</span><span class="Corps-C0"> en qualité
    d’adhérent du Club.</span></p>
<p class="Normal-P0"><span class="Corps-C0">Le consommateur ne reçoit pas de carte d’adhérent, seule la présentation du mail
    de confirmation imprimé ou présent sur son smartphone lui permettra de bénéficier
    de l’offre «&nbsp;Bons Plans&nbsp;» du diffuseur. </span></p>
<p class="Normal-P0"><span class="Normal-C0">&nbsp;</span></p>
<p class="Corps-P"><span class="Corps-C0">&nbsp;</span></p>
<p class="Corps-P"><span class="Corps-C0">&nbsp;</span></p>
<p class="Corps-P"><span class="Corps-C0">&nbsp;</span></p>
<p class="Corps-P"><span class="Corps-C0">Le consommateur identifié peut alors </span><span class="Corps-C">valider la promotion</span><span class="Corps-C0"> qui lui convient.</span></p>
<p class="Corps-P"><span class="Corps-C">Il reçoit une confirmation numérotée par mail lui rappelant, ses coordonnées, ceux
    du partenaire et les caractéristiques de la promotion.</span></p>
<p class="Corps-P"><span class="Corps-C0">Parallèlement, le professionnel reçoit un </span><span class="Corps-C">double numéroté</span><span class="Corps-C0">.</span></p>
<p class="Corps-P"><span class="Corps-C0">L’adhérent </span><span class="Corps-C">se déplace </span><span class="Corps-C0">chez notre partenaire, </span><span class="Corps-C">s’identifie</span><span class="Corps-C0"> en lui montrant </span><span class="Corps-C">le mail
    imprimé</span><span class="Corps-C0"> ou </span><span class="Corps-C">&nbsp;présent sur son Smartphone</span><span class="Corps-C0">.</span></p>
<p class="Corps-P"><span class="Corps-C0">Il bénéfice alors de cette promotion.</span></p>
<p class="Corps-P0"><span class="Corps-C0">&nbsp;</span></p>
</div>
<div style="position:absolute; left:81px; top:1175px; width:101px; height:101px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wp9fe60bc9.png" width="101" height="101" border="0" id="pic_74" name="pic_74" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:250px; top:874px; width:329px; height:40px;">
    <div class="Normal-P">
        <span class="Normal-C">Les points cadeaux</span></div>
</div>
<div style="position:absolute; left:527px; top:980px; width:184px; height:96px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpe6b65753_05.jpg" width="184" height="96" border="0" id="pic_79" name="pic_79" title="" alt=""></div>
<div id="txt_39" style="position:absolute; left:250px; top:927px; width:540px; height:65px;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box; overflow:hidden;">
<p class="Corps-P1"><span class="Corps-C">A chaque concrétisation d&#39;un Bon Plan</span><span class="Corps-C0">, le consommateur valide son achat, crédite
    son compte de 10 points, il les cumule, et les transforme &nbsp;en cadeaux quand il le
    souhaite ... </span></p>
</div>
<div style="position:absolute; left:465px; top:993px; width:51px; height:67px;">
    <div class="Normal-P">
        <span class="Normal-C1">=</span></div>
</div>
<div id="txt_40" style="position:absolute; left:250px; top:1161px; width:540px; height:214px;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box; overflow:hidden;">
<p class="Corps-P1"><span class="Corps-C1">Le fonctionnement !</span></p>
<p class="Corps-P1"><span class="Corps-C0">Le consommateur note le numéro du courriel qu’il a reçu lors de la confirmation de
    sa demande.</span></p>
<p class="Corps-P1"><span class="Corps-C0">Lors de son achat chez le commerçant, il lui demande lui le numéro du courriel qu’il
    a reçu lors de cette demande.</span></p>
<p class="Corps-P1"><span class="Corps-C0">Muni de ces deux codes, le consommateur se connecte sur son compte (ordinateur ou
    mobile), il les enregistre sur les champs correspondant, en les validant notre système
    reconnaîtra les numéros et son compte sera crédité de 10 points. </span></p>
</div>
<div style="position:absolute; left:10px; top:853px; width:790px; height:566px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wp188f76c2.png" width="790" height="566" border="0" id="pic_12" name="pic_12" title="" alt="" onload="OnLoadPngFix()">
	 <div style="position:absolute; left:328px; top:247px; ">
	   <a href = "<?php echo site_url("front/cadeau") ;?>">
	     <img src="<?php echo GetImagePath("front/"); ?>/btn_acces_cadeau.jpg"  border="0" id="pic_cad" name="pic_cad" title="" alt="" onload="OnLoadPngFix()">
	   </a>
	 </div>
	  <div style="position:absolute; left:328px; top:503px; ">
	    <a href = "<?php echo site_url("front/fidelisation") ;?>">
	   <img src="<?php echo GetImagePath("front/"); ?>/btn_validation_cadeau.jpg"  border="0" id="pic_cad" name="pic_valid_cad" title="" alt="" onload="OnLoadPngFix()">
	   </a>
	 </div>
</div>
<div style="position:absolute; left:10px; top:1439px; width:790px; height:343px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpe38975bf.png" width="790" height="343" border="0" id="qs_36" name="qs_36" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:250px; top:1456px; width:541px; height:40px;">
    <div class="Normal-P">
        <span class="Normal-C">Les annonces professionnelles</span></div>
</div>
<div id="txt_42" style="position:absolute; left:250px; top:1518px; width:540px; height:210px;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box; overflow:hidden;">
<p class="Corps-P1"><span class="Corps-C">Seuls les professionnels abonnés à «&nbsp;l’Annuaire Plus&nbsp;» ou aux «&nbsp;BonsPlans&nbsp;» peuvent
    souscrire à cette option.</span></p>
<p class="Corps-P1"><span class="Corps-C0">Ils choisissent un ou plusieurs modules de 20 annonces, ils déposent en temps réel
    leurs produits ou services (destockage, soldes, promotions etc...)</span></p>
<p class="Corps-P1"><span class="Corps-C0">Depuis leur page admin, ils peuvent dans l’année de leur abonnement, gérer ce capital
    de 20 annonces (création, modification, suppression).</span></p>
<p class="Corps-P1"><span class="Corps-C0">Les annonces professionnelles sont également présentes sur notre site internet..
    </span></p>
</div>
<div style="position:absolute; left:812px; top:530px; width:203px; height:590px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpaafd1efe.gif" width="203" height="590" border="0" id="agif2" name="agif2" title="" alt=""></div>
<div style="position:absolute; left:812px; top:1131px; width:203px; height:500px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpa50c3c2b.png" width="203" height="500" border="0" id="pic_44" name="pic_44" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:250px; top:280px; width:406px; height:94px;">
    <div class="Normal-P">
        <span class="Normal-C2">Les avantages des<br></span>
        <span class="Normal-C2">Adhérents du club</span></div>
</div>
<div style="position:absolute; left:333px; top:616px; width:330px; height:40px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wp13ed45f0.png" onclick="document.location = '<?php echo site_url("front/particuliers/inscription"); ?>'" width="330" height="40" border="0" id="pic_90" name="pic_90" title="" alt="" onload="OnLoadPngFix()" style="cursor: pointer;"></div>
<a href="<?php echo site_url("front/annonce/"); ?>" >
	<div style="position:absolute; left:333px; top:1716px; width:330px; height:40px;">
		<img src="<?php echo GetImagePath("front/"); ?>/wpc6377ef4.png" width="330" height="40" border="0" id="pic_93" name="pic_93" title="" alt="" onload="OnLoadPngFix()">
	</div>
</a>
<div style="position:absolute; left:20px; top:378px; width:204px; height:237px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpe5d3c307.png" width="204" height="237" border="0" id="pic_9" name="pic_9" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:69px; top:642px; width:112px; height:114px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpee41f508.png" width="112" height="114" border="0" id="pic_11" name="pic_11" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:25px; top:1443px; width:200px; height:90px;">
    <img src="<?php echo GetImagePath("front/"); ?>/wpe1734cb1.png" width="200" height="90" border="0" id="pic_45" name="pic_45" title="" alt="" onload="OnLoadPngFix()"></div>
<div style="position:absolute; left:812px; top:266px; width:204px; height:255px;">
    <map id="map1" name="map1">
        <area shape="rect" coords="203,0,205,10" href="http://www.proximite-magazine.com/games/games1/cadeaux.html" target="_blank" onclick="window.open(this.href,'_blank','width=820,height=2000,menubar=yes,resizable=no,scrollbars=no,status=no,toolbar=no').focus(); return false;" alt="">
    </map>
    <a href="page21.html"><img src="<?php echo GetImagePath("front/"); ?>/wpaf9f6326.png" width="204" height="255" border="0" id="pic_13" name="pic_13" title="" alt="ACCESCOMPTE" onload="OnLoadPngFix()" usemap="#map1"></a></div>
<?php $this->load->view("front/includes/vwFooter"); ?>