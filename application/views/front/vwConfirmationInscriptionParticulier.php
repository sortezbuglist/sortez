<?php $data["zTitle"] = 'Confirmation inscription particulier' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="fr"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="Generator" content="Serif WebPlus X5">
<meta http-equiv="X-UA-Compatible" content="IE=EmulationIE8">
<!--<meta name="viewport" content="width=800" />-->
<meta name="Viewport" content="width=device-width">
<title>Confirmation inscription</title>
<style type="text/css">
body {margin: 0px; padding: 0px;}
.Normal-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:justify; font-weight:400;
}
.Normal-P-P0
{
    margin:7.0px 0.0px 0.0px 0.0px; text-align:left; font-weight:400;
}
.Corps-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:left; font-weight:400;
}
.Normal-C
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:16.0px; line-height:1.25em;
}
.Normal-C-C0
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:13.0px; line-height:1.23em;
}
.Normal-C-C1
{
    font-family:"Arial", sans-serif; font-size:13.0px; line-height:1.23em;
}
.Corps-C
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C0
{
    font-family:"Arial", sans-serif; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C1
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:21.0px; line-height:1.19em;
}
.Normal-C-C2
{
    font-family:"Arial", sans-serif; font-size:5.0px; line-height:1.20em;
}
.Normal-C-C3
{
    font-family:"Arial", sans-serif; font-weight:700; color:#990000; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C2
{
    font-family:"Verdana", sans-serif; font-size:13.0px; line-height:1.23em;
}
#id_mainbody_head {
	background-image: url(<?php echo GetImagePath("front/"); ?>/fond_mainbody_head.png);
	background-repeat: no-repeat;
	float: left;
	height: 297px;
	width: 620px;
	position: relative;
}
#id_mainbody_main {
	float: left;
	width: 620px;
	position: relative;
	background-image: url(<?php echo GetImagePath("front/"); ?>/fond_mainbody_main.png);
	background-repeat: repeat-y;
}
#id_mainbody_footer {
	background-image: url(<?php echo GetImagePath("front/"); ?>/fond_mainbody_footer.png);
	background-repeat: no-repeat;
	float: left;
	height: 31px;
	width: 620px;
	position: relative;
}
#id_mainbody_content {
	background-image: url(<?php echo GetImagePath("front/"); ?>/fond_mainbody_content.png);
	background-repeat: no-repeat;
	background-position: left bottom;
	float: left;
	width: 610px;
	position: relative;
	padding-right: 5px;
	padding-left: 5px;
	height: auto;
	padding-bottom: 10px;
}
</style>
<script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/jspngfix.js"></script>
<link rel="stylesheet" href="fonctionnementpointscadeaux_fichiers/wpstyles.css" type="text/css">
<script type="text/javascript">var blankSrc = "wpscripts/blank.gif";
</script>
</head>

<body style="background-color: transparent; background-image: url(&quot;<?php echo GetImagePath("front/"); ?>/wp007ff600_06.png&quot;); background-repeat: repeat; background-position: center top; background-attachment: scroll; text-align: center; height: 900px;" text="#000000">
<div style="background-color:#cccccc;text-align:left;margin-left:auto;margin-right:auto;position:relative;width:650px;height:900px;">
<map id="map0" name="map0">
    <area shape="rect" coords="11,121,75,193" href="<?php echo site_url();?>" alt="">
    <area shape="rect" coords="89,121,153,194" href="javascript:history.back();" alt="">
    <area shape="rect" coords="169,121,233,194" href="javascript:history.void();" alt="">
    <area shape="rect" coords="246,121,310,194" href="<?php echo site_url("front/contact");?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wpd22efe04_06.png" id="hs_4" title="" alt="contact" onload="OnLoadPngFix()" usemap="#map0" style="position: absolute; left: 0px; top: 0px;" height="200" border="0" width="650">






<img src="<?php echo GetImagePath("front/"); ?>/wp6cb847ad_06_confirm.png" id="pic_1593" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 15px; top: 216px;" height="643" border="0" width="620">

<div id="txt_477" style="position:absolute;left:25px;top:599px;width:598px;height:206px;overflow:hidden; font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight:bold; text-align:center;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="206">
  <tr>
    <td><?php echo $txtContenu; ?></td>
  </tr>
</table>

	
</div>


</div>

</body></html>