<?php $data["zTitle"] = 'Creation annonce'; ?>
<?php $this->load->view("front/includes/vwHeader", $data); ?>
<?php $this->load->view("front/includes/vwMenu"); ?>
	<script>
		$(function() {
			$( "#IdDateDebut" ).datepicker({ dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
											 monthNames: ['Janvier','Fevier','Mars','Avril','Mai','Juin','Juiller','Aout','Septembre','Octobre','Novembre','Decembre'],
											 dateFormat: 'yy-mm-dd',
											 autoSize: true});
			$( "#IdDateFin" ).datepicker({ dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
											monthNames: ['Janvier','Fevier','Mars','Avril','Mai','Juin','Juiller','Aout','Septembre','Octobre','Novembre','Decembre'],
											dateFormat: 'yy-mm-dd'});
		});
	</script>

    <div id="divFicheAnnonce" class="content" align="center" style="text-align:center; float:left;">
        <form name="frmCreationAnnonce" id="frmCreationAnnonce" action="<?php if (isset($oAnnonce)) { echo site_url("front/annonce/modifAnnonce/$idCommercant"); }else{ echo site_url("front/annonce/creerAnnonce/$idCommercant"); } ?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="annonce[annonce_commercant_id]" id="IdCommercant" value="<?php echo $idCommercant ; ?>" />
		<input type="hidden" name="annonce[annonce_id]" id="IdAnnonce_id" value="<?php if (isset($oAnnonce)) { echo $oAnnonce->annonce_id ; } ?>" />
            <h1><?php echo $title ; ?></h1>
			<span><?php if (isset($msg)) { echo $msg ; } ?></span>
            <fieldset>
                <legend>Annonce</legend>
                <table width="100%" cellpadding="0" cellspacing="0" style="text-align:left; float:left;" align="left">
                    <!--<tr>
                        <td>
                            <label>Sous-rubrique : </label>
                        </td>
                        <td>
                            <select name="annonce[annonce_sous_rubriques_id]" id="idSousRubrique">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($colSousRubriques)) { ?>
                                    <?php foreach($colSousRubriques as $objSousRubrique) { ?>
                                        <option value="<?php echo $objSousRubrique->IdSousRubrique; ?>" <?php if (isset($oAnnonce) && $oAnnonce->annonce_sous_rubrique_id == $objSousRubrique->IdSousRubrique) { echo "selected"; }?> ><?php echo htmlentities($objSousRubrique->Nom); ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>-->
					<tr>
                        <td>
                            <label>Etat : </label>
                        </td>
                        <td>
                            <select name="annonce[annonce_etat]" id="idEtat">
                                	<option value="c">-- Veuillez choisir --</option>
									<option value="0" <?php  if (isset($oAnnonce) && $oAnnonce->annonce_etat == 0) { echo "selected"; } ?>>Revente</option>
									<option value="1"  <?php if (isset($oAnnonce) && $oAnnonce->annonce_etat == 1) { echo "selected"; } ?>>Neuf</option>
                                    <option value="2"  <?php if (isset($oAnnonce) && $oAnnonce->annonce_etat == 2) { echo "selected"; } ?>>Service</option>
                            </select>
                        </td>
                    </tr>
					<tr>
                        <td>
                            <label>Date debut : </label>
                        </td>
                        <td>
                            <input type="text" name="annonce[annonce_date_debut]" id="IdDateDebut" value="<?php if (isset($oAnnonce)) { echo $oAnnonce->annonce_date_debut ; } ?>" />
                        </td>
                    </tr>
					<tr>
                        <td>
                            <label>Date fin : </label>
                        </td>
                        <td>
                            <input type="text" name="annonce[annonce_date_fin]" id="IdDateFin" value="<?php if (isset($oAnnonce)) { echo $oAnnonce->annonce_date_fin ; } ?>" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Titre : </label>
                        </td>
                        <td>
							<textarea rows="2" cols="20" name="annonce[annonce_description_courte]" id="idDescriptionCourt"><?php if (isset($oAnnonce)) { echo $oAnnonce->annonce_description_courte ; } ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>D&eacute;scription long : </label>
                        </td>
                        <td>
							<textarea rows="2" cols="20" name="annonce[annonce_description_longue]" id="idDescriptionLong"><?php if (isset($oAnnonce)) { echo $oAnnonce->annonce_description_longue ; } ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Prix neuf : </label>
                        </td>
                        <td>
                            <input type="text" name="annonce[annonce_prix_neuf]" id="idPrixNeuf" value="<?php if (isset($oAnnonce)) { echo $oAnnonce->annonce_prix_neuf ; } ?>" />
                            &euro;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Prix solde : </label>
                        </td>
                        <td>
                            <input type="text" name="annonce[annonce_prix_solde]" id="IdPrixSolde" value="<?php if (isset($oAnnonce)) { echo $oAnnonce->annonce_prix_solde ; } ?>" />
                            &euro;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Photo 01 : </label>
                        </td>
                        <td>
                        	<?php if (isset($oAnnonce) && $oAnnonce->annonce_photo1 != "" && $oAnnonce->annonce_photo1 != NULL) { ?>
                        		<img src="<?php echo GetImagePath("front/"); ?>/<?php echo $oAnnonce->annonce_photo1; ?>" id="loading1" width="75"/>
                            <?php } ?>
							<input type="hidden" name="annonce[annonce_photo1]" id="IdPhoto1" value="<?php if (isset($oAnnonce)) { echo $oAnnonce->annonce_photo1 ; } ?>" />
                            <input type="file" name="annoncePhoto1" id="annoncePhoto1" value="" onchange="javascript:UploadImage ('<?php echo site_url("front/upload/index/");?>', 'annoncePhoto1', '1');"/><img src="<?php echo GetImagePath("front/"); ?>/loading.gif" id="loading1" style="display:none"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Photo 02 :</label>
                        </td>
                        <td>
                        	<?php if (isset($oAnnonce) && $oAnnonce->annonce_photo2 != "" && $oAnnonce->annonce_photo2 != NULL) { ?>
                        		<img src="<?php echo GetImagePath("front/"); ?>/<?php echo $oAnnonce->annonce_photo2; ?>" id="loading1" width="75"/>
                            <?php } ?>
							<input type="hidden" name="annonce[annonce_photo2]" id="IdPhoto2" value="<?php if (isset($oAnnonce)) { echo $oAnnonce->annonce_photo2 ; } ?>" />
                            <input type="file" name="annoncePhoto2" id="annoncePhoto2" value="" onchange="javascript:UploadImage ('<?php echo site_url("front/upload/index/");?>', 'annoncePhoto2', '2');"/><img src="<?php echo GetImagePath("front/"); ?>/loading.gif" id="loading2" style="display:none"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Photo 03 :</label>
                        </td>
                        <td>
                        	<?php if (isset($oAnnonce) && $oAnnonce->annonce_photo3 != "" && $oAnnonce->annonce_photo3 != NULL) { ?>
                        		<img src="<?php echo GetImagePath("front/"); ?>/<?php echo $oAnnonce->annonce_photo3; ?>" id="loading1" width="75"/>
                            <?php } ?>
							<input type="hidden" name="annonce[annonce_photo3]" id="IdPhoto3" value="<?php if (isset($oAnnonce)) { echo $oAnnonce->annonce_photo3 ; } ?>" />
                            <input type="file" name="annoncePhoto3" id="annoncePhoto3" value="" onchange="javascript:UploadImage ('<?php echo site_url("front/upload/index/");?>', 'annoncePhoto3', '3');"/><img src="<?php echo GetImagePath("front/"); ?>/loading.gif" id="loading3" style="display:none"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Photo 04 :</label>
                        </td>
                        <td>
                        	<?php if (isset($oAnnonce) && $oAnnonce->annonce_photo4 != "" && $oAnnonce->annonce_photo4 != NULL) { ?>
                        		<img src="<?php echo GetImagePath("front/"); ?>/<?php echo $oAnnonce->annonce_photo4; ?>" id="loading1" width="75"/>
                            <?php } ?>
							<input type="hidden" name="annonce[annonce_photo4]" id="IdPhoto4" value="<?php if (isset($oAnnonce)) { echo $oAnnonce->annonce_photo4 ; } ?>" />
                            <input type="file" name="annoncePhoto4" id="annoncePhoto4" value="" onchange="javascript:UploadImage ('<?php echo site_url("front/upload/index/");?>', 'annoncePhoto4', '4');"/><img src="<?php echo GetImagePath("front/"); ?>/loading.gif" id="loading4" style="display:none"/>
                        </td>
                    </tr>
					<tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
					<tr>
                        <td></td>
                        <td align="left"><input type="button" value="Annuler" onclick="javascript:annulationAjout('<?php echo site_url("front/annonce/annulationAjout");?>', '<?php echo site_url("front/annonce/listeMesAnnonces/");?>', '<?php if (!isset($oAnnonce)){ echo "ajout" ;}?>', '<?php echo $idCommercant ;?>');" />&nbsp;<input type="button" value="Valider" onclick="javascript:testFormAnnonce();" /></td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
<?php $this->load->view("front/includes/vwFooter"); ?>