<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="fr"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="Generator" content="Serif WebPlus X5">
<meta http-equiv="X-UA-Compatible" content="IE=EmulationIE8">
<title>Contact</title>

<style type="text/css">
body {margin: 0px; padding: 0px;}
.Corps-artistique-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:left; font-weight:400;
}
.Corps-P
{
    margin:0.0px 0.0px 12.0px 0.0px; text-align:justify; font-weight:400;
}
.Corps-P-P0
{
    margin:0.0px 0.0px 1.0px 0.0px; text-align:left; font-weight:400;
}
.Normal-P
{
    margin:0.0px 0.0px 1.0px 0.0px; text-align:left; font-weight:400;
}
.Corps-artistique-C
{
    font-family:"Verdana", sans-serif; font-size:12.0px; line-height:1.17em;
}
.Corps-C
{
    font-family:"Verdana", sans-serif; font-size:12.0px; line-height:1.17em;
}
.Corps-C-C0
{
    font-family:"Verdana", sans-serif; font-size:11.0px; line-height:1.18em;
}
.Corps-C-C1
{
    font-family:"Verdana", sans-serif; font-size:9.0px; line-height:1.33em;
}
.Corps-C-C2
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C3
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:12.0px; line-height:1.25em;
}
.Normal-C
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:12.0px; line-height:1.25em;
}
.Normal-C-C0
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
</style>
<script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/jspngfix.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>
<link rel="stylesheet" href="contact_fichiers/wpstyles.css" type="text/css">
<script type="text/javascript">var blankSrc = "wpscripts/blank.gif";
</script>

<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/jquery-1.4.2.min.js"></script>
<script type="text/javascript">
	
		 
		 
		 // Use jQuery via $(...)
		 $(document).ready(function(){//debut ready fonction
			   
		//verify Nom	   
		$("#nomcontact").focus(function(){
									$(this).val("");			 
								});
		$("#nomcontact").blur(function(){
									if ($(this).val() == "") {
										$(this).val("Votre nom*");
									}
								});	
		
		//verify Prénom	   
		$("#prenomcontact").focus(function(){
									$(this).val("");			 
								});
		$("#prenomcontact").blur(function(){
									if ($(this).val() == "") {
										$(this).val("Votre prénom*");
									}
								});	
		//verify mail	   
		$("#emailcontact").focus(function(){
									$(this).val("");			 
								});
		$("#emailcontact").blur(function(){
									if ($(this).val() == "") {
										$(this).val("Votre courriel*");
									}
								});	
		//verify tel	   
		$("#telcontact").focus(function(){
									$(this).val("");			 
								});
		$("#telcontact").blur(function(){
									if ($(this).val() == "") {
										$(this).val("Votre numéro de téléphone*");
									}
								});	
		//verify mess	   
		$("#contenucontact").focus(function(){
									$(this).val("");			 
								});
		$("#contenucontact").blur(function(){
									if ($(this).val() == "") {
										$(this).val("Votre message*");
									}
								});	
			   
			   
		//verify field form value 
		$("#btnEnvoicontact").click(function(){
										  
				var txtError = "";
				
				var nomcontact = $('#nomcontact').val();
				  if (nomcontact == "" || nomcontact == "Votre nom*") {
					txtError += "- Vous devez préciser votre Nom<br/>";
				  }  
				
				var prenomcontact = $('#prenomcontact').val();
				  if (prenomcontact == "" || prenomcontact == "Votre prénom*") {
					txtError += "- Vous devez préciser votre prénom<br/>";
				  } 
				
				var emailcontact = $('#emailcontact').val();
				  if (emailcontact == "" || emailcontact == "Votre courriel*") {
					txtError += "- Vous devez préciser votre email<br/>";
				  }
				  if(!isEmail(emailcontact)) {
					txtError += "- Veuillez entrer un email valide.<br/>";
				  }
				  
				var telcontact = $('#telcontact').val();
				  if (telcontact == "" || telcontact == "Votre numéro de téléphone*") {
					txtError += "- Vous devez préciser votre téléphone<br/>";
				  }
				  
				var contenucontact = $('#contenucontact').val();
				  if (contenucontact == "" || contenucontact == "Votre message*") {
					txtError += "- Vous devez préciser le contenu de votre message<br/>";
				  }
				
				if ($('#acceptenews0').is(":checked")) {
					$("#acceptenews").val("1");
				} else {
					$("#acceptenews").val("0");
				}
				
				
				//verify captcha
				var captcha = $('#captcha').val();
				  if (captcha == "") {
					txtError += "- Vous devez remplir le captcha<br/>";
				  } else {
					  $.ajax({
                                    type: "GET",
                                    url: "<?=base_url(); ?>front/professionnels/verify_captcha/"+captcha,
                                    success: function(msg){
                                        //alert(msg);
                                        var numero_reponse = msg;
                                        //alert(numero_reponse);
										$("#divCaptchavalueverify").val(numero_reponse);
                                    }
                             });
                             
				  }
				  
				 var divCaptchavalueverify = $('#divCaptchavalueverify').val();
				  if (divCaptchavalueverify == "0") {
					txtError += "- Les Textes que vous avez entré ne sont pas valides.<br/>";
				  } 
				// end verify captcha  
				  
				
				//final verification of input error
				if(txtError == "") {
					$("#formContact").submit();
				} else {
					$("#divErrorFrmContact").html(txtError);
				}
			})
		
		
		
		
		
    })
		 
		 
    </script>


</head>

<body style="background-color: transparent; background-image: url(&quot;<?php echo GetImagePath("front/"); ?>/wp007ff600_06.png&quot;); background-repeat: repeat; background-position: center top; background-attachment: scroll; text-align: center; height: 1500px;" text="#000000">
<div style="background-color:#cccccc;text-align:left;margin-left:auto;margin-right:auto;position:relative;width:650px;height:1500px;">
<map id="map0" name="map0">
    <area shape="rect" coords="11,121,75,193" href="<?php echo site_url();?>" alt="">
    <area shape="rect" coords="89,121,153,194" href="javascript:history.back()" alt="">
    <area shape="rect" coords="169,121,233,194" alt="">
    <area shape="rect" coords="246,121,310,194" href="<?php echo site_url("front/contact");?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wpd22efe04_06.png" id="hs_4" title="" alt="contact" onload="OnLoadPngFix()" usemap="#map0" style="position: absolute; left: 0px; top: 0px;" height="200" border="0" width="650">
<img src="<?php echo GetImagePath("front/"); ?>/wp4815d3de_06.png" id="pic_1579" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 15px; top: 217px;" height="520" border="0" width="620">
<div id="art_73" style="position:absolute;left:72px;top:297px;width:432px;height:14px;">
    <div class="Corps-artistique-P">
        <span class="Corps-artistique-C">Merci de bien vouloir préciser vos coordonnées et votre demande</span></div>
</div>
<form id="formContact" name="formContact" action="<?php echo site_url('front/contact/envoyer/'); ?>" method="post" style="margin:0px;">

<input id="emailcontact" name="contact[email]" tabindex="3" maxlength="100" value="Votre courriel*" style="position: absolute; left: 68px; top: 367px; width: 232px;" type="text">

<input id="acceptenews0" name="acceptenews0" style="position: absolute; left: 68px; top: 557px;" tabindex="6" type="checkbox">
<input type="hidden" name="contact[acceptenews]" id="acceptenews" value=""/>

<!--<div id="txt_45" style="position:absolute; left:67px; top:598px; width:251px; height:40px; overflow:hidden;">
<span class="Corps-C">Merci de saisir exactement les caractères qui s’affichent dans l’image</span>
</div>-->

<div id="divErrorFrmContact" style="position:absolute; left:49px; top:610px; width:273px; height:126px; overflow:hidden; color:#F00; font-family:Arial, Helvetica, sans-serif; font-size:9px;">
</div>

<input style="position: absolute; left: 475px; top: 683px; width: 90px; height: 22px;" id="btnEnvoicontact" name="btnEnvoicontact" value="Envoyer" type="button" tabindex="8">
<div id="txt_47" style="position:absolute;left:96px;top:555px;width:472px;height:45px;overflow:hidden;">
<p class="Corps-P"><span class="Corps-C-C0">J’accepte de recevoir par courriel des informations provenant de cet établissement.</span></p>
</div>
<input id="telcontact" name="contact[tel]" maxlength="100" tabindex="4" value="Votre numéro de téléphone*" style="position: absolute; left: 332px; top: 367px; width: 236px;" type="text">
<textarea id="contenucontact" rows="8" cols="60" name="contact[contenu]" tabindex="5" style="position: absolute; left: 68px; top: 403px; width: 500px; height: 134px;">Votre message*</textarea>
<input style="position: absolute; left: 329px; top: 684px; width: 87px; height: 22px;" id="butn_2" value="Rétablir" type="reset" tabindex="9">
<div style="position:absolute;left:348px;top:595px;width:190px;height:69px;">
    
    
    <!--<img src="<?php echo GetImagePath("front/"); ?>/img_verify.png"><br><input id="edit_19" name="captcha" size="20" type="text">
    <a title="Ecouter un CAPTCHA audio" href="http://www.serifwebresources.com/util/audio/audio.php?lang="><img alt="Ecouter un CAPTCHA audio" style="vertical-align: middle;" src="<?php echo GetImagePath("front/"); ?>/audio-desc.png" border="0"></a>-->
    
    
    <?php echo $captcha['image']; ?><br />
    <input name="captcha" value="" id="captcha" type="text" tabindex="7">
    <input name="divCaptchavalueverify" id="divCaptchavalueverify" value="" type="hidden" />
    
    
    </div>

<div id="txt_48" style="position:absolute;left:67px;top:315px;width:128px;height:16px;overflow:hidden;">
<p class="Corps-P"><span class="Corps-C-C1">* champs obligatoires</span></p>
</div>
<input id="nomcontact" name="contact[nom]" tabindex="1" maxlength="50" value="Votre nom*" style="position: absolute; left: 68px; top: 335px; width: 232px;" type="text">
<input id="prenomcontact" name="contact[prenom]" tabindex="2" maxlength="50" value="Votre prénom*" style="position: absolute; left: 332px; top: 335px; width: 236px;" type="text">
<img src="<?php echo GetImagePath("front/"); ?>/wp999f4cb7_06.png" id="pic_1581" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 15px; top: 758px;" height="301" border="0" width="620">
<div id="txt_49" style="position:absolute;left:40px;top:826px;width:512px;height:205px;overflow:hidden;">
<p class="Corps-P-P0"><span class="Corps-C-C2">La SARL Phase est l’éditeur de ce site</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C2">L'hébergement est assuré par la SARL Phase</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C3">Adresse :</span><span class="Corps-C-C2"> 60 avenue de Nice 06800 Cagnes sur Mer</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C3">Directeur de publication : </span><span class="Corps-C-C2">Jean Pierre Woignier</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C3">Téléphone direct : </span><span class="Corps-C-C2">04.93.73.84.51</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C3">Téléphone mobile :</span><span class="Corps-C-C2"> 06.72.05.59.35</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C3">Courrier :</span><span class="Corps-C-C2"> proximite-<wbr>magazine@orange.fr</span></p>
<p class="Normal-P"><span class="Normal-C">Statut :</span><span class="Normal-C-C0"> SARL au capital de 7265€ </span></p>
<p class="Normal-P"><span class="Normal-C">N° Registre de commerce :</span><span class="Normal-C-C0"> RCS Antibes 412.071.466</span></p>
<p class="Normal-P"><span class="Normal-C">SIRET :</span><span class="Normal-C-C0"> 412071466 00012</span></p>
<p class="Normal-P"><span class="Normal-C">CODE NAF :</span><span class="Normal-C-C0"> 7022Z</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C3">IBAN :</span><span class="Corps-C-C2"> FR76 3007 6023 5110 6081 0020 072</span></p>
</div>
<script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/jsValidation.js"></script>
</form>
</div>

</body></html>