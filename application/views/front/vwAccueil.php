<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("front/includes/vwHeader", $data); ?>
<?php $this->load->view("front/includes/vwMenu"); ?>

<!--start main menu bloc-->
<style type="text/css">
<!--
#mainbody_header {
	background-image: url(<?php echo GetImagePath("front/"); ?>/bg_mainbody_header.png);
	background-repeat: no-repeat;
	background-position: left;
	float: left;
	height: 133px;
	width: 100%;
}
#id_mainbody_main {
	background-image: url(<?php echo GetImagePath("front/"); ?>/bg_mainbody_main.png);
	background-repeat: repeat-y;
	float: left;
	width: 100%;
	position: relative;
}
#id_mainbody_footer {
	background-image: url(<?php echo GetImagePath("front/"); ?>/bg_mainbody_footer.png);
	background-repeat: no-repeat;
	float: left;
	height: 24px;
	width: 100%;
	position: relative;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	padding-left: 30px;
}
#id_mainbody_mainrepete {
	background-image: url(<?php echo GetImagePath("front/"); ?>/bg_mainbody_separator.png);
	background-repeat: no-repeat;
	background-position: left bottom;
	float: left;
	width: 100%;
	position: relative;
	font-family: "Arial",sans-serif;
    font-size: 12px;
}

.status_btn {
	height: 43px;
}
-->
</style>


<map id="map0" name="map0">
    <area shape="rect" coords="24,172,89,245" href="<?php echo site_url();?>" alt="">
    <area shape="rect" coords="102,173,166,245" href="javascript:history.back()" alt="">
    <area shape="rect" coords="182,173,246,245" href="<?php echo site_url("front/annonce");?>" alt="">
    <area shape="rect" coords="259,173,323,245" href="<?php echo site_url("front/contact");?>" alt="">
</map>
<!--end main menu bloc-->

<!--start menu bloc for home-->
<map id="map1" name="map1">
  <area shape="poly" coords="181,272,187,263,187,43,175,0,7,0,0,18,0,230,12,273,155,273" href="<?php echo site_url();?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wpfc1d0860_06.png" id="pic_1489" title="" alt="Accueil" onload="OnLoadPngFix()" usemap="#map1" style="position: absolute; left: 15px; top: 265px;" height="273" border="0" width="187">

<map id="map3" name="map3">
  <area shape="poly" coords="181,272,187,263,187,43,175,0,7,0,0,18,0,230,12,273,155,273" href="<?php echo site_url("front/annonce");?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wpb284a532_06.png" id="pic_1509" title="" alt="annonces" onload="OnLoadPngFix()" usemap="#map3" style="position: absolute; left: 217px; top: 265px;" height="273" border="0" width="187">

<map id="map2" name="map2">
  <area shape="poly" coords="181,272,187,263,187,43,175,0,7,0,0,18,0,230,12,273,155,273" href="<?php echo site_url("front/bonplan");?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wp9326ed95_06.png" id="pic_1506" title="" alt="bons plans" onload="OnLoadPngFix()" usemap="#map2" style="position: absolute; left: 418px; top: 265px;" height="273" border="0" width="187">

<map id="map4" name="map4">
  <area shape="poly" coords="181,272,187,263,187,43,175,0,7,0,0,18,0,230,12,273,155,273" href="<?php echo site_url("front/cadeau");?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wpc783aaac_06.png" id="pic_1510" title="" alt="fonctionnementpointscadeaux" onload="OnLoadPngFix()" usemap="#map4" style="position: absolute; left: 620px; top: 265px;" height="273" border="0" width="187">

<!--end menu bloc for home-->


<!--start main body bloc-->


<div id="main_body_bloc" style="position: absolute; left: 15px; top: 560px; height:auto; width:792px" border="0">
  <div id="mainbody_header">
  <form name="frmRechercheAnnonce" method="post" action="<?php echo site_url(); ?>">
  <input type="hidden" name="hdnFavoris" id="hdnFavoris" value="" />
  	<div style="margin-left:10px; margin-top:15px;">
    <table width="100%" border="0" cellspacing="5" cellpadding="0">
      <tr>
        <td><span class="Normal-C">Recherche par thèmes</span></td>
        <td><span class="Normal-C">Recherche par communes</span></td>
      </tr>
      <tr>
        <td>
        <select type="inputStringHidden" name="inputStringHidden" style="width:366px">
            <option value="0">-- Veuillez choisir --</option>
            <?php foreach($toCategorie as $oCategorie){ ?>
                <option value="<?php echo $oCategorie->IdSousRubrique ; ?>" <?php if(isset($iCategorieId) && $iCategorieId == $oCategorie->IdSousRubrique) echo 'selected="selected"'; ?>><?php echo $oCategorie->Nom ; ?>&nbsp;(<?php echo $oCategorie->nbCommercant ; ?>)</option>
            <?php } ?>
        </select>
        </td>
        <td>
        <select type="inputStringVilleHidden" name="inputStringVilleHidden" style="width:366px">
            <option value="0">-- Veuillez choisir --</option>
            <?php foreach($toVille as $oVille){ ?>
                <option value="<?php echo $oVille->IdVille ; ?>" <?php if(isset($iVilleId) && $iVilleId == $oVille->IdVille) echo 'selected="selected"'; ?>><?php echo $oVille->Nom ; ?></option>
            <?php } ?>
        </select>
        </td>
      </tr>
      <tr>
        <td><span class="Normal-C">Recherche par mots clés</span></td>
        <td rowspan="2">
        <div style="text-align:right; margin-top:10px; margin-right:10px;"> <a href="Javascript:void();" onclick="document.frmRechercheAnnonce.submit();"><img src="<?php echo GetImagePath("front/"); ?>/wpa9e8bbcd_06.png" alt="" name="pic_1504" width="106" height="30" border="0" id="pic_1504" style="" onload="OnLoadPngFix()"></a>
		  <a href="Javascript:void();" onclick="document.location='<?php echo site_url(); ?>' ;"><img src="<?php echo GetImagePath("front/"); ?>/wpc625ec40_06.png" alt="" name="pic_1505" width="106" height="30" border="0" id="pic_1505" style="" onload="OnLoadPngFix()"></a>
        </div>
        </td>
      </tr>
      <tr>
        <td><input type="text" name="zMotCle" id="zMotCle" value="<?php if(isset($zMotCle)) echo $zMotCle; ?>" style="width: 362px;"/></td>
       
      </tr>
    </table>
	</div>
    </form>
  </div>
  <div id="id_mainbody_main">
  <?php 
		
	/*	
		echo "Liens : ".$iNombreLiens;
		echo "<br/>PerPage : ".$PerPage;
		echo "<br/>TotalRows : ".$TotalRows;*/
		?>
  	<?php  foreach($toCommercant as $oCommercant){ ?>
    <div id="id_mainbody_mainrepete">
		<?php 
        $objasscommrubr = $this->sousRubrique->GetById($oCommercant->IdSousRubrique);
        
        ?>
    
    
<table width="100%" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td rowspan="2" style="width:215px">
            <?php 
				//URL formating
				//echo RemoveExtendedChars2("smèémè émèémèé-hàùhà ùhàùhàùhs");
				//$commercant_url_nom_com = RemoveExtendedChars2($oCommercant->NomSociete);
				//$commercant_url_nom_ville = RemoveExtendedChars2($oCommercant->ville);
				
				//$commercant_url_nom = $commercant_url_nom_com."_".$commercant_url_nom_ville;
				$commercant_url_nom = $oCommercant->nom_url;
				$commercant_url_home = $commercant_url_nom;
				$commercant_url_annonces = $commercant_url_nom."/annonces";
				$commercant_url_notre_bonplan = $commercant_url_nom."/notre_bonplan";
				?>
            <a  target='_blank' href="<?php echo site_url($commercant_url_home);?>">
            	<?php 
				$image_home_vignette = "";
				if (isset($oCommercant->PhotoAccueil) && $oCommercant->PhotoAccueil != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->PhotoAccueil)==true){ $image_home_vignette = $oCommercant->PhotoAccueil;}
				else if (isset($oCommercant->Photo1) && $oCommercant->Photo1 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo1)==true){ $image_home_vignette = $oCommercant->Photo1;}
				else if (isset($oCommercant->Photo2) && $oCommercant->Photo2 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo2)==true){ $image_home_vignette = $oCommercant->Photo2;}
				else if (isset($oCommercant->Photo3) && $oCommercant->Photo3 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo3)==true){ $image_home_vignette = $oCommercant->Photo3;}
				else if (isset($oCommercant->Photo4) && $oCommercant->Photo4 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo4)==true){ $image_home_vignette = $oCommercant->Photo4;}
				else if (isset($oCommercant->Photo5) && $oCommercant->Photo5 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo5)==true){ $image_home_vignette = $oCommercant->Photo5;}
				?>
                <img src="<?php if ($image_home_vignette != ""){ echo GetImagePath("front/photoCommercant/"); ?>/<?php echo $image_home_vignette ; }else{echo GetImagePath("front/")."/wp71b211d2_06.png";}?>" width="210" border="0" id="pic_965" name="pic_965" title="" alt="">
            </a>
            </td>
            <td valign="top" style="width:483px">
            <strong>
            <span style="font-size:13px;"><br />
			<?php if ($objasscommrubr) echo $objasscommrubr->Nom ; ?><br />
            <?php echo $oCommercant->NomSociete ; ?><br /></span>
            <?php echo $oCommercant->ville ; ?>, <?php echo $oCommercant->Adresse2 ; ?>, <?php echo $oCommercant->Adresse1 ; ?></strong>
            </td>
            <td valign="top" rowspan="2" style="width:70px">
            <p class="status_btn">
            
            
            
            	<?php $ob_annonce_com = $this->mdlannonce->listeMesAnnonces($oCommercant->IdCommercant);
				if ($ob_annonce_com) {	?>
                    <a href="<?php echo site_url($commercant_url_annonces);?>" target="_blank">
                    <img src="<?php echo GetImagePath("front/"); ?>/wp36996cc9_06.png" id="pic_1523" alt="" onload="OnLoadPngFix()" height="50" border="0" width="50">
                    </a>
                <?php } else { ?>
                <img src="<?php echo GetImagePath("front/"); ?>/wpe0c4f9b0_06.png" id="pic_1523" alt="" onload="OnLoadPngFix()" height="50" border="0" width="50">
                <?php } ?>
            </p>
            <p class="status_btn">
            	<?php $ob_bonpl_com = $this->mdlbonplan->getListeBonPlan($oCommercant->IdCommercant);
				if ($ob_bonpl_com) {	?>
                	<a href="<?php echo site_url($commercant_url_notre_bonplan);?>" target="_blank">
                    <img src="<?php echo GetImagePath("front/"); ?>/wp12fa27c8_06.png" id="pic_1523" alt="" onload="OnLoadPngFix()" height="50" border="0" width="50">
                    </a>
                <?php } else { ?>
                <img src="<?php echo GetImagePath("front/"); ?>/wp4f97e87c_06.png" id="pic_1523" alt="" onload="OnLoadPngFix()" height="50" border="0" width="50">
                <?php } ?>
                </p>
                
            <p class="status_btn"><a  target='_blank' href="<?php echo site_url($commercant_url_home);?>"><img src="<?php echo GetImagePath("front/"); ?>/wp68233b54_06.png" id="pic_1526" alt="" onload="OnLoadPngFix()" height="50" border="0" width="50"></a></p>
            </td>
          </tr>
          <tr>
            <td>
            <div style="margin-right:30px;">
            <?php if( $oCommercant->Caracteristiques != "" ) { ?>
            <?php //if ($oCommercant->IdCommercant==300066) echo truncate($oCommercant->Caracteristiques,150,$etc = " ..."); else echo truncate($oCommercant->Caracteristiques,300,$etc = " ..."); ?>
            <?php } ?>
            
            <?php 
			$base_path_system_rand = str_replace('system/', '', BASEPATH);
			//$validation_delete_path_rand = delete_directory_rand($base_path_system_rand."/pagecaracteristics");
			$assetPath_rand = $base_path_system_rand."/pagecaracteristics/";
			if (is_dir($assetPath_rand)==false) @mkdir($assetPath_rand,0777,true);
			$fichierNom_rand = md5(rand() * time()).".html";
			$fichierCheminComple_rand = $assetPath_rand.$fichierNom_rand;
			if( $oCommercant->Caracteristiques != "" ) {
				$fichierContenu_rand = truncate($oCommercant->Caracteristiques,300,$etc = " ...");
				$leFichier_rand = fopen($fichierCheminComple_rand, "w+");
				fwrite($leFichier_rand,$fichierContenu_rand);
				fclose($leFichier_rand);
			}
			?>
            <?php if (file_exists($fichierCheminComple_rand)) { ?>
            	<iframe width="450px" src="<?php echo base_url();?>pagecaracteristics/<?php echo $fichierNom_rand;?>" style="border:none;" frameborder="0" scrolling="no" height="120px"></iframe>
            <?php } ?>
            
            </div>
            </td>
           
          </tr>
          
        </table>

    </div>
    <?php  } ?>
  </div>
  <div id="id_mainbody_footer">
  <?php														
            if($iNombreLiens != 1){
                ?>Pages : <?php
                $iPageCourante = $argOffset ;
                for($i=1; $i<=$iNombreLiens; $i++){
                    $argOffset = ($i - 1 ) * $PerPage ;
                    ?>
                    <?php if($iPageCourante == $argOffset){ ?>
                        <span style="cursor:pointer; color:red; font-weight: bold;" onclick="document.location='<?php echo site_url("accueil/redirection/". $argOffset) ; ?>' ;">
                            <?php echo $i ; ?>
                        </span>&nbsp;
                    <?php }else{?>
                        <span style="cursor:pointer;text-decoration: underline" onclick="document.location='<?php echo site_url("accueil/redirection/". $argOffset) ; ?>' ;">
                            <?php echo $i ; ?>
                        </span>&nbsp;
                    <?php }?>
                    <?php
                }
            }
	?>
  </div>
  
  
  
  
  
  <div id="id_mainbody_copyrig" style="width:100%;height:73px; position:relative; float:left; margin-top:40px;">
    <div class="Normal-P-P0">
        <span class="Normal-C-C0">Le &nbsp;Club Proximité est une marque déposée et une réalisation du Magazine Proximité<br></span></div>
    <div class="Corps-P-P0">
        <span class="Corps-C-C6"> Editeur : Phase SARL 60 avenue de Nice 06800 Cagnes-<wbr>sur-<wbr>Mer &nbsp;</span><span class="Corps-C-C7">Sarl au capital de 7265 € -<wbr> RCS Antibes 412.071.466<br></span><span class="Corps-C">Téléphone : 00 33 (0)4 93 73 84 51 -<wbr> Mobile : 00 33 (0)6 72 05 59 35 -<wbr> proximite-<wbr>magazine@orange.fr</span></div>
</div>
  
  
</div>
<!--end main body bloc-->


<?php $this->load->view("front/includes/vwFooter"); ?>