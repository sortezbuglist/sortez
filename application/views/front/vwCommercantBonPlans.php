<?php $data["zTitle"] = 'Liste de mes Bon plans'; ?>
<?php $this->load->view("front/includes/vwHeader", $data); ?>
<?php $this->load->view("front/includes/vwMenu"); ?>
<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra'], headers: { 4: {sorter: false}, 7: { sorter: false}, 8: {sorter: false} }})
				.tablesorterPager({container: $("#pager")});
		});
</script>
    <div id="divMesBnplan" class="content" align="center">
        <form name="frmMesAnnonces" id="frmMesAnnonces" action="" method="POST" enctype="multipart/form-data">
            <h1>Liste de mes Bon plans</h1>
            
				<?php if (count($toListeMesBonplans) == 0) { ?>
				<span align="left"><a href="<?php echo site_url("front/bonplan/ficheBonplan/$idCommercant") ;?>">
				<input type="button" name="add_new_bp" value="Ajouter un nouveau" onclick="document.location='<?php echo site_url("front/bonplan/ficheBonplan/$idCommercant") ;?>';"/>
				</a></span>
				<?php } ?>
                
    <br><span align="left"><a href="<?php echo site_url("front/professionnels/fiche/$idCommercant") ;?>"><input type="button" name="return_menu" value="Retour au menu" onclick="document.location='<?php echo site_url("front/professionnels/fiche/$idCommercant") ;?>';"/></a></span>
				<div id="container">
					<table cellpadding="1" class="tablesorter">
						<thead>
							<tr>
								<th>Titre</th>
								<th width="300">Description</th>
								<!--<th>Categorie</th>-->
								<!--<th>Ville</th>-->
								<!--<th>Nbr pris</th>-->
								<th>Date debut</th>
								<th>Date fin</th>
								<th width="10">&nbsp;</th>
								<th width="10">&nbsp;</th>
							</tr>
						</thead>
						<tbody> 
							<?php foreach($toListeMesBonplans as $oListeMesBonplans){ ?>
								<tr>                    
									<td><?php echo $oListeMesBonplans->bonplan_titre ; ?></td>
									<td><?php echo truncate($oListeMesBonplans->bonplan_texte,150,$etc = " ..."); ?></td>
									<!--<td><?php// echo $oListeMesBonplans->categorie_nom ; ?></td>-->
									<!--<td><?php// echo $oListeMesBonplans->Nom  ; ?></td>-->
									<!--<td><?php// echo $oListeMesBonplans->bonplan_nombrepris  ; ?></td>-->
									<td><?php echo $oListeMesBonplans->bonplan_date_debut ; ?></td>
									<td><?php echo $oListeMesBonplans->bonplan_date_fin ; ?></td>
									<td><a href="<?php echo site_url("front/bonplan/ficheBonplan/" . $oListeMesBonplans->bonplan_commercant_id . "/" . $oListeMesBonplans->bonplan_id ) ; ?>">Modifier</a></td>
									<td><a href="<?php echo site_url("front/bonplan/supprimBonplan/" . $oListeMesBonplans->bonplan_id . "/" . $oListeMesBonplans->bonplan_commercant_id) ; ?>" onclick="if (!confirm('voulez-vous vraiment supprimer cette annoce?')){ return false ; }">Supprimer</a></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
					<div id="pager" class="pager">
							<img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
							<img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
							<input type="text" class="pagedisplay"/>
							<img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
							<img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
							<select class="pagesize" style="visibility:hidden">
								<option selected="selected"  value="10">10</option>
								<option value="20">20</option>
								<option value="30">30</option>
								<option  value="40">40</option>
							</select>
					</div>
				</div>
        </form>
    </div>
<?php $this->load->view("front/includes/vwFooter"); ?>