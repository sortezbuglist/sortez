<?php $data["zTitle"] = 'Accueil' ?>
<?php //$this->load->view("frontAout2013/includes/header_mini_2", $data); ?>
<?php $this->load->view("sortez/includes/header_frontoffice_com", $data); ?>


<!-- Bootstrap core CSS -->
<link href="<?php echo GetCssPath("bootstrap/"); ?>/bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php echo GetCssPath("bootstrap/"); ?>/bootstrap-theme.css" rel="stylesheet" type="text/css">

<div class="container_inscription_pro">
    <?php $this->load->view("sortez/logo_global", $data); ?>

    <div style="text-align:center; display: table; width: 100%;">

        <h1 style="padding-top:50px; padding-bottom:20px;">Mot de passe oubli&eacute;</h1>

        <p style="padding:20px;"><a href="<?php echo site_url("connexion/"); ?>" class="btn btn-primary">Retour &agrave; la page connexion</a>
        </p>
        <p>Indiquer votre <?php echo $identity_label; ?> pour redefinir votre mot de passe.</p>

        <div id="infoMessage"><?php echo $message; ?></div>
        <div style="margin: 0 25%;">
            <?php echo form_open("auth/forgot_password"); ?>

            <p>
                <?php echo $identity_label; ?>: <br/>
                <?php echo form_input($email); ?>
            </p>

            <p><?php echo form_submit('submit', 'Envoyer le mot de passe', "class='btn btn-primary'"); ?></p>

            <?php echo form_close(); ?>
        </div>

    </div>

</div>

<?php //$this->load->view("frontAout2013/includes/footer_mini_2"); ?>
<?php $this->load->view("sortez/includes/footer_frontoffice_com", $data); ?>


