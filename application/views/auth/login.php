<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("frontAout2013/includes/header_mini_2", $data); ?>

    <div style='margin-top:30px; margin-bottom:20px; 
	color: #000000;
    font-family: "Vladimir Script",cursive; text-align:center;
    font-size: 50px;
    line-height: 47px;'>
    Identifiez vous !...
    </div>
    



<div id="infoMessage" style="text-align:center; color:#F00;"><?php echo $message;?></div>

<div style=" padding-left:175px;">
<table width="300" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <p>Identifiez vous avec votre email/nom d'utilisateur et votre mot de passe.</p>
    
    <?php echo form_open("auth/login");?>
    
      <p>
        <label for="identity">Email / Nom d'utilisateur:</label>
        <?php echo form_input($identity);?>
      </p>
    
      <p>
        <label for="password">Mot de passe:</label>
        <?php echo form_input($password);?>
      </p>
    
      <p>
        <label for="remember">Se souvenir de moi:</label>
        <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
      </p>
    
    
      <p><?php echo form_submit('submit', 'Connexion');?></p>
    
    <?php echo form_close();?>
    
    </td>
  </tr>
</table>
</div>


<div align="center" style="padding-top:50px;">
    <a onmouseover="PPImgAction('over','tg_4')" onmouseout="PPImgAction('out','tg_4')" href="<?php echo site_url("auth/forgot_password"); ?>"><img id="tg_4" title="" name="tg_4" alt="Oubli du mot de passe." src="<?php echo GetImagePath("front/"); ?>/passe.gif" onload="OnLoadPngFix()" border="0" width="205" height="35"></a>
    <a onmouseover="PPImgAction('over','tg_1')" onmouseout="PPImgAction('out','tg_1')" href="<?php echo site_url("front/particuliers/inscription"); ?>"><img id="tg_1" title="" name="tg_1" alt="Inscription" src="<?php echo GetImagePath("front/"); ?>/inscription.gif" onload="OnLoadPngFix()" border="0" width="205" height="35"></a>
    <a onmouseover="PPImgAction('over','tg_2')" onmouseout="PPImgAction('out','tg_2')" href="<?php echo base_url(); ?>"><img id="tg_2" title="" name="tg_2" alt="Retour." src="<?php echo GetImagePath("front/"); ?>/retour2.gif" onload="OnLoadPngFix()" border="0" width="205" height="35"></a>
    <div style="margin:20px;">
    <center>
    <p class="Corps-P"><span class="Normal-C3" style="font-size: 12px;">Vous 
    devez obligatoirement &ecirc;tre identifi&eacute; pour acc&eacute;der &agrave; votre espace personnel. <br/>
    Merci de saisir votre identifiant et votre mot de passe. </span></p>
    <p class="Corps-P"><span class="Normal-C3" style="font-size: 12px;">Si vous n'&ecirc;tes pas encore inscrit, 
    cliquez sur le bouton ci-dessus, l'adh&eacute;sion est 
    gratuite.</span></p>
    </center>
    </div>
</div>


<?php $this->load->view("frontAout2013/includes/footer_mini_2"); ?>