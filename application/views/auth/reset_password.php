<?php $data["zTitle"] = 'Accueil' ?>
<?php //$this->load->view("frontAout2013/includes/header_mini_2", $data); ?>
<?php $this->load->view("sortez/includes/header_frontoffice_com", $data); ?>

<!-- Bootstrap core CSS -->
<link href="<?php echo GetCssPath("bootstrap/"); ?>/bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php echo GetCssPath("bootstrap/"); ?>/bootstrap-theme.css" rel="stylesheet" type="text/css">


<div class="container_inscription_pro" style="text-align: center;">

    <?php $this->load->view("sortez/logo_global", $data); ?>

    <div style="display: table; width: 100%; margin: 40px 0;"><h1>Modification de mot de passe</h1></div>

    <div id="infoMessage"><?php echo $message; ?></div>

    <div style="margin: 0 20%;">
        <?php echo form_open('auth/reset_password/' . $code); ?>

        <p>
            Nouveau mot de passe (au moins <?php echo $min_password_length; ?> caract&eacute;res): <br/>
            <?php echo form_input($new_password); ?>
        </p>

        <p>
            Confirmation mot de passe : <br/>
            <?php echo form_input($new_password_confirm); ?>
        </p>

        <?php echo form_input($user_id); ?>
        <?php echo form_hidden($csrf); ?>

        <p><?php echo form_submit('submit', 'Enregistrer', "class='btn btn-primary'"); ?></p>

        <?php echo form_close(); ?>
    </div>

</div>

<?php //$this->load->view("frontAout2013/includes/footer_mini_2"); ?>
<?php $this->load->view("sortez/includes/footer_frontoffice_com", $data); ?>
