<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>
<script type="text/javascript">

$(function(){
     
    $("#submit_nouscontacter").click(function(){
            
            var txtError = "";
            
            //verify input content
            var zNom_nouscontacter = $("#zNom_nouscontacter").val();
            if(zNom_nouscontacter=="" || zNom_nouscontacter=="Votre nom *") {
                
                txtError += "- Veuillez indiquer Votre zNom_nouscontacter <br/>"; 
                //alert("Veuillez indiquer Votre nom");
                $("#zNom_nouscontacter").css('border-color', 'red');
            } else {
                $("#zNom_nouscontacter").css('border-color', '#E3E1E2');
            }
            
            var zPrenom_nouscontacter = $("#zPrenom_nouscontacter").val();
            if(zPrenom_nouscontacter=="" || zPrenom_nouscontacter=="Votre Prénom *") {
                
                txtError += "- Veuillez indiquer Votre zPrenom_nouscontacter <br/>"; 
                //alert("Veuillez indiquer Votre nom");
                $("#zPrenom_nouscontacter").css('border-color', 'red');
            } else {
                $("#zPrenom_nouscontacter").css('border-color', '#E3E1E2');
            }
            
            var zEmail_nouscontacter = $("#zEmail_nouscontacter").val();
            if(zEmail_nouscontacter=="" || zEmail_nouscontacter=="Votre courriel *" || !isEmail(zEmail_nouscontacter)) {
                
                txtError += "- Veuillez indiquer Votre zEmail_nouscontacter <br/>"; 
                //alert("Veuillez indiquer Votre nom");
                $("#zEmail_nouscontacter").css('border-color', 'red');
            } else {
                $("#zEmail_nouscontacter").css('border-color', '#E3E1E2');
            }
            
            var zTelephone_nouscontacter = $("#zTelephone_nouscontacter").val();
            if(zTelephone_nouscontacter=="" || zTelephone_nouscontacter=="Votre numéro de téléphone *") {
                
                txtError += "- Veuillez indiquer Votre zTelephone_nouscontacter <br/>"; 
                //alert("Veuillez indiquer Votre nom");
                $("#zTelephone_nouscontacter").css('border-color', 'red');
            } else {
                $("#zTelephone_nouscontacter").css('border-color', '#E3E1E2');
            }
            
            
            var zCommentaire_nouscontacter = $("#zCommentaire_nouscontacter").val();
            if(zCommentaire_nouscontacter=="") {
                
                txtError += "- Veuillez indiquer Votre zCommentaire_nouscontacter <br/>"; 
                //alert("Veuillez indiquer Votre nom");
                $("#zCommentaire_nouscontacter").css('border-color', 'red');
            } else {
                $("#zCommentaire_nouscontacter").css('border-color', '#E3E1E2');
            }
            
            
            //alert(txtError);
            if(txtError == "") {
                //alert('ok');
                $("#frmNousContacter_x").submit();
                $("#qsdf_x").click();
            }
            
            
            
        });
        
        
        
        $('#zNom_nouscontacter').focusin(function(){
            if($(this).val()=="Votre nom *") {
                $(this).val("");
            }
        });
        $('#zNom_nouscontacter').focusout(function(){
            if($(this).val()=="") {
                $(this).val("Votre nom *");
            }
        });
        $('#zPrenom_nouscontacter').focusin(function(){
            if($(this).val()=="Votre Prénom *") {
                $(this).val("");
            }
        });
        $('#zPrenom_nouscontacter').focusout(function(){
            if($(this).val()=="") {
                $(this).val("Votre Prénom *");
            }
        });
        $('#zEmail_nouscontacter').focusin(function(){
            if($(this).val()=="Votre courriel *") {
                $(this).val("");
            }
        });
        $('#zEmail_nouscontacter').focusout(function(){
            if($(this).val()=="") {
                $(this).val("Votre courriel *");
            }
        });
        $('#zTelephone_nouscontacter').focusin(function(){
            if($(this).val()=="Votre numéro de téléphone *") {
                $(this).val("");
            }
        });
        $('#zTelephone_nouscontacter').focusout(function(){
            if($(this).val()=="") {
                $(this).val("Votre numéro de téléphone *");
            }
        });
    
   
});

  
</script>

<div style="text-align:center;"><?php 
$thissss =& get_instance();
$thissss->load->library('session');
$message = $thissss->session->flashdata('message');
if (isset($message) && $message!="") echo $message;?></div>

<br/>
<form name="frmNousContacter" id="frmNousContacter_x" action="<?php echo site_url("front/annonce/envoiMailNousContacter"); ?>" method="post" enctype="multipart/form-data">

<table width="100%" border="0" cellspacing="5" cellpadding="3">
  <tr>
    <td colspan="2">
    Merci de bien vouloir préciser vos coordonnées et votre demande <br/> <span style="font-size:9px">* champs obligatoires</span>
    </td>
  </tr>
  <tr>
    <td>
    <input type="text" name="zNom" id="zNom_nouscontacter" value="Votre nom *" style="width:250px;"/>
    <input type="hidden" name="zMailTo" id="zMailTo_nouscontacter" value="<?php echo $oInfoCommercant->Email ; ?>"/>
    <input type="hidden" name="zIdCommercant" id="zIdCommercant" value="<?php echo $oInfoCommercant->IdCommercant ; ?>"/>
    </td>
    <td>
    <input type="text" name="zPrenom" id="zPrenom_nouscontacter" value="Votre Prénom *"  style="width:250px;"/>
    </td>
  </tr>
  <tr>
    <td><input type="text" name="zEmail" id="zEmail_nouscontacter" value="Votre courriel *" style="width:250px;"/></td>
    <td><input type="text" name="zTelephone" id="zTelephone_nouscontacter" style="width:250px;" value="Votre numéro de téléphone *"/></td>
  </tr>
  <tr>
    <td>Commentaires</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><textarea name="zCommentaire" id="zCommentaire_nouscontacter" cols="45" rows="5" style="width:515px;"></textarea></td>
  </tr>
  <tr>
    <td colspan="2">
    <input name="receive_mail" type="checkbox" value="" id="receivemail_nouscontacter">
    <label>J’accepte de recevoir par courriel des informations provenant de cet établissement.</label>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td> 
    <button id="reset_nouscontacter" name="submit" type="reset" class="Button1" style="width:100px;">
    Rétablir
    </button>
    <button id="submit_nouscontacter" name="submit" type="button" class="Button1" style="float:right;width:100px;">
    Valider
    </button>
    <input type="submit" name="qsdfq" id="qsdf_x" style="visibility:hidden;" />
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</form>

