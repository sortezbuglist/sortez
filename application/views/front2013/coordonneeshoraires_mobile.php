<?php $data["zTitle"] = 'Coordonnées & horaires';?>

<?php $this->load->view("front2013/includes/mobile_header_page_partner", $data); ?>

<div style="background-color:#FFFFFF;">
<div style="padding-bottom:15px; padding-top:15px;"><a class="Button1" href="javascript:history.go(-1);">Retour</a></div>
</div>
<div style="background-color:#FFFFFF; padding:10px;">
<div id="txt_605" style="overflow:hidden;">
<p class="Corps-P-P0">
<span class="Corps-C-C0">
<?php echo $oInfoCommercant->Adresse1 ; ?><?php if ($oInfoCommercant->Adresse2!="") echo " - ".$oInfoCommercant->Adresse2 ; ?>
<?php if ($oInfoCommercant->CodePostal!="") echo " - ".$oInfoCommercant->CodePostal ; ?><?php if ($oInfoCommercant->ville != "") echo " - ".$oInfoCommercant->ville ; ?>
</span></p>
<p class="Corps-P-P12"><span class="Corps-C-C3"><strong>Tél :</strong></span><span class="Corps-C-C0"> <?php echo '<a href="tel:'.supprime_espace($oInfoCommercant->TelFixe).'">'.$oInfoCommercant->TelFixe.'</a>' ; ?></span></p>
<p class="Corps-P-P12"><span class="Corps-C-C3"><strong>Directe :</strong></span><span class="Corps-C-C0"> <?php echo '<a href="tel:'.supprime_espace($oInfoCommercant->TelDirect).'">'.$oInfoCommercant->TelDirect.'</a>' ; ?></span></p>
<p class="Corps-P-P12"><span class="Corps-C-C3"><strong>Mobile :</strong></span><span class="Corps-C-C0"> <?php echo '<a href="tel:'.supprime_espace($oInfoCommercant->TelMobile).'">'.$oInfoCommercant->TelMobile.'</a>' ; ?></span></p>
<p class="Corps-P-P12"><span class="Corps-C-C3"><strong>Email :</strong></span><span class="Corps-C-C0"> <?php echo '<a href="mailto:'.$oInfoCommercant->Email.'">'.$oInfoCommercant->Email.'</a>' ; ?></span></p>
<p class="Corps-P-P2"><span class="Corps-C-C0"><?php echo $oInfoCommercant->Horaires ; ?> </span></p>
</div>
</div>

<?php $this->load->view("front2013/includes/mobile_footer_page_partner", $data); ?>