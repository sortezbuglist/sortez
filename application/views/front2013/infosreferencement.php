<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="fr"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Referencement</title>
<meta name="Generator" content="Serif WebPlus X6">
<meta name="viewport" content="width=650">
<meta name="viewport" content="width=device-width, maximum-scale=1.0">
 <!--Master Page Head-->
<style type="text/css">
body{margin:0;padding:0;}
.Corps-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:center; font-weight:400;
}
.Normal2-P
{
    margin:0.0px 0.0px 0.0px 24.0px; text-align:justify; font-weight:400; text-indent:-24.0px;
}
.Wp-Normal2-P {
	padding:0px; margin:0px;
	}
.Corps-P-P0
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:justify; font-weight:400;
}
.Corps-P-P1
{
    margin:0.0px 0.0px 12.0px 0.0px; text-align:justify; font-weight:400;
}
.Normal2-P-P0
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:justify; font-weight:400;
}
.Normal2-P-P1
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:justify; font-weight:400; text-indent:-24.0px;
}
.Normal2-P-P2
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:center; font-weight:400;
}
.Corps-P-P2
{
    margin:0.0px 0.0px 12.0px 0.0px; text-align:center; font-weight:400;
}
.Corps-C
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:19.0px; line-height:1.21em;
}
.Strong-C
{
    font-family:"Arial", sans-serif; font-weight:700; color:#3653a2; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C0
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
.Normal2-C
{
    font-family:"Arial", sans-serif; font-weight:700; color:#3653a2; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C1
{
    font-family:"Arial", sans-serif; font-weight:700; color:#3653a2; font-size:12.0px; line-height:1.25em;
}
.Normal2-C-C0
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C2
{
    font-family:"Verdana", sans-serif; font-size:12.0px; line-height:1.17em;
}
.Normal2-C-C1
{
    font-family:"Arial", sans-serif; font-size:13.0px; line-height:1.23em;
}
.Normal2-C-C2
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C3
{
    font-family:"Verdana", sans-serif; font-size:13.0px; line-height:1.23em;
}
.Normal2-C-C3
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:16.0px; line-height:1.25em;
}
.Normal2-C-C4
{
    font-family:"Arial", sans-serif; font-style:italic; font-size:13.0px; line-height:1.23em;
}
.Strong-C-C0
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C4
{
    font-family:"Verdana", sans-serif; font-size:16.0px; line-height:1.13em;
}
.Normal-C
{
    font-family:"Arial", sans-serif; color:#ffffff; font-size:16.0px; line-height:1.19em;
}
.Normal2-C-C5
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:21.0px; line-height:15.0px;
}
.Normal2-C-C6
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:13.0px; line-height:15.0px;
}
.Corps-C-C5
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:13.0px; line-height:1.23em;
}
.Normal-C-C0
{
    font-family:"Arial", sans-serif; font-size:11.0px; line-height:1.27em;
}
.Button1,.Button1:link,.Button1:visited{background-image:url('<?php echo GetImagePath("front/"); ?>/wpimages2013/wpab142e92_06.png');background-position:0px 0px;text-decoration:none;display:block;position:absolute;}
.Button1:focus{outline-style:none;}
.Button1:hover{background-position:0px -30px;}
.Button1 span,.Button1:link span,.Button1:visited span{color:#000000;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:none;font-style:normal;left:22px;top:7px;width:231px;height:15px;font-size:12px;display:block;position:absolute;cursor:pointer;}
.Button2,.Button2:link,.Button2:visited{background-image:url('<?php echo GetImagePath("front/"); ?>/wpimages2013/wp90dfea65_06.png');background-position:0px 0px;text-decoration:none;display:block;position:absolute;}
.Button2:focus{outline-style:none;}
.Button2:hover{background-position:0px -30px;}
.Button2 span,.Button2:link span,.Button2:visited span{color:#000000;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:none;font-style:normal;left:22px;top:7px;width:71px;height:15px;font-size:12px;display:block;position:absolute;cursor:pointer;}
.Button3,.Button3:link,.Button3:visited{background-image:url('<?php echo GetImagePath("front/"); ?>/wpimages2013/wp43dff9af_06.png');background-position:0px 0px;text-decoration:none;display:block;position:absolute;}
.Button3:focus{outline-style:none;}
.Button3:hover{background-position:0px -30px;}
.Button3 span,.Button3:link span,.Button3:visited span{color:#000000;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:none;font-style:normal;left:22px;top:7px;width:324px;height:15px;font-size:12px;display:block;position:absolute;cursor:pointer;}
</style>
<script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/wpimages2013/jspngfix.js"></script>
<link rel="stylesheet" href="referencement_fichiers/wpstyles.css" type="text/css"><script type="text/javascript">
var blankSrc = "wpscripts/blank.gif";
</script>
</head>

<body style="background:transparent url('<?php echo GetImagePath("front/"); ?>/wpimages2013/wp9e1de8e1_05_06.jpg') repeat scroll top center; height:4500px;" text="#000000">
<div style="background-color:#ffffff;margin-left:auto;margin-right:auto;position:relative;width:650px;height:4500px;">
<map id="map0" name="map0">
    <area shape="rect" coords="481,36,651,59" href="<?php echo site_url("front/utilisateur/sessionp/");?>" alt="">
    <area shape="rect" coords="479,8,651,31" href="<?php echo site_url("front/utilisateur/session/");?>" alt="">
    <area shape="poly" coords="624,93,588,93,606,105" href="<?php echo site_url("front/contact");?>" alt="">
    <area shape="poly" coords="630,131,634,125,634,81,629,76,583,76,578,84,578,127,583,132,629,132" href="<?php echo site_url("front/contact");?>" alt="">
    <area shape="poly" coords="559,131,563,125,563,81,558,76,512,76,507,84,507,127,512,132,558,132" href="<?php echo site_url();?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpd6b093a9_06.png" id="art_2017" title="" alt="Les  bonnes  adresses  &amp;  bons  plans  :  Sorties  -  Loisirs  -  Shopping  -  Restos
" onload="OnLoadPngFix()" usemap="#map0" style="position:absolute;left:0px;top:0px;" border="0" height="178" width="650">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp485ee752_06.png" id="pic_3009" alt="" onload="OnLoadPngFix()" style="position:absolute;left:335px;top:2461px;" border="0" height="145" width="306">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp0e34e674_06.png" id="pic_3011" alt="" onload="OnLoadPngFix()" style="position:absolute;left:185px;top:2461px;" border="0" height="140" width="140">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpab609c77_06.png" id="qs_3387" alt="" onload="OnLoadPngFix()" style="position:absolute;left:333px;top:2426px;" border="0" height="25" width="302">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpab609c77_06.png" id="qs_3388" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:2427px;" border="0" height="25" width="302">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpab609c77_06.png" id="qs_3386" alt="" onload="OnLoadPngFix()" style="position:absolute;left:333px;top:2653px;" border="0" height="25" width="302">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpab609c77_06.png" id="qs_3385" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:2654px;" border="0" height="25" width="302">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpc690a166_06.png" id="qs_3383" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:1382px;" border="0" height="325" width="620">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpd87f5624_06.png" id="grp_3289" alt="" onload="OnLoadPngFix()" style="position:absolute;left:141px;top:2363px;" border="0" height="40" width="379">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpe5660ebf_06.png" id="qs_3121" alt="" onload="OnLoadPngFix()" style="position:absolute;left:17px;top:633px;" border="0" height="124" width="619">
<div id="txt_1249" style="position:absolute;left:218px;top:646px;width:403px;height:97px;overflow:hidden;">
<p class="Corps-P"><span class="Corps-C">En souscrivant à cette option,</span></p>
<p class="Corps-P"><span class="Corps-C">les bénéficiaires de l’abonnement “Platinium” bénéficient de la création</span></p>
<p class="Corps-P"><span class="Corps-C">d’un site web individuel et personnalisé.</span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp6469c1b1_06.png" id="grp_3592" alt="" onload="OnLoadPngFix()" style="position:absolute;left:14px;top:603px;" border="0" height="183" width="189">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpab35a00e_06.png" id="pic_1456" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:799px;" border="0" height="48" width="45">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpab35a00e_06.png" id="pic_1457" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:934px;" border="0" height="48" width="45">
<div id="txt_803" style="position:absolute;left:85px;top:935px;width:550px;height:54px;overflow:hidden;">
<p class="Normal2-P"><span class="Strong-C">Actualisation de vos données:</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C0">Toutes les créations, modifications ou suppressions de données effectuées par les
    adhérents</span></p>
<p class="Corps-P-P1"><span class="Corps-C-C0">sur les pages administratives du Club Proximité actualisent leurs sites en temps
    réel.</span></p>
</div>
<div id="txt_1248" style="position:absolute;left:85px;top:799px;width:550px;height:72px;overflow:hidden;">
<p class="Normal2-P-P0"><span class="Normal2-C">Un nom de domaine et des adresses mails personnalisés :</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C0">Le club proximité intègre automatiquement les pages club de chaque adhérent de l’abonnement
    “Platinium” au sein d’un site dont l’URL et son hébergement sont personnalisés. (ex
    : www.mon-<wbr>entreprise.fr et contact@mon-<wbr>entreprise.fr)</span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp91cd98fd_05_06.jpg" id="pic_2651" alt="" style="position:absolute;left:535px;top:982px;" border="0" height="75" width="115">
<div id="txt_1252" style="position:absolute;left:84px;top:995px;width:436px;height:69px;overflow:hidden;">
<p class="Corps-P-P0"><span class="Corps-C-C1">La mobilité :</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C0">Une variante optimisé de votre site pour les mobiles est disponible. &nbsp;&nbsp;Il s’adapte
    à tous les écrans des smartphones du marché. Fonctions interactives : téléphone,
    localisation, email.</span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpab35a00e_06.png" id="pic_3004" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:996px;" border="0" height="48" width="45">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpab35a00e_06.png" id="pic_1459" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:1075px;" border="0" height="48" width="45">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpab35a00e_06.png" id="pic_1460" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:1134px;" border="0" height="48" width="45">
<div id="txt_805" style="position:absolute;left:85px;top:1137px;width:550px;height:164px;overflow:hidden;">
<p class="Normal2-P-P0"><span class="Normal2-C">Renouvellement annuel : </span></p>
<p class="Normal2-P-P0"><span class="Normal2-C-C0">Un nom de domaine est enregistré pour une période annuelle. Durant le mois précédent
    la date d’anniversaire du domaine, il est fondamental de procéder au renouvellement
    de ce domaine si vous souhaitez en conserver l’utilisation. Le Club Proximité prendra
    soin de vous relancer à plusieurs reprises durant ce dernier mois mais il vous appartient
    d’être proactif, de noter cette date.</span></p>
<p class="Normal2-P-P0"><span class="Normal2-C-C0">Une fois la date d’anniversaire dépassée. Le domaine reste la propriété du site et
    redevient disponible pour tout partenaire de notre site souhaitant s’en porter acquéreur
    et le partenaire précédent perd tous ses droits sur ce nom de domaine.</span></p>
<p class="Corps-P-P1"><span class="Corps-C-C2"><br></span></p>
</div>
<div id="txt_1390" style="position:absolute;left:84px;top:1075px;width:551px;height:58px;overflow:hidden;">
<p class="Normal2-P"><span class="Strong-C">Propriété du nom de domaine :</span></p>
<p class="Corps-P-P1"><span class="Corps-C-C0">Le Club Proximité, dépose et administre votre nom de domaine en son nom, il en reste
    le propriétaire sauf indications contraire.</span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpab35a00e_06.png" id="pic_1458" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:1276px;" border="0" height="48" width="45">
<div id="txt_1391" style="position:absolute;left:85px;top:1271px;width:550px;height:100px;overflow:hidden;">
<p class="Normal2-P"><span class="Normal2-C">Transfert de propriété du nom de domaine :</span></p>
<p class="Normal2-P"><span class="Normal2-C-C1">le nom de domaine que vous avez associé à votre site est lié à celui-<wbr>ci.</span></p>
<p class="Normal2-P-P1"><span class="Normal2-C-C1">Son transfert chez un autre prestataire entraîne la fermeture et la mise hors ligne
    automatique de votre site. </span><span class="Normal2-C-C2">Ce transfert est payant &nbsp;et sera facturé 200€ HT. </span></p>
<p class="Wp-Corps-P"><span class="Corps-C-C3"><br></span></p>
</div>
<div id="txt_1389" style="position:absolute;left:216px;top:1409px;width:403px;height:81px;overflow:hidden;">
<p class="Corps-P"><span class="Corps-C">Un protocole de réferéncement est engagé par notre service technique</span></p>
<p class="Corps-P"><span class="Corps-C">et comprend :</span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpfca11f8c_06.png" id="pic_1410" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:1362px;" border="0" height="163" width="180">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpc0e796e5_06.png" id="pic_1446" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:1828px;" border="0" height="48" width="44">
<div id="txt_790" style="position:absolute;left:84px;top:1828px;width:436px;height:73px;overflow:hidden;">
<p class="Wp-Normal2-P"><span class="Strong-C">La création d’un fichier «Site Map»:</span></p>
<p class="Normal2-P-P0"><span class="Normal2-C-C1">Un fichier «&nbsp;Sitemaps&nbsp;» permet d'indiquer facilement aux moteurs de recherche les
    pages de leurs sites à explorer. L’objectif est de favoriser une exploration plus
    intelligente du site par les moteurs de recherche.</span><span class="Normal2-C-C3">L</span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp9e87100c_06.png" id="pic_1433" alt="" onload="OnLoadPngFix()" style="position:absolute;left:548px;top:1828px;" border="0" height="85" width="70">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpc0e796e5_06.png" id="pic_3002" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:1906px;" border="0" height="48" width="44">
<div id="txt_791" style="position:absolute;left:84px;top:1892px;width:436px;height:100px;overflow:hidden;padding:16px 0px 0px;">
<p class="Wp-Normal2-P"><span class="Strong-C">La création d’un fichier «Robots.txt»:</span></p>
<p class="Normal2-P-P0"><span class="Normal2-C-C1">Le fichier robots.txt est un fichier texte contenant des commandes à destination
    des robots d'indexation des moteurs de recherche afin de leur préciser les pages
    qui peuvent ou ne peuvent pas être indexées. Ainsi tout moteur de recherche commence
    l'exploration d'un site web en cherchant le fichier </span><span class="Normal2-C-C4">robots.txt</span><span class="Normal2-C-C1"> à la racine du site.
    </span></p>
<p class="Wp-Normal2-P"><span class="Normal2-C-C3"><br></span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpb4bd54a2_06.png" id="pic_1434" alt="" onload="OnLoadPngFix()" style="position:absolute;left:534px;top:1923px;" border="0" height="75" width="101">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpab35a00e_06.png" id="pic_1447" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:2022px;" border="0" height="48" width="45">
<div id="txt_792" style="position:absolute;left:83px;top:2022px;width:437px;height:71px;overflow:hidden;">
<p class="Normal2-P-P0"><span class="Strong-C">L’optimisation des mots clés.</span></p>
<p class="Normal2-P-P0"><span class="Normal2-C-C1">Les métadonnées sont des informations situées au sein d'un document afin de le décrire.
    Les métadonnées sont ainsi utilisées par les moteurs de recherche lors du référencement
    de la page web. </span></p>
<p class="Normal2-P-P0"><span class="Normal2-C-C1"><br></span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpb9b8d77f_06.png" id="pic_1435" alt="" onload="OnLoadPngFix()" style="position:absolute;left:542px;top:2028px;" border="0" height="70" width="78">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpab35a00e_06.png" id="pic_1448" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:2104px;" border="0" height="48" width="45">
<div id="txt_793" style="position:absolute;left:83px;top:2118px;width:438px;height:20px;overflow:hidden;">
<p class="Wp-Normal2-P"><span class="Strong-C">Le référencement &nbsp;avec des mots clés des vos images</span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpab35a00e_06.png" id="pic_1449" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:2162px;" border="0" height="48" width="45">
<div id="txt_819" style="position:absolute;left:84px;top:2165px;width:436px;height:36px;overflow:hidden;">
<p class="Wp-Normal2-P"><span class="Strong-C">La présence d’un lien URL permanent pour consulter les statistiques de votre site</span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpab35a00e_06.png" id="pic_1450" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:2230px;" border="0" height="48" width="45">
<div id="txt_789" style="position:absolute;left:84px;top:2231px;width:436px;height:55px;overflow:hidden;">
<p class="Wp-Normal2-P"><span class="Strong-C">Le référencement sur GOOGLE MAPS :</span></p>
<p class="Normal2-P-P0"><span class="Strong-C-C0">Nos services optimise votre référencement sur Google Maps cela permet d'améliorer
    significativement votre visibilité sur le moteur de recherche Google </span></p>
<p class="Wp-Corps-P"><span class="Corps-C-C4"><br></span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpc06abcc7_06.png" id="pcrv_16915" alt="" onload="OnLoadPngFix()" style="position:absolute;left:523px;top:2235px;" border="0" height="90" width="112">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp5d642622_06.png" id="pic_2889" alt="" onload="OnLoadPngFix()" style="position:absolute;left:544px;top:2135px;" border="0" height="78" width="78">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpab35a00e_06.png" id="pic_1467" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:2300px;" border="0" height="48" width="45">
<div id="txt_809" style="position:absolute;left:84px;top:2303px;width:436px;height:45px;overflow:hidden;">
<p class="Wp-Normal2-P"><span class="Strong-C">Le référencement optimisé de votre site sur le portail du Club Proximité</span></p>
</div>
<div id="art_2056" style="position:absolute;left:84px;top:2430px;width:142px;height:19px;white-space:nowrap;">
    <div class="Wp-Normal-P">
        <span class="Normal-C">www.qualiroulettes.fr</span></div>
</div>
<div id="art_2057" style="position:absolute;left:432px;top:2430px;width:119px;height:19px;white-space:nowrap;">
    <div class="Wp-Normal-P">
        <span class="Normal-C">www.valdetrolls.fr</span></div>
</div>
<div id="art_2058" style="position:absolute;left:71px;top:2654px;width:184px;height:19px;white-space:nowrap;">
    <div class="Wp-Normal-P">
        <span class="Normal-C">www.jeanlouis-<wbr>lepecheur.fr</span></div>
</div>
<div id="art_2059" style="position:absolute;left:394px;top:2654px;width:180px;height:19px;white-space:nowrap;">
    <div class="Wp-Normal-P">
        <span class="Normal-C">www.etoiledor-<wbr>bijouterie.fr</span></div>
</div>
<map id="map1" name="map1">
    <area shape="rect" coords="255,248,365,278" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="339,228,339,226,341,225,342,223,344,219,342,220,341,222,337,226,335,227,336,225,336,224,339,221,340,219,337,219,336,221,334,222,333,224,329,226,328,227,326,228,326,227,329,224,330,222,332,220,331,220,329,219,327,221,326,223,324,224,325,225,326,224,325,226,325,229,327,229,331,227,332,225,334,224,335,223,333,225,332,227,334,229,337,227,338,226,338,227,337,229" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="288,226,290,224,292,220,290,220,290,222,287,224,285,225,284,227,283,228,285,227,286,226,286,228" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="323,226,324,225,323,225,322,226,321,227,320,227,319,228,322,228" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="277,228,281,226,283,228,282,226,286,223,290,220,289,219,285,220,283,223,280,225,276,228,275,226,278,222,282,219,288,213,290,209,288,209,282,212,280,217,276,221,272,224,268,227,265,228,266,229,270,227,274,224,275,224,274,229" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="326,224,326,223,327,223" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="358,228,356,228,357,226,359,225,360,223,362,221,363,219,359,219,358,221,356,220,356,219,354,220,352,222,351,222,352,220,350,219,349,221,347,222,343,226,342,227,339,229,341,228,344,227,347,224,346,226,344,228,347,228,348,227,349,225,351,224,352,222,353,221,355,220,355,222,350,227,351,229,352,227,356,223,358,222,360,220,360,222,358,223,357,225,355,226,354,229,358,229" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="319,228,317,227,320,224,323,221,323,219,319,220,316,222,318,219,316,218,314,221,311,224,308,226,304,228,306,225,308,222,310,219,306,221,303,224,299,226,295,228,297,225,299,222,301,219,305,217,303,217,306,214,305,211,303,213,301,217,295,217,293,218,300,218,296,222,293,224,290,227,287,229,286,228,287,229,291,227,294,225,295,224,294,227,297,229,299,228,301,226,303,225,302,227,305,229,309,227,310,225,313,223,311,226,310,228,314,226,318,222,321,220,319,223,317,224,315,227,317,229" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="334,216,336,215,336,213,334,213,334,214,332,214,332,217" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="313,215,314,214,313,213,312,213,309,216,312,216" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="279,220,280,218,286,212,287,210,289,210,288,212,286,215,283,217,281,218" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="255,227,259,222,262,219,267,219,273,217,277,213,278,210,274,214,270,216,264,217,266,213,269,211,272,207,278,208,277,205,266,205,261,207,258,211,263,209,268,207,270,207,266,212,263,214,258,218,260,219,256,224,253,229" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="320,199,322,198,325,196,329,192,328,194,326,196,325,198,328,198,329,196,331,194,333,192,334,192,333,194,332,195,331,197,329,199,332,199,334,195,336,193,338,192,340,190,339,192,338,193,337,195,336,196,334,199,340,199,343,196,340,197,337,198,339,194,341,193,342,190,340,189,338,191,336,189,334,190,332,191,333,189,330,190,326,194,324,195,321,198,317,198,318,196,319,195,321,194,322,193,324,192,325,191,325,188,319,190,318,193,315,196,312,197,311,198,308,198,311,195,313,193,314,191,314,189,312,190,309,191,308,188,306,190,305,192,303,193,300,196,296,198,295,197,296,195,298,193,300,191,300,189,298,190,295,191,293,192,295,191,296,189,293,188,292,191,290,192,288,194,286,196,283,197,281,198,282,199,285,198,287,196,290,193,291,193,289,195,288,196,287,198,290,198,291,195,294,194,298,190,297,192,292,197,294,199,297,198,300,198,302,195,304,194,302,196,301,198,303,198,305,196,307,194,309,192,311,191,312,190,312,192,310,193,309,194,308,196,305,199,310,199,314,197,316,196,316,200" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="279,198,281,197,282,195,284,194,285,191,283,189,284,191,283,194,281,193,282,195,280,197,278,198,277,195,279,194,281,193,282,190,279,191,274,196,271,197,269,198,270,199,271,198,275,196,276,195,276,198,277,200" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="370,198,372,194,375,191,377,187,378,188,380,187,378,186,380,184,381,182,378,183,377,185,375,187,369,187,367,188,374,188,371,191,367,195,365,196,361,198,363,195,366,192,365,189,361,191,359,192,362,189,358,191,355,193,353,196,350,197,346,198,345,196,348,195,351,193,352,192,354,188,350,189,347,190,345,193,343,196,344,199,348,199,352,198,354,194,357,192,355,196,353,198,357,196,361,192,365,190,360,195,359,198,362,199,366,197,368,195,370,194,369,198,372,199" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="267,198,271,195,273,189,271,189,274,187,280,181,281,178,276,179,273,183,270,185,268,188,265,192,261,195,258,198,256,198,257,199,261,196,264,194,263,196,263,200" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="370,194,370,193,371,193" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="265,198,265,196,266,194,267,193,269,195,267,197,266,197" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="305,193,304,193,305,192" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="347,194,347,193,350,190,351,190,351,192,348,193" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="335,192,334,192,334,191,336,190,335,191" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="318,195,319,193,322,190,323,190,323,192,319,194" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="270,194,269,194,268,193,268,192,269,190,270,190,271,191,270,193" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="253,197,257,193,259,190,263,190,259,189,263,184,268,179,271,174,265,176,253,184,247,189,241,189,237,192,243,193,238,197,237,199,242,195,247,191,248,190,257,190,253,195,251,198,252,199" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="244,192,243,191,245,190,246,190,244,191" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="269,190,270,189,271,189" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="271,188,271,187,272,187" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="272,187,273,185,278,180,279,181,276,183,274,185,274,186" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="257,189,250,189,253,186,255,184,258,183,260,181,262,179,263,178,266,177,267,176,267,178,265,179,264,181,262,182,261,185,259,186,258,188" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="rect" coords="356,228,358,230" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="rect" coords="339,228,341,230" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="347,227,352,222,351,221,352,219,350,220,347,223,345,224,342,227,341,228,343,227,345,226,347,224,346,226,345,228" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="358,221,357,221,357,222" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="351,228,352,226,354,225,355,223,357,222,356,220,353,220,352,222,353,221,355,222,353,223,352,225,351,227,350,229" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="rect" coords="358,220,360,222" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="356,227,358,226,359,224,361,222,362,220,360,219,359,221,358,223,357,225,356,227,355,228" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="rect" coords="326,228,328,230" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="340,228,339,226,341,225,342,223,343,221,344,220,342,221,340,222,339,224,337,226,335,227,336,225,337,223,338,221,339,220,337,221,335,222,333,224,331,225,332,226,334,224,333,226,333,228,336,228,337,226,338,228,340,229" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="329,227,330,227,331,226,330,226,329,227,328,227,328,228,329,228" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="rect" coords="318,228,320,230" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="321,227,322,227,324,225,323,226,322,226,320,228" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="327,228,326,227,328,225,329,223,331,221,330,219,329,221,327,222,325,223,324,225,326,224,327,222,328,222,326,224,325,226,325,229" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="335,215,335,213,334,214,333,214,332,215,332,216,335,216" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="rect" coords="304,228,306,230" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="312,227,315,224,317,223,319,221,317,222,316,223,317,221,318,219,316,220,314,221,310,225,308,226,306,227,307,228,308,227,311,225,312,224,311,226,310,228" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="318,228,317,227,322,222,322,220,321,221,319,223,317,225,316,226,316,228,317,229" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="rect" coords="319,220,321,222" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="rect" coords="296,228,298,230" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="299,227,300,227,302,225,301,226,300,226,298,228" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="305,228,304,227,306,225,307,223,309,221,308,219,307,221,305,222,303,223,302,225,304,224,305,222,306,222,304,224,303,226,303,229" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="313,215,313,213,312,214,311,214,310,215,310,216,313,216" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="rect" coords="295,228,297,230" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="rect" coords="287,228,289,230" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="290,227,291,227,292,226,297,222,296,222,293,225,292,225,290,227,289,228" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="295,227,297,225,298,223,299,222,302,217,304,215,306,213,305,212,303,214,301,217,294,217,294,218,300,218,298,220,297,222,295,225,294,228" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="rect" coords="275,228,277,230" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="288,228,287,226,289,225,290,223,291,221,292,220,290,221,289,223,288,224,286,225,284,227,282,227,283,225,285,224,286,222,288,221,290,220,289,219,288,220,286,221,284,222,282,224,280,225,278,226,277,228,279,227,281,225,281,228,284,228,286,226,286,229" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="rect" coords="266,228,268,230" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="276,228,275,226,278,222,282,219,286,216,288,213,289,210,287,213,283,216,280,220,279,220,281,217,284,215,287,211,288,209,284,212,281,216,278,218,275,222,271,225,268,228,271,226,275,224,274,229" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="255,228,259,223,262,219,267,218,273,216,278,212,279,208,277,207,278,210,273,214,268,217,264,216,268,211,272,207,277,206,275,205,269,205,264,207,258,209,258,211,263,209,268,207,270,208,265,212,262,217,257,217,259,220,255,225,253,228,254,229" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="rect" coords="370,198,372,200" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="365,197,363,197,363,198,364,198" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="366,196,369,193,368,194,366,196,365,197" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="370,193,369,193" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="371,192,370,192,370,193" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="370,197,371,195,373,193,373,192,377,187,379,185,380,182,375,187,368,187,368,188,374,188,373,190,371,192,370,195,369,198" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="351,197,349,197,349,198,350,198" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="352,196,351,196,351,197" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="356,193,355,193,354,194" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="361,192,360,192,360,193" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="362,198,361,196,363,195,364,193,366,192,366,190,362,190,361,192,362,191,364,192,362,193,361,195,360,197,359,198,360,199" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="355,197,357,196,358,194,360,193,359,193,360,191,361,189,359,190,357,191,356,193,359,191,355,195,354,197,355,198" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="348,198,345,198,345,199,347,199" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="341,197,342,197,343,196,341,196,339,198,340,198" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="345,196,346,194,350,190,348,191,346,192,345,194,343,195,344,197,345,198" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="348,194,350,194,351,193,352,192,353,191,353,190,352,189,350,189,351,190,351,191,348,194,347,195" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="323,197,321,197,321,198,322,198" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="324,196,325,195,328,193,327,193,324,196,323,197" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="331,193,331,192,332,192" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="331,198,333,197,334,195,336,193,338,192,339,190,337,191,337,190,334,190,332,192,333,190,331,189,330,191,328,194,327,196,326,198,328,197,329,195,331,194,332,192,333,191,335,192,332,195,331,197,330,198,331,199" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="330,191,329,191,328,192,328,193" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="338,198,337,197,338,196,340,194,341,193,342,192,342,189,340,189,340,192,339,193,338,194,337,195,336,196,336,198,337,199" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="320,198,317,198,317,199,319,199" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="313,197,314,197,315,196,313,196,311,198,312,198" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="317,196,318,194,322,190,320,191,318,192,317,194,315,195,316,197,317,198" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="320,194,322,194,323,193,324,192,325,191,325,190,324,189,322,189,323,190,323,191,320,194,319,195" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="299,197,297,197,297,198,298,198" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="300,196,299,196,299,197" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="304,193,303,193,302,194" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="309,192,308,192,308,193" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="310,198,309,196,311,195,312,193,314,192,314,190,310,190,309,192,310,191,312,192,310,193,309,195,308,197,307,198,308,199" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="303,197,305,196,306,194,308,193,307,193,308,191,309,189,307,190,305,191,304,193,307,191,303,195,302,197,303,198" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="285,197,283,197,283,198,284,198" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="286,196,285,196,285,197" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="290,193,289,193,288,194" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="295,192,294,192,294,193" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="296,198,295,196,297,195,298,193,300,192,300,190,296,190,295,192,296,191,298,192,296,193,295,195,294,197,293,198,294,199" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="289,197,291,196,292,194,294,193,293,193,294,191,295,189,293,190,291,191,290,193,293,191,289,195,288,197,289,198" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="271,198,272,198,272,197,271,197,271,198,270,198,270,199,271,199" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="274,196,275,195,276,195,277,194,276,194,275,195,274,196,273,197" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="279,198,280,197,282,196,283,194,284,193,282,194,281,193,282,195,280,197,278,198,277,196,279,195,280,193,282,192,283,190,280,190,279,192,277,193,276,195,276,199,279,199" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="285,191,285,190,284,189,283,189,283,190,284,190,284,192,285,192" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="258,198,259,198,260,197,258,197,258,198,257,198,257,199,258,199" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="267,198,271,195,272,190,269,194,268,192,271,189,270,189,273,185,276,181,279,180,276,184,273,187,277,184,280,180,278,178,274,181,271,185,266,190,263,194,260,197,264,194,263,196,266,199" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="266,198,265,197,265,195,266,194,266,193,267,192,268,193,269,194,268,195,267,196,267,197" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="253,198,256,193,259,190,263,190,260,189,264,184,268,179,270,175,267,174,262,177,257,181,252,185,248,189,241,189,237,192,243,191,239,191,241,190,246,191,241,195,238,198,243,194,247,190,257,190,253,195,251,198,252,199" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="257,189,250,189,253,186,255,184,257,183,259,181,261,180,263,178,266,177,268,176,267,177,265,180,263,182,261,185" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="407,262,395,241,214,241,214,285,395,285" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="poly" coords="372,315,381,310,386,301,387,296,387,182,383,173,376,166,371,164,248,164,239,169,234,178,233,184,233,300,238,309,246,315,248,316,371,316" href="<?php echo site_url("front/utilisateur/infosplatinium/");?>" alt="">
    <area shape="rect" coords="50,249,160,278" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="87,226,87,225,91,223,93,222,95,220,93,218,90,219,90,220,88,221,87,222,86,224,85,225,82,226,80,227,79,228,81,228,83,226,85,227,87,228" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="108,228,110,227,113,225,115,223,113,223,112,225,109,226,109,227,106,228,107,226,109,224,112,221,111,218,109,222,105,226,104,229" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="rect" coords="115,222,117,224" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="102,227,103,225,105,224,106,222,108,221,108,220,110,219,108,219,107,220,106,219,103,219,102,221,101,221,102,219,100,220,98,221,96,223,94,225,92,226,90,227,87,228,92,228,95,225,98,223,97,225,95,227,97,228,98,226,100,224,102,222,104,221,105,220,105,221,102,224,101,226,99,228,101,228" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="79,227,81,225,82,223,84,221,84,219,82,218,80,219,79,221,77,223,75,225,74,226,72,227,71,227,69,228,72,228,78,224,80,222,82,220,83,220,81,221,80,223,78,225,78,228" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="130,228,131,227,133,226,138,221,136,224,135,226,134,228,137,227,138,224,140,223,142,221,144,220,142,224,140,225,139,227,138,228,141,227,144,224,148,220,149,221,145,225,145,226,143,228,147,228,145,227,148,224,150,222,151,221,152,219,148,219,146,221,145,221,146,220,143,219,141,221,140,221,141,218,139,220,137,221,135,223,133,224,131,226,129,227,128,228,128,226,129,224,132,221,133,220,131,219,130,222,128,223,124,227,125,224,128,221,129,219,126,218,125,220,122,223,121,225,119,226,115,228,118,227,120,226,123,223,125,222,123,224,122,227,124,228,126,226,127,229" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="115,227,115,226,118,223,119,221,120,221,121,219,118,219,116,221,116,223,114,225,112,228,115,228" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="89,224,88,223,89,222,91,219,93,219,93,220,91,222" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="123,215,124,215,125,214,125,213,122,213,121,214,121,215,122,216" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="59,227,63,222,66,218,71,218,77,216,82,212,83,210,82,209,78,214,73,216,68,217,70,213,73,210,76,206,82,208,82,205,70,205,64,207,62,210,68,208,74,207,70,210,67,215,63,217,62,216,61,217,64,218,60,223,56,228" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="83,208,82,208,82,209" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="115,197,114,195,117,195,123,189,120,189,118,188,117,190,115,191,113,193,112,195,109,196,107,197,106,196,111,191,111,189,108,189,106,190,104,192,105,190,106,188,104,188,103,190,101,191,99,193,97,195,93,197,91,197,97,191,97,189,93,189,90,192,91,190,92,188,90,188,89,190,87,191,86,193,87,193,85,195,85,196,84,198,86,197,90,193,92,192,94,190,95,190,94,192,92,193,91,195,90,196,89,198,94,198,95,197,97,196,99,194,101,193,99,195,99,196,97,198,100,198,101,196,104,193,105,192,107,191,108,190,108,191,104,195,103,198,108,198,112,196,113,198,117,198" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="143,197,142,195,143,195,147,193,149,191,150,188,144,190,141,192,141,194,140,195,138,196,136,197,135,197,132,198,136,198,138,197,140,195,140,198,146,198" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="75,198,74,196,74,195,77,192,78,194,79,194,77,195,77,196,75,198,78,198,78,197,79,194,81,193,81,192,82,190,81,189,77,189,76,191,74,192,72,193,71,195,70,196,68,197,66,198,68,198,69,197,71,196,73,194,72,196,72,197,73,199" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="167,198,168,197,166,197,169,194,170,191,174,187,177,187,179,186,175,185,176,183,178,181,175,183,172,186,167,186,166,187,171,187,169,190,166,192,163,194,160,197,157,198,159,194,162,191,163,188,159,190,156,192,158,189,157,188,154,191,150,193,149,195,146,198,147,198,150,194,153,192,151,196,150,198,153,195,156,193,160,191,156,195,154,199,159,198,162,197,166,193,167,193,165,197,166,199" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="82,197,85,195,86,193,84,193,83,194,83,196,80,197,78,197,79,198" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="64,198,67,196,70,192,66,189,70,186,73,184,76,180,78,178,74,179,71,181,69,182,66,185,64,189,60,193,56,196,53,198,57,197,61,193,60,196,60,199" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="125,193,125,192,126,192" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="87,193,87,192,88,192" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="63,197,61,197,61,195,64,192,64,193,65,194,66,194,64,196" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="129,197,131,195,133,193,135,191,137,190,136,191,135,193,133,195,131,198,132,198,138,192,139,190,139,188,137,189,135,190,134,191,133,189,130,190,129,191,130,189,128,188,126,190,122,194,120,196,117,198,120,198,122,195,125,193,123,195,122,198,124,197,127,194,131,190,132,191,130,193,129,195,127,197,126,197,128,199" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="79,193,78,192,78,191,79,190,81,190,81,191" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="66,194,65,192,65,191,66,190,67,190,67,192,66,193" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="143,194,142,194,144,193,144,192,146,190,147,189,148,189,148,191,145,193" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="115,194,114,194,115,193,118,190,119,189,120,189,120,191,117,193" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="51,197,53,192,56,189,59,189,57,188,61,183,65,178,67,174,62,175,50,183,45,187,43,189,34,189,32,191,37,192,38,191,34,191,36,190,43,190,38,194,34,198,39,194,44,190,45,189,50,190,53,189,50,194,47,197,49,198" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="68,187,69,186,70,184,73,181,75,180,76,179,76,180,74,182,72,183,71,185,69,186" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="54,188,47,188,49,186,51,184,53,183,56,180,59,179,61,177,63,176,64,175,63,177,61,179,60,181,57,184,56,186" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="rect" coords="128,227,131,229" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="140,222,140,221,141,221" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="136,227,140,223,141,221,143,220,141,221,141,219,139,220,137,221,135,223,133,225,131,226,132,227,133,225,134,225,139,221,136,224,135,226,134,228" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="146,227,147,225,148,223,150,222,151,220,151,219,148,219,146,220,145,221,145,219,143,220,144,221,142,223,141,225,139,226,140,228,141,226,143,225,144,223,146,222,147,220,149,221,147,222,146,224,145,226,144,227,146,228" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="rect" coords="115,227,118,229" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="126,226,125,226,125,227" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="119,226,120,225,121,225,124,222,123,223,122,223,119,226,118,227" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="129,227,128,226,129,224,131,223,132,221,133,219,131,220,128,223,127,225,126,226,127,228" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="124,227,124,225,125,224,126,223,127,222,128,221,128,219,127,219,126,220,125,221,123,224,122,225,122,227,123,228" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="109,227,111,227,112,226,113,225,113,224,112,225,111,226,110,226,109,227,107,227,107,228,109,228" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="115,223,114,223,114,224" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="116,222,117,221,116,221,116,222,115,222,115,223,116,223" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="116,227,115,226,119,222,120,221,120,219,118,219,117,220,117,222,115,224,114,225,114,228,116,228" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="124,214,124,213,122,213,122,214,121,214,121,215,124,215" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="rect" coords="89,227,92,229" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="101,222,101,221,102,221" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="97,227,101,223,102,221,104,220,102,221,102,219,100,220,98,221,96,223,94,225,92,226,93,227,94,225,95,225,100,221,97,224,96,226,95,228" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="107,227,108,225,109,223,111,222,112,220,112,219,109,219,107,220,106,221,106,219,104,220,105,221,103,223,102,225,100,226,101,228,102,226,104,225,105,223,107,222,108,220,110,221,108,222,107,224,106,226,105,227,107,228" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="89,227,87,226,89,224,88,223,91,220,92,219,90,220,88,221,86,223,86,225,84,225,82,226,81,227,79,228,81,228,83,227,85,226,86,228,89,228" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="91,223,92,222,93,221,94,220,93,219,93,220,91,222,90,223,89,224,90,224" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="rect" coords="70,227,73,229" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="74,226,75,225,76,225,77,224,76,224,75,225,74,226,73,227" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="78,223,77,223,77,224" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="79,222,80,222,80,221,79,221,78,222,78,223" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="80,227,79,226,80,225,81,224,82,223,83,222,84,221,83,220,82,219,81,218,80,219,79,220,80,221,81,220,82,221,80,223,79,224,78,225,78,228,80,228" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="59,227,63,222,66,218,72,218,77,216,82,211,83,208,79,204,77,205,71,205,66,207,62,210,67,208,73,207,69,212,65,217,62,218,64,218,61,222,57,227,58,228" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="69,217,68,216,70,214,72,212,74,210,75,208,76,206,80,206,82,208,81,211,79,213,76,214,74,215,71,216" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="rect" coords="158,197,161,199" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="167,197,169,193,170,192,173,187,178,187,175,184,177,181,174,183,172,186,168,187,171,188,168,191,165,193,162,196,161,197,163,196,168,192,166,196,165,197,167,198" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="rect" coords="144,197,147,199" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="158,191,159,191,159,190,158,190,158,191,157,191,157,192,158,192" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="152,196,154,195,155,193,157,192,156,191,158,189,156,188,155,190,153,191,152,193,150,194,148,195,147,197,148,196,151,194,155,190,152,194,151,196,150,197" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="158,197,157,196,158,195,159,194,160,193,161,192,162,191,162,189,160,189,161,190,160,191,159,192,158,193,157,194,156,195,156,198,158,198" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="144,197,142,196,144,195,146,193,148,192,149,190,148,189,147,191,145,193,143,194,144,192,145,190,147,189,145,190,143,191,142,193,141,195,139,195,137,196,136,197,134,198,136,198,138,197,140,196,141,198,144,198" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="rect" coords="116,197,119,199" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="128,192,128,191,129,191" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="124,197,127,194,128,192,130,191,131,190,129,191,129,189,127,190,125,191,121,195,119,196,120,197,121,195,122,195,127,191,124,194,123,196,122,198" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="134,197,135,195,136,193,138,192,139,190,139,189,136,189,134,190,133,191,133,189,131,190,132,191,130,193,129,195,127,196,128,198,129,196,131,195,132,193,134,192,135,190,137,191,135,192,134,194,133,196,132,197,134,198" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="116,197,114,196,116,195,118,193,120,192,121,190,120,189,119,191,117,193,115,194,116,192,117,190,119,189,117,190,115,191,114,193,113,195,111,195,109,196,108,197,106,198,108,198,110,197,112,196,113,198,116,198" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="rect" coords="92,197,95,199" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="106,191,107,191,107,190,106,190,106,191,105,191,105,192,106,192" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="100,196,102,195,103,193,105,192,104,191,106,189,104,188,103,190,101,191,100,193,98,194,96,195,95,197,96,196,99,194,103,190,100,194,99,196,98,197" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="106,197,105,196,106,195,107,194,108,193,109,192,110,191,110,189,108,189,109,190,108,191,107,192,106,193,105,194,104,195,104,198,106,198" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="rect" coords="78,197,81,199" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="92,191,93,191,93,190,92,190,92,191,91,191,91,192,92,192" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="86,196,88,195,89,193,91,192,90,191,92,189,90,188,89,190,87,191,86,193,84,194,82,195,81,197,82,196,85,194,89,190,86,194,85,196,84,197" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="92,197,91,196,92,195,93,194,94,193,95,192,96,191,96,189,94,189,95,190,94,191,93,192,92,193,91,194,90,195,90,198,92,198" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="68,197,66,197,66,198,67,198" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="76,197,78,196,79,195,80,193,78,192,79,190,81,191,80,193,81,191,82,190,81,189,77,189,76,191,74,192,72,194,70,195,69,197,71,196,72,194,73,196,72,197,73,198,76,198" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="76,197,74,197,74,195,75,194,77,192,77,193,78,194,78,195,77,196" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="64,197,68,194,69,190,66,189,70,186,72,184,70,185,73,181,75,180,72,184,76,181,75,179,76,178,71,180,68,184,65,188,61,191,58,195,53,197,55,198,59,195,61,193,60,197,63,193,64,192,65,193,68,190,65,194,61,197,64,198" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="68,187,68,186,69,186" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="69,186,69,185,70,185" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="49,197,53,192,56,189,59,189,56,188,60,183,64,178,66,173,61,175,57,179,53,182,48,185,44,189,34,189,33,191,37,192,39,191,35,190,42,190,37,194,34,198,39,194,43,190,49,189,53,190,49,195,48,198" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="55,188,47,188,51,184,53,183,55,181,57,179,60,178,62,176,63,176,62,179,57,184,56,186" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="rect" coords="63,175,65,177" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="201,261,190,239,9,239,9,284,190,284" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
    <area shape="poly" coords="166,314,175,309,181,301,182,294,182,182,179,173,173,166,166,163,43,163,34,168,29,177,28,183,28,300,33,308,41,314,44,315,165,315" href="<?php echo site_url("front/utilisateur/infospremium/");?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp5b142b9a_06.png" id="art_1882" title="" alt="400€ht/an
" onload="OnLoadPngFix()" usemap="#map1" style="position:absolute;left:15px;top:194px;" border="0" height="394" width="620">
<div style="position:absolute;left:15px;top:217px;width:620px;height:22px;">
    <div class="Normal2-P-P2">
        <span class="Normal2-C-C5">Découvrez en détail nos 2 abonnements</span><span class="Normal2-C-C6"></span></div>
</div>
<div id="txt_874" style="position:absolute;left:28px;top:247px;width:593px;height:40px;overflow:hidden;">
<p class="Corps-P"><span class="Corps-C-C5">Comparez nos deux abonnements pour découvrir celui qui vous convient le mieux.</span></p>
<p class="Corps-P-P2"><span class="Corps-C-C5">Tarification annuelle ht. Pour plus d’informations, consultez nos Conditions générales</span></p>
<p class="Wp-Corps-P"><span class="Corps-C-C5"><br></span></p>
<p class="Wp-Corps-P"><span class="Corps-C-C5"><br></span></p>
</div>
<a href="<?php echo site_url("front/utilisateur/comparerabonnements/");?>" id="btn_238" class="Button1" style="position:absolute;left:187px;top:300px;width:274px;height:30px;"><span>Accès&nbsp;à&nbsp;la&nbsp;page&nbsp;comparative&nbsp;&nbsp;&nbsp;</span></a>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp2db6e1e0_06.png" id="qs_3093" alt="" onload="OnLoadPngFix()" style="position:absolute;left:464px;top:528px;" border="0" height="46" width="141">
<map id="map2" name="map2">
    <area shape="poly" coords="159,165,167,160,172,151,173,146,173,32,169,23,162,17,157,14,35,14,26,19,20,27,19,35,19,150,24,159,32,165,34,166,158,166" href="<?php echo site_url("front/utilisateur/infosreferencement/");?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp37aa2e2f_06.png" id="grp_3597" alt="" onload="OnLoadPngFix()" usemap="#map2" style="position:absolute;left:432px;top:344px;" border="0" height="198" width="201">
<div id="art_2062" style="position:absolute;left:455px;top:486px;width:140px;height:14px;white-space:nowrap;">
    <div class="Wp-Normal-P">
        <span class="Normal-C-C0">Puis 100€ ht l’année suivante</span></div>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpc0e796e5_06.png" id="pic_3005" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:1755px;" border="0" height="48" width="44">
<div id="txt_1392" style="position:absolute;left:84px;top:1762px;width:436px;height:22px;overflow:hidden;">
<p class="Normal2-P-P0"><span class="Strong-C">Le référencement de votre home page : </span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp390d754a_06.png" id="pic_3010" alt="" onload="OnLoadPngFix()" style="position:absolute;left:335px;top:2689px;" border="0" height="144" width="174">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp809827ce_06.png" id="pic_3008" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:2466px;" border="0" height="144" width="174">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp90ce72ac_06.png" id="pic_3007" alt="" onload="OnLoadPngFix()" style="position:absolute;left:18px;top:2697px;" border="0" height="144" width="174">
<a href="http://www.qualiroulettes.fr/" id="btn_248" class="Button2" style="position:absolute;left:43px;top:2614px;width:114px;height:30px;"><span>Accès&nbsp;direct</span></a>
<a href="http://www.valdetrolls.fr/" id="btn_256" class="Button2" style="position:absolute;left:361px;top:2611px;width:114px;height:30px;"><span>Accès&nbsp;direct</span></a>
<a href="http://www.jeanlouis-lepecheur.fr/" id="btn_257" class="Button2" style="position:absolute;left:38px;top:2848px;width:114px;height:30px;"><span>Accès&nbsp;direct</span></a>
<a href="http://www.etoiledor-bijouterie.fr/" id="btn_258" class="Button2" style="position:absolute;left:366px;top:2848px;width:114px;height:30px;"><span>Accès&nbsp;direct</span></a>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp6b16bf6a_06.png" id="pic_3013" alt="" onload="OnLoadPngFix()" style="position:absolute;left:185px;top:2686px;" border="0" height="140" width="140">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp29ba5d10_06.png" id="pic_3014" alt="" onload="OnLoadPngFix()" style="position:absolute;left:501px;top:2686px;" border="0" height="140" width="140">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpab35a00e_06.png" id="pic_3015" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:875px;" border="0" height="48" width="45">
<div id="txt_1394" style="position:absolute;left:85px;top:880px;width:550px;height:54px;overflow:hidden;">
<p class="Normal2-P"><span class="Strong-C">La création d’une home page web et mobile personnalisée:</span></p>
<p class="Corps-P-P1"><span class="Corps-C-C0">Le Club Proximité crée une home page attrayante en rappelant votre graphisme, logo.
    … </span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpe5660ebf_06.png" id="qs_3390" alt="" onload="OnLoadPngFix()" style="position:absolute;left:17px;top:2928px;" border="0" height="124" width="619">
<div id="txt_1395" style="position:absolute;left:217px;top:2960px;width:403px;height:97px;overflow:hidden;">
<p class="Corps-P"><span class="Corps-C">Tarif net ht pour la 1ère année : 250€</span></p>
<p class="Corps-P"><span class="Corps-C">et 100€ ht l’année suivante</span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp6469c1b1_06.png" id="grp_3618" alt="" onload="OnLoadPngFix()" style="position:absolute;left:14px;top:2898px;" border="0" height="183" width="189">
<a href="<?php echo site_url("front/professionnels/fiche/");?>" id="btn_130" class="Button3" style="position:absolute;left:141px;top:3122px;width:367px;height:30px;"><span>Je&nbsp;valide&nbsp;mon&nbsp;abonnement&nbsp;PLATINIUM&nbsp;&nbsp;</span></a>
</div>

</body></html>