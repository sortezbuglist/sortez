<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="keywords" content="<?php if (isset($oInfoCommercant->metatag)) echo $oInfoCommercant->metatag;?>" />

<title><?php echo "".$oInfoCommercant->NomSociete ; ?></title>

<!--<script type="text/javascript" src="<?php //echo GetJsPath("front/") ; ?>/wpscripts2013/jquery-1.8.3.js"></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.6.4.js"></script>
<script>
	$(function() {
			jQuery.ajax({
			url: '<?php echo base_url();?>front/commercant/manage_nbrevisites/<?php echo $oInfoCommercant->IdCommercant;?>',
			dataType: 'html',
			type: 'POST',
			async: true,
			success: function(data){
				//window.location.reload();
			}
		});
	});
</script>


<?php $data["zTitle"] = 'Accueil' ?>
<style type="text/css">
body  {
	margin:0px;
	padding:0px;
	background-repeat: repeat;
	background-image: url(<?php echo GetImagePath("front/"); ?>/wpimages2013/wp9e1de8e1_05_06.jpg);
	font-family: "Arial",sans-serif;
    font-size: 12px;
    line-height: 1.25em;
}
img {
	border:none;
}
.contener2013 {
	height: auto;
	width: 900px;
	margin-right: auto;
	margin-left: auto;
	position: relative;
	margin-top: 0px;
}.contenermenu2013 {
	background-color: #535353;
	float: left;
	height: 38px;
	width: 900px;
	position: relative;color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 11px;
    line-height: 1.27em;
}
.contenermenu2013 a {
	color:#FFF;
	text-decoration:none;
}
.ulmenu2013 {
	list-style:none outside none;
	margin:0px;
	padding:0px;
}
.ulmenu2013 li {
	list-style:none outside none;
	display: block;
    float: left;
	width:112px;
	background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages2013/background_menu_part.png);
	background-position:right;
	background-repeat:no-repeat;
	height:25px;
	text-align:center;
	vertical-align:central;
	padding-top:13px;
}
.infolinks2013 {
	background-color: #E3E1E2;
	float: left;
	height: 115px;
	width: 900px;
	position: relative;
}
.infos_part2013 {
	float: left;
	height: 100px;
	width: 530px;
	position: relative;
    font-family: "Arial",sans-serif;
    font-size: 19px;
    font-weight: 700;
    line-height: 1.21em;
	padding-left:20px;
	padding-top:15px;
	<?php if (isset($oInfoCommercant) && $oInfoCommercant->bandeau_color != ''  && $oInfoCommercant->bandeau_color != NULL) { ?>
	color: <?php echo $oInfoCommercant->bandeau_color; ?>;
	<?php } else { ?>
	color: #004DA3;
	<?php } ?>
}
.linkspart_2013 {
	float: right;
	height: 115px;
	width: 350px;
	position: relative;
	text-align:right;
}
.linksparttranslate_2013 {
	height:30px;
	padding-left:100px;
	padding-right:18px;
	padding-top:15px;
	text-align:right;
}
.linkspartsocial_2013{
	height:70px;
	padding-right : 10px;
}
.contenerall2013 {
	/*background-color: #E3E1E2;*/
	<?php if (isset($oInfoCommercant) && $oInfoCommercant->bandeau_top != ''  && $oInfoCommercant->bandeau_top != NULL) { ?>
	background-image: url(<?php echo base_url()."application/resources/front/photoCommercant/images/".$oInfoCommercant->bandeau_top; ?>);
	background-repeat: no-repeat;
	background-position: center top;
	<?php } ?>
	float: left;
	width: 900px;
	position: relative;
}
.contenerall2013_first {
	background-color: #E3E1E2;
	<?php 
	$base_path_system = str_replace('system/', '', BASEPATH);
	if (isset($oInfoCommercant) && $oInfoCommercant->bandeau_color_image != '' && file_exists($base_path_system."application/resources/front/images/wpimages2013/".$oInfoCommercant->bandeau_color_image)) { ?>
	background-image: url(<?php echo base_url()."application/resources/front/images/wpimages2013/".$oInfoCommercant->bandeau_color_image; ?>);
	<?php } else { ?>
	background-image: url(<?php echo GetImagePath("front/"); ?>/wpimages2013/wp5dcf1817_06_first.png);
	<?php } ?>
	background-repeat: no-repeat;
	background-position: center top;
	float: left;
	width: 900px;
	position: relative;
}
.contenermainleft2013 {
	float: left;
	width: 318px;
	position: relative;
	padding-left:7px;
}
.contenermainrigth2013 {
	float: left;
	width: 556px;
	position: relative;
	/*padding-top:60px;*/
	padding-top:53px;
}
.contenerlogo2013 {
	float: left;
	height: auto;
	width: 310px;
	position: relative;
	text-align: center;
	vertical-align: middle;
	padding-top:30px;
	padding-bottom:30px;
}
.contentcontactinfos2013 {
	float: left;
	height: auto;
	width: 280px;
	position: relative;
	margin-left:15px;
	margin-right:15px;
}
.contenercontactheader2013 {
	float: left;
	height: 25px;
	width: 261px;
	position: relative;
	border: 3px solid #FFF;
	color: #FFFFFF;
	font-family: "Arial",sans-serif;
	font-size: 15px;
	font-weight: 700;
	line-height: 1.2em;
	padding-top: 8px;
	padding-right: 5px;
	padding-bottom: 5px;
	padding-left: 8px;
}
.contenercontactmain2013 {
	background-color: #ffffff;
	float: left;
	width: 260px;
	height: auto;
	position: relative;
	padding:10px;
}
.contenerlinkleft2013 {
	float: left;
	height: auto;
	width: 290px;
	position: relative;
	margin:10px;
	text-align:center;
}
.contenerlinkleft2013 a {
	text-decoration:none;
	color: #000;
}
.contenerannoncebtn2013 {
	float: left;
	width: 310px;
	position: relative;
	padding-bottom:10px;
	text-align:center;
}
.conteneragendabtn2013 {
	float: left;
	width: 310px;
	position: relative;
	padding-bottom:10px;
	padding-top:10px;
	text-align:center;
}
.contenercartefidelite2013 {
	background-image: url("<?php echo GetImagePath("front/"); ?>/wpimages2013/wp4dac03cb_06.png");
    background-position: center center;
    background-repeat: no-repeat;
    float: left;
    height: 75px;
    padding: 100px 40px 15px;
    position: relative;
    width: 230px;
	color: #FFFFFF;
    text-decoration: none;
	font-family: "Arial",sans-serif;
    font-size: 13px;
    font-weight: 700;
    line-height: 1.23em;
	text-align:center;
}
.contenerbonplan2013 {
	background-image: url("<?php echo GetImagePath("front/"); ?>/btn_new/wp2993bf5e_06.png");
    background-position: center center;
    background-repeat: no-repeat;
    float: left;
    height: 105px;
    padding: 45px 65px 10px;
    position: relative;
    width: 180px;
	color: #FFFFFF;
    float: left;
    font-family: "Arial",sans-serif;
    font-size: 13px;
    font-weight: 700;
	margin-bottom:10px;
	text-align:center;
}
.contenerbonplan2013 a {
	color:#FFF;
	text-decoration:none;
}
.contenermaincontentrigth2013 {
	background-color: #FFF;
	float: left;
	width: 556px;
	position: relative;
}
.contenerslidemain2013 {
	float: left;
	/*height: 363px;*/
	height:auto;
	width: 550px;
	padding-top: 3px;
	padding-right: 3px;
	padding-bottom: 0px;
	padding-left: 3px;
	position: relative;
	
	background-image: url("<?php echo GetImagePath("front/"); ?>/loading37_color.gif");
    background-position: center center;
    background-repeat: no-repeat;
}
.contenermaincontentmain2013 {
	float: left;
    padding: 10px;
    position: relative;
    width: 536px;
}
.footer_content_partner{
	text-align:center;
	border: 0px solid #FFF; 
	background-color:#E3E1E2; 
	width:556px; float:left; 
	padding-top:15px; 
	padding-bottom:600px;
}
.footer_content_partner a {
	color:#000000;
}
div.navBulletsWrapper div {
	visibility: hidden;
}

</style>

<!--<meta name="google-translate-customization" content="ad114fb2f5d60b29-e2bf26e865a3d116-ga4de9321d692a0e0-29"></meta>-->
<meta name="google-translate-customization" content="ad114fb2f5d60b29-e2bf26e865a3d116-ga4de9321d692a0e0-29"></meta>


</head>

<body>
<div class="contener2013">
  <div class="contenermenu2013">
	<?php $this->load->view("front2013/includes/menu_partner", $data); ?>
  </div>
  <div class="infolinks2013">
    <div class="infos_part2013">
    	<?php $this->load->view("front2013/includes/titleinfo_partner", $data); ?>
    </div>
    <div class="linkspart_2013">
    	<div class="linksparttranslate_2013">
            
            <!-- Begin TranslateThis Button -->
<!--<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'fr', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
}

</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>-->

<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'fr', includedLanguages: 'ar,de,en,es,it,ja,nl,no,pl,pt,ru,sv,zh-CN', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false, multilanguagePage: true}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<!-- End TranslateThis Button -->

            
        </div>
        <div class="linkspartsocial_2013">
        <?php $this->load->view("front2013/includes/linkicon_partner", $data); ?>
        </div>
    </div>
  </div>
  <div class="contenerall2013_first">
  <div class="contenerall2013">
    <div class="contenermainleft2013">
      <div class="contenerlogo2013">
        <?php $this->load->view("front2013/includes/logo_partner", $data); ?>
      </div>
      <div class="contentcontactinfos2013">
        <div class="contenercontactheader2013">Contact & horaires</div>
        <div class="contenercontactmain2013">
            <?php $this->load->view("front2013/includes/contacthoraires_partner", $data); ?>
        </div>
      </div>
      <div class="contenerlinkleft2013">
		<?php $this->load->view("front2013/includes/linkiconleft_partner", $data); ?>
      </div>
		<?php
		$thisss =& get_instance();
		$thisss->load->library('ion_auth');
		$this->load->model("ion_auth_used_by_club");
		$user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);
		if (isset($user_ion_auth_id)) $user_groups = $thisss->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
		if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;
		//echo $nombre_annonce_com."qsdfqsdfffff".$group_id_commercant_user;
		?>
      
        <?php if ($group_id_commercant_user==3) {?>
        <div class="contenerlinkleft2013">
        	<img src="<?php echo GetImagePath("front/"); ?>/mobile2013/wpimages/wp90077af1_06.png" alt="mag">
        </div>
        <?php } ?>
        
        <?php if (!isset($dontshowannoncecase)) { ?>
			<?php if ($group_id_commercant_user==5) { ?>
              <?php if (isset($nombre_annonce_com) && $nombre_annonce_com!="0") { ?>
                  <div class="contenerannoncebtn2013">
                    <?php $this->load->view("front2013/includes/btn_annonce_partner", $data); ?>
                  </div>
              <?php } ?>
            <?php } ?>
        <?php } ?>
      <!--<div class="contenercartefidelite2013">
      	<?php //$this->load->view("front2013/includes/btn_cartefidelite_partner", $data); ?>
      </div>-->
      <?php if (isset($oLastbonplanCom)) { ?>
          <div class="contenerbonplan2013">
          <table width="100%" border="0" height="105" cellspacing="0" cellpadding="0">
              <tr>
                <td><?php $this->load->view("front2013/includes/btn_bonplan_partner", $data); ?></td>
              </tr>
            </table>
          </div>
      <?php } ?>
      <?php if (!isset($dontshowagendacase)) { ?>
			<?php if ($group_id_commercant_user==5) { ?>
              <?php if (isset($nombre_agenda_com) && $nombre_agenda_com!="0") { ?>
                  <div class="conteneragendabtn2013">
                    <?php $this->load->view("front2013/includes/btn_agenda_partner", $data); ?>
                  </div>
              <?php } ?>
            <?php } ?>
        <?php } ?>
    </div>
    <div class="contenermainrigth2013">
      <?php if (isset($pagecategory_partner) && $pagecategory_partner == 'list_agenda') { ?>
      <div style='
                border: 3px solid #FFFFFF;
                color: #FFFFFF;
                float: left;
                font-family: "Arial",sans-serif;
                font-size: 15px;
                font-weight: 700;
                height: 25px;
                line-height: 1.2em;
                padding: 8px 5px 5px 8px;
                position: relative;
                width: 537px;
      '>Notre Agenda</div>
      <?php } ?>
      <div class="contenermaincontentrigth2013">
        <?php if (isset($pagecategory_partner) && $pagecategory_partner == 'annonces_partner') { ?>
        <?php } else if (isset($pagecategory_partner) && $pagecategory_partner == 'details_agenda') { ?>
        <?php } else if (isset($pagecategory_partner) && $pagecategory_partner == 'list_agenda') { ?>
        <?php } else if (isset($pagecategory_partner) && $pagecategory_partner == 'details_annonces_partner') { ?>
        	<div class="contenerslidemain2013">
                <?php $this->load->view("front2013/includes/slide_annonces_partner", $data); ?>
            </div>
		<?php } else if (isset($pagecategory_partner) && $pagecategory_partner == 'bonplans_partner') { ?>
        	<div class="contenerslidemain2013">
                <?php $this->load->view("front2013/includes/slide_bonplans_partner", $data); ?>
            </div>            
        <?php } else { ?>
            <div class="contenerslidemain2013">
                <?php $this->load->view("front2013/includes/slide_partner", $data); ?>
            </div>
        <?php } ?>
        
        
        <div class="contenermaincontentmain2013">