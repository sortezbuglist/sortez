<div style="margin-top:10px; margin-bottom:10px;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="35%" style="text-align:left;">
    <select id="inputStringOrderByHidden_partenaires" size="1" name="inputStringOrderByHidden_partenaires">
    <option value="0"> -- Filtrer la liste -- </option>
    <option value="1" <?php if (isset($iOrderBy) && $iOrderBy == '1') echo 'selected';?>> Les partenaires les plus récents</option>
    <option value="2" <?php if (isset($iOrderBy) && $iOrderBy == '2') echo 'selected';?>> Les partenaires les plus visitées</option>
    <option value="3" <?php if (isset($iOrderBy) && $iOrderBy == '3') echo 'selected';?>> Les partenaires les plus anciens</option>
    </select>
    </td>
    <td width="35%">
    <input id="inputString_zMotCle" type="text" style="width:201px;" value="<?php if (isset($zMotCle) && $zMotCle!="") echo $zMotCle; else echo 'Mots clés';?>" name="inputString_zMotCle">
    <a href="javascript:void(0);" id="inputString_zMotCle_submit" class="inputString_zMotCle_submit"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp0d338d88_06.png" alt="" name="pcrv_4974" width="18" height="18" border="0" id="pcrv_4974" onload="OnLoadPngFix()"></a>
    </td>
    <td width="30%">

<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'fr', includedLanguages: 'ar,de,en,es,it,ja,nl,no,pl,pt,ru,sv,zh-CN', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false, multilanguagePage: true}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

    </td>
  </tr>
</table>
</div>
<script type="text/javascript">

$(function(){
     
    $("#inputString_zMotCle").keydown(function(){
            var inputString_zMotCle = $("#inputString_zMotCle").val();
            $("#zMotCle").val(inputString_zMotCle);
        });
		
		
	$("#inputString_zMotCle").focus(function(){
		var inputString_zMotCle = $("#inputString_zMotCle").val();
		if(inputString_zMotCle=='Mots clés') {
			$("#inputString_zMotCle").val('');
		}
	});
	$("#inputString_zMotCle").blur(function(){
		var inputString_zMotCle = $("#inputString_zMotCle").val();
		if(inputString_zMotCle=='') {
			$("#inputString_zMotCle").val('Mots clés');
		}
		$("#zMotCle").val(inputString_zMotCle);
	});
   
});

</script>