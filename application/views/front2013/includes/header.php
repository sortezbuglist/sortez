<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>ClubProximite</title>

<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/wpscripts2013/jquery-1.8.3.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	/*var O_Dest_xxx = $('.leftcontener2013').height();
	var O_Doc_xxx = $('.maincontener2013').height();
	if (O_Doc_xxx > O_Dest_xxx) {
		$('.leftcontener2013').height(O_Doc_xxx);
	}*/
	//alert(O_Doc_xxx);
	
	<?php if (isset($pagecategory) && $pagecategory == 'annonce') { 
			if (isset($_SESSION['iVilleId'])) {
				$iVilleId_sess = $_SESSION['iVilleId'];
			} else $iVilleId_sess = '0';
			?>
			check_show_categ_annonce(<?php echo $iVilleId_sess;?>);
	<?php } else if (isset($pagecategory) && $pagecategory == 'bonplan') { 
			if (isset($_SESSION['iVilleId'])) {
				$iVilleId_sess = $_SESSION['iVilleId'];
			} else $iVilleId_sess = '0';
			?>
			check_show_categ_bonplan(<?php echo $iVilleId_sess;?>);
	<?php } else { 
			if (isset($_SESSION['iVilleId'])) {
				$iVilleId_sess = $_SESSION['iVilleId'];
			} else $iVilleId_sess = '0';
			?>
			check_show_categ_partner(<?php echo $iVilleId_sess;?>);
	<?php } ?>
	
});
</script>

<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("front2013/ready_partenaires", $data); ?>
<?php $this->load->view("front2013/ready_annonces", $data); ?>
<?php $this->load->view("front2013/ready_bonplans", $data); ?>

<style type="text/css">
body  {
	margin:0px;
	padding:0px;
	background-repeat: repeat;
	background-position:center top;
	/*background-image: url(<?php //echo GetImagePath("front/"); ?>/wpimages2013/wp30e2106b_06.png);*/
	background-image: url(<?php echo GetImagePath("front/"); ?>/wpimages2013/wp9e1de8e1_05_06.jpg);
	/*background-color:#003366;*/
	font-family: "Arial",sans-serif;
    font-size: 13px;
    line-height: 1.23em;
}
img {
	border:none;
}
.contener2013 {
	height: auto;
	width: 1024px;
	margin-right: auto;
	margin-left: auto;
	position: relative;
	margin-top: 0px;
}
.header2013 {
	background-image: url(<?php echo GetImagePath("front/"); ?>/wpimages2013/bg_head_new_main.png);
	background-repeat: no-repeat;
	background-position: center top;
	float: left;
	height: 248px;
	width: 1024px;
	position: relative;
}
.space2013 {
	float: left;
	height: 15px;
	width: 1024px;
	position: relative;
}
.maincontener2013 {
	float: left;
	height: auto;
	width: 1024px;
	position: relative;
	background-color:#FFF;
}
.leftcontener2013 {
	float: left;
	width: 202px;
	position: relative;
	height: auto;
	/*<?php //if (isset($pagecategory) && $pagecategory == 'annonce') { ?>
	background-color: #003333;
	<?php //} else if (isset($pagecategory) && $pagecategory == 'bonplan') { ?>
	background-color: #A70000;
	<?php //} else { ?>*/
	background-color: #3653A2;
	<?php //} ?>
	color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 11px;
    font-weight: 700;
    line-height: 1.27em;
}
.rightcontener2013 {
	float: left;
	width: 822px;
	/*margin-left: 20px;*/
	margin-top:10px;
	position: relative;
}
.pubcontener2013 {
	float: left;
	width: 802px;
	position: relative;
	margin-left:10px;
}
.menucontener2013 {
	float: left;
	width: 802px;
	position: relative;
	margin-left:10px;
}
.contentcontener2013 {
	float: left;
	width: 822px;
	position: relative;
	font-family: "Arial",sans-serif;
    font-size: 12px;
    line-height: 1.25em;
}
.footer2013 {
	<?php //if (isset($pagecategory) && $pagecategory == 'bonplan') { ?>
	/*background-color: #004D9C;*/
	<?php //} else { ?>
	background-color: #3653A2;
	<?php //} ?>
	float: left;
	height: 108px;
	width: 1024px;
	position: relative;
	color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 13px;
    font-weight: 700;
    line-height: 1.23em;
	text-align:center;
	vertical-align: central;
	padding-top:40px;
}
.leftcontener2013top {
	/*<?php //if (isset($pagecategory) && $pagecategory == 'annonce') { ?>
	background-image: url(<?php echo GetImagePath("front/"); ?>/wpimages2013/wpa145ae83_06.png);
	<?php //} else if (isset($pagecategory) && $pagecategory == 'bonplan') { ?>
	background-image: url(<?php echo GetImagePath("front/"); ?>/wpimages2013/wp07e4a24e_06.png);
	<?php //} else { ?>*/
	background-image: url(<?php echo GetImagePath("front/"); ?>/wpimages2013/wp7d90bd82_06.png);
	<?php //} ?>
	background-repeat: no-repeat;
	float: left;
	height: 73px;
	width: 202px;
	position: relative;
	<?php //if (isset($pagecategory) && $pagecategory == 'annonce') { ?>
	/*color: #000000;
	<?php //} else if (isset($pagecategory) && $pagecategory == 'annonce') { ?>
	color: #FFFFFF;*/
	<?php //} else {?>
	color: #FFFFFF;
	<?php //} ?>
    font-family: "Arial",sans-serif;
    font-size: 16px;
    font-weight: 700;
    line-height: 1.25em;
	text-align:center;
	padding-top:5px;
}
.leftcontener2013content {
	float: left;
	width: 187px;
	position: relative;
	padding-left: 10px;
    padding-right: 5px;
}
.leftcontener2013title {
	float: left;
	height: auto;
	width: 177px;
	position: relative;
	/*<?php //if (isset($pagecategory) && $pagecategory == 'annonce') { ?>
	color: #060501;
	<?php //} else { ?>*/
	color: #FFFFFF;
	<?php //} ?>
	font-family: "Arial",sans-serif;
	font-size: 13px;
	font-weight: 700;
	line-height: 1.23em;
	text-align: left;
	/*<?php //if (isset($pagecategory) && $pagecategory == 'annonce') { ?>
	background-color: #FCBD18;
	<?php //} else if (isset($pagecategory) && $pagecategory == 'bonplan') { ?>
	background-color: #3653A3;
	<?php //} else { ?>*/
	background-color: #A60000;
	<?php //} ?>
	padding-left:20px;
	padding-right:5px;
}
.menucontener2013partenaire {
	float: left;
	height: 65px;
	width: 267px;
	position: relative;
	margin-top:10px;
	text-align: left;
}
.menucontener2013annonce {
	float: left;
	height: 65px;
	width: 267px;
	position: relative;
	margin-top:10px;
	text-align:center;
}
.menucontener2013bonplan {
	float: left;
	height: 65px;
	width: 267px;
	position: relative;
	margin-top:6px;
	text-align: right;
}
.menucontener2013partenaire_icon {
	float: left;
	width: 267px;
	position: relative;
	text-align: left;
}
.menucontener2013annonce_icon {
	float: left;
	width: 267px;
	position: relative;
	text-align:center;
}
.menucontener2013bonplan_icon {
	float: left;
	width: 267px;
	position: relative;
	text-align: right;
}
.infocontener2013 {
	<?php if (isset($pagecategory) && $pagecategory == 'annonce') { ?>
	background-color: #003333;
	<?php } else if (isset($pagecategory) && $pagecategory == 'bonplan') { ?>
	background-color: #3653A2;
	<?php } else { ?>
	background-color: #3653A2;
	<?php } ?>
	float: left;
	height: 20px;
	width: 802px;
	position: relative;
	color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 13px;
    line-height: 1.23em;
	text-align:center;
	padding-top:5px;
}
.mainiconhome {
	float: left;
	height: auto;
	width: 802px;
	position: relative;
	margin-left:15px;
	
}
#leftcontener2013contentactualise {
	color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 10px;
    font-weight: 700;
    line-height: 1.2em;
}
#id_annonce_minitext {
	height: 100px;
    padding-left: 27px;
    padding-top: 29px;
    width: 200px;	
	background-image: url(<?php echo GetImagePath("front/"); ?>/wpcda8dfe6_06.png);
	background-repeat: no-repeat;
}

</style>

<meta name="google-translate-customization" content="ad114fb2f5d60b29-e2bf26e865a3d116-ga4de9321d692a0e0-29"></meta>

</head>
<body>
<div class="contener2013">
  <div class="header2013">
	<?php $this->load->view("front2013/includes/header_content", $data); ?>
  </div>
  <!--<div class="space2013"></div>-->
  
  <?php if (isset($page_comparerabonnements_id) && $page_comparerabonnements_id=='1') { //afficher page comparaison abonnement?>
  
  <div class="maincontener2013">
  
  <?php } else {  //afficher page comparaison abonnement ?>
  
  <div class="maincontener2013">
    <div class="leftcontener2013">
    	<?php 
		if (isset($pagecategory) && $pagecategory == 'annonce')
		 $this->load->view("front2013/includes/searchform_content_annonce", $data); 
		else if (isset($pagecategory) && $pagecategory == 'bonplan')
		 $this->load->view("front2013/includes/searchform_content_bonplan", $data); 
		else $this->load->view("front2013/includes/searchform_content_partenaire", $data); 
		?>
    </div>
    <div class="rightcontener2013">
      <div class="pubcontener2013">
      	<!--<img src="<?php// echo GetImagePath("front/"); ?>/wpimages2013/wpf39b079d.gif" width="802" height="237" alt="pub" />-->
        <?php $this->load->view("front2013/includes/pubcontener2013", $data); ?>
      </div>
      <div class="menucontener2013">
        <div class="menucontener2013partenaire"><button onClick="javascript:redirect_home();" style="margin:0px; padding:0px;background: none repeat scroll 0 0 transparent;border: 0 none;cursor: pointer;margin: 0;padding: 0;"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpd0af58a5_06.png" width="249" height="65" alt="partenaires" /></button></div>
      	<div class="menucontener2013annonce"><button onClick="javascript:redirect_annonce();" style="margin:0px; padding:0px;background: none repeat scroll 0 0 transparent;border: 0 none;cursor: pointer;margin: 0;padding: 0;"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp4ef057dd_06.png" width="249" height="66" alt="annonces" /></button></div>
        <div class="menucontener2013bonplan"><button onClick="javascript:redirect_bonplan();" style="margin:0px; padding:0px;background: none repeat scroll 0 0 transparent;border: 0 none;cursor: pointer;margin: 0;padding: 0;"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp8688d212_06.png" width="248" height="68" alt="bonplans" /></button></div>
      </div>
      <!--<div class="infocontener2013">Pour plus d’informations, cliquez sur les icones !</div>-->
      <div class="mainiconhome">
      	<?php $this->load->view("front2013/includes/mainiconhome2013", $data); ?>
      </div>
      <div class="mainiconhome">
      	<?php $this->load->view("front2013/includes/mainfilter2013", $data); ?>
      </div>
      <div class="contentcontener2013">
      
   <?php }  //afficher page comparaison abonnement ?>   