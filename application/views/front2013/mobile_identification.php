<?php $data["zTitle"] = 'Commercant' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="fr"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Clubproximite Identifiant</title>
<meta name="Generator" content="Serif WebPlus X6">
<meta name="viewport" content="width=320">
<style type="text/css">
body{margin:0;padding:0;}
.Normal-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:center; font-weight:400;
}
.Normal-P-P0
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:justify; font-weight:400;
}
.Corps-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:justify; font-weight:400;
}
.Corps-P-P0
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:center; font-weight:400;
}
.Corps-P-P1
{
    margin:0.0px 0.0px 12.0px 0.0px; text-align:justify; font-weight:400;
}
.Normal-C
{
    font-family:"Arial", sans-serif; font-weight:700; color:#b30000; font-size:17.0px; line-height:1.18em;
}
.Normal-C-C0
{
    font-family:"Arial", sans-serif; font-size:8.0px; line-height:1.25em;
}
.Normal-C-C1
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
.Normal-C-C2
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:12.0px; line-height:1.25em;
}
.Strong-C
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
.Normal-C-C3
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:8.0px; line-height:1.25em;
}
.Strong-C-C0
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:12.0px; line-height:1.25em;
}
.Corps-C
{
    font-family:"Arial", sans-serif; font-size:8.0px; line-height:1.25em;
}
.Corps-C-C0
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C1
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C2
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:11.0px; line-height:12.0px;
}
.Corps-C-C3
{
    font-family:"Arial", sans-serif; font-size:11.0px; line-height:1.27em;
}
.Corps-C-C4
{
    font-family:"Verdana", sans-serif; font-size:11.0px; line-height:1.18em;
}
.Corps-artistique-C
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:12.0px; line-height:1.25em;
}
</style>
<link rel="stylesheet" href="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpstyles.css" type="text/css"><script src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/fs_002.js" type="text/javascript"></script><script src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/suggest-10070.js" type="text/javascript"></script><script src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/fs.js" type="text/javascript"></script><script src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/suggest-10070.js" type="text/javascript"></script><script src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/javascript.js" type="text/javascript"></script><script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/w.js"></script><script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/w_002.js"></script>

<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/wpscripts2013/jquery-1.8.3.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>
<script type="text/javascript">

$(function(){
     
    $("#btn_connexion_submit").click(function(){
            
            var txtError = "";
            
            //verify input content
            var user_login_mobile = $("#user_login_mobile").val();
            if(user_login_mobile=="" || user_login_mobile=="Précisez votre courriel") {
                
                txtError += "- Veuillez indiquer Votre user_login_mobile <br/>"; 
                //alert("Veuillez indiquer Votre nom");
                $("#user_login_mobile").css('border-color', 'red');
            } else {
                $("#user_login_mobile").css('border-color', '#E3E1E2');
            }
            
            var user_pass_mobile = $("#user_pass_mobile").val();
            if(user_pass_mobile=="" || user_pass_mobile=="Précisez votre mot de passe") {
                
                txtError += "- Veuillez indiquer Votre user_pass_mobile <br/>"; 
                //alert("Veuillez indiquer Votre nom");
                $("#user_pass_mobile").css('border-color', 'red');
            } else {
                $("#user_pass_mobile").css('border-color', '#E3E1E2');
            }
            
            //alert(txtError);
            if(txtError == "") {
                //alert('ok');
                $("#frmConnexion_mobile").submit();
                //$("#qsdf_x").click();
            }
            
        });
        
        
        
        $('#user_login_mobile').focusin(function(){
            if($(this).val()=="Précisez votre courriel") {
                $(this).val("");
            }
        });
        $('#user_login_mobile').focusout(function(){
            if($(this).val()=="") {
                $(this).val("Précisez votre courriel");
            }
        });
        $('#user_pass_mobile').focusin(function(){
            if($(this).val()=="Précisez votre mot de passe") {
                $(this).val("");
            }
        });
        $('#user_pass_mobile').focusout(function(){
            if($(this).val()=="") {
                $(this).val("Précisez votre mot de passe");
            }
        });
        
    
   
});

   
</script>


</head>

<body style="background:#ffffff url('<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpd5148546_06.jpg') repeat scroll top center; height:1050px;" text="#000000">
<div style="background-color:transparent;margin-left:auto;margin-right:auto;position:relative;width:320px;height:1050px;">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp88416f97_06.png" alt="" style="position:absolute;left:0px;top:0px;" border="0" height="133" width="320">
<div style="position:absolute;left:16px;top:323px;width:292px;height:243px;overflow:hidden;">
<p class="Normal-P"><span class="Normal-C">L’inscription au Club est gratuite!</span></p>
<p class="Normal-P-P0"><span class="Normal-C-C0"><br></span></p>
<p class="Normal-P-P0"><span class="Normal-C-C1">Avec le </span><span class="Normal-C-C2">Club proximité, </span><span class="Strong-C">vous bénéficiez </span><span class="Normal-C-C2">d'infor-<wbr>mations, d’annonces et bons plans
    </span><span class="Normal-C-C1">en temps réel sur le commerce et l’artisanat local dans les &nbsp;Alpes-<wbr>maritimes.</span></p>
<p class="Normal-P-P0"><span class="Normal-C-C0"><br></span></p>
<p class="Normal-P-P0"><span class="Normal-C-C2">Ces données sont disponibles sur votre mobile et ordinateur (www.club-<wbr>proximite.com)</span></p>
<p class="Normal-P-P0"><span class="Normal-C-C3"><br></span></p>
<p class="Corps-P"><span class="Strong-C-C0">En vous inscrivant au Club,</span><span class="Strong-C"> vous recevez directement </span><span class="Strong-C-C0">par courriel et push (smartphones)
    des alertes personnalisées</span><span class="Strong-C"> des partenaires que vous avez référençés dans vos «&nbsp;favoris&nbsp;».</span></p>
<p class="Corps-P"><span class="Corps-C"><br></span></p>
<p class="Corps-P"><span class="Corps-C-C0">De plus, vous recevez toutes les semaines </span><span class="Corps-C-C1">une newsletter informative</span><span class="Corps-C-C0">.</span></p>
<p class="Corps-P"><span class="Corps-C"><br></span></p>
<p class="Corps-P"><span class="Corps-C-C1"><br></span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpb2d10006_06.png" alt="" style="position:absolute;left:26px;top:185px;" border="0" height="57" width="54">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpd30804d5_06.png" alt="" style="position:absolute;left:131px;top:188px;" border="0" height="51" width="51">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpc812400c_06.png" alt="" style="position:absolute;left:242px;top:186px;" border="0" height="54" width="41">
<div style="position:absolute;left:3px;top:251px;width:100px;height:64px;overflow:hidden;">
<p class="Corps-P-P0"><span class="Corps-C-C2">Leurs fiches</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C2">informatives</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C2">ou leurs</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C2">sites web</span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpfbbf7515_06.png" alt="" style="position:absolute;left:33px;top:129px;" border="0" height="40" width="253">
<div style="position:absolute;left:105px;top:251px;width:101px;height:67px;overflow:hidden;">
<p class="Corps-P-P0"><span class="Corps-C-C2">Leurs articles</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C2">ou services (catalogue, &nbsp;promotions…)</span></p>
</div>
<div style="position:absolute;left:208px;top:251px;width:100px;height:79px;overflow:hidden;">
<p class="Corps-P-P0"><span class="Corps-C-C2">Leurs bons</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C2">plans</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C2">(offres uniques</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C2">ou permanentes)</span></p>
</div>
<map id="map0" name="map0">
    <area shape="rect" coords="4,8,291,60" href="<?php echo site_url("front/particuliers/inscription/");?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp05d7e3b3_06.png" alt="" usemap="#map0" style="position:absolute;left:14px;top:725px;" border="0" height="74" width="294">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp9fb8050d_06.png" alt="" style="position:absolute;left:66px;top:815px;" border="0" height="22" width="187">


<form id="frmConnexion_mobile" name="frmConnexion_mobile" action="<?php echo site_url("connexion/entrer/"); ?>" method="post" enctype="multipart/form-data"> 
    <input name="txtLogin" id="user_login_mobile" value="Précisez votre courriel" style="position:absolute; left:16px; top:847px; width:292px;" type="text">
    <input name="txtMotDePasse" id="user_pass_mobile" value="Précisez votre mot de passe" style="position:absolute; left:16px; top:874px; width:292px;" type="password">
    <a href="Javascript:void();" id="btn_connexion_submit">
    <img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpcd5cd45f_06.png" title="" alt="index2" usemap="#map1" style="position:absolute;left:16px;top:902px;" border="0" height="63" width="294">
    </a>
</form>

<!--<map id="map1" name="map1">
        <area shape="rect" coords="0,11,295,53" href="http://www.proximite-magazine.com/club/mobile/index2.html" alt="">
    </map>-->
<div style="position:absolute;left:69px;top:599px;width:239px;height:116px;overflow:hidden;">
<p class="Corps-P"><span class="Corps-C-C3">Réservez en ligne et déplacez vous pour concrétiser &nbsp;l’offre.</span></p>
<p class="Corps-P"><span class="Corps-C"><br></span></p>
<p class="Corps-P"><span class="Corps-C-C3">Réalisez votre achat et votre paiement en ligne et recevez votre achat à domicile.</span></p>
<p class="Corps-P"><span class="Corps-C"><br></span></p>
<p class="Corps-P"><span class="Corps-C-C3">Adressez une demande d’information complé-<wbr>mentaire avec l’envoi d’un message express.</span></p>
<p class="Corps-P-P1"><span class="Corps-C-C4"><br></span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpaf0e668f_06.png" alt="" style="position:absolute;left:23px;top:673px;" border="0" height="38" width="20">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpc7e4eaa7_06.png" alt="" style="position:absolute;left:12px;top:640px;" border="0" height="24" width="44">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp712a8694_06.png" alt="" style="position:absolute;left:16px;top:598px;" border="0" height="31" width="30">
<div style="position:absolute;left:16px;top:573px;width:271px;height:15px;white-space:nowrap;">
    <div class="Wp-Corps-artistique-P">
        <span class="Corps-artistique-C">Suivant les offres des partenaires vous pouvez :</span></div>
</div>
<!--<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp63180d8d_06.png" alt="" style="position:absolute;left:0px;top:996px;" border="0" height="54" width="320">
<a href="http://www.proximite-magazine.com/club/mobile/recherches-partenaires.html"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp5533b116.gif" title="" alt="recherches-partenaires" style="position:absolute;left:2px;top:994px;" border="0" height="55" width="57"></a>
<a href="http://www.proximite-magazine.com/club/mobile/cherchezannonces.html"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp5533b116.gif" title="" alt="cherchezannonces" style="position:absolute;left:75px;top:994px;" border="0" height="55" width="57"></a>
<a href="http://www.proximite-magazine.com/club/mobile/recherchebonsplans.html"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp5533b116.gif" title="" alt="recherchebonsplans" style="position:absolute;left:140px;top:994px;" border="0" height="55" width="57"></a>
<a href="http://www.proximite-magazine.com/club/mobile/favoris-partenaires.html"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp5533b116.gif" title="" alt="favoris-partenaires" style="position:absolute;left:197px;top:994px;" border="0" height="55" width="56"></a>
<a href="http://www.proximite-magazine.com/club/mobile/mon-compte.html"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp5533b116.gif" title="" alt="mon-compte" style="position:absolute;left:260px;top:994px;" border="0" height="55" width="57"></a>
-->
<div style="position:absolute;left:0px;top:996px;">
<?php 
$data["zTitle"] = 'Identification';
$this->load->view("front2013/includes/mobile_footer.php", $data);
?>
</div>

</div>

</body></html>