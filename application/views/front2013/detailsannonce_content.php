

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top">
    
    
    
    <table width="200" border="0">
      <tr>
        <td><a href="<?php echo site_url($commercant_url_annonces);?>"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/btn_retour_details_annonce.png"alt="" name="pcrv_5443"  border="0" id="pcrv_5443"></a></td>
        <td><a href="<?php echo site_url($commercant_url_annonces);?>" style="color:#000000; text-decoration:none;">Retour vers le menu<br/>général des annonces</a></td>
      </tr>
    </table>
    
    <table width="100%" border="0" style="padding-right:10px;">
      <tr>
        <td><p><strong>Désignation : </strong><?php echo $oDetailAnnonce->texte_courte ; ?></p></td>
      </tr>
      <!--<tr>
        <td><strong>Catégorie :</strong> <?php// if ($oDetailAnnonce->sous_rubrique!="") echo $oDetailAnnonce->sous_rubrique ; else echo $oDetailAnnonce->rubrique ; ?></td>
        
      </tr>-->
      <!--<tr>
        <td><strong>Référence produit :</strong></td>
      </tr>-->
      <tr>
        <?php
                $zEtat = "" ;
                if ($oDetailAnnonce->etat == 0) $zEtat = "Revente" ;
                else if ($oDetailAnnonce->etat == 1) $zEtat = "Neuf" ;
                else if ($oDetailAnnonce->etat == 2) $zEtat = "Service" ;
                
            ?>
        <td><strong>Etat :</strong> <?php echo $zEtat ; ?></td>
      </tr>
      
      <tr>
        <td>
        <strong>Annonce N° :</strong> <?php echo ajoutZeroPourString($oDetailAnnonce->annonce_id, 6) ?> 
            <?php if (convertDateWithSlashes($oDetailAnnonce->annonce_date_debut)!="00/00/0000") {?>
            du <?php echo convertDateWithSlashes($oDetailAnnonce->annonce_date_debut) ; ?>
            <?php }?>
        </td>
      </tr>
      
      <tr>
        <td>
        <p><?php echo $oDetailAnnonce->texte_longue ; ?></p>
        </td>
      </tr>
      
    </table>
    
    <div style="height:5px;">&nbsp;</div>
    
    <?php if (isset($oDetailAnnonce->module_paypal) && $oDetailAnnonce->module_paypal == "1") {?>
    <table width="100%" border="0" cellspacing="5" cellpadding="5" style="text-align:center;">
      <tr>
        <td colspan="2"><img src="<?php echo GetImagePath("front/"); ?>/btn_new/wp6ac3e457_05_06.jpg" alt="" name="pcrv_54gfd43"  border="0" id="pcrv_54dfgh43"></td>
      </tr>
      <tr>
        <td><?php echo $oDetailAnnonce->module_paypal_btnpaypal ; ?></td>
      </tr>
      <tr>
        <td><?php echo $oDetailAnnonce->module_paypal_panierpaypal ; ?></td>
      </tr>
    </table>
	<?php } ?>
    
    </td>
    <td width="200" valign="top">
    	<?php if (($oDetailAnnonce->annonce_date_debut=="0000-00-00" || $oDetailAnnonce->annonce_date_debut=="") && ($oDetailAnnonce->annonce_date_fin=="0000-00-00" || $oDetailAnnonce->annonce_date_fin=="")) {} else {?>
    	<div style='
        width:140px; 
        background-color:#434343; 
        background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages2013/wpf43833ae_06.png);
        padding-left:60px;
        color: #FFFFFF;
        font-family: "Arial",sans-serif;
        font-size: 12px;
        line-height: 1.25em;
        height:80px;
        '>
            <table width="100%" border="0" height="80">
              <tr>
                <td>
                <?php if ($oDetailAnnonce->annonce_date_debut!="0000-00-00" && $oDetailAnnonce->annonce_date_debut!=null) {?>
                <strong>Début de l’offre :</strong><br/><?php echo translate_date_to_fr($oDetailAnnonce->annonce_date_debut); ?><br/>
                <?php } ?>
                <?php if ($oDetailAnnonce->annonce_date_fin!="0000-00-00" && $oDetailAnnonce->annonce_date_fin!=null) {?>
                <strong>Fin de l’offre :</strong><br/><?php echo translate_date_to_fr($oDetailAnnonce->annonce_date_fin); ?><br/>
                <?php } ?>
                </td>
              </tr>
            </table>
        </div>
        <div style="height:5px;">&nbsp;</div>
        <?php } ?>
		
        <?php if ($oDetailAnnonce->annonce_quantite=="" || $oDetailAnnonce->annonce_quantite==null || $oDetailAnnonce->annonce_quantite=="0" || intval($oDetailAnnonce->annonce_quantite)==0) {} else { ?>
        <div style='
        width:140px; 
        background-color:#434343; 
        background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages2013/wp485899b8_06.png);
        padding-left:60px;
        color: #FFFFFF;
        font-family: "Arial",sans-serif;
        font-size: 12px;
        line-height: 1.25em;
        height:113px;
        '>
            <table width="100%" border="0">
              <tr>
                <td colspan="2" height="46"><span style='color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 12px;
    font-weight: 700;
    line-height: 1.25em;'>Limite produit </span></td>
              </tr>
              <tr>
                <td width="70"><span style='color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 16px;
    font-weight: 700;
    line-height: 1.25em;'>
    <?php if ($oDetailAnnonce->annonce_quantite=="" || $oDetailAnnonce->annonce_quantite==null) echo 'Reste épuisé'; else echo 'Reste à ce jour'; ?>
    </span></td>
                <td width="70"><span style='color: #434343;
    font-family: "Arial",sans-serif;
    font-size: 32px;
    font-weight: 700;
    line-height: 1.19em;'>&nbsp;<?php if ($oDetailAnnonce->annonce_quantite!="") echo $oDetailAnnonce->annonce_quantite ; else echo '00'; ?></span></td>
              </tr>
            </table>
        </div>
        <div style="height:5px;">&nbsp;</div>
        <?php } ?>
        
        <?php if ($oDetailAnnonce->ancien_prix=='0' || $oDetailAnnonce->ancien_prix=="" || $oDetailAnnonce->ancien_prix==null) {} else {?>
        <div style='
        width:140px; 
        background-color:#434343; 
        background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages2013/wpdf12193d_06.png);
        padding-left:60px;
        padding-bottom:10px;
        color: #FFFFFF;
        font-family: "Arial",sans-serif;
        font-size: 12px;
        line-height: 1.25em;
        height:67px; padding-top:3px;
        '>
            <table width="100%" border="0">
              <tr>
                <td valign="middle">
                <?php if ($oDetailAnnonce->ancien_prix=='0' || $oDetailAnnonce->ancien_prix=="" || $oDetailAnnonce->ancien_prix==null) {} else {?>
                <span style='color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 13px;
    font-weight: 700;
    line-height: 1.23em;'>Prix de vente</span><br/>
                <span style='color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 30px;
    font-weight: 700;
    line-height: 1.23em;'><?php echo $oDetailAnnonce->ancien_prix ; ?> €</span><br/>
    <?php } ?>
    
                <?php if ($oDetailAnnonce->prix_vente=='0' || $oDetailAnnonce->prix_vente=="" || $oDetailAnnonce->prix_vente==null) {} else {?>
                <span style='color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 13px;
    line-height: 1.23em;'>Ancien tarif : <?php echo $oDetailAnnonce->prix_vente ; ?>€</span><br/>
    <?php } ?>
                </td>
              </tr>
            </table>
        </div>
        <div style="height:5px;">&nbsp;</div>
        <?php } ?>
        
        <?php if (isset($oDetailAnnonce->retrait_etablissement) && $oDetailAnnonce->retrait_etablissement == "1") {?>
        <div><img src="<?php echo GetImagePath("front/");?>/btn_new/retrait_etablissement.png" id="retrait_etablissement_img"/></div>
        <?php } ?>
        
        <?php if (isset($oDetailAnnonce->livraison_domicile) && $oDetailAnnonce->livraison_domicile == "1") {?>
        <div><img src="<?php echo GetImagePath("front/");?>/btn_new/livraison_domicile.png" id="livraison_domicilet_img"/></div>
        <?php } ?>
    </td>
  </tr>
</table>




<!--<div style="height:196px; background-image:url(<?php //echo GetImagePath("front/"); ?>/wpimages2013/wp2d202ce4_06.png); background-position:right; background-repeat:no-repeat;">
    <img src="<?php //echo GetImagePath("front/"); ?>/wpimages2013/img_reservation_enligne.png" alt="img">
</div>
-->



<!--<table width="100%" border="0" cellspacing="0" cellpadding="0" style='font-family: "Arial",sans-serif;
    font-size: 11px;
    font-weight: 700;
    line-height: 1.27em; text-align:center; background-color:#FFCC33'>
  <tr>
    <td>
    <img src="<?php //echo GetImagePath("front/"); ?>/wpimages2013/wpf9a6272f_06.png" alt="img"><br/>
    Avant de pouvoir valider<br/>cette réservation il faut être<br/> inscrit au Club Proximité<br/><br/>
    <a href="<?php //echo site_url('front/particuliers/inscription');?>" style="text-decoration:none;"><input type="button" name="" id="" value="Je m'inscris"/></a><br/><br/>
    </td>
    <td>
    <img src="<?php //echo GetImagePath("front/"); ?>/wpimages2013/img_sms_annonce.png" alt="img"><br/>
    Vous recevez par retour un<br/> email vous confirmant cette<br/> réservation et sa durée de<br/> mise à disposition;
    
    </td>
    <td>
    <img src="<?php //echo GetImagePath("front/"); ?>/wpimages2013/img_sdf_annonce.png" alt="img"><br/>
    Pour bénéficier de cette offre,<br/> il vous suffit de nous<br/> présenter lors de votre<br/> déplacement le mail imprimé<br/> ou présent sur  votre<br/> smartphone
    </td>
  </tr>
</table>-->

<?php
$thisss =& get_instance();
$thisss->load->library('ion_auth');
if ($thisss->ion_auth->logged_in() && $thisss->ion_auth->in_group(2)) {
	$user_ion_auth = $thisss->ion_auth->user()->row();
}
?>

<center>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>
<script type="application/javascript">
$(document).ready(function() {
        $("#btn_submit_form_module_detailannnonce").click(function(){
			//alert('test form submit');
			txtErrorform = "";
			
            var txtError_text_mail_form_module_detailannnonce = "";
            var text_mail_form_module_detailannnonce = $("#text_mail_form_module_detailannnonce").val();
            if(text_mail_form_module_detailannnonce=="") {
                //$("#divErrorform_module_detailannnonce").html('<font color="#FF0000">Veuillez saisir votre demande</font>');
                txtErrorform += "1";
				$("#text_mail_form_module_detailannnonce").css('border-color', 'red');
				$("#text_mail_form_module_detailannnonce").focus();
            } else {
				$("#text_mail_form_module_detailannnonce").css('border-color', '#E3E1E2');
			}
			
			
			var nom_mail_form_module_detailannnonce = $("#nom_mail_form_module_detailannnonce").val();
            if(nom_mail_form_module_detailannnonce=="") {
                txtErrorform += "- Veuillez indiquer Votre nom_mail_form_module_detailannnonce <br/>"; 
				$("#nom_mail_form_module_detailannnonce").css('border-color', 'red');
				$("#nom_mail_form_module_detailannnonce").focus();
            } else {
				$("#nom_mail_form_module_detailannnonce").css('border-color', '#E3E1E2');
			}
            
            var email_mail_form_module_detailannnonce = $("#email_mail_form_module_detailannnonce").val();
            if(email_mail_form_module_detailannnonce=="" || !isEmail(email_mail_form_module_detailannnonce)) {
                txtErrorform += "- Veuillez indiquer Votre email_mail_form_module_detailannnonce <br/>"; 
                //alert("Veuillez indiquer Votre nom");
                $("#email_mail_form_module_detailannnonce").css('border-color', 'red');
				$("#email_mail_form_module_detailannnonce").focus();
            } else {
				$("#email_mail_form_module_detailannnonce").css('border-color', '#E3E1E2');
			}
			
            if(txtErrorform == "") {
                $("#form_module_detailannnonce").submit();
            }
        })
		
		
    });
</script>
<?php if(isset($user_ion_auth)) {?>
<style type="text/css">
.inputhidder {
	visibility:hidden;
}
</style>
<?php }?>


<table border="0" align="center" style="text-align:center;">
  <tr>
    <td style="text-align:left"><img src="<?php echo GetImagePath("front/"); ?>/btn_new/info_annonce_img.png" alt="img" width="270"></td>
    <td>
    <form method="post" name="form_module_detailannnonce" id="form_module_detailannnonce" action="" enctype="multipart/form-data">
    
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="inputhidder">
    <td>Votre Nom</td>
    <td><input id="nom_mail_form_module_detailannnonce" name="nom_mail_form_module_detailannnonce" type="text" value="<?php if(isset($user_ion_auth) && $user_ion_auth->first_name!="") echo $user_ion_auth->first_name;?>"></td>
  </tr>
  <tr class="inputhidder">
    <td>Votre Téléphone</td>
    <td><input id="tel_mail_form_module_detailannnonce" name="tel_mail_form_module_detailannnonce" type="text" value="<?php if(isset($user_ion_auth) && $user_ion_auth->phone!="") echo $user_ion_auth->phone;?>"></td>
  </tr>
  <tr class="inputhidder">
    <td>Votre Email</td>
    <td><input id="email_mail_form_module_detailannnonce" name="email_mail_form_module_detailannnonce" type="text" value="<?php if(isset($user_ion_auth) && $user_ion_auth->email!="") echo $user_ion_auth->email;?>"></td>
  </tr>
  <tr>
    <td colspan="2">
    <textarea id="text_mail_form_module_detailannnonce" style="width:246px; height:130px;" cols="28" rows="7" name="text_mail_form_module_detailannnonce"></textarea>
    </td>
  </tr>
  <tr>
    <td colspan="2">
    <input id="btn_reset_form_module_detailannnonce" type="reset" value="Effacer" style="width:82px; height:22px;">
    <input id="btn_submit_form_module_detailannnonce" type="button" name="btn_submit_form_module_detailannnonce" value="Envoyer" style="width:90px; height:22px;">
    </td>
  </tr>
</table>    
    
    
    </form>
    </td>
  </tr>
  <tr>
  <td>&nbsp;</td>
  <td><div id="divErrorform_module_detailannnonce"><?php if (isset($mssg_envoi_module_detail_annonce)) echo $mssg_envoi_module_detail_annonce;?></div></td>
  </tr>
</table>
<br/>
<span style='font-family: "Arial",sans-serif;
    font-size: 12px;
    font-weight: 700;
    line-height: 1.25em;'>Vous avez une question particulière à nous poser, adressez nous un mail express</span>
</center>


