<?php $data["zTitle"] = $oDetailAnnonce->texte_courte;?>

<?php $this->load->view("front2013/includes/mobile_header_page_partner", $data); ?>

<div style="background-color:#FFFFFF;">

<div style="margin-bottom:15px; padding-top:15px;"><a class="Button1" href="javascript:history.go(-1);">Retour</a></div>
<div style="margin-bottom:15px;"><a class="Button1" href="<?php echo site_url("front/annonce/photosannoncemobile");?>/<?php echo $oDetailAnnonce->annonce_id ; ?>">Plus de Photos</a></div>

<div id="txt_578" style="width:100%;height: auto;overflow:hidden; padding:15px;">
<p><strong>Désignation : </strong><?php echo $oDetailAnnonce->texte_courte ; ?></p>
<!--Catégorie :<?php //echo $oDetailAnnonce->rubrique ; ?> - <?php //echo $oDetailAnnonce->sous_rubrique ; ?><br/>
Produit :<br/>-->
<?php
$zEtat = "" ;
$zEtat = ($oDetailAnnonce->etat == 1) ? "Neuf" : "Occasion" ;
?>
<strong>Etat :</strong> <?php echo $zEtat ; ?><br/>
<!--Prix de vente :<?php //echo $oDetailAnnonce->prix_vente ; ?> &euro;<br/>-->
<strong>Annonce N° : </strong><?php echo ajoutZeroPourString($oDetailAnnonce->annonce_id, 6);?> <?php if ($oDetailAnnonce->annonce_date_debut!="0000-00-00") echo " du ".convertDateWithSlashes($oDetailAnnonce->annonce_date_debut) ; ?><br/>
<!--Début de l’offre :<?php// echo convertDateWithSlashes($oDetailAnnonce->annonce_date_debut) ; ?><br/>
Fin de l’offre :<?php// echo convertDateWithSlashes($oDetailAnnonce->annonce_date_fin) ; ?><br/>-->

<!--D&eacute;scription :--><br/>
<?php echo $oDetailAnnonce->texte_longue ; ?><br/>

</div>


<div style="height:10px;">&nbsp;</div>
<?php if (($oDetailAnnonce->annonce_date_debut=="0000-00-00" || $oDetailAnnonce->annonce_date_debut=="") && ($oDetailAnnonce->annonce_date_fin=="0000-00-00" || $oDetailAnnonce->annonce_date_fin=="")) {} else {?>
<div style='
width:100%; 
background-color:#434343; 
background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages2013/wpf43833ae_06_fond.png);
background-repeat:no-repeat;
color: #FFFFFF;
font-family: "Arial",sans-serif;
font-size: 12px;
line-height: 1.25em;
padding-bottom: 5px;
padding-top: 5px;
height: auto;
'>

<table width="300" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="60" valign="middle"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpf43833ae_06_calendar.png"></td>
    <td valign="middle">
    
    <table width="100%" border="0" cellpadding="0" cellspacing="0" height="60">
      <tr>
        <td>
        <?php if ($oDetailAnnonce->annonce_date_debut!="0000-00-00" && $oDetailAnnonce->annonce_date_debut!=null) {?>
        <strong>Début de l’offre :</strong><br/><?php echo translate_date_to_fr($oDetailAnnonce->annonce_date_debut) ; ?>
        <?php } ?>
        </td>
        <td>
        <?php if ($oDetailAnnonce->annonce_date_fin!="0000-00-00" && $oDetailAnnonce->annonce_date_fin!=null) {?>
        <strong>Fin de l’offre :</strong><br/><?php echo translate_date_to_fr($oDetailAnnonce->annonce_date_fin) ; ?>
        <?php } ?>
        </td>
      </tr>
    </table>
    
    </td>
  </tr>
</table>

    
</div>
<?php } ?>
<div style="height:10px;">&nbsp;</div>

<?php if ($oDetailAnnonce->annonce_quantite=="" || $oDetailAnnonce->annonce_quantite==null || $oDetailAnnonce->annonce_quantite=="0" || intval($oDetailAnnonce->annonce_quantite)==0) {} else { ?>
<div style='
width:230px; 
background-color:#434343; 
background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/back_annonce_det_03.png);
background-repeat:no-repeat;
padding-left:60px;
color: #FFFFFF;
font-family: "Arial",sans-serif;
font-size: 12px;
line-height: 1.25em;
height:68px;
'>
    <table width="100%" border="0">
      <tr>
        <td colspan="2" height="56"><span style='color: #FFFFFF;
font-family: "Arial",sans-serif;
font-size: 12px;
font-weight: 700;
line-height: 1.25em;'>Limite produit </span></td>
      
        <td width="70"><span style='color: #FFFFFF;
font-family: "Arial",sans-serif;
font-size: 16px;
font-weight: 700;
line-height: 1.25em;'><?php if ($oDetailAnnonce->annonce_quantite=="" || $oDetailAnnonce->annonce_quantite==null) echo 'Reste épuisé'; else echo 'Reste à ce jour'; ?></span></td>
        <td width="70"><span style='color: #434343;
font-family: "Arial",sans-serif;
font-size: 32px;
font-weight: 700;
line-height: 1.19em;'>&nbsp;<?php if ($oDetailAnnonce->annonce_quantite!="") echo $oDetailAnnonce->annonce_quantite ; else echo '00'; ?></span></td>
      </tr>
    </table>
</div>
<?php } ?>

<div style="height:10px;">&nbsp;</div>

<?php if ($oDetailAnnonce->ancien_prix=='0' || $oDetailAnnonce->ancien_prix=="" || $oDetailAnnonce->ancien_prix==null) {} else {?>
<div style='
width:100%; 
background-color:#434343; 
background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages2013/wpdf12193d_06_fond.png);
background-repeat:no-repeat;
color: #FFFFFF;
font-family: "Arial",sans-serif;
font-size: 12px;
line-height: 1.25em;
height:67px; padding-top:3px;
'>

<table width="50%" border="0" cellspacing="0" cellpadding="0" style="text-align: center; margin-left: 10%;">
  <tr>
    <td width="60"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/euro_img.png"></td>
    <td>
    <table width="100%" border="0">
      <tr>
        <td valign="middle">
        <?php if ($oDetailAnnonce->ancien_prix=='0' || $oDetailAnnonce->ancien_prix=="" || $oDetailAnnonce->ancien_prix==null) {} else {?>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><span style='color: #FFFFFF;
        font-family: "Arial",sans-serif;
        font-size: 13px;
        font-weight: 700;
        line-height: 1.23em;'>Prix<br/>de vente</span></td>
            <td><span style='color: #FFFFFF;
        font-family: "Arial",sans-serif;
        font-size: 30px;
        font-weight: 700;
        line-height: 1.23em;'><?php echo $oDetailAnnonce->ancien_prix ; ?> €</span></td>
          </tr>
        </table>
        
        <?php } ?>

        <?php if ($oDetailAnnonce->prix_vente=='0' || $oDetailAnnonce->prix_vente=="" || $oDetailAnnonce->prix_vente==null) {} else {?>
        <p style="margin:0px; padding:0px;"><span style='color: #FFFFFF;
font-family: "Arial",sans-serif;
font-size: 13px;
line-height: 1.23em;'>Ancien tarif : <?php echo $oDetailAnnonce->prix_vente ; ?>€</span></p>

<?php } ?>
        </td>
      </tr>
    </table>
    </td>
  </tr>
</table>

    
</div>
<?php } ?>

<div style="height:10px;">&nbsp;</div>



<div style="text-align:center;">
<img border="0" alt="" src="<?php echo GetImagePath("front/"); ?>/wpimages2013/img_plus_infos_annonce.png">
</div>


<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>
<script type="application/javascript">
$(document).ready(function() {
        $("#btn_submit_form_module_detailannnonce").click(function(){
			//alert('test form submit');
			txtErrorform = "";
			
            var txtError_text_mail_form_module_detailannnonce = "";
            var text_mail_form_module_detailannnonce = $("#text_mail_form_module_detailannnonce").val();
            if(text_mail_form_module_detailannnonce=="") {
                //$("#divErrorform_module_detailannnonce").html('<font color="#FF0000">Veuillez saisir votre demande</font>');
                txtErrorform += "1";
				$("#text_mail_form_module_detailannnonce").css('border-color', 'red');
				$("#text_mail_form_module_detailannnonce").focus();
            } else {
				$("#text_mail_form_module_detailannnonce").css('border-color', '#E3E1E2');
			}
			
			
			var nom_mail_form_module_detailannnonce = $("#nom_mail_form_module_detailannnonce").val();
            if(nom_mail_form_module_detailannnonce=="") {
                txtErrorform += "- Veuillez indiquer Votre nom_mail_form_module_detailannnonce <br/>"; 
				$("#nom_mail_form_module_detailannnonce").css('border-color', 'red');
				$("#nom_mail_form_module_detailannnonce").focus();
            } else {
				$("#nom_mail_form_module_detailannnonce").css('border-color', '#E3E1E2');
			}
            
            var email_mail_form_module_detailannnonce = $("#email_mail_form_module_detailannnonce").val();
            if(email_mail_form_module_detailannnonce=="" || !isEmail(email_mail_form_module_detailannnonce)) {
                txtErrorform += "- Veuillez indiquer Votre email_mail_form_module_detailannnonce <br/>"; 
                //alert("Veuillez indiquer Votre nom");
                $("#email_mail_form_module_detailannnonce").css('border-color', 'red');
				$("#email_mail_form_module_detailannnonce").focus();
            } else {
				$("#email_mail_form_module_detailannnonce").css('border-color', '#E3E1E2');
			}
			
            if(txtErrorform == "") {
                $("#form_module_detailannnonce").submit();
            }
        })
		
		
    });
</script>
<?php if(isset($user_ion_auth)) {?>
<style type="text/css">
.inputhidder {
	visibility:hidden;
}
</style>
<?php }?>


<div>
<table border="0" align="center" style="text-align:center;">
  <tr>
    <td>
    <form method="post" name="form_module_detailannnonce" id="form_module_detailannnonce" action="" enctype="multipart/form-data">
    
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="inputhidder">
    <td>Votre Nom</td>
    <td><input id="nom_mail_form_module_detailannnonce" name="nom_mail_form_module_detailannnonce" type="text" class="form-control" value="<?php if(isset($user_ion_auth) && $user_ion_auth->first_name!="") echo $user_ion_auth->first_name;?>"></td>
  </tr>
  <tr class="inputhidder">
    <td>Votre Téléphone</td>
    <td><input id="tel_mail_form_module_detailannnonce" name="tel_mail_form_module_detailannnonce" type="text" class="form-control" value="<?php if(isset($user_ion_auth) && $user_ion_auth->phone!="") echo $user_ion_auth->phone;?>"></td>
  </tr>
  <tr class="inputhidder">
    <td>Votre Email</td>
    <td><input id="email_mail_form_module_detailannnonce" name="email_mail_form_module_detailannnonce" type="text" class="form-control" value="<?php if(isset($user_ion_auth) && $user_ion_auth->email!="") echo $user_ion_auth->email;?>"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
    <textarea id="text_mail_form_module_detailannnonce" style="height:130px;" cols="28" rows="7" class="form-control" name="text_mail_form_module_detailannnonce"></textarea>
    </td>
  </tr>
  <tr>
    <td colspan="2">
    <input id="btn_reset_form_module_detailannnonce" type="reset" value="Effacer" class="btn btn-primary">
    <input id="btn_submit_form_module_detailannnonce" type="button" name="btn_submit_form_module_detailannnonce" value="Envoyer" class="btn btn-success">
    </td>
  </tr>
</table>    
    
    
    </form>
    </td>
  </tr>
  <tr>
  <td><div id="divErrorform_module_detailannnonce"><?php if (isset($mssg_envoi_module_detail_annonce)) echo $mssg_envoi_module_detail_annonce;?></div></td>
  </tr>
  <tr>
  <td><span style='font-family: "Arial",sans-serif;
    font-size: 12px;
    font-weight: 700; padding:15px;
    line-height: 1.25em;'>Vous avez une question particulière à nous poser, adressez nous un mail express</span></td>
  </tr>
</table>
</div>



<div style="height:10px;">&nbsp;</div>











</div>


<?php $this->load->view("front2013/includes/mobile_footer_page_partner", $data); ?>