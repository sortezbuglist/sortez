<?php if(count ($toBonPlan)>0) { 
  		        foreach($toBonPlan as $oBonPlan){ ?>
        <?php 
		$utilise  = false;
		$bonplan_valide = 0;
		if (isset($id_client) ) { 
		   $oAssocClientBonPlan = $this->mdlcadeau->getClientAssocBonPlan($id_client,$oBonPlan->bonplan_id); 
		   $bon_plan_utilise_plusieurs = 0;						   
			if(isset($oAssocClientBonPlan) && $oAssocClientBonPlan!=null) {
				//print_r($oAssocClientBonPlan);
				$utilise = $oAssocClientBonPlan->utilise;
				$bonplan_valide = $oAssocClientBonPlan->valide;
				if($oBonPlan->bon_plan_utilise_plusieurs == 1){
				 $bon_plan_utilise_plusieurs = 1;
				}
				
			 } 
		}?>
        <?php } }?>


<table width="547" border="0">
  <tr>
    <td width="347" valign="top">
    <div style='
    width:265px; 
    height: 130px;
    padding: 60px 30px 20px 40px;
    background-image:url(<?php echo GetImagePath("front/"); ?>/btn_new/bg_notre_bon_plan.png);
    background-repeat:no-repeat;
    color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 16px;
    font-weight: 700;
    line-height: 1.25em;
    text-align:center;
    vertical-align:central;'>
        <table width="100%" border="0" height="91">
          <tr>
            <td>
            <?php if(count ($toBonPlan)>0) { 
  		        foreach($toBonPlan as $oBonPlan){ ?>
                <?php echo trim($oBonPlan->bonplan_titre); ?>
            <?php } }?>
            </td>
          </tr>
        </table>
    </div>
    <div style="padding:15px;">
        <?php if (isset($oBonPlan)) echo $oBonPlan->bonplan_texte; ?>
    </div>
    </td>
    <td width="200" valign="top">
    	<div style='
        width:140px; 
        background-color:#434343; 
        background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages2013/wpf43833ae_06.png);
        padding-left:60px;
        color: #FFFFFF;
        font-family: "Arial",sans-serif;
        font-size: 12px;
        line-height: 1.25em;
        height:80px;
        '>
            <table width="100%" border="0" height="80">
              <tr>
                <td>
                <strong>Début de l’offre :</strong><br/><?php echo translate_date_to_fr($oBonPlan->bonplan_date_debut); ?><br/>
                <strong>Fin de l’offre :</strong><br/><?php echo translate_date_to_fr($oBonPlan->bonplan_date_fin); ?><br/>
                </td>
              </tr>
            </table>
        </div>
        <!--<div style="height:5px;">&nbsp;</div>
        <div style='
        background-color:#434343; 
        background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages2013/wp895b1ceb_06.png);
        color: #434343;
        font-family: "Arial",sans-serif;
        font-size: 21px;
        font-weight: 700;
        line-height: 1.19em;
        height: 35px;
        line-height: 1.25em;
        padding: 10px 15px 35px 60px;
        width: 125px;
        '>
            <table width="100%" border="0" cellpadding="5" cellspacing="5">
              <tr>
                <td>10</td>
                <td>20</td>
                <td>20</td>
              </tr>
            </table>
        </div>-->
        <div style="height:5px;">&nbsp;</div>
        <?php if (isset($oBonPlan->bonplan_quantite) && $oBonPlan->bonplan_quantite!=""  && $oBonPlan->bonplan_quantite!="0") { ?>
        <div style='
        width:140px; 
        background-color:#434343; 
        background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages2013/wp485899b8_06.png);
        padding-left:60px;
        color: #FFFFFF;
        font-family: "Arial",sans-serif;
        font-size: 12px;
        line-height: 1.25em;
        height:113px;
        '>
            <table width="100%" border="0">
              <tr>
                <td colspan="2" height="36">
    
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <span style='color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 12px;
    font-weight: 700;
    line-height: 1.25em;'>Offre départ &nbsp;</span>
    <span style='color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 20px;
    font-weight: 700;
    line-height: 1.25em;'><?php if (isset($oBonPlan->bonplan_quantite_depart) && $oBonPlan->bonplan_quantite_depart!="") echo $oBonPlan->bonplan_quantite_depart;?></span>
    </td>
  </tr>
</table>

    
    </td>
              </tr>
              <tr>
                <td width="70"><span style='color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 16px;
    font-weight: 700;
    line-height: 1.25em;'>Reste à ce jour</span></td>
                <td width="70"><div style='color: #434343;
    font-family: "Arial",sans-serif;
    font-size: 25px;
    font-weight: 700; width:60px; height:70px;
    line-height: 1.19em;'>
    <table border="0" cellspacing="0" cellpadding="0" style="text-align:center;" height="70" width="60">
      <tr>
        <td valign="middle"><?php if (isset($oBonPlan->bonplan_quantite) && $oBonPlan->bonplan_quantite!="") echo $oBonPlan->bonplan_quantite;?></td>
      </tr>
    </table>
    </div>
    </td>
              </tr>
            </table>
        </div>
        <div style="height:5px;">&nbsp;</div>
        <?php } ?>
        <div style='
        width:140px; 
        background-color:#434343; 
        background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages2013/wp3622cdd4_06_bonplan.png);
        background-repeat:no-repeat;
        padding-left:60px;
        color: #FFFFFF;
        font-family: "Arial",sans-serif;
        font-size: 12px;
        line-height: 1.25em;
        height:43px; padding-top:7px;
        '>
            <table width="100%" border="0">
              <tr>
                <td valign="middle">
                <?php if  ($oBonPlan->bon_plan_utilise_plusieurs == 0){ ?>
                Offre valable<br/>une seule fois
                <?php } else { ?>
                Offre valable<br/>plusieurs fois
                <?php } ?>
                </td>
              </tr>
            </table>
        </div>
    </td>
  </tr>
</table>

<?php
$thisss =& get_instance();
$thisss->load->library('ion_auth');
$this->load->model("ion_auth_used_by_club");
?>


<table border="0" cellspacing="0" cellpadding="0" style="color:#F00; height:161px; width:100%; margin-top:-80px; background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages2013/btn_valid_bp_modif.png); background-repeat:no-repeat; background-position:center;">
  <tr>
    <td>
    
    <?php if  ($oBonPlan->bonplan_condition_vente == '2' && ($oBonPlan->bonplan_quantite != null && $oBonPlan->bonplan_quantite != "0")){ ?>
   
    
    <center>
    <p style="margin:0px; padding:0px; height:50px;">&nbsp;&nbsp;</p>
    <?php
     
     
            if ($thisss->ion_auth->logged_in() && $thisss->ion_auth->in_group(2)) {
                $user_ion_auth = $thisss->ion_auth->user()->row();
                $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
            ?>
            Bonjour <?php echo $user_ion_auth->first_name . " " . $user_ion_auth->last_name; ?><br />
            <?php 
            if ($mssg != "bonplan_valide") {
            if(($utilise ==1 && $bon_plan_utilise_plusieurs == 0) || ($bonplan_valide ==1 && $bon_plan_utilise_plusieurs == 0)) { 
                    echo "Vous avez d&eacute;ja valid&eacute; ce BonPlan<br/>" ;
                } else { 
                    echo  "Vous pouvez maintenant valider ce BonPlan<br/>"; 
                } 
            }
            ?>
            <?php } else { ?>
                
            <?php } ?>
            
            
            
             <?php if (isset($id_client) && isset($mssg) && $mssg == "bonplan_valide") { ?>
                <?php if ($utilise ==1 && $bon_plan_utilise_plusieurs == 0) { ?>
                    <span style="color:#F00; font-size:10px; text-align:center">Ce bon plan n'a pu &ecirc;tre valid&eacute;, vous l'avez d&eacute;j&agrave; utilis&eacute; !<br/></span>
                <?php } else if ($mssg!="0") { ?>
                    <span style="color:#F00; font-size:10px; text-align:center">Ce bon plan a &eacute;t&eacute; valid&eacute;, vous allez recevoir sous peu un email de confirmation !<br/></span>
                <?php } ?>
              <?php } ?>
              
              
             
              <?php if(count ($toBonPlan)>0) { 
                    foreach($toBonPlan as $oBonPlan){ ?>
            <a href="
            <?php if(($utilise ==1 && $bon_plan_utilise_plusieurs == 0) || ($bonplan_valide ==1 && $bon_plan_utilise_plusieurs == 0)) {
                    echo "Javascript:void();"; 
                } else  { 
                    echo site_url("front/fidelisation/demandeBonPlan/".$oBonPlan->bonplan_id);
                } ?>"
            id="btn_120" class="Button1" style="width: 180px; height: 36px; text-decoration:none;">
            <span>
            <?php if(($utilise ==1 && $bon_plan_utilise_plusieurs == 0)  || ($bonplan_valide ==1 && $bon_plan_utilise_plusieurs == 0)) { 
                    echo '<input type="button" id="" value="Bon Plan d&eacute;j&agrave; valid&eacute;"/>' ;
                } else { 
                    echo  '<input type="button" id="" value="Réservation immédiate en ligne"/>'; 
                } ?>
            </span>
            </a>	
            <?php }
            }?>	
    
    
    <?php if ($thisss->ion_auth->logged_in() && $thisss->ion_auth->in_group(2)) { ?>         
    <?php } else if ($thisss->ion_auth->logged_in()) echo '<br/><font color="#FF0000">Votre compte ne vous permet pas de valider un BonPlan !</font>';?>
    </center>
   
    
    <?php } ?>
    </td>
    <td style="text-align:right;" width="275"><!--<img src="<?php //echo GetImagePath("front/"); ?>/wpimages2013/wp36ea626d_06.png" alt="img">--></td>
  </tr>
</table>





<!--<p>&nbsp;</p>-->
<center>
<table width="537" border="0" style='background-color:#FCC73C; text-align:center; font-family: "Arial",sans-serif;
    font-size: 11px;
    font-weight: 700;
    line-height: 1.27em;' align="center">
  <tr>
    <td><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpf9a6272f_06.png" alt="img"></td>
    <td><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpb363baed_06.png" alt="img"></td>
    <td><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp6fd2f053_06.png" alt="img"></td>
  </tr>
  <tr>
    <td valign="top">
    Avant de pouvoir valider<br/>cette réservation,<br/>le consommateur doit être<br/>inscrit au Club Proximité.
    <?php if (!$thisss->ion_auth->logged_in()) { ?>
    <p>
        <a href="<?php echo site_url("front/particuliers/inscription"); ?>" style="text-decoration:none;"><input id="butn_18" type="button" value="Je m'inscris" style="width:110px; height:22px;"></a>
    </p>
    <?php } ?>
    </td>
    <td valign="top">
    Après avoir validé l'offre,<br/>vous recevrez par retour un<br/>email vous confirmant cette<br/>réservation et sa durée de<br/>mise à disposition.<br/>Parallèlement le partenaire<br/>reçoit un double avec vos<br/>coordonnées.
    </td>
    <td valign="top">
	Pour bénéficier de cette offre,<br/>il vous suffit de nous<br/>présenter lors de votre<br/>déplacement     le mail imprimé<br/>ou présent sur  votre<br/>smartphone.
    </td>
  </tr>
  <tr>
    <td>
    
    </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</center>




<center>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>
<script type="application/javascript">
$(document).ready(function() {
        $("#btn_submit_form_module_detailbonnplan").click(function(){
			//alert('test form submit');
			txtErrorform = "";
			
            var txtError_text_mail_form_module_detailbonnplan = "";
            var text_mail_form_module_detailbonnplan = $("#text_mail_form_module_detailbonnplan").val();
            if(text_mail_form_module_detailbonnplan=="") {
                //$("#divErrorform_module_detailbonnplan").html('<font color="#FF0000">Veuillez saisir votre demande</font>');
                txtErrorform += "1";
				$("#text_mail_form_module_detailbonnplan").css('border-color', 'red');
				$("#text_mail_form_module_detailbonnplan").focus();
            } else {
				$("#text_mail_form_module_detailbonnplan").css('border-color', '#E3E1E2');
			}
			
			var nom_mail_form_module_detailbonnplan = $("#nom_mail_form_module_detailbonnplan").val();
            if(nom_mail_form_module_detailbonnplan=="") {
                txtErrorform += "- Veuillez indiquer Votre nom_mail_form_module_detailbonnplan <br/>"; 
				$("#nom_mail_form_module_detailbonnplan").css('border-color', 'red');
				$("#nom_mail_form_module_detailbonnplan").focus();
            } else {
				$("#nom_mail_form_module_detailbonnplan").css('border-color', '#E3E1E2');
			}
            
            var email_mail_form_module_detailbonnplan = $("#email_mail_form_module_detailbonnplan").val();
            if(email_mail_form_module_detailbonnplan=="" || !isEmail(email_mail_form_module_detailbonnplan)) {
                txtErrorform += "- Veuillez indiquer Votre email_mail_form_module_detailbonnplan <br/>"; 
                //alert("Veuillez indiquer Votre nom");
                $("#email_mail_form_module_detailbonnplan").css('border-color', 'red');
				$("#email_mail_form_module_detailbonnplan").focus();
            } else {
				$("#email_mail_form_module_detailbonnplan").css('border-color', '#E3E1E2');
			}
			
			
			
            if(txtErrorform == "") {
                $("#form_module_detailbonnplan").submit();
            }
        });
		
		
		
    });
</script>
<?php if(isset($user_ion_auth)) {?>
<style type="text/css">
.inputhidder {
	visibility:hidden;
}
</style>
<?php }?>

<table width="540" border="0" align="center" style="text-align:center;">
  <tr>
    <td style="text-align:left;"><img src="<?php echo GetImagePath("front/"); ?>/btn_new/info_annonce_img.png" alt="img" width="269"></td>
    <td>
<form method="post" name="form_module_detailbonnplan" id="form_module_detailbonnplan" action="" enctype="multipart/form-data">    
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="inputhidder">
    <td>Votre Nom</td>
    <td><input id="nom_mail_form_module_detailbonnplan" name="nom_mail_form_module_detailbonnplan" type="text" value="<?php if(isset($user_ion_auth) && $user_ion_auth->first_name!="") echo $user_ion_auth->first_name;?>"></td>
  </tr>
  <tr class="inputhidder">
    <td>Votre Téléphone</td>
    <td><input id="tel_mail_form_module_detailbonnplan" name="tel_mail_form_module_detailbonnplan" type="text" value="<?php if(isset($user_ion_auth) && $user_ion_auth->phone!="") echo $user_ion_auth->phone;?>"></td>
  </tr>
  <tr class="inputhidder">
    <td>Votre Email</td>
    <td><input id="email_mail_form_module_detailbonnplan" name="email_mail_form_module_detailbonnplan" type="text" value="<?php if(isset($user_ion_auth) && $user_ion_auth->email!="") echo $user_ion_auth->email;?>"></td>
  </tr>
  <tr>
    <td colspan="2">
    <textarea id="text_mail_form_module_detailbonnplan" cols="28" rows="7" name="text_mail_form_module_detailbonnplan" style="width:246px; height:130px;font-family:Arial, Helvetica, sans-serif; font-size:10px;"></textarea>
    </td>
  </tr>
  <tr>
    <td colspan="2">
    <input id="btn_reset_form_module_detailbonnplan" type="reset" value="Effacer" style="width:82px; height:22px;">
<input id="btn_submit_form_module_detailbonnplan" type="button" name="btn_submit_form_module_detailbonnplan" value="Envoyer" style="width:90px; height:22px;">
    </td>
  </tr>
</table>
</form>
    </td>
  </tr>
  <tr>
  <td>&nbsp;</td>
  <td><div id="divErrorform_module_detailbonnplan"><?php if (isset($mssg_envoi_module_detail_bonplan)) echo $mssg_envoi_module_detail_bonplan;?></div></td>
  </tr>
</table>
<br/>
<span style='font-family: "Arial",sans-serif;
    font-size: 12px;
    font-weight: 700;
    line-height: 1.25em;'>Vous avez une question particulière à nous poser, adressez nous un mail express</span>
</center>
