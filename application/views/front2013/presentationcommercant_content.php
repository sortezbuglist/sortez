<?php
$data['empty'] = null;
$thisss =& get_instance();
$thisss->load->library('ion_auth');
$this->load->model("ion_auth_used_by_club");
$user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);
if (isset($user_ion_auth_id)) $user_groups = $thisss->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;
?>
<style type="text/css">
    <!--
    #main_data_body {
        text-align: justify;
    }
    -->
</style>
<div class="row">
    <div class="col-lg-12">
        <!--<div id="main_data_head"><strong>Présentation</strong></div>-->
        <p class="text-center pt-5" style="color:darkblue;font-size: 20px;font-weight: bold"><?php echo $oInfoCommercant->subdomain_title; ?></p>
        <br/>
        <!--<p class="Corps-P"><span class="Corps-C">D&eacute;tail de l'activit&eacute;</span></p>-->
        <p class="caract Corps-P-P0 p-5"><span class="Corps-C-C0"><?php echo $oInfoCommercant->Caracteristiques ; ?></span></p>
    </div>
    <div class="col-lg-12 padding0" style="background-color:#FFFFFF;">


        <?php if (isset($pagecategory_partner) && $pagecategory_partner == 'annonces_partner') { ?>
        <?php } else if (isset($pagecategory_partner) && $pagecategory_partner == 'details_agenda') { ?>
        <?php } else if (isset($pagecategory_partner) && $pagecategory_partner == 'list_agenda') { ?>
        <?php } else if (isset($pagecategory_partner) && $pagecategory_partner == 'details_annonces_partner') { ?>
            <div class="contenerslidemain2013">
                <?php $this->load->view("privicarte/includes/slide_annonces_partner", $data); ?>
            </div>
        <?php } else if (isset($pagecategory_partner) && $pagecategory_partner == 'bonplans_partner') { ?>
            <div class="contenerslidemain2013">
                <?php $this->load->view("privicarte/includes/slide_bonplans_partner", $data); ?>
            </div>
        <?php } else { ?>
            <div class="contenerslidemain2013">
                <?php $this->load->view("privicarte/includes/slide_partner", $data); ?>
            </div>
        <?php } ?>
    </div>
    <?php
    if (isset($oInfoCommercant->id_glissiere) AND $oInfoCommercant->id_glissiere !=null ){
        $thi=get_instance();
        $glisieres= $thi->load->mdlcommercant->get_glissiere_by_id_com($oInfoCommercant->IdCommercant);
    }

    ?>
</div>
<div class="row">
    <?php if (isset($glisieres->presentation_1_titre) AND $glisieres->presentation_1_titre !=null ){?>
        <div onclick="glissiere1()" class="mt-5 col-lg-11 mr-5 ml-5 text-center titre_glissiere1"><?php echo $glisieres->presentation_1_titre; ?><img class="float-right img-fluid" src="<?php echo base_url('assets/img/wpc59cfc3d_06.png')?>"></div>
        <div id="g1content" class="p-inherit col-lg-11 mr-5 ml-5" style="display: none">
            <?php echo $glisieres->presentation_1_contenu ??''; ?>
        </div>
    <?php } ?>

    <?php if (isset($glisieres->presentation_2_titre) AND $glisieres->presentation_2_titre !=null ){ ?>
        <div onclick="glissiere2()" class="mt-5 col-lg-11 mr-5 ml-5  text-center titre_glissiere1"><?php echo $glisieres->presentation_2_titre; ?><img class="float-right img-fluid" src="<?php echo base_url('assets/img/wpc59cfc3d_06.png')?>"></div>
        <div id="g2content" class="p-inherit col-lg-11 mr-5 ml-5" style="display: none">
            <?php echo $glisieres->presentation_2_contenu ??''; ?>
        </div>
    <?php } ?>

    <?php if (isset($glisieres->presentation_3_titre) AND $glisieres->presentation_3_titre !=null ){ ?>
        <div onclick="glissiere3()" class="mt-5 col-lg-11 mr-5 ml-5  text-center titre_glissiere1"><?php echo $glisieres->presentation_3_titre; ?><img class="float-right img-fluid" src="<?php echo base_url('assets/img/wpc59cfc3d_06.png')?>"></div>
        <div id="g3content" class="p-inherit col-lg-11 mr-5 ml-5" style="display: none">
            <?php echo $glisieres->presentation_3_contenu ??''; ?>
        </div>
    <?php } ?>

    <?php if (isset($glisieres->presentation_4_titre) AND $glisieres->presentation_4_titre !=null ){ ?>
        <div onclick="glissiere4()" class="mt-5 col-lg-11 mr-5 ml-5  text-center titre_glissiere1"><?php echo $glisieres->presentation_4_titre; ?><img class="float-right img-fluid" src="<?php echo base_url('assets/img/wpc59cfc3d_06.png')?>"></div>
        <div id="g4content" class="p-inherit col-lg-11 mr-5 ml-5" style="display: none">
            <?php echo $glisieres->presentation_4_contenu ??''; ?>
        </div>
    <?php } ?>
</div>
<script type="text/javascript">
    function glissiere1() {
        if(document.getElementById("g1content").style.display==='none'){
            document.getElementById("g1content").style.display="block";
            document.getElementById("g1img").style.display="block";
        }else if (document.getElementById("g1content").style.display==='block') {
            document.getElementById("g1content").style.display="none";
            document.getElementById("g1img").style.display="none";
        }
    }
    function glissiere2() {
        if(document.getElementById("g2content").style.display==='none'){
            document.getElementById("g2content").style.display="block";
            document.getElementById("g2img").style.display="block";
        }else if (document.getElementById("g2content").style.display==='block') {
            document.getElementById("g2content").style.display="none";
            document.getElementById("g2img").style.display="none";
        }
    }
    function glissiere3() {
        if(document.getElementById("g3content").style.display==='none'){
            document.getElementById("g3content").style.display="block";
            document.getElementById("g3img").style.display="block";
        }else if (document.getElementById("g3content").style.display==='block') {
            document.getElementById("g3content").style.display="none";
            document.getElementById("g3img").style.display="none";
        }
    }
    function glissiere4() {
        if(document.getElementById("g4content").style.display==='none'){
            document.getElementById("g4content").style.display="block";
            document.getElementById("g4img").style.display="block";
        }else if (document.getElementById("g4content").style.display==='block') {
            document.getElementById("g4content").style.display="none";
            document.getElementById("g4img").style.display="none";
        }
    }
</script>
<style type="text/css">
    .titre_glissiere1{
        font-size: 20px;
        color: white;
        font-weight: bold;
        background-color: black;
        height: 50px;
        padding-top: 8px;
    }
    #g1content img{
        width: 100%!important;
        height: auto!important;
    }
    #g2content img{
        width: 100%!important;
        height: auto!important;
    }
    #g3content img{
        width: 100%!important;
        height: auto!important;
    }
    #g4content img{
        width: 100%!important;
        height: auto!important;
    }
    .p-inherit{
        padding: inherit;
    }
</style>