<script type="text/javascript">
 
$(function(){
     
    $("#inputStringVilleHidden_bonplans").change(function(){
            
            var inputStringVilleHidden_bonplans = $("#inputStringVilleHidden_bonplans").val();
           
            check_show_categ_bonplan(inputStringVilleHidden_bonplans);
            //alert(inputStringVilleHidden_bonplans);

            $("#span_leftcontener2013_form_bonplans input:checkbox").attr('checked', false);
            $("#frmRechercheBonplan").submit();
        });

    $("#inputString_zMotCle_submit").click(function(){

        var inputString_zMotCle = $("#inputString_zMotCle").val();
        $("#zMotCle").val(inputString_zMotCle);

        $("#span_leftcontener2013_form_bonplans input:checkbox").attr('checked', false);
        $("#frmRechercheBonplan").submit();
    });


    $("#inputStringOrderByHidden_partenaires").change(function(){

        var inputStringVilleHidden_bonplans = $("#inputStringVilleHidden_bonplans").val();
        var inputStringOrderByHidden_partenaires = $("#inputStringOrderByHidden_partenaires").val();

        $("#inputStringOrderByHidden").val(inputStringOrderByHidden_partenaires);

        //check_show_categ_bonplan(inputStringVilleHidden_bonplans);
        //alert(inputStringVilleHidden_bonplans);

        //$("#span_leftcontener2013_form_bonplans input:checkbox").attr('checked', false);
        $("#frmRechercheBonplan").submit();
    });
   
});


function show_current_categ_subcateg(IdRubrique){

    $(".leftcontener2013content").hide();
    var id_to_show = "#leftcontener2013content_"+IdRubrique;
    $(id_to_show).show();

}


function check_show_categ_bonplan(inputStringVilleHidden_bonplans){
    
    var base_url_visurba = '<?php echo site_url();?>';
    
    $("#span_leftcontener2013_form_bonplans").html('<center><img src="'+base_url_visurba+'application/resources/front/images/wait.gif" alt="loading...."/></center>');
            
                $.post(
                     base_url_visurba+'front/bonplan/check_category_list/<?php if (isset($_GET['localdata'])) echo "?localdata=".$_GET['localdata'];?>',
                     {
                        inputStringVilleHidden_bonplans: inputStringVilleHidden_bonplans 
                     }
                     ,
                     function (zReponse)
                     {
                             //alert ("reponse <br/><br/>" + zReponse) ;
                             if (zReponse == "error") { //an other home page exist yet
                                     alert("Un probléme est suvenu, veuillez refaire l'opération !");
                             } else { 
                                     $("#span_leftcontener2013_form_bonplans").html(zReponse);
                                     
                                        //var O_Dest_xxx = $('.leftcontener2013').height();
                                        var O_Doc_xxxx = $('.maincontener2013').height();
                                        //if (O_Doc_xxx > O_Dest_xxx) {
                                                $('.leftcontener2013').height(O_Doc_xxxx);
                                        //}
                                        //alert(O_Doc_xxxx);


                                         $(".leftcontener2013content").hide();

                                         $.post(
                                             base_url_visurba+'accueil/check_Idcategory_of_subCategory/'
                                             ,
                                             function (zReponse)
                                             {
                                                 var article_value = zReponse.split("-");
                                                 for(i = 0; i < article_value.length; i++) {
                                                     var id_to_show = "#leftcontener2013content_"+article_value[i];
                                                     $(id_to_show).show();
                                                 }
                                             }
                                         );

                                     
                             } 
                });
}


function redirect_bonplan(){
    var base_url_visurba = '<?php echo site_url();?>';
    
    document.location.href = base_url_visurba+'front/bonplan/redirect_bonplan/';
}


function submit_search_bonplan(){
    
    //setTimeout(alert('test'),5000);
    var allvalue = '';
    $("#span_leftcontener2013_form_bonplans input:checkbox:checked").each(function(){
        allvalue += ','+$(this).val();
    });
    //alert(allvalue);
    $("#inputStringHidden").val(allvalue);
    $("#frmRechercheBonplan").submit();
}

  
</script>