<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("front2013/includes/header_mini_2", $data); ?>



<script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/jspngfix.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>

<script type="text/javascript">var blankSrc = "wpscripts/blank.gif";
</script>

<script type="text/javascript">
	
		 
		 
		 // Use jQuery via $(...)
		 $(document).ready(function(){//debut ready fonction
			   
		//verify Nom	   
		$("#nomcontact").focus(function(){
									$(this).val("");			 
								});
		$("#nomcontact").blur(function(){
									if ($(this).val() == "") {
										$(this).val("Votre nom*");
									}
								});	
		
		//verify Prénom	   
		$("#prenomcontact").focus(function(){
									$(this).val("");			 
								});
		$("#prenomcontact").blur(function(){
									if ($(this).val() == "") {
										$(this).val("Votre prénom*");
									}
								});	
		//verify mail	   
		$("#emailcontact").focus(function(){
									$(this).val("");			 
								});
		$("#emailcontact").blur(function(){
									if ($(this).val() == "") {
										$(this).val("Votre courriel*");
									}
								});	
		//verify tel	   
		$("#telcontact").focus(function(){
									$(this).val("");			 
								});
		$("#telcontact").blur(function(){
									if ($(this).val() == "") {
										$(this).val("Votre numéro de téléphone*");
									}
								});	
		//verify mess	   
		$("#contenucontact").focus(function(){
									$(this).val("");			 
								});
		$("#contenucontact").blur(function(){
									if ($(this).val() == "") {
										$(this).val("Votre message*");
									}
								});	
			   
			   
		//verify field form value 
		$("#btnEnvoicontact").click(function(){
										  
				var txtError = "";
				
				var nomcontact = $('#nomcontact').val();
				  if (nomcontact == "" || nomcontact == "Votre nom*") {
					txtError += "- Vous devez préciser votre Nom<br/>";
				  }  
				
				var prenomcontact = $('#prenomcontact').val();
				  if (prenomcontact == "" || prenomcontact == "Votre prénom*") {
					txtError += "- Vous devez préciser votre prénom<br/>";
				  } 
				
				var emailcontact = $('#emailcontact').val();
				  if (emailcontact == "" || emailcontact == "Votre courriel*") {
					txtError += "- Vous devez préciser votre email<br/>";
				  }
				  if(!isEmail(emailcontact)) {
					txtError += "- Veuillez entrer un email valide.<br/>";
				  }
				  
				var telcontact = $('#telcontact').val();
				  if (telcontact == "" || telcontact == "Votre numéro de téléphone*") {
					txtError += "- Vous devez préciser votre téléphone<br/>";
				  }
				  
				var contenucontact = $('#contenucontact').val();
				  if (contenucontact == "" || contenucontact == "Votre message*") {
					txtError += "- Vous devez préciser le contenu de votre message<br/>";
				  }
				
				if ($('#acceptenews0').is(":checked")) {
					$("#acceptenews").val("1");
				} else {
					$("#acceptenews").val("0");
				}
				
				
				//verify captcha
				/*var captcha = $('#captcha').val();
				  if (captcha == "") {
					txtError += "- Vous devez remplir le captcha<br/>";
				  } else {
					  $.ajax({
                                    type: "GET",
                                    url: "<?=base_url(); ?>front/professionnels/verify_captcha/"+captcha,
                                    success: function(msg){
                                        //alert(msg);
                                        var numero_reponse = msg;
                                        //alert(numero_reponse);
										$("#divCaptchavalueverify").val(numero_reponse);
                                    }
                             });
                             
				  }
				  
				 var divCaptchavalueverify = $('#divCaptchavalueverify').val();
				  if (divCaptchavalueverify == "0") {
					txtError += "- Les Textes que vous avez entré ne sont pas valides.<br/>";
				  } */
				// end verify captcha  
				  
				
				//final verification of input error
				if(txtError == "") {
					$("#formContact").submit();
				} else {
					$("#divErrorFrmContact").html(txtError);
				}
			})
		
		
		
		
		
    })
		 
		 
    </script>


<div style="margin-top:30px; margin-bottom:20px; text-align:center">
	<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/img_nous_contacter.png" alt="">
</div>


<div style="background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages2013/bg_page_conso_pro_souri.png); background-repeat:no-repeat; background-position:center bottom;">
<form id="formContact" name="formContact" action="<?php echo site_url('front/contact/envoyer/'); ?>" method="post">
  <input type="hidden" name="contact[acceptenews]" id="acceptenews" value=""/>
  <center>
<table width="500px" border="0">
  <tr>
    <td colspan="2">Merci de bien vouloir préciser vos coordonnées et votre demande<br/>
        <span style='font-family: "Verdana",sans-serif;
    font-size: 9px;
    line-height: 1.33em;'>* champs obligatoires</span></td>
  </tr>
  <tr>
    <td><input id="nomcontact" name="contact[nom]" tabindex="1" maxlength="50" value="Votre nom*" style="width: 232px;" type="text"></td>
    <td><input id="prenomcontact" name="contact[prenom]" tabindex="2" maxlength="50" value="Votre prénom*" style="width: 236px;" type="text"></td>
  </tr>
  <tr>
    <td><input id="emailcontact" name="contact[email]" tabindex="3" maxlength="100" value="Votre courriel*" style="width: 232px;" type="text"></td>
    <td><input id="telcontact" name="contact[tel]" maxlength="100" tabindex="4" value="Votre numéro de téléphone*" style="width: 236px;" type="text"></td>
  </tr>
  <tr>
    <td colspan="2"><textarea id="contenucontact" rows="8" cols="60" name="contact[contenu]" tabindex="5" style="width: 500px; height: 134px;">Votre message*</textarea></td>
  </tr>
  <tr>
    <td colspan="2">
      <div id="txt_47" style="width:472px;height:45px;overflow:hidden;">
        <p class="Corps-P"><input id="acceptenews0" name="acceptenews0" tabindex="6" type="checkbox"> <span class="Corps-C-C0">J’accepte de recevoir par courriel des informations provenant de cet établissement.</span></p>
    </div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
    <!--<div style="width:190px;height:69px;">
    
    
    
    <?php// echo $captcha['image']; ?><br />
    <input name="captcha" value="" id="captcha" type="text" tabindex="7">
    <input name="divCaptchavalueverify" id="divCaptchavalueverify" value="" type="hidden" />
    
    
    </div>-->
    </td>
  </tr>
  <tr>
    <td style="text-align:center;"><input style="width: 87px; height: 22px;" id="butn_2" value="Rétablir" type="reset" tabindex="9"></td>
    <td style="text-align:left;">
    <input style="width: 90px; height: 22px;" id="btnEnvoicontact" name="btnEnvoicontact" value="Envoyer" type="button" tabindex="8">
	
    </td>
  </tr>
  <tr>
    <td><div id="divErrorFrmContact" style="width:273px; height:126px; overflow:hidden; color:#F00; font-family:Arial, Helvetica, sans-serif; font-size:9px; margin-top:20px;"></div></td>
    <td>
    
    </td>
  </tr>
</table>
</center>

<script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/jsValidation.js"></script>
</form>
</div>





<!--<div id="txt_45" style="position:absolute; left:67px; top:598px; width:251px; height:40px; overflow:hidden;">
<span class="Corps-C">Merci de saisir exactement les caractères qui s’affichent dans l’image</span>
</div>-->
<div id="txt_49" style="text-align:center;">
SARL Phase<br/>
<strong>Adresse</strong> : 60 avenue de Nice 06800 Cagnes sur Mer<br/>
<strong>Directeur de publication</strong> : Jean Pierre Woignier<br/>
<strong>Téléphone direct</strong> : 04.93.73.84.51<br/>
<strong>Téléphone mobile</strong> : 06.72.05.59.35<br/>
<strong>Courrier</strong> : proximite-magazine@orange.fr<br/>
<strong>Statut</strong> : SARL au capital de 7265€<br/>
<strong>N° Registre de commerce </strong>: RCS Antibes 412.071.466<br/>
<strong>SIRET</strong> : 412071466 00012<br/>
<strong>CODE NAF</strong> : 7022Z<br/>
<strong>IBAN</strong> : FR76 3007 6023 5110 6081 0020 072<br/><br/><br/><br/><br/><br/>
</div>




<?php $this->load->view("front2013/includes/footer_mini_2"); ?>