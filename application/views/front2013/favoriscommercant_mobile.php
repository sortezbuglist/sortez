<?php $data["zTitle"] = 'Favoris' ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Clubproximite - Favoris</title>
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0; user-zoom=fixed;"/>
<style type="text/css">
.mobile_contener_all {
	width: 320px;
	margin-right: auto;
	margin-left: auto;
}
body {
	padding:0px;
	margin:0px;
	background-color:#003566;
	font-family: "Arial",sans-serif;
    font-size: 11px;
    line-height: 1.27em;
	color:#FFFFFF;
}.header_mobile {
	float: left;
	width: 320px;
	position: relative;
}
.main_mobile {
	float: left;
	width: 320px;
	position: relative;
	padding-top:5px;
	padding-bottom:10px;
}
.footer_mobile {
	float: left;
	width: 320px;
	position: relative;
}
</style>

</head>

<body>

<div class="mobile_contener_all">
  <div class="header_mobile">
  	<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/head_mobile_court_nv.png" alt="" border="0" width="320" height="68">
    <div style=" width:320px; text-align:center; z-index:1">
    <span style='color: #ffffff;
        font-family: "Vladimir Script",cursive;
        font-size: 30px;
        line-height: 47px;'>Mes favoris</span>
    </div>
  </div>
  
  <div style="text-align:center;padding-bottom:15px;"> 
  	<a href="javascript:history.back();"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/btn_back_favoris.png" alt="" border="0" width="141" height="52" style="padding-top:15px;"></a>
  </div>
  
  <div class="main_mobile">

<?php 
if (isset($user_groups) && $user_groups->id!=2) echo '<div style=" color:#FF0000; font-weight:bold; text-align:center; padding:10px;">Seuls les consommateurs ont une liste de partenaires favoris.</div>';
?>

<div style="height:2px; background-color:#FFF; margin-bottom:4px;"></div>

<?php if (count($toCommercant)==0) { ?>
<span style="text-align:center; font-weight:bold;">Liste vide</span>
<?php } ?>
  	
<?php  foreach($toCommercant as $oCommercant){ 

		$objasscommrubr = $this->sousRubrique->GetById($oCommercant->IdSousRubrique);
		
		$oInfoCommercant = $oCommercant;
		$base_path_system = str_replace('system/', '', BASEPATH);
		include($base_path_system.'application/views/front2013/includes/url_var_definition.php');
		//$this->load->view("front2013/includes/url_var_definition", $data); 
		?>
		
		
		<table width="100%" border="0">
              <tr>
                
                <td valign="top" width="120">
               	  <a href="<?php echo site_url($commercant_url_home);?>" style="color:#fff; text-decoration:none;">
                    <?php 
                    $image_home_vignette = "";
                    if (isset($oCommercant->PhotoAccueil) && $oCommercant->PhotoAccueil != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->PhotoAccueil)==true){ $image_home_vignette = $oCommercant->PhotoAccueil;}
                    else if (isset($oCommercant->Photo1) && $oCommercant->Photo1 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo1)==true){ $image_home_vignette = $oCommercant->Photo1;}
                    else if (isset($oCommercant->Photo2) && $oCommercant->Photo2 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo2)==true){ $image_home_vignette = $oCommercant->Photo2;}
                    else if (isset($oCommercant->Photo3) && $oCommercant->Photo3 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo3)==true){ $image_home_vignette = $oCommercant->Photo3;}
                    else if (isset($oCommercant->Photo4) && $oCommercant->Photo4 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo4)==true){ $image_home_vignette = $oCommercant->Photo4;}
                    else if (isset($oCommercant->Photo5) && $oCommercant->Photo5 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo5)==true){ $image_home_vignette = $oCommercant->Photo5;}
                    ?>
                    <img src="<?php if ($image_home_vignette != ""){ echo GetImagePath("front/photoCommercant/"); ?>/<?php echo $image_home_vignette ; }else{echo GetImagePath("front/")."/wp71b211d2_06.png";}?>" width="120" border="0" alt="<?php echo $commercant_url_nom;?>">
				  </a>                    
                </td>
                
                <td valign="top">

                
<table width="100%" border="0" cellspacing="0" cellpadding="4" style="padding-left:10px;"> 
  <tr>
    <td colspan="2"><strong>
    <?php //if ($objasscommrubr) echo $objasscommrubr->Nom ; <br />?>
	<?php echo $oCommercant->NomSociete ; ?><br /></strong>
    <?php echo $oCommercant->ville ; ?> <?php //echo $oCommercant->Adresse2 ; ?> <?php //echo $oCommercant->Adresse1 ; ?>
    </td>
  </tr>
  <tr>
    <td width="25">
    	<a href="<?php echo site_url("front/utilisateur/delete_favoris/".$oCommercant->IdCommercant);?>"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/btn_delete_favoriss.png" width="25" alt="partenaire" /></a>
    </td>
    <td>
    	<a href="<?php echo site_url("front/utilisateur/delete_favoris/".$oCommercant->IdCommercant);?>" style="text-decoration:none; color:#FFFFFF;">Supprimer<br/>de mes favoris</a></td>
  </tr>
</table>
                
                </td>
                <td valign="middle" width="22">
                	<a href="<?php echo site_url($commercant_url_home);?>" style="color:#fff; text-decoration:none;">
                    <img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/btn_fleche_favoris.png" width="22" alt="partenaire" />
                    </a>
                </td>
              </tr>
    </table>
        
    <div style="height:2px; background-color:#FFF;"></div>
		
		
		<?php
		
	}
?>
    
 
    
  </div>
  <div class="footer_mobile">
  	<?php 
	$data["zTitle"] = 'Identification';
	$this->load->view("front2013/includes/mobile_footer.php", $data);
	?>
  </div>
</div>
</body>
</html>
