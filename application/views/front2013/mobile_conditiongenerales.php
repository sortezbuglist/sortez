<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="fr"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>conditions-generales</title>
<meta name="Generator" content="Serif WebPlus X6">
<meta name="viewport" content="width=320">
<style type="text/css">
body{margin:0;padding:0;}
.Corps-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:center; font-weight:400;
}
.Normal2-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:justify; font-weight:400;
}
.Corps-C
{
    font-family:"Arial", sans-serif; color:#ffffff; font-size:12.0px; line-height:12.0px;
}
.Normal2-C
{
    font-family:"Arial", sans-serif; font-weight:700; color:#fcc73c; font-size:13.0px; line-height:1.23em;
}
.Normal2-C-C0
{
    font-family:"Arial", sans-serif; color:#ffffff; font-size:11.0px; line-height:1.27em;
}
.Normal2-C-C1
{
    font-family:"Arial", sans-serif; color:#fcc73c; font-size:11.0px; line-height:1.27em;
}
.Normal2-C-C2
{
    font-family:"Arial", sans-serif; font-style:italic; color:#ffffff; font-size:11.0px; line-height:1.27em;
}
.Normal2-C-C3
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:11.0px; line-height:1.27em;
}
.Corps-C-C0
{
    font-family:"Arial", sans-serif; color:#ffffff; font-size:11.0px; line-height:1.27em;
}
</style>
<link rel="stylesheet" href="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpstyles.css" type="text/css"></head>

<body style="background:#003566; height:4000px;" text="#000000">
<div style="background-color:transparent;margin-left:auto;margin-right:auto;position:relative;width:320px;height:4000px;">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/head_mobile_court_nv.png" alt="" style="position:absolute;left:0px;top:0px;" border="0" height="68" width="320">
<map id="map0" name="map0">
    <area shape="poly" coords="129,50,137,45,142,37,143,34,143,14,138,6,130,0,13,0,5,6,1,12,0,16,0,38,6,46,15,51,128,51" href="javascript:history.back()" alt="">
</map>

<div style=" width:320px; text-align:center; z-index:1; position:absolute;left:0px; top:80px;"">
    <span style='color: #ffffff;
        font-family: "Vladimir Script",cursive;
        font-size: 30px;
        line-height: 47px;'>Conditions générales</span>
    </div>

<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp9f0fe9f8_06.png" alt="" usemap="#map0" style="position:absolute; left:92px; top:153px;" border="0" height="51" width="143">
<div style="position:absolute; left:97px; top:161px; width:136px; height:50px; overflow:hidden;">
<p class="Corps-P"><span class="Corps-C"><a href="javascript:history.back()" style="color:#ffffff;text-decoration:none;">Retour vers</a></span></p>
<p class="Corps-P"><span class="Corps-C"><a href="javascript:history.back()" style="color:#ffffff;text-decoration:none;">la page</a></span></p>
<p class="Corps-P"><span class="Corps-C"><a href="javascript:history.back()" style="color:#ffffff;text-decoration:none;">précédente</a></span></p>
</div>
<div style="position:absolute;left:15px;top:237px;width:290px;height:3195px;overflow:hidden;">
<p class="Normal2-P"><span class="Normal2-C">L'adhésion du Consommateur</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Afin de bénéficier des avantages du Club Proximité :</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Le consommateur doit s'identifier et s'inscrire uniquement en ligne sur www.club-<wbr>proximite.com
    ou sur nos applications mobiles.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Le consommateur doit confirmer qu'il est majeur.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Le consommateur doit accepter sans réserve les conditions générales du Club Proximité.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Le consommateur doit valider sa demande d'inscription.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Après validation de sa demande il reçoit par retour de mail une confirmation d'enregistrement
    avec la présence de son identifiant et mot de passe qu'il doit confirmer.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Le consommateur gère lui-<wbr>même sa fiche, il peut la modifier à volonté.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Le consommateur s'engage sur l'honneur à n'ouvrir qu'un seul compte adhérent et de
    ne s'enregistrer qu'une seule fois en qualité de consommateur.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Le consommateur qui désire se désinscrire du Club devra nous adresser par mail sa
    demande de retrait.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Le consommateur ne reçoit pas de carte d'adhérent, seule la présentation du mail
    de confirmation imprimé ou présent sur son Smartphone lui permettra de bénéficier
    de l'offre « Bons Plans » du diffuseur. </span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C">L'adhésion du Professionnel</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Le club Proximité référence les professionnels ayant souscrits un abonnement &nbsp;Basic
    , PREMIUM ou PLATINIUM </span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Les abonnés Premium proposent des bons plans.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Les abonnés Platinium proposent des bons plans mais aussi des annonces promonionnelles.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C">Les Bons Plans : </span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Avec ce module, nos partenaires concèdent des offres exceptionnelles pour motiver
    les consommateurs à leur rendre visite.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C1">L'offre "Bon Plan" &nbsp;peut être unique</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">La qualité de l'offre sera précisée sur chacune d'elles :</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Celle ci peut-<wbr>être validée qu'une seule fois : </span><span class="Normal2-C-C2">Exemple pour un restaurant (1 menu
    acheté = 1 menu offert) L'adhérent &nbsp;identifié ne peut la valider qu'une seule fois.
    Si par nature il désire valider une promotion déjà utilisé, notre système lui précisera
    son impossibilité de le faire.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C2">Cette offre est exceptionnelle et utilisable qu'une seule fois, une fois le consommateur
    identifié, il valide son offre, si celle-<wbr>ci a déjà été utilisée par ce même consommateur,
    le bouton change de couleur et le message devient « Cette offre a déjà été utilisée
    ».</span></p>
<p class="Normal2-P"><span class="Normal2-C-C2"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C2">Si un consommateur ayant déjà profité de l'offre découverte s'inscrit sous un autre
    nom et/ou &nbsp;avec un autre e-<wbr>mail pour pouvoir bénéficier une seconde fois de l'offre
    découverte chez le même professionnel, ce consommateur verra son compte supprimé
    du Club Proximité.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C2"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C2">Si le diffuseur reconnaît ce consommateur, il est en droit de refuser à concéder
    son offre découverte et à avertir le club de la mauvaise foi de ce consommateur.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C1">Ou bien les partenaires peuvent choisir qu'elle soit permanente :</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Pendant la totalité de leur abonnement ou pendant une période précisée. L'adhérent
    &nbsp;identifié peut valider votre offre autant de fois qu'il le désire.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C">Le Fonctionnement des “Bons Plans”</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Les offres « Bons Plans » sont valables uniquement pendant la période de l'abonnement
    du partenaire à creezvotresiteweb.fr</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">La date de fin de cet abonnement est précisée sur l'offre du professionnel.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Avant de valider l'offre « Bon Plan », le consommateur doit s'identifier (identifiant
    et mot de passe).</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Puis il valide en cliquant sur le bouton « Validation de ce Bon Plan ».</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Dès validation de cette offre, le consommateur reçoit un mail personnalisé qui lui
    confirme son choix, il imprime ce bon et le présente chez le diffuseur pour en bénéficier
    ou présente le courriel sur son Smartphone.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">L'offre « Bon Plans » n'est pas cumulable avec les offres de fidélisation, soldes
    ou promotions en cours déjà concédés par le diffuseur.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Parallèlement, ce même diffuseur reçoit un double de la demande, précisant les coordonnées
    du consommateur par courriel.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Le consommateur autorise ce diffuseur à prendre contact avec lui afin de préciser
    un jour et une heure de rendez vous.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">L'offre découverte est valable 15 jours après son émission, passé ce délai, le diffuseur
    peut refuser de concéder cette offre au consommateur.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C">Les Annonces Professionnelles</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Les partenaires ayant souscrits un abonnement «&nbsp;Platinium&nbsp;» bénéficient d’un module
    de 20 annonces, ils déposent en temps réel leurs produits ou services (destockage,
    soldes, promotions etc...)</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Depuis leur page admin, ils peuvent dans l'année de leur abonnement, gérer ce capital
    de 20 annonces (création, modification, suppression).</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Ces annonces professionnelles sont &nbsp;présentes sur leur site, le site internet et
    sur les applications mobiles du Club Proximité</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C">L’engagement des Partenaires du Club :</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Le Club propose sur sa base de données hébergée sur le site internet www.club-<wbr>proximite.com
    et ses applications mobiles des informations et des offres des commerçants et artisans
    qui ont adhérés en qualité de partenaires.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Les diffuseurs commerçants, artisans, PME, PMI s'engagent pendant la période de leur
    abonnement à :</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Respecter strictement leurs offres « Bons Plans » et «&nbsp;annonces&nbsp;».</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"> </span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Assumer seuls la responsabilité de la régularité des informations précisées sur leur
    page informative et de l'annonce de leurs offres. </span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Les offres réservées aux consommateurs abonnés du « Club Proximité » sont proposées
    par les diffuseurs sous leur entière responsabilité.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C">Engagement du Club Proximité :</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Le Club fourni un outil complet de communication.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Avec une base de données relationnelle hébergée sur le site internet www.club-<wbr>proximite.com
    </span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Avec la présence d'insertions publicitaires sur l'ensemble des éditions du Magazine
    Proximité.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Ainsi que tous autres moyens de communication que le Club jugera utile. </span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">La page d'accueil du Club Proximité :</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Sur cette page : www.club-<wbr>proximite.com, le consom-<wbr>mateur découvre l'ensemble des
    professionnels et diffuseurs ainsi que leurs offres concédées. </span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">A l'aide de recherches multicritères, le consommateur navigue et choisit la ou les
    offres pouvant l'intéresser.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C">La Charte de Confidentialité</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"> </span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Le Club Proximité s'engage à ce que les données personnelles qui sont recueillies
    au moment de votre inscription au Club restent totalement confidentielles et ne soient
    transmises à aucun tiers.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Toutefois, en adhérant au club vous indiquez que vous souhaitez recevoir des informations
    régulières concernant la vie du club et du magazine Proximité.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C">Informatique et libertés</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Conformément à la Loi numéro 78-<wbr>17 du 6 janvier 1978, dite "Loi Informatique et Libertés",
    vous avez un droit d'accès et de rectification des données nominatives que vous fournissez
    dans le cadre de votre adhésion au Club.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Si, à un moment quelconque, vous souhaitez exercer votre droit d'accès et de rectification
    aux informations nominatives vous concernant, merci d'adresser votre demande avec
    votre identifiant et votre mot de passe par mail à proximite-<wbr>magazine@orange.fr.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Une copie confidentielle des informations nominatives spécifiques à chaque membre
    vous sera adressée. Par mesure de confidentialité, toute demande ne contenant pas
    l'identifiant et le mot de passe du membre sera systématiquement rejetée.</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C">Mentions Légales</span></p>
<p class="Normal2-P"><span class="Normal2-C-C1">Editeur du site Internet :</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Normal2-P"><span class="Normal2-C-C3">SARL PHASE</span></p>
<p class="Normal2-P"><span class="Normal2-C-C3">60 avenue de Nice</span></p>
<p class="Normal2-P"><span class="Normal2-C-C3">06800 Cagnes sur Mer</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Tél. : 04 93 73 84 51</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">Mobile : 06 72 05 59 35</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">SARL au capital de 7265€ </span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">RCS Antibes 412.071.466</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">SIRET : 412071466 00012</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">CODE NAF : 7022Z</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0">IBAN : FR76 3007 6023 5110 6081 0020 072</span></p>
<p class="Normal2-P"><span class="Normal2-C-C0"><br></span></p>
<p class="Wp-Corps-P"><span class="Corps-C-C0"><br></span></p>
</div>

<div style="position:absolute;left:0px;top:3350px;">
<?php 
$data["zTitle"] = 'Identification';
$this->load->view("front2013/includes/mobile_footer.php", $data);
?>
</div>

</div>

</body></html>