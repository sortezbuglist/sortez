<?php $data["zTitle"] = 'Creation bonplan'; ?>
<?php $this->load->view("front2013/includes/header_mini_2", $data); ?>
	<script>
		$(function() {
			$( "#IdDateDebut" ).datepicker({ dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
											 dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
											 monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
											 dateFormat: 'DD, d MM yy',
											 autoSize: true});
			$( "#IdDateFin" ).datepicker({ dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
											dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
											monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
											dateFormat: 'DD, d MM yy'});
		});
		
		function deleteFile(_IdBonplan,_FileName) {
			//alert(gCONFIG["SITE_URL"] + 'front/bonplan/delete_files/' + _IdBonplan + '/' + _FileName);
        jQuery.ajax({
            url: gCONFIG["SITE_URL"] + 'front/bonplan/delete_files/' + _IdBonplan + '/' + _FileName,
            dataType: 'html',
            type: 'POST',
            async: true,
            success: function(data){
                window.location.reload();
            }
        });
    }
	</script>
    
    <script type="text/javascript">
		function limite(textarea, max)
		{
			if(textarea.value.length >= max)
			{
				textarea.value = textarea.value.substring(0,max);
			}
			var reste = max - textarea.value.length;
			var affichage_reste =  reste +' caract&egrave;res restants';
			document.getElementById('max_desc').innerHTML = affichage_reste;
		}
		
		function limite_title(textarea, max)
		{
			if(textarea.value.length >= max)
			{
				textarea.value = textarea.value.substring(0,max);
			}
			var reste = max - textarea.value.length;
			var affichage_reste =  reste +' caract&egrave;res restants';
			document.getElementById('max_desc_title').innerHTML = affichage_reste;
		}
	</script>
    <style type="text/css">
	.input_width {
		width:400px;
	}
	</style>
    
    <div id="divFicheBonplan" class="content" align="center">
        <form name="frmCreationBonplan" id="frmCreationBonplan" action="<?php if (isset($oBonplan)) { echo site_url("front/bonplan/modifBonplan/$idCommercant"); }else{ echo site_url("front/bonplan/creerBonplan/$idCommercant"); } ?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="bonplan[bonplan_commercant_id]" id="IdCommercant" value="<?php echo $idCommercant ; ?>" />
		<input type="hidden" name="bonplan[bonplan_id]" id="IdBonplan" value="<?php if (isset($oBonplan)) { echo $oBonplan->bonplan_id ; } ?>" />
            <h1><?php echo $title ; ?></h1>
            <p>&nbsp;</p>
            <div><p><a href="<?php echo site_url("front/utilisateur/contenupro");?>" style="text-decoration:none;"><input type="button" value="Retour"/></a></p></div>
                       
			<span><?php if (isset($msg)) { echo $msg ; } ?></span>
            <fieldset>
                <legend>Bon plan</legend>
                <table width="100%" cellpadding="3" cellspacing="3" style="text-align:left; float:left;" align="left">
                    <!--<tr>
                        <td>
                            <label>Categorie : </label>
                        </td>
                        <td>
                            <select name="bonplan[bonplan_categorie_id]" id="idCategorie">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($colCategorie)) { ?>
                                    <?php foreach($colCategorie as $objColCategorie) { ?>
                                        <option value="<?php echo $objColCategorie->IdRubrique; ?>" <?php if (isset($oBonplan) && $oBonplan->bonplan_categorie_id == $objColCategorie->IdRubrique) { echo "selected"; }?> ><?php echo htmlentities($objColCategorie->Nom); ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>-->
					 <!--<tr>
                        <td>
                            <label>Ville : </label>
                        </td>
                        <td>
                            <select name="bonplan[bonplan_ville_id]" id="idVille">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($listVille)) { ?>
                                    <?php foreach($listVille as $objlistVille) { ?>
                                        <option value="<?php echo $objlistVille->IdVille; ?>" <?php if (isset($oBonplan) && $oBonplan->bonplan_ville_id == $objlistVille->IdVille) { echo "selected"; }?> ><?php echo htmlspecialchars(stripcslashes($objlistVille->Nom)); ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>-->
					<tr>
                        <td>
                            <label>Titre : </label>
                        </td>
                        <td>
                            <textarea rows="2" cols="20" name="bonplan[bonplan_titre]" onkeyup="limite_title(this,'100');" onkeydown="limite_title(this,'100');" id="idTitre" class="input_width"><?php if (isset($oBonplan)) { echo $oBonplan->bonplan_titre ; } ?></textarea><br /><span id="max_desc_title"></span>
                        </td>
                    </tr>
					<tr>
                        <td valign="top">
                            <label>Description : </label>
                        </td>
                        <td>
                            <textarea rows="2" cols="40" name="bonplan[bonplan_texte]" id="idDescription" onkeyup="limite(this,'250');" onkeydown="limite(this,'250');" style="height:100px;"  class="input_width"><?php if (isset($oBonplan)) { echo $oBonplan->bonplan_texte ; } ?></textarea>
                            <?php echo display_ckeditor($ckeditor0); ?>
                            <br /><span id="max_desc"></span>
                        </td>
                    </tr>
					<tr>
                        <td>
                            <label>Utilisable plusieurs fois : </label>
                        </td>
                        <td>
                            <input style = "width:137px;" type="hidden" name="bonplan[bonplan_nombrepris]" id="IdNbrpris" value="<?php if (isset($oBonplan)) { echo $oBonplan->bonplan_nombrepris ; } ?>" />
							 <input  type="checkbox" name="chk_bon" id="chk_bon" onchange ="setbon_plan_utilise_plusieurs();"  <?php if (isset($oBonplan)) { if($oBonplan->bon_plan_utilise_plusieurs == true) { echo "checked = 'checked'";}  } ?> />
							 <input type = "hidden"  id = "bon_plan_utilise_plusieurs"  name ="bonplan[bon_plan_utilise_plusieurs]" value = "<?php if (isset($oBonplan)) {  echo $oBonplan->bon_plan_utilise_plusieurs ;}  ?>">
							
                        </td>
                    </tr>
                    <tr>
                        <td style=" width:200px;">
                            <label>Vos Conditions : </label>
                        </td>
                        <td>
                          <select name="bonplan[bonplan_condition_vente]" id="idConditionVente" class="input_width">
                                <option value="0">Choisissez vos conditions de réservation ou de règlement</option>
                                <option value="2" <?php  if (isset($oBonplan) && $oBonplan->bonplan_condition_vente == 2) { echo "selected"; } ?>>Réservation en ligne</option>
                            </select>
                            <br/><span style="font-size:10px; color:#F00;">Si la "quantité proposée" est vide ou 0 et la "réservation en ligne" n'est pas choisie dans "Vos conditions", le module de réservation en ligne n'est pas affiché dans la partie publique de votre site.</span>
                        </td>
                    </tr>
					<tr>
                        <td>
                            <label>Date debut : </label>
                        </td>
                        <td>
                            <input type="text" name="bonplan[bonplan_date_debut]" id="IdDateDebut" value="<?php if (isset($oBonplan)) { echo convert_Sqldate_to_Frenchdate($oBonplan->bonplan_date_debut) ; } ?>"  class="input_width"/>
                        </td>
                    </tr>
					<tr>
                        <td>
                            <label>Date fin : </label>
                        </td>
                        <td>
                            <input  class="input_width" type="text" name="bonplan[bonplan_date_fin]" id="IdDateFin" value="<?php if (isset($oBonplan)) { echo convert_Sqldate_to_Frenchdate($oBonplan->bonplan_date_fin) ; } ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Quantité proposée : </label>
                        </td>
                        <td>
                          <input type="text" name="bonplan[bonplan_quantite]" id="IdQuantite" value="<?php if (isset($oBonplan)) { echo $oBonplan->bonplan_quantite ; } ?>"  class="input_width"/>
                          <br/><span style="font-size:10px; color:#F00;">Si la "quantité proposée" est vide ou 0 et la "réservation en ligne" n'est pas choisie dans "Vos conditions", le module de réservation en ligne n'est pas affiché dans la partie publique de votre site.</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Durée de Mise à disposition : </label>
                        </td>
                        <td>
                          <input type="text" name="bonplan[bonplan_livraison]" id="IdLivraison" value="<?php if (isset($oBonplan)) { echo $oBonplan->bonplan_livraison ; } ?>"  class="input_width"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Photo 01 : </label>
                        </td>
                        <td>
							<input type="hidden" name="photo1Associe" id="photo1Associe" value="<?php if (isset($oBonplan)) { echo $oBonplan->bonplan_photo1 ; } ?>" />
                            <?php if(!empty($oBonplan->bonplan_photo1)) { ?>
								<?php echo image_thumb("application/resources/front/images/" . $oBonplan->bonplan_photo1, 100, 100,'',''); ?>
                                <a href="javascript:void(0);" onclick="deleteFile('<?php echo $oBonplan->bonplan_id; ?>','bonplan_photo1');">Supprimer</a>
                            <?php } else { ?>
                            <input type="file" name="bonplanPhoto1" id="bonplanPhoto1" value="" />
                            <?php }?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Photo 02 :</label>
                        </td>
                        <td>
							<input type="hidden" name="photo2Associe" id="photo2Associe" value="<?php if (isset($oBonplan)) { echo $oBonplan->bonplan_photo2 ; } ?>" />
                            <?php if(!empty($oBonplan->bonplan_photo2)) { ?>
								<?php echo image_thumb("application/resources/front/images/" . $oBonplan->bonplan_photo2, 100, 100,'',''); ?>
                                <a href="javascript:void(0);" onclick="deleteFile('<?php echo $oBonplan->bonplan_id; ?>','bonplan_photo2');">Supprimer</a>
                            <?php } else { ?>
                            <input type="file" name="bonplanPhoto2" id="bonplanPhoto2" value="" />
                            <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Photo 03 :</label>
                        </td>
                        <td>
							<input type="hidden" name="photo3Associe" id="photo3Associe" value="<?php if (isset($oBonplan)) { echo $oBonplan->bonplan_photo3 ; } ?>" />
                            <?php if(!empty($oBonplan->bonplan_photo3)) { ?>
								<?php echo image_thumb("application/resources/front/images/" . $oBonplan->bonplan_photo3, 100, 100,'',''); ?>
                                <a href="javascript:void(0);" onclick="deleteFile('<?php echo $oBonplan->bonplan_id; ?>','bonplan_photo3');">Supprimer</a>
                            <?php } else { ?>
                            <input type="file" name="bonplanPhoto3" id="bonplanPhoto3" value=""/>
                            <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Photo 04 :</label>
                        </td>
                        <td>
							<input type="hidden" name="photo4Associe" id="photo4Associe" value="<?php if (isset($oBonplan)) { echo $oBonplan->bonplan_photo4 ; } ?>" />
                            <?php if(!empty($oBonplan->bonplan_photo4)) { ?>
								<?php echo image_thumb("application/resources/front/images/" . $oBonplan->bonplan_photo4, 100, 100,'',''); ?>
                                <a href="javascript:void(0);" onclick="deleteFile('<?php echo $oBonplan->bonplan_id; ?>','bonplan_photo4');">Supprimer</a>
                            <?php } else { ?>
                            <input type="file" name="bonplanPhoto4" id="bonplanPhoto4" value="" />
                            <?php } ?>
                        </td>
                    </tr>
					<tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
					<tr>
                        <td>
                        <?php if (isset($oBonplan) && $oBonplan->bonplan_id!='0') { ?>
                        <a href="<?php echo site_url("front/bonplan/supprimBonplan/" . $oBonplan->bonplan_id . "/" . $idCommercant) ; ?>" onclick="if (!confirm('voulez-vous vraiment supprimer cette annoce?')){ return false ; }">Supprimer ce bonplan</a>
                        <?php } ?>
                        </td>
                        <td align="left"><input type="button" value="Annuler" onclick="document.location='<?php echo site_url("front/utilisateur/contenupro");?>';" />&nbsp;<input type="button" value="Valider" onclick="javascript:testFormBonplan();"/></td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
<?php $this->load->view("front2013/includes/footer_mini_2"); ?>
