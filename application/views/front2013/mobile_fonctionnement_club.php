<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="fr"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>fonctionnement-club</title>
<meta name="Generator" content="Serif WebPlus X6">
<meta name="viewport" content="width=320">
<style type="text/css">
body{margin:0;padding:0;}
.Corps-P
{
    margin:0.0px 0.0px 12.0px 0.0px; text-align:justify; font-weight:400;
}
.Corps-P-P0
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:center; font-weight:400;
}
.Corps-P-P1
{
    margin:0.0px 0.0px 12.0px 0.0px; text-align:center; font-weight:400;
}
.Corps-C
{
    font-family:"Arial", sans-serif; color:#ffffff; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C0
{
    font-family:"Arial", sans-serif; color:#ffffff; font-size:12.0px; line-height:12.0px;
}
.Corps-artistique-C
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:15.0px; line-height:1.20em;
}
.Corps-C-C1
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C2
{
    font-family:"Arial", sans-serif; font-weight:700; color:#fcc73c; font-size:16.0px; line-height:1.25em;
}
</style>
<link rel="stylesheet" href="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpstyles.css" type="text/css"></head>

<body style="background:#003566; height:3000px;" text="#000000">
<div style="background-color:transparent;margin-left:auto;margin-right:auto;position:relative;width:320px;height:3000px;">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/head_mobile_court_nv.png" alt="" style="position:absolute;left:0px;top:0px;" border="0" height="68" width="320">

<div style='margin-top:0px; margin-bottom:10px; color: #ffffff;
font-family: "Vladimir Script",cursive;
font-size: 30px;
line-height: 47px; text-align:center;position:absolute;left:0px;top:85px; width:320px;'>
Le fonctionnement du Club
</div>

<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp31a3bec2_06.png" alt="" style="position:absolute;left:0px;top:1258px;" border="0" height="374" width="320">
<div style="position:absolute;left:15px;top:234px;width:290px;height:207px;overflow:hidden;">
<p class="Corps-P"><span class="Corps-C">Le Club proximité est un concept dynamique hébergé sur le web conçu par le Magazine
    Proximité.</span></p>
<p class="Corps-P"><span class="Corps-C">Cet outil a pour objectif de promouvoir le commerce et l’artisanat local dans les
    Alpes-<wbr>Maritimes.</span></p>
<p class="Corps-P"><span class="Corps-C">Le Club référence sur son site web et sur ses applications mobiles (iphone-<wbr>android)
    les professionnels ayant souscrits un abonnement.</span></p>
<p class="Corps-P"><span class="Corps-C">En fonction de leur engagement, leurs données sont présentes sur les 3 annuaires
    (l’annuaire des parte naires, l’annuaire des annonces et l’annuaire des bons plans).</span></p>
</div>
<map id="map0" name="map0">
    <area shape="poly" coords="129,50,137,45,142,37,143,34,143,14,138,6,130,0,13,0,5,6,1,12,0,16,0,38,6,46,15,51,128,51" href="javascript:history.back()" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp9f0fe9f8_06.png" alt="" usemap="#map0" style="position:absolute;left:89px;top:154px;" border="0" height="51" width="143">
<div style="position:absolute;left:92px;top:160px;width:136px;height:50px;overflow:hidden;">
<p class="Corps-P-P0"><span class="Corps-C-C0"><a href="javascript:history.back()" style="color:#ffffff;text-decoration:none;">Retour vers</a></span></p>
<p class="Corps-P-P0"><span class="Corps-C-C0"><a href="javascript:history.back()" style="color:#ffffff;text-decoration:none;">la page</a></span></p>
<p class="Corps-P-P0"><span class="Corps-C-C0"><a href="javascript:history.back()" style="color:#ffffff;text-decoration:none;">précédente</a></span></p>
</div>
<a href="<?php echo site_url('front/particuliers/inscription/'); ?>"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp9d25a235_06.png" alt="" style="position:absolute;left:88px;top:448px;" border="0" height="138" width="145"></a>
<div style="position:absolute;left:15px;top:656px;width:290px;height:82px;overflow:hidden;">
<p class="Corps-P"><span class="Corps-C">Sur cet annuaire, vous effectuez des recherches multicritères (communes et activités),
    puis vous accédez aux fiches informatives des partenaires (ces infos varient en fonction
    de l’importance de leur abonnement).</span></p>
</div>
<a href="<?php echo site_url('front/mobile/recherchepartenaires'); ?>"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpdcd8cc61_06.png" alt="" style="position:absolute;left:16px;top:591px;" border="0" height="56" width="290"></a>
<a href="<?php echo site_url('front/mobile/recherchepartenaires'); ?>">
<div style="position:absolute;left:92px;top:610px;width:190px;height:18px;white-space:nowrap;">
    <div class="Wp-Corps-artistique-P">
    <span class="Corps-artistique-C">L’annuaire des partenaires</span></div>
</div>
</a>
<a href="<?php echo site_url('front/mobile/rechercheannonces'); ?>">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp5343713f_06.png" alt="" style="position:absolute;left:16px;top:758px;" border="0" height="54" width="290">
</a>
<a href="<?php echo site_url('front/mobile/recherchebonplans'); ?>">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp34c123e5_06.png" alt="" style="position:absolute;left:16px;top:821px;" border="0" height="54" width="290">
</a>
<a href="<?php echo site_url('front/mobile/rechercheannonces'); ?>">
<div style="position:absolute;left:97px;top:776px;width:178px;height:18px;white-space:nowrap;">
    <div class="Wp-Corps-artistique-P">
        <span class="Corps-artistique-C">L’annuaire des annonces</span></div>
</div>
</a>
<a href="<?php echo site_url('front/mobile/recherchebonplans'); ?>">
<div style="position:absolute;left:97px;top:839px;width:186px;height:18px;white-space:nowrap;">
    <div class="Wp-Corps-artistique-P">
        <span class="Corps-artistique-C">L’annuaire des bons plans</span></div>
</div>
</a>
<div style="position:absolute;left:16px;top:893px;width:290px;height:57px;overflow:hidden;">
<p class="Corps-P"><span class="Corps-C">Le professionnel partenaire dépose en temps réel ses annonces et ses bons plans (destockage,
    soldes, promotions, informations etc...).</span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp3034ccff_06.png" alt="" style="position:absolute;left:16px;top:964px;" border="0" height="69" width="290">
<div style="position:absolute;left:16px;top:1061px;width:290px;height:54px;overflow:hidden;">
<p class="Corps-P"><span class="Corps-C">Dès qu’un produit, service ou bon plan vous séduit, le partenaire vous proposera
    deux solutions de concrétisation :</span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp1279a0c9_06.png" alt="" style="position:absolute;left:15px;top:1133px;" border="0" height="112" width="290">
<div style="position:absolute;left:15px;top:1326px;width:290px;height:38px;overflow:hidden;">
<p class="Corps-P-P1"><span class="Corps-C-C1">Avant de pouvoir valider cette réservation , le consommateur doit être inscrit au
    Club Proximité.</span></p>
</div>
<div style="position:absolute;left:15px;top:1420px;width:290px;height:73px;overflow:hidden;">
<p class="Corps-P-P1"><span class="Corps-C-C1">Après avoir validé l’offre, vous recevez par retour un email vous confirmant cette
    réservation et sa durée de mise à disposition. Parallèlement le partenaire reçoit
    un double avec vos coordonnées.</span></p>
</div>
<div style="position:absolute;left:15px;top:1567px;width:290px;height:59px;overflow:hidden;">
<p class="Corps-P-P1"><span class="Corps-C-C1">Pour bénéficier de cette offre, il vous suffit de nous présenter lors de votre déplacement
    le mail imprimé ou présent sur &nbsp;votre smartphone.</span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp36d609b7_06.png" alt="" style="position:absolute;left:18px;top:1654px;" border="0" height="90" width="285">
<div style="position:absolute;left:15px;top:1759px;width:290px;height:134px;overflow:hidden;">
<p class="Corps-P"><span class="Corps-C">Sur certaines offres vous retrouverez un formulaire de contact express, il vous permettra
    de poser différentes questions ou de demander un rendez-<wbr>vous.</span></p>
<p class="Corps-P"><span class="Corps-C">Dès validation, ce formulaire, &nbsp;reprenant vos coordonnées sera directement adressé
    au partenaire.</span></p>
</div>
<div style="position:absolute;left:15px;top:1910px;width:290px;height:29px;overflow:hidden;">
<p class="Corps-P-P1"><span class="Corps-C-C2">La gestion des favoris</span></p>
</div>
<div style="position:absolute;left:160px;top:1964px;width:145px;height:78px;overflow:hidden;">
<p class="Corps-P"><span class="Corps-C">Vous recevez une alerte email à chaque nouvelle annonce et bon plan déposés par vos
    favoris.</span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp21da982f_06.png" alt="" style="position:absolute;left:15px;top:1939px;" border="0" height="103" width="112">
<div style="position:absolute;left:15px;top:2078px;width:145px;height:101px;overflow:hidden;">
<p class="Corps-P"><span class="Corps-C">Les adhérents consom-<wbr>mateurs reçoivent chaque semaine une newsletter personnalisée
    leur préci-<wbr>sant les nouvelles offres déposées</span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpeec4d4ab_06.png" alt="" style="position:absolute;left:182px;top:2062px;" border="0" height="123" width="123">
<map id="map1" name="map1">
    <area shape="poly" coords="24,65,32,57,34,54,283,54,290,48,291,42,291,29,290,13,287,11,288,10,50,10,45,2,42,0,27,0,25,10,6,10,1,18,0,22,0,44,6,52,16,54,16,60,23,66" href="<?php echo site_url('front/particuliers/inscription/'); ?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp826a54b4_06.png" alt="" usemap="#map1" style="position:absolute;left:15px;top:2225px;" border="0" height="66" width="291">

<div style="position:absolute;left:0px;top:2330px;">
<?php 
$data["zTitle"] = 'Identification';
$this->load->view("front2013/includes/mobile_footer.php", $data);
?>
</div>

<div style="position:absolute;left:98px;top:2246px;width:176px;height:18px;white-space:nowrap;">
    <div class="Wp-Corps-artistique-P">
        <span class="Corps-artistique-C"><a href="<?php echo site_url('front/particuliers/inscription/'); ?>" style="color:#ffffff;text-decoration:none;">Je m’inscris, c’est gratuit</a></span></div>
</div>
</div>

</body></html>