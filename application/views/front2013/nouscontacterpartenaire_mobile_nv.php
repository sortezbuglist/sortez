<?php $data["zTitle"] = 'Nous Contacter';?>


<?php $this->load->view("front2013/includes/mobile_header_page_partner", $data); ?>



<script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/jspngfix.js"></script>
<link rel="stylesheet" href="formulairemobile_fichiers/wpstyles.css" type="text/css">
<script type="text/javascript">var blankSrc = "wpscripts/blank.gif";
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.6.4.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery("#submitbtn").click(function(){
            var txtError = "";
            
            var zNom = jQuery("#zNom").val();
            if(zNom=="" || zNom=="Votre nom*") {
                txtError += "- Veuillez indiquer Votre nom <br/>";    
            }
            
            var zEmail = jQuery("#zEmail").val();
            if(!isEmail(zEmail)) {
                txtError += "- Veuillez saisir un email valide <br/>";
            } 
                        
            var zTelephone = jQuery("#zTelephone").val();
            if(zTelephone=="" || zTelephone=="Votre numéro de téléphone*") {
                txtError += "- Veuillez indiquer votre numéro de téléphone <br/>";    
            }
            
            var zCommentaire = jQuery("#zCommentaire").val();
            if(zCommentaire=="" || zCommentaire=="Votre message*") {
                txtError += "- Veuillez indiquer votre message <br/>";    
            }
            
            
            jQuery("#divErrortxtmobile").html(txtError);
            
            if(txtError == "") {
                jQuery("#frmNousContacter").submit();
            }
        });
		
    })
</script>

<div style="background-color: #FFF;"> 
<div style="padding-bottom:10px;padding-top:15px;"><a class="Button1" href="javascript:history.go(-1);">Retour</a></div>
</div>
<div style="background-color: #FFF; padding-left:10px; padding-right:10px; padding-bottom:15px;"> 
<div id="txt_761" style="width:100%;height: auto;overflow:hidden; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height: 1.25em;">
  <p class="Corps-artistique-P"><span class="Corps-artistique-C-C2">Merci de bien vouloir préciser vos coordonnées et votre demande</span></p>
  <p class="Corps-P"><span class="Corps-C">* champs obligatoires</span></p>

  <form name="frmNousContacter" id="frmNousContacter" action="<?php echo site_url("front/annonce/envoiMailNousContacter"); ?>" method="post" enctype="multipart/form-data"> 
  <table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td colspan="2"><input type="text" class="form-control" name="zNom" id="zNom" style="width:98%;" value="Votre nom*" onfocus="if (this.value=='Votre nom*') this.value='';" onblur="if (this.value=='') this.value='Votre nom*';"/>
		<input type="hidden" name="zMailTo" id="zMailTo" value="<?php echo $oInfoCommercant->Email ; ?>"/></td>
  </tr>
  <tr>
    <td colspan="2"><input type="text" class="form-control" name="zEmail" id="zEmail" style="width:98%;" value="Votre courriel*" onfocus="if (this.value=='Votre courriel*') this.value='';" onblur="if (this.value=='') this.value='Votre courriel*';"/></td>
  </tr>
  <tr>
    <td colspan="2"><input type="text" class="form-control" name="zTelephone" id="zTelephone" style="width:98%;" value="Votre numéro de téléphone*" onfocus="if (this.value=='Votre numéro de téléphone*') this.value='';" onblur="if (this.value=='') this.value='Votre numéro de téléphone*';"/></td>
  </tr>
  <tr>
    <td colspan="2"><textarea name="zCommentaire" id="zCommentaire" class="form-control" cols="45" rows="5" style="width:98%;" onfocus="if (this.value=='Votre message*') this.value='';" onblur="if (this.value=='') this.value='Votre message*';">Votre message*</textarea></td>
  </tr>
  <tr>
    <td colspan="2"><input name="checkmail" type="checkbox" value="" /> J'accepte de recevoir par courriel des informations provenant de cet &eacute;tablissement.</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><input name="resetbtn" id="resetbtn" type="reset" value="R&eacute;tablir" class="btn btn-primary"/></td>
    <td align="right"><input name="submitbtn" id="submitbtn" type="button" value="Envoyer" class="btn btn-success"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
 
<div id="divErrortxtmobile" style="width:290px;height: auto; text-align:left; color:#F00;"></div> 
</div>
</div>


<?php $this->load->view("front2013/includes/mobile_footer_page_partner", $data); ?>