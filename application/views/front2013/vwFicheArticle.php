<?php $data["zTitle"] = 'Creation Article'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>
<script>
	function ValidationFormArticle(){
		var zErreur = "";
		
		var article_typeid = $("#article_typeid").val();
		if (article_typeid == "" || article_typeid == "0"){
			zErreur += "Veuillez selectionner un Type d'article</br>" ;
			$("#article_typeid").css('border-color', 'red');
		} else {
			$("#article_typeid").css('border-color', '#E3E1E2');
		}
		
		var article_categid = $("#article_categid").val();
		if (article_categid == "" || article_categid == "0"){
			zErreur += "Veuillez selectionner une catégorie d'article</br>" ;
			$("#article_categid").css('border-color', 'red');
		} else {
			$("#article_categid").css('border-color', '#E3E1E2');
		}
		
		var article_subcategid = $("#article_subcategid").val();
		if (article_subcategid == "" || article_subcategid == "0"){
			zErreur += "Veuillez selectionner une sous-catégorie d'article</br>" ;
			$("#article_subcategid").css('border-color', 'red');
		} else {
			$("#article_subcategid").css('border-color', '#E3E1E2');
		}
		
		var title = $("#title").val();
		if (title == ""){
			zErreur += "Veuillez indiquer un Titre pour article</br>" ;
			$("#title").css('border-color', 'red');
		} else {
			$("#title").css('border-color', '#E3E1E2');
		}
		
		/*var content = $("#content").val();
		if (content == ""){
			zErreur += "Veuillez indiquer une description pour article</br>" ;
			$("#content").css('border-color', 'red');
		} else {
			$("#content").css('border-color', '#E3E1E2');
		}*/
		
		var email = $("#email").val();
		if (!isEmail(email)){
			zErreur += "Veuillez indiquer un Email valide pour l'article</br>" ;
			$("#email").css('border-color', 'red');
		} else {
			$("#email").css('border-color', '#E3E1E2');
		}
		
		
		
		if (zErreur != ""){
			//alert ('Tout les champs sont obligatoires !') ;
			$("#zErreur").html(zErreur);
		}else{
			document.frmCreationArticle.submit();
		}
		
		return false;
	}
	
	function getCategoryTypeArticle(){
	      var article_typeid = jQuery('#article_typeid').val();
          $.post(
            '<?php echo site_url("front/articles/getCategoryTypeArticle"); ?>' + '/' + article_typeid,
			{article_typeid: article_typeid},
            function (zReponse)
            {
                $('#SpanCategoryTypeArticle').html(zReponse);
           });
		   $('#SpanSousCategoryTypeArticle').html("");
	}	
	
	function getSousCategoryTypeArticle(){
	      var article_categid = jQuery('#article_categid').val();
          $.post(
            '<?php echo site_url("front/articles/getSousCategoryTypeArticle"); ?>' + '/' + article_categid,
			{article_categid: article_categid},
            function (zReponse)
            {
                $('#SpanSousCategoryTypeArticle').html(zReponse);
           });
	}	
	
	function event_handicap_checkbox_clic() {
		if ($('#event_handicap_checkbox').attr('checked')) {
			$("#event_handicap").val("1");
		} else {
			$("#event_handicap").val("0");
		}
	}
	
	function event_parking_checkbox_clic() {
		if ($('#event_parking_checkbox').attr('checked')) {
			$("#event_parking").val("1");
		} else {
			$("#event_parking").val("0");
		}
	}
	
	function getCP_event_orgidville(){
	      var event_orgidville = jQuery('#event_orgidville').val();
          jQuery.get(
            '<?php echo site_url("front/professionnels/getPostalCode"); ?>' + '/' + event_orgidville,
            function (zReponse)
            {
                jQuery('#event_orgpostcode').val(zReponse);
           });
	}
	
	$(document).ready(function() {
		<?php if (!isset($idarticle) || $idarticle=="0" || $idarticle=="" || $idarticle==NULL) {?>
		$("#article_categid").attr("disabled", "disabled");
		$("#article_subcategid").attr("disabled", "disabled");
		<?php } ?>
		

	});
	
	$(function() {
			$("#event_startdate").datepicker({
				dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
				monthNames: ['Janvier','Fevier','Mars','Avril','Mai','Juin','Juiller','Aout','Septembre','Octobre','Novembre','Decembre'],
				dateFormat: 'yy-mm-dd',
				autoSize: true
			});
			$("#event_enddate").datepicker({
				dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
				monthNames: ['Janvier','Fevier','Mars','Avril','Mai','Juin','Juiller','Aout','Septembre','Octobre','Novembre','Decembre'],
				dateFormat: 'yy-mm-dd',
				autoSize: true
			});
		});
</script>

<style type="text/css">
.input_fichearticle {
	width:400px;
}
</style>
    
    <div id="divFicheAnnonce" class="content" align="center">
	     <br>
	     <a style = "color:black; text-decoration:none;" href = "<?php echo site_url("front/utilisateur/contenupro" ) ; ?>"> <input type = "button" value= "retour au menu" id = "btn"/> </a>
        <form name="frmCreationArticle" id="frmCreationArticle" action="<?php echo site_url("front/articles/modification/"); ?>" method="post" enctype="multipart/form-data">
		
        <input type="hidden" name="Article[idarticle]" id="IdArticle_id" value="<?php if (isset($oArticle)) echo $oArticle->idarticle; else echo "0"; ?>" />
        <input type="hidden" name="Article[IdCommercant]" id="IdCommercant" value="<?php if (isset($IdCommercant)) echo $IdCommercant; else echo "0"; ?>" />
        
            <h1><?php echo $title ; ?></h1>
			<span><?php if (isset($msg)) { echo $msg ; } ?></span>
            <fieldset>
                <legend>Article</legend>
                <table cellpadding="3" cellspacing="2">
                    
					<tr>
                        <td>
                            <label>Type Article : </label>
                        </td>
                        <td>
                            <select name="Article[article_typeid]" id="article_typeid" onChange="javascript:getCategoryTypeArticle();" class="input_fichearticle">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($colTypeArticle)) { ?>
                                    <?php foreach($colTypeArticle as $objcolTypeArticle) { ?>
                                        <option <?php if(isset($oArticle->article_typeid) && $oArticle->article_typeid == $objcolTypeArticle->article_typeid) echo 'selected="selected"';?> value="<?php echo $objcolTypeArticle->article_typeid; ?>"><?php echo $objcolTypeArticle->article_type; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
					
                    
                    <tr id="SpanCategoryTypeArticle">
                    	
                        <td>
                            <label>Catégorie : </label>
                        </td>
                        <td>
                            <select name="Article[article_categid]" id="article_categid" onChange="javascript:getSousCategoryTypeArticle();" class="input_fichearticle">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($colCategorie)) { ?>
                                    <?php foreach($colCategorie as $objcolCategorie_xx) { ?>
                                        <option <?php if(isset($oArticle->article_categid) && $oArticle->article_categid == $objcolCategorie_xx->article_categid) echo 'selected="selected"';?> value="<?php echo $objcolCategorie_xx->article_categid; ?>"><?php echo $objcolCategorie_xx->category; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>

                        
                    </tr>
                    
                    <tr id="SpanSousCategoryTypeArticle">
                    	
                        <td>
                            <label>Sous-catégorie : </label>
                        </td>
                        <td>
                            <select name="Article[article_subcategid]" id="article_subcategid" onChange="javascript:void(0);" class="input_fichearticle">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($colSousCategorie)) { ?>
                                    <?php foreach($colSousCategorie as $objcolSousCategorie_xx) { ?>
                                        <option <?php if(isset($oArticle->article_subcategid) && $oArticle->article_subcategid == $objcolSousCategorie_xx->article_subcategid) echo 'selected="selected"';?> value="<?php echo $objcolSousCategorie_xx->article_subcategid; ?>"><?php echo $objcolSousCategorie_xx->subcateg; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                        
                    </tr>
                    
                    
                    <tr>
                        <td>
                            <label>Titre : </label>
                        </td>
                        <td>
                            <input type="text" name="Article[title]" id="title" value="<?php if (isset($oArticle)) { echo $oArticle->title ; } ?>" class="input_fichearticle" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Description : </label>
                        </td>
                        <td>
                            <textarea name="Article[content]" id="content" ><?php if(isset($oArticle->content)) echo htmlspecialchars($oArticle->content); ?></textarea>
        					<?php echo display_ckeditor($ckeditor_article); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Conditions tarifaires : </label>
                        </td>
                        <td>
                            <textarea name="Article[price]" id="price" ><?php if(isset($oArticle->price)) echo $oArticle->price; ?></textarea>
                            <?php echo display_ckeditor($ckeditor_price); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Horaire : </label>
                        </td>
                        <td>
                            <textarea name="Article[timeplan]" id="timeplan" ><?php if(isset($oArticle->timeplan)) echo $oArticle->timeplan; ?></textarea>
                            <?php echo display_ckeditor($ckeditor_timeplan); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Promotion : </label>
                        </td>
                        <td>
                            <textarea name="Article[promotion]" id="promotion" ><?php if(isset($oArticle->promotion)) echo $oArticle->promotion; ?></textarea>
                            <?php echo display_ckeditor($ckeditor_promotion); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Site web : </label>
                        </td>
                        <td>
                            <input type="text" name="Article[web]" id="web" value="<?php if (isset($oArticle)) { echo $oArticle->web ; } ?>" class="input_fichearticle" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Email : </label>
                        </td>
                        <td>
                            <input type="text" name="Article[email]" id="email" value="<?php if (isset($oArticle)) { echo $oArticle->email ; } ?>"  class="input_fichearticle"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Audio (mp3) : </label>
                        </td>
                        <td>
                            <input type="file" name="ArticleAudio" id="ArticleAudio"/>
                            
                            
                            <object type="application/x-shockwave-flash" data="'.$container_payer_audio_path.'dewplayer.swf?mp3='.$music_to_load.'&autoplay=false" width="200" height="20">
                            <param name="movie" value="'.$container_payer_audio_path.'dewplayer.swf?mp3='.$music_to_load.'&autoplay=true" />
                            </object>
                            
                            <input type="hidden" name="ArticleAudioAssocie" id="ArticleAudioAssocie" value="<?php if(isset($oArticle->audio)) echo $oArticle->audio; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Video (mp4) : </label>
                        </td>
                        <td>
                            <input type="file" name="ArticleVideo" id="ArticleVideo"/>
                            <input type="hidden" name="ArticleVideoAssocie" id="ArticleVideoAssocie" value="<?php if(isset($oArticle->video)) echo $oArticle->video; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Lien Twitter : </label>
                        </td>
                        <td>
                            <input type="text" name="Article[twitter]" id="twitter" value="<?php if (isset($oArticle)) { echo $oArticle->twitter ; } ?>"  class="input_fichearticle"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Lien Facebook : </label>
                        </td>
                        <td>
                            <input type="text" name="Article[facebook]" id="facebook" value="<?php if (isset($oArticle)) { echo $oArticle->facebook ; } ?>"  class="input_fichearticle"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Lien Google Plus : </label>
                        </td>
                        <td>
                            <input type="text" name="Article[googleplus]" id="googleplus" value="<?php if (isset($oArticle)) { echo $oArticle->googleplus ; } ?>"  class="input_fichearticle"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Emplacement de l'événement : </label>
                        </td>
                        <td>
                            <select name="Article[event_locationid]" id="event_locationid" onChange="javascript:void(0);" class="input_fichearticle">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($colLocation)) { ?>
                                    <?php foreach($colLocation as $objcolLocation) { ?>
                                        <option <?php if(isset($oArticle->event_locationid) && $oArticle->event_locationid == $objcolLocation->location_id) echo 'selected="selected"';?> value="<?php echo $objcolLocation->location_id; ?>"><?php echo $objcolLocation->location; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>ou Autre emplacement : </label>
                        </td>
                        <td>
                            <input type="text" name="Article[event_otherlocation]" id="event_otherlocation" value="<?php if (isset($oArticle)) { echo $oArticle->event_otherlocation ; } ?>" class="input_fichearticle" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Adresse de l'événement : </label>
                        </td>
                        <td>
                            <input type="text" name="Article[event_address]" id="event_address" value="<?php if (isset($oArticle)) { echo $oArticle->event_address ; } ?>"  class="input_fichearticle"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Ville de l'événement : </label>
                        </td>
                        <td>
                            <select name="Article[event_idville]" id="event_idville" onChange="javascript:void(0);" class="input_fichearticle">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($colVilles)) { ?>
                                    <?php foreach($colVilles as $objVille_localisation) { ?>
                                        <option <?php if(isset($oArticle->location_villeid) && $oArticle->location_villeid == $objVille_localisation->IdVille) echo 'selected="selected"';?> value="<?php echo $objVille_localisation->IdVille; ?>"><?php echo $objVille_localisation->Nom; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Date de début de l'événement : </label>
                        </td>
                        <td>
                            <input type="text" name="Article[event_startdate]" id="event_startdate" value="<?php if (isset($oArticle) && $oArticle->event_startdate != "0000-00-00 00:00:00") { echo $oArticle->event_startdate ; } ?>"  class="input_fichearticle"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Date de fin de l'événement : </label>
                        </td>
                        <td>
                            <input type="text" name="Article[event_enddate]" id="event_enddate" value="<?php if (isset($oArticle) && $oArticle->event_enddate != "0000-00-00 00:00:00") { echo $oArticle->event_enddate ; } ?>"  class="input_fichearticle"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label> Accès handicapé</label>
                        </td>
                        <td>
                            <input type="checkbox" name="event_handicap_checkbox" id="event_handicap_checkbox" onClick="javascript:event_handicap_checkbox_clic();" <?php if (isset($oArticle->event_handicap) && $oArticle->event_handicap=="1") echo "checked";?>/>
                            <input type="hidden" name="Article[event_handicap]" id="event_handicap" value="<?php if (isset($oArticle->event_handicap)) echo $oArticle->event_handicap; else echo "0"; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label> Parking</label>
                        </td>
                        <td>
                            <input type="checkbox" name="event_parking_checkbox" id="event_parking_checkbox" onClick="javascript:event_parking_checkbox_clic();" <?php if (isset($oArticle->event_parking) && $oArticle->event_parking=="1") echo "checked";?>/>
                            <input type="hidden" name="Article[event_parking]" id="event_parking" value="<?php if (isset($oArticle->event_parking)) echo $oArticle->event_parking; else echo "0"; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Site de réservation : </label>
                        </td>
                        <td>
                            <input type="text" name="Article[event_booklink]" id="event_booklink" value="<?php if (isset($oArticle)) { echo $oArticle->event_booklink ; } ?>"  class="input_fichearticle"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Nom de l'organisateur : </label>
                        </td>
                        <td>
                            <input type="text" name="Article[event_orgname]" id="event_orgname" value="<?php if (isset($oArticle)) { echo $oArticle->event_orgname ; } ?>"  class="input_fichearticle"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Adresse de l'organisateur : </label>
                        </td>
                        <td>
                            <input type="text" name="Article[event_orgaddress]" id="event_orgaddress" value="<?php if (isset($oArticle)) { echo $oArticle->event_orgaddress ; } ?>"  class="input_fichearticle"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Ville de l'organisateur : </label>
                        </td>
                        <td>
                            <select name="Article[event_orgidville]" id="event_orgidville" onChange="javascript:getCP_event_orgidville();" class="input_fichearticle">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if(sizeof($colVilles)) { ?>
                                    <?php foreach($colVilles as $objVille_localisation) { ?>
                                        <option <?php if(isset($oArticle->event_orgidville) && $oArticle->event_orgidville == $objVille_localisation->IdVille) echo 'selected="selected"';?> value="<?php echo $objVille_localisation->IdVille; ?>"><?php echo $objVille_localisation->Nom; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Code Postal de l'organisateur : </label>
                        </td>
                        <td>
                            <input type="text" name="Article[event_orgpostcode]" id="event_orgpostcode" value="<?php if (isset($oArticle)) { echo $oArticle->event_orgpostcode ; } ?>"  class="input_fichearticle"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Téléphone de l'organisateur : </label>
                        </td>
                        <td>
                            <input type="text" name="Article[event_orgphone]" id="event_orgphone" value="<?php if (isset($oArticle)) { echo $oArticle->event_orgphone ; } ?>" class="input_fichearticle" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Dernière modification : </label>
                        </td>
                        <td>
                            <?php if (isset($oArticle->lastupdate) && $oArticle->lastupdate != NULL && $oArticle->lastupdate != "") echo convert_datetime_to_fr($oArticle->lastupdate); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                            
                        </td>
                        <td>&nbsp;
                            
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                            
                        </td>
                        <td>
                            <div id="zErreur" style="color:#F00;"></div>
                        </td>
                    </tr>
                    
                    
					<tr>
                        <td></td>
                        <td align="left">
                        <input type="button" value="Annuler" onclick="javascript:document.location='<?php echo site_url("front/articles/liste");?>';" />&nbsp;
                        <input type="button" value="Valider" onclick="javascript:ValidationFormArticle();" /></td>
                    </tr>
                </table>
            </fieldset>
            
        </form>
    </div>


<?php $this->load->view("admin/includes/vwFooter2013"); ?>