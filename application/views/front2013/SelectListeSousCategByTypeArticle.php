<td>
    <label>Sous-catégorie : </label>
</td>
<td>
    <select name="Article[article_subcategid]" id="article_subcategid" onChange="javascript:void(0);" class="input_fichearticle">
        <option value="0">-- Veuillez choisir --</option>
        <?php if(sizeof($colSousCategorie)) { ?>
            <?php foreach($colSousCategorie as $objcolSousCategorie) { ?>
                <option <?php if(isset($oArticle->article_subcategid) && $oArticle->article_subcategid == $objcolSousCategorie->article_subcategid) echo 'selected="selected"';?> value="<?php echo $objcolSousCategorie->article_subcategid; ?>"><?php echo $objcolSousCategorie->subcateg; ?></option>
            <?php } ?>
        <?php } ?>
    </select>
</td>