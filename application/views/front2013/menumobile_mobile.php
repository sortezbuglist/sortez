<?php $data["zTitle"] = $oInfoCommercant->NomSociete;?>

<?php $this->load->view("front2013/includes/mobile_header_page_partner", $data); ?>


<div style="margin-bottom:20px; padding-top:20px;"><a class="Button1" href="javascript:history.go(-1);">Retour</a></div>

<?php if (isset($oInfoCommercant->Pdf) && $oInfoCommercant->Pdf!="" && file_exists("application/resources/front/photoCommercant/images/".$oInfoCommercant->Pdf)== true) { 
$link_doc_pdf_partner_to_show = base_url()."application/resources/front/photoCommercant/images/".$oInfoCommercant->Pdf;
} else {
 $link_doc_pdf_partner_to_show = '#';
}
?>


<ul style="text-decoration:none;">

<li style=" width:75px; display:block; float: left;">
	<?php if (isset($oCommercantFavoris) && $oCommercantFavoris != NULL && $oCommercantFavoris->Favoris == "1") { ?>
        <a href="<?php echo site_url('front/utilisateur/delete_favoris/'.$oInfoCommercant->IdCommercant);?>" title="Favoris">
            <img border="0" alt="" src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpfa09cef2_06.png">
        </a>
    <?php } else { ?>
        <a href="<?php echo site_url('front/utilisateur/ajout_favoris/'.$oInfoCommercant->IdCommercant);?>" title="Favoris">
            <img border="0" alt="" src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpfa09cef2_06.png">
        </a>
    <?php } ?>
</li>
<?php if (isset($user_groups) &&( $user_groups->id=='5' || $user_groups->id=='4' )) { ?>
<li style=" width:75px; display:block; float: left;">
	<?php if ($oInfoCommercant->Video!="" || $oInfoCommercant->Video!=NULL) {?>
    <a href="<?php echo site_url($commercant_url_home.'/video');?>" title="Video"><img border="0" alt="" src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp6658722d_06.png"></a>
    <?php } ?>
</li>
<li style=" width:75px; display:block; float: left;">
	<?php if ($oInfoCommercant->Pdf!="" || $oInfoCommercant->Pdf!=NULL) {?>
    <a href="<?php echo $link_doc_pdf_partner_to_show;?>" title="<?php echo $oInfoCommercant->titre_Pdf;?>" target="_blank"><img border="0" alt="" src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp9d94109b_06.png"></a>
    <?php } ?>
</li>
<?php } ?>
<li style=" width:75px; display:block; float: left;">
	<a href="<?php echo site_url($commercant_url_recommandation);?>" title="Recommandatin"><img border="0" alt="" src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp085e821e_06.png"></a>
</li>
<?php if (isset($user_groups) &&( $user_groups->id=='5' || $user_groups->id=='4' )) { ?>
<li style=" width:75px; display:block; float: left;">
	<?php if ($oInfoCommercant->google_plus != "" && $oInfoCommercant->google_plus != NULL) {?>
    <a href="http://www.twitter.com/<?php echo $oInfoCommercant->google_plus ; ?>" title="twitter"><img border="0" alt="" src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpad8cc955_06.png"></a>
    <?php } ?>
</li>
<li style=" width:75px; display:block; float: left;">
	<?php if ($oInfoCommercant->Facebook != "" && $oInfoCommercant->Facebook != NULL) {?>
    <a href="http://www.facebook.com/<?php echo $oInfoCommercant->Facebook ; ?>" title="facebook"><img border="0" alt="" src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp4fcb465e_06.png"></a>
    <?php } ?>
</li>
<li style=" width:75px; display:block; float: left;">
	<?php if ($oInfoCommercant->Twitter != "" && $oInfoCommercant->Twitter != NULL) {?>
    <a href="http://www.googleplus.com/<?php echo $oInfoCommercant->Twitter ; ?>" title="googleplus"><img border="0" alt="" src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpdd2d5aff_06.png"></a>
    <?php } ?>
</li>
<?php } ?>
<li style=" width:75px; display:block; float: left;">
	<?php if ($oInfoCommercant->SiteWeb!='' && $oInfoCommercant->SiteWeb!=null && $oInfoCommercant->SiteWeb!='http://www.') {?>
    <a href="<?php echo $oInfoCommercant->SiteWeb;?>" title="Site Web"><img border="0" alt="" src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpbc918667_06.png"></a>
    <?php } ?>
</li>
<li style=" width:75px; display:block; float: left;">
	<?php if ($oInfoCommercant->page_web_marchand!='' && $oInfoCommercant->page_web_marchand!=null && $oInfoCommercant->page_web_marchand!='http://www.') {?>
    <a href="<?php echo $oInfoCommercant->page_web_marchand; ?>"><img border="0" alt="" src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpde2ea3be_06.png"></a>
    <?php } ?>
</li>

</ul>


<?php $this->load->view("front2013/includes/mobile_footer_page_partner", $data); ?>