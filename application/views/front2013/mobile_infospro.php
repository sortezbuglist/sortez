<?php $data["zTitle"] = 'Commercant' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="fr"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>infospro</title>
<meta name="Generator" content="Serif WebPlus X6">
<meta name="viewport" content="width=320">
<style type="text/css">
body{margin:0;padding:0;}
a.hlink_1:link {color:#0000ff;}
a.hlink_1:visited {color:#393939;}
a.hlink_1:hover {color:#393939;}
a.hlink_1:active {color:#393939;}
.Normal-P
{
    margin:0.0px 0.0px 7.0px 0.0px; text-align:justify; font-weight:400;
}
.Normal-P-P0
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:justify; font-weight:400;
}
.Corps-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:justify; font-weight:400;
}
.Corps-P-P0
{
    margin:0.0px 0.0px 12.0px 0.0px; text-align:justify; font-weight:400;
}
.Normal-C
{
    font-family:"Arial", sans-serif; font-size:13.0px; line-height:1.23em;
}
.Normal-C-C0
{
    font-family:"Arial", sans-serif; font-weight:700; color:#a60000; font-size:13.0px; line-height:1.23em;
}
.Normal-C-C1
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:13.0px; line-height:1.23em;
}
.Normal-C-C2
{
    font-family:"Verdana", sans-serif; font-size:16.0px; line-height:1.13em;
}
.Corps-C
{
    font-family:"Arial", sans-serif; font-size:11.0px; line-height:1.27em;
}
.Corps-C-C0
{
    font-family:"Arial", sans-serif; font-size:8.0px; line-height:1.25em;
}
.Corps-C-C1
{
    font-family:"Verdana", sans-serif; font-size:11.0px; line-height:1.18em;
}
.Corps-artistique-C
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:12.0px; line-height:1.25em;
}
</style>
<link rel="stylesheet" href="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpstyles.css" type="text/css"><script src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/fs.js" type="text/javascript"></script><script src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/suggest-10070.js" type="text/javascript"></script><script src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/fs_002.js" type="text/javascript"></script><script src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/suggest-10070.js" type="text/javascript"></script><script src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/javascript.js" type="text/javascript"></script><script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/w_002.js"></script><script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/w.js"></script></head>

<body style="background:#ffffff url('<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpd5148546_06.jpg') repeat scroll top center; height:2000px;" text="#000000">
<div style="background-color:transparent;margin-left:auto;margin-right:auto;position:relative;width:320px;height:2000px;">
<map id="map0" name="map0">
    <area shape="rect" coords="19,18,169,56" href="<?php echo site_url("front/mobile/menugenerale/") ; ?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp0bd4ae08_06.png" title="" alt="menu-general" usemap="#map0" style="position:absolute;left:0px;top:0px;" border="0" height="109" width="320">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp788d4daf_06.png" alt="" style="position:absolute;left:51px;top:131px;" border="0" height="19" width="217">
<div style="position:absolute;left:16px;top:198px;width:287px;height:495px;overflow:hidden;">
<p class="Normal-P"><span class="Normal-C">Le Club proximité est un concept mutualisé de communication pour le commerce local
    dans les départements des Alpes-<wbr>Maritimes et du Var.</span></p>
<p class="Normal-P"><span class="Normal-C">Le Club Proximité est une réalisation du Magazine Proximité.</span></p>
<p class="Normal-P"><span class="Normal-C">Le Club proximité propose deux types d’abonne-<wbr>ments annuels. </span></p>
<p class="Normal-P"><span class="Normal-C">Ces abonnements sont créés et gérés (modifi-<wbr>cation et suppression) en ligne et en
    temps réel sur un compte personnel et sécurisé.</span></p>
<p class="Normal-P"><span class="Normal-C-C0">-<wbr> Le pack Référence :</span><span class="Normal-C"> cet abonnement permet de référencer son entreprise dans l’annuaire
    des partenaires et la gestion d’un bon plan.</span></p>
<p class="Normal-P"><span class="Normal-C-C0">-<wbr> Le pack Web :</span><span class="Normal-C-C1"> </span><span class="Normal-C">cet abonnement vous permet de compléter d’une manière importante
    les infor-<wbr>mations de votre entreprise, de gérer en plus du bon plan des annonces
    (catalogues, promotions, soldes).</span></p>
<p class="Normal-P"><span class="Normal-C"><a class="hlink_1" href="http://www.proximite-magazine.com/club/les-abonnements.html" target="_blank" style="text-decoration:underline;">En cliquant sur ce lien</a>, vous accédez à une page informative sur la différence des
    abonnements.</span></p>
<p class="Normal-P"><span class="Normal-C">Avec ce pack, vous pouvez également person-<wbr>naliser la présentation de vos pages.</span></p>
<p class="Normal-P"><span class="Normal-C">Enfin, le Club Proximité duplique vos pages et vos données sur le web et créé votre
    site &nbsp;avec un URL personnalisé (ex: mon-<wbr>entreprise.fr). Il s’occupe également de
    son référencement naturel sur GOOGLE.</span></p>
<p class="Normal-P-P0"><span class="Normal-C-C2"><br></span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp0dea87cd_06.png" alt="" style="position:absolute;left:17px;top:173px;" border="0" height="17" width="123">
<textarea name="text_6" rows="7" cols="31" style="position:absolute; left:25px; top:973px; width:269px; height:124px;"></textarea>
<input style="position:absolute; left:205px; top:1117px; width:90px; height:22px;" value="Envoyer" type="button">
<input style="position:absolute; left:100px; top:1117px; width:82px; height:22px;" value="Effacer" type="reset">
<div style="position:absolute;left:68px;top:733px;width:239px;height:116px;overflow:hidden;">
<p class="Corps-P"><span class="Corps-C">Réserver en ligne et se déplacer dans votre srtucture pour concrétiser &nbsp;l’offre.</span></p>
<p class="Corps-P"><span class="Corps-C-C0"><br></span></p>
<p class="Corps-P"><span class="Corps-C">Réaliser leur achat et leur paiement en ligne afin de recevoir leur achat à domicile.</span></p>
<p class="Corps-P"><span class="Corps-C-C0"><br></span></p>
<p class="Corps-P"><span class="Corps-C">Adresser une demande d’information complé-<wbr>mentaire avec l’envoi d’un message express.</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C1"><br></span></p>
</div>
<div style="position:absolute;left:15px;top:703px;width:248px;height:15px;white-space:nowrap;">
    <div class="Wp-Corps-artistique-P">
        <span class="Corps-artistique-C">Suivant vos offres les internautes pourront :</span></div>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpaf0e668f_06.png" alt="" style="position:absolute;left:23px;top:806px;" border="0" height="38" width="20">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpc7e4eaa7_06.png" alt="" style="position:absolute;left:12px;top:773px;" border="0" height="24" width="44">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp712a8694_06.png" alt="" style="position:absolute;left:16px;top:731px;" border="0" height="31" width="30">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpd3cc080b_06.png" alt="" style="position:absolute;left:51px;top:875px;" border="0" height="88" width="200">
</div>

</body>
</html>