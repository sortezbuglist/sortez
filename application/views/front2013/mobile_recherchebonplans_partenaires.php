<?php $data["zTitle"] = 'Commercant' ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Clubproximite - Bonplans</title>
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0; user-zoom=fixed;"/>
<style type="text/css">
.mobile_contener_all {
	width: 320px;
	margin-right: auto;
	margin-left: auto;
}
body {
	padding:0px;
	margin:0px;
	background-color:#003566;
}.header_mobile {
	float: left;
	width: 320px;
	position: relative;
	height:68px;
}
.main_mobile {
	float: left;
	width: 320px;
	position: relative;
}
.footer_mobile {
	float: left;
	width: 320px;
	position: relative;
}
</style>

<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/wpscripts2013/jquery-1.8.3.js"></script>
<script src="<?php echo GetJsPath("front/") ; ?>/js_geo/geo-min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">

$(document).ready(function() {
      
        $('#inputStringVilleHidden_bonplans').change(function() {
            
            $("#span_categ_list_mobile").html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...."/>');
            var inputStringVilleHidden_bonplans = $('#inputStringVilleHidden_bonplans').val();     
            $.post(
              '<?php echo site_url("front/mobile/getcategoriebonplans"); ?>',
              {inputStringVilleHidden_bonplans: inputStringVilleHidden_bonplans},
              function (zReponse)
              {
                  // alert (zReponse) ;
                  $('#span_categ_list_mobile').html(zReponse) ;       


             });
             //alert(inputStringHidden_main_form_mobile);
          });
		  
		  
		  $('#inputStringHidden_main_form_mobile').change(function() {
            
            $("#span_souscateg_list_mobile").html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="Uploading...."/>');
            var inputStringHidden_main_form_mobile = $('#inputStringHidden_main_form_mobile').val();     
            $.post(
              '<?php echo site_url("front/mobile/getsouscategoriebonplans"); ?>',
              {inputStringHidden_main_form_mobile: inputStringHidden_main_form_mobile},
              function (zReponse)
              {
                  // alert (zReponse) ;
                  $('#span_souscateg_list_mobile').html(zReponse) ;       


             });
             //alert(inputStringHidden_main_form_mobile);
          });
          
          
          $("#btn_submit_search_partner_mobile").click(function(){
                var txtError = "";
                
				var inputStringHidden_main_form_mobile = $("#inputStringHidden_main_form_mobile").val();
                if(inputStringHidden_main_form_mobile=="" || inputStringHidden_main_form_mobile=="0") {

                    //txtError += "- Veuillez indiquer Votre inputStringHidden_main_form_mobile <br/>"; 
                    //alert("Veuillez indiquer Votre nom");
                    //$("#inputStringHidden_main_form_mobile").css('border-color', 'red');
                } else {
                    $("#inputStringHidden_main_form_mobile").css('border-color', '#E3E1E2');
                }

                var inputStringHidden_form_mobile = $("#inputStringHidden_form_mobile").val();
                if(inputStringHidden_form_mobile=="" || inputStringHidden_form_mobile=="0") {

                    //txtError += "- Veuillez indiquer Votre inputStringHidden_form_mobile <br/>"; 
                    //alert("Veuillez indiquer Votre nom");
                    //$("#inputStringHidden_form_mobile").css('border-color', 'red');
                } else {
                    $("#inputStringHidden_form_mobile").css('border-color', '#E3E1E2');
                }
				
				var inputStringVilleHidden_form_mobile = $("#inputStringVilleHidden_bonplans").val();
				
				
				if ((inputStringHidden_form_mobile=="" || inputStringHidden_form_mobile=="0") && (inputStringVilleHidden_form_mobile=="" || inputStringVilleHidden_form_mobile=="0") && (inputStringHidden_main_form_mobile!="" && inputStringHidden_main_form_mobile!="0")) {
					$.post(
						  '<?php echo site_url("front/mobile/getsouscategoriebonplans_to_mobile"); ?>',
						  {inputStringHidden_main_form_mobile: inputStringHidden_main_form_mobile,
						  inputStringVilleHidden_form_mobile: inputStringVilleHidden_form_mobile},
						  function (zReponse)
						  {
							  //alert (zReponse) ;
							  $("#inputStringHidden").val(","+zReponse);
							  $("#frmRechercheAnnonce_mobile_page").submit();    
						 });
				} else {
					if(txtError == "") {
						//alert('ok');
						$("#inputStringHidden").val(","+inputStringHidden_form_mobile);
						$("#frmRechercheAnnonce_mobile_page").submit();
						//$("#qsdf_x").click();
					}	
				}
				
				
            });
			
			
			//click on "autour de ma position" button
			$("#btn_geolocalisation_mobile").click(function(){
				
				$("#loading_geolocalisation").html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...."/>');
				
				if(geo_position_js.init()){
					geo_position_js.getCurrentPosition(success_callback,error_callback,{enableHighAccuracy:true});
				}
				else{
					alert("Fonction non accessible");
				}
				
		   });
	
});


function success_callback(p)
	{
		//alert('lat='+p.coords.latitude.toFixed(2)+';lon='+p.coords.longitude.toFixed(2));
		$("#inputGeoLatitude").val(p.coords.latitude.toFixed(4));
		$("#inputGeoLongitude").val(p.coords.longitude.toFixed(4));
		
		var inputStringHidden_main_form_mobile = $("#inputStringHidden_main_form_mobile").val();
		var inputStringHidden_form_mobile = $("#inputStringHidden_form_mobile").val();
		$("#inputStringHidden").val(","+inputStringHidden_form_mobile);
		$("#inputFromGeolocalisation").val("1");
		var ttttt = $("#inputGeoLatitude").val();
		var tttttx = $("#inputGeoLongitude").val();
		//alert(ttttt+" / "+tttttx);
		$("#loading_geolocalisation").html("");
		$("#frmRechercheAnnonce_mobile_page").submit();
	}
	
function error_callback(p)
	{
		alert('error='+p.message);
	}


function change_categ_getsouscateg_bonplans(){
	
            $("#span_souscateg_list_mobile").html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="Uploading...."/>');
            var inputStringHidden_main_form_mobile = $('#inputStringHidden_main_form_mobile').val();     
            $.post(
              '<?php echo site_url("front/mobile/getsouscategoriebonplans"); ?>',
              {inputStringHidden_main_form_mobile: inputStringHidden_main_form_mobile},
              function (zReponse)
              {
                  // alert (zReponse) ;
                  $('#span_souscateg_list_mobile').html(zReponse) ;       


             });
             //alert(inputStringHidden_main_form_mobile);
	}


</script>

</head>

<body>
<div class="mobile_contener_all">
  <div class="header_mobile">
    <img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/head_mobile_court_nv.png" alt="" border="0" width="320" height="68">
  </div>
  <div class="main_mobile">
  
  <div id="loading_geolocalisation" class="loading_geolocalisation" style="text-align:center;"></div>
  	
<div style='color: #ffffff;
    font-family: "Vladimir Script",cursive;
    font-size: 30px; text-align:center;
    line-height: 30px;'><!--L'anunaire des bons plans-->
    <img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/d.PNG" alt="" border="0"/>
    </div>    
    
<form name="frmRechercheAnnonce" id="frmRechercheAnnonce_mobile_page" method="post" action="<?php echo site_url('front/bonplan/index/'); ?>">
<table width="100%" border="0" cellspacing="4" cellpadding="0" style="text-align:center;" align="center">
<input type="hidden" name="hdnFavoris" id="hdnFavoris" value="" />
<input type="hidden" name="from_mobile_search_page_partner" id="from_mobile_search_page_partner" value="1"/>
<input type="hidden" name="inputStringHidden" id="inputStringHidden" value=""/>

<?php 
$thisss =& get_instance();
$thisss->load->library('ion_auth');
$this->load->model("ion_auth_used_by_club");
if ($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
	$user_ion_auth = $thisss->ion_auth->user()->row();
	$iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
	$oUser = $this->user->GetById($iduser);
}
?>
<input type="hidden" name="inputFromGeolocalisation" id="inputFromGeolocalisation" value="0"/>
<input type="hidden" name="inputGeolocalisationValue" id="inputGeolocalisationValue" value="<?php if (isset($oUser)) echo $oUser->geodistance; else echo "10";?>"/>
<input type="hidden" name="inputGeoLatitude" id="inputGeoLatitude" value="0"/>
<input type="hidden" name="inputGeoLongitude" id="inputGeoLongitude" value="0"/>

  <tr>
      <td style="text-align:left">
      <span style='padding-left:20px;color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 10px;
    font-weight: 700;
    line-height: 1.05em;'>Recherche par communes</span>
      </td>
  </tr>
  <tr>
    <td>
    <select id="inputStringVilleHidden_bonplans" name="inputStringVilleHidden" style="width:250px">
        <option value="0">Toutes&nbsp;les&nbsp;communes</option>
        <?php 
        ////$this->firephp->log($toVille, 'toVille');
        foreach($toVille as $oVille){ ?>
            <option value="<?php echo $oVille->IdVille ; ?>" <?php if(isset($iVilleId) && $iVilleId == $oVille->IdVille) echo 'selected="selected"'; ?>><?php echo $oVille->Nom." (".$oVille->nb_commercant.")" ; ?></option>
        <?php } ?>
    </select>
    <!--<table width="250" border="0" style="text-align:center;" align="center">
<tbody>
<tr>
<td>
<img width="48" height="48" border="0" alt="pub" src="<?php //echo GetImagePath("front/"); ?>/wpimages2013/wp0edae9a5_06.png">
</td>
<td>
<span style='color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 10px;
    font-weight: 700;
    line-height: 1.05em;'>A chaque recherche, les catégories ci-dessous sont actualisées…</span>
</td>
</tr>
</tbody>
</table>-->
</td>
  </tr>
<tr>
    <td>
	<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/b.png" alt="" border="0"/>
    </td>
  </tr>
  <tr>
    <td>
	<div id="span_categ_list_mobile" class="span_categ_list_mobile">
    <select type="inputStringHidden_main" name="inputStringHidden_main" id="inputStringHidden_main_form_mobile" style="width:250px">
        <option value="0">Toutes les rubriques</option>
        <?php foreach($toRubriquePrincipale as $oCategorie_main){ ?>
            <option value="<?php echo $oCategorie_main->IdRubrique ; ?>"><?php echo $oCategorie_main->Nom ; ?>&nbsp;(<?php echo $oCategorie_main->nb_bonplan ; ?>)</option>
        <?php } ?>
    </select>
    </div>
    </td>
  </tr>
  <tr>
    <td>
    <div id="span_souscateg_list_mobile" class="span_souscateg_list_mobile">
    <select type="inputStringHidden_sous" name="inputStringHidden_sous" id="inputStringHidden_form_mobile" style="width:250px">
        <option value="0">Toutes les sous-rubriques</option>
        <?php foreach($toCategorie as $oCategorie){ ?>
            <option value="<?php echo $oCategorie->IdSousRubrique ; ?>"><?php echo $oCategorie->Nom; ?>&nbsp;(<?php echo $oCategorie->nb_bonplan ; ?>)</option>
        <?php } ?>
    </select>
    </div><br/>
    </td>
  </tr>
  <tr>
    <td>
    <a href="Javascript:void();" id="btn_geolocalisation_mobile">
    <img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp2b13b3ee_06.png" alt="" border="0">
    </a>
    </td>
  </tr>
  <tr>
    <td>
    <a href="Javascript:void();" id="btn_submit_search_partner_mobile">
    <img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpa5a2a996_06.png" alt="" border="0">
    </a>
    </td>
  </tr>
  
</table>
</form>
    
    
    
    
  </div>
  <div class="footer_mobile">
  	<?php 
	$data["zTitle"] = 'Annonces';
	$this->load->view("front2013/includes/mobile_footer.php", $data);
	?>
  </div>
</div>
</body>
</html>
