<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.js"></script>
<script type="text/javascript">

$(document).ready(function() {

	$("#frmRecomanderAmi").validate({
		rules: {
			zNom:  {
				required: true,
				email: true
			},
			zTelephone: "required",
			zEmail: {
				required: true,
				email: true
			},
			zCommentaire: "required"
		},
		messages: {
			zNom: "Veuillez entrer un adresse mail valide.",
			zTelephone: "Veuillez indiquer votre Nom",
			zEmail: "Veuillez entrer un adresse mail valide.",
			zCommentaire: "Veuillez iniquer votre message."
		}
		
		
	});
	
	
});

</script>

<style type="text/css">
#frmRecomanderAmi label.error {
	color:#FF0000;
	display:inline;
	margin-left:10px;
	width:auto;
}
</style>

<br/>
<!--<form name="frmNousContacter" id="frmNousContacter" action="<?php// echo site_url("front/annonce/envoiMailNousContacter"); ?>" method="post" enctype="multipart/form-data">-->
<form name="frmRecomanderAmi" id="frmRecomanderAmi" action="<?php echo site_url("front/commercant/envoiMailRecommander/" . $oInfoCommercant->IdCommercant); ?>" method="post" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="3" cellpadding="3">
  <tr>
    <td colspan="2">Contact bureau <?php echo $oInfoCommercant->TelFixe ; ?></td>
  </tr>
  <tr>
    <td colspan="2">Contact mobile &nbsp;<?php echo $oInfoCommercant->TelMobile ; ?></td>
  </tr>
  <tr>
    <td colspan="2">Email : <?php echo $oInfoCommercant->Email ; ?></td>
  </tr>
  <tr>
    <td colspan="2">Web : <?php echo $oInfoCommercant->SiteWeb ; ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<table align="center">
<tr>
    <td colspan="2"><span style="font-weight:700; font-size:29.0px; line-height:1.21em; color:#000000;">Recommander &agrave; un ami</span></td>
  </tr>
</table>

<table width="100%" align="center">
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<tr>
    <td>Email de votre ami</td>
    <td><input type="text" name="zEmail" id="zEmail"/></td>
  </tr>
  <tr>
    <td>Votre Email</td>
    <td>
    <input type="text" name="zNom" id="zNom"/>
    <input type="hidden" name="zMailTo" id="zMailTo" value="<?php echo $oInfoCommercant->Email ; ?>"/>
    </td>
  </tr>
  
  <tr>
    <td>Votre Nom</td>
    <td><input type="text" name="zTelephone" id="zTelephone"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
    <button id="submit" name="submit" type="submit" class="Button1">
    Valider
    </button>
    
   <!-- <a href="Javascript:void()" class="Button1">Valider</a>-->
    </td>
  </tr>
  <tr style="visibility:hidden;">
    <td>Commentaires</td>
    <td>
    <textarea name="zCommentaire" id="zCommentaire" cols="45" rows="5" style="width:100%;">Ceci est une recommandation de mail venant d'un de vos contacts sur Un commerant du site Club-Proximit&eacute; http://proximite-magazine.com/portail/front/annonce/ficheCommercantAnnonce/<?php echo $oInfoCommercant->IdCommercant;?> </textarea>
    </td>
  </tr>
  
</table>

</form>
