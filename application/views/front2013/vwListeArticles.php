<?php $data["zTitle"] = 'Gestion Articles'; ?>
<?php $this->load->view("admin/includes/vwHeader2013", $data); ?>

<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra'],  headers: {6: { sorter: false}, 7: {sorter: false} }})
				.tablesorterPager({container: $("#pager")});
		});
		
		function confirm_delete_article(IdRubrique){
			if (confirm("Voulez-vous supprimer cet article ?")) {
			   document.location="<?php echo site_url("front/articles/suppression/");?>/"+IdRubrique;
		   }
		}
</script>

    <div id="divAdminHome" class="content" align="center">
	     <p><a style = "color:black; text-decoration:none;" href = "<?php echo site_url("front/utilisateur/contenupro" ) ; ?>"> <input type = "button" value= "retour au menu" id = "btn" onclick="document.location='<?php echo site_url("admin/home/" );?>';"/>  </a></p>
		 <p><a style = "color:black;text-decoration:none;" href = "<?php echo site_url("front/articles/fiche/0") ;?>"> <input type = "button" value= "Ajouter Article" id = "btn"/>  </a></p>
        <br><div class="H1-C">Liste des Articles</div><br>
        
        <div style="color:#33CC33"><?php if (isset($msg)) echo $msg;?></div>
        <table cellpadding="1" class="tablesorter" style="text-align:left">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Titre</th>
                    <th>Type</th>
                    <th>Catégorie</th>
                    <th>Souscatégorie</th>
                    <th>Mise à jour</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
            <?php if(sizeof($colArticles)) { ?>
                <?php foreach($colArticles as $objArticle) { ?>
                    <tr>
                        <td>
                            <a href="<?php echo site_url("admin/articles/fiche/".$objArticle->idarticle); ?>">
                                <?php echo htmlspecialchars(stripcslashes($objArticle->idarticle)); ?>
                            </a>
                        </td>
                        <td><?php echo htmlspecialchars(stripcslashes($objArticle->title)); ?></td>
                        <td>
                        <?php 
						$this->load->model("mdl_types_article");
						$oTypeArticle = $this->mdl_types_article->getById($objArticle->article_typeid);
        				echo $oTypeArticle->article_type;
						?>
                        </td>
                        <td>
                        <?php 
						$this->load->model("mdl_categories_article");
						$oTypeArticle = $this->mdl_categories_article->getById($objArticle->article_categid);
        				echo $oTypeArticle->category;
						?>
                        </td>
                        <td>
                        <?php 
						$this->load->model("mdl_categories_article");
						$oTypeArticle = $this->mdl_categories_article->getByIdSousCateg($objArticle->article_subcategid);
        				echo $oTypeArticle->subcateg;
						?>
                        </td>
                        <td><?php echo htmlspecialchars(stripcslashes($objArticle->lastupdate)); ?></td>
                        <td>
                            <a href="<?php echo site_url("front/articles/fiche/".$objArticle->idarticle); ?>">
                                Modifier
                            </a>
                        </td>
                        <td>
                            <a href="javascript:void();" onClick="confirm_delete_article(<?php echo $objArticle->idarticle;?>);">
                                Supprimer
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
            </tbody>
        </table>
		<div id="pager" class="pager">
			<img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
			<img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
			<input type="text" class="pagedisplay"/>
			<img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
			<img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
			<select class="pagesize" style="visibility:hidden">
				<option selected="selected"  value="10">10</option>
				<option value="20">20</option>
				<option value="30">30</option>
				<option  value="40">40</option>
			</select>
		</div>
    </div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>