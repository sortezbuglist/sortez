<?php $data["zTitle"] = 'Localisation' ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Clubproximite - Annonces</title>
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0; user-zoom=fixed;"/>
<style type="text/css">
.mobile_contener_all {
	width: 320px;
	margin-right: auto;
	margin-left: auto;
}
body {
	padding:0px;
	margin:0px;
	/*background-image:url(<?php// echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpd5148546_06.jpg);*/
	background-color:#003566;
	font-family: "Arial",sans-serif;
    font-size: 11px;
    line-height: 1.27em;
}.header_mobile {
	float: left;
	width: 320px;
	position: relative;
}
.main_mobile {
	float: left;
	width: 320px;
	position: relative;
	padding-bottom:10px;
}
.footer_mobile {
	float: left;
	width: 320px;
	position: relative;
}
</style>

</head>

<body>
<div class="mobile_contener_all">
  <div class="header_mobile">
    <img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/head_mobile_court_nv.png" usemap="#map1" alt="" border="0" width="320" height="68">
  </div>
  
  
  <div class="main_mobile">

<div style='margin-top:0px; margin-bottom:10px; color: #ffffff;
font-family: "Vladimir Script",cursive;
font-size: 30px;
line-height: 30px; text-align:center;'>
Configurer la localisation
</div>


<div style="text-align:center; padding-bottom:10px; padding-top:10px;"> 
	<a href="<?php echo site_url("front/mobile/mon_compte");?>">
    <img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/btn_back_favoris.png" alt="" border="0" width="141" height="52">
    </a>
</div>

<style type="text/css">
.line_right {
	text-align:right;
	width:40%
}
.line_left {
	text-align:left;
}
</style>

<div style='text-align:center;color: #FFFFFF;
font-family: "Arial",sans-serif;
font-size: 13px;
line-height: 1.23em;'>
<form name="geolocalisation_form_user" id="geolocalisation_form_user" method="post"/>
<table border="0" cellspacing="0" cellpadding="5" style="text-align:center;" align="center">
  <tr>
    <td colspan="2">Choisissez la distance de recherche des <br/>partenaires et des évènements situés à proximité.</td>
  </tr>
  <tr>
    <td colspan="2" style="color:#00FF00">&nbsp;<?php if (isset($mssg_return) && $mssg_return!="") echo $mssg_return;?></td>
  </tr>
  <tr>
    <td class="line_right"><input type="radio" name="geodistance" id="geodistance_2" class="geodistance_2" value="2" <?php if (isset($oUser) && $oUser->geodistance=="2") echo "checked";?> onClick="javascript:document.geolocalisation_form_user.submit();"/></td>
    <td class="line_left">2 kilomètres</td>
  </tr>
  <tr>
    <td class="line_right"><input type="radio" name="geodistance" id="geodistance_5" class="geodistance_5" value="5" <?php if (isset($oUser) && $oUser->geodistance=="5") echo "checked";?> onClick="javascript:document.geolocalisation_form_user.submit();"/></td>
    <td class="line_left">5 kilomètres</td>
  </tr>
  <tr>
    <td class="line_right"><input type="radio" name="geodistance" id="geodistance_10" class="geodistance_10" value="10" <?php if (isset($oUser) && $oUser->geodistance=="10") echo "checked"; else if ($oUser->geodistance=="" || $oUser->geodistance==NULL) echo "checked";?> onClick="javascript:document.geolocalisation_form_user.submit();"/></td>
    <td class="line_left">10 kilomètres (par défaut)</td>
  </tr>
  <tr>
    <td class="line_right"><input type="radio" name="geodistance" id="geodistance_20" class="geodistance_20" value="20" <?php if (isset($oUser) && $oUser->geodistance=="20") echo "checked";?> onClick="javascript:document.geolocalisation_form_user.submit();"/></td>
    <td class="line_left">20 kilomètres</td>
  </tr>
  <tr>
    <td class="line_right"><input type="radio" name="geodistance" id="geodistance_50" class="geodistance_50" value="50" <?php if (isset($oUser) && $oUser->geodistance=="50") echo "checked";?> onClick="javascript:document.geolocalisation_form_user.submit();"/></td>
    <td class="line_left">50 kilomètres</td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
</table>
</form>
</div>

    
    
  </div>
  
  
  <div class="footer_mobile">
  	<?php 
	$data["zTitle"] = 'Localisation';
	$this->load->view("front2013/includes/mobile_footer.php", $data);
	?>
  </div>
</div>
</body>
</html>
