<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("front2013/includes/header_mini_2", $data); ?>

<link rel="stylesheet" href="inscriptionparticuliers_fichiers/wpstyles.css" type="text/css">
<script type="text/javascript">var blankSrc = "wpscripts/blank.gif";</script>
<script type="text/javascript" src="<?php echo GetJsPath("front/");?>/fields.check.js"></script>
<!--<link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/") ; ?>/style.css" />-->
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/") ; ?>/blue/style.css" />
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/jquery.ui.core.js"></script>
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/") ; ?>/demo_table_jui.css" />
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/") ; ?>/jquery-ui-1.8.4.custom.css" />
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery("#btnSinscrire").click(function(){
            var txtError = "";
            
            var txtNom = jQuery("#txtNom").val();
            if(txtNom=="") {
                txtError += "- Veuillez indiquer Votre nom <br/>";    
            }
            
            /*var txtEmail = jQuery("#txtEmail").val();
            if(!isEmail(txtEmail)) {
                txtError += "- L'adresse mail n'est pas valide. Veuillez saisir un email valide <br/>";
            }*/ 
			
			 var txtEmail_verif = jQuery('#txtEmail_verif').val();
			  if (txtEmail_verif == 1) {
				  txtError += "- Votre Email existe déjà sur notre site <br/>";
			  }
			
            // :Check if a city has been selected before validating
              var ivilleId = jQuery('#txtVille').val();
              if (ivilleId == 0) {
                  txtError += "- Vous devez sélectionner une ville <br/>";
              }
            
            var txtLogin = jQuery("#txtLogin").val();
            if(!isEmail(txtLogin)) {
                txtError += "- Votre login doit &ecirc;tre un email valide <br/>";    
            }
			
			var txtLogin_verif = jQuery('#txtLogin_verif').val();
			  if (txtLogin_verif == 1) {
				  txtError += "- Votre Login existe déjà sur notre site <br/>";
			  }
            <?php if($prmId==0) { ?>
            var txtPassword = jQuery("#txtPassword").val();
            if(txtPassword=="") {
                txtError += "- Veuillez indiquer un Password <br/>";    
            }
            
            if(jQuery("#txtPassword").val() != jQuery("#txtConfirmPassword").val()) {
                    txtError += "- Les deux mots de passe ne sont pas identiques. <br/>";
            }
            <?php } ?>
            var validationabonnement = $('#validationabonnement').val();
            //alert("coche "+validationabonnement);
            if ($('#validationabonnement').is(":checked")) {} else {
                    txtError += "- Vous devez valider les conditions générales<br/>";
            }
            
            jQuery("#divErrortxtInscription").html(txtError);
            
            if(txtError == "") {
                jQuery("#frmInscriptionParticulier").submit();
            }
        });
		
		
		/*jQuery("#txtEmail").blur(function(){
										   //alert('cool');
										   var txtEmail = jQuery("#txtEmail").val();
										   //alert('<?php //echo site_url("front/particuliers/verifier_email"); ?>' + '/' + txtEmail);
										   //jQuery(".EmailLoading").addClass("EmailLoading_show").removeClass("EmailLoading");
										   jQuery.post(
													'<?php //echo site_url("front/particuliers/verifier_email");?>',
													{ txtEmail_var: txtEmail },
													function (zReponse)
													{
														//alert (zReponse) ;
														var zReponse_html = '';
														if (zReponse == "1") {
															zReponse_html = '<span style="color:#F00; font-weight:bold;">Ce mail est d&eacute;j&agrave; utilis&eacute;</span>';
														} else { 
															zReponse_html = '<span style="color:#3C0; font-weight:bold;">Ce mail est disponible</span>';
														} 
														
														
														if (txtEmail != "") {jQuery('#divErrortxtEmail_').html(zReponse_html);}       
													    jQuery('#txtEmail_verif').val(zReponse);
													 
												   });
										   
										   jQuery(".FieldError").removeClass("FieldError");
										   //jQuery(".EmailLoading").addClass("EmailLoading").removeClass("EmailLoading_show");
										   
										   });*/
		
		
		
		
		jQuery("#txtLogin").blur(function(){
										   //alert('cool');
										   var txtLogin = jQuery("#txtLogin").val();
										   var txtEmail = txtLogin;
										   
										   var value_result_to_show = "0";
										   
										   //alert('<?php //echo site_url("front/particuliers/verifier_login"); ?>' + '/' + txtLogin);
										   //jQuery(".EmailLoading").addClass("EmailLoading_show").removeClass("EmailLoading");
										   jQuery.post(
													'<?php echo site_url("front/particuliers/verifier_login");?>',
													{ txtLogin_var: txtLogin },
													function (zReponse)
													{
														//alert (zReponse) ;
														if (zReponse == "1") {
															value_result_to_show = "1";
														}  
														
														
														if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtLogin_').html(result_to_show);      
															$('#txtLogin_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
															$('#divErrortxtLogin_').html(result_to_show);    
															$('#txtLogin_verif').val("0");
														}
														
													 
												   });
												   
												   
											$.post(
													'<?php echo site_url("front/professionnels/verifier_login_ionauth");?>',
													{ txtLogin_var_ionauth: txtLogin },
													function (zReponse_ionauth)
													{
														//alert (zReponse) ;
														if (zReponse_ionauth == "1") {
															value_result_to_show = "1";
														} 
														
														
														if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtLogin_').html(result_to_show);       
															$('#txtLogin_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
															$('#divErrortxtLogin_').html(result_to_show);    
															$('#txtLogin_verif').val("0");
														}
													 
												   });
												   
											
											jQuery.post(
													'<?php echo site_url("front/particuliers/verifier_email");?>',
													{ txtEmail_var: txtEmail },
													function (zReponse_)
													{
														//alert (zReponse) ;
														if (zReponse_ == "1") {
															value_result_to_show = "1";
														}
														
														
														if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtLogin_').html(result_to_show);      
															$('#txtEmail_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
															$('#divErrortxtLogin_').html(result_to_show);    
															$('#txtEmail_verif').val("0");
														}
														
													 
												   });
												   
												   
											$.post(
													'<?php echo site_url("front/professionnels/verifier_email_ionauth");?>',
													{ txtEmail_var_ionauth: txtEmail },
													function (zReponse_ionauth)
													{
														//alert (zReponse) ;
														//var zReponse_html_ionauth = '';
														if (zReponse_ionauth == "1") {
															value_result_to_show = "1";
														} 
														
														
														
													 	if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtLogin_').html(result_to_show);       
															$('#txtEmail_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
															$('#divErrortxtLogin_').html(result_to_show);  
															$('#txtEmail_verif').val("0");
														}
														
														
												   });
										  
										   jQuery(".FieldError").removeClass("FieldError");
										   //jQuery(".EmailLoading").addClass("EmailLoading").removeClass("EmailLoading_show");
										   
										   });
										   
		
		
		jQuery( "#txtDateNaissance" ).datepicker({
				dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
				monthNames: ['Janvier','Fevier','Mars','Avril','Mai','Juin','Juiller','Aout','Septembre','Octobre','Novembre','Decembre'],
				dateFormat: 'yy-mm-dd',
				autoSize: true
			});
		
    })
	function getCP(){
	      var ivilleId = jQuery('#txtVille').val();     
          jQuery.get(
            '<?php echo site_url("front/particuliers/getPostalCode"); ?>' + '/' + ivilleId,
            function (zReponse)
            {
                // alert (zReponse) ;
                jQuery('#trReponseVille').html(zReponse) ;       
               
             
           });
	}
	 
</script>




<div style="text-align:center;">

<div style="margin-top:30px; margin-bottom:20px;">
<?php if($prmId!=0) {?>
	<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp046ded7f_06.png" alt="" style="text-align:center">
<?php } else { ?>
	<?php if (CURRENT_SITE == "agenda"){ ?>
		<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcb0bbca8_06.png" alt="" style="text-align:center"><br/>
        <img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp35dbb3fe_06.png" alt="" style="text-align:center">
    <?php } else { ?>
    	<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpa2e2bebd_06.png" alt="" style="text-align:center">
    <?php } ?>
<?php } ?>
</div>
<div style="margin-top:20px; margin-bottom:20px;">
<a href="<?php echo site_url("front/utilisateur/menuconsommateurs"); ?>" style="text-decoration:none;">
<input id="butn_106" type="button" value="Retour ver le menu" style="width:253px; height:22px;">
</a>
</div>

<form name="frmInscriptionParticulier" id="frmInscriptionParticulier" action="<?php echo site_url("front/particuliers/ajouter"); ?>" method="POST" enctype="multipart/form-data">
<div id="txt_586" style="text-align:center;background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages2013/bg_page_conso_pro_souri.png); background-repeat:no-repeat; background-position:center bottom; padding-bottom:150px;">

			<center>
<table id="table_form_inscriptionpartculier" width="475" cellpadding="0" cellspacing="5">
                
                <?php if($prmId!=0) { 
                echo '<tr>
                    <td colspan="2">
                    <p>
                        <span style="font-family: Arial;    font-size: 16px;    font-weight: 700;    line-height: 1.25em;">Bienvenue </span>';
						
						if ($oParticulier->Nom == 0) echo "Mr "; 
						if ($oParticulier->Nom == 1) echo "Mme ";
						if ($oParticulier->Nom == 2) echo "Mlle ";
						echo $oParticulier->Nom.", ";
                        echo $oParticulier->Adresse.", ";
                        echo $oParticulier->CodePostal.", ";
						$obj_ville = $this->mdlville->getVilleById($oParticulier->IdVille);
                        if ($obj_ville) echo $obj_ville->Nom; 
                   echo '</p>
                    </td>
                </tr>';
				
				} 
				?>
                
                <tr>
                    <td class="td_part_form">
                        <label>Nom *: </label>
                    </td>
                    <td class="td_part_form">
                        <input type="text" name="Particulier[Nom]" style = "width:273px;" id="txtNom" value="<?php if( isset ($oParticulier)) { echo $oParticulier->Nom; }?>" />
                    </td>
                </tr>
                
                <tr>
                    <td class="td_part_form">
                        <label>Pr&eacute;nom : </label>
                    </td>
                    <td class="td_part_form">
                        <input type="text" name="Particulier[Prenom]" style = "width:273px;" id="txtPrenom" value="<?php if( isset ($oParticulier)) { echo $oParticulier->Prenom; }?>" />
                    </td>
                </tr>
                
                <tr>
                    <td class="td_part_form">
                        <label>Date de naissance : </label>
                    </td>
                    <td class="td_part_form">
                        <input style = "width:273px;" type="text" name="Particulier[DateNaissance]" id="txtDateNaissance" value="<?php if( isset ($oParticulier)) { echo $oParticulier->DateNaissance; }?>" />
                    </td>
                </tr>
                
                <tr>
                    <td class="td_part_form">
                        <label>Civilit&eacute; : </label>
                    </td>
                    <td class="td_part_form">
                        <select style = "width:280px;" name="Particulier[Civilite]" id="txtParticulier">
                            <option value="0">Monsieur</option>
                            <option value="1">Madame</option>
                            <option value="2">Mademoiselle</option>
                        </select>
                    </td>
                </tr>
                
                <!--<tr>
                    <td class="td_part_form">
                        <label>Profession : </label>
                    </td>
                    <td class="td_part_form">
                        <input type="text" name="Particulier[Profession]" style = "width:273px;" id="txtProfession" value="<?php //if( isset ($oParticulier)) { echo $oParticulier->Profession; }?>" />
                    </td>
                </tr>-->
                
                <tr>
                    <td class="td_part_form">
                        <label>Adresse : </label>
                    </td>
                    <td class="td_part_form">
                        <textarea name="Particulier[Adresse]" style = "width:273px;" id="txtAdresse"><?php if( isset ($oParticulier)) { echo $oParticulier->Adresse; }?></textarea>
                    </td>
                </tr>
                
                <tr>
                    <td class="td_part_form">
                        <label>Ville *: </label>
                    </td>
                    <td class="td_part_form">
                        <select name="Particulier[IdVille]" id="txtVille" style = "width:280px;" onchange ="getCP();">
                            <option value="0">-- Veuillez choisir --</option>
                            <?php if(sizeof($colVilles)) { ?>
                                <?php foreach($colVilles as $objVille) { ?>
                                    <option value="<?php echo $objVille->IdVille; ?>" <?php if( isset ($oParticulier) && $oParticulier->IdVille == $objVille->IdVille) { ?>selected="selected"<?php } ?>><?php echo htmlspecialchars(stripcslashes($objVille->Nom)); ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        <div class="FieldError" id="divErrorCity"></div>
                    </td>
                </tr>
                
                <tr>
                    <td class="td_part_form">
                        <label>Code postal : </label>
                    </td>
                     <td id ="trReponseVille" name = "trReponseVille"  class="td_part_form">
                        <input type="text" name="Particulier[CodePostal]" id="txtCodePostal" style = "width:273px;" value="<?php if( isset ($oParticulier)) { echo $oParticulier->CodePostal; }?>" />
                    </td>
                </tr>
				
                
                <tr>
                    <td class="td_part_form">
                        <label>N° T&eacute;l&eacute;phone fixe : </label>
                    </td>
                    <td class="td_part_form">
                        <input type="text" name="Particulier[Telephone]" id="txtTelephone" style = "width:273px;" value="<?php if( isset ($oParticulier)) { echo $oParticulier->Telephone; }?>" />
                    </td>
                </tr>
                
                <!--<tr>
                    <td class="td_part_form">
                        <label>N° T&eacute;l&eacute;phone Portable : </label>
                    </td>
                    <td class="td_part_form">
                        <input type="text" name="Particulier[Portable]" style = "width:273px;" id="txtPortable" value="<?php //if( isset ($oParticulier)) { echo $oParticulier->Portable; }?>" />
                    </td>
                </tr>
                
                <tr>
                    <td class="td_part_form">
                        <label>N° de Fax : </label>
                    </td>
                    <td class="td_part_form">
                        <input type="text" name="Particulier[Fax]" style = "width:273px;" id="txtFax" value="<?php //if( isset ($oParticulier)) { echo $oParticulier->Fax; }?>" />
                    </td>
                </tr>
                
                <tr>
                    <td class="td_part_form">
                        <label>Email *: </label>
                    </td>
                    <td class="td_part_form">
                        <input type="text" name="Particulier[Email]" style = "width:273px;" id="txtEmail" value="<?php //if( isset ($oParticulier)) { echo $oParticulier->Email; }?>"/>
                        <div class="FieldError" id="divErrortxtEmail_"></div>
                        
					</td>
                </tr>-->
                
                <tr>
                    <td class="td_part_form">
                        <label>Login *: </label>
                    </td>
                    <td class="td_part_form">
                        <input type="text" name="Particulier[Login]" style = "width:273px;" id="txtLogin" value="<?php if( isset ($oParticulier)) { echo $oParticulier->Login; }?>" />
                        <div class="FieldError" id="divErrortxtLogin_"></div>
                        <!--<img src="<?php echo GetImagePath("front/"); ?>/loading.gif" class="LoginLoading" title="" alt="loading" onload="OnLoadPngFix()"/>-->
                        <input type="hidden" name="txtLogin_verif" style = "width:273px;" id="txtLogin_verif" value="0"/>
                        <input type="hidden" name="txtEmail_verif" style = "width:273px;" id="txtEmail_verif" value="0"/>
					</td>
                </tr>
                <?php if($prmId==0) { ?>
                <tr>
                    <td class="td_part_form">
                        <label>Password *: </label>
                    </td>
                    <td class="td_part_form">
                        <input type="password" name="Particulier_Password" style = "width:273px;" id="txtPassword" value="" />
                    </td>
                </tr>
                
                <tr>
                    <td class="td_part_form">
                        <label>Confirm Password *: </label>
                    </td>
                    <td class="td_part_form">
                        <input type="password" id="txtConfirmPassword" style = "width:273px;" value="" />
						<div class="FieldError" id="divErrortxtPassword"></div>
                    </td>
                </tr>
                <?php } ?>
                
                
                <input type="hidden" name="Particulier[IdUser]" style = "width:273px;" id="IdUser" value="<?php if( isset ($oParticulier)) { echo $oParticulier->IdUser; } else echo "0";?>" />
                <!--<tr>
                    <td class="td_part_form"></td>
                    <td align="left"><?php if(!isset($oParticulier) || $oParticulier->IdUser == 0) { ?><input type="button" id="btnSinscrire"  value="S'inscrire" /><?php } ?>  </td>
                </tr>-->
                
            </table>
</center>

<div>
<center>
<table width="450" border="0">
  <tr>
    <td><input id="validationabonnement" style=" <?php if($prmId!=0) echo 'visibility:hidden;';?>" name="check_3" value="1" type="checkbox" <?php if($prmId!=0) echo 'checked="checked"'; ?>  /></td>
    <td>
    <?php if($prmId==0) { ?>
    <div id="txt_587" style="overflow:hidden; padding:0px 0px 0px 2px; text-align: left">
    <p class="Corps-P">
    <span class="Corps-C-C0">Je déclare être majeur et accepter les conditions générales du Club Proximité.</span>
    <span class="Corps-C-C0">Vous recevez par retour de mail, la confirmation de votre adhésion ainsi que la validation
        de votre identifiant et mot de passe.</span>
    <span class="Corps-C-C0">L’abonnement à notre newsletter est automatique.</span></p>
    </div>
    <?php } ?>
    </td>
  </tr>
</table>
</center>

</div>

<div id="divErrortxtInscription" style="padding:0px 0px 0px 2px; font-family: Arial; font-size: 12px; color:#F00;">
</div>

<div style="margin-top:20px; margin-bottom:30px;">
<a id="btnSinscrire" href="Javascript:void();" style="text-decoration:none;">
<input type="button" value="<?php if($prmId==0) {?>Je valide mon inscription<?php } else { ?>Je modifie mes donn&eacute;es<?php } ?>" style="width:253px; height:22px;">
</a>
</div>

<?php if (CURRENT_SITE == "agenda"){ ?>
<div>
<a href="<?php echo site_url("front/utilisateur/conditionsgenerales"); ?>" style="text-decoration:none;">
<input id="butn_10sdqs6" type="button" value="Conditions Générales" style="width:253px; height:22px;">
</a>
</div>
<?php } ?>

</div>

</form>

</div>

<?php $this->load->view("front2013/includes/footer_mini_2"); ?>