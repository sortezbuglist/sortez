<?php $data["zTitle"] = 'Commercant' ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Clubproximite - Partenaires</title>
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0; user-zoom=fixed;"/>
<style type="text/css">
.mobile_contener_all {
	width: 320px;
	margin-right: auto;
	margin-left: auto;
}
body {
	padding:0px;
	margin:0px;
	/*background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpd5148546_06.jpg);*/
	background-color:#003566;
	font-family: "Arial",sans-serif;
    font-size: 11px;
    line-height: 1.27em;
	color:#FFFFFF;
}.header_mobile {
	float: left;
	width: 320px;
	position: relative;
}
.main_mobile {
	float: left;
	width: 320px;
	position: relative;
	padding-bottom:10px;
}
.footer_mobile {
	float: left;
	width: 320px;
	position: relative;
}
</style>

</head>

<body>
<div class="mobile_contener_all">
  <div class="header_mobile">
    <img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/head_mobile_court_nv.png" usemap="#map1" alt="" border="0" width="320" height="68">
  </div>
  <div class="main_mobile">

<div style='color: #ffffff;
font-family: "Vladimir Script",cursive;
font-size: 28px; margin-bottom:10px;
line-height: 30px; text-align:center;'>
<!--L'annuaire des professionnels-->
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/a.PNG" alt="" border="0"/>
</div>

<div style="padding-bottom:10px;"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:center;">
  <tr>
    <td><a href="<?php echo site_url("front/mobile/recherchepartenaires/") ; ?>"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/btn_nv_search_mobile.png" usemap="#map1" alt="" border="0" width="142" height="50"></a></td>
    <td><a href="<?php echo site_url("front/mobile/fonctionnement_club/") ; ?>"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/btn_more_info_mobile.png" usemap="#map1" alt="" border="0" width="142" height="50"></a></td>
  </tr>
</table>
</div>


<!--<table width="100%" border="0">
  <tr>
    <td><img src="<?php// echo GetImagePath("front/"); ?>/wpimages2013/mobile/ligne_details_partner.png" height="3" alt="line" width="310" /></td>
  </tr>
</table>-->
<div style="height:3px; width:100%; background-color:#FFFFFF; margin-bottom:5px;"></div>

  	
<?php  foreach($toCommercant as $oCommercant){ 

		$objasscommrubr = $this->sousRubrique->GetById($oCommercant->IdSousRubrique);
		
		$oInfoCommercant = $oCommercant;
		$base_path_system = str_replace('system/', '', BASEPATH);
		include($base_path_system.'application/views/front2013/includes/url_var_definition.php');
		//$this->load->view("front2013/includes/url_var_definition", $data); 
		?>
		
		<a href="<?php echo site_url($commercant_url_home);?>" style="color:#fff; text-decoration:none;">
		<table width="100%" border="0">
              <tr>
                
                <td valign="top" width="120">
                    <?php 
                    $image_home_vignette = "";
                    if (isset($oCommercant->PhotoAccueil) && $oCommercant->PhotoAccueil != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->PhotoAccueil)==true){ $image_home_vignette = $oCommercant->PhotoAccueil;}
                    else if (isset($oCommercant->Photo1) && $oCommercant->Photo1 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo1)==true){ $image_home_vignette = $oCommercant->Photo1;}
                    else if (isset($oCommercant->Photo2) && $oCommercant->Photo2 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo2)==true){ $image_home_vignette = $oCommercant->Photo2;}
                    else if (isset($oCommercant->Photo3) && $oCommercant->Photo3 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo3)==true){ $image_home_vignette = $oCommercant->Photo3;}
                    else if (isset($oCommercant->Photo4) && $oCommercant->Photo4 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo4)==true){ $image_home_vignette = $oCommercant->Photo4;}
                    else if (isset($oCommercant->Photo5) && $oCommercant->Photo5 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo5)==true){ $image_home_vignette = $oCommercant->Photo5;}
                    ?>
                    <img src="<?php if ($image_home_vignette != ""){ echo GetImagePath("front/photoCommercant/"); ?>/<?php echo $image_home_vignette ; }else{echo GetImagePath("front/")."/wp71b211d2_06.png";}?>" width="120" border="0" alt="<?php echo $commercant_url_nom;?>">
                </td>
                
                <td valign="top">
                <strong>
                <?php if ($objasscommrubr) echo $objasscommrubr->Nom ; ?><br />
                <?php echo $oCommercant->NomSociete ; ?><br /></strong>
                <?php echo $oCommercant->ville ; ?>, <?php echo $oCommercant->Adresse2 ; ?>, <?php echo $oCommercant->Adresse1 ; ?><br />
                
                </td>
                <td valign="middle" width="22">
                    <!--<img src="<?php// echo GetImagePath("front/"); ?>/wpimages2013/mobile/btn_fleche_details_partner.png" width="16" alt="partenaire" />-->
                    <img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/flechebleu.png" width="22" alt="partenaire" />
                </td>
              </tr>
          </table>
        </a>
          <!--<table width="100%" border="0">
              <tr>
                <td><img src="<?php// echo GetImagePath("front/"); ?>/wpimages2013/mobile/ligne_details_partner.png" height="3" alt="line" width="310" /></td>
              </tr>
            </table>-->
		<div style="height:3px; width:100%; background-color:#FFFFFF;"></div>
		
		<?php
		
	}
?>
    
<!--<div style="text-align:right; font-size:12px;">
<br/>
  <?php														
            /*if($iNombreLiens != 1){
                ?>Pages : <?php
                $iPageCourante = $argOffset ;
                for($i=1; $i<=$iNombreLiens; $i++){
                    $argOffset = ($i - 1 ) * $PerPage ;*/
                    ?>
                    <?php //if($iPageCourante == $argOffset){ ?>
                        <span style="cursor:pointer; color:red; font-weight: bold;" onclick="document.location='<?php //echo site_url("accueil/redirection/". $argOffset) ; ?>';">
                            <?php //echo $i;?>
                        </span>&nbsp;
                    <?php //}else{?>
                        <span style="cursor:pointer;text-decoration: underline" onclick="document.location='<?php //echo site_url("accueil/redirection/". $argOffset) ; ?>';">
                            <?php //echo $i;?>
                        </span>&nbsp;
                    <?php //}?>
                    <?php
               /* }
            }*/
	?>
  </div>--> 

<?php if (isset($links_pagination) && $links_pagination!="") {?>    
<style type="text/css">
#view_pagination_ci strong {
	color:#FF0000;
	font-weight:bold;
	background-color: #6C8599;
	padding: 5px 15px;
}
#view_pagination_ci a {
	background-color: #6C8599;
    color: #FFFFFF;
    padding: 5px 15px;
}
</style>
<div id="view_pagination_ci" style="text-align:right; font-size:14px; height:20px; vertical-align:central; padding-top:5px; padding-right:0px; margin-top:5px;">
<!--<span style="font-size:12px;">Pages : </span>--><?php echo $links_pagination; ?>
</div> 
<?php }?> 
  
    
  </div>
  <div class="footer_mobile">
  	<?php 
	$data["zTitle"] = 'Identification';
	$this->load->view("front2013/includes/mobile_footer.php", $data);
	?>
  </div>
</div>
</body>
</html>
