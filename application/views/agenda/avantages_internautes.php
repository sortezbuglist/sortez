<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="fr">

<!-- Mirrored from www.proximite-magazine.com/agenda-avril/informations.html by HTTrack Website Copier/3.x [XR&CO'2010], Mon, 06 May 2013 15:18:45 GMT -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Informations</title>
<meta name="Generator" content="Serif WebPlus X6">
<meta name="viewport" content="width=650">
<meta name="viewport" content="width=device-width, maximum-scale=1.0" />
 <!--Master Page Head-->
<style type="text/css">
body{margin:0;padding:0;}
.Corps-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:justify; font-weight:400;
}
.Corps-P-P0
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:left; font-weight:400;
}
.H22-P
{
    margin:0.0px 0.0px 7.0px 0.0px; text-align:left; font-weight:400;
}
.Corps-P-P1
{
    margin:0.0px 0.0px 5.0px 0.0px; text-align:left; font-weight:400;
}
.Corps-P-P2
{
    margin:0.0px 0.0px 12.0px 0.0px; text-align:justify; font-weight:400;
}
.Corps-C
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
.H22-C
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:15.0px; line-height:1.20em;
}
.Corps-C-C0
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:15.0px; line-height:1.20em;
}
.Corps-C-C1
{
    font-family:"Arial", sans-serif; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C2
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C3
{
    font-family:"Verdana", sans-serif; font-size:12.0px; line-height:1.17em;
}
.Corps-C-C4
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ff0000; font-size:15.0px; line-height:1.20em;
}
</style>
<script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/agenda/wpscripts/jspngfix.js"></script>
<link rel="stylesheet" href="wpscripts/wpstyles.css" type="text/css"><script type="text/javascript">
var blanksrc="<?php echo GetImagePath("front/"); ?>/agenda/wpscripts/blank.gif";
</script>
</head>

<body text="#000000" style="background:transparent url('<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp9e1de8e1_05_06.jpg') repeat scroll top center; height:4000px;">
<div style="background-color:#ffffff;margin-left:auto;margin-right:auto;position:relative;width:650px;height:4000px;">
<map id="map0" name="map0">
    <area shape="rect" coords="481,36,651,59" href="<?php echo site_url("front/utilisateur/sessionp");?>" alt="">
    <area shape="rect" coords="479,8,651,31" href="<?php echo site_url("front/utilisateur/session/");?>" alt="">
    <area shape="poly" coords="624,105,588,105,606,117" href="<?php echo site_url("front/contact");?>" alt="">
    <area shape="poly" coords="630,143,634,137,634,93,628,88,584,88,578,96,578,140,582,144,630,144" href="<?php echo site_url("front/contact");?>" alt="">
    <area shape="poly" coords="559,143,563,137,563,93,557,88,513,88,507,96,507,140,511,144,559,144" href="<?php echo base_url();?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp440da234_06.png" border="0" width="650" height="178" id="grp_4025" alt="" onload="OnLoadPngFix()" usemap="#map0" style="position:absolute;left:0px;top:0px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpc2f5d1f4_06.png" border="0" width="98" height="88" id="meta_1" alt="" onload="OnLoadPngFix()" style="position:absolute;left:522px;top:1072px;">
<input type="button" style="position:absolute; left:334px; top:322px; width:219px; height:22px;" id="butn_90" value="Retour vers le menu général ">
<a href="javascript:history.back()"><img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp5533b116.gif" border="0" width="219" height="21" id="hs_114" title="" alt="Retour (historique)" style="position:absolute;left:334px;top:322px;"></a>
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp0265327b_06.png" border="0" width="276" height="188" id="pic_2122" alt="" onload="OnLoadPngFix()" style="position:absolute;left:0px;top:198px;">
<map id="map1" name="map1">
    <area shape="rect" coords="481,36,651,59" href="<?php echo site_url("front/utilisateur/sessionp");?>" alt="">
    <area shape="rect" coords="479,8,651,31" href="<?php echo site_url("front/utilisateur/session/");?>" alt="">
    <area shape="poly" coords="624,105,588,105,606,117" href="<?php echo site_url("front/contact");?>" alt="">
    <area shape="poly" coords="630,143,634,137,634,93,628,88,584,88,578,96,578,140,582,144,630,144" href="<?php echo site_url("front/contact");?>" alt="">
    <area shape="poly" coords="559,143,563,137,563,93,557,88,513,88,507,96,507,140,511,144,559,144" href="<?php echo base_url();?>" alt="">
    <area shape="rect" coords="481,36,651,59" href="<?php echo site_url("front/utilisateur/sessionp");?>" alt="">
    <area shape="rect" coords="479,8,651,31" href="<?php echo site_url("front/utilisateur/session/");?>" alt="">
    <area shape="poly" coords="624,105,588,105,606,117" href="<?php echo site_url("front/contact");?>" alt="">
    <area shape="poly" coords="630,143,634,137,634,93,628,88,584,88,578,96,578,140,582,144,630,144" href="<?php echo site_url("front/contact");?>" alt="">
    <area shape="poly" coords="559,143,563,137,563,93,557,88,513,88,507,96,507,140,511,144,559,144" href="<?php echo base_url();?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpc088006a_06.png" border="0" width="650" height="178" id="grp_4135" alt="" onload="OnLoadPngFix()" usemap="#map1" style="position:absolute;left:0px;top:0px;">
<div id="txt_212" style="position:absolute;left:28px;top:386px;width:592px;height:269px;overflow:hidden;">
<p class="Corps-P"><span class="Corps-C">Madame, Monsieur,</span></p>
<p class="Corps-P"><span class="Corps-C"><br></span></p>
<p class="Corps-P"><span class="Corps-C">Cet agenda est une réalisation du magazine Proximité.</span></p>
<p class="Corps-P"><span class="Corps-C"><br></span></p>
<p class="Corps-P"><span class="Corps-C">vous trouverez sur cet agenda plus de 300 manifestations référencées.</span></p>
<p class="Corps-P"><span class="Corps-C"><br></span></p>
<p class="Corps-P"><span class="Corps-C">En ouvrant un compte en qualité d’internaute vous accédez à certains avantages dont
    vous trouverez le détail ci-<wbr>dessous.</span></p>
<p class="Corps-P"><span class="Corps-C"><br></span></p>
<p class="Corps-P"><span class="Corps-C">Si vous organisez un événement, &nbsp;vous pouvez ouvrir un compte (voir ci-<wbr>dessous le
    détail des différents abon-<wbr>nements et modules), déposer et modifier en temps réel
    vos données.</span></p>
<p class="Corps-P-P0"><span class="Corps-C">Nous vous en souhaitons une bonne utilisation.</span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp484168d4_06.png" border="0" width="630" height="42" id="pcrv_19204" alt="" onload="OnLoadPngFix()" style="position:absolute;left:10px;top:580px;">
<div id="art_304" style="position:absolute;left:28px;top:589px;width:218px;height:25px;white-space:nowrap;">
    <div class="H22-P">
        <span class="H22-C">Les avantages des internautes</span></div>
</div>
<div id="txt_209" style="position:absolute;left:28px;top:630px;width:605px;height:38px;overflow:hidden;">
<p class="Corps-P-P0"><span class="Corps-C-C0">Des recherches multicritères</span></p>
</div>
<div id="txt_214" style="position:absolute;left:28px;top:655px;width:592px;height:96px;overflow:hidden;">
<p class="Corps-P-P1"><span class="Corps-C-C1">Grâce aux recherches multicritères (catégories, dates, communes) &nbsp;les internautes
    accédent aux &nbsp;données des différentes manifestations déposées en temps réel par les
    associations, les collectivités locales et les organisateurs événements situés entre
    Saint-<wbr>Tropez et menton</span></p>
<p class="Wp-Corps-P"><span class="Corps-C-C1">Ces données sont variées : informations texte, photos, vidéos, documents PDF, localisation
    de l’événement, des liens (web, facebook, twitter), des tarifs, des réservations
    en ligne…)</span></p>
<p class="Wp-Corps-P"><span class="Corps-C-C1"><br></span></p>
</div>
<div id="txt_211" style="position:absolute;left:28px;top:751px;width:370px;height:23px;overflow:hidden;">
<p class="Corps-P-P0"><span class="Corps-C-C0">Newsletters, agenda et favoris</span></p>
</div>
<div id="txt_151" style="position:absolute;left:28px;top:774px;width:592px;height:100px;overflow:hidden;">
<p class="Corps-P-P1"><span class="Corps-C-C1">Les internautes ouvrent un compte gratuitement et :</span></p>
<p class="Corps-P-P1"><span class="Corps-C-C1">-<wbr>s’abonnent gratuitement à une Newsletter hebdomadaire.</span></p>
<p class="Corps-P-P1"><span class="Corps-C-C1">-<wbr> configurent un agenda personnalisé correspondants à leurs souhaits.</span></p>
<p class="Corps-P-P1"><span class="Corps-C-C1">-<wbr> retrouvent les organisateurs d’événements qu’ils ont préalablement mis en favoris
    lors des dernières consultations</span></p>
</div>
<div id="txt_210" style="position:absolute;left:28px;top:876px;width:384px;height:22px;overflow:hidden;">
<p class="Corps-P-P0"><span class="Corps-C-C0">Un site multilingue</span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpffdb5ce9_06.png" border="0" width="101" height="62" id="meta_6" alt="" onload="OnLoadPngFix()" style="position:absolute;left:519px;top:892px;">
<div id="txt_152" style="position:absolute;left:28px;top:903px;width:487px;height:40px;overflow:hidden;">
<p class="Corps-P-P0"><span class="Corps-C-C1">D’un simple clic les visiteurs accèdent dans la langue de leur choix aux différents
    événements.</span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp484168d4_06.png" border="0" width="630" height="42" id="pcrv_22207" alt="" onload="OnLoadPngFix()" style="position:absolute;left:10px;top:1038px;">
<div id="art_2374" style="position:absolute;left:28px;top:1047px;width:488px;height:25px;white-space:nowrap;">
    <div class="H22-P">
        <span class="H22-C">Les différents abonnements pour les diffuseurs : l’abonnement Basic</span></div>
</div>
<div id="txt_165" style="position:absolute;left:28px;top:1087px;width:494px;height:64px;overflow:hidden;">
<p class="Corps-P-P2"><span class="Corps-C-C1">Ils souscrivent un abonnement gratuitement qui leur permettent de référencer les
    principales données «&nbsp;texte&nbsp;» de leurs événements.</span></p>
<p class="Corps-P-P2"><span class="Corps-C-C1">Ce compte est personnalisé et sécurisé.</span></p>
</div>
<div id="txt_167" style="position:absolute;left:29px;top:1175px;width:592px;height:46px;overflow:hidden;">
<p class="Corps-P-P2"><span class="Corps-C-C1">Ils créent, modifient, suppriment en ligne et en temps réel &nbsp;les informations sur
    chacun de leurs événements (date, lieu, description, tarifs, coordonnées, liens sociaux,
    plan de localisation...).</span></p>
</div>
<a href="<?php echo site_url("front/particuliers/inscription/");?>"><img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp884a7537_06.png" border="0" width="350" height="41" id="art_271" title="" alt="Inscription des particuliers&#10;" onload="OnLoadPngFix()" style="position:absolute;left:158px;top:958px;"></a>
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp484168d4_06.png" border="0" width="630" height="42" id="pcrv_22208" alt="" onload="OnLoadPngFix()" style="position:absolute;left:10px;top:1240px;">
<div id="art_2375" style="position:absolute;left:28px;top:1249px;width:511px;height:25px;white-space:nowrap;">
    <div class="H22-P">
        <span class="H22-C">Les différents abonnements pour les diffuseurs : l’abonnement Premium</span></div>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp5d5e610c_06.png" border="0" width="102" height="71" id="meta_9" alt="" onload="OnLoadPngFix()" style="position:absolute;left:25px;top:1290px;">
<div id="txt_168" style="position:absolute;left:153px;top:1295px;width:467px;height:73px;overflow:hidden;">
<p class="Corps-P-P2"><span class="Corps-C">Ils pouvent à tout moment </span><span class="Corps-C-C2">“upgrader”</span><span class="Corps-C"> leur abonnement de base vers un abonnement Premium.</span></p>
<p class="Corps-P-P2"><span class="Corps-C">Ce dernier leur permet d’accéder à des services complémentaires.</span></p>
<p class="Corps-P-P2"><span class="Corps-C-C3"><br></span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp3e466a1b_06.png" border="0" width="96" height="91" id="meta_10" alt="" onload="OnLoadPngFix()" style="position:absolute;left:28px;top:1377px;">
<div id="txt_169" style="position:absolute;left:152px;top:1384px;width:468px;height:70px;overflow:hidden;">
<p class="Wp-Corps-P" style="margin:0px; padding:0px;"><span class="Corps-C">Téléchargement d’une affiche, d’un document (prospectus, dépliant). </span></p>
<p class="Corps-P-P2"><span class="Corps-C">Intégration d’une galerie photos, d’une vidéo (youtube, dailymotion), d’un fichier
    audio.</span></p>
</div>
<div id="txt_160" style="position:absolute;left:155px;top:1523px;width:465px;height:95px;overflow:hidden;">
<p class="Corps-P-P2"><span class="Corps-C">Extractions personnalisées et automatiques d’événements choisis (diffuseurs, catégories,
    &nbsp;villes) et intégration d’un simple clic &nbsp;sur votre site web (iframe).</span></p>
<p class="Corps-P-P2"><span class="Corps-C">Le diffuseur décide de la couleur des cadres et boutons.. </span></p>
<p class="Corps-P-P2"><span class="Corps-C"><br></span></p>
<p class="Corps-P-P2"><span class="Corps-C-C3"><br></span></p>
</div>
<div id="txt_202" style="position:absolute;left:152px;top:1474px;width:468px;height:38px;overflow:hidden;">
<p class="Corps-P-P0"><span class="Corps-C-C4">Intégration d’un simple clic d’événements choisis</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C4">sur votre site internet</span></p>
</div>
</div>
</body>

<!-- Mirrored from www.proximite-magazine.com/agenda-avril/informations.html by HTTrack Website Copier/3.x [XR&CO'2010], Mon, 06 May 2013 15:18:51 GMT -->
</html>