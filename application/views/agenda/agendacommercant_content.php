<?php
$thisss =& get_instance();
$thisss->load->library('ion_auth');
$this->load->model("ion_auth_used_by_club");
$user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);
if (isset($user_ion_auth_id)) $user_groups = $thisss->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;
?>

<?php $data['pagetitle'] = 'Agenda';?>
<?php $this->load->view("agenda/partner_agenda_form_filtre", $data); ?>

<p>&nbsp;</p>
<p>&nbsp;</p>
 
<div style="height:3px; width:100%; background-color:#000000"></div>
          
<?php  foreach($toAgenda as $oAgenda){ ?>

      <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-left:15px; margin-top:7px; margin-bottom:3px;">
          <tr>
            
            <td style="width:110px">
            <a  target='_blank' href="<?php echo site_url($oInfoCommercant->nom_url."/details_agenda/".$oAgenda->id); ?>">
            	<?php 
				$image_home_vignette = "";
				if (isset($oAgenda->photo1) && $oAgenda->photo1 != "" && is_file("application/resources/front/images/agenda/photoCommercant/".$oAgenda->photo1)==true){ $image_home_vignette = $oAgenda->photo1;}
				else if ($image_home_vignette == "" && isset($oAgenda->photo2) && $oAgenda->photo2 != "" && is_file("application/resources/front/images/agenda/photoCommercant/".$oAgenda->photo2)==true){ $image_home_vignette = $oAgenda->photo2;}
				else if ($image_home_vignette == "" && isset($oAgenda->photo3) && $oAgenda->photo3 != "" && is_file("application/resources/front/images/agenda/photoCommercant/".$oAgenda->photo3)==true){ $image_home_vignette = $oAgenda->photo3;}
				else if ($image_home_vignette == "" && isset($oAgenda->photo4) && $oAgenda->photo4 != "" && is_file("application/resources/front/images/agenda/photoCommercant/".$oAgenda->photo4)==true){ $image_home_vignette = $oAgenda->photo4;}
				else if ($image_home_vignette == "" && isset($oAgenda->photo5) && $oAgenda->photo5 != "" && is_file("application/resources/front/images/agenda/photoCommercant/".$oAgenda->photo5)==true){ $image_home_vignette = $oAgenda->photo5;}
				////$this->firephp->log($image_home_vignette, 'image_home_vignette');
				
				//showing category img if all image of agenda is null
				$this->load->model("mdl_categories_agenda");
				$toCateg_for_agenda = $this->mdl_categories_agenda->getById($oAgenda->agenda_categid);
				if ($image_home_vignette == "" && isset($toCateg_for_agenda->images) && $toCateg_for_agenda->images != "" && is_file("application/resources/front/images/agenda/category/".$toCateg_for_agenda->images)==true){ 
					echo '<img src="'.GetImagePath("front/").'/agenda/category/'.$toCateg_for_agenda->images.'" width="110"/>';
				} else {
				
					if ($image_home_vignette != ""){
						$img_photo_split_array = explode('.',$image_home_vignette);
						$img_photo_path = "application/resources/front/images/agenda/photoCommercant/".$img_photo_split_array[0]."_thumb_385_255.".$img_photo_split_array[1];
						if (is_file($img_photo_path)==false) {
							echo image_thumb("application/resources/front/images/agenda/photoCommercant/" . $image_home_vignette, 385, 255,'','');
						} else echo '<img src="'.GetImagePath("front/").'/agenda/photoCommercant/'.$img_photo_split_array[0]."_thumb_385_255.".$img_photo_split_array[1].'" width="110"/>';
						
					} else {
						$image_home_vignette_to_show = GetImagePath("front/")."/wp71b211d2_06.png";
						echo '<img src="'.$image_home_vignette_to_show.'" width="110" height="150"/>';
					}
				
				}
				?>
                
            </a>
            </td>
            
            <td valign="top" style="padding-left:15px; padding-right:15px;">
            <span style="font-size:12px;">
            <strong>
            <?php echo $oAgenda->category ; ?><br />
            <?php echo $oAgenda->nom_manifestation ; ?><br />
            </strong>
            <?php echo $oAgenda->ville ; ?>, <?php echo $oAgenda->adresse_localisation ; ?>, <?php echo $oAgenda->codepostal_localisation ; ?><br />
            <?php 
			if ($oAgenda->date_debut == $oAgenda->date_fin) echo "Le ".translate_date_to_fr($oAgenda->date_debut); 
			else echo "Du ".translate_date_to_fr($oAgenda->date_debut)." au ".translate_date_to_fr($oAgenda->date_fin);
			?>
            </span>
            
            <div style="padding-top:30px;"><?php echo truncate(strip_tags($oAgenda->description),150,$etc = " ... (suite) ...")?></div>
            </td>
            <td style="width:80px" valign="bottom">
            	<table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td height="48"><a href="<?php echo site_url($oInfoCommercant->nom_url."/details_agenda/".$oAgenda->id); ?>" title="D&eacute;tails"><img src="<?php echo GetImagePath("front/"); ?>/agenda/btn_info_agenda_parner.png" width="58" height="58" alt="agenda" /></a></td>
                  </tr>
                </table>
            </td>
          </tr>
      </table><div style="height:3px; width:100%; background-color:#000000"></div>
<?php  } ?>


<?php if (isset($links_pagination) && $links_pagination!="") {?>
<style type="text/css">
#view_pagination_ci strong {
	color:#FF0000;
	font-weight:bold;
}
</style>
<div id="view_pagination_ci" style="text-align:right; font-size:14px; height:20px; vertical-align:central; padding-top:5px; padding-right:20px;">
<span style="font-size:12px;">Pages : </span><?php echo $links_pagination; ?>
</div>
<?php }?>

