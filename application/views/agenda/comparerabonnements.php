<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="fr">

<!-- Mirrored from www.proximite-magazine.com/agenda-avril/comparaison-offres.html by HTTrack Website Copier/3.x [XR&CO'2010], Mon, 06 May 2013 15:19:41 GMT -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Comparaison des Offres</title>
<meta name="Generator" content="Serif WebPlus X6">
<meta name="viewport" content="width=1024">
<style type="text/css">
body{margin:0;padding:0;}
.Corps-P
{
    margin:0.0px 0.0px 12.0px 0.0px; text-align:center; font-weight:400;
}
.Corps-artistique-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:center; font-weight:400;
}
.Normal-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:center; font-weight:400;
}
.Corps-P-P0
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:center; font-weight:400;
}
.Corps-P-P1
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:left; font-weight:400;
}
.Corps-C
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:13.0px; line-height:1.23em;
}
.Corps-artistique-C
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C0
{
    font-family:"Verdana", sans-serif; font-size:16.0px; line-height:1.13em;
}
.Corps-artistique-C-C0
{
    font-family:"Arial", sans-serif; color:#ffffff; font-size:18.0px; line-height:1.22em; letter-spacing:4.00px;
}
.Corps-C-C1
{
    font-family:"Arial", sans-serif; font-size:18.0px; line-height:1.22em; letter-spacing:4.00px;
}
.Corps-artistique-C-C1
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:16.0px; line-height:1.25em;
}
.Normal-C
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C2
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C3
{
    font-family:"Arial", sans-serif; color:#ffffff; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C4
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C5
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:15.0px; line-height:1.20em;
}
.Corps-C-C6
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:16.0px; line-height:1.25em;
}
.Corps-C-C7
{
    font-family:"Arial", sans-serif; font-weight:700; color:#ffffff; font-size:24.0px; line-height:1.25em;
}
.Button1,.Button1:link,.Button1:visited{background-image:url('<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpe3e8533a_06.png');background-position:0px 0px;text-decoration:none;display:block;position:absolute;}
.Button1:focus{outline-style:none;}
.Button1:hover{background-position:0px -30px;}
.Button1 span,.Button1:link span,.Button1:visited span{color:#000000;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:none;font-style:normal;left:22px;top:7px;width:106px;height:15px;font-size:12px;display:block;position:absolute;cursor:pointer;}
a:hover .Corps-C {
	color:#000;
}
</style>
<script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/agenda/wpscripts/jspngfix.js"></script>
<link rel="stylesheet" href="<?php echo GetImagePath("front/"); ?>/agenda/wpscripts/wpstyles.css" type="text/css">
<script type="text/javascript">
var blanksrc="<?php echo GetImagePath("front/"); ?>/agenda/wpscripts/blank.gif";
</script>
</head>

<body text="#000000" style="background:transparent url('<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp9e1de8e1_05_06.jpg') repeat scroll top center; height:3000px;">
<div style="background-color:#ffffff;margin-left:auto;margin-right:auto;position:relative;width:1024px;height:3000px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpc869abe1_06.png" border="0" width="1024" height="246" id="grp_3978" alt="" onload="OnLoadPngFix()" style="position:absolute;left:0px;top:0px;">
<div id="txt_750" style="position:absolute;left:419px;top:224px;width:193px;height:20px;overflow:hidden;">
<p class="Corps-P"><span class="Corps-C">Le fonctionnement</span></p>
</div>
<div id="txt_747" style="position:absolute;left:10px;top:224px;width:198px;height:24px;overflow:hidden;">
<p class="Corps-artistique-P"><span class="Corps-artistique-C">Manifestations à la une</span></p>
<p class="Wp-Corps-P"><span class="Corps-C-C0"><br></span></p>
</div>
<div id="txt_961" style="position:absolute;left:10px;top:181px;width:1004px;height:28px;overflow:hidden;">
<p class="Corps-artistique-P"><span class="Corps-artistique-C-C0">Animations -<wbr> Cinéma -<wbr> Théâtres -<wbr> Concerts -<wbr> Spectacles -<wbr> Musées -<wbr> Salons -<wbr> Loisirs</span></p>
<p class="Corps-P"><span class="Corps-C-C1"><br></span></p>
</div>
<div id="txt_748" style="position:absolute;left:217px;top:224px;width:193px;height:24px;overflow:hidden;">
<p class="Corps-P"><span class="Corps-C">Recherchez sur l’agenda</span></p>
</div>
<div id="txt_751" style="position:absolute;left:620px;top:221px;width:193px;height:20px;overflow:hidden;">
<p class="Corps-P"><span class="Corps-C">Espace particulier</span></p>
</div>
<a href="<?php echo site_url("front/utilisateur/sessionp");?>"><img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp5533b116.gif" border="0" width="193" height="31" id="hs_17" title="" alt="espace-pro" style="position:absolute;left:816px;top:212px;"></a>
<a href="<?php echo site_url("front/utilisateur/sessionp");?>">
<div id="txt_749" style="position:absolute;left:816px;top:223px;width:193px;height:20px;overflow:hidden;">
<p class="Corps-P"><span class="Corps-C">Espace Professionnel</span></p>
</div>
</a>
<a href="<?php echo site_url("front/utilisateur/session/");?>"><img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp5533b116.gif" border="0" width="196" height="32" id="hs_178" title="" alt="menu basic" style="position:absolute;left:611px;top:214px;"></a>
<a href="<?php echo base_url();?>"><img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp5533b116.gif" border="0" width="187" height="24" id="hs_174" title="" alt="agenda-une" style="position:absolute;left:15px;top:222px;"></a>
<a href="<?php echo site_url("agenda/liste");?>"><img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp5533b116.gif" border="0" width="203" height="29" id="hs_9" title="" alt="agenda-menu" style="position:absolute;left:208px;top:217px;"></a>
<map id="map0" name="map0">
    <area shape="poly" coords="185,0,2,0,0,29,184,29" href="<?php echo site_url('agenda/avantages_internautes');?>" alt="">
</map>
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp5533b116.gif" border="0" width="185" height="29" id="hs_177" title="" alt="informations" usemap="#map0" style="position:absolute;left:419px;top:219px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp86feebb5_06.png" border="0" width="213" height="30" id="qs_3251" alt="" onload="OnLoadPngFix()" style="position:absolute;left:811px;top:16px;">
<div id="art_2010" style="position:absolute;left:821px;top:23px;width:173px;height:20px;white-space:nowrap;">
    <div class="Wp-Corps-artistique-P">
        <span class="Corps-artistique-C-C1">Le magazine Proximité</span></div>
</div>
<a href="http://www.proximite-magazine.com/"><img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp5533b116.gif" border="0" width="209" height="26" id="hs_38" alt="" style="position:absolute;left:811px;top:18px;"></a>
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp86feebb5_06.png" border="0" width="213" height="30" id="qs_3252" alt="" onload="OnLoadPngFix()" style="position:absolute;left:811px;top:63px;">
<div id="art_2011" style="position:absolute;left:821px;top:67px;width:135px;height:20px;white-space:nowrap;">
    <div class="Wp-Corps-artistique-P">
        <span class="Corps-artistique-C-C1">Le Club Proximité</span></div>
</div>

<a href="<?php echo base_url();?>"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpd985cd97_06.png" border="0" width="74" height="74" id="hs_39" title="" alt="menu-professionnels" style="position:absolute; left:729px; top:105px;"></a>
<a href="<?php echo site_url("front/contact");?>"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpb6e45b55_06.png" border="0" width="74" height="74" id="hs_39" title="" alt="menu-professionnels" style="position:absolute; left:800px; top:105px;"></a>
<a href="<?php echo base_url();?>"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpd42f72d0_06.png" border="0" width="74" height="74" id="hs_39" title="" alt="menu-professionnels" style="position:absolute; left:870px; top:105px;"></a>
<a href="<?php echo base_url();?>"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp75dbfae0_06.png" border="0" width="74" height="74" id="hs_39" title="" alt="menu-professionnels" style="position:absolute; left:945px; top:105px;"></a>



<a href="http://www.club-proximite.com/"><img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp5533b116.gif" border="0" width="209" height="35" id="hs_39" alt="" style="position:absolute;left:815px;top:58px;"></a>
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp78e6edc9_06.png" border="0" width="1021" height="108" id="qs_1846" alt="" onload="OnLoadPngFix()" style="position:absolute;left:0px;top:2892px;">
<div style="position:absolute;left:15px;top:2916px;width:994px;height:73px;">
    <div class="Normal-P">
        <span class="Normal-C">Le &nbsp;Club Proximité est une marque déposée et une réalisation du Magazine Proximité<br></span></div>
    <div class="Corps-P">
        <span class="Corps-C-C2"> Editeur : Phase SARL 60 avenue de Nice 06800 Cagnes-<wbr>sur-<wbr>Mer &nbsp;</span><span class="Corps-C-C3">Sarl au capital de 7265 € -<wbr> RCS Antibes 412.071.466<br></span><span class="Corps-C-C4">Téléphone : 00 33 (0)4 93 73 84 51 -<wbr> Mobile : 00 33 (0)6 72 05 59 35 -<wbr> proximite-<wbr>magazine@orange.fr</span></div>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp83c3ac49_06.png" border="0" width="163" height="993" id="qs_1947" alt="" onload="OnLoadPngFix()" style="position:absolute;left:846px;top:380px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp74585ce0_06.png" border="0" width="163" height="984" id="qs_1945" alt="" onload="OnLoadPngFix()" style="position:absolute;left:679px;top:380px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp59facc1c_06.png" border="0" width="463" height="316" id="grp_3517" alt="" onload="OnLoadPngFix()" style="position:absolute;left:0px;top:287px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp0f1f3a41_06.png" border="0" width="994" height="45" id="pic_2980" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:1330px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp1bded783_06.png" border="0" width="994" height="30" id="qs_1950" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:1178px;">
<div id="txt_874" style="position:absolute;left:674px;top:272px;width:335px;height:99px;overflow:hidden;">
<p class="Corps-P-P0"><span class="Corps-C-C5">Comparez nos différents abonnements</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C5">pour découvrir*</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C5">Celui qui vous convient le mieux.</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C5">Tarification annuelle ht.</span></p>
<p class="Corps-P"><span class="Corps-C"><br></span></p>
<p class="Corps-P"><span class="Corps-C"><br></span></p>
<p class="Corps-P"><span class="Corps-C"><br></span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp1bded783_06.png" border="0" width="994" height="30" id="qs_1920" alt="" onload="OnLoadPngFix()" style="position:absolute;left:16px;top:607px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp1bded783_06.png" border="0" width="994" height="30" id="qs_1921" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:645px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp1bded783_06.png" border="0" width="994" height="30" id="qs_1922" alt="" onload="OnLoadPngFix()" style="position:absolute;left:16px;top:682px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp1bded783_06.png" border="0" width="994" height="30" id="qs_1924" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:720px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp1bded783_06.png" border="0" width="994" height="30" id="qs_1926" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:758px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp1bded783_06.png" border="0" width="994" height="30" id="qs_1927" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:795px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp3d357852_06.png" border="0" width="994" height="40" id="pic_2955" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:869px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp1bded783_06.png" border="0" width="994" height="30" id="qs_1929" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:1216px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp1bded783_06.png" border="0" width="994" height="30" id="qs_1931" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:1253px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp2689ab8e_06.png" border="0" width="993" height="30" id="qs_1933" alt="" onload="OnLoadPngFix()" style="position:absolute;left:16px;top:954px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp1bded783_06.png" border="0" width="994" height="30" id="qs_1935" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:992px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp1bded783_06.png" border="0" width="994" height="30" id="qs_1936" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:917px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp1bded783_06.png" border="0" width="994" height="30" id="qs_1937" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:832px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp1bded783_06.png" border="0" width="994" height="30" id="qs_1938" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:1292px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp1bded783_06.png" border="0" width="994" height="30" id="qs_1932" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:1066px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp1bded783_06.png" border="0" width="994" height="30" id="qs_2071" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:1104px;">
<div id="txt_904" style="position:absolute;left:688px;top:393px;width:146px;height:134px;overflow:hidden;">
<p class="Corps-P-P0"><span class="Corps-C-C6">ABONNEMENT</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C6">annuel</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C6">AGENDA BASIC</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C7">GRATUIT</span></p>
<p class="Corps-P"><span class="Corps-C-C7"><br></span></p>
</div>
<div id="txt_905" style="position:absolute;left:851px;top:393px;width:153px;height:142px;overflow:hidden;">
<p class="Corps-P-P0"><span class="Corps-C-C6">ABONNEMENT</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C6">annuel</span></p>
<p class="Corps-P-P0"><span class="Corps-C-C6">AGENDA PREMIUM </span><span class="Corps-C-C7">500 € ht</span></p>
<p class="Corps-P"><span class="Corps-C-C2"><br></span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp1bded783_06.png" border="0" width="994" height="30" id="qs_2075" alt="" onload="OnLoadPngFix()" style="position:absolute;left:16px;top:1141px;">
<a href="<?php echo site_url("front/utilisateur/infospremium/");?>" id="btn_253" class="Button1" style="position:absolute;left:686px;top:502px;width:149px;height:30px;"><span>Plus&nbsp;d&#39;infos&nbsp;</span></a>
<a href="<?php echo site_url("front/utilisateur/infospremium/");?>" id="btn_254" class="Button1" style="position:absolute;left:852px;top:502px;width:149px;height:30px;"><span>Plus&nbsp;d&#39;infos&nbsp;</span></a>
<a href="<?php echo site_url("front/professionnels/inscription/basic");?>" id="btn_248" class="Button1" style="position:absolute;left:686px;top:543px;width:149px;height:30px;"><span>Je&nbsp;m&#39;inscris&nbsp;</span></a>
<a href="<?php echo site_url("front/professionnels/inscription/premium");?>" id="btn_249" class="Button1" style="position:absolute;left:852px;top:543px;width:149px;height:30px;"><span>Je&nbsp;m&#39;inscris&nbsp;</span></a>
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2924" alt="" onload="OnLoadPngFix()" style="position:absolute;left:745px;top:606px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2925" alt="" onload="OnLoadPngFix()" style="position:absolute;left:745px;top:643px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2926" alt="" onload="OnLoadPngFix()" style="position:absolute;left:745px;top:685px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpadd68924_06.png" border="0" width="32" height="64" id="pic_2964" alt="" onload="OnLoadPngFix()" style="position:absolute;left:745px;top:961px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2928" alt="" onload="OnLoadPngFix()" style="position:absolute;left:745px;top:919px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2930" alt="" onload="OnLoadPngFix()" style="position:absolute;left:745px;top:834px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2931" alt="" onload="OnLoadPngFix()" style="position:absolute;left:745px;top:796px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2932" alt="" onload="OnLoadPngFix()" style="position:absolute;left:745px;top:760px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2934" alt="" onload="OnLoadPngFix()" style="position:absolute;left:745px;top:718px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2950" alt="" onload="OnLoadPngFix()" style="position:absolute;left:911px;top:606px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2951" alt="" onload="OnLoadPngFix()" style="position:absolute;left:911px;top:643px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2952" alt="" onload="OnLoadPngFix()" style="position:absolute;left:911px;top:685px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpadd68924_06.png" border="0" width="32" height="64" id="pic_2967" alt="" onload="OnLoadPngFix()" style="position:absolute;left:911px;top:961px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2954" alt="" onload="OnLoadPngFix()" style="position:absolute;left:911px;top:919px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2956" alt="" onload="OnLoadPngFix()" style="position:absolute;left:911px;top:834px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2957" alt="" onload="OnLoadPngFix()" style="position:absolute;left:911px;top:796px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2958" alt="" onload="OnLoadPngFix()" style="position:absolute;left:911px;top:760px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2960" alt="" onload="OnLoadPngFix()" style="position:absolute;left:911px;top:718px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2962" alt="" onload="OnLoadPngFix()" style="position:absolute;left:911px;top:1104px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2966" alt="" onload="OnLoadPngFix()" style="position:absolute;left:911px;top:1030px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2969" alt="" onload="OnLoadPngFix()" style="position:absolute;left:911px;top:1217px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2970" alt="" onload="OnLoadPngFix()" style="position:absolute;left:911px;top:1179px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2971" alt="" onload="OnLoadPngFix()" style="position:absolute;left:911px;top:1141px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2974" alt="" onload="OnLoadPngFix()" style="position:absolute;left:911px;top:1292px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2975" alt="" onload="OnLoadPngFix()" style="position:absolute;left:911px;top:1252px;">
<div id="txt_1357" style="position:absolute;left:35px;top:614px;width:384px;height:18px;overflow:hidden;">
<p class="Wp-Corps-P"><span class="Corps-C">Gestion de vos données sur un compte personnel et sécurisé</span></p>
</div>
<div id="txt_1358" style="position:absolute;left:35px;top:651px;width:712px;height:17px;overflow:hidden;">
<p class="Wp-Corps-P"><span class="Corps-C">Dépôts illimités de vos événements (effacement automatique 15 jours après la date
    de l’événement</span></p>
</div>
<div id="txt_1359" style="position:absolute;left:35px;top:687px;width:457px;height:33px;overflow:hidden;">
<p class="Wp-Corps-P"><span class="Corps-C">Création, modification, suppression de vos données</span></p>
</div>
<div id="txt_1360" style="position:absolute;left:35px;top:926px;width:434px;height:15px;overflow:hidden;">
<p class="Wp-Corps-P"><span class="Corps-C">Description de l’événement</span></p>
</div>
<div id="txt_1361" style="position:absolute;left:35px;top:1073px;width:477px;height:17px;overflow:hidden;">
<p class="Wp-Corps-P"><span class="Corps-C">La présence de votre événement sur notre application mobile</span></p>
</div>
<div id="txt_1362" style="position:absolute;left:35px;top:766px;width:391px;height:18px;overflow:hidden;">
<p class="Wp-Corps-P"><span class="Corps-C">Présence du titre de votre événement</span></p>
</div>
<div id="txt_1363" style="position:absolute;left:35px;top:805px;width:437px;height:15px;overflow:hidden;">
<p class="Wp-Corps-P"><span class="Corps-C">Présence de la date de début et de fin de votre événement</span></p>
</div>
<div id="txt_1364" style="position:absolute;left:35px;top:842px;width:445px;height:15px;overflow:hidden;">
<p class="Wp-Corps-P"><span class="Corps-C">Présence du lieu et d’une carte de localisation</span></p>
</div>
<div id="txt_1365" style="position:absolute;left:35px;top:876px;width:557px;height:33px;overflow:hidden;">
<p class="Wp-Corps-P"><span class="Corps-C">Présence des coordonnées du diffuseur (si celui-<wbr>ci n’est pas l’organisateur)</span></p>
</div>
<div id="txt_1366" style="position:absolute;left:35px;top:959px;width:455px;height:33px;overflow:hidden;">
<p class="Wp-Corps-P"><span class="Corps-C">Présence d’une image correspondante au thème choisi</span></p>
</div>
<div id="txt_1368" style="position:absolute;left:32px;top:1299px;width:480px;height:17px;overflow:hidden;">
<p class="Wp-Corps-P"><span class="Corps-C">Extraction de vos sélections et intégration d’un fichier HTML sur votre site</span></p>
</div>
<div id="txt_1369" style="position:absolute;left:40px;top:1110px;width:472px;height:18px;overflow:hidden;">
<p class="Wp-Corps-P"><span class="Corps-C">Présence d’une galerie photos &nbsp;(Nombre maximum 5)</span></p>
</div>
<div id="txt_1370" style="position:absolute;left:35px;top:1149px;width:461px;height:16px;overflow:hidden;">
<p class="Wp-Corps-P"><span class="Corps-C">Présence d’un lien pour une réservation en ligne</span></p>
</div>
<div id="txt_1371" style="position:absolute;left:35px;top:1187px;width:433px;height:16px;overflow:hidden;">
<p class="Wp-Corps-P"><span class="Corps-C">Présence d’un lien pour un fichier vidéo (You Tube, Dailymotion)</span></p>
</div>
<div id="txt_1372" style="position:absolute;left:35px;top:1224px;width:450px;height:15px;overflow:hidden;">
<p class="Wp-Corps-P"><span class="Corps-C">Téléchargement d’une plaquette en PDF</span></p>
</div>
<div id="txt_1373" style="position:absolute;left:33px;top:1260px;width:616px;height:33px;overflow:hidden;">
<p class="Corps-P-P1"><span class="Corps-C">Présence d’un lien pour un fichier audio (MP4)</span></p>
</div>
<div id="txt_1376" style="position:absolute;left:35px;top:727px;width:465px;height:14px;overflow:hidden;">
<p class="Wp-Corps-P"><span class="Corps-C">Présence du thème de votre événement</span></p>
</div>
<div id="txt_1378" style="position:absolute;left:35px;top:1000px;width:570px;height:15px;overflow:hidden;">
<p class="Wp-Corps-P"><span class="Corps-C">La présence d’un lien pour recommander cet événement à un ami, lien d’alerte et favoris</span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp1bded783_06.png" border="0" width="994" height="30" id="qs_3329" alt="" onload="OnLoadPngFix()" style="position:absolute;left:15px;top:1029px;">
<div id="txt_1379" style="position:absolute;left:35px;top:1037px;width:427px;height:15px;overflow:hidden;">
<p class="Wp-Corps-P"><span class="Corps-C">Présence de vos liens sociaux (Facebook, Twitter, Google+)</span></p>
</div>
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2976" alt="" onload="OnLoadPngFix()" style="position:absolute;left:745px;top:1030px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2977" alt="" onload="OnLoadPngFix()" style="position:absolute;left:911px;top:1030px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2978" alt="" onload="OnLoadPngFix()" style="position:absolute;left:745px;top:1067px;">
<img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpcff7a732_06.png" border="0" width="32" height="32" id="pic_2979" alt="" onload="OnLoadPngFix()" style="position:absolute;left:911px;top:1067px;">
<div id="txt_1460" style="position:absolute;left:32px;top:1336px;width:565px;height:51px;overflow:hidden;">
<p class="Wp-Corps-P"><span class="Corps-C">Intégration automatique de votre agenda sur les pages web et mobile du Club Proximité
    si vous avez souscris à un abonnement premium ou platinium</span></p>
<p class="Wp-Corps-P"><span class="Corps-C"><br></span></p>
</div>
</div>
</body>

<!-- Mirrored from www.proximite-magazine.com/agenda-avril/comparaison-offres.html by HTTrack Website Copier/3.x [XR&CO'2010], Mon, 06 May 2013 15:19:55 GMT -->
</html>