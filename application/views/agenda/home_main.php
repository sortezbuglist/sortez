<?php $data["zTitle"] = 'Accueil' ?>


<?php 
if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) { 
	
	if (isset($from_mobile_search_page_partner) && $from_mobile_search_page_partner == '1') {
		$this->load->view("agenda/home_mobile_list_partner", $data);
	} else {
		$this->load->view('agenda/home_menu_mobile', $data);
	}
	
} else { 

	$this->load->view("agenda/home", $data);

} 
?>
