<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("front2013/includes/header_mini_2", $data); ?>

<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.6.4.js"></script>

	<script type="text/javascript">
	
		 
		 
		 // Use jQuery via $(...)
		 $(document).ready(function(){//debut ready fonction
			   
			   
			   //To show sousrubrique corresponding to rubrique
			   $("#RubriqueSociete").change(function(){
                                var irubId = $("#RubriqueSociete").val();
                                //alert(irubId);
								$.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>front/professionnels/testAjax/"+irubId,
                                    success: function(msg){
                                        //alert(msg);
                                        var numero_reponse = msg;
                                        $("#trReponseRub").html(numero_reponse);
                                        //alert(numero_reponse);
                                    }
                                    });
                                    //alert ("test "+$("#RubriqueSociete").val());
                                                                    
                           });
			   
			   
			   $("#EmailSociete").blur(function(){
										   //alert('cool');
										   //var result_to_show = "";
										   var value_result_to_show = "0";
										   
										   var txtEmail = $("#EmailSociete").val();
										   //alert('<?php //echo site_url("front/professionnels/verifier_email"); ?>' + '/' + txtEmail);
										   //jQuery(".EmailLoading").addClass("EmailLoading_show").removeClass("EmailLoading");
										   $.post(
													'<?php echo site_url("front/professionnels/verifier_email");?>',
													{ txtEmail_var: txtEmail },
													function (zReponse)
													{
														//alert (zReponse) ;
														//var zReponse_html = '';
														if (zReponse == "1") {
															value_result_to_show = "1";
														}
														
														
														if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce mail est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtEmail_').html(result_to_show);       
															$('#txtEmail_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce mail est disponible</span>';
															$('#divErrortxtEmail_').html(result_to_show);    
															$('#txtEmail_verif').val("0");
														}
													 
												   });
											
											$.post(
													'<?php echo site_url("front/professionnels/verifier_email_ionauth");?>',
													{ txtEmail_var_ionauth: txtEmail },
													function (zReponse_ionauth)
													{
														//alert (zReponse) ;
														//var zReponse_html_ionauth = '';
														if (zReponse_ionauth == "1") {
															value_result_to_show = "1";
														} 
														
														
														
													 	if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce mail est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtEmail_').html(result_to_show);       
															$('#txtEmail_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce mail est disponible</span>';
															$('#divErrortxtEmail_').html(result_to_show);    
															$('#txtEmail_verif').val("0");
														}
														
														
												   });
												   
												   
											
											
												   
										   
										   //jQuery(".FieldError").removeClass("FieldError");
										   //jQuery(".EmailLoading").addClass("EmailLoading").removeClass("EmailLoading_show");
										   
										   });
			   
			   
			   $("#txtLogin").blur(function(){
										   //alert('cool');
										   var txtLogin = $("#txtLogin").val();
										   
										   var value_result_to_show = "0";
										   
										   //alert('<?php //echo site_url("front/professionnels/verifier_login"); ?>' + '/' + txtEmail);
										   //jQuery(".EmailLoading").addClass("EmailLoading_show").removeClass("EmailLoading");
										   $.post(
													'<?php echo site_url("front/professionnels/verifier_login");?>',
													{ txtLogin_var: txtLogin },
													function (zReponse)
													{
														//alert (zReponse) ;
														var zReponse_html = '';
														if (zReponse == "1") {
															value_result_to_show = "1";
														} 
														
														
														if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtLogin_').html(result_to_show);       
															$('#txtLogin_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
															$('#divErrortxtLogin_').html(result_to_show);    
															$('#txtLogin_verif').val("0");
														}
													 
												   });
										   
										   
										   $.post(
													'<?php echo site_url("front/professionnels/verifier_login_ionauth");?>',
													{ txtLogin_var_ionauth: txtLogin },
													function (zReponse_ionauth)
													{
														//alert (zReponse) ;
														var zReponse_html_ionauth = '';
														if (zReponse_ionauth == "1") {
															value_result_to_show = "1";
														} 
														
														
														if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtLogin_').html(result_to_show);       
															$('#txtLogin_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
															$('#divErrortxtLogin_').html(result_to_show);    
															$('#txtLogin_verif').val("0");
														}
													 
												   });
										   
										   
										   //jQuery(".FieldError").removeClass("FieldError");
										   //jQuery(".EmailLoading").addClass("EmailLoading").removeClass("EmailLoading_show");
										   
										   });
			
			   
			   //To show postal code automatically
			   $("#VilleSociete").change(function(){
								var irubId = $("#VilleSociete").val();
                                //alert(irubId);
								$.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>front/professionnels/getPostalCode/"+irubId,
                                    success: function(msg){
                                        //alert(msg);
                                        var numero_reponse = msg;
                                        $("#CodePostalSociete").val(numero_reponse);
                                        //alert(numero_reponse);
                                    }
                                    });
                                    //alert ("test "+$("#VilleSociete").val());
				});
			   
		/*
		var valabonnementht = 0;
		var valmoduleht = 0;
		var totalmontantht = 0;
		var montanttva = 0;
		var valtva = 0.196;
		var montantttc = 0;
		
		function calcmontantht(valabonnementht, valmoduleht){
			totalmontantht = valabonnementht + valmoduleht;
			$("#divMontantHT").html(totalmontantht+"€");
			calcmontanttva (totalmontantht, valtva);
			calcmontantttc (totalmontantht, montanttva);
			}
		
		function calcmontanttva (totalmontantht, valtva){
			montanttva = totalmontantht * valtva;
			$("#divMontantTVA").html(_roundNumber(montanttva,2)+"€");
		}
		
		//limit decimal
		function _roundNumber(num,dec) {
			
			return (parseFloat(num)).toFixed(dec);
		}
		
		function calcmontantttc (totalmontantht, montanttva){
			montantttc = totalmontantht + montanttva;
			$("#divMontantTTC").html(montantttc+"€");
			$("#montantttcvalue_abonnement").val(montantttc);
		}
		
			
		
		//show subscription value and label
		   //$("#AbonnementSociete").change(function(){
							var irubId = $("#AbonnementSociete").val();
							//alert(irubId);
							//$("#divAbonnementdesc").html($(this).find("option:selected").text());
							
                                                        $.post(
                                                        '<?php// echo base_url();?>front/utilisateur/get_tarif_abonnement/',
                                                            { 
                                                                IdAbonnement: irubId
                                                            },
                                                            function (zReponse)
                                                            {
                                                                valabonnementht = parseInt(zReponse);
                                                                calcmontantht(valabonnementht, valmoduleht);
																$("#divAbonnementmontant").html(zReponse+"€");
																$("#divAbonnementmontant2").html(zReponse+"€");
                                                            });
                                                        
			//}); 
			*/
		
		//show module subscription value and label
		   /*$("#nb_module_nb_page").change(function(){
							var irubId = $("#nb_module_nb_page").val();
							//alert(irubId);
							$("#divModuledesc").html($(this).find("option:selected").text());
							if (irubId == 0) {
								valmoduleht = 0;
								calcmontantht(valabonnementht, valmoduleht);
								$("#divModulemontant").html("");
								$("#divModuledesc").html("");
							}
							if (irubId == 1) {
								valmoduleht = 400;
								calcmontantht(valabonnementht, valmoduleht);
								$("#divModulemontant").html("400€");
							}
							if (irubId == 2) {
								valmoduleht = 800;
								calcmontantht(valabonnementht, valmoduleht);
								$("#divModulemontant").html("800€");
							}
							if (irubId == 3) {
								valmoduleht = 1200;
								calcmontantht(valabonnementht, valmoduleht);
								$("#divModulemontant").html("1200€");
							}
							if (irubId == 4) {
								valmoduleht = 1600;
								calcmontantht(valabonnementht, valmoduleht);
								$("#divModulemontant").html("1600€");
							}
							if (irubId == 5) {
								valmoduleht = 2000;
								calcmontantht(valabonnementht, valmoduleht);
								$("#divModulemontant").html("2000€");
							}
							
			});*/ 
			   
			   
			   
		//verify field form value 
		$("#btnSinscrire").click(function(){
										  
				var txtError = "";
				/*var EmailSociete = $("#EmailSociete").val();
				if(!isEmail(EmailSociete)) {
					$("#divErrorEmailSociete").html("Cet email n'est pas valide. Veuillez saisir un email valide");
					$("#divErrorEmailSociete").show();
					txtError += "1";
				} else {
					$("#divErrorEmailSociete").hide();
				}*/
				// :Check if a city has been selected before validating
				
				var RubriqueSociete = $('#RubriqueSociete').val();
				  if (RubriqueSociete == "") {
					txtError += "- Vous devez préciser votre activité<br/>";
				  }  
				
				var SousRubriqueSociete = $('#SousRubriqueSociete').val();
				  if (SousRubriqueSociete == "0") {
					txtError += "- Vous devez préciser une sous-rubrique<br/>";
				  } 
				
				var NomSociete = $('#NomSociete').val();
				  if (NomSociete == "") {
					txtError += "- Vous devez préciser le Nom ou enseigne<br/>";
				  }
				
				var ivilleId = $('#VilleSociete').val();
				  if (ivilleId == 0) {
					txtError += "- Vous devez sélectionner une ville<br/>";
				  }
				
				var CodePostalSociete = $('#CodePostalSociete').val();
				  if (CodePostalSociete == "") {
					txtError += "- Vous devez préciser le code postal<br/>";
				  }
				
				var EmailSociete = $("#EmailSociete").val();
				if(!isEmail(EmailSociete)) {
					txtError += "- Veuillez indiquer un email valide.<br/>";
				} 
				
				var txtEmail_verif = $("#txtEmail_verif").val();
				if(txtEmail_verif==1) {
					txtError += "- Votre Email existe déjà sur notre site <br/>";
				} 
				
				
				var NomResponsableSociete = $('#NomResponsableSociete').val();
				  if (NomResponsableSociete == "") {
					txtError += "- Vous devez préciser le Nom du Decideur <br/>";
				  }
				
				var PrenomResponsableSociete = $('#PrenomResponsableSociete').val();
				  if (PrenomResponsableSociete == "") {
					txtError += "- Vous devez préciser le Prenom du Decideur <br/>";
				  }
				  
				var ResponsabiliteResponsableSociete = $('#ResponsabiliteResponsableSociete').val();
				  if (ResponsabiliteResponsableSociete == "") {
					txtError += "- Vous devez préciser la responsabilité du Decideur <br/>";
				  }
				
				var TelDirectResponsableSociete = $('#TelDirectResponsableSociete').val();
				  if (TelDirectResponsableSociete == "") {
					txtError += "- Vous devez préciser le numero de téléphone du Decideur <br/>";
				  }
				
				var Email_decideurResponsableSociete = $("#Email_decideurResponsableSociete").val();
				if(!isEmail(Email_decideurResponsableSociete)) {
					txtError += "- Veuillez indiquer un email valide pour le decideur.<br/>";
				} 
				
				
				var AbonnementSociete = $('#AbonnementSociete').val();
				  if (AbonnementSociete == "0") {
					txtError += "- Vous devez choisir votre abonnement<br/>";
				  }  
				
				var activite1Societe = $('#activite1Societe').val();
				  if (activite1Societe == "") {
					txtError += "- Vous devez décrire votre activité<br/>";
				  }
				
				var txtLogin = $("#txtLogin").val();
				if(!isEmail(txtLogin)) {
					txtError += "- Votre login doit &ecirc;tre un email valide.<br/>";
				} 
				
				
				var txtLogin_verif = $("#txtLogin_verif").val();
				if(txtLogin_verif==1) {
					txtError += "- Votre Login existe déjà sur notre site <br/>";
				}
				
				var passs = $('#txtPassword').val();
				  if (passs == "") {
					txtError += "- Vous devez spécifier un mot de passe<br/>";
				  }
				
				if($("#txtPassword").val() != $("#txtConfirmPassword").val()) {
					txtError += "- Les deux mots de passe ne sont pas identiques.<br/>";
				}
				
				var validationabonnement = $('#validationabonnement').val();
				//alert("coche "+validationabonnement);
				if ($('#validationabonnement').is(":checked")) {} else {
					txtError += "- Vous devez valider les conditions générales<br/>";
				}
				
				
				if ($('#idreferencement0').is(":checked")) {
					$("#idreferencement").val("1");
				} else {
					$("#idreferencement").val("0");
				}
				//alert ("test "+$('#idreferencement').val());
				
				//verify captcha
				var captcha = $('#captcha').val();
				  if (captcha == "") {
					txtError += "- Vous devez remplir le captcha<br/>";
				  } else {
					  $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>front/professionnels/verify_captcha/"+captcha,
                                    success: function(msg){
                                        //alert(msg);
                                        var numero_reponse = msg;
                                        //alert(numero_reponse);
										$("#divCaptchavalueverify").val(numero_reponse);
                                    }
                             });
                             
				  }
				  
				 var divCaptchavalueverify = $('#divCaptchavalueverify').val();
				  if (divCaptchavalueverify == "0") {
					txtError += "- Les Textes que vous avez entré ne sont pas valides.<br/>";
				  } 
				// end verify captcha  
				  
				
				//final verification of input error
				if(txtError == "") {
					$("#frmInscriptionProfessionnel").submit();
				} else {
					$("#divErrorFrmInscriptionProfessionnel").html(txtError);
				}
			})
		
		
		
		
		
    })
		 
		 
    </script>

<script type="text/javascript">var blankSrc = "wpscripts/blank.gif";
</script>





<style type="text/css">
.Normal-C-C1 {
    color: #710036;
    font-family: "Vladimir Script",cursive;
    font-size: 48px;
    line-height: 47px;
}
.contect_all_data_pro_subscription {
	 margin-left:15px; margin-right:15px;
}
.title_sub_pro {
	color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 21px;
    font-weight: 700;
    line-height: 1.19em;
	text-align:center;
	margin:15px;
	}
.bloc_sub_pro {
	background-color:#710036;
	padding-left:25px; padding-right:15px;
	padding-bottom:20px;
	padding-top:5px;
}	
.space_sub_pro {
	height:20px;}
.table_sub_pro {
	color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 13px;
    line-height: 1.23em;
	}
.table_sub_pro tr {
	height:35px;}
.table_sub_pro_abonnement td {
	border: medium solid #710036;
	}		
</style>

<div style="text-align:center; background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages2013/img_admin_platinium.png); background-repeat:no-repeat; height:170px; padding-left:150px; padding-top:30px;">
	<span class="Normal-C-C1">Inscription à l'abonnement<br/>
    <?php if ($type=="basic") echo '"Basic"';?>
    <?php if ($type=="premium") echo '"Premium"';?>
    <?php if ($type=="platinium" || $type=="platinum") echo '"Platinium"';?>
    </span>
</div>

<form name="frmInscriptionProfessionnel" id="frmInscriptionProfessionnel" action="<?php echo site_url("front/professionnels/ajouter"); ?>" method="POST" accept-charset="UTF-8" target="_self" enctype="multipart/form-data" style="margin:0px;">

<input type="hidden" name="Societe[agenda]" id="AgendaSociete" value="1"/>

<div id="contect_all_data_pro_subscription" class="contect_all_data_pro_subscription">








<div class="bloc_sub_pro">
<div class="title_sub_pro">Votre activité</div>
<div class="table_sub_pro">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="202px">Préciser votre activité *</td>
    <td>
    <select name="AssCommercantRubrique[IdRubrique]" onChange="javascript:listeSousRubrique();" id="RubriqueSociete" tabindex="1" style="width: 202px;">
        <option value="">-- Veuillez choisir --</option>
        <?php if(sizeof($colRubriques)) { ?>
            <?php foreach($colRubriques as $objRubrique) { ?>
                <option value="<?php echo $objRubrique->IdRubrique; ?>"><?php echo $objRubrique->Nom; ?></option>
            <?php } ?>
        <?php } ?>
    </select>
    </td>
  </tr>
  <tr id='trReponseRub'  style="height:auto;">
    
  </tr>
  <tr>
    <td>Statut</td>
    <td>
    <select name="Societe[idstatut]" size="1" style="width: 202px;" tabindex="3">
        <option selected="selected" value="">Choisir&nbsp;votre&nbsp;statut</option>
        <?php if(sizeof($colStatut)) { ?>
            <?php foreach($colStatut as $objStatut) { ?>
                <option value="<?php echo $objStatut->id; ?>"><?php echo stripcslashes($objStatut->Nom); ?></option>
            <?php } ?>
        <?php } ?>
    </select>
    </td>
  </tr>
  <tr>
    <td>Autre, préciser</td>
    <td>
    <input name="Autre" style="width: 202px;" type="text" tabindex="4">
    </td>
  </tr>
  <tr>
    <td>Nom ou enseigne *</td>
    <td>
    <input type="text" name="Societe[NomSociete]" id="NomSociete" value="" style="width: 202px;" tabindex="5"/>
    </td>
  </tr>
  <tr>
    <td>Adresse 1</td>
    <td>
    <input type="text" name="Societe[Adresse1]" id="Adresse1Societe" value="" style="width: 202px;" tabindex="6"/>
    </td>
  </tr>
  <tr>
    <td>Adresse 2</td>
    <td>
    <input type="text" name="Societe[Adresse2]" id="Adresse2Societe" value="" style="width: 202px;" tabindex="7"/>
    </td>
  </tr>
  <tr>
    <td>Ville *</td>
    <td>
    <select name="Societe[IdVille]" id="VilleSociete" style="width: 202px;" tabindex="8">
      <option value="0">-- Veuillez choisir --</option>
        <?php if(sizeof($colVilles)) { ?>
            <?php foreach($colVilles as $objVille) { ?>
                <option value="<?php echo $objVille->IdVille; ?>"><?php echo htmlspecialchars(stripcslashes($objVille->Nom)); ?></option>
            <?php } ?>
        <?php } ?>
    </select>
    </td>
  </tr>
  <tr>
    <td>Code Postal *</td>
    <td><input type="text" name="Societe[CodePostal]" id="CodePostalSociete" value="" style="width: 202px;" tabindex="9"/></td>
  </tr>
  <tr>
    <td>Téléphone direct</td>
    <td>
    <input type="text" name="Societe[TelFixe]" id="TelFixeSociete" value="" style="width: 202px;" tabindex="10"/>
    </td>
  </tr>
  <tr>
    <td>Téléphone mobile</td>
    <td><input type="text" name="Societe[TelMobile]" id="TelMobileSociete" value="" style="width: 202px;" tabindex="11"/></td>
  </tr>
  <tr>
    <td>Email *</td>
    <td>
    <input type="text" name="Societe[Email]" id="EmailSociete" value="" style="width: 202px;" tabindex="12"/>
    <div id="divErrortxtEmail_" style="width:152px; height:20px; overflow:hidden; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;"></div>
    <input type="hidden" name="txtEmail_verif" id="txtEmail_verif" value="0" style="width: 202px;" tabindex="18"/>
    </td>
  </tr>
</table>
</div>
</div>
<div class="space_sub_pro">&nbsp;</div>



<div class="bloc_sub_pro">
<div class="title_sub_pro">Les coordonnées du décideur</div>
<div class="table_sub_pro">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2">
    Le soussigné déclare avoir la faculté 
d’engager en son nom sa structure (commerce, entreprise, collectivité…) 
dont les coordonnées sont précisées ci-<wbr>dessus.
    </td>
  </tr>
  <tr>
    <td width="202px"s>Civilité *</td>
    <td>
    <select name="Societe[Civilite]" id="CiviliteResponsableSociete" style="width: 202px;" tabindex="13">
        <option value="0">Monsieur</option>
        <option value="1">Madame</option>
        <option value="2">Mademoiselle</option>
    </select>
    </td>
  </tr>
  <tr>
    <td>Nom responsable *</td>
    <td><input type="text" name="Societe[Nom]" id="NomResponsableSociete" value="" style="width: 202px;" tabindex="14"/></td>
  </tr>
  <tr>
    <td>Prénom responsable *</td>
    <td><input type="text" name="Societe[Prenom]" id="PrenomResponsableSociete" value="" style="width: 202px;" tabindex="15"/></td>
  </tr>
  <tr>
    <td>Fonction responsable *</td>
    <td><input type="text" name="Societe[Responsabilite]" id="ResponsabiliteResponsableSociete" value="" style="width: 202px;" tabindex="16"/></td>
  </tr>
  <tr>
    <td>Téléphone direct *</td>
    <td><input type="text" name="Societe[TelDirect]" id="TelDirectResponsableSociete" value="" style="width: 202px;" tabindex="17"/></td>
  </tr>
  <tr>
    <td>Email *</td>
    <td><input type="text" name="Societe[Email_decideur]" id="Email_decideurResponsableSociete" value="" style="width: 202px;" tabindex="18"/></td>
  </tr>
</table>
</div>
</div>
<div class="space_sub_pro">&nbsp;</div>


<div class="bloc_sub_pro">
<div class="title_sub_pro">Votre identifiant et mot de passe</div>
<div class="table_sub_pro">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="202px">Identifiant *</td>
    <td>
    <input type="text" name="Societe[Login]" id="txtLogin" value="" style="width: 202px;" tabindex="20"/>
    <div id="divErrortxtLogin_" style="width:152px; overflow:hidden; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;"></div>
    <div id="inputMontantTTC">
       <input type="hidden" name="montantttcvalue_abonnement" id="montantttcvalue_abonnement"/>
       <input type="hidden" name="txtLogin_verif" id="txtLogin_verif" value="0" />
    </div>
    </td>
  </tr>
  <tr>
    <td>Mot de passe *</td>
    <td><input type="password" name="Societe_Password" id="txtPassword" value="" style="width: 202px;" tabindex="21"/></td>
  </tr>
  <tr>
    <td>Confirmation du mot de passe</td>
    <td><input type="password" id="txtConfirmPassword" value="" style="width: 202px;" tabindex="22"/></td>
  </tr>
</table>
</div>
</div>
<div class="space_sub_pro">&nbsp;</div>

<?php 
$this->load->Model("Abonnement");
$obj_abonnement_gratuit = $this->Abonnement->GetWhere(" type='gratuit' ");
$obj_abonnement_premium = $this->Abonnement->GetWhere(" type='premium' ");
$obj_abonnement_platinum = $this->Abonnement->GetWhere(" type='platinum' ");

if (isset($obj_abonnement_gratuit) && $type=="basic") $value_abonnement_sub_pro = $obj_abonnement_gratuit->IdAbonnement;
else if (isset($obj_abonnement_premium) && $type=="premium") $value_abonnement_sub_pro = $obj_abonnement_premium->IdAbonnement;
else if (isset($obj_abonnement_platinum) && ($type=="platinium" || $type=="platinum")) $value_abonnement_sub_pro = $obj_abonnement_platinum->IdAbonnement;
else $value_abonnement_sub_pro = '1';
?>
<input type="hidden" name="AssAbonnementCommercant[IdAbonnement]" id="AbonnementSociete" value="<?php echo $value_abonnement_sub_pro;?>"/>

<div class="bloc_sub_pro">
<div class="title_sub_pro">Votre abonnement 
	<?php if ($type=="basic") echo 'BASIC GRATUIT';?>
    <?php if ($type=="premium") echo 'PREMIUM';?>
    <?php if ($type=="platinium" || $type=="platinum") echo 'PLATINIUM';?>
</div>
<?php if ($type=="platinium" || $type=="platinum" || $type=="premium") { ?>

<div style='color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 13px;
    font-weight: 700;
    line-height: 1.23em;'>
Votre commande :<br/>
La souscription de chaque option dure le temps de l’abonnement 
<?php if ($type=="premium") echo 'PREMIUM';?>
<?php if ($type=="platinium" || $type=="platinum") echo 'PLATINIUM';?>
</div>

<div class="table_sub_pro">
<table width="570" border="1" cellspacing="0" cellpadding="0" style="border: thin; border-color:#710036; background-color:#FFFFFF; color:#000; text-align:center;" class="table_sub_pro_abonnement">
  <tr style="height:25px;">
    <td width="366">&nbsp;</td>
    <td width="102">Montant u. ht</td>
    <td width="102">Montant ht</td>
  </tr>
  <tr>
    <td>
    <div id="divAbonnementdesc" >Abonnement annuel 
    <?php if ($type=="premium") echo 'PREMIUM';?>
    <?php if ($type=="platinium" || $type=="platinum") echo 'PLATINIUM';?>
    </div>
    </td>
    <td>
    <div id="divAbonnementmontant" style='font-family: "Arial",sans-serif;
    font-size: 16px;
    font-weight: 700;
    line-height: 1.25em;'>
	<?php if ($type=="premium") echo '500 €';?>
    <?php if ($type=="platinium" || $type=="platinum") echo '400 €';?>
    </div>
    
    </td>
    <td>
    <div id="divAbonnementmontant2" style='font-family: "Arial",sans-serif;
    font-size: 16px;
    font-weight: 700;
    line-height: 1.25em;'>
   <?php if ($type=="premium") echo '500 €';?>
    <?php if ($type=="platinium" || $type=="platinum") echo '400 €';?>
</div>
    </td>
  </tr>
  <tr>
    <td>
    
<?php if ($type=="platinium" || $type=="platinum") { ?>    
<script type="text/javascript">
		 $(function(){
			 $("#check_referencement").click(function(){
				 if ($('#check_referencement').attr('checked')) {
					$("#check_referencement_value").val("1");
					$("#divValueReferencementHt").html("250 €");
					$("#divMontantHT").html("650 €");
					$("#divMontantTVA").html("127,4 €");
					$("#divMontantTTC").html("777,4 €");
				} else {
					$("#check_referencement_value").val("0");
					$("#divValueReferencementHt").html("");
					$("#divMontantHT").html("400 €");
					$("#divMontantTVA").html("78,4 €");
					$("#divMontantTTC").html("478,4 €");
				}
			 });
			   
		 })
</script>
    
    
    <div id="divModuledesc">
   Module optionnel « Site et référencement »<br/>
   <input id="check_referencement" type="checkbox" name="check_referencement"/> Cliquez ici pour valider ce module
<?php } ?>   
   <input type="hidden" name="Societe[referencement]" id="check_referencement_value" value="0"/>
   
</div>
    </td>
    <td>
    <div id="divAbonnemenreferencement" style='font-family: "Arial",sans-serif;
    font-size: 16px;
    font-weight: 700;
    line-height: 1.25em;'>
    <?php if ($type=="platinium" || $type=="platinum") echo '250 €';?>
    </div>
    </td>
    <td>
    
    <div id="divValueReferencementHt" style='font-family: "Arial",sans-serif;
    font-size: 16px;
    font-weight: 700;
    line-height: 1.25em;'>
	</div>
    
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" style="text-align:right;">Montant total ht </td>
    <td>
    <div id="divMontantHT" >
   <?php if ($type=="premium") echo '500 €';?>
    <?php if ($type=="platinium" || $type=="platinum") echo '400 €';?>
</div>
    
    </td>
  </tr>
  <tr>
    <td colspan="2" style="text-align:right;">T.V.A 19,60% </td>
    <td>
    <div id="divMontantTVA">
   <?php if ($type=="premium") echo '98 €';?>
    <?php if ($type=="platinium" || $type=="platinum") echo '78,4 €';?>
</div>
    </td>
  </tr>
  <tr>
    <td colspan="2" style="text-align:right;">Montant T.T.C. à régler </td>
    <td>
    <div id="divMontantTTC" style='font-family: "Arial",sans-serif;
    font-size: 16px;
    font-weight: 700;
    line-height: 1.25em;'>
   <?php if ($type=="premium") echo '598 €';?>
    <?php if ($type=="platinium" || $type=="platinum") echo '478,4 €';?>
</div>
    </td>
  </tr>
</table>
</div>

<div style='color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 13px;
    font-weight: 700;
    line-height: 1.23em;'>
<p>Vos conditions de règlement :</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <input id="check_264" type="radio" name="Societe[Conditions_paiement]" value="Cheque"> Chèque
    </td>
    <td>
    <input id="check_265" type="radio" name="Societe[Conditions_paiement]" value="Virement"> Virement
    </td>
    <td>
    <input id="check_266" type="radio" name="Societe[Conditions_paiement]" value="Carte Bancaire avec Paypal"> Carte Bancaire avec Paypal
    </td>
  </tr>
</table>
</div>
<?php } ?>
</div>
<div class="space_sub_pro">&nbsp;</div>



<div class="bloc_sub_pro">
<div class="title_sub_pro">L'ouverture de votre abonnement 
<?php if ($type=="premium") echo 'PREMIUM';?>
<?php if ($type=="platinium" || $type=="platinum") echo 'PLATINIUM';?>
</div>
<div style='color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 13px;
    font-weight: 700;s
    line-height: 1.23em;'>

Le soussigné déclare avoir la faculté dengager en son nom son association, son entreprise, sa collectivité.

<?php if ($type=="basic") { ?>
<p style="text-align:center;"><strong>La confirmation de votre demande d'inscription</strong></p>
Dès validation de cette demande dinscription, vous recevez une confirmation de réception.
Sous 48h00 après étude de votre demande et validation de notre service, nous vous adresserons votre identifiant et votre mot de passe. Muni de ces derniers, vous pourrez commencer à déposer vos événements.
Proximité peut refuser sans en donner la justification l’ouverture d’un compte qui ne correspondrait pas à son éthique. (voir conditions générales)
Le soussigné confirme sa demande d'inscription en qualité de diffuseur pour l’abonnement « Basic »
et valide les conditions générales d'utilisation de ce service. <a href="<?php echo site_url('front/utilisateur/conditionsgenerales');?>" style="color:#FFFFFF; text-decoration:none;">(cliquez ici pour consulter les conditions d'utilisation)</a>
<?php } else if ($type=="premium") { ?>

<p style="text-align:center;">Le coût d'un abonnement Prémium<br/>
L'abonnement Premium est valable 12 mois.<br/>
Le dépôt des dossiers est illimité pendant cette période .<br/>
Le côut de cet abonnement annuel est de 500 € ht soit 598 € ttc</p>

<p style="text-align:center;"><strong>La confirmation de votre demande d'inscription</strong></p>
<p style="text-align:center;">
1. Dès validation de votre demande d'inscription, vous recevrez une confirmation de réception.<br/>
2. Sous 48h00 notre équipe après étude vous confirmera ou pas la validation de cette demande,
en effet  Proximité peut refuser sans en donner la justification l'ouverture d'un compte
qui ne correspondrait pas à son éthique. (voir conditions générales).<br/>
3. La confirmation de cette demande sera accompagnée d'une facture.<br/>
4. L'ouverture de votre compte prendra effet dès réception de votre règlement par chèque ou par
virement. (Pour les collectivités leur compte sera ouvert à réception de leur bon de commande
par courrier, courriel ou fax) <a href="<?php echo site_url('front/utilisateur/conditionsgenerales');?>" style="color:#FFFFFF; text-decoration:none;">(cliquez ici pour consulter les conditions d'utilisation)</a></p> 
<?php } ?>



</div>
</div>
<div class="space_sub_pro">&nbsp;</div>



<div style="margin:15px; padding:15px;">
<p>*Saisie obligatoire</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top"><input name="validationabonnement" id="validationabonnement" value="1" type="checkbox" tabindex="24"></td>
    <td>
    Je confirme* ma demande d'inscription et la validation des conditions générales.<br/>
Je souhaite recevoir des informations à propos du Club et du Magazine Proximité,     je peux me désinscrire à tout moment.<br/>
<a href="<?php echo site_url('front/utilisateur/conditionsgenerales');?>" style=" color:#000000; text-decoration:none;">(cliquez ici pour consulter les conditions d'utilisation)</a>
    </td>
  </tr>
</table>
</div>


<div style="text-align:center;">
<?php echo $captcha['image']; ?><br />
    <input name="captcha" value="" id="captcha" type="text" tabindex="25">
    <input name="divCaptchavalueverify" id="divCaptchavalueverify" value="" type="hidden" />
</div>


<div style="text-align:center; margin-top:20px; margin-bottom:20px;">
<input id="btnSinscrire" style="width: 227px; height: 30px;" name="envoyer" value="Validation" type="button" tabindex="26">
</div>

<div class="FieldError" id="divErrorFrmInscriptionProfessionnel" style="height: auto; color: red; font-family: arial; font-size: 12px; text-align:center;"></div>


<!--<div style="text-align:center;">
<img src="<?php //echo GetImagePath("front/"); ?>/wpimages2013/img_sb_pro_bottom.png">
</div>-->







</div>
</form>



<?php $this->load->view("front2013/includes/footer_mini_2"); ?>