
<script type="application/javascript">
$(function() {
			$("#inputStringDatedebutHidden").datepicker({
				dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
				monthNames: ['Janvier','Fevier','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
				dateFormat: 'yy-mm-dd',
				autoSize: true,
				changeMonth: true,
	            changeYear: true
			});
			$("#inputStringDatefinHidden").datepicker({
				dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
				monthNames: ['Janvier','Fevier','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
				dateFormat: 'yy-mm-dd',
				autoSize: true,
				changeMonth: true,
	            changeYear: true
			});
			
		});	
</script>




<p><img src="<?php echo GetImagePath("front/"); ?>/agenda/btn_favoris.png"  alt="" border="0" ></p>
<p><img src="<?php echo GetImagePath("front/"); ?>/agenda/btn_mon_agenda.png"  alt="" border="0" ></p>



<form name="frmRecherchePartenaire" id="frmRecherchePartenaire" method="post" action="<?php echo site_url("agenda/liste"); ?>">

<input name="inputStringHidden" id="inputStringHidden" type="hidden" value="">
<input name="inputStringHidden_sub" id="inputStringHidden_sub" type="hidden" value="">
<input name="inputStringOrderByHidden" id="inputStringOrderByHidden" type="hidden" value="">
<input name="zMotCle" id="zMotCle" type="hidden" value="">

<table width="100%" border="0" cellspacing="2" cellpadding="2" style="padding-left:15px;">
  <tr>
    <td><img src="<?php echo GetImagePath("front/"); ?>/agenda/btn_left_filtre_quand.png"  alt="" border="0" ></td>
  </tr>
  <tr>
    <td>
    <select id="inputStringQuandHidden" size="1" name="inputStringQuandHidden" onChange="javascript:refresh_categ_agenda_list();">
        <option value="0">Toutes les dates</option>
        <option value="1">Aujourd'hui</option>
        <option value="2">Ce Week-end</option>
        <option value="3">Cette semaine</option>
        <option value="4">Semaine prochaine</option>
        <option value="5">Ce mois</option>
    </select>
    </td>
  </tr>
  <tr>
    <td><img src="<?php echo GetImagePath("front/"); ?>/agenda/btn_left_filtre_datedebut.png"  alt="" border="0" ></td>
  </tr>
  <tr>
    <td>
    <input id="inputStringDatedebutHidden" type="text" style="width:145px;" accesskey="" name="inputStringDatedebutHidden" onChange="javascript:refresh_categ_agenda_list();">
    </td>
  </tr>
  <tr>
    <td><img src="<?php echo GetImagePath("front/"); ?>/agenda/btn_left_filtre_datefin.png"  alt="" border="0" ></td>
  </tr>
  <tr>
    <td>
    <input id="inputStringDatefinHidden" type="text" style="width:145px;" accesskey="" name="inputStringDatefinHidden" onChange="javascript:refresh_categ_agenda_list();">
    </td>
  </tr>
  <tr>
    <td><img src="<?php echo GetImagePath("front/"); ?>/agenda/btn_left_filtre_ou.png"  alt="" border="0" ></td>
  </tr>
  <tr>
    <td>
    <select id="inputStringVilleHidden_partenaires" name="inputStringVilleHidden" size="1"  style="width:155px;" onChange="javascript:refresh_categ_agenda_list();">
        <option value="0">-- Toutes les Communes --</option>
        <?php foreach($toVille as $oVille){ ?>
            <option value="<?php echo $oVille->IdVille ; ?>"><?php echo $oVille->Nom." (".$oVille->nb_agenda.")" ; ?></option>
        <?php } ?>
    </select>
    </td>
  </tr>
  <tr>
    <td><img src="<?php echo GetImagePath("front/"); ?>/agenda/btn_left_filtre_achaque.png"  alt="" border="0" ></td>
  </tr>
  <tr>
    <td colspan="2" style="text-align:center;"><input type="button" value="Réinitialisation" onClick="javascript:btn_re_init_agenda_list();"/></td>
  </tr>
  <tr>
    <td><img src="<?php echo GetImagePath("front/"); ?>/agenda/btn_left_filtre_quoi.png"  alt="" border="0" ></td>
  </tr>
</table>



<span id="span_leftcontener2013_form_partenaires">
<?php if (count($toCategorie_principale)!=0) { ?>
<?php foreach($toCategorie_principale as $oCategorie_principale){ ?>
    <div class="leftcontener2013title" style="margin-top:5px;">
    	
    	<?php echo ucfirst(strtolower($oCategorie_principale->category)) ; ?>&nbsp;(<?php echo $oCategorie_principale->nb_agenda ; ?>)
        
    </div>
    <div class="leftcontener2013content" style="margin-bottom:5px;">
    	<?php 
		$this->load->model('mdl_categories_agenda');
		$OCommercantSousRubrique__x = $this->mdl_categories_agenda->GetAgendaSouscategorieByRubrique($oCategorie_principale->agenda_categid,"0","","","0");
		//////$this->firephp->log($OCommercantSousRubrique__x, 'OCommercantSousRubrique__x');
		?>
        <!--<br/>-->
        <?php 
		
		if (count($OCommercantSousRubrique__x)>0) {
			?>
			<?php foreach($OCommercantSousRubrique__x as $ObjCommercantSousRubrique__x){ 
				////$this->firephp->log($ObjCommercantSousRubrique__x, 'ObjCommercantSousRubrique__x');
			?>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="22">
                    <span id="span_check_part">
                    <input onClick="javascript:submit_search_agenda();" name="check_part[<?php echo $ObjCommercantSousRubrique__x->agenda_subcategid ; ?>]" id="check_part_<?php echo $ObjCommercantSousRubrique__x->agenda_subcategid ; ?>" type="checkbox" value="<?php echo $ObjCommercantSousRubrique__x->agenda_subcategid ; ?>">
                    </span>
                    </td>
                    <td><?php echo ucfirst(strtolower($ObjCommercantSousRubrique__x->subcateg)) ; ?>&nbsp;(<?php echo $ObjCommercantSousRubrique__x->nb_agenda ; ?>)</td>
                  </tr>
                </table>
			<?php } ?>
        <?php } ?>
    </div>
<?php } ?>
<?php } ?>
</span>


</form>


