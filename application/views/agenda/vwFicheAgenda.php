<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("front2013/includes/header_mini_2", $data); ?>

<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
function listeSousRubrique(){
         
		jQuery('#trReponseRub').html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...." id="img_loaging_agenda_x"/>') ; 
		
		var irubId = jQuery('#IdCategory').val();     
        jQuery.get(
            '<?php echo site_url("front/utilisateur/GetallSubcateg_by_categ"); ?>' + '/' + irubId,
            function (zReponse)
            {
                // alert (zReponse) ;
                jQuery('#trReponseRub').html(zReponse) ;
               
             
           });
		   //alert(irubId);
    }
	function getCP(){
	      var ivilleId = jQuery('#IdVille').val();
          jQuery.get(
            '<?php echo site_url("front/utilisateur/getPostalCode_localisation"); ?>' + '/' + ivilleId,
            function (zReponse)
            {
                jQuery('#codepostal').val(zReponse);
           });
	}

    function getCP_localisation(){
        var ivilleId = jQuery('#IdVille_localisation').val();
        jQuery.get(
                '<?php echo site_url("front/utilisateur/getPostalCode_localisation"); ?>' + '/' + ivilleId,
                function (zReponse)
                {
                    jQuery('#codepostal_localisation').val(zReponse);
                });
    }
	
    function deleteFile(_IdAgenda,_FileName) {
        jQuery.ajax({
            url: '<?php echo base_url();?>agenda/delete_files/' + _IdAgenda + '/' + _FileName,
            dataType: 'html',
            type: 'POST',
            async: true,
            success: function(data){
                window.location.reload();
            }
        });
		
    }
    
    jQuery(document).ready(function() {
		
		jQuery("#img_loaging_agenda").hide();
		jQuery("#img_loaging_agenda_addbonplan").hide();
        
		jQuery(".btnSinscrire").click(function(){
			
            var txtError = "";
			
            var nom_societe = jQuery("#nom_societe").val();
            if(nom_societe=="" || nom_societe==null) {
                txtError += "- Veuillez indiquer qui vous-êtes.<br/>";
				$("#nom_societe").css('border-color', '#FF0000');
            } else {
                $("#nom_societe").css('border-color', '#FFFFFF');
            }
			
			var nom_manifestation = jQuery("#nom_manifestation").val();
            if(nom_manifestation=="" || nom_manifestation==null) {
                txtError += "- Veuillez indiquer un Nom à la manifestion.<br/>";
				$("#nom_manifestation").css('border-color', '#FF0000');
            } else {
                $("#nom_manifestation").css('border-color', '#FFFFFF');
            }
			
			var organisateur = jQuery("#organisateur").val();
            if(organisateur=="" || organisateur==null) {
                txtError += "- Veuillez indiquer qui est l'Organisateur.<br/>";
				$("#organisateur").css('border-color', '#FF0000');
            } else {
                $("#organisateur").css('border-color', '#FFFFFF');
            }
			
			var email = jQuery("#email").val();
            if(!isEmail(email) || email=="" || email==null) {
                txtError += "- Veuillez indiquer un adresse email valide.<br/>";
				$("#email").css('border-color', '#FF0000');
            } else {
                $("#email").css('border-color', '#FFFFFF');
            }
			
			var date_depot = jQuery("#date_depot").val();
            if(date_depot=="" || date_depot==null) {
                txtError += "- Veuillez indiquer la date de dépôt.<br/>";
				$("#date_depot").css('border-color', '#FF0000');
            } else {
                $("#date_depot").css('border-color', '#FFFFFF');
            }
			
			var IdCategory = jQuery("#IdCategory").val();
            if(IdCategory=="" || IdCategory==null || IdCategory=="0") {
                txtError += "- Veuillez indiquer une categorie.<br/>";
				$("#IdCategory").css('border-color', '#FF0000');
            } else {
                $("#IdCategory").css('border-color', '#FFFFFF');
            }
			
			/*var IdSubcategory = jQuery("#IdSubcategory").val();
            if(IdSubcategory=="" || IdSubcategory==null || IdSubcategory=="0") {
                txtError += "- Veuillez indiquer une sous-categorie.<br/>";
				$("#IdSubcategory").css('border-color', '#FF0000');
            } else {
                $("#IdSubcategory").css('border-color', '#FFFFFF');
            }*/
			
			var date_debut = jQuery("#date_debut").val();
            if(date_debut=="" || date_debut==null) {
                txtError += "- Veuillez indiquer la date de début.<br/>";
				$("#date_debut").css('border-color', '#FF0000');
            } else {
                $("#date_debut").css('border-color', '#FFFFFF');
            }
			
			var date_fin = jQuery("#date_fin").val();
            if(date_fin=="" || date_debut==null) {
                txtError += "- Veuillez indiquer la date de fin.<br/>";
				$("#date_fin").css('border-color', '#FF0000');
            } else {
                $("#date_fin").css('border-color', '#FFFFFF');
            }
			
			
            if(txtError == "") {
                jQuery("#frmInscriptionProfessionnel").submit();
            } else {
				jQuery("#div_error_agenda_submit").html(txtError);
			}
        });
		
		
		$("#adresse_localisation_diffuseur_checkbox").click(function(){
			if ($('#adresse_localisation_diffuseur_checkbox').attr('checked')) {
				$("#adresse_localisation_diffuseur").val("1");
				var adress_organisateur1 = $("#adresse1").val();
				var adress_organisateur2 = $("#adresse2").val();
				var IdVille_organisateur = $("#IdVille").val();
				var codepostal_organisateur = $("#codepostal").val();
				$("#adresse_localisation").val(adress_organisateur1+" "+adress_organisateur2);
				$("#IdVille_localisation").val(IdVille_organisateur);
				$("#codepostal_localisation").val(codepostal_organisateur);
			} else {
				$("#adresse_localisation_diffuseur").val("0");
				$("#adresse_localisation").val("");
				$("#IdVille_localisation").val("0");
				$("#codepostal_localisation").val("");
			}
		});
		
		$("#diffusion_facebook_checkbox").click(function(){
			 if ($('#diffusion_facebook_checkbox').attr('checked')) {
				$("#diffusion_facebook").val("1");
			} else {
				$("#diffusion_facebook").val("0");
			}
		 });
		 
		 $("#diffusion_twitter_checkbox").click(function(){
			 if ($('#diffusion_twitter_checkbox').attr('checked')) {
				$("#diffusion_twitter").val("1");
			} else {
				$("#diffusion_twitter").val("0");
			}
		 });
		 
		 $("#diffusion_rss_checkbox").click(function(){
			 if ($('#diffusion_rss_checkbox').attr('checked')) {
				$("#diffusion_rss").val("1");
			} else {
				$("#diffusion_rss").val("0");
			}
		 });
		 
		 
		 //checkbox manif_gratuite START
		 $("#manif_gratuite_checkbox").click(function(){
			 if ($('#manif_gratuite_checkbox').attr('checked')) {
				$("#manif_gratuite").val("1");
				//$("#description_tarif").text("Manifestation gratuite");
				CKEDITOR.instances.description_tarif.insertText("Manifestation gratuite");
			} else {
				$("#manif_gratuite").val("0");
				$("#description_tarif").text("");
			}
		 });
		 //checkbox manif_gratuit END
		 
		 
		 
		 //checkbox ajout_bonplan START
		 $("#ajout_bonplan_checkbox").click(function(){
			 if ($('#ajout_bonplan_checkbox').attr('checked')) {
				$("#ajout_bonplan").val("1");
				
				
				$("#img_loaging_agenda_addbonplan").show();
				 
				 
				 $.post(
						'<?php echo site_url(); ?>/agenda/check_bonplan/',
						{ 
						IdCommercant: <?php echo $objCommercant->IdCommercant; ?>
						},
						function (zReponse)
						{
							   //alert ("reponse " + zReponse) ;

								if (zReponse!="error") {
                                    
									CKEDITOR.instances.conditions_promo.insertText(zReponse);
                                    $("#img_loaging_agenda_addbonplan").hide();
									
								} else {
									$("#spam_error_bonplan_agenda").html("Votre bonplan a éxpiré ou vous n'avez pas encore de bonplan !");
								}
				   });
				
				
				
			} else {
				$("#ajout_bonplan").val("0");
				$("#conditions_promo").text("");
			}
		 });
		 //checkbox ajout_bonplan END
		 
		 
		 
		 //radio buttton diffuseur_organisateur START
		 $("#diffuseur_organisateur_1").click(function(){
			if ($('#diffuseur_organisateur_1').attr('checked')){
				 $("#diffuseur_organisateur").val("1");
				 
				 $("#img_loaging_agenda").show();
				 
				 
				 $.post(
						'<?php echo site_url(); ?>/agenda/check_commercant/',
						{ 
						IdCommercant: <?php echo $objCommercant->IdCommercant; ?>
						},
						function (zReponse)
						{
							   //alert ("reponse " + zReponse) ;

								if (zReponse) {
                                    var article_value = zReponse.split("==!!!==");
                                    for(i = 0; i < article_value.length; i++) {
                                        var article_value_id = article_value[i].split("!!!==>");
                                        if (article_value_id[0]=="NomSociete") $("#organisateur").val(article_value_id[1]);
										if (article_value_id[0]=="Adresse1") $("#adresse1").val(article_value_id[1]);
										if (article_value_id[0]=="Adresse2") $("#adresse2").val(article_value_id[1]);
										if (article_value_id[0]=="IdVille") $("#IdVille").val(article_value_id[1]);
										if (article_value_id[0]=="CodePostal") $("#codepostal").val(article_value_id[1]);
										if (article_value_id[0]=="TelFixe") $("#telephone").val(article_value_id[1]);
										if (article_value_id[0]=="TelMobile") $("#mobile").val(article_value_id[1]);
										if (article_value_id[0]=="fax") $("#fax").val(article_value_id[1]);
										if (article_value_id[0]=="Email") $("#email").val(article_value_id[1]);
										if (article_value_id[0]=="SiteWeb") $("#siteweb").val(article_value_id[1]);
										if (article_value_id[0]=="Facebook") $("#facebook").val(article_value_id[1]);
										if (article_value_id[0]=="Twitter") $("#googleplus").val(article_value_id[1]);
										if (article_value_id[0]=="google_plus") $("#twitter").val(article_value_id[1]);
                                    }

                                    $("#img_loaging_agenda").hide();
								}
				   });
				   
				   
				   
				   
				 
				 
			} else {
				$("#diffuseur_organisateur").val("0");
			}
		 });
		 $("#diffuseur_organisateur_0").click(function(){
			 if ($('#diffuseur_organisateur_0').attr('checked')) {
				 $("#diffuseur_organisateur").val("0");
				 
					$("#organisateur").val("");
					$("#adresse1").val("");
					$("#adresse2").val("");
					$("#IdVille").val("0");
					$("#codepostal").val("");
					$("#telephone").val("");
					$("#mobile").val("");
					$("#fax").val("");
					$("#email").val("");
					$("#siteweb").val("http://www.");
					$("#facebook").val("");
					$("#googleplus").val("");
					$("#twitter").val("");
				 
			 } else {
				 $("#diffuseur_organisateur").val("1");
			 }
		 });
		 //radio buttton diffuseur_organisateur END
		 
		 
		 
		 //radio buttton handicapes
		 $("#handicapes_1").click(function(){
			 if ($('#handicapes_1').attr('checked')) {$("#handicapes").val("1");} else {$("#handicapes").val("0");}
		 });
		 $("#handicapes_0").click(function(){
			 if ($('#handicapes_0').attr('checked')) {$("#handicapes").val("0");} else {$("#handicapes").val("1");}
		 });
		 //radio buttton parking
		 $("#parking_1").click(function(){
			 if ($('#parking_1').attr('checked')) {$("#parking").val("1");} else {$("#parking").val("0");}
		 });
		 $("#parking_0").click(function(){
			 if ($('#parking_0').attr('checked')) {$("#parking").val("0");} else {$("#parking").val("1");}
		 });
		 
		 
		 $("#admin_agenda_activation_1").click(function(){if ($('#admin_agenda_activation_1').attr('checked')) {$("#IsActif").val("1");}});
		 $("#admin_agenda_activation_0").click(function(){if ($('#admin_agenda_activation_0').attr('checked')) {$("#IsActif").val("0");}});
		 $("#admin_agenda_activation_2").click(function(){if ($('#admin_agenda_activation_2').attr('checked')) {$("#IsActif").val("2");}});
		 $("#admin_agenda_activation_3").click(function(){if ($('#admin_agenda_activation_3').attr('checked')) {$("#IsActif").val("3");}});
		
    });
	
	$(function() {
			$("#date_debut").datepicker({
				dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
				dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
				monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
				dateFormat: 'DD, d MM yy',
				autoSize: true
			});
			$("#date_fin").datepicker({
				dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
				dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
				monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
				dateFormat: 'DD, d MM yy',
				autoSize: true
			});
			$("#date_depot").datepicker({
				dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
				dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
				monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
				dateFormat: 'DD, d MM yy',
				autoSize: true
			});
		}); 
		
		function alert_save_agenda_before_view(){
			$("#id_save_agenda_before_view").html("Veuillez enregistrer l'événnement avant de visualiser !&nbsp;&nbsp;&nbsp;");
		}
</script>

<style type="text/css">
.stl_long_input_platinum {
	width:413px;
}
.stl_long_input_platinum_td {
	width:180px;
	height:30px;
}
.div_stl_long_platinum {
	color: #000000;
	background-image:url(<?php echo GetImagePath("front/");?>/agenda/wpimages/wp484168d4_06.png);
	background-repeat:no-repeat;
    font-family: "Arial",sans-serif;
    font-size: 13px;
    font-weight: 700;
	font-weight:bold;
    line-height: 1.23em;
    height:25px; padding-top:10px; padding-left:20px; margin-top:15px; margin-bottom:15px;
	margin-left:10px;
}
.div_error_taille_3_4 {
	color: #F00;
}
.FieldError {
	color:#FF0000;
	font-weight:bold;
}
</style>

<div style="text-align:left; background-image:url(<?php echo GetImagePath("front/");?>/wpimages2013/img_admin_platinium.png); background-repeat:no-repeat; height:200px;">
  <div style='
    color: #000000;
    font-family: "Vladimir Script",cursive;
    font-size: 48px;
    line-height: 47px; text-align:center; padding-left:200px; padding-top:25px;'>
        Dépôt Agenda<br/> 
        <?php if (isset($objAbonnementCommercant) && $user_groups->id=='5') { ?>
        "Platinium"
        <?php } else if (isset($objAbonnementCommercant) && $user_groups->id=='4') { ?>
        "Premium"
        <?php } else if (isset($objAbonnementCommercant) && $user_groups->id=='3') {?>
        "Basic"
        <?php } ?>
        
  </div>
  <div style="padding-left:400px; padding-top:30px;"><?php //echo convert_Frenchdate_to_Sqldate("Samedi, 13 Juillet 2013"); ?>
  <a href="<?php echo site_url("front/utilisateur/agenda/".$objCommercant->IdCommercant);?>" style="text-decoration:none;"><input type="button" value="Retour"/></a></div>
</div>


<div style=" padding-left:200px;">
<?php if (isset($mssg) && $mssg==1) {?> <div align="center" style="text-align:left; font-style:italic;color:#0C0;">Les modifications ont &eacute;t&eacute; enregistr&eacute;es</div><br/><?php }?>
</div>
<div style=" padding-left:120px;">
<?php if (isset($mssg) && $mssg!=1 && $mssg!="") {?> <div align="center" style="text-align:left; font-style:italic;color:#F00;">Une erreur est constatée, Veuillez  vérifier la conformité de vos données</div><br/><?php }?>
</div>
<?php //if(isset($objAgenda)) {  ?>
		   <!--<table  style="text-align:center;">
				<?php// if(isset($show_annonce_btn) && ($show_annonce_btn==1)) { ?>
				<tr>
					<td align="left"><a style="text-decoration:none;" href="<?php// echo site_url("front/annonce/listeMesAnnonces/$objAgenda->IdCommercant");?>"><input type ="button" style="width:200px;"id="btnannonce" value="gestions des annonces" onclick="document.location='<?php// echo site_url("front/annonce/listeMesAnnonces/$objAgenda->IdCommercant");?>';"/>  </a></td>
				</tr>
				<?php// } ?>
				<?php// if(isset($show_bonplan_btn) && ($show_bonplan_btn==1)) { ?>
				 <tr>
					<td align="left"><a  href="<?php// //echo site_url("front/bonplan/listeMesBonplans/$objAgenda->IdCommercant");?>"><input type ="button" style="width:200px;" id="btnbonplan" value="gestions des bons plans" onclick="document.location='<?php// echo site_url("front/bonplan/listeMesBonplans/$objAgenda->IdCommercant");?>';"/> </a></td>
				</tr>
				<?php// //} ?>
			</table>-->
		<?php //} ?>




<div>

<form name="frmInscriptionProfessionnel" id="frmInscriptionProfessionnel" action="<?php echo site_url("front/utilisateur/save"); ?>" method="POST" enctype="multipart/form-data">

<input type="hidden" name="Agenda[id]" id="idAgenda" value="<?php if(isset($objAgenda->id)) echo htmlspecialchars($objAgenda->id); else echo "0"; ?>">

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:20px;">
    <tr> 
        <td class="stl_long_input_platinum_td">
            <label class="label">Vous êtes : </label>
        </td>
        <td>
        	<?php 
			if (isset($objCommercant->IdVille_localisation) && $objCommercant->IdVille_localisation!=""  && $objCommercant->IdVille_localisation!=NULL) {
				$this->load->Model("mdlville");
				$obj_ville_commercant = $this->mdlville->getVilleById($objCommercant->IdVille_localisation);
				$ville_name_to_show = $obj_ville_commercant->Nom;
			} else {
				$ville_name_to_show = "";
			}
			?>
          <input type="text" name="Agenda[nom_societe]" id="nom_societe" value="<?php if(isset($objAgenda->nom_societe) && $objAgenda->nom_societe!="") echo htmlspecialchars($objAgenda->nom_societe); else echo $objCommercant->NomSociete." ".$objCommercant->codepostal_localisation." ".$ville_name_to_show;?>" class="stl_long_input_platinum"/>
        </td>
    </tr>
        <tr> 
        <td class="stl_long_input_platinum_td">
            <label class="label">Nom de la manifestation : </label>
        </td>
        <td>
          <input type="text" name="Agenda[nom_manifestation]" id="nom_manifestation" value="<?php if(isset($objAgenda->nom_manifestation) && $objAgenda->nom_manifestation!="") echo htmlspecialchars($objAgenda->nom_manifestation);?>" class="stl_long_input_platinum"/>
        </td>
    </tr>
    <tr> 
        <td class="stl_long_input_platinum_td">
            <label class="label">Le diffuseur est-il organisateur ? </label>
        </td>
        <td>
          <input type="radio" value="1" name="diffuseur_organisateur_radio" id="diffuseur_organisateur_1" <?php if(isset($objAgenda->diffuseur_organisateur) && $objAgenda->diffuseur_organisateur=="1") {?>checked<?php } ?>/> OUI
          <input type="radio" value="0" name="diffuseur_organisateur_radio" id="diffuseur_organisateur_0" <?php if(isset($objAgenda->diffuseur_organisateur) && $objAgenda->diffuseur_organisateur=="0") {?>checked<?php } ?>/> NON
          <input type="hidden" name="Agenda[diffuseur_organisateur]" id="diffuseur_organisateur" value="<?php if(isset($objAgenda->diffuseur_organisateur)) echo htmlspecialchars($objAgenda->diffuseur_organisateur); ?>">
          &nbsp;<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...." id="img_loaging_agenda"/>
        </td>
    </tr>
</table>

<div class="div_stl_long_platinum">Les coordonnées de l'organisateur</div>


<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:20px;">
  
<tr> 
    <td class="stl_long_input_platinum_td">
        <label class="label">Organisateur : </label>
    </td>
    <td>
        <input type="text" name="Agenda[organisateur]" id="organisateur" value="<?php if(isset($objAgenda->organisateur) && $objAgenda->organisateur!="") echo htmlspecialchars($objAgenda->organisateur);?>"class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Adresse1 : </label>
  	</td>
    <td>
        <input type="text" name="Agenda[adresse1]" id="adresse1" value="<?php if(isset($objAgenda->adresse1)) echo htmlspecialchars($objAgenda->adresse1); ?>"class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Adresse2 : </label>
  	</td>
    <td>
        <input type="text" name="Agenda[adresse2]" id="adresse2" value="<?php if(isset($objAgenda->adresse2)) echo htmlspecialchars($objAgenda->adresse2); ?>"class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Ville : </label>
  </td>
    <td>
        <select name="Agenda[IdVille]" id="IdVille" class="stl_long_input_platinum" onChange="javascript:getCP();"> 
            <option value="0">-- Veuillez choisir --</option>
            <?php if(sizeof($colVilles)) { ?>
                <?php foreach($colVilles as $objVille) { ?>
                    <option <?php if(isset($objAgenda->IdVille) && $objAgenda->IdVille == $objVille->IdVille) { ?>selected="selected"<?php } ?> value="<?php echo $objVille->IdVille; ?>"><?php echo $objVille->Nom; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Code Postal : </label>
  </td>
    <td id="trReponseVille">
    	<input type="text" name="Agenda[codepostal]" id="codepostal" value="<?php if(isset($objAgenda->codepostal)) echo $objAgenda->codepostal; ?>"  class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">T&eacute;l&eacute;phone : </label>
  </td>
    <td>
        <input type="text" name="Agenda[telephone]" id="telephone" value="<?php if(isset($objAgenda->telephone)) echo htmlspecialchars($objAgenda->telephone); ?>"class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Mobile : </label>
  </td>
    <td>
        <input type="text" name="Agenda[mobile]" id="mobile" value="<?php if(isset($objAgenda->mobile)) echo htmlspecialchars($objAgenda->mobile); ?>"class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Fax : </label>
  </td>
    <td>
        <input type="text" name="Agenda[fax]" id="fax" value="<?php if(isset($objAgenda->fax)) echo htmlspecialchars($objAgenda->fax); ?>"class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Courriel : </label>
  </td>
    <td>
        <input type="text" name="Agenda[email]" id="email" value="<?php if(isset($objAgenda->email)) echo htmlspecialchars($objAgenda->email); ?>"class="stl_long_input_platinum"/>
        <div class="FieldError" id="divErrorEmailSociete"></div>
    </td>
</tr>
<tr>
        <td class="stl_long_input_platinum_td">
            <label class="label">Site internet : </label>
      </td>
        <td>
          <input type="text" name="Agenda[siteweb]" id="siteweb" value="<?php if(isset($objAgenda->siteweb) && $objAgenda->siteweb!="") echo htmlspecialchars($objAgenda->siteweb); else echo 'http://www.';  ?>"class="stl_long_input_platinum"/>
        </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Lien Facebook : </label>
  </td>
    <td>
        <input type="text" name="Agenda[facebook]" id="facebook" value="<?php if(isset($objAgenda->facebook)) echo htmlspecialchars($objAgenda->facebook); ?>"class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Lien Twitter : </label>
  </td>
    <td>
    	<input type="text" name="Agenda[twitter]" id="twitter" value="<?php if(isset($objAgenda->twitter)) echo htmlspecialchars($objAgenda->twitter); ?>"class="stl_long_input_platinum"/>
  </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Lien Google + : </label>
  </td>
    <td>
        <input type="text" name="Agenda[googleplus]" id="googleplus" value="<?php if(isset($objAgenda->googleplus)) echo htmlspecialchars($objAgenda->googleplus); ?>" class="stl_long_input_platinum"/>
  </td>
</tr>
</table>



<div class="div_stl_long_platinum">Détails sur la manifestation</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:20px;">
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Date de dépôt : </label>
  </td>
    <td>
        <input type="text" name="Agenda[date_depot]" id="date_depot" value="<?php if(isset($objAgenda->date_depot)) echo convert_Sqldate_to_Frenchdate($objAgenda->date_depot); else echo convert_Sqldate_to_Frenchdate(date("Y-m-d")); ?>" class="stl_long_input_platinum"/>
  </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Catégorie : </label>
  </td>
    <td>
        <select name="Agenda[agenda_categid]" onchange="javascript:listeSousRubrique();" id="IdCategory"class="stl_long_input_platinum">
            <option value="0">-- Veuillez choisir --</option>
            <?php if(sizeof($colCategorie)) { ?>
				<?php foreach($colCategorie as $objcolCategorie_xx) { ?>
                    <option <?php if(isset($objAgenda->agenda_categid) && $objAgenda->agenda_categid == $objcolCategorie_xx->agenda_categid) echo 'selected="selected"';?> value="<?php echo $objcolCategorie_xx->agenda_categid; ?>"><?php echo $objcolCategorie_xx->category; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>
<tr id='trReponseRub'>
    <td class="stl_long_input_platinum_td">
        <label class="label">Sous-catégorie : </label>
  </td>
    <td>
        <select name="Agenda[agenda_subcategid]" id="IdSubcategory"class="stl_long_input_platinum" disabled>
            <option value="0">-- Veuillez choisir --</option>
            <?php if(sizeof($colSousCategorie)) { ?>
				<?php foreach($colSousCategorie as $objcolSousCategorie_xx) { ?>
                    <option <?php if(isset($objAgenda->agenda_subcategid) && $objAgenda->agenda_subcategid == $objcolSousCategorie_xx->agenda_subcategid) echo 'selected="selected"';?> value="<?php echo $objcolSousCategorie_xx->agenda_subcategid; ?>"><?php echo $objcolSousCategorie_xx->subcateg; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>

<?php if (isset($user_groups->id) && $user_groups->id == 3) {} else {?>
<!--<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Intégration à un festival : </label>
  </td>
    <td>
        <select name="Agenda[IdFestival]" id="IdFestival" class="stl_long_input_platinum">
            <option value="0">-- Veuillez choisir --</option>
        </select>
    </td>
</tr>-->
<?php } ?>

<tr>
    <td class="stl_long_input_platinum_td" colspan="2">
        <label class="label">Description de la manifestation : </label>
  </td>
</tr>
<tr>  
    <td colspan="2">
        <textarea name="Agenda[description]" id="description" ><?php if(isset($objAgenda->description)) echo htmlspecialchars($objAgenda->description); ?></textarea>
        <?php echo display_ckeditor($ckeditor0); ?>
    </td>
</tr>
<tr> 
        <td class="stl_long_input_platinum_td">
            <label class="label">Accès handicapés :</label>
        </td>
        <td>
          <input type="radio" value="1" name="handicapes_radio" id="handicapes_1" <?php if(isset($objAgenda->handicapes) && $objAgenda->handicapes=="1") {?>checked<?php } ?>/> OUI
          <input type="radio" value="0" name="handicapes_radio" id="handicapes_0" <?php if(isset($objAgenda->handicapes) && $objAgenda->handicapes=="0") {?>checked<?php } ?>/> NON
          <input type="hidden" name="Agenda[handicapes]" id="handicapes" value="<?php if(isset($objAgenda->handicapes)) echo htmlspecialchars($objAgenda->handicapes); ?>">
        </td>
    </tr>
    <tr> 
        <td class="stl_long_input_platinum_td">
            <label class="label">Parking</label>
        </td>
        <td>
          <input type="radio" value="1" name="parking_radio" id="parking_1" <?php if(isset($objAgenda->parking) && $objAgenda->parking=="1") {?>checked<?php } ?>/> OUI
          <input type="radio" value="0" name="parking_radio" id="parking_0" <?php if(isset($objAgenda->parking) && $objAgenda->parking=="0") {?>checked<?php } ?>/> NON
          <input type="hidden" name="Agenda[parking]" id="parking" value="<?php if(isset($objAgenda->parking)) echo htmlspecialchars($objAgenda->parking); ?>">
        </td>
    </tr>
<tr>
    <td class="stl_long_input_platinum_td">
    Date de début *
    : </td>
    <td>
    <input type="text" name="Agenda[date_debut]" id="date_debut" value="<?php if(isset($objAgenda->date_debut)) echo convert_Sqldate_to_Frenchdate($objAgenda->date_debut); ?>" class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">Date de fin *: </td>
    <td>
    <input type="text" name="Agenda[date_fin]" id="date_fin" value="<?php if(isset($objAgenda->date_fin)) echo convert_Sqldate_to_Frenchdate($objAgenda->date_fin); ?>" class="stl_long_input_platinum"/>
    </td>
</tr>
</table>
<br/><br/>








<div  class="div_stl_long_platinum">Tarif et Horaires</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:20px;">

<tr>
    <td class="stl_long_input_platinum_td" colspan="2">Manifestation gratuite : 
    <input type="checkbox" name="manif_gratuite_checkbox" id="manif_gratuite_checkbox" <?php if(isset($objAgenda->manif_gratuite) && $objAgenda->manif_gratuite=="1") {?>checked<?php } ?>/>
    <input type="hidden" name="Agenda[manif_gratuite]" id="manif_gratuite" value="<?php if(isset($objAgenda->manif_gratuite)) echo $objAgenda->manif_gratuite; ?>"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td" colspan="2">
        <label class="label">Description des Tarifs et Horaires : </label>
  </td>
</tr>
<tr>  
    <td colspan="2">
        <textarea name="Agenda[description_tarif]" id="description_tarif"><?php if(isset($objAgenda->description_tarif)) echo htmlspecialchars($objAgenda->description_tarif); ?></textarea>
        <?php echo display_ckeditor($ckeditor1); ?><br/><br/>
    </td>
</tr>


<?php if (isset($user_groups->id) && $user_groups->id == 3) {} else {?>
<tr>
    <td class="stl_long_input_platinum_td">Lien de réservation en ligne : </td>
    <td>
    <input type="text" name="Agenda[reservation_enligne]" id="reservation_enligne" value="<?php if(isset($objAgenda->reservation_enligne)) echo htmlspecialchars($objAgenda->reservation_enligne); ?>" class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td" colspan="2">Ajouter notre BonPlan : 
    <input type="checkbox" name="ajout_bonplan_checkbox" id="ajout_bonplan_checkbox" <?php if(isset($objAgenda->ajout_bonplan) && $objAgenda->ajout_bonplan=="1") {?>checked<?php } ?>/>
    <input type="hidden" name="Agenda[ajout_bonplan]" id="ajout_bonplan" value="<?php if(isset($objAgenda->ajout_bonplan)) echo $objAgenda->ajout_bonplan; ?>"/>
    &nbsp;<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...." id="img_loaging_agenda_addbonplan"/>
    <span id="spam_error_bonplan_agenda"></span>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td" colspan="2">
        <label class="label">Si promo : précisez les conditions : </label>
  </td>
</tr>
<tr>  
    <td colspan="2">
        <textarea name="Agenda[conditions_promo]" id="conditions_promo" ><?php if(isset($objAgenda->conditions_promo)) echo htmlspecialchars($objAgenda->conditions_promo); ?></textarea>
        <?php echo display_ckeditor($ckeditor2); ?>
    </td>
</tr>

</table>


<div  class="div_stl_long_platinum">Intégration de documents multimédias</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:20px;">

<input type="hidden" name="photo1Associe" id="photo1Associe" value="<?php if(isset($objAgenda->photo1)) echo htmlspecialchars($objAgenda->photo1); ?>" />
<input type="hidden" name="photo2Associe" id="photo2Associe" value="<?php if(isset($objAgenda->photo2)) echo htmlspecialchars($objAgenda->photo2); ?>" />
<input type="hidden" name="photo3Associe" id="photo3Associe" value="<?php if(isset($objAgenda->photo3)) echo htmlspecialchars($objAgenda->photo3); ?>" />
<input type="hidden" name="photo4Associe" id="photo4Associe" value="<?php if(isset($objAgenda->photo4)) echo htmlspecialchars($objAgenda->photo4); ?>" />
<input type="hidden" name="photo5Associe" id="photo5Associe" value="<?php if(isset($objAgenda->photo5)) echo htmlspecialchars($objAgenda->photo5); ?>" />
          

<!--<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Intégration de votre affiche : </label>
        <input type="hidden" name="doc_afficheAssocie" id="doc_afficheAssocie" value="<?php //if(isset($objAgenda->doc_affiche)) echo htmlspecialchars($objAgenda->doc_affiche); ?>" />
  </td>
    <td>
        <?php //if(!empty($objAgenda->doc_affiche)) { ?>
            <?php 
			/*$img_doc_affiche_split_array = explode('.',$objAgenda->doc_affiche);
			$img_doc_affiche_path = "application/resources/front/images/agenda/photoCommercant/".$img_doc_affiche_split_array[0]."_thumb_100_100.".$img_doc_affiche_split_array[1];
			//echo $img_doc_affiche_path;
			if (file_exists($img_doc_affiche_path)== true) {
				echo '<img  src="'.GetImagePath("front/").'/agenda/photoCommercant/'.$img_doc_affiche_split_array[0].'_thumb_100_100.'.$img_doc_affiche_split_array[1].'"/>';
			} else {
				echo image_thumb("application/resources/front/images/agenda/photoCommercant/" . $objAgenda->doc_affiche, 100, 100,'',''); 
			}*/
			?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php //echo $objAgenda->id; ?>','doc_affiche');">Supprimer</a>
        <?php //} else { ?>
        <input type="file" name="Agendadoc_affiche" id="Agendadoc_affiche" value=""  class="stl_long_input_platinum"/>
        <?php //} ?>
        <div id="div_error_taille_doc_affiche" class="div_error_taille_3_4"><?php //if (isset($img_error_verify_array) && in_array("doc_affiche", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>-->

<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Photo 1 : </label>
  </td>
  <?php 
  if (isset($mssg) && $mssg!='1'){
	  $img_error_verify_array = preg_split('/-/',$mssg);
  }
  
  ?>
    <td>
        <?php if(!empty($objAgenda->photo1)) { ?>
            <?php 
			$img_photo1_split_array = explode('.',$objAgenda->photo1);
			$img_photo1_path = "application/resources/front/images/agenda/photoCommercant/".$img_photo1_split_array[0]."_thumb_200_200.".$img_photo1_split_array[1];
			//echo $img_photo1_path;
			if (file_exists($img_photo1_path)== true) {
				echo '<img  src="'.GetImagePath("front/").'/agenda/photoCommercant/'.$img_photo1_split_array[0].'_thumb_200_200.'.$img_photo1_split_array[1].'"/>';
			} else {
				echo image_thumb("application/resources/front/images/agenda/photoCommercant/" . $objAgenda->photo1, 200, 200,'',''); 
			}
			?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objAgenda->id; ?>','photo1');">Supprimer</a>
        <?php } else { ?>
        <input type="file" name="AgendaPhoto1" id="AgendaPhoto1" value=""  class="stl_long_input_platinum"/>
        <?php } ?>
        <div id="div_error_taille_Photo1" class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>

<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Photo 2 : </label>
  </td>
    <td>
        <?php if(!empty($objAgenda->photo2)) { ?>
            <?php 
			$img_photo2_split_array = explode('.',$objAgenda->photo2);
			$img_photo2_path = "application/resources/front/images/agenda/photoCommercant/".$img_photo2_split_array[0]."_thumb_200_200.".$img_photo2_split_array[1];
			//echo $img_photo2_path;
			if (file_exists($img_photo2_path)== true) {
				echo '<img  src="'.GetImagePath("front/").'/agenda/photoCommercant/'.$img_photo2_split_array[0].'_thumb_200_200.'.$img_photo2_split_array[1].'"/>';
			} else {
				echo image_thumb("application/resources/front/images/agenda/photoCommercant/" . $objAgenda->photo2, 200, 200,'',''); 
			}
			?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objAgenda->id; ?>','photo2');">Supprimer</a>
        <?php } else { ?>
        <input type="file" name="AgendaPhoto2" id="AgendaPhoto2" value=""  class="stl_long_input_platinum"/>
        <?php } ?>
        <div id="div_error_taille_Photo2" class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo2", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Photo 3 : </label>
  </td>
    <td>
        <?php if(!empty($objAgenda->photo3)) { ?>
            <?php 
			$img_photo3_split_array = explode('.',$objAgenda->photo3);
			$img_photo3_path = "application/resources/front/images/agenda/photoCommercant/".$img_photo3_split_array[0]."_thumb_200_200.".$img_photo3_split_array[1];
			//echo $img_photo3_path;
			if (file_exists($img_photo3_path)== true) {
				echo '<img  src="'.GetImagePath("front/").'/agenda/photoCommercant/'.$img_photo3_split_array[0].'_thumb_200_200.'.$img_photo3_split_array[1].'"/>';
			} else {
				echo image_thumb("application/resources/front/images/agenda/photoCommercant/" . $objAgenda->photo3, 200, 200,'',''); 
			}
			?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objAgenda->id; ?>','photo3');">Supprimer</a>
        <?php } else { ?>
        <input type="file" name="AgendaPhoto3" id="AgendaPhoto3" value=""  class="stl_long_input_platinum"/>
        <?php } ?>
        <div id="div_error_taille_Photo3" class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo3", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Photo 4 : </label>
  </td>
    <td>
        <?php if(!empty($objAgenda->photo4)) { ?>
            <?php 
			$img_photo4_split_array = explode('.',$objAgenda->photo4);
			$img_photo4_path = "application/resources/front/images/agenda/photoCommercant/".$img_photo4_split_array[0]."_thumb_200_200.".$img_photo4_split_array[1];
			//echo $img_photo4_path;
			if (file_exists($img_photo4_path)== true) {
				echo '<img  src="'.GetImagePath("front/").'/agenda/photoCommercant/'.$img_photo4_split_array[0].'_thumb_200_200.'.$img_photo4_split_array[1].'"/>';
			} else {
				echo image_thumb("application/resources/front/images/agenda/photoCommercant/" . $objAgenda->photo4, 200, 200,'',''); 
			}
			?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objAgenda->id; ?>','photo4');">Supprimer</a>
        <?php } else { ?>
        <input type="file" name="AgendaPhoto4" id="AgendaPhoto4" value=""  class="stl_long_input_platinum"/>
        <?php } ?>
        <div id="div_error_taille_Photo4" class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo4", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Photo 5 : </label>
  </td>
    <td>
        <?php if(!empty($objAgenda->photo5)) { ?>
            <?php 
			$img_photo5_split_array = explode('.',$objAgenda->photo5);
			$img_photo5_path = "application/resources/front/images/agenda/photoCommercant/".$img_photo5_split_array[0]."_thumb_200_200.".$img_photo5_split_array[1];
			//echo $img_photo5_path;
			if (file_exists($img_photo5_path)== true) {
				echo '<img  src="'.GetImagePath("front/").'/agenda/photoCommercant/'.$img_photo5_split_array[0].'_thumb_200_200.'.$img_photo5_split_array[1].'"/>';
			} else {
				echo image_thumb("application/resources/front/images/agenda/photoCommercant/" . $objAgenda->photo5, 200, 200,'',''); 
			}
			?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objAgenda->id; ?>','photo5');">Supprimer</a>
        <?php } else { ?>
        <input type="file" name="AgendaPhoto5" id="AgendaPhoto5" value=""  class="stl_long_input_platinum"/>
        <?php } ?>
        <div id="div_error_taille_Photo5" class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo5", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Lien vers votre vidéo<br/>YouTube ou DailyMotion : </label>
  </td>
    <td>
          <span style="font-family: Verdana, Geneva, sans-serif; font-size:9px;">http://www.youtube.com/watch?v=IMX1M46MQAI</span><br/>
          <input type="text" name="Agenda[video]" id="video" value="<?php if(isset($objAgenda->video)) echo htmlspecialchars($objAgenda->video); ?>"  class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Lien vers un fichier audio : </label>
  </td>
    <td>
        <input type="text" name="Agenda[audio]" id="audio" value="<?php if(isset($objAgenda->audio)) echo htmlspecialchars($objAgenda->audio); ?>"  class="stl_long_input_platinum"/>
    </td>
</tr>
<tr valign="middle">
    <td class="stl_long_input_platinum_td" valign="top">
        <label class="label">Document PDF : </label>
        <input type="hidden" name="pdfAssocie" id="pdfAssocie" value="<?php if(isset($objAgenda->pdf)) echo htmlspecialchars($objAgenda->pdf); ?>" />
  </td>
    <td  valign="top">
        <?php if(!empty($objAgenda->pdf)) { ?>
            <?php //echo $objAgenda->Pdf; ?>
            
            <?php if ($objAgenda->pdf != "" && $objAgenda->pdf != NULL && file_exists("application/resources/front/images/agenda/pdf/".$objAgenda->pdf)== true) {?><a href="<?php echo base_url(); ?>application/resources/front/images/agenda/pdf/<?php echo $objAgenda->pdf ; ?>" target="_blank"><img src="<?php echo base_url(); ?>application/resources/front/images/pdf_icone.png" width="64" alt="PDF" /></a><?php } ?>
            
            
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objAgenda->id; ?>','pdf');">Supprimer</a><br/>
        
    <?php } else { ?>
        <input type="file" name="AgendaPdf" id="AgendaPdf" value=""  class="stl_long_input_platinum"/>
    <?php } ?>
  </td>
</tr>

<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Titre doc PDF : </label>
  </td>
    <td>
    	<input type="text" name="Agenda[titre_pdf]" id="titre_pdf" value="<?php if(isset($objAgenda->titre_pdf)) echo $objAgenda->titre_pdf; ?>"  class="stl_long_input_platinum"/>
    </td>
</tr>

<!--<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Intégration autre document : </label>
        <input type="hidden" name="autre_doc_1Associe" id="autre_doc_1Associe" value="<?php //if(isset($objAgenda->autre_doc_1)) echo htmlspecialchars($objAgenda->autre_doc_1); ?>" />
  </td>
    <td>
        <?php //if(!empty($objAgenda->autre_doc_1)) { ?>
            <?php //echo $objAgenda->autre_doc_1; ?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php //echo $objAgenda->id; ?>','autre_doc_1');">Supprimer</a>
        <?php //} else { ?>
        <input type="file" name="Agendaautre_doc_1" id="Agendaautre_doc_1" value=""  class="stl_long_input_platinum"/>
        <?php } ?>
        <div id="div_error_taille_autre_doc_1" class="div_error_taille_3_4"><?php //if (isset($img_error_verify_array) && in_array("autre_doc_1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>

<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Titre autre doc : </label>
  </td>
    <td>
    	<input type="text" name="Agenda[titre_autre_doc_1]" id="titre_autre_doc_1" value="<?php //if(isset($objAgenda->titre_autre_doc_1)) echo $objAgenda->titre_autre_doc_1; ?>"  class="stl_long_input_platinum"/>
    </td>
</tr>

<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Intégration autre document : </label>
        <input type="hidden" name="autre_doc_2Associe" id="autre_doc_2Associe" value="<?php //if(isset($objAgenda->autre_doc_2)) echo htmlspecialchars($objAgenda->autre_doc_2); ?>" />
  </td>
    <td>
        <?php //if(!empty($objAgenda->autre_doc_2)) { ?>
            <?php //echo $objAgenda->autre_doc_2; ?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php //echo $objAgenda->id; ?>','autre_doc_2');">Supprimer</a>
        <?php //} else { ?>
        <input type="file" name="Agendaautre_doc_2" id="Agendaautre_doc_2" value=""  class="stl_long_input_platinum"/>
        <?php //} ?>
        <div id="div_error_taille_autre_doc_2" class="div_error_taille_3_4"><?php //if (isset($img_error_verify_array) && in_array("autre_doc_2", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>

<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Titre autre doc : </label>
  </td>
    <td>
    	<input type="text" name="Agenda[titre_autre_doc_2]" id="titre_autre_doc_2" value="<?php //if(isset($objAgenda->titre_autre_doc_2)) echo $objAgenda->titre_autre_doc_2; ?>"  class="stl_long_input_platinum"/>
    </td>
</tr>
-->
<?php //} ?>

</table>

<div  class="div_stl_long_platinum">Lieu de l'évènnement</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:20px;">
<tr>
    <td class="stl_long_input_platinum_td">L’adresse est-elle celle<br/>précisée par le diffuseur : </td>
    <td>
    <input type="checkbox" name="adresse_localisation_diffuseur_checkbox" id="adresse_localisation_diffuseur_checkbox" <?php if(isset($objAgenda->adresse_localisation_diffuseur) && $objAgenda->adresse_localisation_diffuseur=="1") {?>checked<?php } ?>/>
    <input type="hidden" name="Agenda[adresse_localisation_diffuseur]" id="adresse_localisation_diffuseur" value="<?php if(isset($objAgenda->adresse_localisation_diffuseur)) echo htmlspecialchars($objAgenda->adresse_localisation_diffuseur); ?>">
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Nom du lieu : </label>
  </td>
    <td>
    	<input type="text" name="Agenda[nom_localisation]" id="nom_localisation" value="<?php if(isset($objAgenda->nom_localisation)) echo $objAgenda->nom_localisation; ?>"  class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Adresse : </label>
  </td>
    <td>
    	<input type="text" name="Agenda[adresse_localisation]" id="adresse_localisation" value="<?php if(isset($objAgenda->adresse_localisation)) echo $objAgenda->adresse_localisation; ?>"  class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Ville : </label>
  </td>
    <td>
        <select name="Agenda[IdVille_localisation]" id="IdVille_localisation" onchange = "getCP_localisation();" class="stl_long_input_platinum">
            <option value="0">-- Veuillez choisir --</option>
            <?php if(sizeof($colVilles)) { ?>
                <?php foreach($colVilles as $objVille_localisation) { ?>
                    <option <?php if(isset($objAgenda->IdVille_localisation) && $objAgenda->IdVille_localisation == $objVille_localisation->IdVille) { echo 'selected="selected"'; $ville_to_show_onn_map = $objVille_localisation->Nom; } ?> value="<?php echo $objVille_localisation->IdVille; ?>"><?php echo $objVille_localisation->Nom; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Code Postal : </label>
  </td>
    <td id="trReponseVille_localisation">
    	<input type="text" name="Agenda[codepostal_localisation]" id="codepostal_localisation" value="<?php if(isset($objAgenda->codepostal_localisation)) echo $objAgenda->codepostal_localisation; ?>"  class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td colspan="2">
    <?php if (isset($ville_to_show_onn_map)) $ville_map = $ville_to_show_onn_map; else $ville_map =''; ?>
    <?php if (isset($objAgenda->adresse_localisation)) { ?>
    <iframe width="630" height="483" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.fr/maps?f=q&amp;source=s_q&amp;hl=fr&amp;geocode=&amp;q=<?php echo $objAgenda->adresse_localisation.", ".$objAgenda->codepostal_localisation." &nbsp;".$ville_map;?>&amp;aq=0&amp;ie=UTF8&amp;hq=&amp;hnear=<?php echo $objAgenda->adresse_localisation.", ".$objAgenda->codepostal_localisation." &nbsp;".$ville_map;?>&amp;t=m&amp;vpsrc=0&amp;output=embed"></iframe>
    <?php } ?>
    </td>
</tr>


</table>

<?php if (isset($user_groups->id) && $user_groups->id == 3) {} else {?>

<!--<div  class="div_stl_long_platinum">Diffusion de votre évènnement</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:20px;">
<tr>
    <td class="stl_long_input_platinum_td">Sur votre compte facebook : </td>
    <td>
    <input type="checkbox" name="diffusion_facebook_checkbox" id="diffusion_facebook_checkbox" <?php //if(isset($objAgenda->diffusion_facebook) && $objAgenda->diffusion_facebook=="1") {?>checked<?php //} ?>/>
    <input type="hidden" name="Agenda[diffusion_facebook]" id="diffusion_facebook" value="<?php //if(isset($objAgenda->diffusion_facebook)) echo htmlspecialchars($objAgenda->diffusion_facebook); ?>"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">Sur votre compte twitter  : </td>
    <td>
    <input type="checkbox" name="diffusion_twitter_checkbox" id="diffusion_twitter_checkbox" <?php //if(isset($objAgenda->diffusion_twitter) && $objAgenda->diffusion_twitter=="1") {?>checked<?php //} ?>/>
    <input type="hidden" name="Agenda[diffusion_twitter]" id="diffusion_twitter" value="<?php //if(isset($objAgenda->diffusion_twitter)) echo htmlspecialchars($objAgenda->diffusion_twitter); ?>"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">Diffusion par RSS : </td>
    <td>
    <input type="checkbox" name="diffusion_rss_checkbox" id="diffusion_rss_checkbox" <?php //if(isset($objAgenda->diffusion_rss) && $objAgenda->diffusion_rss=="1") {?>checked<?php //} ?>/>
    <input type="hidden" name="Agenda[diffusion_rss]" id="diffusion_rss" value="<?php //if(isset($objAgenda->diffusion_rss)) echo htmlspecialchars($objAgenda->diffusion_rss); ?>"/>
    </td>
</tr>
</table>-->

<?php } ?>

<div  class="div_stl_long_platinum">La Diffusion</div>

<fieldset>
<legend>Publication</legend>
<table border="0" cellspacing="0" cellpadding="0" style="padding-left:20px;">
<tr>
    <td><input type="radio" name="admin_agenda_activation" id="admin_agenda_activation_1" <?php if(isset($objAgenda->IsActif) && $objAgenda->IsActif=="1") {?>checked<?php } ?>/></td>
    <td>Actif</td>
</tr>
<tr>
    <td><input type="radio" name="admin_agenda_activation" id="admin_agenda_activation_0" <?php if(isset($objAgenda->IsActif) && $objAgenda->IsActif=="0") {?>checked<?php } ?>/></td>
    <td>En attente de validation</td>
</tr>
<tr>
    <td><input type="radio" name="admin_agenda_activation" id="admin_agenda_activation_2" <?php if(isset($objAgenda->IsActif) && $objAgenda->IsActif=="2") {?>checked<?php } ?>/></td>
    <td>Annulé</td>
</tr>
<tr>
    <td><input type="radio" name="admin_agenda_activation" id="admin_agenda_activation_3" <?php if(isset($objAgenda->IsActif) && $objAgenda->IsActif=="3") {?>checked<?php } ?>/></td>
    <td>Supprimé</td>
</tr>

<?php if (count($objAgenda)!=0) { ?>
<!--<tr>
    <td colspan="2"><p>
    <a style="text-decoration:none;" href="<?php //echo site_url("admin/agenda/deleteAgenda/" . $objAgenda->id . "/" . $objAgenda->IdCommercant) ; ?>" onclick="if (!confirm('voulez-vous vraiment supprimer cette événnement?')){ return false ; }"><input type="button" name="btn_delete_agenda_definitf" id="btn_delete_agenda_definitf" value="Supprimer définitivement de la base de données"/></a>
    </p></td>
</tr>-->
<?php } ?>
</table>
<input type="hidden" name="Agenda[IsActif]" id="IsActif" value="<?php if(isset($objAgenda->IsActif)) echo htmlspecialchars($objAgenda->IsActif); else echo "0"; ?>">
</fieldset>
<p>&nbsp;</p>


<div style="text-align:right;" id="id_save_agenda_before_view"> 
<a href="<?php if(isset($objAgenda->id)) echo site_url("agenda/preview_agenda/".$objAgenda->id); else echo "javascript:alert_save_agenda_before_view();"; ?>" <?php if(isset($objAgenda->id)) echo 'target="_blank"';?>><img src="<?php echo GetImagePath("front/");?>/visualisezsite.png"  alt="previsualisatiton_agenda" title="previsualisatiton_agenda"/></a>
</div>

<div style="height:40px;  margin-top:40px; text-decoration:none; margin-bottom:20px; text-align:center; padding-top:15px;">
<button type="button" class="btnSinscrire" value="Validation du dépôt">
<img src="<?php echo GetImagePath("front/");?>/agenda/wpimages/wp9ef3b26e_06.png"  alt="submit_agenda" title="" style="z-index:1;"/>
</button>
</div>

</form>


<div id="div_error_agenda_submit" style="color:#FF0000; text-align:center; width:100%; margin-top:40px;"></div>


</div>




<?php $this->load->view("front2013/includes/footer_mini_2"); ?>