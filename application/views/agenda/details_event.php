<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("agenda/includes/header", $data); ?>

<div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%" valign="top">
    
        <div style="text-align:center; background-color:#FFCC33; padding-top:10px; padding-bottom:10px; margin-top:10px; margin-bottom:10px;">
        <p>
        <span style='font-family: "Arial",sans-serif;
        font-size: 15px;
        line-height: 12px;'><?php echo $oDetailAgenda->subcateg; ?><br />
        <?php 
		if ($oDetailAgenda->date_debut == $oDetailAgenda->date_fin) echo "Le ".translate_date_to_fr($oDetailAgenda->date_debut); 
		else echo "Du ".translate_date_to_fr($oDetailAgenda->date_debut)." au ".translate_date_to_fr($oDetailAgenda->date_fin);
		?>
        </span><br />
        <span style='font-family: "Arial",sans-serif;
        font-size: 21px;
        font-weight: 700;
        line-height: 1.19em;'><?php echo $oDetailAgenda->nom_manifestation; ?></span><br />
        <span style='font-family: "Arial",sans-serif;
        font-size: 15px;
        line-height: 12px;'><?php echo $oDetailAgenda->ville ; ?> - <?php echo $oDetailAgenda->adresse_localisation ; ?> - <?php echo $oDetailAgenda->codepostal_localisation ; ?></span>
        </p>
        <p>
        <span style='font-family: "Arial",sans-serif;
        font-size: 13px;
        font-weight: 700;
        line-height: 1.23em;'>Organisateur :</span><br />
        <?php echo $oDetailAgenda->organisateur ; ?>, <?php echo $oDetailAgenda->adresse_localisation ; ?> <?php echo $oDetailAgenda->codepostal_localisation ; ?><br/>
        <?php echo $oDetailAgenda->ville ; ?><br/>
        <?php if ($oDetailAgenda->telephone!="") echo "Tél. ".$oDetailAgenda->telephone ; ?>
        <?php if ($oDetailAgenda->mobile!="") echo "<br/>Mobile. ".$oDetailAgenda->mobile ; ?>
        <?php if ($oDetailAgenda->fax!="") echo "<br/>Fax. ".$oDetailAgenda->fax ; ?>
        </p>
        </div>
        
        <div>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:center;">
          <tr>
          	<?php if ($oDetailAgenda->siteweb != "") $siteweb_agenda = $oDetailAgenda->siteweb; else $siteweb_agenda = "javascript:void(0);";?>
            <td><a href="<?php echo $siteweb_agenda;?>" target="_blank"><img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpbc918667_06.png" /></a></td>
            <td><a href="<?php echo "mailto:".$oDetailAgenda->email;?>"><img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpb6e45b55_06.png" /></a></td>
            <td><img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpafa61874_06.png" /></td>
          </tr>
          <tr>
            <td>Site Web</td>
            <td>Couriel</td>
            <td>Programmer<br/>une alarme</td>
          </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp68e4823b_06.png" /></td>
            <td><img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpc5257d1a_06.png" /></td>
            <td><img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp6658722d_06.png" /></td>
          </tr>
          <tr>
            <td>Imprimer<br/>cet article</td>
            <td>Fichier<br/>audio</td>
            <td>Fichier<br/>vidéo</td>
          </tr>
        </table>
        </div>
        
    </td>
    <td width="50%" style="text-align:right;" valign="top">
    	<div style="margin-top:10px; margin-left:10px;">
    	<!--<img src="<?php //echo GetImagePath("front/"); ?>/agenda/wpimages/wp9b28ba94_05_06.jpg"  width="287" height="411" border="0" />-->
		<?php $this->load->view("agenda/includes/slide_details_agenda", $data); ?>
    	</div>
    </td>
  </tr>
  <tr style="text-align:center;">
  	<?php 
	$this->load->model("mdlcommercant");
	$oCommercant = $this->mdlcommercant->infoCommercant($oDetailAgenda->IdCommercant) ;
	?>
    <td><input type="button" value="Accès à la fiche de ce déposant" style=" margin-top:20px;"/></td>
    <td><input type="button" value="Tous les articles de ce déposant" style=" margin-top:20px;" onClick="javascript:document.location='<?php echo site_url($oCommercant->nom_url."/agenda");?>'"/></td>
  </tr>
</table>

</div>


<div style=" margin-bottom:20px; margin-top:20px;">
<span style='font-family: "Arial",sans-serif;
    font-size: 15px;
    font-weight: 700;
    line-height: 1.2em;'><?php echo $oDetailAgenda->nom_manifestation; ?></span><br />
<p><?php echo $oDetailAgenda->description; ?></p>
</div>


<div style='font-family: "Arial",sans-serif;
    font-size: 13px;
    font-weight: 700;
    height: 22px;
    line-height: 1.23em;
    padding-left: 15px;
    padding-top: 10px; background-image:url(<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpe50c6d73_06.png); background-repeat:no-repeat;'>
Tarifs & horaires
</div>
<div><p><?php echo $oDetailAgenda->description_tarif; ?></p></div>


<div style="text-align:center;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:center;">
  <tr>
    <td width="50%">
        <div style=" background-image:url(<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpa3ac994a_06.png); background-repeat:no-repeat; width:300px; height:150px;color: #FFFFFF;
    height: 120px; text-align:left;
    padding: 15px;
    width: 270px;">
			<strong>Tarif promotionnel</strong><br/> 
            <?php echo $oDetailAgenda->conditions_promo; ?>      
        </div>
    </td>
    <td width="50%">
    <?php if ($oDetailAgenda->reservation_enligne != "") $reservation_enligne_agenda = $oDetailAgenda->reservation_enligne; else $reservation_enligne_agenda = "javascript:void(0);";?>
    <a href="<?php echo $reservation_enligne_agenda; ?>"><img src="<?php echo GetImagePath("front/"); ?>/agenda/img_reser_online.png" alt="agenda" /></a>
    </td>
  </tr>
</table>
</div>

<div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%">
    <div style='font-family: "Arial",sans-serif;
        font-size: 13px;
        font-weight: 700;
        height: 22px;
        line-height: 1.23em; margin-bottom:20px;
        padding-left: 15px;
        padding-top: 8px; background-image:url(<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp1009df42_06.png); background-repeat:no-repeat;'>
    Partager cet article
    </div>
    </td>
    <td width="50%">
    <div style='font-family: "Arial",sans-serif;
        font-size: 13px;
        font-weight: 700; margin-bottom:20px;
        height: 22px;
        line-height: 1.23em;
        padding-left: 15px;
        padding-top: 8px; background-image:url(<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp1009df42_06.png); background-repeat:no-repeat;'>
    Espace journaliste
    </div>
    </td>
  </tr>
  <tr>
    <td>
    
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:center;">
          <tr>
            <td><a href="javascript:void(0);"><img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpe5c549b5_06.png" /></a></td>
            <td><a href="javascript:void(0);"><img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp3382972e_06.png" /></a></td>
            <?php if ($oDetailAgenda->twitter != "") $twitter_agenda = $oDetailAgenda->twitter; else $twitter_agenda = "javascript:void(0);";?>
            <td><a href="<?php echo $twitter_agenda;?>" target="_blank"><img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp05886a9f_06.png" /></a></td>
          </tr>
          <tr>
            <td>Ajouter<br/>à mes favoris</td>
            <td>Proposer<br/>à un ami</td>
            <td>Lien<br/>Twitter</td>
          </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
          	<?php if ($oDetailAgenda->facebook != "") $facebook_agenda = $oDetailAgenda->facebook; else $facebook_agenda = "javascript:void(0);";?>
            <?php if ($oDetailAgenda->googleplus != "") $googleplus_agenda = $oDetailAgenda->googleplus; else $googleplus_agenda = "javascript:void(0);";?>
            <td><a href="<?php echo $facebook_agenda;?>" target="_blank"><img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpa29af2ce_06.png" /></a></td>
            <td><a href="<?php echo $googleplus_agenda;?>" target="_blank"><img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp182fea66_06.png" /></a></td>
            <td><a href="<?php echo "mailto:".$oDetailAgenda->email;?>"><img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpb6e45b55_06.png" /></a></td>
          </tr>
          <tr>
            <td>Lien<br/>Facebook</td>
            <td>Lien<br/>Google+</td>
            <td>Couriel</td>
          </tr>
        </table>
    
    </td>
    <td>
    <div style="text-align:center;">
    <p>
    Disponibilité de ce module<br/>
    Septembre 2013
    </p>
    <p>
    <img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wp056c85bd_06.png" />
    </p>
    </div>
    </td>
  </tr>
</table>
</div>


<div style='font-family: "Arial",sans-serif;
    font-size: 13px;
    font-weight: 700;
    height: 24px; margin-top:30px;
    line-height: 1.23em;
    padding-left: 15px;
    padding-top: 10px; background-image:url(<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpe50c6d73_06.png); background-repeat:no-repeat;'>
Localisation de l'événnement
</div>

<div>
<p><strong><?php echo $oDetailAgenda->ville ; ?>, <?php echo $oDetailAgenda->adresse_localisation ; ?> <?php echo $oDetailAgenda->codepostal_localisation ; ?></strong></p>
<p>
<?php if (isset($oDetailAgenda->ville)) $ville_map = $oDetailAgenda->ville; else $ville_map =''; ?>
<?php if (isset($oDetailAgenda->adresse_localisation)) { ?>
<iframe width="600" height="483" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.fr/maps?f=q&amp;source=s_q&amp;hl=fr&amp;geocode=&amp;q=<?php echo $oDetailAgenda->adresse_localisation.", ".$oDetailAgenda->codepostal_localisation." &nbsp;".$ville_map;?>&amp;aq=0&amp;ie=UTF8&amp;hq=&amp;hnear=<?php echo $oDetailAgenda->adresse_localisation.", ".$oDetailAgenda->codepostal_localisation." &nbsp;".$ville_map;?>&amp;t=m&amp;vpsrc=0&amp;output=embed"></iframe>
<?php } ?>
</p>
</div>


<div style="margin-bottom:10px; margin-top:10px;">
<strong>Fiche déposée</strong><br/>
le <?php if ($oDetailAgenda->date_depot!="" && $oDetailAgenda->date_depot!="0000-00-00" && $oDetailAgenda->date_depot!="0") echo translate_date_to_fr($oDetailAgenda->date_depot); ?><br/>
par <?php echo $oDetailAgenda->organisateur ; ?> de <?php echo $oDetailAgenda->ville ; ?><br/>
<strong>Dernière modification :</strong>  <?php if ($oDetailAgenda->last_update!="" && $oDetailAgenda->last_update!="0000-00-00" && $oDetailAgenda->last_update!="0") echo translate_date_to_fr($oDetailAgenda->last_update); ?>
</div>



<div style="margin-bottom:10px; margin-top:10px;">
<center>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>
<script type="application/javascript">
$(document).ready(function() {
        $("#btn_submit_form_module_detailbonnplan").click(function(){
			//alert('test form submit');
			txtErrorform = "";
			
            var txtError_text_mail_form_module_detailbonnplan = "";
            var text_mail_form_module_detailbonnplan = $("#text_mail_form_module_detailbonnplan").val();
            if(text_mail_form_module_detailbonnplan=="") {
                //$("#divErrorform_module_detailbonnplan").html('<font color="#FF0000">Veuillez saisir votre demande</font>');
                txtErrorform += "1";
				$("#text_mail_form_module_detailbonnplan").css('border-color', 'red');
				$("#text_mail_form_module_detailbonnplan").focus();
            } else {
				$("#text_mail_form_module_detailbonnplan").css('border-color', '#E3E1E2');
			}
			
			var nom_mail_form_module_detailbonnplan = $("#nom_mail_form_module_detailbonnplan").val();
            if(nom_mail_form_module_detailbonnplan=="") {
                txtErrorform += "- Veuillez indiquer Votre nom_mail_form_module_detailbonnplan <br/>"; 
				$("#nom_mail_form_module_detailbonnplan").css('border-color', 'red');
				$("#nom_mail_form_module_detailbonnplan").focus();
            } else {
				$("#nom_mail_form_module_detailbonnplan").css('border-color', '#E3E1E2');
			}
            
            var email_mail_form_module_detailbonnplan = $("#email_mail_form_module_detailbonnplan").val();
            if(email_mail_form_module_detailbonnplan=="" || !isEmail(email_mail_form_module_detailbonnplan)) {
                txtErrorform += "- Veuillez indiquer Votre email_mail_form_module_detailbonnplan <br/>"; 
                //alert("Veuillez indiquer Votre nom");
                $("#email_mail_form_module_detailbonnplan").css('border-color', 'red');
				$("#email_mail_form_module_detailbonnplan").focus();
            } else {
				$("#email_mail_form_module_detailbonnplan").css('border-color', '#E3E1E2');
			}
			
			
			
            if(txtErrorform == "") {
                $("#form_module_detailbonnplan").submit();
            }
        });
		
		
		
    });
</script>
<?php if(isset($user_ion_auth)) {?>
<style type="text/css">
.inputhidder {
	visibility:hidden;
}
</style>
<?php }?>

<table border="0" align="center" style="text-align:center; width:100%;">
  <tr>
    <td style="text-align:left;"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/img_plus_infos_annonce.png" alt="img"></td>
    <td>
<form method="post" name="form_module_detailbonnplan" id="form_module_detailbonnplan" action="" enctype="multipart/form-data">    
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="inputhidder">
    <td>Votre Nom</td>
    <td><input id="nom_mail_form_module_detailbonnplan" name="nom_mail_form_module_detailbonnplan" type="text" value="<?php if(isset($user_ion_auth) && $user_ion_auth->first_name!="") echo $user_ion_auth->first_name;?>"></td>
  </tr>
  <tr class="inputhidder">
    <td>Votre Téléphone</td>
    <td><input id="tel_mail_form_module_detailbonnplan" name="tel_mail_form_module_detailbonnplan" type="text" value="<?php if(isset($user_ion_auth) && $user_ion_auth->phone!="") echo $user_ion_auth->phone;?>"></td>
  </tr>
  <tr class="inputhidder">
    <td>Votre Email</td>
    <td><input id="email_mail_form_module_detailbonnplan" name="email_mail_form_module_detailbonnplan" type="text" value="<?php if(isset($user_ion_auth) && $user_ion_auth->email!="") echo $user_ion_auth->email;?>"></td>
  </tr>
  <tr>
    <td colspan="2">
    <textarea id="text_mail_form_module_detailbonnplan" cols="28" rows="7" name="text_mail_form_module_detailbonnplan" style="width:246px; height:130px;font-family:Arial, Helvetica, sans-serif; font-size:10px;"></textarea>
    </td>
  </tr>
  <tr>
    <td colspan="2">
    <input id="btn_reset_form_module_detailbonnplan" type="reset" value="Effacer" style="width:82px; height:22px;">
<input id="btn_submit_form_module_detailbonnplan" type="button" name="btn_submit_form_module_detailbonnplan" value="Envoyer" style="width:90px; height:22px;">
    </td>
  </tr>
</table>
</form>
    </td>
  </tr>
  <tr>
  <td>&nbsp;</td>
  <td><div id="divErrorform_module_detailbonnplan"><?php if (isset($mssg_envoi_module_detail_bonplan)) echo $mssg_envoi_module_detail_bonplan;?></div></td>
  </tr>
</table>
<br/>
<div style='font-family: "Arial",sans-serif;
    font-size: 12px; margin-bottom:40px;
    font-weight: 700;
    line-height: 1.25em;'>Vous avez une question particulière à nous poser, adressez nous un mail express</div>
</center>
</div>
















<?php $this->load->view("agenda/includes/footer"); ?>