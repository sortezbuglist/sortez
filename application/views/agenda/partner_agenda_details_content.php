<?php $data['currentpage'] = 'agenda_details';?>
<div style="background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/bg_head_agenda_details.png); background-position:center top; background-repeat:no-repeat; padding-right:5px;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="300" valign="top">
    
        <div style="text-align:center;padding-top:10px; padding-bottom:0px; margin-top:0px; margin-bottom:0px; color:#FFF; height:110px;">
        <p>
        <span style='font-family: "Arial",sans-serif;
        font-size: 12px; font-weight:bold;
        line-height: 16px;'><?php echo $oDetailAgenda->subcateg; ?><br />
        <?php //echo translate_date_to_fr($oDetailAgenda->date_debut); ?>
        <?php 
		if ($oDetailAgenda->date_debut == $oDetailAgenda->date_fin) echo "Le ".translate_date_to_fr($oDetailAgenda->date_debut); 
		else echo "Du ".translate_date_to_fr($oDetailAgenda->date_debut)." au ".translate_date_to_fr($oDetailAgenda->date_fin);
		?>
        </span><br />
        <span style='font-family: "Arial",sans-serif;
        font-size: 16px;line-height: 16px;
        font-weight: bold;
        '><?php echo $oDetailAgenda->nom_manifestation; ?></span><br />
        <span style='font-family: "Arial",sans-serif;
        font-size: 12px;line-height: 16px;
        '><?php echo $oDetailAgenda->ville ; ?> - <?php echo $oDetailAgenda->adresse_localisation ; ?> - <?php echo $oDetailAgenda->codepostal_localisation ; ?></span>
        
        </p>
        </div>
        
        <div style="text-align:center;padding-bottom:0px; margin-top:0px; margin-bottom:0px; height:75px; margin-bottom:20px;">
        <?php 
		$this->load->model("mdlcommercant");
		$oCommercant = $this->mdlcommercant->infoCommercant($oDetailAgenda->IdCommercant) ;
		?>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100" style="text-align:center;">
          <tr>
            <td>
            <span style='font-family: "Arial",sans-serif;font-size: 12px;line-height: 16px;'>
			<strong><?php echo "Organisateur : ".$oDetailAgenda->organisateur ; ?></strong><!--,<br/>--><?php //echo $oDetailAgenda->adresse_localisation ; ?> <?php //echo $oDetailAgenda->codepostal_localisation ; ?>
            <?php //echo $oDetailAgenda->ville ; ?><br/>
            <strong>
            <?php if ($oDetailAgenda->telephone!="" || $oDetailAgenda->mobile!="") { ?>
            Renseignements : 
            <?php } ?>
            <?php if ($oDetailAgenda->telephone!="") echo "Tél. ".$oDetailAgenda->telephone ; ?>
            <?php if ($oDetailAgenda->mobile!="") echo "<br/>Mobile. ".$oDetailAgenda->mobile ; ?>
            <?php //if ($oDetailAgenda->fax!="") echo "<br/>Fax. ".$oDetailAgenda->fax ; ?>
            </strong>
            </span>
            </td>
          </tr>
        </table>
        </div>
        
        
        <div>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:center;">
          <tr>
          	<?php if (isset($oDetailAgenda->siteweb) && $oDetailAgenda->siteweb != "") $siteweb_agenda = $oDetailAgenda->siteweb; else $siteweb_agenda = "javascript:void(0);";?>
            <td>
            	<a href="<?php echo site_url($oCommercant->nom_url."/agenda");?>"><img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/img_adet_1.png" /></a>
            </td>
            <td>
                <?php if (isset($siteweb_agenda) && $siteweb_agenda!='' && $siteweb_agenda!=null && $siteweb_agenda!='http://www.') {?>
                <a href="<?php echo $siteweb_agenda;?>" title="Site web"><img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/img_adet_2.png" /></a>
                <?php } else { ?>
                    <a href="javascript:void(0);" title="Site web"><img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/img_adet_2_gris.png" /></a>
                <?php } ?>
            </td>
            <td>
            	<a href="<?php echo "mailto:".$oDetailAgenda->email;?>"><img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/img_adet_3.png" /></a>
            </td>
            <td><a href="javascript:window.print()"><img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/img_adet_4.png" /></a>
            </td>
          </tr>
          <!--<tr>
            <td>Site Web</td>
            <td>Couriel</td>
            <td>Programmer<br/>une alarme</td>
          </tr>-->
        <!--<tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>-->
          
          
          
          <?php 
		  	$this->load->model("user") ;
			$thisss =& get_instance();
			$thisss->load->library('ion_auth');
			$this->load->model("ion_auth_used_by_club");
			$user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oDetailAgenda->IdCommercant);
			if (isset($user_ion_auth_id)) $user_groups = $thisss->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
			if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;
			
			
			if ($thisss->ion_auth->logged_in()){
				$user_ion_auth = $thisss->ion_auth->user()->row();
				$iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
				if ($iduser==null || $iduser==0 || $iduser==""){
					$iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
				}
			} else $iduser=0;
			if (isset($iduser) && $iduser!=0 && $iduser!= NULL && $iduser !="") {
				$oCommercantFavoris = $this->user->verify_favoris($iduser, $oDetailAgenda->IdCommercant);
			}
			
		  ?>
          
          
          
          <tr>
            <td>
            	<img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/img_adet_5.png" />
            </td>
            <td>
                <?php if (isset($oCommercantFavoris) && $oCommercantFavoris != NULL && $oCommercantFavoris->Favoris == "1") { ?>
                    <a href="<?php echo site_url('front/utilisateur/delete_favoris/'.$oDetailAgenda->IdCommercant);?>" title="Supprimer de mes Favoris">
                        <img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/img_adet_6.png" />
                    </a>
                <?php } else { ?>
                    <a href="<?php echo site_url('front/utilisateur/ajout_favoris/'.$oDetailAgenda->IdCommercant);?>" title="Ajouter à mes Favoris">
                        <img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/img_adet_6.png" />
                    </a>
                <?php } ?>
            </td>
            <td>
                <a href="<?php echo site_url($commercant_url_recommandation);?>">
                	<img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/img_adet_7.png" />
                </a>
            </td>
            <td>
            <a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;pub=xa-4ab3b0412ad55820" title="Partager">
            <img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/img_adet_8.png" />
            </a>
            </td>
          </tr>
          <!--<tr>
            <td>Imprimer<br/>cet article</td>
            <td>Fichier<br/>audio</td>
            <td>Fichier<br/>vidéo</td>
          </tr>-->
        </table>
        </div>
        
    </td>
    <td width="230" style="text-align:right;" valign="top">
    
    	<!--<img src="<?php //echo GetImagePath("front/"); ?>/agenda/wpimages/wp9b28ba94_05_06.jpg"  width="260" border="0" />-->
		<?php $this->load->view("agenda/includes/slide_details_agenda", $data); ?>
    
    </td>
  </tr>
  <!--<tr style="text-align:center;">
  	
    <td>&nbsp;</td>
    <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td style="text-align:right;"><a href="<?php // echo site_url($oCommercant->nom_url."/agenda");?>"><img src="<?php // echo GetImagePath("front/"); ?>/agenda/btn_back_agenda.png" /></a></td>
            <td style="padding-left:10px; text-align:left">Retour vers le menu<br/>général de l’agenda</td>
          </tr>
        </table>
    </td>
  </tr>-->
</table>

</div>


<div style='font-family: "Arial",sans-serif;
    font-size: 13px; margin-top:10px;
    font-weight: 700;
    height: 18px;
    line-height: 1.23em; color:#FFFFFF;
    padding-left: 15px;
    padding-top: 5px; background-color:#4C4C4C'>
Description
</div>

<div style=" margin-bottom:20px; margin-top:0px; margin-top:15px;">
<!--<span style='font-family: "Arial",sans-serif;
    font-size: 15px;
    font-weight: 700;
    line-height: 1.2em;'><?php //echo $oDetailAgenda->nom_manifestation; ?></span><br />-->
<p><?php echo $oDetailAgenda->description; ?></p>
</div>


<div style='font-family: "Arial",sans-serif;
    font-size: 13px;
    font-weight: 700;
    height: 18px;
    line-height: 1.23em; color:#FFFFFF;
    padding-left: 15px;
    padding-top: 5px; background-color:#4C4C4C'>
Tarifs & horaires
</div>
<div><p><?php echo $oDetailAgenda->description_tarif; ?></p></div>


<div style="text-align:center;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:center;">
  <tr>
    <td width="50%">
    <?php if (isset($oDetailAgenda->conditions_promo) && $oDetailAgenda->conditions_promo!="") { ?>
        <div style=" background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/wp3224239b_06.png); background-repeat:no-repeat; width:292px; height:150px;color: #FFFFFF;
    height: 120px; text-align:left;
    padding: 15px;
    width: 278px;">
			<strong>Tarif promotionnel</strong><br/> 
            <?php echo $oDetailAgenda->conditions_promo; ?>      
        </div>
    <?php } ?>
    </td>
    <td width="50%">
    <?php if ($oDetailAgenda->reservation_enligne != "") {
		$reservation_enligne_agenda = $oDetailAgenda->reservation_enligne; 
		?>
		<a href="<?php echo $reservation_enligne_agenda; ?>"><img src="<?php echo GetImagePath("front/"); ?>/agenda/img_reser_online.png" alt="agenda" /></a>
		<?php
	} else {
		$reservation_enligne_agenda = "javascript:void(0);";
		?>
        <a href="<?php echo $reservation_enligne_agenda; ?>"><img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/img_reser_online_gris.png" alt="agenda" /></a>
        <?php
	}?>
    
    </td>
  </tr>
</table>
</div>

<div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%">
    <div style='font-family: "Arial",sans-serif;
        font-size: 13px;
        font-weight: 700;
        height: 23px; color:#FFF; height:16px; padding-bottom:2px;
        line-height: 1.23em; margin-bottom:5px;
        padding-left: 15px; margin-right:5px; margin-top:10px;
        padding-top: 5px; background-color:#4C4C4C;'>
    Documents complémentaires
    </div>
    </td>
    <td width="50%">
    <div style='font-family: "Arial",sans-serif;
        font-size: 13px;
        font-weight: 700; margin-bottom:5px;
        height: 18px;
        line-height: 1.23em; color:#FFF; background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/dossier_presse_bg.png);
        padding-left: 15px; background-repeat:no-repeat;
        padding-top: 15px;'>
    Dossier presse
    </div>
    </td>
  </tr>
  <tr>
    <td valign="top">
    
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:center;">
          <tr>
            <td>
            <?php if ($oDetailAgenda->video != "") { ?>
				<a href="<?php echo $oDetailAgenda->video; ?>" target="_blank" title="Vidéo"><img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/wp2cfa761b_06.png" alt="video" /></a>
			<?php } else {?>
				<img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/wp18960ecb_06.png" alt="video" />
			<?php }?>
            </td>
            <td>
            <?php if ($oDetailAgenda->audio != "") { ?>
				<a href="<?php echo base_url()."/application/resources/front/audios/".$oDetailAgenda->audio; ?>" title="Audio" target="_blank"><img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/wp558454a0_06.png" alt="video" /></a>
			<?php } else {?>
				<img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/wp58a2287a_06.png" alt="audio" />
			<?php }?>
            </td>
            <td>
            <?php if ($oDetailAgenda->pdf != "") { ?>
				<a href="<?php echo base_url()."/application/resources/front/images/agenda/pdf/".$oDetailAgenda->pdf; ?>" title="<?php echo $oDetailAgenda->titre_pdf ?>" target="_blank"><img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/wp26f97948_06.png" alt="pdf" /></a>
			<?php } else {?>
				<img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/wp057fe5de_06.png" alt="pdf" />
			<?php }?>
            </td>
          </tr>
         <!-- <tr>
            <td>Ajouter<br/>à mes favoris</td>
            <td>Proposer<br/>à un ami</td>
            <td>Lien<br/>Twitter</td>
          </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
          	<?php // if ($oDetailAgenda->facebook != "") $facebook_agenda = $oDetailAgenda->facebook; else $facebook_agenda = "javascript:void(0);";?>
            <?php // if ($oDetailAgenda->googleplus != "") $googleplus_agenda = $oDetailAgenda->googleplus; else $googleplus_agenda = "javascript:void(0);";?>
            <td><a href="<?php // echo $facebook_agenda;?>" target="_blank"><img src="<?php // echo GetImagePath("front/"); ?>/agenda/wpimages/wpa29af2ce_06.png" /></a></td>
            <td><a href="<?php // echo $googleplus_agenda;?>" target="_blank"><img src="<?php // echo GetImagePath("front/"); ?>/agenda/wpimages/wp182fea66_06.png" /></a></td>
            <td><a href="<?php // echo "mailto:".$oDetailAgenda->email;?>"><img src="<?php // echo GetImagePath("front/"); ?>/agenda/wpimages/wpb6e45b55_06.png" /></a></td>
          </tr>
          <tr>
            <td>Lien<br/>Facebook</td>
            <td>Lien<br/>Google+</td>
            <td>Couriel</td>
          </tr>-->
        </table>
    
    </td>
    <td>
    <div style="text-align:center;">
    Module optionnel<br/>
    disponible fin octobre 2013<br/>
    <img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/dossier_presse.png" />
    </div>
    </td>
  </tr>
</table>
</div>


<div style='font-family: "Arial",sans-serif;
    font-size: 13px;
    font-weight: 700;
    height: 20px; margin-top:0px;
    line-height: 1.23em; color:#FFFFFF;
    padding-left: 15px;
    padding-top: 5px; background-color:#4C4C4C;'>
Localisation de l'événnement
</div>

<div>
<p><strong><?php echo $oDetailAgenda->nom_localisation ; ?> <?php echo $oDetailAgenda->ville ; ?>, <?php echo $oDetailAgenda->adresse_localisation ; ?> <?php echo $oDetailAgenda->codepostal_localisation ; ?></strong></p>
<p>
<?php if (isset($oDetailAgenda->ville)) $ville_map = $oDetailAgenda->ville; else $ville_map =''; ?>
<?php if (isset($oDetailAgenda->adresse_localisation)) { ?>
<iframe width="535" height="483" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.fr/maps?f=q&amp;source=s_q&amp;hl=fr&amp;geocode=&amp;q=<?php echo $oDetailAgenda->adresse_localisation.", ".$oDetailAgenda->codepostal_localisation." &nbsp;".$ville_map;?>&amp;aq=0&amp;ie=UTF8&amp;hq=&amp;hnear=<?php echo $oDetailAgenda->adresse_localisation.", ".$oDetailAgenda->codepostal_localisation." &nbsp;".$ville_map;?>&amp;t=m&amp;vpsrc=0&amp;output=embed"></iframe>
<?php } ?>
</p>
</div>


<div style="margin-bottom:10px; margin-top:10px;">
<strong>Fiche déposée</strong><br/>
le <?php if ($oDetailAgenda->date_depot!="" && $oDetailAgenda->date_depot!="0000-00-00" && $oDetailAgenda->date_depot!="0") echo translate_date_to_fr($oDetailAgenda->date_depot); ?><br/>
par <?php echo $oDetailAgenda->organisateur ; ?> de <?php echo $oDetailAgenda->ville ; ?>
<?php if ($oDetailAgenda->telephone!="") echo "<br/>Tél. ".$oDetailAgenda->telephone ; ?>
<?php if ($oDetailAgenda->mobile!="") echo "<br/>Mobile. ".$oDetailAgenda->mobile ; ?>
<?php if ($oDetailAgenda->fax!="") echo "<br/>Fax. ".$oDetailAgenda->fax; ?>
<br/>
<strong>Dernière modification :</strong>  <?php if ($oDetailAgenda->last_update!="" && $oDetailAgenda->last_update!="0000-00-00" && $oDetailAgenda->last_update!="0") echo translate_date_to_fr($oDetailAgenda->last_update); ?>
</div>



<div style="margin-top:10px;">
<center>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>
<script type="application/javascript">
$(document).ready(function() {
        $("#btn_submit_form_module_detailbonnplan").click(function(){
			//alert('test form submit');
			txtErrorform = "";
			
            var txtError_text_mail_form_module_detailbonnplan = "";
            var text_mail_form_module_detailbonnplan = $("#text_mail_form_module_detailbonnplan").val();
            if(text_mail_form_module_detailbonnplan=="") {
                //$("#divErrorform_module_detailbonnplan").html('<font color="#FF0000">Veuillez saisir votre demande</font>');
                txtErrorform += "1";
				$("#text_mail_form_module_detailbonnplan").css('border-color', 'red');
				$("#text_mail_form_module_detailbonnplan").focus();
            } else {
				$("#text_mail_form_module_detailbonnplan").css('border-color', '#E3E1E2');
			}
			
			var nom_mail_form_module_detailbonnplan = $("#nom_mail_form_module_detailbonnplan").val();
            if(nom_mail_form_module_detailbonnplan=="") {
                txtErrorform += "- Veuillez indiquer Votre nom_mail_form_module_detailbonnplan <br/>"; 
				$("#nom_mail_form_module_detailbonnplan").css('border-color', 'red');
				$("#nom_mail_form_module_detailbonnplan").focus();
            } else {
				$("#nom_mail_form_module_detailbonnplan").css('border-color', '#E3E1E2');
			}
            
            var email_mail_form_module_detailbonnplan = $("#email_mail_form_module_detailbonnplan").val();
            if(email_mail_form_module_detailbonnplan=="" || !isEmail(email_mail_form_module_detailbonnplan)) {
                txtErrorform += "- Veuillez indiquer Votre email_mail_form_module_detailbonnplan <br/>"; 
                //alert("Veuillez indiquer Votre nom");
                $("#email_mail_form_module_detailbonnplan").css('border-color', 'red');
				$("#email_mail_form_module_detailbonnplan").focus();
            } else {
				$("#email_mail_form_module_detailbonnplan").css('border-color', '#E3E1E2');
			}
			
			
			
            if(txtErrorform == "") {
                $("#form_module_detailbonnplan").submit();
            }
        });
		
		
		
    });
</script>
<?php if(isset($user_ion_auth)) {?>
<style type="text/css">
.inputhidder {
	visibility:hidden;
}
</style>
<?php }?>
<div style='font-family: "Arial",sans-serif;
    font-size: 12px; margin-bottom:10px; margin-top:20px;
    font-weight: 700;
    line-height: 1.25em;'>Vous avez une question particulière à nous poser, adressez nous un mail express</div>
</center>
</div>

<table border="0" align="center" style="text-align:center; width:100%;">
  <tr>
    <td style="text-align:left;"><img src="<?php echo GetImagePath("front/"); ?>/btn_new/info_annonce_img.png" alt="img" width="250"></td>
    <td>
<form method="post" name="form_module_detailbonnplan" id="form_module_detailbonnplan" action="" enctype="multipart/form-data">    
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="inputhidder">
    <td>Votre Nom</td>
    <td><input id="nom_mail_form_module_detailbonnplan" name="nom_mail_form_module_detailbonnplan" type="text" value="<?php if(isset($user_ion_auth) && $user_ion_auth->first_name!="") echo $user_ion_auth->first_name;?>"></td>
  </tr>
  <tr class="inputhidder">
    <td>Votre Téléphone</td>
    <td><input id="tel_mail_form_module_detailbonnplan" name="tel_mail_form_module_detailbonnplan" type="text" value="<?php if(isset($user_ion_auth) && $user_ion_auth->phone!="") echo $user_ion_auth->phone;?>"></td>
  </tr>
  <tr class="inputhidder">
    <td>Votre Email</td>
    <td><input id="email_mail_form_module_detailbonnplan" name="email_mail_form_module_detailbonnplan" type="text" value="<?php if(isset($user_ion_auth) && $user_ion_auth->email!="") echo $user_ion_auth->email;?>"></td>
  </tr>
  <tr>
    <td colspan="2">
    <textarea id="text_mail_form_module_detailbonnplan" cols="28" rows="7" name="text_mail_form_module_detailbonnplan" style="width:246px; height:130px;font-family:Arial, Helvetica, sans-serif; font-size:10px;"></textarea>
    </td>
  </tr>
  <tr>
    <td colspan="2">
    <input id="btn_reset_form_module_detailbonnplan" type="reset" value="Effacer" style="width:82px; height:22px;">
<input id="btn_submit_form_module_detailbonnplan" type="button" name="btn_submit_form_module_detailbonnplan" value="Envoyer" style="width:90px; height:22px;">
    </td>
  </tr>
</table>
</form>
    </td>
  </tr>
  <tr>
  <td>&nbsp;</td>
  <td><div id="divErrorform_module_detailbonnplan"><?php if (isset($mssg_envoi_module_detail_bonplan)) echo $mssg_envoi_module_detail_bonplan;?></div></td>
  </tr>
</table>
<br/>

