<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("front2013/includes/header_mini_2", $data); ?>
 <!--Master Page Head-->
<style type="text/css">
body{margin:0;padding:0;}
.Corps-P
{
    margin:0.0px 0.0px 12.0px 0.0px; text-align:justify; font-weight:400;
}
.Corps-P-P0
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:justify; font-weight:400;
}
.Corps-C
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C0
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C1
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C2
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:15.0px;
}
</style>


<div style="margin-top:30px; margin-bottom:20px; text-align:center;">
<p><img src="<?php echo GetImagePath("front/"); ?>/agenda/wpimages/wpa5e86f70_06.png" alt="" style="text-align:center"></p>
<p><input  type="button" value="Retour" onClick="javascript:history.back();"/></p>
</div>

<div id="txt_589" style="position: relative;width:620px;height: auto;overflow:hidden; padding-left:15px; padding-right:15px;">
<p class="Corps-P"><span class="Corps-C">Le Site mis en ligne est géré parla SARL PHASE, domicilée au 60 avenue de Nice à
    Cagnes sur Mer, ci-<wbr>après dénommé l&#39;Exploitant.</span></p>
<p class="Corps-P"><span class="Corps-C">Les conditions d&#39;utilisation s&#39;appliquent à tout Utilisateur du site et/ou Annonceur
    sur le site.</span></p>
<p class="Corps-P"><span class="Corps-C">En accédant à ce site et quel que soit votre qualité, vous vous engagez à prendre
    connaissance de ces conditions d&#39;utilisation et à les respecter.</span></p>
<p class="Corps-P"><span class="Corps-C-C0">Mentions légales</span></p>
<p class="Corps-P"><span class="Corps-C">Conformément à la loi, le site www.proximite-<wbr>agenda.com a fait l&#39;objet d&#39;une déclaration
    de traitement automatisé d&#39;informations nominatives à la Commission Nationale Informatique
    et Libertés.</span></p>
<p class="Corps-P"><span class="Corps-C">Le Site mis en ligne est géré par la SARL PHASE, domicilée au 60 avenue de Nice à
    Cagnes sur Mer et est hébergé par 1&amp;1.</span></p>
<p class="Corps-P"><span class="Corps-C-C1">Contenu du site</span></p>
<p class="Corps-P"><span class="Corps-C">Le Site a pour objet de proposer des idées de sorties dont les événements culturels,
    traditionnels, sportifs, musicaux...</span></p>
<p class="Corps-P"><span class="Corps-C">Ce site contient des informations, images et autres materiaux qui sont la propriété
    des Annonceurs sur le Site. Soumis à ces conditions, il est interdit à quiconque:</span></p>
<p class="Corps-P"><span class="Corps-C">-<wbr> de reproduire ce Site, ou toute partie de ce Site, sur un autre serveur ou dans
    un autre lieu;</span></p>
<p class="Corps-P"><span class="Corps-C">-<wbr> de redistribuer, prêter, échanger ou divulguer toute information obtenue sur ce
    Site à un tiers (qu&#39;il s&#39;agisse d&#39;un site internet ou autre);</span></p>
<p class="Corps-P"><span class="Corps-C">-<wbr> de vendre, modifier ou produire des travaux dérivés de l&#39;information obtenue sur
    ce Site.</span></p>
<p class="Corps-P"><span class="Corps-C">Le Site est accessible par le réseau Internet 24 heures sur 24 et 7 jours sur 7,
    sauf cas de force majeure, événement hors de contrôle de l&#39;Exploitant et/ou de l&#39;hébergeur
    du service, pannes éventuelles ou interventions de maintenance nécessaires pour assurer
    le bon fonctionnement du Site. </span></p>
<p class="Corps-P"><span class="Corps-C">L&#39;Exploitant s&#39;engage à mettre en oeuvre tous les moyens dont il dispose pour assurer
    une bonne qualité d&#39;accès au service et assurer la fiabilité et la rapidité de mise
    en ligne des données qu&#39;il diffuse.</span></p>
<p class="Corps-P"><span class="Corps-C-C1">Annonceur :</span></p>
<p class="Corps-P"><span class="Corps-C">En plaçant de l&#39;information sur ce site, l&#39;Annonceur donne à l&#39;Exploitant du Site
    le droit de reproduire, modifier, distribuer, afficher ou représenter publiquement,
    et de créer des travaux dérivés à partir de cette information pour utilisation sur
    ce Site, et de redistribuer cette information à un tiers dans le cadre du développement
    et de la maintenance du Site. L&#39;Annonceur peut modifier ou supprimer toute information
    placée sur le Site à tout moment.</span></p>
<p class="Corps-P"><span class="Corps-C-C1">Responsabilité de l&#39;annonceur</span></p>
<p class="Corps-P"><span class="Corps-C">L&#39;annonceur est informé des données qui figureront sur le Site. Il est, à ce titre,
    entièrement et exclusivement responsable pour toute l&#39;information placée par lui
    sur le Site et s&#39;engage à respecter le droit en vigueur.</span></p>
<p class="Corps-P"><span class="Corps-C-C1">Contenu des annonces</span></p>
<p class="Corps-P"><span class="Corps-C">L&#39;annonceur s&#39;engage:</span></p>
<p class="Corps-P"><span class="Corps-C">-<wbr> à ne pas placer sur le Site des annonces multiples au contenu identique.</span></p>
<p class="Corps-P"><span class="Corps-C">-<wbr> à ne pas diffuser des informations personnelles relatives à un individu sans son
    accord.</span></p>
<p class="Corps-P"><span class="Corps-C">-<wbr> à ne pas diffuser des informations mensongères, difamatoires, haineuses, abusives,
    pornographiques ou de toute autre nature considérée comme non diffusable par par
    le droit en vigueur ou par l&#39;exploitant du Site.</span></p>
<p class="Corps-P"><span class="Corps-C">-<wbr> à ne pas diffuser de l&#39;information appartenant à un autre site.</span></p>
<p class="Corps-P"><span class="Corps-C-C1">Droits d&#39;auteur</span></p>
<p class="Corps-P"><span class="Corps-C">Le titre, la conception, la forme et l&#39;ensemble des éléments tels que les textes,
    commentaires, illustrations et images originales et leur organisation diffusés sur
    le Site demeure la propriété de l&#39;Annonceur et sont protégés par le droit d&#39;auteur
    et pour le monde entier.L&#39;accès au site n&#39;entraîne aucun transfert de droit au bénéfice
    de l&#39;Utilisateur ou de toute autre personne.</span></p>
<p class="Corps-P"><span class="Corps-C">La durée d’archivage de chaque événement</span></p>
<p class="Corps-P"><span class="Corps-C">Chaque événement déposé sera automatiquement supprimer dans une délais de 15 jours
    après la date &nbsp;de fin de l’événement.</span></p>
<p class="Corps-P"><span class="Corps-C-C1">Les formules d’abonnements</span></p>
<p class="Corps-P"><span class="Corps-C">Deux formules d’abonnements sont proposées, l’une gratuite « l’abonnement basique
    », la seconde payante « l’abonnement Premium ». Ce dernier abonnement vous permettra
    d’accéder à des services complémentaires.</span></p>
<p class="Corps-P"><span class="Corps-C">Le détail de l’abonnement « Basic » : un abonnement gratuit</span></p>
<p class="Corps-P"><span class="Corps-C">Vous créez, modifiez, supprimez en ligne et en temps réel &nbsp;les informations sur chacun
    de vos événements (nom de la manifestation, date de début, date de fin, description,
    tarifs, coordonnées de l’organisateur, liens sociaux, localisation)</span></p>
<p class="Corps-P"><span class="Corps-C-C1">L’abonnement « Basique » : &nbsp;la demande d’inscription</span></p>
<p class="Corps-P"><span class="Corps-C">Le futur annonceur valide une demande d’inscription, il reçoit une confirmation de
    réception. Sous 48h00 après étude de cette demande et validation de notre service,
    nous vous confirmerons votre identifiant et votre mot de passe. Muni de ces derniers,
    vous pourrez commencer à déposer vos événements.</span></p>
<p class="Corps-P"><span class="Corps-C">www.proximite-<wbr>agenda.com peut refuser sans en donner la justification l’ouverture
    d’un compte qui ne correspondrait pas à son éthique. (voir contenu des annonces)</span></p>
<p class="Corps-P"><span class="Corps-C-C1">Le détail de l’abonnement « Premium »</span></p>
<p class="Corps-P"><span class="Corps-C">Vous créez, modifiez, supprimez en ligne et en temps réel &nbsp;les informations sur chacun
    de vos événements (nom de la manifestation, date de début, date de fin, description,
    tarifs, coordonnées de l’organisateur, liens sociaux, localisation) comme l’abonnement
    basique mais avec une accession à des modules complémentaires :</span></p>
<p class="Corps-P"><span class="Corps-C">-<wbr> Téléchargement d’une affiche, d’un document (prospectus, dépliant) (PDF)</span></p>
<p class="Corps-P"><span class="Corps-C">-<wbr> Intégration d’une galerie photos, d’une vidéo (youtube, dailymotion), d’un fichier
    audio.</span></p>
<p class="Corps-P"><span class="Corps-C">-<wbr> Extractions personnalisées et automatiques d’événements choisis (diffuseurs, catégories,
    &nbsp;villes) et intégration d’un simple clic &nbsp;sur votre site web. Vous décidez de la
    couleur des cadres et boutons..</span></p>
<p class="Corps-P"><span class="Corps-C">-<wbr> Présence du lien du site web de réservation en ligne que vous avez choisi (Fnac,
    carrefour, Ticketnet...)</span></p>
<p class="Corps-P"><span class="Corps-C">-<wbr> Si vous possédez un abonnement de diffuseur annuel sur notre site “dossiersdepresse-<wbr>paca.com”,
    vous ajoutez sur chaque événement &nbsp;un lien direct d’accès pour la consultation et
    le téléchargement de vos communiqués et dossiers de presse (site accessible uniquement
    aux journalistes abonnés).</span></p>
<p class="Corps-P"><span class="Corps-C">-<wbr> Présence d’un générateur de QRCODES. Avec ce générateur de QRCODES, vous pouvez
    &nbsp;diffuser une adresse URL, un email, un sms, un message, un numéro de téléphone.</span></p>
<p class="Corps-P"><span class="Corps-C-C1">L’abonnement « Premium » : la demande d’inscription</span></p>
<p class="Corps-P"><span class="Corps-C">Le futur annonceur valide une demande d’inscription, il reçoit une confirmation de
    réception. Sous 48h00 après étude de cette demande et validation de notre service,
    nous vous confirmerons votre identifiant et votre mot de passe. &nbsp;Cette confirmation
    sera accompagnée par un devis ou une facture proforma suivant votre demande.</span></p>
<p class="Corps-P"><span class="Corps-C">www.proximite-<wbr>agenda.com peut refuser sans en donner la justification l’ouverture
    d’un compte qui ne correspondrait pas à son éthique. (voir contenu des annonces)</span></p>
<p class="Corps-P"><span class="Corps-C-C1">L’abonnement « Premium » : l’ouverture du compte</span></p>
<p class="Corps-P"><span class="Corps-C">Lors de chaque commande, nous vous demanderons de bien vouloir nous préciser le type
    de document comptable (devis, facture proforma) adapté à votre organisation comptable.
    Après validation de votre part, vous recevrez le document demandé.</span></p>
<p class="Corps-P"><span class="Corps-C">Pour les entreprises, commerçants : après réception de votre chèque par courrier,
    vous serez avertis par courriel de l’ouverture de votre compte.</span></p>
<p class="Corps-P"><span class="Corps-C">Pour les collectivités : après réception ou confirmation de votre commande par courrier,
    télécopie ou courriel, &nbsp;vous serez avertis par courriel de l’ouverture de vos compte.</span></p>
<p class="Corps-P"><span class="Corps-C">Le diffuseur doit nous avertir par lettre recommandée de sa volonté d&#39;arrêter cet
    abonnement 2 mois avant la fin de la date d&#39;anniversaire, sinon l&#39;abonnement continuera
    avec un effet complémentaire de 12 mois.</span></p>
<p class="Corps-P"><span class="Corps-C-C1">L’utilisateur ou particulier</span></p>
<p class="Corps-P"><span class="Corps-C">L’Utilisateur déclare connaître et accepter les caractéristiques et les limites de
    la transmission d’informations par le réseau Internet, ainsi que les coûts propres
    à la connexion à ce réseau.</span></p>
<p class="Corps-P"><span class="Corps-C">Il lui appartient notamment de s’assurer que les caractéristiques techniques du matériel
    et des logiciels qu’il utilise lui permettent un accès au service dans de bonnes
    conditions, et de prendre toutes mesures appropriées pour être protégé d’une contamination
    par d’éventuels virus. L&#39;Utilisateur est seul responsable des questions qu’il formule
    et de l’emploi qu’il fait des résultats qu’il obtient..</span></p>
<p class="Corps-P"><span class="Corps-C">En tout état de cause, l&#39;Utilisateur reconnaît expressément que la responsabilité
    de l&#39;exploitant ne pourra en aucun cas être recherchée en cas d’un quelconque préjudice
    ou dommage direct ou indirect résultant d&#39;une inexactitude, erreur d’indexation,
    retard de mise en ligne, insuffisance d’exhaustivité, etc. des informations figurant
    dans les différentes bases disponibles sur le Site.</span></p>
<p class="Corps-P"><span class="Corps-C">L&#39;Utilisateur s&#39;interdit expressément de procéder à quelque modification, adaptation,
    arrangement ou transformation que ce soit des textes, commentaires, illustrations
    et images, en totalité ou en partie.</span></p>
<p class="Corps-P"><span class="Corps-C-C1">Données à caractère personnel</span></p>
<p class="Corps-P"><span class="Corps-C">Collecte de données à caractère personnel</span></p>
<p class="Corps-P"><span class="Corps-C">L&#39;Exploitant demande à chaque Annonceur de lui communiquer certaines informations
    qualifiées de données à caractère personnel : nom, prénom, adresse électronique,
    adresse postale, etc. afin d&#39;être en mesure de l&#39;identifier..</span></p>
<p class="Corps-P"><span class="Corps-C">Dans le formulaire de demande d&#39;inscription, l&#39;Annonceur est informé du caractère
    facultatif ou obligatoire de ses réponses : les informations obligatoires sont accompagnées
    d&#39;un signe distinctif tel un astérisque (*). A défaut de réponses obligatoires, l&#39;Annonceur
    ne pourra enregistrer son annonce.</span></p>
<p class="Corps-P"><span class="Corps-C">Garantie de confidentialité et de sécurité</span></p>
<p class="Corps-P"><span class="Corps-C">L&#39;Exploitant garantit à l&#39;Annonceur et à l’utilisateur que le traitement des données
    à caractère personnel est soumis à la loi n° 78-<wbr>17 du 6 janvier 1978 relative à l&#39;informatique,
    aux fichiers et aux libertés:</span></p>
<p class="Corps-P"><span class="Corps-C">-<wbr> les données sont enregistrées dans des zones sécurisées et inaccessibles au public.</span></p>
<p class="Corps-P"><span class="Corps-C">-<wbr> les données sont destinées exclusivement à l&#39;Exploitant. L&#39;Exploitant ne vend pas,
    ne loue pas et n&#39;échange pas avec des tiers les informations identifiant un Annonceur,
    sauf accord écrit de ce dernier.</span></p>
<p class="Corps-P"><span class="Corps-C">-<wbr> les données ne seront conservées qu&#39;à des fins statistiques ou de gestion des réclamations
    et pour une durée maximale de 1 an avant qu&#39;elles ne soient totalement détruites;</span></p>
<p class="Corps-P"><span class="Corps-C-C1">Droit d&#39;accès au fichier</span></p>
<p class="Corps-P"><span class="Corps-C">Conformément à la loi du 6 janvier 1978 précitée, toute personne inscrite sur le
    site a un droit d’accès, de rectification et de suppression de toutes données à caractère
    personnel.</span></p>
<p class="Corps-P"><span class="Corps-C">Le droit d&#39;accès et de rectification des données pourra etre exercé par la personne
    concernée:</span></p>
<p class="Corps-P"><span class="Corps-C">-<wbr> par mail : &nbsp;www.proximite-<wbr>agenda.com</span></p>
<p class="Corps-P"><span class="Corps-C">-<wbr> par courrier postal à l&#39;adresse suivante : Sarl PHASE, 60 avenue de Nice 06800
    Cagnes sur Mer. </span></p>
<p class="Corps-P"><span class="Corps-C">-<wbr> par téléphone: 06.72.05.59.35</span></p>
<p class="Corps-P"><span class="Corps-C-C0">Responsabilité de l’éditeur du site</span></p>
<p class="Corps-P"><span class="Corps-C">L’Exploitant se dégage de toute responsabilité relative au contenu des annonces:
    véracité, qualité ou légalité de l&#39;information, produits ou services placés sur le
    Site.</span></p>
<p class="Corps-P"><span class="Corps-C">La responsabilité de l&#39;exploitant du site ne pourra être engagée relativement à la
    précision, le contenu, l&#39;exhaustivité, la légitimité, la fiabilité, l&#39;opérabilité
    ou la disponibilité des informations ou des données affichées dans les résultats
    obtenus par menu Recherche. L&#39;exploitant se réserve la droit de refuser, de déplacer
    ou de retirer du Site tout contenu jugé non conforme à l&#39;objet du site.</span></p>
<p class="Corps-P"><span class="Corps-C">Le Site peut contenir des liens vers d&#39;autres sites ou informations étrangères au
    site. L&#39;Exploitant du Site ne vérifie ou n&#39;exerce aucun contrôle sur de tels sites
    ou informations étrangères au Site, ne les cautionne et ne peut en aucun cas être
    rendu responsable de toute information, produits ou services contenus dans ces sites
    étrangers.</span></p>
<p class="Corps-P"><span class="Corps-C">L&#39;exploitant ne peut être tenu responsable des dommages incluant toute perte de données,
    de revenu, de chance ou de profit, résultant d&#39;un dysfonctionnement du site.</span></p>
<p class="Corps-P"><span class="Corps-C">Clauses relatives aux liens hypertextes</span></p>
<p class="Corps-P"><span class="Corps-C-C1">-<wbr> Liens hypertextes internes :</span></p>
<p class="Corps-P"><span class="Corps-C">L&#39;Exploitant autorise tout site Internet ou tout autre support à citer le Site ou
    à mettre en place un lien hypertexte pointant vers l&#39;adresse www.agenda-<wbr>cotedazur.fr.
    L&#39;autorisation de mise en place d&#39;un lien est valable pour tout support, à l&#39;exception
    de ceux diffusant des informations à caractère politique, religieux, pornographique,
    xénophobe ou contaire au droit en vigueur.</span></p>
<p class="Corps-P"><span class="Corps-C">Au titre de cette autorisation, l&#39;Exploitant se réserve un droit d&#39;opposition et
    décline toute responsabilité concernant le contenu des sites mais également des risques
    techniques.</span></p>
<p class="Corps-P"><span class="Corps-C-C1">-<wbr> Liens hypertextes externes :</span></p>
<p class="Corps-P"><span class="Corps-C">Les liens mis en place depuis le Site vers des sites extérieurs ne sauraient engager
    la responsabilité de l&#39;Exploitant notamment au regard du contenu de ces sites mais
    également des risques techniques.</span></p>
<p class="Corps-P"><span class="Corps-C">En acceptant d&#39;utiliser un lien hypertexte pour accéder à un site extérieur, l&#39;Utilisateur
    accepte de prendre les risques et de subir un éventuel préjudice direct ou indirect.</span></p>
<p class="Corps-P-P0"><span class="Corps-C">Phase SARL </span></p>
<p class="Corps-P-P0"><span class="Corps-C">60 Avenue de Nice -<wbr> 06800 Cagnes sur Mer </span></p>
<p class="Corps-P-P0"><span class="Corps-C">Téléphone : 00.33.(0)4.93.73.84.51</span></p>
<p class="Corps-P-P0"><span class="Corps-C">Mobile: 00.33.(0)6.72.05.59.35 </span></p>
<p class="Corps-P-P0"><span class="Corps-C-C2">contact@agenda-<wbr>cotedazur.fr</span></p>
<p class="Corps-P-P0"><span class="Corps-C"> SARL au capital de 7265 € -<wbr> RCS Antibes 412.071.466</span></p>
<p class="Corps-P-P0"><span class="Corps-C">SIRET : 41207146600012 -<wbr> CODE NAF :7022Z</span></p>
<p class="Corps-P-P0"><span class="Corps-C">IBAN: FR76 3007 6023 5110 6081 0020 072</span></p>
<p class="Corps-P-P0"><span class="Corps-C">Crédit du Nord -<wbr> Cagnes sur Mer -<wbr> Code Banque: 30076</span></p>
<p class="Corps-P-P0"><span class="Corps-C">Code guichet :02351 Compte : 106 081 002 00 Clé :72</span></p>
<p class="Corps-P"><span class="Corps-C"><br></span></p>
<p class="Corps-P-P0"><span class="Corps-C">Pour tout litige, merci d&#39;écrire à &nbsp;contact@agenda-<wbr>cotedazur.fr</span></p>
<p class="Corps-P-P0"><span class="Corps-C">Nous nous engageons à vous répondre dans les 48 heures .</span></p>
<p class="Corps-P-P0"><span class="Corps-C">Un problème ? une question ?</span></p>
<p class="Corps-P-P0"><span class="Corps-C">N&#39; hésitez pas à nous contacter par e-<wbr>mail :</span></p>
<p class="Corps-P-P0"><span class="Corps-C">contact@proximite-<wbr>agenda.com</span></p>
</div>


<?php $this->load->view("front2013/includes/footer_mini_2"); ?>