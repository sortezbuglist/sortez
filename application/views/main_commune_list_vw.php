<div>

    <form method="post" action="<?php echo site_url('front/annuaire/main_commune_list');?>" id="myform" name="myform">
<span class="custom-dropdown custom-dropdown--white bord">
        <select  id="mySelect" onchange="javascript:myform.submit();" name="id_ville_selected" class="custom-dropdown__select custom-dropdown__select--white select_list_departement_select pl-5 text-center">

            <option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Toutes les Commune</option>

            <?php if (isset($toDepartement) && count($toDepartement)>0) { ?>

                <?php foreach ($toDepartement as $oDepartement) { ?>

                    <option value="<?php echo $oDepartement->IdVille;?>"><?php echo $oDepartement->ville_nom;?>&nbsp;(<?php echo $oDepartement->nbCommercant ?>)</option>

                <?php } ?>

            <?php } ?>

        </select>
</span>
    </form>

</div>
<style type="text/css">
    .custom-dropdown--large {
        font-size: 1.5em;
    }

    .custom-dropdown--small {
        font-size: .7em;
    }

    .custom-dropdown__select{
        background-color: white;
        font-size: inherit; /* inherit size from .custom-dropdown */
        padding: .5em; /* add some space*/
        margin: 0; /* remove default margins */
        color: black!important;
    }
    .custom-dropdown__select {

        border-bottom-left-radius: 10px!important;
        border-color:#DC1A95;
        border-bottom-right-radius: 10px!important;
        border-top-left-radius: 10px!important;
        border-top-right-radius: 10px!important;
        height: 45px!important;
        width: 350px;
        border-width: medium;
    }


    .custom-dropdown__select--white {
        color: white;
        border-bottom-left-radius: 50%;
        border-bottom-right-radius: 50%;
        border-top-left-radius: 50%;
        border-top-right-radius: 50%;

    }


</style>


