<?php $data["zTitle"] = 'Creation bonplan'; ?>
<?php $this->load->view("sortez/includes/header_backoffice_com", $data); ?>
<script>
    $(function () {
        $("#IdDateDebut").datepicker({
            dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            monthNames: ['Janvier', 'Févier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            dateFormat: 'DD, d MM yy',
            autoSize: true,
            changeMonth: true,
            changeYear: true,
            yearRange: '1900:2040'
        });
        $("#IdDateFin").datepicker({
            dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            monthNames: ['Janvier', 'Févier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            dateFormat: 'DD, d MM yy',
            autoSize: true,
            changeMonth: true,
            changeYear: true,
            yearRange: '1900:2040'
        });
        $("#bp_simple_date_debut").datepicker({
            dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            monthNames: ['Janvier', 'Févier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            dateFormat: 'DD, d MM yy',
            autoSize: true,
            changeMonth: true,
            changeYear: true,
            yearRange: '1900:2040'
        });
        $("#bp_simple_date_fin").datepicker({
            dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            monthNames: ['Janvier', 'Févier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            dateFormat: 'DD, d MM yy',
            autoSize: true,
            changeMonth: true,
            changeYear: true,
            yearRange: '1900:2040'
        });
        $("#bp_unique_date_debut").datepicker({
            dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            monthNames: ['Janvier', 'Févier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            dateFormat: 'DD, d MM yy',
            autoSize: true,
            changeMonth: true,
            changeYear: true,
            yearRange: '1900:2040'
        });
        $("#bp_unique_date_fin").datepicker({
            dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            monthNames: ['Janvier', 'Févier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            dateFormat: 'DD, d MM yy',
            autoSize: true,
            changeMonth: true,
            changeYear: true,
            yearRange: '1900:2040'
        });
        $("#bp_multiple_date_debut").datepicker({
            dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            monthNames: ['Janvier', 'Févier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            dateFormat: 'DD, d MM yy',
            autoSize: true,
            changeMonth: true,
            changeYear: true,
            yearRange: '1900:2040'
        });
        $("#bp_multiple_date_fin").datepicker({
            dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            monthNames: ['Janvier', 'Févier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            dateFormat: 'DD, d MM yy',
            autoSize: true,
            changeMonth: true,
            changeYear: true,
            yearRange: '1900:2040'
        });


    });

    function deleteFile(_IdBonplan, _FileName) {
        //alert(gCONFIG["SITE_URL"] + 'front/bonplan/delete_files/' + _IdBonplan + '/' + _FileName);
        jQuery.ajax({
            url: gCONFIG["SITE_URL"] + 'front/bonplan/delete_files/' + _IdBonplan + '/' + _FileName,
            dataType: 'html',
            type: 'POST',
            async: true,
            success: function (data) {
                window.location.reload();
            }
        });
    }

    function setbon_plan_utilise_plusieurs() {
        if (document.getElementById("chk_bon").checked) {
            document.getElementById("bon_plan_utilise_plusieurs").value = 1;
        } else {
            document.getElementById("bon_plan_utilise_plusieurs").value = 0;
        }
    }

    function checkbox_activ_status(check_activ, chek_status) {
        if (document.getElementById(check_activ).checked) {
            document.getElementById(chek_status).value = 1;
        } else {
            document.getElementById(chek_status).value = 0;
        }
    }
</script>

<script type="text/javascript">
    function limite(textarea, max) {
        if (textarea.value.length >= max) {
            textarea.value = textarea.value.substring(0, max);
        }
        var reste = max - textarea.value.length;
        var affichage_reste = reste + ' caract&egrave;res restants';
        document.getElementById('max_desc').innerHTML = affichage_reste;
    }

    function limite_title(textarea, max) {
        if (textarea.value.length >= max) {
            textarea.value = textarea.value.substring(0, max);
        }
        var reste = max - textarea.value.length;
        var affichage_reste = reste + ' caract&egrave;res restants';
        document.getElementById('max_desc_title').innerHTML = affichage_reste;
    }


    function testFormBonplan() {
        var zCategorie = jQuery("#idCategorie").val();
        var zVille = jQuery("#idVille").val();
        var zTitre = jQuery("#idTitre").val();
        var zDescription = jQuery("#idDescription").val();
        var zPhoto1 = jQuery("#IdPhoto1").val();
        var zPhoto2 = jQuery("#IdPhoto2").val();
        var zPhoto3 = jQuery("#IdPhoto3").val();
        var zPhoto4 = jQuery("#IdPhoto4").val();
        var bonplan_type = jQuery("input[name='bonplan[bonplan_type]']:checked").val();

        var zErreur = "";
        if (zTitre == "") zErreur += "Veuillez entrer un titre\n";

        if (bonplan_type == "1") {
            var zDateDebut = jQuery("#IdDateDebut").val();
            if (zDateDebut == "") zErreur += "Veuillez indiquer la date du debut\n";
            var zDateFin = jQuery("#IdDateFin").val();
            if (zDateFin == "") zErreur += "Veuillez indiquer la date fin\n";
        } else if (bonplan_type == "2") {
            var bp_unique_date_debut = jQuery("#bp_unique_date_debut").val();
            if (bp_unique_date_debut == "") zErreur += "Veuillez indiquer la date du debut\n";
            var bp_unique_date_fin = jQuery("#bp_unique_date_fin").val();
            if (bp_unique_date_fin == "") zErreur += "Veuillez indiquer la date fin\n";
        } else if (bonplan_type == "3") {
            var bp_multiple_date_debut = jQuery("#bp_multiple_date_debut").val();
            if (bp_multiple_date_debut == "") zErreur += "Veuillez indiquer la date du debut\n";
            var bp_multiple_date_fin = jQuery("#bp_multiple_date_fin").val();
            if (bp_multiple_date_fin == "") zErreur += "Veuillez indiquer la date fin\n";
        }

        if (zErreur != "") alert(zErreur);
        else document.frmCreationBonplan.submit();

        return false;
    }

    //add script facebook by iotadev
     function ouvrir(){
        //alert("test")
        var a= window.open("http://www.facebook.com/sharer.php?u=<?php echo site_url($oInfoCommercant->nom_url.'/notre_bonplan/' . $oBonplan->bonplan_id); ?>", "facebookPrivicarteForm_share", "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=500,height=800");
                            // setTimeout
                            // (
                            //     function()
                            //     {
                            //         a.window.close();
                            //        window.open("http://www.facebook.com/sharer.php?u=<?php //echo site_url('agenda/details/' . $$oBonplan->bonplan_id); ?>", "facebookPrivicarteForm_share", "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=500,height=800");
                            //     },
                            //     2500
                            // );
    }

</script>
<style type="text/css">
    .input_width {
        width: 400px;
    }

    .adminbp_title_content {
        text-align: center;
        width: 100%;
        display: table;
        margin-bottom: 30px;
    }

    .adminbp_title_img {
        width: 100%;
        margin-top: 15px;
    }

    .admin_bp_green_btn_regler, .admin_bp_green_btn_regler:hover, .admin_bp_green_btn_regler:active, .admin_bp_green_btn_regler:focus {
        font-weight: bold;
        color: #FFFFFF;
        background-color: #008000;
        padding: 10px 30px;
        text-align: center;
        display: table;
        text-decoration: none;
        border-radius: 15px;
        margin: 30px auto;
    }

    .admin_bp_green_btn, .admin_bp_green_btn:hover, .admin_bp_green_btn:active, .admin_bp_green_btn:focus {
        font-weight: bold;
        color: #FFFFFF;
        background-color: #008000;
        width: 100%;
        padding: 10px;
        text-align: center;
        display: table;
        text-decoration: none;
    }

    .admin_bp_red_btn, .admin_bp_red_btn:hover, .admin_bp_red_btn:active, .admin_bp_red_btn:focus {
        font-weight: bold;
        color: #FFFFFF;
        background-color: #CF1590;
        width: 100%;
        padding: 10px;
        text-align: center;
        display: table;
        text-decoration: none;
    }

    .btn_adminbp, .btn_adminbp:hover, .btn_adminbp:focus {
        background-color: #CF1590;
        padding: 10px 29px;
        color: #FFFFFF;
        text-decoration: none;
        font-weight: bold;
    }

    .dtable {
        display: table;
    }

    .adminbp_title {
        padding: 10px 15px;
        color: #FFFFFF;
        background-color: #CF1590;
        text-align: left;
        font-weight: bold;
        height: 40px;
    }

    .greybg {
        background-color: #E5E5E5;
    }

    .bpcolor {
        color: #CF1590;
    }

    #max_desc_title {
        color: #CF1590 !important;
    }

    .admin_bo_menu {
        display: inline-table;
        height: 50px;
        width: 100%;
        text-align: center;
    }
</style>


<link rel="stylesheet" href="<?php echo GetCssPath("privicarte/"); ?>/global.css">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
      integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"
      integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<div id="divFicheBonplan" class="content">
    <form name="frmCreationBonplan" id="frmCreationBonplan" action="<?php if (isset($oBonplan)) {
        echo site_url("front/bonplan/modifBonplan/$idCommercant");
    } else {
        echo site_url("front/bonplan/creerBonplan/$idCommercant");
    } ?>" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="bonplan[bonplan_commercant_id]" id="IdCommercant"
               value="<?php echo $idCommercant; ?>"/>
        <input type="hidden" name="bonplan[bonplan_id]" id="IdBonplan" value="<?php if (isset($oBonplan)) {
            echo $oBonplan->bonplan_id;
        } ?>"/>


        <div class="adminbp_title_content dtable">
            <div class="adminbp_title_img dtable">
                <!--<p><img src="<?php echo GetImagePath("privicarte/"); ?>/adminbp_title.png"/></p>-->
                <h1>Mon Bon plan</h1>
            </div>

        </div>

        <?php
        $thisss =& get_instance();
        $from_privicarte = $thisss->input->get('from_privicarte');
        if (isset($from_privicarte) && $from_privicarte=='1') {
        ?>
            <div class="col-lg-12 padding0">
                <div class="admin_bo_menu"><a href="<?php echo site_url('front/fidelity/mes_parametres/'); ?>"
                                              class="btn_adminbp">Retour Menu</a></div>
            </div>
            <?php
        } else {
        ?>
        <div class="col-lg-12 padding0">
            <div class="admin_bo_menu"><a href="<?php echo site_url('front/utilisateur/contenupro/'); ?>"
                                          class="btn_adminbp">Retour Menu</a></div>
            <div class="admin_bo_menu" style="min-height: 50px;"><a
                    href="<?php echo site_url('front/bonplan/listeMesBonplans/' . $idCommercant); ?>"
                    class="btn_adminbp">Retour aux Bonplans</a></div>
            <div class="admin_bo_menu" style="min-height: 50px;"><a
                    href="<?php echo site_url('front/bonplan/ficheBonplan/' . $idCommercant . '/0'); ?>"
                    class="btn_adminbp">Ajouter un nouveau Bonplan</a></div>
        </div>
        <?php } ?>


        <?php if (isset($mssg) && $mssg == 1) { ?>
            <div class="col-lg-12 alert alert-success textaligncenter">Les modifications ont &eacute;t&eacute; enregistr&eacute;es</div>
            <br/>
        <?php } ?>


        <?php if (isset($mssg) && $mssg != 1 && $mssg != "") { ?>
            <div class="col-lg-12 alert alert-danger textaligncenter">Une erreur est constatée, Veuillez vérifier la
                conformité de vos données
            </div><br/>
        <?php } ?>


        <span><?php if (isset($msg)) {
                echo $msg;
            } ?></span>

        <div class="col-xs-12 adminbp_title">Les détails de votre Bon Plan</div>

        <div class="col-xs-12 padding0 greybg">

            <div class="col-xs-12 margintop15">
                <div class="col-xs-4 padding0"><label>Titre : </label></div>
                <div class="col-xs-8 padding0">
                    <textarea rows="2" cols="20" name="bonplan[bonplan_titre]" onkeyup="limite_title(this,'100');"
                              onkeydown="limite_title(this,'100');" id="idTitre"
                              class="input_width form-control"><?php if (isset($oBonplan)) {
                            echo $oBonplan->bonplan_titre;
                        } ?></textarea><br/><span id="max_desc_title"></span>
                </div>
            </div>
            <div class="col-xs-12">
                <label>Description : </label>
                <textarea rows="2" cols="40" name="bonplan[bonplan_texte]" id="idDescription"
                          onkeyup="limite(this,'250');" onkeydown="limite(this,'250');" style="height:100px;"
                          class="input_width form-control"><?php if (isset($oBonplan)) {
                        echo $oBonplan->bonplan_texte;
                    } ?></textarea>
                <?php echo display_ckeditor($ckeditor0); ?>
                <br/><span id="max_desc"></span>
            </div>


        </div>


        <div class="col-xs-12 adminbp_title">
            Intégration d'Images
            <small>Dimensions 4/3</small>
        </div>


        <div class="col-xs-12 greybg paddingbottom15">

            <?php if (isset($oBonplan->bonplan_id) && $oBonplan->bonplan_id != '0') { ?>

                <div class="col-xs-12 margintop15">
                    <div class="col-xs-4 padding0"><label>Photo 01 : </label></div>
                    <div class="col-xs-8 padding0">


                        <input type="hidden" name="photo1Associe" id="bonplan_photo1Associe"
                               value="<?php if (isset($oBonplan)) {
                                   echo $oBonplan->bonplan_photo1;
                               } ?>"/>
                        <div id="ArticlePhoto1_container">
                            <?php if (!empty($oBonplan->bonplan_photo1)) { ?>
                                <?php
                                $path_img_gallery = 'application/resources/front/photoCommercant/imagesbank/' . $oInfoCommercant->user_ionauth_id . '/';
                                $path_img_gallery_old = 'application/resources/front/images/';
                                if (file_exists($path_img_gallery . $oBonplan->bonplan_photo1) == true) {
                                    echo '<img  src="' . base_url() . $path_img_gallery . $oBonplan->bonplan_photo1 . '" width="200"/>';
                                } else {
                                    echo '<img  src="' . base_url() . $path_img_gallery_old . $oBonplan->bonplan_photo1 . '" width="200"/>';
                                }
                                ?>
                                <a href="javascript:void(0);" class="btn btn-danger"
                                   onclick="deleteFile('<?php echo $oBonplan->bonplan_id; ?>','bonplan_photo1');">Supprimer</a>
                            <?php } else { ?>
                                <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $oBonplan->bonplan_id . "-bp-bonplan_photo1"); ?>", "Bonplan image", "width=1045, height=675, scrollbars=yes");'
                                   href='javascript:void(0);' title="Photo 1" class="btn btn-default"
                                   id="ArticlePhoto1_link">Ajouter la photo 1</a>
                            <?php } ?>
                        </div>
                        <div id="div_error_taille_Photo1"
                             class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>


                    </div>
                </div>


                <div class="col-xs-12">
                    <div class="col-xs-4 padding0"><label>Photo 02 :</label></div>
                    <div class="col-xs-8 padding0">


                        <input type="hidden" name="photo2Associe" id="bonplan_photo2Associe"
                               value="<?php if (isset($oBonplan)) {
                                   echo $oBonplan->bonplan_photo2;
                               } ?>"/>
                        <div id="ArticlePhoto2_container">
                            <?php if (!empty($oBonplan->bonplan_photo2)) { ?>
                                <?php
                                $path_img_gallery = 'application/resources/front/photoCommercant/imagesbank/' . $oInfoCommercant->user_ionauth_id . '/';
                                $path_img_gallery_old = 'application/resources/front/images/';
                                if (file_exists($path_img_gallery . $oBonplan->bonplan_photo2) == true) {
                                    echo '<img  src="' . base_url() . $path_img_gallery . $oBonplan->bonplan_photo2 . '" width="200"/>';
                                } else {
                                    echo '<img  src="' . base_url() . $path_img_gallery_old . $oBonplan->bonplan_photo2 . '" width="200"/>';
                                }
                                ?>
                                <a href="javascript:void(0);" class="btn btn-danger"
                                   onclick="deleteFile('<?php echo $oBonplan->bonplan_id; ?>','bonplan_photo2');">Supprimer</a>
                            <?php } else { ?>
                                <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $oBonplan->bonplan_id . "-bp-bonplan_photo2"); ?>", "Bonplan image", "width=1045, height=675, scrollbars=yes");'
                                   href='javascript:void(0);' title="Photo 1" class="btn btn-default"
                                   id="Articlephoto2_link">Ajouter la photo 2</a>
                            <?php } ?>
                        </div>
                        <div id="div_error_taille_photo2"
                             class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("photo2", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>


                    </div>
                </div>


                <div class="col-xs-12">
                    <div class="col-xs-4 padding0"><label>Photo 03 :</label></div>
                    <div class="col-xs-8 padding0">

                        <input type="hidden" name="photo3Associe" id="bonplan_photo3Associe"
                               value="<?php if (isset($oBonplan)) {
                                   echo $oBonplan->bonplan_photo3;
                               } ?>"/>
                        <div id="ArticlePhoto3_container">
                            <?php if (!empty($oBonplan->bonplan_photo3)) { ?>
                                <?php
                                $path_img_gallery = 'application/resources/front/photoCommercant/imagesbank/' . $oInfoCommercant->user_ionauth_id . '/';
                                $path_img_gallery_old = 'application/resources/front/images/';
                                if (file_exists($path_img_gallery . $oBonplan->bonplan_photo3) == true) {
                                    echo '<img  src="' . base_url() . $path_img_gallery . $oBonplan->bonplan_photo3 . '" width="200"/>';
                                } else {
                                    echo '<img  src="' . base_url() . $path_img_gallery_old . $oBonplan->bonplan_photo3 . '" width="200"/>';
                                }
                                ?>
                                <a href="javascript:void(0);" class="btn btn-danger"
                                   onclick="deleteFile('<?php echo $oBonplan->bonplan_id; ?>','bonplan_photo3');">Supprimer</a>
                            <?php } else { ?>
                                <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $oBonplan->bonplan_id . "-bp-bonplan_photo3"); ?>", "Bonplan image", "width=1045, height=675, scrollbars=yes");'
                                   href='javascript:void(0);' title="Photo 1" class="btn btn-default"
                                   id="Articlephoto3_link">Ajouter la photo 3</a>
                            <?php } ?>
                        </div>
                        <div id="div_error_taille_photo3"
                             class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("photo3", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>


                    </div>
                </div>


                <div class="col-xs-12">
                    <div class="col-xs-4 padding0"><label>Photo 04 :</label></div>
                    <div class="col-xs-8 padding0">


                        <input type="hidden" name="photo4Associe" id="bonplan_photo4Associe"
                               value="<?php if (isset($oBonplan)) {
                                   echo $oBonplan->bonplan_photo4;
                               } ?>"/>
                        <div id="ArticlePhoto4_container">
                            <?php if (!empty($oBonplan->bonplan_photo4)) { ?>
                                <?php
                                $path_img_gallery = 'application/resources/front/photoCommercant/imagesbank/' . $oInfoCommercant->user_ionauth_id . '/';
                                $path_img_gallery_old = 'application/resources/front/images/';
                                if (file_exists($path_img_gallery . $oBonplan->bonplan_photo4) == true) {
                                    echo '<img  src="' . base_url() . $path_img_gallery . $oBonplan->bonplan_photo4 . '" width="200"/>';
                                } else {
                                    echo '<img  src="' . base_url() . $path_img_gallery_old . $oBonplan->bonplan_photo4 . '" width="200"/>';
                                }
                                ?>
                                <a href="javascript:void(0);" class="btn btn-danger"
                                   onclick="deleteFile('<?php echo $oBonplan->bonplan_id; ?>','bonplan_photo4');">Supprimer</a>
                            <?php } else { ?>
                                <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $oBonplan->bonplan_id . "-bp-bonplan_photo4"); ?>", "Bonplan image", "width=1045, height=675, scrollbars=yes");'
                                   href='javascript:void(0);' title="Photo 1" class="btn btn-default"
                                   id="Articlephoto4_link">Ajouter la photo 4</a>
                            <?php } ?>
                        </div>
                        <div id="div_error_taille_photo4"
                             class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("photo4", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>


                    </div>
                </div>

            <?php } else { ?>
                <div class="col-lg-12">
                    <div class="alert alert-danger">Veuillez enregistrer votre article avant d'ajouter les images !
                    </div>
                </div>
            <?php } ?>

        </div>


        <div class="col-xs-12 adminbp_title">
            <div class="col-xs-9 padding0">Bon plan personnel
                <small>(sans validation de la carte Vivresaville)</small>
            </div>
            <div class="col-xs-3 padding0 textalignright"><input name="bonplan[bonplan_type]" type="radio"
                                                                 value="1" <?php if (isset($oBonplan->bonplan_type) && $oBonplan->bonplan_type == "1") {
                    echo 'checked="checked"';
                } else if ($oBonplan->bonplan_type==null) echo 'checked="checked"';?>/><label class="marginleft15">Activer</label></div>
        </div>

        <div class="col-xs-12 greybg paddingbottom15">


            <div class="col-xs-12 padding0 margintop15">
                <div class="col-xs-9 padding0">
                    <p><span class="bpcolor">BON PLAN PERSONNEL</span> : Ce bon plan n’est pas attaché au concept
                        Sortez, il suffit au consommateur d’imprimer le coupon et de le présenter au professionnel lors
                        de son déplacement.</p>
                    <p>Sur la page du professionnel il n’est pas fait mention des conditions de Sortez.</p>
                </div>
                <div class="col-xs-3 padding0 textalignright"><img
                        src="<?php echo GetImagePath("privicarte/"); ?>/bp_simple.png"/></div>
            </div>

            <div class="col-xs-12 padding0 marginbottom5 margintop15">
                <div class="col-xs-4 padding0"><label>Date debut (Obligatoire) : </label></div>
                <div class="col-xs-8 padding0">
                    <input type="text" name="bonplan[bonplan_date_debut]" id="IdDateDebut"
                           value="<?php if (isset($oBonplan->bonplan_date_debut) && $oBonplan->bonplan_date_debut != "0000-00-00") {
                               echo convert_Sqldate_to_Frenchdate($oBonplan->bonplan_date_debut);
                           } ?>" class="input_width form-control"/>
                </div>
            </div>

            <div class="col-xs-12 padding0 marginbottom5">
                <div class="col-xs-4 padding0"><label>Date fin (Obligatoire) : </label></div>
                <div class="col-xs-8 padding0">
                    <input class="input_width form-control" type="text" name="bonplan[bonplan_date_fin]" id="IdDateFin"
                           value="<?php if (isset($oBonplan->bonplan_date_fin) && $oBonplan->bonplan_date_fin != "0000-00-00") {
                               echo convert_Sqldate_to_Frenchdate($oBonplan->bonplan_date_fin);
                           } ?>"/>
                </div>
            </div>

            <div class="col-xs-12 padding0 margintop15 marginbottom5">
                <div class="col-xs-4 padding0"><label>Prix bon plan : </label></div>
                <div class="col-xs-8 padding0"><input type="text" name="bonplan[bp_simple_prix]" id="bp_simple_prix"
                                                      class="input_width form-control"
                                                      value="<?php if (isset($oBonplan->bp_simple_prix)) {
                                                          echo $oBonplan->bp_simple_prix;
                                                      } ?>"/></div>
            </div>
            <div class="col-xs-12 padding0 marginbottom5">
                <div class="col-xs-4 padding0"><label>Valeur : </label></div>
                <div class="col-xs-8 padding0"><input type="text" name="bonplan[bp_simple_value]" id="bp_simple_value"
                                                      class="input_width form-control"
                                                      value="<?php if (isset($oBonplan->bp_simple_value)) {
                                                          echo $oBonplan->bp_simple_value;
                                                      } ?>"></div>
            </div>
            <div class="col-xs-12 padding0 marginbottom5">
                <div class="col-xs-4 padding0"><label>Economie : </label></div>
                <div class="col-xs-8 padding0"><input type="text" name="bonplan[bp_simple_eco]" id="bp_simple_eco"
                                                      class="input_width form-control"
                                                      value="<?php if (isset($oBonplan->bp_simple_eco)) {
                                                          echo $oBonplan->bp_simple_eco;
                                                      } ?>"></div>
            </div>


        </div>


        <div class="col-xs-12 adminbp_title">
            <div class="col-xs-9 padding0">Bon plan unique avec Sortez
                <small>(gestion automatis&eacute;e des transactions)</small>
            </div>
            <div class="col-xs-3 padding0 textalignright"><input name="bonplan[bonplan_type]" type="radio"
                                                                 value="2" <?php if (isset($oBonplan->bonplan_type) && $oBonplan->bonplan_type == "2") {
                    echo 'checked="checked"';
                } ?>/><label class="marginleft15">Activer</label></div>
        </div>

        <div class="col-xs-12 greybg paddingbottom15">


            <div class="col-xs-12 padding0 margintop15">
                <div class="col-xs-9 padding0">
                    <p class="bpcolor">BON PLAN OFFRE UNIQUE AVEC OU SANS QUANTITE DE D&Eacute;PART :<br/>
                        Le consommateur ne peut valider qu'une seule fois le m&ecirc;me bon plan</p>
                    <p>Ce bon plan est attach&eacute; au concept Sortez, il suffit au consommateur de r&eacute;server en
                        ligne, notre syst&egrave;me le valide automatiquement ou le refuse si celui-ci a
                        d&eacute;jà &eacute;t&eacute; utilis&eacute;.
                        <br/>
                        Le professionnel et le consommateur recoivent un e-mail de confirmation.
                        <br/>
                        La quantit&eacute; restante est d&eacute;cr&eacute;ment&eacute;e automatiquement.
                        <br/>
                        Cette r&eacute;servation est alors int&eacute;gr&eacute; sur les pages des r&eacute;servations
                        en cours.
                        <br/>
                        Lors de son d&eacute;placement le consommateur pr&eacute;sente sa carte sur son mobile.
                        <br/>
                        Avec l'application Sortez, le professionnel identifie le consommateur, valide d&eacute;finitivement
                        ce bon plan et le supprime sur la page des r&eacute;servations en cours.
                        <br/>
                        La transaction est enregistr&eacute;e sur l'historique du consommateur (compte consommateur et
                        compte professionnel).</p>
                </div>
                <div class="col-xs-3 padding0 textalignright"><img
                        src="<?php echo GetImagePath("privicarte/"); ?>/bp_unique_bn.png"
                        class="marginbottom15 margintop15"/><img
                        src="<?php echo GetImagePath("privicarte/"); ?>/bp_unique_img.png" class="margintop15"/></div>
            </div>

            <div class="col-xs-12 padding0 margintop15 marginbottom5">
                <div class="col-xs-4 padding0"><label>Date début (obligatoire) : </label></div>
                <div class="col-xs-8 padding0"><input type="text" name="bonplan[bp_unique_date_debut]"
                                                      id="bp_unique_date_debut" class="input_width form-control"
                                                      value="<?php if (isset($oBonplan->bp_unique_date_debut) && $oBonplan->bp_unique_date_debut != "0000-00-00") {
                                                          echo convert_Sqldate_to_Frenchdate($oBonplan->bp_unique_date_debut);
                                                      } ?>"></div>
            </div>
            <div class="col-xs-12 padding0 marginbottom5">
                <div class="col-xs-4 padding0"><label>Date de fin (obligatoire) : </label></div>
                <div class="col-xs-8 padding0"><input type="text" name="bonplan[bp_unique_date_fin]"
                                                      id="bp_unique_date_fin" class="input_width form-control"
                                                      value="<?php if (isset($oBonplan->bp_unique_date_fin) && $oBonplan->bp_unique_date_fin != "0000-00-00") {
                                                          echo convert_Sqldate_to_Frenchdate($oBonplan->bp_unique_date_fin);
                                                      } ?>"></div>
            </div>

            <div class="col-xs-12 padding0 margintop15 marginbottom5">
                <div class="col-xs-4 padding0"><label>Prix bon plan
                        <small>(Cochez pour activer)</small>
                        : </label></div>
                <div class="col-xs-2"><input name="checkbox_activ_bp_unique_prix" id="checkbox_activ_bp_unique_prix"
                                             type="checkbox" value=""
                                             onchange="checkbox_activ_status('checkbox_activ_bp_unique_prix', 'activ_bp_unique_prix');" <?php if (isset($oBonplan->activ_bp_unique_prix)) {
                        if ($oBonplan->activ_bp_unique_prix == true) {
                            echo "checked = 'checked'";
                        }
                    } ?>/><input id="activ_bp_unique_prix" type="hidden"
                                 value="<?php if (isset($oBonplan->activ_bp_unique_prix)) {
                                     echo $oBonplan->activ_bp_unique_prix;
                                 } ?>" name="bonplan[activ_bp_unique_prix]"></div>
                <div class="col-xs-6 padding0"><input type="text" name="bonplan[bp_unique_prix]" id="bp_unique_prix"
                                                      class="form-control"
                                                      value="<?php if (isset($oBonplan->bp_unique_prix)) {
                                                          echo $oBonplan->bp_unique_prix;
                                                      } ?>"></div>
            </div>
            <div class="col-xs-12 padding0 marginbottom5">
                <div class="col-xs-4 padding0"><label>Valeur : </label></div>
                <div class="col-xs-2 padding0"></div>
                <div class="col-xs-6 padding0"><input type="text" name="bonplan[bp_unique_value]" id="bp_unique_value"
                                                      class="form-control"
                                                      value="<?php if (isset($oBonplan->bp_unique_value)) {
                                                          echo $oBonplan->bp_unique_value;
                                                      } ?>"></div>
            </div>
            <div class="col-xs-12 padding0 marginbottom5">
                <div class="col-xs-4 padding0"><label>Economie : </label></div>
                <div class="col-xs-2 padding0"></div>
                <div class="col-xs-6 padding0"><input type="text" name="bonplan[bp_unique_eco]" id="bp_unique_eco"
                                                      class="form-control"
                                                      value="<?php if (isset($oBonplan->bp_unique_eco)) {
                                                          echo $oBonplan->bp_unique_eco;
                                                      } ?>"></div>
            </div>

            <div class="col-xs-12 padding0 margintop15 marginbottom5">
                <div class="col-xs-4 padding0"><label>Quantité proposée
                        <small>(Cochez pour activer)</small>
                        : </label></div>
                <div class="col-xs-2"><input name="checkbox_activ_bp_unique_qttprop"
                                             id="checkbox_activ_bp_unique_qttprop" type="checkbox" value=""
                                             onchange="checkbox_activ_status('checkbox_activ_bp_unique_qttprop', 'activ_bp_unique_qttprop');" <?php if (isset($oBonplan->activ_bp_unique_qttprop)) {
                        if ($oBonplan->activ_bp_unique_qttprop == true) {
                            echo "checked = 'checked'";
                        }
                    } ?>/><input id="activ_bp_unique_qttprop" type="hidden"
                                 value="<?php if (isset($oBonplan->activ_bp_unique_qttprop)) {
                                     echo $oBonplan->activ_bp_unique_qttprop;
                                 } ?>" name="bonplan[activ_bp_unique_qttprop]"></div>
                <div class="col-xs-6 padding0"><input type="text" name="bonplan[bp_unique_qttprop]"
                                                      id="bp_unique_qttprop" class="form-control"
                                                      value="<?php if (isset($oBonplan->bp_unique_qttprop)) {
                                                          echo $oBonplan->bp_unique_qttprop;
                                                      } ?>"></div>
            </div>


            <div class="col-xs-12 padding0 marginbottom5">
                <div class="col-xs-4 padding0"><label>Précisez le nombre maximum de réservation par bon plan
                        <small>(Cochez pour activer)</small>
                        : </label></div>
                <div class="col-xs-2"><input name="checkbox_activ_bp_unique_nbmax" id="checkbox_activ_bp_unique_nbmax"
                                             type="checkbox" value=""
                                             onchange="checkbox_activ_status('checkbox_activ_bp_unique_nbmax', 'activ_bp_unique_nbmax');" <?php if (isset($oBonplan->activ_bp_unique_nbmax)) {
                        if ($oBonplan->activ_bp_unique_nbmax == true) {
                            echo "checked = 'checked'";
                        }
                    } ?>/><input id="activ_bp_unique_nbmax" type="hidden"
                                 value="<?php if (isset($oBonplan->activ_bp_unique_nbmax)) {
                                     echo $oBonplan->activ_bp_unique_nbmax;
                                 } ?>" name="bonplan[activ_bp_unique_nbmax]"></div>
                <div class="col-xs-6 padding0">
                    <select name="bonplan[bp_unique_nbmax]" id="bp_unique_nbmax" class="form-control">
                        <option value="1" <?php if (isset($oBonplan->bp_unique_nbmax)) {
                            if ($oBonplan->bp_unique_nbmax == "1") {
                                echo 'selected="selected"';
                            }
                        } ?>>01
                        </option>
                        <option value="2" <?php if (isset($oBonplan->bp_unique_nbmax)) {
                            if ($oBonplan->bp_unique_nbmax == "2") {
                                echo 'selected="selected"';
                            }
                        } ?>>02
                        </option>
                        <option value="3" <?php if (isset($oBonplan->bp_unique_nbmax)) {
                            if ($oBonplan->bp_unique_nbmax == "3") {
                                echo 'selected="selected"';
                            }
                        } ?>>03
                        </option>
                        <option value="4" <?php if (isset($oBonplan->bp_unique_nbmax)) {
                            if ($oBonplan->bp_unique_nbmax == "4") {
                                echo 'selected="selected"';
                            }
                        } ?>>04
                        </option>
                        <option value="5" <?php if (isset($oBonplan->bp_unique_nbmax)) {
                            if ($oBonplan->bp_unique_nbmax == "5") {
                                echo 'selected="selected"';
                            }
                        } ?>>05
                        </option>
                        <option value="6" <?php if (isset($oBonplan->bp_unique_nbmax)) {
                            if ($oBonplan->bp_unique_nbmax == "6") {
                                echo 'selected="selected"';
                            }
                        } ?>>06
                        </option>
                        <option value="7" <?php if (isset($oBonplan->bp_unique_nbmax)) {
                            if ($oBonplan->bp_unique_nbmax == "7") {
                                echo 'selected="selected"';
                            }
                        } ?>>07
                        </option>
                        <option value="8" <?php if (isset($oBonplan->bp_unique_nbmax)) {
                            if ($oBonplan->bp_unique_nbmax == "8") {
                                echo 'selected="selected"';
                            }
                        } ?>>08
                        </option>
                        <option value="9" <?php if (isset($oBonplan->bp_unique_nbmax)) {
                            if ($oBonplan->bp_unique_nbmax == "9") {
                                echo 'selected="selected"';
                            }
                        } ?>>09
                        </option>
                        <option value="10" <?php if (isset($oBonplan->bp_unique_nbmax)) {
                            if ($oBonplan->bp_unique_nbmax == "10") {
                                echo 'selected="selected"';
                            }
                        } ?>>10
                        </option>
                        <option value="11" <?php if (isset($oBonplan->bp_unique_nbmax)) {
                            if ($oBonplan->bp_unique_nbmax == "11") {
                                echo 'selected="selected"';
                            }
                        } ?>>11
                        </option>
                        <option value="12" <?php if (isset($oBonplan->bp_unique_nbmax)) {
                            if ($oBonplan->bp_unique_nbmax == "12") {
                                echo 'selected="selected"';
                            }
                        } ?>>12
                        </option>
                        <option value="13" <?php if (isset($oBonplan->bp_unique_nbmax)) {
                            if ($oBonplan->bp_unique_nbmax == "13") {
                                echo 'selected="selected"';
                            }
                        } ?>>13
                        </option>
                        <option value="14" <?php if (isset($oBonplan->bp_unique_nbmax)) {
                            if ($oBonplan->bp_unique_nbmax == "14") {
                                echo 'selected="selected"';
                            }
                        } ?>>14
                        </option>
                        <option value="15" <?php if (isset($oBonplan->bp_unique_nbmax)) {
                            if ($oBonplan->bp_unique_nbmax == "15") {
                                echo 'selected="selected"';
                            }
                        } ?>>15
                        </option>
                        <option value="16" <?php if (isset($oBonplan->bp_unique_nbmax)) {
                            if ($oBonplan->bp_unique_nbmax == "16") {
                                echo 'selected="selected"';
                            }
                        } ?>>16
                        </option>
                        <option value="17" <?php if (isset($oBonplan->bp_unique_nbmax)) {
                            if ($oBonplan->bp_unique_nbmax == "17") {
                                echo 'selected="selected"';
                            }
                        } ?>>17
                        </option>
                        <option value="18" <?php if (isset($oBonplan->bp_unique_nbmax)) {
                            if ($oBonplan->bp_unique_nbmax == "18") {
                                echo 'selected="selected"';
                            }
                        } ?>>18
                        </option>
                        <option value="19" <?php if (isset($oBonplan->bp_unique_nbmax)) {
                            if ($oBonplan->bp_unique_nbmax == "19") {
                                echo 'selected="selected"';
                            }
                        } ?>>19
                        </option>
                        <option value="20" <?php if (isset($oBonplan->bp_unique_nbmax)) {
                            if ($oBonplan->bp_unique_nbmax == "20") {
                                echo 'selected="selected"';
                            }
                        } ?>>20
                        </option>
                    </select>
                </div>
            </div>

            <div class="col-xs-12 padding0 marginbottom5">
                <div class="col-xs-4 padding0"><label>Précisez la date de visite
                        <small>(Cochez pour activer)</small>
                        : </label></div>
                <div class="col-xs-2"><input name="checkbox_activ_bp_unique_date_visit"
                                             id="checkbox_activ_bp_unique_date_visit" type="checkbox" value=""
                                             onchange="checkbox_activ_status('checkbox_activ_bp_unique_date_visit', 'activ_bp_unique_date_visit');" <?php if (isset($oBonplan->activ_bp_unique_date_visit)) {
                        if ($oBonplan->activ_bp_unique_date_visit == true) {
                            echo "checked = 'checked'";
                        }
                    } ?>/><input id="activ_bp_unique_date_visit" type="hidden"
                                 value="<?php if (isset($oBonplan->activ_bp_unique_date_visit)) {
                                     echo $oBonplan->activ_bp_unique_date_visit;
                                 } ?>" name="bonplan[activ_bp_unique_date_visit]"></div>
                <div class="col-xs-6 padding0"></div>
            </div>

            <div class="col-xs-12 padding0 marginbottom5">
                <div class="col-xs-4 padding0"><label>Réserver & régler
                        <small>(Cochez pour activer)</small>
                        : </label></div>
                <div class="col-xs-8"><input name="checkbox_activ_bp_unique_reserv_regler"
                                             id="checkbox_activ_bp_unique_reserv_regler" type="checkbox" value=""
                                             onchange="checkbox_activ_status('checkbox_activ_bp_unique_reserv_regler', 'activ_bp_unique_reserv_regler');" <?php if (isset($oBonplan->activ_bp_unique_reserv_regler)) {
                        if ($oBonplan->activ_bp_unique_reserv_regler == true) {
                            echo "checked = 'checked'";
                        }
                    } ?>/><input id="activ_bp_unique_reserv_regler" type="hidden"
                                 value="<?php if (isset($oBonplan->activ_bp_unique_reserv_regler)) {
                                     echo $oBonplan->activ_bp_unique_reserv_regler;
                                 } ?>" name="bonplan[activ_bp_unique_reserv_regler]"></div>
            </div>


        </div>


        <div class="col-xs-12 adminbp_title">
            <div class="col-xs-9 padding0">Bon plan Multiple avec Sortez
                <small>(gestion automatis&eacute;e des transactions)</small>
            </div>
            <div class="col-xs-3 padding0 textalignright"><input name="bonplan[bonplan_type]" type="radio"
                                                                 value="3" <?php if (isset($oBonplan->bonplan_type) && $oBonplan->bonplan_type == "3") {
                    echo 'checked="checked"';
                } ?>/><label class="marginleft15">Activer</label></div>
        </div>

        <div class="col-xs-12 greybg paddingbottom15">


            <div class="col-xs-12 padding0 margintop15">
                <div class="col-xs-9 padding0">
                    <p class="bpcolor">BON PLAN OFFRE MULTIPLE AVEC OU SANS QUANTITE DE DÉPART :<br/>
                        Le consommateur peut valider plusieurs fois le même bon plan. </p>
                    <p>Ce bon plan est attaché au concept Sortez, il suffit au consommateur de réserver en ligne, notre
                        système le valide automatiquement.<br/>
                        Le professionnel et le consommateur recoivent un e-mail de confirmation.<br/>
                        La quantité restante est décrémentée automatiquement.<br/>
                        Cette réservation est alors intégré sur les pages des réservations en cours.<br/>
                        Lors de son déplacement le consommateur présente sa carte sur son mobile.<br/>
                        Avec l’application Sortez, le professionnel identifie le consommateur, valide définitivement ce
                        bon plan et le supprime sur la page des réservations en cours.<br/>
                        La transaction est enregistrée sur l’historique du consommateur (compte consommateur et compte
                        professionnel).</p>
                </div>
                <div class="col-xs-3 padding0 textalignright"><img
                        src="<?php echo GetImagePath("privicarte/"); ?>/bp_multiple_img.png"
                        class="marginbottom15 margintop15"/><img
                        src="<?php echo GetImagePath("privicarte/"); ?>/bp_multiple_nb.png" class="margintop15"/></div>
            </div>

            <div class="col-xs-12 padding0 margintop15 marginbottom5">
                <div class="col-xs-4 padding0"><label>Date début (obligatoire) : </label></div>
                <div class="col-xs-8 padding0"><input type="text" name="bonplan[bp_multiple_date_debut]"
                                                      id="bp_multiple_date_debut" class="input_width form-control"
                                                      value="<?php if (isset($oBonplan->bp_multiple_date_debut) && $oBonplan->bp_multiple_date_debut != "0000-00-00") {
                                                          echo convert_Sqldate_to_Frenchdate($oBonplan->bp_multiple_date_debut);
                                                      } ?>"></div>
            </div>
            <div class="col-xs-12 padding0 marginbottom5">
                <div class="col-xs-4 padding0"><label>Date de fin (obligatoire) : </label></div>
                <div class="col-xs-8 padding0"><input type="text" name="bonplan[bp_multiple_date_fin]"
                                                      id="bp_multiple_date_fin" class="input_width form-control"
                                                      value="<?php if (isset($oBonplan->bp_multiple_date_fin) && $oBonplan->bp_multiple_date_fin != "0000-00-00") {
                                                          echo convert_Sqldate_to_Frenchdate($oBonplan->bp_multiple_date_fin);
                                                      } ?>"></div>
            </div>

            <div class="col-xs-12 padding0 margintop15 marginbottom5">
                <div class="col-xs-4 padding0"><label>Prix bon plan
                        <small>(Cochez pour activer)</small>
                        : </label></div>
                <div class="col-xs-2"><input name="checkbox_activ_bp_multiple_prix" id="checkbox_activ_bp_multiple_prix"
                                             type="checkbox" value=""
                                             onchange="checkbox_activ_status('checkbox_activ_bp_multiple_prix', 'activ_bp_multiple_prix');" <?php if (isset($oBonplan->activ_bp_multiple_prix)) {
                        if ($oBonplan->activ_bp_multiple_prix == true) {
                            echo "checked = 'checked'";
                        }
                    } ?>/><input id="activ_bp_multiple_prix" type="hidden"
                                 value="<?php if (isset($oBonplan->activ_bp_multiple_prix)) {
                                     echo $oBonplan->activ_bp_multiple_prix;
                                 } ?>" name="bonplan[activ_bp_multiple_prix]"></div>
                <div class="col-xs-6 padding0"><input type="text" name="bonplan[bp_multiple_prix]" id="bp_multiple_prix"
                                                      class="form-control"
                                                      value="<?php if (isset($oBonplan->bp_multiple_prix)) {
                                                          echo $oBonplan->bp_multiple_prix;
                                                      } ?>"></div>
            </div>
            <div class="col-xs-12 padding0 marginbottom5">
                <div class="col-xs-4 padding0"><label>Valeur : </label></div>
                <div class="col-xs-2 padding0"></div>
                <div class="col-xs-6 padding0"><input type="text" name="bonplan[bp_multiple_value]"
                                                      id="bp_multiple_value" class="form-control"
                                                      value="<?php if (isset($oBonplan->bp_multiple_value)) {
                                                          echo $oBonplan->bp_multiple_value;
                                                      } ?>"></div>
            </div>
            <div class="col-xs-12 padding0 marginbottom5">
                <div class="col-xs-4 padding0"><label>Economie : </label></div>
                <div class="col-xs-2 padding0"></div>
                <div class="col-xs-6 padding0"><input type="text" name="bonplan[bp_multiple_eco]" id="bp_multiple_eco"
                                                      class="form-control"
                                                      value="<?php if (isset($oBonplan->bp_multiple_eco)) {
                                                          echo $oBonplan->bp_multiple_eco;
                                                      } ?>"></div>
            </div>

            <div class="col-xs-12 padding0 margintop15 marginbottom5">
                <div class="col-xs-4 padding0"><label>Quantité proposée
                        <small>(Cochez pour activer)</small>
                        : </label></div>
                <div class="col-xs-2"><input name="checkbox_activ_bp_multiple_qttprop"
                                             id="checkbox_activ_bp_multiple_qttprop" type="checkbox" value=""
                                             onchange="checkbox_activ_status('checkbox_activ_bp_multiple_qttprop', 'activ_bp_multiple_qttprop');" <?php if (isset($oBonplan->activ_bp_multiple_qttprop)) {
                        if ($oBonplan->activ_bp_multiple_qttprop == true) {
                            echo "checked = 'checked'";
                        }
                    } ?>/><input id="activ_bp_multiple_qttprop" type="hidden"
                                 value="<?php if (isset($oBonplan->activ_bp_multiple_qttprop)) {
                                     echo $oBonplan->activ_bp_multiple_qttprop;
                                 } ?>" name="bonplan[activ_bp_multiple_qttprop]"></div>
                <div class="col-xs-6 padding0"><input type="text" name="bonplan[bp_multiple_qttprop]"
                                                      id="bp_multiple_qttprop" class="form-control"
                                                      value="<?php if (isset($oBonplan->bp_multiple_qttprop)) {
                                                          echo $oBonplan->bp_multiple_qttprop;
                                                      } ?>"></div>
            </div>


            <div class="col-xs-12 padding0 marginbottom5">
                <div class="col-xs-4 padding0"><label>Précisez le nombre maximum de réservation par bon plan
                        <small>(Cochez pour activer)</small>
                        : </label></div>
                <div class="col-xs-2"><input name="checkbox_activ_bp_multiple_nbmax"
                                             id="checkbox_activ_bp_multiple_nbmax" type="checkbox" value=""
                                             onchange="checkbox_activ_status('checkbox_activ_bp_multiple_nbmax', 'activ_bp_multiple_nbmax');" <?php if (isset($oBonplan->activ_bp_multiple_nbmax)) {
                        if ($oBonplan->activ_bp_multiple_nbmax == true) {
                            echo "checked = 'checked'";
                        }
                    } ?>/><input id="activ_bp_multiple_nbmax" type="hidden"
                                 value="<?php if (isset($oBonplan->activ_bp_multiple_nbmax)) {
                                     echo $oBonplan->activ_bp_multiple_nbmax;
                                 } ?>" name="bonplan[activ_bp_multiple_nbmax]"></div>
                <div class="col-xs-6 padding0">
                    <select name="bonplan[bp_multiple_nbmax]" id="bp_multiple_nbmax" class="form-control">
                        <option value="1" <?php if (isset($oBonplan->bp_multiple_nbmax)) {
                            if ($oBonplan->bp_multiple_nbmax == "1") {
                                echo 'selected="selected"';
                            }
                        } ?>>01
                        </option>
                        <option value="2" <?php if (isset($oBonplan->bp_multiple_nbmax)) {
                            if ($oBonplan->bp_multiple_nbmax == "2") {
                                echo 'selected="selected"';
                            }
                        } ?>>02
                        </option>
                        <option value="3" <?php if (isset($oBonplan->bp_multiple_nbmax)) {
                            if ($oBonplan->bp_multiple_nbmax == "3") {
                                echo 'selected="selected"';
                            }
                        } ?>>03
                        </option>
                        <option value="4" <?php if (isset($oBonplan->bp_multiple_nbmax)) {
                            if ($oBonplan->bp_multiple_nbmax == "4") {
                                echo 'selected="selected"';
                            }
                        } ?>>04
                        </option>
                        <option value="5" <?php if (isset($oBonplan->bp_multiple_nbmax)) {
                            if ($oBonplan->bp_multiple_nbmax == "5") {
                                echo 'selected="selected"';
                            }
                        } ?>>05
                        </option>
                        <option value="6" <?php if (isset($oBonplan->bp_multiple_nbmax)) {
                            if ($oBonplan->bp_multiple_nbmax == "6") {
                                echo 'selected="selected"';
                            }
                        } ?>>06
                        </option>
                        <option value="7" <?php if (isset($oBonplan->bp_multiple_nbmax)) {
                            if ($oBonplan->bp_multiple_nbmax == "7") {
                                echo 'selected="selected"';
                            }
                        } ?>>07
                        </option>
                        <option value="8" <?php if (isset($oBonplan->bp_multiple_nbmax)) {
                            if ($oBonplan->bp_multiple_nbmax == "8") {
                                echo 'selected="selected"';
                            }
                        } ?>>08
                        </option>
                        <option value="9" <?php if (isset($oBonplan->bp_multiple_nbmax)) {
                            if ($oBonplan->bp_multiple_nbmax == "9") {
                                echo 'selected="selected"';
                            }
                        } ?>>09
                        </option>
                        <option value="10" <?php if (isset($oBonplan->bp_multiple_nbmax)) {
                            if ($oBonplan->bp_multiple_nbmax == "10") {
                                echo 'selected="selected"';
                            }
                        } ?>>10
                        </option>
                        <option value="11" <?php if (isset($oBonplan->bp_multiple_nbmax)) {
                            if ($oBonplan->bp_multiple_nbmax == "11") {
                                echo 'selected="selected"';
                            }
                        } ?>>11
                        </option>
                        <option value="12" <?php if (isset($oBonplan->bp_multiple_nbmax)) {
                            if ($oBonplan->bp_multiple_nbmax == "12") {
                                echo 'selected="selected"';
                            }
                        } ?>>12
                        </option>
                        <option value="13" <?php if (isset($oBonplan->bp_multiple_nbmax)) {
                            if ($oBonplan->bp_multiple_nbmax == "13") {
                                echo 'selected="selected"';
                            }
                        } ?>>13
                        </option>
                        <option value="14" <?php if (isset($oBonplan->bp_multiple_nbmax)) {
                            if ($oBonplan->bp_multiple_nbmax == "14") {
                                echo 'selected="selected"';
                            }
                        } ?>>14
                        </option>
                        <option value="15" <?php if (isset($oBonplan->bp_multiple_nbmax)) {
                            if ($oBonplan->bp_multiple_nbmax == "15") {
                                echo 'selected="selected"';
                            }
                        } ?>>15
                        </option>
                        <option value="16" <?php if (isset($oBonplan->bp_multiple_nbmax)) {
                            if ($oBonplan->bp_multiple_nbmax == "16") {
                                echo 'selected="selected"';
                            }
                        } ?>>16
                        </option>
                        <option value="17" <?php if (isset($oBonplan->bp_multiple_nbmax)) {
                            if ($oBonplan->bp_multiple_nbmax == "17") {
                                echo 'selected="selected"';
                            }
                        } ?>>17
                        </option>
                        <option value="18" <?php if (isset($oBonplan->bp_multiple_nbmax)) {
                            if ($oBonplan->bp_multiple_nbmax == "18") {
                                echo 'selected="selected"';
                            }
                        } ?>>18
                        </option>
                        <option value="19" <?php if (isset($oBonplan->bp_multiple_nbmax)) {
                            if ($oBonplan->bp_multiple_nbmax == "19") {
                                echo 'selected="selected"';
                            }
                        } ?>>19
                        </option>
                        <option value="20" <?php if (isset($oBonplan->bp_multiple_nbmax)) {
                            if ($oBonplan->bp_multiple_nbmax == "20") {
                                echo 'selected="selected"';
                            }
                        } ?>>20
                        </option>
                    </select>
                </div>
            </div>

            <div class="col-xs-12 padding0 marginbottom5">
                <div class="col-xs-4 padding0"><label>Précisez la date de visite
                        <small>(Cochez pour activer)</small>
                        : </label></div>
                <div class="col-xs-2"><input name="checkbox_activ_bp_multiple_date_visit"
                                             id="checkbox_activ_bp_multiple_date_visit" type="checkbox" value=""
                                             onchange="checkbox_activ_status('checkbox_activ_bp_multiple_date_visit', 'activ_bp_multiple_date_visit');" <?php if (isset($oBonplan->activ_bp_multiple_date_visit)) {
                        if ($oBonplan->activ_bp_multiple_date_visit == true) {
                            echo "checked = 'checked'";
                        }
                    } ?>/><input id="activ_bp_multiple_date_visit" type="hidden"
                                 value="<?php if (isset($oBonplan->activ_bp_multiple_date_visit)) {
                                     echo $oBonplan->activ_bp_multiple_date_visit;
                                 } ?>" name="bonplan[activ_bp_multiple_date_visit]"></div>
                <div class="col-xs-6 padding0"></div>
            </div>

            <div class="col-xs-12 padding0 marginbottom5">
                <div class="col-xs-4 padding0"><label>Réserver & régler
                        <small>(Cochez pour activer)</small>
                        : </label></div>
                <div class="col-xs-8"><input name="checkbox_activ_bp_multiple_reserv_regler"
                                             id="checkbox_activ_bp_multiple_reserv_regler" type="checkbox" value=""
                                             onchange="checkbox_activ_status('checkbox_activ_bp_multiple_reserv_regler', 'activ_bp_multiple_reserv_regler');" <?php if (isset($oBonplan->activ_bp_multiple_reserv_regler)) {
                        if ($oBonplan->activ_bp_multiple_reserv_regler == true) {
                            echo "checked = 'checked'";
                        }
                    } ?>/><input id="activ_bp_multiple_reserv_regler" type="hidden"
                                 value="<?php if (isset($oBonplan->activ_bp_multiple_reserv_regler)) {
                                     echo $oBonplan->activ_bp_multiple_reserv_regler;
                                 } ?>" name="bonplan[activ_bp_multiple_reserv_regler]"></div>
            </div>


        </div>


        <div class="col-xs-12 adminbp_title" style="display: none;">RESERVER & REGLER</div>


        <div class="col-xs-12 greybg paddingbottom15" style="display: none;">

            <div class="col-xs-12 textaligncenter"><span class="admin_bp_green_btn_regler">RESERVER & REGLER</span>
            </div>
            <div class="col-xs-12 padding0">
                <div class="col-xs-3 padding0">Lien direct client de paiement en ligne</div>
                <div class="col-xs-9 padding0"><input name="bonplan[bp_reserver_link]" id="bp_reserver_link" type="text"
                                                      class="width100p form-control"
                                                      value="<?php if (isset($oBonplan->bp_reserver_link)) {
                                                          echo $oBonplan->bp_reserver_link;
                                                      } ?>"/></div>
            </div>
            <div class="col-xs-12 padding0">
                <div class="col-xs-3 padding0">Activation module Paypal</div>
                <div class="col-xs-9 padding0"><input name="checkbox_activ_bp_reserver_paypal"
                                                      id="checkbox_activ_bp_reserver_paypal" type="checkbox" value=""
                                                      onchange="checkbox_activ_status('checkbox_activ_bp_reserver_paypal', 'activ_bp_reserver_paypal');" <?php if (isset($oBonplan->activ_bp_reserver_paypal)) {
                        if ($oBonplan->activ_bp_reserver_paypal == true) {
                            echo "checked = 'checked'";
                        }
                    } ?>/><input id="activ_bp_reserver_paypal" type="hidden"
                                 value="<?php if (isset($oBonplan->activ_bp_reserver_paypal)) {
                                     echo $oBonplan->activ_bp_reserver_paypal;
                                 } ?>" name="bonplan[activ_bp_reserver_paypal]"></div>
            </div>
            <div class="col-xs-12 padding0">
                <div class="col-xs-9 padding0">Comment ouvrir un compte PAYPAL et intégrer un bouton de paiement
                    automatique PAYPAL à ce bon plan ?
                </div>
                <div class="col-xs-3 padding0"><a
                        href="https://www.paypal.com/ci/cgi-bin/webscr?cmd=_singleitem-intro-outside"
                        target="_blank"><img src="<?php echo GetImagePath("privicarte/"); ?>/wpe77cb282_06.png"/></a>
                </div>
            </div>
            <div class="col-xs-12 padding0">
                Intégration de votre bouton Paypal (Copier le code html fournit par PAYPAL).<br/>
                Le bouton apparaîtra automatiquement sur votre page.
            </div>
            <div class="col-xs-12 padding0">
                <div class="col-xs-3 padding0"></div>
                <div class="col-xs-9 padding0">
					 <textarea name="bonplan[bp_code_btn_paypal]" id="bp_code_btn_paypal" cols="" rows=""
                               class="width100p form-control">
                         <?php if (isset($oBonplan->bp_code_btn_paypal)) {
                             echo $oBonplan->bp_code_btn_paypal;
                         } ?>
                     </textarea>
                </div>
            </div>
            <div class="col-xs-12 padding0">
                Si vous choisissez d’intégrer un panier, nous vous invitons à intégrer le code html PAYPAL du bouton
                afficher le panier » Le bouton apparaîtra automatique-ment sur votre page.
            </div>
            <div class="col-xs-12 padding0">
                <div class="col-xs-3 padding0"></div>
                <div class="col-xs-9 padding0"><textarea name="bonplan[bp_code_panier_paypal]"
                                                         id="bp_code_panier_paypal" cols="" rows=""
                                                         class="width100p form-control"><?php if (isset($oBonplan->bp_code_panier_paypal)) {
                            echo $oBonplan->bp_code_panier_paypal;
                        } ?></textarea></div>
            </div>

        </div>


        <div class="col-xs-12 adminbp_title">
        </div>


        <div class="col-xs-12 margintop15 padding0">
            <div class="col-xs-12 textaligncenter padding0"><a href="javascript:void(0);"
                                                               onclick="javascript:testFormBonplan();"
                                                               class="admin_bp_green_btn">1. Validation</a></div>
        </div>
        <div class="col-xs-12 margintop15 padding0">
            <div class="col-xs-12 textaligncenter padding0">
                <a href="javascript:void(0);"
                       onclick='javascript:ouvrir()'><img src="<?php echo GetImagePath("privicarte/"); ?>/wp9efa6d49_06.png"/>
                </a>
            </div>
        </div>
        <div class="col-xs-12 margintop15 padding0">
            <div class="col-xs-12 textaligncenter padding0">
                <a href="javascript:void(0);"
                       onclick='javascript:window.open("https://twitter.com/home?status=<?php echo site_url($oInfoCommercant->nom_url.'/notre_bonplan/' . $oBonplan->bonplan_id); ?>", "TwitterPrivicarteForm_share", "width=500, height=800");'
                       title="Partage Twitter"><img src="<?php echo GetImagePath("privicarte/"); ?>/wp9cd46019_06.png"/>
                </a>
            </div>
        </div>

        <div class="col-xs-12 margintop30 padding0">
            <div class="col-xs-6 padding0">
                <?php if (isset($from_privicarte) && $from_privicarte=='1') { ?>
                <a href="<?php echo site_url("front/fidelity/mes_parametres/"); ?>"
                                              class="admin_bp_red_btn">Annuler</a>
                <?php } else { ?>
                    <a href="<?php echo site_url("front/utilisateur/contenupro"); ?>"
                       class="admin_bp_red_btn">Annuler</a>
                <?php } ?>
            </div>
            <div class="col-xs-6 paddingright0">
                <?php if (isset($oBonplan) && $oBonplan->bonplan_id != '0') { ?>
                    <a href="<?php echo site_url("front/bonplan/supprimBonplan/" . $oBonplan->bonplan_id . "/" . $idCommercant); ?>"
                       onclick="if (!confirm('voulez-vous vraiment supprimer ce Bon Plan ?')){ return false ; }"
                       class="admin_bp_red_btn">Supprimer</a>
                <?php } else { ?>
                    <a href="javascript:void(0);" class="admin_bp_red_btn">Supprimer</a>
                <?php } ?>
            </div>
        </div>


        <?php
        if (isset($oBonplan)) {

            $thisss =& get_instance();
            $thisss->load->model("commercant");
            $objCommercant = $thisss->commercant->GetById($idCommercant);
            ?>

            <div class="col-xs-12 margintop15">
                <p style="text-align:center;"><a
                        href="<?php echo site_url($objCommercant->nom_url . '/notre_bonplan/' . $oBonplan->bonplan_id); ?>"
                        target="_blank"><img src="<?php echo GetImagePath("privicarte/"); ?>/admin_bp_testing.png"/></a>
                </p>
            </div>

        <?php } ?>


    </form>
</div>
<?php $this->load->view("frontAout2013/includes/footer_mini_2"); ?>
