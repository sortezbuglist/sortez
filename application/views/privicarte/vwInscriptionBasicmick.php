<?php $data["zTitle"] = 'Inscription professionnel' ?>

<?php $this->load->view("sortez/includes/header_frontoffice_com", $data); ?>



    <?php $this->load->view("sortez/logo_global", $data); ?>



    <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/fields.check.js"></script>



    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.6.4.js"></script>

    <script src="https://www.google.com/recaptcha/api.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-3.2.1.js"></script>

    <meta name="viewport" content="width=device-width" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url("application/resources/privicarte/css/global.css") ?>">

    <script type="text/javascript">



        // Use jQuery via $(...)

        $(document).ready(function () {//debut ready fonction





            //To show sousrubrique corresponding to rubrique

            $("#RubriqueSociete").change(function () {

                $('#trReponseRub').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

                var irubId = $("#RubriqueSociete").val();

                //alert(irubId);

                $.ajax({

                    type: "GET",

                    url: "<?php echo base_url(); ?>front/professionnels/testAjax/" + irubId,

                    success: function (msg) {

                        //alert(msg);

                        var numero_reponse = msg;

                        $("#trReponseRub").html(numero_reponse);

                        //alert(numero_reponse);

                    }

                });

                //alert ("test "+$("#RubriqueSociete").val());



            });





            $("#EmailSociete").blur(function () {

                //alert('cool');

                //var result_to_show = "";

                var value_result_to_show = "0";



                var txtEmail = $("#EmailSociete").val();

                //alert('<?php //echo site_url("front/professionnels/verifier_email"); ?>' + '/' + txtEmail);

                //jQuery(".EmailLoading").addClass("EmailLoading_show").removeClass("EmailLoading");

                $.post(

                    '<?php echo site_url("front/professionnels/verifier_email");?>',

                    {txtEmail_var: txtEmail},

                    function (zReponse) {

                        //alert (zReponse) ;

                        //var zReponse_html = '';

                        if (zReponse == "1") {

                            value_result_to_show = "1";

                        }





                        if (value_result_to_show == "1") {

                            result_to_show = '<span style="color:#F00; font-weight:bold;">Ce mail est d&eacute;j&agrave; utilis&eacute;</span>';

                            $('#divErrortxtEmail_').html(result_to_show);

                            $('#txtEmail_verif').val("1");

                        } else {

                            result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce mail est disponible</span>';

                            $('#divErrortxtEmail_').html(result_to_show);

                            $('#txtEmail_verif').val("0");

                        }



                    });



                $.post(

                    '<?php echo site_url("front/professionnels/verifier_email_ionauth");?>',

                    {txtEmail_var_ionauth: txtEmail},

                    function (zReponse_ionauth) {

                        //alert (zReponse) ;

                        //var zReponse_html_ionauth = '';

                        if (zReponse_ionauth == "1") {

                            value_result_to_show = "1";

                        }





                        if (value_result_to_show == "1") {

                            result_to_show = '<span style="color:#F00; font-weight:bold;">Ce mail est d&eacute;j&agrave; utilis&eacute;</span>';

                            $('#divErrortxtEmail_').html(result_to_show);

                            $('#txtEmail_verif').val("1");

                        } else {

                            result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce mail est disponible</span>';

                            $('#divErrortxtEmail_').html(result_to_show);

                            $('#txtEmail_verif').val("0");

                        }





                    });





                //jQuery(".FieldError").removeClass("FieldError");

                //jQuery(".EmailLoading").addClass("EmailLoading").removeClass("EmailLoading_show");



            });





            $("#txtLogin").blur(function () {

                //alert('cool');

                var txtLogin = $("#txtLogin").val();



                var value_result_to_show = "0";



                //alert('<?php //echo site_url("front/professionnels/verifier_login"); ?>' + '/' + txtEmail);

                //jQuery(".EmailLoading").addClass("EmailLoading_show").removeClass("EmailLoading");

                $.post(

                    '<?php echo site_url("front/professionnels/verifier_login");?>',

                    {txtLogin_var: txtLogin},

                    function (zReponse) {

                        //alert (zReponse) ;

                        var zReponse_html = '';

                        if (zReponse == "1") {

                            value_result_to_show = "1";

                        }





                        if (value_result_to_show == "1") {

                            result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';

                            $('#divErrortxtLogin_').html(result_to_show);

                            $('#txtLogin_verif').val("1");

                        } else {

                            result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';

                            $('#divErrortxtLogin_').html(result_to_show);

                            $('#txtLogin_verif').val("0");

                        }



                    });





                $.post(

                    '<?php echo site_url("front/professionnels/verifier_login_ionauth");?>',

                    {txtLogin_var_ionauth: txtLogin},

                    function (zReponse_ionauth) {

                        //alert (zReponse) ;

                        var zReponse_html_ionauth = '';

                        if (zReponse_ionauth == "1") {

                            value_result_to_show = "1";

                        }





                        if (value_result_to_show == "1") {

                            result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';

                            $('#divErrortxtLogin_').html(result_to_show);

                            $('#txtLogin_verif').val("1");

                        } else {

                            result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';

                            $('#divErrortxtLogin_').html(result_to_show);

                            $('#txtLogin_verif').val("0");

                        }



                    });





                //jQuery(".FieldError").removeClass("FieldError");

                //jQuery(".EmailLoading").addClass("EmailLoading").removeClass("EmailLoading_show");



            });





            //To show postal code automatically

            $("#VilleSociete").change(function () {

                var irubId = $("#VilleSociete").val();

                //alert(irubId);

                $.ajax({

                    type: "GET",

                    url: "<?php echo base_url(); ?>front/professionnels/getPostalCode/" + irubId,

                    success: function (msg) {

                        //alert(msg);

                        var numero_reponse = msg;

                        $("#CodePostalSociete").val(numero_reponse);

                        //alert(numero_reponse);

                    }

                });

                //alert ("test "+$("#VilleSociete").val());

            });





            var valabonnementht = 0;

            var valmoduleht = 0;





            function calcmontantht() {



                var totalmontantht = 0;

                var montanttva = 0;

                var valtva = 0.20;

                var montantttc = 0;



                var $check_abonnement_list = new Array();

                <?php foreach ($colAbonnements as $objAbonnement) { ?>

                if ($('#check_abonnement_<?php echo $objAbonnement->IdAbonnement;?>').attr('checked'))  totalmontantht += parseInt("<?php echo $objAbonnement->tarif;?>");

                <?php } ?>



                //totalmontantht = check_358_premium_value + check_358_platinium_value + check_358_agenda_plus_value + check_358_web_ref1_value + check_358_web_ref_n_value + check_358_restauration_value;

                //alert(totalmontantht);

                $("#divMontantHT").html(totalmontantht + "€");

                $("#hidemontantht").val(totalmontantht + "€");

                //calcmontanttva (parseInt(totalmontantht), valtva);

                //calcmontantttc (parseInt(totalmontantht), montanttva);

                montanttva = totalmontantht * valtva;

                $("#divMontantTVA").html(_roundNumber(montanttva, 2) + "€");

                $("#hidemontanttva").val(_roundNumber(montanttva, 2) + "€");

                montantttc = totalmontantht + montanttva;

                $("#divMontantTTC").html(montantttc + "€");

                $("#hidemontantttc").val(montantttc + "€");

                $("#montantttcvalue_abonnement").val(montantttc);

            }





            function calcmontanttva(totalmontantht, valtva) {

                montanttva = totalmontantht * valtva;

                $("#divMontantTVA").html(_roundNumber(montanttva, 2) + "€");

            }



            //limit decimal

            function _roundNumber(num, dec) {



                return (parseFloat(num)).toFixed(dec);

            }



            function calcmontantttc(totalmontantht, montanttva) {

                montantttc = totalmontantht + montanttva;

                $("#divMontantTTC").html(montantttc + "€");

                $("#montantttcvalue_abonnement").val(montantttc);

            }









        })





    </script>



    <script type="text/javascript">var blankSrc = "wpscripts/blank.gif";

    </script>



    <script type="text/javascript">

        function btn_login_page_avantagepro() {

            //alert('qsdfgqsdf');

            var txtError = "";



            var user_login = $("#user_login").val();

            if (user_login == "" || user_login == "Préciser votre courriel") {

                txtError += "<br/>- Veuillez indiquer Votre login !";

                $("#user_login").css('border-color', 'red');

            }



            var user_pass = $("#user_pass").val();

            if (user_pass == "" || user_pass == "Préciser votre mot de passe") {

                txtError += "<br/>- Veuillez indiquer Votre mot de passe !";

                $("#user_pass").css('border-color', 'red');

            }



            if (txtError == "") {

                $("#frmConnexion").submit();

            }

        }



        $(function () {

            $("#user_login").focusin(function () {

                if ($(this).val() == "Préciser votre courriel") {

                    $(this).val("");

                }

            });

            $("#user_login").focusout(function () {

                if ($(this).val() == "") {

                    $(this).val("Préciser votre courriel");

                }

            });

            $("#user_pass").focusin(function () {

                if ($(this).val() == "Préciser votre mot de passe") {

                    $(this).val("");

                }

            });

            $("#user_pass").focusout(function () {

                if ($(this).val() == "") {

                    $(this).val("Préciser votre mot de passe");

                }

            });

        });

        jQuery(document).ready(function () {

            jQuery("#btnSinscrire").click(function () {

                var cap=document.getElementById("g-recaptcha-response").value;

                             test(cap);//21103 //toFirstIdDatatourisme

                    });

        });

        function test(cap) {

            jQuery.ajax({

                type: "POST",

                url: "<?php echo site_url("front/professionnels/test_captcha"); ?>",

                data: 'g-recaptcha-response=' + (cap),

                dataType: "json",

                success: function (data) {



                    var txtError = false;

                    var RubriqueSociete = $('#RubriqueSociete').val();

                    if (RubriqueSociete === "") {

                        txtError += "- Vous devez préciser votre activité<br/>";

                    }

                    var SousRubriqueSociete = $('#SousRubriqueSociete').val();

                    if (SousRubriqueSociete == "0") {

                        txtError += "- Vous devez préciser une sous-rubrique<br/>";

                    }

                    var NomSociete = $('#NomSociete').val();

                    if (NomSociete == "") {

                        txtError += "- Vous devez préciser le Nom ou enseigne<br/>";

                    }

                    var ivilleId = $('#VilleSociete').val();

                    if (ivilleId == 0) {

                        txtError += "- Vous devez sélectionner une ville<br/>";

                    }

                    var CodePostalSociete = $('#CodePostalSociete').val();

                    if (CodePostalSociete == "") {

                        txtError += "- Vous devez préciser le code postal<br/>";

                    }

                    var EmailSociete = $("#EmailSociete").val();

                    if (!isEmail(EmailSociete)) {

                        txtError += "- Veuillez indiquer un email valide.<br/>";

                    }

                    var txtEmail_verif = $("#txtEmail_verif").val();

                    if (txtEmail_verif == 1) {

                        txtError += "- Votre Email existe déjà sur notre site <br/>";

                    }

                    var NomResponsableSociete = $('#NomResponsableSociete').val();

                    if (NomResponsableSociete == "") {

                        txtError += "- Vous devez préciser le Nom du Decideur <br/>";

                    }

                    var PrenomResponsableSociete = $('#PrenomResponsableSociete').val();

                    if (PrenomResponsableSociete == "") {

                        txtError += "- Vous devez préciser le Prenom du Decideur <br/>";

                    }

                    var ResponsabiliteResponsableSociete = $('#ResponsabiliteResponsableSociete').val();

                    if (ResponsabiliteResponsableSociete == "") {

                        txtError += "- Vous devez préciser la responsabilité du Decideur <br/>";

                    }

                    var TelDirectResponsableSociete = $('#TelDirectResponsableSociete').val();

                    if (TelDirectResponsableSociete == "") {

                        txtError += "- Vous devez préciser le numero de téléphone du Decideur <br/>";

                    }

                    var Email_decideurResponsableSociete = $("#Email_decideurResponsableSociete").val();

                    if (!isEmail(Email_decideurResponsableSociete)) {

                        txtError += "- Veuillez indiquer un email valide pour le decideur.<br/>";

                    }

                    var AbonnementSociete = $('#AbonnementSociete').val();

                    if (AbonnementSociete == "0") {

                        txtError += "- Vous devez choisir votre abonnement<br/>";

                    }

                    var activite1Societe = $('#activite1Societe').val();

                    if (activite1Societe == "") {

                        txtError += "- Vous devez décrire votre activité<br/>";

                    }

                    var txtLogin = $("#txtLogin").val();

                    if (!isEmail(txtLogin)) {

                        txtError += "- Votre login doit &ecirc;tre un email valide.<br/>";

                    }

                    var txtLogin_verif = $("#txtLogin_verif").val();

                    if (txtLogin_verif == 1) {

                        txtError += "- Votre Login existe déjà sur notre site <br/>";

                    }

                    var passs = $('#txtPassword').val();

                    if (passs == "") {

                        txtError += "- Vous devez spécifier un mot de passe<br/>";

                    }

                    if ($("#txtPassword").val() != $("#txtConfirmPassword").val()) {

                        txtError += "- Les deux mots de passe ne sont pas identiques.<br/>";

                    }

                    if ($('#validationabonnement').is(":checked")) {

                    } else {

                        txtError += "- Vous devez valider les conditions générales<br/>";

                    }

                    if ($('#idreferencement0').is(":checked")) {

                        $("#idreferencement").val("1");

                    } else {

                        $("#idreferencement").val("0");

                    }

                    if (txtError ==false && data.captcha == "OK"){



                        $("#frmInscriptionProfessionnel").submit();

                    }if (data.captcha == "NO") {

                        alert("captha non valide");

                    }

                    if (txtError !=false){

                        $("#divErrorFrmInscriptionProfessionnel").html(txtError);

                    }

                },

                error: function (data) {



                    alert("data");

                }

            });

        }



        function CP_getDepartement() {

            //alert(jQuery('#CodePostalSociete').val());

            jQuery('#departementCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

            var CodePostalSociete = jQuery('#CodePostalSociete').val();

            jQuery.post(

                '<?php echo site_url("front/professionnels/departementcp"); ?>',

                {CodePostalSociete: CodePostalSociete},

                function (zReponse) {

                    jQuery('#departementCP_container').html(zReponse);

                });

        }



        function CP_getVille() {

            //alert(jQuery('#CodePostalSociete').val());

            jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

            var CodePostalSociete = jQuery('#CodePostalSociete').val();

            jQuery.post(

                '<?php echo site_url("front/professionnels/villecp"); ?>',

                {CodePostalSociete: CodePostalSociete},

                function (zReponse) {

                    jQuery('#villeCP_container').html(zReponse);

                });

        }



        function CP_getVille_D_CP() {

            //alert(jQuery('#CodePostalSociete').val());

            jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

            var CodePostalSociete = jQuery('#CodePostalSociete').val();

            var departement_id = jQuery('#departement_id').val();

            jQuery.post(

                '<?php echo site_url("front/professionnels/villecp"); ?>',

                {CodePostalSociete: CodePostalSociete, departement_id: departement_id},

                function (zReponse) {

                    jQuery('#villeCP_container').html(zReponse);

                });

        }



    </script>





    <style type="text/css">


        body {

            background-repeat: no-repeat;

            background-position: center center;

            background-size: 100% 100%;

        }


        .form-control {
            font-size: 14px !important;
        }


        .Normal-C-C1 {

            color: #000054;

            font-family: "Vladimir Script", cursive;

            font-size: 48px;

            line-height: 47px;

        }



        .contect_all_data_pro_subscription {

            margin-left: 15px;

            margin-right: 15px;

            /*margin-top:50px;*/

        }



        .title_sub_pro {

            color: #FFFFFF;

            font-family: "Arial", sans-serif;

            font-size: 15px;

            padding-bottom: 5px;

            padding-top: 5px;

            font-weight: 700;

            line-height: 1.19em;

            text-align: center;

            margin-top: 15px;

            margin-bottom: 15px;

            background-color: #000000;

            min-height: 27px;

        }



        .bloc_sub_pro {

            /*background-color:#3653A2;*/

            padding-bottom: 0px;

            padding-top: 5px;

        }



        .space_sub_pro {

            height: 20px;

        }



        .table_sub_pro {

            color: #000000;

            font-family: "Arial", sans-serif;

            font-size: 13px;

            line-height: 1.23em;

        }



        .table_sub_pro tr {

            height: 35px;

        }



        .table_sub_pro_abonnement td {

            border: 2px solid #000000;

        }



        .input_width {

            width: 100%;

        }



        .td_color_1 {

            background-color: #F4F4F4;

        }



        .td_color_2 {

            background-color: #E5E5E5;

        }



        .td_color_3 {

            background-color: #B6B6B6;

        }



        .container_inscription_pro {

            background-color: #ffffff;

            display: table;

            margin-top: 0 !important;

        }



       /* @media screen and (max-width: 694px){

        .container_inscription_pro{

            width: 100%!important;

        }

        }*/

        /*.titre_type{*/
            /*font-family:Libre-Baskerville,Sans-serif!important;*/
            /*font-size:40px;*/
            /*color: #000000;*/
            /*font-style:italic;*/
        /*}*/
        /*.titre{*/
            /*font-size:20px;*/
            /*font-family: Futura-LT-Book,Sans-serif!important;*/
            /*font-weight: normal!important;*/
            /*padding-top: 15px;*/
            /*padding-bottom: 15px;*/
            /*height: auto!important;*/
        /*}*/
        /*.btn_retour{*/
            /*font-family:Libre-Baskerville,Sans-serif!important;*/
            /*font-size:20px;*/
            /*color: #000000;*/
            /*border-radius: 20px!important;*/
            /*border: solid rgba(50, 50, 50, 1) 2px!important;*/
            /*background: #ffffff;*/
        /*}*/
        /*.btn_retour:hover{*/
            /*color: #ffffff;*/
            /*background: rgba(50, 50, 50, 1);*/
        /*}*/



    </style>



<!--    <div style="text-align:center;">-->
<!---->
<!--        <img src="--><?php //echo GetImagePath("privicarte/"); ?><!--/abo_prem.png" alt="inscription" style="text-align:center">-->
<!---->
<!--    </div>-->
<!---->
<!---->
<!---->
<!--    <div style="text-align:center;">-->
<!---->
<!--        <a href=" --><?php //echo base_url(); ?><!--" class="btn btn-primary">Retour sur Sortez</a>-->
<!---->
<!--    </div>-->
<!---->
<!---->
<!---->
<!--    <div style="text-align:center;">-->
<!---->
<!--        <h1 style="font-family: futura_bold; font-size: 32px !important; text-transform: uppercase; color: #dc1a95; font-weight: normal; margin-top: 20px !important;">-->
<!---->
<!--            JE SOUSCRIS<br/>À UN ABONNEMENT --><?php //if (isset($type) AND $type=="platinum"){ ?><!--PREMIUM--><?php //}else{ ?><!--PREMIUM--><?php //} ?><!--</h1>-->
<!---->
<!--    </div>-->

    <div class="container" style="max-width: 785px">
<!--        <div class="row">-->
<!--            <div class="col-lg-4 offset-lg-4 text-center pr-0">-->
<!--                <a href="--><?php //echo site_url(); ?><!--">-->
<!--                    <div class="btn btn_retour w-100">Retour sortez</div>-->
<!--                </a>-->
<!--            </div>-->
<!--        </div>-->
        <div class="row mt-5 mb-5" style="box-shadow: 0px 22px 17px -20px rgba(0,0,0,0.2)">
            <div class="col-lg-7" style="padding-top: 7rem!important;">
                <!-- <div class="row">
                    <p class="titre_type text-center" style="margin: auto">JE SOUSCRIS<br>
                        UN ABONNEMENT<br>PREMIUM
                    </p>
                </div> -->
                <div id="comp-k9d3lo0t" class="_2Hij5" data-testid="richTextElement">
                    <h2 class="font_2" style="font-size:35px; line-height:normal; text-align:center;">
                        <span style="letter-spacing:0em;">
                            <span style="font-size:35px;">
                                <span style="font-style:italic;"><span style="font-family:libre baskerville,serif;">
                                    <span style="color:#221133;">&nbsp;Souscription</span></span></span></span><span style="font-size:35px;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;"><span style="color:#221133;"> pour<br>un abonnement Basique</span></span></span></span></span>
                    </h2>
                </div>
                <div id="comp-k9dwsclh" class="XUUsC" title=""><div data-testid="linkElement" class="xQ_iF"><wix-image id="img_comp-k9dwsclh" class="_1-6YJ _1Fe8-" data-image-info="{&quot;containerId&quot;:&quot;comp-k9dwsclh&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:286,&quot;height&quot;:286,&quot;uri&quot;:&quot;5fa146_0094fd1bdc98456790edfdd9cc211ad4~mv2.png&quot;,&quot;name&quot;:&quot;flyer11.png&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}" data-bg-effect-name="" data-is-svg="false" data-is-svg-mask="false" data-image-zoomed="" data-has-ssr-src="" data-src="https://static.wixstatic.com/media/5fa146_0094fd1bdc98456790edfdd9cc211ad4~mv2.png/v1/fill/w_166,h_166,al_c,usm_0.66_1.00_0.01,enc_auto/flyer11.png"><img src="https://static.wixstatic.com/media/5fa146_0094fd1bdc98456790edfdd9cc211ad4~mv2.png/v1/fill/w_166,h_166,al_c,usm_0.66_1.00_0.01,enc_auto/flyer11.png" alt="flyer11.png" style="width:166px;height:166px;object-fit:cover;object-position:50% 50%"></wix-image>
                </div>
        </div>

        </div>
        <div class="col-lg-5 p-0">
                <img class="img-fluid" style="padding-top: 0rem!important;width: 100%;height: auto;" src="<?php echo base_url("/assets/images/ordi-woman1.webp")?>">
            </div>
        </div>
    </div>

<div class="container_inscription_pro">

    <div id="contect_all_data_pro_subscription" class="contect_all_data_pro_subscription">

    <div id="comp-l0efuohk" class="_2Hij5" data-testid="richTextElement"><h2 class="font_2" style="font-size:25px; line-height:normal; text-align:center;"><span style="font-size:25px;"><span style="letter-spacing:0em;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;"><span style="color:#221133;">Les coordonnées de votre établissement</span></span></span></span></span></h2></div>



<form name="frmInscriptionProfessionnel" id="frmInscriptionProfessionnel"

              action="<?php echo site_url("front/professionnels/ajouter"); ?>" method="POST" accept-charset="UTF-8"

              target="_self" enctype="multipart/form-data" style="margin:0px;">

              <div class="row">
  <form class="form-horizontal">
    <div class="span6">

      <div class="control-group">
        <label class="control-label" for="input01"></label>
        <div class="controls">
          <input type="text" class="input-xlarge" id="input01" name="nom-ou enseigne*" id="input_comp-l0efuohp" class="_1SOvY has-custom-focus" value="" placeholder="Nom ou enseigne*" required="" maxlength="100" aria-label="Nom ou enseigne*">
         
        </div>
      </div>
        <div class="control-group">
          <label class="control-label" for="input01"></label>
          <div class="controls">
            <input type="text" class="input-xlarge" id="input01" name="votre-activité*" id="input_comp-l0efuoi8" class="_1SOvY has-custom-focus" value="" placeholder="Votre activité*" required="" aria-label="Votre activité*">
           
          </div>
        </div>
        <div class="control-group">
          <label class="control-label" for="input01"></label>
          <div class="controls">
            <input type="text" class="input-xlarge" id="input01" name="code-postal*" id="input_comp-l0efuoip" class="_1SOvY has-custom-focus" value="" placeholder="Code postal*" required="" aria-label="Code postal*">
            <p class="help-block"></p>
          </div>
        </div> 
        <div class="control-group">
          <label class="control-label" for="input01"></label>
          <div class="controls">
            <input type="text" class="input-xlarge" id="input01" name="courriel*" id="input_comp-l0efuoj0" class="_1SOvY has-custom-focus" value="" placeholder="Courriel*" required="" aria-label="Courriel*">
            <p class="help-block"></p>
          </div>
        </div>   
        <div class="control-group">
          <label class="control-label" for="input01"></label>
          <div class="controls">
            <input type="text" class="input-xlarge" id="input01" name="siret" id="input_comp-l0egu04x" class="_1SOvY has-custom-focus" value="" placeholder="SIRET" required="" aria-label="SIRET">
            <p class="help-block"></p>
          </div>
        </div>           

    </div>
    <div class="span6">

            <div class="control-group">
                <label class="control-label" for="input01"></label>
                    <div class="controls">
                        <!-- <input type="text" class="input-xlarge" id="input01"> -->
                        <select name="AssCommercantRubrique[IdRubrique]" id="RubriqueSociete" tabindex="1"

                                        class="input_width form-control" id="input01">

                                    <option value="">Votre statut</option>

                                    <?php if (sizeof($colRubriques)) { ?>

                                        <?php foreach ($colRubriques as $objRubrique) { ?>

                                            <option

                                                    value="<?php echo $objRubrique->IdRubrique; ?>"><?php echo $objRubrique->Nom; ?></option>

                                        <?php } ?>

                                    <?php } ?>

                         </select>
                       
                    </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="input01"></label>
                    <div class="controls">
                        <input type="text" class="input-xlarge" id="input01" name="adresse*-" id="input_comp-l0efuok5" class="_1SOvY has-custom-focus" value="" placeholder="Adresse* " required="" aria-label="Adresse* ">
                        
                    </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="input01"></label>
                <div class="controls">
                        <!-- <input type="text" class="input-xlarge" id="input01"> -->
                        <select name="AssCommercantRubrique[IdRubrique]" id="RubriqueSociete" tabindex="1"

                                        class="input_width form-control" id="input01">

                                    <option value="">Nombre d'employés</option>

                                    <?php if (sizeof($colRubriques)) { ?>

                                        <?php foreach ($colRubriques as $objRubrique) { ?>

                                            <option

                                                    value="<?php echo $objRubrique->IdRubrique; ?>"><?php echo $objRubrique->Nom; ?></option>

                                        <?php } ?>

                                    <?php } ?>

                         </select>
                        <p class="help-block"></p>
                    </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="input01"></label>
                    <div class="controls">
                        <input type="text" class="input-xlarge" id="input01" name="site-web*" id="input_comp-l0efuokp" class="_1SOvY has-custom-focus" value="" placeholder="Site web*" required="" aria-label="Site web*">
                        <p class="help-block"></p>
                    </div>
            </div>
            <label class="control-label" for="input01"></label>
            <div class="controls">
                        <input type="text" class="input-xlarge" id="input01" name="ville*" id="input_comp-l0efuokf" class="_1SOvY has-custom-focus" value="" placeholder="Ville*" required="" aria-label="Ville*">
                        <p class="help-block"></p>
                    </div>
            </div>
    </div>
  </form>
  <div id="comp-l0efuolx" class="_1giiM">
    <div class="_19Et-"></div>
</div>
<div id="comp-l0efuom2" class="_2Hij5" data-testid="richTextElement"><h2 class="font_2" style="font-size:25px; line-height:normal; text-align:center;"><span style="font-size:25px;"><span style="letter-spacing:0em;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;"><span style="color:#221133;">Les coordonnées du décideur</span></span></span></span></span></h2></div>
<form name="frmInscriptionProfessionnel" id="frmInscriptionProfessionnel"

              action="<?php echo site_url("front/professionnels/ajouter"); ?>" method="POST" accept-charset="UTF-8"

              target="_self" enctype="multipart/form-data" style="margin:0px;">
<div class="row">
  <form class="form-horizontal">
    <div class="span6">

      <div class="control-group">
        <label class="control-label" for="input01"></label>
        <div class="controls">
          <input type="text" class="input-xlarge" id="input01" name="nom-ou enseigne*" id="input_comp-l0efuohp" class="_1SOvY has-custom-focus" value="" placeholder="Nom ou enseigne*" required="" maxlength="100" aria-label="Nom ou enseigne*">
         
        </div>
      </div>
        <div class="control-group">
          <label class="control-label" for="input01"></label>
          <div class="controls">
            <input type="text" class="input-xlarge" id="input01" name="votre-activité*" id="input_comp-l0efuoi8" class="_1SOvY has-custom-focus" value="" placeholder="Votre activité*" required="" aria-label="Votre activité*">
           
          </div>
        </div>           
    </div>
    <div class="span6">
            <div class="control-group">
                <label class="control-label" for="input01"></label>
                    <div class="controls">
                        <input type="text" class="input-xlarge" id="input01" name="adresse*-" id="input_comp-l0efuok5" class="_1SOvY has-custom-focus" value="" placeholder="Adresse* " required="" aria-label="Adresse* ">
                        
                    </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="input01"></label>
                    <div class="controls">
                        <input type="text" class="input-xlarge" id="input01" name="ville*" id="input_comp-l0efuokf" class="_1SOvY has-custom-focus" value="" placeholder="Ville*" required="" aria-label="Ville*">
                        <p class="help-block"></p>
                    </div>
            </div>
            
            
     </div>
    </div>
</form>
<div id="comp-l0eg032m">
    <div id="comp-l029myfg" class="_2Hij5" data-testid="richTextElement"><h2 class="font_2" style="font-size:25px; line-height:normal; text-align:center;"><span style="color:#FFFFFF;"><span style="font-size:25px;"><span style="letter-spacing:0em;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;">Identifiant et mot de passe</span></span></span></span></span></h2>
    </div>
    <form name="frmInscriptionProfessionnel" id="frmInscriptionProfessionnel" action="<?php echo site_url("front/professionnels/ajouter"); ?>" method="POST" accept-charset="UTF-8" target="_self" enctype="multipart/form-data" style="margin:0px;">
    <div class="row" id="user_id">
            <div class="span6">
                <div class="control-group">
                     <label class="control-label" for="input01"></label>
                        <div class="controls">
                            <input type="text" class="input-xlarge" id="input01" name="identifiant" id="input_comp-l0efynm0" class="_1SOvY has-custom-focus" value="" placeholder="Identifiant" required="" aria-label="Identifiant">
         
                        </div>
                 </div>          
            </div>
            <div class="span6">
                <div class="control-group">
                    <label class="control-label" for="input01"></label>
                        <div class="controls">
                            <input type="text" class="input-xlarge" id="input01" name="mdp" id="input_comp-l0efzeh8" class="_1SOvY has-custom-focus" value="" placeholder="MDP" required="" aria-label="MDP">
                        
                        </div>
                </div>            
            </div>
    </div>
</form>
    <div> 
        <form name="frmInscriptionProfessionnel" id="frmInscriptionProfessionnel" action="<?php echo site_url("front/professionnels/ajouter"); ?>" method="POST" accept-charset="UTF-8" target="_self" enctype="multipart/form-data" style="margin:0px;">
                    <div class="controls" id="form_id_modf">
                         <input type="text" class="modif_id" id="input01" name="modif_id" value="" placeholder="Modifier votre identifiant et mot de pass" required="" aria-label="Modifier votre identifiant et mot de pass">
                        
                    </div>                             
        </form>  
    </div>

    </div>
    <div id="comp-k9dw6iep" class="_2Hij5" data-testid="richTextElement"><h2 class="font_2" style="font-size:25px; line-height:normal; text-align:center;"><span style="font-size:25px;"><span style="letter-spacing:0em;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;"><span style="color:#221133;">Vos données</span></span></span></span></span></h2>
    </div>
        <div id="comp-l0egjtbl" class="bItEI gPrpT"><label for="textarea_comp-l0egjtbl" class="_20uhs"></label><textarea id="textarea_comp-l0egjtbl" class="_1VWbH has-custom-focus" placeholder="Ajouter votre texte de présentation ici !" tabindex="0"></textarea>
        </div>

        <div id="comp-l0egniff" class="bItEI"><label for="textarea_comp-l0egniff" class="_20uhs"></label><textarea id="textarea_comp-l0egniff" class="_1VWbH has-custom-focus" placeholder="Ajouter vos jours et heures d'ouverture" tabindex="0"></textarea>
        </div>
        <div id="comp-l0egs3no" class="XUUsC" title=""><div data-testid="linkElement" class="xQ_iF"><wix-image id="img_comp-l0egs3no" class="_1-6YJ _1Fe8-" data-image-info="{&quot;containerId&quot;:&quot;comp-l0egs3no&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:512,&quot;height&quot;:512,&quot;uri&quot;:&quot;5fa146_cedb54778af544b7ad9df3c7249939b2~mv2.png&quot;,&quot;name&quot;:&quot;4326320.png&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}" data-bg-effect-name="" data-is-svg="false" data-is-svg-mask="false" data-image-zoomed="" data-has-ssr-src="" data-src="https://static.wixstatic.com/media/5fa146_cedb54778af544b7ad9df3c7249939b2~mv2.png/v1/fill/w_198,h_198,al_c,usm_0.66_1.00_0.01,enc_auto/4326320.png"><img src="https://static.wixstatic.com/media/5fa146_cedb54778af544b7ad9df3c7249939b2~mv2.png/v1/fill/w_198,h_198,al_c,usm_0.66_1.00_0.01,enc_auto/4326320.png" alt="4326320.png" style="width:198px;height:198px;object-fit:cover;object-position:50% 50%"></wix-image></div>
    </div>
    <div id="comp-l0egpoz7" class="_2dBhC _3TyBu"><div class="XRJUI"><input type="text" name="préciser-le lien de votre page doctolib..." id="input_comp-l0egpoz7" class="_1SOvY has-custom-focus" value="" placeholder="Préciser le lien de votre page doctolib..." aria-label="Préciser le lien de votre page doctolib..." tabindex="0"></div>
    </div>
    <div id="comp-l029tfee" class="_2Hij5" data-testid="richTextElement"><h2 class="font_2" style="font-size:25px; line-height:normal; text-align:center;"><span style="font-size:25px;"><span style="letter-spacing:0em;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;"><span style="color:#221133;">Votre souscription à l'abonnement basique</span></span></span></span></span></h2></div>

    <div id="comp-k9dvbs6j" class="_2Hij5" data-testid="richTextElement"><ol class="font_8" style="color:rgb(0, 0, 0); font-family:futura-lt-w01-book,sans-serif; font-size:17px;">
<li style="line-height:normal;">
<p class="font_8" style="font-size:17px; line-height:normal;"><span style="font-size:17px;"><span style="letter-spacing:normal;"><span style="font-family:futura-lt-w01-book,sans-serif;"><span style="color:#000000;">Vous complétez l'ensemble des champs ;</span></span></span></span></p>
</li>
<li style="line-height:normal;">
<p class="font_8" style="font-size:17px; line-height:normal;"><span style="font-size:17px;"><span style="letter-spacing:normal;"><span style="font-family:futura-lt-w01-book,sans-serif;"><span style="color:#000000;">dès validation et acceptation de votre demande, votre page basique sera créée ;</span></span></span></span></p>
</li>
<li style="line-height:normal;">
<p class="font_8" style="font-size:17px; line-height:normal;"><span style="font-size:17px;"><span style="letter-spacing:normal;"><span style="font-family:futura-lt-w01-book,sans-serif;"><span style="color:#000000;">par la suite, si vous désirez modifier vos données, il vous suffira de vous connecter sur votre compte en cliquant sur le bouton "Mon compte" présent sur les bannières des pages de notre site et en précisant votre identifiant et mot de passe pour accéder à votre formulaire intuitif.</span></span></span></span></p>
</li>
</ol>

<p class="font_8" style="font-size:17px; line-height:normal;"><span style="font-size:17px;"><span class="wixGuard">&ZeroWidthSpace;</span></span></p></div>

<div id="comp-l02ana8y" class="_2Hij5" data-testid="richTextElement"><p class="font_7" style="font-size:17px;"><span style="font-size:17px;"><span style="font-style:normal;"><span style="font-weight:400;"><span style="letter-spacing:normal;"><span style="font-family:futura-lt-w01-book,sans-serif;"><span style="color:#000000;">Attention : notre service peut refuser sans en donner la justification l’ouverture d’un compte qui ne correspondrait pas à son éthique. <a href="https://www.sortez.org/mentions-legales.html" target="_blank" rel="noreferrer noopener" tabindex="0"><span style="font-style:normal;"><span style="font-weight:400;"><span style="letter-spacing:normal;"><span style="font-family:futura-lt-w01-book,sans-serif;"><span style="color:#000000;"><span style="text-decoration:underline;">(</span></span></span></span></span></span><span style="font-style:normal;"><span style="font-weight:400;"><span style="letter-spacing:normal;"><span style="font-family:futura-lt-w01-book,sans-serif;"><span style="color:#000000;"><span style="text-decoration:underline;">Voir nos conditions générales</span></span></span></span></span></span><span style="font-style:normal;"><span style="font-weight:400;"><span style="letter-spacing:normal;"><span style="font-family:futura-lt-w01-book,sans-serif;"><span style="color:#000000;"><span style="text-decoration:underline;">)</span></span></span></span></span></span></a> ;</span></span></span></span></span></span></p></div>

    <div id="calendrier">
    <div id="comp-l0egpoz7" class="_2dBhC _3TyBu"><div class="XRJUI"><input type="date" name="date" id="input_comp-l0egpoz7" class="_1SOvY has-custom-focus" value="" placeholder="Sélectionner"></div>
    </div>
       <div>
                <!-- <input type="checkbox" class="_3ESMu" value="checked" required="" tabindex="0"><span data-testid="text" class="_1Avq3">Je confirme ma demande de souscription gratuite à un abonnement basique</span> -->
                <input type="checkbox"><label style="font-size:20px; line-height:normal; text-align:center;" class="_1Avq3"> Je confirme ma demande de souscription gratuite à un abonnement basique</label>
        </div>
        <div>
        <input type="checkbox"><label style="font-size:20px; line-height:normal; text-align:center;" class="_1Avq3"> Je confirme ma demande de souscription gratuite à un abonnement basique</label>
            <!-- <input type="checkbox" class="_3ESMu" value="checked" required="" tabindex="0"><span data-testid="text" class="_1Avq3">Le soussigné déclare avoir la faculté d’engager en son nom sa structure dont les coordonnées sont précisées ci- dessus.</span> -->
        </div>
</div>

            <!-- <div class="bloc_sub_pro">

                <div class="title_sub_pro">Votre activité</div>

                <div class="table_sub_pro">

                    <table width="100%" border="0" cellspacing="0" cellpadding="0">

                        <tr>

                            <td width="202px">Préciser votre activité *</td>

                            <td>

                                <select name="AssCommercantRubrique[IdRubrique]" id="RubriqueSociete" tabindex="1"

                                        class="input_width form-control">

                                    <option value="">-- Veuillez choisir --</option>

                                    <?php if (sizeof($colRubriques)) { ?>

                                        <?php foreach ($colRubriques as $objRubrique) { ?>

                                            <option

                                                    value="<?php echo $objRubrique->IdRubrique; ?>"><?php echo $objRubrique->Nom; ?></option>

                                        <?php } ?>

                                    <?php } ?>

                                </select>

                            </td>

                        </tr>

                        <tr id='trReponseRub' style="height:auto;">



                        </tr>

                        <tr>

                            <td>Statut</td>

                            <td>

                                <select name="Societe[idstatut]" size="1" class="input_width form-control" tabindex="3">

                                    <option selected="selected" value="0">Choisir&nbsp;votre&nbsp;statut</option>

                                    <?php if (sizeof($colStatut)) { ?>

                                        <?php foreach ($colStatut as $objStatut) { ?>

                                            <option value="<?php echo $objStatut->id; ?>"><?php echo stripcslashes($objStatut->Nom); ?></option>

                                        <?php } ?>

                                    <?php } ?>

                                </select>

                            </td>

                        </tr>

                        <tr>

                            <td>Autre, préciser</td>

                            <td>

                                <input name="Autre" class="input_width form-control" type="text" tabindex="4">

                            </td>

                        </tr>

                        <tr>

                            <td>Nom ou enseigne *</td>

                            <td>

                                <input type="text" name="Societe[NomSociete]" id="NomSociete" value=""

                                       class="input_width form-control" tabindex="5"/>

                            </td>

                        </tr>

                        <tr>

                            <td>Adresse 1</td>

                            <td>

                                <input type="text" name="Societe[Adresse1]" id="Adresse1Societe" value=""

                                       class="input_width form-control" tabindex="6"/>

                            </td>

                            <input type="hidden" name="Societe[limit_article]" id="ar" value="5" class="" />

                            <input type="hidden" name="Societe[limit_agenda]" id="ag" value="5" class="" />

                            <input type="hidden" name="Societe[limit_annonce]" id="an" value="5" class="" />

                        </tr>

                        <tr>

                            <td>Adresse 2</td>

                            <td>

                                <input type="text" name="Societe[Adresse2]" id="Adresse2Societe" value=""

                                       class="input_width form-control" tabindex="7"/>

                            </td>

                        </tr>





                        <tr>

                            <td>Code Postal *</td>

                            <td>

                                <input type="text" name="Societe[CodePostal]" id="CodePostalSociete" value=""

                                       class="input_width form-control" tabindex="9"

                                       onblur="javascript:CP_getDepartement();CP_getVille();"/>

                            </td>

                        </tr>



                        <tr>

                            <td>Departement :</td>

                            <td>

    	<span id="departementCP_container">

        <select name="Societe[departement_id]" id="departement_id" disabled="disabled" class="input_width form-control"

                onchange="javascript:CP_getVille_D_CP();">

            <option value="0">-- Choisir --</option>

            <?php if (sizeof($colDepartement)) { ?>

                <?php foreach ($colDepartement as $objDepartement) { ?>

                    <option

                            value="<?php echo $objDepartement->departement_id; ?>"><?php echo $objDepartement->departement_nom; ?></option>

                <?php } ?>

            <?php } ?>

        </select>

        </span>

                            </td>

                        </tr>



                        <tr>

                            <td>Ville *</td>

                            <td>

    	<span id="villeCP_container">

        <input type="text" value="" name="IdVille_Nom_text" id="IdVille_Nom_text" disabled="disabled"

               class="input_width form-control"/>

        <input type="hidden" value="" name="Societe[IdVille]" id="VilleSociete"/>

        </span>

                            </td>

                        </tr>





                        <tr>

                            <td>Téléphone direct</td>

                            <td>

                                <input type="text" name="Societe[TelFixe]" id="TelFixeSociete" value=""

                                       class="input_width form-control" tabindex="10"/>

                            </td>

                        </tr>
                        <tr>

                            <td>Téléphone mobile</td>

                            <td><input type="text" name="Societe[TelMobile]" id="TelMobileSociete" value=""

                                       class="input_width form-control" tabindex="11"/></td>

                        </tr>

                        <tr>

                            <td>Email *</td>

                            <td>

                                <input type="text" name="Societe[Email]" id="EmailSociete" value=""

                                       class="input_width form-control"

                                       tabindex="12"/>



                                <div id="divErrortxtEmail_"

                                     style="width:152px; height:20px; overflow:hidden; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;"></div>

                                <input type="hidden" name="txtEmail_verif" id="txtEmail_verif" value="0"

                                       class="input_width" tabindex="18"/>

                            </td>

                        </tr>

                    </table>

                </div>

            </div>

            <div class="space_sub_pro">&nbsp;</div>





            <div class="bloc_sub_pro">

                <div class="title_sub_pro">Les coordonnées du décideur</div>

                <div class="table_sub_pro">

                    <table width="100%" border="0" cellspacing="0" cellpadding="0">

                        <tr>

                            <td colspan="2">

                                Le soussigné déclare avoir la faculté

                                d’engager en son nom sa structure (commerce, entreprise, collectivité…)

                                dont les coordonnées sont précisées ci-

                                <wbr>

                                dessus.

                            </td>

                        </tr>

                        <tr>

                            <td width="202px" s>Civilité *</td>

                            <td>

                                <select name="Societe[Civilite]" id="CiviliteResponsableSociete"

                                        class="input_width form-control"

                                        tabindex="13">

                                    <option value="0">Monsieur</option>

                                    <option value="1">Madame</option>

                                    <option value="2">Mademoiselle</option>

                                </select>

                            </td>

                        </tr>

                        <tr>

                            <td>Nom responsable *</td>

                            <td><input type="text" name="Societe[Nom]" id="NomResponsableSociete" value=""

                                       class="input_width form-control" tabindex="14"/></td>

                        </tr>

                        <tr>

                            <td>Prénom responsable *</td>

                            <td><input type="text" name="Societe[Prenom]" id="PrenomResponsableSociete" value=""

                                       class="input_width form-control" tabindex="15"/></td>

                        </tr>

                        <tr>

                            <td>Fonction responsable *</td>

                            <td><input type="text" name="Societe[Responsabilite]" id="ResponsabiliteResponsableSociete"

                                       value="" class="input_width form-control" tabindex="16"/></td>

                        </tr>

                        <tr>

                            <td>Téléphone direct *</td>

                            <td><input type="text" name="Societe[TelDirect]" id="TelDirectResponsableSociete" value=""

                                       class="input_width form-control" tabindex="17"/></td>

                        </tr>
                        <tr>

                            <td>Email *</td>

                            <td><input type="text" name="Societe[Email_decideur]" id="Email_decideurResponsableSociete"

                                       value="" class="input_width form-control" tabindex="18"/></td>

                        </tr>

                    </table>

                </div>

            </div>

            <div class="space_sub_pro">&nbsp;</div>





            <div class="bloc_sub_pro">

                <div class="title_sub_pro">Votre identifiant et mot de passe</div>

                <div class="table_sub_pro">

                    <table width="100%" border="0" cellspacing="0" cellpadding="0">

                        <tr>

                            <td width="202px">Identifiant *</td>

                            <td>

                                <input type="text" name="Societe[Login]" id="txtLogin" value=""

                                       class="input_width form-control"

                                       tabindex="20"/>



                                <div id="divErrortxtLogin_"

                                     style="width:152px; overflow:hidden; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;"></div>

                                <div id="inputMontantTTC">

                                    <input type="hidden" name="montantttcvalue_abonnement"

                                           id="montantttcvalue_abonnement"/>

                                    <input type="hidden" name="txtLogin_verif" id="txtLogin_verif" value="0"/>

                                </div>

                            </td>

                        </tr>

                        <tr>

                            <td>Mot de passe *</td>

                            <td><input type="password" name="Societe_Password" id="txtPassword" value=""

                                       class="input_width form-control" tabindex="21"/></td>

                        </tr>

                        <tr>

                            <td>Confirmation du mot de passe</td>

                            <td><input type="password" id="txtConfirmPassword" value="" class="input_width form-control"

                                       tabindex="22"/></td>

                        </tr>

                    </table>

                </div>

            </div>

            <div class="space_sub_pro">&nbsp;</div>



            <?php

            $this->load->Model("Abonnement");

            $obj_abonnement_gratuit = $this->Abonnement->GetWhere(" type='gratuit' ");

            $obj_abonnement_premium = $this->Abonnement->GetWhere(" type='premium' ");

            $obj_abonnement_platinum = $this->Abonnement->GetWhere(" type='platinum' ");



            if (isset($obj_abonnement_gratuit) && $type == "basic") $value_abonnement_sub_pro = $obj_abonnement_gratuit->IdAbonnement;

            else if (isset($obj_abonnement_premium) && $type == "premium") $value_abonnement_sub_pro = $obj_abonnement_premium->IdAbonnement;

            else if (isset($obj_abonnement_platinum) && ($type == "platinium" || $type == "platinum")) $value_abonnement_sub_pro = $obj_abonnement_platinum->IdAbonnement;

            else $value_abonnement_sub_pro = '1';

            ?>

            <input type="hidden" name="AssAbonnementCommercant[IdAbonnement]" id="AbonnementSociete"

                   value="<?php echo $value_abonnement_sub_pro; ?>"/>





            <div class="title_sub_pro"></div>

            <?php $this->load->view("privicarte/txt_mentions", $data); ?>

            <div class="space_sub_pro">&nbsp;</div>

            <div style="margin:15px; padding:15px; font-weight:bold;">



                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                    <tr>

                        <td valign="top" style="width: 40px;"><input name="validationabonnement"

                                                                     id="validationabonnement" value="1"

                                                                     type="checkbox" tabindex="24"></td>

                        <td>

                            Je confirme* ma demande d'inscription et la validation des conditions générales. (<a

                                    href="/mentions-legales.html" target="_blank"

                                    style="font-weight:normal;">cliquez ici pour consulter les conditions générales</a>)

                        </td>

                    </tr>

                </table>

            </div>









            <div class="FieldError" id="divErrorFrmInscriptionProfessionnel"

                 style="height: auto; color: red; font-family: arial; font-size: 12px; text-align:center;"></div> -->




        


        </form>



        <div style="text-align: center;" id="capt" class="g-recaptcha" data-sitekey="6Lfjgm0UAAAAAOl1qieKqiWV5gZuSjjAc19jUIRg"></div>



        <div style="text-align:center; margin-bottom:40px; margin-top:60px;">

            <input id="btnSinscrire" style="width: 227px;" name="envoyer"

                   value="Je valide mon inscription" type="button" tabindex="26" class="btn btn-success">

        </div>


        <div id="comp-l0efuouw" aria-disabled="false" class="_2UgQw"><span class="_1Qjd7">SOUSCRIPTION A UN ABONNEMENT BASIQUE</span></div>
        <div id="comp-l0efl1k8" class="_2Hij5" data-testid="richTextElement"><h2 class="font_2" style="font-size:25px; line-height:normal; text-align:center;"><span style="font-size:25px;"><span style="letter-spacing:0em;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;"><span style="color:#221133;">Découvrez nous deux autres abonnements </span></span></span></span></span></h2></div>
    </div>

    <!-- <div class="row"> -->
    <div style="display: flex; margin: auto;" id="three-blocks" class="row">
    <div class="col-lg-6">
        <!-- <div style="width: 100%;">
            <p class="font_2" style="font-size:128px; line-height:normal; margin-top:-11%; text-align:center;">
                <span class="color_24">
                    <span style="font-size:128px; color:#3453A2;">
                        <span style="font-family:helvetica-w01-bold,helvetica-w02-bold,helvetica-lt-w10-bold,sans-serif;">
                            <span style="letter-spacing:0em;">
                                <span style="font-weight:bold;" class="number">02</span>
                            </span>
                        </span>
                    </span>
                </span>
            </p>
        </div> -->
        <div style="background-color: #3453A2; margin: 5px; padding: 30px;   margin-top: 8%;  height: 94%;">
            <div style="height: 280px;margin-top:-4%;">
                <span style="font-size:22px; color:#FFFFFF;" class="title-bloc">
                    <span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;">L'abonnement Premium</span>
                </span>

                <h4 class="font_4" style="font-size:14px; line-height:normal; text-align:center; color:#FFFFFF;">
                    <span style="font-size:14px;">
                        <span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;">L'ouverture à des outils Marketings innovants</span>
                    </span>
                </h4>
                <img class="img_head" src="<?php echo site_url('/application/views/front_soutenons/premium_ordi.png'); ?>" alt="" />
                <!-- <img class="gratuit" src="</*?php echo site_url('/application/views/front_soutenons/gratuit2.webp');?>" alt="" /> -->
            </div>
            <div style="text-align: center; color: aliceblue; margin-top:-17%;  ">
                <p>
                <p>En souscrivant à un abonnement Premium, <br> vous bénéficiez d'une page informative<br> évolutive enrichie de textes, de liens, <br> avec l'intégration de vidéos, d'images</p> et de documents complémentaires, d'un URL</p> personnalisé (www.monentreprise.sortez.org)</p> et la fourniture d'un QRCODE</p> pour l'accès à la mobilité...</p>
                <p> Cet abonnement et la création de cette page<br> est obligatoirement lié<br> à la souscription à un ou plusieurs outils<br> Marketing innovants : e-commerce, deals,<br> de fidélité et bien plus encore... </p>
                <p>Cette page et les outils associés sont<br>multilingues et s'adaptent à tous les écrans <br>(ordinateurs, tablettes, mobiles...).</p>
                <p>Les tarifs varient suivant l'importance<br>des établissements... </p>
                </p>
                <div style="display: flex; justify-content: center;">
                    <a href="https://www.soutenonslecommercelocal.fr/abonnement-premium" class="btngreen" style="text-align: center;   height: 100%;background-color: #EDD23B;width: 50%;    margin-top: 44%;">​Détails</a>
                </div>
                <div style="display: flex; justify-content: center;">
                    <a href="<?php echo site_url('/mon-etablissement-modele/presentation_commercants') ?>" class="btngreen" style="text-align: center;   height: 100%;background-color: #EDD23B;width: 50%;    margin-top: 1%;"> ​Modèle</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <!-- <div style="width: 100%;">
            <p class="font_2" style="font-size:128px; line-height:normal; margin-top:-11%; text-align:center;">
                <span class="color_24">
                    <span style="font-size:128px; color:#E80EAE;">
                        <span style="font-family:helvetica-w01-bold,helvetica-w02-bold,helvetica-lt-w10-bold,sans-serif;">
                            <span style="letter-spacing:0em;">
                                <span style="font-weight:bold;" class="number">03</span>
                            </span>
                        </span>
                    </span>
                </span>
            </p>
        </div> -->
        <div style="background-color: #E80EAE; margin: 5px; padding: 30px;  margin-top: 8%;   height: 94%;">
            <div style="height: 280px; margin-top:-4%;">
                <span style="font-size:22px; color:#FFFFFF;" class="title-bloc">
                    <span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;">L'abonnement Platinium</span>
                </span>

                <h4 class="font_4" style="font-size:14px; line-height:normal; text-align:center; color:#FFFFFF;">
                    <span style="font-size:14px;">
                        <span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;">Un site web professionnel complet</span>
                    </span>
                </h4>
                <img class="img_head" src="<?php echo site_url('/application/views/front_soutenons/platinium_ordi.png'); ?>" alt="" />
                <!--<img class="gratuit" src="</*?php echo site_url('/application/views/front_soutenons/gratuit2.webp');?>" alt="" /> -->
            </div>
            <div style="text-align: center; color: aliceblue; margin-top:-17%; ">
                <p>
                <p>Si vous n'avez pas encore de site web, <br> l'abonnement Platinium est fait pour vous.<br>
                <p> Il vous permet d'enrichir graphiquement votre<br> site Premium : logo, bannière, fond de page,<br> couleurs boutons et textes, présence de 2 <br>pages complémentaires.</p>
                <p>Des fonctions complémentaires sont <br>en choisissant un ou plusieurs modules<br>« livre d’or », les commentaires « Google »... <br> </p>
                <p>Vous pouvez enrichir cet abonnement<br> sera automatiquement actualisée <br>marketing innovants : e-commerce, deals,<br>fidélisation, clic & collect, réservations,<br>permet de booster votre référencement<br>sur Google et de bénéficier d'un URL<br>personnalisé (www.monentreprise.fr). </p>
                <p>Cette page et les outils sont multilingues <br>et s'adaptent à tous les écrans (ordinateurs, <br>tablettes, mobiles...</p>
                <p> Les tarifs varient suivant l'importance<br>des établissements</p>
                </p>
                <div style="display: flex; justify-content: center;">
                    <a href="https://www.soutenonslecommercelocal.fr/abonnement-platinium" class="btngreen" style="text-align: center;  height: 100%;background-color: #EDD23B;width: 50%;    margin-top: 33%;">​Détails</a>
                </div>
                <div style="display: flex; justify-content: center;">
                    <a href=" https://www.soutenonslecommercelocal.fr/page-platinium" class="btngreen" style="text-align: center;  height: 100%;background-color: #EDD23B;width: 50%;    margin-top: 1%;"> ​Modèle</a>
                </div>
            </div>
        </div>
    </div>          
    </div>
    <div id="comp-l0efuovr" class="_2Hij5" data-testid="richTextElement"><p class="font_8" style="font-size:60px; text-align:center;"><span style="color:#E80EAE;"><span style="font-size:60px;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;">06.72.05.59.35</span></span></span></span></p>

<p class="font_8" style="text-align:center; font-size:15px;"><span style="color:#E80EAE;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="font-size:25px;">PRIVICONCEPT SAS - Le Magazine Sortez</span></span></span><br>
<span style="color:#000000;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;">427, chemin de Vosgelade, le Mas Raoum, 06140 Vence<br>
WWW.MAGAZINE-SORTEZ.ORG - TÉL. 06.72.05.59.35</span></span></p>

<p class="font_8" style="text-align:center; font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;">Siret : 820 043 693 00010 - Code NAF : 6201Z</span></span></p>

<p class="font_8" style="text-align:center; font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span class="wixGuard">&ZeroWidthSpace;&ZeroWidthSpace;&ZeroWidthSpace;&ZeroWidthSpace;&ZeroWidthSpace;</span></span></span></p>

<p class="font_8" style="text-align:center; font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span class="wixGuard">&ZeroWidthSpace;</span></span></span></p>

<p class="font_8" style="text-align:center; font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span class="wixGuard">&ZeroWidthSpace;</span></span></span></p></div>
    <style type="text/css">
         #comp-l0efl1k8 {
            margin-top: 15%;
        } 
        #comp-l0efuouw {
            text-align: center;
        }
        ._1Qjd7 {
            background: #E80EAE;
            color: white;
            padding: 2%;
        }

        #capt div {text-align:center; width: auto !important;}
         input#input01{
             width: 100%;
             height: 37px;
             border-color: #E80EAE;
        }
        .span6{
            width: 40%;
            margin-left: 7%;
        }
        #comp-k9dwsclh {
            text-align: center;
        }
        select.form-control:not([size]):not([multiple]) {
            height: calc(2.25rem + 14px);
            border-color: #E80EAE;
        }
        ._1giiM ._19Et- {
    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAAB0CAYAAAG3V00GAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAADbxJREFUeNq81MFKA0EQBNA3u+6qQQVvfoUf4f9/jBCJMZrtXHpg9LSRZAqaPgx0TXVTVfxGcVlEHVpwg1vcZ58wNu9rBy444hv7rEOJiBF3eMJz9g1mDEm2Bsck+cIO71nbEhH1t2MztJypolVTFVVVS0kVV0XB29VJIuK1B8nQg+SlB4keh++qpFwwWtr1RD38lH7ZZLTMjTnPjZUfHPCZzt9Xp894yEh5TMKpIVqDNrd22GZ9tCsa/nT/jJVWVWA5AQAA///s1ssKwjAUhOH/2KLYgvj+b1lBaihy3ExgrKt42UgXge4GMuTrxA+pfyq7s06O+u4NShquq3ZSxHwBlsjMAzACZ51RgS1BHnADLsAk6q+V+kp890En3sd9o36j/g+pjy/T8kJ9XY+DLci+UeC0R7jYwJuBErJqEPEnsbK3oJZ/iQdMon72pbhbLcd485pyRX0+AAAA///cmEsOwjAMRO2kLZ8lF+Aa3JO7FhHahAWxGI1CxSYIOZI3UdTancjzXKU+9emFoYNFf61pIzLvKTTfsUqxp9iBRAMZaOiQPPI6IsMC0wjGwywrgD9GSHigixxJGe2kTOvrmzOttSCMl2NtNC6V/12llexZHCwtpVy9FHLyUsjopZCLi0JExE0hbq5WC00ChDaMsLfXlA1zZOcvhqbm5pO8f3BaHCumTDQJR9g7wLRsz4iEM62h1dDjXhH2VpEjgYsb6i5wbq7nDH2TiCRWIhKqMJ4wmugP1MjA7yuhig3W+QkAAP//7JrRboJAEEXvLm6RP+ArfO7//1QjrtIXh16G2W1NBHRSk02I0YQrzDDnrMHDeAKDDWqj/B5jfOnq6ONZYdem38YIt0Yg048Zky8fT7dWouJtiUO4eBMVL4/zz+YRXlnxyGCwyPneDK4youggHXWrVnWsaJDjs4NoBhnI/ekgGUCW9hsKt9Vh465l1QSHsqBqAqu/FPpWD8Lfit3kdXkgvlv7LRLiEQ5eflAXG5jsf2Z/MMjJSxA38iF6CeIGdXsvQT69BIGLIJ68Vu8lSPJYI+GFR/qx9h6DlWUZ4w5wNVbwV1PjwjRGQttEjJ6wtIZxxUC1/UPB2wut2R6inHiLn1193tkXCdGoYPKdDvMd4ITl33TkV850YloofJEVuSk2H+gzokpZseZAdiSRNZH1QVdI+621dRBbFLkC5/uSYJdJB8H2vo26pSwrv4VFKZkUdsCToLN0aE37hB261IjyVgMAjN8AAAD//+xcMW7DMAyk5KVLl36gW8c8onsf0P+/o2InBTRzpIg6AeiUBow4ig0EdxZJ646OuPXaH8Nz9pWYR6QD6zcOXLszsetjOZetdbsOzrdSTTspYXzgjrfWFIdxznWGaJClyNCNWLaBc6w6wFNXziAneYu3Wk/TSWPQXlMYKN7q+oRqy1XCbyAsodnRCau8m1Ep9+DMaCeaIRysyOXdr6sQOTt+1PUsH0WIYsJuX4SkSFg6S8LnA6Fs5SUdiGTUp+IBF+2haP8kwvCBYsCssjrVlmo9661gyEXIpWDIRch3wZCr7P0sGHIR8l4w5CLkpWDIRUgrGHIR8low5CLko2DIRchXwZDrOaQISUZIhaxkhFRST0ZIlb3JkjqRr3NE1b62IP5ZtqgGwmDcG9s9GHpKodU67amHK9XwrIrhSta1VEFtZkAuFCJhwkQuUt0tjT6Rtt4AcSg8Zg6XDMgY4FiCLHVy1M2tP7W2fn2tnTYpSMCRS1Z+l8bSDZCK9Him3Erl/H9Ma2ODBHW2EctdOningXaO3zSxTkK6Ea60owS5TfRskMdEOayc9wpVk6hBdjf9ALt2mOhx921gRL4DJeI2eYbcsSKJA3nF2skZu1tlVKXzugoLnfcLAAD//+ydfY7iMAzF3cLcYY+y9z/WMk32ryLjPn8kzEhuMVJFJVCR/NrENnm/jAayAv+7Ig2vbB8Zgj519fv0mqzI2L8Ez2fEPbsA3UiVreWnZLyrk7NnQZiZ4M8mTmQZqQx0U4pEVDDK77+sXNTS3gWkt6jOWCm+1vesgqAMyQr+JgrG5lTsL+xYzR8iV76vokK/KcJ9ijcE7k1hVO2bUZc8BbkTtiHcg8cqhOKQX35+tqcFtUo6aJsgRq+k/KFjA5V+K8NOsteOOZc9LdTP0p4aPrShVoqVlWVvLnrgQTRUcQub7Gt9K03G57Vkt9dyUCGv9C0wwUfS5OwtEvTeSPduayJZDqqnYUdzSK2BY/Y/kbNlWVa6q6W1kQP+H6LdzVEr20w2dTVLm5WFNQpgF6UgRH7Xd2QuuFql3ieHt9GKnUY8hkVxmO9nxUkOA2lvdXp/XjgY5DJ9ZqpD6CLk2ysJUi7cZIKUpS2ZIGWLTiZIuXCTCVI+9UyCXAUCfyVBashKJkhN6skEqbQ3mSBVGCYTpFonyQSp5mIyQaqtnkyQrwpDLkHKp55MkD8VhlyCFFojmSB/Kwy5BCkaUDJBKgqZBKHiZaUTpOaQZIJUlpVMkKpDkglSlXoyQaqXlUyQ6vaeoA4p8Mz86z34DPg/xMMsjaKXlg8J9gi+Sbse3KWND2URhMYVrWyeAB5iY5SZ9bwWN33uAUNsE8/oiShyM09TdkFGyA7IANrJwGoQg2AS6e5bbyNJz43rDX0ZRViMYQfd5TLgGtLP4p4ctoZfCPvTNSop2heXC0r06s5aT5Th9cBwJEWQ3nONRvptiNN3koPklXCCw51et7iXlFIJE9AAmQsd8bP8N+T1NEIEiXMusLUhOw+cxSN5sM/5nd3BdeRmxPv19t/4R0ca6cMSZ6EjmeE2IAg/ZMA9qsPChoeMr0ZHEqkFBthAsB9ChAdhxEbjgsg7EBGBEP9Em1sWRwwvE8skCClzR1OeGPm0eBDljQCeScuqbs7k7k3sPGvrFAcMZMywPHhZA+cb6dzeDU3oxLi9HvtdO+ffjdQmZ0p7uyOQNl+RIZL2TuSAlCMsxtHU9kpojXcLQxWv4d3B0TbKp7dOfmSriv0JebdpWM3F8cai+t3/AAAA///snU1ynDAQhVuQRda5QG6QQ+QWOVeuG4+ysV2Kpn/F4JmJv1dFgYDBhPSj1epGb+VhnvEfwH8qOGrgp1yz4nPbB+xbJQwE+z8MvBeP3Xrf1fFMqFaVB6uuM0Q9m2DgfEKsGG52OlJvnT2mtlvB6L00YSaKtM4VZ98tiARxPt4zHDF8USJnkZxMUU+eK0Zkrkbpm/OGtwzbmy52S7RF8rn6zEhCG/5R0Sjyow/5PzIpmuRqDFpghFIwZmuUvToFr3XMus93guyGwUV6Lpq8iFUL4Y3QR4SKPFjG28yeCoIcI0jWQ3heIZKHtGpFtNygVkcyyrdEAquuYN4eeANvFvhZcnLMO0Z1KuPf3uVajnLMnFczLm1621nzb0OUPDHEeds2idW1LC+gCT+NKTzN0C25A017Vav9sTJVZkFWRA5LhMojiFbxMBPky+Q1doUUo1HvBkG0buI2/SYqOPNeANmUqeWBu+giKlrXT5yuzGxkLdEPt1KWWiGApxmrGZa8Hh+NXkRXRxvv7cXoSo3XHO/9j0GQWYA4S5CRdC5JxopFzeiq2urZtnY9EV2RZ/YmkjC0zAgXnmMtMI9Gqnpiv+Y1RCGf1S3y9OmjttW1Usk811iL1GoaspqTm0O6bHBejTmi1D7dq7Vu1ooyR3boNRusX4yAPRtjRF73/R6tr0Ki0azMcG8m0BbJa4EekUfGY9x/qLdaj1PVKap8fZMe6p2/Yzv6TVslKXgkv1GpS8JT3G/ot0oay1ArXshbR/uu2m9f3q6+fVcV2W6dBYcAz+llqr9ZLSPpi/fXq1NkUagInoFkN7smk2kAEBCE6X8AcAjChGUAOARhikUAHIIwKSwADkGYxhoAhyBMvA+ARRCkQgDwCYIHAcAhCDEIAA5BGMUCwCEIeRAAHIKQSQfAIQi1WAA4BKGaFwCHIBuPAQCbIF95DADYBPnGYwDAJsh3HgMANkF+8BgAsAnyk8cAgE2Q3zwGAAyCiMgvHgMANkHoYgHgEIQgHQCHIAzzAuAQhEQhAA5BKDUBwCEIxYoAWAQ5aVYTSujBPdA/giDtBiRoJ5AI0n1uY+4Hz1kS+bE+mKrInR3Zjo6tEgQyPb7RZ425J49VtzPtfwgSGfSq1uARmbYqWSHN45GhYpSr8mqr2obh3x+/KPQEPKuqt9klSzKLSFXSQJJzyVHRIIwM2TLuWy1itK8Isic8gCbvvBnrWQ/9bVucaxxRx131Ooh71gjRDniD7Bvdk322ZKBnTfT5NxfnGpEC7jtBRsPblO3R0Del/bbsw/E9WI/b4zXldb9ITk99Nc6BHGteYzU+iMggIvIyGP5oxC/DeS/B+vK6fVGWrrRHws3bncmrAQgC1z3ZrfI8R2bZle7XPl1fpn3W0g3PIkpXTNvf6WYtda/a9MYXxUuI4Tnm31vL7EVm7zB6lcvi0o3u2JWXa1OXKhOMb04cMhNAa2tdKqtbVx0Ju/XIF1gfkVodgdK6O14ccQnaERm6E6z3VjDAOU4R5e1vESo6NxuMZ2KJW+VSwNoQb0QSbzg3Ct5nzyKOwVvneqNXJkEkYYxWuyUIlTH87H10IRfyyASJyNIkn9TLECky+O5cL7yPJvmcwbNk0yHB45PnKbLoIscTavcwZgjwObzPWaQrXeNMY8OQwaMTL8TfAQD2J8EY0cmSPwAAAABJRU5ErkJggg==);
    background-position: 0 58px;
    margin: 0 12px;
    height: 29px;
} 
.container_inscription_pro {
    width: 78%;
}
.mb-5, .my-5 {
    margin-bottom: 0rem!important;
}
#comp-l0eg032m {
    background-color: #E80EAE;
}
#comp-l029myfg {
    padding: 18px 0px 18px 0px;
}
#user_id {
    padding: 0px 0px 18px 0;
}
#form_id_modf {
    width: 50%;
    margin-left: 27%;
    padding-bottom: 5%;
}
.modif_id{
    text-align: center;
}
.bItEI ._1VWbH {
    -webkit-appearance: none;
    box-shadow: var(--shd,0 0 0 transparent);
    border-radius: var(--corvid-border-radius,var(--rd,0));
    font: var(--fnt,var(--font_8));
    border-width: var(--corvid-border-width,var(--brw,1px));
    /* resize: none; */
    /* background-color: var(--corvid-background-color,rgba(var(--bg,255,255,255),var(--alpha-bg,1))); */
    box-sizing: border-box!important;
    color: var(--corvid-color,rgb(var(--txt,var(--color_15))));
    border-style: solid;
    border-color: var(--corvid-border-color,rgba(var(--brd,227,227,227),var(--alpha-brd,1)));
    padding: var(--textPadding,3px);
    margin: 0;
    padding-top: 0.75em;
    max-width: 100%;
    min-width: 100%;
    overflow-y: auto;
    text-align: var(--textAlign);
    direction: var(--dir);
    height: var(--inputHeight);
    display: block;
}
#comp-l0egjtbl {
    --shd: none;
    --fntlbl: normal normal normal 14px/1.4em helvetica-w01-light,helvetica-w02-light,sans-serif;
    --rd: 0px 0px 0px 0px;
    --fnt: normal normal normal 16px/1.4em futura-lt-w01-book,futura-lt-w05-book,sans-serif;
    --brw: 2px;
    --bg: 255,255,255;
    --txt: 0,0,0;
    --alpha-txt: 1;
    --brd: 232,14,174;
    --txt2: 0,0,0;
    --txtlbl: 0,166,255;
    --alpha-txt2: 1;
    --brwh: 1px;
    --bgh: 234,247,255;
    --brdh: 198,226,247;
    --bgd: 255,255,255;
    --txtd: 219,219,219;
    --alpha-txtd: 1;
    --brwd: 0px;
    --brdd: 219,219,219;
    --alpha-brdd: 1;
    --alpha-txtlbl: 1;
    --brwf: 1px;
    --bgf: 255,255,255;
    --brdf: 0,166,255;
    --brwe: 1px;
    --bge: 255,255,255;
    --txtlblrq: 0,0,0;
    --alpha-txtlblrq: 1;
    --alpha-bg: 1;
    --alpha-bgd: 1;
    --alpha-bge: 1;
    --alpha-bgf: 1;
    --alpha-bgh: 1;
    --alpha-brd: 1;
    --alpha-brde: 1;
    --alpha-brdf: 1;
    --alpha-brdh: 1;
    --bg2: 170,170,170;
    --alpha-bg2: 1;
    --boxShadowToggleOn-shd: none;
    --dir: ltr;
    --textAlign: left;
    --textPadding: 3px 10px 3px 14px;
    --labelPadding: 0 20px 0 0px;
    --labelMarginBottom: 14px;
    --requiredIndicationDisplay: inline;
    height: auto;
    --brde: 255,64,64;
    --inputHeight: 231px;
}
#comp-l0egniff {
    --shd: none;
    --fntlbl: normal normal normal 14px/1.4em helvetica-w01-light,helvetica-w02-light,sans-serif;
    --rd: 0px 0px 0px 0px;
    --fnt: normal normal normal 16px/1.4em futura-lt-w01-book,futura-lt-w05-book,sans-serif;
    --brw: 2px;
    --bg: 255,255,255;
    --txt: 0,0,0;
    --alpha-txt: 1;
    --brd: 232,14,174;
    --txt2: 0,0,0;
    --txtlbl: 0,166,255;
    --alpha-txt2: 1;
    --brwh: 1px;
    --bgh: 234,247,255;
    --brdh: 198,226,247;
    --bgd: 255,255,255;
    --txtd: 219,219,219;
    --alpha-txtd: 1;
    --brwd: 0px;
    --brdd: 219,219,219;
    --alpha-brdd: 1;
    --alpha-txtlbl: 1;
    --brwf: 1px;
    --bgf: 255,255,255;
    --brdf: 0,166,255;
    --brwe: 1px;
    --bge: 255,255,255;
    --txtlblrq: 0,0,0;
    --alpha-txtlblrq: 1;
    --alpha-bg: 1;
    --alpha-bgd: 1;
    --alpha-bge: 1;
    --alpha-bgf: 1;
    --alpha-bgh: 1;
    --alpha-brd: 1;
    --alpha-brde: 1;
    --alpha-brdf: 1;
    --alpha-brdh: 1;
    --bg2: 170,170,170;
    --alpha-bg2: 1;
    --boxShadowToggleOn-shd: none;
    --dir: ltr;
    --textAlign: left;
    --textPadding: 3px 10px 3px 14px;
    --labelPadding: 0 20px 0 0px;
    --labelMarginBottom: 14px;
    --requiredIndicationDisplay: inline;
    height: auto;
    --brde: 255,64,64;
    --inputHeight: 105px;
}
/* #comp-l0egniff {
    width: 900px;
} */
#comp-l0egs3no {
    text-align: center;
}
._2dBhC ._1SOvY {
    box-shadow: var(--shd,0 0 0 transparent);
    font: var(--fnt,var(--font_8));
    -webkit-appearance: none;
    -moz-appearance: none;
    border-radius: var(--corvid-border-radius,var(--rd,0));
    background-color: var(--corvid-background-color,rgba(var(--bg,255,255,255),var(--alpha-bg,1)));
    border-color: var(--corvid-border-color,rgba(var(--brd,227,227,227),var(--alpha-brd,1)));
    border-width: var(--corvid-border-width,var(--brw,1px));
    box-sizing: border-box!important;
    border-style: solid;
    padding: var(--textPadding);
    margin: 0;
    max-width: 100%;
    text-overflow: ellipsis;
    text-align: var(--textAlign);
    direction: var(--dir);
    min-height: var(--inputHeight);
    width: 100%;
}
#comp-l0egpoz7 {
    --shd: none;
    --brdd: 219,219,219;
    /* --rd: 0px 0px 0px 0px; */
    --fnt: normal normal normal 16px/1.4em futura-lt-w01-book,futura-lt-w05-book,sans-serif;
    --brw: 2px;
    --bg: 255,255,255;
    --txt: 0,0,0;
    --alpha-txt: 1;
    --brd: 232,14,174;
    --txt2: 0,0,0;
    --alpha-brdd: 1;
    --alpha-txt2: 1;
    --brwh: 1px;
    --bgh: 234,247,255;
    --brdh: 198,226,247;
    --brwf: 1px;
    --bgf: 255,255,255;
    --brdf: 0,166,255;
    --brwe: 1px;
    --bge: 255,255,255;
    --brde: 255,64,64;
    --fntlbl: normal normal normal 14px/1.4em helvetica-w01-light,helvetica-w02-light,sans-serif;
    --trns: opacity 0.5s ease 0s, border 0.5s ease 0s, color 0.5s ease 0s;
    --bgd: 255,255,255;
    --alpha-bgd: 1;
    --txtd: 219,219,219;
    --alpha-txtd: 1;
    --txtlbl: 0,166,255;
    --alpha-txtlbl: 1;
    --txtlblrq: 0,0,0;
    --alpha-txtlblrq: 1;
    --fntprefix: normal normal normal 16px/1.4em helvetica-w01-roman,helvetica-w02-roman,helvetica-lt-w10-roman,sans-serif;
    --alpha-brd: 1;
    --alpha-brdf: 1;
    --alpha-brdh: 1;
    --boxShadowToggleOn-shd: none;
    --alpha-bg: 1;
    --alpha-bgh: 1;
    --alpha-bgf: 1;
    --alpha-bge: 1;
    --alpha-brde: 1;
    --dir: ltr;
    --textAlign: left;
    --textPadding: 3px 3px 3px 14px;
    --labelPadding: 0 20px 0 0px;
    --requiredIndicationDisplay: inline;
    --labelMarginBottom: 14px;
    height: auto;
    --brwd: 1px;
    /* --inputHeight: 37px; */
}
#comp-l0egpoz7 {
    width: 40%;
   margin-left: 30%;
}
[data-mesh-id=comp-l0efuofwinlineContent-gridContainer] > [id="comp-l0egpoz7"] {
    position: relative;
    margin: 0px 0px 48px 0;
    left: 336px;
    grid-area: 20 / 1 / 21 / 2;
    justify-self: start;
    align-self: start;
}
#comp-l029tfee {
    width: 100%;

}
#comp-l0efuosp {
    --rd: 0px 0px 0px 0px;
    --alpha-txt-day: 1;
    --fnt: normal normal normal 16px/1.4em futura-lt-w01-book,futura-lt-w05-book,sans-serif;
    --brw: 2px;
    --shd: none;
    --bg: 255,255,255;
    --brd: 232,14,174;
    --brwe: 1px;
    --bge: 255,255,255;
    --brde: 255,64,64;
    --brwd: 1px;
    --txt: 0,0,0;
    --bgd: 255,255,255;
    --alpha-bgd: 1;
    --brdd: 204,204,204;
    --alpha-brdd: 1;
    --brwf: 1px;
    --bgf: 255,255,255;
    --brdf: 0,166,255;
    --brwh: 1px;
    --bgh: 234,247,255;
    --brdh: 198,226,247;
    --alpha-txt: 1;
    --txtd: 204,204,204;
    --alpha-txtd: 1;
    --txt-placeholder: 0,0,0;
    --alpha-txt-placeholder: 1;
    --fntlbl: normal normal normal 14px/1.4em helvetica-w01-light,helvetica-w02-light,sans-serif;
    --txtlbl: 0,166,255;
    --alpha-txtlbl: 1;
    --txtlblrq: 0,0,0;
    --alpha-txtlblrq: 1;
    --shd-calendar: none;
    --rd-calendar: 0px;
    --bg-calendar: 255,255,255;
    --brw-calendar: 1px;
    --brd-calendar: 232,14,174;
    --fnt-family-day: helvetica-w01-light;
    --alpha-brd-calendar: 1;
    --bg-day-selected: var(--color_12);
    --txt-day-selected: var(--color_11);
    --alpha-txt-day-selected: 1;
    --rd-day-selected: 30px;
    --fnt-family-header: helvetica-w01-light;
    --txt-header: 0,0,0;
    --alpha-txt-header: 1;
    --txt-size-header: 16px;
    --bg-header: 255,255,255;
    --alpha-bg: 1;
    --alpha-bgh: 1;
    --txt-size-day: 14px;
    --alpha-brd: 1;
    --alpha-brdf: 1;
    --alpha-brdh: 1;
    --alpha-icn-color: 0.5;
    --boxShadowToggleOn-shd: none;
    --boxShadowToggleOn-shd-calendar: none;
    --alpha-bg-day-selected: 1;
    --alpha-bgf: 1;
    --alpha-bge: 1;
    --alpha-brde: 1;
    --textAlign: left;
    --alpha-bg-calendar: 1;
    --icn-color: 232,14,174;
    --dir: ltr;
    --labelMarginBottom: 14px;
    --labelPadding: 0 20px 0 0px;
    --requiredIndicationDisplay: inline;
    --inputWrapperAlign: left;
    --inputPadding: 1px 2px 1px 14px;
    --inputWrapperAlignItems: flex-start;
    --inputWrapperHeight: 37px;
    height: auto;
    --calendarButtonLeft: auto;
    --alpha-bg-header: 1;
    --txt-day: 0,0,0;
    --calendarButtonRight: 20px;
}
._2awB6 .TUQC6 svg {
    width: 100%;
    height: 100%;
    fill: rgba(var(--icn-color,202,202,202),var(--alpha-icn-color,1));
    position: absolute;
    left: var(--calendarButtonLeft);
    right: var(--calendarButtonRight);
    width: 16px;
    height: 16px;

}
._2awB6 ._1HyVs {
    margin-bottom: 0;
    display: flex;
    flex-direction: column;
    height: 100%;
}
input[type=checkbox]  {
            display: block;
            margin: 0.2em;
            cursor: pointer;
            padding: 0.2em;
            }

            input[type=checkbox] {
            display: none;

            }

            input[type=checkbox] + label:before {
            content: "\2713";
            border: 0.1em solid #E80EAE;
            border-radius: 0em;
            display: inline-block;
            width: 1em;
            height: 1em;
            padding-left: 0.2em;
            padding-bottom: 0.3em;
            margin-right: 0.2em;
            vertical-align: bottom;
            color: transparent;
            transition: .2s;
            }
/* 
            input[type=checkbox] + span:active:before {
            transform: scale(0);
            } */

            input[type=checkbox]:checked + label:before {
            background-color: #fff;
            border-color: #E80EAE;
            color: #E80EAE;
            }

            input[type=checkbox]:disabled + label:before {
            transform: scale(1);
            border-color: #fff;
            }

            input[type=checkbox]:checked:disabled + label:before {
            transform: scale(1);
            background-color: #fff;
            border-color: #fff;
            }
    </style>

    <?php ///$this->load->view("privicarte/pub_compte", $data); ?>



</div>



<?php $this->load->view("sortez/includes/footer_frontoffice_com", $data); ?>

