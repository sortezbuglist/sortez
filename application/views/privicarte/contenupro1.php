<?php $data["zTitle"] = 'Accueil'; ?>
<?php $this->load->view("sortez/includes/header_frontoffice_com", $data); ?>

    <style type="text/css">
        .container_inscription_pro {
            background-color: #ffffff;
            display: table;
            margin-top: 0 !important;
        }

        .validation_referencement_div {
            background-color: #008000;
            text-transform: uppercase;
            color: #ffffff;
            margin-top: 40px;
            padding: 15px;
            font-family: futura_regular;
        }

        .txt_mentions_sortez {
            font-family: "Arial", sans-serif;
            font-size: 13px;
            line-height: 1.23em;
            display: table;
            padding: 15px;
            text-align: justify;
            color: #DC188E;
        }

        .txt_mentions_sortez a, .txt_mentions_sortez a:hover, .txt_mentions_sortez a:focus, .txt_mentions_sortez a:active {
            color: #DC188E;
        }

        .alert_validation_pro {
            display: table;
            text-transform: none;
            width: 100%;
        }
    </style>

    <div class="container_inscription_pro">

        <?php $this->load->view("sortez/logo_global", $data); ?>

        <?php if (isset($_SESSION['pop_view']) && $_SESSION['pop_view'] == '1') {
            $_SESSION['pop_view'] = '0';
            ?>
            <script type="text/javascript">
                //$("a.fancybox-close", window.parent.document).click();
                window.top.window.$.fancybox.close();
                //window.open("<?php //echo site_url("front/utilisateur/contenupro");?>");
                window.top.window.location = "<?php echo site_url("front/utilisateur/contenupro");?>";
            </script>
        <?php } ?>


        <?php
        $thisss =& get_instance();
        $thisss->load->library('ion_auth');
        ?>

        <div style="text-align:center; width:100%;">

            <?php if (isset($from_super_admin_account) && $from_super_admin_account == '1') { ?>
                <div class="col-lg-12"><a
                        href="<?php echo site_url("front/utilisateur/back_super_admin_account") . "/" . $iduser; ?>"
                        class="btn btn-success">Revenir &agrave; la gestion Super-Admin</a></div>
            <?php } ?>

            <div style="text-align:center;">
                <img src="<?php echo GetImagePath("privicarte/"); ?>/abo_prem.png" alt="inscription"
                     style="text-align:center">
            </div>

            <?php if (isset($toCommercant)) { ?>
                <div style='margin-top:20px; margin-bottom:20px;font-family: "Arial",sans-serif;
    font-size: 16px;
    font-weight: 700;
    line-height: 1.25em;'>
                    Bienvenue <?php echo $toCommercant->Civilite . ' ' . $toCommercant->Prenom . ' ' . $toCommercant->Nom; ?>
                </div>
            <?php } ?>

            <?php if ($thisss->ion_auth->logged_in()) {
                if (isset($iduser)) $iduser = $iduser; else $iduser = "";
            } else $iduser = "";
            ?>


            <div class="textaligncenter" style='line-height: 16.00px;
font-family: "Arial", sans-serif;
font-style: normal;
font-weight: normal;
color: #000000;
background-color: transparent;
text-decoration: none;
font-variant: normal;
font-size: 13.3px;
vertical-align: 0;
margin-bottom: 60px;
'>


                <p>Nous vous remercions de votre inscription.</p>

                <p>A l’aide de notre interface intuitive, nous vous invitons à créer votre page de présentation<br/>

                    en remplissant les champs préconfigurés par du texte, images, liens, vidéo, document PDF…</p>

                <p>A tout moment, vous pouvez visualiser et perfectionner le résultat.</p>


                <p>Si vous rencontrez une difficulté ou bug, adressez nous une alerte,<br/>

                    notre service vous répondra sous 48h00…</p>

            </div>


            <div style="margin-bottom:10px;">
                <a href="<?php echo site_url("front/professionnels/fiche") . "/" . $iduser . "/?page_data=coordonnees"; ?>">
                    <img src="<?php echo GetImagePath("privicarte/"); ?>/btn_pro_coordonnees.png" alt="coordonnees"/>
                </a>
            </div>

            <div style="margin-bottom:10px;">
                <a href="<?php echo site_url("front/professionnels/fiche") . "/" . $iduser . "/?page_data=contenus"; ?>">
                    <img src="<?php echo GetImagePath("privicarte/"); ?>/btn_pro_contenu.png" alt="contenus"/>
                </a>
            </div>

            <div style="margin-bottom:10px;">
                <a href="<?php echo site_url("front/packarticle"); ?>" class="" style="width: 395px;">
                    <img src="<?php echo GetImagePath("privicarte/"); ?>/btn_pro_packarticle.png" alt="Packarticle"/>
                </a>
            </div>

            <!-- <div style="margin-bottom:40px;">
    	<a href="<?php //echo site_url("front/professionnels/glissiere")."/".$iduser;?>">
            <img src="<?php //echo GetImagePath("privicarte/"); ?>/btn_pro_glissieres.png" alt="glissieres" />
        </a>
    </div>-->


            <script>
                function btn_pro_carte() {
                    window.open("<?php echo site_url('front/fidelity/contenupro');?>", "_blank", "toolbar=no, scrollbars=yes, resizable=yes, top=500, left=500, width=650, height=800");
                }
            </script>


            <?php if ($thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(4)) { ?>
                <?php if (isset($toCommercant->bonplan) && $toCommercant->bonplan == "1") { ?>
                    <div style="margin-bottom:10px;"><a
                            href="<?php echo site_url('front/bonplan/listeMesBonplans/' . $toCommercant->IdCommercant); ?>"><img
                                src="<?php echo GetImagePath("privicarte/"); ?>/btn_pro_bonplan.png" alt="coordonnees"/></a>
                    </div>
                    <?php if ($thisss->ion_auth->in_group(5)) { ?>
                        <?php if (isset($toCommercant->agenda_export) && $toCommercant->agenda_export == "1") { ?>
                            <div style="margin-bottom:10px;"><a href="<?php echo site_url('bonplan/personnalisation'); ?>"><img
                                        src="<?php echo GetImagePath("privicarte/"); ?>/export_bon_plan.jpg" alt=""
                                        style="text-align:center"></a></div>
                        <?php } ?>
                    <?php } ?>
                    <!--<div style="margin-bottom:10px;">
            <a href="#">
                <img src="<?php echo GetImagePath("privicarte/"); ?>/btn_pro_fidelisation.png" alt="coordonnees" />
            </a>
        </div>-->
                    <div style="margin-bottom:10px;">
                        <a href="javascript:void(0);" onclick="btn_pro_carte();"> <!--onclick="btn_pro_carte();"-->
                            <img src="<?php echo GetImagePath("privicarte/"); ?>/btn_pro_carte.png" alt="coordonnees"/>
                        </a>
                    </div>
                <?php } ?>
            <?php } ?>






            <?php if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5)) { ?>
                <?php if (isset($toCommercant->agenda) && $toCommercant->agenda == "1") { ?>
                    <div style="margin-bottom:10px;"><a
                            href="<?php echo site_url('front/utilisateur/agenda/' . $toCommercant->IdCommercant); ?>"><img
                                src="<?php echo GetImagePath("privicarte/"); ?>/btn_pro_gest_agenda_100.png"
                                alt="coordonnees"/></a>
                    </div>
                <?php } else if (isset($toCommercant->agenda_25) && $toCommercant->agenda_25 == "1") { ?>
                    <div style="margin-bottom:10px;"><a
                            href="<?php echo site_url('front/utilisateur/agenda/' . $toCommercant->IdCommercant); ?>"><img
                                src="<?php echo GetImagePath("privicarte/"); ?>/btn_pro_gest_agenda_25.png"
                                alt="coordonnees"/></a>
                    </div>
                <?php } ?>
            <?php } ?>

            <?php if ($thisss->ion_auth->in_group(5)) { ?>
                <?php if (isset($toCommercant->agenda_export) && $toCommercant->agenda_export == "1") { ?>
                    <div style="margin-bottom:10px;"><a href="<?php echo site_url('agenda/personnalisation'); ?>"><img
                                src="<?php echo GetImagePath("privicarte/"); ?>/btn_pro_export_agenda.png"
                                alt="coordonnees"/></a>
                    </div>
                <?php } ?>
            <?php } ?>


            <!-- <div style="margin-bottom:40px;">
    	<a href="#">
            <img src="<?php //echo GetImagePath("privicarte/"); ?>/btn_pro_balise.png" alt="coordonnees" />
        </a>
    </div>-->


            <?php if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5)) { ?>
                <?php if (isset($toCommercant->annonce) && $toCommercant->annonce == "1") { ?>
                    <div style="margin-bottom:10px;"><!-- ma boutique -->
                        <a
                            href="<?php echo site_url('front/annonce/listeMesAnnonces/' . $toCommercant->IdCommercant); ?>">
                            <img src="<?php echo GetImagePath("privicarte/"); ?>/btn_pro_boutique_100.png"
                                 alt="coordonnees"/>
                        </a>
                    </div>
                <?php } else if (isset($toCommercant->annonce_25) && $toCommercant->annonce_25 == "1") { ?>
                    <div style="margin-bottom:10px;"><a
                            href="<?php echo site_url('front/annonce/listeMesAnnonces/' . $toCommercant->IdCommercant); ?>">
                            <img src="<?php echo GetImagePath("privicarte/"); ?>/btn_pro_boutique_25.png"
                                 alt="coordonnees"/>
                        </a>
                    </div>
                <?php } ?>
            <?php } ?>
            <?php if ($thisss->ion_auth->in_group(5)) { ?>
                <?php if (isset($toCommercant->agenda_export) && $toCommercant->agenda_export == "1") { ?>
                    <div style="margin-bottom:10px;"><a
                            href="<?php echo site_url('annonce/personnalisation'); ?>">
                            <img src="<?php echo GetImagePath("privicarte/"); ?>/export.jpg"
                                 alt="coordonnees"/>
                        </a>
                    </div>
                <?php } ?>
            <?php } ?>

            <?php if ($thisss->ion_auth->in_group(5)) { ?>
                <?php if (isset($toCommercant->IdCommercant) && $toCommercant->IdCommercant !=0) { ?>
                    <div style="margin-bottom:10px;"><a
                            href="<?php echo site_url('front/gestion_livre_dor/liste/'.$toCommercant->IdCommercant); ?>">
                            <img src="<?php echo GetImagePath("privicarte/"); ?>/btn_livre_dor.jpg"
                                 alt="coordonnees"/>
                        </a>
                    </div>
                <?php } ?>
            <?php } ?>


            <?php if (($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5)) && isset($toCommercant->blog) && $toCommercant->blog == "1") { ?>
                <div style="margin-bottom:10px;"><a
                        href="<?php echo site_url('front/articles/liste/' . $toCommercant->IdCommercant); ?>"><img
                            src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/btn_gest_articles.png" alt=""
                            style="text-align:center"></a>
                </div>
            <?php } ?>


            <?php if ($thisss->ion_auth->in_group(5)) { ?>
                <?php if (isset($toCommercant->agenda_export) && $toCommercant->agenda_export == "1") { ?>
                    <div style="margin-bottom:10px;"><a href="<?php echo site_url('article/personnalisation'); ?>"><img
                                src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/btn_gest_articles_export.png" alt=""
                                style="text-align:center"></a>
                    </div>
                <?php } ?>
            <?php } ?>
            <?php if (($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5)) && isset($toCommercant->blog) && $toCommercant->blog == "1") { ?>
                <div style="margin-bottom:10px;"><a
                        href="<?php echo site_url('front/festival/liste/' . $toCommercant->IdCommercant); ?>"><img
                            src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/festival.jpg" alt=""
                            style="text-align:center"></a>
                </div>
            <?php } ?>
            <?php if ($thisss->ion_auth->in_group(5)) { ?>
                <?php if (isset($toCommercant->agenda_export) && $toCommercant->agenda_export == "1") { ?>
                    <div style="margin-bottom:10px;"><a href="<?php echo site_url('front/festival/personnalisation'); ?>"><img
                                src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/export_festival.jpg" alt=""
                                style="text-align:center"></a>
                    </div>
                <?php } ?>
            <?php } ?>
            <?php if ($thisss->ion_auth->in_group(5)) { ?>
                <?php if (isset($toCommercant->agenda_export) && $toCommercant->agenda_export == "1") { ?>
                    <div class="row" style="padding-bottom: 10px">
                        <div class="col-lg-12"><a  href="<?php echo site_url('article/revue_presse')?>"><img src="<?php echo base_url('assets/img/rev.png')?>"></a></div>
                    </div>
                <?php } ?>
            <?php } ?>
            <?php if ($thisss->ion_auth->in_group(5)  AND $toCommercant->referencement_resto==1) { ?>
                <div class="row" style="padding-bottom: 10px"><!-- plat du jours -->
                    <div class="col-lg-12"><a  href="<?php echo site_url('front/Plat_du_jour/listePlatDuJour')?>"><img src="<?php echo base_url('assets/img/plat.png')?>"></a></div>
                </div>
            <?php } ?>

            <?php if ($thisss->ion_auth->in_group(5) AND $toCommercant->referencement_gite==1) { ?>
                <div class="row" style="padding-bottom: 10px"><!-- plat du jours -->
                    <div class="col-lg-12"><a  href="<?php echo site_url('admin/Reservations/index')?>"><img src="<?php echo base_url('assets/img/res_gite.jpg')?>"></a></div>
                </div>
            <?php } ?>

            <?php if ($thisss->ion_auth->in_group(5)  AND $toCommercant->referencement_news==1) { ?>
                <div class="row" style="padding-bottom: 10px"><!-- plat du jours -->
                    <div class="col-lg-12"><a  href="<?php echo site_url('admin/Newsletter_commercant_backoffice/index')?>"><img src="<?php echo GetImagePath("privicarte/"); ?>/newslettres.jpg"></a></div>
                </div>
            <?php } ?>

            <?php if ($thisss->ion_auth->in_group(3) && isset($toCommercant->IdCommercant)) { ?>
                <div style="margin-bottom:10px;"><a
                        href="<?php echo site_url('front/utilisateur/agenda/' . $toCommercant->IdCommercant); ?>"><img
                            src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/btn_ag_gratuit.png" alt=""
                            style="text-align:center"></a>
                </div>
            <?php } ?>
            <div style="margin-bottom:10px;">
                <a onclick='javascript:window.open("<?php echo site_url('contact'); ?>", "contact", "width=550, height=500, scrollbars=yes");' title="Contactez-nous">
                    <img src="<?php echo GetImagePath("privicarte/"); ?>/bouton_alerte_contact.png"
                         alt="alerte contact"/>
                </a>
            </div>


            <?php if (isset($iduser) && $iduser != 0 && $iduser != "") { ?>
                <div><a href="<?php echo site_url("connexion/sortir"); ?>">
                        <img src="<?php echo GetImagePath("privicarte/"); ?>/btn_pro_deconnexion.png"
                             alt="deconnexion"/>
                    </a>
                </div>
            <?php } ?>


        </div>

        <?php if (!isset($toCommercant->referencement_annuaire) || $toCommercant->referencement_annuaire != "1") { ?>
            <div class="col-xs-12 padding0 textaligncenter validation_referencement_div">
                <div style="padding: 15px;">
                    Ma page est maintenant finalisée,<br/>
                    Je demande son référencement définitif sur l'annuaire complet<br/>
                    de sortez.org
                </div>
                <?php if (isset($validation_pro)) { ?>
                    <?php foreach ($validation_pro as $validation_pro_item) { ?>
                        <?php if ($validation_pro_item->id_pack_test == '4' && $validation_pro_item->idCommercant == $iduser) { ?>
                            <div class="alert alert-success alert_validation_pro">Vous avez une demande de validation en
                                attente de confirmation par l'administrateur Sortez
                            </div>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
                <div style="padding: 15px;">
                    <a href="#divDemandeActivationComptePro" id="btnDemandeActivationComptePro" class="btn btn-success"
                       style="font-family: futura_medium;">VALIDATION</a>
                </div>
            </div>
        <?php } ?>

        <div class="col-xs-12 txt_mentions_sortez">
            Attention : Sortez.org peut refuser sans en donner la justification l’ouverture d’un compte qui ne
            correspondrait pas &agrave; son &eacute;thique. (<a href="<?php echo base_url();?>mentions-legales.html"
                                                                target="_blank"
                                                                style="font-weight:normal;">Voir nos conditions g&eacute;n&eacute;rales</a>).
        </div>

        <?php $this->load->view("privicarte/content_dmd_premium", $data); ?>

    </div>


    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('#btncancelDemandeActivationComptePro').click(function () {
                jQuery('.fancybox-skin .fancybox-close').click();
            });
            jQuery('#btnconfirmDemandeActivationComptePro').click(function () {
                jQuery("#resultDemandeActivationComptePro").html("<img src='<?php echo base_url();?>application/resources/front/images/wait.gif' />");
                jQuery.post(
                    "<?php echo site_url("front/professionnels/send_validation_pro/");?>",
                    {
                        contact_to: "contact@sortez.org",
                        contact_from: "noreply@sortez.org",
                        contact_object: "Demande de validation referencement Annuaire",
                        contact_message: "demande_validation_referencement_annuaire"
                    },
                    function (data) {
                        if (data == "1") {
                            jQuery("#resultDemandeActivationComptePro").html("Un email vient d'être envoyé aux administrateurs de Sortez !");
                            setTimeout(function () {
                                jQuery('.fancybox-skin .fancybox-close').click();
                            }, 3000);
                        } else {
                            jQuery("#resultDemandeActivationComptePro").html("Une erreur est survenue, veuillez refaire l'opération !");
                        }
                    });
            });
            jQuery('#btncancelDemandeActivationPackPromo').click(function () {
                jQuery('.fancybox-skin .fancybox-close').click();
            });
            jQuery('#btnconfirmDemandeActivationPackPromo').click(function () {
                jQuery("#resultDemandeActivationPackPromo").html("<img src='<?php echo base_url();?>application/resources/front/images/wait.gif' />");
                jQuery.post(
                    "<?php echo site_url("front/professionnels/send_validation_pro/");?>",
                    {
                        contact_to: "contact@sortez.org",
                        contact_from: "noreply@sortez.org",
                        contact_object: "Demande de validation essai PACK PROMO",
                        contact_message: "demande_validation_pack_promo"
                    },
                    function (data) {
                        if (data == "1") {
                            jQuery("#resultDemandeActivationPackPromo").html("Un email vient d'être envoyé aux administrateurs de Sortez !");
                            setTimeout(function () {
                                jQuery('.fancybox-skin .fancybox-close').click();
                            }, 3000);
                        } else {
                            jQuery("#resultDemandeActivationPackPromo").html("Une erreur est survenue, veuillez refaire l'opération !");
                        }
                    });
            });
            jQuery('#btncancelDemandeActivationPackInfo').click(function () {
                jQuery('.fancybox-skin .fancybox-close').click();
            });
            jQuery('#btnconfirmDemandeActivationPackInfo').click(function () {
                jQuery("#resultDemandeActivationPackInfo").html("<img src='<?php echo base_url();?>application/resources/front/images/wait.gif' />");
                jQuery.post(
                    "<?php echo site_url("front/professionnels/send_validation_pro/");?>",
                    {
                        contact_to: "contact@sortez.org",
                        contact_from: "noreply@sortez.org",
                        contact_object: "Demande de validation essai PACK INFO",
                        contact_message: "demande_validation_pack_info"
                    },
                    function (data) {
                        if (data == "1") {
                            jQuery("#resultDemandeActivationPackInfo").html("Un email vient d'être envoyé aux administrateurs de Sortez !");
                            setTimeout(function () {
                                jQuery('.fancybox-skin .fancybox-close').click();
                            }, 3000);
                        } else {
                            jQuery("#resultDemandeActivationPackInfo").html("Une erreur est survenue, veuillez refaire l'opération !");
                        }
                    });
            });
            jQuery('#btncancelDemandeActivationPackPlatinium').click(function () {
                jQuery('.fancybox-skin .fancybox-close').click();
            });
            jQuery('#btnconfirmDemandeActivationPackPlatinium').click(function () {
                jQuery("#resultDemandeActivationPackPlatinium").html("<img src='<?php echo base_url();?>application/resources/front/images/wait.gif' />");
                jQuery.post(
                    "<?php echo site_url("front/professionnels/send_validation_pro/");?>",
                    {
                        contact_to: "contact@sortez.org",
                        contact_from: "noreply@sortez.org",
                        contact_object: "Demande de validation essai PACK PLATINIUM",
                        contact_message: "demande_validation_pack_platinium"
                    },
                    function (data) {
                        if (data == "1") {
                            jQuery("#resultDemandeActivationPackPlatinium").html("Un email vient d'être envoyé aux administrateurs de Sortez !");
                            setTimeout(function () {
                                jQuery('.fancybox-skin .fancybox-close').click();
                            }, 3000);
                        } else {
                            jQuery("#resultDemandeActivationPackPlatinium").html("Une erreur est survenue, veuillez refaire l'opération !");
                        }
                    });
            });
            jQuery("#btnDemandeActivationComptePro").fancybox({
                'autoScale': false,
                'overlayOpacity': 0.8, // Set opacity to 0.8
                'overlayColor': "#000000", // Set color to Black
                'padding': 5,
                'width': 320,
                'height': 215,
                'transitionIn': 'elastic',
                'transitionOut': 'elastic'
            });
            jQuery("#btnDemandeActivationPackPromo").fancybox({
                'autoScale': false,
                'overlayOpacity': 0.8, // Set opacity to 0.8
                'overlayColor': "#000000", // Set color to Black
                'padding': 5,
                'width': 320,
                'height': 215,
                'transitionIn': 'elastic',
                'transitionOut': 'elastic'
            });
            jQuery("#btnDemandeActivationPackInfo").fancybox({
                'autoScale': false,
                'overlayOpacity': 0.8, // Set opacity to 0.8
                'overlayColor': "#000000", // Set color to Black
                'padding': 5,
                'width': 320,
                'height': 215,
                'transitionIn': 'elastic',
                'transitionOut': 'elastic'
            });
            jQuery("#btnDemandeActivationPackPlatinium").fancybox({
                'autoScale': false,
                'overlayOpacity': 0.8, // Set opacity to 0.8
                'overlayColor': "#000000", // Set color to Black
                'padding': 5,
                'width': 320,
                'height': 215,
                'transitionIn': 'elastic',
                'transitionOut': 'elastic'
            });
        });
    </script>

    <div class="divDemandeActivationComptePro" id="divDemandeActivationComptePro" style="display: none;">
        <div class="col-xs-12 textaligncenter" style="padding: 15px">
            <img src="<?php echo base_url(); ?>application/resources/privicarte/images/abo_prem.png" alt="validation"
                 style="text-align:center">
        </div>
        <div class="col-xs-12 textaligncenter" style="padding: 15px;">
            <div class="alert alert-info" style="width: 600px; margin: 0 auto;"> Confirmez votre demande d'activation de
                compte pour être référencé dans Sortez
            </div>
        </div>
        <div class="col-xs-12 textaligncenter" style="padding: 15px">
            <div class="col-xs-6 textaligncenter">
                <button class="btn btn-success" id="btnconfirmDemandeActivationComptePro" onclick="return false;">
                    Confirmer
                </button>
            </div>
            <div class="col-xs-6 textaligncenter">
                <button class="btn btn-danger" id="btncancelDemandeActivationComptePro" onclick="return false;">
                    Annuler
                </button>
            </div>
        </div>
        <div class="col-xs-12 textaligncenter" id="resultDemandeActivationComptePro"></div>
    </div>


    <div class="divDemandeActivationPackPromo" id="divDemandeActivationPackPromo" style="display: none;">
        <div class="col-xs-12 textaligncenter" style="padding: 15px">
            <img src="<?php echo base_url(); ?>application/resources/privicarte/images/abo_prem.png" alt="validation"
                 style="text-align:center">
        </div>
        <div class="col-xs-12 textaligncenter" style="padding: 15px;">
            <div class="alert alert-info" style="width: 600px; margin: 0 auto;"> Confirmez votre demande d'essai PACK
                PROMO de 15 jours
            </div>
        </div>
        <div class="col-xs-12 textaligncenter" style="padding: 15px">
            <div class="col-xs-6 textaligncenter">
                <button class="btn btn-success" id="btnconfirmDemandeActivationPackPromo" onclick="return false;">
                    Confirmer
                </button>
            </div>
            <div class="col-xs-6 textaligncenter">
                <button class="btn btn-danger" id="btncancelDemandeActivationPackPromo" onclick="return false;">
                    Annuler
                </button>
            </div>
        </div>
        <div class="col-xs-12 textaligncenter" id="resultDemandeActivationPackPromo"></div>
    </div>


    <div class="divDemandeActivationPackInfo" id="divDemandeActivationPackInfo" style="display: none;">
        <div class="col-xs-12 textaligncenter" style="padding: 15px">
            <img src="<?php echo base_url(); ?>application/resources/privicarte/images/abo_prem.png" alt="validation"
                 style="text-align:center">
        </div>
        <div class="col-xs-12 textaligncenter" style="padding: 15px;">
            <div class="alert alert-info" style="width: 600px; margin: 0 auto;"> Confirmez votre demande d'essai PACK
                INFO de 15 jours
            </div>
        </div>
        <div class="col-xs-12 textaligncenter" style="padding: 15px">
            <div class="col-xs-6 textaligncenter">
                <button class="btn btn-success" id="btnconfirmDemandeActivationPackInfo" onclick="return false;">
                    Confirmer
                </button>
            </div>
            <div class="col-xs-6 textaligncenter">
                <button class="btn btn-danger" id="btncancelDemandeActivationPackInfo" onclick="return false;">
                    Annuler
                </button>
            </div>
        </div>
        <div class="col-xs-12 textaligncenter" id="resultDemandeActivationPackInfo"></div>
    </div>


    <div class="divDemandeActivationPackPlatinium" id="divDemandeActivationPackPlatinium" style="display: none;">
        <div class="col-xs-12 textaligncenter" style="padding: 15px">
            <img src="<?php echo base_url(); ?>application/resources/privicarte/images/abo_prem.png" alt="validation"
                 style="text-align:center">
        </div>
        <div class="col-xs-12 textaligncenter" style="padding: 15px;">
            <div class="alert alert-info" style="width: 600px; margin: 0 auto;"> Confirmez votre demande d'essai PACK
                PLATINIUM de 15 jours
            </div>
        </div>
        <div class="col-xs-12 textaligncenter" style="padding: 15px">
            <div class="col-xs-6 textaligncenter">
                <button class="btn btn-success" id="btnconfirmDemandeActivationPackPlatinium" onclick="return false;">
                    Confirmer
                </button>
            </div>
            <div class="col-xs-6 textaligncenter">
                <button class="btn btn-danger" id="btncancelDemandeActivationPackPlatinium" onclick="return false;">
                    Annuler
                </button>
            </div>
        </div>
        <div class="col-xs-12 textaligncenter" id="resultDemandeActivationPackPlatinium"></div>
    </div>
<?php $this->load->view("frontAout2013/includes/footer_mini_2"); ?>ZZZ