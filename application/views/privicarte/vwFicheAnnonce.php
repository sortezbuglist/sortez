<?php $data["zTitle"] = 'Creation annonce'; ?>
<?php $this->load->view("sortez/includes/header_backoffice_com", $data); ?>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <link href="<?php echo GetCssPath("privicarte/"); ?>/global.css" rel="stylesheet" type="text/css">

    <script>
        $(function () {
            $("#IdDateDebut").datepicker({
                dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                monthNames: ['Janvier', 'Févier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                dateFormat: 'DD, d MM yy',
                autoSize: true,
                changeMonth: true,
                changeYear: true,
                yearRange: '1900:2020'
            });
            $("#IdDateFin").datepicker({
                dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                monthNames: ['Janvier', 'Févier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                dateFormat: 'DD, d MM yy',
                autoSize: true,
                changeMonth: true,
                changeYear: true,
                yearRange: '1900:2020'
            });
            $(".complete_img_chekings").hide();
        });

        function deleteFile(_IdAnnonce, _FileName) {
            $('.loading_' + _FileName).html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            //alert('<?php //echo base_url();?>front/annonce/delete_files/' + _IdAnnonce + '/' + _FileName);
            $.ajax({
                url: '<?php echo base_url();?>front/annonce/delete_files/' + _IdAnnonce + '/' + _FileName,
                dataType: 'html',
                type: 'POST',
                async: true,
                success: function (data) {
                    $('.loading_' + _FileName).html("");
                    window.location.reload();
                }
            });
        }
    </script>
    <style type="text/css">
        .input_width {
            width: 100%;
        }

        .link_button {
            background-color: #006699;
            border: 2px solid #003366;
            border-radius: 8px;
            color: #ffffff;
            font-size: 12px;
            padding: 10px 15px;
            float: right;
            margin-right: 150px;
            text-decoration: none;
        }

        .annonce_blue_barr {
            background-color: #3653a3;
            color: #ffffff;
            font-family: "Arial", sans-serif;
            font-size: 13px;
            font-weight: 700;
            height: auto;
            line-height: 30px;
            margin: 0;
            min-height: 30px;
            padding-left: 15px;
            padding-top: 3px;
            text-align: left;
        }

        .textaligncenter {
            text-align: center;
        }

        #submitFormAnnonce {
            margin: 30px 0 0 0;
        }

        .admin_bo_menu {
            display: inline-table;
            height: 50px;
        }

        .btn_adminbp, .btn_adminbp:hover, .btn_adminbp:focus {
            background-color: #3653A3;
            padding: 10px 26.5px;
            color: #FFFFFF;
            text-decoration: none;
            font-weight: bold;
        }

        .greybg {
            background-color: #E5E5E5;
            padding: 15px;
            display: table;
            width: 100%;
        }

        .admin_bp_green_btn, .admin_bp_green_btn:hover, .admin_bp_green_btn:active, .admin_bp_green_btn:focus {
            font-weight: bold;
            color: #FFFFFF;
            background-color: #008000;
            width: 100%;
            padding: 10px;
            text-align: center;
            display: table;
            text-decoration: none;
        }
    </style>


    <div class="col-xs-12 admin_bo_menu textaligncenter">
        <h1>Mon Annonce</h1>
    </div>

    <div class="col-xs-12 padding0">
        <div class="admin_bo_menu"><a href="<?php echo site_url('front/utilisateur/contenupro/'); ?>"
                                      class="btn_adminbp">Retour Menu</a></div>
        <div class="admin_bo_menu" style="min-height: 50px;"><a
                href="<?php echo site_url('front/annonce/listeMesAnnonces/' . $idCommercant); ?>" class="btn_adminbp">Retour
                aux Annonces</a></div>
        <?php if (isset($limit_annnonce) AND count($limit_annnonce) < intval($objCommercant->$limit_annnonce)){ ?>
        <div class="admin_bo_menu" style="min-height: 50px;"><a
                href="<?php echo site_url('front/annonce/ficheAnnonce/' . $idCommercant . '/0'); ?>"
                class="btn_adminbp">Ajouter une nouvelle Annonce</a></div>
        <?php } ?>
    </div>


<?php if (isset($mssg) && $mssg == 1) { ?>
    <div class="col-xs-12 alert alert-success textaligncenter">Les modifications ont &eacute;t&eacute; enregistr&eacute;es</div>
    <br/>
<?php } ?>


<?php if (isset($mssg) && $mssg != 1 && $mssg != "") { ?>
    <div class="col-xs-12 alert alert-danger textaligncenter">Une erreur est constatée, Veuillez vérifier la conformité
        de vos données
    </div><br/>
<?php } ?>

    <div id="divFicheAnnonce" class="content col-xs-12 padding0" align="center">
        <form name="frmCreationAnnonce" id="frmCreationAnnonce" action="<?php if (isset($oAnnonce)) {
            echo site_url("front/annonce/modifAnnonce/$idCommercant");
        } else {
            echo site_url("front/annonce/creerAnnonce/$idCommercant");
        } ?>" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="annonce[annonce_commercant_id]" id="IdCommercant"
                   value="<?php echo $idCommercant; ?>"/>
            <input type="hidden" name="annonce[annonce_id]" id="IdAnnonce_id" value="<?php if (isset($oAnnonce)) {
                echo $oAnnonce->annonce_id;
            } ?>"/>
            <div class="annonce_blue_barr">
                Le détail de votre annonce
            </div>
            <span><?php if (isset($msg)) {
                    echo $msg;
                } ?></span>
            <div class="col-xs-12 greybg">
                <table width="100%" cellpadding="3" cellspacing="3" style="text-align:left; float:left;" align="left">
                    <!--<tr>
                        <td>
                            <label>Sous-rubrique : </label>
                        </td>
                        <td>
                            <select name="annonce[annonce_sous_rubriques_id]" id="idSousRubrique">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php //if(sizeof($colSousRubriques)) { ?>
                                    <?php //foreach($colSousRubriques as $objSousRubrique) { ?>
                                        <option value="<?php //echo $objSousRubrique->IdSousRubrique; ?>" <?php //if (isset($oAnnonce) && $oAnnonce->annonce_sous_rubrique_id == $objSousRubrique->IdSousRubrique) { echo "selected"; }?> ><?php //echo htmlentities($objSousRubrique->Nom); ?></option>
                                    <?php //} ?>
                                <?php //} ?>
                            </select>
                        </td>
                    </tr>-->


                    <tr>
                        <td>
                            <label>Titre : </label>
                        </td>
                        <td>
                            <textarea rows="2" cols="20" name="annonce[annonce_description_courte]"
                                      id="idDescriptionCourt"
                                      class="input_width form-control"><?php if (isset($oAnnonce)) {
                                    echo $oAnnonce->annonce_description_courte;
                                } ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>D&eacute;scription longue : </label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <textarea name="annonce[annonce_description_longue]" id="idDescriptionLong"
                                      class="form-control"><?php if (isset($oAnnonce)) {
                                    echo $oAnnonce->annonce_description_longue;
                                } ?></textarea>
                            <?php echo display_ckeditor($ckeditor0); ?>
                        </td>
                    </tr>
                    <tr>
                        <td style=" width:200px;">
                            <label>Etat : </label>
                        </td>
                        <td>
                            <select name="annonce[annonce_etat]" id="idEtat" class="input_width form-control">
                                <option value="c">-- Veuillez choisir --</option>
                                <option value="0" <?php if (isset($oAnnonce) && $oAnnonce->annonce_etat == 0) {
                                    echo "selected";
                                } ?>>Revente
                                </option>
                                <option value="1" <?php if (isset($oAnnonce) && $oAnnonce->annonce_etat == 1) {
                                    echo "selected";
                                } ?>>Neuf
                                </option>
                                <option value="2" <?php if (isset($oAnnonce) && $oAnnonce->annonce_etat == 2) {
                                    echo "selected";
                                } ?>>Service
                                </option>
                            </select>
                        </td>
                    </tr>
                    <!--<tr>
                        <td style=" width:200px;">
                            <label>Conditions de vente : </label>
                        </td>
                        <td>
                          <select name="annonce[annonce_condition_vente]" id="idConditionVente" class="input_width">
                                <option value="0">Choisissez vos conditions de réservation ou de règlement</option>
                                <option value="1" <?php //if (isset($oAnnonce) && $oAnnonce->annonce_condition_vente == 1) { echo "selected"; } ?>>Demande d'informations complémentaires</option>
                                <option value="2" <?php //if (isset($oAnnonce) && $oAnnonce->annonce_condition_vente == 2) { echo "selected"; } ?>>Réservation en ligne</option>
                            </select>
                        </td>
                    </tr>-->
                    <tr>
                        <td>
                            <label>Date debut : </label>
                        </td>
                        <td>
                            <input type="text" name="annonce[annonce_date_debut]" id="IdDateDebut"
                                   value="<?php if (isset($oAnnonce->annonce_date_debut) AND $oAnnonce->annonce_date_debut !=null AND $oAnnonce->annonce_date_debut !='' AND $oAnnonce->annonce_date_debut !="00/00/0000") {
                                       echo convert_Sqldate_to_Frenchdate($oAnnonce->annonce_date_debut);
                                   } ?>" class="input_width form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Date fin : </label>
                        </td>
                        <td>
                            <input type="text" name="annonce[annonce_date_fin]" id="IdDateFin"
                                   value="<?php if (isset($oAnnonce->annonce_date_fin) AND $oAnnonce->annonce_date_fin !=null AND $oAnnonce->annonce_date_fin !='' AND $oAnnonce->annonce_date_fin !="00/00/0000") {
                                       echo convert_Sqldate_to_Frenchdate($oAnnonce->annonce_date_fin);
                                   } ?>" class="input_width form-control"/>
                        </td>
                    </tr>
                    <!--<tr>
                        <td>
                            <label>Quantité proposée : </label>
                        </td>
                        <td>
                          <input type="text" name="annonce[annonce_quantite]" id="IdQuantite" value="<?php //if (isset($oAnnonce)) { echo $oAnnonce->annonce_quantite ; } ?>"  class="input_width form-control"/>
                        </td>
                    </tr>--><input type="hidden" name="annonce[annonce_quantite]" id="IdQuantite" value=""
                                   class="input_width"/>
                    <!--<tr>
                        <td>
                            <label>Durée de mise à disposition : </label>
                        </td>
                        <td>
                          <input type="text" name="annonce[annonce_livraison]" id="IdLivraison" value="<?php //if (isset($oAnnonce)) { echo $oAnnonce->annonce_livraison ; } ?>"  class="input_width form-control"/>
                        </td>
                    </tr>-->
                    <tr>
                        <td>
                            <label>Ancien prix (&euro;) : </label>
                        </td>
                        <td>
                            <input type="text" name="annonce[annonce_prix_neuf]" id="idPrixNeuf"
                                   value="<?php if (isset($oAnnonce)) {
                                       echo $oAnnonce->annonce_prix_neuf;
                                   } ?>" class="input_width form-control"/></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Prix (&euro;) : </label>
                        </td>
                        <td>
                            <input type="text" name="annonce[annonce_prix_solde]" id="IdPrixSolde"
                                   value="<?php if (isset($oAnnonce)) {
                                       echo $oAnnonce->annonce_prix_solde;
                                   } ?>" class="input_width form-control"/></td>
                    </tr>
                </table>
            </div>
            <div class="col-xs-12 annonce_blue_barr">Vos images</div>
            <div class="col-xs-12 greybg">
                <table width="100%" cellpadding="3" cellspacing="3" style="text-align:left; float:left;" align="left">


                    <?php if (isset($oAnnonce->annonce_id) && $oAnnonce->annonce_id != '0') { ?>

                        <tr>
                            <td>
                                <label>Photo 01 : </label>
                            </td>
                            <td>

                                <input type="hidden" name="annonce[annonce_photo1]" id="annonce_photo1Associe"
                                       value="<?php if (isset($oAnnonce)) {
                                           echo $oAnnonce->annonce_photo1;
                                       } ?>"/>
                                <div id="ArticlePhoto1_container">
                                    <?php if (!empty($oAnnonce->annonce_photo1)) { ?>
                                        <?php
                                        $path_img_gallery = 'application/resources/front/photoCommercant/imagesbank/' . $oInfoCommercant->user_ionauth_id . '/';
                                        $path_img_gallery_old = 'application/resources/front/images/';
                                        if (file_exists($path_img_gallery . $oAnnonce->annonce_photo1) == true) {
                                            echo '<img  src="' . base_url() . $path_img_gallery . $oAnnonce->annonce_photo1 . '" width="200"/>';
                                        } else {
                                            echo '<img  src="' . base_url() . $path_img_gallery_old . $oAnnonce->annonce_photo1 . '" width="200"/>';
                                        }
                                        ?>
                                        <a href="javascript:void(0);" class="btn btn-danger"
                                           onclick="deleteFile('<?php echo $oAnnonce->annonce_id; ?>','annonce_photo1');">Supprimer</a>
                                    <?php } else { ?>
                                        <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $oAnnonce->annonce_id . "-an-annonce_photo1"); ?>", "Bonplan image", "width=1045, height=675, scrollbars=yes");'
                                           href='javascript:void(0);' title="Photo 1" class="btn btn-default"
                                           id="ArticlePhoto1_link">Ajouter la photo 1</a>
                                    <?php } ?>
                                </div>
                                <div id="div_error_taille_Photo1"
                                     class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Photo 02 :</label>
                            </td>
                            <td>
                                <input type="hidden" name="annonce[annonce_photo2]" id="annonce_photo2Associe"
                                       value="<?php if (isset($oAnnonce)) {
                                           echo $oAnnonce->annonce_photo2;
                                       } ?>"/>
                                <div id="ArticlePhoto2_container">
                                    <?php if (!empty($oAnnonce->annonce_photo2)) { ?>
                                        <?php
                                        $path_img_gallery = 'application/resources/front/photoCommercant/imagesbank/' . $oInfoCommercant->user_ionauth_id . '/';
                                        $path_img_gallery_old = 'application/resources/front/images/';
                                        if (file_exists($path_img_gallery . $oAnnonce->annonce_photo2) == true) {
                                            echo '<img  src="' . base_url() . $path_img_gallery . $oAnnonce->annonce_photo2 . '" width="200"/>';
                                        } else {
                                            echo '<img  src="' . base_url() . $path_img_gallery_old . $oAnnonce->annonce_photo2 . '" width="200"/>';
                                        }
                                        ?>
                                        <a href="javascript:void(0);" class="btn btn-danger"
                                           onclick="deleteFile('<?php echo $oAnnonce->annonce_id; ?>','annonce_photo2');">Supprimer</a>
                                    <?php } else { ?>
                                        <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $oAnnonce->annonce_id . "-an-annonce_photo2"); ?>", "Bonplan image", "width=1045, height=675, scrollbars=yes");'
                                           href='javascript:void(0);' title="Photo 1" class="btn btn-default"
                                           id="ArticlePhoto2_link">Ajouter la photo 2</a>
                                    <?php } ?>
                                </div>
                                <div id="div_error_taille_Photo2"
                                     class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo2", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Photo 03 :</label>
                            </td>
                            <td>
                                <input type="hidden" name="annonce[annonce_photo3]" id="annonce_photo3Associe"
                                       value="<?php if (isset($oAnnonce)) {
                                           echo $oAnnonce->annonce_photo3;
                                       } ?>"/>
                                <div id="ArticlePhoto3_container">
                                    <?php if (!empty($oAnnonce->annonce_photo3)) { ?>
                                        <?php
                                        $path_img_gallery = 'application/resources/front/photoCommercant/imagesbank/' . $oInfoCommercant->user_ionauth_id . '/';
                                        $path_img_gallery_old = 'application/resources/front/images/';
                                        if (file_exists($path_img_gallery . $oAnnonce->annonce_photo3) == true) {
                                            echo '<img  src="' . base_url() . $path_img_gallery . $oAnnonce->annonce_photo3 . '" width="200"/>';
                                        } else {
                                            echo '<img  src="' . base_url() . $path_img_gallery_old . $oAnnonce->annonce_photo3 . '" width="200"/>';
                                        }
                                        ?>
                                        <a href="javascript:void(0);" class="btn btn-danger"
                                           onclick="deleteFile('<?php echo $oAnnonce->annonce_id; ?>','annonce_photo3');">Supprimer</a>
                                    <?php } else { ?>
                                        <a onclick='javascript:window.open("<?php echo site_url("media/index/_" . $oAnnonce->annonce_id . "-an-annonce_photo3"); ?>", "Bonplan image", "width=1045, height=675, scrollbars=yes");'
                                           href='javascript:void(0);' title="Photo 1" class="btn btn-default"
                                           id="ArticlePhoto3_link">Ajouter la photo 3</a>
                                    <?php } ?>
                                </div>
                                <div id="div_error_taille_Photo3"
                                     class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo3", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Photo 04 :</label>
                            </td>
                            <td>
                                <input type="hidden" name="annonce[annonce_photo4]" id="annonce_photo4Associe"
                                       value="<?php if (isset($oAnnonce)) {
                                           echo $oAnnonce->annonce_photo4;
                                       } ?>"/>
                                <div id="ArticlePhoto4_container">
                                    <?php if (!empty($oAnnonce->annonce_photo4)) { ?>
                                        <?php
                                        $path_img_gallery = 'application/resources/front/photoCommercant/imagesbank/' . $oInfoCommercant->user_ionauth_id . '/';
                                        $path_img_gallery_old = 'application/resources/front/images/';
                                        if (file_exists($path_img_gallery . $oAnnonce->annonce_photo4) == true) {
                                            echo '<img  src="' . base_url() . $path_img_gallery . $oAnnonce->annonce_photo4 . '" width="200"/>';
                                        } else {
                                            echo '<img  src="' . base_url() . $path_img_gallery_old . $oAnnonce->annonce_photo4 . '" width="200"/>';
                                        }
                                        ?>
                                        <a href="javascript:void(0);" class="btn btn-danger"
                                           onclick="deleteFile('<?php echo $oAnnonce->annonce_id; ?>','annonce_photo4');">Supprimer</a>
                                    <?php } else { ?>
                                        <a onclick='javascript:window.open("<?php echo site_url("media/index/_" . $oAnnonce->annonce_id . "-an-annonce_photo4"); ?>", "Bonplan image", "width=1045, height=675, scrollbars=yes");'
                                           href='javascript:void(0);' title="Photo 1" class="btn btn-default"
                                           id="ArticlePhoto4_link">Ajouter la photo 4</a>
                                    <?php } ?>
                                </div>
                                <div id="div_error_taille_Photo4"
                                     class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo4", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Type de slide:</label>
                            </td>
                            <td>
                                <label>
                                    <select class="form-control" name="annonce[slide_type]" id="slide_type">
                                        <option value="0">Normale</option>
                                        <option <?php if (isset($oAnnonce->slide_type) AND $oAnnonce->slide_type =='1'){echo 'selected';} ?> value="1">Avec loupe</option>
                                    </select>
                                </label>
                            </td>
                        </tr>

                    <?php } else { ?>

                        <tr>
                            <td colspan="2">
                                <div class="col-xs-12">
                                    <div class="alert alert-danger">Veuillez enregistrer votre annonce avant d'ajouter
                                        les images !
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>

                </table>
            </div>
            <div class="col-xs-12 annonce_blue_barr">
                Intégration de vos conditions
            </div>
            <div class="col-xs-12 greybg">
                <table width="100%" cellpadding="3" cellspacing="3" style="text-align:left; float:left;" align="left">

                    <tr>
                        <td colspan="2">

                            <script type="text/javascript">
                                $(function () {
                                    $("#checkbox_retrait_etablissement").click(function () {
                                        $("#Idretrait_etablissement").val("1");
                                        $("#Idlivraison_domicile").val("0");
                                    });
                                    $("#checkbox_livraison_domicile").click(function () {
                                        $("#Idlivraison_domicile").val("1");
                                        $("#Idretrait_etablissement").val("0");
                                    });
                                    $("#checkbox_module_paypal").click(function () {
                                        if ($('#checkbox_module_paypal').attr('checked')) {
                                            $("#Idmodule_paypal").val("1");
                                        } else {
                                            $("#Idmodule_paypal").val("0");
                                        }
                                    });
                                })
                            </script>


                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <br/>

                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100px;">
                                            <tr>
                                                <td>
                                                    <input type="radio" name="radiobtn_livraison"
                                                           id="checkbox_retrait_etablissement" <?php if (isset($oAnnonce->retrait_etablissement) && $oAnnonce->retrait_etablissement == "1") echo 'checked="checked"'; ?>/>
                                                    <input type="hidden" name="annonce[retrait_etablissement]"
                                                           id="Idretrait_etablissement"
                                                           value="<?php if (isset($oAnnonce->retrait_etablissement) && $oAnnonce->retrait_etablissement == "1") echo '1'; else echo '0'; ?>"/>
                                                </td>
                                                <td><label>Retrait dans notre<br/>etablissement</label></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="radio" name="radiobtn_livraison"
                                                           id="checkbox_livraison_domicile" <?php if (isset($oAnnonce->livraison_domicile) && $oAnnonce->livraison_domicile == "1") echo 'checked="checked"'; ?>/>
                                                    <input type="hidden" name="annonce[livraison_domicile]"
                                                           id="Idlivraison_domicile"
                                                           value="<?php if (isset($oAnnonce->livraison_domicile) && $oAnnonce->livraison_domicile == "1") echo '1'; else echo '0'; ?>"/>
                                                </td>
                                                <td><label>Livraison à domicile</label></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td><img
                                            src="<?php echo GetImagePath("front/"); ?>/btn_new/retrait_etablissement.png"
                                            id="loading1"/></td>
                                    <td><img src="<?php echo GetImagePath("front/"); ?>/btn_new/livraison_domicile.png"
                                             id="loading1"/></td>
                                </tr>
                            </table>


                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-xs-12 annonce_blue_barr">
                <input type="checkbox" name="checkbox_module_paypal"
                       id="checkbox_module_paypal" <?php if (isset($oAnnonce->module_paypal) && $oAnnonce->module_paypal == "1") echo 'checked="checked"'; ?>/>
                <input type="hidden" name="annonce[module_paypal]" id="Idmodule_paypal"
                       value="<?php if (isset($oAnnonce->module_paypal) && $oAnnonce->module_paypal == "1") echo '1'; else echo '0'; ?>"/>
                <label>Valider le module PAYPAL pour cette annonce.</label>
            </div>
            <div class="col-xs-12 greybg textalignleft">


                <div class="col-xs-12 padding0">
                    <div class="col-xs-9 padding0">Comment ouvrir un compte PAYPAL et intégrer un bouton de paiement
                        automatique PAYPAL à ce bon plan ?
                    </div>
                    <div class="col-xs-3 padding0"><a
                            href="https://www.paypal.com/ci/cgi-bin/webscr?cmd=_singleitem-intro-outside"
                            target="_blank"><img
                                src="<?php echo GetImagePath("privicarte/"); ?>/wpe77cb282_06.png"/></a></div>
                </div>
                <div class="col-xs-12 padding0">
                    Intégration de votre bouton Paypal (Copier le code html fournit par PAYPAL).<br/>
                    Le bouton apparaîtra automatiquement sur votre page.
                </div>
                <div class="col-xs-12 padding0">
                    <div class="col-xs-3 padding0"></div>
                    <div class="col-xs-9 padding0">
					 <textarea name="annonce_module_paypal_btnpaypal" id="module_paypal_btnpaypal" style="height:120px;"
                               class="form-control">
                         <?php if (isset($oAnnonce->module_paypal_btnpaypal) && $oAnnonce->module_paypal_btnpaypal != "") echo $oAnnonce->module_paypal_btnpaypal; ?>
                     </textarea>
                    </div>
                </div>
                <div class="col-xs-12 padding0">
                    Si vous choisissez d’intégrer un panier, nous vous invitons à intégrer le code html PAYPAL du bouton
                    afficher le panier » Le bouton apparaîtra automatique-ment sur votre page.
                </div>
                <div class="col-xs-12 padding0">
                    <div class="col-xs-3 padding0"></div>
                    <div class="col-xs-9 padding0">
                        <textarea name="annonce_module_paypal_panierpaypal" id="module_paypal_panierpaypal"
                                  style="height:120px;" class="form-control">
                            <?php if (isset($oAnnonce->module_paypal_panierpaypal) && $oAnnonce->module_paypal_panierpaypal != "") echo $oAnnonce->module_paypal_panierpaypal; ?>
                        </textarea>
                    </div>
                </div>


            </div>

            <div class="col-xs-12 annonce_blue_barr">

            </div>


            <div class="col-xs-12 padding0">
                <a href="javascript:void(0);" id="submitFormAnnonce" class="admin_bp_green_btn"
                   onclick="javascript:testFormAnnonce();">Validation</a>
            </div>

        </form>

        <?php if (isset($oAnnonce)) { ?>
            <div class="col-xs-12 textaligncenter">
                <a href="<?php echo site_url($oInfoCommercant->nom_url . '/detail_annonce-' . $oAnnonce->annonce_id) ?>"
                   target="_blank"
                   onclick="var w = window.open(this.href,'_blank','width=1200,height=1200,menubar=no,resizable=no,scrollbars=yes,status=no,toolbar=no'); if( w != null ){ w.focus(); }; return false;"
                   alt="show">
                    <img src="<?php echo base_url(); ?>application/resources/privicarte/images/visualisation.png"
                         alt="logo" title="" style="z-index:1;">
                </a>
            </div>
        <?php } ?>

    </div>

<?php $this->load->view("frontAout2013/includes/footer_mini_2"); ?>