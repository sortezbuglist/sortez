<style type="text/css">
@media (max-width: 670px) {
  #accordion img {
  	width:100% !important;
	height:auto !important;
	margin:0 !important;
	padding: 10px 0 !important;
  }
}
.panel-heading a {
	line-height: 23.00px;
	font-family: "Arial", sans-serif;
	font-style: normal;
	font-weight: normal;
	color: #000000;
	background-color: transparent;
	text-decoration: none;
	font-variant: normal;
	font-size: 18.7px;
	vertical-align: 0;
}
.panel-heading h4 {
	text-align:center;
	font-size: 18.7px !important;
}
.panel-heading {
	background-image: url("<?php echo GetImagePath("privicarte/");?>/bg_accordion_title.png") !important;
    background-repeat: no-repeat !important;
    border: medium none !important;
    height: 76px;
    padding-top: 28px;
	background-size: 100% 76px;
}
.panel-group .panel, .panel-body {
	border: none !important;
}
</style>




<?php 

if (isset($pageglissiere) && $pageglissiere == "presentation") {

	$objComm_Glissiere = $this->mdlglissiere->GetByIdCommercant($oInfoCommercant->IdCommercant);
	if (isset($objComm_Glissiere->isActive_presentation_1)){
        $glissiere1_activation = $objComm_Glissiere->isActive_presentation_1;
        $glissiere1_titre = $objComm_Glissiere->presentation_1_titre;
        $glissiere1_contenu = $objComm_Glissiere->presentation_1_contenu;

        $glissiere2_activation = $objComm_Glissiere->isActive_presentation_2;
        $glissiere2_titre = $objComm_Glissiere->presentation_2_titre;
        $glissiere2_contenu = $objComm_Glissiere->presentation_2_contenu;

        $glissiere3_activation = $objComm_Glissiere->isActive_presentation_3;
        $glissiere3_titre = $objComm_Glissiere->presentation_3_titre;
        $glissiere3_contenu = $objComm_Glissiere->presentation_3_contenu;

        $glissiere4_activation = $objComm_Glissiere->isActive_presentation_4;
        $glissiere4_titre = $objComm_Glissiere->presentation_4_titre;
        $glissiere4_contenu = $objComm_Glissiere->presentation_4_contenu;
    }


} else if (isset($pageglissiere) && $pageglissiere == "page1") {

	$objComm_Glissiere = $this->mdlglissiere->GetByIdCommercant($oInfoCommercant->IdCommercant);
	
	$glissiere1_activation = $objComm_Glissiere->isActive_page1_1;
	$glissiere1_titre = $objComm_Glissiere->page1_1_titre;
	$glissiere1_contenu = $objComm_Glissiere->page1_1_contenu;
	
	$glissiere2_activation = $objComm_Glissiere->isActive_page1_2;
	$glissiere2_titre = $objComm_Glissiere->page1_2_titre;
	$glissiere2_contenu = $objComm_Glissiere->page1_2_contenu;
	
	$glissiere3_activation = $objComm_Glissiere->isActive_page1_3;
	$glissiere3_titre = $objComm_Glissiere->page1_3_titre;
	$glissiere3_contenu = $objComm_Glissiere->page1_3_contenu;
	
	$glissiere4_activation = $objComm_Glissiere->isActive_page1_4;
	$glissiere4_titre = $objComm_Glissiere->page1_4_titre;
	$glissiere4_contenu = $objComm_Glissiere->page1_4_contenu;

} else if (isset($pageglissiere) && $pageglissiere == "page2") {

	$objComm_Glissiere = $this->mdlglissiere->GetByIdCommercant($oInfoCommercant->IdCommercant);
	
	$glissiere1_activation = $objComm_Glissiere->isActive_page2_1;
	$glissiere1_titre = $objComm_Glissiere->page2_1_titre;
	$glissiere1_contenu = $objComm_Glissiere->page2_1_contenu;
	
	$glissiere2_activation = $objComm_Glissiere->isActive_page2_2;
	$glissiere2_titre = $objComm_Glissiere->page2_2_titre;
	$glissiere2_contenu = $objComm_Glissiere->page2_2_contenu;
	
	$glissiere3_activation = $objComm_Glissiere->isActive_page2_3;
	$glissiere3_titre = $objComm_Glissiere->page2_3_titre;
	$glissiere3_contenu = $objComm_Glissiere->page2_3_contenu;
	
	$glissiere4_activation = $objComm_Glissiere->isActive_page2_4;
	$glissiere4_titre = $objComm_Glissiere->page2_4_titre;
	$glissiere4_contenu = $objComm_Glissiere->page2_4_contenu;

}

?>





<div class="col-lg-12 padding0" style="background-color: #ffffff; padding-top: 15px !important; text-align:justify !important;">
    <div class="panel-group" id="accordion">
    
    
    
    	<?php if (isset($glissiere1_activation) && $glissiere1_activation == "1") { ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                <h4 class="panel-title">
                    <?php echo $glissiere1_titre; ?>
                </h4>
                </a>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p><?php echo $glissiere1_contenu; ?></p>
                </div>
            </div>
        </div>
        <?php } ?>
        
        
        
        <?php if (isset($glissiere2_activation) && $glissiere2_activation == "1") { ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                <h4 class="panel-title">
                    <?php echo $glissiere2_titre; ?>
                </h4>
                </a>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php echo $glissiere2_contenu; ?></p>
                </div>
            </div>
        </div>
        <?php } ?>
        
        
        
        <?php if (isset($glissiere3_activation) && $glissiere3_activation == "1") { ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                <h4 class="panel-title">
                    <?php echo $glissiere3_titre; ?>
                </h4>
                </a>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php echo $glissiere3_contenu; ?></p>
                </div>
            </div>
        </div>
        <?php } ?>
        
        
        
        
        <?php if (isset($glissiere4_activation) && $glissiere4_activation == "1") { ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                <h4 class="panel-title">
                    <?php echo $glissiere4_titre; ?>
                </h4>
                </a>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php echo $glissiere4_contenu; ?></p>
                </div>
            </div>
        </div>
        <?php } ?>
        
        
        
        
    </div>
</div>