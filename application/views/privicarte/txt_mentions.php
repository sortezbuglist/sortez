<div class="col-xs-12 padding0">
    <ol style="font-size: 17px; line-height: 24px; font-family:futura-lt-w01-book,sans-serif; letter-spacing:normal;">
        <li>Vous complétez l'ensemble des champs ;</li>
        <li>dès validation et acceptation de votre demande, votre page basique sera créée ;</li>
        <li>par la suite, si vous désirez modifier vos données, il vous suffira de vous connecter sur votre compte en cliquant sur le bouton "Mon compte" présent sur les bannières des pages de notre site et en précisant votre identifiant et mot de passe pour accéder à votre formulaire intuitif.</li>
        <!-- <li>Sur l’interface intuitive, vous int&eacute;grez  imm&eacute;diatement vos donn&eacute;es (textes, photos, vid&eacute;o, liens, PDF, formulaires, banni&egrave;re…) pour construire votre page informative.</li>
        <li>A tout moment, vous visualisez  et perfectionner le r&eacute;sultat.</li>
        <li>Votre page finalis&eacute;e, vous demandez la validation de votre r&eacute;f&eacute;rencement.</li>
        <li>Vos donn&eacute;es seront alors d&eacute;finitivement int&eacute;gr&eacute;es sur l’annuaire complet de sortez.org.</li> -->
    </ol>
</div>
<div style='
    font-family: "Arial",sans-serif;
    font-size: 16px; margin-top:20px;
    line-height: 1.23em;
    display: table;
    margin-left: 30px;
    '>
    Attention : notre service peut refuser sans en donner la justification l’ouverture d’un compte qui ne correspondrait pas à son éthique.(<a href="/mentions-legales.html"
                                          target="_blank"
                                          style="font-weight:normal;">Voir nos conditions g&eacute;n&eacute;rales</a>).
</div>