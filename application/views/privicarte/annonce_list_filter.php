<script type="text/javascript">
function check_zMotCle_annonce_focus() {
    var searchvalue = $("#inputString_zMotCle").val();
	if (searchvalue=="RECHERCHER") $("#inputString_zMotCle").val("");
	//alert("focus");
}	
function check_zMotCle_annonce_blur() {	
	var searchvalue = $("#inputString_zMotCle").val();
	if (searchvalue=="") $("#inputString_zMotCle").val("RECHERCHER");
	//alert("blur");
}  
</script>


<div class="col-sm-4 paddingleft0">   
    <?php 
	$this_session =& get_instance();
	$this_session->load->library('session');
	$session_inputStringQuandHidden_verification = $this_session->session->userdata('inputStringQuandHidden_x');
	$session_zMotCle_verification = $this_session->session->userdata('zMotCle_x');
	?>
    
    <select id="inputStringEtatByHidden" size="1" name="inputStringEtatByHidden" class="input_filter_agenda" style="text-indent:30px;">
    	<option value="">Toutes les offres</option>
        <option value="1" <?php if (isset($iEtat) && $iEtat == '1') {?>selected="selected"<?php }?>>Annonces "Neuf"</option>
        <option value="0" <?php if (isset($iEtat) && $iEtat == '0') {?>selected="selected"<?php }?>>Annonces "Occasion"</option>
        <option value="2" <?php if (isset($iEtat) && $iEtat == '2') {?>selected="selected"<?php }?>>Annonces "Services"</option>
    </select>
    
    
</div>

<div class="col-sm-4 paddingleft0">
	<select id="inputStringOrderByHidden_partenaires" size="1" name="inputStringOrderByHidden_partenaires" class="input_filter_agenda" style="text-indent:125px;">
        <option value="0">TRIER</option>
        <option value="1" <?php if (isset($iOrderBy) && $iOrderBy == '1') {?>selected="selected"<?php }?>>Les partenaires les plus récents</option>
        <option value="2" <?php if (isset($iOrderBy) && $iOrderBy == '2') {?>selected="selected"<?php }?>>Les partenaires les plus visitées</option>
        <option value="3" <?php if (isset($iOrderBy) && $iOrderBy == '3') {?>selected="selected"<?php }?>>Les partenaires les plus anciens</option>
    </select>
</div>
    
<div class="col-sm-4 paddingleft0">    
	<input type="text" name="inputString_zMotCle" id="inputString_zMotCle" value="<?php if (isset($session_zMotCle_verification) && $session_zMotCle_verification != "") echo $session_zMotCle_verification; else echo "RECHERCHER"; ?>" class="input_filter_agenda" style="width:80%; text-align:center;" onfocus="javascript:check_zMotCle_annonce_focus();" onblur="javascript:check_zMotCle_annonce_blur();"/><button name="zMotCle_to_check_btn" id="inputString_zMotCle_submit"  class="button_filter_agenda_ok"><img src="<?php echo GetImagePath("privicarte/"); ?>/ok_rechercer.png" alt="rechercher"/></button>
</div>