<?php $data["zTitle"] = 'Accueil';?>
<?php $this->load->view("sortez/includes/header_frontoffice_com", $data);?>
<?php
$thisss = &get_instance();
$thisss->load->library('ion_auth');
if ($thisss->ion_auth->in_group(3)) {redirect("front/professionnels/fiche/" . $iduser . "/?page_data=coordonnees");}
?>
<link rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
      crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<style type="text/css">

    #sous_platinum:hover {
        color: white;
        background-color: rgb(220, 24, 142);
    }

    #sous_platinum {
        background-color: rgb(9, 157, 88);
        font-size: 18px;
        color: white
    }

    #superadmin:hover {

        background-color: rgb(220, 24, 142);

    }

    #superadmin {
        background-color: rgb(0, 128, 0);

    }

    #superadmin:visited {
        color: white;

    }

    #lien {
        color: white !important;
    }

    @media screen and (max-width: 768px) {
        .container {
            width: 100% !important;
        }
    }

    li {
        color: grey;
    }

    a {
        text-decoration: none;
        color: black !important;
    }

    a:hover {
        text-decoration: none;
        color: black
    }

    a:visited {
        text-decoration: none;
        color: black
    }

    body {
        font-family: 'Futura Md', sans-serif !important;
    }

    li {
        padding-top: 15px;
        font-family: "Futura Md", sans-serif;
        font-size: 15px;
    }

    .greyed {
        background-color: rgb(225, 225, 225);
        height: 100%;
    }
</style>

<script>
    function btn_pro_fidelity() {
        window.open("<?php echo site_url('front/fidelity/mes_parametres'); ?>", "_blank", "toolbar=no, scrollbars=yes, resizable=yes, top=500, left=500, width=650, height=800");
    }

    function btn_pro_carte() {
        window.open("<?php echo site_url('front/fidelity/contenupro'); ?>", "_blank", "toolbar=no, scrollbars=yes, resizable=yes, top=500, left=500, width=650, height=800");
    }
</script>

<div class="container m-auto"
     style="width: 60%">
    <div class="row p-4">
        <?php if (isset($from_super_admin_account) && $from_super_admin_account == '1') {?>
            <div class="col-lg-12 text-center ">
                <div class="m-auto pt-2 pb-2 mb-2 text-center"
                     id="superadmin"
                     style="font-size: 18px;">
                    <a id="lien"
                       href="<?php echo site_url("front/utilisateur/back_super_admin_account") . "/" . $iduser; ?>">
                        Revenir
                        &agrave;
                        la
                        gestion
                        Super-Admin</a>
                </div>
            </div>
        <?php }?>
    </div>
    <div class="row text-center">
        <div class="col-lg-12 pt-4">
            <div>
                <img src="<?php echo base_url('assets/img/logo_pro.png') ?>">
            </div>
            <div class="col-12 p-4"
                 style="font-size: 20px;font-weight: bold">
                BIENVENUE <?php echo $toCommercant->Prenom . ' ' . $toCommercant->Nom; ?> </div>
        </div><? //var_dump($user_ion_auth);?>
    </div>
    <div class="row pt-5 pb-4">
        <div class="col-12"
             style="font-family: 'Futura Md', sans-serif;font-style: normal;
    font-weight: normal;
    color: #000000;font-size: 15px;">
            <p>
                Nous
                vous
                remercions
                de
                votre
                inscription
                à
                l’abonnement
                Premium.</p>
            <p class="pt-4">
                Sur
                l’interface
                intuitive,
                vous
                intégrez
                immédiatement
                vos
                données
                (textes,
                photos,
                vidéo,
                liens,
                PDF,
                formulaires,
                bannière…)
                pour
                construire
                votre
                page
                informative.</p>
            <p class="pt-4">
                A
                tout
                moment,
                vous
                visualisez
                et
                perfectionner
                le
                résultat.</p>
            <p class="pt-4">
                Votre
                page
                finalisée,
                vous
                demandez
                la
                validation
                de
                votre
                référencement.
                Vos
                données
                seront
                alors
                définitivement
                intégrées
                sur
                l’annuaire
                complet
                de
                sortez.org.
                Attention
                :
                Sortez.org
                peut
                refuser
                sans
                en
                donner
                la
                justification
                l’ouverture
                d’un
                compte
                qui
                ne
                correspondrait
                pas
                à
                son
                éthique.
                (Voir
                nos
                conditions
                générales).</p>
            <p class="pt-4">
                Par
                la
                suite,
                sur
                cette
                même
                interface,
                vous
                pouvez
                enrichir
                votre
                abonnement
                de
                base
                avec
                la
                souscription
                à
                des
                modules
                optionnelles
                complémentaires.</p>
            <p class="pt-4">
                Si
                vous
                rencontrez
                une
                difficulté
                ou
                bug,
                adressez
                nous
                une
                alerte,
                notre
                service
                vous
                répondra
                sous
                48h00…</p>
        </div>
    </div>
    <div class="row pt-4">
        <div class="col-12">
            <div id="sous_platinum"
                 class="btn w-100 text-center pt-2 pb-2 pl-0 pr-0"
                 style="">
                Validation
                de
                mon
                référencement
                Premium
                et
                accès
                aux
                modules
                complémentaires
            </div>
        </div>
        <input type="hidden"
               value="0"
               id="is_showed_form">
        <div class="col-12 pr-4 pl-4 pt-0"
             id="New_module"></div>
    </div>
    <div class="row pt-4">
        <div class="col-6">
            <a href="#"
               onclick='javascript:window.open("<?php echo site_url('contact'); ?>", "contact", "width=550, height=500, scrollbars=yes");'
               title="Contactez-nous">
                <div class="col-12 pt-2 pb-2 mb-4 text-center"
                     style="background-color: red;font-size: 18px;color: white;">
                    UN
                    PROBLÈME
                    ?
                    CONTACTEZ-NOUS
                    !
                </div>
            </a>
        </div>
        <div class="col-6">
            <a href="<?php echo site_url("connexion/sortir"); ?>">
                <div class="col-12 pt-2 pb-2 mb-4 text-center"
                     style="background-color: red;font-size: 18px;color: white;">
                    DÉCONNEXION
                </div>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="greyed">
                <div class="col-12 text-center pt-4  pb-4"
                     style="font-size: 18px;font-weight: bold;">
                    MON
                    ABONNEMENT
                    PRÉMIUM
                </div>
                <div class="col-12 pb-4">
                    <img class="img-fluid"
                         style="width: 100%;"
                         src="<?php echo base_url('assets/img/ijkl.jpg') ?>">
                </div>
                <div class="col-12">
                    <ul>
                        <li><?php if ($thisss->ion_auth->in_group(4)) {?>
                            <a href="<?php echo site_url("front/professionnels/fiche") . "/" . $iduser . "/?page_data=coordonnees"; ?>"><?php }?>
                                Mon
                                abonnement
                                et
                                mes
                                coordonnées<?php if ($thisss->ion_auth->in_group(4)) {?></a><?php }?>
                        </li>
                        <?//var_dump($toCommercant->referencement_click);?>
                        <li><? if ($toCommercant->referencement_click == "1"){?>
                            <a href="<?php echo site_url("soutenons/admin/Gestion_compte"); ?>"><?php }?>
                            Formulaire
                            livré
                            ou à emporter<? if ($toCommercant->referencement_click == "1"){?></a><?php }?>
                        </li>
                        

                        <li><?php if ($thisss->ion_auth->in_group(4)) {?>
                            <a href="<?php echo site_url("front/professionnels/fiche") . "/" . $iduser . "/?page_data=contenus"; ?>"><?php }?>
                                Mon
                                contenu
                                «
                                Premium
                                »<?php if ($thisss->ion_auth->in_group(4)) {?></a><?php }?>
                        </li>

                        <!-- <li><a href="<?php //echo site_url("soutenons/admin/Gestion_compte") ?>">
                                Formulaire
                                en
                                ligne
                            </a>
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-6 ">
            <div class="greyed">
                <div class="col-12 pt-4  text-center pb-4"
                     style="font-size: 18px;font-weight: bold;">
                    MON
                    ABONNEMENT
                    PLATINIUM
                </div>
                <div class="col-12 pb-4">
                    <img class="img-fluid"
                         style="width: 100%;"
                         src="<?php echo base_url('assets/img/plat_img.jpg') ?>">
                </div>
                <div class="col-12">
                    <ul>
                        <li><?php if ($thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7)) {?>
                            <a href="<?php echo site_url("front/professionnels/fiche") . "/" . $iduser . "/?page_data=coordonnees"; ?>"><?php }?>
                                Mon
                                abonnement
                                et
                                mes
                                coordonnées<?php if ($thisss->ion_auth->in_group(5)) {?></a><?php }?>
                        </li>
                        <li><?php if ($thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7)) {?>
                            <a href="<?php echo site_url("front/professionnels/fiche") . "/" . $iduser . "/?page_data=contenus"; ?>"><?php }?>
                                Mon
                                contenu
                                «
                                Platinium
                                »<?php if ($thisss->ion_auth->in_group(5)) {?></a><?php }?>
                        </li>
                        <li><?php if ($thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7)) {?>
                            <a href="<?php echo site_url('front/gestion_livre_dor/liste/' . $toCommercant->IdCommercant); ?>"><?php }?>
                                Les
                                avis,
                                les
                                commentaires,
                                livre
                                d’or<?php if ($thisss->ion_auth->in_group(5)) {?></a><?php }?>
                        </li>
                        <li><?php if ($thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7)) {?>
                            <a href="<?php echo site_url('front/gestion_livre_dor/comment/' . $toCommercant->IdCommercant); ?>"><?php }?>
                                Les
                                avis
                                «
                                Google
                                »<?php if ($thisss->ion_auth->in_group(5)) {?></a><?php }?>
                        </li>
                        <li><?php if ($thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and $toCommercant->referencement_news == 1) {?>
                            <a href="<?php echo site_url('admin/Newsletter_commercant_backoffice/index') ?>"><?php }?>
                                La
                                gestion
                                des
                                newsletters<?php if ($thisss->ion_auth->in_group(5) and $toCommercant->referencement_news == 1) {?></a><?php }?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-6">
            <div class="greyed">
                <div class="col-12 text-center pt-4  pb-4"
                     style="font-size: 18px;font-weight: bold;">
                    L'ACTUALITÉ
                </div>
                <div class="col-12 pb-4">
                    <img class="img-fluid"
                         style="width: 100%;"
                         src="<?php echo base_url('assets/img/art_b.jpg') ?>">
                </div>
                <div class="col-12">
                    <ul>
                        <li><?php if (($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7)) && $toCommercant->blog == 1) {?>
                            <a href="<?php echo site_url('front/articles/liste/' . $toCommercant->IdCommercant); ?>"><?php }?>
                                Gestion
                                de
                                mes
                                articles <?php if (($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5)) && $toCommercant->blog == 1) {?></a><?php }?>
                        </li>
                        <li>
                            <?php if ($toCommercant->blog == 1){?>
                            <a href="<?php echo site_url("front/packarticle"); ?>"
                               class=""><?php }?>Mon
                                pack
                                articles<?php if ($toCommercant->blog == 1){?></a><?php }?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-6 ">
            <div class="greyed">
                <div class="col-12 pt-4  text-center pb-4"
                     style="font-size: 18px;font-weight: bold;">
                    L'AGENDA
                </div>
                <div class="col-12 pb-4">
                    <img class="img-fluid"
                         style="width: 100%;"
                         src="<?php echo base_url('assets/img/AB8B.jpg') ?>">
                </div>
                <div class="col-12">
                    <ul>
                        <li><?php if ($toCommercant->agenda == 1) {?>
                            <a href="<?php echo site_url('front/utilisateur/agenda/' . $toCommercant->IdCommercant); ?>"><?php }?>
                                Gestion
                                de
                                mes
                                événements<?php if ($toCommercant->agenda == 1) {?></a><?php }?>
                        </li>

                        <li><?php if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and $toCommercant->festival == 1) {?>
                            <a
                                    href="<?php echo site_url('front/festival/liste/' . $toCommercant->IdCommercant); ?>"><?php }?>
                                Le
                                module
                                «
                                festival
                                »<?php if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and $toCommercant->festival == 1) {?></a><?php }?>
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-6">
            <div class="greyed">
                <div class="col-12 text-center pt-4  pb-4"
                     style="font-size: 18px;font-weight: bold;">
                    LA
                    BOUTIQUE
                    EN
                    LIGNE
                </div>
                <div class="col-12 pb-4">
                    <img class="img-fluid"
                         style="width: 100%;"
                         src="<?php echo base_url('assets/img/bout_b.jpg') ?>">
                </div>
                <div class="col-12">
                    <ul>
                        <li><?php if ($toCommercant->annonce == 1) {?>
                            <a href="<?php echo site_url('front/annonce/listeMesAnnonces/' . $toCommercant->IdCommercant); ?>"><?php }?>
                                Gestion
                                de
                                mes
                                annonces<?php if ($toCommercant->annonce == 1) {?></a><?php }?>
                        </li>
                        <li>
                            Mon
                            compte
                            Paypal
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-6 ">
            <div class="greyed">
                <div class="col-12 pt-4  text-center pb-4"
                     style="font-size: 18px;font-weight: bold;">
                    LES
                    ACTIONS
                    PROMOTIONNELLES
                </div>
                <div class="col-12 pb-4">
                    <img class="img-fluid"
                         style="width: 100%;"
                         src="<?php echo base_url('assets/img/PROMO.jpg') ?>">
                </div>
                <div class="col-12">
                    <ul>
                        <li><?php if (isset($toCommercant->bonplan) && $toCommercant->bonplan == "1") {?>
                            <a href="<?php echo site_url('front/bonplan/listeMesBonplans/' . $toCommercant->IdCommercant); ?>"><?php }?>
                                Gestion
                                de
                                mes
                                bons
                                plans<?php if (isset($toCommercant->bonplan) && $toCommercant->bonplan == "1") {?></a><?php }?>
                        </li>
                        <li><?php if (($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7)) and isset($toCommercant->bonplan_export) && $toCommercant->bonplan_export == "1") {?>
                            <a href="<?php echo site_url('bonplan/personnalisation'); ?>"><?php }?>
                                Le
                                module
                                export
                                «
                                Bons
                                Plans
                                »<?php if (($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7)) and isset($toCommercant->bonplan_export) && $toCommercant->bonplan_export == "1") {?></a><?php }?>
                        </li>
                        <li><?php if ($thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) && $toCommercant->referencement_fidelite == 1) {?>
                            <a href=""
                               onclick="btn_pro_fidelity()"><?php }?>
                                Mes
                                conditions
                                de
                                fidélisation <?php if ($thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) && $toCommercant->referencement_fidelite == 1) {?> </a><?php }?>
                        </li>
                        <li><?php if ($thisss->ion_auth->in_group(5) or $thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(7)) {?>
                            <a href=""
                               onclick="btn_pro_carte()"><?php }?>
                                La
                                gestion
                                de la carte Fidélité client<?php if ($thisss->ion_auth->in_group(5) or $thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(7)) {?> </a><?php }?>
                        </li>
                        <li><?php  if (isset($toCommercant->bonplan) && $toCommercant->bonplan == "1") {?>
                            <a href="<?php echo site_url('front/bonplan/view_personalise_deal/' . $toCommercant->IdCommercant); ?>"><?php }?>
                               Valider le bouton commun bon plan et fidélité<?php if (isset($toCommercant->bonplan) && $toCommercant->bonplan == "1") {?></a><?php }?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-6">
            <div class="greyed">
                <div class="col-12 text-center pt-4  pb-4"
                     style="font-size: 18px;font-weight: bold;">
                    LES
                    PACKS
                    «
                    EXPORTS
                    »
                </div>
                <div class="col-12 pb-4">
                    <img class="img-fluid"
                         style="width: 100%;"
                         src="<?php echo base_url('assets/img/EXP8B.jpg') ?>">
                </div>
                <div class="col-12">
                    <ul>
                        <li><?php if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and isset($toCommercant->article_export) && $toCommercant->article_export  == "1") {?>
                            <a href="<?php echo site_url('article/personnalisation'); ?>"><?php }?>
                                Le
                                pack
                                export
                                «
                                article
                                »<?php if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and isset($toCommercant->article_export) && $toCommercant->article_export  == "1") {?></a><?php }?>
                        </li>
                        <li><?php if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and isset($toCommercant->agenda_export) && $toCommercant->agenda_export == "1") {?>
                            <a href="<?php echo site_url('agenda/personnalisation'); ?>"><?php }?>
                                Le
                                pack
                                export
                                «
                                événement
                                »<?php if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and isset($toCommercant->agenda_export) && $toCommercant->agenda_export == "1") {?></a><?php }?>
                        </li>
                        <li><?php if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and isset($toCommercant->festival_export) && $toCommercant->festival_export == "1") {?>
                            <a href="<?php echo site_url('front/festival/personnalisation'); ?>"><?php }?>
                                La
                                pack
                                export
                                «
                                festival
                                »<?php if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and isset($toCommercant->festival_export) && $toCommercant->festival_export == "1") {?></a><?php }?>
                        </li>
                        <li><?php if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and isset($toCommercant->annonce_export) && $toCommercant->annonce_export == "1") {?>
                            <a href="<?php echo site_url('annonce/personnalisation'); ?>"><?php }?>
                                Le
                                pack
                                export
                                «
                                boutique
                                »<?php if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and isset($toCommercant->annonce_export) && $toCommercant->annonce_export == "1") {?></a><?php }?>
                        </li>
                        <li><?php if (isset($toCommercant->bonplan_export) && $toCommercant->bonplan_export == "1") {?>
                            <a href="<?php echo site_url('bonplan/personnalisation'); ?>"><?php }?>
                                Le
                                pack
                                export
                                «
                                bon-plan
                                »<?php if (isset($toCommercant->bonplan_export) && $toCommercant->bonplan_export == "1") {?></a><?php }?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-6 ">
            <div class="greyed">
                <div class="col-12 pt-4  text-center pb-4"
                     style="font-size: 18px;font-weight: bold;">
                    LE
                    WEB
                    SCRAPPING
                </div>
                <div class="col-12 pb-4">
                    <img class="img-fluid"
                         style="width: 100%;"
                         src="<?php echo base_url('assets/img/WEB8B.jpg') ?>">
                </div>
                <div class="col-12">
                    <p style="font-size: 12px;">
                        Le
                        Web
                        Scraping
                        permet
                        d'extraire
                        des
                        informations
                        provenant
                        de
                        sites
                        web.
                        Nous
                        importons
                        vos
                        données
                        html
                        pour
                        les
                        transformer
                        et
                        les
                        intégrer
                        sur
                        nos
                        bases
                        informatives.
                        Cette
                        technique
                        automatisée
                        évite
                        la
                        création
                        de
                        données
                        manuelles
                        ainsi
                        que
                        les
                        mises
                        à
                        jour.</p>
                    <ul>
                        <li>
                            Demande
                            de
                            devis
                        </li>
                        <li><?php if ($thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and isset($toCommercant->agenda_export) && $toCommercant->agenda_export == "1") {?>
                            <a href="<?php echo site_url('article/revue_presse') ?>"><?php }?>
                                Mon
                                compte
                                web
                                scrapping<?php if ($thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and isset($toCommercant->agenda_export) && $toCommercant->agenda_export == "1") {?></a><?php }?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-6">
            <div class="greyed">
                <div class="col-12 text-center pt-4  pb-4"
                     style="font-size: 18px;font-weight: bold;">
                    LE
                    PACK
                    RESTAURANT
                </div>
                <div class="col-12 pb-4">
                    <img class="img-fluid"
                         style="width: 100%;"
                         src="<?php echo base_url('assets/img/RETO8B.jpg') ?>">
                </div>
                <div class="col-12">
                    <ul>
                        <li><?php if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and $toCommercant->referencement_resto == 1) {?>
                            <a href="<?php echo site_url('admin/menu/Info_menu_commercant'); ?>"><?php }?>
                            Menu
                            Digital
                            <?php if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and $toCommercant->referencement_resto == 1) {?></a><?php }?>
                        </li>
                        <li><?php if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and $toCommercant->referencement_resto == 1) {?>
                            <a href="<?php echo site_url('front/Plat_du_jour/listePlatDuJour') ?>"><?php }?>
                                Le
                                module
                                «
                                plats
                                du
                                jour
                                »<?php if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and $toCommercant->referencement_resto == 1) {?></a><?php }?>
                        </li>
                       <!--  <li><?php //if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and $toCommercant->referencement_resto == 1) {?>
                            <a href="<?php/// echo site_url('front/Plat_du_jour/listePlatDuJour_menu_carte') ?>"><?php //}?>
                                L’intégration
                                de
                                vos
                                menus
                                et
                                cartes<?php //if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and $toCommercant->referencement_resto == 1) {?></a><?php// }?>
                        </li> -->
                        <li><?php if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and $toCommercant->referencement_resto == 1) {?>
                            <a href="<?php echo site_url('front/Plat_du_jour/listePlatDuJour_res_info') ?>"><?php }?>
                                L’intégration
                                d’un
                                formulaire
                                de
                                réservation<?php if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and $toCommercant->referencement_resto == 1) {?></a><?php }?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-6 ">
            <div class="greyed">
                <div class="col-12 pt-4  text-center pb-4"
                     style="font-size: 18px;font-weight: bold;">
                    LE
                    PACK
                    HÉBERGEMENT
                </div>
                <div class="col-12 pb-4">
                    <img class="img-fluid"
                         style="width: 100%;"
                         src="<?php echo base_url('assets/img/HEB8B.jpg') ?>">
                </div>
                <div class="col-12">
                    <ul>
                        <li><?php if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and $toCommercant->referencement_gite == 1) {?>
                            <a href="<?php echo site_url('admin/Reservations/index') ?>"><?php }?>
                                L’intégration
                                du
                                lien
                                de
                                réservation
                                des
                                gîtes
                                de
                                france
                                correspondant
                                à
                                votre
                                établissement.<?php if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and $toCommercant->referencement_gite == 1) {?></a><?php }?>
                        </li>
                        <li><?php if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and $toCommercant->referencement_gite == 1) {?>
                            <a href="<?php echo site_url('admin/Reservations/index_res_form') ?>"><?php }?>
                                L’intégration
                                d’un
                                formulaire
                                de
                                réservation.<?php if ($thisss->ion_auth->in_group(4) || $thisss->ion_auth->in_group(5) || $thisss->ion_auth->in_group(7) and $toCommercant->referencement_gite == 1) {?></a><?php }?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <!-- **Pack SEO** -->
      <div class="row mt-5">
        <div class="col-12">
            <div class="greyed">
                <div class="col-12 text-center pt-4  pb-4"
                     style="font-size: 18px;font-weight: bold;">
                    LES
                    PACKS
                    «
                    SEO
                    »
                </div>
                <div class="col-12 pb-4">
                    <?php if ($toCommercant->active_seo == "1"){?>
                    <a href="<?php echo site_url('front/utilisateur/seo/'.$toCommercant->IdCommercant) ?>"><?php }?>
                        <img class="img-fluid" style="width: 100%;" src="<?php echo base_url('assets/img/seo.jpg') ?>">
                    <?php if ($toCommercant->active_seo == "1"){?></a><?php }?>
                </div>
               
            </div>
            
        </div>
    </div>
    
    <div class="row pt-4">
        <div class="col-12 text-center">
            <p style="
    font-family: 'Futura Md', sans-serif;
            font-style: normal;
            font-weight: normal;
            color: #000000;
            background-color: transparent;
            font-size: 15px;
            vertical-align: 0;">
                Seuls
                les
                liens
                «
                texte
                »
                en
                noir
                correspondant
                à
                votre
                abonnements
                sont
                actifs.</p>

            <p style="
    font-family: 'Futura Md', sans-serif;
            font-style: normal;
            font-weight: normal;
            color: #000000;
            background-color: transparent;
            font-size: 15px;
            vertical-align: 0;">
                Vous
                pouvez
                à
                tout
                moment
                enrichir
                votre
                abonnement
                en
                souscrivant
                à
                un
                ou
                plusieurs
                modules
                ou
                fonctions.</p>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {

        $("#sous_platinum").click(function () {
            if ($('#is_showed_form').val() === '0') {
                $('#is_showed_form').val('1');
                jQuery.ajax({
                    url: '<?php echo site_url('front/professionnels/get_abonnement_plat') ?>',
                    type: 'POST',
                    data: 'commercant=' + '<?php echo $toCommercant->IdCommercant; ?>',
                    async: true,
                    success: function (datas) {
                        $('#New_module').html(datas);

                    }
                });
            } else {
                $('#is_showed_form').val('0');
                $('#New_module').html('');
            }
        });

        $('input[id="showthis"]').hide();

        //show it when the checkbox is clicked
        $('input[name="checkbox"]').on('click', function () {
            if ($(this).prop('checked')) {
                $('input[id="showthis"]').fadeIn();
            } else {
                $('input[id="showthis"]').hide();
            }
        });

    });
</script>
