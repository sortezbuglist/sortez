<div id="div_pro_page1" style="display:none;">
    <div class="div_stl_long_platinum">GALERIE ET CONTENUS DE LA PAGE</div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4">
                        <label class="label">Titre page 1 <span style="font-size:11px;">(Maximum 18 caractères et espaces)</span>:</label>
                    </div>
                    <div class="col-lg-8">
                        <span style="font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#F00;">&nbsp;&nbsp;(si ce champ est vide l'onglet de la page 1 est masqu&eacute;)</span><br/>
                        <input type="text" name="Societe[labelactivite1]" id="labelactivite1Societe"
                               value="<?php if (isset($objCommercant->labelactivite1)) echo htmlspecialchars($objCommercant->labelactivite1); ?>"
                               class="stl_long_input_platinum form-control"/>
                        <div class="FieldError" id="divErrorEmailSociete"></div>
                    </div>
                </div>

            </div>
            <div class="col-lg-12">
                <div style="padding:15px;">Vos images doit avoir les proportions 1 x 4/3 (par exemple 640x480,
                    800x600, 1024x768)
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-6" id="Articleactivite1_image1_container">

                                    <a href='javascript:void(0);' title="activite1_image1"
                                        <?php if (empty($objCommercant->activite1_image1) || $objCommercant->activite1_image1 == "") { ?>
                                            onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-activite1_image1"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                        <?php } ?>
                                       class="btn btn-info w-75" id="Articleactivite1_image1_link">Ajouter la
                                        photo1</a>

                                    <a href="javascript:void(0);" class="btn btn-danger w-75 mt-3 mb-3"
                                        <?php if (!empty($objCommercant->activite1_image1) && $objCommercant->activite1_image1 != "") { ?>
                                            onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite1_image1');"
                                        <?php } ?>
                                    >Supprimer</a>

                                </div>
                                <div class="col-lg-6">
                                    <?php
                                    if (file_exists($path_img_gallery . $objCommercant->activite1_image1) && $objCommercant->activite1_image1 != "") {
                                        echo '<img class="img-fluid"  src="' . base_url() . $path_img_gallery . $objCommercant->activite1_image1 . '" />';
                                    } else if (file_exists($path_img_gallery_old . $objCommercant->activite1_image1) && $objCommercant->activite1_image1 != "") {
                                        echo '<img class="img-fluid"  src="' . base_url() . $path_img_gallery_old . $objCommercant->activite1_image1 . '" />';
                                    }
                                    ?>
                                    <input type="hidden" name="activite1_image1Associe" id="activite1_image1Associe"
                                           value="<?php if (isset($objCommercant->activite1_image1)) echo htmlspecialchars($objCommercant->activite1_image1); ?>"/>
                                    <div id="div_error_taille_activite1_image1"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite1_image1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6" id="Articleactivite1_image2_container">

                                    <a href='javascript:void(0);' title="activite1_image2"
                                        <?php if (empty($objCommercant->activite1_image2) || $objCommercant->activite1_image2 == "") { ?>
                                            onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-activite1_image2"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                        <?php } ?>
                                       class="btn btn-info  w-75" id="Articleactivite1_image2_link">Ajouter la
                                        photo2</a>

                                    <a href="javascript:void(0);" class="btn btn-danger w-75 mt-3 mb-3"
                                        <?php if (!empty($objCommercant->activite1_image2) && $objCommercant->activite1_image2 != "") { ?>
                                            onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite1_image2');"
                                        <?php } ?>
                                    >Supprimer</a>



                                </div>
                                <div class="col-lg-6">
                                    <?php
                                    if (file_exists($path_img_gallery . $objCommercant->activite1_image2) && $objCommercant->activite1_image2 != "") {
                                        echo '<img style="max-width:100%" class="img-fluid" src="' . base_url() . $path_img_gallery . $objCommercant->activite1_image2 . '" />';
                                    } else if (file_exists($path_img_gallery_old . $objCommercant->activite1_image2) && $objCommercant->activite1_image2 != "") {
                                        echo '<img style="max-width:100%" class="img-fluid" src="' . base_url() . $path_img_gallery_old . $objCommercant->activite1_image2 . '" />';
                                    }
                                    ?>
                                    <input type="hidden" name="activite1_image2Associe" id="activite1_image2Associe"
                                           value="<?php if (isset($objCommercant->activite1_image2)) echo htmlspecialchars($objCommercant->activite1_image2); ?>"/>
                                    <div id="div_error_taille_activite1_image2"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite1_image2", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6" id="Articleactivite1_image3_container">

                                    <a href='javascript:void(0);' title="activite1_image3"
                                        <?php if (empty($objCommercant->activite1_image3) || $objCommercant->activite1_image3 == "") { ?>
                                            onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-activite1_image3"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                        <?php } ?>
                                       class="btn btn-info w-75" id="Articleactivite1_image3_link">Ajouter la
                                        photo3</a>

                                    <a href="javascript:void(0);" class="btn btn-danger w-75 mt-3 mb-3"
                                        <?php if (!empty($objCommercant->activite1_image3) && $objCommercant->activite1_image3 != "") { ?>
                                            onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite1_image3');"
                                        <?php } ?>
                                    >Supprimer</a>

                                </div>
                                <div class="col-lg-6">
                                    <?php
                                    if (file_exists($path_img_gallery . $objCommercant->activite1_image3) && $objCommercant->activite1_image3 != "") {
                                        echo '<img  class="img-fluid" src="' . base_url() . $path_img_gallery . $objCommercant->activite1_image3 . '"/>';
                                    } else if (file_exists($path_img_gallery_old . $objCommercant->activite1_image3) && $objCommercant->activite1_image3 != "") {
                                        echo '<img  class="img-fluid" src="' . base_url() . $path_img_gallery_old . $objCommercant->activite1_image3 . '"/>';
                                    }
                                    ?>
                                    <input type="hidden" name="activite1_image3Associe" id="activite1_image3Associe"
                                           value="<?php if (isset($objCommercant->activite1_image3)) echo htmlspecialchars($objCommercant->activite1_image3); ?>"/>
                                    <div id="div_error_taille_activite1_image3"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite1_image3", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-6" id="Articleactivite1_image4_container">



                                    <a href='javascript:void(0);' title="activite1_image4"
                                        <?php if (empty($objCommercant->activite1_image4) || $objCommercant->activite1_image4 == "") { ?>
                                            onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-activite1_image4"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                        <?php } ?>
                                       class="btn btn-info w-75" id="Articleactivite1_image4_link">Ajouter la
                                        photo4</a>

                                    <a href="javascript:void(0);" class="btn btn-danger w-75 mt-3 mb-3"
                                        <?php if (!empty($objCommercant->activite1_image4) && $objCommercant->activite1_image4 != "") { ?>
                                            onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite1_image4');"
                                        <?php } ?>
                                    >Supprimer</a>

                                </div>
                                <div class="col-lg-6">
                                    <?php
                                    if (file_exists($path_img_gallery . $objCommercant->activite1_image4) && $objCommercant->activite1_image4 != "") {
                                        echo '<img style="max-width:100%" class="img-fluid" src="' . base_url() . $path_img_gallery . $objCommercant->activite1_image4 . '" />';
                                    } else if (file_exists($path_img_gallery_old . $objCommercant->activite1_image4) && $objCommercant->activite1_image4 != "") {
                                        echo '<img style="max-width:100%" class="img-fluid" src="' . base_url() . $path_img_gallery_old . $objCommercant->activite1_image4 . '" />';
                                    }
                                    ?>
                                    <input type="hidden" name="activite1_image4Associe" id="activite1_image4Associe"
                                           value="<?php if (isset($objCommercant->activite1_image4)) echo htmlspecialchars($objCommercant->activite1_image4); ?>"/>
                                    <div id="div_error_taille_activite1_image4"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite1_image4", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6" id="Articleactivite1_image5_container">

                                    <a href='javascript:void(0);' title="activite1_image5"
                                        <?php if (empty($objCommercant->activite1_image5) || $objCommercant->activite1_image5 == "") { ?>
                                            onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-activite1_image5"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                        <?php } ?>
                                       class="btn btn-info w-75" id="Articleactivite1_image5_link">Ajouter la
                                        photo5</a>

                                    <a href="javascript:void(0);" class="btn btn-danger w-75 mt-3 mb-3"
                                        <?php if (!empty($objCommercant->activite1_image5) && $objCommercant->activite1_image5 != "") { ?>
                                            onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite1_image5');"
                                        <?php } ?>
                                    >Supprimer</a>


                                </div>
                                <div class="col-lg-6">
                                    <?php
                                    if (file_exists($path_img_gallery . $objCommercant->activite1_image5) && $objCommercant->activite1_image5 != "") {
                                        echo '<img style="max-width:100%" class="img-fluid" src="' . base_url() . $path_img_gallery . $objCommercant->activite1_image5 . '"/>';
                                    } else if (file_exists($path_img_gallery_old . $objCommercant->activite1_image5) && $objCommercant->activite1_image5 != "") {
                                        echo '<img style="max-width:100%" class="img-fluid" src="' . base_url() . $path_img_gallery_old . $objCommercant->activite1_image5 . '" />';
                                    }
                                    ?>
                                    <input type="hidden" name="activite1_image5Associe" id="activite1_image5Associe"
                                           value="<?php if (isset($objCommercant->activite1_image5)) echo htmlspecialchars($objCommercant->activite1_image5); ?>"/>
                                    <div id="div_error_taille_activite1_image5"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite1_image5", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-12">
                                            <textarea name="Societe[activite1]"
                                                      id="activite1Societe"><?php if (isset($objCommercant->activite1)) echo htmlspecialchars($objCommercant->activite1); ?></textarea>
                <script>
                    CKEDITOR.replace('activite1Societe');
                </script>
            </div>

        </div>
    </div>




</div>