<script type="text/javascript">
    jQuery(document).ready(function () {
        <?php if (isset($objbloc_info->is_activ_bloc3_page2) AND $objbloc_info->is_activ_bloc3_page2 == 1) {  ?>
                jQuery('#type_bloc3_page2_choose').css("display", "block"); 
                <?php if (isset($objbloc_info->type_bloc3_page2) AND $objbloc_info->type_bloc3_page2 == '1' OR isset($objbloc_info->type_bloc3_page2) AND $objbloc_info->type_bloc3_page2 =='2'){  ?>
                document.getElementById('bloc_3_page2').style.display = "block";
                document.getElementById('bloc2_3_page2').style.display = "none";
                document.getElementById('bloc3_3_page2').style.display = "none";
                document.getElementById('bonplan_config_3_page2').style.display = "none";
                document.getElementById('fidelity_3_config_page2').style.display = "none";
                $('#type_bloc3_page2js').val("1");
                $("#bloc_3_page2").removeClass("col-lg-6");
                $("#bloc2_3_page2").removeClass("col-lg-6");
                $("#bloc_3_page2").removeClass("col-lg-4");
                $("#bloc2_3_page2").removeClass("col-lg-4");
                $("#bloc3_3_page2").removeClass("col-lg-4");
            <?php } ?>
            <?php if (isset($objbloc_info->type_bloc3_page2) AND $objbloc_info->type_bloc3_page2 == '2') {  ?>
                 document.getElementById('bloc_3_page2').style.display = "block";
                document.getElementById('bloc2_3_page2').style.display = "block";
                document.getElementById('bloc3_3_page2').style.display = "none";
                document.getElementById('bonplan_config_3_page2').style.display = "none";
                document.getElementById('fidelity_3_config_page2').style.display = "none";
                $('#type_bloc3_page2js').val("2");
                $("#bloc_3_page2").addClass("col-lg-6");
                $("#bloc2_3_page2").addClass("col-lg-6");
                $("#bloc_3_page2").removeClass("col-lg-4");
                $("#bloc2_3_page2").removeClass("col-lg-4");
                $("#bloc3_3_page2").removeClass("col-lg-4");
            <?php } ?>
            <?php if (isset($objbloc_info->type_bloc3_page2) AND $objbloc_info->type_bloc3_page2 == '3') {  ?>
                 document.getElementById('bloc_3_page2').style.display = "block";
                document.getElementById('bloc2_3_page2').style.display = "block";
                document.getElementById('bloc3_3_page2').style.display = "block";
                document.getElementById('bonplan_config_3_page2').style.display = "none";
                document.getElementById('fidelity_3_config_page2').style.display = "none";
                $('#type_bloc3_page2js').val("3");
                $("#bloc_3_page2").addClass("col-lg-4");
                $("#bloc2_3_page2").addClass("col-lg-4");
                $("#bloc3_3_page2").addClass("col-lg-4");
                $("#bloc_3_page2").removeClass("col-lg-6");
                $("#bloc2_3_page2").removeClass("col-lg-6");
            <?php } ?>
            <?php if (isset($objbloc_info->type_bloc3_page2) AND $objbloc_info->type_bloc3_page2 == '1bp') {  ?>
               document.getElementById('bonplan_config_3_page2').style.display = "block";
                document.getElementById('bloc_3_page2').style.display = "none";
                document.getElementById('bloc2_3_page2').style.display = "none";
                document.getElementById('fidelity_3_config_page2').style.display = "none";
                $('#type_bloc3_page2js').val("1bp");
            <?php } ?>
            <?php if (isset($objbloc_info->type_bloc3_page2) AND $objbloc_info->type_bloc3_page2 == '1fd') {  ?>
               document.getElementById('fidelity_3_config_page2').style.display = "block";
                document.getElementById('bonplan_config_3_page2').style.display = "none";
                document.getElementById('bloc_3_page2').style.display = "none";
                document.getElementById('bloc2_3_page2').style.display = "none";
                $('#type_bloc3_page2js').val("1fd");
            <?php } ?>
            <?php if (isset($objbloc_info->is_activ_btn_bloc1_3_page2) && $objbloc_info->is_activ_btn_bloc1_3_page2 == '1') {  ?>
                document.getElementById('link_bloc1_3_page2').style.display = "block";
            <?php } ?>
            <?php if (isset($objbloc_info->is_activ_btn_bloc2_3_page2) && $objbloc_info->is_activ_btn_bloc2_3_page2 == '1') {  ?>
                document.getElementById('link_bloc2_3_page2').style.display = "block";
            <?php } ?>
            <?php if (isset($objbloc_info->is_activ_btn_bloc3_3_page2) && $objbloc_info->is_activ_btn_bloc3_3_page2 == '1') {  ?>
                document.getElementById('link_bloc3_3_page2').style.display = "block";
            <?php } ?>
            <?php if (isset($objbloc_info->is_activ_btn_bp_3_page2) && $objbloc_info->is_activ_btn_bp_3_page2 == '1') {  ?>
                document.getElementById('link_bp_3_page2').style.display = "block";
            <?php } ?>
            <?php if (isset($objbloc_info->is_activ_btn_fd_3_page2) && $objbloc_info->is_activ_btn_fd_3_page2 == '1') {  ?>
                document.getElementById('link_fd_3_page2').style.display = "block";
            <?php } ?>      
        <?php } ?>
        $("#is_activ_bloc3_page2").change(function () {
            if (document.getElementById('is_activ_bloc3_page2').value == 0) {
                 jQuery('#type_bloc3_page2_choose').css("display", "none");
               
                jQuery('#bloc_3_page2').css("display", "none");
                
                jQuery('#bloc2_3_page2').css("display", "none");

                jQuery('#bloc3_3_page2').css("display", "none");

                jQuery('#bonplan_config_3_page2').css("display", "none");
                
                jQuery('#fidelity_3_config_page2').css("display", "none");
            }
            else if (document.getElementById('is_activ_bloc3_page2').value == 1) {
                jQuery('#type_bloc3_page2_choose').css("display", "block");
                jQuery('#bloc_3_page2').css("display", "none");
                
                jQuery('#bloc2_3_page2').css("display", "none");

                jQuery('#bloc3_3_page2').css("display", "none");

                jQuery('#bonplan_config_3_page2').css("display", "none");
                
                jQuery('#fidelity_3_config_page2').css("display", "none");
            }
        });
      $("#1b3_1_page2").click(function () {
            document.getElementById('bloc_3_page2').style.display = "block";
            document.getElementById('bloc2_3_page2').style.display = "none";
            document.getElementById('bloc3_3_page2').style.display = "none";
            document.getElementById('bonplan_config_3_page2').style.display = "none";
            document.getElementById('fidelity_3_config_page2').style.display = "none";
            $('#type_bloc3_page2js').val("1");
            $("#bloc_3_page2").removeClass("col-lg-6");
            $("#bloc2_3_page2").removeClass("col-lg-6");
            $("#bloc_3_page2").removeClass("col-lg-4");
            $("#bloc2_3_page2").removeClass("col-lg-4");
            $("#bloc3_3_page2").removeClass("col-lg-4");
        });
        $("#2b3_1_page2").click(function () {
            document.getElementById('bloc_3_page2').style.display = "block";
            document.getElementById('bloc2_3_page2').style.display = "block";
            document.getElementById('bloc3_3_page2').style.display = "none";
            document.getElementById('bonplan_config_3_page2').style.display = "none";
            document.getElementById('fidelity_3_config_page2').style.display = "none";
            $('#type_bloc3_page2js').val("2");
            $("#bloc_3_page2").addClass("col-lg-6");
            $("#bloc2_3_page2").addClass("col-lg-6");
            $("#bloc_3_page2").removeClass("col-lg-4");
            $("#bloc2_3_page2").removeClass("col-lg-4");
            $("#bloc3_3_page2").removeClass("col-lg-4");
        });
        $("#3b3_1_page2").click(function () {
            document.getElementById('bloc_3_page2').style.display = "block";
            document.getElementById('bloc2_3_page2').style.display = "block";
            document.getElementById('bloc3_3_page2').style.display = "block";
            document.getElementById('bonplan_config_3_page2').style.display = "none";
            document.getElementById('fidelity_3_config_page2').style.display = "none";
            $('#type_bloc3_page2js').val("3");
            $("#bloc_3_page2").addClass("col-lg-4");
            $("#bloc2_3_page2").addClass("col-lg-4");
            $("#bloc3_3_page2").addClass("col-lg-4");
            $("#bloc_3_page2").removeClass("col-lg-6");
            $("#bloc2_3_page2").removeClass("col-lg-6");
        });
        $("#1bp3_1_page2").click(function () {
            document.getElementById('bonplan_config_3_page2').style.display = "block";
            document.getElementById('bloc_3_page2').style.display = "none";
            document.getElementById('bloc2_3_page2').style.display = "none";
            document.getElementById('bloc3_3_page2').style.display = "none";
            document.getElementById('fidelity_3_config_page2').style.display = "none";
            $('#type_bloc3_page2js').val("1bp");
        });
        $("#1fd3_1_page2").click(function () {
            document.getElementById('fidelity_3_config_page2').style.display = "block";
            document.getElementById('bonplan_config_3_page2').style.display = "none";
            document.getElementById('bloc_3_page2').style.display = "none";
            document.getElementById('bloc2_3_page2').style.display = "none";
            document.getElementById('bloc3_3_page2').style.display = "none";
            $('#type_bloc3_page2js').val("1fd");
        });
        $("#link_btn_bloc1_3_page2").change(function () {

            if (document.getElementById('link_btn_bloc1_3_page2').value == 1) {
                document.getElementById('link_bloc1_3_page2').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc1_3_page2').value == 0) {
                document.getElementById('link_bloc1_3_page2').style.display = "none";

            }

        }); 
        $("#link_btn_bloc2_3_page2").change(function () {


            if (document.getElementById('link_btn_bloc2_3_page2').value == 1) {
                document.getElementById('link_bloc2_3_page2').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc2_3_page2').value == 0) {
                document.getElementById('link_bloc2_3_page2').style.display = "none";

            }

        });
        $("#link_btn_bloc3_3_page2").change(function () {


            if (document.getElementById('link_btn_bloc3_3_page2').value == 1) {
                document.getElementById('link_bloc3_3_page2').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc3_3_page2').value == 0) {
                document.getElementById('link_bloc3_3_page2').style.display = "none";

            }

        });
        $("#link_btn_bp_3_page2").change(function () {

            if (document.getElementById('link_btn_bp_3_page2').value == 1) {
                document.getElementById('link_bp_3_page2').style.display = "block";

            }
            else if (document.getElementById('link_btn_bp_3_page2').value == 0) {
                document.getElementById('link_bp_3_page2').style.display = "none";

            }

        });
        $("#link_btn_fd_3_page2").change(function () {

            if (document.getElementById('link_btn_fd_3_page2').value == 1) {
                document.getElementById('link_fd_3_page2').style.display = "block";

            }
            else if (document.getElementById('link_btn_fd_3_page2').value == 0) {
                document.getElementById('link_fd_3_page2').style.display = "none";

            }

        }); 
    });
</script>