<div id="div_pro_glissiere1_page2" style="display:none;">
    <div class="col-lg-12"
         style="background-color: #3653A3;
            color: #FFFFFF;
                    font-size: 13px;
                    font-weight: 700;
                    line-height: 1.23em;
                    margin-top: 12px;height: 40px; padding-top: 12px;"><?php if (isset($user_groups) && $user_groups->id == '5') { ?>Glissi&egrave;re 1 page 2<?php } else { ?>Informations glissi&egrave;re 1 page 2<?php } ?>
        <span style="float:right;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Glissiere[isActive_page2_1]" id="isActive_page2_1" style="color:#000000;">
        <option value="0"
                <?php if (isset($objGlissiere->isActive_page2_1) AND $objGlissiere->isActive_page2_1 == '0') { ?>selected="selected"<?php } ?>>non</option>
        <option value="1"
                <?php if (isset($objGlissiere->isActive_page2_1) AND $objGlissiere->isActive_page2_1 == '1') { ?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>


    </div>
    <input type="hidden" name="Glissiere[nombre_blick_gli_p2]" id="nbglissiere1p2" value="<?php if (isset($objGlissiere->nombre_blick_gli_p2)){echo $objGlissiere->nombre_blick_gli_p2;} ?>">
    <div class="pl-3 pr-3 pt-0 pb-0"  id="dispnbglip2" style=" color:#000000; <?php if (isset($objGlissiere->isActive_page2_1) AND $objGlissiere->isActive_page2_1 =='1'){echo 'display:block';}else{echo 'display:none';} ?>">
        <div class="row rowbtn">
            <div class="col-lg-6 btn_vert" id="1g1_2"><img style="width: 100%;" src="<?php echo base_url('assets/img/1.png')?>"></div>
            <div class="col-lg-6 btn_vert " id="2g1_2"><img style="width: 100%;" src="<?php echo base_url('assets/img/2.png')?>"></div>

        </div>
    </div>

    <div id="gli1p2co" class="grey_bloc_independent container-fluid" style="<?php if (isset($objGlissiere->isActive_page2_1) AND $objGlissiere->isActive_page2_1 == '1'){echo 'display:block';}else{echo 'display:none';} ?>">
        <div id="glissiere1contentp2" class="grey_bloc_independent container-fluid" style="<?php if (isset($objGlissiere->nombre_blick_gli_p2) && $objGlissiere->nombre_blick_gli_p2 == '1') {echo "display:block";}elseif (isset($objGlissiere->nombre_blick_gli_p2) && $objGlissiere->nombre_blick_gli_p2 == '2') {echo "display:block";}else {echo "display:none";} ?>; width:100%;">
            <div class="row">
                <div class="col-lg-12" id="gl1_p2_tittle">
                    <div class="stl_long_input_platinum_td">
                        <label class="label">Titre Glissière1: </label>
                    </div>
                    <div>
                        <input class="form-control" type="text" name="Glissiere[page2_1_titre]"
                               id="page_2_titre"
                               value="<?php if (isset($objGlissiere->page2_1_titre)) echo htmlspecialchars_decode($objGlissiere->page2_1_titre); ?>"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6" id="gl1_content_p2">

                    <div class="img_tab" id="gl1_p2_img">
                        <div style="background-color: #3EB4D7;padding-top: 13px;height: 40px;color: white;margin-bottom: 20px"
                             class="text-center">Glissière1 page2 champ1
                        </div>
                        <div class="row">

                            <div class="text-center col-lg-12" id="Articlepage_2_image_1_container">

                                <a href='javascript:void(0);' title="page_2_image_1"
                                    <?php if (empty($objGlissiere->page_2_image_1) || $objGlissiere->page_2_image_1 == "") { ?>
                                        onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-gli-page_2_image_1"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                    <?php } ?>
                                   class="btn btn-info" id="page_2_image_1_link">Ajouter une Photo</a>

                                <a href="javascript:void(0);" class="btn btn-danger"
                                    <?php if (!empty($objGlissiere->page_2_image_1) && $objGlissiere->page_2_image_1 != "") { ?>
                                        onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','page_2_image_1');"
                                    <?php } ?>
                                >Supprimer</a>


                            </div>
                            <div class="col-lg-12">
                                <?php
                                if (is_object($objGlissiere) AND file_exists($path_img_gallery . $objGlissiere->page_2_image_1) && $objGlissiere->page_2_image_1 != "") {
                                    echo '<img style="max-width:100%" class="img-fluid" src="' . base_url() . $path_img_gallery . $objGlissiere->page_2_image_1 . '"/>';
                                } else if (is_object($objGlissiere) AND file_exists($path_img_gallery_old . $objGlissiere->page_2_image_1) && $objGlissiere->page_2_image_1 != "") {
                                    echo '<img style="max-width:100%" class="img-fluid" src="' . base_url() . $path_img_gallery_old . $objGlissiere->page_2_image_1 . '"/>';
                                }
                                ?>
                                <input type="hidden" name="page_2_image_1" id="page_2_image_1"
                                       value="<?php if (is_object($objGlissiere) AND isset($objGlissiere->page_2_image_1)) echo htmlspecialchars_decode($objGlissiere->page_2_image_1); ?>"/>
                                <div id="div_error_taille_activite1_image1"
                                     class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite1_image1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                            </div>


                        </div>
                    </div>
                    <div colspan="2">

                                <textarea name="Glissiere[page2_1_contenu1]"
                                          id="page2_1_contenu1"><?php if (isset($objGlissiere->page2_1_contenu1)) echo htmlspecialchars_decode($objGlissiere->page2_1_contenu1); ?></textarea>
                        <script>

                            CKEDITOR.replace('page2_1_contenu1');

                        </script>
                    </div>
                    <div style="border-bottom: double;padding-bottom: 50px;">
                        <div>
                            <div class="col-lg-12" id="btn_gli1_p2"
                                 style="background-color: #3EB4D7;height: 40px;padding-top: 12px;color: black">
                                <span style="color: white!important;">Ajouter un Bouton</span> <span
                                    style="float: right;">
                                                            <select name="Glissiere[is_activ_btn_glissiere1_page2_champ1]"
                                                                    style="margin-right: 10px;color: black!important;"
                                                                    id="link_btn_p2_gli1">
                                                                <option <?php if (!isset($objGlissiere->is_activ_btn_glissiere1_page2_champ1) or (isset($objGlissiere->is_activ_btn_glissiere1_page2_champ1) && $objGlissiere->is_activ_btn_glissiere1_page2_champ1 == 0)) {
                                                                    echo 'selected';
                                                                } ?> value="0">non</option>
                                                                <option <?php if (isset($objGlissiere->is_activ_btn_glissiere1_page2_champ1) && $objGlissiere->is_activ_btn_glissiere1_page2_champ1 == 1) {
                                                                    echo 'selected';
                                                                } ?> value="1">oui</option>
                                                            </select></span>
                            </div>
                            <?php //var_dump($objGlissiere->is_activ_btn_glissiere1_page2_champ1);die();   ?>
                            <div class="text-center" id="alllink_p2"
                                 style="<?php if (isset($objGlissiere->is_activ_btn_glissiere1_page2_champ1) AND $objGlissiere->is_activ_btn_glissiere1_page2_champ1 == '1') { ?>display:block <?php } else { ?>display: none;<?php } ?>">

                                <div class="row pt-3">
                                    <div class="col-lg-3">
                                        <label for="btn_gli1_p2_content1">Texte du bouton</label>
                                    </div>
                                    <div class="col-lg-9">
                                        <input class="form-control" id="btn_gli1_p2_content1" type="text"
                                               value="<?php if (isset($objGlissiere->btn_gli1_p2_content1) AND $objGlissiere->btn_gli1_p2_content1 != "") {
                                                   echo $objGlissiere->btn_gli1_p2_content1;
                                               } ?>" name="Glissiere[btn_gli1_p2_content1]">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label for="valuelink_p2">Lien du bouton</label>
                                    </div>
                                    <div class="col-lg-9">
                                        <select name="Glissiere[gl1_existed_p2_link1]" class="form-control"
                                                style="margin-right: 10px;color: black" id="valuelink_p2">
                                            <option <?php if (isset($objGlissiere->gl1_existed_p2_link1) AND $objGlissiere->gl1_existed_p2_link1 == '0') {
                                                echo 'selected';
                                            } ?> value="0">Choisir un lien local1</option><?php if ($user_groups->id == '5') {?>
                                            <option <?php if (isset($objGlissiere->gl1_existed_p2_link1) AND $objGlissiere->gl1_existed_p2_link1 == 'p1') {
                                                echo 'selected';
                                            } ?> value="p1">Vers la page 1</option><?php } ?>
                                            <option <?php if (isset($objGlissiere->gl1_existed_p2_link1) AND $objGlissiere->gl1_existed_p2_link1 == 'p2') {
                                                echo 'selected';
                                            } ?> value="p2">Vers la page 2</option>
                                            <option <?php if (isset($objGlissiere->gl1_existed_p2_link1) AND $objGlissiere->gl1_existed_p2_link1 == 'ag') {
                                                echo 'selected';
                                            } ?> value="ag">Mes Agendas</option>
                                            <option <?php if (isset($objGlissiere->gl1_existed_p2_link1) AND $objGlissiere->gl1_existed_p2_link1 == 'art') {
                                                echo 'selected';
                                            } ?> value="art">Mes Articles</option>
                                            <option <?php if (isset($objGlissiere->gl1_existed_p2_link1) AND $objGlissiere->gl1_existed_p2_link1 == 'bp') {
                                                echo 'selected';
                                            } ?> value="bp">Mes Bons Plans</option>
                                            <option <?php if (isset($objGlissiere->gl1_existed_p2_link1) AND $objGlissiere->gl1_existed_p2_link1 == 'bt') {
                                                echo 'selected';
                                            } ?> value="bt">Mes Boutiques</option>
                                            <option <?php if (isset($objGlissiere->gl1_existed_p2_link1) AND $objGlissiere->gl1_existed_p2_link1 == 'fd') {
                                                echo 'selected';
                                            } ?> value="fd">Ma Fidelisation</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        Ou ajouter un lien externe
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="text-center form-control" id="htt_p2">
                                            <option <?php if (isset($objGlissiere->gl1_p1_custom_link1) && preg_match('/http:/', $objGlissiere->gl1_p1_custom_link1)) {
                                                echo 'selected';
                                            } ?> value="http://">http://
                                            </option>
                                            <option <?php if (isset($objGlissiere->gl1_p1_custom_link1) && preg_match('/http:/', $objGlissiere->gl1_p1_custom_link1)) {
                                                echo 'selected';
                                            } ?> value="https://">https://
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6">
                                        <input onchange="sub_link_custom1()" name="Glissiere[gl1_p2_custom_link1]"
                                               class="form-control text-center" type="text" value="<?php echo $objGlissiere->gl1_p1_custom_link1 ?? '';?>"
                                               id="link_htt_p2" placeholder="votre liens ici">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-lg-6"  id="gl1_p2_content2">
                    <div class="img_tab" id="gl1_p2_img2">
                        <div style="background-color: #3EB4D7;padding-top: 13px;height: 40px;color: white;margin-bottom: 20px"
                             class="text-center">Glissière1 page2 champ2
                        </div>

                        <div class="row">

                            <div class="text-center col-lg-12" id="Articlepage_2_image_2_container">

                                <a href='javascript:void(0);' title="page_2_image_2"
                                    <?php if (empty($objGlissiere->page_2_image_2) || $objGlissiere->page_2_image_2 == "") { ?>
                                        onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-gli-page_2_image_2"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                    <?php } ?>
                                   class="btn btn-info" id="page_2_image_2_link">Ajouter une Photo</a>

                                <a href="javascript:void(0);" class="btn btn-danger"
                                    <?php if (!empty($objGlissiere->page_2_image_2) && $objGlissiere->page_2_image_2 != "") { ?>
                                        onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','page_2_image_2');"
                                    <?php } ?>
                                >Supprimer</a>

                            </div>
                            <div class="col-lg-12">
                                <?php
                                if (is_object($objGlissiere) AND file_exists($path_img_gallery . $objGlissiere->page_2_image_2) == true) {
                                    echo '<img style="max-width:100%" class="img-fluid" src="' . base_url() . $path_img_gallery . $objGlissiere->page_2_image_2 . '"/>';
                                } else if (is_object($objGlissiere) AND file_exists($path_img_gallery_old . $objGlissiere->page_2_image_2) && $objGlissiere->page_2_image_2 != "") {
                                    echo '<img style="max-width:100%" class="img-fluid" src="' . base_url() . $path_img_gallery_old . $objGlissiere->page_2_image_2 . '"/>';
                                }
                                ?>
                                <input type="hidden" name="page_2_image_2" id="page_2_image_2"
                                       value="<?php if (isset($objGlissiere->page_2_image_2)) echo htmlspecialchars_decode($objGlissiere->page_2_image_2); ?>"/>
                                <div id="div_error_taille_activite1_image2"
                                     class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("page_2_image_2", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                            </div>


                        </div>
                    </div>



                    <div id="gl1_p1_content2">
                        <div colspan="2">
                                <textarea name="Glissiere[page2_1_contenu2]"
                                          id="page2_1_contenu2"><?php if (isset($objGlissiere->page2_1_contenu2)) echo htmlspecialchars_decode($objGlissiere->page2_1_contenu2); ?></textarea>
                            <script>

                                CKEDITOR.replace('page2_1_contenu2');

                            </script>
                        </div>

                    </div>
                    <div>
                        <div>
                            <div class="col-lg-12" id="btn_p2_gli2"
                                 style="background-color: #3EB4D7;height: 40px;padding-top: 12px;color: black">
                                <span style="color: white!important;">Ajouter un Bouton</span> <span
                                    style="float: right;">
                                                            <select name="Glissiere[is_activ_btn_glissiere1_page2_champ2]"
                                                                    style="margin-right: 10px;color: black!important;"
                                                                    id="link_btn_p2_gli2">
                                                                <option <?php if (isset($objGlissiere->is_activ_btn_glissiere1_page2_champ2) && $objGlissiere->is_activ_btn_glissiere1_page2_champ2 == 1) {
                                                                    echo 'selected';
                                                                } ?> value="1">oui</option>
                                                                <option <?php if (!isset($objGlissiere->is_activ_btn_glissiere1_page2_champ2) or (isset($objGlissiere->is_activ_btn_glissiere1_page2_champ2) && $objGlissiere->is_activ_btn_glissiere1_page2_champ2 == 0)) {
                                                                    echo 'selected';
                                                                } ?> value="0">non</option>
                                                            </select></span>
                            </div>
                            <div class="text-center" id="alllink2p2"  style="<?php if (isset($objGlissiere->is_activ_btn_glissiere1_page2_champ2) && $objGlissiere->is_activ_btn_glissiere1_page2_champ2 == 1) {
                                echo 'display:block';
                            }elseif (!isset($objGlissiere->is_activ_btn_glissiere1_page2_champ2) or (isset($objGlissiere->is_activ_btn_glissiere1_page2_champ2) && $objGlissiere->is_activ_btn_glissiere1_page2_champ2 == 0)) {
                                echo 'display:none';
                            } ?>">

                                <div class="row pt-3">
                                    <div class="col-lg-3">
                                        <label for="btn_gli1_p2_content2">Texte du bouton</label>
                                    </div>
                                    <div class="col-lg-9">
                                        <input class="form-control" id="btn_gli1_p2_content2" type="text"
                                               value="<?php if (isset($objGlissiere->btn_gli1_p2_content2) AND $objGlissiere->btn_gli1_p2_content2 != "") {
                                                   echo $objGlissiere->btn_gli1_p2_content2;
                                               } ?>" name="Glissiere[btn_gli1_p2_content2]">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label for="valuelink2p2">Lien du bouton</label>
                                    </div>
                                    <div class="col-lg-9">
                                        <select name="Glissiere[gl1_existed_p2_link2]"
                                                class="form-control"
                                                style="margin-right: 10px;color: black"
                                                id="valuelink2p2">
                                            <option <?php if (isset($objGlissiere->gl1_existed_p2_link2) AND $objGlissiere->gl1_existed_p2_link2 == '0') {
                                                echo 'selected';
                                            } ?> value="0">Choisir un lien local2</option><?php if ($user_groups->id == '5') {?>
                                            <option <?php if (isset($objGlissiere->gl1_existed_p2_link2) AND $objGlissiere->gl1_existed_p2_link2 == 'p1') {
                                                echo 'selected';
                                            } ?> value="p1">Vers la page 1</option><?php } ?>
                                            <option <?php if (isset($objGlissiere->gl1_existed_p2_link2) AND $objGlissiere->gl1_existed_p2_link2 == 'p2') {
                                                echo 'selected';
                                            } ?> value="p2">Vers la page 2</option>
                                            <option <?php if (isset($objGlissiere->gl1_existed_p2_link2) AND $objGlissiere->gl1_existed_p2_link2 == 'ag') {
                                                echo 'selected';
                                            } ?> value="ag">Mes Agendas</option>
                                            <option <?php if (isset($objGlissiere->gl1_existed_p2_link2) AND $objGlissiere->gl1_existed_p2_link2 == 'art') {
                                                echo 'selected';
                                            } ?> value="art">Mes Articles</option>
                                            <option <?php if (isset($objGlissiere->gl1_existed_p2_link2) AND $objGlissiere->gl1_existed_p2_link2 == 'bp') {
                                                echo 'selected';
                                            } ?> value="bp">Mes Bons Plans</option>
                                            <option <?php if (isset($objGlissiere->gl1_existed_p2_link2) AND $objGlissiere->gl1_existed_p2_link2 == 'bt') {
                                                echo 'selected';
                                            } ?> value="bt">Mes Boutiques</option>
                                            <option <?php if (isset($objGlissiere->gl1_existed_p2_link2) AND $objGlissiere->gl1_existed_p2_link2 == 'fd') {
                                                echo 'selected';
                                            } ?> value="fd">Ma Fidelisation</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        Ou ajouter un lien externe
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control text-left" id="htt2p2">
                                            <option <?php if (isset($objGlissiere->gl1_p2_custom_link2) && preg_match('/http:/', $objGlissiere->gl1_p2_custom_link2)) {
                                                echo 'selected';
                                            } ?> value="http://">http://
                                            </option>
                                            <option <?php if (isset($objGlissiere->gl1_p2_custom_link2) && preg_match('/https:/', $objGlissiere->gl1_p2_custom_link2)) {
                                                echo 'selected';
                                            } ?> value="https://">https://
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6">
                                        <input onchange="sub_link_custom2()"
                                               name="Glissiere[gl1_p2_custom_link2]"
                                               class="form-control text-center" type="text" value="<?php echo $objGlissiere->gl1_p2_custom_link2 ?? '';?>"
                                               id="link_htt2p2" placeholder="votre liens ici">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>


</div>