<?php
$path_img_gallery = 'application/resources/front/photoCommercant/imagesbank/' . $objCommercant->user_ionauth_id . '/';
$path_img_gallery_old = 'application/resources/front/photoCommercant/images/';
if (isset($mssg) && $mssg != '1') {
    $img_error_verify_array = preg_split('/-/', $mssg);
}
?>

<div id="div_pro_image_presentation"
     >

    <div <?php if (isset($user_groups) && $user_groups->id == '5') { ?>style="display:none;"<?php } ?> class="div_stl_long_platinum text-uppercase titre">IMAGES BANNIERE DE L'EN TÊTE</div>
    <div <?php if (isset($user_groups) && $user_groups->id == '5') { ?>style="display:none;"<?php } ?> class="container">
        <div class="row">
            <div class="col-lg-2 text-center">
                <img class="img-fluid" src="<?php echo base_url("/assets/images/point.webp") ?>" width="62" height="62">
                <p class="text_content" style="text-align: center">Aide</p>
            </div>
            <div class="col-lg-8 pl-0">
                <ul class="m-0 p-0">
                    <li>Pour optimiser l'affichage de vos images, nous vous conseillons d’intégrer des photos au format de 850 pixels sur la hauteur et 800 pixels sur la largeur.</li>
                    <li>Avec l'éditeur de photo en ligne vous pouvez recadrer et redimensionner vos images.</li>
                </ul>
            </div>
            <div class="col-lg-2">
                <div class="row">
                    <div class="col-lg-4 text-center p-0">
                        <p class="text_content">Accès à l'éditeur photo.</p>
                    </div>
                    <div class="col-lg-8 text-center">
                        <a href="https://fotoflexer.com/editor/" target="_blank">
                            <img class="img-fluid" src="<?php echo base_url("/assets/images/IMAGE45.webp") ?>" width="81" height="78">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" name="photogaucheAssocie" id="photogaucheAssocie"
               value="<?php if (isset($objCommercant->photogauche)) echo htmlspecialchars($objCommercant->photogauche); ?>"/>
        <input type="hidden" name="photodroiteAssocie" id="photodroiteAssocie"
               value="<?php if (isset($objCommercant->photodroite)) echo htmlspecialchars($objCommercant->photodroite); ?>"/>
        <div class="row mt-2">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="btn btn_insert w-100">
                            <a href='javascript:void(0);' title="photogauche"
                                <?php if (empty($objCommercant->photogauche) || $objCommercant->photogauche == "") { ?>
                                    onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-photogauche"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                <?php } ?>
                               id="Articlephotogauche_link">
                                Ajouter l'image de gauche
                            </a>
                        </div>
                        <div class="btn btn_suppr w-100 mt-3 mb-3">
                            <a href="javascript:void(0);"
                                <?php if (!empty($objCommercant->photogauche) && $objCommercant->photogauche != "") { ?>
                                    onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','photogauche');"
                                <?php } ?> >
                                Supprimer l'image
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6" id="Articlephotogauche_container">
                        <?php
                        if (file_exists($path_img_gallery . $objCommercant->photogauche) && $objCommercant->photogauche != "") {
                            echo '<img style="max-width:100%" src="' . base_url() . $path_img_gallery . $objCommercant->photogauche . '" "/>';
                        } elseif (file_exists($path_img_gallery_old . $objCommercant->photogauche) && $objCommercant->photogauche != "") {
                            echo '<img style="max-width:100%" style="max-width:100%"  src="' . base_url() . $path_img_gallery_old . $objCommercant->photogauche . '" "/>';
                        }else{ ?>
                            <img class="img-fluid" style="width: 100%; height: auto;background: darkgrey" src="<?php echo base_url("/assets/images/down.webp") ?>">
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="btn btn_insert w-100">
                            <a href='javascript:void(0);' title="photodroite"
                                <?php if (empty($objCommercant->photodroite) || $objCommercant->photodroite == "") { ?>
                                    onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-photodroite"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                <?php } ?>
                               id="Articlephotodroite_link">
                                Ajouter l'image de droite
                            </a>
                        </div>
                        <div class="btn btn_suppr w-100 mt-3 mb-3">
                            <a href="javascript:void(0);"
                                <?php if (!empty($objCommercant->photodroite) && $objCommercant->photodroite != "") { ?>
                                    onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','photodroite');"
                                <?php } ?> >
                                Supprimer l'image
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6" id="Articlephotodroite_container">
                        <?php
                        if (file_exists($path_img_gallery . $objCommercant->photodroite) && $objCommercant->photodroite != "") {
                            echo '<img style="max-width:100%" src="' . base_url() . $path_img_gallery . $objCommercant->photodroite . '" "/>';
                        } elseif (file_exists($path_img_gallery_old . $objCommercant->photodroite) && $objCommercant->photodroite != "") {
                            echo '<img style="max-width:100%" style="max-width:100%"  src="' . base_url() . $path_img_gallery_old . $objCommercant->photodroite . '" "/>';
                        }else{
                        ?>
                            <img class="img-fluid" style="width: 100%; height: auto;background: darkgrey" src="<?php echo base_url("/assets/images/down.webp") ?>">
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="div_stl_long_platinum text-uppercase titre">IMAGES DU SLIDE DE LA PAGE D'ACCUEIL</div>
    <div class="container">
        <div class="row">
            <div class="col-lg-2 text-center">
                <img class="img-fluid" src="<?php echo base_url("/assets/images/point.webp") ?>" width="62" height="62">
                <p class="text_content" style="text-align: center">Aide</p>
            </div>
            <div class="col-lg-8 pl-0">
                <ul class="m-0 p-0">
                    <li>Pour optimiser l'affichage de vos images, nous vous conseillons d’intégrer des photos au format 16/9.</li>
                    <li>Avec l'éditeur de photo en ligne vous pouvez recadrer et redimensionner vos images</li>
                </ul>
            </div>
            <div class="col-lg-2">
                <div class="row">
                    <div class="col-lg-4 text-center p-0">
                        <p class="text_content">Accès à l'éditeur photo.</p>
                    </div>
                    <div class="col-lg-8 text-center">
                        <a href="https://fotoflexer.com/editor/" target="_blank">
                            <img class="img-fluid" src="<?php echo base_url("/assets/images/IMAGE45.webp") ?>" width="81" height="78">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid text-center mt-2">

        <input type="hidden" name="pdfAssocie" id="pdfAssocie"
               value="<?php if (isset($objCommercant->Pdf)) echo htmlspecialchars($objCommercant->Pdf); ?>"/>
        <input type="hidden" name="photologoAssocie" id="LogoAssocie"
               value="<?php if (isset($objCommercant->Logo)) echo htmlspecialchars($objCommercant->Logo); ?>"/>
        <input type="hidden" name="photo1Associe" id="Photo1Associe"
               value="<?php if (isset($objCommercant->Photo1)) echo htmlspecialchars($objCommercant->Photo1); ?>"/>
        <input type="hidden" name="photo2Associe" id="Photo2Associe"
               value="<?php if (isset($objCommercant->Photo2)) echo htmlspecialchars($objCommercant->Photo2); ?>"/>
        <input type="hidden" name="photo3Associe" id="Photo3Associe"
               value="<?php if (isset($objCommercant->Photo3)) echo htmlspecialchars($objCommercant->Photo3); ?>"/>
        <input type="hidden" name="photo4Associe" id="Photo4Associe"
               value="<?php if (isset($objCommercant->Photo4)) echo htmlspecialchars($objCommercant->Photo4); ?>"/>
        <input type="hidden" name="photo5Associe" id="Photo5Associe"
               value="<?php if (isset($objCommercant->Photo5)) echo htmlspecialchars($objCommercant->Photo5); ?>"/>
        <input type="hidden" name="bandeau_topAssocie" id="bandeau_topAssocie"
               value="<?php if (isset($objCommercant->bandeau_top)) echo htmlspecialchars($objCommercant->bandeau_top); ?>"/>
        <input type="hidden" name="background_imageAssocie" id="background_imageAssocie"
               value="<?php if (isset($objCommercant->background_image)) echo htmlspecialchars($objCommercant->background_image); ?>"/>

        <div class="row">
            <div class="col-lg-6"><!-- photo 1 container start -->
                <div class="row">
                    <div class="col-lg-6 pb-3">
                        <a href='javascript:void(0);' title="Photo1" class="btn btn_insert w-100"
                            <?php if (empty($objCommercant->Photo1) || $objCommercant->Photo1 == "") { ?>
                                onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-Photo1"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                            <?php } ?>
                           id="ArticlePhoto1_link">Ajouter l'image 1</a>
                        <a href="javascript:void(0);" class="btn btn_suppr w-100 mt-3 mb-3"
                            <?php if (!empty($objCommercant->Photo1) && $objCommercant->Photo1 != "") { ?>
                                onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Photo1');"
                            <?php } ?>
                        >Supprimer l'image</a>

                    </div>
                    <div class="col-lg-6" id="ArticlePhoto1_container">
                        <?php
                        if (file_exists($path_img_gallery . $objCommercant->Photo1) && $objCommercant->Photo1 != "") {
                            echo '<img style="max-width:100%" src="' . base_url() . $path_img_gallery . $objCommercant->Photo1 . '" />';
                        } elseif (file_exists($path_img_gallery_old . $objCommercant->Photo1) && $objCommercant->Photo1 != "") {
                            echo '<img style="max-width:100%" src="' . base_url() . $path_img_gallery_old . $objCommercant->Photo1 . '" "/>';
                        }else{ ?>
                            <img class="img-fluid" style="width: 100%; height: auto;background: darkgrey" src="<?php echo base_url("/assets/images/down.webp") ?>">
                        <?php } ?>
                    </div>
                    <div id="div_error_taille_Photo1"
                         class="div_error_taille_3_4 col-lg-12"><?php if (isset($img_error_verify_array) && in_array("Photo1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                </div>
            </div><!-- photo 1 container end -->

            <div class="col-lg-6"><!-- photo 2 container start -->
                <div class="row">
                    <div class="col-lg-6 pb-3">
                            <a href='javascript:void(0);' title="Photo2" class="btn btn_insert w-100"
                                <?php if (empty($objCommercant->Photo2) && $objCommercant->Photo2 == "") { ?>
                                    onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-Photo2"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                <?php } ?>
                               id="ArticlePhoto2_link">Ajouter l'image 2</a>
                            <a href="javascript:void(0);" class="btn btn_suppr w-100 mt-3 mb-3"
                                <?php if (!empty($objCommercant->Photo2) && $objCommercant->Photo2 != "") { ?>
                                    onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Photo2');"
                                <?php } ?>
                            >Supprimer l'image</a>

                    </div>
                    <div class="col-lg-6" id="ArticlePhoto2_container">
                        <?php
                        if (file_exists($path_img_gallery . $objCommercant->Photo2) && $objCommercant->Photo2 != "") {
                            echo '<img style="max-width:100%" src="' . base_url() . $path_img_gallery . $objCommercant->Photo2 . '" "/>';
                        } elseif (file_exists($path_img_gallery_old . $objCommercant->Photo2) && $objCommercant->Photo2 != "") {
                            echo '<img style="max-width:100%" style="max-width:100%"  src="' . base_url() . $path_img_gallery_old . $objCommercant->Photo2 . '" "/>';
                        }else{ ?>
                            <img class="img-fluid" style="width: 100%; height: auto;background: darkgrey" src="<?php echo base_url("/assets/images/down.webp") ?>">
                        <?php } ?>
                    </div>
                    <div id="div_error_taille_Photo2"
                         class="div_error_taille_3_4 col-lg-12"><?php if (isset($img_error_verify_array) && in_array("Photo2", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                </div>
            </div><!-- photo 2 container end -->
        </div>

        <div class="row pt-3">
            <div class="col-lg-6"><!-- photo 3 container start -->
                <div class="row">
                    <div class="col-lg-6 pb-3">
                            <a href='javascript:void(0);' title="Photo3" class="btn btn_insert w-100"
                                <?php if (empty($objCommercant->Photo3) || $objCommercant->Photo3 == "") { ?>
                                    onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-Photo3"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                <?php } ?>
                               id="ArticlePhoto3_link">Ajouter l'image 3</a>
                            <a href="javascript:void(0);" class="btn btn_suppr w-100 mt-3 mb-3"
                                <?php if (!empty($objCommercant->Photo3) && $objCommercant->Photo3 != "") { ?>
                                    onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Photo3');"
                                <?php } ?>
                            >Supprimer l'image</a>

                    </div>
                    <div class="col-lg-6" id="ArticlePhoto3_container">
                        <?php
                        if (file_exists($path_img_gallery . $objCommercant->Photo3) && $objCommercant->Photo3 != "") {
                            echo '<img style="max-width:100%" src="' . base_url() . $path_img_gallery . $objCommercant->Photo3 . '" "/>';
                        } elseif (file_exists($path_img_gallery_old . $objCommercant->Photo3) && $objCommercant->Photo3 != "") {
                            echo '<img style="max-width:100%" src="' . base_url() . $path_img_gallery_old . $objCommercant->Photo3 . '" "/>';
                        }
                        ?>
                    </div>
                    <div id="div_error_taille_Photo3"
                         class="div_error_taille_3_4 col-lg-12"><?php if (isset($img_error_verify_array) && in_array("Photo3", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                </div>
            </div>
            <div class="col-lg-6"><!-- photo 4 container start -->
                <div class="row">
                    <div class="col-lg-6 pb-3">
                        <a href='javascript:void(0);' title="Photo4" class="btn btn_insert w-100"
                            <?php if (empty($objCommercant->Photo4) || $objCommercant->Photo4 == "") { ?>
                                onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-Photo4"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                            <?php } ?>
                           id="ArticlePhoto4_link">Ajouter l'image 4</a>
                        <a href="javascript:void(0);" class="btn btn_suppr w-100 mt-3 mb-3"
                            <?php if (!empty($objCommercant->Photo4) && $objCommercant->Photo4 != "") { ?>
                                onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Photo4');"
                            <?php } ?>
                        >Supprimer l'image</a>
                    </div>
                    <div class="col-lg-6" id="ArticlePhoto4_container">
                        <?php
                        if (file_exists($path_img_gallery . $objCommercant->Photo4) && $objCommercant->Photo4 != "") {
                            echo '<img style="max-width:100%" src="' . base_url() . $path_img_gallery . $objCommercant->Photo4 . '" "/>';
                        } elseif (file_exists($path_img_gallery_old . $objCommercant->Photo4) && $objCommercant->Photo4 != "") {
                            echo '<img style="max-width:100%" src="' . base_url() . $path_img_gallery_old . $objCommercant->Photo4 . '" "/>';
                        }
                        ?>
                    </div>
                    <div id="div_error_taille_Photo4"
                         class="div_error_taille_3_4 col-lg-12"><?php if (isset($img_error_verify_array) && in_array("Photo4", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                </div>
            </div>
        </div>

<!--        <div class="row pt-3">-->
<!--            <div class="col-lg-6"> /*photo 5 container start*/ -->
<!--                <div class="row">-->
<!--                    <div class="col-lg-6 pb-3">-->
<!--                        <div id="ArticlePhoto5_container">-->
<!---->
<!--                            <a href='javascript:void(0);' title="Photo5" class="btn btn-info w-75"-->
<!--                                --><?php //if (empty($objCommercant->Photo5) && $objCommercant->Photo5 == "") { ?>
<!--                                    onclick='javascript:window.open("--><?php //echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-Photo5"); ?><!--", "Commercant image", "width=1045, height=725, scrollbars=yes");'-->
                                <?php //} ?>
<!--                               id="ArticlePhoto5_link">Ajouter la photo5</a>-->
<!--                            <a href="javascript:void(0);" class="btn btn-danger w-75 mt-3 mb-3"-->
<!--                                --><?php //if (!empty($objCommercant->Photo5) && $objCommercant->Photo5 != "") { ?>
<!--                                    onclick="deleteFile('--><?php //echo $objCommercant->IdCommercant; ?><!--','Photo5');"-->
                                <?php //} ?>
<!--                            >Supprimer</a>-->
<!---->
<!---->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-lg-6">-->
<!--                        --><?php
//                        if (file_exists($path_img_gallery . $objCommercant->Photo5) && $objCommercant->Photo5 != "") {
//                            echo '<img style="max-width:100%" src="' . base_url() . $path_img_gallery . $objCommercant->Photo5 . '" "/>';
//                        } elseif (file_exists($path_img_gallery_old . $objCommercant->Photo5) && $objCommercant->Photo5 != "") {
//                            echo '<img style="max-width:100%" src="' . base_url() . $path_img_gallery_old . $objCommercant->Photo5 . '" "/>';
//                        }
//                        ?>
<!--                    </div>-->
<!--                    <div id="div_error_taille_Photo5"-->
<!--                         class="div_error_taille_3_4 col-lg-12">--><?php //if (isset($img_error_verify_array) && in_array("Photo5", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?><!--</div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-lg-6">-->
<!---->
<!--            </div>-->
<!--        </div>-->

    </div>


</div>

<div id="div_pro_pagepresentation"
     <?php if (isset($user_groups) && $user_groups->id == '5') { ?>style="display:none;"<?php } ?>>
    <div class="div_stl_long_platinum text-uppercase titre"> TEXTE DE PRESENTATION  PAGE D'ACCUEIL</div>
    <div class="container-fluid">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">
            <tr>
                <td class="text_content">Texte de présentation:</td>
            </tr>
            <tr>
                <td colspan="2">
                                            <textarea name="Societe[Caracteristiques]"
                                                      id="CaracteristiquesSociete"><?php if (isset($objCommercant->Caracteristiques)) echo htmlspecialchars_decode($objCommercant->Caracteristiques); ?></textarea>
                    <script>

                        CKEDITOR.replace('CaracteristiquesSociete');

                    </script>
                </td>
            </tr>
        </table>
    </div>
</div>
<style>
    a{
        color: #ffffff!important;
    }
    a:hover{
        color: #ffffff!important;
        text-decoration: unset!important;
    }
    a:focus{
        color: #ffffff!important;
        text-decoration: unset!important;
    }
    .text-center.btn.btn-danger{
        display: none!important;
    }
    ul li{
        font-family: Futura-LT-Book, Sans-Serif!important;
        font-size: 15px;
    }
</style>