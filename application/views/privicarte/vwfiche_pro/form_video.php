<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
</head>
<div class="row bg_color_row">
    <div class="container">
            <p class="titre_content">Importer votre vidéo ici</p>
    </div>
</div>
<div class="container mt-3">
    <?php if(isset($message_success) && $message_success != null){ ?>
        <p class="alert alert-success"> <?php echo $message_success; ?></p>
    <?php } ?>
    <?php if(isset($message_error) && $message_error != null){ ?>
        <p class="alert alert-danger"> <?php echo $message_error; ?></p>
    <?php } ?>
    <div class="row">
        <form id="form_validation" method="post" action="<?php echo site_url('front/professionnels/add_video_cover') ?>" enctype="multipart/form-data">
            <div class="form-group">
                <label for="file_content">Séléctionnez vidéo:</label>
                <input type="hidden" name="IdCommercant" value="<?php echo $icat; ?>">
                <input type="hidden" name="cat" value="<?php echo $cat; ?>">
                <input type="hidden" name="user_ionauth_id" value="<?php echo $user_ionauth_id; ?>">
                <input name="video" type="file" class="form-control" id="file_content">
            </div>
            <div id="content_load" style="display: none">
                chargement du fichier .....
                <img src="<?php echo base_url('assets/soutenons/ajax-loader.gif'); ?>" class="img-fluid">
            </div>
            <button type="submit" class="btn btn-info">
                Téléchargez votre vidéo
            </button>
        </form>
    </div>
    <div class="row">
        <div class="content_video">
            <?php if(isset($get_data) && (!isset($message_error))){ ?>
                <script type="text/javascript">
                    $(document).ready(function(){
                        window.opener.location.reload();
                        window.close();
                    });
                </script>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#form_validation').submit(function(){
        $('#content_load').css('display','block');
    });
</script>
<style>
    .titre_content{
        font-size: 35px;
        color: #ffffff;
        text-align: center;
    }
    .bg_color_row{
        background-color: #3653A3;
    }
</style>