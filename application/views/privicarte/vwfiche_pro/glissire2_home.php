<div class="" id="div_pro_glissiere2_home"
     style="<?php if ($user_groups->id == '4') { ?>display:block;<?php } elseif ($user_groups->id == '5') { ?>display:none;<?php } ?>">
    <div class="col-lg-12"
         style="background-color: #3653A3;
                        color: #FFFFFF;
                                font-size: 13px;
                                font-weight: 700;
                                line-height: 1.23em;
                                margin-top: 12px;height: 40px; padding-top: 12px;"><?php if (isset($user_groups) && $user_groups->id == '5') { ?>Glissi&egrave;re 2 page présentation<?php } else { ?>Informations glissi&egrave;re 2<?php } ?>
        <span style="float:right;">
                                        <label style="color:#FFFFFF !important;">Active :</label>
                                            <select name="Glissiere[isActive_presentation_2]" id="isActive_presentation_2" style="color:#000000;">
                                                <option value="0"
                                                        <?php if (!isset($objGlissiere->isActive_presentation_2) OR
                                                        (isset($objGlissiere->isActive_presentation_2) && $objGlissiere->isActive_presentation_2 == '0') OR
                                                        (isset($objGlissiere->isActive_presentation_2) && $objGlissiere->isActive_presentation_2 == '')) { ?>selected="selected"<?php } ?>>non</option>
                                                <option value="1"
                                                        <?php if (isset($objGlissiere->isActive_presentation_2) AND $objGlissiere->isActive_presentation_2 == '1') { ?>selected="selected"<?php } ?>>oui</option>
                                            </select>
                                        </span>
    </div>
    <input type="hidden" id="nombre_blick_gli_presentation2" name="Glissiere[nombre_blick_gli_presentation2]" value="<?php if (isset($objGlissiere->nombre_blick_gli_presentation2)){echo $objGlissiere->nombre_blick_gli_presentation2;} ?>">
    <div class="pl-3 pr-3" id="dispnbgli2"
         style=" color:#000000;<?php if (!isset($objGlissiere->isActive_presentation_2) || (isset($objGlissiere->isActive_presentation_2) && $objGlissiere->isActive_presentation_2 == 0)) {
             echo 'display:none';
         } else if (isset($objGlissiere->isActive_presentation_2) && $objGlissiere->isActive_presentation_2 == '1') {
             echo 'display:block';
         } ?>">
        <div class="row rowbtn">
            <div class="col-lg-6 btn_vert" id="1g2_1"><img style="width: 100%;" src="<?php echo base_url('assets/img/1.png')?>"></div>
            <div class="col-lg-6 btn_vert " id="2g2_1"><img style="width: 100%;" src="<?php echo base_url('assets/img/2.png')?>"></div>

        </div>
    </div>

    <div id="glissiere2content" class="container-fluid"
         style="<?php if (isset($objGlissiere->isActive_presentation_2) AND $objGlissiere->isActive_presentation_2 =='1'){echo "display:block;";}else{echo "display:none;";} ?> width:100%;">
        <div class="row pt-3" id="gl2_tittle">
            <div class="col-lg-3 stl_long_input_platinum_td">
                <label class="label">Titre Glissière2: </label>
            </div>
            <div class="col-lg-9">
                <input class="form-control" type="text" name="Glissiere[presentation_2_titre]"
                       id="presentation_2_titre"
                       value="<?php if (isset($objGlissiere->presentation_2_titre)) echo htmlspecialchars_decode($objGlissiere->presentation_2_titre); ?>"/>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6" style="<?php if ((isset($objGlissiere->nombre_blick_gli_presentation2) AND $objGlissiere->nombre_blick_gli_presentation2 == 1) OR isset($objGlissiere->nombre_blick_gli_presentation2) AND $objGlissiere->nombre_blick_gli_presentation2 == 2 ){echo "display:block";}else{echo "display:none";} ?>" id="champ1gli2">

                <div class="container-fluid">
                    <div class="row pt-3">
                        <div class="col-lg-12" style="background-color: #3EB4D7;padding-top: 13px;height: 40px;color: white;margin-bottom: 20px"
                             class="text-center">Glissière2 champ1
                        </div>
                        <div class="col-lg-12">
                            <div class="img_tab" id="gl2_img">
                                <div class="row">
                                    <div class="col-lg-12 p-0">
                                        <div id="Articlepresentation_2_image_1_container" class="text-center row pb-3">
                                            <div class="col-lg-6">
                                                <a href='javascript:void(0);' title="presentation_2_image_1"
                                                    <?php if (empty($objGlissiere->presentation_2_image_1) || $objGlissiere->presentation_2_image_1 == "") { ?>
                                                        onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-gli-presentation_2_image_1"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                                    <?php } ?>
                                                   class="btn btn-info" id="presentation_2_image_1_link">Ajouter une Photo</a>
                                            </div>
                                            <div class="col-lg-6">
                                                <a href="javascript:void(0);" class="btn btn-danger"
                                                    <?php if (!empty($objGlissiere->presentation_2_image_1) && $objGlissiere->presentation_2_image_1 != "") { ?>
                                                        onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','presentation_2_image_1');"
                                                    <?php } ?>
                                                >Supprimer</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <?php
                                        if (is_object($objGlissiere) && file_exists($path_img_gallery . $objGlissiere->presentation_2_image_1) && $objGlissiere->presentation_2_image_1 != "") {
                                            echo '<img style="max-width:100%" class="img-fluid"  src="' . base_url() . $path_img_gallery . $objGlissiere->presentation_2_image_1 . '"/>';
                                        } else if (is_object($objGlissiere) && file_exists($path_img_gallery_old . $objGlissiere->presentation_2_image_1) && $objGlissiere->presentation_2_image_1 != "") {
                                            echo '<img  style="width:100%" class="img-fluid"  src="' . base_url() . $path_img_gallery_old . $objGlissiere->presentation_2_image_1 . '"/>';
                                        }
                                        ?>
                                        <input type="hidden" name="presentation_2_image_1" id="presentation_2_image_1"
                                               value="<?php if (is_object($objGlissiere) && isset($objGlissiere->presentation_2_image_1)) echo htmlspecialchars_decode($objGlissiere->presentation_2_image_1); ?>"/>
                                        <div id="div_error_taille_activite1_image1"
                                             class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite2_image1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 p-0">
                            <div id="gl1_content">
                                <div colspan="2">

                                            <textarea name="Glissiere[presentation_2_contenu1]"
                                                      id="presentation_2_contenu1"><?php if (isset($objGlissiere->presentation_2_contenu1)) echo htmlspecialchars_decode($objGlissiere->presentation_2_contenu1); ?></textarea>
                                    <script>

                                        CKEDITOR.replace('presentation_2_contenu1');

                                    </script>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-12 p-0">
                            <div class="container-fluid p-0" style="">
                                <div class="pb-3">
                                    <div class="col-lg-12" id="btn_gli2"
                                         style="background-color: #3EB4D7;height: 40px;padding-top: 12px;color: black">
                                        <span style="color: white!important;">Ajouter un Bouton</span> <span
                                            style="float: right;">
                                                                        <select name="Glissiere[is_activ_btn_glissiere2_champ1]"
                                                                                style="margin-right: 10px;color: black!important;"
                                                                                id="link_btn_2_gli">
                                                                            <option <?php if (is_object($objGlissiere) && $objGlissiere->is_activ_btn_glissiere2_champ1 == 0) {
                                                                                echo 'selected';
                                                                            } ?> value="0">non</option>
                                                                            <option <?php if (is_object($objGlissiere) && $objGlissiere->is_activ_btn_glissiere2_champ1 == 1) {
                                                                                echo 'selected';
                                                                            } ?> value="1">oui</option>
                                                                        </select></span>
                                    </div>


                                    <div class="text-center pt-3" id="alllinkg2" style="<?php if (is_object($objGlissiere) && $objGlissiere->is_activ_btn_glissiere2_champ1 == 1) {echo "display: block";} else {echo "display:none";} ?>">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <label for="btn_gli1_content1">Texte du bouton</label>
                                            </div>
                                            <div class="col-lg-9">
                                                <input class="form-control" id="btn_gli2_content1" type="text"
                                                       value="<?php if (isset($objGlissiere->btn_gli2_content1) AND $objGlissiere->btn_gli2_content1 != "") {
                                                           echo $objGlissiere->btn_gli2_content1;
                                                       } ?>" name="Glissiere[btn_gli2_content1]">
                                            </div>
                                        </div>
                                        <div class="row pt-2">
                                            <div class="col-lg-3">
                                                <label for="valuelink">Lien du bouton</label>
                                            </div>
                                            <div class="col-lg-9">
                                                <select
                                                    name="Glissiere[gl2_existed_link1]" class="form-control"
                                                    style="margin-right: 10px;color: black" id="valuelink2">
                                                    <option <?php if (isset($objGlissiere->gl2_existed_link1) AND $objGlissiere->gl2_existed_link1 == '0') {
                                                        echo 'selected';
                                                    } ?> value="0">Choisir un lien local1</option><?php if ($user_groups->id == '5') {?>
                                                    <option <?php if (isset($objGlissiere->gl2_existed_link1) AND $objGlissiere->gl2_existed_link1 == 'p1') {
                                                        echo 'selected';
                                                    } ?> value="p1">Vers la page 1</option><?php } ?>
                                                    <option <?php if (isset($objGlissiere->gl2_existed_link1) AND $objGlissiere->gl2_existed_link1 == 'p2') {
                                                        echo 'selected';
                                                    } ?> value="p2">Vers la page 2</option>
                                                    <option <?php if (isset($objGlissiere->gl2_existed_link1) AND $objGlissiere->gl2_existed_link1 == 'ag') {
                                                        echo 'selected';
                                                    } ?> value="ag">Mes Agendas</option>
                                                    <option <?php if (isset($objGlissiere->gl2_existed_link1) AND $objGlissiere->gl2_existed_link1 == 'art') {
                                                        echo 'selected';
                                                    } ?> value="art">Mes Articles</option>
                                                    <option <?php if (isset($objGlissiere->gl2_existed_link1) AND $objGlissiere->gl2_existed_link1 == 'bp') {
                                                        echo 'selected';
                                                    } ?> value="bp">Mes Bons Plans</option>
                                                    <option <?php if (isset($objGlissiere->gl2_existed_link1) AND $objGlissiere->gl2_existed_link1 == 'bt') {
                                                        echo 'selected';
                                                    } ?> value="bt">Mes Boutiques</option>
                                                    <option <?php if (isset($objGlissiere->gl2_existed_link1) AND $objGlissiere->gl2_existed_link1 == 'fd') {
                                                        echo 'selected';
                                                    } ?> value="fd">Ma Fidelisation</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                Ou ajouter un lien externe
                                            </div>
                                            <div class="col-lg-9">
                                                <div class="row">
                                                    <div class="col-lg-3 text-left pl-3"
                                                         style="">
                                                        <select class="text-center form-control" id="htt">
                                                            <option <?php if (is_object($objGlissiere) && preg_match('/http:/', $objGlissiere->gl2_custom_link1)) {
                                                                echo 'selected';
                                                            } ?> value="http://">http://
                                                            </option>
                                                            <option <?php if (is_object($objGlissiere) && preg_match('/http:/', $objGlissiere->gl2_custom_link1)) {
                                                                echo 'selected';
                                                            } ?> value="https://">https://
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-9 text-right pr0"
                                                         style="">
                                                        <input onchange="sub_link_custom1()"
                                                               name="Glissiere[gl2_custom_link1]"
                                                               class="form-control text-center" type="text" value="<?php echo $objGlissiere->gl2_custom_link1 ?? '';?>"
                                                               id="link_htt"
                                                               placeholder="votre liens ici">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6" style="<?php if (isset($objGlissiere->nombre_blick_gli_presentation2) AND $objGlissiere->nombre_blick_gli_presentation2 == 2){echo "display:block";}elseif (isset($objGlissiere->nombre_blick_gli_presentation2) AND $objGlissiere->nombre_blick_gli_presentation2 !=2){echo "display:none";} ?>" id="champ2_gli2content">
                <div class="row  pt-3">
                    <div class="col-lg-12">
                        <div style="background-color: #3EB4D7;padding-top: 13px;height: 40px;color: white;margin-bottom: 20px"
                             class="text-center">Glissière2 champ2
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="img_tab" id="gl1_img2">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="text-center row pb-3" id="Articlepresentation_2_image_2_container">
                                        <div class="col-lg-6">
                                            <a href='javascript:void(0);' title="presentation_2_image_2"
                                                <?php if (empty($objGlissiere->presentation_2_image_2) || $objGlissiere->presentation_2_image_2 == "") { ?>
                                                    onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-gli-presentation_2_image_2"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                                <?php } ?>
                                               class="btn btn-info" id="presentation_2_image_2_link">Ajouter une Photo</a>
                                        </div>
                                        <div class="col-lg-6">
                                            <a href="javascript:void(0);" class="btn btn-danger"
                                                <?php if (!empty($objGlissiere->presentation_2_image_2) && $objGlissiere->presentation_2_image_2 != "") { ?>
                                                    onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','presentation_2_image_2');"
                                                <?php } ?>
                                            >Supprimer</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <?php
                                    if (is_object($objGlissiere) && file_exists($path_img_gallery . $objGlissiere->presentation_2_image_2) && $objGlissiere->presentation_2_image_2 != "") {
                                        echo '<img style="max-width:100%" class="img-fluid"  src="' . base_url() . $path_img_gallery . $objGlissiere->presentation_2_image_2 . '"/>';
                                    } else if (is_object($objGlissiere) && file_exists($path_img_gallery_old . $objGlissiere->presentation_1_image_2) && $objGlissiere->presentation_2_image_2 != "") {
                                        echo '<img style="max-width:100%" class="img-fluid"  src="' . base_url() . $path_img_gallery_old . $objGlissiere->presentation_2_image_2 . '"/>';
                                    }
                                    ?>
                                    <input type="hidden" name="presentation_2_image_2" id="presentation_2_image_2"
                                           value="<?php if (isset($objGlissiere->presentation_2_image_2)) echo htmlspecialchars_decode($objGlissiere->presentation_2_image_2); ?>"/>
                                    <div id="div_error_taille_activite2_image2"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite2_image1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div id="gl2_content2">
                            <div colspan="2">

                                            <textarea name="Glissiere[presentation_2_contenu2]"
                                                      id="presentation_2_contenu2"><?php if (isset($objGlissiere->presentation_2_contenu2)) echo htmlspecialchars_decode($objGlissiere->presentation_2_contenu2); ?></textarea>
                                <script>

                                    CKEDITOR.replace('presentation_2_contenu2');

                                </script>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div>
                            <div class="pb-3">
                                <div class="col-lg-12" id="btn_2_gli2"
                                     style="background-color: #3EB4D7;height: 40px;padding-top: 12px;color: black">
                                    <span style="color: white!important;">Ajouter un Bouton</span> <span
                                        style="float: right;">
                                                                        <select name="Glissiere[is_activ_btn_glissiere2_champ2]"
                                                                                style="margin-right: 10px;color: black!important;"
                                                                                id="link_btn_2_gl2">
                                                                            <option <?php if (!isset($objGlissiere->is_activ_btn_glissiere2_champ2) OR (isset($objGlissiere->is_activ_btn_glissiere2_champ2) && $objGlissiere->is_activ_btn_glissiere2_champ2 == 0)) {
                                                                                echo 'selected';
                                                                            } ?> value="0">non</option>
                                                                            <option <?php if (isset($objGlissiere->is_activ_btn_glissiere2_champ2) && $objGlissiere->is_activ_btn_glissiere2_champ2 == 1) {
                                                                                echo 'selected';
                                                                            } ?> value="1">oui</option>
                                                                        </select></span>
                                </div>

                                <div class="text-center pt-3" id="alllink2g2" style="<?php if (isset($objGlissiere->is_activ_btn_glissiere2_champ2) AND $objGlissiere->is_activ_btn_glissiere2_champ2=='1'){echo "display:block;";}else{echo "display:none;";} ?>">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label for="btn_gli2_content2">Texte du bouton</label>
                                        </div>
                                        <div class="col-lg-9">
                                            <input class="form-control" id="btn_gli1_content2" type="text"
                                                   value="<?php if (isset($objGlissiere->btn_gli2_content2) AND $objGlissiere->btn_gli2_content2 != "") {
                                                       echo $objGlissiere->btn_gli2_content2;
                                                   } ?>" name="Glissiere[btn_gli2_content2]">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label for="valuelink2g2">Lien du bouton</label>
                                        </div>
                                        <div class="col-lg-9">
                                            <select name="Glissiere[gl2_existed_link2]"
                                                    class="form-control"
                                                    style="margin-right: 10px;color: black"
                                                    id="valuelink2g2">
                                                <option <?php if (isset($objGlissiere->gl2_existed_link2) AND $objGlissiere->gl2_existed_link2 == '0') {
                                                    echo 'selected';
                                                } ?> value="0">Choisir un lien local2</option><?php if ($user_groups->id == '5') {?>
                                                <option <?php if (isset($objGlissiere->gl2_existed_link2) AND $objGlissiere->gl2_existed_link2 == 'p1') {
                                                    echo 'selected';
                                                } ?> value="p1">Vers la page 1</option><?php } ?>
                                                <option <?php if (isset($objGlissiere->gl2_existed_link2) AND $objGlissiere->gl2_existed_link2 == 'p2') {
                                                    echo 'selected';
                                                } ?> value="p2">Vers la page 2</option>
                                                <option <?php if (isset($objGlissiere->gl2_existed_link2) AND $objGlissiere->gl2_existed_link2 == 'ag') {
                                                    echo 'selected';
                                                } ?> value="ag">Mes Agendas</option>
                                                <option <?php if (isset($objGlissiere->gl2_existed_link2) AND $objGlissiere->gl2_existed_link2 == 'art') {
                                                    echo 'selected';
                                                } ?> value="art">Mes Articles</option>
                                                <option <?php if (isset($objGlissiere->gl2_existed_link2) AND $objGlissiere->gl2_existed_link2 == 'bp') {
                                                    echo 'selected';
                                                } ?> value="bp">Mes Bons Plans</option>
                                                <option <?php if (isset($objGlissiere->gl2_existed_link2) AND $objGlissiere->gl2_existed_link2 == 'bt') {
                                                    echo 'selected';
                                                } ?> value="bt">Mes Boutiques</option>
                                                <option <?php if (isset($objGlissiere->gl2_existed_link2) AND $objGlissiere->gl2_existed_link2 == 'fd') {
                                                    echo 'selected';
                                                } ?> value="fd">Ma Fidelisation</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            Ou ajouter un lien externe
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <div class="text-left" style="">
                                                        <select class="form-control text-left" id="htt2g2">
                                                            <option <?php if (isset($objGlissiere->gl2_custom_link2) && preg_match('/http:/', $objGlissiere->gl2_custom_link2)) {
                                                                echo 'selected';
                                                            } ?> value="http://">http://
                                                            </option>
                                                            <option <?php if (isset($objGlissiere->gl2_custom_link2) && preg_match('/https:/', $objGlissiere->gl2_custom_link2)) {
                                                                echo 'selected';
                                                            } ?> value="https://">https://
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-9">
                                                    <div class="text-right pr-0" style="">
                                                        <input onchange="sub_link_custom2()" name="Glissiere[gl2_custom_link2]"
                                                               class="form-control text-center" type="text" value="<?php echo $objGlissiere->gl2_custom_link2 ?? '';?>"
                                                               id="link_htt2g2"
                                                               placeholder="votre liens ici">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>