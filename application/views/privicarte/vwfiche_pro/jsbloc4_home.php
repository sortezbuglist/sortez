<script type="text/javascript">
    jQuery(document).ready(function () {
        <?php if (isset($objGlissiere->is_activ_bloc4) AND $objGlissiere->is_activ_bloc4 == 1) {  ?>
        jQuery('#type_bloc4_choose').css("display", "block");
        <?php if (isset($objGlissiere->type_bloc4) AND $objGlissiere->type_bloc4 == '1' OR isset($objGlissiere->type_bloc4) AND $objGlissiere->type_bloc4 =='2'){  ?>
        document.getElementById('bloc_4a').style.display = "block";
        document.getElementById('bloc2_4a').style.display = "none";
        document.getElementById('bloc3_4a').style.display = "none";
        document.getElementById('bonplan_config_4').style.display = "none";
        document.getElementById('fidelity_4_config').style.display = "none";
        $('#type_bloc4js').val("1");
        $("#bloc_4a").removeClass("col-lg-6");
        $("#bloc2_4a").removeClass("col-lg-6");
        $("#bloc_4a").removeClass("col-lg-4");
        $("#bloc2_4a").removeClass("col-lg-4");
        $("#bloc3_4a").removeClass("col-lg-4");
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc4) AND $objGlissiere->type_bloc4 == '2') {  ?>
        document.getElementById('bloc_4a').style.display = "block";
        document.getElementById('bloc2_4a').style.display = "block";
        document.getElementById('bloc3_4a').style.display = "none";
        document.getElementById('bonplan_config_4').style.display = "none";
        document.getElementById('fidelity_4_config').style.display = "none";
        $('#type_bloc4js').val("2");
        $("#bloc_4a").addClass("col-lg-6");
        $("#bloc2_4a").addClass("col-lg-6");
        $("#bloc_4a").removeClass("col-lg-4");
        $("#bloc2_4a").removeClass("col-lg-4");
        $("#bloc3_4a").removeClass("col-lg-4");
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc4) AND $objGlissiere->type_bloc4 == '3') {  ?>
        document.getElementById('bloc_4a').style.display = "block";
        document.getElementById('bloc2_4a').style.display = "block";
        document.getElementById('bloc3_4a').style.display = "block";
        document.getElementById('bonplan_config_4').style.display = "none";
        document.getElementById('fidelity_4_config').style.display = "none";
        $('#type_bloc4js').val("3");
        $("#bloc_4a").addClass("col-lg-4");
        $("#bloc2_4a").addClass("col-lg-4");
        $("#bloc3_4a").addClass("col-lg-4");
        $("#bloc_4a").removeClass("col-lg-6");
        $("#bloc2_4a").removeClass("col-lg-6");
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc4) AND $objGlissiere->type_bloc4 == '1bp') {  ?>
        document.getElementById('bonplan_config_4').style.display = "block";
        document.getElementById('bloc_4a').style.display = "none";
        document.getElementById('bloc2_4a').style.display = "none";
        document.getElementById('bloc3_4a').style.display = "none";
        document.getElementById('fidelity_4_config').style.display = "none";
        $('#type_bloc4js').val("1bp");
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc4) AND $objGlissiere->type_bloc4 == '1fd') {  ?>
        document.getElementById('fidelity_4_config').style.display = "block";
        document.getElementById('bonplan_config_4').style.display = "none";
        document.getElementById('bloc_4a').style.display = "none";
        document.getElementById('bloc2_4a').style.display = "none";
        document.getElementById('bloc3_4a').style.display = "none";
        $('#type_bloc4js').val("1fd");
        <?php } ?>
        <?php if (isset($objGlissiere->is_activ_btn_bloc1_4) && $objGlissiere->is_activ_btn_bloc1_4 == '1') {  ?>
        document.getElementById('link_bloc1_4').style.display = "block";
        <?php } ?>
        <?php if (isset($objGlissiere->is_activ_btn_bloc2_4a) && $objGlissiere->is_activ_btn_bloc2_4a == '1') {  ?>
        document.getElementById('link_bloc2_4a').style.display = "block";
        <?php } ?>
        <?php if (isset($objGlissiere->is_activ_btn_bp_4) && $objGlissiere->is_activ_btn_bp_4 == '1') {  ?>
        document.getElementById('link_bp_4').style.display = "block";
        <?php } ?>
        <?php if (isset($objGlissiere->is_activ_btn_fd_4) && $objGlissiere->is_activ_btn_fd_4 == '1') {  ?>
        document.getElementById('link_fd_4').style.display = "block";
        <?php } ?>
        <?php } ?>
        $("#is_activ_bloc4").change(function () {
            if (document.getElementById('is_activ_bloc4').value == 0) {
                jQuery('#type_bloc4_choose').css("display", "none");

                jQuery('#bloc_4a').css("display", "none");

                jQuery('#bloc2_4a').css("display", "none");

                jQuery('#bloc3_4a').css("display", "none");

                jQuery('#bonplan_config_4').css("display", "none");

                jQuery('#fidelity_4_config').css("display", "none");
            }
            else if (document.getElementById('is_activ_bloc4').value == 1) {
                jQuery('#type_bloc4_choose').css("display", "block");
                jQuery('#bloc_4a').css("display", "none");

                jQuery('#bloc2_4a').css("display", "none");

                jQuery('#bloc3_4a').css("display", "none");

                jQuery('#bonplan_config_4').css("display", "none");

                jQuery('#fidelity_4_config').css("display", "none");
            }
        });
        $("#1b4_1").click(function () {
            document.getElementById('bloc_4a').style.display = "block";
            document.getElementById('bloc2_4a').style.display = "none";
            document.getElementById('bloc3_4a').style.display = "none";
            document.getElementById('bonplan_config_4').style.display = "none";
            document.getElementById('fidelity_4_config').style.display = "none";
            $('#type_bloc4js').val("1");
            $("#bloc_4a").removeClass("col-lg-6");
            $("#bloc2_4a").removeClass("col-lg-6");
            $("#bloc_4a").removeClass("col-lg-4");
            $("#bloc2_4a").removeClass("col-lg-4");
            $("#bloc3_4a").removeClass("col-lg-4");
        });
        $("#2b4_1").click(function () {
            document.getElementById('bloc_4a').style.display = "block";
            document.getElementById('bloc2_4a').style.display = "block";
            document.getElementById('bloc3_4a').style.display = "none";
            document.getElementById('bonplan_config_4').style.display = "none";
            document.getElementById('fidelity_4_config').style.display = "none";
            $('#type_bloc4js').val("2");
            $("#bloc_4a").addClass("col-lg-6");
            $("#bloc2_4a").addClass("col-lg-6");
            $("#bloc_4a").removeClass("col-lg-4");
            $("#bloc2_4a").removeClass("col-lg-4");
            $("#bloc3_4a").removeClass("col-lg-4");
        });$("#3b4_1").click(function () {
            document.getElementById('bloc_4a').style.display = "block";
            document.getElementById('bloc2_4a').style.display = "block";
            document.getElementById('bloc3_4a').style.display = "block";
            document.getElementById('bonplan_config_4').style.display = "none";
            document.getElementById('fidelity_4_config').style.display = "none";
            $('#type_bloc4js').val("3");
            $("#bloc_4a").addClass("col-lg-4");
            $("#bloc2_4a").addClass("col-lg-4");
            $("#bloc3_4a").addClass("col-lg-4");
            $("#bloc_4a").removeClass("col-lg-6");
            $("#bloc2_4a").removeClass("col-lg-6");
        });
        $("#1bp4_1").click(function () {
            document.getElementById('bonplan_config_4').style.display = "block";
            document.getElementById('bloc_4a').style.display = "none";
            document.getElementById('bloc2_4a').style.display = "none";
            document.getElementById('bloc3_4a').style.display = "none";
            document.getElementById('fidelity_4_config').style.display = "none";
            $('#type_bloc4js').val("1bp");
        });
        $("#1fd4_1").click(function () {
            document.getElementById('fidelity_4_config').style.display = "block";
            document.getElementById('bonplan_config_4').style.display = "none";
            document.getElementById('bloc_4a').style.display = "none";
            document.getElementById('bloc2_4a').style.display = "none";
            document.getElementById('bloc3_4a').style.display = "none";
            $('#type_bloc4js').val("1fd");
        });
        $("#link_btn_bloc1_4").change(function () {

            if (document.getElementById('link_btn_bloc1_4').value == 1) {
                document.getElementById('link_bloc1_4').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc1_4').value == 0) {
                document.getElementById('link_bloc1_4').style.display = "none";

            }

        });
        $("#link_btn_bloc2_4").change(function () {


            if (document.getElementById('link_btn_bloc2_4').value == 1) {
                document.getElementById('link_bloc2_4').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc2_4').value == 0) {
                document.getElementById('link_bloc2_4').style.display = "none";

            }

        });
        $("#link_btn_bloc3_4").change(function () {


            if (document.getElementById('link_btn_bloc3_4').value == 1) {
                document.getElementById('link_bloc3_4').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc3_4').value == 0) {
                document.getElementById('link_bloc3_4').style.display = "none";

            }

        });
        $("#link_btn_bp_4").change(function () {

            if (document.getElementById('link_btn_bp_4').value == 1) {
                document.getElementById('link_bp_4').style.display = "block";

            }
            else if (document.getElementById('link_btn_bp_4').value == 0) {
                document.getElementById('link_bp_4').style.display = "none";

            }

        });
        $("#link_btn_fd_4").change(function () {

            if (document.getElementById('link_btn_fd_4').value == 1) {
                document.getElementById('link_fd_4').style.display = "block";

            }
            else if (document.getElementById('link_btn_fd_4').value == 0) {
                document.getElementById('link_fd_4').style.display = "none";

            }

        });
    });
</script>