<script type="text/javascript">
    jQuery(document).ready(function () {
        <?php if (isset($objbloc_info->is_activ_bloc4_page1) AND $objbloc_info->is_activ_bloc4_page1 == 1) {  ?>
                jQuery('#type_bloc4_page1_choose').css("display", "block"); 
                <?php if (isset($objbloc_info->type_bloc4_page1) AND $objbloc_info->type_bloc4_page1 == '1' OR isset($objbloc_info->type_bloc4_page1) AND $objbloc_info->type_bloc4_page1 =='2'){  ?>
                document.getElementById('bloc_4_page1').style.display = "block";
                document.getElementById('bloc2_4_page1').style.display = "none";
                document.getElementById('bloc3_4_page1').style.display = "none";
                document.getElementById('bonplan_config_4_page1').style.display = "none";
                document.getElementById('fidelity_4_config_page1').style.display = "none";
                $('#type_bloc4_page1js').val("1");
                $("#bloc_4_page1").removeClass("col-lg-6");
                $("#bloc2_4_page1").removeClass("col-lg-6");
                $("#bloc_4_page1").removeClass("col-lg-4");
                $("#bloc2_4_page1").removeClass("col-lg-4");
                $("#bloc3_4_page1").removeClass("col-lg-4");
            <?php } ?>
            <?php if (isset($objbloc_info->type_bloc4_page1) AND $objbloc_info->type_bloc4_page1 == '2') {  ?>
                 document.getElementById('bloc_4_page1').style.display = "block";
                document.getElementById('bloc2_4_page1').style.display = "block";
                document.getElementById('bloc3_4_page1').style.display = "none";
                document.getElementById('bonplan_config_4_page1').style.display = "none";
                document.getElementById('fidelity_4_config_page1').style.display = "none";
                $('#type_bloc4_page1js').val("2");
                $("#bloc_4_page1").addClass("col-lg-6");
                $("#bloc2_4_page1").addClass("col-lg-6");
                $("#bloc_4_page1").removeClass("col-lg-4");
                $("#bloc2_4_page1").removeClass("col-lg-4");
                $("#bloc3_4_page1").removeClass("col-lg-4");
            <?php } ?>
            <?php if (isset($objbloc_info->type_bloc4_page1) AND $objbloc_info->type_bloc4_page1 == '3') {  ?>
                 document.getElementById('bloc_4_page1').style.display = "block";
                document.getElementById('bloc2_4_page1').style.display = "block";
                document.getElementById('bloc3_4_page1').style.display = "block";
                document.getElementById('bonplan_config_4_page1').style.display = "none";
                document.getElementById('fidelity_4_config_page1').style.display = "none";
                $('#type_bloc4_page1js').val("3");
                $("#bloc_4_page1").addClass("col-lg-4");
                $("#bloc2_4_page1").addClass("col-lg-4");
                $("#bloc3_4_page1").addClass("col-lg-4");
                $("#bloc_4_page1").removeClass("col-lg-6");
                $("#bloc2_4_page1").removeClass("col-lg-6");
            <?php } ?>
            <?php if (isset($objbloc_info->type_bloc4_page1) AND $objbloc_info->type_bloc4_page1 == '1bp') {  ?>
               document.getElementById('bonplan_config_4_page1').style.display = "block";
                document.getElementById('bloc_4_page1').style.display = "none";
                document.getElementById('bloc2_4_page1').style.display = "none";
                document.getElementById('bloc3_4_page1').style.display = "none";
                document.getElementById('fidelity_4_config_page1').style.display = "none";
                $('#type_bloc4_page1js').val("1bp");
            <?php } ?>
            <?php if (isset($objbloc_info->type_bloc4_page1) AND $objbloc_info->type_bloc4_page1 == '1fd') {  ?>
               document.getElementById('fidelity_4_config_page1').style.display = "block";
                document.getElementById('bonplan_config_4_page1').style.display = "none";
                document.getElementById('bloc_4_page1').style.display = "none";
                document.getElementById('bloc2_4_page1').style.display = "none";
                document.getElementById('bloc3_4_page1').style.display = "none";
                $('#type_bloc4_page1js').val("1fd");
            <?php } ?>
            <?php if (isset($objbloc_info->is_activ_btn_bloc1_4_page1) && $objbloc_info->is_activ_btn_bloc1_4_page1 == '1') {  ?>
                document.getElementById('link_bloc_1_4_page1').style.display = "block";
            <?php } ?>
            <?php if (isset($objbloc_info->is_activ_btn_bloc2_4_page1) && $objbloc_info->is_activ_btn_bloc2_4_page1 == '1') {  ?>
                document.getElementById('link_bloc2_4_page1').style.display = "block";
            <?php } ?>  
            <?php if (isset($objbloc_info->is_activ_btn_bp_4_page1) && $objbloc_info->is_activ_btn_bp_4_page1 == '1') {  ?>
                document.getElementById('link_bp_4_page1').style.display = "block";
            <?php } ?>
            <?php if (isset($objbloc_info->is_activ_btn_fd_4_page1) && $objbloc_info->is_activ_btn_fd_4_page1 == '1') {  ?>
                document.getElementById('link_fidelity_4page1').style.display = "block";
            <?php } ?>            
        <?php } ?>
        $("#is_activ_bloc4_page1").change(function () {
            if (document.getElementById('is_activ_bloc4_page1').value == 0) {
                 jQuery('#type_bloc4_page1_choose').css("display", "none");
               
                jQuery('#bloc_4_page1').css("display", "none");
                
                jQuery('#bloc2_4_page1').css("display", "none");

                jQuery('#bloc3_4_page1').css("display", "none");

                jQuery('#bonplan_config_4_page1').css("display", "none");
                
                jQuery('#fidelity_4_config_page1').css("display", "none");
            }
            else if (document.getElementById('is_activ_bloc4_page1').value == 1) {
                jQuery('#type_bloc4_page1_choose').css("display", "block");
                jQuery('#bloc_4_page1').css("display", "none");
                
                jQuery('#bloc2_4_page1').css("display", "none");

                jQuery('#bloc3_4_page1').css("display", "none");

                jQuery('#bonplan_config_4_page1').css("display", "none");
                
                jQuery('#fidelity_4_config_page1').css("display", "none");
            }
        });
      $("#1b4_1_page1").click(function () {
            document.getElementById('bloc_4_page1').style.display = "block";
            document.getElementById('bloc2_4_page1').style.display = "none";
            document.getElementById('bloc3_4_page1').style.display = "none";
            document.getElementById('bonplan_config_4_page1').style.display = "none";
            document.getElementById('fidelity_4_config_page1').style.display = "none";
            $('#type_bloc4_page1js').val("1");
            $("#bloc_4_page1").removeClass("col-lg-6");
            $("#bloc2_4_page1").removeClass("col-lg-6");
            $("#bloc_4_page1").removeClass("col-lg-4");
            $("#bloc2_4_page1").removeClass("col-lg-4");
            $("#bloc3_4_page1").removeClass("col-lg-4");
        });
        $("#2b4_1_page1").click(function () {
            document.getElementById('bloc_4_page1').style.display = "block";
            document.getElementById('bloc2_4_page1').style.display = "block";
            document.getElementById('bloc3_4_page1').style.display = "none";
            document.getElementById('bonplan_config_4_page1').style.display = "none";
            document.getElementById('fidelity_4_config_page1').style.display = "none";
            $('#type_bloc4_page1js').val("2");
            $("#bloc_4_page1").addClass("col-lg-6");
            $("#bloc2_4_page1").addClass("col-lg-6");
            $("#bloc_4_page1").removeClass("col-lg-4");
            $("#bloc2_4_page1").removeClass("col-lg-4");
            $("#bloc3_4_page1").removeClass("col-lg-4");
        });$("#3b4_1_page1").click(function () {
            document.getElementById('bloc_4_page1').style.display = "block";
            document.getElementById('bloc2_4_page1').style.display = "block";
            document.getElementById('bloc3_4_page1').style.display = "block";
            document.getElementById('bonplan_config_4_page1').style.display = "none";
            document.getElementById('fidelity_4_config_page1').style.display = "none";
            $('#type_bloc4_page1js').val("3");
            $("#bloc_4_page1").addClass("col-lg-4");
            $("#bloc2_4_page1").addClass("col-lg-4");
            $("#bloc3_4_page1").addClass("col-lg-4");
            $("#bloc_4_page1").removeClass("col-lg-6");
            $("#bloc2_4_page1").removeClass("col-lg-6");
        });
        $("#1bp4_1_page1").click(function () {
            document.getElementById('bonplan_config_4_page1').style.display = "block";
            document.getElementById('bloc_4_page1').style.display = "none";
            document.getElementById('bloc2_4_page1').style.display = "none";
            document.getElementById('bloc3_4_page1').style.display = "none";
            document.getElementById('fidelity_4_config_page1').style.display = "none";
            $('#type_bloc4_page1js').val("1bp");
        });
        $("#1fd4_1_page1").click(function () {
            document.getElementById('fidelity_4_config_page1').style.display = "block";
            document.getElementById('bonplan_config_4_page1').style.display = "none";
            document.getElementById('bloc_4_page1').style.display = "none";
            document.getElementById('bloc2_4_page1').style.display = "none";
            document.getElementById('bloc3_4_page1').style.display = "none";
            $('#type_bloc4_page1js').val("1fd");
        });
        $("#link_btn_bloc_1_4_page1").change(function () {

            if (document.getElementById('link_btn_bloc_1_4_page1').value == 1) {
                document.getElementById('link_bloc_1_4_page1').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc_1_4_page1').value == 0) {
                document.getElementById('link_bloc_1_4_page1').style.display = "none";

            }

        }); 
        $("#link_btn_bloc2_4_page1").change(function () {


            if (document.getElementById('link_btn_bloc2_4_page1').value == 1) {
                document.getElementById('link_bloc2_4_page1').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc2_4_page1').value == 0) {
                document.getElementById('link_bloc2_4_page1').style.display = "none";

            }

        });
        $("#link_btn_bloc3_4_page1").change(function () {


            if (document.getElementById('link_btn_bloc3_4_page1').value == 1) {
                document.getElementById('link_bloc3_4_page1').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc3_4_page1').value == 0) {
                document.getElementById('link_bloc3_4_page1').style.display = "none";

            }

        });
        $("#link_btn_bp_4_page1").change(function () {

            if (document.getElementById('link_btn_bp_4_page1').value == 1) {
                document.getElementById('link_bp_4_page1').style.display = "block";

            }
            else if (document.getElementById('link_btn_bp_4_page1').value == 0) {
                document.getElementById('link_bp_4_page1').style.display = "none";

            }

        });
        $("#link_btn_fidelity_4page1").change(function () {

            if (document.getElementById('link_btn_fidelity_4page1').value == 1) {
                document.getElementById('link_fidelity_4page1').style.display = "block";

            }
            else if (document.getElementById('link_btn_fidelity_4page1').value == 0) {
                document.getElementById('link_fidelity_4page1').style.display = "none";

            }

        }); 
    });
</script>