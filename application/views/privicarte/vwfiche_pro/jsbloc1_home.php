<script type="text/javascript">
    jQuery(document).ready(function () {

        <?php if (isset($objGlissiere->is_activ_bloc) AND $objGlissiere->is_activ_bloc == 1) {  ?>
        jQuery('#type_bloc_choose').css("display", "block");
        <?php if (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc == '1') {  ?>
        document.getElementById('bloc_1_1').style.display = "block";
        $("#bloc_1_1").removeClass("col-lg-6");
        $("#bloc_1_2").removeClass("col-lg-6");
        $("#bloc_1_2").removeClass("col-lg-4");
        $("#bloc_1_1").removeClass("col-lg-4");
        $("#bloc_1_3").removeClass("col-lg-4");
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc == '2') {  ?>
        document.getElementById('bloc_1_2').style.display = "block";
        document.getElementById('bloc_1_1').style.display = "block";
        $("#bloc_1_2").addClass("col-lg-6");
        $("#bloc_1_1").addClass("col-lg-6");
        $("#bloc_1_2").removeClass("col-lg-4");
        $("#bloc_1_1").removeClass("col-lg-4");
        $("#bloc_1_3").removeClass("col-lg-4");
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc == '3') {  ?>
        document.getElementById('bloc_1_2').style.display = "block";
        document.getElementById('bloc_1_1').style.display = "block";
        document.getElementById('bloc_1_3').style.display = "block";
        $("#bloc_1_2").addClass("col-lg-4");
        $("#bloc_1_1").addClass("col-lg-4");
        $("#bloc_1_3").addClass("col-lg-4");
        $("#bloc_1_1").removeClass("col-lg-6");
        $("#bloc_1_2").removeClass("col-lg-6");
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc == '1bp') {  ?>
        document.getElementById('bonplan_config').style.display = "block";
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc == '1fd') {  ?>
        document.getElementById('fidelity_config').style.display = "block";
        <?php } ?>
        <?php if (isset($objGlissiere->is_activ_btn_bloc_1_1) && $objGlissiere->is_activ_btn_bloc_1_1 == '1') {  ?>
        document.getElementById('link_bloc_1_1').style.display = "block";
        <?php } ?>
        <?php if (isset($objGlissiere->is_activ_btn_bloc_1_1_2) && $objGlissiere->is_activ_btn_bloc_1_1_2 == '1') {  ?>
        document.getElementById('link_bloc_1_2').style.display = "block";
        <?php } ?>
        <?php if (isset($objGlissiere->is_activ_btn_bp) && $objGlissiere->is_activ_btn_bp == '1') {  ?>
        document.getElementById('link_bp').style.display = "block";
        <?php } ?>
        <?php if (isset($objGlissiere->is_activ_btn_fd) && $objGlissiere->is_activ_btn_fd == '1') {  ?>
        document.getElementById('link_fd').style.display = "block";
        <?php } ?>
        <?php } ?>
        $("#is_activ_bloc").change(function () {
            if (document.getElementById('is_activ_bloc').value == 0) {

                jQuery('#type_bloc_choose').css("display", "none");

                jQuery('#bloc_1_1').css("display", "none");

                jQuery('#bloc_1_2').css("display", "none");

                jQuery('#bloc_1_3').css("display", "none");

                jQuery('#bonplan_config').css("display", "none");

                jQuery('#fidelity_config').css("display", "none");
            }
            else if (document.getElementById('is_activ_bloc').value == 1) {

                jQuery('#type_bloc_choose').css("display", "block");

                jQuery('#bloc_1_1').css("display", "none");

                jQuery('#bloc_1_2').css("display", "none");

                jQuery('#bloc_1_3').css("display", "none");

                jQuery('#bonplan_config').css("display", "none");

                jQuery('#fidelity_config').css("display", "none");
            }
        });
        $("#1b1_1").click(function () {

            document.getElementById('bloc_1_1').style.display = "block";
            document.getElementById('bloc_1_2').style.display = "none";
            document.getElementById('bloc_1_3').style.display = "none";
            document.getElementById('bonplan_config').style.display = "none";
            document.getElementById('fidelity_config').style.display = "none";
            $('#type_blocjs').val("1");
            $("#bloc_1_1").removeClass("col-lg-6");
            $("#bloc_1_2").removeClass("col-lg-6");
            $("#bloc_1_1").removeClass("col-lg-4");
            $("#bloc_1_2").removeClass("col-lg-4");
            $("#bloc_1_3").removeClass("col-lg-4");

        });
        $("#2b1_1").click(function () {
            document.getElementById('bloc_1_1').style.display = "block";
            document.getElementById('bloc_1_2').style.display = "block";
            document.getElementById('bloc_1_3').style.display = "none";
            document.getElementById('bonplan_config').style.display = "none";
            document.getElementById('fidelity_config').style.display = "none";
            $('#type_blocjs').val("2");
            $("#bloc_1_1").addClass("col-lg-6");
            $("#bloc_1_2").addClass("col-lg-6");
            $("#bloc_1_1").removeClass("col-lg-4");
            $("#bloc_1_2").removeClass("col-lg-4");
            $("#bloc_1_3").removeClass("col-lg-4");
        });
        $("#3b1_1").click(function () {

            document.getElementById('bloc_1_1').style.display = "block";
            document.getElementById('bloc_1_2').style.display = "block";
            document.getElementById('bloc_1_3').style.display = "block";
            document.getElementById('bonplan_config').style.display = "none";
            document.getElementById('fidelity_config').style.display = "none";
            $('#type_blocjs').val("3");
            $("#bloc_1_1").removeClass("col-lg-6");
            $("#bloc_1_2").removeClass("col-lg-6");
            $("#bloc_1_1").addClass("col-lg-4");
            $("#bloc_1_2").addClass("col-lg-4");
            $("#bloc_1_3").addClass("col-lg-4");
        });
        $("#1bp1_1").click(function () {
            document.getElementById('bonplan_config').style.display = "block";
            document.getElementById('bloc_1_1').style.display = "none";
            document.getElementById('bloc_1_2').style.display = "none";
            document.getElementById('bloc_1_3').style.display = "none";
            document.getElementById('fidelity_config').style.display = "none";
            $('#type_blocjs').val("1bp");
        });
        $("#1fd1_1").click(function () {
            document.getElementById('fidelity_config').style.display = "block";
            document.getElementById('bonplan_config').style.display = "none";
            document.getElementById('bloc_1_1').style.display = "none";
            document.getElementById('bloc_1_2').style.display = "none";
            document.getElementById('bloc_1_3').style.display = "none";
            $('#type_blocjs').val("1fd");
        });
        $("#link_btn_bloc1").change(function () {

            if (document.getElementById('link_btn_bloc1').value == 1) {
                document.getElementById('link_bloc1').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc1').value == 0) {
                document.getElementById('link_bloc1').style.display = "none";

            }

        });
        $("#link_btn_bloc2").change(function () {


            if (document.getElementById('link_btn_bloc2').value == 1) {
                document.getElementById('link_bloc2').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc2').value == 0) {
                document.getElementById('link_bloc2').style.display = "none";

            }

        });
        $("#link_btn_bloc3").change(function () {


            if (document.getElementById('link_btn_bloc3').value == 1) {
                document.getElementById('link_bloc3').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc3').value == 0) {
                document.getElementById('link_bloc3').style.display = "none";

            }

        });
        $("#link_btn_bp").change(function () {

            if (document.getElementById('link_btn_bp').value == 1) {
                document.getElementById('link_bp').style.display = "block";

            }
            else if (document.getElementById('link_btn_bp').value == 0) {
                document.getElementById('link_bp').style.display = "none";

            }

        });
        $("#link_btn_fd").change(function () {

            if (document.getElementById('link_btn_fd').value == 1) {
                document.getElementById('link_fd').style.display = "block";

            }
            else if (document.getElementById('link_btn_fd').value == 0) {
                document.getElementById('link_fd').style.display = "none";

            }

        });
    });
</script>