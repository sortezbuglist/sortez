<script type="text/javascript">
    jQuery(document).ready(function () {
        <?php if (isset($objGlissiere->is_activ_bloc2) AND $objGlissiere->is_activ_bloc2 == 1) {  ?>
        jQuery('#type_bloc2_choose').css("display", "block");
        <?php if (isset($objGlissiere->type_bloc2) AND $objGlissiere->type_bloc2 == '1'){  ?>
        document.getElementById('bloc_2_1').style.display = "block";
        document.getElementById('bloc_2_2').style.display = "none";
        document.getElementById('bloc_2_3').style.display = "none";
        document.getElementById('bonplan_config2').style.display = "none";
        document.getElementById('fidelity_2_config').style.display = "none";
        $('#type_bloc2js').val("1");
        $("#bloc_2_1").removeClass("col-lg-6");
        $("#bloc_2_2").removeClass("col-lg-6");
        $("#bloc_2_1").removeClass("col-lg-4");
        $("#bloc_2_2").removeClass("col-lg-4");
        $("#bloc_2_3").removeClass("col-lg-4");
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc2) AND $objGlissiere->type_bloc2 == '2') {  ?>
        document.getElementById('bloc_2_1').style.display = "block";
        document.getElementById('bloc_2_2').style.display = "block";
        document.getElementById('bloc_2_3').style.display = "none";
        document.getElementById('bonplan_config2').style.display = "none";
        document.getElementById('fidelity_2_config').style.display = "none";
        $('#type_bloc2js').val("2");
        $("#bloc_2_1").addClass("col-lg-6");
        $("#bloc_2_2").addClass("col-lg-6");
        $("#bloc_2_1").removeClass("col-lg-4");
        $("#bloc_2_2").removeClass("col-lg-4");
        $("#bloc_2_3").removeClass("col-lg-4");
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc2) AND $objGlissiere->type_bloc2 == '3') {  ?>
        document.getElementById('bloc_2_1').style.display = "block";
        document.getElementById('bloc_2_2').style.display = "block";
        document.getElementById('bloc_2_3').style.display = "block";
        document.getElementById('bonplan_config2').style.display = "none";
        document.getElementById('fidelity_2_config').style.display = "none";
        $('#type_bloc2js').val("3");
        $("#bloc_2_1").addClass("col-lg-4");
        $("#bloc_2_2").addClass("col-lg-4");
        $("#bloc_2_3").addClass("col-lg-4");
        $("#bloc_2_1").removeClass("col-lg-6");
        $("#bloc_2_2").removeClass("col-lg-6");
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc2) AND $objGlissiere->type_bloc2 == '1bp') {  ?>
        document.getElementById('bonplan_config2').style.display = "block";
        document.getElementById('bloc_2_1').style.display = "none";
        document.getElementById('bloc_2_2').style.display = "none";
        document.getElementById('bloc_2_3').style.display = "none";
        document.getElementById('fidelity_2_config').style.display = "none";
        $('#type_bloc2js').val("1bp");
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc2) AND $objGlissiere->type_bloc2 == '1fd') {  ?>
        document.getElementById('fidelity_2_config').style.display = "block";
        document.getElementById('bonplan_config2').style.display = "none";
        document.getElementById('bloc_2_1').style.display = "none";
        document.getElementById('bloc_2_2').style.display = "none";
        document.getElementById('bloc_2_3').style.display = "none";
        $('#type_bloc2js').val("1fd");
        <?php } ?>
        <?php if (isset($objGlissiere->is_activ_btn_bloc2) && $objGlissiere->is_activ_btn_bloc2 == '1') {  ?>
        document.getElementById('link_bloc1_2').style.display = "block";
        <?php } ?>
        <?php if (isset($objGlissiere->is_activ_btn_bloc2_2) && $objGlissiere->is_activ_btn_bloc2_2 == '1') {  ?>
        document.getElementById('link_bloc2_2').style.display = "block";
        <?php } ?>
        <?php if (isset($objGlissiere->is_activ_btn_bp_2) && $objGlissiere->is_activ_btn_bp_2 == '1') {  ?>
        document.getElementById('link_bp_2').style.display = "block";
        <?php } ?>
        <?php if (isset($objGlissiere->is_activ_btn_fd_2) && $objGlissiere->is_activ_btn_fd_2 == '1') {  ?>
        document.getElementById('link_fd_2').style.display = "block";
        <?php } ?>
        <?php if (isset($objGlissiere->is_activ_btn_bloc_2_3) && $objGlissiere->is_activ_btn_bloc_2_3 == '1') {  ?>
        document.getElementById('link_bloc2_2_1_3').style.display = "block";
        <?php } ?>
        <?php } ?>
        $("#is_activ_bloc2").change(function () {
            if (document.getElementById('is_activ_bloc2').value == 0) {
                jQuery('#type_bloc2_choose').css("display", "none");

                jQuery('#bloc_2_1').css("display", "none");

                jQuery('#bloc_2_2').css("display", "none");

                jQuery('#bloc_2_3').css("display", "none");

                jQuery('#bonplan_config2').css("display", "none");

                jQuery('#fidelity_2_config').css("display", "none");
            }
            else if (document.getElementById('is_activ_bloc2').value == 1) {
                jQuery('#type_bloc2_choose').css("display", "block");
                jQuery('#bloc_2_1').css("display", "none");

                jQuery('#bloc_2_2').css("display", "none");

                jQuery('#bloc_2_3').css("display", "none");

                jQuery('#bonplan_config2').css("display", "none");

                jQuery('#fidelity_2_config').css("display", "none");
            }
        });
        $("#1b2_1").click(function () {
            document.getElementById('bloc_2_1').style.display = "block";
            document.getElementById('bloc_2_2').style.display = "none";
            document.getElementById('bloc_2_3').style.display = "none";
            document.getElementById('bonplan_config2').style.display = "none";
            document.getElementById('fidelity_2_config').style.display = "none";
            $('#type_bloc2js').val("1");
            $("#bloc_2_1").removeClass("col-lg-6");
            $("#bloc_2_2").removeClass("col-lg-6");
            $("#bloc_2_1").removeClass("col-lg-4");
            $("#bloc_2_2").removeClass("col-lg-4");
            $("#bloc_2_3").removeClass("col-lg-4");
        });
        $("#2b2_1").click(function () {
            document.getElementById('bloc_2_1').style.display = "block";
            document.getElementById('bloc_2_2').style.display = "block";
            document.getElementById('bloc_2_3').style.display = "none";
            document.getElementById('bonplan_config2').style.display = "none";
            document.getElementById('fidelity_2_config').style.display = "none";
            $('#type_bloc2js').val("2");
            $("#bloc_2_1").addClass("col-lg-6");
            $("#bloc_2_2").addClass("col-lg-6");
            $("#bloc_2_1").removeClass("col-lg-4");
            $("#bloc_2_2").removeClass("col-lg-4");
            $("#bloc_2_3").removeClass("col-lg-4");
        });
        $("#3b2_1").click(function () {
            document.getElementById('bloc_2_1').style.display = "block";
            document.getElementById('bloc_2_2').style.display = "block";
            document.getElementById('bloc_2_3').style.display = "block";
            document.getElementById('bonplan_config2').style.display = "none";
            document.getElementById('fidelity_2_config').style.display = "none";
            $('#type_bloc2js').val("3");
            $("#bloc_2_1").addClass("col-lg-4");
            $("#bloc_2_2").addClass("col-lg-4");
            $("#bloc_2_3").addClass("col-lg-4");
            $("#bloc_2_1").removeClass("col-lg-6");
            $("#bloc_2_2").removeClass("col-lg-6");
        });
        $("#1bp2_1").click(function () {
            document.getElementById('bonplan_config2').style.display = "block";
            document.getElementById('bloc_2_1').style.display = "none";
            document.getElementById('bloc_2_2').style.display = "none";
            document.getElementById('bloc_2_3').style.display = "none";
            document.getElementById('fidelity_2_config').style.display = "none";
            $('#type_bloc2js').val("1bp");
        });
        $("#1fd2_1").click(function () {
            document.getElementById('fidelity_2_config').style.display = "block";
            document.getElementById('bonplan_config2').style.display = "none";
            document.getElementById('bloc_2_1').style.display = "none";
            document.getElementById('bloc_2_2').style.display = "none";
            document.getElementById('bloc_2_3').style.display = "none";
            $('#type_bloc2js').val("1fd");
        });
        $("#link_btn_bloc1_2").change(function () {

            if (document.getElementById('link_btn_bloc1_2').value == 1) {
                document.getElementById('link_bloc1_2').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc1_2').value == 0) {
                document.getElementById('link_bloc1_2').style.display = "none";

            }

        });
        $("#link_btn_bloc_2_2").change(function () {


            if (document.getElementById('link_btn_bloc_2_2').value == 1) {
                document.getElementById('link_bloc2_2').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc_2_2').value == 0) {
                document.getElementById('link_bloc2_2').style.display = "none";

            }

        });
        $("#link_btn_bloc_2_1_3").change(function () {


            if (document.getElementById('link_btn_bloc_2_1_3').value == 1) {
                document.getElementById('link_bloc2_2_1_3').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc_2_1_3').value == 0) {
                document.getElementById('link_bloc2_2_1_3').style.display = "none";

            }

        });
        $("#link_btn_bp_2").change(function () {

            if (document.getElementById('link_btn_bp_2').value == 1) {
                document.getElementById('link_bp_2').style.display = "block";

            }
            else if (document.getElementById('link_btn_bp_2').value == 0) {
                document.getElementById('link_bp_2').style.display = "none";

            }

        });
        $("#link_btn_fd_2").change(function () {

            if (document.getElementById('link_btn_fd_2').value == 1) {
                document.getElementById('link_fd_2').style.display = "block";

            }
            else if (document.getElementById('link_btn_fd_2').value == 0) {
                document.getElementById('link_fd_2').style.display = "none";

            }

        });
    });
</script>