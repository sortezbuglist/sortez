<script type="text/javascript">
    jQuery(document).ready(function () {
        <?php if (isset($objbloc_info->is_activ_bloc2_page1) AND $objbloc_info->is_activ_bloc2_page1 == 1) {  ?>
                jQuery('#type_bloc2_page1_choose').css("display", "block"); 
                <?php if (isset($objbloc_info->type_bloc2_page1) AND $objbloc_info->type_bloc2_page1 == '1' OR isset($objbloc_info->type_bloc2_page1) AND $objbloc_info->type_bloc2_page1 =='2'){  ?>
                document.getElementById('bloc2_1page1').style.display = "block";
                document.getElementById('bloc_2_page1').style.display = "none";
                document.getElementById('bonplan_config_2_page1').style.display = "none";
                document.getElementById('fidelity_2_config_page1').style.display = "none";
                $('#type_bloc2_page1js').val("1");
                $("#bloc2_1page1").removeClass("col-lg-6");
                $("#bloc_2_page1").removeClass("col-lg-6");
            <?php } ?>
            <?php if (isset($objbloc_info->type_bloc2_page1) AND $objbloc_info->type_bloc2_page1 == '2') {  ?>
                 document.getElementById('bloc2_1page1').style.display = "block";
                document.getElementById('bloc_2_page1').style.display = "block";
                document.getElementById('bonplan_config_2_page1').style.display = "none";
                document.getElementById('fidelity_2_config_page1').style.display = "none";
                $('#type_bloc2_page1js').val("2");
                $("#bloc2_1page1").addClass("col-lg-6");
                $("#bloc_2_page1").addClass("col-lg-6");
            <?php } ?>
            <?php if (isset($objbloc_info->type_bloc2_page1) AND $objbloc_info->type_bloc2_page1 == '1bp') {  ?>
               document.getElementById('bonplan_config_2_page1').style.display = "block";
                document.getElementById('bloc2_1page1').style.display = "none";
                document.getElementById('bloc_2_page1').style.display = "none";
                document.getElementById('fidelity_2_config_page1').style.display = "none";
                $('#type_bloc2_page1js').val("1bp");
            <?php } ?>
            <?php if (isset($objbloc_info->type_bloc2_page1) AND $objbloc_info->type_bloc2_page1 == '1fd') {  ?>
               document.getElementById('fidelity_2_config_page1').style.display = "block";
                document.getElementById('bonplan_config_2_page1').style.display = "none";
                document.getElementById('bloc2_1page1').style.display = "none";
                document.getElementById('bloc_2_page1').style.display = "none";
                $('#type_bloc2_page1js').val("1fd");
            <?php } ?> 
            <?php if (isset($objbloc_info->is_activ_btn_bloc2_page1) && $objbloc_info->is_activ_btn_bloc2_page1 == '1') {  ?>
                document.getElementById('link_bloc1_2_page1').style.display = "block";
            <?php } ?>
            <?php if (isset($objbloc_info->is_activ_btn_bloc2_2_page1) && $objbloc_info->is_activ_btn_bloc2_2_page1 == '1') {  ?>
                document.getElementById('link_bloc_2_page1').style.display = "block";
            <?php } ?>  
            <?php if (isset($objbloc_info->is_activ_btn_bp_2_page1) && $objbloc_info->is_activ_btn_bp_2_page1 == '1') {  ?>
                document.getElementById('link_bp_2_page1').style.display = "block";
            <?php } ?>
            <?php if (isset($objbloc_info->is_activ_btn_fd_2_page1) && $objbloc_info->is_activ_btn_fd_2_page1 == '1') {  ?>
                document.getElementById('link_fd_2_page1').style.display = "block";
            <?php } ?>  
        <?php } ?>
    	$("#is_activ_bloc2_page1").change(function () {
            if (document.getElementById('is_activ_bloc2_page1').value == 0) {
                jQuery('#type_bloc2_page1_choose').css("display", "none");
               
                jQuery('#bloc2_1page1').css("display", "none");
                
                jQuery('#bloc_2_page1').css("display", "none");
            }
            else if (document.getElementById('is_activ_bloc2_page1').value == 1) {
                jQuery('#type_bloc2_page1_choose').css("display", "block");
                jQuery('#bloc2_1page1').css("display", "none");
                
                jQuery('#bloc_2_page1').css("display", "none");
            }
        });
      $("#1b2_1_page1").click(function () {
            document.getElementById('bloc2_1page1').style.display = "block";
            document.getElementById('bloc_2_page1').style.display = "none";
            document.getElementById('bonplan_config_2_page1').style.display = "none";
            document.getElementById('fidelity_2_config_page1').style.display = "none";
            $('#type_bloc2_page1js').val("1");
            $("#bloc2_1page1").removeClass("col-lg-6");
            $("#bloc_2_page1").removeClass("col-lg-6");
        });
        $("#2b2_1_page1").click(function () {
            document.getElementById('bloc2_1page1').style.display = "block";
            document.getElementById('bloc_2_page1').style.display = "block";
            document.getElementById('bonplan_config_2_page1').style.display = "none";
            document.getElementById('fidelity_2_config_page1').style.display = "none";
            $('#type_bloc2_page1js').val("2");
            $("#bloc2_1page1").addClass("col-lg-6");
            $("#bloc_2_page1").addClass("col-lg-6");
        });
        $("#1bp2_1_page1").click(function () {
            document.getElementById('bonplan_config_2_page1').style.display = "block";
            document.getElementById('bloc2_1page1').style.display = "none";
            document.getElementById('bloc_2_page1').style.display = "none";
            document.getElementById('fidelity_2_config_page1').style.display = "none";
            $('#type_bloc2_page1js').val("1bp");
        });
        $("#1fd2_1_page1").click(function () {
            document.getElementById('fidelity_2_config_page1').style.display = "block";
            document.getElementById('bonplan_config_2_page1').style.display = "none";
            document.getElementById('bloc2_1page1').style.display = "none";
            document.getElementById('bloc_2_page1').style.display = "none";
            $('#type_bloc2_page1js').val("1fd");
        });
        $("#link_btn_bloc1_2_page1").change(function () {
            
            if (document.getElementById('link_btn_bloc1_2_page1').value == 1) {
                document.getElementById('link_bloc1_2_page1').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc1_2_page1').value == 0) {
                document.getElementById('link_bloc1_2_page1').style.display = "none";

            }

        }); 
        $("#link_btn_bloc_2_page1").change(function () {


            if (document.getElementById('link_btn_bloc_2_page1').value == 1) {
                document.getElementById('link_bloc_2_page1').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc_2_page1').value == 0) {
                document.getElementById('link_bloc_2_page1').style.display = "none";

            }

        }); 
    });
</script>