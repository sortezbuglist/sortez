<script type="text/javascript">
jQuery(document).ready(function () {
    	$("#is_activ_bloc").change(function () {
            if (document.getElementById('is_activ_bloc').value == 0) {
                jQuery('#type_bloc1_choose').css("display", "none");
               
                jQuery('#bloc1').css("display", "none");
                
                jQuery('#bloc2').css("display", "none");
                
               
            }
            else if (document.getElementById('is_activ_bloc').value == 1) {
                jQuery('#type_bloc1_choose').css("display", "block");

                jQuery('#bloc1').css("display", "none");
                
                jQuery('#bloc2').css("display", "none");
               
            }
        });
      $("#1b1").click(function () {
            document.getElementById('bloc1').style.display = "block";
            document.getElementById('bloc2').style.display = "none";
            $('#type_bloc_page1js').val("1");
            $("#bloc1").removeClass("col-lg-6");
            $("#bloc2").removeClass("col-lg-6");
        });
        $("#2b1").click(function () {
            document.getElementById('bloc1').style.display = "block";
            document.getElementById('bloc2').style.display = "block";
            $('#type_bloc_page1js').val("2");
            $("#bloc1").addClass("col-lg-6");
            $("#bloc2").addClass("col-lg-6");
        });
        $("#link_btn_bloc1_page1").change(function () {

            if (document.getElementById('link_btn_bloc1_page1').value == 1) {
                document.getElementById('link_bloc1').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc1_page1').value == 0) {
                document.getElementById('link_bloc1').style.display = "none";

            }

        }); 
        $("#link_btn_bloc2_page1").change(function () {


            if (document.getElementById('link_btn_bloc2_page1').value == 1) {
                document.getElementById('link_bloc2').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc2_page1').value == 0) {
                document.getElementById('link_bloc2').style.display = "none";

            }

        }); 
});
</script>