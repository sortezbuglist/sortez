<?php
$data['objGlissiere']= $objGlissiere;
//var_dump($objGlissiere); die();
$this->load->view('privicarte/vwfiche_pro/jsbloc1_home',$data); ?>
<div class="mt-4" id="blocss" style="<?php if ($user_groups->id == '5' AND $user_groups->id == '4'){echo "display:none";} ?>">

    <div class="col-lg-12 text-center pb-3"
         style="background-color: #008000;height: 40px;padding-top: 12px;color: white">AJOUTER DES
        BLOCS
        IMAGES ET TEXT COMPL&Eacute;MENTAIRES
        <span style="float: right">
                                    <label for="is_activ_bloc" style="color:#FFFFFF !important;">Activé:</label>

                <select name="Glissiere[is_activ_bloc]" id="is_activ_bloc" style="color:#000000;">
                    <option value="0"
                            <?php if (isset($objGlissiere->is_activ_bloc) AND $objGlissiere->is_activ_bloc == 0){ ?>selected="selected"<?php } ?>>non</option>
                    <option value="1"
                            <?php if (isset($objGlissiere->is_activ_bloc) AND $objGlissiere->is_activ_bloc == 1){ ?>selected="selected"<?php } ?>>oui</option>
                </select>
                                </span>
    </div>
    <?php
    $thiss = get_instance();
    $thiss->load->model('commercant');
    $objbonplan = $thiss->commercant->get_bonplan_commercant_by_id($objCommercant->IdCommercant);

    $thiss->load->model('commercant');
    $objfidelity = $thiss->commercant->get_fidelity_commercant_by_id($objCommercant->IdCommercant);
    //var_dump(count($objbonplan));die();
    ?>
    <div class="col-lg-12 text-center grey_bloc_independent pt-3 pb-3" id="type_bloc_choose">
        <input type="hidden" name="Glissiere[type_bloc]" value="<?php if (isset($objGlissiere->type_bloc)){echo $objGlissiere->type_bloc;} ?>" id="type_blocjs">
        <div class="row">
            <div style="width: 20%;" class="btn_vert" id="1b1_1"><img style="width: 100%;" src="<?php echo base_url('assets/img/1.png')?>"></div>
            <div style="width: 20%;" class="btn_vert" id="2b1_1"><img style="width: 100%;" src="<?php echo base_url('assets/img/2.png')?>"></div>
            <div style="width: 20%;" class="btn_vert" id="3b1_1"><img style="width: 100%;" src="<?php echo base_url('assets/img/5.png')?>"></div>
            <div style="width: 20%;" class="btn_vert" id="1bp1_1"><img style="width: 100%;" src="<?php echo base_url('assets/img/3.png')?>"></div>
            <div style="width: 20%;" class="btn_vert" id="1fd1_1"><img style="width: 100%;" src="<?php echo base_url('assets/img/4.png')?>"></div>
        </div>

    </div>

    <div class="container-fluid">
        <div class="row">
            <div id="bloc_1_1" class="container-fluid grey_bloc_independent pb-3" style="<?php
            if (isset($objGlissiere->is_activ_bloc) AND $objGlissiere->is_activ_bloc == 1 AND
                ((isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc == '1') || (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc == '2'))
            ) {
                ?>display: block;<?php } else { ?>display: none;<?php } ?>">
                <div class="col-lg-12 pt-3 pb-0 pl-0 pr-0">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-lg-12 text-center font-weight-bold pb-3">BLOC 1</div>
                            <div id="Articlebloc_1_image1_content_container" class="text-center row">
                                <div class="col-lg-12 text-center pb-3">
                                    <a href='javascript:void(0);' title="bloc_1_image1_content"
                                        <?php if (empty($objGlissiere->bloc_1_image1_content) || $objGlissiere->bloc_1_image1_content == "") { ?>
                                            onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-gli-bloc_1_image1_content"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                        <?php } ?>
                                       class="btn btn-info mt-2" id="bloc_1_image1_content_link">Ajouter une
                                        Photo du Bloc</a>

                                    <a href="javascript:void(0);" class="btn btn-danger mt-2"
                                        <?php if (!empty($objGlissiere->bloc_1_image1_content) AND $objGlissiere->bloc_1_image1_content != "") { ?>
                                            onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','bloc_1_image1_content');"
                                        <?php } ?>
                                    >Supprimer</a>
                                </div>
                                <div class="col-lg-12 pb-3">
                                    <?php
                                    if (is_object($objGlissiere) AND file_exists($path_img_gallery . $objGlissiere->bloc_1_image1_content) AND $objGlissiere->bloc_1_image1_content != "") {
                                        echo '<img class="img-fluid"  src="' . base_url() . $path_img_gallery . $objGlissiere->bloc_1_image1_content . '"/>';
                                    } else if (is_object($objGlissiere) AND file_exists($path_img_gallery_old . $objGlissiere->bloc_1_image1_content) AND $objGlissiere->bloc_1_image1_content != "") {
                                        echo '<img class="img-fluid"  src="' . base_url() . $path_img_gallery_old . $objGlissiere->bloc_1_image1_content . '"/>';
                                    }
                                    ?>
                                    <input type="hidden" name="bloc_1_image1_content"
                                           id="bloc_1_image1_content"
                                           value="<?php if (isset($objGlissiere->bloc_1_image1_content)) echo htmlspecialchars_decode($objGlissiere->bloc_1_image1_content); ?>"/>
                                    <div id="bloc_1_image1_content_error"
                                         class="div_error_taille_3_4">
                                        <?php if (isset($img_error_verify_array) AND in_array("activite1_image1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <textarea name="Glissiere[bloc1_content]" id="bloc1_content">
                                <?php if (isset($objGlissiere->bloc1_content)) echo htmlspecialchars_decode($objGlissiere->bloc1_content); ?>
                            </textarea>
                            <script>
                                CKEDITOR.replace('bloc1_content');
                            </script>
                        </div>
                    </div>
                </div>


                <div class="container-fluid p-0">


                    <div class="col-lg-12" id="btn_bloc1"
                         style="background-color: #3EB4D7;height: 40px;padding-top: 12px;color: black">
                        <span style="color: white!important;">Ajouter un Bouton</span> <span
                            style="float: right;">
                                                                            <select name="Glissiere[is_activ_btn_bloc1]"
                                                                                    style="margin-right: 10px;color: black!important;"
                                                                                    id="link_btn_bloc1">
                                                                                <option value="0" <?php if (isset($objGlissiere->is_activ_btn_bloc1) AND $objGlissiere->is_activ_btn_bloc1 == '0') {
                                                                                    echo 'selected';
                                                                                } ?>>non</option>
                                                                                <option value="1" <?php if (isset($objGlissiere->is_activ_btn_bloc1) AND $objGlissiere->is_activ_btn_bloc1 == '1') {
                                                                                    echo 'selected';
                                                                                } ?>>oui</option>
                                                                            </select></span>
                    </div>

                    <div class="text-center" id="link_bloc1" style="<?php if (isset($objGlissiere->is_activ_btn_bloc1) AND $objGlissiere->is_activ_btn_bloc1 =='0' ){echo 'display:none';}elseif (isset($objGlissiere->is_activ_btn_bloc1) AND $objGlissiere->is_activ_btn_bloc1 =='1'){echo 'display:block';}else{echo 'display:none';} ?>">
                        <div class="row pt-3">
                            <div class="col-lg-3 text-center pt-2" >Texte du bouton</div>
                            <div class="col-lg-9">
                                <input class="form-control" id="btn_bloc1_content1" type="text"
                                       value="<?php if (isset($objGlissiere->btn_bloc1_content1) AND $objGlissiere->btn_bloc1_content1 != "") {
                                           echo $objGlissiere->btn_bloc1_content1;
                                       } ?>" name="Glissiere[btn_bloc1_content1]">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3  text-center  pt-2">Lien du bouton</div>
                            <div class="col-lg-9">
                                <select
                                    name="Glissiere[bloc1_existed_link1]" class="form-control"
                                    style="margin-right: 10px;color: black" id="valuelink_bloc1">
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link1) AND $objGlissiere->bloc1_existed_link1 == '0') {
                                        echo 'selected';
                                    } ?> value="0">Choisir un lien local1</option><?php if ($user_groups->id == '5' AND $user_groups->id == '4') {?>
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link1) AND $objGlissiere->bloc1_existed_link1 == 'p1') {
                                        echo 'selected';
                                    } ?> value="p1">Vers la page 1</option><?php } ?><?php if ($user_groups->id == '5' AND $user_groups->id == '4') {?>
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link1) AND $objGlissiere->bloc1_existed_link1 == 'p2') {
                                        echo 'selected';
                                    } ?> value="p2">Vers la page 2</option><?php } ?>
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link1) AND $objGlissiere->bloc1_existed_link1 == 'ag') {
                                        echo 'selected';
                                    } ?> value="ag">Mes Agendas</option>
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link1) AND $objGlissiere->bloc1_existed_link1 == 'art') {
                                        echo 'selected';
                                    } ?> value="art">Mes Articles</option>
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link1) AND $objGlissiere->bloc1_existed_link1 == 'bp') {
                                        echo 'selected';
                                    } ?> value="bp">Mes Bons Plans</option>
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link1) AND $objGlissiere->bloc1_existed_link1 == 'bt') {
                                        echo 'selected';
                                    } ?> value="bt">Mes Boutiques</option>
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link1) AND $objGlissiere->bloc1_existed_link1 == 'fd') {
                                        echo 'selected';
                                    } ?> value="fd">Ma Fidelisation</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3  text-center  pt-2">Ou ajouter un lien externe</div>
                            <div class="col-lg-3  pl-3 " style="">
                                <select class="text-center form-control" id="httbloc1">
                                    <option <?php if (is_object($objGlissiere) AND preg_match('/http:/', $objGlissiere->bloc1_custom_link1)) {
                                        echo 'selected';
                                    } ?> value="http://">http://
                                    </option>
                                    <option <?php if (is_object($objGlissiere) AND preg_match('/http:/', $objGlissiere->bloc1_custom_link1)) {
                                        echo 'selected';
                                    } ?> value="https://">https://
                                    </option>
                                </select>
                            </div>
                            <div class="col-lg-6" style="">
                                <input onchange="sub_link_custom_bloc1()"
                                       name="Glissiere[bloc1_custom_link1]"
                                       class="form-control text-center" type="text" value="<?php echo $objGlissiere->bloc1_custom_link1 ?? '';?>"
                                       id="link_htt_bloc1"
                                       placeholder="votre liens ici">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">Afficher le lien en lightbox</div>
                            <div class="col-lg-9">
                                <select class="form-control" name="Glissiere[bloc1_islightbox]">
                                    <option value="0">Non</option>
                                    <option <?php if (isset($objGlissiere->bloc1_islightbox) AND $objGlissiere->bloc1_islightbox =='1'){echo 'selected';} ?> value="1">Oui</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="bloc_1_2" class="container-fluid grey_bloc_independent pb-3" style="<?php
            if (isset($objGlissiere->is_activ_bloc) AND $objGlissiere->is_activ_bloc == 1 AND
                (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc == '2')
            ) {
                ?>display: block;<?php } else { ?>display: none;<?php } ?>">
                <div class="col-lg-12 p-0">
                    <div class="row">
                        <div class="col-lg-12 text-center font-weight-bold pb-3">BLOC 2</div>
                        <div class="col-lg-12 text-center pb-0">
                            <div id="Articlebloc_1_image2_content_container" class="text-center row pt-3 pb-3">
                                <div class="col-lg-12 text-center pb-3">
                                    <a href='javascript:void(0);' title="bloc_1_image2_content"
                                        <?php if (empty($objGlissiere->bloc_1_image2_content) || $objGlissiere->bloc_1_image2_content == "") { ?>
                                            onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-gli-bloc_1_image2_content"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                        <?php } ?>
                                       class="btn btn-info mt-2" id="bloc_1_image2_content_link">Ajouter une
                                        Photo2 du Bloc</a>

                                    <a href="javascript:void(0);" class="btn btn-danger mt-2"
                                        <?php if (!empty($objGlissiere->bloc_1_image2_content) AND $objGlissiere->bloc_1_image2_content != "") { ?>
                                            onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','bloc_1_image2_content');"
                                        <?php } ?>
                                    >Supprimer</a>
                                </div>
                                <div class="col-lg-12">
                                    <?php
                                    if (is_object($objGlissiere) AND file_exists($path_img_gallery . $objGlissiere->bloc_1_image2_content) AND $objGlissiere->bloc_1_image2_content != "") {
                                        echo '<img style="max-width:100%" src="' . base_url() . $path_img_gallery . $objGlissiere->bloc_1_image2_content . '" "/>';
                                    } else if (is_object($objGlissiere) AND file_exists($path_img_gallery_old . $objGlissiere->bloc_1_image2_content) AND $objGlissiere->bloc_1_image2_content != "") {
                                        echo '<img style="max-width:100%" src="' . base_url() . $path_img_gallery_old . $objGlissiere->bloc_1_image2_content . '" "/>';
                                    }
                                    ?>
                                    <input type="hidden" name="bloc_1_image2_content"
                                           id="bloc_1_image2_content"
                                           value="<?php if (isset($objGlissiere->bloc_1_image2_content)) echo htmlspecialchars_decode($objGlissiere->bloc_1_image2_content); ?>"/>
                                    <div id="bloc_1_image2_content_error"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) AND in_array("activite1_image1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                                                        <textarea name="Glissiere[bloc1_content2]"
                                                                  id="bloc1_content2"><?php if (isset($objGlissiere->bloc1_content2)) echo htmlspecialchars_decode($objGlissiere->bloc1_content2); ?></textarea>
                            <script>
                                CKEDITOR.replace('bloc1_content2');
                            </script>
                        </div>
                    </div>
                </div>

                <div class="container-fluid p-0">
                    <div class="col-lg-12" id="btn_bloc2"
                         style="background-color: #3EB4D7;height: 40px;padding-top: 12px;color: black">
                        <span style="color: white!important;">Ajouter un Bouton</span> <span
                            style="float: right;">
                                                                            <select name="Glissiere[is_activ_btn_bloc2]"
                                                                                    style="margin-right: 10px;color: black!important;"
                                                                                    id="link_btn_bloc2">
                                                                                <option value="0" <?php if (isset($objGlissiere->is_activ_btn_bloc2) AND $objGlissiere->is_activ_btn_bloc2 == 0) {
                                                                                    echo 'selected';
                                                                                } ?>>non</option>
                                                                                <option value="1" <?php if (isset($objGlissiere->is_activ_btn_bloc2) AND $objGlissiere->is_activ_btn_bloc2 == 1) {
                                                                                    echo 'selected';
                                                                                } ?>>oui</option>

                                                                            </select></span>
                    </div>

                    <div class="text-center" id="link_bloc2" style="<?php if (isset($objGlissiere->is_activ_btn_bloc2) AND $objGlissiere->is_activ_btn_bloc2 =='1'){echo 'display:block';}else{echo 'display:none';} ?>">

                        <div class="row pt-3">
                            <div class="col-lg-3">
                                <label for="btn_bloc1_content2">Texte du bouton</label>
                            </div>
                            <div class="col-lg-9">
                                <input class="form-control" id="btn_bloc1_content2" type="text"
                                       value="<?php if (isset($objGlissiere->btn_bloc1_content2) AND $objGlissiere->btn_bloc1_content2 != "") {
                                           echo $objGlissiere->btn_bloc1_content2;
                                       } ?>" name="Glissiere[btn_bloc1_content2]">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <label for="valuelink_bloc1">Lien du bouton</label>
                            </div>
                            <div class="col-lg-9">
                                <select
                                    name="Glissiere[bloc1_existed_link2]" class="form-control"
                                    style="margin-right: 10px;color: black" id="valuelink_bloc1">
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link2) AND $objGlissiere->bloc1_existed_link2 == '0') {
                                        echo 'selected';
                                    } ?> value="0">Choisir un lien local1</option><?php if ($user_groups->id == '5' AND $user_groups->id == '4') {?>
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link2) AND $objGlissiere->bloc1_existed_link2 == 'p1') {
                                        echo 'selected';
                                    } ?> value="p1">Vers la page 1</option><?php } ?><?php if ($user_groups->id == '5' AND $user_groups->id == '4') {?>
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link2) AND $objGlissiere->bloc1_existed_link2 == 'p2') {
                                        echo 'selected';
                                    } ?> value="p2">Vers la page 2</option><?php } ?>
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link2) AND $objGlissiere->bloc1_existed_link2 == 'ag') {
                                        echo 'selected';
                                    } ?> value="ag">Mes Agendas</option>
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link2) AND $objGlissiere->bloc1_existed_link2 == 'art') {
                                        echo 'selected';
                                    } ?> value="art">Mes Articles</option>
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link2) AND $objGlissiere->bloc1_existed_link2 == 'bp') {
                                        echo 'selected';
                                    } ?> value="bp">Mes Bons Plans</option>
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link2) AND $objGlissiere->bloc1_existed_link2 == 'bt') {
                                        echo 'selected';
                                    } ?> value="bt">Mes Boutiques</option>
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link2) AND $objGlissiere->bloc1_existed_link2 == 'fd') {
                                        echo 'selected';
                                    } ?> value="fd">Ma Fidelisation</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                Ou ajouter un lien externe
                            </div>
                            <div class="col-lg-3">
                                <select class="text-center form-control" id="httbloc2">
                                    <option <?php if (is_object($objGlissiere) AND preg_match('/http:/', $objGlissiere->bloc1_custom_link2)) {
                                        echo 'selected';
                                    } ?> value="http://">http://
                                    </option>
                                    <option <?php if (is_object($objGlissiere) AND preg_match('/http:/', $objGlissiere->bloc1_custom_link2)) {
                                        echo 'selected';
                                    } ?> value="https://">https://
                                    </option>
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <input onchange="sub_link_custom_bloc1()"
                                       name="Glissiere[bloc1_custom_link2]"
                                       class="form-control text-center" type="text" value="<?php echo $objGlissiere->bloc1_custom_link2 ?? '';?>"
                                       id="link_htt_bloc2"
                                       placeholder="votre liens ici">
                            </div>
                        </div>
                        <div class="row pt-2">
                            <div class="col-lg-3">Afficher le lien en lightbox</div>
                            <div class="col-lg-9">
                                <select class="form-control" name="Glissiere[bloc1_islightbox2]">
                                    <option value="0">Non</option>
                                    <option <?php if (isset($objGlissiere->bloc1_islightbox2) AND $objGlissiere->bloc1_islightbox2 =='1'){echo 'selected';} ?> value="1">Oui</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--- bloc 3 home --->
            <div id="bloc_1_3" class="container-fluid grey_bloc_independent pb-3" style="<?php
            if (isset($objGlissiere->is_activ_bloc) AND $objGlissiere->is_activ_bloc == 1 AND
                (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc == '3')
            ) {
                ?>display: block;<?php } else { ?>display: none;<?php } ?>">
                <div class="col-lg-12 p-0">
                    <div class="row">
                        <div class="col-lg-12 text-center font-weight-bold pb-3">BLOC 3</div>
                        <div class="col-lg-12 text-center pb-0">
                            <div id="Articlebloc_1_image3_content_container" class="text-center row pt-3 pb-3">
                                <div class="col-lg-12 text-center pb-3">
                                    <a href='javascript:void(0);' title="bloc_1_image3_content"
                                        <?php if (empty($objGlissiere->bloc_1_image3_content) || $objGlissiere->bloc_1_image3_content == "") { ?>
                                            onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-gli-bloc_1_image3_content"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                        <?php } ?>
                                       class="btn btn-info mt-2" id="bloc_1_image3_content_link">Ajouter une
                                        Photo3 du Bloc</a>

                                    <a href="javascript:void(0);" class="btn btn-danger mt-2"
                                        <?php if (!empty($objGlissiere->bloc_1_image3_content) AND $objGlissiere->bloc_1_image3_content != "") { ?>
                                            onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','bloc_1_image3_content');"
                                        <?php } ?>
                                    >Supprimer</a>
                                </div>
                                <div class="col-lg-12">
                                    <?php
                                    if (isset($objGlissiere->bloc_1_image3_content) AND file_exists($path_img_gallery . $objGlissiere->bloc_1_image3_content) AND $objGlissiere->bloc_1_image3_content != "") {
                                        echo '<img style="max-width:100%" src="' . base_url() . $path_img_gallery . $objGlissiere->bloc_1_image3_content . '" "/>';
                                    } else if (isset($objGlissiere->bloc_1_image3_content) AND file_exists($path_img_gallery_old . $objGlissiere->bloc_1_image3_content) AND $objGlissiere->bloc_1_image3_content != "") {
                                        echo '<img style="max-width:100%" src="' . base_url() . $path_img_gallery_old . $objGlissiere->bloc_1_image3_content . '" "/>';
                                    }
                                    ?>
                                    <input type="hidden" name="bloc_1_image3_content"
                                           id="bloc_1_image3_content"
                                           value="<?php if (isset($objGlissiere->bloc_1_image3_content)) echo htmlspecialchars_decode($objGlissiere->bloc_1_image3_content); ?>"/>
                                    <div id="bloc_1_image3_content_error"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) AND in_array("activite1_image1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                                                        <textarea name="Glissiere[bloc1_content3]"
                                                                  id="bloc1_content3"><?php if (isset($objGlissiere->bloc1_content3)) echo htmlspecialchars_decode($objGlissiere->bloc1_content3); ?></textarea>
                            <script>
                                CKEDITOR.replace('bloc1_content3');
                            </script>
                        </div>
                    </div>
                </div>

                <div class="container-fluid p-0">
                    <div class="col-lg-12" id="btn_bloc3"
                         style="background-color: #3EB4D7;height: 40px;padding-top: 12px;color: black">
                        <span style="color: white!important;">Ajouter un Bouton</span> <span
                                style="float: right;">
                                                                            <select name="Glissiere[is_activ_btn_bloc3]"
                                                                                    style="margin-right: 10px;color: black!important;"
                                                                                    id="link_btn_bloc3">
                                                                                <option value="0" <?php if (isset($objGlissiere->is_activ_btn_bloc3) AND $objGlissiere->is_activ_btn_bloc3 == 0) {
                                                                                    echo 'selected';
                                                                                } ?>>non</option>
                                                                                <option value="1" <?php if (isset($objGlissiere->is_activ_btn_bloc3) AND $objGlissiere->is_activ_btn_bloc3 == 1) {
                                                                                    echo 'selected';
                                                                                } ?>>oui</option>

                                                                            </select></span>
                    </div>

                    <div class="text-center" id="link_bloc3" style="<?php if (isset($objGlissiere->is_activ_btn_bloc3) AND $objGlissiere->is_activ_btn_bloc3 =='1'){echo 'display:block';}else{echo 'display:none';} ?>">

                        <div class="row pt-3">
                            <div class="col-lg-3">
                                <label for="btn_bloc1_content3">Texte du bouton</label>
                            </div>
                            <div class="col-lg-9">
                                <input class="form-control" id="btn_bloc1_content3" type="text"
                                       value="<?php if (isset($objGlissiere->btn_bloc1_content3) AND $objGlissiere->btn_bloc1_content3 != "") {
                                           echo $objGlissiere->btn_bloc1_content3;
                                       } ?>" name="Glissiere[btn_bloc1_content3]">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <label for="valuelink_bloc1">Lien du bouton</label>
                            </div>
                            <div class="col-lg-9">
                                <select
                                        name="Glissiere[bloc1_existed_link3]" class="form-control"
                                        style="margin-right: 10px;color: black" id="valuelink_bloc1">
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link3) AND $objGlissiere->bloc1_existed_link3 == '0') {
                                        echo 'selected';
                                    } ?> value="0">Choisir un lien local1</option><?php if ($user_groups->id == '5' AND $user_groups->id == '4') {?>
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link3) AND $objGlissiere->bloc1_existed_link3 == 'p1') {
                                        echo 'selected';
                                    } ?> value="p1">Vers la page 1</option><?php } ?><?php if ($user_groups->id == '5' AND $user_groups->id == '4') {?>
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link3) AND $objGlissiere->bloc1_existed_link3 == 'p2') {
                                        echo 'selected';
                                    } ?> value="p2">Vers la page 2</option><?php } ?>
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link3) AND $objGlissiere->bloc1_existed_link3 == 'ag') {
                                        echo 'selected';
                                    } ?> value="ag">Mes Agendas</option>
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link3) AND $objGlissiere->bloc1_existed_link3 == 'art') {
                                        echo 'selected';
                                    } ?> value="art">Mes Articles</option>
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link3) AND $objGlissiere->bloc1_existed_link3 == 'bp') {
                                        echo 'selected';
                                    } ?> value="bp">Mes Bons Plans</option>
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link3) AND $objGlissiere->bloc1_existed_link3 == 'bt') {
                                        echo 'selected';
                                    } ?> value="bt">Mes Boutiques</option>
                                    <option <?php if (isset($objGlissiere->bloc1_existed_link3) AND $objGlissiere->bloc1_existed_link3 == 'fd') {
                                        echo 'selected';
                                    } ?> value="fd">Ma Fidelisation</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                Ou ajouter un lien externe
                            </div>
                            <div class="col-lg-3">
                                <select class="text-center form-control" id="httbloc2">
                                    <option <?php if (is_object($objGlissiere) AND preg_match('/http:/', $objGlissiere->bloc1_custom_link3)) {
                                        echo 'selected';
                                    } ?> value="http://">http://
                                    </option>
                                    <option <?php if (is_object($objGlissiere) AND preg_match('/http:/', $objGlissiere->bloc1_custom_link3)) {
                                        echo 'selected';
                                    } ?> value="https://">https://
                                    </option>
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <input onchange="sub_link_custom_bloc1()"
                                       name="Glissiere[bloc1_custom_link3]"
                                       class="form-control text-center" type="text" value="<?php echo $objGlissiere->bloc1_custom_link3 ?? '';?>"
                                       id="link_htt_bloc2"
                                       placeholder="votre liens ici">
                            </div>
                        </div>
                        <div class="row pt-2">
                            <div class="col-lg-3">Afficher le lien en lightbox</div>
                            <div class="col-lg-9">
                                <select class="form-control" name="Glissiere[bloc1_islightbox3]">
                                    <option value="0">Non</option>
                                    <option <?php if (isset($objGlissiere->bloc1_islightbox3) AND $objGlissiere->bloc1_islightbox3 =='1'){echo 'selected';} ?> value="1">Oui</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--- bloc 3 fin --->
        </div>
    </div>

    <div style="<?php if (isset($objGlissiere->is_activ_bloc) AND $objGlissiere->is_activ_bloc=='1' AND isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc =='1bp'){echo 'display:block';}else{echo 'display:none';} ?>" id="bonplan_config" class="grey_bloc_independent">
        <div class="grey_bloc_2_independent container-fluid">
            <div class="col-lg-12 p-0 mb-3">
                <div style="height: 40px;background-color: #3552A1;text-align: center;padding-top: 13px;color: white">
                    UN BON PLAN ET LE BLOC TEXTE
                </div>
            </div>
            <select name="Glissiere[bonplan_id]" id="bonplan_select" class="form-control">
                <option value=""> choisir bonplan</option>
                <?php if (count($objbonplan) > 0) {
                    foreach ($objbonplan as $bonplan) { ?>
                        <option <?php if (isset($objGlissiere->bonplan_id) AND $objGlissiere->bonplan_id == $bonplan->bonplan_id) {
                            echo 'selected';
                        } ?> value="<?php echo $bonplan->bonplan_id; ?>"><?php echo $bonplan->bonplan_titre;?></option>
                    <?php } ?>
                <?php } ?>
            </select>
            <div class="col-lg-12" id="bonplan_content_posted">

            </div>
            <div id="bonplan_div22">
                                                <textarea name="Glissiere[bonplan_content]"
                                                          id="bonplan_content"><?php if (isset($objGlissiere->bonplan_content)) echo htmlspecialchars_decode($objGlissiere->bonplan_content); ?>
                    </textarea>

                <script>

                    CKEDITOR.replace('bonplan_content');

                </script>
            </div>
            <div class="col-lg-12" id="btn_fd"
                 style="background-color: #3EB4D7;height: 40px;padding-top: 12px;color: black">
                <span style="color: white!important;">Ajouter un Bouton</span> <span
                        style="float: right;">
                                                                        <select name="Glissiere[is_activ_btn_bp]"
                                                                                style="margin-right: 10px;color: black!important;"
                                                                                id="link_btn_bp">
                                                                            <option value="0" <?php if (isset($objGlissiere->is_activ_btn_bp) AND $objGlissiere->is_activ_btn_bp == 0) {
                                                                                echo 'selected';
                                                                            } ?>>non</option>
                                                                            <option value="1" <?php if (isset($objGlissiere->is_activ_btn_bp) AND $objGlissiere->is_activ_btn_bp == 1) {
                                                                                echo 'selected';
                                                                            } ?>>oui</option>
                                                                        </select></span>
            </div>

            <div class="text-center" id="link_bp" style="<?php if (isset($objGlissiere->is_activ_btn_bp) AND $objGlissiere->is_activ_btn_bp == 1){echo "display:block";}else{echo "display:none";} ?>">
                <div class="row">
                    <div class="col-lg-3 pt-2 text-center">Texte du bouton:</div>
                    <div class="col-lg-9">
                        <input class="form-control" id="btn_bp_content" type="text"
                               value="<?php if (isset($objGlissiere->btn_bp_content) AND $objGlissiere->btn_bp_content != "") {
                                   echo $objGlissiere->btn_bp_content;
                               } ?>" name="Glissiere[btn_bp_content]">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 pt-2 text-center">Lien du bouton:</div>
                    <div class="col-lg-9">
                        <select name="Glissiere[bp_existed_link]" class="form-control"
                                style="margin-right: 10px;color: black" id="valuelink_2_bp">
                            <option <?php if (isset($objGlissiere->bp_existed_link) AND $objGlissiere->bp_existed_link == '0') {
                                echo 'selected';
                            } ?> value="0">Choisir un lien local1</option><?php if ($user_groups->id == '5' AND $user_groups->id == '4') {?>
                            <option <?php if (isset($objGlissiere->bp_existed_link) AND $objGlissiere->bp_existed_link == 'p1') {
                                echo 'selected';
                            } ?> value="p1">Vers la page 1</option><?php } ?><?php if ($user_groups->id == '5' AND $user_groups->id == '4') {?>
                            <option <?php if (isset($objGlissiere->bp_existed_link) AND $objGlissiere->bp_existed_link == 'p2') {
                                echo 'selected';
                            } ?> value="p2">Vers la page 2</option><?php } ?>
                            <option <?php if (isset($objGlissiere->bp_existed_link) AND $objGlissiere->bp_existed_link == 'ag') {
                                echo 'selected';
                            } ?> value="ag">Mes Agendas</option>
                            <option <?php if (isset($objGlissiere->bp_existed_link) AND $objGlissiere->bp_existed_link == 'art') {
                                echo 'selected';
                            } ?> value="art">Mes Articles</option>
                            <option <?php if (isset($objGlissiere->bp_existed_link) AND $objGlissiere->bp_existed_link == 'bp') {
                                echo 'selected';
                            } ?> value="bp">Mes Bons Plans</option>
                            <option <?php if (isset($objGlissiere->bp_existed_link) AND $objGlissiere->bp_existed_link == 'bt') {
                                echo 'selected';
                            } ?> value="bt">Mes Boutiques</option>
                            <option <?php if (isset($objGlissiere->bp_existed_link) AND $objGlissiere->bp_existed_link == 'fd') {
                                echo 'selected';
                            } ?> value="fd">Ma Fidelisation</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 pt-2 text-center"> Ou ajouter un lien externe:</div>
                    <div class="col-lg-2 pl-3 pr-0" style="">
                        <select class="text-center form-control" id="httbp_2">
                            <option <?php if (isset($objGlissiere->bp_custom_link) AND preg_match('/http:/', $objGlissiere->bp_custom_link)) {
                                echo 'selected';
                            } ?> value="http://">http://
                            </option>
                            <option <?php if (isset($objGlissiere->bp_custom_link) AND preg_match('/http:/', $objGlissiere->bp_custom_link)) {
                                echo 'selected';
                            } ?> value="https://">https://
                            </option>
                        </select>
                    </div>
                    <div class="col-lg-7  pl-0" style="">
                        <input onchange="sub_link_custom_fd()" name="Glissiere[bp_custom_link]"
                               class="form-control text-center" type="text"
                               value="<?php echo $objGlissiere->bp_custom_link ?? ''; ?>" id="link_htt_bp_2"
                               placeholder="votre liens ici">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">Afficher le lien en lightbox</div>
                    <div class="col-lg-9">
                        <select class="form-control" name="Glissiere[bloc2_islightboxbp]">
                            <option value="0">Non</option>
                            <option <?php if (isset($objGlissiere->bloc_islightboxbp) AND $objGlissiere->bloc_islightboxbp =='1'){echo 'selected';} ?> value="1">Oui</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="fidelity_config" style="" class="container-fluid grey_bloc_independent">
        <div class="grey_bloc_independent container-fluid col-lg-12 p-0">
            <div style="height: 40px;background-color: #3552A1;text-align: center;padding-top: 13px;color: white">
                UN FIDELIT&Eacute; ET LE BLOC TEXTE
            </div>
        </div>
        <label class="text-center" for="fidelity_select">Choisir fidelité</label>
        <select name="Glissiere[fidelity_id]" id="fidelity_select" class="form-control">
            <option value=""> choisir fidelité</option>
            <?php if (count($objfidelity) > 1) {
                foreach ($objfidelity as $fidelity) { ?>
                    <option <?php if (isset($objGlissiere->fidelity_id) AND $objGlissiere->fidelity_id == $fidelity->id) {
                        echo 'selected';
                    } ?> value="<?php echo intval($fidelity->id); ?>"><?php echo $fidelity->description ?? ""; ?> </option>
                <?php } ?>
            <?php } elseif (count($objfidelity) == 1) { ?>
                <option <?php if (isset($objfidelity->id) AND $objfidelity->id == $objfidelity->id) {
                    echo 'selected';
                } ?> value="<?php echo intval($objfidelity->id); ?>"><?php echo $objfidelity->description ?? ""; ?> </option>
            <?php } ?>
        </select>


        <div class="col-lg-12" id="fidelity_content_posted"></div>
        <div id="fidelity_div">
            <label class="label text-center mt-2 mb-2" for="fidelity_content">Contenu du
                fidelité:</label>
            <textarea name="Glissiere[fidelity_content]"
                      id="fidelity_content"><?php if (isset($objGlissiere->fidelity_content)) echo htmlspecialchars_decode($objGlissiere->fidelity_content); ?>
                    </textarea>

            <script>

                CKEDITOR.replace('fidelity_content');

            </script>

            <div class="col-lg-12" id="btn_fd"
                 style="background-color: #3EB4D7;height: 40px;padding-top: 12px;color: black">
                <span style="color: white!important;">Ajouter un Bouton</span> <span
                    style="float: right;">
                                                                        <select name="Glissiere[is_activ_btn_fd]"
                                                                                style="margin-right: 10px;color: black!important;"
                                                                                id="link_btn_fd">
                                                                            <option value="0" <?php if (isset($objGlissiere->is_activ_btn_fd) AND $objGlissiere->is_activ_btn_fd == 0) {
                                                                                echo 'selected';
                                                                            } ?>>non</option>
                                                                            <option value="1" <?php if (isset($objGlissiere->is_activ_btn_fd) AND $objGlissiere->is_activ_btn_fd == 1) {
                                                                                echo 'selected';
                                                                            } ?>>oui</option>
                                                                        </select></span>
            </div>

            <div class="text-center" id="link_fd" style="<?php if (isset($objGlissiere->is_activ_btn_fd) AND $objGlissiere->is_activ_btn_fd=='1' ){echo "display:block";}else{echo "display:none";} ?>">
                <div class="row">
                    <div class="col-lg-3 pl-2  text-center">Texte du bouton</div>
                    <div class="col-lg-9">
                        <input class="form-control" id="btn_fd_content" type="text"
                               value="<?php if (isset($objGlissiere->btn_fd_content) AND $objGlissiere->btn_fd_content != "") {
                                   echo $objGlissiere->btn_fd_content;
                               } ?>" name="Glissiere[btn_fd_content]">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 pl-2  text-center">Lien du bouton</div>
                    <div class="col-lg-9">
                        <select name="Glissiere[FD_existed_link]" class="form-control"
                                style="margin-right: 10px;color: black" id="valuelink_FD">
                            <option <?php if (isset($objGlissiere->FD_existed_link) AND $objGlissiere->FD_existed_link == '0') {
                                echo 'selected';
                            } ?> value="0">Choisir un lien local1</option><?php if ($user_groups->id == '5' AND $user_groups->id == '4') {?>
                            <option <?php if (isset($objGlissiere->FD_existed_link) AND $objGlissiere->FD_existed_link == 'p1') {
                                echo 'selected';
                            } ?> value="p1">Vers la page 1</option><?php } ?><?php if ($user_groups->id == '5' AND $user_groups->id == '4') {?>
                            <option <?php if (isset($objGlissiere->FD_existed_link) AND $objGlissiere->FD_existed_link == 'p2') {
                                echo 'selected';
                            } ?> value="p2">Vers la page 2</option><?php } ?>
                            <option <?php if (isset($objGlissiere->FD_existed_link) AND $objGlissiere->FD_existed_link == 'ag') {
                                echo 'selected';
                            } ?> value="ag">Mes Agendas</option>
                            <option <?php if (isset($objGlissiere->FD_existed_link) AND $objGlissiere->FD_existed_link == 'art') {
                                echo 'selected';
                            } ?> value="art">Mes Articles</option>
                            <option <?php if (isset($objGlissiere->FD_existed_link) AND $objGlissiere->FD_existed_link == 'bp') {
                                echo 'selected';
                            } ?> value="bp">Mes Bons Plans</option>
                            <option <?php if (isset($objGlissiere->FD_existed_link) AND $objGlissiere->FD_existed_link == 'bt') {
                                echo 'selected';
                            } ?> value="bt">Mes Boutiques</option>
                            <option <?php if (isset($objGlissiere->FD_existed_link) AND $objGlissiere->FD_existed_link == 'fd') {
                                echo 'selected';
                            } ?> value="fd">Ma Fidelisation</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 pl-2 text-center"> Ou ajouter un lien externe</div>
                    <div class="col-lg-2 p-0 pl-3" style="">
                        <select class="text-center form-control" id="httfd">
                            <option <?php if (isset($objGlissiere->fd_custom_link) AND preg_match('/http:/', $objGlissiere->fd_custom_link)) {
                                echo 'selected';
                            } ?> value="http://">http://
                            </option>
                            <option <?php if (isset($objGlissiere->fd_custom_link) AND preg_match('/http:/', $objGlissiere->fd_custom_link)) {
                                echo 'selected';
                            } ?> value="https://">https://
                            </option>
                        </select>
                    </div>
                    <div class="col-lg-7 text-right pl-0" style="">
                        <input onchange="sub_link_custom_fd()" name="Glissiere[fd_custom_link]"
                               class="form-control text-center" type="text"
                               value="<?php echo $objGlissiere->fd_custom_link ?? ''; ?>" id="link_htt_fd"
                               placeholder="votre liens ici">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">Afficher le lien en lightbox</div>
                    <div class="col-lg-9">
                        <select class="form-control" name="Glissiere[bloc1_islightboxfd1]">
                            <option value="0">Non</option>
                            <option <?php if (isset($objGlissiere->bloc1_islightboxfd1) AND $objGlissiere->bloc1_islightboxfd1 =='1'){echo 'selected';} ?> value="1">Oui</option>
                        </select>
                    </div>
                </div>
            </div>

        </div>


    </div>

</div>