<div class="" id="div_pro_glissiere4_page2"
     style="<?php if ($user_groups->id == '4') { ?>display:block;<?php } elseif ($user_groups->id == '5') { ?>display:none;<?php } ?>">
    <div class="col-lg-12"
         style="background-color: #4654A4;
            color: #FFFFFF;
                    font-size: 13px;
                    font-weight: 700;
                    line-height: 1.24em;
                    margin-top: 12px;height: 40px; padding-top: 12px;"><?php if ((isset($user_groups->id) && $user_groups->id == '5') OR (isset($user_groups->id) && $user_groups->id == '4')) { ?>Glissi&egrave;re 4 page 2<?php } else { ?>Informations glissi&egrave;re 4<?php } ?>
        <span style="float:right;">
                            <label style="color:#FFFFFF !important;">Active :</label>
                                <select name="Glissiere[isActive_page2_4]" id="isActive_page2_4" style="color:#000000;">
                                    <option value="0"
                                            <?php if (!isset($objGlissiere->isActive_page2_4) OR
                                            (isset($objGlissiere->isActive_page2_4) && $objGlissiere->isActive_page2_4 == '0') OR
                                            (isset($objGlissiere->isActive_page2_4) && $objGlissiere->isActive_page2_4 == '')) { ?>selected="selected"<?php } ?>>non</option>
                                    <option value="1"
                                            <?php if (isset($objGlissiere->isActive_page2_4) AND $objGlissiere->isActive_page2_4 == '1') { ?>selected="selected"<?php } ?>>oui</option>
                                </select>
                            </span>
    </div>

    <input type="hidden" name="Glissiere[nombre_blick_gli_page2_gli4]" id="nbglissiere4p2" value="<?php if (isset($objGlissiere->nombre_blick_gli_page2_gli4)){echo $objGlissiere->nombre_blick_gli_page2_gli4;} ?>">
    <div class="pl-3 pr-3 pt-0 pb-0"  id="dispnbgli4p2" style=" color:#000000; <?php if (isset($objGlissiere->isActive_page2_4) AND $objGlissiere->isActive_page2_4 =='1'){echo 'display:block';}else{echo 'display:none';} ?>">
        <div class="row rowbtn">
            <div class="col-lg-6 btn_vert" id="1g14_2"><img style="width: 100%;" src="<?php echo base_url('assets/img/1.png')?>"></div>
            <div class="col-lg-6 btn_vert " id="2g14_2"><img style="width: 100%;" src="<?php echo base_url('assets/img/2.png')?>"></div>

        </div>
    </div>

    <div id="glissiere4contentp2" class="container-fluid" style="<?php if (isset($objGlissiere->isActive_page2_4) AND $objGlissiere->isActive_page2_4 == '1'){echo 'display:block';}else{ echo 'display:none';}?>; width:100%;">
        <div class="row pt-4" id="gl4_tittle_p2">

            <label class="label">Titre Glissière4: </label>

            <div class="col-lg-12">
                <input class="form-control" type="text" name="Glissiere[page2_4_titre]"
                       id="page2_2_titre"
                       value="<?php if (isset($objGlissiere->page2_4_titre)) echo htmlspecialchars_decode($objGlissiere->page2_4_titre); ?>"/>
            </div>
        </div>
        <div class="row" style="padding-bottom: 200px">
            <div class="col-lg-6" id="champ2gli4p2" style="<?php if (isset($objGlissiere->nombre_blick_gli_page2_gli4) AND $objGlissiere->nombre_blick_gli_page2_gli4 == '1' OR (isset($objGlissiere->nombre_blick_gli_page2_gli4) AND $objGlissiere->nombre_blick_gli_page2_gli4 == '2')){echo 'display:block';}else{echo 'display:none';} ?>">

                <div class="container-fluid">
                    <div class="row pt-4">
                        <div class="col-lg-12" style="background-color: #4EB4D7;padding-top: 14px;height: 40px;color: white;margin-bottom: 20px"
                        >Glissière4 champ2
                        </div>
                        <div class="col-lg-12">
                            <div class="img_tab" id="gl2_img_p2">
                                <div class="row">
                                    <div class="col-lg-12 text-center">


                                        <a href='javascript:void(0);' title="page2_4_image_1"
                                            <?php if (empty($objGlissiere->page2_4_image_1) || $objGlissiere->page2_4_image_1 == "") { ?>
                                                onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-gli-page2_4_image_1"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                            <?php } ?>
                                           class="btn btn-info" id="page2_4_image_1">Ajouter une Photo</a>


                                        <a href="javascript:void(0);" class="btn btn-danger"
                                            <?php if (!empty($objGlissiere->page2_4_image_1) && $objGlissiere->page2_4_image_1 != "") { ?>
                                                onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','page2_4_image_1');"
                                            <?php } ?>
                                        >Supprimer</a>


                                    </div>
                                    <div class="col-lg-12">
                                        <?php
                                        if (is_object($objGlissiere) && file_exists($path_img_gallery . $objGlissiere->page2_4_image_1) && $objGlissiere->page2_4_image_1 != "") {
                                            echo '<img style="max-width:100%" class="img-fluid"  src="' . base_url() . $path_img_gallery . $objGlissiere->page2_4_image_1 . '"/>';
                                        } else if (is_object($objGlissiere) && file_exists($path_img_gallery_old . $objGlissiere->page2_2_image_1) && $objGlissiere->page2_4_image_1 != "") {
                                            echo '<img style="max-width:100%" class="img-fluid"  src="' . base_url() . $path_img_gallery_old . $objGlissiere->page2_4_image_1 . '"/>';
                                        }
                                        ?>
                                        <input type="hidden" name="page2_2_image_1" id="page2_2_image_1"
                                               value="<?php if (is_object($objGlissiere) && isset($objGlissiere->page2_4_image_1)) echo htmlspecialchars_decode($objGlissiere->page2_4_image_1); ?>"/>
                                        <div id="div_error_taille_activite1_image1"
                                             class="div_error_taille_4_4"><?php if (isset($img_error_verify_array) && in_array("activite4_image1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 p-0">
                            <div id="gl4_contentp2">
                                <div colspan="2">

                                <textarea name="Glissiere[page2_4_contenu1]"
                                          id="page2_4_contenu1"><?php if (isset($objGlissiere->page2_4_contenu1)) echo htmlspecialchars_decode($objGlissiere->page2_4_contenu1); ?></textarea>
                                    <script>

                                        CKEDITOR.replace('page2_4_contenu1');

                                    </script>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-12 p-0">
                            <div class="container-fluid p-0" style="">
                                <div class="pb-4">
                                    <div class="col-lg-12" id="btn_gli4_p2"
                                         style="background-color: #4EB4D7;height: 40px;padding-top: 12px;color: black">
                                        <span style="color: white!important;">Ajouter un Bouton</span> <span
                                            style="float: right;">
                                                            <select name="Glissiere[is_activ_btn_glissiere4_champ1_p2]"
                                                                    style="margin-right: 10px;color: black!important;"
                                                                    id="link_btn_4_gl_p2">
                                                                <option <?php if (is_object($objGlissiere) && $objGlissiere->is_activ_btn_glissiere4_champ1_p2 == 0) {
                                                                    echo 'selected';
                                                                } ?> value="0">non</option>
                                                                <option <?php if (is_object($objGlissiere) && $objGlissiere->is_activ_btn_glissiere4_champ1_p2 == 1) {
                                                                    echo 'selected';
                                                                } ?> value="1">oui</option>
                                                            </select></span>
                                    </div>


                                    <div class="text-center" id="alllinkg4p2"  style="<?php if (isset($objGlissiere->is_activ_btn_glissiere4_champ1_p2) AND $objGlissiere->is_activ_btn_glissiere4_champ1_p2 == '0'){echo 'display:none';}elseif (isset($objGlissiere->is_activ_btn_glissiere4_champ1_p2) AND $objGlissiere->is_activ_btn_glissiere4_champ1_p2 == '1'){echo 'display:block';}else{echo 'display:none';} ?>">
                                        <div class="row pt-3">
                                            <div class="col-lg-3">
                                                <label for="btn_gli4_content1_p2">Texte du bouton</label>
                                            </div>
                                            <div class="col-lg-9">
                                                <input class="form-control" id="btn_gli4_content1_p2" type="text"
                                                       value="<?php if (isset($objGlissiere->btn_gli4_content1_p2) AND $objGlissiere->btn_gli4_content1_p2 != "") {
                                                           echo $objGlissiere->btn_gli4_content1_p2;
                                                       } ?>" name="Glissiere[btn_gli4_content1_p2]">
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <label for="valuelink_gli4_p2">Lien du bouton</label>
                                            </div>
                                            <div class="col-lg-9">
                                                <select
                                                    name="Glissiere[gl4_existed_link1_p2]" class="form-control"
                                                    style="margin-right: 10px;color: black" id="valuelink4">
                                                    <option <?php if (isset($objGlissiere->gl4_existed_link1_p2) AND $objGlissiere->gl4_existed_link1_p2 == '0') {
                                                        echo 'selected';
                                                    } ?> value="0">Choisir un lien local1</option><?php if ($user_groups->id == '5') {?>
                                                    <option <?php if (isset($objGlissiere->gl4_existed_link1_p2) AND $objGlissiere->gl4_existed_link1_p2 == 'p1') {
                                                        echo 'selected';
                                                    } ?> value="p1">Vers la page 1</option><?php } ?>
                                                    <option <?php if (isset($objGlissiere->gl4_existed_link1_p2) AND $objGlissiere->gl4_existed_link1_p2 == 'p2') {
                                                        echo 'selected';
                                                    } ?> value="p2">Vers la page 2</option>
                                                    <option <?php if (isset($objGlissiere->gl4_existed_link1_p2) AND $objGlissiere->gl4_existed_link1_p2 == 'ag') {
                                                        echo 'selected';
                                                    } ?> value="ag">Mes Agendas</option>
                                                    <option <?php if (isset($objGlissiere->gl4_existed_link1_p2) AND $objGlissiere->gl4_existed_link1_p2 == 'art') {
                                                        echo 'selected';
                                                    } ?> value="art">Mes Articles</option>
                                                    <option <?php if (isset($objGlissiere->gl4_existed_link1_p2) AND $objGlissiere->gl4_existed_link1_p2 == 'bp') {
                                                        echo 'selected';
                                                    } ?> value="bp">Mes Bons Plans</option>
                                                    <option <?php if (isset($objGlissiere->gl4_existed_link1_p2) AND $objGlissiere->gl4_existed_link1_p2 == 'bt') {
                                                        echo 'selected';
                                                    } ?> value="bt">Mes Boutiques</option>
                                                    <option <?php if (isset($objGlissiere->gl4_existed_link1_p2) AND $objGlissiere->gl4_existed_link1_p2 == 'fd') {
                                        echo 'selected';
                                    } ?> value="fd">Ma Fidelisation</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                Ou ajouter un lien externe
                                            </div>
                                            <div class="col-lg-3">
                                                <select class="text-center form-control" id="htt">
                                                    <option <?php if (is_object($objGlissiere) && preg_match('/http:/', $objGlissiere->gl4_custom_link1_p2)) {
                                                        echo 'selected';
                                                    } ?> value="http://">http://
                                                    </option>
                                                    <option <?php if (is_object($objGlissiere) && preg_match('/http:/', $objGlissiere->gl4_custom_link1_p2)) {
                                                        echo 'selected';
                                                    } ?> value="https://">https://
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="col-lg-6 text-right pr0"
                                                 style="">
                                                <input onchange="sub_link_custom1()"
                                                       name="Glissiere[gl4_custom_link1_p2]"
                                                       class="form-control text-center" type="text" value="<?php echo $objGlissiere->gl4_custom_link1_p2 ?? '';?>"
                                                       id="link_htt"
                                                       placeholder="votre liens ici">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6"  id="champ2_gli4contentp2" style="<?php if (isset($objGlissiere->nombre_blick_gli_page2_gli4) AND $objGlissiere->nombre_blick_gli_page2_gli4 == '2'){echo 'display:block';}else{echo 'display:none';} ?>">
                <div class="row pt-4">
                    <div class="col-lg-12">
                        <div style="background-color: #4EB4D7;padding-top: 14px;height: 40px;color: white;margin-bottom: 20px"
                             class="text-center">Glissière4 champ2
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="img_tab" id="gl1_img2_p2">
                            <div class="row">
                                <div class="text-center col-lg-12" id="Articlepage2_4_image_2_container">

                                    <a href='javascript:void(0);' title="page2_4_image_2"
                                        <?php if (empty($objGlissiere->page2_4_image_2) || $objGlissiere->page2_4_image_2 == "") { ?>
                                            onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-gli-page2_4_image_2"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                        <?php } ?>
                                       class="btn btn-info" id="page2_4_image_2">Ajouter une Photo</a>

                                    <a href="javascript:void(0);" class="btn btn-danger"
                                        <?php if (!empty($objGlissiere->page2_4_image_2) && $objGlissiere->page2_4_image_2 != "") { ?>
                                            onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','page2_4_image_2');"
                                        <?php } ?>
                                    >Supprimer</a>

                                </div>
                                <div class="col-lg-12">
                                    <?php
                                    if (is_object($objGlissiere) && file_exists($path_img_gallery . $objGlissiere->page2_4_image_2) && $objGlissiere->page2_4_image_2 != "") {
                                        echo '<img style="max-width:100%" class="img-fluid"  src="' . base_url() . $path_img_gallery . $objGlissiere->page2_4_image_2 . '"/>';
                                    } else if (is_object($objGlissiere) && file_exists($path_img_gallery_old . $objGlissiere->page2_4_image_2) && $objGlissiere->page2_4_image_2 != "") {
                                        echo '<img style="max-width:100%" class="img-fluid"  src="' . base_url() . $path_img_gallery_old . $objGlissiere->page2_4_image_2 . '"/>';
                                    }
                                    ?>
                                    <input type="hidden" name="page2_4_image_2" id="page2_4_image_2"
                                           value="<?php if (isset($objGlissiere->page2_4_image_2)) echo htmlspecialchars_decode($objGlissiere->page2_4_image_2); ?>"/>
                                    <div id="div_error_taille_activite4_image2"
                                         class="div_error_taille_4_4"><?php if (isset($img_error_verify_array) && in_array("activite4_image1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 p-0">
                        <div id="gl4_content2">
                            <div colspan="2">

                                <textarea name="Glissiere[page2_4_contenu2]"
                                          id="page2_4_contenu2"><?php if (isset($objGlissiere->page2_4_contenu2)) echo htmlspecialchars_decode($objGlissiere->page2_4_contenu2); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page2_4_contenu2');

                                </script>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-12 p-0">
                        <div>
                            <div class="pb-4">
                                <div class="col-lg-12" id="btn_4_gli2_p2"
                                     style="background-color: #4EB4D7;height: 40px;padding-top: 12px;color: black">
                                    <span style="color: white!important;">Ajouter un Bouton</span> <span
                                        style="float: right;">
                                                            <select name="Glissiere[is_activ_btn_glissiere4_champ2_p2]"
                                                                    style="margin-right: 10px;color: black!important;"
                                                                    id="link_btn_4_gl2_p2">
                                                                <option <?php if (!isset($objGlissiere->is_activ_btn_glissiere4_champ2_p2) or (isset($objGlissiere->is_activ_btn_glissiere4_champ2_p2) && $objGlissiere->is_activ_btn_glissiere4_champ2_p2 == 0)) {
                                                                    echo 'selected';
                                                                } ?> value="0">non</option>
                                                                <option <?php if (isset($objGlissiere->is_activ_btn_glissiere4_champ2_p2) && $objGlissiere->is_activ_btn_glissiere4_champ2_p2 == 1) {
                                                                    echo 'selected';
                                                                } ?> value="1">oui</option>
                                                            </select></span>
                                </div>

                                <div class="text-center" id="alllink2g4p2" style="<?php if (isset($objGlissiere->is_activ_btn_glissiere4_champ2_p2) AND $objGlissiere->is_activ_btn_glissiere4_champ2_p2 =='1' ){echo "display:block;";}else{echo "display:none;";} ?>">
                                    <div class="row pt-3">
                                        <div class="col-lg-3">
                                            <label for="btn_gli4_content2_p2">Texte du bouton</label>
                                        </div>
                                        <div class="col-lg-9">
                                            <input class="form-control" id="btn_gli4_content2_p2_p2" type="text"
                                                   value="<?php if (isset($objGlissiere->btn_gli4_content2_p2) AND $objGlissiere->btn_gli4_content2_p2 != "") {
                                                       echo $objGlissiere->btn_gli4_content2_p2;
                                                   } ?>" name="Glissiere[btn_gli4_content2_p2]">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label for="valuelink2g4p2">Lien du bouton</label>
                                        </div>
                                        <div class="col-lg-9">
                                            <select name="Glissiere[gl4_existed_link2_p2]"
                                                    class="form-control"
                                                    style="margin-right: 10px;color: black"
                                                    id="valuelink2g4">
                                                <option <?php if (isset($objGlissiere->gl4_existed_link2_p2) AND $objGlissiere->gl4_existed_link2_p2 == '0') {
                                                    echo 'selected';
                                                } ?> value="0">Choisir un lien local2</option><?php if ($user_groups->id == '5') {?>
                                                <option <?php if (isset($objGlissiere->gl4_existed_link2_p2) AND $objGlissiere->gl4_existed_link2_p2 == 'p2') {
                                                    echo 'selected';
                                                } ?> value="p1">Vers la page 1</option><?php } ?>
                                                <option <?php if (isset($objGlissiere->gl4_existed_link2_p2) AND $objGlissiere->gl4_existed_link2_p2 == 'p1') {
                                                    echo 'selected';
                                                } ?> value="p2">Vers la page 2</option>
                                                <option <?php if (isset($objGlissiere->gl4_existed_link2_p2) AND $objGlissiere->gl4_existed_link2_p2 == 'ag') {
                                                    echo 'selected';
                                                } ?> value="ag">Mes Agendas</option>
                                                <option <?php if (isset($objGlissiere->gl4_existed_link2_p2) AND $objGlissiere->gl4_existed_link2_p2 == 'art') {
                                                    echo 'selected';
                                                } ?> value="art">Mes Articles</option>
                                                <option <?php if (isset($objGlissiere->gl4_existed_link2_p2) AND $objGlissiere->gl4_existed_link2_p2 == 'bp') {
                                                    echo 'selected';
                                                } ?> value="bp">Mes Bons Plans</option>
                                                <option <?php if (isset($objGlissiere->gl4_existed_link2_p2) AND $objGlissiere->gl4_existed_link2_p2 == 'bt') {
                                                    echo 'selected';
                                                } ?> value="bt">Mes Boutiques</option>
                                                <option <?php if (isset($objGlissiere->gl4_existed_link2_p2) AND $objGlissiere->gl4_existed_link2_p2 == 'fd') {
                                        echo 'selected';
                                    } ?> value="fd">Ma Fidelisation</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            Ou ajouter un lien externe
                                        </div>
                                        <div class="col-lg-3">
                                            <select class="form-control text-left" id="htt2g4">
                                                <option <?php if (isset($objGlissiere->gl4_custom_link2_p2) && preg_match('/http:/', $objGlissiere->gl4_custom_link2_p2)) {
                                                    echo 'selected';
                                                } ?> value="http://">http://
                                                </option>
                                                <option <?php if (isset($objGlissiere->gl4_custom_link2_p2) && preg_match('/https:/', $objGlissiere->gl4_custom_link2_p2)) {
                                                    echo 'selected';
                                                } ?> value="https://">https://
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6">

                                            <input onchange="sub_link_custom2()" name="Glissiere[gl4_custom_link2_p2]"
                                                   class="form-control text-center" type="text" value="<?php echo $objGlissiere->gl4_custom_link2_p2 ?? '';?>"
                                                   id="link_htt2g4p2"
                                                   placeholder="votre liens ici">
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

</div>