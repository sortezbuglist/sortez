<script type="text/javascript">
jQuery(document).ready(function () {
     <?php if (isset($objbloc_info->is_activ_bloc_page1) AND $objbloc_info->is_activ_bloc_page1 == 1) {  ?>
                jQuery('#type_bloc_page1_choose').css("display", "block"); 
          <?php if (isset($objbloc_info->type_bloc_page1) AND $objbloc_info->type_bloc_page1 == '1') {  ?>
                document.getElementById('bloc1_page1').style.display = "block";
                $("#bloc1_page1").removeClass("col-lg-6");
               $("#bloc2_page1").removeClass("col-lg-6");
          <?php } ?>
            <?php if (isset($objbloc_info->type_bloc_page1) AND $objbloc_info->type_bloc_page1 == '2') {  ?>
                document.getElementById('bloc2_page1').style.display = "block";
                document.getElementById('bloc1_page1').style.display = "block";
                $("#bloc2_page1").addClass("col-lg-6");
               $("#bloc1_page1").addClass("col-lg-6");
            <?php } ?>
            <?php if (isset($objbloc_info->type_bloc_page1) AND $objbloc_info->type_bloc_page1 == '3') {  ?>
            document.getElementById('bloc2_page1').style.display = "block";
            document.getElementById('bloc1_page1').style.display = "block";
            document.getElementById('bloc3_page1').style.display = "block";
            $("#bloc2_page1").addClass("col-lg-4");
            $("#bloc1_page1").addClass("col-lg-4");
            $("#bloc3_page1").addClass("col-lg-4");
            <?php } ?>
            <?php if (isset($objbloc_info->type_bloc_page1) AND $objbloc_info->type_bloc_page1 == '1bp') {  ?>
               document.getElementById('bonplan_config_page1').style.display = "block";
        <?php } ?>
            <?php if (isset($objbloc_info->type_bloc_page1) AND $objbloc_info->type_bloc_page1 == '1fd') {  ?>
               document.getElementById('fidelity_config_page1').style.display = "block";
            <?php } ?>
            <?php if (isset($objbloc_info->is_activ_btn_bloc1_page1) && $objbloc_info->is_activ_btn_bloc1_page1 == '1') {  ?>
                document.getElementById('link_bloc1_page1').style.display = "block";
            <?php } ?>
            <?php if (isset($objbloc_info->is_activ_btn_bloc1_2_page1) && $objbloc_info->is_activ_btn_bloc1_2_page1 == '1') {  ?>
                document.getElementById('link_bloc2_page1').style.display = "block";
        <?php } ?>  
            <?php if (isset($objbloc_info->is_activ_btn_bp_page1) && $objbloc_info->is_activ_btn_bp_page1 == '1') {  ?>
                document.getElementById('link_bp_page1').style.display = "block";
            <?php } ?>
            <?php if (isset($objbloc_info->is_activ_btn_fd_page1) && $objbloc_info->is_activ_btn_fd_page1 == '1') {  ?>
                document.getElementById('link_fd_page1').style.display = "block";
            <?php } ?>       
        <?php } ?>
    	$("#is_activ_bloc_page1").change(function () {
            if (document.getElementById('is_activ_bloc_page1').value == 0) {
                jQuery('#type_bloc_page1_choose').css("display", "none");
               
                jQuery('#bloc1_page1').css("display", "none");
                
                jQuery('#bloc2_page1').css("display", "none");

                jQuery('#bloc3_page1').css("display", "none");
                
                jQuery('#bonplan_config_page1').css("display", "none");
                
                jQuery('#fidelity_config_page1').css("display", "none");
            }
            else if (document.getElementById('is_activ_bloc_page1').value == 1) {
                jQuery('#type_bloc_page1_choose').css("display", "block");

                jQuery('#bloc1_page1').css("display", "none");
                
                jQuery('#bloc2_page1').css("display", "none");

                jQuery('#bloc3_page1').css("display", "none");

                jQuery('#bonplan_config_page1').css("display", "none");
                
                jQuery('#fidelity_config_page1').css("display", "none");
            }
        });
      $("#1b1_1_page1").click(function () {
            document.getElementById('bloc1_page1').style.display = "block";
            document.getElementById('bloc2_page1').style.display = "none";
            document.getElementById('bloc3_page1').style.display = "none";
            document.getElementById('bonplan_config_page1').style.display = "none";
            document.getElementById('fidelity_config_page1').style.display = "none";
            $('#type_bloc_page1js').val("1");
            $("#bloc1_page1").removeClass("col-lg-6");
            $("#bloc2_page1").removeClass("col-lg-6");
            $("#bloc1_page1").removeClass("col-lg-4");
            $("#bloc2_page1").removeClass("col-lg-4");
            $("#bloc3_page1").removeClass("col-lg-4");

        });
        $("#2b1_1_page1").click(function () {
            document.getElementById('bloc1_page1').style.display = "block";
            document.getElementById('bloc2_page1').style.display = "block";
            document.getElementById('bloc3_page1').style.display = "none";
            document.getElementById('bonplan_config_page1').style.display = "none";
            document.getElementById('fidelity_config_page1').style.display = "none";
            $('#type_bloc_page1js').val("2");
            $("#bloc1_page1").addClass("col-lg-6");
            $("#bloc2_page1").addClass("col-lg-6");
        });
    $("#3b1_1_page1").click(function () {
        document.getElementById('bloc1_page1').style.display = "block";
        document.getElementById('bloc2_page1').style.display = "block";
        document.getElementById('bloc3_page1').style.display = "block";
        document.getElementById('bonplan_config_page1').style.display = "none";
        document.getElementById('fidelity_config_page1').style.display = "none";
        $('#type_bloc_page1js').val("3");
        $("#bloc1_page1").removeClass("col-lg-6");
        $("#bloc2_page1").removeClass("col-lg-6");
        $("#bloc1_page1").addClass("col-lg-4");
        $("#bloc2_page1").addClass("col-lg-4");
        $("#bloc3_page1").addClass("col-lg-4");
    });
    $("#1bp1_1_page1").click(function () {
            document.getElementById('bonplan_config_page1').style.display = "block";
            document.getElementById('bloc1_page1').style.display = "none";
            document.getElementById('bloc2_page1').style.display = "none";
            document.getElementById('bloc3_page1').style.display = "none";
            document.getElementById('fidelity_config_page1').style.display = "none";
            $('#type_bloc_page1js').val("1bp");
        });
        $("#1fd1_1_page1").click(function () {
            document.getElementById('fidelity_config_page1').style.display = "block";
            document.getElementById('bonplan_config_page1').style.display = "none";
            document.getElementById('bloc1_page1').style.display = "none";
            document.getElementById('bloc2_page1').style.display = "none";
            document.getElementById('bloc3_page1').style.display = "none";
            $('#type_bloc_page1js').val("1fd");
        });
        $("#link_btn_bloc1_page1").change(function () {

            if (document.getElementById('link_btn_bloc1_page1').value == 1) {
                document.getElementById('link_bloc1_page1').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc1_page1').value == 0) {
                document.getElementById('link_bloc1_page1').style.display = "none";

            }

        }); 
        $("#link_btn_bloc2_page1").change(function () {


            if (document.getElementById('link_btn_bloc2_page1').value == 1) {
                document.getElementById('link_bloc2_page1').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc2_page1').value == 0) {
                document.getElementById('link_bloc2_page1').style.display = "none";

            }

        });
        $("#link_btn_bloc3_page1").change(function () {


            if (document.getElementById('link_btn_bloc3_page1').value == 1) {
                document.getElementById('link_bloc3_page1').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc3_page1').value == 0) {
                document.getElementById('link_bloc3_page1').style.display = "none";

            }

        });
    $("#link_btn_bp_page1").change(function () {

            if (document.getElementById('link_btn_bp_page1').value == 1) {
                document.getElementById('link_bp_page1').style.display = "block";

            }
            else if (document.getElementById('link_btn_bp_page1').value == 0) {
                document.getElementById('link_bp_page1').style.display = "none";

            }

        });
        $("#link_btn_fd_page1").change(function () {

            if (document.getElementById('link_btn_fd_page1').value == 1) {
                document.getElementById('link_fd_page1').style.display = "block";

            }
            else if (document.getElementById('link_btn_fd_page1').value == 0) {
                document.getElementById('link_fd_page1').style.display = "none";

            }

        }); 
});
</script>