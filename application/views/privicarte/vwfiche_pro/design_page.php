
<input type="hidden" name="current_page" id="current_page" value="design">
<input type="hidden" name="page_data" id="page_data" value="<?php echo $page_data; ?>"/>
<?php if (isset($page_data) && $page_data == 'coordonnees') echo '<div style="display:none;">'; ?>
<?php if (isset($objAbonnementCommercant) && $user_groups->id == '5') { ?>
    <div id="div_pro_fondecran">
        <div class="div_stl_long_platinum">Image page et arrière plan (Dimensions 2024 pixels x 1100
            pixels)
        </div>
        <div class="container-fluid">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">

                <tr>
                    <td class="stl_long_input_platinum_td" style="vertical-align: top;">
                        <label style="font-size:14px;">Importer votre image : </label>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-lg-6">
                                <?php
                                $path_img_gallery_bg = 'application/resources/front/photoCommercant/imagesbank/' . $objCommercant->user_ionauth_id . '/bg/';
                                $path_img_gallery_bg_sample = 'application/resources/front/photoCommercant/imagesbank/bg_sample/';
                                $path_img_gallery_old = 'application/resources/front/photoCommercant/images/';
                                ?>

                                <div id="Articlebackground_image_container">

                                    <a href='javascript:void(0);' title="Photo1" class="btn btn-info"
                                        <?php if (empty($objCommercant->background_image) || $objCommercant->background_image == "") { ?>
                                            onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-bg-background_image"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                        <?php } ?>
                                       id="Articlebackground_link">Ajouter une image</a>

                                    <a href="javascript:void(0);" class="btn btn-danger"
                                        <?php if (!empty($objCommercant->background_image)) { ?>
                                            onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','background_image');"
                                        <?php } ?>
                                    >Supprimer</a>


                                </div>

                                <script type="text/javascript">
                                    jQuery(document).ready(function () {
                                        jQuery("#Societe_bg_default_image_check").click(function () {
                                            if (jQuery(this).is(':checked')) {
                                                jQuery("#Societe_bg_default_image").val("1");
                                            } else {
                                                jQuery("#Societe_bg_default_image").val("0");
                                            }
                                        });
                                        jQuery("#Societe_bg_default_color_container_check").click(function () {
                                            if (jQuery(this).is(':checked')) {
                                                jQuery("#Societe_bg_default_color_container").val("1");
                                            } else {
                                                jQuery("#Societe_bg_default_color_container").val("0");
                                            }
                                        });
                                    });
                                </script>

                                <div class="col-xs-12 pt-3"><img
                                        src="<?php echo GetImagePath("privicarte/"); ?>/bg_ico.png"
                                        alt="bg_ico"
                                        style="width: 200px;"/></div>
                                <div style="width:100%; margin-top:30px;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <input type="checkbox"
                                                       name="Societe_bg_default_image_check"
                                                       <?php if ($objCommercant->bg_default_image == '1') { ?>checked="checked"<?php } ?>
                                                       id="Societe_bg_default_image_check"/>
                                                <input type="hidden" name="Societe[bg_default_image]"
                                                       id="Societe_bg_default_image"
                                                       value="<?php echo $objCommercant->bg_default_image; ?>"/>
                                            </td>
                                            <td>Fond arrière plan Sortez par d&eacute;faut</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox"
                                                       name="Societe_bg_default_color_container_check"
                                                       <?php if ($objCommercant->bg_default_color_container == '1') { ?>checked="checked"<?php } ?>
                                                       id="Societe_bg_default_color_container_check"/>
                                                <input type="hidden"
                                                       name="Societe[bg_default_color_container]"
                                                       id="Societe_bg_default_color_container"
                                                       value="<?php echo $objCommercant->bg_default_color_container; ?>"/>
                                            </td>
                                            <td>Si coché couleur de la page : gris 225</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <?php
                                if (file_exists($path_img_gallery_bg . $objCommercant->background_image) && isset($objCommercant->background_image) && $objCommercant->background_image != "") {
                                    echo '<img style="max-width:100%" src="' . base_url() . $path_img_gallery_bg . $objCommercant->background_image . '" "/>';
                                } else if (file_exists($path_img_gallery_bg_sample . $objCommercant->background_image) && isset($objCommercant->background_image) && $objCommercant->background_image != "") {
                                    echo '<img style="max-width:100%" src="' . base_url() . $path_img_gallery_bg_sample . $objCommercant->background_image . '" "/>';
                                } else if (file_exists($path_img_gallery_old . $objCommercant->background_image) && isset($objCommercant->background_image) && $objCommercant->background_image != "") {
                                    echo '<img style="max-width:100%" src="' . base_url() . $path_img_gallery_old . $objCommercant->background_image . '" "/>';
                                }
                                ?>
                            </div>
                        </div>


                    </td>
                </tr>
            </table>
        </div>
    </div>
<?php } ?>



<?php if (isset($objAbonnementCommercant) && ($user_groups->id == '5' || $user_groups->id == '4')) { ?>
    <?php if (isset($user_groups->id) AND $user_groups->id =='5'){ ?>
        <div id="div_pro_entete_banniere"
             <?php if (isset($objAbonnementCommercant) && $user_groups->id == '4') { ?>style="background-color:#E1E1E1;"<?php } ?>>

            <div class="div_stl_long_platinum">Image bannière page de présentation (dimentions 1200 pixels x
                350 pixels)
            </div>
            <div class="container-fluid">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">

                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label style="font-size:14px;">Intégration d’une image bannière : </label>
                        </td>
                        <td>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div id="Articlebandeau_top_container">
                                        <?php
                                        $path_img_gallery_bn = 'application/resources/front/photoCommercant/imagesbank/' . $objCommercant->user_ionauth_id . '/bn/';
                                        $path_img_gallery_bn_sample = 'application/resources/front/photoCommercant/imagesbank/bn_sample/';
                                        $path_img_gallery_old = 'application/resources/front/photoCommercant/images/';
                                        ?>


                                        <a href='javascript:void(0);' title="Photo1" class="btn btn-info"
                                            <?php if (empty($objCommercant->bandeau_top) || $objCommercant->bandeau_top == "") { ?>
                                                onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-bn-bandeau_top"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                            <?php } ?>
                                           id="Articlebandeau_link">Ajouter une image</a>

                                        <a href="javascript:void(0);" class="btn btn-danger"
                                            <?php if (!empty($objCommercant->bandeau_top) && $objCommercant->bandeau_top != "") { ?>
                                                onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','bandeau_top');"
                                            <?php } ?>
                                        >Supprimer</a>

                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <?php
                                    if (file_exists($path_img_gallery_bn . $objCommercant->bandeau_top) && $objCommercant->bandeau_top != "") {
                                        echo '<img style="max-width:100%"  src="' . base_url() . $path_img_gallery_bn . $objCommercant->bandeau_top . '" "/>';
                                    } else if (file_exists($path_img_gallery_bn_sample . $objCommercant->bandeau_top) && $objCommercant->bandeau_top != "") {
                                        echo '<img style="max-width:100%" src="' . base_url() . $path_img_gallery_bn_sample . $objCommercant->bandeau_top . '" "/>';
                                    } else if (file_exists($path_img_gallery_old . $objCommercant->bandeau_top) && $objCommercant->bandeau_top != "") {
                                        echo '<img style="max-width:100%" src="' . base_url() . $path_img_gallery_old . $objCommercant->bandeau_top . '" "/>';
                                    }
                                    ?>
                                </div>
                            </div>

                        </td>
                    </tr>

                    <tr>
                        <td class="stl_long_input_platinum_td">&nbsp;
                        </td>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                   style="margin-top:5px;">
                                <tr>
                                    <td style="vertical-align: top;"><img
                                            src="<?php echo GetImagePath("privicarte/"); ?>/bandeau_top_icon.png"
                                            alt="bandeau_top_icon" width="200"/></td>
                                    <td style="vertical-align: top; padding: 5px">

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <script type="text/javascript">
                                            jQuery(document).ready(function () {
                                                jQuery("#bandeau_top_default_Societe_checker").click(function () {
                                                    if (jQuery(this).is(':checked')) {
                                                        jQuery("#bandeau_top_default").val("1");
                                                    } else {
                                                        jQuery("#bandeau_top_default").val("0");
                                                    }
                                                });
                                            });
                                        </script>
                                        <input type="checkbox" name="Societebandeau_top_default_checker"
                                               id="bandeau_top_default_Societe_checker" value=""
                                               <?php if (isset($objCommercant->bandeau_top_default) && $objCommercant->bandeau_top_default == "1") { ?>checked="checked"<?php } ?>/>
                                        <input type="hidden" name="Societe[bandeau_top_default]"
                                               id="bandeau_top_default"
                                               value="<?php if (isset($objCommercant->bandeau_top_default) && $objCommercant->bandeau_top_default == "1") echo "1"; else echo "0"; ?>"/>
                                        Image par d&eacute;faut
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <?php if (isset($objAbonnementCommercant) && $user_groups->id == '5') { ?>
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label style="font-size:14px;">et définir une couleur (fond menu et boutons)
                                    : </label>
                            </td>
                            <td>

                                <link rel="stylesheet"
                                      href="<?php echo GetJsPath("front/"); ?>/colorPicker/colorpicker.css"
                                      type="text/css"/>

                                <style type="text/css">

                                    .label{
                                        font-size: 12px!important;
                                    }
                                    #colorSelector2 {
                                        position: relative;
                                        width: 36px;
                                        height: 36px;
                                    }

                                    #colorSelector2 div {
                                        position: relative;
                                        width: 28px;
                                        height: 28px;
                                        background: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/select2.png) center;
                                    }

                                    #colorpickerHolder2 {
                                        width: 356px;
                                        height: 0;
                                        overflow: hidden;
                                        position: relative;
                                    }

                                    #colorpickerHolder2 .colorpicker {
                                        background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_background.png);
                                        position: relative;
                                        bottom: 0;
                                        left: 0;
                                    }

                                    #colorpickerHolder2 .colorpicker_hue div {
                                        background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_indic.gif);
                                    }

                                    #colorpickerHolder2 .colorpicker_hex {
                                        background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_hex.png);
                                    }

                                    #colorpickerHolder2 .colorpicker_rgb_r {
                                        background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_rgb_r.png);
                                    }

                                    #colorpickerHolder2 .colorpicker_rgb_g {
                                        background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_rgb_g.png);
                                    }

                                    #colorpickerHolder2 .colorpicker_rgb_b {
                                        background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_rgb_b.png);
                                    }

                                    #colorpickerHolder2 .colorpicker_hsb_s {
                                        background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_hsb_s.png);
                                        display: none;
                                    }

                                    #colorpickerHolder2 .colorpicker_hsb_h {
                                        background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_hsb_h.png);
                                        display: none;
                                    }

                                    #colorpickerHolder2 .colorpicker_hsb_b {
                                        background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_hsb_b.png);
                                        display: none;
                                    }

                                    #colorpickerHolder2 .colorpicker_submit {
                                        background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_submit.png);
                                    }

                                    #colorpickerHolder2 .colorpicker input {
                                        color: #778398;
                                    }

                                    #customWidget {
                                        position: relative;
                                        height: 36px;
                                    }

                                </style>

                                <input type="hidden" name="Societe[bandeau_color]" id="bandeau_colorSociete"
                                       value="<?php if (isset($objCommercant->bandeau_color)) echo $objCommercant->bandeau_color; else echo '#3653A3'; ?>"/>
                                <div id="colorSelector2">
                                    <div style="background-color: <?php if (isset($objCommercant->bandeau_color)) echo $objCommercant->bandeau_color; else echo '#3653A3'; ?>"></div>
                                </div>
                                <div id="colorpickerHolder2">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="couleurLogo">Couleur texte titre</label>
                            </td>
                            <td>
                                <select id="couleurLogo" class="form-control" name="Glissiere[couleurLogo]">
                                    <option <?php if (isset($objGlissiere->couleurLogo) && $objGlissiere->couleurLogo == 'n') {
                                        echo 'selected';
                                    } ?> value="n">Noir
                                    </option>
                                    <option <?php if (isset($objGlissiere->couleurLogo) && $objGlissiere->couleurLogo == 'b') {
                                        echo 'selected';
                                    } ?> value="b">Blanc
                                    </option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label for="couleurmenu">Couleur texte menu</label>
                            </td>
                            <td>
                                <select id="couleurmenu" class="form-control" name="Glissiere[couleurTexteMenu]">
                                    <option <?php if (isset($objGlissiere->couleurTexteMenu) && $objGlissiere->couleurTexteMenu == 'n') {
                                        echo 'selected';
                                    } ?> value="n">Noir
                                    </option>
                                    <option <?php if (isset($objGlissiere->couleurTexteMenu) && $objGlissiere->couleurTexteMenu == 'b') {
                                        echo 'selected';
                                    } ?> value="b">Blanc
                                    </option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label for="nomducommeranct">Nom du commercant</label>
                            </td>
                            <td>
                                <select id="nomducommeranct" class="form-control" name="Glissiere[affichageNomCommercant]">
                                    <option <?php if (isset($objGlissiere->affichageNomCommercant) && $objGlissiere->affichageNomCommercant == 'o') {
                                        echo 'selected';
                                    } ?> value="o">Oui
                                    </option>
                                    <option <?php if (isset($objGlissiere->affichageNomCommercant) && $objGlissiere->affichageNomCommercant == 'n') {
                                        echo 'selected';
                                    } ?> value="n">Non
                                    </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="Transparent_du_banniere">Transparence du banniere</label>
                            </td>
                            <td>
                                <!--  <input type="number" name="Glissiere[Transparent_du_banniere]" min="0" max="1">-->
                                <input value="<?php if (isset($objGlissiere->Transparent_du_banniere)){echo $objGlissiere->Transparent_du_banniere; } ?>" type="range" min="0" max="1" name="Glissiere[Transparent_du_banniere]" step="0.1" list="steplist">
                                <datalist id="steplist">
                                    <option>0.1</option>
                                    <option>0.2</option>
                                    <option>0.3</option>
                                    <option>0.4</option>
                                    <option>0.5</option>
                                    <option>0.6</option>
                                    <option>0.7</option>
                                    <option>0.8</option>
                                    <option>0.9</option>
                                    <option>1</option>
                                </datalist>
                            </td>
                        </tr>
                        <?php if ($user_groups->id =='5'){ ?>
                        <tr>
                            <td>
                                <label for="is_actif_livre_or">Activer le system Livre d'or:</label>
                            </td>
                            <td>
                                <!--  <input type="number" name="Glissiere[Transparent_du_banniere]" min="0" max="1">-->
                                <select  class="form-control" name="Glissiere[is_actif_livre_or]" id="is_actif_livre_or">
                                    <option value="0">Non</option>
                                    <option value="1" <?php if (isset($objGlissiere->is_actif_livre_or) AND $objGlissiere->is_actif_livre_or==1 ){echo "selected"; } ?>>Oui</option>
                                </select>
                            </td>
                        </tr>
                    <?php } ?>
                    <?php } ?>
                </table>
            </div>
        </div>
    <?php } ?>
<?php } ?>





<?php if (isset($user_groups) && $user_groups->id == '5') { ?>
    <div id="div_pro_logo">
        <div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px;">Votre Logo</div>
        <div class="container-fluid">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">
                <tr>
                    <td class="stl_long_input_platinum_td">
                        Int&eacute;gration de votre logo
                        <p style='font-family:"Arial",sans-serif;font-size: 9px;line-height: 1.27em;'>
                            (la
                            largeur ne doit pas dépasser 270 pixels, fond transparent)</p>
                    </td>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div id="ArticleLogo_container">
                                                <?php
                                                $path_img_gallery_lg = 'application/resources/front/photoCommercant/imagesbank/' . $objCommercant->user_ionauth_id . '/lg/';
                                                $path_img_gallery_old = 'application/resources/front/photoCommercant/images/';
                                                ?>
                                                <a href='javascript:void(0);' title="Photo1"
                                                    <?php if (empty($objCommercant->Logo) || empty($objCommercant->Logo == "")) { ?>
                                                        onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-lg-Logo"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                                    <?php } ?>
                                                   class="btn btn-info" id="ArticleLogo_link">Ajouter
                                                    une
                                                    image</a>
                                                <a href="javascript:void(0);" class="btn btn-danger"
                                                    <?php if (!empty($objCommercant->Logo) && $objCommercant->Logo != "") { ?>
                                                        onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Logo');"
                                                    <?php } ?>
                                                >Supprimer</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <?php
                                            if (file_exists($path_img_gallery_lg . $objCommercant->Logo) && $objCommercant->Logo != "") {
                                                echo '<img style="max-width:100%" src="' . base_url() . $path_img_gallery_lg . $objCommercant->Logo . '" "/>';
                                            } else if (file_exists($path_img_gallery_old . $objCommercant->Logo) && $objCommercant->Logo != "") {
                                                echo '<img style="max-width:100%" src="' . base_url() . $path_img_gallery_old . $objCommercant->Logo . '" "/>';
                                            }
                                            ?>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <script type="text/javascript">
                    var maxLength = 100;

                    function fn_titre_entete() {
                        var length = jQuery('#titre_entete').val().length;
                        var length = maxLength - length;
                        jQuery('#chars_titre_entete').text(length);
                    }
                </script>

                <tr>
                    <td class="stl_long_input_platinum_td">
                        <label class="label">D&eacute;signation<br/>de votre activit&eacute; : </label>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-lg-12">
                                                        <textarea name="Societe[titre_entete]" id="titre_entete"
                                                                  class="stl_long_input_platinum form-control" maxlength="100"
                                                                  onkeyup="javascript:fn_titre_entete();"
                                                                  style="height:30px;margin-top:15px;"><?php if (isset($objCommercant->titre_entete)) echo htmlspecialchars($objCommercant->titre_entete); ?></textarea>
                                            <br/><span id="chars_titre_entete">100</span> caract&eacute;res
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <?php if ($user_groups->id == '5') { ?>
                    <tr>
                        <td>
                            <label for="place_logo">Position du logo</label>
                        </td>
                        <td>
                            <select class="form-control" name="Glissiere[place_logo]">
                                <option <?php if (isset($objGlissiere->place_logo) && $objGlissiere->place_logo == 'g') {
                                    echo 'selected';
                                } ?> value="g">Placé à gauche
                                </option>
                                <option <?php if (isset($objGlissiere->place_logo) && $objGlissiere->place_logo == 'c') {
                                    echo 'selected';
                                } ?> value="c">Placé au centre
                                </option>
                                <option <?php if (isset($objGlissiere->place_logo) && $objGlissiere->place_logo == 'd') {
                                    echo 'selected';
                                } ?> value="d">Placé à droite
                                </option>
                            </select>
                        </td>
                    </tr>

                <?php } ?>


            </table>
        </div>
    </div>
<?php } ?>



<?php if (isset($page_data) && $page_data == 'coordonnees') echo '</div>'; ?>




<?php if (isset($page_data) && $page_data == 'contenus') echo '<div style="display:none;">'; ?>
<div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px; margin-bottom:0 !important;">
    Les coordonnées de votre établissement
</div>
<?php if (isset($page_data) && $page_data == 'contenus') echo '</div>'; ?>



<?php if (isset($page_data) && $page_data == 'contenus') echo '<div style="display:none;">'; ?>
<div style="width: 100%;background: #E1E1E1;">
    <div class="p-3 mr-auto ml-auto" style=" width: 75%; background: #E1E1E1;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0"
               style="padding-left:10px; background-color:#E1E1E1;">


            <?php if (isset($objAbonnementCommercant) && $user_groups->id == '4') { ?>

                <script type="text/javascript">
                    var maxLength = 100;

                    function fn_titre_entete() {
                        var length = jQuery('#titre_entete').val().length;
                        var length = maxLength - length;
                        jQuery('#chars_titre_entete').text(length);
                    }
                </script>

                <tr>
                    <td class="stl_long_input_platinum_td">
                        <label class="label">D&eacute;signation<br/>de votre activit&eacute; : </label>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-lg-12">
                                                            <textarea name="Societe[titre_entete]" id="titre_entete"
                                                                      class="stl_long_input_platinum form-control"
                                                                      maxlength="100" onkeyup="javascript:fn_titre_entete();"
                                                                      style="height:40px;margin-top:15px;"><?php if (isset($objCommercant->titre_entete)) echo htmlspecialchars($objCommercant->titre_entete); ?></textarea>
                                            <br/><span id="chars_titre_entete">100</span> caract&eacute;res
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

            <?php } ?>


            <tr>
                <td class="stl_long_input_platinum_td">
                    <label class="label">Rubrique : </label>
                </td>
                <td>
                    <select name="AssCommercantRubrique[IdRubrique]" onchange="javascript:listeSousRubrique();"
                            id="RubriqueSociete" class="form-control stl_long_input_platinum">
                        <option value="0">-- Veuillez choisir --</option>
                        <?php if (sizeof($colRubriques)) { ?>
                            <?php foreach ($colRubriques as $objRubrique) { ?>
                                <option
                                    <?php if (isset($objAssCommercantRubrique[0]->IdRubrique) && $objAssCommercantRubrique[0]->IdRubrique == $objRubrique->IdRubrique) { ?>selected="selected"<?php } ?>
                                    value="<?php echo $objRubrique->IdRubrique; ?>"><?php echo $objRubrique->Nom; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </td>
            </tr>
            <tr id='trReponseRub'>
                <td class="stl_long_input_platinum_td">
                    <label class="label">Sous-rubrique : </label>
                </td>
                <td>

                    <?php if (isset($objAssCommercantRubrique[0]->IdRubrique) && $objAssCommercantRubrique[0]->IdRubrique != 0 && $objAssCommercantRubrique[0]->IdRubrique != "0" && $objAssCommercantRubrique[0]->IdRubrique != "" && $objAssCommercantRubrique[0]->IdRubrique != NULL) { ?>

                        <?php
                        $this->load->model("SousRubrique");
                        $colSousRubriques = $this->SousRubrique->GetByRubrique($objAssCommercantRubrique[0]->IdRubrique);
                        ?>

                        <select name="AssCommercantSousRubrique[IdSousRubrique]" id="SousRubriqueSociete"
                                class="form-control stl_long_input_platinum">
                            <option value="0">-- Veuillez choisir --</option>
                            <?php if (sizeof($colSousRubriques)) { ?>
                                <?php foreach ($colSousRubriques as $objSousRubrique) { ?>
                                    <option
                                        <?php if (isset($objAssCommercantSousRubrique[0]->IdSousRubrique) && $objAssCommercantSousRubrique[0]->IdSousRubrique == $objSousRubrique->IdSousRubrique) { ?>selected="selected"<?php } ?>
                                        value="<?php echo $objSousRubrique->IdSousRubrique; ?>"><?php echo $objSousRubrique->Nom; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>

                    <?php } else { ?>

                        <select name="AssCommercantSousRubrique[IdSousRubrique]" id="SousRubriqueSociete"
                                class="form-control stl_long_input_platinum">
                            <option value="0">-- Veuillez choisir --</option>
                            <?php if (sizeof($colSousRubriques)) { ?>
                                <?php foreach ($colSousRubriques as $objSousRubrique) { ?>
                                    <option
                                        <?php if (isset($objAssCommercantSousRubrique[0]->IdSousRubrique) && $objAssCommercantSousRubrique[0]->IdSousRubrique == $objSousRubrique->IdSousRubrique) { ?>selected="selected"<?php } ?>
                                        value="<?php echo $objSousRubrique->IdSousRubrique; ?>"><?php echo $objSousRubrique->Nom; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>

                    <?php } ?>
                </td>
            </tr>


            <script type="text/javascript">
                var maxLength_st = 35;

                function fn_NomSociete() {
                    var length = jQuery('#NomSociete').val().length;
                    var length = maxLength_st - length;
                    jQuery('#chars_NomSociete').text(length);
                }
            </script>

            <tr>
                <td class="stl_long_input_platinum_td">
                    <label class="label">Nom de la soci&eacute;t&eacute; : </label>
                </td>
                <td>
                                        <textarea name="Societe[NomSociete]" id="NomSociete" class="form-control stl_long_input_platinum"
                                                  maxlength="35" onkeyup="javascript:fn_NomSociete();"
                                                  style="height:30px;margin-top:15px;"><?php if (isset($objCommercant->NomSociete)) echo htmlspecialchars($objCommercant->NomSociete); ?></textarea>
                    <br/><span id="chars_NomSociete">35</span> caract&eacute;res
                </td>
            </tr>


            <?php if (isset($user_groups) && ($user_groups->id == '5' || $user_groups->id == '4')) { ?>
                <tr>
                    <td class="stl_long_input_platinum_td">
                        <label class="label">Statut : </label>
                    </td>
                    <td>
                        <input type="text" name="Societe[statut]" id="statutSociete"
                               value="<?php if (isset($objCommercant->statut)) echo htmlspecialchars($objCommercant->statut); ?>"
                               class="form-control stl_long_input_platinum"/>
                    </td>
                </tr>
                <tr>
                    <td class="stl_long_input_platinum_td">
                        <label class="label">Capital : </label>
                    </td>
                    <td>
                        <input type="text" name="Societe[capital]" id="capitalSociete"
                               value="<?php if (isset($objCommercant->capital)) echo htmlspecialchars($objCommercant->capital); ?>"
                               class="form-control stl_long_input_platinum"/>
                    </td>
                </tr>
            <?php } ?>
            <tr>
                <td class="stl_long_input_platinum_td">
                    <label class="label">Siret : </label>
                </td>
                <td>
                    <input type="text" name="Societe[Siret]" id="SiretSociete"
                           value="<?php if (isset($objCommercant->Siret)) echo htmlspecialchars($objCommercant->Siret); ?>"
                           class="form-control stl_long_input_platinum"/>
                </td>
            </tr>
            <tr>
                <td class="stl_long_input_platinum_td">
                    <label class="label">Code APE : </label>
                </td>
                <td>
                    <input type="text" name="Societe[code_ape]" id="CodeApeSociete"
                           value="<?php if (isset($objCommercant->code_ape)) echo htmlspecialchars($objCommercant->code_ape); ?>"
                           class="form-control stl_long_input_platinum"/>
                </td>
            </tr>
            <tr>
                <td class="stl_long_input_platinum_td">
                    <label class="label">Adresse de l'&eacute;tablissement : </label>
                </td>
                <td>
                    <input type="text" name="Societe[Adresse1]" id="Adresse1Societe"
                           value="<?php if (isset($objCommercant->Adresse1)) echo htmlspecialchars($objCommercant->Adresse1); ?>"
                           class="form-control stl_long_input_platinum"/>
                </td>
            </tr>
            <tr>
                <td class="stl_long_input_platinum_td">
                    <label class="label">Compl&eacute;ment d'Adresse : </label>
                </td>
                <td>
                    <input type="text" name="Societe[Adresse2]" id="Adresse2Societe"
                           value="<?php if (isset($objCommercant->Adresse2)) echo htmlspecialchars($objCommercant->Adresse2); ?>"
                           class="form-control stl_long_input_platinum"/>
                </td>
            </tr>

            <tr>
                <td class="stl_long_input_platinum_td">
                    <label class="label">Code postal : </label>
                </td>
                <td id="trReponseVille">
                    <input type="text" name="Societe[CodePostal]" id="CodePostalSociete"
                           value="<?php if (isset($objCommercant->CodePostal)) echo htmlspecialchars($objCommercant->CodePostal); ?>"
                           class="form-control stl_long_input_platinum"
                           onblur="javascript:CP_getDepartement();CP_getVille();"/>
                </td>
            </tr>

            <tr>
                <td class="stl_long_input_platinum_td">
                    <label class="label">Departement : </label>
                </td>
                <td>
                    <span id="departementCP_container">
                    <select name="Societe[departement_id]" id="departement_id" class="form-control stl_long_input_platinum"
                            onchange="javascript:CP_getVille_D_CP();">
                        <option value="0">-- Choisir --</option>
                        <?php if (sizeof($colDepartement)) { ?>
                            <?php foreach ($colDepartement as $objDepartement) { ?>
                                <option
                                    <?php if (isset($objCommercant->departement_id) && $objCommercant->departement_id == $objDepartement->departement_id) { ?>selected="selected"<?php } ?>
                                    value="<?php echo $objDepartement->departement_id; ?>"><?php echo $objDepartement->departement_nom; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                    </span>
                </td>
            </tr>
            <tr>
                <td class="stl_long_input_platinum_td">
                    <label class="label">Ville : </label>
                </td>
                <td>
                    <span id="villeCP_container">
                    <input type="text"
                           value="<?php if (isset($objCommercant->IdVille)) echo $this->mdlville->getVilleById($objCommercant->IdVille)->Nom; ?>"
                           name="IdVille_Nom_text" id="IdVille_Nom_text" class="form-control" disabled="disabled" style="width: 413px;"/>
                    <input type="hidden" value="<?php if (isset($objCommercant->IdVille)) echo $objCommercant->IdVille; ?>"
                           name="Societe[IdVille]" id="VilleSociete" class="form-control"/>
                    </span>
                </td>
            </tr>
            <tr>
                <td class="stl_long_input_platinum_td">
                    <label class="label">Latitude : (ex: -18.7972)</label>
                </td>
                <td>
                    <input type="text" name="Societe[latitude]" id="latitudeSociete"
                           value="<?php if (isset($objCommercant->latitude)) echo $objCommercant->latitude; ?>"
                           class="form-control stl_long_input_platinum"/>
                </td>
            </tr>
            <tr>
                <td class="stl_long_input_platinum_td">
                    <label class="label">Longitude : (ex: 47.4761)</label>
                </td>
                <td>
                    <input type="text" name="Societe[longitude]" id="longitudeSociete"
                           value="<?php if (isset($objCommercant->longitude)) echo htmlspecialchars($objCommercant->longitude); ?>"
                           class="form-control stl_long_input_platinum"/>
                </td>
            </tr>


            <tr>
                <td class="stl_long_input_platinum_td">
                    <label class="label">T&eacute;l&eacute;phone fixe : </label>
                </td>
                <td>
                    <input type="text" name="Societe[TelFixe]" id="TelFixeSociete"
                           value="<?php if (isset($objCommercant->TelFixe)) echo htmlspecialchars($objCommercant->TelFixe); ?>"
                           class="form-control stl_long_input_platinum"/>
                </td>
            </tr>
            <tr>
                <td class="stl_long_input_platinum_td">
                    <label class="label">T&eacute;l&eacute;phone mobile : </label>
                </td>
                <td>
                    <input type="text" name="Societe[TelMobile]" id="TelMobileSociete"
                           value="<?php if (isset($objCommercant->TelMobile)) echo htmlspecialchars($objCommercant->TelMobile); ?>"
                           class="form-control stl_long_input_platinum"/>
                </td>
            </tr>
            <tr>
                <td class="stl_long_input_platinum_td">
                    <label class="label">T&eacute;l&eacute;phone Fax : </label>
                </td>
                <td>
                    <input type="text" name="Societe[fax]" id="faxSociete"
                           value="<?php if (isset($objCommercant->fax)) echo htmlspecialchars($objCommercant->fax); ?>"
                           class="form-control stl_long_input_platinum"/>
                </td>
            </tr>
            <tr>
                <td class="stl_long_input_platinum_td">
                    <label class="label">Courriel : </label>
                </td>
                <td>
                    <input type="text" name="Societe[Email]" id="EmailSociete"
                           value="<?php if (isset($objCommercant->Email)) echo htmlspecialchars($objCommercant->Email); ?>"
                           class="form-control stl_long_input_platinum"/>
                    <div class="FieldError" id="divErrorEmailSociete"></div>
                </td>
            </tr>
            <tr>
                <td class="stl_long_input_platinum_td">
                    <label class="label">Site internet : </label>
                </td>
                <td>
                    <div style="font-size:10px; margin-top:15px;">Exemple: http://www.votresite.com/</div>
                    <input type="text" name="Societe[SiteWeb]" id="SiteWeb"
                           value="<?php if (isset($objCommercant->SiteWeb) && $objCommercant->SiteWeb != "") echo htmlspecialchars($objCommercant->SiteWeb); else echo 'http://www.'; ?>"
                           class="form-control stl_long_input_platinum"/>
                </td>
            </tr>
            <tr>
                <td class="stl_long_input_platinum_td">
                    <label class="label">URL Espace marchand : </label>
                </td>
                <td>
                    <div style="font-size:10px; margin-top:15px;">Exemple: http://www.votresite.com/</div>
                    <input type="text" name="Societe[page_web_marchand]" id="SiteWeb"
                           value="<?php if (isset($objCommercant->page_web_marchand) && $objCommercant->page_web_marchand != "") echo htmlspecialchars($objCommercant->page_web_marchand); else echo 'http://www.'; ?>"
                           class="form-control stl_long_input_platinum"/>
                </td>
            </tr>

            <?php if (isset($user_groups) && ($user_groups->id == '5' || $user_groups->id == '4')) { ?>

                <tr>
                    <td class="stl_long_input_platinum_td">
                        <label class="label">Lien Facebook : </label>
                    </td>
                    <td>
                        <div style="font-size:10px; margin-top:15px;">Exemple:
                            https://www.facebook.com/sortez.org
                        </div>
                        <input type="text" name="Societe[Facebook]" id="FacebookSociete"
                               value="<?php if (isset($objCommercant->Facebook)) echo htmlspecialchars($objCommercant->Facebook); ?>"
                               class="form-control stl_long_input_platinum"/>
                    </td>
                </tr>
                <tr>
                    <td class="stl_long_input_platinum_td">
                        <label class="label">Lien Twitter : </label>
                    </td>
                    <td>
                        <div style="font-size:10px; margin-top:15px;">Exemple:
                            https://www.twitter.com/sortez_org
                        </div>
                        <input type="text" name="Societe[google_plus]" id="google_plusSociete"
                               value="<?php if (isset($objCommercant->google_plus)) echo htmlspecialchars($objCommercant->google_plus); ?>"
                               class="form-control stl_long_input_platinum"/>
                    </td>
                </tr>
                <tr>
                    <td class="stl_long_input_platinum_td">
                        <!-- <label class="label">Lien Google + : </label> -->
                        <label class="label">Lien Instagram  : </label>

                    </td>
                    <td>
                        <div style="font-size:10px; margin-top:15px;"><!-- Exemple:
                            https://plus.google.com/sortez_org -->Exemple:
                            https://www.instagram.com/sortez_org
                        </div>
                        <input type="text" name="Societe[Twitter]" id="TwitterSociete"
                               value="<?php if (isset($objCommercant->Twitter)) echo htmlspecialchars($objCommercant->Twitter); ?>"
                               class="form-control stl_long_input_platinum"/>
                    </td>
                </tr>

            <?php } ?>

            <?php if (isset($user_groups) && $user_groups->id == '4') { ?><!--Horaires affichés en Premium-->
            <tr>
                <td class="stl_long_input_platinum_td" colspan="2">
                    <label class="label">Heures et jours d’ouvertures : </label>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="pl-1">
                                        <textarea name="Societe[Horaires]"
                                                  id="HorairesSociete"><?php if (isset($objCommercant->Horaires)) echo htmlspecialchars($objCommercant->Horaires); ?></textarea>
                    <script>

                        CKEDITOR.replace('HorairesSociete');

                    </script>
                </td>
            </tr>
        <?php } ?>

        </table>
    </div>
</div>



<?php if (isset($page_data) && $page_data == 'contenus') echo '</div>'; ?>




<?php if (isset($page_data) && $page_data == 'contenus') echo '<div style="display:none;">'; ?>

<?php if (isset($user_groups) && ($user_groups->id == '5' OR $user_groups->id == '4')) { ?>

    <div id="div_pro_multimedia" style="background-color: rgb(225, 225, 225)">
        <div class="div_stl_long_platinum">Integration Multimedia</div>
        <div class="container-fluid">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">
                <tr>
                    <td class="stl_long_input_platinum_td">
                        <label class="label">Lien vid&eacute;o : </label>
                    </td>
                    <td>
                        <span style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">&nbsp;&nbsp;&nbsp;Format : http://www.youtube.com/watch?v=gvGymDhY49E </span><br/>
                        <input type="text" name="Societe[Video]" id="VideoSociete"
                               value="<?php if (isset($objCommercant->Video)) echo htmlspecialchars($objCommercant->Video); ?>"
                               class="stl_long_input_platinum form-control"/>
                    </td>
                </tr>
                <tr valign="middle">
                    <td class="stl_long_input_platinum_td" valign="top">
                        <label class="label">Document PDF : <br/>(max 3MB)</label>
                    </td>
                    <td valign="top">
                        <?php if (!empty($objCommercant->Pdf)) { ?>
                            <?php //echo $objCommercant->Pdf; ?>

                            <?php if ($objCommercant->Pdf != "" && $objCommercant->Pdf != NULL && file_exists("application/resources/front/photoCommercant/images/" . $objCommercant->Pdf) == true) { ?>
                            <a
                                href="<?php echo base_url(); ?>application/resources/front/photoCommercant/images/<?php echo $objCommercant->Pdf; ?>"
                                target="_blank"><img
                                    src="<?php echo base_url(); ?>application/resources/front/images/pdf_icone.png"
                                    width="64" alt="PDF"/></a><?php } ?>


                            <a href="javascript:void(0);"
                               onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Pdf');"
                               class="btn btn-danger">Supprimer</a>
                            <span class="loading_Pdf"></span>
                            <br/>

                            <input type="file" name="SocietePdf" id="PdfSociete" value=""
                                   style="display:none;" class="stl_long_input_platinum form-control"/>
                            <input type="hidden" name="PdfSociete_checker" id="PdfSociete_checker"
                                   value=""/>
                        <?php } else { ?>
                            <input type="file" name="SocietePdf" id="PdfSociete" value=""
                                   class="stl_long_input_platinum form-control"/>
                            <input type="hidden" name="PdfSociete_checker" id="PdfSociete_checker"
                                   value=""/>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td class="stl_long_input_platinum_td">&nbsp;

                    </td>
                    <td>
                        <span style="font-size:11px"> Préciser le titre de votre document : ex : plaquette commerciale</span><br/>
                        <input type="text" name="Societe[titre_Pdf]" id="titre_PdfSociete"
                               value="<?php if (isset($objCommercant->titre_Pdf)) echo htmlspecialchars($objCommercant->titre_Pdf); ?>"
                               class="stl_long_input_platinum form-control"/>
                    </td>
                </tr>
            </table>
        </div>
        <div class="container-fluid">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;margin-top: 25px">

                <tr>
                    <td class="stl_long_input_platinum_td" style="vertical-align: top;">
                        <label style="font-size:14px;">Importer votre vidéo : </label>
                    </td>
                    <td>
                        <div class="row pb-3">
                            <div class="col-lg-12">
                                <?php
                                $thiss = get_instance();
                                $video_background = $thiss->load->mdlcommercant->get_video_bg_by_idcom($objCommercant->IdCommercant);
                                $path_video_gallery_bg = 'application/resources/front/photoCommercant/imagesbank/' . $objCommercant->user_ionauth_id . '/bgvideo/';
                                ?>

                                <div id="Articlebackground_video_container">

                                    <?php if (empty($video_background->background_video) || $video_background->background_video == "") { ?>
                                        <a href='javascript:void(0);' title="video" class="btn btn-info" id="Articlebackground_link_video"
                                           onclick='javascript:window.open("<?php echo site_url("front/professionnels/media_video_send/" . $objCommercant->IdCommercant . "-bgvideo-background_video"); ?>", "Commercant video", "width=1045, height=500, scrollbars=yes");'
                                        >Ajouter une vidéo</a>
                                    <?php } ?>
                                    <?php if (!empty($video_background->background_video)) { ?>
                                        <a href="javascript:void(0);" class="btn btn-danger"
                                           onclick="suppression_video('<?php echo $video_background->IdCommercant; ?>','<?php echo $video_background->background_video; ?>');"
                                        >Supprimer</a>
                                        <script type="text/javascript">
                                            function suppression_video(idcom,filename){
                                                var datas = 'idcom='+idcom+'&filename='+filename;
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo site_url('front/professionnels/suppression_video'); ?>",
                                                    data: datas,
                                                    success: function(response){
                                                        //$('#loading_file').css("display", "none");
                                                        if(response == '1'){
                                                            alert('Votre vidéo a été supprimé!');
                                                            location.reload();
                                                        }else{
                                                            alert('erreur de suppression');
                                                        }
                                                        console.log(response);
                                                    },
                                                });
                                            }
                                        </script>
                                    <?php } ?>

                                </div>

                                <?php
                                if (file_exists($path_video_gallery_bg . $video_background->background_video) && isset($video_background->background_video) && $video_background->background_video != "") {
                                    ?>
                                    <video width="500" height="300" controls>
                                        <source src="<?php echo base_url() . $path_video_gallery_bg . $video_background->background_video ?>" type="<?php echo $video_background->Type; ?>">
                                    </video>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>


                    </td>
                </tr>
            </table>
        </div>
    </div>

<?php } ?>
<?php if (isset($user_groups) && ($user_groups->id == '5' OR $user_groups->id == '4')) { ?>


    <style type="text/css">
        #table_geolocalisation td {
            padding: 0 15px !important;
        }
    </style>

    <div id="div_pro_geolocalisation">
        <div class="div_stl_long_platinum" style="margin-bottom:0 !important;">G&eacute;olocalisation
        </div>
        <table width="100%" border="0" id="table_geolocalisation" cellspacing="0" cellpadding="0"
               style="padding-left:10px; background-color:#E1E1E1;">

            <tr>
                <td colspan="2" style="padding: 15px !important;">
                    <strong>Mon adresse de géolocalisation</strong><br/>La redirection se fera sur
                    l’adresse précisée sur le module « Localisation » voir ci-dessous
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="checkbox" name="adresse_localisation_diffuseur_checkbox"
                           id="adresse_localisation_diffuseur_checkbox"
                           <?php if (isset($objCommercant->adresse_localisation_diffuseur) && $objCommercant->adresse_localisation_diffuseur == "1") { ?>checked<?php } ?>/>
                    <input type="hidden" name="Societe[adresse_localisation_diffuseur]"
                           id="adresse_localisation_diffuseur"
                           value="<?php if (isset($objCommercant->adresse_localisation_diffuseur)) echo htmlspecialchars($objCommercant->adresse_localisation_diffuseur); ?>">
                    L’adresse est celle précisée par le diffuseur
                </td>
            </tr>
            <tr>
                <td class="stl_long_input_platinum_td">
                    <label class="label">Adresse : </label>
                </td>
                <td>
                    <input type="text" name="Societe[adresse_localisation]"
                           id="adresse_localisationSociete"
                           value="<?php if (isset($objCommercant->adresse_localisation)) echo $objCommercant->adresse_localisation; ?>"
                           class="stl_long_input_platinum form-control"
                           onChange="getLatitudeLongitudeLocalisation();"/>
                </td>
            </tr>
            <tr>
                <td class="stl_long_input_platinum_td">
                    <label class="label">Code Postal : </label>
                </td>
                <td id="trReponseVille_localisation">
                    <input type="text" name="Societe[codepostal_localisation]"
                           id="codepostal_localisationSociete"
                           value="<?php if (isset($objCommercant->codepostal_localisation)) echo $objCommercant->codepostal_localisation; ?>"
                           class="stl_long_input_platinum form-control"
                           onblur="javascript:CP_getVille_localisation();"/>
                </td>
            </tr>
            <tr>
                <td class="stl_long_input_platinum_td">
                    <label class="label">Ville : </label>
                </td>
                <td>
                    <span id="villeCP_container_localisation">

                    <input type="hidden"
                           value="<?php if (isset($objCommercant->IdVille_localisation)) echo $objCommercant->IdVille_localisation; ?>
            " name="Societe[IdVille_localisation]" id="IdVille_localisationSociete"/>

                    <input type="text"
                           value="<?php if (isset($this->mdlville->getVilleById($objCommercant->IdVille_localisation)->Nom)) echo $this->mdlville->getVilleById($objCommercant->IdVille_localisation)->Nom; ?>
            " name="Societe_IdVille_localisation" class="form-control" id="Societe_IdVille_localisation" disabled="disabled"
                           style="width:413px;"/>

                    </span>
                </td>
            </tr>

            <tr>
                <td colspan="2" style="text-align:center;">
                    <a href="javascript:void(0);" onclick="javascript:open_map_localisation();"><img
                            src="<?php echo GetImagePath("privicarte/"); ?>/img_localisation_detailagenda.png"
                            alt="localisation"/></a>
                </td>
            </tr>


        </table>
    </div>
<?php } ?>
<div style="background-color:rgb(54, 83, 163)" class="div_stl_long_platinum">Les coordonnées du décideur</div>
<div class="p-3" style="background-color: rgb(225, 225, 225)">
    <table class="p-3" width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">
        <tr>
            <td class="stl_long_input_platinum_td">
                <label class="label">Civilit&eacute; : </label>
            </td>
            <td>
                <select name="Societe[Civilite]" id="CiviliteResponsableSociete"
                        class="form-control stl_long_input_platinum">
                    <option
                        <?php if (isset($objCommercant->Civilite) && $objCommercant->Civilite == "0") { ?>selected="selected"<?php } ?>
                        value="0">Monsieur
                    </option>
                    <option
                        <?php if (isset($objCommercant->Civilite) && $objCommercant->Civilite == "1") { ?>selected="selected"<?php } ?>
                        value="1">Madame
                    </option>
                    <option
                        <?php if (isset($objCommercant->Civilite) && $objCommercant->Civilite == "2") { ?>selected="selected"<?php } ?>
                        value="2">Mademoiselle
                    </option>
                </select>
            </td>
        </tr>
        <tr>
            <td class="stl_long_input_platinum_td">
                <label class="label">Nom : </label>
            </td>
            <td>
                <input type="text" name="Societe[Nom]" id="NomResponsableSociete"
                       value="<?php if (isset($objCommercant->Nom)) echo htmlspecialchars($objCommercant->Nom); ?>"
                       class="form-control stl_long_input_platinum"/>
            </td>
        </tr>
        <tr>
            <td class="stl_long_input_platinum_td">
                <label class="label">Pr&eacute;nom : </label>
            </td>
            <td>
                <input type="text" name="Societe[Prenom]" id="PrenomResponsableSociete"
                       value="<?php if (isset($objCommercant->Prenom)) echo htmlspecialchars($objCommercant->Prenom); ?>"
                       class="form-control stl_long_input_platinum"/>
            </td>
        </tr>
        <tr>
            <td class="stl_long_input_platinum_td">
                <label class="label">Fonction <span class="indication">(G&eacute;rant, Directeur ...)</span>:
                </label>
            </td>
            <td>
                <input type="text" name="Societe[Responsabilite]" id="ResponsabiliteResponsableSociete"
                       value="<?php if (isset($objCommercant->Responsabilite)) echo htmlspecialchars($objCommercant->Responsabilite); ?>"
                       class="form-control stl_long_input_platinum"/>
            </td>
        </tr>
        <tr>
            <td class="stl_long_input_platinum_td">
                <label class="label">T&eacute;l&eacute;phone direct <span class="indication">(fixe ou portable)</span>
                    : </label>
            </td>
            <td>
                <input type="text" name="Societe[TelDirect]" id="TelDirectResponsableSociete"
                       value="<?php if (isset($objCommercant->TelDirect)) echo htmlspecialchars($objCommercant->TelDirect); ?>"
                       class="form-control stl_long_input_platinum"/>
            </td>
        </tr>
        <?php if (isset($user_groups) && ($user_groups->id == '5' || $user_groups->id == '4')) { ?>
            <tr>
                <td class="stl_long_input_platinum_td">
                    <label class="label">Email Personnel : </label>
                </td>
                <td>
                    <input type="text" name="Societe[Email_decideur]" id="Email_decideurSociete"
                           value="<?php if (isset($objCommercant->Email_decideur)) echo htmlspecialchars($objCommercant->Email_decideur); ?>"
                           class="form-control stl_long_input_platinum"/>
                    <div class="FieldError" id="divErrorEmail_decideurSociete"></div>
                </td>
            </tr>
        <?php } ?>
    </table>
</div>

<div class="div_stl_long_platinum" style="margin-bottom:0 !important;">Votre abonnement</div>
<div class="p-3" style="background-color:#E1E1E1 ;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0"
           style="padding-left:10px; background-color:#E1E1E1;">

        <tr>
            <td class="label stl_long_input_platinum_td">
                Date de début
                :
            </td>
            <td>
                <?php if (isset($objAbonnementCommercant)) echo translate_date_to_fr($objAbonnementCommercant->DateDebut); ?>
            </td>
        </tr>
        <tr>
            <td class="label stl_long_input_platinum_td">Date de fin :</td>
            <td>
                <?php if (isset($objAbonnementCommercant)) echo translate_date_to_fr($objAbonnementCommercant->DateFin); ?>
            </td>
        </tr>
        <tr>
            <td class="label stl_long_input_platinum_td"><strong>Votre abonnement : </strong></td>
            <td>
                <strong><?php
                    //echo $user_groups->id;
                    if (isset($user_groups)) {
                        if ($user_groups->id == 3) echo "Basic";
                        if ($user_groups->id == 4) echo "Premium";
                        if ($user_groups->id == 5) echo "Platinium";
                    }
                    ?></strong>
            </td>
        </tr>
        <tr>
            <td>
                <label class="label">Identifiant : </label>
            </td>
            <td>
                <?php if (isset($objCommercant->Login)) echo htmlspecialchars($objCommercant->Login); ?>
                <div class="FieldError" id="divErrorLoginSociete"></div>
            </td>
        </tr>
<!--         <tr>
            <td colspan="2" style="text-align:center; padding:15px;">
                <button class="btn btn-primary"
                        onclick="javascript:window.location.href='<?php //echo site_url("front/professionnels/mon_inscription"); ?>';return false;">
                    D&eacute;tails de votre abonnement
                </button>
            </td>
        </tr> -->

                        <?php
                            if(isset($objCommercant->Email)){
                                $thisss = get_instance();
                                $thisss->load->model('mdlcommercant');
                                if(isset($objCommercant->Email)){
                                    $pwdClaire= $thisss->mdlcommercant->get_log_pwd_claire($objCommercant->Email);
                                    if($pwdClaire){

                        ?>
                                    <tr>
                                        <td>
                                            <label>Mot de passe :</label>
                                        </td>
                                        <td><?php echo($pwdClaire[0]->password); ?></td>
                                    </tr>
                                    <?php }; ?>
                                <?php //}else{
                                    //echo '' ;
                                }; ?>
                        <?php }; ?>               

    </table>
</div>

<?php if (isset($page_data) && $page_data == 'contenus') echo '</div>'; ?>


<?php if (isset($page_data) && $page_data == 'coordonnees') echo '<div style="display:none;">'; ?><!--START CONTAINER MON CONTENU-->

<?php if (isset($user_groups) && $user_groups->id == '5') { ?>
    <div id="div_pro_horaires">
        <div class="div_stl_long_platinum">Horaires</div>
        <div class="container-fluid">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">
                <tr>
                    <td class="stl_long_input_platinum_td" colspan="2">
                        <label class="label">Heures et jours d’ouvertures : </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="pl-1">
                                                <textarea name="Societe[Horaires]"
                                                          id="HorairesSociete"><?php if (isset($objCommercant->Horaires)) echo htmlspecialchars($objCommercant->Horaires); ?></textarea>
                        <script>

                            CKEDITOR.replace('HorairesSociete');

                        </script>
                    </td>
                </tr>
            </table>
        </div>
    </div>
<?php } ?>