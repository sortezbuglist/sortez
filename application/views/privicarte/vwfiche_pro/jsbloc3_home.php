<script type="text/javascript">
    jQuery(document).ready(function () {
        <?php if (isset($objGlissiere->is_activ_bloc3) AND $objGlissiere->is_activ_bloc3 == 1) {  ?>
        jQuery('#type_bloc3_choose').css("display", "block");
        <?php if (isset($objGlissiere->type_bloc3) AND $objGlissiere->type_bloc3 == '1' OR isset($objGlissiere->type_bloc3) AND $objGlissiere->type_bloc3 =='2'){  ?>
        document.getElementById('bloc_3_1').style.display = "block";
        document.getElementById('bloc_3_2').style.display = "none";
        document.getElementById('bloc_3_3').style.display = "none";
        document.getElementById('bonplan_config_3').style.display = "none";
        document.getElementById('fidelity_3_config').style.display = "none";
        $('#type_bloc3js').val("1");
        $("#bloc_3_1").removeClass("col-lg-6");
        $("#bloc_3_2").removeClass("col-lg-6");
        $("#bloc_3_1").removeClass("col-lg-4");
        $("#bloc_3_2").removeClass("col-lg-4");
        $("#bloc_3_3").removeClass("col-lg-4");
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc3) AND $objGlissiere->type_bloc3 == '2') {  ?>
        document.getElementById('bloc_3_1').style.display = "block";
        document.getElementById('bloc_3_2').style.display = "block";
        document.getElementById('bloc_3_3').style.display = "none";
        document.getElementById('bonplan_config_3').style.display = "none";
        document.getElementById('fidelity_3_config').style.display = "none";
        $('#type_bloc3js').val("2");
        $("#bloc_3_1").addClass("col-lg-6");
        $("#bloc_3_2").addClass("col-lg-6");
        $("#bloc_3_1").removeClass("col-lg-4");
        $("#bloc_3_2").removeClass("col-lg-4");
        $("#bloc_3_3").removeClass("col-lg-4");
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc3) AND $objGlissiere->type_bloc3 == '3') {  ?>
        document.getElementById('bloc_3_1').style.display = "block";
        document.getElementById('bloc_3_2').style.display = "block";
        document.getElementById('bloc_3_3').style.display = "block";
        document.getElementById('bonplan_config_3').style.display = "none";
        document.getElementById('fidelity_3_config').style.display = "none";
        $('#type_bloc3js').val("3");
        $("#bloc_3_1").addClass("col-lg-4");
        $("#bloc_3_2").addClass("col-lg-4");
        $("#bloc_3_3").addClass("col-lg-4");
        $("#bloc_3_1").removeClass("col-lg-6");
        $("#bloc_3_2").removeClass("col-lg-6");
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc3) AND $objGlissiere->type_bloc3 == '1bp') {  ?>
        document.getElementById('bonplan_config_3').style.display = "block";
        document.getElementById('bloc_3_1').style.display = "none";
        document.getElementById('bloc_3_2').style.display = "none";
        document.getElementById('bloc_3_3').style.display = "none";
        document.getElementById('fidelity_3_config').style.display = "none";
        $('#type_bloc3js').val("1bp");
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc3) AND $objGlissiere->type_bloc3 == '1fd') {  ?>
        document.getElementById('fidelity_3_config').style.display = "block";
        document.getElementById('bonplan_config_3').style.display = "none";
        document.getElementById('bloc_3_1').style.display = "none";
        document.getElementById('bloc_3_2').style.display = "none";
        document.getElementById('bloc_3_3').style.display = "none";
        $('#type_bloc3js').val("1fd");
        <?php } ?>
        <?php if (isset($objGlissiere->is_activ_btn_bloc1_3) && $objGlissiere->is_activ_btn_bloc1_3 == '1') {  ?>
        document.getElementById('link_bloc1_3').style.display = "block";
        <?php } ?>
        <?php if (isset($objGlissiere->is_activ_btn_bloc3_2) && $objGlissiere->is_activ_btn_bloc3_2 == '1') {  ?>
        document.getElementById('link_bloc3_2').style.display = "block";
        <?php } ?>
        <?php if (isset($objGlissiere->is_activ_btn_bloc3_3) && $objGlissiere->is_activ_btn_bloc3_3 == '1') {  ?>
        document.getElementById('link_bloc_3_3').style.display = "block";
        <?php } ?>
        <?php if (isset($objGlissiere->is_activ_btn_bp_3) && $objGlissiere->is_activ_btn_bp_3 == '1') {  ?>
        document.getElementById('link_bp_3').style.display = "block";
        <?php } ?>
        <?php if (isset($objGlissiere->is_activ_btn_fd_3) && $objGlissiere->is_activ_btn_fd_3 == '1') {  ?>
        document.getElementById('link_fd_3').style.display = "block";
        <?php } ?>
        <?php } ?>
        $("#is_activ_bloc3").change(function () {
            if (document.getElementById('is_activ_bloc3').value == 0) {
                jQuery('#type_bloc3_choose').css("display", "none");

                jQuery('#bloc_3_1').css("display", "none");

                jQuery('#bloc_3_2').css("display", "none");

                jQuery('#bloc_3_3').css("display", "none");

                jQuery('#bonplan_config_3').css("display", "none");

                jQuery('#fidelity_3_config').css("display", "none");
            }
            else if (document.getElementById('is_activ_bloc3').value == 1) {
                jQuery('#type_bloc3_choose').css("display", "block");
                jQuery('#bloc_3_1').css("display", "none");

                jQuery('#bloc_3_2').css("display", "none");

                jQuery('#bloc_3_3').css("display", "none");

                jQuery('#bonplan_config_3').css("display", "none");

                jQuery('#fidelity_3_config').css("display", "none");
            }
        });
        $("#1b3_1").click(function () {
            document.getElementById('bloc_3_1').style.display = "block";
            document.getElementById('bloc_3_2').style.display = "none";
            document.getElementById('bloc_3_3').style.display = "none";
            document.getElementById('bonplan_config_3').style.display = "none";
            document.getElementById('fidelity_3_config').style.display = "none";
            $('#type_bloc3js').val("1");
            $("#bloc_3_1").removeClass("col-lg-6");
            $("#bloc_3_2").removeClass("col-lg-6");
            $("#bloc_3_1").removeClass("col-lg-4");
            $("#bloc_3_2").removeClass("col-lg-4");
            $("#bloc_3_3").removeClass("col-lg-4");
        });
        $("#2b3_1").click(function () {
            document.getElementById('bloc_3_1').style.display = "block";
            document.getElementById('bloc_3_2').style.display = "block";
            document.getElementById('bloc_3_3').style.display = "none";
            document.getElementById('bonplan_config_3').style.display = "none";
            document.getElementById('fidelity_3_config').style.display = "none";
            $('#type_bloc3js').val("2");
            $("#bloc_3_1").addClass("col-lg-6");
            $("#bloc_3_2").addClass("col-lg-6");
            $("#bloc_3_1").removeClass("col-lg-4");
            $("#bloc_3_2").removeClass("col-lg-4");
            $("#bloc_3_3").removeClass("col-lg-4");
        });$("#3b3_1").click(function () {
            document.getElementById('bloc_3_1').style.display = "block";
            document.getElementById('bloc_3_2').style.display = "block";
            document.getElementById('bloc_3_3').style.display = "block";
            document.getElementById('bonplan_config_3').style.display = "none";
            document.getElementById('fidelity_3_config').style.display = "none";
            $('#type_bloc3js').val("3");
            $("#bloc_3_1").addClass("col-lg-4");
            $("#bloc_3_2").addClass("col-lg-4");
            $("#bloc_3_3").addClass("col-lg-4");
            $("#bloc_3_1").removeClass("col-lg-6");
            $("#bloc_3_2").removeClass("col-lg-6");
        });
        $("#1bp3_1").click(function () {
            document.getElementById('bonplan_config_3').style.display = "block";
            document.getElementById('bloc_3_1').style.display = "none";
            document.getElementById('bloc_3_2').style.display = "none";
            document.getElementById('bloc_3_3').style.display = "none";
            document.getElementById('fidelity_3_config').style.display = "none";
            $('#type_bloc3js').val("1bp");
        });
        $("#1fd3_1").click(function () {
            document.getElementById('fidelity_3_config').style.display = "block";
            document.getElementById('bonplan_config_3').style.display = "none";
            document.getElementById('bloc_3_1').style.display = "none";
            document.getElementById('bloc_3_2').style.display = "none";
            document.getElementById('bloc_3_3').style.display = "none";
            $('#type_bloc3js').val("1fd");
        });
        $("#link_btn_bloc1_3").change(function () {

            if (document.getElementById('link_btn_bloc1_3').value == 1) {
                document.getElementById('link_bloc1_3').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc1_3').value == 0) {
                document.getElementById('link_bloc1_3').style.display = "none";

            }

        });
        $("#link_btn_bloc3_2").change(function () {


            if (document.getElementById('link_btn_bloc3_2').value == 1) {
                document.getElementById('link_bloc3_2').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc3_2').value == 0) {
                document.getElementById('link_bloc3_2').style.display = "none";

            }

        }); $("#link_btn_bloc_3_3").change(function () {


            if (document.getElementById('link_btn_bloc_3_3').value == 1) {
                document.getElementById('link_bloc_3_3').style.display = "block";

            }
            else if (document.getElementById('link_btn_bloc_3_3').value == 0) {
                document.getElementById('link_bloc_3_3').style.display = "none";

            }

        });
        $("#link_btn_bp_3").change(function () {

            if (document.getElementById('link_btn_bp_3').value == 1) {
                document.getElementById('link_bp_3').style.display = "block";

            }
            else if (document.getElementById('link_btn_bp_3').value == 0) {
                document.getElementById('link_bp_3').style.display = "none";

            }

        });
        $("#link_btn_fd_3").change(function () {

            if (document.getElementById('link_btn_fd_3').value == 1) {
                document.getElementById('link_fd_3').style.display = "block";

            }
            else if (document.getElementById('link_btn_fd_3').value == 0) {
                document.getElementById('link_fd_3').style.display = "none";

            }

        });
    });
</script>