<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("privicarte/includes/header_mini_2", $data); ?>

    <style type="text/css">
        body {
            background-image: none !important;
            background-color: #ffffff !important;
        }
    </style>


<?php if (isset($_SESSION['pop_view']) && $_SESSION['pop_view'] == '1') {
    $_SESSION['pop_view'] = '0';
    ?>
    <script type="text/javascript">
        //$("a.fancybox-close", window.parent.document).click();
        window.top.window.$.fancybox.close();
        window.top.window.location = "<?php echo site_url("front/utilisateur/menuconsommateurs");?>";
    </script>
<?php } ?>


    <!--<link rel="stylesheet" href="inscriptionparticuliers_fichiers/wpstyles.css" type="text/css">-->
    <script type="text/javascript">var blankSrc = "wpscripts/blank.gif";</script>
    <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/fields.check.js"></script>
    <!--<link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/"); ?>/style.css" />-->
    <link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/"); ?>/blue/style.css"/>
    <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jquery.ui.datepicker.js"></script>
    <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jquery.ui.core.js"></script>
    <link rel="stylesheet" media="screen" type="text/css"
          href="<?php echo GetCssPath("front/"); ?>/demo_table_jui.css"/>
    <link rel="stylesheet" media="screen" type="text/css"
          href="<?php echo GetCssPath("front/"); ?>/jquery-ui-1.8.4.custom.css"/>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#btnSinscrire").click(function () {
                var txtError = "";

                var txtNom = jQuery("#txtNom").val();
                if (txtNom == "") {
                    txtError += "- Veuillez indiquer Votre nom <br/>";
                }

                /*var txtEmail = jQuery("#txtEmail").val();
                 if(!isEmail(txtEmail)) {
                 txtError += "- L'adresse mail n'est pas valide. Veuillez saisir un email valide <br/>";
                 }*/

                var txtEmail_verif = jQuery('#txtEmail_verif').val();
                if (txtEmail_verif == 1) {
                    txtError += "- Votre Email existe d�j� sur notre site <br/>";
                }

                // :Check if a city has been selected before validating
                var ivilleId = jQuery('#txtVille').val();
                if (ivilleId == 0) {
                    txtError += "- Vous devez s\350lectionner une ville <br/>";
                }

                var txtLogin = jQuery("#txtLogin").val();
                if (!isEmail(txtLogin)) {
                    txtError += "- Votre login doit &ecirc;tre un email valide <br/>";
                }

                var txtLogin_verif = jQuery('#txtLogin_verif').val();
                if (txtLogin_verif == 1) {
                    txtError += "- Votre Login existe d�j� sur notre site <br/>";
                }


                var validationabonnement = $('#validationabonnement').val();
                //alert("coche "+validationabonnement);
                if ($('#validationabonnement').is(":checked")) {
                } else {
                    txtError += "- Vous devez valider les conditions g�n�rales<br/>";
                }

                jQuery("#divErrortxtInscription").html(txtError);

                if (txtError == "") {
                    jQuery("#frmInscriptionParticulier").submit();
                }
            });


            jQuery("#txtLogin").blur(function () {
                //alert('cool');
                var txtLogin = jQuery("#txtLogin").val();
                var txtEmail = txtLogin;

                var value_result_to_show = "0";

                //alert('<?php //echo site_url("front/particuliers/verifier_login"); ?>' + '/' + txtLogin);
                //jQuery(".EmailLoading").addClass("EmailLoading_show").removeClass("EmailLoading");
                jQuery.post(
                    '<?php echo site_url("front/particuliers/verifier_login");?>',
                    {txtLogin_var: txtLogin},
                    function (zReponse) {
                        //alert (zReponse) ;
                        if (zReponse == "1") {
                            value_result_to_show = "1";
                        }


                        if (value_result_to_show == "1") {
                            result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
                            $('#divErrortxtLogin_').html(result_to_show);
                            $('#txtLogin_verif').val("1");
                        } else {
                            result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
                            $('#divErrortxtLogin_').html(result_to_show);
                            $('#txtLogin_verif').val("0");
                        }


                    });


                $.post(
                    '<?php echo site_url("front/professionnels/verifier_login_ionauth");?>',
                    {txtLogin_var_ionauth: txtLogin},
                    function (zReponse_ionauth) {
                        //alert (zReponse) ;
                        if (zReponse_ionauth == "1") {
                            value_result_to_show = "1";
                        }


                        if (value_result_to_show == "1") {
                            result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
                            $('#divErrortxtLogin_').html(result_to_show);
                            $('#txtLogin_verif').val("1");
                        } else {
                            result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
                            $('#divErrortxtLogin_').html(result_to_show);
                            $('#txtLogin_verif').val("0");
                        }

                    });


                jQuery.post(
                    '<?php echo site_url("front/particuliers/verifier_email");?>',
                    {txtEmail_var: txtEmail},
                    function (zReponse_) {
                        //alert (zReponse) ;
                        if (zReponse_ == "1") {
                            value_result_to_show = "1";
                        }


                        if (value_result_to_show == "1") {
                            result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
                            $('#divErrortxtLogin_').html(result_to_show);
                            $('#txtEmail_verif').val("1");
                        } else {
                            result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
                            $('#divErrortxtLogin_').html(result_to_show);
                            $('#txtEmail_verif').val("0");
                        }


                    });


                $.post(
                    '<?php echo site_url("front/professionnels/verifier_email_ionauth");?>',
                    {txtEmail_var_ionauth: txtEmail},
                    function (zReponse_ionauth) {
                        //alert (zReponse) ;
                        //var zReponse_html_ionauth = '';
                        if (zReponse_ionauth == "1") {
                            value_result_to_show = "1";
                        }


                        if (value_result_to_show == "1") {
                            result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
                            $('#divErrortxtLogin_').html(result_to_show);
                            $('#txtEmail_verif').val("1");
                        } else {
                            result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
                            $('#divErrortxtLogin_').html(result_to_show);
                            $('#txtEmail_verif').val("0");
                        }


                    });

                jQuery(".FieldError").removeClass("FieldError");
                //jQuery(".EmailLoading").addClass("EmailLoading").removeClass("EmailLoading_show");

            });


            jQuery("#txtDateNaissance").datepicker({
                dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                monthNames: ['Janvier', 'F�vier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Ao�t', 'Septembre', 'Octobre', 'Novembre', 'D�cembre'],
                dateFormat: 'DD, d MM yy',
                autoSize: true,
                changeMonth: true,
                changeYear: true,
                yearRange: '1900:2020'
            });

        });


        function getCP() {
            var ivilleId = jQuery('#txtVille').val();
            jQuery.get(
                '<?php echo site_url("front/particuliers/getPostalCode"); ?>' + '/' + ivilleId,
                function (zReponse) {
                    // alert (zReponse) ;
                    jQuery('#trReponseVille').html(zReponse);


                });
        }

        function CP_getDepartement() {
            //alert(jQuery('#CodePostalSociete').val());
            jQuery('#departementCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var CodePostalSociete = jQuery('#txtCodePostal').val();
            jQuery.post(
                '<?php echo site_url("front/professionnels/departementcp_particulier"); ?>',
                {CodePostalSociete: CodePostalSociete},
                function (zReponse) {
                    jQuery('#departementCP_container').html(zReponse);
                });
        }

        function CP_getVille() {
            //alert(jQuery('#CodePostalSociete').val());
            jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var CodePostalSociete = jQuery('#txtCodePostal').val();
            jQuery.post(
                '<?php echo site_url("front/professionnels/villecp_particulier"); ?>',
                {CodePostalSociete: CodePostalSociete},
                function (zReponse) {
                    jQuery('#villeCP_container').html(zReponse);
                });
        }

        function CP_getVille_D_CP() {
            //alert(jQuery('#CodePostalSociete').val());
            jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var CodePostalSociete = jQuery('#txtCodePostal').val();
            var departement_id = jQuery('#departement_id').val();
            jQuery.post(
                '<?php echo site_url("front/professionnels/villecp_particulier"); ?>',
                {CodePostalSociete: CodePostalSociete, departement_id: departement_id},
                function (zReponse) {
                    jQuery('#villeCP_container').html(zReponse);
                });
        }

    </script>


    <div style="text-align:center;">

        <div style="margin-top:10px; margin-bottom:20px;">
            <img src="<?php echo GetImagePath("privicarte/"); ?>/pvc_particulier.png" alt="" style="text-align:center">
        </div>
        <?php

        $this->load->library('ion_auth');
        $this->load->model("ion_auth_used_by_club");

        if ($this->ion_auth->logged_in()) {
            if (isset($iduser)) $iduser = $iduser; else $iduser = "";
        } else $iduser = "";
        ?>
        <div style="margin-bottom:30px;">
            <?php
            if (isset($user_ion_auth) && is_object($user_ion_auth)) {
                echo "<strong>Bonjour " . $user_ion_auth->first_name . " " . $user_ion_auth->last_name . "</strong>";
            }
            ?>
        </div>


        <div class="col-lg-12">
            <form name="frmInscriptionParticulier" id="frmInscriptionParticulier"
                  action="<?php echo site_url("front/particuliers/ajouter"); ?>" method="POST"
                  enctype="multipart/form-data">
                <div id="txt_586" style="text-align:center;">

                    <center>
                        <table id="table_form_inscriptionpartculier" width="475" cellpadding="0" cellspacing="5">

                            <tr>
                                <td class="td_part_form">
                                    <label>Nom *: </label>
                                </td>
                                <td class="td_part_form">
                                    <input type="text" name="Particulier[Nom]" class="form-control" style="width:273px;"
                                           id="txtNom"
                                           value="<?php if (isset ($oParticulier) && is_object($oParticulier)) {
                                               echo $oParticulier->Nom;
                                           } ?>"/>
                                </td>
                            </tr>

                            <tr>
                                <td class="td_part_form">
                                    <label>Pr&eacute;nom : </label>
                                </td>
                                <td class="td_part_form">
                                    <input type="text" name="Particulier[Prenom]" class="form-control"
                                           style="width:273px;" id="txtPrenom"
                                           value="<?php if (isset ($oParticulier) && is_object($oParticulier)) {
                                               echo $oParticulier->Prenom;
                                           } ?>"/>
                                </td>
                            </tr>

                            <tr>
                                <td class="td_part_form">
                                    <label>Date de naissance : </label>
                                </td>
                                <td class="td_part_form">
                                    <input style="width:273px;" type="text" class="form-control"
                                           name="Particulier[DateNaissance]" id="txtDateNaissance"
                                           value="<?php if (isset ($oParticulier) && is_object($oParticulier)) {
                                               echo convert_Sqldate_to_Frenchdate($oParticulier->DateNaissance);
                                           } ?>"/>
                                </td>
                            </tr>

                            <tr>
                                <td class="td_part_form">
                                    <label>Civilit&eacute; : </label>
                                </td>
                                <td class="td_part_form">
                                    <select style="width:280px;" name="Particulier[Civilite]" class="form-control"
                                            id="txtParticulier">
                                        <option value="0">Monsieur</option>
                                        <option value="1">Madame</option>
                                        <option value="2">Mademoiselle</option>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td class="td_part_form">
                                    <label>Adresse : </label>
                                </td>
                                <td class="td_part_form">
                                    <textarea name="Particulier[Adresse]" class="form-control" style="width:273px;"
                                              id="txtAdresse"><?php if (isset ($oParticulier) && is_object($oParticulier)) {
                                            echo $oParticulier->Adresse;
                                        } ?></textarea>
                                </td>
                            </tr>


                            <tr>
                                <td class="td_part_form">
                                    <label>Code postal : </label>
                                </td>
                                <td id="trReponseVille" name="trReponseVille" class="td_part_form">
                                    <input type="text" name="Particulier[CodePostal]" class="form-control"
                                           id="txtCodePostal" style="width:273px;"
                                           value="<?php if (isset ($oParticulier) && is_object($oParticulier)) {
                                               echo $oParticulier->CodePostal;
                                           } ?>" onblur="javascript:CP_getDepartement();CP_getVille();"/>
                                </td>
                            </tr>


                            <tr>
                                <td class="td_part_form">
                                    <label>Departement : </label>
                                </td>
                                <td class="td_part_form">
                        <span id="departementCP_container">
                        <select name="Particulier__departement_id" id="departement_id"
                                class="stl_long_input_platinum form-control" disabled="disabled"
                                onchange="javascript:CP_getVille_D_CP();" style="width:273px;">
                            <option value="0">-- Choisir --</option>
                            <?php if (sizeof($colDepartement)) { ?>
                                <?php foreach ($colDepartement as $objDepartement) { ?>
                                    <option
                                            value="<?php echo $objDepartement->departement_id; ?>"><?php echo $objDepartement->departement_nom; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_part_form">
                                    <label>Ville : </label>
                                </td>
                                <td class="td_part_form">
                        <span id="villeCP_container">
                        <input type="text" class="form-control"
                               value="<?php if (isset($oParticulier->IdVille)) echo $this->mdlville->getVilleById($oParticulier->IdVille)->Nom; ?>"
                               name="IdVille_Nom_text" id="IdVille_Nom_text" disabled="disabled" style="width:273px;"/>
                        <input type="hidden"
                               value="<?php if (isset($oParticulier->IdVille)) echo $oParticulier->IdVille; ?>"
                               name="Particulier[IdVille]" id="txtVille"/>
                        </span>
                                </td>
                            </tr>


                            <tr>
                                <td class="td_part_form">
                                    <label>N&deg; T&eacute;l&eacute;phone fixe : </label>
                                </td>
                                <td class="td_part_form">
                                    <input type="text" class="form-control" name="Particulier[Telephone]"
                                           id="txtTelephone" style="width:273px;"
                                           value="<?php if (isset ($oParticulier) && is_object($oParticulier)) {
                                               echo $oParticulier->Telephone;
                                           } ?>"/>
                                </td>
                            </tr>

                            <tr>
                                <td class="td_part_form">
                                    <label>N&deg; T&eacute;l&eacute;phone Mobile : </label>
                                </td>
                                <td class="td_part_form">
                                    <input type="text" class="form-control" name="Particulier[Portable]"
                                           style="width:273px;" id="txtPortable"
                                           value="<?php if (isset ($oParticulier) && is_object($oParticulier)) {
                                               echo $oParticulier->Portable;
                                           } ?>"/>
                                </td>
                            </tr>

                            <tr>
                                <td class="td_part_form">
                                    <label>Login *: </label>
                                </td>
                                <td class="td_part_form">
                                    <input type="text" name="Particulier[Login]" style="width:273px;"
                                           class="form-control"
                                           disabled="disabled" id="txtLogin"
                                           value="<?php if (isset ($oParticulier) && is_object($oParticulier)) {
                                               echo $oParticulier->Login;
                                           } ?>"/>
                                    <div class="FieldError" id="divErrortxtLogin_"></div>
                                    <!--<img src="<?php //echo GetImagePath("front/"); ?>/loading.gif" class="LoginLoading" title="" alt="loading" onload="OnLoadPngFix()"/>-->
                                    <input type="hidden" name="txtLogin_verif" style="width:273px;" id="txtLogin_verif"
                                           value="0"/>
                                    <input type="hidden" name="txtEmail_verif" style="width:273px;" id="txtEmail_verif"
                                           value="0"/>
                                </td>
                            </tr>


                            <input type="hidden" name="Particulier[IdUser]" style="width:273px;" id="IdUser"
                                   value="<?php if (isset($oParticulier) && is_object($oParticulier)) {
                                       echo $oParticulier->IdUser;
                                   } else echo "0"; ?>"/>

                            <input type="hidden" name="from_menuconsommateurs" id="from_menuconsommateurs" value="1"/>

                        </table>
                    </center>

                    <div>
                        <center>
                            <table width="450" border="0">
                                <tr>
                                    <td><input id="validationabonnement"
                                               style=" <?php if ($prmId != 0) echo 'visibility:hidden;'; ?>"
                                               name="check_3" value="1"
                                               type="checkbox" <?php if ($prmId != 0) echo 'checked="checked"'; ?> />
                                    </td>
                                    <td>
                                        <?php if ($prmId == 0) {
                                        } ?>
                                    </td>
                                </tr>
                            </table>
                        </center>

                    </div>

                    <div id="divErrortxtInscription"
                         style="padding:0px 0px 0px 2px; font-family: Arial; font-size: 12px; color:#F00;">
                        <?php if (isset($_REQUEST['_msg_menuconsommateurs']) && $_REQUEST['_msg_menuconsommateurs'] == "1") echo '<span style="color:#00CC00;">Modifications enregistr&eacute;e !</span>'; ?>
                    </div>

                    <div style="margin-top:20px; margin-bottom:20px;">
                        <button id="btnSinscrire" type="button"
                                style="-moz-appearance: none !important; margin: 0 !important; padding: 0 !important;">
                            <img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/btn_edit_data_part.png"
                                 alt="" style="text-align:center"></button>
                    </div>

                    <?php if (CURRENT_SITE == "agenda") { ?>
                        <div>
                            <a href="<?php echo site_url("front/utilisateur/conditionsgenerales"); ?>"
                               style="text-decoration:none;">
                                <input id="butn_10sdqs6" type="button" value="Conditions G�n�rales"
                                       style="width:253px; height:22px;">
                            </a>
                        </div>
                    <?php } ?>

                </div>

            </form>
        </div>


        <script>
            function btn_part_privicarte() {
                window.open("<?php echo site_url('front/fidelity/menuconsommateurs');?>", "_blank", "toolbar=no, scrollbars=yes, resizable=yes, top=500, left=500, width=650, height=800");
            }
        </script>


        <div style="margin-bottom:20px;">
            <a href="javascript:void(0);" onclick="btn_part_privicarte();"><!--onclick="btn_part_privicarte();"-->
                <img src="<?php echo GetImagePath("privicarte/"); ?>/btn_part_privicarte.png" alt="btn_part_privicarte"
                     style="text-align:center">
            </a>
        </div>


        <?php if (isset($iduser) && $iduser != 0 && $iduser != "") { ?>
            <div><a href="<?php echo site_url("connexion/sortir"); ?>"><img
                            src="<?php echo base_url(); ?>application/resources/privicarte/images/btn_pro_deconnexion.png"
                            alt="btn_pro_deconnexion" style="text-align:center"></a>
            </div>
        <?php } ?>
    </div>


<?php $this->load->view("frontAout2013/includes/footer_mini_2"); ?>