<div class="row pl-3 pt-4 pb-4 pr-4" style="">
    <?php     $path_img_gallery_gl = 'application/resources/front/photoCommercant/imagesbank/'.$oInfoCommercant->user_ionauth_id;
    ?>
    <div class="col-sm-6 text-center pl-0 text-justify">
        <div class="p-4 sautligne allign_button" style=";height:auto;padding-right:0px!important;">
            <?php if (isset($objbloc_info->bloc_1_2_image1_content_page2) AND $objbloc_info->bloc_1_2_image1_content_page2 != '' AND $objbloc_info->bloc_1_2_image1_content_page2 != null) { ?>
                <div style="width: 100%;height: auto">
                    <img style="max-width: 100%;height: auto;" class="img-fluid"
                         src="<?php echo base_url($path_img_gallery_gl . '/' . $objbloc_info->bloc_1_2_image1_content_page2) ?>">
                </div>
            <?php } ?>
            <?php if (isset($objbloc_info->bloc1_2_content_page2) AND $objbloc_info->bloc1_2_content_page2 != '' AND $objbloc_info->bloc1_2_content_page2 != null) { ?>
                <div class="pb-2 pt-2 text-justify"><?php echo htmlspecialchars_decode($objbloc_info->bloc1_2_content_page2) ?></div>
            <?php } ?>
            <?php if (isset($objbloc_info->is_activ_btn_bloc1_2_page2) AND $objbloc_info->is_activ_btn_bloc1_2_page2 == '1' AND $objbloc_info->is_activ_btn_bloc1_2_page2 != null) {
                if ($objbloc_info->bloc1_2_existed_link1_page2 == 'ag') {
                    $linkd = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($objbloc_info->bloc1_2_existed_link1_page2 == 'art') {
                    $linkd = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($objbloc_info->bloc1_2_existed_link1_page2 == 'p1') {
                    $linkd = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($objbloc_info->bloc1_2_existed_link1_page2 == 'p2') {
                    $linkd = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($objbloc_info->bloc1_2_existed_link1_page2 == 'bp') {
                    $linkd = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($objbloc_info->bloc1_2_existed_link1_page2 == 'fd') {
                    $linkd = $link_fidelity;
                } elseif ($objbloc_info->bloc1_2_existed_link1_page2 == 'bt') {
                    $linkd = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif (isset($objbloc_info->bloc1_2_custom_link1_page2) AND $objbloc_info->bloc1_2_custom_link1_page2 != '' AND $objbloc_info->bloc1_2_custom_link1_page2 != null) {
                    $linkd = 'https://' . $objbloc_info->bloc1_2_custom_link1_page2;
                }

                ?>
                <?php if (isset($objbloc_info->bloc2_islightbox_page2) AND $objbloc_info->bloc2_islightbox_page2=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkd ?? '' ?>"  class="fancybox.iframe"><div class="bouton"><?php echo $objbloc_info->btn_bloc1_2_content1_page2 ?></div></a>
                <?php }else{  ?>
                    <a class="pb-5 pt-5 lienBouton " href="<?php echo ($linkd) ?? '' ?>">
                        <div class="bouton"><?php echo $objbloc_info->btn_bloc1_2_content1_page2 ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
            
        </div>
    </div>
    <div class="col-sm-6 text-center pl-0 text-justify">
        <div class="p-4 sautligne allign_button" style=";height:auto;padding-right:0px!important;">
        <?php if (isset($objbloc_info->bloc_1_2_image2_content_page2) AND $objbloc_info->bloc_1_2_image2_content_page2 != '' AND $objbloc_info->bloc_1_2_image2_content_page2 != null) { ?>
                <div class="image">
                    <img style="max-width: 100%;height: auto;"
                         src="<?php echo base_url($path_img_gallery_gl . '/' . $objbloc_info->bloc_1_2_image2_content_page2) ?>">
                </div>
            <?php } ?>
            <?php if (isset($objbloc_info->bloc1_2_content2_page2) AND $objbloc_info->bloc1_2_content2_page2 != '' AND $objbloc_info->bloc1_2_content2_page2 != null) { ?>
                <div class="pb-2 pt-2 text-justify"><?php echo htmlspecialchars_decode($objbloc_info->bloc1_2_content2_page2) ?></div>
            <?php } ?>
            <?php if (isset($objbloc_info->is_activ_btn_bloc2_2_page2) AND $objbloc_info->is_activ_btn_bloc2_2_page2 == '1' AND $objbloc_info->is_activ_btn_bloc2_2_page2 != null) {
                if ($objbloc_info->bloc1_2_existed_link2_page2 == 'ag') {
                    $linke = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($objbloc_info->bloc1_2_existed_link2_page2 == 'art') {
                    $linke = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($objbloc_info->bloc1_2_existed_link2_page2 == 'p1') {
                    $linke = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($objbloc_info->bloc1_2_existed_link2_page2 == 'p2') {
                    $linke = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($objbloc_info->bloc1_2_existed_link2_page2 == 'bp') {
                    $linke = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($objbloc_info->bloc1_2_existed_link2_page2 == 'fd') {
                    $linke = $link_fidelity;
                } elseif ($objbloc_info->bloc1_2_existed_link2_page2 == 'bt') {
                    $linke = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif (isset($objbloc_info->bloc1_2_custom_link2_page2) AND $objbloc_info->bloc1_2_custom_link2_page2 != '' AND $objbloc_info->bloc1_2_custom_link2_page2 != null) {
                    $linke = 'https://' . $objbloc_info->bloc1_2_custom_link2_page2;
                }
                ?>
                <?php if (isset($objbloc_info->bloc2_islightbox2_page2) AND $objbloc_info->bloc2_islightbox2_page2=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linke ?? '' ?>"  class="fancybox.iframe"><div class="bouton"><?php echo $objbloc_info->btn_bloc1_2_content2_page2 ?></div></a>
                <?php }else{  ?>
                    <a class="pb-5 pt-5 lienBouton " href="<?php echo ($linke) ?? '' ?>">
                        <div class="bouton"><?php echo $objbloc_info->btn_bloc1_2_content2_page2 ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
            
        </div>
    </div>
</div>