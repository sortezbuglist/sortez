<?php 
$data['umpty'] = '';
//$this->load->view("privicarte/includes/main_header", $data); ?>
<?php
$this_session_localdata =& get_instance();
$this_session_localdata->load->library('session');
$publightbox = $this_session_localdata->session->userdata('publightbox');
?>
    <script src="https://www.google.com/recaptcha/api.js"></script>
			<script type="text/javascript">
				$(document).ready(function() {
				
				$.ajax({
			        type : 'POST',
			        url : '<?php echo site_url("front/professionnels/allcommercant"); ?>',           
			        success:function (data) {
			            $('#counter_view_bolder_adresses').html(data);
			        }
			    });
				$.ajax({
			        type : 'POST',
			        url : '<?php echo site_url("front/utilisateur/allparticulier"); ?>',           
			        success:function (data) {
			            $('#counter_view_bolder_inscrits').html(data);
			        }          
			    });
				
			});

			function publightbox_mail_focus() {
				if ($("#publightbox_mail").val()=="Entrez votre E-mail") $("#publightbox_mail").val('');	
			}
			function publightbox_name_focus() {
				if ($("#publightbox_name").val()=="Entrez votre E-mail") $("#publightbox_name").val('');	
			}
			function publightbox_mail_blur() {
				if ($("#publightbox_mail").val()=="") $("#publightbox_mail").val('Entrez votre E-mail');	
			}
			function publightbox_name_blur() {
				if ($("#publightbox_name").val()=="") $("#publightbox_name").val('Entrez votre nom');	
			}
			function publightbox_submit_function () {

					$("#spanpublightbox_loading").html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
					var error = "";
					var publightbox_mail = $("#publightbox_mail").val();
					var publightbox_name = $("#publightbox_name").val();
					//var publightbox_departement_value = $("#publightbox_departement_value").val();
					// alert(publightbox_departement_value);
					if (publightbox_mail == '' || publightbox_mail == 'VOTRE MAIL') {
						error += '- Veuillez Enregistrer votre e-mail';			
					}
					if (publightbox_name == '' || publightbox_name == 'VOTRE NOM') {
						error += '- Veuillez Enregistrer votre nom';			
					}
					if (!validateEmail(publightbox_mail)) {
						error += '- Adresse email invalide';
						
					}
					/*if (publightbox_departement_value == '' || publightbox_departement_value == '0' || publightbox_departement_value == '00') {
						error += '<span style="color:#FF0000;"> - Veuillez choisir votre departement</span>';
					}*/

					if (error != "") {
						$("#spanpublightbox_loading").html(error);

					} else {
						//$("#spanpublightbox_loading").html(publightbox_mail+" "+publightbox_departement_value);

						$("#spanpublightbox_loading").html('');
						var client_pass = Math.random().toString(36).slice(2);
						
						$.post(
							"<?php echo site_url("front/utilisateur/lightbox_mail_verify");?>", 
							{
								publightbox_mail:publightbox_mail
							},
							function(data){
								 if (data=="1") {


									$.post(
										"<?php echo site_url("front/professionnels/publightbox_identity");?>",
										{
											publightbox_identity: publightbox_mail
										},
										function( data00 ) {
										
											var html_data0033 = $("#publighbox_enter_content").html();	
											$("#pub_form_slide_container").html(html_data0033);	
											$("#spanpublightbox_loading").html("");	
											$("#publighbox_enter_content_mail").html(publightbox_mail);

											//open privicarte in new window
											//var win_rand = window.open('<?php echo base_url();?>', '_blank');
			                                <?php
			                                // if($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL) $url_link_result = site_url("vivresaville/accueil");
			                                // else $url_link_result = site_url("");
			                                ?>
											// var win_rand = window.open('<?php echo $url_link_result;?>', '_blank');
											// win_rand.focus();
										});



								 } else {
								 	
										$.post(
											"<?php echo site_url("front/utilisateur/lightbox_mail_save/");?>",
											{
												publightbox_mail: publightbox_mail,publightbox_name: publightbox_name 
											},
											function( data2 ) {

													//$("#spanpublightbox_loading").html(' - Votre e-mail est enregistr&eacute; avec succ&egrave;s !');
													
													var html_data0022 = $("#publighbox_enter_content").html();	
													$("#pub_form_slide_container").html(html_data0022);
													$("#publighbox_enter_content_mail").html(publightbox_mail);
													$("#publighbox_enter_content_mail").html(publightbox_name);
													
													//open privicarte in new window
													//var win_rand = window.open('<?php echo base_url();?>', '_blank');
													// var win_rand = window.open('/<?php echo site_url('front/annuaire'); ?>', '_blank');
			  								// 		win_rand.focus();
													
													$.ajax({
														type : 'POST',
														url : '<?php echo site_url("front/professionnels/allcommercant"); ?>',           
														success:function (data) {
															$('#counter_view_bolder_adresses').html(data);
														}          
													});
													$.ajax({
														type : 'POST',
														url : '<?php echo site_url("front/utilisateur/allparticulier"); ?>',           
														success:function (data) {
															$('#counter_view_bolder_inscrits').html(data);
														}          
													});
														
													
											});
										
								 }
							});
							
					}	
					return false;
				
			}


			function validateEmail(sEmail) {
				var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
				if (filter.test(sEmail)) {
					return true;
				}
				else {
					return false;
				}
			}
			function lightbox_depart_select(xCode, xText) {
				$("#publightbox_departement").text(xText);
				$("#publightbox_departement_value").val(xCode);
			}
                jQuery(document).ready(function () {
                    jQuery("#publightbox_submit").click(function () {
                        var cap=document.getElementById("g-recaptcha-response").value;
                        test(cap);//21103 //toFirstIdDatatourisme

                    });
                });
                function test(cap) {
                    jQuery.ajax({
                        type: "POST",
                        url: "<?php echo site_url("front/professionnels/test_captcha"); ?>",
                        data: 'g-recaptcha-response=' + (cap),
                        dataType: "json",
                        success: function (data) {

                            var txtError = false;
                            var RubriqueSociete = $('#publightbox_name').val();
                            if (RubriqueSociete === "") {
                                txtError += "- Vous devez préciser votre nom<br/>";
                            }
                            var SousRubriqueSociete = $('#publightbox_mail').val();
                            if (SousRubriqueSociete == "") {
                                txtError += "- Vous devez préciser Votre Email<br/>";
                            }
                            if(!validateEmail(SousRubriqueSociete))
                            {
                                txtError += "- Vous devez entrer une mail valide!<br/>";
                            }
                            var EmailSociete = $("#publightbox_mail").val();
                            /*if (!isEmail(EmailSociete)) {
                                txtError += "- Veuillez indiquer un email valide.<br/>";
                            }*/
                            if ($('#input_mail_save_check').is(":checked")) {
                            } else {
                                txtError += "- Vous devez valider les conditions générales<br/>";
                            }
                            if ($('#input_mail_save_check').is(":checked")) {
                                //$("#idreferencement").val("1");
                            } else {
                                //$("#idreferencement").val("0");
                            }
                            if (txtError ==false && data.captcha == "OK"){
                                $("#publightbox_submit").submit();
                            }if (data.captcha == "NO") {
                                alert("captha non valide");
                            }
                            if (txtError !=false){
                                $("#divErrorFrmInscriptionProfessionnel").html(txtError);
                            }
                        },
                        error: function (data) {

                            alert("data");
                        }
                    });
                }

			</script>

<!-- ici eplacemetn ancien javascript -->


    <link href="https://getbootstrap.com/docs/3.3/examples/navbar/navbar.css" rel="stylesheet">
<style type="text/css">
<!--
    .g-recaptcha iframe{
    margin-left:170px;
        margin-top: 15px;
    }
@font-face {
    font-family: FuturaBk;
    src: url(<?php echo base_url()."application/resources/fonts/Futura-T-OT-Book_19064.ttf";?>);
}
.container_Idpublightbox .pub_form_content {
	width: 100%;
	display: table;
	height:327px;
}
.message_loadingpub {
	color: #FFFFFF;
	text-align:center;
	margin-left: auto;
    margin-right: auto;
    text-align: center;
    width: 460px;
}
.input_mail_save {
	color: #000000;/*FFFFFF*/
	height:40px;
	text-align:left;
    margin-left: auto;
    margin-right: auto;
    text-align: left;
    width: 460px;
}
.counter_view_bolder {
	font-size: 26px;
}
.publightbox_inputs {
	height: auto;
	width: 500px;
	margin-right: auto;
	margin-left: auto;
	/*background-image: url("<?php 
// 	echo GetImagePath("privicarte/");
	?>/bg_lightbox_inputs.png");*/
	background-repeat: no-repeat;
	/*background-color:red;*/
	background-position: center top;
	min-height: 83px;
}

.counter_view {
	height: 85px;
	width: 525px;
	margin-right: auto;
	margin-left: auto;
	background-image: url("<?php echo GetImagePath("privicarte/");?>/bg_counter.png");
	background-repeat: no-repeat;
	background-position: center top;
	background-color: rgba(0, 0, 0, 0.4);
	border-radius: 20px 20px 0 0;
	display:table;
	padding-top:10px;
}
.counter_view .col-lg-12 {
	color: #ffffff;
    font-family: FuturaBk;
    font-size: 24px;
    font-style: normal;
    font-variant: normal;
    font-weight: bold;
    line-height: 30.7px;
    margin-bottom: 0;
    text-align: center;
    vertical-align: 0;
	display:table;
}

#publightbox_submit {
	/*background-color: #000000;*/
	height: 50px;
	width: 300px;
	background-color:#dc1a95;
	color: black;
	border: none;
	margin-top:20px;
    font-size: 15px;
    color: #ffffff;
}
.fb_login_pub {
	text-align: center;
	display:table;
	margin:20px 0;
}

#view_Idpublightbox .pub_form_content #publightbox_form .pub_frm_item40 .dropup #publightbox_departement {
	background-image: url("<?php echo GetImagePath("privicarte/");?>/publightbox_mail.png");
	background-repeat: no-repeat;
	background-position: center top;
	background-color: #FFFFFF;
	height: 55px;
	width: 260px;
	padding: 10px 15px 15px;
	border: none;
	font-weight: bold;
	border:none;
	box-shadow: none !important;
}
#spanpublightbox_loading {
	color:#FFFFFF;
}
.container_Idpublightbox {
	/*background-color: #DC1A95;*/
    background-color: #ffffff !important;
	width:650px;
	/*height:auto;*/
    height:550px;
	margin-left:auto;
	margin-right: auto;
	min-height:450px;
	border-radius: 35px;
	/*border: 2px solid #000000;*/
    border: 2px solid #ffffff !important;
}
#view_Idpublightbox .pub_text_content {
	background-color: transparent;
    color: #000000 !important;/*ffffff*/
    font-family: FuturaBk;
    font-size: 33px;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 29.3px;
    margin-bottom: 0;
    text-align: center;
    vertical-align: 0;
	text-align:center;
	height:40px;
	margin-top:40px;
}

#publightbox_departement_ul {
    padding: 5px 10px 0;
    max-height: 300px;
    overflow-y: scroll;
}
#navigation_up_down_3107 { display:none !important; }

.publightbox_inputs #publightbox_mail {
	width: 470px;
    height: 45px;
	border: solid 2px #1976bc;
	font-size:16px;
	color:#7F7F7F;
	text-align:left;
	font-family:Arial, Helvetica, sans-serif;
}
.publightbox_inputs #publightbox_name {
	width: 470px;
    height: 45px;
	border: solid 2px #1976bc;
	font-size:16px;
	color:#7F7F7F;
	text-align:left;
	font-family:Arial, Helvetica, sans-serif;
}
/*#publightbox_submit img { padding-left:10px; padding-bottom:10px;}*/
body {
    margin-top: 0px !important;
	background:transparent none repeat scroll 0 0 !important;
}
#pub_form_slide_container {
	height:242px;
	display: block;
}
-->
</style>

<!--
-->

<!-- wvwxcvc -->
<head>
<title>Newsletter</title>

<meta name="description"  content=" Newsletter du Magazine Sortez">
</head>

	
<div id="view_Idpublightbox" class="container_Idpublightbox">

    <div class="message_loadingpub">
    <div id="spanpublightbox_loading"></div>
    </div>
    
    <div class="pub_form_content padding0">
    <div class="text-center"><img style="margin-left: 7rem;" src="https://static.wixstatic.com/media/5fa146_89b4ac26f6e643288e245a604cb32e3d~mv2.png/v1/fill/w_409,h_223,al_c,usm_0.66_1.00_0.01,enc_auto/newsletter-bandeau.png<?php //echo base_url('assets/img/logo_newsletter.png')?>"></div>
    <div id="pub_form_slide_container" class="padding0">
    <form name="publightbox_form" id="publightbox_form" enctype="multipart/form-data" method="post"  action="">
    	<!-- mis à jours du formaulaire -->
   		<div class="publightbox_inputs">
    		<input name="publightbox_name" class="publightbox_name form-control" id="publightbox_name" onfocus="javascript:publightbox_name_focus();" onblur="javascript:publightbox_name_blur();" type="text" value="" placeholder="Votre nom" />
    		<br>
    		<br>
      		<input name="publightbox_mail" class="publightbox_mail form-control" id="publightbox_mail" onfocus="javascript:publightbox_mail_focus();" onblur="javascript:publightbox_mail_blur();" type="text" value="" placeholder="Votre Mail"/>
      		
    	</div>
    	<br>
	
    
    	<div class="input_mail_save">
    		<input name="input_mail_save_check" class="input_mail_save_check" id="input_mail_save_check" type="checkbox"/>
        	<label style="margin-top:-20px;margin-left:20px;font-size: 14px;">J 'accepte de recevoir les newsletters de Magazine Sortez ciblés en fonction de vos intérêts</label>
    	</div>
        <div style="text-align: center;" id="capt" class="g-recaptcha text-center" data-sitekey="6Lfjgm0UAAAAAOl1qieKqiWV5gZuSjjAc19jUIRg"></div>
        <div id="divErrorFrmInscriptionProfessionnel" style="color: red"></div>
    	<div class="publightbox_inputs" style="">
      		<center><button name="publightbox_submit" class="publightbox_submit" id="publightbox_submit" onclick="javascript:publightbox_submit_function();return false;">S' INSCRIRE</button></center>
    	</div>
    </form>    
    
      
    <div class="col-lg-12 fb_login_pub" style="display: none;">
    	<!--<a href="#"><img src="<?php //echo GetImagePath("privicarte/");?>/fb_login.png" /></a>-->
        <fb:login-button scope="public_profile,email" data-size="xlarge" onlogin="checkLoginState();">
        </fb:login-button>
        <div id="facebooklogin_status" style="color:#FFFFFF; padding-top:15px; font-size:12px;">
        </div>
        
    </div>
        
        
    </div>
    
    <div class="counter_view text-center" style="margin-top:100px;background-color: #dc1a95; display: none;">
   	  <div class="col-sm-12 padding0 text-center">
          <div class="col-sm-6 text-center"><span class="counter_view_bolder" id="counter_view_bolder_adresses">...</span><br/>adresses</div>
          <div class="col-sm-6 text-center"><span class="counter_view_bolder" id="counter_view_bolder_inscrits">...</span><br/>inscrits</div>
      </div>
    </div>
    
    </div>
    
</div>




<div id="publighbox_enter_content" style="display:none;">
  <div style="width: 450px; margin-right: auto; color:#044ca4; font-size:30px; font-family:FuturaBk; display:table; padding-top:25px; padding-left: 95px; /*background-image:url(<?php echo GetImagePath("privicarte/");?>/user_publightbox.png);*/ background-position:left top; background-repeat:no-repeat; font-weight:bold; text-align:center;">
    <div style="font-size: 30px;">Vous vous êtes Bien inscrit sur notre Newsletter, Nous vous remercions de votre inscription</div>
      <div id="publighbox_enter_content_mail"></div>
      <br>
      <div style="width: 100%; display: table;">
      	<a href="<? echo site_url(''); ?>" >
      	<img src="https://static.wixstatic.com/media/5fa146_a57f071aabf94592a87d8fb6a402d081~mv2.png/v1/fill/w_339,h_100,al_c,usm_0.66_1.00_0.01,enc_auto/logo%20sortez.png" ></a>
          <?php
          // if($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL) $url_link_result = site_url("vivresaville/accueil");
          // else $url_link_result = site_url("front/annuaire");
          ?>
          <!-- <button id="login_submit" name="login_submit" style="background-color: rgb(0, 0, 0); border: medium none; border-radius: 5px; padding: 5px 45px; font-size: 40px; font-weight: bold;" onclick="javascript:window.open('<?php echo $url_link_result;?>', '_blank'); return false;">Je me connecte</button> -->
      </div>
  </div>
</div>




<?php //$this->session->set_userdata('publightbox', "1"); ?>
<?php //} ?>
<?php 
$data['umpty'] = '';
$this->load->view("privicarte/includes/main_footer", $data); 
?>