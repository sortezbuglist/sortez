<div class="row pb-4" style="">
    <?php     $path_img_gallery_gl = 'application/resources/front/photoCommercant/imagesbank/'.$oInfoCommercant->user_ionauth_id;
    ?>
    <div style="width: 100%;" class="pr-4 pb-0 pt-0  pl-3">

        <div class="col-sm-12 text-center text-justify pl-4 pt-4 pb-4 pr-3" style="">
            <?php if (isset($objbloc_info->bloc_1_3_image1_content_page2) AND $objbloc_info->bloc_1_3_image1_content_page2 != '' AND $objbloc_info->bloc_1_3_image1_content_page2 != null) { ?>
                <div style="width: 100%;height: auto">
                    <img style="max-width: 100%;height: auto;" class="img-fluid"
                         src="<?php echo base_url($path_img_gallery_gl . '/' . $objbloc_info->bloc_1_3_image1_content_page2) ?>">
                </div>
            <?php } ?>
            <?php if (isset($objbloc_info->bloc1_3_content_page2) AND $objbloc_info->bloc1_3_content_page2 != '' AND $objbloc_info->bloc1_3_content_page2 != null) { ?>
                <div class="text-justify"><?php echo htmlspecialchars_decode($objbloc_info->bloc1_3_content_page2) ?></div>
            <?php } ?>
            <?php if (isset($objbloc_info->is_activ_btn_bloc1_3_page2) AND $objbloc_info->is_activ_btn_bloc1_3_page2 == '1' AND $objbloc_info->is_activ_btn_bloc1_3_page2 != null) {
                if ($objbloc_info->bloc1_3_existed_link1_page2 == 'ag') {
                    $linkg = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($objbloc_info->bloc1_3_existed_link1_page2 == 'art') {
                    $linkg = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($objbloc_info->bloc1_3_existed_link1_page2 == 'p1') {
                    $linkg = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($objbloc_info->bloc1_3_existed_link1_page2 == 'p2') {
                    $linkg = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($objbloc_info->bloc1_3_existed_link1_page2 == 'bp') {
                    $linkg = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($objbloc_info->bloc1_3_existed_link1_page2 == 'fd') {
                    $linkg = $link_fidelity;
                } elseif ($objbloc_info->bloc1_3_existed_link1_page2 == 'bt') {
                    $linkg = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif (isset($objbloc_info->bloc1_3_custom_link1_page2) AND $objbloc_info->bloc1_3_custom_link1_page2 != '' AND $objbloc_info->bloc1_3_custom_link1_page2 != null) {
                    $linkg = 'https://' . $objbloc_info->bloc1_3_custom_link1_page2;
                }

                ?>

                <?php if (isset($objbloc_info->bloc3_islightbox_page2) AND $objbloc_info->bloc3_islightbox_page2=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkg ?? '' ?>"  class="fancybox.iframe"><div class="bouton2"><?php echo $objbloc_info->btn_bloc1_3_content1_page2 ?></div></a>
                <?php }else{  ?>
                    <a class="pb-5 pt-5 lienBouton2 " href="<?php echo ($linkg) ?? '' ?>">
                        <div class="bouton2"><?php echo $objbloc_info->btn_bloc1_3_content1_page2 ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>