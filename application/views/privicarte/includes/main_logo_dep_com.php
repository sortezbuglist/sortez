<?php 
//LOCALDATA FILTRE
$this_session_localdata =& get_instance();
$this_session_localdata->load->library('session');
$localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
$localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
if((isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && $localdata_IdVille != NULL) || (isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && $localdata_IdDepartement != NULL)) {
?>
<style type="text/css">
body { margin-top:0 !important;}
#main_logo_dep_com_localdata { display:none !important;}
</style>
<?php
} //LOCALDATA FILTRE
?>


<style type="text/css">
<!--
.div_dep_com {
	float: right;
	padding-right: 20px;
	position: relative;
	padding-top: 42px;
	padding-bottom: 42px;
}
.div_logo {
	float: left;
	position: relative;
}
.main_list_select_pvc button
{
-moz-appearance: none !important;
border: medium none !important;
margin: 0 !important;
padding: 0 !important;
box-shadow: none !important;
text-transform:uppercase;
font-family:Arial, Helvetica, sans-serif;
font-weight:bold; 
color:#ffffff;
font-size:15px;
width:212px; 
height:43px;
}
#main_dep_select button {
background-image:url(<?php echo GetImagePath("privicarte/") ; ?>/test/btn_dep.png) !important; 
background-repeat:no-repeat !important;
padding-left:25px !important;
text-align:center;
}
#main_com_select button {
background-image:url(<?php echo GetImagePath("privicarte/") ; ?>/test/btn_com.png) !important; 
background-repeat:no-repeat !important;
padding-left:17px !important;
}
#main_dep_select button:hover, #main_dep_select button:focus, #main_com_select button:hover, #main_com_select button:focus  {
background-position:0px !important;
background-color: #FFFFFF;
}
.dropdown.main_list_select_pvc {
    display: inline;
}
.main_list_select_pvc button .caret {
    display: none !important;
}
.main_list_select_pvc .dropdown-menu {
    margin-top: 15px;
}
#main_dep_select ul.dropdown-menu {
margin-left:45px;
}
#main_com_select ul.dropdown-menu {
margin-left:5px;
}
-->
</style>

<script type="text/javascript">
function pvc_select_departement(departement_id) {
 $("#inputStringDepartementHidden_bonplans").val(departement_id);
 $("#inputStringDepartementHidden_partenaires").val(departement_id);
 $("#inputStringDepartementHidden_annnonces").val(departement_id);
 $("#frmRechercheBonplan").submit();
 $("#frmRecherchePartenaire").submit();
 $("#frmRechercheAnnonce").submit();
}
function pvc_select_commune(id_commune){
 $("#inputStringVilleHidden_bonplans").val(id_commune);
 $("#inputStringVilleHidden_partenaires").val(id_commune);
 $("#inputStringVilleHidden_annonces").val(id_commune);
 $("#frmRechercheBonplan").submit();
 $("#frmRecherchePartenaire").submit();
 $("#frmRechercheAnnonce").submit();
}
</script>

<div id="main_logo_dep_com_localdata" class="container" style="height:auto; background-color:#FFFFFF;">
<div class="div_logo"><a href="<?php echo site_url();?>"><img src="<?php echo GetImagePath("privicarte/") ; ?>/logo.png" alt="logo" /></a></div>
<div class="div_dep_com">

<div class="dropdown main_list_select_pvc" id="main_dep_select">
  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu_dep" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width:310px;">
	<?php 
    if(isset($iDepartementId) && $iDepartementId != "" && $iDepartementId != NULL && $iDepartementId != 0 && $iDepartementId != "0") 
    {
        foreach($toDepartement as $oDepartement){
            if ($oDepartement->departement_id == $iDepartementId) echo $oDepartement->departement_nom." (".$oDepartement->nbCommercant.")";
			//else echo "DEPARTEMENT";
        }
    }
    else echo "DEPARTEMENTS";
	//echo "<br/>".$iDepartementId;
    ?>
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu_dep">
  	<li><a href="javascript:void(0);" onclick="javascript:pvc_select_departement(0);">Tous les departements</a></li>

<?php 
 if(isset($iDepartementId) && $iDepartementId != "" && $iDepartementId != NULL && $iDepartementId != 0 && $iDepartementId != "0") 
    {} else {

	//LOCALDATA FILTRE
	$this_session_localdata =& get_instance();
	$this_session_localdata->load->library('session');
	$localdata_value = $this_session_localdata->session->userdata('localdata');
	if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
	} else {
	////$this->firephp->log($toVille, 'toVille');
		foreach($toDepartement as $oDepartement){ ?>
			<li><a href="javascript:void(0);" onclick="javascript:pvc_select_departement(<?php echo $oDepartement->departement_id;?>);"><?php echo $oDepartement->departement_nom." (".$oDepartement->nbCommercant.")" ; ?></a></li>
		<?php } ?>
	<?php } 

} ?>
  
  </ul>
</div>


<div class="dropdown main_list_select_pvc" id="main_com_select">
  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu_com" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width:310px; height:43px;">
  <?php 
    if(isset($iVilleId) && $iVilleId != "" && $iVilleId != NULL && $iVilleId != 0 && $iVilleId != "0") 
    {
        foreach($toVille as $oVille){
            if ($oVille->IdVille == $iVilleId) echo $oVille->Nom." (".$oVille->nbCommercant.")";
			//else echo "COMMUNE";
        }
    }
    else echo "COMMUNES";
    ?>
<span class="caret"></span>
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu_com">
    <li><a href="javascript:void(0);" onclick="javascript:pvc_select_commune(0);">Toutes les communes</a></li>
<?php 

if(isset($iVilleId) && $iVilleId != "" && $iVilleId != NULL && $iVilleId != 0 && $iVilleId != "0") 
    {} else {

	//LOCALDATA FILTRE
	$this_session_localdata =& get_instance();
	$this_session_localdata->load->library('session');
	$localdata_value = $this_session_localdata->session->userdata('localdata');
	if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
	} else {
	////$this->firephp->log($toVille, 'toVille');
		foreach($toVille as $oVille){ ?>
			<li><a href="javascript:void(0);" onclick="javascript:pvc_select_commune(<?php echo $oVille->IdVille ; ?>);"><?php echo $oVille->Nom." (".$oVille->nbCommercant.")" ; ?></a></li>
		<?php } ?>
	<?php } 

}?>    
    
  </ul>
</div>

<!--<a href="#"><img src="<?php echo GetImagePath("privicarte/") ; ?>/test/btn_dep.png" alt="dep" style="padding-right:5px;"/></a><a href="#"><img src="<?php echo GetImagePath("privicarte/") ; ?>/test/btn_com.png" alt="com" style="padding-left:5px;"/></a>-->

</div>




</div>


