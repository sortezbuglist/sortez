<style type="text/css">
<!--
ul.main_menu_navigation {
text-transform:uppercase; list-style:none; height:60px; background-color:#DC198F; margin:0px !important; padding-left:0px;
}
.main_menu_navigation li {
display: inline;
float:left; width:20%;
padding:19px 15px;
text-align:center;
}
.main_menu_navigation li:hover {
background:#000000;
}
.main_menu_navigation li a {
padding:0px;
text-decoration:none;
color:#FFFFFF;
font-weight:700 !important;
font-size:16px;
font-family:Arial, Helvetica, sans-serif !important;
}
.main_menu_navigation li > a {
    padding: 18px 15px !important;
}
.fancybox-inner {
/*min-height: 800px !important;*/
}
-->
</style>

<script type="text/javascript">
	$(document).ready(function() {

		//$.noConflict();
		
		/*$(".fancybox").fancybox({
			autoScale : false,
			overlayOpacity      : 0.8, // Set opacity to 0.8
			overlayColor        : "#000000", // Set color to Black
			padding         : 5,
			height        : 850,
			transitionIn      : 'elastic',
			transitionOut     : 'elastic',
			type          : 'iframe',
		});*/
		
		$(".fancybox_fonctionnement").fancybox();
		$("#menu_fonctionnement").fancybox({
			autoScale : false,
			overlayOpacity      : 0.8, // Set opacity to 0.8
			overlayColor        : "#000000", // Set color to Black
			padding         : 5,
			width         : 500,
			height        : 850,
			transitionIn      : 'elastic',
			transitionOut     : 'elastic',
			type          : 'iframe',
		});
		
		$(".fancybox_mon_compte").fancybox();
		$("#menu_mon_compte").fancybox({
			'autoScale' : false,
			'overlayOpacity'      : 0.8, // Set opacity to 0.8
			'overlayColor'        : "#000000", // Set color to Black
			'padding'         : 5,
			'width'         : 650,
			'height'		: 850,
			'transitionIn'     : 'elastic',
			'transitionOut'     : 'elastic',
			'type'          : 'iframe',
			'onClosed':function () {
			  window.location.reload();
			}
		});
		
		$(".fancybox_ma_carte").fancybox();
		$("#menu_ma_carte").fancybox({
			'autoScale' : false,
			'overlayOpacity'      : 0.8, // Set opacity to 0.8
			'overlayColor'        : "#000000", // Set color to Black
			'padding'         : 5,
			'width'         : 650,
			'height'		: 850,
			'transitionIn'     : 'elastic',
			'transitionOut'     : 'elastic',
			'type'          : 'iframe',
			'onClosed':function () {
			  window.location.reload();
			}
		});
	
		$(".fancybox_villes_privicarte").fancybox();
		$("#menu_villes_privicarte").fancybox({
			autoScale : false,
			overlayOpacity      : 0.8, // Set opacity to 0.8
			overlayColor        : "#000000", // Set color to Black
			padding         : 5,
			width         : 800,
			height        : 800,
			transitionIn      : 'elastic',
			transitionOut     : 'elastic',
			type          : 'iframe',
		});
		
		$(".fancybox_magazine_sortez").fancybox();
		$("#menu_magazine_sortez").fancybox({
			autoScale : false,
			overlayOpacity      : 0.8, // Set opacity to 0.8
			overlayColor        : "#000000", // Set color to Black
			padding         : 5,
			width         : 800,
			height        : 800,
			transitionIn      : 'elastic',
			transitionOut     : 'elastic',
			type          : 'iframe',
		});
		
		
	});
	</script>

<div class="container" style="height:60px;">
<!--<img src="<?php //echo GetImagePath("privicarte/") ; ?>/test/menu_main_pvc.png">-->

<?php
$thisss_ =& get_instance();
$thisss_->load->library('ion_auth');

$this_session_localdata =& get_instance();
$this_session_localdata->load->library('session');
$publightbox_email = $this_session_localdata->session->userdata('publightbox_email');
?>

<ul class="main_menu_navigation">
<li><a href="<?php echo site_url();?>">Page d'accueil</a></li>
<li><a href="/annonce-infos.html" id="menu_fonctionnement" class="fancybox_fonctionnement fancybox.iframe">Le fonctionnement</a></li>
<li>
<?php if ($thisss_->ion_auth->logged_in()) {?>
<a href="<?php echo site_url("auth/login");?>" target="_blank">Ma Carte</a>
<?php } else { ?>
	<?php if (isset($publightbox_email) && $publightbox_email != "") { ?>
    	<a href="<?php echo site_url("publightbox/carte");?>">Ma Carte</a>
    <?php } else { ?>
	    <a href="<?php echo site_url("auth/login");?>?pop_view=1" id="menu_ma_carte" class="fancybox_ma_carte fancybox.iframe">Ma Carte</a>
    <?php } ?>
<?php } ?>
</li>

<li>
<?php if ($thisss_->ion_auth->logged_in()) {?>
<a href="<?php echo site_url("auth/login");?>" target="_blank">Mon compte pro</a>
<?php } else { ?>
<a href="<?php echo site_url("auth/login");?>?pop_view=1" id="menu_mon_compte" class="fancybox_mon_compte fancybox.iframe">Mon compte pro</a>
<?php } ?>
</li>

</ul>
</div>