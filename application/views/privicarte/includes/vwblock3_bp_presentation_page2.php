<div class="row pl-3 pt-4 pb-4 pr-2" style="width: 100%">

    <div class="ml-4" style="width: 100%;">

        <div class="col-sm-12 p-4 sautligne" style="background-image: url('<?php echo base_url('assets/img/bonplan_new.png') ?>');background-repeat: no-repeat;width: 100%;height: 250px;background-size: 100% 100%;">

            <div class="d-flex justify-content-center" style="padding-left: 140px;width: 50%;padding-top: 20px">
                <?php if (isset($objbloc_info->bonplan_id_3_page2) AND $objbloc_info->bonplan_id_3_page2 > 0 AND $objbloc_info->bonplan_id_3_page2 != null AND $objbloc_info->bonplan_id_3_page2 != "") {
                    $this->load->model('mdlbonplan');
                    $bonplan_contenue = $this->mdlbonplan->get_bonplan_by_id($objbloc_info->bonplan_id_3_page2);
                } ?>
                <?php if (isset($bonplan_contenue) AND $bonplan_contenue != null) { ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <h4 style="color:white"><?php echo $bonplan_contenue->bonplan_titre; ?></h4>
                        </div>
                        <script type="text/javascript">
                            function compte_a_rebours_3<?php echo $objbloc_info->bonplan_id_3_page2; ?>() {
                                var compte_a_rebours_3<?php echo $objbloc_info->bonplan_id_3_page2; ?> = document.getElementById("compte_a_rebours_3<?php echo $objbloc_info->bonplan_id_3_page2; ?>");

                                var date_actuelle = new Date();
                                var date_evenement = new Date("<?php if ($bonplan_contenue->bonplan_date_fin !=''){ echo $bonplan_contenue->bonplan_date_fin; }elseif ($bonplan_contenue->bp_unique_date_fin !=''){echo $bonplan_contenue->bp_unique_date_fin;}elseif ($bonplan_contenue->bp_multiple_date_fin !=''){echo $bonplan_contenue->bp_multiple_date_fin;} ?>");
                                var total_secondes = (date_evenement - date_actuelle) / 1000;

                                var prefixe = "";
                                if (total_secondes < 0) {
                                    prefixe = "Exp "; // On modifie le préfixe si la différence est négatif

                                    total_secondes = Math.abs(total_secondes); // On ne garde que la valeur absolue

                                }

                                if (total_secondes > 0) {
                                    var jours = Math.floor(total_secondes / (60 * 60 * 24));
                                    var heures = Math.floor((total_secondes - (jours * 60 * 60 * 24)) / (60 * 60));
                                    minutes = Math.floor((total_secondes - ((jours * 60 * 60 * 24 + heures * 60 * 60))) / 60);
                                    secondes = Math.floor(total_secondes - ((jours * 60 * 60 * 24 + heures * 60 * 60 + minutes * 60)));

                                    var et = "et";
                                    var mot_jour = "jours";
                                    var mot_heure = "heures,";
                                    var mot_minute = "minutes,";
                                    var mot_seconde = "secondes";

                                    if (jours == 0) {
                                        jours = '';
                                        mot_jour = '';
                                    }
                                    else if (jours == 1) {
                                        mot_jour = "jour,";
                                    }

                                    if (heures == 0) {
                                        heures = '';
                                        mot_heure = '';
                                    }
                                    else if (heures == 1) {
                                        mot_heure = "heure,";
                                    }

                                    if (minutes == 0) {
                                        minutes = '';
                                        mot_minute = '';
                                    }
                                    else if (minutes == 1) {
                                        mot_minute = "minute,";
                                    }

                                    if (secondes == 0) {
                                        secondes = '';
                                        mot_seconde = '';
                                        et = '';
                                    }
                                    else if (secondes == 1) {
                                        mot_seconde = "seconde";
                                    }

                                    if (minutes == 0 && heures == 0 && jours == 0) {
                                        et = "";
                                    }

                                    //compte_a_rebours.innerHTML = prefixe + jours + ' ' + mot_jour + ' ' + heures + ' ' + mot_heure + ' ' + minutes + ' ' + mot_minute + ' ' + et + ' ' + secondes + ' ' + mot_seconde;
                                    //compte_a_rebours_3<?php //echo $objbloc_info->bonplan_id_3_page2 ; ?>.innerHTML = prefixe + jours + ' ' + mot_jour + ' ' + heures + ':' + minutes + ':' + secondes ;
                                    compte_a_rebours_3<?php echo $objbloc_info->bonplan_id_3_page2; ?>.innerHTML =  '<div class="margintop10">'+'<span style="color:white; padding-right: 10px">'+prefixe + jours + ' ' + mot_jour +'</span><span class="noirblanc">' + heures + '</span>:<span class="noirblanc">' + minutes + '</span>:<span class="noirblanc">' + secondes + '</span></div>';
                                }
                                else {
                                    compte_a_rebours_3<?php echo $objbloc_info->bonplan_id_3_page2; ?>.innerHTML = 'Expiré !';
                                    //alert(date_actuelle+ " "+date_evenement);
                                }

                                var actualisation = setTimeout("compte_a_rebours_3<?php echo $objbloc_info->bonplan_id_3_page2; ?>();", 1000);
                            }
                        </script>
                        <div style="font-size:30px" class="col-sm-12 " id="compte_a_rebours_3<?php echo $objbloc_info->bonplan_id_3_page2; ?>"></div>
                    </div>
                    <script type="text/javascript">
                        compte_a_rebours_3<?php echo $objbloc_info->bonplan_id_3_page2; ?>();
                    </script>
                <?php } ?>
            </div>
        </div>
    </div>
    
    <?php if (isset($objbloc_info->bonplan_content_3_page2) AND $objbloc_info->bonplan_content_3_page2 != null) { ?>
        <div class="col-sm-12 pl-4 text-justify">
            <p><?php echo $objbloc_info->bonplan_content_3_page2 ?></p>
        </div>
    <?php } ?>


    <?php if (isset($objbloc_info->bonplan_content_3_page2) AND $objbloc_info->bonplan_content_3_page2 != '' AND $objbloc_info->bonplan_content_3_page2 != null) { ?>

        <?php if (isset($objbloc_info->is_activ_btn_bp_3_page2) AND $objbloc_info->is_activ_btn_bp_3_page2 == '1' AND $objbloc_info->is_activ_btn_bp_3_page2 != null) {
            if ($objbloc_info->bp_existed_link_3_page2 == 'ag') {
                $linkq3 = base_url() . $oInfoCommercant->nom_url . '/agenda';
            } elseif ($objbloc_info->bp_existed_link_3_page2 == 'art') {
                $linkq3 = base_url() . $oInfoCommercant->nom_url . '/article';
            } elseif ($objbloc_info->bp_existed_link_3_page2 == 'p1') {
                $linkq3 = base_url() . $oInfoCommercant->nom_url . '/infos';
            } elseif ($objbloc_info->bp_existed_link_3_page2 == 'p2') {
                $linkq3 = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
            } elseif ($objbloc_info->bp_existed_link_3_page2 == 'bp') {
                $linkq3 = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
            } elseif ($objbloc_info->bp_existed_link_3_page2 == 'fd') {
                $linkq3 = $link_fidelity;
            } elseif ($objbloc_info->bp_existed_link_3_page2 == 'bt') {
                $linkq3 = base_url() . $oInfoCommercant->nom_url . '/annonces';
            } elseif (isset($objbloc_info->bp_custom_link_3_page2) AND $objbloc_info->bp_custom_link_3_page2 != '' AND $objbloc_info->bp_custom_link_3_page2 != null) {
                $linkq3 = 'https://' . $objbloc_info->bp_custom_link_3_page2;
            }

            ?>
            <div class="pt-3 pb-3" style="width: 100%">
                <?php if (isset($objbloc_info->bloc3_islightbox_page2bp_page2) AND $objbloc_info->bloc3_islightbox_page2bp_page2=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkq3 ?? '' ?>"  class="fancybox.iframe"><div class="bouton2"><?php echo $objbloc_info->btn_bp_content_3_page2 ?></div></a>
                <?php }else{  ?>
                    <a class="lienBouton2" href="<?php echo ($linkq3) ?? '' ?>">
                        <div class="bouton2"><?php echo $objbloc_info->btn_bp_content_3_page2 ?></div>
                    </a>
                <?php } ?>
            </div>
        <?php } ?>
    <?php } ?>

</div>