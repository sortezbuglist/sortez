<?php 
$thisss =& get_instance();
$thisss->load->library('ion_auth');
?>
<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="keywords"
          content="sorties, loisirs, concerts, spectacles, théâtres, alpes-maritimes, monaco, menton, nice, cannes, antibes, Théâtres, concerts, spectacles, animations, magazine, sortez, sortir, cannes, alpes-maritimes">
    <meta name="description"
          content="Le Magazine Sortez : l'essentiel des sorties et des loisirs sur les Alpes-Maritimes et Monaco, animations, concerts, spectacles, vie locale...">
    <meta name="author" content="Priviconcept">

    <?php 
    // var_dump($zTitle);
    // var_dump($zDescription);
    // var_dump($zMetaImage);
    ?>
    <meta property="fb:app_id" content="324903384595223"/>
    <meta property="og:locale" content="fr_FR"/>
    <meta property="og:title"
          content="<?php if (isset($zTitle) && $zTitle != "" && $zTitle != NULL) echo $zTitle; else echo "Le Magazine Sortez"; ?>"/>
    <meta property="og:description"
          content="<?php if (isset($zDescription) && $zDescription != "" && $zDescription != NULL) echo $zDescription; else echo "Sortez : L'essentiel des sorties et des loisirs Alpes-Maritimes et Monaco"; ?>"/>
    <meta property="og:url"
          content='<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>'/>
    <meta property="og:site_name"
          content="Le Magazine Sortez : L'essentiel des sorties et des loisirs Alpes-Maritimes et Monaco"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image"
          content="<?php if (isset($zMetaImage) && $zMetaImage != "" && $zMetaImage != NULL) echo $zMetaImage; else echo base_url()."application/resources/sortez/images/logo.png"; ?>"/>

    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:site" content="@Sortez_org"/>
    <meta name="twitter:creator" content="@techsortez">
    <meta name="twitter:title"
          content="<?php if (isset($zTitle) && $zTitle != "" && $zTitle != NULL) echo $zTitle; else echo "Le Magazine Sortez"; ?>"/>
    <meta name="twitter:description"
          content="<?php if (isset($zDescription) && $zDescription != "" && $zDescription != NULL) echo $zDescription; else echo "Sortez : L'essentiel des sorties et des loisirs Alpes-Maritimes et Monaco"; ?>"/>
    <meta name="twitter:image"
          content="<?php if (isset($zMetaImage) && $zMetaImage != "" && $zMetaImage != NULL) echo $zMetaImage; else echo base_url()."application/resources/sortez/images/logo.png"; ?>"/>

    <link rel="icon" href="<?php echo base_url();?>application/views/vivresavilleView/ville-test/favicon.ico">
    <link href="<?php echo GetCssPath("privicarte/"); ?>/responsive_max_575.css" rel="stylesheet" type="text/css">
    <link href="<?php echo GetCssPath("privicarte/"); ?>/responsive_min_768.css" rel="stylesheet" type="text/css">
    <link href="<?php echo GetCssPath("privicarte/"); ?>/responsive_min_992.css" rel="stylesheet" type="text/css">
    <link href="<?php echo GetCssPath("privicarte/"); ?>/responsive_min_1200.css" rel="stylesheet" type="text/css">
    <?php if (!isset($data)) $data['data_init'] = true; ?>

    <title><?php if (isset($zTitle) && $zTitle != "" && $zTitle != NULL) echo $zTitle; else echo "Privicarte"; ?></title>

    <?php
    if (isset($oInfoCommercant)) {
        $thisss->load->model("ion_auth_used_by_club");
        $user_ion_auth_id = $thisss->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);
        if (isset($user_ion_auth_id)) $user_groups = $thisss->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
        if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;
        //echo $nombre_annonce_com."qsdfqsdfffff".$group_id_commercant_user;
    } else {
        $group_id_commercant_user = 0;
    }
    ?>

    <!-- Bootstrap core CSS
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    -->
    <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <!--lightbox--->
    <style type="text/css">
        .fancybox-slide--iframe .fancybox-content {
            width  : 800px;
            height : 600px;
            max-width  : 80%;
            max-height : 80%;
            margin: 0;
            background: #191919;
        }
    </style>
    <script>
        /*$('.fancybox').fancybox({
            toolbar  : false,
            smallBtn : true,
            iframe : {
                preload : false
            }
        });*/
    </script>
    <!--<link  href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.min.css" rel="stylesheet">-->
    <link  href="<?php echo base_url();?>assets/css/fancy.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.min.js"></script>

    <!--slides--->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/easy_slides/jquery.easy_slides.css"/>
    <script src="<?php echo base_url();?>assets/easy_slides/jquery.easy_slides.js"></script>

    <script>
        $(document).ready(function() {
            $('.slider_circle_10').EasySlides({
                'autoplay': true,
                'show': 13
            })
        });
    </script>

    <script type="text/javascript" src="<?php echo GetJsPath("privicarte/"); ?>/global.js"></script>

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script type="text/javascript" src="<?php echo GetJsPath("privicarte/"); ?>/ie-emulation-modes-warning.js"></script>
    <!--loupe slide !-->
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/loupe.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php if (isset($currentpage) && $currentpage == "publightbox") {
    } else $thisss->load->view("privicarte/includes/global_js", $data); ?>

    <?php $thisss->load->view("privicarte/includes/global_css", $data); ?>


    <link href="<?php echo GetCssPath("privicarte/"); ?>/global.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/global_premium.css" rel="stylesheet" type="text/css">

    <?php
    //LOCALDATA FILTRE
    $this_session_localdata =& get_instance();
    $this_session_localdata->load->library('session');
    $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
    $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
    //LOCALDATA FILTRE
    ?>

    <?php if ($group_id_commercant_user == 5) { ?>
        <!--<link href="<?php echo GetCssPath("privicarte/"); ?>/navbar-fixed-top.css" rel="stylesheet" type="text/css">-->

        <?php
        $path_img_gallery_bg = 'application/resources/front/photoCommercant/imagesbank/' . $oInfoCommercant->user_ionauth_id . '/bg/';
        $path_img_gallery_bg_sample = 'application/resources/front/photoCommercant/imagesbank/bg_sample/';
        $path_img_gallery_old = 'application/resources/front/photoCommercant/images/';
        $filename = "";
        if (file_exists($path_img_gallery_bg . $oInfoCommercant->background_image)) {
            $background_image_to_show = base_url() . $path_img_gallery_bg . $oInfoCommercant->background_image;
            $filename = $path_img_gallery_bg . $oInfoCommercant->background_image;
        } else if (file_exists($path_img_gallery_bg_sample . $oInfoCommercant->background_image)) {
            $background_image_to_show = base_url() . $path_img_gallery_bg_sample . $oInfoCommercant->background_image;
            $filename = $path_img_gallery_bg_sample . $oInfoCommercant->background_image;
        } else {
            $background_image_to_show = base_url() . $path_img_gallery_old . $oInfoCommercant->background_image;
            $filename = $path_img_gallery_old . $oInfoCommercant->background_image;
        }

        if (isset($oInfoCommercant->background_image) &&
            $oInfoCommercant->background_image != "" &&
            $oInfoCommercant->background_image != NULL &&
            file_exists($filename) &&
            $oInfoCommercant->bg_default_image != "1") { ?>
            <style type="text/css">
                body {
                    background-image: url("<?php echo $background_image_to_show; ?>") !important;
                    background-repeat: no-repeat !important;
                    background-attachment: fixed !important;
                    background-size: 100% 100%;
                }
            </style>
        <?php } ?>


    <?php } else if ((isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0" && $localdata_IdVille != NULL && !isset($oInfoCommercant)) || (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0" && $localdata_IdDepartement != NULL && !isset($oInfoCommercant))) { ?>

        <style type="text/css">
            body {
                margin-top: 0 !important;
                background: #FFFFFF !important;
            }
        </style>

    <?php } ?>


    <?php
    if ((isset($oInfoCommercant->bg_default_color_container) && $oInfoCommercant->bg_default_color_container == "1" && $group_id_commercant_user == 5) || $group_id_commercant_user == 4) { ?>
        <style type="text/css">
            .bg_gris_225 {
                background-color: #E1E1E1 !important;
            }
        </style>
    <?php } ?>


    <style type="text/css">
        .link_button {
            background-color: #006699;
            border: 2px solid #003366;
            border-radius: 8px;
            color: #ffffff;
            font-size: 12px;
            padding: 10px 15px;
        }

        <?php if (!isset($oInfoCommercant) || !isset($group_id_commercant_user)) {?>
        body {
            margin-top: 50px;
        }

        <?php }  ?>

        <?php if (isset($pagecategory) && $pagecategory == 'admin_commercant') { ?>
        body {
            background-color: #f6f6f6 !important;
            background-image: none !important;
        }

        <?php } ?>


        .ulmenu2013 li {
            list-style: none outside none;
            display: block;
            float: left;
            width: 155px;
            /*background-image: url(<?php echo GetImagePath("privicarte/"); ?>/background_menu_part.png);*/
            /*background-position: right top;*/
            border-right: dotted;
            background-repeat: no-repeat;
            background-size: auto 85%;
            height: auto;
            text-align: center;
            vertical-align: middle;
            margin-top:15px;
            margin-bottom:15px;
        <?php if (isset($group_id_commercant_user) && ($group_id_commercant_user == 4 || $group_id_commercant_user == 5)) { ?>
            padding: 9px;
        <?php } else { ?>
            padding-top: 13px;
        <?php } ?>
        }
        .ulmenu2013 li > a {
            color:#FFFFFF;
        }
        .navmen{
            height: 40px;
            text-align: center;
            border-width: 2px !important;
        }
        @media screen and (min-width: 992px) {
            .navmenk{
                border-right: dotted;
                text-align: center;
                border-width: 2px !important;
            }
        }
        .navmenkl{
            text-align: center;
            border-width: 2px !important;
        }
        @media (min-width: 1200px){
            .container, .container_pro {
                width: 1250px !important;
                max-width: 1250px !important;
            }
          }
        .titre_glissiere1{
            background-color: <?php if (isset($oInfoCommercant->bandeau_color) AND $oInfoCommercant->bandeau_color !=null ){echo $oInfoCommercant->bandeau_color.'!important';}else{echo "3B579D!important";} ?>;
        }
        .row:before, .row:after {display: none !important;}
    </style>

    <meta name="google-translate-customization" content="ad114fb2f5d60b29-e2bf26e865a3d116-ga4de9321d692a0e0-29"></meta>


</head>

<body>
<?php
$thisss=$thisss =& get_instance();
$thisss->load->Model("mdl_card_tampon");
$thisss->load->Model("mdl_card_capital");
$thisss->load->Model("mdl_card_remise");
$thisss->load->Model("Mdl_reservation");

$objTampon = $thisss->mdl_card_tampon->getByIdCommercant($oInfoCommercant->IdCommercant);
$objCapital = $thisss->mdl_card_capital->getByIdCommercant($oInfoCommercant->IdCommercant);
$objRemise = $thisss->mdl_card_remise->getByIdCommercant($oInfoCommercant->IdCommercant);

if (!empty($objTampon) OR !empty($objCapital) OR !empty($objRemise)){
    $idcom=$oInfoCommercant->IdCommercant;

    if (isset($objTampon->is_activ) AND $objTampon->is_activ=='1'){
        $fidelityid=$objTampon->id;
    }

    if (isset($objCapital->id) AND $objCapital->is_activ=='1'){
        $fidelityid=$objCapital->id;
    }

    if (isset($objRemise->id) AND $objRemise->is_activ=='1'){
        $fidelityid=$objRemise->id;
    }

    if (isset($objTampon->is_activ) AND $objTampon->is_activ=='1'){
        $_iTypeFidelity='tampon';
    }

    if (isset($objCapital->id) AND $objCapital->is_activ=='1'){
        $_iTypeFidelity='capital';
    }

    if (isset($objRemise->id) AND $objRemise->is_activ=='1'){
        $_iTypeFidelity='remise';
    }
}

if((isset($_iTypeFidelity) AND $_iTypeFidelity != null) AND (isset($fidelityid) AND $fidelityid != null)){
    $link_fidelity=site_url().$oInfoCommercant->nom_url.'/notre_fidelisation/'.$_iTypeFidelity.'/'.$fidelityid;
    $data['link_fidelity']=$link_fidelity;
}
?>
<?php $thisss->load->view("privicarte/includes/facebook_login_js", $data); ?>
  
  
  
  
  
  
