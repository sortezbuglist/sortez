<style type="text/css">
/*responsive mobile version*********************************************************************/
@media (max-width: 479px) {
.nv_button_link_privicarte_mobile a {padding: 0;}
}
@media (min-width: 480px) {
.nv_button_link_privicarte_mobile a {padding:15px 20%;}
}
.nv_button_link_privicarte_mobile div {
background-image:url(<?php echo GetImagePath("privicarte/"); ?>/mobile/bg_btn_link_menu_mobile.png);
background-repeat:no-repeat;
background-position:center center;
}
.nv_button_link_privicarte_mobile div.separator {
background-image: none !important;
padding:0 !important;
margin:0 !important;
}
.nv_button_link_privicarte_mobile a {
/*background-color:#000000;*/
border-radius: 15px;
text-decoration:none;
color:#FFFFFF;
font-family: Arial, Helvetica, sans-serif;
font-style: normal;
font-weight: normal;
text-decoration: none;
font-variant: normal;
font-size: 20.0px;
vertical-align: 0;
}
.nv_button_link_privicarte_mobile div { padding:15px 0;}
.nv_button_link_privicarte_mobile .separator { height:0px;}
.nv_button_link_privicarte_mobile a:hover { text-decoration:none;}
/*responsive mobile version*********************************************************************/

</style>