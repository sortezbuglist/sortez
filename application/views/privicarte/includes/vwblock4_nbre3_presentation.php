<div class="row" style="">
    <div class="col-sm-4 text-center text-justify pr-0">
        <div class="ml-4 pb-4 pt-4 sautligne allign_button content_sm_block content_right_padd" style="height:auto;padding-right:0px;padding-left:0px;">
            <?php if (isset($glisieres->bloc_1_4_image1_content) AND $glisieres->bloc_1_4_image1_content != '' AND $glisieres->bloc_1_4_image1_content != null) { ?>
                <div class="image">
                    <img style="max-width: 100%;height: auto;"
                         src="<?php echo base_url($path_img_gallery_gl . '/' . $glisieres->bloc_1_4_image1_content) ?>">
                </div>
            <?php } ?>
            <?php if (isset($glisieres->bloc1_4_content) AND $glisieres->bloc1_4_content != '' AND $glisieres->bloc1_4_content != null) { ?>
                <div class="content pt-4 pb-0 content_caract"><?php echo htmlspecialchars_decode($glisieres->bloc1_4_content) ?></div>
            <?php } ?>
            <?php if (isset($glisieres->is_activ_btn_bloc1_4) AND $glisieres->is_activ_btn_bloc1_4 == '1' AND $glisieres->is_activ_btn_bloc1_4 != null) {
                if ($glisieres->bloc1_4_existed_link1 == 'ag') {
                    $linkk = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($glisieres->bloc1_4_existed_link1 == 'art') {
                    $linkk = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($glisieres->bloc1_4_existed_link1 == 'p1') {
                    $linkk = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($glisieres->bloc1_4_existed_link1 == 'p2') {
                    $linkk = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($glisieres->bloc1_4_existed_link1 == 'bp') {
                    $linkk = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($glisieres->bloc1_4_existed_link1 == 'fd') {
                    $linkk = $link_fidelity;
                } elseif ($glisieres->bloc1_4_existed_link1 == 'bt') {
                    $linkk = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif ($glisieres->bloc1_4_existed_link1 == 'cmd') {
                    $linkk = base_url() . $oInfoCommercant->nom_url . '/commandes_commercants';
                } elseif (isset($glisieres->bloc1_4_custom_link1) AND $glisieres->bloc1_4_custom_link1 != '' AND $glisieres->bloc1_4_custom_link1 != null) {
                    $linkk = 'https://' . $glisieres->bloc1_4_custom_link1;
                }
                ?>
                <?php if (isset($glisieres->bloc4_islightbox) AND $glisieres->bloc4_islightbox=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkk ?? '' ?>"  class="fancybox.iframe lienbtn3"><div class="bouton1"><?php echo $glisieres->btn_bloc1_4_content1 ?></div></a>
                <?php }else{  ?>
                    <a class="pb-5 pt-5 lienBouton " href="<?php echo ($linkk) ?? '' ?>">
                        <div class="bouton1"><?php echo $glisieres->btn_bloc1_4_content1 ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <div class="col-sm-4 text-center pl-0 text-justify content_sm_nb2 content_right_padd">
        <div class="p-4 sautligne allign_button content_sm_nb2ct" style=";height:auto;padding-right:0px!important">
            <?php if (isset($glisieres->bloc_1_4_image2_content) AND $glisieres->bloc_1_4_image2_content != '' AND $glisieres->bloc_1_4_image2_content != null) { ?>
                <div class="image">
                    <img style="max-width: 100%;height: auto;"
                         src="<?php echo base_url($path_img_gallery_gl . '/' . $glisieres->bloc_1_4_image2_content) ?>">
                </div>
            <?php } ?>
            <?php if (isset($glisieres->bloc1_4_content2) AND $glisieres->bloc1_4_content2 != '' AND $glisieres->bloc1_4_content2 != null) { ?>
                <div class="content pt-4 pb-0 content_caract"><?php echo htmlspecialchars_decode($glisieres->bloc1_4_content2) ?></div>
            <?php } ?>
            <?php if (isset($glisieres->is_activ_btn_bloc2_4) AND $glisieres->is_activ_btn_bloc2_4 == '1' AND $glisieres->is_activ_btn_bloc2_4 != null) {
                if ($glisieres->bloc1_4_existed_link2 == 'ag') {
                    $linkl = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($glisieres->bloc1_4_existed_link2 == 'art') {
                    $linkl = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($glisieres->bloc1_4_existed_link2 == 'p1') {
                    $linkl = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($glisieres->bloc1_4_existed_link2 == 'p2') {
                    $linkl = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($glisieres->bloc1_4_existed_link2 == 'bp') {
                    $linkl = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($glisieres->bloc1_4_existed_link2 == 'fd') {
                    $linkl = $link_fidelity;
                } elseif ($glisieres->bloc1_4_existed_link2 == 'bt') {
                    $linkl = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif ($glisieres->bloc1_4_existed_link2 == 'cmd') {
                    $linkl = base_url() . $oInfoCommercant->nom_url . '/commandes_commercants';
                } elseif (isset($glisieres->bloc1_4_custom_link2) AND $glisieres->bloc1_4_custom_link2 != '' AND $glisieres->bloc1_4_custom_link2 != null) {
                    $linkl = 'https://' . $glisieres->bloc1_4_custom_link2;
                }

                ?>
                <?php if (isset($glisieres->bloc4_islightbox2) AND $glisieres->bloc4_islightbox2=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkl ?? '' ?>"  class="fancybox.iframe lienbtn3"><div class="bouton1"><?php echo $glisieres->btn_bloc1_4_content2 ?></div></a>
                <?php }else{  ?>
                    <a class="pb-5 pt-5 lienBouton " href="<?php echo ($linkl) ?? '' ?>">
                        <div class="bouton1"><?php echo $glisieres->btn_bloc1_4_content2 ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <!-- bloc 3 home debut --->
    <div class="col-sm-4 text-center pl-0 text-justify content_sm_nb2 content_right_padd">
        <div class="p-4 sautligne allign_button content_sm_nb2ct" style=";height:auto;padding-right:0px!important">
            <?php if (isset($glisieres->bloc_1_4_image3_content) AND $glisieres->bloc_1_4_image3_content != '' AND $glisieres->bloc_1_4_image3_content != null) { ?>
                <div class="image">
                    <img style="max-width: 100%;height: auto;"
                         src="<?php echo base_url($path_img_gallery_gl . '/' . $glisieres->bloc_1_4_image3_content) ?>">
                </div>
            <?php } ?>
            <?php if (isset($glisieres->bloc1_4_content3) AND $glisieres->bloc1_4_content3 != '' AND $glisieres->bloc1_4_content3 != null) { ?>
                <div class="content pt-4 pb-0 content_caract"><?php echo htmlspecialchars_decode($glisieres->bloc1_4_content3) ?></div>
            <?php } ?>
            <?php if (isset($glisieres->is_activ_btn_bloc3_4) AND $glisieres->is_activ_btn_bloc3_4 == '1' AND $glisieres->is_activ_btn_bloc3_4 != null) {
                if ($glisieres->bloc1_4_existed_link3 == 'ag') {
                    $linkl3 = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($glisieres->bloc1_4_existed_link3 == 'art') {
                    $linkl3 = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($glisieres->bloc1_4_existed_link3 == 'p1') {
                    $linkl3 = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($glisieres->bloc1_4_existed_link3 == 'p2') {
                    $linkl3 = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($glisieres->bloc1_4_existed_link3 == 'bp') {
                    $linkl3 = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($glisieres->bloc1_4_existed_link3 == 'fd') {
                    $linkl3 = $link_fidelity;
                } elseif ($glisieres->bloc1_4_existed_link3 == 'bt') {
                    $linkl3 = base_url() . $oInfoCommercant->nom_url . '/annonces';
                 } elseif ($glisieres->bloc1_4_existed_link3 == 'cmd') {
                    $linkl3 = base_url() . $oInfoCommercant->nom_url . '/commandes_commercants';
                } elseif (isset($glisieres->bloc1_4_custom_link3) AND $glisieres->bloc1_4_custom_link3 != '' AND $glisieres->bloc1_4_custom_link3 != null) {
                    $linkl3 = 'https://' . $glisieres->bloc1_4_custom_link3;
                }

                ?>
                <?php if (isset($glisieres->bloc4_islightbox3) AND $glisieres->bloc4_islightbox3=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkl3 ?? '' ?>"  class="fancybox.iframe lienbtn3"><div class="bouton1"><?php echo $glisieres->btn_bloc1_4_content3 ?></div></a>
                <?php }else{  ?>
                    <a class="pb-5 pt-5 lienBouton " href="<?php echo ($linkl3) ?? '' ?>">
                        <div class="bouton1"><?php echo $glisieres->btn_bloc1_4_content3 ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <!-- bloc 3 home fin --->
</div>