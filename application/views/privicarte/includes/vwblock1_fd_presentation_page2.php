<div class="row pl-3 pt-4 pb-4 pr-2" style="width: 100%">

    <div style="width: 100%" class="ml-4 mb-4">

        <div class="col-sm-12 p-4" style="background-image: url('<?php echo base_url('assets/img/fidelity_new.png') ?>');background-repeat: no-repeat;width: 100%;height: 250px;background-size: 100% 100%;position: relative;">

            <div class="d-flex" style="width: 50%;padding-left: 135px;align-items: center;color: white;position: absolute;

  top: 50%; /* poussé de la moitié de hauteur du référent */

  transform: translateY(-50%);">
                <?php if (isset($objbloc_info->fidelity_id_page2) AND $objbloc_info->fidelity_id_page2 > 0) {
                    $thisz = get_instance();
                    $thisz->load->model('mdlfidelity');
                    $fidelity_contenue = $thisz->mdlfidelity->get_annonce_by_id($objbloc_info->fidelity_id_page2, $oInfoCommercant->IdCommercant);
                } ?>
                <?php if (isset($fidelity_contenue) AND $fidelity_contenue != null) { ?>
                    <p><?php echo $fidelity_contenue->description; ?></p>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="col-sm-12 text-center text-justify pl-0 pr-0">

        <?php if (isset($objbloc_info->fidelity_content_page2) AND $objbloc_info->fidelity_content_page2 != '' AND $objbloc_info->fidelity_content_page2 != null) { ?>
            <div class="" style="">
                <div class="pl-4 pr-4 text-justify"><?php echo htmlspecialchars_decode($objbloc_info->fidelity_content_page2) ?></div>
            </div>
        <?php } ?>

            <?php if (isset($objbloc_info->is_activ_btn_fd_page2) AND $objbloc_info->is_activ_btn_fd_page2 == '1' AND $objbloc_info->is_activ_btn_fd_page2 != null) {
                if ($objbloc_info->FD_existed_link_page2 == 'ag') {
                    $linkq = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($objbloc_info->FD_existed_link_page2 == 'art') {
                    $linkq = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($objbloc_info->FD_existed_link_page2 == 'p1') {
                    $linkq = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($objbloc_info->FD_existed_link_page2 == 'p2') {
                    $linkq = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($objbloc_info->FD_existed_link_page2 == 'bp') {
                    $linkq = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($objbloc_info->FD_existed_link_page2 == 'fd') {
                    $linkq = $link_fidelity;
                } elseif ($objbloc_info->FD_existed_link_page2 == 'bt') {
                    $linkq = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif (isset($objbloc_info->fd_custom_link_page2) AND $objbloc_info->fd_custom_link_page2 != '' AND $objbloc_info->fd_custom_link_page2 != null) {
                    $linkq = 'https://' . $objbloc_info->fd_custom_link_page2;
                }
                ?>

                <?php if (isset($objbloc_info->bloc1_islightbox_page2fd1) AND $objbloc_info->bloc1_islightbox_page2fd1=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkq ?? '' ?>"  class="fancybox.iframe p-4"><div class="bouton2"><?php echo $objbloc_info->btn_fd_content_page2 ?></div></a>
                <?php }else{  ?>
                    <a class="lienBouton2 p-3" href="<?php echo ($linkq) ?? '' ?>">
                        <div class="bouton2"><?php echo $objbloc_info->btn_fd_content_page2 ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
    </div>
</div>