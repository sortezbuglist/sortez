<?php
if (isset($oInfoCommercant->id_glissiere) AND $oInfoCommercant->id_glissiere != null) {
    $thi = get_instance();
    $glisieres = $this->load->mdlcommercant->get_glissiere_by_id_com($oInfoCommercant->IdCommercant);
}
?>
<?php
    if(isset($oCommercant->nom_url)){
        $commercant_url_nom = $oCommercant->nom_url;
        $commercant_url_home = $commercant_url_nom;
        $logo_url_to_show = $commercant_url_home;
    }
?>

<?php
$path_img_gallery_lg = 'application/resources/front/photoCommercant/imagesbank/' . $oInfoCommercant->user_ionauth_id . '/lg/';
$path_img_gallery_old = 'application/resources/front/photoCommercant/images/';
if (file_exists($path_img_gallery_lg . $oInfoCommercant->Logo))
    $logo_image_to_show = base_url() . $path_img_gallery_lg . $oInfoCommercant->Logo;
else if (file_exists($path_img_gallery_old . $oInfoCommercant->Logo))
    $logo_image_to_show = base_url() . $path_img_gallery_old . $oInfoCommercant->Logo;
else
    $logo_image_to_show = '';

if ($oInfoCommercant->Logo != "" && $oInfoCommercant->Logo != NULL) {
    ?>
    <div class="row align-items-center" style="margin: auto">
        <div class="sm-block mt-0"
             style="height:100%;display: block!important; width: 100%;<?php if (isset($oInfoCommercant->titre_entete) && $oInfoCommercant->titre_entete != "" && $oInfoCommercant->titre_entete != NULL) { ?>margin-top: 40px;<?php } else { ?><?php } ?>">
            <?php if (isset($logo_image_to_show) && $logo_image_to_show!='') { ?><img style="max-height: 210px;width: auto" class="img-fluid" src="<?php echo $logo_image_to_show; ?>"
                 alt="<?php echo $oInfoCommercant->NomSociete; ?>"/><?php } ?>
            <?php if (isset($oInfoCommercant->titre_entete) && $oInfoCommercant->titre_entete != "" && $oInfoCommercant->titre_entete != NULL) { ?>

            <?php if (isset($glisieres->affichageNomCommercant) AND $glisieres->affichageNomCommercant == 'o'){ ?>
                <div style="font-size: 20px;color:<?php if (isset($glisieres->couleurLogo) AND $glisieres->couleurLogo == 'b') {echo 'white';}else{echo 'black';} ?> ">
                    <?php echo $oInfoCommercant->NomSociete; ?>
                </div>
            <?php } ?>
                <div class="p-3" style="font-size: 20px;color: <?php if (isset($glisieres->couleurLogo) AND $glisieres->couleurLogo == 'b') {echo 'white';}else{echo 'black';} ?>">
                    <?php echo $oInfoCommercant->titre_entete; ?>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>


