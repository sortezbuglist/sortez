<div class="row" style=";width: 100%">
    <div style="width: 100%" class="ml-4 mb-4">
        <div class="col-sm-12 p-4 ml-4" style="background-image: url('<?php echo base_url('assets/img/fidelity_new.png') ?>');background-repeat: no-repeat;width: 100%;height: 250px;background-size: 100% 100%;position: relative;
                ">
            <div class="d-flex" style="width: 50%;padding-left: 135px;;color: white;position: absolute;
  top: 50%; /* poussé de la moitié de hauteur du référent */
  transform: translateY(-50%);">
                <?php if (isset($glisieres->fidelity_3_id) AND $glisieres->fidelity_3_id > 0) {
                    $thisz = get_instance();
                    $thisz->load->model('mdlfidelity');
                    $fidelity_contenue = $thisz->mdlfidelity->get_annonce_by_id($glisieres->fidelity_3_id, $oInfoCommercant->IdCommercant);
                } ?>
                <?php if (isset($glisieres->fidelity_3_id) AND $glisieres->fidelity_3_id > 0) {
                } ?>
                <?php if (isset($fidelity_contenue) AND $fidelity_contenue != null) { ?>
                    <p><?php echo $fidelity_contenue->description; ?></p>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-sm-12 text-center text-justify pl-0 pr-0 pb-3">

        <?php if (isset($glisieres->fidelity_3_content) AND $glisieres->fidelity_3_content != '' AND $glisieres->fidelity_3_content != null) { ?>
            <div class="ml-4" style="">
                <div class="pl-4 pr-4"><?php echo htmlspecialchars_decode($glisieres->fidelity_3_content) ?></div>
            </div>
        <?php } ?>
            <?php if (isset($glisieres->is_activ_btn_fd_3) AND $glisieres->is_activ_btn_fd_3 == '1' AND $glisieres->is_activ_btn_fd_3 != null) {
                if ($glisieres->FD_3_existed_link == 'ag') {
                    $linkq3 = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($glisieres->FD_3_existed_link == 'art') {
                    $linkq3 = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($glisieres->FD_3_existed_link == 'p1') {
                    $linkq3 = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($glisieres->FD_3_existed_link == 'p2') {
                    $linkq3 = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($glisieres->FD_3_existed_link == 'bp') {
                    $linkq3 = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($glisieres->FD_3_existed_link == 'fd') {
                    $linkq3 = $link_fidelity;
                } elseif ($glisieres->FD_3_existed_link == 'bt') {
                    $linkq3 = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif (isset($glisieres->fd_custom_3_link) AND $glisieres->fd_custom_3_link != '' AND $glisieres->fd_custom_3_link != null) {
                    $linkq3 = 'https://' . $glisieres->fd_custom_3_link;
                }

                ?>
                <?php if (isset($linkq3)) { ?>
                    <?php if (isset($glisieres->bloc3_islightboxfd) AND $glisieres->bloc3_islightboxfd=="1"){ ?>

                        <a data-fancybox data-type="iframe" data-src="<?php echo $linkq3 ?? '' ?>"  class="fancybox.iframe"><div class="bouton2"><?php echo $glisieres->btn_fd_3_content ?></div></a>
                    <?php }else{  ?>
                        <a class="lienBouton2" href="<?php echo ($linkq3) ?? '' ?>">
                            <div class="bouton2"><?php echo $glisieres->btn_fd_3_content ?></div>
                        </a>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
    </div>
</div>