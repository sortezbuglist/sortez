<div class="row" style=";width: 100%">
    <div style="width: 100%" class="ml-4 mb-4">
        <div class="col-sm-12 p-4 ml-4" style="background-image: url('<?php echo base_url('assets/img/fidelity_new.png') ?>');background-repeat: no-repeat;width: 100%;height: 250px;background-size: 100% 100%;position: relative;">
            <div class="d-flex" style="width: 50%;padding-left: 135px;color: white;position: absolute;
  top: 50%; /* poussé de la moitié de hauteur du référent */
  transform: translateY(-50%);">
                <?php if (isset($glisieres->fidelity_4_id) AND $glisieres->fidelity_4_id > 0) {
                    $thisz = get_instance();
                    $thisz->load->model('mdlfidelity');
                    $fidelity_contenue = $thisz->mdlfidelity->get_annonce_by_id($glisieres->fidelity_4_id, $oInfoCommercant->IdCommercant);
                } ?>
                <?php if (isset($glisieres->fidelity_4_id) AND $glisieres->fidelity_4_id > 0) {
                } ?>
                <?php if (isset($fidelity_contenue) AND $fidelity_contenue != null) { ?>
                    <p><?php echo $fidelity_contenue->description; ?></p>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-sm-12 text-center text-justify pl-0 pr-0">

        <?php if (isset($glisieres->fidelity_4_content) AND $glisieres->fidelity_4_content != '' AND $glisieres->fidelity_4_content != null) { ?>
            <div class="ml-4" style="">
                <div class="pl-4 pr-4"><?php echo htmlspecialchars_decode($glisieres->fidelity_4_content) ?></div>
            </div>
        <?php } ?>
        <?php if (isset($glisieres->is_activ_btn_bloc1_3) AND $glisieres->is_activ_btn_bloc1_3 == '1' AND $glisieres->is_activ_btn_bloc1_3 != null) {
            if ($glisieres->bloc1_4_existed_link1 == 'ag') {
                $linkl = base_url() . $oInfoCommercant->nom_url . '/agenda';
            } elseif ($glisieres->bloc1_4_existed_link1 == 'art') {
                $linkl = base_url() . $oInfoCommercant->nom_url . '/article';
            } elseif ($glisieres->bloc1_4_existed_link1 == 'p1') {
                $linkl = base_url() . $oInfoCommercant->nom_url . '/infos';
            } elseif ($glisieres->bloc1_4_existed_link1 == 'p2') {
                $linkl = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
            } elseif ($glisieres->bloc1_4_existed_link1 == 'bp') {
                $linkl = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
            } elseif ($glisieres->bloc1_4_existed_link1 == 'fd') {
                $linkl = $link_fidelity;
            } elseif ($glisieres->bloc1_4_existed_link1 == 'bt') {
                $linkl = base_url() . $oInfoCommercant->nom_url . '/annonces';
            }  elseif (isset($glisieres->bloc1_4_custom_link1) AND $glisieres->bloc1_4_custom_link1 != '' AND $glisieres->bloc1_4_custom_link1 != null) {
                $linkl = 'https://' . $glisieres->bloc1_4_custom_link1;
            }

            ?>
            <?php if (isset($glisieres->bloc4_islightboxfd) AND $glisieres->bloc4_islightboxfd=="1"){ ?>

                <a data-fancybox data-type="iframe" data-src="<?php echo $linkl ?? '' ?>"  class="fancybox.iframe"><div class="bouton2"><?php echo $glisieres->btn_fd_4_content ?></div></a>
            <?php }else{  ?>
                <a class="pb-5 pt-5 lienBouton2 " href="<?php echo ($linkl) ?? '' ?>">
                    <div class="bouton2"><?php echo $glisieres->btn_fd_4_content ?></div>
                </a>
            <?php } ?>
        <?php } ?>
    </div>
</div>