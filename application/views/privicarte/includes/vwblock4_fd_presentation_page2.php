<div class="row pl-3 pt-4 pb-4 pr-2" style="width: 100%">

    <div style="width: 100%" class="ml-4 mb-4">

        <div class="col-sm-12 p-4" style="background-image: url('<?php echo base_url('assets/img/fidelity_new.png') ?>');background-repeat: no-repeat;width: 100%;height: 250px;background-size: 100% 100%;position: relative;">

            <div class="d-flex" style="width: 50%;padding-left: 135px;align-items: center;color: white;position: absolute;

  top: 50%; /* poussé de la moitié de hauteur du référent */

  transform: translateY(-50%);">
                <?php if (isset($objbloc_info->fidelity_4_id_page2) AND $objbloc_info->fidelity_4_id_page2 > 0) {
                    $thisz = get_instance();
                    $thisz->load->model('mdlfidelity');
                    $fidelity_contenue = $thisz->mdlfidelity->get_annonce_by_id($objbloc_info->fidelity_4_id_page2, $oInfoCommercant->IdCommercant);
                } ?>
                <?php if (isset($objbloc_info->fidelity_4_id_page2) AND $objbloc_info->fidelity_4_id_page2 > 0) {
                } ?>
                <?php if (isset($fidelity_contenue) AND $fidelity_contenue != null) { ?>
                    <p><?php echo $fidelity_contenue->description; ?></p>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-sm-12 text-center text-justify pl-0 pr-0">

        <?php if (isset($objbloc_info->fidelity_4_content_page2) AND $objbloc_info->fidelity_4_content_page2 != '' AND $objbloc_info->fidelity_4_content_page2 != null) { ?>
            <div class="" style="">
                <div class="pl-4 pr-4 text-justify"><?php echo htmlspecialchars_decode($objbloc_info->fidelity_4_content_page2) ?></div>
            </div>
        <?php } ?>
        <?php if (isset($objbloc_info->is_activ_btn_fd_4_page2) AND $objbloc_info->is_activ_btn_fd_4_page2 == '1' AND $objbloc_info->is_activ_btn_fd_4_page2 != null) {
            if ($objbloc_info->bloc1_4_existed_link1_page2 == 'ag') {
                $linkl = base_url() . $oInfoCommercant->nom_url . '/agenda';
            } elseif ($objbloc_info->bloc1_4_existed_link1_page2 == 'art') {
                $linkl = base_url() . $oInfoCommercant->nom_url . '/article';
            } elseif ($objbloc_info->bloc1_4_existed_link1_page2 == 'p1') {
                $linkl = base_url() . $oInfoCommercant->nom_url . '/infos';
            } elseif ($objbloc_info->bloc1_4_existed_link1_page2 == 'p2') {
                $linkl = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
            } elseif ($objbloc_info->bloc1_4_existed_link1_page2 == 'bp') {
                $linkl = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
            } elseif ($objbloc_info->bloc1_4_existed_link1_page2 == 'fd') {
                $linkl = $link_fidelity;
            } elseif ($objbloc_info->bloc1_4_existed_link1_page2 == 'bt') {
                $linkl = base_url() . $oInfoCommercant->nom_url . '/annonces';
            }  elseif (isset($objbloc_info->bloc1_4_custom_link1_page2) AND $objbloc_info->bloc1_4_custom_link1_page2 != '' AND $objbloc_info->bloc1_4_custom_link1_page2 != null) {
                $linkl = 'https://' . $objbloc_info->bloc1_4_custom_link1_page2;
            }

            ?>
            <?php if (isset($objbloc_info->bloc4_islightbox_page2fd) AND $objbloc_info->bloc4_islightbox_page2fd=="1"){ ?>

                <a data-fancybox data-type="iframe" data-src="<?php echo $linkl ?? '' ?>"  class="fancybox.iframe"><div class="bouton2"><?php echo $objbloc_info->btn_fd_4_content_page2 ?></div></a>
            <?php }else{  ?>
                <a class="pb-5 pt-5 lienBouton2" href="<?php echo ($linkl) ?? '' ?>">
                    <div class="bouton2"><?php echo $objbloc_info->btn_fd_4_content_page2 ?></div>
                </a>
            <?php } ?>
        <?php } ?>
    </div>
</div>