<script type="text/javascript">
$(document).ready(function() {

	$("#contact_recommandation_nom").focusin(function() {	  if ($(this).val()=="Votre nom *") $(this).val('');	});
	$("#contact_recommandation_nom").focusout(function() {	  if ($(this).val()=="") $(this).val('Votre nom *');	});

	$("#contact_recommandation_mail").focusin(function() {	  if ($(this).val()=="Votre courriel *") $(this).val('');	});
	$("#contact_recommandation_mail").focusout(function() {	  if ($(this).val()=="") $(this).val('Votre courriel *');	});
	
	$("#contact_recommandation_mail_ami").focusin(function() {	  if ($(this).val()=="Courriel de votre ami *") $(this).val('');	});
	$("#contact_recommandation_mail_ami").focusout(function() {	  if ($(this).val()=="") $(this).val('Courriel de votre ami *');	});
	
	$("#contact_recommandation_msg").focusin(function() {	  if ($(this).val()=="Votre message *") $(this).val('');	});
	$("#contact_recommandation_msg").focusout(function() {	  if ($(this).val()=="") $(this).val('Votre message *');	});
	
	$("#contact_recommandation_reset").click(function() {
	  $("#contact_recommandation_nom").val('Votre nom *');
	  $("#contact_recommandation_mail_ami").val('Courriel de votre ami *');
	  $("#contact_recommandation_mail").val('Votre courriel *');
	  $("#contact_recommandation_msg").val('Votre message *');
	  $("#spanContactPrivicarteForm").html('* champs obligatoires');
	});
	
	$("#contact_recommandation_send").click(function() {
		var error = 0;
		var contact_recommandation_nom = $("#contact_recommandation_nom").val();
		if (contact_recommandation_nom == '' || contact_recommandation_nom == 'Votre nom *') error = 1;
		var contact_recommandation_mail_ami = $("#contact_recommandation_mail_ami").val();
		if (contact_recommandation_mail_ami == '' || contact_recommandation_mail_ami == 'Courriel de votre ami *') error = 1;
		if (!validateEmail(contact_recommandation_mail_ami)) error = 3;
		var contact_recommandation_mail = $("#contact_recommandation_mail").val();
		if (contact_recommandation_mail == '' || contact_recommandation_mail == 'Votre courriel *') error = 1;
		if (!validateEmail(contact_recommandation_mail)) error = 2;
		var contact_recommandation_msg = $("#contact_recommandation_msg").val();
		if (contact_recommandation_msg == '' || contact_recommandation_msg == 'Votre message *') error = 1;
		$("#spanContactPrivicarteForm").html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
		
		if (error == 1) {
			$("#spanContactPrivicarteForm").html('<span style="color:#FF0000;">* Les champs sont obligatoires</span>');
		} else if (error == 2) {
			$("#spanContactPrivicarteForm").html('<span style="color:#FF0000;">* Adresse email invalide</span>');
			$("#contact_recommandation_mail").css('border-color','#ff0000'); 
		} else if (error == 3) {
			$("#spanContactPrivicarteForm").html('<span style="color:#FF0000;">* Adresse email invalide</span>');
			$("#contact_recommandation_mail_ami").css('border-color','#ff0000'); 
		} else {
			$.post(
				"<?php echo site_url("front/professionnels/contact_partner_sendmail/");?>", 
				{
					contact_partner_nom:contact_recommandation_nom,
					contact_partner_tel:'',
					contact_partner_mail:contact_recommandation_mail,
					contact_partner_msg:contact_recommandation_msg,
					contact_partner_mailto:contact_recommandation_mail_ami
				},
				function( data ) {
				  $("#spanContactPrivicarteForm").html(data);
				  setTimeout(function(){ $.fancybox.close(); }, 4000);
				});
			}
	});

	$.noConflict();//this activate all fancybox at all page calling this navbar
	
	$("#IdRecommandationPartnerForm").fancybox({
		'autoScale' : false,
		'overlayOpacity'      : 0.8, // Set opacity to 0.8
		'overlayColor'        : "#000000", // Set color to Black
		'padding'         : 5,
		'width'         : 520,
		'height'        : 410,
		'transitionIn'      : 'elastic',
		'transitionOut'     : 'elastic'
	});
	
	
	$("#IdContactPartnerForm_footer_link_msg").fancybox({
		'autoScale' : false,
		'overlayOpacity'      : 0.8, // Set opacity to 0.8
		'overlayColor'        : "#000000", // Set color to Black
		'padding'         : 5,
		'width'         : 520,
		'height'        : 410,
		'transitionIn'      : 'elastic',
		'transitionOut'     : 'elastic'
	});
	
});

function validateEmail(sEmail) {
	var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if (filter.test(sEmail)) {
		return true;
	}
	else {
		return false;
	}
}
</script>

<?php
$thisss =& get_instance();
$thisss->load->library('ion_auth');
$this->load->model("ion_auth_used_by_club");
$user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);
if (isset($user_ion_auth_id)) $user_groups = $thisss->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;
?>
<?php
////set preview & next icon
$link_partner_preview = '0';
$link_partner_next = '0';
if (isset($link_partner_current_page) && $link_partner_current_page != "") $link_partner_current = $link_partner_current_page; else $link_partner_current = 'presentation';

if ($link_partner_current == 'presentation') {//////////////current page presentation
	
	if (isset($oInfoCommercant->activite1) && $oInfoCommercant->activite1 != "" && $oInfoCommercant->activite1 != NULL) $link_partner_next = $commercant_url_infos;
	else if (isset($oInfoCommercant->activite2) && $oInfoCommercant->activite2 != "" && $oInfoCommercant->activite2 != NULL) $link_partner_next = $commercant_url_autresinfos;
	else if ($nombre_annonce_com != '0') $link_partner_next = $commercant_url_annonces;
	else if (isset($oLastbonplanCom)) $link_partner_next = $commercant_url_notre_bonplan;
	else if (isset($nombre_agenda_com) && $nombre_agenda_com!="0" && $group_id_commercant_user != 3) $link_partner_next = $commercant_url_agenda;
	$link_partner_preview = '#';
	
} else if ($link_partner_current == 'page1') {//////////////current page page1

	if (isset($oInfoCommercant->activite2) && $oInfoCommercant->activite2 != "" && $oInfoCommercant->activite2 != NULL) $link_partner_next = $commercant_url_autresinfos;
	else if ($nombre_annonce_com != '0') $link_partner_next = $commercant_url_annonces;
	else if (isset($oLastbonplanCom)) $link_partner_next = $commercant_url_notre_bonplan;
	else if (isset($nombre_agenda_com) && $nombre_agenda_com!="0" && $group_id_commercant_user != 3) $link_partner_next = $commercant_url_agenda;
	$link_partner_preview = $commercant_url_presentation;
	
} else if ($link_partner_current == 'page2') {//////////////current page page2

	if ($nombre_annonce_com != '0') $link_partner_next = $commercant_url_annonces;
	else if (isset($oLastbonplanCom)) $link_partner_next = $commercant_url_notre_bonplan;
	else if (isset($nombre_agenda_com) && $nombre_agenda_com!="0" && $group_id_commercant_user != 3) $link_partner_next = $commercant_url_agenda;
	
	if (isset($oInfoCommercant->activite1) && $oInfoCommercant->activite1 != "" && $oInfoCommercant->activite1 != NULL) $link_partner_preview = $commercant_url_infos;
	else $link_partner_preview = $commercant_url_presentation;
	
} else if ($link_partner_current == 'boutique') {//////////////current page boutique

	if (isset($oLastbonplanCom)) $link_partner_next = $commercant_url_notre_bonplan;
	else if (isset($nombre_agenda_com) && $nombre_agenda_com!="0" && $group_id_commercant_user != 3) $link_partner_next = $commercant_url_agenda;
	
	if (isset($oInfoCommercant->activite2) && $oInfoCommercant->activite2 != "" && $oInfoCommercant->activite2 != NULL) $link_partner_preview = $commercant_url_autresinfos;
	else if (isset($oInfoCommercant->activite1) && $oInfoCommercant->activite1 != "" && $oInfoCommercant->activite1 != NULL) $link_partner_preview = $commercant_url_infos;
	else $link_partner_preview = $commercant_url_presentation;
	
} else if ($link_partner_current == 'bonplan') {//////////////current page bonplan

	if (isset($nombre_agenda_com) && $nombre_agenda_com!="0" && $group_id_commercant_user != 3) $link_partner_next = $commercant_url_agenda;
	if ($nombre_annonce_com != '0') $link_partner_next = $commercant_url_annonces;
	else if (isset($oInfoCommercant->activite2) && $oInfoCommercant->activite2 != "" && $oInfoCommercant->activite2 != NULL) $link_partner_preview = $commercant_url_autresinfos;
	else if (isset($oInfoCommercant->activite1) && $oInfoCommercant->activite1 != "" && $oInfoCommercant->activite1 != NULL) $link_partner_preview = $commercant_url_infos;
	else $link_partner_preview = $commercant_url_presentation;
	
} else if ($link_partner_current == 'agenda') {//////////////current page agenda

	if (isset($oLastbonplanCom)) $link_partner_next = $commercant_url_notre_bonplan;
	else if ($nombre_annonce_com != '0') $link_partner_next = $commercant_url_annonces;
	else if (isset($oInfoCommercant->activite2) && $oInfoCommercant->activite2 != "" && $oInfoCommercant->activite2 != NULL) $link_partner_preview = $commercant_url_autresinfos;
	else if (isset($oInfoCommercant->activite1) && $oInfoCommercant->activite1 != "" && $oInfoCommercant->activite1 != NULL) $link_partner_preview = $commercant_url_infos;
	else $link_partner_preview = $commercant_url_presentation;
	
} 
?>
<!-- background-color:#CFCFCF
 --><div class="container_pro" style="background-color:#CFCFCF; width:1250px !important;">

<!-- ajout entete menu  #FFFFFF -->
<div class="col-lg-12 padding0" style="background-color:#CFCFCF; width:1250px !important;">
  <div class="col-lg-2 col-sm-12" style="text-align: center;border-right: dotted; margin-bottom:15px;margin-top:15px;"><a style="margin:0px !important; padding:0px !important;" href="<?php echo site_url($commercant_url_home."/presentation"); ?>" title="Pr&eacute;sentation">Présentation</a></div>
   <div class="col-lg-2 col-sm-12" style="text-align: center;border-right: dotted; margin-bottom:15px;margin-top:15px;"><a style="margin:0px !important; padding:0px !important;" href="<?php if ($link_partner_preview == '#') echo 'javascript:void(0);'; else if ($link_partner_preview != '0') echo site_url($link_partner_preview); else echo 'javascript:void(0);'; ?>" title="Page pr&eacute;cedente">Page précedente</a></div>
   <div class="col-lg-2 col-sm-12" style="text-align: center;border-right: dotted; margin-bottom:15px;margin-top:15px;"><a style="margin:0px !important; padding:0px !important;" href="<?php if ($link_partner_next != '0') echo site_url($link_partner_next); else echo 'javascript:void(0);'; ?>" title="Page suivante">Page suivante</a></div>
        <div class="col-lg-2 col-sm-12" style="text-align: center;border-right: dotted; margin-bottom:15px;margin-top:15px;">

        
        <?php if (isset($oCommercantFavoris) && $oCommercantFavoris != NULL && $oCommercantFavoris->Favoris == "1") { ?>
            <a href="<?php echo site_url('front/utilisateur/delete_favoris/'.$oInfoCommercant->IdCommercant);?>" title="Ajouter aux Favoris" style="margin:0px !important; padding:0px !important;">
                Mettre en favoris
            </a>
        <?php } else { ?>
            <a href="<?php echo site_url('front/utilisateur/ajout_favoris/'.$oInfoCommercant->IdCommercant);?>" title="Ajouter aux Favoris" style="margin:0px !important; padding:0px !important;">
                Mettre en favoris
            </a>
        <?php } ?>
        
        </div>
        <div class="col-lg-2 col-sm-12" style="text-align: center;border-right: dotted;margin-bottom:15px;margin-top:15px;"><a style="margin:0px !important; padding:0px !important;" href="#divContactRecommandationForm" id="IdRecommandationPartnerForm" title="Recommandation">Recommandation</a></div>
        <!--<li><a style="margin:0px !important; padding:0px !important;" href="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";//echo site_url($commercant_url_home."/presentation"); ?>" title="Ouvrir dans une nouvelle page" target="_blank"><img src="<?php echo GetImagePath("privicarte/") ; ?>/nav_pro_view_premium.png" alt="favoris" /></a></li>-->
        
        
        <div class="col-lg-2 col-sm-12" style="text-align: center;margin-bottom:15px;margin-top:15px;"><a style="margin:0px !important; padding:0px !important;" href="<?php echo site_url("front/utilisateur/contenupro"); ?>" title="Vous &ecirc;tes commercant, g&eacute;rer vos articles" target="_blank">Mon compt Admin</a></div>        
      
  </div>

  <!-- fin entete menu -->

<div class="col-sm-12">

  <div class="col-lg-12 padding0" style="margin-top: 20px;margin-bottom:10px;">
  	
	<?php
	$thisss =& get_instance();
	$thisss->load->library('ion_auth');
	$this->load->model("ion_auth_used_by_club");
	$user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);
	if (isset($user_ion_auth_id)) $user_groups = $thisss->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
	if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;
	//echo $nombre_annonce_com."qsdfqsdfffff".$group_id_commercant_user;
	?>
	
	<?php
	if ($group_id_commercant_user==5) { 
		$this->load->view("privicarte/includes/logo_partner", $data); 
	?>
    <?php } else {?>
    <span style="font-size:30px;"><?php echo '<center>'.$oInfoCommercant->NomSociete.'</center>'; ?></span>
    <?php } ?>
    
  </div>
  
  <div class="col-lg-12 padding0" style="color:#000000; font-size:20px; font-style:italic;margin-bottom:10px;"><?php echo '<center>'.$oInfoCommercant->titre_entete.'</center>'; ?></div>
</div>



<!-- <div class="col-sm-6" style="text-align:right;">

  <div class="col-lg-12 padding0">
  	
    <div id="google_translate_element" style="padding-left: 40px; padding-top: 20px"></div><script type="text/javascript">
	function googleTranslateElementInit() {
	  new google.translate.TranslateElement({pageLanguage: 'fr', includedLanguages: 'ar,de,en,es,it,ja,nl,no,pl,pt,ru,sv,zh-CN', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false, multilanguagePage: true}, 'google_translate_element');
	}
	</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
  
  </div> -->
  
<!--   <div class="col-lg-12 padding0">
  <div class="col-lg-2 col-sm-12" style="text-align: center;border-right: dotted;"><a style="margin:0px !important; padding:0px !important;" href="<?php echo site_url($commercant_url_home."/presentation"); ?>" title="Pr&eacute;sentation">Présentation</a></div>
   <div class="col-lg-2 col-sm-12" style="text-align: center;border-right: dotted;"><a style="margin:0px !important; padding:0px !important;" href="<?php if ($link_partner_preview == '#') echo 'javascript:void(0);'; else if ($link_partner_preview != '0') echo site_url($link_partner_preview); else echo 'javascript:void(0);'; ?>" title="Page pr&eacute;cedente">Page précedente</a></div>
   <div class="col-lg-2 col-sm-12" style="text-align: center;border-right: dotted;"><a style="margin:0px !important; padding:0px !important;" href="<?php if ($link_partner_next != '0') echo site_url($link_partner_next); else echo 'javascript:void(0);'; ?>" title="Page suivante">Page suivante</a></div>
        <div class="col-lg-2 col-sm-12" style="text-align: center;border-right: dotted;">

        
        <?php if (isset($oCommercantFavoris) && $oCommercantFavoris != NULL && $oCommercantFavoris->Favoris == "1") { ?>
            <a href="<?php echo site_url('front/utilisateur/delete_favoris/'.$oInfoCommercant->IdCommercant);?>" title="Ajouter aux Favoris" style="margin:0px !important; padding:0px !important;">
                Mettre en favoris
            </a>
        <?php } else { ?>
            <a href="<?php echo site_url('front/utilisateur/ajout_favoris/'.$oInfoCommercant->IdCommercant);?>" title="Ajouter aux Favoris" style="margin:0px !important; padding:0px !important;">
                Mettre en favoris
            </a>
        <?php } ?>
        
        </div>
        <div class="col-lg-2 col-sm-12" style="text-align: center;border-right: dotted;"><a style="margin:0px !important; padding:0px !important;" href="#divContactRecommandationForm" id="IdRecommandationPartnerForm" title="Recommandation">Recommandation</a></div>
        <!--<li><a style="margin:0px !important; padding:0px !important;" href="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";//echo site_url($commercant_url_home."/presentation"); ?>" title="Ouvrir dans une nouvelle page" target="_blank"><img src="<?php echo GetImagePath("privicarte/") ; ?>/nav_pro_view_premium.png" alt="favoris" /></a></li>-->
        
        
        <!-- <div class="col-lg-2 col-sm-12" style="text-align: center;border-right: dotted;"><a style="margin:0px !important; padding:0px !important;" href="<?php echo site_url("front/utilisateur/contenupro"); ?>" title="Vous &ecirc;tes commercant, g&eacute;rer vos articles" target="_blank">Mon compt Admin</a></div>         -->
      
  <!-- </div> --> 
  
<!-- </div> -->

</div>




<!--Recommandation form contet-->
<div id="divContactRecommandationForm" style="display:none; background-color:#FFFFFF; width:350px; height:400px;">
    <form name="formContactPrivicarteForm" id="formContactPrivicarteForm" action="#">
        <table width="100%" id="tableContactPartnerForm" border="0" cellspacing="0" cellpadding="0" style="text-align:center; width:350px; height:400px;">
          <tr>
            <td><div style="font-family:arial; font-size:24px; font-weight:bold;">Recommander &agrave; un ami</div></td>
          </tr>
          <tr>
            <td><input type="text" name="contact_recommandation_nom" id="contact_recommandation_nom" value="Votre nom *"/></td>
          </tr>
          <tr>
            <td><input type="text" name="contact_recommandation_mail_ami" id="contact_recommandation_mail_ami" value="Courriel de votre ami *"/></td>
          </tr>
          <tr>
            <td><input type="text" name="contact_recommandation_mail" id="contact_recommandation_mail" value="Votre courriel *"/></td>
          </tr>
          <tr>
            <td>
<textarea name="contact_recommandation_msg" id="contact_recommandation_msg" style="height: 150px;">
Bonjour,
Je te recommande un professionnel de chez Sortez.org sur le lien ci-dessous,
<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>

A+ chez Sortez.org
</textarea>
            </td>
          </tr>
          <tr>
            <td><span id="spanContactPrivicarteForm">* champs obligatoires</span></td>
          </tr>
          <tr>
            <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><input type="button" class="btn btn-default" name="contact_recommandation_reset" id="contact_recommandation_reset" value="Retablir"/></td>
                <td><input type="button" class="btn btn-default" name="contact_recommandation_send" id="contact_recommandation_send" value="Envoyer"/></td>
              </tr>
            </table>
            </td>
          </tr>
        </table>
    </form>
</div>

