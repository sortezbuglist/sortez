<div class="row ml-4 pl-0 pb-4 pt-4 pr-4" style="">
    <?php     $path_img_gallery_gl = 'application/resources/front/photoCommercant/imagesbank/'.$oInfoCommercant->user_ionauth_id;
    ?>
    <div class="col-sm-4 text-center pl-0 text-justify">
        <div class="p-4 sautligne allign_button" style=";height:auto;padding-right:0px!important;">

            <?php if (isset($objbloc_info->bloc_1_2_image1_content_page1) AND $objbloc_info->bloc_1_2_image1_content_page1 != '' AND $objbloc_info->bloc_1_2_image1_content_page1 != null) { ?>

                <div style="width: 100%;height: auto">

                    <img style="max-width: 100%;height: auto;" class="img-fluid"

                         src="<?php echo base_url($path_img_gallery_gl . '/' . $objbloc_info->bloc_1_2_image1_content_page1) ?>">

                </div>

            <?php } ?>

            <?php if (isset($objbloc_info->bloc1_2_content_page1) AND $objbloc_info->bloc1_2_content_page1 != '' AND $objbloc_info->bloc1_2_content_page1 != null) { ?>

                <div class="pb-0 pt-2 content"><?php echo htmlspecialchars_decode($objbloc_info->bloc1_2_content_page1) ?></div>

            <?php } ?>

            <?php if (isset($objbloc_info->is_activ_btn_bloc1_2_page1) AND $objbloc_info->is_activ_btn_bloc1_2_page1 == '1' AND $objbloc_info->is_activ_btn_bloc1_2_page1 != null) {

                if ($objbloc_info->bloc1_2_existed_link1_page1 == 'ag') {

                    $linkd = base_url() . $oInfoCommercant->nom_url . '/agenda';

                } elseif ($objbloc_info->bloc1_2_existed_link1_page1 == 'art') {

                    $linkd = base_url() . $oInfoCommercant->nom_url . '/article';

                } elseif ($objbloc_info->bloc1_2_existed_link1_page1 == 'p1') {

                    $linkd = base_url() . $oInfoCommercant->nom_url . '/infos';

                } elseif ($objbloc_info->bloc1_2_existed_link1_page1 == 'p2') {

                    $linkd = base_url() . $oInfoCommercant->nom_url . '/autresinfos';

                } elseif ($objbloc_info->bloc1_2_existed_link1_page1 == 'bp') {

                    $linkd = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';

                } elseif ($objbloc_info->bloc1_2_existed_link1_page1 == 'fd') {

                    $linkd = $link_fidelity;

                } elseif ($objbloc_info->bloc1_2_existed_link1_page1 == 'bt') {

                    $linkd = base_url() . $oInfoCommercant->nom_url . '/annonces';

                } elseif (isset($objbloc_info->bloc1_2_custom_link1_page1) AND $objbloc_info->bloc1_2_custom_link1_page1 != '' AND $objbloc_info->bloc1_2_custom_link1_page1 != null) {

                    $linkd = 'https://' . $objbloc_info->bloc1_2_custom_link1_page1;

                }



                ?>

                <?php if (isset($objbloc_info->bloc2_islightbox_page1) AND $objbloc_info->bloc2_islightbox_page1 =="1"){ ?>



                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkd ?? '' ?>"  class="fancybox.iframe"><div class="bouton1"><?php echo $objbloc_info->btn_bloc1_2_content1_page1 ?></div></a>

                <?php }else{  ?>

                    <a class="pb-5 pt-5 lienbouton " href="<?php echo ($linkd) ?? '' ?>">

                        <div class="bouton1"><?php echo $objbloc_info->btn_bloc1_2_content1_page1 ?></div>

                    </a>

                <?php } ?>

            <?php } ?>

        </div>

    </div>

    <div class="col-sm-4 text-center pl-0 text-justify">
        <div class="p-4 sautligne allign_button" style=";height:auto;padding-right:0px!important;">

            <?php if (isset($objbloc_info->bloc_1_2_image2_content_page1) AND $objbloc_info->bloc_1_2_image2_content_page1 != '' AND $objbloc_info->bloc_1_2_image2_content_page1 != null) { ?>

                <div class="image">

                    <img style="max-width: 100%;height: auto;"

                         src="<?php echo base_url($path_img_gallery_gl . '/' . $objbloc_info->bloc_1_2_image2_content_page1) ?>">

                </div>

            <?php } ?>

            <?php if (isset($objbloc_info->bloc1_2_content2_page1) AND $objbloc_info->bloc1_2_content2_page1 != '' AND $objbloc_info->bloc1_2_content2_page1 != null) { ?>

                <div class="pb-0 pt-2 content"><?php echo htmlspecialchars_decode($objbloc_info->bloc1_2_content2_page1) ?></div>

            <?php } ?>

            <?php if (isset($objbloc_info->is_activ_btn_bloc2_2_page1) AND $objbloc_info->is_activ_btn_bloc2_2_page1 == '1' AND $objbloc_info->is_activ_btn_bloc2_2_page1 != null) {

                if ($objbloc_info->bloc1_2_existed_link2_page1 == 'ag') {

                    $linke = base_url() . $oInfoCommercant->nom_url . '/agenda';

                } elseif ($objbloc_info->bloc1_2_existed_link2_page1 == 'art') {

                    $linke = base_url() . $oInfoCommercant->nom_url . '/article';

                } elseif ($objbloc_info->bloc1_2_existed_link2_page1 == 'p1') {

                    $linke = base_url() . $oInfoCommercant->nom_url . '/infos';

                } elseif ($objbloc_info->bloc1_2_existed_link2_page1 == 'p2') {

                    $linke = base_url() . $oInfoCommercant->nom_url . '/autresinfos';

                } elseif ($objbloc_info->bloc1_2_existed_link2_page1 == 'bp') {

                    $linke = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';

                } elseif ($objbloc_info->bloc1_2_existed_link2_page1 == 'fd') {

                    $linke =$link_fidelity;

                } elseif ($objbloc_info->bloc1_2_existed_link2_page1 == 'bt') {

                    $linke = base_url() . $oInfoCommercant->nom_url . '/annonces';

                } elseif (isset($objbloc_info->bloc2_2_custom_link1_page1) AND $objbloc_info->bloc2_2_custom_link1_page1 != '' AND $objbloc_info->bloc2_2_custom_link1_page1 != null) {

                    $linke = 'https://' . $objbloc_info->bloc2_2_custom_link1_page1;

                }

                ?>

                <?php if (isset($objbloc_info->bloc2_islightbox2_page1) AND $objbloc_info->bloc2_islightbox2_page1=="1"){ ?>



                    <a data-fancybox data-type="iframe" data-src="<?php echo $linke ?? '' ?>"  class="fancybox.iframe"><div class="bouton1"><?php echo $objbloc_info->btn_bloc1_2_content2_page1 ?></div></a>

                <?php }else{  ?>

                    <a class="pb-5 pt-5 lienbouton " href="<?php echo ($linke) ?? '' ?>">

                        <div class="bouton1"><?php echo $objbloc_info->btn_bloc1_2_content2_page1 ?></div>

                    </a>

                <?php } ?>

            <?php } ?>

        </div>

    </div>
    <!-- bloc3 debut --->
    <div class="col-sm-4 text-center pl-0 text-justify">
        <div class="p-4 sautligne allign_button" style=";height:auto;padding-right:0px!important;">

            <?php if (isset($objbloc_info->bloc_2_3_image3_content_page1) AND $objbloc_info->bloc_2_3_image3_content_page1 != '' AND $objbloc_info->bloc_2_3_image3_content_page1 != null) { ?>

                <div class="image">

                    <img style="max-width: 100%;height: auto;"

                         src="<?php echo base_url($path_img_gallery_gl . '/' . $objbloc_info->bloc_2_3_image3_content_page1) ?>">

                </div>

            <?php } ?>

            <?php if (isset($objbloc_info->bloc2_3_content2_page1) AND $objbloc_info->bloc2_3_content2_page1 != '' AND $objbloc_info->bloc2_3_content2_page1 != null) { ?>

                <div class="pb-0 pt-2 content"><?php echo htmlspecialchars_decode($objbloc_info->bloc1_2_content2_page1) ?></div>

            <?php } ?>

            <?php if (isset($objbloc_info->is_activ_btn_bloc_2_3_page1) AND $objbloc_info->is_activ_btn_bloc_2_3_page1 == '1' AND $objbloc_info->is_activ_btn_bloc_2_3_page1 != null) {

                if ($objbloc_info->bloc2_3_existed_link2_page1 == 'ag') {

                    $linkr = base_url() . $oInfoCommercant->nom_url . '/agenda';

                } elseif ($objbloc_info->bloc2_3_existed_link2_page1 == 'art') {

                    $linkr = base_url() . $oInfoCommercant->nom_url . '/article';

                } elseif ($objbloc_info->bloc2_3_existed_link2_page1 == 'p1') {

                    $linkr = base_url() . $oInfoCommercant->nom_url . '/infos';

                } elseif ($objbloc_info->bloc2_3_existed_link2_page1 == 'p2') {

                    $linkr = base_url() . $oInfoCommercant->nom_url . '/autresinfos';

                } elseif ($objbloc_info->bloc2_3_existed_link2_page1 == 'bp') {

                    $linkr = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';

                } elseif ($objbloc_info->bloc2_3_existed_link2_page1 == 'fd') {

                    $linkr = $link_fidelity;

                } elseif ($objbloc_info->bloc2_3_existed_link2_page1 == 'bt') {

                    $linkr = base_url() . $oInfoCommercant->nom_url . '/annonces';

                } elseif (isset($objbloc_info->bloc2_3_custom_link2_page1) AND $objbloc_info->bloc2_3_custom_link2_page1 != '' AND $objbloc_info->bloc2_3_custom_link2_page1 != null) {

                    $linkr = 'https://' . $objbloc_info->bloc2_3_custom_link2_page1;

                }

                ?>

                <?php if (isset($objbloc_info->bloc2_islightbox3_page1) AND $objbloc_info->bloc2_islightbox3_page1=="1"){ ?>



                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkr ?? '' ?>"  class="fancybox.iframe"><div class="bouton1"><?php echo $objbloc_info->btn_bloc2_3_content2_page1 ?></div></a>

                <?php }else{  ?>

                    <a class="pb-5 pt-5 lienbouton " href="<?php echo ($linkr) ?? '' ?>">

                        <div class="bouton1"><?php echo $objbloc_info->btn_bloc2_3_content2_page1 ?></div>

                    </a>

                <?php } ?>

            <?php } ?>

        </div>

    </div>
    <!-- bloc3 fin --->

</div>