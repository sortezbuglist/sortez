<script>

// This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('facebooklogin_status').innerHTML = 'Connectez vous ' +
        'à l\'application Privicarte.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('facebooklogin_status').innerHTML = 'Se connecter avec un compte ' +
        'Facebook.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

$(document).ready(function() {

  window.fbAsyncInit = function() {
	FB.init({
	  appId      : '324903384595223',//appId      : '1292726270756136',
	  cookie     : true,  // enable cookies to allow the server to access 
                        // the session
	  xfbml      : true,
	  version    : 'v2.9'//version    : 'v2.6'
	});

	  // Now that we've initialized the JavaScript SDK, we call 
	  // FB.getLoginStatus().  This function gets the state of the
	  // person visiting this page and can return one of three states to
	  // the callback you provide.  They can be:
	  //
	  // 1. Logged into your app ('connected')
	  // 2. Logged into Facebook, but not your app ('not_authorized')
	  // 3. Not logged into Facebook and can't tell if they are logged into
	  //    your app or not.
	  //
	  // These three cases are handled in the callback function.

	  FB.getLoginStatus(function(response) {
	    statusChangeCallback(response);
	  });

  };

});  


  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Bienvenue! Accès à votre compte facebook.... ');
    FB.api('/me', {fields: 'email'}, function(response) {
      console.log('Successful login for: ' + response.email);
      //console.log(response);
      $("#spanpublightbox_loading").html('Merci de votre connexion , ' + response.email + '! <a href="javascript:publightbox_deconnexion(\'<?php echo base_url();?>\');">Deconnexion</a>');
      $("#spanpublightbox_loading").html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
      $("#publightbox_mail").val(response.email);
      publightbox_submit_function();
    });
  }



function publightbox_deconnexion(href) {
    try {
        if (FB.getAccessToken() != null) {
            FB.logout(function(response) {
                // user is now logged out from facebook do your post request or just redirect
                window.location.replace(href);
            });
        } else {
            // user is not logged in with facebook, maybe with something else
            window.location.replace(href);
        }
    } catch (err) {
        // any errors just logout
        window.location.replace(href);
    }
   }


</script>

