<script type="text/javascript">
$(document).ready(function() {
	$("#IdGooglePrivicarteForm").fancybox({
		'autoScale' : false,
		'overlayOpacity'      : 0.8, // Set opacity to 0.8
		'overlayColor'        : "#000000", // Set color to Black
		'padding'         : 5,
		'width'         : 320,
		'height'        : 215,
		'transitionIn'      : 'elastic',
		'transitionOut'     : 'elastic',
		'type'          : 'iframe'
	});
	
	$("#IdFacebookPrivicarteForm_share").fancybox({
		'autoScale' : false,
		'overlayOpacity'      : 0.8, // Set opacity to 0.8
		'overlayColor'        : "#000000", // Set color to Black
		'padding'         : 5,
		'width'         : 500,
		'height'        : 800,
		'transitionIn'      : 'elastic',
		'transitionOut'     : 'elastic',
		'type'          : 'iframe'
	});
	
	
	$("#IdTwitterPrivicarteForm_share").fancybox({
		'autoScale' : false,
		'overlayOpacity'      : 0.8, // Set opacity to 0.8
		'overlayColor'        : "#000000", // Set color to Black
		'padding'         : 5,
		'width'         : 500,
		'height'        : 800,
		'transitionIn'      : 'elastic',
		'transitionOut'     : 'elastic',
		'type'          : 'iframe'
	});
	
	$("#IdGoogleplusPrivicarteForm_share").fancybox({
		'autoScale' : false,
		'overlayOpacity'      : 0.8, // Set opacity to 0.8
		'overlayColor'        : "#000000", // Set color to Black
		'padding'         : 5,
		'width'         : 500,
		'height'        : 800,
		'transitionIn'      : 'elastic',
		'transitionOut'     : 'elastic',
		'type'          : 'iframe'
	});
	
	$("#IdMentionPrivicarteForm").fancybox({
		'autoScale' : false,
		'overlayOpacity'      : 0.8, // Set opacity to 0.8
		'overlayColor'        : "#000000", // Set color to Black
		'padding'         : 5,
		'width'         : 500,
		'height'        : 800,
		'transitionIn'      : 'elastic',
		'transitionOut'     : 'elastic',
		'type'          : 'iframe'
	});
	
	
});
</script>



<style type="text/css">
.panel-default > .panel-heading { background-color:#FFFFFF !important; border: none !important;}
</style>

<div class="container">
    <!--<img src="<?php //echo GetImagePath("privicarte/") ; ?>/test/footer_link_pvc.png">-->
    <a href="/mentions-legales.html" id="IdMentionPrivicarteForm_" title="Mention legale" target="_blank"><img src="<?php echo GetImagePath("privicarte/") ; ?>/footer_link_info.png" alt="info" /></a>
    <a href="#divContactPrivicarteForm" id="IdContactPrivicarteForm_footer" title="Nous Contacter"><img src="<?php echo GetImagePath("privicarte/") ; ?>/footer_link_msg.png" alt="message" /></a>
    <!--<a href="<?php //echo site_url("front/professionnels/FacebookPrivicarteForm_share"); ?>" id="IdFacebookPrivicarteForm_share"><img src="<?php //echo GetImagePath("privicarte/") ; ?>/footer_link_fb.png" alt="facebook" /></a>-->
    <a href="javascript:void(0);" onclick='javascript:window.open("https://www.facebook.com/sharer/sharer.php?u=http%3A//sortez.org", "FacebookPrivicarteForm_share", "width=500, height=800");' title="Partage Facebook"><img src="<?php echo GetImagePath("privicarte/") ; ?>/footer_link_fb.png" alt="facebook" /></a>
    <!--<a href="<?php //echo site_url("front/professionnels/TwitterPrivicarteForm_share"); ?>" id="IdTwitterPrivicarteForm_share"><img src="<?php //echo GetImagePath("privicarte/") ; ?>/footer_link_twt.png" alt="twitter" /></a>-->
    <a href="javascript:void(0);" onclick='javascript:window.open("https://twitter.com/home?status=http%3A//sortez.org", "TwitterPrivicarteForm_share", "width=500, height=800");' title="Partage Twitter"><img src="<?php echo GetImagePath("privicarte/") ; ?>/footer_link_twt.png" alt="twitter" /></a>
    <!--<a href="<?php //echo site_url("front/professionnels/GoogleplusPrivicarteForm"); ?>" id="IdGooglePrivicarteForm"><img src="<?php //echo GetImagePath("privicarte/") ; ?>/footer_link_gplus.png" alt="googleplus" /></a>-->
    <!--<a href="<?php //echo site_url("front/professionnels/GoogleplusPrivicarteForm_share"); ?>" id="IdGoogleplusPrivicarteForm_share"><img src="<?php //echo GetImagePath("privicarte/") ; ?>/footer_link_gplus.png" alt="googleplus" /></a>-->
    <a href="javascript:void(0);" onclick='javascript:window.open("https://plus.google.com/share?url=http%3A//sortez.org", "GoogleplusPrivicarteForm_share", "width=500, height=800");' title="Partage Google+"><img src="<?php echo GetImagePath("privicarte/") ; ?>/footer_link_gplus.png" alt="googleplus" /></a>
	</div>
