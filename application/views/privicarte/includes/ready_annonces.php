<script type="text/javascript">
    

$(function(){
     
    $("#inputStringVilleHidden_annonces").change(function(){
            
            var inputStringVilleHidden_annonces = $("#inputStringVilleHidden_annonces").val();

            check_show_categ_annonce(inputStringVilleHidden_annonces);
            //alert(inputStringVilleHidden_annonces);

            $("#span_leftcontener2013_form_annonces input:checkbox").attr('checked', false);
            $("#frmRechercheAnnonce").submit();
            
        });

    $("#inputString_zMotCle_submit").click(function(){

        var inputString_zMotCle = $("#inputString_zMotCle").val();
        $("#zMotCle").val(inputString_zMotCle);

        $("#span_leftcontener2013_form_annonces input:checkbox").attr('checked', false);
        $("#frmRechercheAnnonce").submit();
    });

    $("#inputStringOrderByHidden_partenaires").change(function(){

        var inputStringVilleHidden_annonces = $("#inputStringVilleHidden_annonces").val();
        var inputStringOrderByHidden_partenaires = $("#inputStringOrderByHidden_partenaires").val();

        $("#inputStringOrderByHidden").val(inputStringOrderByHidden_partenaires);

        //check_show_categ_annonce(inputStringVilleHidden_annonces);
        //alert(inputStringVilleHidden_annonces);

        //$("#span_leftcontener2013_form_annonces input:checkbox").attr('checked', false);
        $("#frmRechercheAnnonce").submit();
    });
    
    $("#inputStringEtatByHidden").change(function(){

        var inputStringEtatByHidden = $("#inputStringEtatByHidden").val();
        var inputStringOrderByHidden_partenaires = $("#inputStringOrderByHidden_partenaires").val();
        var inputString_zMotCle = $("#inputString_zMotCle").val();
        if (inputString_zMotCle == "RECHERCHER") inputString_zMotCle = "";

        $("#iEtat").val(inputStringEtatByHidden);
        $("#inputStringOrderByHidden").val(inputStringOrderByHidden_partenaires);
        $("#zMotCle").val(inputString_zMotCle);

        //$("#span_leftcontener2013_form_annonces input:checkbox").attr('checked', false);
        $("#frmRechercheAnnonce").submit();
    });
   
});


function show_current_categ_subcateg(IdRubrique){

    $(".leftcontener2013content").hide();
    var id_to_show = "#leftcontener2013content_"+IdRubrique;
    $(id_to_show).show();

}


function check_show_categ_annonce(inputStringDepartementHidden_annnonces, inputStringVilleHidden_annonces){
    
    var base_url_visurba = '<?php echo site_url();?>';
    
    $("#span_leftcontener2013_form_annonces").html('<center><img src="'+base_url_visurba+'application/resources/front/images/wait.gif" alt="loading...."/></center>');
            
                $.post(
                     base_url_visurba+'front/annonce/check_category_list/',
                     {
                        inputStringDepartementHidden_annnonces: inputStringDepartementHidden_annnonces,
                        inputStringVilleHidden_annonces: inputStringVilleHidden_annonces 
                     }
                     ,
                     function (zReponse)
                     {
                             //alert ("reponse <br/><br/>" + zReponse) ;
                             if (zReponse == "error") { //an other home page exist yet
                                     alert("Un probléme est suvenu, veuillez refaire l'opération !");
                             } else { 
                                     $("#span_leftcontener2013_form_annonces").html(zReponse);
                                     
                                        //var O_Dest_xxx = $('.leftcontener2013').height();
                                        var O_Doc_xxxx = $('.maincontener2013').height();
                                        //if (O_Doc_xxx > O_Dest_xxx) {
                                                $('.leftcontener2013').height(O_Doc_xxxx);
                                        //}
                                        //alert(O_Doc_xxxx);


                                         $(".leftcontener2013content").hide();

                                         $.post(
                                             base_url_visurba+'accueil/check_Idcategory_of_subCategory/'
                                             ,
                                             function (zReponse)
                                             {
                                                 var article_value = zReponse.split("-");
                                                 for(i = 0; i < article_value.length; i++) {
                                                     var id_to_show = "#leftcontener2013content_"+article_value[i];
                                                     $(id_to_show).show();
                                                 }
                                             }
                                         );



                             } 
                });
}


function redirect_annonce(){
    var base_url_visurba = '<?php echo site_url();?>';
    
    document.location.href = base_url_visurba+'front/annonce/redirect_annonce/';
}


function submit_search_annonce(){
    
    //setTimeout(alert('test'),5000);
    var allvalue = '';
    $("#span_leftcontener2013_form_annonces input:checkbox:checked").each(function(){
        allvalue += ','+$(this).val();
    });
    //alert(allvalue);
    $("#inputStringHidden").val(allvalue);
    $("#frmRechercheAnnonce").submit();
}

function btn_re_init_annonce_list(){
    $("#inputStringQuandHidden").val("0");
    $("#inputStringDatedebutHidden").val("");
    $("#inputStringDatefinHidden").val("");
    $("#inputStringDepartementHidden_partenaires").val("0");
    $("#inputStringVilleHidden_partenaires").val("0");
    $("#inputStringHidden").val("");
    $("#inputStringHidden_sub").val("");
    $("#inputStringOrderByHidden").val("");
    $("#zMotCle").val("");

    $("#frmRechercheAnnonce").submit();
}
    
</script>    