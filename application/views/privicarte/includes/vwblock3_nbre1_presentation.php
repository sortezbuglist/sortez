<div class="row" style="">
    <div style="width: 100%;" class="pr-4 pb-0 pt-0  pl-4 mr-4 content_sm_block">
        <div class="col-sm-12 text-center text-justify pl-0 pr-0 ml-4 sautligne content_sm_block" style="">
            <?php if (isset($glisieres->bloc_1_3_image1_content) AND $glisieres->bloc_1_3_image1_content != '' AND $glisieres->bloc_1_3_image1_content != null) { ?>
                <div style="width: 100%;height: auto">
                    <img style="text-align:center!important;max-width: 100%;height: auto;" class="img-fluid"
                         src="<?php echo base_url($path_img_gallery_gl . '/' . $glisieres->bloc_1_3_image1_content) ?>">
                </div>
            <?php } ?>
            <?php if (isset($glisieres->bloc1_3_content) AND $glisieres->bloc1_3_content != '' AND $glisieres->bloc1_3_content != null) { ?>
                <p class="pt-4 content_caract"><?php echo htmlspecialchars_decode($glisieres->bloc1_3_content) ?></p>
            <?php } ?>
            <?php if (isset($glisieres->is_activ_btn_bloc1_3) AND $glisieres->is_activ_btn_bloc1_3 == '1' AND $glisieres->is_activ_btn_bloc1_3 != null) {
                if ($glisieres->bloc1_3_existed_link1 == 'ag') {
                    $linkg = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($glisieres->bloc1_3_existed_link1 == 'art') {
                    $linkg = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($glisieres->bloc1_3_existed_link1 == 'p1') {
                    $linkg = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($glisieres->bloc1_3_existed_link1 == 'p2') {
                    $linkg = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($glisieres->bloc1_3_existed_link1 == 'bp') {
                    $linkg = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($glisieres->bloc1_3_existed_link1 == 'fd') {
                    $linkg = $link_fidelity;
                } elseif ($glisieres->bloc1_3_existed_link1 == 'bt') {
                    $linkg = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif ($glisieres->bloc1_existed_link1 == 'cmd') {
                     $linkc = base_url() . $oInfoCommercant->nom_url . '/commandes_commercants';
                } elseif (isset($glisieres->bloc1_3_custom_link1) AND $glisieres->bloc1_3_custom_link1 != '' AND $glisieres->bloc1_3_custom_link1 != null) {
                    $linkg = 'https://' . $glisieres->bloc1_3_custom_link1;
                }

                ?>

                <?php if (isset($glisieres->bloc3_islightbox) AND $glisieres->bloc3_islightbox=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkg ?? '' ?>"  class="fancybox.iframe"><div class="bouton2"><?php echo $glisieres->btn_bloc1_3_content1 ?></div></a>
                <?php }else{  ?>
                    <a class="pb-5 pt-5 lienBouton2 " href="<?php echo ($linkg) ?? '' ?>">
                        <div class="bouton2"><?php echo $glisieres->btn_bloc1_3_content1 ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>