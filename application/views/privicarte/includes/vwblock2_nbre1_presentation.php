<div class="row">
    <div style="width: 100%;" class="p-4 pb-0 pt-0  pl-0 mr-4 content_sm_block">
        <div class="col-sm-12 text-center text-justify pl-0 pr-0 ml-4 content_sm_block" style="">
            <?php if (isset($glisieres->bloc_1_2_image1_content) AND $glisieres->bloc_1_2_image1_content != '' AND $glisieres->bloc_1_2_image1_content != null) { ?>
                <div class="" style="text-align: center!important;width: 100%;height: auto">
                    <img style="max-width: 100%;height: auto;" class="img-fluid"
                         src="<?php echo base_url($path_img_gallery_gl . '/' . $glisieres->bloc_1_2_image1_content) ?>">
                </div>
            <?php } ?>
            <?php if (isset($glisieres->bloc1_2_content) AND $glisieres->bloc1_2_content != '' AND $glisieres->bloc1_2_content != null) { ?>
                <div class="pt-4 content_caract"><?php echo htmlspecialchars_decode($glisieres->bloc1_2_content) ?></div>
            <?php } ?>
            <?php if (isset($glisieres->is_activ_btn_bloc1_2) AND $glisieres->is_activ_btn_bloc1_2 == '1' AND $glisieres->is_activ_btn_bloc1_2 != null) {
                if ($glisieres->bloc1_2_existed_link1 == 'ag') {
                    $linkd = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($glisieres->bloc1_2_existed_link1 == 'art') {
                    $linkd = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($glisieres->bloc1_2_existed_link1 == 'p1') {
                    $linkd = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($glisieres->bloc1_2_existed_link1 == 'p2') {
                    $linkd = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($glisieres->bloc1_2_existed_link1 == 'bp') {
                    $linkd = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($glisieres->bloc1_2_existed_link1 == 'fd') {
                    $linkd =$link_fidelity;
                } elseif ($glisieres->bloc1_2_existed_link1 == 'bt') {
                    $linkd = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif ($glisieres->bloc1_2_existed_link1 == 'cmd') {
                    $linkd = base_url() . $oInfoCommercant->nom_url . '/commandes_commercants';
                } elseif (isset($glisieres->bloc1_2_custom_link1) AND $glisieres->bloc1_2_custom_link1 != '' AND $glisieres->bloc1_2_custom_link1 != null) {
                    $linkd = 'https://' . $glisieres->bloc1_2_custom_link1;
                }

                ?>
                <?php if (isset($glisieres->bloc1_islightbox) AND $glisieres->bloc2_islightbox=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkd ?? '' ?>"  class="fancybox.iframe"><div class="bouton2"><?php echo $glisieres->btn_bloc1_2_content1 ?></div></a>
                <?php }else{  ?>
                    <a class="pb-5 pt-5 lienBouton2 " href="<?php echo ($linkd) ?? '' ?>">
                        <div class="bouton2"><?php echo $glisieres->btn_bloc1_2_content1 ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>