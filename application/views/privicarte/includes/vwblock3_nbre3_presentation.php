<div class="row" style="">
    <div class="col-sm-4 text-center text-justify pr-0">
        <div class="ml-4 pb-4 pt-4 sautligne allign_button content_sm_block content_right_padd" style="height:auto;padding-right:0px;padding-left:0px;">
            <?php if (isset($glisieres->bloc_1_3_image1_content) AND $glisieres->bloc_1_3_image1_content != '' AND $glisieres->bloc_1_3_image1_content != null) { ?>
                <div class="image">
                    <img style="text-align:center!important;max-width: 100%;height: auto;"
                         src="<?php echo base_url($path_img_gallery_gl . '/' . $glisieres->bloc_1_3_image1_content) ?>">
                </div>
            <?php } ?>
            <?php if (isset($glisieres->bloc1_3_content) AND $glisieres->bloc1_3_content != '' AND $glisieres->bloc1_3_content != null) { ?>
                <div class="content pt-4 pb-0 content_caract"><?php echo htmlspecialchars_decode($glisieres->bloc1_3_content) ?></div>
            <?php } ?>
            <?php if (isset($glisieres->is_activ_btn_bloc1_3) AND $glisieres->is_activ_btn_bloc1_3 == '1' AND $glisieres->is_activ_btn_bloc1_3 != null) {
                if ($glisieres->bloc1_3_existed_link1 == 'ag') {
                    $linkh = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($glisieres->bloc1_3_existed_link1 == 'art') {
                    $linkh = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($glisieres->bloc1_3_existed_link1 == 'p1') {
                    $linkh = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($glisieres->bloc1_3_existed_link1 == 'p2') {
                    $linkh = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($glisieres->bloc1_3_existed_link1 == 'bp') {
                    $linkh = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($glisieres->bloc1_3_existed_link1 == 'fd') {
                    $linkh = $link_fidelity;
                } elseif ($glisieres->bloc1_3_existed_link1 == 'bt') {
                    $linkh = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif ($glisieres->bloc1_existed_link1 == 'cmd') {
                     $linkh = base_url() . $oInfoCommercant->nom_url . '/commandes_commercants';
                } elseif (isset($glisieres->bloc1_3_custom_link1) AND $glisieres->bloc1_3_custom_link1 != '' AND $glisieres->bloc1_3_custom_link1 != null) {
                    $linkh = 'https://' . $glisieres->bloc1_3_custom_link1;
                }

                ?>
                <?php if (isset($glisieres->bloc3_islightbox) AND $glisieres->bloc3_islightbox=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkh ?? '' ?>"  class="fancybox.iframe lienbtn3"><div class="bouton1"><?php echo $glisieres->btn_bloc1_3_content1 ?></div></a>
                <?php }else{  ?>
                    <a class="pb-5 pt-5 lienBouton " href="<?php echo ($linkh) ?? '' ?>">
                        <div class="bouton1"><?php echo $glisieres->btn_bloc1_3_content1 ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <div class="col-sm-4 text-center pl-0 text-justify content_sm_nb2 content_right_padd">
        <div class="p-4 allign_button content_sm_nb2ct" style=";height:auto;padding-right:0px!important">
            <?php if (isset($glisieres->bloc_1_3_image2_content) AND $glisieres->bloc_1_3_image2_content != '' AND $glisieres->bloc_1_3_image2_content != null) { ?>
                <div class="image">
                    <img style="text-align:center!important;max-width: 100%;height: auto;"
                         src="<?php echo base_url($path_img_gallery_gl . '/' . $glisieres->bloc_1_3_image2_content) ?>">
                </div>
            <?php } ?>
            <?php if (isset($glisieres->bloc1_3_content2) AND $glisieres->bloc1_3_content2 != '' AND $glisieres->bloc1_3_content2 != null) { ?>
                <div class="content pt-4 pb-0 content_caract"><?php echo htmlspecialchars_decode($glisieres->bloc1_3_content2) ?></div>
            <?php } ?>
            <?php if (isset($glisieres->is_activ_btn_bloc2_3) AND $glisieres->is_activ_btn_bloc2_3 == '1' AND $glisieres->is_activ_btn_bloc2_3 != null) {
                if ($glisieres->bloc1_3_existed_link2 == 'ag') {
                    $linki = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($glisieres->bloc1_3_existed_link2 == 'art') {
                    $linki = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($glisieres->bloc1_3_existed_link2 == 'p1') {
                    $linki = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($glisieres->bloc1_3_existed_link2 == 'p2') {
                    $linki = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($glisieres->bloc1_3_existed_link2 == 'bp') {
                    $linki = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($glisieres->bloc1_3_existed_link2 == 'fd') {
                    $linki = $link_fidelity;
                } elseif ($glisieres->bloc1_3_existed_link2 == 'bt') {
                    $linki = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif ($glisieres->bloc1_existed_link1 == 'cmd') {
                     $linki = base_url() . $oInfoCommercant->nom_url . '/commandes_commercants';
                } elseif (isset($glisieres->bloc1_3_custom_link2) AND $glisieres->bloc1_3_custom_link2 != '' AND $glisieres->bloc1_3_custom_link2 != null) {
                    $linki = 'https://' . $glisieres->bloc1_3_custom_link2;
                }

                ?>
                <?php if (isset($glisieres->bloc3_islightbox2) AND $glisieres->bloc3_islightbox2=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linki ?? '' ?>"  class="fancybox.iframe lienbtn3"><div class="bouton1"><?php echo $glisieres->btn_bloc1_3_content2 ?></div></a>
                <?php }else{  ?>
                    <a class="pb-5 pt-5 lienBouton " href="<?php echo ($linki) ?? '' ?>">
                        <div class="bouton1"><?php echo $glisieres->btn_bloc1_3_content2 ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <!-- bloc home debut --->
    <div class="col-sm-4 text-center pl-0 text-justify content_sm_nb2 content_right_padd">
        <div class="p-4 allign_button content_sm_nb2ct" style=";height:auto;padding-right:0px!important">
            <?php if (isset($glisieres->bloc_1_3_image3_content) AND $glisieres->bloc_1_3_image3_content != '' AND $glisieres->bloc_1_3_image3_content != null) { ?>
                <div class="image">
                    <img style="text-align:center!important;max-width: 100%;height: auto;"
                         src="<?php echo base_url($path_img_gallery_gl . '/' . $glisieres->bloc_1_3_image3_content) ?>">
                </div>
            <?php } ?>
            <?php if (isset($glisieres->bloc1_3_content3) AND $glisieres->bloc1_3_content3 != '' AND $glisieres->bloc1_3_content3 != null) { ?>
                <div class="content pt-4 pb-0 content_caract"><?php echo htmlspecialchars_decode($glisieres->bloc1_3_content3) ?></div>
            <?php } ?>
            <?php if (isset($glisieres->is_activ_btn_bloc3_3) AND $glisieres->is_activ_btn_bloc3_3 == '1' AND $glisieres->is_activ_btn_bloc3_3 != null) {
                if ($glisieres->bloc1_3_existed_link3 == 'ag') {
                    $linki3 = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($glisieres->bloc1_3_existed_link3 == 'art') {
                    $linki3 = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($glisieres->bloc1_3_existed_link3 == 'p1') {
                    $linki3 = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($glisieres->bloc1_3_existed_link3 == 'p2') {
                    $linki3 = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($glisieres->bloc1_3_existed_link3 == 'bp') {
                    $linki3 = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($glisieres->bloc1_3_existed_link3 == 'fd') {
                    $linki3 =$link_fidelity;
                } elseif ($glisieres->bloc1_3_existed_link3 == 'bt') {
                    $linki3 = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif ($glisieres->bloc1_existed_link1 == 'cmd') {
                     $linki3 = base_url() . $oInfoCommercant->nom_url . '/commandes_commercants';
                } elseif (isset($glisieres->bloc1_3_custom_link3) AND $glisieres->bloc1_3_custom_link3 != '' AND $glisieres->bloc1_3_custom_link3 != null) {
                    $linki3 = 'https://' . $glisieres->bloc1_3_custom_link3;
                }

                ?>
                <?php if (isset($glisieres->bloc3_islightbox3) AND $glisieres->bloc3_islightbox3=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linki3 ?? '' ?>"  class="fancybox.iframe lienbtn3"><div class="bouton1"><?php echo $glisieres->btn_bloc1_3_content3 ?></div></a>
                <?php }else{  ?>
                    <a class="pb-5 pt-5 lienBouton " href="<?php echo ($linki3) ?? '' ?>">
                        <div class="bouton1"><?php echo $glisieres->btn_bloc1_3_content3 ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <!-- bloc home fin --->
</div>