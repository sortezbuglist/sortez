<?php
$this_session_localdata =& get_instance();
$this_session_localdata->load->library('session');
$publightbox = $this_session_localdata->session->userdata('publightbox');
if(isset($publightbox) && $publightbox!="" && $publightbox!=NULL) {} else {
?>

<script type="text/javascript">
$(document).ready(function() {
	setTimeout(function(){ 
			$("#Idpublightbox_link").fancybox({
				'autoScale' : false,
				'overlayOpacity'      : 0.8, // Set opacity to 0.8
				'overlayColor'        : "#000000", // Set color to Black
				'padding'         : 5,
				'width'         : 750,
				'height'        : 'auto',
				'scrolling'   : 'no',
				'background-color'	: '#ffffff',
				'transitionIn'      : 'elastic',
				'transitionOut'     : 'elastic'
			}).trigger('click');
		}, 6000);
		
	
	$("#publightbox_mail").focusin(function() {	  if ($(this).val()=="Enregistrer votre adresse email") $(this).val('');	});
	$("#publightbox_mail").focusout(function() {	  if ($(this).val()=="") $(this).val('Enregistrer votre adresse email');	});
		
	$("#publightbox_submit").click(function() {
	
		$("#spanpublightbox_loading").html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
		var error = "";
		var publightbox_mail = $("#publightbox_mail").val();
		var publightbox_departement_value = $("#publightbox_departement_value").val();
		
		if (publightbox_mail == '' || publightbox_mail == 'Enregistrer votre adresse email') {
			error += '<span style="color:#FF0000;"> - Veuillez Enregistrer votre e-mail</span>';
			$("#spanpublightbox_loading").removeClass("alert alert-success").addClass("alert alert-danger");
		}
		if (!validateEmail(publightbox_mail)) {
			error += '<span style="color:#FF0000;"> - Adresse email invalide</span>';
			$("#spanpublightbox_loading").removeClass("alert alert-success").addClass("alert alert-danger");
		}
		/*if (publightbox_departement_value == '' || publightbox_departement_value == '0' || publightbox_departement_value == '00') {
			error += '<span style="color:#FF0000;"> - Veuillez choisir votre departement</span>';
			$("#spanpublightbox_loading").removeClass("alert alert-success").addClass("alert alert-danger");
		}*/
		
		if (error != "") {
			$("#spanpublightbox_loading").html(error);
			$("#spanpublightbox_loading").removeClass("alert alert-success").addClass("alert alert-danger");
		} else {
			//$("#spanpublightbox_loading").html(publightbox_mail+" "+publightbox_departement_value);
			$("#spanpublightbox_loading").removeClass("alert alert-danger").addClass("alert alert-success");
		
			$("#spanpublightbox_loading").html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
			
			$.post(
				"<?php echo site_url("front/utilisateur/lightbox_mail_verify/");?>", 
				{
					publightbox_mail:publightbox_mail
				},
				function(data){
					 if (data=="1") {
					 	$("#spanpublightbox_loading").html('<span style="color:#FF0000;"> - Votre e-mail est d&eacute;j&agrave; enregistr&eacute; !</span>');
						$("#spanpublightbox_loading").removeClass("alert alert-success").addClass("alert alert-danger");
					 } else {
					 	
							$.post(
								"<?php echo site_url("front/utilisateur/lightbox_mail_save/");?>", 
								{
									publightbox_mail:publightbox_mail,
									publightbox_departement_value:publightbox_departement_value
								},
								function( data ) {
									if (data=="1") {
										$("#spanpublightbox_loading").html('<span style="color:#06BC00;"> - Votre e-mail est enregistr&eacute; avec succ&egrave;s !</span>');
										$("#spanpublightbox_loading").removeClass("alert alert-danger").addClass("alert alert-success");
										if(typeof $.fancybox == 'function') {
											 setTimeout(function(){ window.parent.$.fancybox.close(); }, 10000);
										} 
									} else {
										$("#spanpublightbox_loading").html('<span style="color:#FF0000;"> - Une erreur est survenue, veuillez enregistrer votre email ult&eacute;rieurement !</span>');
										$("#spanpublightbox_loading").removeClass("alert alert-success").addClass("alert alert-danger");
									}
								});
							
					 }
				});
				
		}	
	});
	
	$.ajax({
        type : 'POST',
        url : '<?php echo site_url("front/utilisateur/alldepartement"); ?>',           
        success:function (data) {
            $('#publightbox_departement_ul').empty().append(data); 
        }          
    });
	
});

function validateEmail(sEmail) {
	var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if (filter.test(sEmail)) {
		return true;
	}
	else {
		return false;
	}
}
function lightbox_depart_select(xCode = "00", xText = "") {
	$("#publightbox_departement").text(xText);
	$("#publightbox_departement_value").val(xCode);
}
</script>
<style type="text/css">
<!--
.container_Idpublightbox .pub_form_content {
	padding: 15px;
	width: 100%;
	display: table;
}
#view_Idpublightbox .pub_form_content #publightbox_form .pub_frm_item20 #publightbox_submit {
	background-color: #FFFFFF;
	background-image: url(<?php echo GetImagePath("privicarte/");?>/publightbox_submit.png);
	background-repeat: no-repeat;
	background-position: center top;
	height: 55px;
	width: 138px;
	padding: 15px;
	border: none;
}
#view_Idpublightbox .pub_form_content #publightbox_form .pub_frm_item40 #publightbox_mail {
	background-image: url(<?php echo GetImagePath("privicarte/");?>/publightbox_mail.png);
	background-repeat: no-repeat;
	background-position: center top;
	background-color: #FFFFFF;
	height: 55px;
	width: 260px;
	padding: 10px 15px 15px;
	border: none;
	font-weight: bold;
}
#view_Idpublightbox .pub_form_content #publightbox_form .pub_frm_item40 .dropup #publightbox_departement {
	background-image: url(<?php echo GetImagePath("privicarte/");?>/publightbox_mail.png);
	background-repeat: no-repeat;
	background-position: center top;
	background-color: #FFFFFF;
	height: 55px;
	width: 260px;
	padding: 10px 15px 15px;
	border: none;
	font-weight: bold;
	border:none;
	box-shadow: none !important;
}
#view_Idpublightbox #spanpublightbox_loading {
	text-align: center;
	width: 100%;
}
.container_Idpublightbox {
	background-color: #FFFFFF;
}
.pub_frm_item40 {
	float: left;
	<?php 
	$localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
	if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != NULL) {
	?>
	width: 60%;
	<?php } else { ?>
	width: 38%;
	<?php } ?>
	position: relative;
	text-align: center;
}
.pub_frm_item20 {
	float: left;
	width: 24%;
	position: relative;
	text-align: center;
}
.divPublightbox_link {
	display:none;
}
#view_Idpublightbox .pub_text_content {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 20px;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	padding: 15px;
}

#publightbox_departement_ul {
    padding: 5px 10px 0;
    max-height: 300px;
    overflow-y: scroll;
}
.pub_img_content {
	text-align: center;
}
-->
</style>

<!--
-->

<div class="divPublightbox_link"><a id="Idpublightbox_link" href="#view_Idpublightbox">Content for  class "divPublightbox_link" Goes Here</a></div>

<div id="view_Idpublightbox" class="container_Idpublightbox">
	<div class="pub_img_content"><img src="<?php echo GetImagePath("privicarte/"); ?>/publightbox.jpg" alt="Privicarte-news" /></div>
    
    <div class="pub_text_content">Acc&egrave;s imm&eacute;diat aux offres et avantages</div>
    <div class="pub_form_content">
    <form name="publightbox_form" id="publightbox_form" enctype="multipart/form-data" method="post"  action="">
          <div class="pub_frm_item40"><input name="publightbox_mail" class="publightbox_mail" id="publightbox_mail" type="text" value="Enregistrer votre adresse email"/></div>
		  <?php 
		  $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
		  if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != NULL) {} else {
		  ?>
          <div class="pub_frm_item40">

<input name="publightbox_departement_value" class="publightbox_departement_value" id="publightbox_departement_value" type="hidden" value="0"/>
<div class="dropup">
<button id="publightbox_departement" class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Choisissez votre d&eacute;partement
<span class="caret"></span></button>
<ul id="publightbox_departement_ul" class="dropdown-menu"></ul>
</div>

<?php 
    if(isset($toDepartement)) 
    {
        foreach($toDepartement as $oDepartement){
            if ($oDepartement->departement_id == $iDepartementId) echo $oDepartement->departement_nom." (".$oDepartement->nbCommercant.")";
        }
    }
?>

          </div>
          <?php } ?>
          <div class="pub_frm_item20"><input name="publightbox_submit" class="publightbox_submit" id="publightbox_submit" type="button" /></div>
    </form>      
    </div>
    <div id="spanpublightbox_loading" class="spanpublightbox_loading"></div>
</div>

<?php $this->session->set_userdata('publightbox', "1"); ?>
<?php } ?>