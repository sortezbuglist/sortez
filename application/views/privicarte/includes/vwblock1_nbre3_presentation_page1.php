<div class="row ml-4 pl-0 pb-4 pt-4 pr-4" style="">
    <?php     $path_img_gallery_gl = 'application/resources/front/photoCommercant/imagesbank/'.$oInfoCommercant->user_ionauth_id;
    ?>
    <div class="col-sm-4 text-center text-justify pr-0">
        <div class="pl-2 pb-4 pt-4 pr-4 allign_button" style="height:auto;padding-right:0px;padding-left:0px;">

            <?php if (isset($objbloc_info->bloc_1_image1_content_page1) AND $objbloc_info->bloc_1_image1_content_page1 != '' AND $objbloc_info->bloc_1_image1_content_page1 != null) { ?>

                <div class="image">

                    <img style="max-width: 100%;height: auto;"

                         src="<?php echo base_url($path_img_gallery_gl . '/' . $objbloc_info->bloc_1_image1_content_page1) ?>">

                </div>

            <?php } ?>

            <?php if (isset($objbloc_info->bloc1_content_page1) AND $objbloc_info->bloc1_content_page1 != '' AND $objbloc_info->bloc1_content_page1 != null) { ?>

                <div class="pb-0 pt-2 content"><?php echo htmlspecialchars_decode($objbloc_info->bloc1_content_page1) ?></div>

            <?php } ?>

            <?php if (isset($objbloc_info->is_activ_btn_bloc1_page1) AND $objbloc_info->is_activ_btn_bloc1_page1 == '1' AND $objbloc_info->is_activ_btn_bloc1_page1 != null) {

                if ($objbloc_info->bloc1_existed_link1_page1 == 'ag') {

                    $linkc = base_url() . $oInfoCommercant->nom_url . '/agenda';

                } elseif ($objbloc_info->bloc1_existed_link1_page1 == 'art') {

                    $linkc = base_url() . $oInfoCommercant->nom_url . '/article';

                } elseif ($objbloc_info->bloc1_existed_link1_page1 == 'p1') {

                    $linkc = base_url() . $oInfoCommercant->nom_url . '/infos';

                } elseif ($objbloc_info->bloc1_existed_link1_page1 == 'p2') {

                    $linkc = base_url() . $oInfoCommercant->nom_url . '/autresinfos';

                } elseif ($objbloc_info->bloc1_existed_link1_page1 == 'bp') {

                    $linkc = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';

                } elseif ($objbloc_info->bloc1_existed_link1_page1 == 'fd') {

                    $linkc =$link_fidelity;

                } elseif ($objbloc_info->bloc1_existed_link1_page1 == 'bt') {

                    $linkc = base_url() . $oInfoCommercant->nom_url . '/annonces';

                } elseif (isset($objbloc_info->bloc1_custom_link1_page1) AND $objbloc_info->bloc1_custom_link1_page1 != '' AND $objbloc_info->bloc1_custom_link1_page1 != null) {

                    $linkc = 'https://' . $objbloc_info->bloc1_custom_link1_page1;

                }



                ?>

                <?php if (isset($objbloc_info->bloc1_islightbox_page1) AND $objbloc_info->bloc1_islightbox_page1 =="1"){ ?>



                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkc ?? '' ?>"  class="fancybox.iframe"><div class="bouton1"><?php echo $objbloc_info->btn_bloc1_content1_page1 ?></div></a>

                <?php }else{  ?>

                    <a class="pb-5 pt-5 lienBouton " href="<?php echo ($linkc) ?? '' ?>">

                        <div class="bouton1"><?php echo $objbloc_info->btn_bloc1_content1_page1 ?></div>

                    </a>

                <?php } ?>

            <?php } ?>

        </div>

    </div>

    <div class="col-sm-4 text-center pl-0 text-justify">
        <div class="p-4 sautligne allign_button" style=";height:auto;padding-right:0px!important;">

            <?php if (isset($objbloc_info->bloc_1_image2_content_page1) AND $objbloc_info->bloc_1_image2_content_page1 != '' AND $objbloc_info->bloc_1_image2_content_page1 != null) { ?>

                <div class="image">

                    <img style="max-width: 100%;height: auto;"

                         src="<?php echo base_url($path_img_gallery_gl . '/' . $objbloc_info->bloc_1_image2_content_page1) ?>">

                </div>

            <?php } ?>

            <?php if (isset($objbloc_info->bloc1_content2_page1) AND $objbloc_info->bloc1_content2_page1 != '' AND $objbloc_info->bloc1_content2_page1 != null) { ?>

                <div class="pb-0 pt-2 content"><?php echo htmlspecialchars_decode($objbloc_info->bloc1_content2_page1) ?></div>

            <?php } ?>

            <?php if (isset($objbloc_info->is_activ_btn_bloc2_page1) AND $objbloc_info->is_activ_btn_bloc2_page1 == '1' AND $objbloc_info->is_activ_btn_bloc2_page1 != null) {

                if ($objbloc_info->bloc1_existed_link2_page1 == 'ag') {

                    $link2 = base_url() . $oInfoCommercant->nom_url . '/agenda';

                } elseif ($objbloc_info->bloc1_existed_link2_page1 == 'art') {

                    $link2 = base_url() . $oInfoCommercant->nom_url . '/article';

                } elseif ($objbloc_info->bloc1_existed_link2_page1 == 'p1') {

                    $link2 = base_url() . $oInfoCommercant->nom_url . '/infos';

                } elseif ($objbloc_info->bloc1_existed_link2_page1 == 'p2') {

                    $link2 = base_url() . $oInfoCommercant->nom_url . '/autresinfos';

                } elseif ($objbloc_info->bloc1_existed_link2_page1 == 'bp') {

                    $link2 = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';

                } elseif ($objbloc_info->bloc1_existed_link2_page1 == 'fd') {

                    $link2 = $link_fidelity;

                } elseif ($objbloc_info->bloc1_existed_link2_page1 == 'bt') {

                    $link2 = base_url() . $oInfoCommercant->nom_url . '/annonces';

                } elseif (isset($objbloc_info->bloc1_custom_link2_page1) AND $objbloc_info->bloc1_custom_link2_page1 != '' AND $objbloc_info->bloc1_custom_link2_page1 != null) {

                    $link2 = 'https://' . $objbloc_info->bloc1_custom_link2_page1;

                }



                ?>

                <?php if (isset($objbloc_info->bloc1_islightbox2_page1) AND $objbloc_info->bloc1_islightbox2_page1=="1"){ ?>



                    <a data-fancybox data-type="iframe" data-src="<?php echo $link2 ?? '' ?>"  class="fancybox.iframe"><div class="bouton1"><?php echo $objbloc_info->btn_bloc1_content2_page1 ?></div></a>

                <?php }else{  ?>

                    <a class="pb-5 pt-5 lienBouton " href="<?php echo ($link2) ?? '' ?>">

                        <div class="bouton1"><?php echo $objbloc_info->btn_bloc1_content2_page1 ?></div>

                    </a>

                <?php } ?>

            <?php } ?>

        </div>

    </div>
    <!-- bloc3 debut --->
    <div class="col-sm-4 text-center pl-0 text-justify">
        <div class="p-4 sautligne allign_button" style=";height:auto;padding-right:0px!important;">

            <?php if (isset($objbloc_info->bloc_1_image3_content_page1) AND $objbloc_info->bloc_1_image3_content_page1 != '' AND $objbloc_info->bloc_1_image3_content_page1 != null) { ?>

                <div class="image">

                    <img style="max-width: 100%;height: auto;"

                         src="<?php echo base_url($path_img_gallery_gl . '/' . $objbloc_info->bloc_1_image3_content_page1) ?>">

                </div>

            <?php } ?>

            <?php if (isset($objbloc_info->bloc1_content3_page1) AND $objbloc_info->bloc1_content3_page1 != '' AND $objbloc_info->bloc1_content3_page1 != null) { ?>

                <div class="pb-0 pt-2 content"><?php echo htmlspecialchars_decode($objbloc_info->bloc1_content3_page1) ?></div>

            <?php } ?>

            <?php if (isset($objbloc_info->is_activ_btn_bloc3_page1) AND $objbloc_info->is_activ_btn_bloc3_page1 == '1' AND $objbloc_info->is_activ_btn_bloc3_page1 != null) {

                if ($objbloc_info->bloc1_existed_link3_page1 == 'ag') {

                    $linkw = base_url() . $oInfoCommercant->nom_url . '/agenda';

                } elseif ($objbloc_info->bloc1_existed_link3_page1 == 'art') {

                    $linkw = base_url() . $oInfoCommercant->nom_url . '/article';

                } elseif ($objbloc_info->bloc1_existed_link3_page1 == 'p1') {

                    $linkw = base_url() . $oInfoCommercant->nom_url . '/infos';

                } elseif ($objbloc_info->bloc1_existed_link3_page1 == 'p2') {

                    $linkw = base_url() . $oInfoCommercant->nom_url . '/autresinfos';

                } elseif ($objbloc_info->bloc1_existed_link3_page1 == 'bp') {

                    $linkw = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';

                } elseif ($objbloc_info->bloc1_existed_link3_page1 == 'fd') {

                    $linkw = $link_fidelity;

                } elseif ($objbloc_info->bloc1_existed_link3_page1 == 'bt') {

                    $linkw = base_url() . $oInfoCommercant->nom_url . '/annonces';

                } elseif (isset($objbloc_info->bloc1_custom_link3_page1) AND $objbloc_info->bloc1_custom_link3_page1 != '' AND $objbloc_info->bloc1_custom_link3_page1 != null) {

                    $linkw = 'https://' . $objbloc_info->bloc1_custom_link3_page1;

                }



                ?>

                <?php if (isset($objbloc_info->bloc1_islightbox3_page1) AND $objbloc_info->bloc1_islightbox3_page1=="1"){ ?>



                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkw ?? '' ?>"  class="fancybox.iframe"><div class="bouton1"><?php echo $objbloc_info->btn_bloc1_content3_page1 ?></div></a>

                <?php }else{  ?>

                    <a class="pb-5 pt-5 lienBouton " href="<?php echo ($linkw) ?? '' ?>">

                        <div class="bouton1"><?php echo $objbloc_info->btn_bloc1_content3_page1 ?></div>

                    </a>

                <?php } ?>

            <?php } ?>

        </div>

    </div>
    <!-- bloc3 fin --->

</div>