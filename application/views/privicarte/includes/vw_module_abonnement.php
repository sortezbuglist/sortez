<style type="text/css">
    #btn_demander:hover {

        background-color: rgb(220, 24, 142);
        text-decoration: none;
        cursor: pointer;

    }

    #btn_demander {
        background-color: rgb(0, 128, 0);
        width: 200px;
        text-decoration: none;
    }

    #btn_demander a:visited {
        color: white;
        text-decoration: none;
    }
</style>
<div class="pl-4 pr-4 pt-0 pb-0">
    <div class="row p-0" style="background-color: rgb(218, 255, 255)">
        <div class="col-12 text-center pt-4">
            <h3 style="color: ">FONCTIONS COMPLÉMENTAIRES
                OPTIONNELLES PAYANTES</h3>
        </div>
        <div class="col-12 mb-5">
            <div class="row">
                <table class="table table-bordered">
                    <thead class="">
                    <tr class="text-center table-success">
                        <th style="width: 30%" scope="col">Nom</th>
                        <th style="width: 30%" scope="col">Description</th>
                        <th style="width: 15%" scope="col">Montant</th>
                        <th style="width: 15%" scope="col">Total (€)</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (isset($objModule)) { ?>
                        <?php foreach ($objModule as $module) { ?>

                            <script type="text/javascript">
                                jQuery(document).ready(function () {
                                    ht = 0;
                                    tva = 0;
                                    ttc = 0;
                                    jQuery("#check<?php echo $module->IdAbonnement;?>").click(function () {
                                        if (jQuery("#check<?php echo $module->IdAbonnement;?>").is(':checked')) {
                                            jQuery('#m_<?php echo $module->IdAbonnement;?>').val('on_<?php echo $module->IdAbonnement;?>');
                                        } else {
                                            jQuery('#m_<?php echo $module->IdAbonnement;?>').val('off_<?php echo $module->IdAbonnement;?>');
                                        }

                                    });
                                    jQuery("#check<?php echo $module->IdAbonnement?>").change(function () {
                                        if (jQuery("#check<?php echo $module->IdAbonnement;?>").is(':checked')) {
                                            $("#sous_total_<?php echo $module->IdAbonnement;?>").html('<?php echo $module->tarif; ?>');
                                            $("#sous_total_<?php echo $module->IdAbonnement;?>").css('background-color', 'rgb(255, 238, 186)');
                                            ht +=<?php echo $module->tarif; ?>;
                                            $("#value_ht").html(ht);
                                            tva = (ht * 20) / 100;
                                            $("#value_tva").html(tva);
                                            ttc = ht + tva;
                                            $("#value_ttc").html(ttc);
                                            $("#form_value_ttc").val(ttc);
                                        } else {
                                            $("#sous_total_<?php echo $module->IdAbonnement;?>").html('');
                                            $("#sous_total_<?php echo $module->IdAbonnement;?>").css('background-color', 'rgb(253, 253, 254)');
                                            ht -=<?php echo $module->tarif; ?>;
                                            $("#value_ht").html(ht);
                                            tva = (ht * 20) / 100;
                                            $("#value_tva").html(tva);
                                            ttc = ht + tva;
                                            $("#value_ttc").html(ttc);
                                            $("#form_value_ttc").val(ttc);
                                        }
                                    });
                                });
                            </script>

                            <tr>
                                <td scope="row"><input type="hidden" value="off_<?php echo $module->IdAbonnement; ?>"
                                                       id="m_<?php echo $module->IdAbonnement ?>"><input type="checkbox"
                                                                                                         id="check<?php echo $module->IdAbonnement ?>">&nbsp;&nbsp;<span
                                            style="font-weight: bold;color: rgb(220, 36, 154);font-size: 13px"><?php echo $module->Nom ?></span>:<br>
                                </td>
                                <td colspan="1"><span style="font-size:12px"><?php echo $module->Description; ?></span><br></td>
                                <td class="text-center table-primary"><span
                                            style="color: deeppink;font-size: 15px"><?php echo $module->tarif; ?>
                                        €</span></td>
                                <td class="text-center table-light" style="color: blue;font-size: 13px"
                                    id="sous_total_<?php echo $module->IdAbonnement; ?>"></td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td class="text-center  table-active"
                            style="font-weight: bold;color: rgb(220, 36, 154);font-size: 13px;font-size: 13px">Montant
                            ht:
                        </td>
                        <td class="text-center table-warning" style="color:blue;font-size: 13px" id="value_ht">0</td>
                        <input type="hidden" value="" id="form_value_ht">
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td class="text-center table-active"
                            style="font-weight: bold;color: rgb(220, 36, 154);font-size: 13px">T.V.A 20%
                        </td>
                        <td class="text-center  table-warning" style="color:blue;font-size: 13px" id="value_tva">0</td>
                        <input type="hidden" value="" id="form_value_tva">
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td class="text-center  table-active"
                            style="font-weight: bold;color: rgb(220, 36, 154);font-size: 13px;">Montant TTC
                        </td>
                        <td class="text-center table-danger" style="color:blue;font-size: 13px" id="value_ttc">0</td>
                        <input type="hidden" value="" id="form_value_ttc">

                    </tr>
                    </tbody>
                </table>
                <!--<div class="col-3">
                        <?php if (isset($module->image) AND $module->image != '' AND $module->image != null AND file_exists('application/resources/front/images/image_module/' . $module->image . '.png')) { ?>
                            <img src="<?php echo base_url('application/resources/front/images/image_module/' . $module->image . '.png') ?>" width="50px" height="50px">
                        <?php } elseif (isset($module->image) AND $module->image != '' AND $module->image != null AND file_exists('application/resources/front/images/image_module/' . $module->image . '.jpg')) { ?>
                            <img src="<?php echo base_url('application/resources/front/images/image_module/' . $module->image . '.jpg') ?>" width="50px" height="50px">
                        <?php } ?>
                    </div> !-->
            </div>
            <div class="row">
                <div class="col-12">
                    <p class="pt-3" style="font-size: 12px">Validez votre demande, par retour de mail vous recevez une confirmation de réception.
                        Sous 24h00 après étude de votre demande et validation de notre service, nous vous adresserons un
                        document conforme à votre organisation administrative (précisez ci-dessous la qualité de ce document
                        et vos conditions de règlement)
                    </p>
                </div>
            </div>
            <div class="row pt-4">
                <div class="col-12">
                    <div style="font-size: 15px;text-decoration: underline" class="col-12 text-center">Type de demande
                    </div>
                    <div class="col-12">
                        <div class="row" style="font-size: 13px;">
                            <div class="col-4"><input name="type_demande" type="radio" value="un devis">&nbsp;Un devis
                            </div>
                            <div class="col-4"><input name="type_demande" type="radio" value="une facture proforma">&nbsp;Une
                                facture proforma
                            </div>
                            <div class="col-4"><input name="type_demande" type="radio" value="une facture">&nbsp;Une
                                facture
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pt-4">
                <div class="col-12">
                    <div style="font-size: 15px;text-decoration: underline" class="col-12 text-center">Mode de
                        paiement
                    </div>
                    <div class="col-12">
                        <div class="row" style="font-size: 13px;">
                            <div class="col-4"><input name="type_paiement" type="radio" value="Chèque">&nbsp;Chèque
                            </div>
                            <div class="col-4"><input name="type_paiement" type="radio" value="Virement">&nbsp;Virement
                            </div>
                            <div class="col-4"><input name="type_paiement" type="radio"
                                                      value="Carte bancaire avec paypal">&nbsp;Carte bancaire avec
                                paypal
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <input type="hidden" id="val_type_demande">
        <input type="hidden" id="val_type_paiement">
        <div class="m-auto pt-2 pb-2 text-center" id="btn_demander" style="font-size: 18px;color: white">
            Demander
        </div>
        <div class="mt-5 p-5"></div>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        $('input[type=radio][name=type_demande]').on('change', function () {
            $('#val_type_demande').val($(this).val());
        });

        $('input[type=radio][name=type_paiement]').on('change', function () {
            $('#val_type_paiement').val($(this).val());
        });
        jQuery("#btn_demander").click(function () {
            $("#btn_demander").html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            <?php foreach ($objModule as $mod) {?>
            var <?php echo 'k_' . (string)$mod->IdAbonnement ?> =
            $('<?php echo '#m_' . (string)$mod->IdAbonnement ?>').val();
            <?php } ?>
            var val_type_demande = $("#val_type_demande").val();
            var val_type_paiement = $("#val_type_paiement").val();
            var price = $("#form_value_ttc").val();
            var obj = {<?php foreach ($objModule as $mod){ ?> <?php echo 'r_' . (string)$mod->IdAbonnement ?> : <?php echo 'k_' . (string)$mod->IdAbonnement ?>,
            <?php } ?>'Nom_complet'
        :
            '<?php echo $toCommercant->NomSociete;?>', 'mail'
        :
            '<?php echo $toCommercant->Email;?>', 'price'
        :
            price, 'val_type_demande'
        :
            val_type_demande, 'val_type_paiement'
        :
            val_type_paiement,
        }
            ;
            var datas = JSON.stringify(obj);
            console.log(datas);
            jQuery.ajax({
                url: '<?php echo site_url('front/professionnels/valid_demande_module_platinum')?>',
                type: 'POST',
                data: 'object=' + datas,
                async: true,
                success: function (datase) {
                    if (datase === 'ok') {
                        alert('Votre demande a été envoyé');
                        jQuery('#btn_demander').html("Demande envoyée !");
                    } else {
                        alert('Une erreur s\'est produite');
                    }
                }
            });
        });
    });
</script>