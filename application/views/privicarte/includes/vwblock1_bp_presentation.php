<div class="row" style="width: 100%">
    <div class="ml-4" style="width: 100%;">
        <div class="col-sm-12 p-4 ml-4 sautligne" style="background-image: url('<?php echo base_url('assets/img/bonplan_new.png') ?>');background-repeat: no-repeat;width: 100%;height: 250px;background-size: 100% 100%;">
            <div class="d-flex justify-content-center" style="padding-left: 140px;width: 50%;padding-top: 20px">
                    <?php if (isset($glisieres->bonplan_id) AND $glisieres->bonplan_id > 0 AND $glisieres->bonplan_id != null AND $glisieres->bonplan_id != "") {
                        $this->load->model('mdlbonplan');
                        $bonplan_contenue = $this->mdlbonplan->get_bonplan_by_id($glisieres->bonplan_id);
                    } ?>
                    <?php if (isset($bonplan_contenue) AND $bonplan_contenue != null) { ?>
                        <div class="row">
                        <div class="col-sm-12">
                            <h4 style="color:white"><?php echo $bonplan_contenue->bonplan_titre; ?></h4>
                        </div>
                        <script type="text/javascript">
                            function compte_a_rebours_<?php echo $glisieres->bonplan_id; ?>() {
                                var compte_a_rebours_<?php echo $glisieres->bonplan_id; ?> = document.getElementById("compte_a_rebours_<?php echo $glisieres->bonplan_id; ?>");

                                var date_actuelle = new Date();
                                var date_evenement = new Date("<?php if ($bonplan_contenue->bonplan_date_fin !=''){ echo $bonplan_contenue->bonplan_date_fin; }elseif ($bonplan_contenue->bp_unique_date_fin !=''){echo $bonplan_contenue->bp_unique_date_fin;}elseif ($bonplan_contenue->bp_multiple_date_fin !=''){echo $bonplan_contenue->bp_multiple_date_fin;} ?>");
                                var total_secondes = (date_evenement - date_actuelle) / 1000;

                                var prefixe = "";
                                if (total_secondes < 0) {
                                    prefixe = "Exp "; // On modifie le préfixe si la différence est négatif

                                    total_secondes = Math.abs(total_secondes); // On ne garde que la valeur absolue

                                }

                                if (total_secondes > 0) {
                                    var jours = Math.floor(total_secondes / (60 * 60 * 24));
                                    var heures = Math.floor((total_secondes - (jours * 60 * 60 * 24)) / (60 * 60));
                                    minutes = Math.floor((total_secondes - ((jours * 60 * 60 * 24 + heures * 60 * 60))) / 60);
                                    secondes = Math.floor(total_secondes - ((jours * 60 * 60 * 24 + heures * 60 * 60 + minutes * 60)));

                                    var et = "et";
                                    var mot_jour = "jours";
                                    var mot_heure = "heures,";
                                    var mot_minute = "minutes,";
                                    var mot_seconde = "secondes";

                                    if (jours == 0) {
                                        jours = '';
                                        mot_jour = '';
                                    }
                                    else if (jours == 1) {
                                        mot_jour = "jour,";
                                    }

                                    if (heures == 0) {
                                        heures = '';
                                        mot_heure = '';
                                    }
                                    else if (heures == 1) {
                                        mot_heure = "heure,";
                                    }

                                    if (minutes == 0) {
                                        minutes = '';
                                        mot_minute = '';
                                    }
                                    else if (minutes == 1) {
                                        mot_minute = "minute,";
                                    }

                                    if (secondes == 0) {
                                        secondes = '';
                                        mot_seconde = '';
                                        et = '';
                                    }
                                    else if (secondes == 1) {
                                        mot_seconde = "seconde";
                                    }

                                    if (minutes == 0 && heures == 0 && jours == 0) {
                                        et = "";
                                    }

                                    //compte_a_rebours.innerHTML = prefixe + jours + ' ' + mot_jour + ' ' + heures + ' ' + mot_heure + ' ' + minutes + ' ' + mot_minute + ' ' + et + ' ' + secondes + ' ' + mot_seconde;
                                    //compte_a_rebours_<?php //echo $glisieres->bonplan_id ; ?>.innerHTML = prefixe + jours + ' ' + mot_jour + ' ' + heures + ':' + minutes + ':' + secondes ;
                                    compte_a_rebours_<?php echo $glisieres->bonplan_id; ?>.innerHTML =  '<div class="margintop10">'+'<span style="color:white; padding-right: 10px">'+prefixe + jours + ' ' + mot_jour +'</span><span class="noirblanc">' + heures + '</span>:<span class="noirblanc">' + minutes + '</span>:<span class="noirblanc">' + secondes + '</span></div>';
                                }
                                else {
                                    compte_a_rebours_<?php echo $glisieres->bonplan_id; ?>.innerHTML = 'Expiré !';
                                    //alert(date_actuelle+ " "+date_evenement);
                                }

                                var actualisation = setTimeout("compte_a_rebours_<?php echo $glisieres->bonplan_id; ?>();", 1000);
                            }
                        </script>
                        <div style="font-size:30px" class="col-sm-12 " id="compte_a_rebours_<?php echo $glisieres->bonplan_id; ?>"></div>
                        </div>
                        <script type="text/javascript">
                            compte_a_rebours_<?php echo $glisieres->bonplan_id; ?>();
                        </script>
                    <?php } ?>
            </div>
        </div>
    </div>

    <?php if (isset($glisieres->bonplan_content) AND $glisieres->bonplan_content != null) { ?>
        <div class="col-sm-12 ml-5">
            <p><?php echo $glisieres->bonplan_content ?></p>
        </div>
    <?php } ?>

    <?php if (isset($glisieres->bonplan_content) AND $glisieres->bonplan_content != '' AND $glisieres->bonplan_content != null) { ?>



        <?php if (isset($glisieres->is_activ_btn_bp) AND $glisieres->is_activ_btn_bp == '1' AND $glisieres->is_activ_btn_bp != null) {
            if ($glisieres->bp_existed_link == 'ag') {
                $linkq2 = base_url() . $oInfoCommercant->nom_url . '/agenda';
            } elseif ($glisieres->bp_existed_link == 'art') {
                $linkq2 = base_url() . $oInfoCommercant->nom_url . '/article';
            } elseif ($glisieres->bp_existed_link == 'p1') {
                $linkq2 = base_url() . $oInfoCommercant->nom_url . '/infos';
            } elseif ($glisieres->bp_existed_link == 'p2') {
                $linkq2 = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
            } elseif ($glisieres->bp_existed_link == 'bp') {
                $linkq2 = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
            } elseif ($glisieres->bp_existed_link == 'fd') {
                $linkq2 = $link_fidelity;
            } elseif ($glisieres->bp_existed_link == 'bt') {
                $linkq2 = base_url() . $oInfoCommercant->nom_url . '/annonces';
            } elseif (isset($glisieres->bp_custom_link) AND $glisieres->bp_custom_link != '' AND $glisieres->bp_custom_link != null) {
                $linkq2 = 'https://' . $glisieres->bp_custom_link;
            }

            ?>
            <div class="pt-3 pb-3" style="width: 100%">
                <?php if (isset($glisieres->bloc1_islightboxbp1) AND $glisieres->bloc1_islightboxbp1=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkq2 ?? '' ?>"  class="fancybox.iframe"><div class="bouton2"><?php echo $glisieres->btn_bp_content ?></div></a>
                <?php }else{  ?>
                    <a class="lienBouton2" href="<?php echo ($linkq2) ?? '' ?>">
                        <div class="bouton2"><?php echo $glisieres->btn_bp_content ?></div>
                    </a>
                <?php } ?>
            </div>
        <?php } ?>
    <?php } ?>
</div>