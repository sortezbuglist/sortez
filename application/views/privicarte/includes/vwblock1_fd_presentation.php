<div class="row" style="width: 100%">
    <div style="width: 100%" class="ml-4 mb-4">
        <div class="col-sm-12 p-4 ml-4" style="background-image: url('<?php echo base_url('assets/img/fidelity_new.png') ?>');background-repeat: no-repeat;width: 100%;height: 250px;background-size: 100% 100%;position: relative;">
            <div class="d-flex" style="width: 50%;padding-left: 135px;align-items: center;color: white;position: absolute;
  top: 50%; /* poussé de la moitié de hauteur du référent */
  transform: translateY(-50%);">
                <?php if (isset($glisieres->fidelity_id) AND $glisieres->fidelity_id > 0) {
                    $thisz = get_instance();
                    $thisz->load->model('mdlfidelity');
                    $fidelity_contenue = $thisz->mdlfidelity->get_annonce_by_id($glisieres->fidelity_id, $oInfoCommercant->IdCommercant);
                } ?>
                <?php if (isset($fidelity_contenue) AND $fidelity_contenue != null) { ?>
                    <p><?php echo $fidelity_contenue->description; ?></p>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="col-sm-12 text-center text-justify pl-0 pr-0">

        <?php if (isset($glisieres->fidelity_content) AND $glisieres->fidelity_content != '' AND $glisieres->fidelity_content != null) { ?>
            <div class="ml-4" style="">
                <div class="pl-4 pr-4"><?php echo htmlspecialchars_decode($glisieres->fidelity_content) ?></div>
            </div>
        <?php } ?>

            <?php if (isset($glisieres->is_activ_btn_fd) AND $glisieres->is_activ_btn_fd == '1' AND $glisieres->is_activ_btn_fd != null) {
                if ($glisieres->FD_existed_link == 'ag') {
                    $linkq = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($glisieres->FD_existed_link == 'art') {
                    $linkq = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($glisieres->FD_existed_link == 'p1') {
                    $linkq = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($glisieres->FD_existed_link == 'p2') {
                    $linkq = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($glisieres->FD_existed_link == 'bp') {
                    $linkq = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($glisieres->FD_existed_link == 'fd') {
                    $linkq = $link_fidelity;
                } elseif ($glisieres->FD_existed_link == 'bt') {
                    $linkq = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif (isset($glisieres->fd_custom_link) AND $glisieres->fd_custom_link != '' AND $glisieres->fd_custom_link != null) {
                    $linkq = 'https://' . $glisieres->fd_custom_link;
                }
                ?>

                <?php if (isset($glisieres->bloc1_islightboxfd1) AND $glisieres->bloc1_islightboxfd1=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkq ?? '' ?>"  class="fancybox.iframe p-4"><div class="bouton2"><?php echo $glisieres->btn_fd_content ?></div></a>
                <?php }else{  ?>
                    <a class="lienBouton2 p-3" href="<?php echo ($linkq) ?? '' ?>">
                        <div class="bouton2 "><?php echo $glisieres->btn_fd_content ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
    </div>
</div>