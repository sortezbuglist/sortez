<div class="row ml-4 pl-3 pt-4 pb-4 pr-4" style="">
    <?php     $path_img_gallery_gl = 'application/resources/front/photoCommercant/imagesbank/'.$oInfoCommercant->user_ionauth_id;
    ?>
    <div class="col-sm-4 text-center pl-0 text-justify">
        <div class="p-4 sautligne allign_button" style=";height:auto;padding-right:0px!important;">
            <?php if (isset($objbloc_info->bloc_1_4_image1_content_page2) AND $objbloc_info->bloc_1_4_image1_content_page2 != '' AND $objbloc_info->bloc_1_4_image1_content_page2 != null) { ?>
                <div style="width: 100%;height: auto">
                    <img style="max-width: 100%;height: auto;" class="img-fluid"
                         src="<?php echo base_url($path_img_gallery_gl . '/' . $objbloc_info->bloc_1_4_image1_content_page2) ?>">
                </div>
            <?php } ?>
            <?php if (isset($objbloc_info->bloc_1_4_content_page2) AND $objbloc_info->bloc_1_4_content_page2 != '' AND $objbloc_info->bloc_1_4_content_page2 != null) { ?>
                <div class="pb-0 pt-2 content"><?php echo htmlspecialchars_decode($objbloc_info->bloc_1_4_content_page2) ?></div>
            <?php } ?>
            <?php if (isset($objbloc_info->is_activ_btn_bloc1_4_page2) AND $objbloc_info->is_activ_btn_bloc1_4_page2 == '1' AND $objbloc_info->is_activ_btn_bloc1_4_page2 != null) {
                if ($objbloc_info->bloc1_4_existed_link1_page2 == 'ag') {
                    $linkj = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($objbloc_info->bloc1_4_existed_link1_page2 == 'art') {
                    $linkj = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($objbloc_info->bloc1_4_existed_link1_page2 == 'p1') {
                    $linkj = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($objbloc_info->bloc1_4_existed_link1_page2 == 'p2') {
                    $linkj = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($objbloc_info->bloc1_4_existed_link1_page2 == 'bp') {
                    $linkj = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($objbloc_info->bloc1_4_existed_link1_page2 == 'fd') {
                    $linkj = $link_fidelity;
                } elseif ($objbloc_info->bloc1_4_existed_link1_page2 == 'bt') {
                    $linkj = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif (isset($objbloc_info->bloc1_4_custom_link1_page2) AND $objbloc_info->bloc1_4_custom_link1_page2 != '' AND $objbloc_info->bloc1_4_custom_link1_page2 != null) {
                    $linkj = 'https://' . $objbloc_info->bloc1_4_custom_link1_page2;
                }

                ?>
                <?php if (isset($objbloc_info->bloc4_islightbox_page2) AND $objbloc_info->bloc4_islightbox_page2=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkj ?? '' ?>"  class="fancybox.iframe"><div class="bouton1"><?php echo $objbloc_info->btn_bloc1_4_content_page2 ?></div></a>
                <?php }else{  ?>
                    <a class="pb-5 pt-5 lienbouton" href="<?php echo ($linkj) ?? '' ?>">
                        <div class="bouton1"><?php echo $objbloc_info->btn_bloc1_4_content_page2 ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <div class="col-sm-4 text-center pl-0 text-justify">
        <div class="p-4 sautligne allign_button" style=";height:auto;padding-right:0px!important;">
            <?php if (isset($objbloc_info->bloc_1_4_image2_content_page2) AND $objbloc_info->bloc_1_4_image2_content_page2 != '' AND $objbloc_info->bloc_1_4_image2_content_page2 != null) { ?>
                <div class="image">
                    <img style="max-width: 100%;height: auto;"
                         src="<?php echo base_url($path_img_gallery_gl . '/' . $objbloc_info->bloc_1_4_image2_content_page2) ?>">
                </div>
            <?php } ?>
            <?php if (isset($objbloc_info->bloc1_4_content2_page2) AND $objbloc_info->bloc1_4_content2_page2 != '' AND $objbloc_info->bloc1_4_content2_page2 != null) { ?>
                <div class="pb-0 pt-2 content"><?php echo htmlspecialchars_decode($objbloc_info->bloc1_4_content2_page2) ?></div>
            <?php } ?>
            <?php if (isset($objbloc_info->is_activ_btn_bloc2_4_page2) AND $objbloc_info->is_activ_btn_bloc2_4_page2 == '1' AND $objbloc_info->is_activ_btn_bloc2_4_page2 != null) {
                if ($objbloc_info->bloc1_4_existed_link2_page2 == 'ag') {
                    $linkl = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($objbloc_info->bloc1_4_existed_link2_page2 == 'art') {
                    $linkl = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($objbloc_info->bloc1_4_existed_link2_page2 == 'p1') {
                    $linkl = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($objbloc_info->bloc1_4_existed_link2_page2 == 'p2') {
                    $linkl = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($objbloc_info->bloc1_4_existed_link2_page2 == 'bp') {
                    $linkl = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($objbloc_info->bloc1_4_existed_link2_page2 == 'fd') {
                    $linkl = $link_fidelity;
                } elseif ($objbloc_info->bloc1_4_existed_link2_page2 == 'bt') {
                    $linkl = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif (isset($objbloc_info->bloc1_4_custom_link2_page2) AND $objbloc_info->bloc1_4_custom_link2_page2 != '' AND $objbloc_info->bloc1_4_custom_link2_page2 != null) {
                    $linkl = 'https://' . $objbloc_info->bloc1_4_custom_link2_page2;
                }

                ?>
                <?php if (isset($objbloc_info->bloc4_islightbox2_page2) AND $objbloc_info->bloc4_islightbox2_page2=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkl ?? '' ?>"  class="fancybox.iframe"><div class="bouton1"><?php echo $objbloc_info->btn_bloc1_4_content2_page2 ?></div></a>
                <?php }else{  ?>
                    <a class="pb-5 pt-5 lienbouton" href="<?php echo ($linkl) ?? '' ?>">
                        <div class="bouton1"><?php echo $objbloc_info->btn_bloc1_4_content2_page2 ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <!-- bloc3 debut --->
    <div class="col-sm-4 text-center pl-0 text-justify">
        <div class="p-4 sautligne allign_button" style=";height:auto;padding-right:0px!important;">
            <?php if (isset($objbloc_info->bloc_1_4_image3_content_page2) AND $objbloc_info->bloc_1_4_image3_content_page2 != '' AND $objbloc_info->bloc_1_4_image3_content_page2 != null) { ?>
                <div class="image">
                    <img style="max-width: 100%;height: auto;"
                         src="<?php echo base_url($path_img_gallery_gl . '/' . $objbloc_info->bloc_1_4_image3_content_page2) ?>">
                </div>
            <?php } ?>
            <?php if (isset($objbloc_info->bloc1_4_content3_page2) AND $objbloc_info->bloc1_4_content3_page2 != '' AND $objbloc_info->bloc1_4_content3_page2 != null) { ?>
                <div class="pb-0 pt-2 content"><?php echo htmlspecialchars_decode($objbloc_info->bloc1_4_content2_page2) ?></div>
            <?php } ?>
            <?php if (isset($objbloc_info->is_activ_btn_bloc3_4_page2) AND $objbloc_info->is_activ_btn_bloc3_4_page2 == '1' AND $objbloc_info->is_activ_btn_bloc3_4_page2 != null) {
                if ($objbloc_info->bloc1_4_existed_link3_page2 == 'ag') {
                    $linkm = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($objbloc_info->bloc1_4_existed_link3_page2 == 'art') {
                    $linkm = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($objbloc_info->bloc1_4_existed_link3_page2 == 'p1') {
                    $linkm = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($objbloc_info->bloc1_4_existed_link3_page2 == 'p2') {
                    $linkm = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($objbloc_info->bloc1_4_existed_link3_page2 == 'bp') {
                    $linkm = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($objbloc_info->bloc1_4_existed_link3_page2 == 'fd') {
                    $linkm =$link_fidelity;
                } elseif ($objbloc_info->bloc1_4_existed_link3_page2 == 'bt') {
                    $linkm = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif (isset($objbloc_info->bloc1_4_custom_link3_page2) AND $objbloc_info->bloc1_4_custom_link3_page2 != '' AND $objbloc_info->bloc1_4_custom_link3_page2 != null) {
                    $linkm = 'https://' . $objbloc_info->bloc1_4_custom_link3_page2;
                }

                ?>
                <?php if (isset($objbloc_info->bloc4_islightbox3_page2) AND $objbloc_info->bloc4_islightbox3_page2=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkm ?? '' ?>"  class="fancybox.iframe"><div class="bouton1"><?php echo $objbloc_info->btn_bloc1_4_content3_page2 ?></div></a>
                <?php }else{  ?>
                    <a class="pb-5 pt-5 lienbouton" href="<?php echo ($linkm) ?? '' ?>">
                        <div class="bouton1"><?php echo $objbloc_info->btn_bloc1_4_content3_page2 ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <!-- bloc3 fin --->
</div>