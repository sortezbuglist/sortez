<?php $data["empty"] = null; ?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php if (isset($zTitle)) echo $zTitle; ?> | Sortez</title>
    <style type="text/css">
        body {
            margin: 0px;
            padding: 0px;
            font-family: arial !important;
            font-size: 12px;
            line-height: 1.25em;
            margin: 0px;
            padding: 0px;
            /*background-image: url(https://www.sortez.org/wpimages/wpe3440087_06.jpg) !important;*/
            background-color: #ffffff;
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-position: center top;
            background-size: 100% 100%;
        }

        img {
            border: none;
        }

        .contener2013 {
            height: auto;
            width: 940px;
            margin-right: auto;
            margin-left: auto;
            position: relative;
            margin-top: 0px;
        }

        .maincontener2013 {
            background-color: #FFFFFF;
            float: left;
            height: auto;
            position: relative;
            width: 940px;
        }

        #table_form_inscriptionpartculier .td_part_form {
            text-align: left;
            font-weight: bold;
        }

        #table_form_inscriptionpartculier {
            text-align: center;
        }

        label {
            color: #000000 !important;
            white-space: normal !important;
        }


    </style>

    <?php if (isset($current_page) && $current_page == "subscription_pro") {
    } else { ?>

        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jspngfix.js"></script>
        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/proximite.js"></script>
        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jquery.ui.datepicker.js"></script>
        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jquery.tablesorter.js"></script>
        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jquery.tablesorter.pager.js"></script>
        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/ajaxfileupload.js"></script>
        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jquery.ui.core.js"></script>

        <link rel="stylesheet" media="screen" type="text/css"
              href="<?php echo GetCssPath("front/"); ?>/blue/style.css"/>

        <link rel="stylesheet" media="screen" type="text/css"
              href="<?php echo GetCssPath("front/"); ?>/jquery-ui-1.8.4.custom.css"/>
        <link rel="stylesheet" media="screen" type="text/css"
              href="<?php echo GetCssPath("front/"); ?>/demo_table_jui.css"/>

        <link href="<?php echo GetCssPath("bootstrap/"); ?>/bootstrap.css" rel="stylesheet" type="text/css">

        <link href="<?php echo GetCssPath("bootstrap/"); ?>/bootstrap-theme.css" rel="stylesheet" type="text/css">

        <link href="<?php echo GetCssPath("privicarte/"); ?>/global.css" rel="stylesheet" type="text/css">

    <?php } ?>

    <script type="text/javascript">
        //variables globales
        gCONFIG = new Array();
        gCONFIG["BASE_URL"] = "<?php echo base_url(); ?>";
        gCONFIG["SITE_URL"] = "<?php echo site_url(); ?>";
    </script>
</head>
<?php $data["zTitle"] = 'Accueil' ?>

<body style="margin-top:0px;">
<div class="contener2013">
    <div class="maincontener2013" style="padding-top: 15px;">
        <div class="container-fluid" style=" background-color:#ffffff; ">
            <!--<div class="col-lg-12 padding0" style="text-align: center;"><img src="<?php //echo base_url(); ?>/application/resources/sortez/images/logo.png" style="width: 450px;" alt="logo"></div>-->
            <div class="col-lg-12 padding0" style="text-align: center;">
                <a href="<?php echo site_url(); ?>" class="btn btn-primary">Retour site</a>
            </div>
        </div>
    