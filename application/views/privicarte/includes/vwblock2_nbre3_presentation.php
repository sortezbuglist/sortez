<div class="row" style="">
    <div class="col-sm-4 text-center text-justify pr-0">
        <div class="ml-4 pb-4 pt-4 sautligne allign_button content_sm_block" style="height:auto;padding-right:0px;padding-left:0px;">
            <?php if (isset($glisieres->bloc_1_2_image1_content) AND $glisieres->bloc_1_2_image1_content != '' AND $glisieres->bloc_1_2_image1_content != null) { ?>
                <div class="image content_right_padd">
                    <img style="text-align: center!important;max-width: 100%;height: auto;"
                         src="<?php echo base_url($path_img_gallery_gl . '/' . $glisieres->bloc_1_2_image1_content) ?>">
                </div>
            <?php } ?>
            <?php if (isset($glisieres->bloc1_2_content) AND $glisieres->bloc1_2_content != '' AND $glisieres->bloc1_2_content != null) { ?>
                <div class="content pt-4 pb-0 content_caract"><?php echo htmlspecialchars_decode($glisieres->bloc1_2_content) ?></div>
            <?php } ?>
            <?php if (isset($glisieres->is_activ_btn_bloc1_2) AND $glisieres->is_activ_btn_bloc1_2 == '1' AND $glisieres->is_activ_btn_bloc1_2 != null) {
                if ($glisieres->bloc1_2_existed_link1 == 'ag') {
                    $linke = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($glisieres->bloc1_2_existed_link1 == 'art') {
                    $linke = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($glisieres->bloc1_2_existed_link1 == 'p1') {
                    $linke = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($glisieres->bloc1_2_existed_link1 == 'p2') {
                    $linke = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($glisieres->bloc1_2_existed_link1 == 'bp') {
                    $linke = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($glisieres->bloc1_2_existed_link1 == 'fd') {
                    $linke =$link_fidelity;
                } elseif ($glisieres->bloc1_2_existed_link1 == 'bt') {
                    $linke = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif ($glisieres->bloc1_2_existed_link1 == 'cmd') {
                    $linke = base_url() . $oInfoCommercant->nom_url . '/commandes_commercants';
                } elseif (isset($glisieres->bloc1_2_custom_link1) AND $glisieres->bloc1_2_custom_link1 != '' AND $glisieres->bloc1_2_custom_link1 != null) {
                    $linke = 'https://' . $glisieres->bloc1_2_custom_link1;
                }
                ?>
                <?php if (isset($glisieres->bloc1_islightbox) AND $glisieres->bloc2_islightbox=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linke ?? '' ?>"  class="fancybox.iframe lienbtn3"><div class="bouton1"><?php echo $glisieres->btn_bloc1_2_content1 ?></div></a>
                <?php }else{  ?>
                    <a class="pb-5 pt-5 lienBouton " href="<?php echo ($linke) ?? '' ?>">
                        <div class="bouton1"><?php echo $glisieres->btn_bloc1_2_content1 ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <div class="col-sm-4 text-center pl-0 text-justify sautligne content_sm_nb2">
        <div class="p-4 allign_button content_sm_nb2ct" style=";height:auto;padding-right:0px!important">
            <?php if (isset($glisieres->bloc_1_2_image2_content) AND $glisieres->bloc_1_2_image2_content != '' AND $glisieres->bloc_1_2_image2_content != null) { ?>
                <div class="image content_right_padd">
                    <img style="text-align: center!important;max-width: 100%;height: auto;"
                         src="<?php echo base_url($path_img_gallery_gl . '/' . $glisieres->bloc_1_2_image2_content) ?>">
                </div>
            <?php } ?>
            <?php if (isset($glisieres->bloc1_2_content2) AND $glisieres->bloc1_2_content2 != '' AND $glisieres->bloc1_2_content2 != null) { ?>
                <div class="content pt-4 pb-0 content_caract"><?php echo htmlspecialchars_decode($glisieres->bloc1_2_content2) ?></div>
            <?php } ?>
            <?php if (isset($glisieres->is_activ_btn_bloc2_2) AND $glisieres->is_activ_btn_bloc2_2 == '1' AND $glisieres->is_activ_btn_bloc2_2 != null) {
                if ($glisieres->bloc1_2_existed_link2 == 'ag') {
                    $linkf = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($glisieres->bloc1_2_existed_link2 == 'art') {
                    $linkf = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($glisieres->bloc1_2_existed_link2 == 'p1') {
                    $linkf = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($glisieres->bloc1_2_existed_link2 == 'p2') {
                    $linkf = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($glisieres->bloc1_2_existed_link2 == 'bp') {
                    $linkf = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($glisieres->bloc1_2_existed_link2 == 'fd') {
                    $linkf = $link_fidelity;
                } elseif ($glisieres->bloc1_2_existed_link2 == 'bt') {
                    $linkf = base_url() . $oInfoCommercant->nom_url . '/annonces';
                 } elseif ($glisieres->bloc1_2_existed_link2 == 'cmd') {
                    $linkf = base_url() . $oInfoCommercant->nom_url . '/commandes_commercants';
                } elseif (isset($glisieres->bloc1_2_custom_link2) AND $glisieres->bloc1_2_custom_link2 != '' AND $glisieres->bloc1_2_custom_link2 != null) {
                    $linkf = 'https://' . $glisieres->bloc1_2_custom_link2;
                }

                ?>
                <?php if (isset($glisieres->bloc2_islightbox2) AND $glisieres->bloc2_islightbox2=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkf ?? '' ?>"  class="fancybox.iframe lienbtn3"><div class="bouton1"><?php echo $glisieres->btn_bloc1_2_content2 ?></div></a>
                <?php }else{  ?>
                    <a class="pb-5 pt-5 lienBouton " href="<?php echo ($linkf) ?? '' ?>">
                        <div class="bouton1"><?php echo $glisieres->btn_bloc1_2_content2 ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <!-- bloc home debut -->
    <div class="col-sm-4 text-center pl-0 text-justify sautligne content_sm_nb2">
        <div class="p-4 allign_button content_sm_nb2ct" style=";height:auto;padding-right:0px!important">
            <?php if (isset($glisieres->bloc_1_2_image3_content) AND $glisieres->bloc_1_2_image3_content != '' AND $glisieres->bloc_1_2_image3_content != null) { ?>
                <div class="image content_right_padd">
                    <img style="text-align: center!important;max-width: 100%;height: auto;"
                         src="<?php echo base_url($path_img_gallery_gl . '/' . $glisieres->bloc_1_2_image3_content) ?>">
                </div>
            <?php } ?>
            <?php if (isset($glisieres->bloc1_2_content3) AND $glisieres->bloc1_2_content3 != '' AND $glisieres->bloc1_2_content3 != null) { ?>
                <div class="content pt-4 pb-0 content_caract"><?php echo htmlspecialchars_decode($glisieres->bloc1_2_content3) ?></div>
            <?php } ?>
            <?php if (isset($glisieres->is_activ_btn_bloc_2_3) AND $glisieres->is_activ_btn_bloc_2_3 == '1' AND $glisieres->is_activ_btn_bloc_2_3 != null) {
                if ($glisieres->bloc1_2_existed_link3 == 'ag') {
                    $linkf3 = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($glisieres->bloc1_2_existed_link3 == 'art') {
                    $linkf3 = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($glisieres->bloc1_2_existed_link3 == 'p1') {
                    $linkf3 = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($glisieres->bloc1_2_existed_link3 == 'p2') {
                    $linkf3 = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($glisieres->bloc1_2_existed_link3 == 'bp') {
                    $linkf3 = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($glisieres->bloc1_2_existed_link3 == 'fd') {
                    $linkf3 =$link_fidelity;
                } elseif ($glisieres->bloc1_2_existed_link3 == 'bt') {
                    $linkf3 = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif ($glisieres->bloc1_2_existed_link3 == 'cmd') {
                    $linkf3 = base_url() . $oInfoCommercant->nom_url . '/commandes_commercants';
                } elseif (isset($glisieres->bloc1_2_custom_link3) AND $glisieres->bloc1_2_custom_link3 != '' AND $glisieres->bloc1_2_custom_link3 != null) {
                    $linkf3 = 'https://' . $glisieres->bloc1_2_custom_link3;
                }

                ?>
                <?php if (isset($glisieres->bloc2_islightbox3) AND $glisieres->bloc2_islightbox3=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkf3 ?? '' ?>"  class="fancybox.iframe lienbtn3"><div class="bouton1"><?php echo $glisieres->btn_bloc1_2_content3 ?></div></a>
                <?php }else{  ?>
                    <a class="pb-5 pt-5 lienBouton " href="<?php echo ($linkf3) ?? '' ?>">
                        <div class="bouton1"><?php echo $glisieres->btn_bloc1_2_content3 ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <!-- bloc home fin -->
</div>