<div class="row pl-3 pt-4 pb-4 pr-2" style=";width: 100%">

    <div style="width: 100%" class="ml-4 mb-4">

        <div class="col-sm-12 p-4" style="background-image: url('<?php echo base_url('assets/img/fidelity_new.png') ?>');background-repeat: no-repeat;width: 100%;height: 250px;background-size: 100% 100%;position: relative;">

            <div class="d-flex" style="width: 50%;padding-left: 135px;;color: white;position: absolute;

  top: 50%; /* poussé de la moitié de hauteur du référent */

  transform: translateY(-50%); /* tiré de la moitié de sa propre hauteur */">

                <?php if (isset($objbloc_info->fidelity_2_id_page2) AND $objbloc_info->fidelity_2_id_page2 > 0) {
                    $thisz = get_instance();
                    $thisz->load->model('mdlfidelity');
                    $fidelity_contenue = $thisz->mdlfidelity->get_annonce_by_id($objbloc_info->fidelity_2_id_page2, $oInfoCommercant->IdCommercant);
                } ?>
                <?php if (isset($fidelity_contenue) AND $fidelity_contenue != null) { ?>
                    <p><?php echo $fidelity_contenue->description; ?></p>
                <?php } ?>

            </div>

        </div>

    </div>

        <div class="col-sm-12 text-center text-justify pl-0 pr-0 pb-3">

            <?php if (isset($objbloc_info->fidelity_2_content_page2) AND $objbloc_info->fidelity_2_content_page2 != '' AND $objbloc_info->fidelity_2_content_page2 != null) { ?>
                <div class="" style="">
                    <div class="pl-4 pr-4 text-justify"><?php echo htmlspecialchars_decode($objbloc_info->fidelity_2_content_page2) ?></div>
                </div>
            <?php } ?>
            <?php if (isset($objbloc_info->is_activ_btn_fd_2_page2) AND $objbloc_info->is_activ_btn_fd_2_page2 == '1' AND $objbloc_info->is_activ_btn_fd_2_page2 != null) {
                if ($objbloc_info->FD_2_existed_link_page2 == 'ag') {
                    $linkq3 = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($objbloc_info->FD_2_existed_link_page2 == 'art') {
                    $linkq3 = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($objbloc_info->FD_2_existed_link_page2 == 'p1') {
                    $linkq3 = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($objbloc_info->FD_2_existed_link_page2 == 'p2') {
                    $linkq3 = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($objbloc_info->FD_2_existed_link_page2 == 'bp') {
                    $linkq3 = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($objbloc_info->FD_2_existed_link_page2 == 'fd') {
                    $linkq3 = $link_fidelity;
                } elseif ($objbloc_info->FD_2_existed_link_page2 == 'bt') {
                    $linkq3 = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif (isset($objbloc_info->fd_custom_2_link_page2) AND $objbloc_info->fd_custom_2_link_page2 != '' AND $objbloc_info->fd_custom_2_link_page2 != null) {
                    $linkq3 = 'https://' . $objbloc_info->fd_custom_2_link_page2;
                }

                ?>
                <?php if (isset($objbloc_info->bloc2_islightbox_page2fd_page2) AND $objbloc_info->bloc2_islightbox_page2fd_page2=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkq3 ?? '' ?>"  class="fancybox.iframe"><div class="bouton2"><?php echo $objbloc_info->btn_fd_2_content_page2 ?></div></a>
                <?php }else{  ?>
                    <a class="lienBouton2" href="<?php echo ($linkq3) ?? '' ?>">
                        <div class="bouton2"><?php echo $objbloc_info->btn_fd_2_content_page2 ?></div>
                    </a>


                <?php } ?>
            <?php } ?>
        </div>
</div>