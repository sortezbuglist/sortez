<?php
$this->load->library('ion_auth');
if(isset($ion_auth->user_ionauth_id)){
    $user_ionauth_id=$ion_auth->user_ionauth_id;
}

if(isset($user_ionauth_id)){
    $imagepath = 'application/resources/front/photoCommercant/imagesbank/' . $user_ionauth_id . '/';
    $imagepath2 = 'application/resources/front/photoCommercant/imagesbank/';
    $imagepath3 = 'application/resources/front/photoCommercant/images/';
    if (is_file(base_url().$imagepath.($bonplan->bonplan_photo ?? ''))){
        $image=base_url().$imagepath.($bonplan->bonplan_photo ?? '');
    }elseif (is_file(base_url().$imagepath2.($bonplan->bonplan_photo ?? ''))){
        $image=  base_url().$imagepath2.($bonplan->bonplan_photo ??  '');
    }elseif (is_file(base_url().$imagepath3.($bonplan->bonplan_photo ??''))){
        $image=base_url().$imagepath3.($bonplan->bonplan_photo  ?? '');
    }else{
        $image=0;
    }
}
if(isset($bonplan->bonplan_date_fin) AND isset($bonplan->bp_multiple_date_fin) AND isset($bonplan->bp_multiple_date_fin) AND isset($bonplan->date_fin)){
    $date_fin=$bonplan->bonplan_date_fin ?? $bonplan->bp_multiple_date_fin ?? $bonplan->bp_multiple_date_fin  ?? $bonplan->date_fin;
    $date_fin_exploded=explode('-',$date_fin);
    $annee=$date_fin_exploded[0];
    $mois=$date_fin_exploded[1];
    $jours=$date_fin_exploded[2];
//var_dump($date_fin_exploded);
    if(!isset($date_fin) || $date_fin == "") {
        $annee="0000";
        $mois="00";
        $jours="00";
    }
}

?>
<script type="text/javascript">
    function CompteARebours(){

        var s=parseInt(0);
        var min=parseInt(0);
        var h=parseInt(0);
        var j=parseInt(<?php echo $jours ?>);
        var m=parseInt(<?php echo $mois ?>);
        var annee=parseInt(<?php echo $annee ?>);

        var date_actuel = new Date();
        var date_fin= new Date(annee,m,j,h,min,s);

if (date_fin.getTime() > date_actuel.getTime()) {

    var tps_restant = date_fin.getTime() - date_actuel.getTime();
    var s_restantes = tps_restant / 1000; // On convertit en secondes
    var i_restantes = s_restantes / 60;
    var H_restantes = i_restantes / 60;
    var d_restants = H_restantes / 24;

    s_restantes = Math.floor(s_restantes % 60); // Secondes restantes
    i_restantes = Math.floor(i_restantes % 60); // Minutes restantes
    H_restantes = Math.floor(H_restantes % 24); // Heures restantes
    d_restants = Math.floor(d_restants); // Jours restants


    var texte = '<span style="font-size: 36px;">Cette offre expire dans:</span><br>' +
        "<strong>" +d_restants+ " J</strong>, <strong>" +H_restantes+ " H</strong>," +
        " <strong>" +i_restantes+ " min</strong> et <strong>" +s_restantes+ "s</strong>.";


    document.getElementById("affichage").innerHTML = texte;


}else if (date_fin.getTime() < date_actuel.getTime()) {
    var tps_restant = date_actuel.getTime() - date_fin.getTime();
    var s_restantes = tps_restant / 1000; // On convertit en secondes
    var i_restantes = s_restantes / 60;
    var H_restantes = i_restantes / 60;
    var d_restants = H_restantes / 24;

    s_restantes = Math.floor(s_restantes % 60); // Secondes restantes
    i_restantes = Math.floor(i_restantes % 60); // Minutes restantes
    H_restantes = Math.floor(H_restantes % 24); // Heures restantes
    d_restants = Math.floor(d_restants); // Jours restants


    var texte = '<span style="font-size: 36px;">Cette Offre est expiré, il y a:</span> <br>' +
        "<strong>" +d_restants+ " J</strong>, <strong>" +H_restantes+ " H</strong>," +
        " <strong>" +i_restantes+ " min</strong> et <strong>" +s_restantes+ "s</strong>.";



    document.getElementById("affichage").innerHTML = texte;

}

    }
    setInterval(CompteARebours, 1000); // Rappel de la fonction toutes les 1000 millisecondes (toutes les secondes quoi !).
</script>
<div >
    <?php if(isset($bonplan->bonplan_titre) AND isset($bonplan->description)){ ?>
    <h3 class="text-center"><?php echo $bonplan->bonplan_titre ?? $bonplan->description;?></h3>

    <?php } ?>
        <div onload="CompteARebours()" id="affichage" style="font-size: 36px;" class="text-center"></div>


</div>





