<style type="text/css">
<!--
.div_dep_com {
	float: right;
	padding-right: 20px;
	position: relative;
	padding-top: 42px;
	padding-bottom: 42px;
}
.div_logo {
	float: left;
	position: relative;
}
.main_list_select_pvc button
{
-moz-appearance: none !important;
border: medium none !important;
margin: 0 !important;
padding: 0 !important;
box-shadow: none !important;
text-transform:uppercase;
font-family:Arial, Helvetica, sans-serif;
font-weight:bold; 
color:#000000;
font-size:15px;
width:212px; 
height:43px;
}
#main_dep_select button {
background-image:url(<?php echo GetImagePath("privicarte/") ; ?>/test/btn_dep.png) !important; 
background-color:#000000 !important;
background-repeat:no-repeat !important;
padding-left:25px !important;
text-align:center;
}
#main_com_select button, #all_commune_select_fixe {
background-image:url(<?php echo GetImagePath("privicarte/") ; ?>/test/btn_com_round.png) !important; 
background-color:#ffffff !important;
background-repeat:no-repeat !important;
padding-left:17px !important;
background-size: 218px 100%;
}
#all_commune_select_fixe:focus, #all_commune_select_fixe:hover {
	border:none;
}
#main_dep_select button:hover, #main_dep_select button:focus, #main_com_select button:hover, #main_com_select button:focus  {
background-position:0px !important;
}
.dropdown.main_list_select_pvc {
    display: inline;
}
.main_list_select_pvc button .caret {
    display: none !important;
}
.main_list_select_pvc .dropdown-menu {
    margin-top: 15px;
}
#main_dep_select ul.dropdown-menu {
margin-left:45px;
}
#main_com_select ul.dropdown-menu {
margin-left:5px;
}
-->
</style>

<script type="text/javascript">
function pvc_select_departement(departement_id) {
 $("#inputStringDepartementHidden_bonplans").val(departement_id);
 $("#inputStringDepartementHidden_partenaires").val(departement_id);
 $("#inputStringDepartementHidden_annnonces").val(departement_id);
 $("#frmRechercheBonplan").submit();
 $("#frmRecherchePartenaire").submit();
 $("#frmRechercheAnnonce").submit();
}
function pvc_select_commune(id_commune){	
 $("#inputStringVilleHidden_bonplans").val(id_commune);
 $("#inputStringVilleHidden_partenaires").val(id_commune);
 $("#inputStringVilleHidden_annonces").val(id_commune);
 $("#frmRechercheBonplan").submit();
 $("#frmRecherchePartenaire").submit();
 $("#frmRechercheAnnonce").submit();
}

</script>

<div>
<button id="all_commune_select_fixe" onclick="javascript:pvc_select_commune(0);" style="width:220px; height:43px; margin-bottom:15px; border:none; font-weight:bold;">Toutes les communes</button>
</div>

<div class="dropdown main_list_select_pvc" id="main_com_select">
  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu_com" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width:220px; height:43px;">
  <?php 
    if(isset($iVilleId) && $iVilleId != "" && $iVilleId != NULL && $iVilleId != 0 && $iVilleId != "0") 
    {
        foreach($toVille as $oVille){
            if ($oVille->IdVille == $iVilleId) echo $oVille->Nom." (".$oVille->nbCommercant.")";
			//else echo "COMMUNE";
        }
    }
    else echo "COMMUNES";
    ?>
<span class="caret"></span>
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu_com">
    <li><a href="javascript:void(0);" onclick="javascript:pvc_select_commune(0);">Toutes les communes</a></li>
<?php 

if(isset($iVilleId) && $iVilleId != "" && $iVilleId != NULL && $iVilleId != 0 && $iVilleId != "0") 
    {} else {

	//LOCALDATA FILTRE
	$this_session_localdata =& get_instance();
	$this_session_localdata->load->library('session');
	$localdata_value = $this_session_localdata->session->userdata('localdata');
	if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
	} else {
	////$this->firephp->log($toVille, 'toVille');
		foreach($toVille as $oVille){ ?>
			<li><a href="javascript:void(0);" onclick="javascript:pvc_select_commune(<?php echo $oVille->IdVille ; ?>);"><?php echo $oVille->Nom." (".$oVille->nbCommercant.")" ; ?></a></li>
		<?php } ?>
	<?php } 

}?>    
    
  </ul>
</div>