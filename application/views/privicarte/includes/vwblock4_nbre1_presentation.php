<div class="row" style="">
    <div style="width: 100%;" class="pr-4 pb-0 pt-0  pl-4 mr-4 content_sm_block">
        <div class="col-sm-12 text-center text-justify pl-0 pr-0 ml-4 sautligne content_sm_block" style="">
            <?php if (isset($glisieres->bloc_1_4_image1_content) AND $glisieres->bloc_1_4_image1_content != '' AND $glisieres->bloc_1_4_image1_content != null) { ?>
                <div style="width: 100%;height: auto">
                    <img style="max-width: 100%;height: auto;" class="img-fluid"
                         src="<?php echo base_url($path_img_gallery_gl . '/' . $glisieres->bloc_1_4_image1_content) ?>">
                </div>
            <?php } ?>
            <?php if (isset($glisieres->bloc1_4_content) AND $glisieres->bloc1_4_content != '' AND $glisieres->bloc1_4_content != null) { ?>
                <p class="pt-4 content_caract"><?php echo htmlspecialchars_decode($glisieres->bloc1_4_content) ?></p>
            <?php } ?>
            <?php if (isset($glisieres->is_activ_btn_bloc1_4) AND $glisieres->is_activ_btn_bloc1_4 == '1' AND $glisieres->is_activ_btn_bloc1_4 != null) {
                if ($glisieres->bloc1_4_existed_link1 == 'ag') {
                    $linkj = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($glisieres->bloc1_4_existed_link1 == 'art') {
                    $linkj = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($glisieres->bloc1_4_existed_link1 == 'p1') {
                    $linkj = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($glisieres->bloc1_4_existed_link1 == 'p2') {
                    $linkj = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($glisieres->bloc1_4_existed_link1 == 'bp') {
                    $linkj = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($glisieres->bloc1_4_existed_link1 == 'fd') {
                    $linkj = $link_fidelity;
                } elseif ($glisieres->bloc1_4_existed_link1 == 'bt') {
                    $linkj = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif ($glisieres->bloc1_4_existed_link1 == 'cmd') {
                    $linkj = base_url() . $oInfoCommercant->nom_url . '/commandes_commercants';
                } elseif (isset($glisieres->bloc1_4_custom_link1) AND $glisieres->bloc1_4_custom_link1 != '' AND $glisieres->bloc1_4_custom_link1 != null) {
                    $linkj = 'https://' . $glisieres->bloc1_4_custom_link1;
                }

                ?>
                <?php if (isset($glisieres->bloc4_islightbox) AND $glisieres->bloc4_islightbox=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkj ?? '' ?>"  class="fancybox.iframe"><div class="bouton2"><?php echo $glisieres->btn_bloc1_4_content1 ?></div></a>
                <?php }else{  ?>
                    <a class="pb-5 pt-5 lienBouton2 " href="<?php echo ($linkj) ?? '' ?>">
                        <div class="bouton2"><?php echo $glisieres->btn_bloc1_4_content1 ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>