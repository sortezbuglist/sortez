<style type="text/css">
    @font-face {
        font-family: futura_medium;
        src: url(<?php echo base_url();?>/application/resources/sortez/fonts/futura_medium.ttf);
    }
    @font-face {
        font-family: futura_regular;
        src: url(<?php echo base_url();?>/application/resources/sortez/fonts/futura_regular.ttf);
    }
    @font-face {
        font-family: futura_bold;
        src: url(<?php echo base_url();?>/application/resources/sortez/fonts/futura_bold.ttf);
    }

    body {
        margin: 0px;
        padding: 0px;
        /*background-repeat: repeat;
        background-position:center top;*/
        /*background-image: url(
    <?php //echo GetImagePath("front/"); ?> /wpimages2013/wp30e2106b_06.png);*/
        /*background-image: url(
    <?php echo GetImagePath("front/"); ?> /wpimages2013/wp9e1de8e1_05_06.jpg);*/
        /*background-color:#003366;*/
        background-color: #f6f6f6;
        background-image: none;
        font-family: "Arial", sans-serif;
        font-size: 13px;


       /* background:#ffffff;
        margin-left:auto;
        margin-right:auto;
        position:relative;
        width:1250px;
        height:3000px;*/
    }

    img {
        border: none;
    }

    .contener2013 {
        height: auto;
        width: 1024px;
        margin-right: auto;
        margin-left: auto;
        position: relative;
        margin-top: 0px;
    }

    .header2013 {
        float: left;
        background-color: #FFF;
        height: 467px;
        width: 1024px;
        position: relative;
    }

    .space2013 {
        float: left;
        height: 15px;
        width: 1024px;
        position: relative;
    }

    .maincontener2013 {
        float: left;
        height: auto;
        width: 1024px;
        position: relative;
        background-color: #FFF;
    }

    .leftcontener2013 {
        float: left;
        width: 202px;
        position: relative;
        height: auto;
        /*
    <?php //if (isset($pagecategory) && $pagecategory == 'annonce') { ?>
	background-color: #003333;

    <?php //} else if (isset($pagecategory) && $pagecategory == 'bonplan') { ?>
	background-color: #A70000;

    <?php //} else { ?> */
        background-color: #3653A3;
    <?php //} ?> color: #FFFFFF;
        font-family: "Arial", sans-serif;
        font-size: 11px;
        font-weight: 700;
        line-height: 1.27em;
    }

    .rightcontener2013 {
        float: left;
        width: 198px;
        /*margin-left: 20px;*/
        margin-top: 10px;
        position: relative;
    }

    .rightcontener2013 a {
        color: #000000;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
        text-align: center;
        text-decoration: none;
        font-weight: bold;
    }

    .centercontener2013 {
        float: left;
        width: 624px;
        /*margin-left: 20px;*/
        margin-top: 10px;
        position: relative;
    }
    .container_backoffice_pro {
        width: 624px;
        position: relative;
        margin:0 auto;
    }
    .main_container_backoffice_pro {
        width: 100%;
        float: left;
        position: relative;
    }
    .container_inscription_pro {
        width: 624px;
        margin: 10px auto 768px;
        position: relative;
    }

    .pubcontener2013 {
        float: left;
        width: 620px;
        position: relative;
        margin-left: 0px;
    }

    .menucontener2013 {
        float: left;
        width: 620px;
        position: relative;
        margin-left: 10px;
    }

    .contentcontener2013 {
        float: left;
        width: 620px;
        position: relative;
        font-family: "Arial", sans-serif;
        font-size: 12px;
        line-height: 1.25em;
    }

    .footer2013 {
    <?php //if (isset($pagecategory) && $pagecategory == 'bonplan') { ?>
        /*background-color: #004D9C;*/
    <?php //} else { ?> background-color: #3653A2;
    <?php //} ?> float: left;
        height: 108px;
        width: 1024px;
        position: relative;
        color: #FFFFFF;
        font-family: "Arial", sans-serif;
        font-size: 13px;
        font-weight: 700;
        line-height: 1.23em;
        text-align: center;
        vertical-align: central;
        padding-top: 40px;
    }

    .leftcontener2013top {
        /*
    <?php //if (isset($pagecategory) && $pagecategory == 'annonce') { ?>
	background-image: url(
    <?php echo GetImagePath("front/"); ?> /wpimages2013/wpa145ae83_06.png);

    <?php //} else if (isset($pagecategory) && $pagecategory == 'bonplan') { ?>
	background-image: url(
    <?php echo GetImagePath("front/"); ?> /wpimages2013/wp07e4a24e_06.png);

    <?php //} else { ?> */
        background-image: url(<?php echo GetImagePath("front/"); ?>/wpimages2013/wp7d90bd82_06.png);
    <?php //} ?> background-repeat: no-repeat;
        float: left;
        height: 73px;
        width: 202px;
        position: relative;
    <?php //if (isset($pagecategory) && $pagecategory == 'annonce') { ?>
        /*color: #000000;

    <?php //} else if (isset($pagecategory) && $pagecategory == 'annonce') { ?>
	color: #FFFFFF;*/
    <?php //} else {?> color: #FFFFFF;
    <?php //} ?> font-family: "Arial", sans-serif;
        font-size: 16px;
        font-weight: 700;
        line-height: 1.25em;
        text-align: center;
        padding-top: 5px;
    }

    .leftcontener2013content {
        float: left;
        width: 100%;
        font-size: 12px;
        position: relative;
        /*padding-left: 15px;
        padding-right: 5px;*/
    }

    .leftcontener2013content_head {
        float: left;
        width: 187px;
        position: relative;
        padding-left: 10px;
        padding-right: 5px;
    }

    .leftcontener2013title {
        float: left;
        height: auto;
        width: 100%;
        position: relative;
        /*
    <?php //if (isset($pagecategory) && $pagecategory == 'annonce') { ?>
	color: #060501;

    <?php //} else { ?> */
        color: #000000;
    <?php //} ?> font-family: "Arial", sans-serif;
        font-size: 15px;
        font-weight: 700;
        line-height: 1.7em;
        text-align: left;
        /*
    <?php //if (isset($pagecategory) && $pagecategory == 'annonce') { ?>
	background-color: #FCBD18;

    <?php //} else if (isset($pagecategory) && $pagecategory == 'bonplan') { ?>
	background-color: #3653A3;

    <?php //} else { ?> */
        /*background-color: #FCC73C;*/
    <?php //} ?> padding-left: 7px;
        padding-right: 0px;
        padding-top: 0px;
        padding-bottom: 0px;
        cursor: pointer;
    }

    .menucontener2013partenaire {
        float: left;
        height: 65px;
        width: 267px;
        position: relative;
        margin-top: 10px;
        text-align: left;
    }

    .menucontener2013annonce {
        float: left;
        height: 65px;
        width: 267px;
        position: relative;
        margin-top: 10px;
        text-align: center;
    }

    .menucontener2013bonplan {
        float: left;
        height: 65px;
        width: 267px;
        position: relative;
        margin-top: 6px;
        text-align: right;
    }

    .menucontener2013partenaire_icon {
        float: left;
        width: 267px;
        position: relative;
        text-align: left;
    }

    .menucontener2013annonce_icon {
        float: left;
        width: 267px;
        position: relative;
        text-align: center;
    }

    .menucontener2013bonplan_icon {
        float: left;
        width: 267px;
        position: relative;
        text-align: right;
    }

    .infocontener2013 {
    <?php if (isset($pagecategory) && $pagecategory == 'annonce') { ?> background-color: #003333;
    <?php } else if (isset($pagecategory) && $pagecategory == 'bonplan') { ?> background-color: #3653A2;
    <?php } else { ?> background-color: #3653A2;
    <?php } ?> float: left;
        height: 20px;
        width: 802px;
        position: relative;
        color: #FFFFFF;
        font-family: "Arial", sans-serif;
        font-size: 13px;
        line-height: 1.23em;
        text-align: center;
        padding-top: 5px;
    }

    .mainiconhome {
        float: left;
        height: auto;
        width: 600px;
        position: relative;
        margin-left: 15px;

    }

    #leftcontener2013contentactualise {
        color: #FFFFFF;
        font-family: "Arial", sans-serif;
        font-size: 10px;
        font-weight: 700;
        line-height: 1.2em;
    }

    #id_annonce_minitext {
        height: 100px;
        padding-left: 27px;
        padding-top: 29px;
        width: 200px;
        background-image: url(<?php echo GetImagePath("front/"); ?>/wpcda8dfe6_06.png);
        background-repeat: no-repeat;
    }

    #table_form_inscriptionpartculier .td_part_form {
        text-align: left;
        font-weight: bold;
    }

    #table_form_inscriptionpartculier {
        text-align: center;
    }

</style>