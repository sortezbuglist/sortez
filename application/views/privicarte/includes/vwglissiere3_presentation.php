<script type="text/javascript">
    function glissiere3() {
        if (jQuery("#g3content")[0] && $('#g3content').hasClass("d-none")) {
            if (jQuery("#g3content")[0]) jQuery("#g3content").removeClass('d-none');
            if (jQuery("#g3content")[0]) jQuery("#g3content").addClass('d-flex');
            if (jQuery("#g3content2")[0]) jQuery("#g3content2").removeClass('d-none');
            if (jQuery("#g3content2")[0]) jQuery("#g3content2").addClass('d-flex');
            if (jQuery("#autoslide_off_3")[0]) jQuery("#autoslide_off_3").removeClass('d-none');
            if (jQuery("#autoslide_off_3")[0]) jQuery("#autoslide_off_3").addClass('d-flex');

            <?php if (isset($glisieres->nombre_blick_gli_presentation3) AND $glisieres->nombre_blick_gli_presentation3=='2'){ ?>

            if ($('#g3content').hasClass("padded") ==false){

                jQuery("#g3content").addClass('padded');
                jQuery("#g3content").css('height',jQuery("#autoslide_off_3").css('height','+=40'));
                jQuery("#g3content2").css('height',jQuery("#autoslide_off_3").css('height','+=40'));

                jQuery("#g3content").css('height',jQuery("#autoslide_off_3").css('height'));
                jQuery("#g3content2").css('height',jQuery("#autoslide_off_3").css('height'));

            }
            <?php } ?>
        } else if (jQuery("#g3content")[0] && $('#g3content').hasClass("d-flex")) {

            if (jQuery("#g3content")[0]) jQuery("#g3content").removeClass('d-flex');
            if (jQuery("#g3content")[0]) jQuery("#g3content").addClass('d-none');
            if (jQuery("#g3content2")[0]) jQuery("#g3content2").removeClass('d-flex');
            if (jQuery("#g3content2")[0]) jQuery("#g3content2").addClass('d-none');
            if (jQuery("#autoslide_off_3")[0]) jQuery("#autoslide_off_3").removeClass('d-flex');
            if (jQuery("#autoslide_off_3")[0]) jQuery("#autoslide_off_3").addClass('d-none');
        }
    }
</script>
<div class="row  <?php if ($group_id_commercant_user == 4) { ?>mt-5<?php } elseif ($group_id_commercant_user == 5) { ?>mt-4<?php } ?>" style="">
    <div class="pl-3 pr-3 pb-0" style="width: 100%">
        <div class="pl-4 pb-0 pt-0 pr-0">
            <div onclick="glissiere3()"
                 class="col-12 text-justify titre_glissiere1"><?php echo $glisieres->presentation_3_titre; ?>
                <img style="height: 28px;padding-top: 12px;" class="float-right img-fluid"
                     src="<?php echo base_url('assets/img/wpc59cfc3d_06.png') ?>"></div>
        </div>
    </div>
</div>
<div class="row " id="autoslide_off_3" style="display: none">
    <div id="g3content"
         class=" col-sm-<?php if (isset($glisieres->nombre_blick_gli_presentation3) AND $glisieres->nombre_blick_gli_presentation3 == '1') {
             echo 12;
         } elseif (isset($glisieres->nombre_blick_gli_presentation3) AND $glisieres->nombre_blick_gli_presentation3 == '2') {
             echo 6;
         } ?> text-justify pt-0 pr-3 d-none"
         style="<?php if ($glisieres->isActive_presentation_3 == '1' AND $glisieres->presentation_3_contenu1 != '') { ?>display: none;<?php } else { ?>display: none;<?php } ?>">
        <div class="p-0  text-justify text-center ml-4 sautligne " style="height: auto">
            <?php if (isset($glisieres->presentation_3_image_1) AND $glisieres->presentation_3_image_1 != '' AND $glisieres->presentation_3_image_1 != null) { ?>
                <div class="image">
                    <img style="max-width: 100%;height: auto;"
                         src="<?php echo base_url($path_img_gallery_gl . '/' . $glisieres->presentation_3_image_1) ?>">
                </div>
            <?php } ?>
            <p class="pt-4"><?php echo htmlspecialchars_decode($glisieres->presentation_3_contenu1 ?? ''); ?></p>
            <?php if ($glisieres->btn_gli3_content1 != '' AND $glisieres->is_activ_btn_glissiere3_champ1 == '1') { ?>
                <?php if (isset($glisieres->gli3_islightbox) AND $glisieres->gli3_islightbox=="1"){ ?>
                    <a data-fancybox data-type="iframe" data-src="<?php if ($glisieres->gl3_existed_link1 != '0' AND $glisieres->gl3_existed_link1 == 'p1') {
                        echo site_url($oInfoCommercant->nom_url . "/infos");
                    } elseif ($glisieres->gl3_existed_link1 != '0' AND $glisieres->gl3_existed_link1 == 'p2') {
                        echo site_url($oInfoCommercant->nom_url . "/autresinfos");
                    } elseif ($glisieres->gl3_existed_link1 != '0' AND $glisieres->gl3_existed_link1 == 'ag') {
                        echo site_url($oInfoCommercant->nom_url . "/agenda");
                    } elseif ($glisieres->gl3_existed_link1 != '0' AND $glisieres->gl3_existed_link1 == 'art') {
                        echo site_url($oInfoCommercant->nom_url . "/article");
                    } elseif ($glisieres->gl3_existed_link1 != '0' AND $glisieres->gl3_existed_link1 == 'bp') {
                        echo site_url($oInfoCommercant->nom_url . "/notre_bonplan");
                    }elseif ($glisieres->gl3_existed_link1 != '0' AND $glisieres->gl3_existed_link1 == 'fd') {
                        echo $link_fidelity;
                    }  elseif ($glisieres->gl3_custom_link1 != '') {
                        echo $glisieres->gl3_custom_link1;
                    } ?>" class="lienBouton">
                        <div style="margin-top: 20px;margin-bottom: 20px"
                             class="bouton"><?php echo $glisieres->btn_gli3_content1; ?></div>
                    </a>
                <?php }else{ ?>
                    <a href="<?php if ($glisieres->gl3_existed_link1 != '0' AND $glisieres->gl3_existed_link1 == 'p1') {
                        echo site_url($oInfoCommercant->nom_url . "/infos");
                    } elseif ($glisieres->gl3_existed_link1 != '0' AND $glisieres->gl3_existed_link1 == 'p2') {
                        echo site_url($oInfoCommercant->nom_url . "/autresinfos");
                    } elseif ($glisieres->gl3_existed_link1 != '0' AND $glisieres->gl3_existed_link1 == 'ag') {
                        echo site_url($oInfoCommercant->nom_url . "/agenda");
                    } elseif ($glisieres->gl3_existed_link1 != '0' AND $glisieres->gl3_existed_link1 == 'art') {
                        echo site_url($oInfoCommercant->nom_url . "/article");
                    } elseif ($glisieres->gl3_existed_link1 != '0' AND $glisieres->gl3_existed_link1 == 'bp') {
                        echo site_url($oInfoCommercant->nom_url . "/notre_bonplan");
                    }elseif ($glisieres->gl3_existed_link1 != '0' AND $glisieres->gl3_existed_link1 == 'fd') {
                        echo $link_fidelity;
                    }  elseif ($glisieres->gl3_custom_link1 != '') {
                        echo $glisieres->gl3_custom_link1;
                    } ?>" class="lienBouton">
                        <div style="margin-top: 20px;margin-bottom: 20px"
                             class="bouton"><?php echo $glisieres->btn_gli3_content1; ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <?php if (isset($glisieres->presentation_3_contenu2) AND $glisieres->presentation_3_contenu2 != '' AND $glisieres->nombre_blick_gli_presentation3 == '2') { ?>
        <div id="g3content2" class="col-sm-6 text-justify pt-0 pl-0 "
             style="<?php if ($glisieres->isActive_presentation_3 == '1' AND $glisieres->presentation_3_contenu2 != '') { ?>display: none;<?php } else { ?>display: none;<?php } ?>">
            <div class="p-0  text-justify text-center sautligne " style="height: auto">
                <?php if (isset($glisieres->presentation_3_image_2) AND $glisieres->presentation_3_image_1 != '' AND $glisieres->presentation_3_image_1 != null) { ?>
                    <div class="image">
                        <img style="max-width: 100%;height: auto;"
                             src="<?php echo base_url($path_img_gallery_gl . '/' . $glisieres->presentation_3_image_2) ?>">
                    </div>
                <?php } ?>
                <p class="pt-4"><?php echo htmlspecialchars_decode($glisieres->presentation_3_contenu2 ?? ''); ?></p>

                <?php if ($glisieres->btn_gli3_content2 != '' AND $glisieres->is_activ_btn_glissiere3_champ2 == '1') { ?>
                    <?php if (isset($glisieres->gli3_islightbox2) AND $glisieres->gli3_islightbox2=="1"){ ?>

                        <a data-fancybox data-type="iframe" data-src="<?php if ($glisieres->gl3_existed_link2 != '0' AND $glisieres->gl3_existed_link2 == 'p1') {
                            echo site_url($oInfoCommercant->nom_url . "/infos");
                        } elseif ($glisieres->gl3_existed_link2 != '0' AND $glisieres->gl3_existed_link2 == 'p2') {
                            echo site_url($oInfoCommercant->nom_url . "/autresinfos");
                        } elseif ($glisieres->gl3_existed_link2 != '0' AND $glisieres->gl3_existed_link2 == 'ag') {
                            echo site_url($oInfoCommercant->nom_url . "/agenda");
                        } elseif ($glisieres->gl3_existed_link2 != '0' AND $glisieres->gl3_existed_link2 == 'art') {
                            echo site_url($oInfoCommercant->nom_url . "/article");
                        } elseif ($glisieres->gl3_existed_link2 != '0' AND $glisieres->gl3_existed_link2 == 'bp') {
                            echo site_url($oInfoCommercant->nom_url . "/notre_bonplan");
                        }elseif ($glisieres->gl3_existed_link2 != '0' AND $glisieres->gl3_existed_link2 == 'fd') {
                            echo $link_fidelity;
                        } elseif ($glisieres->gl3_custom_link2 != '') {
                            echo $glisieres->gl3_custom_link2;
                        } ?>" class="lienBouton">
                            <div style="margin-top: 20px;margin-bottom: 20px"
                                 class="bouton"><?php echo $glisieres->btn_gli3_content2; ?>
                            </div>
                        </a>
                    <?php }else{ ?>
                        <a href="<?php if ($glisieres->gl3_existed_link2 != '0' AND $glisieres->gl3_existed_link2 == 'p1') {
                            echo site_url($oInfoCommercant->nom_url . "/infos");
                        } elseif ($glisieres->gl3_existed_link2 != '0' AND $glisieres->gl3_existed_link2 == 'p2') {
                            echo site_url($oInfoCommercant->nom_url . "/autresinfos");
                        } elseif ($glisieres->gl3_existed_link2 != '0' AND $glisieres->gl3_existed_link2 == 'ag') {
                            echo site_url($oInfoCommercant->nom_url . "/agenda");
                        } elseif ($glisieres->gl3_existed_link2 != '0' AND $glisieres->gl3_existed_link2 == 'art') {
                            echo site_url($oInfoCommercant->nom_url . "/article");
                        } elseif ($glisieres->gl3_existed_link2 != '0' AND $glisieres->gl3_existed_link2 == 'bp') {
                            echo site_url($oInfoCommercant->nom_url . "/notre_bonplan");
                        }elseif ($glisieres->gl3_existed_link2 != '0' AND $glisieres->gl3_existed_link2 == 'fd') {
                            echo $link_fidelity;
                        }  elseif ($glisieres->gl3_custom_link2 != '') {
                            echo $glisieres->gl3_custom_link2;
                        } ?>" class="lienBouton">
                            <div style="margin-top: 20px;margin-bottom: 20px"
                                 class="bouton"><?php echo $glisieres->btn_gli3_content2; ?>
                            </div>
                        </a>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
</div>