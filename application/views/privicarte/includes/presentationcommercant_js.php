<script type="text/javascript">

    function compte_a_rebours_<?php echo $bonplan_contenue->bonplan_id; ?>() {
        var compte_a_rebours_<?php echo $bonplan_contenue->bonplan_id; ?> = document.getElementById("compte_a_rebours_<?php echo $bonplan_contenue->bonplan_id; ?>");

        var date_actuelle = new Date();
        var date_evenement = new Date("<?php echo $bonplan_contenue->bp_multiple_date_fin; ?>");
        var total_secondes = (date_evenement - date_actuelle) / 1000;

        var prefixe = "";
        if (total_secondes < 0) {
            prefixe = "Exp "; // On modifie le préfixe si la différence est négatif

            total_secondes = Math.abs(total_secondes); // On ne garde que la valeur absolue

        }

        if (total_secondes > 0) {
            var jours = Math.floor(total_secondes / (60 * 60 * 24));
            var heures = Math.floor((total_secondes - (jours * 60 * 60 * 24)) / (60 * 60));
            minutes = Math.floor((total_secondes - ((jours * 60 * 60 * 24 + heures * 60 * 60))) / 60);
            secondes = Math.floor(total_secondes - ((jours * 60 * 60 * 24 + heures * 60 * 60 + minutes * 60)));

            var et = "et";
            var mot_jour = "jours";
            var mot_heure = "heures,";
            var mot_minute = "minutes,";
            var mot_seconde = "secondes";

            if (jours == 0) {
                jours = '';
                mot_jour = '';
            }
            else if (jours == 1) {
                mot_jour = "jour,";
            }

            if (heures == 0) {
                heures = '';
                mot_heure = '';
            }
            else if (heures == 1) {
                mot_heure = "heure,";
            }

            if (minutes == 0) {
                minutes = '';
                mot_minute = '';
            }
            else if (minutes == 1) {
                mot_minute = "minute,";
            }

            if (secondes == 0) {
                secondes = '';
                mot_seconde = '';
                et = '';
            }
            else if (secondes == 1) {
                mot_seconde = "seconde";
            }

            if (minutes == 0 && heures == 0 && jours == 0) {
                et = "";
            }

            //compte_a_rebours.innerHTML = prefixe + jours + ' ' + mot_jour + ' ' + heures + ' ' + mot_heure + ' ' + minutes + ' ' + mot_minute + ' ' + et + ' ' + secondes + ' ' + mot_seconde;
            //compte_a_rebours_<?php //echo $bonplan_contenue->bonplan_id ; ?>.innerHTML = prefixe + jours + ' ' + mot_jour + ' ' + heures + ':' + minutes + ':' + secondes ;
            compte_a_rebours_<?php echo $bonplan_contenue->bonplan_id; ?>.innerHTML = prefixe + jours + ' ' + mot_jour + '<p class="margintop10"><span class="noirblanc">' + heures + '</span>:<span class="noirblanc">' + minutes + '</span>:<span class="noirblanc">' + secondes + '</span></p>';
        }
        else {
            compte_a_rebours_<?php echo $bonplan_contenue->bonplan_id; ?>.innerHTML = 'Expiré !';
            //alert(date_actuelle+ " "+date_evenement);
        }

        var actualisation = setTimeout("compte_a_rebours_<?php echo $bonplan_contenue->bonplan_id; ?>();", 1000);
    }
</script>