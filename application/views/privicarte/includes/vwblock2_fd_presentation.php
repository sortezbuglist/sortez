<div class="row" style=";width: 100%">
    <div style="width: 100%" class="ml-4 mb-4">
        <div class="col-sm-12 p-4 ml-4" style="background-image: url('<?php echo base_url('assets/img/fidelity_new.png') ?>');background-repeat: no-repeat;width: 100%;height: 250px;background-size: 100% 100%;position: relative;">
            <div class="d-flex" style="width: 50%;padding-left: 135px;;color: white;position: absolute;
  top: 50%; /* poussé de la moitié de hauteur du référent */
  transform: translateY(-50%); /* tiré de la moitié de sa propre hauteur */">
                    <?php if (isset($glisieres->fidelity_2_id) AND $glisieres->fidelity_2_id > 0) {
                    } ?>
                    <?php if (isset($fidelity_contenue) AND $fidelity_contenue != null) { ?>
                        <p><?php echo $fidelity_contenue->description; ?></p>
                    <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-sm-12 text-center text-justify pl-0 pr-0 pb-3">
        <?php if (isset($glisieres->fidelity_2_content) AND $glisieres->fidelity_2_content != '' AND $glisieres->fidelity_2_content != null) { ?>
            <div class="ml-4" style="">
                <div class="pl-4 pr-4"><?php echo htmlspecialchars_decode($glisieres->fidelity_2_content) ?></div>
            </div>
        <?php } ?>
            <?php if (isset($glisieres->is_activ_btn_fd_2) AND $glisieres->is_activ_btn_fd_2 == '1' AND $glisieres->is_activ_btn_fd_2 != null) {
                if ($glisieres->FD_2_existed_link == 'ag') {
                    $linkq3 = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($glisieres->FD_2_existed_link == 'art') {
                    $linkq3 = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($glisieres->FD_2_existed_link == 'p1') {
                    $linkq3 = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($glisieres->FD_2_existed_link == 'p2') {
                    $linkq3 = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($glisieres->FD_2_existed_link == 'bp') {
                    $linkq3 = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($glisieres->FD_2_existed_link == 'fd') {
                    $linkq3 = $link_fidelity;
                } elseif ($glisieres->FD_2_existed_link == 'bt') {
                    $linkq3 = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif (isset($glisieres->bp_2_custom_link) AND $glisieres->bp_2_custom_link != '' AND $glisieres->bp_2_custom_link != null) {
                    $linkq3 = 'https://' . $glisieres->bp_2_custom_link;
                }

                ?>
                <?php if (isset($glisieres->bloc2_islightboxbp) AND $glisieres->bloc2_islightboxfd=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkq3 ?? '' ?>"  class="fancybox.iframe"><div class="bouton2"><?php echo $glisieres->btn_fd_2_content ?></div></a>
                <?php }else{  ?>
                    <a class="lienBouton2" href="<?php echo ($linkq3) ?? '' ?>">
                        <div class="bouton2"><?php echo $glisieres->btn_fd_2_content ?></div>
                    </a>


                <?php } ?>
            <?php } ?>
    </div>
</div>