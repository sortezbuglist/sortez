<div class="row" style="">
    <div class="col-sm-6 text-center text-justify pr-0">
        <div class="ml-4 pb-4 pt-4 allign_button content_sm_block" style="height:auto;padding-right:0px;padding-left:0px;">
            <?php if (isset($glisieres->bloc_1_image1_content) AND $glisieres->bloc_1_image1_content != '' AND $glisieres->bloc_1_image1_content != null) { ?>
                <div class="image content_right_padd">
                    <img style="text-align: center!important;max-width: 100%;height: auto;"
                         src="<?php echo base_url($path_img_gallery_gl . '/' . $glisieres->bloc_1_image1_content) ?>">
                </div>
            <?php } ?>
            <?php if (isset($glisieres->bloc1_content) AND $glisieres->bloc1_content != '' AND $glisieres->bloc1_content != null) { ?>
                <div class="content_caract"><?php echo htmlspecialchars_decode($glisieres->bloc1_content) ?></div>
            <?php } ?>
            <?php if (isset($glisieres->is_activ_btn_bloc1) AND $glisieres->is_activ_btn_bloc1 == '1' AND $glisieres->is_activ_btn_bloc1 != null) {
                if ($glisieres->bloc1_existed_link1 == 'ag') {
                    $linkc = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($glisieres->bloc1_existed_link1 == 'art') {
                    $linkc = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($glisieres->bloc1_existed_link1 == 'p1') {
                    $linkc = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($glisieres->bloc1_existed_link1 == 'p2') {
                    $linkc = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($glisieres->bloc1_existed_link1 == 'bp') {
                    $linkc = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($glisieres->bloc1_existed_link1 == 'fd') {
                    $linkc =$link_fidelity;
                } elseif ($glisieres->bloc1_existed_link1 == 'bt') {
                    $linkc = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif ($glisieres->bloc1_existed_link1 == 'cmd') {
                     $linkc = base_url() . $oInfoCommercant->nom_url . '/commandes_commercants';    
                } elseif (isset($glisieres->bloc1_custom_link1) AND $glisieres->bloc1_custom_link1 != '' AND $glisieres->bloc1_custom_link1 != null) {
                    $linkc = 'https://' . $glisieres->bloc1_custom_link1;
                }

                ?>
                <?php if (isset($glisieres->bloc1_islightbox) AND $glisieres->bloc1_islightbox=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $linkc ?? '' ?>"  class="fancybox.iframe lienbtn2"><div class="bouton"><?php echo $glisieres->btn_bloc1_content1 ?></div></a>
                <?php }else{  ?>
                    <a class="pb-5 pt-5 lienBouton2 " href="<?php echo ($linkc) ?? '' ?>">
                        <div class="bouton"><?php echo $glisieres->btn_bloc1_content1 ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <div class="col-sm-6 text-center pl-0 text-justify content_sm_nb2">
        <div class="p-4 sautligne allign_button content_sm_nb2ct" style=";height:auto;padding-right:0px!important;">
            <?php if (isset($glisieres->bloc_1_image2_content) AND $glisieres->bloc_1_image2_content != '' AND $glisieres->bloc_1_image2_content != null) { ?>
                <div class="image content_right_padd">
                    <img style="text-align: center!important;max-width: 100%;height: auto;"
                         src="<?php echo base_url($path_img_gallery_gl . '/' . $glisieres->bloc_1_image2_content) ?>">
                </div>
            <?php } ?>
            <?php if (isset($glisieres->bloc1_content2) AND $glisieres->bloc1_content2 != '' AND $glisieres->bloc1_content2 != null) { ?>
                <div class="content_caract"><?php echo htmlspecialchars_decode($glisieres->bloc1_content2) ?></div>
            <?php } ?>
            <?php if (isset($glisieres->is_activ_btn_bloc2) AND $glisieres->is_activ_btn_bloc2 == '1' AND $glisieres->is_activ_btn_bloc2 != null) {
                if ($glisieres->bloc1_existed_link2 == 'ag') {
                    $link2 = base_url() . $oInfoCommercant->nom_url . '/agenda';
                } elseif ($glisieres->bloc1_existed_link2 == 'art') {
                    $link2 = base_url() . $oInfoCommercant->nom_url . '/article';
                } elseif ($glisieres->bloc1_existed_link2 == 'p1') {
                    $link2 = base_url() . $oInfoCommercant->nom_url . '/infos';
                } elseif ($glisieres->bloc1_existed_link2 == 'p2') {
                    $link2 = base_url() . $oInfoCommercant->nom_url . '/autresinfos';
                } elseif ($glisieres->bloc1_existed_link2 == 'bp') {
                    $link2 = base_url() . $oInfoCommercant->nom_url . '/notre_bonplan';
                } elseif ($glisieres->bloc1_existed_link2 == 'fd') {
                    $link2 = $link_fidelity;
                } elseif ($glisieres->bloc1_existed_link2 == 'bt') {
                    $link2 = base_url() . $oInfoCommercant->nom_url . '/annonces';
                } elseif ($glisieres->bloc1_existed_link1 == 'cmd') {
                     $link2 = base_url() . $oInfoCommercant->nom_url . '/commandes_commercants';    
                } elseif (isset($glisieres->bloc1_custom_link2) AND $glisieres->bloc1_custom_link2 != '' AND $glisieres->bloc1_custom_link2 != null) {
                    $link2 = 'https://' . $glisieres->bloc1_custom_link2;
                }

                ?>
                <?php if (isset($glisieres->bloc1_islightbox2) AND $glisieres->bloc1_islightbox2=="1"){ ?>

                    <a data-fancybox data-type="iframe" data-src="<?php echo $link2 ?? '' ?>"  class="fancybox.iframe lienbtn2"><div class="bouton"><?php echo $glisieres->btn_bloc1_content2 ?></div></a>
                <?php }else{  ?>
                    <a class="pb-5 pt-5 lienBouton2 " href="<?php echo ($link2) ?? '' ?>">
                        <div class="bouton"><?php echo $glisieres->btn_bloc1_content2 ?></div>
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>