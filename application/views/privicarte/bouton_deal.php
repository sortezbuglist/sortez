
<?php
$data['empty'] = null;
$this->load->view("sortez/includes/backoffice_pro_css", $data);
?>

<?php $this->load->view("privicarte/includes/main_header", $data); ?>
<?php //$this->load->view("privicarte/includes/ready_annonces", $data); ?>
<?php $this->load->view("privicarte/includes/main_navbar", $data); ?>
<?php //$this->load->view("privicarte/includes/main_logo_dep_com", $data); ?>
<?php //$this->load->view("privicarte/includes/main_menu", $data); ?>
<?php //$this->load->view("privicarte/includes/main_slide", $data); ?>
<?php //$this->load->view("privicarte/includes/main_map", $data); ?>
<?php //$this->load->view("privicarte/includes/main_menu_contents", $data); ?>

<div class="container" style=" background-color:#ffffff; padding-top:60px;">
    <div class="col-lg-12 padding0" style="text-align: center; display: none;"><img src="<?php echo base_url(); ?>application/resources/sortez/images/logo.png" alt="logo"></div>
</div>


<div class="container top_bottom_15" style="background-color: #fff; padding-bottom:40px;">

    <div class="col-sm-12">
        <link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/") ; ?>/style.css" />
        <link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/") ; ?>/blue/style.css" />
        <link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/") ; ?>/jquery-ui-1.8.4.custom.css" />
        <link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/") ; ?>/demo_table_jui.css" />
        <script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/jquery.tablesorter.js"></script>
        <script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/jquery.tablesorter.pager.js"></script>
        <script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/ajaxfileupload.js"></script>
        <script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/jquery.ui.core.js"></script>
    </div>


<script type="text/javascript" charset="utf-8"> 


      function delete_jq_bnp_icon(id_com){
        var data = "idcom="+id_com;
        $.ajax({
            type: "POST",
            url: "https://www.sortez.org/bonplan/delete_bnp_icon/"+id_com,
            data: data,
           
            success: function(data) {
                if (data != "") {
                    var to_open = "'https://www.sortez.org/media/index/"+id_com+"-bnp_icon-photo_menu_gen"+""+"' , '' ,'width=1045 , height=675, scrollbars=yes'"
                    var to_ch = '<div id="Menuphoto_menu_gen_container" onclick="javascript:window.open('+to_open+');", href="javascript:void(0);" class="w-100" ><img  src="https://www.sortez.org/assets/image/download-icon-png-5.jpg" style="height: 160px"></div>';
                    $("#img_menu_gen_contents").html(to_ch);
               }
            },
            error: function() {
               
            alert(data);
            }
        });
    }

    
</script>

<div class="">
        <h1>Personnalisé votre bouton ici!...</h1><br><br>
    </div>


<form method="post" action="<?php echo site_url();?>bonplan/save_icon_bnp/<? echo $infocom->IdCommercant?>" class="row pb-5 shadowed p-2 gli_content d-flex">
 <div class="col-4 text-center">

        choisissez la couleur du bouton en cliquant sur le panel 
        <input type="color" name="data_bnp[bnp_color]" style="width:150px; height: 150px"  value="<?php if (isset($infocom->bnp_color)) echo $infocom->bnp_color; ?>">
    </div>
   <div class="col-4">

        <?php if (isset($infocom) AND $infocom!=null){?>
        <div class="w-100 text-center top_menu_gen">
         Télécharger l'image de personnalisation, Cliquez sur l'icone !...
        </div><?}else{?><div class="w-100 text-center top_menu_gen">
          Veuiller enregistré la couleur et le titre avant de Télécharger l'image de personnalisation en Cliquez sur l'icone !...
        </div><?}?>

        <div class="w-100 text-center" id="img_menu_gen_contents">
            <?php if (isset($infocom->bnp_icon) AND $infocom->bnp_icon !=null && is_file("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/bnp_icon/".$infocom->bnp_icon)){ ?>
                <div class="w-100">
                    <img style="max-width: 200px" src="<?php echo base_url()?>application/resources/front/photoCommercant/imagesbank/<?php echo $infocom->user_ionauth_id; ?>/bnp_icon/<?php echo $infocom->bnp_icon ?>">
                    <a onclick="delete_jq_bnp_icon(<?php echo $infocom->IdCommercant; ?>)" class="btn btn-danger mt-3">Supprimer image</a>
                </div>
            <?php }else{ ?>
                <div class="w-100" id="Menuphoto_menu_gen_container">
                    <img onclick='javascript:window.open("<?php echo site_url("media/index/".$infocom->IdCommercant."-bnp_icon-photo_menu_gen"); ?>", "", "width=1045, height=675, scrollbars=yes");' src="<?php echo base_url()?>assets/image/download-icon-png-5.jpg">
                </div>
                <div id="Articleimg_menuphoto_menu_gen_container"></div>
            <?php } ?>
        </div>
    </div>

    <div class="col-4">
        <div class="w-100 top_menu_gen">
            Précisez le texte
            du bouton
        </div>
        <div class="w-100 text-center menu_txt_container">
            <input maxlength="22" value="<?php if (isset($infocom->bnp_menu)) echo $infocom->bnp_menu; ?>" type="text" class="form-control input_menu_txt" name="data_bnp[bnp_menu]">
            <?php if($error_message!=NULL):?>
           <div class="alert alert-danger mt-2" role="alert">
                <strong>Erreur ! </strong><?php print_r($error_message);?>
            </div>
            <?php endif;?>
        </div>
    </div>
    <div class="w-100 text-center pt-4 pb-4">
        <input type="submit" class="btn btn-primary" value="Enregistrer"/>
    </div>
</form>
</div>
<?php $this->load->view("privicarte/includes/main_footer_link", $data); ?>
<?php $this->load->view("privicarte/includes/main_footer_copyright", $data); ?>
<?php $this->load->view("privicarte/includes/main_footer", $data); ?>