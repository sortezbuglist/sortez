<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Demande de Devis</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js">
      
        <?php $data["zTitle"] = 'Demande de devis' ?>

        <?php  //$this->load->view("sortez/includes/header_frontoffice_com", $data); ?>



        <?php  $this->load->view("sortez/logo_global", $data); ?>


      
    <script type="text/javascript" src="<?php echo GetJsPath('front/'); ?>/fields.check.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.6.4.js"></script>

    <script src="https://www.google.com/recaptcha/api.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-3.2.1.js"></script>

    <meta name="viewport" content="width=device-width" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('application/resources/privicarte/css/global.css') ?>">

    <script type="text/javascript">



        // Use jQuery via $(...)

        $(document).ready(function () {//debut ready fonction

            $('#divErrortxtEmail_').hide();



            //To show sousrubrique corresponding to rubrique

            $("#RubriqueSociete").change(function () {

                $('#trReponseRub').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

                var irubId = $("#RubriqueSociete").val();

                //alert(irubId);

                $.ajax({

                    type: "GET",

                    url: "<?php echo base_url(); ?>front/professionnels/testAjax/" + irubId,

                    success: function (msg) {

                        //alert(msg);

                        var numero_reponse = msg;

                        $("#trReponseRub").html(numero_reponse);

                        //alert(numero_reponse);

                    }

                });

                //alert ("test "+$("#RubriqueSociete").val());



            });





            $("#EmailSociete").blur(function () {

                //alert('cool');

                //var result_to_show = "";

                var value_result_to_show = "0";



                var txtEmail = $("#EmailSociete").val();

                //alert('<?php //echo site_url("front/professionnels/verifier_email"); ?>' + '/' + txtEmail);

                //jQuery(".EmailLoading").addClass("EmailLoading_show").removeClass("EmailLoading");

                $.post(

                    '<?php echo site_url("front/professionnels/verifier_email");?>',

                    {txtEmail_var: txtEmail},

                    function (zReponse) {

                        //alert (zReponse) ;

                        //var zReponse_html = '';

                        if (zReponse == "1") {

                            value_result_to_show = "1";

                        }





                        if (value_result_to_show == "1") {

                            result_to_show = '<span style="color:#F00; font-weight:bold;">Ce mail est d&eacute;j&agrave; utilis&eacute;</span>';

                            $('#divErrortxtEmail_').html(result_to_show);

                            $('#txtEmail_verif').val("1");

                        } else {

                            result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce mail est disponible</span>';

                            $('#divErrortxtEmail_').html(result_to_show);

                            $('#txtEmail_verif').val("0");

                        }



                    });



                $.post(

                    '<?php echo site_url("front/professionnels/verifier_email_ionauth");?>',

                    {txtEmail_var_ionauth: txtEmail},

                    function (zReponse_ionauth) {

                        //alert (zReponse) ;

                        //var zReponse_html_ionauth = '';

                        if (zReponse_ionauth == "1") {

                            value_result_to_show = "1";

                        }





                        if (value_result_to_show == "1") {

                            result_to_show = '<span style="color:#F00; font-weight:bold;">Ce mail est d&eacute;j&agrave; utilis&eacute;</span>';

                            $('#divErrortxtEmail_').html(result_to_show);
                            $('#divErrortxtEmail_').show();

                            $('#txtEmail_verif').val("1");

                        } else {

                            result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce mail est disponible</span>';

                            $('#divErrortxtEmail_').html(result_to_show);
                            $('#divErrortxtEmail_').show();

                            $('#txtEmail_verif').val("0");

                        }





                    });





                //jQuery(".FieldError").removeClass("FieldError");

                //jQuery(".EmailLoading").addClass("EmailLoading").removeClass("EmailLoading_show");



            });





            $("#txtLogin").blur(function () {

                //alert('cool');

                var txtLogin = $("#txtLogin").val();



                var value_result_to_show = "0";



                //alert('<?php //echo site_url("front/professionnels/verifier_login"); ?>' + '/' + txtEmail);

                //jQuery(".EmailLoading").addClass("EmailLoading_show").removeClass("EmailLoading");

                $.post(

                    '<?php echo site_url("front/professionnels/verifier_login");?>',

                    {txtLogin_var: txtLogin},

                    function (zReponse) {

                        //alert (zReponse) ;

                        var zReponse_html = '';

                        if (zReponse == "1") {

                            value_result_to_show = "1";

                        }





                        if (value_result_to_show == "1") {

                            result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';

                            $('#divErrortxtLogin_').html(result_to_show);

                            $('#txtLogin_verif').val("1");

                        } else {

                            result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';

                            $('#divErrortxtLogin_').html(result_to_show);

                            $('#txtLogin_verif').val("0");

                        }



                    });





                $.post(

                    '<?php echo site_url("front/professionnels/verifier_login_ionauth");?>',

                    {txtLogin_var_ionauth: txtLogin},

                    function (zReponse_ionauth) {

                        //alert (zReponse) ;

                        var zReponse_html_ionauth = '';

                        if (zReponse_ionauth == "1") {

                            value_result_to_show = "1";

                        }





                        if (value_result_to_show == "1") {

                            result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';

                            $('#divErrortxtLogin_').html(result_to_show);

                            $('#txtLogin_verif').val("1");

                        } else {

                            result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';

                            $('#divErrortxtLogin_').html(result_to_show);

                            $('#txtLogin_verif').val("0");

                        }



                    });





                //jQuery(".FieldError").removeClass("FieldError");

                //jQuery(".EmailLoading").addClass("EmailLoading").removeClass("EmailLoading_show");



            });





            //To show postal code automatically

            $("#VilleSociete").change(function () {

                var irubId = $("#VilleSociete").val();

                //alert(irubId);

                $.ajax({

                    type: "GET",

                    url: "<?php echo base_url(); ?>front/professionnels/getPostalCode/" + irubId,

                    success: function (msg) {

                        //alert(msg);

                        var numero_reponse = msg;

                        $("#CodePostalSociete").val(numero_reponse);

                        //alert(numero_reponse);

                    }

                });

                //alert ("test "+$("#VilleSociete").val());

            });





            var valabonnementht = 0;

            var valmoduleht = 0;





            function calcmontantht() {



                var totalmontantht = 0;

                var montanttva = 0;

                var valtva = 0.20;

                var montantttc = 0;



                var $check_abonnement_list = new Array();

                <?php foreach ($colAbonnements as $objAbonnement) { ?>

                if ($('#check_abonnement_<?php echo $objAbonnement->IdAbonnement;?>').attr('checked'))  totalmontantht += parseInt("<?php echo $objAbonnement->tarif;?>");

                <?php } ?>



                //totalmontantht = check_358_premium_value + check_358_platinium_value + check_358_agenda_plus_value + check_358_web_ref1_value + check_358_web_ref_n_value + check_358_restauration_value;

                //alert(totalmontantht);

                $("#divMontantHT").html(totalmontantht + "€");

                $("#hidemontantht").val(totalmontantht + "€");

                //calcmontanttva (parseInt(totalmontantht), valtva);

                //calcmontantttc (parseInt(totalmontantht), montanttva);

                montanttva = totalmontantht * valtva;

                $("#divMontantTVA").html(_roundNumber(montanttva, 2) + "€");

                $("#hidemontanttva").val(_roundNumber(montanttva, 2) + "€");

                montantttc = totalmontantht + montanttva;

                $("#divMontantTTC").html(montantttc + "€");

                $("#hidemontantttc").val(montantttc + "€");

                $("#montantttcvalue_abonnement").val(montantttc);

            }





            function calcmontanttva(totalmontantht, valtva) {

                montanttva = totalmontantht * valtva;

                $("#divMontantTVA").html(_roundNumber(montanttva, 2) + "€");

            }



            //limit decimal

            function _roundNumber(num, dec) {



                return (parseFloat(num)).toFixed(dec);

            }



            function calcmontantttc(totalmontantht, montanttva) {

                montantttc = totalmontantht + montanttva;

                $("#divMontantTTC").html(montantttc + "€");

                $("#montantttcvalue_abonnement").val(montantttc);

            }









        })





    </script>



    <script type="text/javascript">var blankSrc = "wpscripts/blank.gif";

    </script>



    <script type="text/javascript">

        function btn_login_page_avantagepro() {

            //alert('qsdfgqsdf');

            var txtError = "";



            var user_login = $("#user_login").val();

            if (user_login == "" || user_login == "Préciser votre courriel") {

                txtError += "<br/>- Veuillez indiquer Votre login !";

                $("#user_login").css('border-color', 'red');

            }



            var user_pass = $("#user_pass").val();

            if (user_pass == "" || user_pass == "Préciser votre mot de passe") {

                txtError += "<br/>- Veuillez indiquer Votre mot de passe !";

                $("#user_pass").css('border-color', 'red');

            }



            if (txtError == "") {

                $("#frmConnexion").submit();

            }

        }



        $(function () {

            $("#user_login").focusin(function () {

                if ($(this).val() == "Préciser votre courriel") {

                    $(this).val("");

                }

            });

            $("#user_login").focusout(function () {

                if ($(this).val() == "") {

                    $(this).val("Préciser votre courriel");

                }

            });

            $("#user_pass").focusin(function () {

                if ($(this).val() == "Préciser votre mot de passe") {

                    $(this).val("");

                }

            });

            $("#user_pass").focusout(function () {

                if ($(this).val() == "") {

                    $(this).val("Préciser votre mot de passe");

                }

            });

        });



        jQuery(document).ready(function () {

            jQuery("#btnSinscrire").click(function () {

                var cap=document.getElementById("g-recaptcha-response").value;

                             test(cap);//21103 //toFirstIdDatatourisme

            });
             


            // $(".checkBox").click(function() {
            //   var x=$('.packs').find('.checkBox');
            //    if(x.is(':checked')){
            //      var packs=$(this).next().text();
            //      console.log(packs);
            //    }                                     
            // });

                       

    
$('#send1').click(function(){  
    $( '#response' ).hide();
    // $('#msg' ).hide();
    //Abonnement Choisi
       var radio_value = []; 
       $('.radioAbon').each(function(){  
            //if($(this).is(":checked")) { 
            if(this.checked) {              
                 radio_value.push($(this).val());                                                                               
            }  
       });
       radio_value = radio_value.toString(); 
    //packs cocher   
       var checkboxes_value = []; 
        var inputval=$(".text").val();//getting value of input field
       $('.checkBox').each(function(){  
            //if($(this).is(":checked")) { 
            if(this.checked) {              
                 checkboxes_value.push($(this).next().text());                                                                               
            }  
       });                              
       checkboxes_value = checkboxes_value.toString(); 
    //autre packs
       var checkBox_autre = []; 
       $('.checkBox_autre').each(function(){  
            //if($(this).is(":checked")) { 
            if(this.checked) {              
                 checkBox_autre.push($(this).next().text());                                                                               
            }

       });                              
       checkBox_autre = checkBox_autre.toString();        

            var IdCommercant=$('#IdCommercant').val();
            var RubriqueSociete=$('#RubriqueSociete option:selected').text().trim();
            // var RubriqueSociete = $( '#RubriqueSociete' ).val();
            // var SousRubriqueSociete = $( '#SousRubriqueSociete' ).val();
            var SousRubriqueSociete=$('#SousRubriqueSociete option:selected').text().trim();
            var inputZip1 = $( '#inputZip1' ).val();
            var status=$('#status option:selected').text().trim();
            // var status = $( '#status' ).val();
            var Adresse1Societe = $( '#Adresse1Societe' ).val();
            var CodePostalSociete = $( '#CodePostalSociete' ).val();
            var IdVille_Nom_text = $( '#IdVille_Nom_text' ).val();
            var departement_id = $( '#departement_id' ).val();
            var EmailSociete = $( '#EmailSociete' ).val();
            var TelFixeSociete = $( '#TelFixeSociete' ).val();
            var SiteWebSociete = $( '#SiteWebSociete' ).val();
            var nbr_employe = $( '#nbr_employe' ).val();
            var siret = $( '#siret' ).val();
            var CiviliteResponsableSociete=$('#CiviliteResponsableSociete option:selected').text().trim();

            var NomResponsableSociete = $( '#NomResponsableSociete' ).val();
            var Prenom = $( '#Prenom' ).val();
            var FoncRespSociete = $( '#FoncRespSociete' ).val();
            var Email_decidRespSociete = $( '#Email_decidRespSociete' ).val();
            var TelDirectRespSociete = $( '#TelDirectRespSociete' ).val();
            
                // $( '#msg' ).hide();          
               if($('#valide').is(":checked")) { 
       $.ajax({  
            url:"",  
            method:"POST",
            url: "<?php echo site_url("front/professionnels/sendEmail"); ?>",  
            data:{ radio_value:radio_value, checkboxes_value:checkboxes_value,checkBox_autre:checkBox_autre, IdCommercant:IdCommercant, RubriqueSociete: RubriqueSociete, SousRubriqueSociete: SousRubriqueSociete, inputZip1: inputZip1, status:status, Adresse1Societe:Adresse1Societe, CodePostalSociete:CodePostalSociete,IdVille_Nom_text:IdVille_Nom_text, departement_id:departement_id, EmailSociete:EmailSociete, TelFixeSociete:TelFixeSociete, SiteWebSociete:SiteWebSociete, nbr_employe:nbr_employe, siret:siret, CiviliteResponsableSociete:CiviliteResponsableSociete, NomResponsableSociete:NomResponsableSociete, Prenom:Prenom, FoncRespSociete:FoncRespSociete, Email_decidRespSociete:Email_decidRespSociete, TelDirectRespSociete:TelDirectRespSociete},  
            success:function(data){  
                 // $('#msg' ).text().fadeIn( 'slow' ).delay( 3000 ).fadeOut( 'slow' );

                 $( '#msg2' ).html( "<h1 style='color:green;'>Demande envoyer :</h1>").fadeIn( 'slow' ).delay( 3000 ).fadeOut( 'slow' );
                 $( '#response' ).html("<p style='color:green;'>"+data+"</p>" ).fadeIn( 'slow' ).delay( 3000 ).fadeOut( 'slow' );

            },
            error: function () {
                alert("erreur d'envoie");
            }            

       });                      
                            
               }else{
                    $( '#msg' ).html( "<p style='color:red'>Veuillez cochez la case valide ma demande</p>").fadeIn( 'slow' ).delay( 3000 ).fadeOut( 'slow' );
                    
               }   

});
//end jquery
});

        function test(cap) {

            jQuery.ajax({

                type: "POST",

                url: "<?php echo site_url("front/professionnels/test_captcha"); ?>",

                data: 'g-recaptcha-response=' + (cap),

                dataType: "json",

                success: function (data) {



                    var txtError = false;

                    var RubriqueSociete = $('#RubriqueSociete').val();

                    if (RubriqueSociete === "") {

                        txtError = "";
                        txtError += "- Vous devez préciser votre activité<br/>";

                    }

                    var SousRubriqueSociete = $('#SousRubriqueSociete').val();

                    if (SousRubriqueSociete == "0") {
                        txtError = "";
                        txtError += "- Vous devez préciser une sous-rubrique<br/>";

                    }

                    var NomSociete = $('#NomSociete').val();

                    if (NomSociete == "") {
                        txtError = "";
                        txtError += "- Vous devez préciser le Nom ou enseigne<br/>";

                    }

                    var ivilleId = $('#VilleSociete').val();

                    if (ivilleId == 0) {
                        txtError = "";
                        txtError += "- Vous devez sélectionner une ville<br/>";

                    }

                    var CodePostalSociete = $('#CodePostalSociete').val();

                    if (CodePostalSociete == "") {
                        txtError = "";
                        txtError += "- Vous devez préciser le code postal<br/>";

                    }

                    var EmailSociete = $("#EmailSociete").val();

                    if (!isEmail(EmailSociete)) {
                        txtError = "";
                        txtError += "- Veuillez indiquer un email valide.<br/>";

                    }

                    var txtEmail_verif = $("#txtEmail_verif").val();

                    if (txtEmail_verif == 1) {
                        txtError = "";
                        txtError += "- Votre Email existe déjà sur notre site <br/>";

                    }

                    var NomResponsableSociete = $('#NomResponsableSociete').val();

                    if (NomResponsableSociete == "") {
                        txtError = "";
                        txtError += "- Vous devez préciser le Nom du Decideur <br/>";

                    }

                    var PrenomResponsableSociete = $('#PrenomResponsableSociete').val();

                    if (PrenomResponsableSociete == "") {
                        txtError = "";
                        txtError += "- Vous devez préciser le Prenom du Decideur <br/>";

                    }

                    var ResponsabiliteResponsableSociete = $('#ResponsabiliteResponsableSociete').val();

                    if (ResponsabiliteResponsableSociete == "") {
                        txtError = "";
                        txtError += "- Vous devez préciser la responsabilité du Decideur <br/>";

                    }

                    var TelDirectResponsableSociete = $('#TelDirectResponsableSociete').val();

                    if (TelDirectResponsableSociete == "") {
                        txtError = "";
                        txtError += "- Vous devez préciser le numero de téléphone du Decideur <br/>";

                    }

                    var Email_decideurResponsableSociete = $("#Email_decideurResponsableSociete").val();

                    if (!isEmail(Email_decideurResponsableSociete)) {
                        txtError = "";
                        txtError += "- Veuillez indiquer un email valide pour le decideur.<br/>";

                    }

                    var AbonnementSociete = $('#AbonnementSociete').val();

                    if (AbonnementSociete == "0") {
                        txtError = "";
                        txtError += "- Vous devez choisir votre abonnement<br/>";

                    }

                    var activite1Societe = $('#activite1Societe').val();

                    if (activite1Societe == "") {
                        
                        txtError = "";
                        txtError += "- Vous devez décrire votre activité<br/>";

                    }

                    var txtLogin = $("#txtLogin").val();

                    if (!isEmail(txtLogin)) {
                        txtError = "";
                        txtError += "- Votre login doit &ecirc;tre un email valide.<br/>";

                    }

                    var txtLogin_verif = $("#txtLogin_verif").val();

                    if (txtLogin_verif == 1) {
                        txtError = "";
                        txtError += "- Votre Login existe déjà sur notre site <br/>";

                    }

                    var passs = $('#txtPassword').val();

                    if (passs == "") {
                        txtError = "";
                        txtError += "- Vous devez spécifier un mot de passe<br/>";

                    }

                    // if ($("#txtPassword").val() != $("#txtConfirmPassword").val()) {

                    //     txtError += "- Les deux mots de passe ne sont pas identiques.<br/>";

                    // }

                    // if ($('#validationabonnement').is(":checked")) {

                    // } else {
                    //     txtError = "";
                    //     txtError += "- Vous devez valider les conditions générales<br/>";

                    // }

                    if ($('#idreferencement0').is(":checked")) {

                        $("#idreferencement").val("1");

                    } else {

                        $("#idreferencement").val("0");

                    }

                    if (txtError ==false && data.captcha == "OK"){



                        $("#frmInscriptionProfessionnel").submit();

                    }if (data.captcha == "NO") {

                        alert("captha non valide");

                    }

                    if (txtError !=false){

                        $("#divErrorFrmInscriptionProfessionnel").html(txtError);

                    }

                },

                error: function (data) {



                    alert("data");

                }

            });

        }



        function CP_getDepartement() {

            //alert(jQuery('#CodePostalSociete').val());

            jQuery('#departementCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

            var CodePostalSociete = jQuery('#CodePostalSociete').val();

            jQuery.post(

                '<?php echo site_url("front/professionnels/departementcp"); ?>',

                {CodePostalSociete: CodePostalSociete},

                function (zReponse) {

                    jQuery('#departementCP_container').html(zReponse);

                });

        }



        function CP_getVille() {

            //alert(jQuery('#CodePostalSociete').val());

            jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

            var CodePostalSociete = jQuery('#CodePostalSociete').val();

            jQuery.post(

                '<?php echo site_url("front/professionnels/villecp"); ?>',

                {CodePostalSociete: CodePostalSociete},

                function (zReponse) {

                    jQuery('#villeCP_container').html(zReponse);

                });

        }



        function CP_getVille_D_CP() {

            //alert(jQuery('#CodePostalSociete').val());

            jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

            var CodePostalSociete = jQuery('#CodePostalSociete').val();

            var departement_id = jQuery('#departement_id').val();

            jQuery.post(

                '<?php echo site_url("front/professionnels/villecp"); ?>',

                {CodePostalSociete: CodePostalSociete, departement_id: departement_id},

                function (zReponse) {

                    jQuery('#villeCP_container').html(zReponse);

                });

        }



    </script>
        <style>
            
       
            
            input[type="text" ] {
                /* color: #f8f8f8; */
                height: 37px;
                padding: 1px 2px;
                padding-top: 1px;
                padding-right: 2px;
                padding-bottom: 1px;
                padding-left: 13px;
                border:solid 2px;
                border-radius: 0px;
                border-color: #E80EAE;
            }
            input[type="text"]:hover {
                background-color: rgb(214, 255, 255);
                border: solid 1px;
                border-color: rgb(128, 210, 224);
            }
            input[type="date"]{
                color: #000000;
                height: 37px;
                padding: 1px 2px;
                padding-top: 1px;
                padding-right: 2px;
                padding-bottom: 1px;
                padding-left: 20px;
                border:solid 2px;
                border-radius: 0px;
                width: 300px;
                border-color: #E80EAE;
            }
            #VilleSociete select 
            {           
                border-radius: 0px;
                border: solid 2px;
                border-color: #E80EAE;
            }
            input[type="date"]:hover{
                background-color: rgb(214, 255, 255);
                border: solid 1px;
                border-color: rgb(128, 210, 224);
            }
            
            #cible select
             {
                border-radius: 0px;
                border: solid 2px;
                border-color: #E80EAE;
            }
            #cible:hover select
            {
                background-color:rgb(214, 255, 255);
                border: solid 1px;
                border-color: rgb(128, 210, 224);
            }
            
            .nav-link{
            /*font-size: 16px;*/
            border-radius: 25px;

            transition-duration: 0.5s;
        }

            .nav-item:hover{
                margin: 1.5rem!important;
            }
            .nav-link:hover{
                background-color: rgb(63, 118, 82);
                border: 2px solid rgb(63, 118, 82);
                color: #FFF;
                
            }

            #lien
            {
            text-decoration: inherit;
            color: white;
            color: white;
            height: 39px;
            width: 329px;
            border: 2px solid #fff;
            background-color: #e52bb3;
            font-size: 14px;
            display: flex;
            justify-content: center;
            align-items: center;
            font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;
            }
            #lien:hover
            {
            text-decoration: inherit;
            color: white;
            height: 39px;
            width: 329px;
            border: 2px solid #fff;
            background-color: #5eccff;
            font-size: 14px;
            display: flex;
            justify-content: center;
            align-items: center;
            font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;
            }
            .liens
            {
            text-decoration: inherit;
            color: white;
            height: 49px;
            width: 377px;
            font-family: Georgia,serif;
            border: 2px solid #fff;
            background-color: #e52bb3;
            font-size: 14px;
            display: flex;
            justify-content: center;
            align-items: center;
            font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;
            }
            .liens:hover
            {
            text-decoration: inherit;
            font-family: Georgia,serif;
            color: white;
            height: 49px;
            width: 377px;
            border: 2px solid #fff;
            background-color: #5eccff;
            font-size: 14px;
            display: flex;
            justify-content: center;
            align-items: center;
            font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;
            }

            /*style checkbox*/

            input[type="checkbox"]{
            appearance: none;
            -webkit-appearance: none;
            height: 19px;
            width: 18px;
            background-color: #fff;
            border: #E80EAE solid 3px;
            border-radius: 0px;
            cursor: pointer;
        }
        input[type="checkbox"]:after{
            font-family: "Font Awesome 5 Free";
            font-size: 12px;
            margin-top: -3px;
            font-weight: 900;
            content: "\2713";
            color: #E80EAE;
            display: none;
        }
        input[type="checkbox"]:hover{
            border: rgb(128, 210, 224) solid 3px;
            background-color: #fff;

        }
        input[type="checkbox"]:checked{
            background-color: #fff;
        }
        input[type="checkbox"]:checked:after{
            display: block;
        }

        /*style radio*/
        
         .radio{
            font-size: 18px;
            font-weight: 500;
            text-transform: capitalize;
            display: inline-block;
            vertical-align: middle;
            color: rgb(29, 27, 27);
            position: relative;
            cursor: pointer;
            padding-left: 30px;
        }
        .radio + .radio{
            margin-left: 20px;
        }
        .radio input[type="radio"]{
            display: none;
        }
        .radio span{
            height: 20px;
            width: 20px;
            border-radius: 50%;
            border: 3px solid #E80EAE;
            display: block;
            position: absolute;
            left: 0;
            top: 7px;
        }
        .radio span:after{
            content: "";
            height: 8px;
            width: 8px;
            background: #E80EAE;
            display: block;
            position: absolute;
            left: 50%;
            top: 50%;
            transform:translate(-50%,-50%) scale(0);
            border-radius: 50%;
            transition: 300ms ease-in-out 0s;
        }
        .radio input[type="radio"]:checked ~ span:after{
            transform:translate(-50%,-50%) scale(1);

        }
    
        </style>
    </head>
    <body>

            <div class="container">
                <nav>
                    <ul class="nav d-flex justify-content-center text-center mb-2 p-2">
                      <li class="nav-item m-4" style="background-color:#E80EAE;border:2px solid #E80EAE;font-size:16px;font-family:proxina nova;width:200px;border-radius:25px;">
                        <a class="nav-link divi" href="<?php echo site_url(); ?>" style="color: #fff;font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;">Accueil Sortez</a>
                      </li>
                      <li class="nav-item m-4" style="background-color:#E80EAE;border:2px solid #E80EAE;font-size:16px;font-family:proxina nova;width:200px;border-radius:25px;">
                        <a class="nav-link divi" href="https://www.soutenonslecommercelocal.fr/abonnement-premium" style="color: #fff;font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;">Détails Premium</a>
                      </li>
                      <li class="nav-item m-4" style="background-color:#E80EAE;border:2px solid #E80EAE;font-size:16px;font-family:proxina nova;width:200px;border-radius:25px;">
                        <a class="nav-link divi" href="https://www.soutenonslecommercelocal.fr/abonnement-platinium" style="color: #fff;font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;">Détails Platinium</a>
                      </li>
                      <li class="nav-item m-4" style="background-color:#E80EAE;border:2px solid #E80EAE;font-size:16px;font-family:proxina nova;width:200px;border-radius:25px;">
                        <a class="nav-link divi" href="https://www.soutenonslecommercelocal.fr/modules-packs-optionnel" style="color: #fff;font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;">Détails marketplace</a>
                      </li>
                    </ul>
                </nav>
            </div>   
        
        <div class="container" style="width:980px;">
                <div class=" row mt-5 mb-5 d-flex justify-content-center text-center" style="margin-bottom:0 !important">
                    <div class="col-lg-7">
                        <div class="row">
                            <h3 class="font_3" style="font-size:40px; line-height:1em; text-align:center;">
                                <span style="font-size:40px; ">
                                    <span style="font-style:italic;">
                                        <span style="font-family:libre baskerville,serif;">
                                            <span style="color:#E80EAE;">
                                                <span style="letter-spacing:normal;">Demande de souscription<br>
                                                        à nos abonnements<br>
                                                        Premium ou Platinium!...                                          
                                                </span>
                                            </span>
                                        </span>
                                    </span>
                                </span>
                            </h3>

                            <div id="comp-l0l03z0a" class="_2Hij5" data-testid="richTextElement" style="margin-top: 65px;">
                                <p class="font_7" style="text-align:center; font-size:15px; padding-top:30px;">
                                    <span style="color:#000000;">
                                        <span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;">Si vous êtes déjà possesseur d'un </span>
                                    </span>
                                    <span style="color:#000000;">
                                        <span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;">abonnement basique ou Premium</span>
                                    </span>
                                </p>

                                <p class="font_7" style="text-align:center; font-size:15px;">
                                    <span style="color:#000000;">
                                        <span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;">inscrivez votre identifiant et mot de passe</span>
                                    </span>
                                </p>
                                <p class="font_7" style="text-align:center; font-size:15px;">
                                    <span style="color:#000000;">
                                        <span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;">et vos coordonnées enregistrées rejoindront</span>
                                    </span>
                                </p>
                                
                                <p class="font_7" style="text-align:center; font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;">automatiquement les champs correspondants...</span></span></p></div>
                        </div>
                    </div>
                    <div class="col-lg-5 p-0">
                        <div style="width:400px;">
                        <img class="img-fluid img-responsive" style="padding-top: 0rem!important;width: 100%;height: auto;" src="https://www.randawilly.ovh/assets/images/ordi-woman1.webp">
                        </div>
                    </div>
                </div>
            </div>
        <div class="container card" style="background: #f8f8f8; width:980px;">
            <form name="frmInscriptionProfessionnel" id="frmInscriptionProfessionnel" action="<?php //echo site_url('front/professionnels/ajouter'); ?>" method="POST" accept-charset="UTF-8" target="_self" enctype="multipart/form-data">
                <div class="container" style="background: #f8f8f8; width:820px;">
                    <fieldset>
                        <legend></legend>
                        <br><br>

                        <div class="row"  id="radio" class="radio">
                               
                                <div class="col-md-4">
                                    <div class="radio"> 
                                        <label for="radio-1">
                                            <input type="radio" name="radio" id="radio-1" value= "Abonnement Premium" class="radioAbon" checked="checked"> 
                                            Abonnement "Premium"
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="radio"> 
                                        <label for="radio-2">
                                            <input type="radio" name="radio" id="radio-2" value= "Abonnement Platinium" class="radioAbon"> 
                                            Abonnement "Platinium"
                                            <span></span>
                                        </label>
                                    </div>
                                </div><div class="col-md-4">
                                    <div class="radio"> 
                                        <label for="radio-3">
                                            <input type="radio" name="radio" id="radio-3" value= "Autre demande" class="radioAbon"> 
                                            Autre demande
                                            <span></span>
                                        </label>
                                    </div>
                                </div>   
                        </div>
                    </fieldset>
                    <hr style=" height: 12px;width: 827px;margin-left: -35px;border: 0;box-shadow: inset 0 12px 12px -12px rgba(0, 0, 0, 0.5);">
                    <div style="box-shadow: 0px 22px 17px -20px rgba(0,0,0,0.2)" > </div>
                </div>
                <div class="row"  >
                    <span style="background-image: url('download.png');background-position:0 58px;margin: 0 62px;height: 29px;width: 855px;padding-bottom: 43px;">
                    </span><br>
                    
                    <h2 class="font_2" style="font-size:25px; line-height:normal; text-align:center;"><span style="font-size:25px;"><span style="letter-spacing:0em;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;"><span style="color:#221133;">Les coordonnées de votre établissement</span></span></span></span></span></h2><br><br>
                    
                  
                    <div class="col-md-2">
                        <br>
                    </div>
<!--                     <div>
                        <input type="hidden" name="IdCommercant" id="IdCommercant" value="<?php //echo $objCommercant->IdCommercant;  ?>"/>
                    </div> -->
                    <div class="col-md-4" id="cible">
                        <select name="RubriqueSociete" id="RubriqueSociete" class="form-control" style="height: 35px;">
                            <option value="Catégorie"></option>
                            <?php if (sizeof($colRubriques)) { ?>
                                <?php foreach ($colRubriques as $objRubrique) { ?>
                                    <option value="<?php echo $objRubrique->IdRubrique; ?>"                               <?php if (isset($assComRubrique[0]->IdRubrique) && $assComRubrique[0]->IdRubrique == $objRubrique->IdRubrique) {
                                            echo "selected='selected'";
                                    }else; ?>
                                    >
                                        <?php echo $objRubrique->Nom;?>


                                    </option>
                                <?php } ?>
                            <?php } ?>
                        </select>     
                        <div id='trReponseRub' style="height:auto;" >
                        </div>
                    </div>
                    <div class="col-md-4" id="cible">
                        <select class="form-control" name="SousRubriqueSociete" id="SousRubriqueSociete" style="height: 35px;">
                                    <option value="<?php echo $objSousRubrique->IdSousRubrique; ?>">-- Veuillez choisir --</option>

                                    <?php if (sizeof($colSousRubriques)) { ?>
                                        <?php foreach ($colSousRubriques as $objSousRubrique) { ?>

                                            <option value="<?php echo $objRubrique->IdRubrique; ?>"
                                            <?php if (isset($assComSousRubrique[0]->IdSousRubrique) && $assComSousRubrique[0]->IdSousRubrique == $objSousRubrique->IdSousRubrique) {
                                                    echo "selected='selected'";
                                            }; ?>
                                            >
                                                <?php echo $objSousRubrique->Nom;?>


                                            </option>                                                                                    
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div><br>
                <div class="row" >
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control NomSociete" name="inputZip1" id="inputZip1"  placeholder="Nom ou enseigne *"  value="<?php if(isset($objCommercant->IdCommercant)){ echo $objCommercant->NomSociete;} ?>">
                    </div>
                    <div class="col-md-4" id="cible">
                        <!-- <input type="text" class="form-control" name="Autre" id="inputZip"  placeholder="Votre activité*"> -->
                        <select id="status" name="status" class="form-control" style="height: 35px;">
                            <option value="">votre status</option>
                            <?php if (sizeof($colStatut)) { ?>
                                <?php foreach ($colStatut as $objStatut) { ?>
                                    <option 

                                    <?php if (isset($objCommercant->idstatut) && $objCommercant->idstatut == $objStatut->id){?>selected="selected" <? } ?>

                                    value="<?php echo $objStatut->id; ?>"><?php echo htmlspecialchars(stripcslashes($objStatut->Nom)); ?>
                                    </option>
                                <?php } ?>
                            <?php } ?>
                        </select>                        
                    </div>
                    <div class="col-md-1">
                    </div>
                </div><br>
                <div class="row" >
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-4">

                        <input type="text" class="form-control" name="Adresse1Societe" id="Adresse1Societe"  placeholder="Adresse*" value="<?php if(isset($objCommercant->IdCommercant)){ echo $objCommercant->Adresse1;} ?>">
                        <input type="hidden" name="Societe[limit_article]" id="ar" value="5" class="" />

                            <input type="hidden" name="Societe[limit_agenda]" id="ag" value="5" class="" />

                            <input type="hidden" name="Societe[limit_annonce]" id="an" value="5" class="" />
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="CodePostalSociete" id="CodePostalSociete" onblur="javascript:CP_getDepartement();CP_getVille();"  placeholder="Code Postal *" value="<?php if(isset($objCommercant->IdCommercant)){ echo $objCommercant->CodePostal;} ?>" >
                    </div>
                    <div class="col-md-1">
                    </div>
                </div><br>
                <div class="row" >
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-4">
                        <span id="villeCP_container">
                            <input type="text" class="form-control" name="IdVille_Nom_text" id="IdVille_Nom_text"  placeholder="Ville *" disabled="disabled" value="<?php 
                             $thisss = get_instance();
                             $thisss->load->model('mdlville');
                             if (isset($objCommercant->IdVille)) {
                                $ville=$this->mdlville->getVilleById($objCommercant->IdVille);
                                echo($ville->Nom);
                             };
                            ?> ">
                            <input type="hidden" name="Societe[IdVille]"  class="form-control" id="VilleSociete" onchange="getLatitudeLongitudeAdresse();" value="<?php 
                             $thisss = get_instance();
                             $thisss->load->model('mdlville');
                             if (isset($objCommercant->IdVille)) {
                                $ville=$this->mdlville->getVilleById($objCommercant->IdVille);
                                echo($ville->Nom);
                                //var_dump($ville);
                             };
                            ?> "/>
                        </span>
                        
                    </div>
                    <div class="col-md-4" id="cible">
                        <span id="departementCP_container">
                            <select name="departement_id" onchange="javascript:CP_getVille_D_CP();" id="departement_id" class="form-control"disabled>
                                <option selected>
                                    <?php                              
                                        $thisss = get_instance();
                                        $thisss->load->model('mdldepartement');
                                        if(isset($objCommercant->departement_id)){
                                            $departement= $this->mdldepartement->getById($objCommercant->departement_id);
                                            echo($departement->departement_nom);}
                                        else{
                                            echo "-- Departement : --";
                                        }
                                    ?>
                                </option>
                                <?php 
                                if (sizeof($colDepartement)) { ?>

                                    <?php foreach ($colDepartement as $objDepartement) { ?>

                                        <option

                                                value="<?php echo $objDepartement->departement_id; ?>"><?php echo $objDepartement->departement_nom; ?></option>

                                    <?php } ?>

                                <?php } ?>
                            </select>
                        </span>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div><br>
                <div class="row" >
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="EmailSociete" id="EmailSociete"  placeholder="Email *"  value="<?php if(isset($objCommercant->IdCommercant)){ echo $objCommercant->Email;} ?>"><br>
                        <div id="divErrortxtEmail_" style="width:152px; height:20px; overflow:hidden; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;"></div>
                        <input type="hidden" name="txtEmail_verif" id="txtEmail_verif" value="0" class="input_width" tabindex="18"/>
                    </div>
                    
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="TelFixeSociete" id="TelFixeSociete"  placeholder="Téléphone direct" value="<?php if(isset($objCommercant->IdCommercant)){ echo $objCommercant->TelFixe;} ?>">
                    </div>
                    <div class="col-md-1">
                    </div>
                </div><br>
                <div class="row" >
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="SiteWebSociete]" id="SiteWebSociete"  placeholder="Site web*"  value="<?php if(isset($objCommercant->IdCommercant)){ echo $objCommercant->SiteWeb;} ?>"><br>
                    </div>
                    
                    <div class="col-md-4" id="cible">
                        <!-- <input type="text" class="form-control" name="Societe[TelFixe]" id="TelFixeSociete"  placeholder="Téléphone direct"> -->
                       <select name="nbr_employe" id="nbr_employe" class="form-control" style="height: 35px;" >
                        <!-- <option value="">Nombre d'employés</option> -->
                            <option
                                        <?php if (isset($new_fields->nb_employe) && $new_fields->nb_employe == "0")  { ?>selected="selected"<?php } ?>
                                        value="0">De 1 à 5
                            </option>
                            <option
                                        <?php if (isset($new_fields->nb_employe) && $new_fields->nb_employe == "1")  { ?>selected="selected"<?php } ?>
                                        value="1">De 6 à 20
                            </option>
                            <option
                                        <?php if (isset($new_fields->nb_employe) && $new_fields->nb_employe == "2")  { ?>selected="selected"<?php } ?>
                                        value="2">De 21 à 50
                            </option>
                            <option
                                        <?php if (isset($new_fields->nb_employe) && $new_fields->nb_employe == "3")  { ?>selected="selected"<?php } ?>
                                        value="3">Plus de 50
                            </option>
                        </select>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div><br>

                 <div class="row" >
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="siret" id="siret" placeholder="SIRET" value="<?php if($objCommercant->IdCommercant!='0'){ echo $objCommercant->Siret ;}else{echo 'Siret';} ?>">
                    </div>
                    
                    <div class="col-md-4" id="cible">
                        <!-- <input type="text" class="form-control" name="Societe[TelFixe]" id="TelFixeSociete"  placeholder="Téléphone direct"> -->
                        
                    </div>
                    <div class="col-md-1">
                    </div>
                </div><br>
                <!-- <div class="row" >
                    <div class="col-md-2">
                    
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="Societe[TelMobile]" id="TelMobileSociete"  placeholder="Téléphone mobile">
                    </div>
                    
                    <div class="col-md-1">
                    </div><br>
                    
                </div> -->


<div class="container">

    <div class="row m-4 p-2 d-flex justify-content-center">
                <h2 class="font_2" style="font-size:25px; line-height:normal; text-align:center;"><span style="font-size:25px;"><span style="letter-spacing:0em;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;"><span style="color:#221133;">Les coordonnées du décideur</span></span></span></span></span></h2><br>        
      <div class="col" id="cible">
        <select name="CiviliteResponsableSociete" id="CiviliteResponsableSociete" class="form-control" style="height:35px">
            <option
                <?php if (isset($objCommercant->Civilite) && $objCommercant->Civilite == "0") { ?>selected="selected"<?php } ?>
                value="0">Monsieur
            </option>
            <option
                <?php if (isset($objCommercant->Civilite) && $objCommercant->Civilite == "1") { ?>selected="selected"<?php } ?>
                value="1">Madame
            </option>
            <option
                <?php if (isset($objCommercant->Civilite) && $objCommercant->Civilite == "2") { ?>selected="selected"<?php } ?>
                value="2">Mademoiselle
            </option>
        </select>        
      </div>
      <div class="col">
        <input type="text" name="NomResponsableSociete" class="form-control" id="NomResponsableSociete"  placeholder="Nom responsable *" value="<?php if(isset($objCommercant->IdCommercant)){ echo $objCommercant->Nom;} ?>">
      </div>
      <div class="col">
        <input type="text" name="NomResponsableSociete" class="form-control" id="Prenom"  placeholder="Prenom responsable *" value="<?php if(isset($objCommercant->IdCommercant)){ echo $objCommercant->Prenom;} ?>">
      </div>      
    </div>
    
    
    <div class="row m-4 p-2 d-flex justify-content-center">
      <div class="col">
        <input type="text" class="form-control" name="FoncRespSociete" id="FoncRespSociete"  placeholder="Fonction responsable *" value="<?php if(isset($objCommercant->IdCommercant)){ echo $objCommercant->Responsabilite;} ?>">
      </div>        
      <div class="col">
        <input type="text" class="form-control" name="Email_decidRespSociete" id="Email_decidRespSociete"  placeholder="Email *"  value="<?php if(isset($objCommercant->IdCommercant)){ echo $objCommercant->Email_decideur;} ?>">
      </div>
      <div class="col">
        <input type="text" class="form-control" name="TelDirectRespSociete" id="TelDirectRespSociete"  placeholder="Téléphone direct *" value="<?php if(isset($objCommercant->IdCommercant)){ echo $objCommercant->TelDirect;} ?>">
      </div>
    </div>    

</div>
                 
                <!-- <div class="row" >
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="Societe[Prenom]" id="PrenomResponsableSociete"  placeholder="Prénom responsable *">
                    </div>
                    
                    <div class="col-md-1">
                    </div>
                </div><br> -->

            
<!--                 <div class="row">
                    <div class="col-md-12">
                        <div class="container" style="background: #E80EAE; width:980px;margin-left:-14px; height: 200px;">
                            <br><h2 class="font_2" style="font-size:25px; line-height:normal; text-align:center; "><span style="font-size:25px;"><span style="letter-spacing:0em;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;"><span style="color: #FFFFFF;">Identifiant et mot de passe</span></span></span></span></span></h2><br>
                            <div class="row">
                                <div class="col-md-1">
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="Societe[Login]"  id="txtLogin"  placeholder="Identifiant *">
                                    <div id="divErrortxtLogin_" style="width:152px; overflow:hidden; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;"></div>

                                    <div id="inputMontantTTC">
                                        <input type="hidden" name="montantttcvalue_abonnement" id="montantttcvalue_abonnement"/>

                                        <input type="hidden" name="txtLogin_verif" id="txtLogin_verif" value="0"/>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <input type="password" class="form-control" name="Societe_Password"  id="txtPassword"  placeholder="MDP *">
                                </div>
                                <div class="col-md-1">
                                    <div class="space_sub_pro">&nbsp;</div>
                                    <?php

                                       // $this->load->Model("Abonnement");

                                        //$obj_abonnement_gratuit = $this->Abonnement->GetWhere(" type='gratuit' ");

                                        //$obj_abonnement_premium = $this->Abonnement->GetWhere(" type='premium' ");

                                        //$obj_abonnement_platinum = $this->Abonnement->GetWhere(" type='platinum' ");



                                        //if (isset($obj_abonnement_gratuit) && $type == "basic") $value_abonnement_sub_pro = $obj_abonnement_gratuit->IdAbonnement;

                                        //else if (isset($obj_abonnement_premium) && $type == "premium") $value_abonnement_sub_pro = $obj_abonnement_premium->IdAbonnement;

                                        //else if (isset($obj_abonnement_platinum) && ($type == "platinium" || $type == "platinum")) $value_abonnement_sub_pro = $obj_abonnement_platinum->IdAbonnement;

                                        //else $value_abonnement_sub_pro = '1';

                                    ?>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col-md-4">
                                </div>           
                                <div class="col-md-6">
                                   
                                    <a href="" id="lien">Je confirme mon idenfiant et mot de passe</a>
                                </div>
                            </div>
                        </div>
                    </div>        
                </div> -->
                <p>
                    <div>
                        <h2 class="font_2" style="font-size:25px; line-height:normal; text-align:center;"><span style="font-size:25px;"><span style="letter-spacing:0em;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;"><span style="color:#221133;">Je choisis un pack un ou plusieurs outils adaptés à mon activité</span></span></span></span></span><br>
                            <span style="font-size:20px;"><span style="letter-spacing:0em;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;"><span style="color:#221133;">(L'abonnement annuel hors taxes varie suivant le nombre d'employés)</span></span></span></span></span></h2><br>
                    </div>
                    <div class="container" style="width: 85%;">
                            <div class="row">
<!--                                 <div class="col-md-1">
                                </div> -->
                                <div class="col-md-12 packs" id="packs">
                                    <input type="checkbox" name = "liste_select[]" id="pack_info" value="1" class="checkBox">
                                    <label for="1" style=" cursor: pointer; margin-left: 12px; font-size:20px; line-height:normal; text-align:center;" class="_1Avq3"> 01. Pack infos (actualité et agenda) : Quota de 50 articles &amp; 50 événements</label><br>

                                    <input type="hidden" id= "pack_article" name="liste_select[]">
                                    <input type="checkbox" name="Societe1[referencement_annonce]" id="2" value="2" class="checkBox"><label for="2" style=" cursor: pointer; margin-left: 12px; font-size:20px; line-height:normal; text-align:center;" class="_1Avq3"> 02. Les boutiques en ligne : Quota de 50 annonces</label><br>
                                    <input type="checkbox" name="Societe1[referencement_bonplan]" id="3" value="3" class="checkBox"><label for="3" style=" cursor: pointer; margin-left: 12px; font-size:20px; line-height:normal; text-align:center;" class="_1Avq3"> 03. Pack "Promos" : 5 bons plans et une condition de fidélisation</label><br>
                                    <input type="checkbox" class="checkBox" name ="liste_select[]" id="4" value="4"><label for="4" style=" cursor: pointer; margin-left: 12px; font-size:20px; line-height:normal; text-align:center;" class="_1Avq3"> 04. Pack Restaurant : Réservations de tables; plats du jour et menu digital</label><br>
                                    <input type="checkbox" class="checkBox" name ="liste_select[]" id="5" value="5"><label for="5" style=" cursor: pointer; margin-left: 12px; font-size:20px; line-height:normal; text-align:center;" class="_1Avq3"> 05. Pack "Gîtes et chambres d'hôtes" : Réservations et boutique en ligne</label><br>
                                    <input type="checkbox" class="checkBox" name ="liste_select[]" id="5" value="6"><label for="5" style=" cursor: pointer; margin-left: 12px; font-size:20px; line-height:normal; text-align:center;" class="_1Avq3"> 06. Module graphique "Platinium"</label><br>
                                    <input type="checkbox" class="checkBox" name ="liste_select[]" id="6" value="7"><label for="6" style=" cursor: pointer; margin-left:13px; font-size:20px; line-height:normal; text-align:center;margin-top: -24px;" class="_1Avq3"> 07. Module de référencement "Google : uniquement avec l'abonnement "Platinium"</label><br>
                                </div>
                            </div>
                    </div>
                </p>
                <hr style=" height: 12px;width: 827px;margin-left: 63px;border: 0;box-shadow: inset 0 12px 12px -12px rgba(0, 0, 0, 0.5);">
                <span style="background-image: url(download.png);background-position:0 58px;margin: 0 50px;height: 29px;">
                </span><br>
                <div id="comp-l0e8856r" class="_2Hij5" data-testid="richTextElement"><h2 class="font_2" style="font-size:25px; line-height:normal; text-align:center;"><span style="font-size:25px;"><span style="letter-spacing:0em;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;"><span style="color:#221133;">Les autres services optionnels</span></span></span></span></span><br>
                    <span style="font-size:20px;"><span style="letter-spacing:0em;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;"><span style="color:#221133;">Plus d'informations : nous contacter au 06.72.05.9.35</span></span></span></span></span></h2></div><br>
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="" style="margin-left: 10rem;">
                        <input type="checkbox" name = "Societe1[agenda_export]" id="p" value="1" class="checkBox_autre"><label for="p" style=" cursor: pointer; margin-left: 12px; font-size:20px; line-height:normal; text-align:center;" class="_1Avq3"> 07. Pack export (sur devis) : agenda; actualité; boutique; bons plans; pack restaurant</label><br>
                        <input type="checkbox" name="new_fields[aide_creation_page]" id="a" value="1" class="checkBox_autre"><label for="a" style=" cursor: pointer; margin-left: 12px; font-size:20px; line-height:normal; text-align:center;" class="_1Avq3"> 08. Aide à la création de vos pages (sur devis)</label><br>
                        <input type="checkbox" name="new_fields[gestion_reseaux_sociaux]" id="c" value="1" class="checkBox_autre"><label for="c" style=" cursor: pointer; margin-left: 12px; font-size:20px; line-height:normal; text-align:center;" class="_1Avq3"> 09. Création et gestion de vos réseaux sociaux</label><br>
                        <input type="checkbox" name="new_fields[realisation_video]" id="r" value="1" class="checkBox_autre"><label for="r" style=" cursor: pointer; margin-left: 12px; font-size:20px; line-height:normal; text-align:center;" class="_1Avq3"> 10. Réalisation d'une vidéo de présentation </label><br>
                        <input type="checkbox" name="new_fields[publicite_magazine_sortez]" id="pp" value="1" class="checkBox_autre"><label for="pp" style=" cursor: pointer; margin-left: 12px; font-size:20px; line-height:normal; text-align:center;" class="_1Avq3"> 11. Publicité sur le Magazine Sortez </label><br>
                        <input type="checkbox" name="new_fields[impression_toutes_forme]" id="l" value="1" class="checkBox_autre"><label for="l" style=" cursor: pointer; margin-left: 12px; font-size:20px; line-height:normal; text-align:center;" class="_1Avq3"> 12. L'impression sur toutes ses formes ! Sortez département Print </label><br>
                        <input type="checkbox" name="new_fields[vivresaville]" id="v" value="1" class="checkBox_autre"><label for="v" style=" cursor: pointer; margin-left: 12px; font-size:20px; line-height:normal; text-align:center;" class="_1Avq3"> 13. vivresaville.fr </label><br>
                    </div>
                </div><br>
                <hr style=" height: 12px;width: 827px;margin-left: 63px;border: 0;box-shadow: inset 0 12px 12px -12px rgba(0, 0, 0, 0.5);">
                <span style="background-image: url(download.png);background-position:0 58px;margin: 0 50px;height: 29px;">
                </span><br>

                <p style="font-size:17px; text-align:center;"><span style="font-size:17px;"><span style="color:hsl(316, 89%, 48%);"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;">Attention : Sortez.org peut refuser sans en donner la justification l’ouverture d’un compte</span></span></span><br>
                    <span style="font-size:17px; text-align:center;"><span style="font-size:17px; "><span style="color:#E80EAE;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif; padding-top:-57px;">qui ne correspondrait pas à son éthique. (<span style="text-decoration:underline;"><a href="https://www.sortez.org/mentions-legales.html" style="color: #E80EAE;" target="_blank" rel="noreferrer noopener">Voir nos conditions générales</a></span>).</span></span></span></span>   
                </p><br>
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-11">
                        <ol class="font_8" style="color:rgb(0, 0, 0); font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif; font-size:17px;">
                            <li>
                            <p class="font_8" style="font-size:17px;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="color:#000000;"><span style="font-size:17px;">En validant votre demande de devis, vous recevez par retour de mail une confirmation de réception ; </span></span></span></p>
                            </li>
                            <li>
                            <p class="font_8" style="font-size:17px;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="color:#000000;"><span style="font-size:17px;">sous 24h00 après étude de votre demande, nous vous adresserons le devis correspondant ;</span></span></span></p>
                            </li>
                            <li>
                            <p class="font_8" style="font-size:17px;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="color:#000000;"><span style="font-size:17px;">puis notre service vous contactera pour vous demander votre avis et vous préparer les documents conformes à votre organisation administrative (commande, facture-proforma, facture...) ;</span></span></span></p>
                            </li>
                            <li>
                            <p class="font_8" style="font-size:17px;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="color:#000000;"><span style="font-size:17px;">les abonnements annuels sont réglés en totalité sur présentation de la facture par virement bancaire ou paiement en ligne par carte bancaire ;</span></span></span></p>
                            </li>
                            <li>
                            <p class="font_8" style="font-size:17px;"><span style="font-size:17px;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="color:#000000;">dès la confirmation, vous accédez à votre compte en cliquant sur le bouton "Mon compte" qui se trouve en haut de notre page d'accueil ;</span></span></span></p>
                            </li>
                            <li>
                            <p class="font_8" style="font-size:17px;"><span style="font-size:17px;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="color:#000000;">vous confirmez votre identifiant et mot de passe et vous accédez </span></span></span><span style="font-size:17px;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="color:#000000;">sur une interface intuitive ;</span></span></span></p>
                            </li>
                            <li>
                            <p class="font_8" style="font-size:17px;"><span style="font-size:17px;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="color:#000000;">vous pouvez alors, intégrer immédiatement vos données (textes, photos, vidéo, liens, PDF, formulaires, bannière…) ;</span></span></span></p>
                            </li>
                            <li>
                            <p class="font_8" style="font-size:17px;"><span style="font-size:17px;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="color:#000000;">à tout moment, vous visualisez et perfectionnez le résultat ;</span></span></span></p>
                            </li>
                            <li>
                            <p class="font_8" style="font-size:17px;"><span style="font-size:17px;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="color:#000000;">vos données seront alors définitivement intégrées sur l’annuaire complet de sortez.org.</span></span></span></p>
                            </li>
                            </ol>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                        <input type="date" name="new_fields[calendar]" class="form-group" id="" placeholder="selctionner ">
                    </div>
                    <div class="col-md-4">
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-md-1">
                        
                    </div>
                    <div class="col-md-11">
                        
                        <input type="checkbox" id="valide"><label for="valide" style="font-size:15px; cursor:pointer; " class="_1Avq3">&nbsp .Je valide ma demande de devis</label><br>
                        <input type="checkbox" id="le"><label for="le" style="font-size:15px; line-height:normal; cursor: pointer; text-align:center;" class="_1Avq3">&nbsp .Le soussigné déclare avoir la faculté d’engager en son nom sa structure dont les coordonnées sont  précisées ci- dessus.</label><br>
                        <div id="msg"></div>
                    </div>
                </div><br><br><br>

                <div class="FieldError" id="divErrorFrmInscriptionProfessionnel" style="height: auto; color: red; font-family: arial; font-size: 12px; text-align:center;"></div>


                <div style="text-align: center; margin: auto;display: flex;justify-content: center;" id="capt" class="g-recaptcha" data-sitekey="6Lfjgm0UAAAAAOl1qieKqiWV5gZuSjjAc19jUIRg"></div>
                <br>
                <div class="row">
                                <div class="col-md-3">
                                </div>           
                                <div class="col-md-6 ml-5 ">
                                    
                                    <input id="send1" class="liens" name="envoyer" value="ADRESSEZ VOTRE DEMANDE DE DEVIS !" type="button" tabindex="26">


                                </div>
                </div>
                <div class="row d-flex justify-content-center text-center">
                <p id="msg2"> </p>                    
                <div class="col-md-6" id="response">
                    <!-- message ajax -->
                    <!-- <div class=" alert alert-info" id="msg"></div> -->
                    
                </div>
                </div>

                    </form>
                <!-- captcha -->
                
                
                
                <br><br>
                <div id="comp-l0fchyg0" class="_2Hij5" data-testid="richTextElement" style="margin-top: 60px;"><h2 class="font_2" style="font-size:25px; line-height:normal; text-align:center;"><span style="font-size:27px;"><span style="letter-spacing:0em;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;"><span style="color:#221133;">Vous désirez plus d'informations avant de nous adresser ce formulaire.</span></span></span></span></span><br>
                    
                    <span style="font-size:55px;"><span style="letter-spacing:0em;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;"><span style="color:#221133;">Contactez nous !...</span></span></span></span></span></h2></div>
                    <div id="comp-l0eeg7bc" class="_2Hij5" data-testid="richTextElement"><p class="font_8" style="font-size:60px; text-align:center;"><span style="color:#E80EAE;"><span style="font-size:60px;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;">06.72.05.59.35</span></span></span></span></p>

                        <p class="font_8" style="text-align:center; font-size:15px;"><span style="color:#E80EAE;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="font-size:25px;">PRIVICONCEPT SAS - Le Magazine Sortez</span></span></span><br>
                        <span style="color:#000000;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;">427, chemin de Vosgelade, le Mas Raoum, 06140 Vence<br>
                        WWW.MAGAZINE-SORTEZ.ORG - TÉL. 06.72.05.59.35</span></span></p>
                        
                        <p class="font_8" style="text-align:center; font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;">Siret : 820 043 693 00010 - Code NAF : 6201Z</span></span></p>
                        
                        <p class="font_8" style="text-align:center; font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span class="wixGuard">&ZeroWidthSpace;&ZeroWidthSpace;&ZeroWidthSpace;&ZeroWidthSpace;&ZeroWidthSpace;</span></span></span></p>
                        
                        <p class="font_8" style="text-align:center; font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span class="wixGuard">&ZeroWidthSpace;</span></span></span></p>
                        
                        <p class="font_8" style="text-align:center; font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span class="wixGuard">&ZeroWidthSpace;</span></span></span></p></div>
            </div>
        
<!-- <input type="checkbox" class="cb" value="PHP" /> PHP <br />  
<input type="checkbox" class="cb" value="ASP" /> ASP <br />  
<input type="checkbox" class="cb" value="JSP" /> JSP <br />  
<input type="text" class="text" value="" />

<button type="button" class="values">Submit</button> -->
    </body>
</html>