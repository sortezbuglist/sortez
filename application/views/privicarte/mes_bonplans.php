<?php $data["zTitle"] = 'Mes Bonplans' ?>


<?php $this->load->view("privicarte/includes/main_header", $data); ?>
<?php //$this->load->view("privicarte/includes/ready_annonces", $data); ?>
<?php $this->load->view("privicarte/includes/main_navbar", $data); ?>
<?php //$this->load->view("privicarte/includes/main_logo_dep_com", $data); ?>
<?php //$this->load->view("privicarte/includes/main_menu", $data); ?>
<?php //$this->load->view("privicarte/includes/main_slide", $data); ?>
<?php //$this->load->view("privicarte/includes/main_map", $data); ?>
<?php //$this->load->view("privicarte/includes/main_menu_contents", $data); ?>

<div class="container" style=" background-color:#ffffff; padding-top:60px;">
    <div class="col-lg-12 padding0" style="text-align: center; display: none;"><img src="<?php echo base_url(); ?>application/resources/sortez/images/logo.png" alt="logo"></div>
</div>


<div class="container top_bottom_15" style="background-color: #fff; padding-bottom:40px;">

    <div class="col-sm-12">
    	<link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/") ; ?>/style.css" />
		<link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/") ; ?>/blue/style.css" />
        <link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/") ; ?>/jquery-ui-1.8.4.custom.css" />
		<link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/") ; ?>/demo_table_jui.css" />
		<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/jquery.tablesorter.js"></script>
		<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/jquery.tablesorter.pager.js"></script>
        <script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/ajaxfileupload.js"></script>
		<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/jquery.ui.core.js"></script>
        <?php $this->load->view("privicarte/vwMesBonplans", $data); ?>
    </div>

</div>



<?php $this->load->view("privicarte/includes/main_footer_link", $data); ?>
<?php $this->load->view("privicarte/includes/main_footer_copyright", $data); ?>
<?php $this->load->view("privicarte/includes/main_footer", $data); ?>