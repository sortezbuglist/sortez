<?php $data["zTitle"] = 'Accueil' ?>
<?php //$this->load->view("privicarte/includes/header_mini_2_fancy", $data); ?>
<?php $this->load->view("sortez/includes/header_frontoffice_com", $data); ?>


<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery("#IdContactSortez").fancybox({
            'autoScale': false,
            'overlayOpacity': 0.8, // Set opacity to 0.8
            'overlayColor': "#000000", // Set color to Black
            'padding': 5,
            'width': 700,
            'height': 500,
            'transitionIn': 'elastic',
            'transitionOut': 'elastic',
            'type': 'iframe'
        });
    });
</script>

<style type="text/css">
    body {
        background-image: none !important;
        background-color: #ffffff !important;
    }
</style>


<?php if (isset($_SESSION['pop_view']) && $_SESSION['pop_view'] == '1') {
    $_SESSION['pop_view'] = '0';
    ?>
    <script type="text/javascript">
        //$("a.fancybox-close", window.parent.document).click();
        window.top.window.$.fancybox.close();
        //window.top.window.$.fancybox.resize();
        //window.open("<?php //echo site_url("front/utilisateur/expired_account");?>");
        window.top.window.location = "<?php echo site_url("front/utilisateur/expired_account");?>";
    </script>
<?php } ?>

<div class="container_inscription_pro" style="display: table; width: 100%">
    <?php $this->load->view("sortez/logo_global", $data); ?>


    <div class="col-lg-12" style="display:table; margin-top:40px; text-align:center;">
        <h1 style="font-weight:bold; line-height:40px;">Votre compte a expiré<br/> ou n'a pas encore été validé par un
            Administrateur !</h1>

        <img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/expired_account.png" alt=""
             style="text-align:center">
        <p><a href="<?php echo site_url('contact'); ?>" id="IdContactSortez" class="btn btn-primary">Veuillez contacter
                un Administrateur !</a></p>
    </div>

</div>


<?php //$this->load->view("front2013/includes/footer_mini_2"); ?>
<?php $this->load->view("sortez/includes/footer_frontoffice_com", $data); ?>
