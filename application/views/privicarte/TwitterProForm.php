<?php if (isset($oInfoCommercant) && $oInfoCommercant->google_plus != "" && $oInfoCommercant->google_plus != NULL) {?>
<?php 
if (preg_match('#https://#', $oInfoCommercant->google_plus) || preg_match('#http://#', $oInfoCommercant->google_plus)) {
	$link_google_plus_to_show = $oInfoCommercant->google_plus;
} else {
	$link_google_plus_to_show = "http://www.twitter.com/".$oInfoCommercant->google_plus;
}
?>

<a class="twitter-timeline" href="<?php echo $link_google_plus_to_show; ?>" data-widget-id="681829190988099584">Tweets de @<?php echo $oInfoCommercant->NomSociete;?></a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

<?php } else { 
	echo "<div style='padding: 25px; color: red; font-family: arial;'><strong>Le lien Twitter n'est pas valide !</strong></div>";
 } ?>