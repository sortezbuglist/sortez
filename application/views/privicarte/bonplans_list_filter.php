<script type="text/javascript">
function check_zMotCle_bonplan_focus() {
    var searchvalue = $("#inputString_zMotCle").val();
	if (searchvalue=="RECHERCHER") $("#inputString_zMotCle").val("");
	//alert("focus");
}	
function check_zMotCle_bonplan_blur() {	
	var searchvalue = $("#inputString_zMotCle").val();
	if (searchvalue=="") $("#inputString_zMotCle").val("RECHERCHER");
	//alert("blur");
}  
</script>


<div class="col-sm-4 paddingleft0">   
    <?php 
	$this_session =& get_instance();
	$this_session->load->library('session');
	$session_inputStringQuandHidden_verification = $this_session->session->userdata('inputStringQuandHidden_x');
	$session_zMotCle_verification = $this_session->session->userdata('zMotCle_x');
	?>
    
    <select id="inputStringOrderByHidden_partenaires" size="1" name="inputStringOrderByHidden_partenaires" style="<?php if (isset($iOrderBy) && $iOrderBy!='0') {} else {?>background-image:url('<?php echo GetImagePath("privicarte/"); ?>/bg_trier.png'); background-repeat:no-repeat; background-position:center center;<?php }?> padding-left: 15px; cursor:pointer;" class="input_filter_agenda">
        <option value="0"></option>
        <option value="1" <?php if (isset($iOrderBy) && $iOrderBy == "1") echo 'selected="selected"';?>>Nouveaux Bons plans</option>
        <option value="2" <?php if (isset($iOrderBy) && $iOrderBy == "2") echo 'selected="selected"';?>>Les plus consult&eacute;s</option>
        <option value="3" <?php if (isset($iOrderBy) && $iOrderBy == "3") echo 'selected="selected"';?>>Bientôts termin&eacute;s</option>
    </select>
    
    
    
</div>

<div class="col-sm-4 paddingleft0">
	<select class="input_filter_agenda" id="inputStringWhereMultiple_partenaires" name="inputStringWhereMultiple_partenaires" style="<?php if (isset($iWhereMultiple) && $iWhereMultiple!='') {} else { ?>background-image:url('<?php echo GetImagePath("privicarte/"); ?>/bg_bonplan.png'); background-repeat:no-repeat; background-position:center center;<?php }?> padding-left: 15px; cursor:pointer;">
        <option value=""></option>
        <option value="0" <?php if (isset($iWhereMultiple) && $iWhereMultiple == "0") echo 'selected="selected"';?>>Les offres uniques</option>
        <option value="1" <?php if (isset($iWhereMultiple) && $iWhereMultiple == "1") echo 'selected="selected"';?>>Les offres multiples</option>
    </select>
</div>
    
<div class="col-sm-4 paddingleft0">    
	<input type="text" value="<?php if (isset($zMotCle) && $zMotCle!="") echo $zMotCle; else echo 'RECHERCHER';?>" id="inputString_zMotCle" name="inputString_zMotCle" class="input_filter_agenda" style="width:80%; text-align:center;" onfocus="javascript:check_zMotCle_bonplan_focus();" onblur="javascript:check_zMotCle_bonplan_blur();"/><button name="zMotCle_to_check_btn" id="inputString_zMotCle_submit"  class="button_filter_agenda_ok"><img src="<?php echo GetImagePath("privicarte/"); ?>/ok_rechercer.png" alt="rechercher"/></button>
    
    
</div>