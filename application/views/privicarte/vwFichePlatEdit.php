<html>
<head>
    <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link  href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.min.js"></script>
    <style type="text/css">
        .btnmenu{
            text-align: center;
            margin-top: 39px;
            background-color: #af4106;
            width:300px;
            padding-bottom: 10px;
            padding-top:10px;
            color:white;
            font-size:20px;


        }
        .titre{
            text-align: center;
            background-color: #af4106;
            color: white;
            height: 40px;
            line-height: 2.5;
        }
        .contenu{
            background-color: #e1e1e1;
            width: 700px;
            padding-top:10px;
            padding-bottom: 10px;
            margin:auto;
        }
        .btnvalide{
            width: 700px;
        }
        a{
            text-decoration: none;
        }
    </style>

    <script type="text/javascript">
        function deleteFile(_IdPlat, Col) {
            jQuery.ajax({
                url: '<?php echo base_url();?>front/plat_du_jour/delete_photo/' + _IdPlat + '/' + Col,
                dataType: 'html',
                type: 'POST',
                async: true,
                success: function (data) {
                    window.location.reload();
                }
            });

        }
    </script>
</head>
<body>
<div class="container">

    <div class="row justify-content-center">
            <a href="<?php echo site_url();?>"><div class="col-sm-12 btnmenu">RETOUR SITE</div></a>
    </div>
    <div class="row justify-content-center">
        <a href="<?php echo site_url('auth/login');?>"><div class="col-sm-12 btnmenu" style="margin-top:20px;!important;">RETOUR MENU</div></a>
    </div>
    <div class="row justify-content-center">
        <a href="<?php echo site_url('front/Plat_du_jour/listePlatDuJour/');?>"><div class="col-sm-12 btnmenu" style="margin-top:20px;!important;">RETOUR LISTE</div></a>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-12 titre" style="width:700px;margin:auto;!important;margin-top: 30px">LA RESERVATION DE NOS PLATS DE JOUR</div>
                <div class="contenu col-sm-12" >
                    <form action="<?php echo site_url('front/Plat_du_jour/savemodifplat'); ?>" method="POST" enctype="multipart/form-data">
                            <input class="form-control"  type="hidden" name="plat[id]" value=" <?php echo $plat->id?>">
                            <input class="form-control"  type="hidden" name="plat[IdCommercant]" value=" <?php echo $idCommercant ?? $plat->IdCommercant; ?>">
                            <input class="form-control"  type="hidden" name="plat[IdUsers_ionauth]" value=" <?php echo $IdUsers_ionauth ?? $plat->IdUsers_ionauth; ?>">
                            <table>
                                <tr>
                                    <td>date debut<td><td><input class="form-control"  type="date" name="plat[date_debut_plat]" value="<?php echo $plat->date_debut_plat ?>" ><td>
                                </tr>
                                <tr>
                                    <td>date fin<td><td><input class="form-control"  type="date" name="plat[date_fin_plat]" value="<?php echo $plat->date_fin_plat ?>"><td>
                                </tr>
                                <tr>
                                    <td>Précisez l’heure de debut de réservation<td><td><input class="form-control"  type="time" name="plat[heure_debut_reservation]" value="<?php echo $plat->heure_debut_reservation?>"><td>
                                </tr>
                                <tr>
                                    <td>Précisez l’heure de fin de réservation<td><td><input class="form-control"  type="time" name="plat[heure_fin_reservation]" value="<?php echo $plat->heure_fin_reservation ?>"><td>
                                </tr>
                                <tr>
                                    <td>Texte plat du jour<td><td><input class="form-control"  type="text" name="plat[description_plat]" value="<?php echo $plat->description_plat ?>"><td>
                                </tr>
                                <tr>
                                    <td>Prix plat du jour<td><td><input class="form-control"  type="number" name="plat[prix_plat]" value="<?php echo $plat->prix_plat?>"><td>
                                </tr>
                                <tr>
                                    <td>Ajouter une image<td>
                                    <td>
                                        <div id="PlatPhoto_container">

                                            <?php if (!empty($plat->photo)) { ?>

                                                <?php

                                                echo '<img  src="' . base_url() . 'application/resources/front/photoCommercant/imagesbank/' . $plat->IdUsers_ionauth . '/' . $plat->photo. '" width="200"/>';

                                                ?>

                                                <a href="javascript:void(0);" class="btn btn-danger"

                                                   onclick="deleteFile('<?php echo $plat->id; ?>','photo');">Supprimer</a>

                                            <?php } else { ?>

                                                <a data-fancybox data-type="iframe" data-src="<?php echo site_url("media/index/" . $plat->id . "-plat-photo"); ?>" id="ArticlePhoto3_link" class="btn btn-info w-100 fancybox.iframe">Ajouter la photo</a>

                                            <?php } ?>

                                        </div>

                                        <div id="div_error_taille_Photo"
                                             class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo3", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?>
                                        </div>
                                    <td>
                                </tr>
                                <tr>
                                    <td>Nombre de plats du jour proposésr<td><td><input class="form-control"  type="number" name="plat[nbre_plat_propose]" value="<?php echo $plat->nbre_plat_propose ?>"><td>
                                </tr>
                                <tr>
                                    <td>Actif<td><td><select name="plat[IsActif]" style="width:250px;height;padding-top:5px;padding-bottom:5px;">
                                            <option value="0" >Inactif</option>
                                            <option value="1" <?php if ($plat->IsActif==1){ echo "selected='selected'";};?> >Actif</option>

                                        </select><td>
                                </tr>
                            </table>
                </div>

            <div class="col-sm-12 contenu text-center pt-4 pb-5" >
                <button  onclick="return confirm('Valider les modifications?')"  class="btn btn-success" type="submit"> Modifier ce plat du jour</button>
            </div>

        </div>

            </div>
        </div>

        </div>



            </form>
        </div>
<input type="hidden" value="" id="PdfSociete_checker">
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {

    jQuery('#PlatPdf').bind('change', function () {
        if (this.files[0].size > 3005000) {
            alert("La taille de votre fichier PDF dépasse le 3Mb !");
        }
    });
    });
</script>
</body>
</html>