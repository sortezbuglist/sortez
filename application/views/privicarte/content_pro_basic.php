

<?php $data["zTitle"] = 'Inscription professionnel' ?>

<?php $this->load->view("sortez/includes/header_frontoffice_com", $data); ?>



    <?php $this->load->view("sortez/logo_global", $data); ?>



    <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/fields.check.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.6.4.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>application/resources/ckeditor/ckeditor.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-3.2.1.js"></script>

    <meta name="viewport" content="width=device-width" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url("application/resources/privicarte/css/global.css") ?>">

    <script type="text/javascript">
        // $(function () {
        //     $("#DateDebut").datepicker({
        //         dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
        //         dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        //         monthNames: ['Janvier', 'Fevier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'],
        //         dateFormat: 'DD, d MM yy',
        //         autoSize: true
        //     });
        //     $("#DateFin").datepicker({
        //         dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
        //         dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        //         monthNames: ['Janvier', 'Fevier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juiller', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'],
        //         dateFormat: 'DD, d MM yy',
        //         autoSize: true
        //     });
        //     $("#DateInscription").datepicker({
        //         dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
        //         dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        //         monthNames: ['Janvier', 'Fevier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juiller', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'],
        //         dateFormat: 'DD, d MM yy',
        //         autoSize: true
        //     });
        // });


        function effacerPhotoAccueil(_iCommercantId) {
            jQuery.post('<?php echo site_url("admin/commercants/effacerPhotoCommercant"); ?>',
                {iCommercantId: _iCommercantId},
                function success(data) {
                    jQuery("#divPhotoAccueil").hide();
                });
        }
        function effacerPhoto1(_iCommercantId) {
            jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto1Commercant"); ?>',
                {iCommercantId: _iCommercantId},
                function success(data) {
                       location.reload();
                });
        }
        function effacerPhoto2(_iCommercantId) {
            jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto2Commercant"); ?>',
                {iCommercantId: _iCommercantId},
                function success(data) {
                    jQuery("#divPhoto2").hide();
                });
        }
        function effacerPhoto3(_iCommercantId) {
            jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto3Commercant"); ?>',
                {iCommercantId: _iCommercantId},
                function success(data) {
                    jQuery("#divPhoto3").hide();
                });
        }
        function effacerPhoto4(_iCommercantId) {
            jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto4Commercant"); ?>',
                {iCommercantId: _iCommercantId},
                function success(data) {
                    jQuery("#divPhoto4").hide();
                });
        }
        function effacerPhoto5(_iCommercantId) {
            jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto5Commercant"); ?>',
                {iCommercantId: _iCommercantId},
                function success(data) {
                    jQuery("#divPhoto5").hide();
                });
        }
        function effacerPdf(_iCommercantId) {
            jQuery.post('<?php echo site_url("admin/commercants/effacerPdfCommercant"); ?>',
                {iCommercantId: _iCommercantId},
                function success(data) {
                    jQuery("#divPdf").hide();
                });
        }

        function deleteFile(_IdCommercant, _FileName) {

            jQuery.ajax({
                url: gCONFIG["SITE_URL"] + 'front/professionnels/delete_files/' + _IdCommercant + '/' + _FileName,
                dataType: 'html',
                type: 'POST',
                success: function (data) {
                    location.reload();
                }
            });
        }


        function listeSousRubrique() {

            var irubId = jQuery('#RubriqueSociete').val();
            jQuery.get(
                '<?php echo site_url("front/professionnels/testAjax"); ?>' + '/' + irubId,
                function (zReponse) {
                    // alert (zReponse) ;
                    jQuery('#trReponseRub').html("");
                    jQuery('#trReponseRub').html(zReponse);


                });
        }


        function CP_getDepartement() {
            //alert(jQuery('#CodePostalSociete').val());
            jQuery('#departementCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var CodePostalSociete = jQuery('#CodePostalSociete').val();
            jQuery.post(
                '<?php echo site_url("front/professionnels/departementcp"); ?>',
                {CodePostalSociete: CodePostalSociete},
                function (zReponse) {
                    jQuery('#departementCP_container').html(zReponse);
                });
        }

        function CP_getVille() {
            //alert(jQuery('#CodePostalSociete').val());
            jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var CodePostalSociete = jQuery('#CodePostalSociete').val();
            jQuery.post(
                '<?php echo site_url("front/professionnels/villecp"); ?>',
                {CodePostalSociete: CodePostalSociete},
                function (zReponse) {
                    jQuery('#villeCP_container').html(zReponse);
                });
        }

        function CP_getVille_D_CP() {
            //alert(jQuery('#CodePostalSociete').val());
            jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var CodePostalSociete = jQuery('#CodePostalSociete').val();
            var departement_id = jQuery('#departement_id').val();
            jQuery.post(
                '<?php echo site_url("front/professionnels/villecp"); ?>',
                {CodePostalSociete: CodePostalSociete, departement_id: departement_id},
                function (zReponse) {
                    jQuery('#villeCP_container').html(zReponse);
                });
        }
    </script>


    <script type="text/javascript">


        $(document).ready(function () {
            //To show sousrubrique corresponding to rubrique

            $("#RubriqueSociete").change(function () {

                $('#trReponseRub').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

                var irubId = $("#RubriqueSociete").val();

                //alert(irubId);

                $.ajax({

                    type: "GET",

                    url: "<?php echo base_url(); ?>front/professionnels/testAjax/" + irubId,

                    success: function (msg) {

                        //alert(msg);

                        var numero_reponse = msg;

                        $("#trReponseRub").html(numero_reponse);

                        //alert(numero_reponse);

                    }

                });

                //alert ("test "+$("#RubriqueSociete").val());

            });


            // $('#lien_doctolib').blur(function(){
            //    var valeur=$(this).val();
            //    if($.trim(valeur)!= ''){
            //       $('.rdv').html()
            //    }
               
            // });            


            $("#EmailSociete").blur(function () {

                //alert('cool');

                //var result_to_show = "";

                var value_result_to_show = "0";



                var txtEmail = $("#EmailSociete").val();

                //alert('<?php //echo site_url("front/professionnels/verifier_email"); ?>' + '/' + txtEmail);

                //jQuery(".EmailLoading").addClass("EmailLoading_show").removeClass("EmailLoading");

                $.post(

                    '<?php echo site_url("front/professionnels/verifier_email");?>',

                    {txtEmail_var: txtEmail},

                    function (zReponse) {

                        //alert (zReponse) ;

                        //var zReponse_html = '';

                        if (zReponse == "1") {

                            value_result_to_show = "1";

                        }





                        if (value_result_to_show == "1") {

                            result_to_show = '<span style="color:#F00; font-weight:bold;">Ce mail est d&eacute;j&agrave; utilis&eacute;</span>';

                            $('#divErrortxtEmail_').html(result_to_show);

                            $('#txtEmail_verif').val("1");

                        } else {

                            result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce mail est disponible</span>';

                            $('#divErrortxtEmail_').html(result_to_show);

                            $('#txtEmail_verif').val("0");

                        }



                    });



                $.post(

                    '<?php echo site_url("front/professionnels/verifier_email_ionauth");?>',

                    {txtEmail_var_ionauth: txtEmail},

                    function (zReponse_ionauth) {

                        //alert (zReponse) ;

                        //var zReponse_html_ionauth = '';

                        if (zReponse_ionauth == "1") {

                            value_result_to_show = "1";

                        }





                        if (value_result_to_show == "1") {

                            result_to_show = '<span style="color:#F00; font-weight:bold;">Ce mail est d&eacute;j&agrave; utilis&eacute;</span>';

                            $('#divErrortxtEmail_').html(result_to_show);

                            $('#txtEmail_verif').val("1");

                        } else {

                            result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce mail est disponible</span>';

                            $('#divErrortxtEmail_').html(result_to_show);

                            $('#txtEmail_verif').val("0");

                        }





                    });





                //jQuery(".FieldError").removeClass("FieldError");

                //jQuery(".EmailLoading").addClass("EmailLoading").removeClass("EmailLoading_show");



            });





            $("#txtLogin").blur(function () {

                //alert('cool');

                var txtLogin = $("#txtLogin").val();



                var value_result_to_show = "0";



                //alert('<?php //echo site_url("front/professionnels/verifier_login"); ?>' + '/' + txtEmail);

                //jQuery(".EmailLoading").addClass("EmailLoading_show").removeClass("EmailLoading");

                $.post(

                    '<?php echo site_url("front/professionnels/verifier_login");?>',

                    {txtLogin_var: txtLogin},

                    function (zReponse) {

                        //alert (zReponse) ;

                        var zReponse_html = '';

                        if (zReponse == "1") {

                            value_result_to_show = "1";

                        }





                        if (value_result_to_show == "1") {

                            result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';

                            $('#divErrortxtLogin_').html(result_to_show);

                            $('#txtLogin_verif').val("1");

                        } else {

                            result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';

                            $('#divErrortxtLogin_').html(result_to_show);

                            $('#txtLogin_verif').val("0");

                        }



                    });





                $.post(

                    '<?php echo site_url("front/professionnels/verifier_login_ionauth");?>',

                    {txtLogin_var_ionauth: txtLogin},

                    function (zReponse_ionauth) {

                        //alert (zReponse) ;

                        var zReponse_html_ionauth = '';

                        if (zReponse_ionauth == "1") {

                            value_result_to_show = "1";

                        }





                        if (value_result_to_show == "1") {

                            result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';

                            $('#divErrortxtLogin_').html(result_to_show);

                            $('#txtLogin_verif').val("1");

                        } else {

                            result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';

                            $('#divErrortxtLogin_').html(result_to_show);

                            $('#txtLogin_verif').val("0");

                        }



                    });





                //jQuery(".FieldError").removeClass("FieldError");

                //jQuery(".EmailLoading").addClass("EmailLoading").removeClass("EmailLoading_show");



            });





            //To show postal code automatically

            $("#VilleSociete").change(function () {

                var irubId = $("#VilleSociete").val();

                //alert(irubId);

                $.ajax({

                    type: "GET",

                    url: "<?php echo base_url(); ?>front/professionnels/getPostalCode/" + irubId,

                    success: function (msg) {

                        //alert(msg);

                        var numero_reponse = msg;

                        $("#CodePostalSociete").val(numero_reponse);

                        //alert(numero_reponse);

                    }

                });

                //alert ("test "+$("#VilleSociete").val());

            });





            var valabonnementht = 0;

            var valmoduleht = 0;





            function calcmontantht() {



                var totalmontantht = 0;

                var montanttva = 0;

                var valtva = 0.20;

                var montantttc = 0;



                var $check_abonnement_list = new Array();

                <?php foreach ($colAbonnements as $objAbonnement) { ?>

                if ($('#check_abonnement_<?php echo $objAbonnement->IdAbonnement;?>').attr('checked'))  totalmontantht += parseInt("<?php echo $objAbonnement->tarif;?>");

                <?php } ?>



                //totalmontantht = check_358_premium_value + check_358_platinium_value + check_358_agenda_plus_value + check_358_web_ref1_value + check_358_web_ref_n_value + check_358_restauration_value;

                //alert(totalmontantht);

                $("#divMontantHT").html(totalmontantht + "€");

                $("#hidemontantht").val(totalmontantht + "€");

                //calcmontanttva (parseInt(totalmontantht), valtva);

                //calcmontantttc (parseInt(totalmontantht), montanttva);

                montanttva = totalmontantht * valtva;

                $("#divMontantTVA").html(_roundNumber(montanttva, 2) + "€");

                $("#hidemontanttva").val(_roundNumber(montanttva, 2) + "€");

                montantttc = totalmontantht + montanttva;

                $("#divMontantTTC").html(montantttc + "€");

                $("#hidemontantttc").val(montantttc + "€");

                $("#montantttcvalue_abonnement").val(montantttc);

            }





            function calcmontanttva(totalmontantht, valtva) {

                montanttva = totalmontantht * valtva;

                $("#divMontantTVA").html(_roundNumber(montanttva, 2) + "€");

            }



            //limit decimal

            function _roundNumber(num, dec) {



                return (parseFloat(num)).toFixed(dec);

            }



            function calcmontantttc(totalmontantht, montanttva) {

                montantttc = totalmontantht + montanttva;

                $("#divMontantTTC").html(montantttc + "€");

                $("#montantttcvalue_abonnement").val(montantttc);

            }









        })

    </script>



    <script type="text/javascript">var blankSrc = "wpscripts/blank.gif";

    </script>

<script type="text/javascript">
    
    jQuery(document).ready(function() {
        jQuery("#btnSinscrire").click(function() {
            var cap = document.getElementById("g-recaptcha-response").value;
            test(cap); //21103 //toFirstIdDatatourisme
        });
    });
    function test(cap) {
        jQuery.ajax({
            type: "POST",
            url: "<?php //echo site_url("front/professionnels/test_captcha"); ?>",
            data: 'g-recaptcha-response=' + (cap),
            dataType: "json",
            success: function(data) {
                var txtError = false;
                var RubriqueSociete = $('#RubriqueSociete').val();
                if (RubriqueSociete === "") {
                    txtError += "- Vous devez préciser votre activité<br/>";
                }
                var SousRubriqueSociete = $('#SousRubriqueSociete').val();
                if (SousRubriqueSociete == "0") {
                    txtError += "- Vous devez préciser une sous-rubrique<br/>";
                }
                var NomSociete = $('#NomSociete').val();
                if (NomSociete == "") {
                    txtError += "- Vous devez préciser le Nom ou enseigne<br/>";
                }
                var ivilleId = $('#VilleSociete').val();
                if (ivilleId == 0) {
                    txtError += "- Vous devez sélectionner une ville<br/>";
                }
                var CodePostalSociete = $('#CodePostalSociete').val();
                if (CodePostalSociete == "") {
                    txtError += "- Vous devez préciser le code postal<br/>";
                }
                var EmailSociete = $("#EmailSociete").val();
                if (!isEmail(EmailSociete)) {
                    txtError += "- Veuillez indiquer un email valide.<br/>";
                }
                var txtEmail_verif = $("#txtEmail_verif").val();
                if (txtEmail_verif == 1) {
                    txtError += "- Votre Email existe déjà sur notre site <br/>";
                }
                var NomResponsableSociete = $('#NomResponsableSociete').val();
                if (NomResponsableSociete == "") {
                    txtError += "- Vous devez préciser le Nom du Responsable <br/>";
                }
                var PrenomResponsableSociete = $('#PrenomResponsableSociete').val();
                if (PrenomResponsableSociete == "") {
                    txtError += "- Vous devez préciser le Prenom du Responsable <br/>";
                }
                var ResponsabiliteResponsableSociete = $('#ResponsabiliteResponsableSociete').val();
                if (ResponsabiliteResponsableSociete == "") {
                    txtError += "- Vous devez préciser la responsabilité du Decideur <br/>";
                }
                var TelDirectResponsableSociete = $('#TelDirectResponsableSociete').val();
                if (TelDirectResponsableSociete == "") {
                    txtError += "- Vous devez préciser le numero de téléphone du Responsable <br/>";
                }
                var Email_decideurResponsableSociete = $("#Email_decideurResponsableSociete").val();
                if (!isEmail(Email_decideurResponsableSociete)) {
                    txtError += "- Veuillez indiquer un email valide pour le Responsable.<br/>";
                }
                var AbonnementSociete = $('#AbonnementSociete').val();
                if (AbonnementSociete == "0") {
                    txtError += "- Vous devez choisir votre abonnement<br/>";
                }
                var activite1Societe = $('#activite1Societe').val();
                if (activite1Societe == "") {
                    txtError += "- Vous devez décrire votre activité<br/>";
                }
                // var txtLogin = $("#txtLogin").val();
                // if (!isEmail(txtLogin)) {
                //     txtError += "- Votre login doit &ecirc;tre un email valide.<br/>";
                // }
                //  if ($('#validationdata').is(":checked")) {
                // } else {
                //     txtError += "- Vous devez valider vos données<br/>";
                // }
                // if ($('#validationabonnement').is(":checked")) {
                // } else {
                //     txtError += "- Vous devez valider les conditions générales<br/>";
                // }
                if (txtError == false && data.captcha == "OK") {
                    $("#frmInscriptionProfessionnel").submit();
                }
                if (data.captcha == "NO") {
                    alert("captha non valide");
                }
                if (txtError != false) {
                    $("#divErrorFrmInscriptionProfessionnel").html(txtError);
                }
            },
            error: function(data) {
                alert("data");
            }
        });
    }
</script>


    <style type="text/css">
        @font-face {
        font-family: "futura";
        src: url("<?php echo base_url(); ?>assets/fonts/FuturaLT.eot") format("eot"),
            url("<?php echo base_url(); ?>assets/fonts/FuturaLT.woff") format("woff"),
            url("<?php echo base_url(); ?>assets/fonts/FuturaLT.ttf") format("truetype"),
            url("<?php echo base_url(); ?>assets/fonts/FuturaLT.svg") format("svg");
    }

    @font-face {
        font-family: "LibreBaskerville-italic";
        src: url("<?php echo base_url(); ?>assets/fonts/Libre_Baskerville_Italic/LibreBaskerville-Italic.eot") format("eot"),
            url("<?php echo base_url(); ?>assets/fonts/Libre_Baskerville_Italic/LibreBaskerville-Italic.woff") format("woff"),
            url("<?php echo base_url(); ?>assets/fonts/Libre_Baskerville_Italic/LibreBaskerville-Italic.ttf") format("truetype"),
            url("<?php echo base_url(); ?>assets/fonts/Libre_Baskerville_Italic/LibreBaskerville-Italic.svg") format("svg");
    }

    div,
    p,
    a,
    input,
    button {
        font-family: 'futura' !important;
    }


        body {

            background-repeat: no-repeat;

            background-position: center center;

            background-size: 100% 100%;

        }


        .form-control {
            font-size: 14px !important;
            border: 2px solid #E80EAE;
        }


        .Normal-C-C1 {

            color: #000054;

            font-family: "Vladimir Script", cursive;

            font-size: 48px;

            line-height: 47px;

        }



        .contect_all_data_pro_subscription {

            margin-left: 15px;

            margin-right: 15px;

            /*margin-top:50px;*/

        }



        .title_sub_pro {

            color: #FFFFFF;

            font-family: "Arial", sans-serif;

            font-size: 15px;

            padding-bottom: 5px;

            padding-top: 5px;

            font-weight: 700;

            line-height: 1.19em;

            text-align: center;

            margin-top: 15px;

            margin-bottom: 15px;

            background-color: #000000;

            min-height: 27px;

        }



        .bloc_sub_pro {

            /*background-color:#3653A2;*/

            padding-bottom: 0px;

            padding-top: 5px;

        }



        .space_sub_pro {

            height: 20px;

        }



        .table_sub_pro {

            color: #000000;

            font-family: "Arial", sans-serif;

            font-size: 13px;

            line-height: 1.23em;

        }



        .table_sub_pro tr {

            height: 35px;

        }



        .table_sub_pro_abonnement td {

            border: 2px solid #000000;

        }



        .input_width {

            width: 100%;

        }



        .td_color_1 {

            background-color: #F4F4F4;

        }



        .td_color_2 {

            background-color: #E5E5E5;

        }



        .td_color_3 {

            background-color: #B6B6B6;

        }



        .container_inscription_pro {

            background-color: #f8f8f8;

            display: table;

            margin-top: 0 !important;
             width: 100%; 

        }



      
        label{
            color: #fff !important;
            height: 47px;
            
            background-color: #E80EAE;
            position: absolute;
            margin: auto;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            font-size: 13px;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        
        /* Responsive Mobile*/
        @media screen and (max-width: 414px) {
            .container{
                flex-direction: column; 
            }
            header{
                padding-top: 0px !important;
            }
            .col-md-3{
                margin-left: 20%;
                margin-right: 25%;
                margin-top: 1%;
                font-size: 15px;
            }
            #img_comp-l0qv1fg0{
                margin-left: -1rem !important;
                width: 300px !important;
            }
            .bloc_sub_pro{
                padding-top:10px;
            }
            .col-lg-12, .font_2, .font{
                font-size: 25px !important;
                margin-top: 7%;
            }
            .font_2{
                margin-top: -20% !important;
            }
            #texte{
                padding-top: 20px !important;
            }
            #img-three-blocs{
                margin-left: -19% !important;
            }
            #img{
                margin-left: -1rem !important;
            }
            .image{
                margin-left: 90px !important;
                width: 210px !important;
                height: 110px !important;
                margin-top: -20%;
            }
            .fontH2{
                margin-top: -20%;
            }
            ._2Hij5{
                margin-top: 25%;
            }
            #logout{
                width: 40%;
            }
        }
        

    </style>


    <div class="container" style="max-width: 980px">
         <header style="padding-top: 32px;">

        <div class="container">
            <nav style="text-align: center;">
                <ul style="list-style-type: none;margin: 0;padding: 0;display: inline-block;">
                    <div class="row" style="/*margin-right: -216px;">
                        <div class="col-md-3">
                            <li style="display: inline;justify-content: center;"><a href="https://www.sortez.org/" style="text-decoration: none;padding-top: 23px;">
                                    <div style="    padding-top: 7px; background-color: #E80EAE;border-radius: 21px; width: 180px; height: 40px;"><span style="/*margin-left: 26px;*/ color: #fff;font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;">Accueil Sortez</span></div>
                                </a> </li>
                        </div>
                        <div class="col-md-3">
                            <li style="display: inline;justify-content: center;"><a href="https://www.soutenonslecommercelocal.fr/abonnement-premium" style="text-decoration: none;">
                                    <div style="padding-top: 7px;background-color: #E80EAE;border-radius: 21px; width: 180px; height: 40px;"><span style="/*margin-left: 26px;*/ color: #fff;font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;">Détails Premium</span></div>
                                </a> </li>
                        </div>
                        <div class="col-md-3">
                            <li style="display: inline;justify-content: center;"><a href="https://www.soutenonslecommercelocal.fr/abonnement-platinium" style="text-decoration: none;">
                                    <div style="padding-top: 7px;background-color: #E80EAE;border-radius: 21px; width: 180px; height: 40px;"><span style="/*margin-left: 26px;*/ color: #fff;font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;">Détails Platinium</span></div>
                                </a> </li>
                        </div>
                        <div class="col-md-3">
                            <li style="display: inline;justify-content: center;"><a href="https://www.soutenonslecommercelocal.fr/modules-packs-optionnels" style="text-decoration: none;">
                                    <div style="padding-top: 7px;background-color: #E80EAE;border-radius: 21px; width: 180px; height: 40px;"><span style="/*margin-left: 26px; */color: #fff;font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;">Détails marketplace</span></div>
                                </a> </li>
                        </div>
                    </div>
                </ul>
            </nav>
        </div>
    </header>


        <div class="row mt-5 mb-5" style="    height: 115px;">

            <div class="col-lg-12" id="comp-k9d3lo0t" class="_2Hij5" data-testid="richTextElement"><h2 class="font_2" style="font-size:40px; line-height:normal; text-align:center;"><span style="font-size:40px;"><span style="letter-spacing:0em;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;"><span class="font" style="color:#221133;">Modification<br>de votre Abonnement Basique&nbsp;</span></span></span></span></span><br><br>&nbsp;</h2>
            </div>
        </div>
        <div id="img" class="row mt-5 mb-5" style="margin-left: 29rem;">
            <div id="comp-l0qv1fg0" class="XUUsC" title=""><div data-testid="linkElement" class="xQ_iF"><wix-image id="img_comp-l0qv1fg0" class="_1-6YJ _1Fe8-" data-image-info="{&quot;containerId&quot;:&quot;comp-l0qv1fg0&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:661,&quot;height&quot;:336,&quot;uri&quot;:&quot;5fa146_33138aadb64d479284c21beb68653cbd~mv2.png&quot;,&quot;name&quot;:&quot;basique ordi.png&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}" data-bg-effect-name="" data-is-svg="false" data-is-svg-mask="false" data-image-zoomed="" data-has-ssr-src="" data-src="https://static.wixstatic.com/media/5fa146_33138aadb64d479284c21beb68653cbd~mv2.png/v1/fill/w_472,h_240,al_c,usm_0.66_1.00_0.01,enc_auto/basique%20ordi.png"><img class="image" src="https://static.wixstatic.com/media/5fa146_33138aadb64d479284c21beb68653cbd~mv2.png/v1/fill/w_472,h_240,al_c,usm_0.66_1.00_0.01,enc_auto/basique%20ordi.png" alt="basique ordi.png" style="width:398px;height:202px;object-fit:cover;object-position:50% 50%"></wix-image></div></div>
        </div>
        

    <div class="container_inscription_pro">

        <div id="contect_all_data_pro_subscription" class="contect_all_data_pro_subscription">





        <form name="frmInscriptionProfessionnel" id="frmInscriptionProfessionnel"

              action="<?php echo site_url("front/professionnels/modifier"); ?>" method="POST" accept-charset="UTF-8"

              target="_self" enctype="multipart/form-data" style="margin:0px;">



            <div class="bloc_sub_pro" style=" /*box-shadow: 0 12px 2px -2px black;">

                <!-- <div class="title_sub_pro">Votre activité</div> -->
                <div id="comp-l0efuohk" class="_2Hij5" data-testid="richTextElement"><h2 class="font_2" style="font-size:25px; line-height:normal; text-align:center;"><span style="font-size:25px;"><span style="letter-spacing:0em;"><span style="font-style:italic;"><span class="fontH2" style="font-family:libre baskerville,serif;"><span style="color:#221133;">Les coordonnées de votre établissement</span></span></span></span></span></h2></div>

                <div class="table_sub_pro">
                <!-- debut formulaire basic -->
                <div class="col-md-1">
                    </div>
                </div><br>
                <div class="row" >
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-4">
                        <select name="AssCommercantRubrique[IdRubrique]" id="RubriqueSociete" class="form-control" style="height: 35px;">
                            <option value="Catégorie"></option>
                            <?php if (sizeof($colRubriques)) { ?>
                                <?php foreach ($colRubriques as $objRubrique) { ?>
                                    <option
                                                <?php if (isset($objAssCommercantRubrique[0]->IdRubrique) && $objAssCommercantRubrique[0]->IdRubrique == $objRubrique->IdRubrique) { ?>selected="selected"<?php } ?>
                                                value="<?php echo $objRubrique->IdRubrique; ?>"><?php echo $objRubrique->Nom; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-4" id="cible">
                       <div id='trReponseRub'  style="height:auto;">
                        <select class="form-control" name="AssCommercantSousRubrique[IdSousRubrique]"id="SousRubriqueSociete" style="height: 35px;">
                                    <option value="0">-- Veuillez choisir --</option>
                                    <?php if (sizeof($colSousRubriques)) { ?>
                                        <?php foreach ($colSousRubriques as $objSousRubrique) { ?>
                                            <option
                                                <?php if (isset($objAssCommercantSousRubrique[0]->IdSousRubrique) && $objAssCommercantSousRubrique[0]->IdSousRubrique == $objSousRubrique->IdSousRubrique) { ?>selected="selected"<?php } ?>
                                                value="<?php echo $objSousRubrique->IdSousRubrique; ?>"><?php echo $objSousRubrique->Nom; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                    </div>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-4">
                        <select id="status" name="Societe[idstatut]" class="form-control" style="height: 35px;">
                            <option value="">votre status</option>
                            <?php if (sizeof($colStatut)) { ?>
                                <?php foreach ($colStatut as $objStatut) { ?>
                                    <option 

                                    <?php if (isset($objCommercant->idstatut) && $objCommercant->idstatut == $objStatut->id){?>selected="selected" <? } ?>

                                    value="<?php echo $objStatut->id; ?>"><?php echo htmlspecialchars(stripcslashes($objStatut->Nom)); ?>
                                    </option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        <!-- <input type="text" class="form-control" name="votre-activité*" id="inputZip"  placeholder="votre-activité*"> -->
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="Societe[NomSociete]" id="inputZip"  placeholder="Nom ou enseigne *" value="<?php if (isset($objCommercant->NomSociete)) echo htmlspecialchars($objCommercant->NomSociete); ?>">
                        <!-- <input type="text" class="form-control" name="Societe[Adresse*]" id="inputZip"  placeholder="Adresse1*"> -->
                    </div>
                    <div class="col-md-1">
                    </div>
                </div><br>
                <div class="row" >
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="Societe[Adresse1]" id="inputZip" value="<? echo $objCommercant->Adresse1?>">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="Societe[Adresse2]" id="inputZip"  placeholder="Adresse*">
                    </div>
                    <div class="col-md-1">
                    </div>
                </div><br>
                <div class="row" >
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="Societe[CodePostal]" id="CodePostalSociete"  placeholder="Code Postal *"value="<?php if (isset($objCommercant->CodePostal)) echo htmlspecialchars($objCommercant->CodePostal); ?>" onblur="javascript:CP_getDepartement();CP_getVille();">
                    </div>
                    <div class="col-md-4" id="villeCP_container">
                        <input type="text" class="form-control" name="IdVille_Nom_text" id="inputZip"  placeholder="Ville *" disabled="disabled" value="<?php if (isset($objCommercant->IdVille)) echo $this->mdlville->getVilleById($objCommercant->IdVille)->Nom; ?>">
                        <input type="hidden"
                                   value="<?php if (isset($objCommercant->IdVille)) echo $objCommercant->IdVille; ?>"
                                   name="Societe[IdVille]" id="VilleSociete"/>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div><br>
                <div class="row" >
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-4">
                        <span id="departementCP_container">
                            <select name="Societe[departement_id]" id="departement_id" disabled="disabled" class="form-control" onchange="javascript:CP_getVille_D_CP();" style="height: 35px;">
                                  <option value="<?php //if (isset($objCommercant->departement_id)) 

                                // {
                                //     $get = $this->mdl_departement->getById($objCommercant->departement_id); 
                                //     echo $get;
                                // }
                                //  var_dump($objCommercant->departement_id);
                                ?>">Département</option>
                                <?php if (sizeof($colDepartement)) { ?>
                                    <?php foreach ($colDepartement as $objDepartement) { ?>
                                        <option 
                                         <?php if (isset($objCommercant->departement_id) && $objDepartement->departement_id == $objCommercant->departement_id) { ?>selected="selected"<?php } ?>

                                            value="<?php echo $objDepartement->departement_id; ?>"><?php echo $objDepartement->departement_nom; ?></option>

                                    <?php } ?>
                                <?php } ?>
                            </select>
                            </span>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="Societe[TelFixe]" id="inputZip"  placeholder="Téléphone" value="<?php if (isset($objCommercant->TelFixe)) echo htmlspecialchars($objCommercant->TelFixe); ?>">
                    </div>
                    <div class="col-md-1">
                    </div>
                </div><br>
                <div class="row" >
                    <div class="col-md-2">
                    
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="Societe[Email]" id="EmailSociete"  value="<? echo $objCommercant->Email?>">
                    </div>
                     <div class="col-md-4" id="SiteWebSociete">

                        <input type="text" class="form-control" name="Societe[SiteWeb]" id="SiteWebSociete"  placeholder="Site web*" value="<?php if (isset($objCommercant->SiteWeb)) echo htmlspecialchars($objCommercant->SiteWeb); ?>">
                    </div>
                    <div class="col-md-1"> </div>
                </div><br>
                <div class="row" >
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="Societe[Siret]" id="inputZip"  value="<?php if (isset($objCommercant->Siret)) echo htmlspecialchars($objCommercant->Siret); ?>" placeholder="SIRET">
                    </div>
                    <div class="col-md-4">
                       <select name="new_field[nb_employe]" id="inputState" class="form-control" style="height: 35px;" >
                        <option value="">Nombre d'employés</option>
                            <option
                                        <?php if (isset($new_fields->nb_employe) && $new_fields->nb_employe == "0")  { ?>selected="selected"<?php } ?>
                                        value="0">De 1 à 5
                            </option>
                            <option
                                        <?php if (isset($new_fields->nb_employe) && $new_fields->nb_employe == "1")  { ?>selected="selected"<?php } ?>
                                        value="1">De 6 à 20
                            </option>
                            <option
                                        <?php if (isset($new_fields->nb_employe) && $new_fields->nb_employe == "2")  { ?>selected="selected"<?php } ?>
                                        value="2">De 21 à 50
                            </option>
                            <option
                                        <?php if (isset($new_fields->nb_employe) && $new_fields->nb_employe == "3")  { ?>selected="selected"<?php } ?>
                                        value="3">Plus de 50
                            </option>
                        </select>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div><br><?//var_dump($new_fields->nb_employe)?>

            </div>

<div style="">
        <div class="_19Et-" >
            
        </div>
</div>
             <div class="bloc_sub_pro">

                <!-- <div class="title_sub_pro">Les coordonnées du décideur</div> -->
                <!-- titre cordonnées de votre correspondant -->
                <div id="comp-l0efuohk" class="_2Hij5" data-testid="richTextElement"><h2 class="font_2" style="font-size:25px; line-height:normal; text-align:center;"><span style="font-size:25px;"><span style="letter-spacing:0em;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;"><span style="color:#221133;">Les coordonnées de votre correspondant</span></span></span></span></span></h2>
                </div>
                <!-- fin titre cordonnées de votre correspondant -->

                <div class="table_sub_pro">
                    <!-- fomulaire de coordonnée de votre correspondant -->
                    <div class="col-md-1">
                    </div>
                </div><br>
                <div class="row" >
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-4">
                         <select name="Societe[Civilite]" id="CiviliteResponsableSociete" class="form-control" style="height:35px">
                                    <option
                                        <?php if (isset($objCommercant->Civilite) && $objCommercant->Civilite == "0") { ?>selected="selected"<?php } ?>
                                        value="0">Monsieur
                                    </option>
                                    <option
                                        <?php if (isset($objCommercant->Civilite) && $objCommercant->Civilite == "1") { ?>selected="selected"<?php } ?>
                                        value="1">Madame
                                    </option>
                                    <option
                                        <?php if (isset($objCommercant->Civilite) && $objCommercant->Civilite == "2") { ?>selected="selected"<?php } ?>
                                        value="2">Mademoiselle
                                    </option>
                                </select>
                    </div>
                    <div class="col-md-4" id="cible">
                        <input type="text" class="form-control" name="Societe[Nom]" id="NomResponsableSociete"  placeholder="Nom responsable *" value="<?php if (isset($objCommercant->Nom)) echo htmlspecialchars($objCommercant->Nom); ?>">
                    </div>
                    <div class="col-md-1">
                    </div>
                </div><br>
                <div class="row" >
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="Societe[Prenom]" id="PrenomResponsableSociete"  placeholder="Prénom responsable*" value="<?php if (isset($objCommercant->Prenom)) echo htmlspecialchars($objCommercant->Prenom); ?>">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="Societe[TelMobile]" id="inputZip"  placeholder="Téléphone mobile*" value="<?php if (isset($objCommercant->TelDirect)) echo htmlspecialchars($objCommercant->TelDirect); ?>">
                    </div>
                    <div class="col-md-1">
                    </div>
                </div><br>
                <div class="row" >
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="Societe[Email_decideur]" id="Email_decideurResponsableSociete"  placeholder="Courriel *" value="<?php if (isset($objCommercant->Email_decideur)) echo htmlspecialchars($objCommercant->Email_decideur); ?>">
                        <div class="FieldError" id="divErrorEmailSociete"></div>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="Societe[Responsabilite]*" id="inputZip"  placeholder="fonction du responsable*" value="<?php if (isset($objCommercant->Responsabilite)) echo htmlspecialchars($objCommercant->Responsabilite); ?>">
                    </div>
                    <div class="col-md-1">
                    </div>
                </div><br>
                <div style="">
                    <div class="_19Et-"></div>
                </div>

    <div id="choix_image">
                <div id="comp-l0efuohk" class="_2Hij5" data-testid="richTextElement"><h2 class="font_2" style="font-size:25px; line-height:normal; text-align:center;"><span style="font-size:25px;"><span style="letter-spacing:0em;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;"><span style="color:#221133;">Vos données</span></span></span></span></span></h2></div>
                <div class="space_sub_pro">&nbsp;</div>

                <div class="row">
                    <div class="col-lg-4"></div>

                    
                    <div class="col-lg-4" id="Menuphoto_menu_gen_container" 

                    <?php if (empty($objCommercant->Photo1) || $objCommercant->Photo1 == "") { ?>
                                            
                                      
                    onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-Photo1"); ?>", "Commercant image", "width=1045, height=675,scrollbars=yes");'  <?php } ?>  ,="" href="javascript:void(0);">

                    <label for="file">importer image +</label>
                
                </div>
                    <div class="col-lg-4"></div>

                </div>
                <div><p class="import" style="padding: 22px; opacity: .7;font-size: 12px; text-align: center;">Importez un fichier pris en charge (max. 15 Mo)</p></div>
            

    </div><?  //var_dump($objCommercant)?>

        <?php if (isset($objCommercant->Photo1) AND $objCommercant->Photo1 !=null && is_file("application/resources/front/photoCommercant/imagesbank/".$objCommercant->user_ionauth_id."/".$objCommercant->Photo1)){ ?>
<div class="col-lg-4"></div>
    <div class="col-lg-4" style=" width: 410px; height: 231px;" >
        <input type="hidden" name="photo1Associe" value="<? echo $objCommercant->Photo1?>">
       <img style="max-width: 410px ; max-height: 231px;" src="<?php echo base_url()?>application/resources/front/photoCommercant/imagesbank/<?php echo $objCommercant->user_ionauth_id; echo "/" .$objCommercant->Photo1 ?>">
                    
    </div>
            

<a onclick="deleteFile(<?echo $objCommercant->IdCommercant?>, 'Photo1')" class="">
    <div class="col-lg-4">
        <div id="suprimer_img">
            <svg data-bbox="82.189 55.096 481.811 481.808" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 600 592" style="width: 18px; fill: white; height: 18px;margin-top: 22px; margin-left: 21px;">
                <g>
                    <path d="M531.936 536.904L323.094 328.063 114.253 536.904l-32.064-32.062L291.032 296 82.189 87.157l32.064-32.061 208.842 208.842L531.936 55.096 564 87.157 355.155 296 564 504.842l-32.064 32.062z">
                    </path>
                </g>
            </svg>
        </div>
    </div>
</a>
<?php } ?> 

        <div id="comp-k9dw6iep3" class="_2Hij5" data-testid="richTextElement" <?if($objCommercant->Photo1 !=null)echo 'style="margin-top: 30%!important;"'?>><h2 class="font_2" style="font-size:25px; line-height:normal; text-align:center;"><span style="font-size:25px;"><span style="letter-spacing:0em;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;"><span style="color:#221133;">Ajouter votre texte de présentation ici !</span></span></span></span></span></h2>
                <textarea id="CaracteristiquesSociete" class="_1VWbH has-custom-focus" name = "Societe[Caracteristiques]" placeholder="Ajouter votre texte de présentation ici !">
                       
                
                <? if(isset($objCommercant->Caracteristiques) && $objCommercant->Caracteristiques != null && $objCommercant->Caracteristiques != "") {echo $objCommercant->Caracteristiques;} ?>
            </textarea>
            <script>
                 CKEDITOR.config.uiColor = '#E80EAE';  
                CKEDITOR.replace('CaracteristiquesSociete');

            </script>
        </div>

        <div id="comp-k9dw6iep2" class="_2Hij5" data-testid="richTextElement"><h2 class="font_2" style="font-size:25px; line-height:normal; text-align:center;"><span style="font-size:25px;"><span style="letter-spacing:0em;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;"><span style="color:#221133;">Ajouter vos jours et heures d'ouverture</span></span></span></span></span></h2>
                    <textarea id="HorraireSociete" class="_1VWbH has-custom-focus" name="Societe[Horaires]" placeholder="Ajouter vos jours et heures d'ouverture">
                         
                          <? if(isset($objCommercant->Horaires) && $objCommercant->Horaires != null && $objCommercant->Horaires != "  ") {echo $objCommercant->Horaires;} ?>
                    </textarea>
                    <script>

                CKEDITOR.replace('HorraireSociete');

            </script>
                
        </div>
                <div class="space_sub_pro">&nbsp;</div>

                <div id="comp-l0egs3no" class="XUUsC" title="">
                    <div data-testid="linkElement" class="xQ_iF" style="text-align: center;">
                        <wix-image id="img_comp-l0egs3no" class="_1-6YJ _1Fe8-" data-image-info="{&quot;containerId&quot;:&quot;comp-l0egs3no&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:512,&quot;height&quot;:512,&quot;uri&quot;:&quot;5fa146_cedb54778af544b7ad9df3c7249939b2~mv2.png&quot;,&quot;name&quot;:&quot;4326320.png&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}" data-bg-effect-name="" data-is-svg="false" data-is-svg-mask="false" data-image-zoomed="" data-has-ssr-src="" data-src="https://static.wixstatic.com/media/5fa146_cedb54778af544b7ad9df3c7249939b2~mv2.png/v1/fill/w_198,h_198,al_c,usm_0.66_1.00_0.01,enc_auto/4326320.png">
                            <img src="https://static.wixstatic.com/media/5fa146_cedb54778af544b7ad9df3c7249939b2~mv2.png/v1/fill/w_198,h_198,al_c,usm_0.66_1.00_0.01,enc_auto/4326320.png" alt="4326320.png" style="width:198px;height:198px;object-fit:cover;object-position:50% 50%">
                        </wix-image>
                    </div>
                </div>
         <div class="row">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4"> <input type="text" id="lien_doctolib" class="form-control"  name="new_field[doctolib]" id="inputZip"  placeholder="Préciser-le lien de votre page doctolib..." value="<? echo $new_fields->doctolib ?>"></div>
                    <div class="col-lg-4"></div>


         </div>

        <div style="">
            <div class="_19Et-">
            </div>
        </div>
        <?php //$this->load->view("privicarte/txt_mentions", $data); ?>

 <!-- <div id="comp-l0efuohk" class="_2Hij5" data-testid="richTextElement"><h2 class="font_2" style="font-size:25px; line-height:normal; text-align:center;"><span style="font-size:25px;">
        <span style="letter-spacing:0em;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;"><span style="color:#221133;">Validation de vos données</span></span></span></span></span></h2>
 </div>
 -->
<!-- <div>
        <p style="font-size: 17px;">Attention : notre service peut refuser sans en donner la justification l’ouverture d’un compte qui ne correspondrait pas à son éthique. (<a href="/mentions-legales.html" target="_blank" style="font-weight:normal;"><span style="text-decoration:underline;color: black;">Voir nos conditions générales</span></a>) ;</p>
</div>


            <div class="space_sub_pro">&nbsp;</div>

            <div style="margin:15px; padding:15px; font-weight:bold;">



                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                    <tr>

                        <td valign="top" style="width: 40px;">

                            <input name="validationdata" id="validationdata"  type="checkbox" ></td>

                        <td>
                            Je confirme la validation de mes données 
                        </td>

                    </tr>
                     <tr>

                        <td valign="top" style="width: 40px;">

                            <input name="validationabonnement" id="validationabonnement" type="checkbox" ></td>

                        <td>
                            Le soussigné déclare avoir la faculté d’engager en son nom sa structure.
                        </td>

                    </tr>

                </table>

            </div> -->

       <div class="FieldError" id="divErrorFrmInscriptionProfessionnel" style="height: auto; color: red; font-family: arial; font-size: 12px; text-align:center;"></div>
        
      <!--   <div style="text-align: center;" id="capt" name="g-recaptcha-response" class="g-recaptcha" data-sitekey="6Lfjgm0UAAAAAOl1qieKqiWV5gZuSjjAc19jUIRg"></div>
        <br><br> -->
        <!-- <div class="col-lg-4"></div>
        <div class="col-lg-4">
            <input type="text" class="form-control" name="Societe[datecreation]"
                                       id="DateInscription"
                                       value="<?php// if (isset($objCommercant->datecreation)) echo convert_Sqldate_to_Frenchdate(date("Y-m-d",$objCommercant->datecreation)); ?>"/>
        </div>
        <div class="col-lg-4"></div> -->



        <div style="text-align:center; margin-bottom:40px; margin-top:60px;">

            <input id="btnSinscrire" style="width: 36%;" name="envoyer"

                   value="VALIDATION" type="submit" tabindex="26" class="btn btn-success">

        </div>


    </div>
     </form>
     <div style=" text-align: center;">
     <a href="https://www.sortez.org/connexion/sortir">
                <div id= "logout" class="btn btn-success" style="width: 36%;text-decoration: none">
                    DÉCONNEXION
                </div>
            </a>
            </div>
        <div class="" style="text-align: center;">

        <a href="<?php echo site_url($objCommercant->nom_url . "/presentation_commercants"); ?>" target="_blank" alt="show">
            <img src="<?php echo GetImagePath("privicarte/"); ?>/visualisation.png" alt="logo"
                 title="" style="width: 180px"/>
        </a>

    </div>
</div>
</div>





<div class="container" style="width: 100%; margin-top: -70rem;">
<div id="comp-l0efl1k8" class="_2Hij5" data-testid="richTextElement" style="margin-bottom: 6%;" ><h2 class="font_2" style="font-size:30px; line-height:normal; text-align:center;"><span style="font-size:30px;"><span style="letter-spacing:0em;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;"><span style="color:#221133;">Découvrez nous deux autres abonnements </span></span></span></span></span></h2>
</div>


    <div class="row" style="display: flex; margin: auto;" id="three-blocks">
        <div class="col-md-6 bloc-parent bloc-parent2">
            <div class="blocs bloc2" style="background-color: #3453A2; margin: 5px; padding: 30px;   margin-top: %;  height: 99%;text-align: center;">
                <div style="height: 280px;">
                    <span style="font-size:25px; color:#FFFFFF;" class="title-bloc">
                        <span class="title-blocs title-bloc2" style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;">L'abonnement Premium</span>
                    </span>

                        <h4 class="font_4" style="font-size:14px; line-height:normal; text-align:center; color:#FFFFFF;">
                            <span style="font-size:14px;">
                                <span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;">L'ouverture à des outils Marketings innovants</span>
                            </span>
                        </h4>
                            <img id="img-three-blocs" class="img_head" src="<?php echo site_url('/application/views/front_soutenons/premium_ordi.png'); ?>" alt="" />
                <!-- <img class="gratuit" src="</*?php echo site_url('/application/views/front_soutenons/gratuit2.webp');?>" alt="" /> -->
                </div>
                <div id="texte" class="bloc-text-in-three-blocs" style="text-align: center; color: aliceblue; margin-top:-17%;  ">
                <p><p>En souscrivant à un abonnement Premium, <br> vous bénéficiez d'une page informative<br> évolutive enrichie de textes, de liens, <br> avec l'intégration de vidéos, d'images</p> et de documents complémentaires, d'un URL</p> personnalisé (www.monentreprise.sortez.org)</p> et la fourniture d'un QRCODE</p> pour l'accès à la mobilité...</p>
                <p> Cet abonnement et la création de cette page<br> est obligatoirement lié<br> à la souscription à un ou plusieurs outils<br> Marketing innovants : e-commerce, deals,<br> de fidélité et bien plus encore... </p>
                <p>Cette page et les outils associés sont<br>multilingues et s'adaptent à tous les écrans <br>(ordinateurs, tablettes, mobiles...).</p>
                <p>Les tarifs varient suivant l'importance<br>des établissements... </p>
                </p>
                    <div style="display: flex; justify-content: center;">
                        <a href="https://www.soutenonslecommercelocal.fr/abonnement-premium" class="btngreen" style="text-align: center;   height: 100%;background-color: #EDD23B;width: 54%; padding:2%;  font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif; margin-top: 11%;color: #000 !important;">​Détails</a>
                    </div>
                    <div style="display: flex; justify-content: center;">
                    <a href="<?php echo site_url('/mon-etablissement-modele/presentation_commercants') ?>" id="btn-modele" class="btngreen" style="text-align: center;   height: 100%;background-color: #EDD23B;width: 54%; padding:2%; font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif; margin-top: 1%; color: #000 !important;"> ​Modèle</a>
                    </div>
                </div>
            </div>
<!-- <i style="font-size:125px;color:#3453A2!important" class="fa fa-arrow-down "></i> -->
        </div>
    


    <div class="col-md-6 bloc-parent bloc-paren3">
        <div class="blocs bloc3" style="background-color: #E80EAE; margin: 5px; padding: 30px;  margin-top: 0%;   height: auto;text-align: center;">
            <div style="height: 280px;">
                <span style="font-size:25px; color:#FFFFFF;" class="title-bloc">
                    <span class="title-blocs title-bloc3" style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;">L'abonnement Platinium</span>
                </span>

                <h4 class="font_4" style="font-size:14px; line-height:normal; text-align:center; color:#FFFFFF;">
                    <span style="font-size:14px;">
                        <span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;">Un site web professionnel complet</span>
                    </span>
                </h4>
                <img id="img-three-blocs" class="img_head" src="<?php echo site_url('/application/views/front_soutenons/platinium_ordi.png'); ?>" alt="" />
                <!--<img class="gratuit" src="</*?php echo site_url('/application/views/front_soutenons/gratuit2.webp');?>" alt="" /> -->
            </div>
            <div class="bloc-text-in-three-blocs" style="text-align: center; color: aliceblue; margin-top:-17%; ">
                <p>
                <p>Si vous n'avez pas encore de site web, <br> l'abonnement Platinium est fait pour vous.<br>
                <p> Il vous permet d'enrichir graphiquement votre<br> site Premium : logo, bannière, fond de page,<br> couleurs boutons et textes, présence de 2 <br>pages complémentaires.</p>
                <p>Des fonctions complémentaires sont <br>en choisissant un ou plusieurs modules<br>« livre d’or », les commentaires « Google »... <br> </p>
                <p>Vous pouvez enrichir cet abonnement<br> sera automatiquement actualisée <br>marketing innovants : e-commerce, deals,<br>fidélisation, clic & collect, réservations,<br>permet de booster votre référencement<br>sur Google et de bénéficier d'un URL<br>personnalisé (www.monentreprise.fr). </p>
                <p>Cette page et les outils sont multilingues <br>et s'adaptent à tous les écrans (ordinateurs, <br>tablettes, mobiles...</p>
                <p> Les tarifs varient suivant l'importance<br>des établissements</p>
                </p>
                <div style="display: flex; justify-content: center;">
                    <a href="https://www.soutenonslecommercelocal.fr/abonnement-platinium" class="btngreen" style="text-align: center;  height: 100%;background-color: #EDD23B;width: 54%; padding: 2%; font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;  margin-top: 1%;color: #000 !important;">​Détails</a>
                </div>
                <div style="display: flex; justify-content: center;">
                    <a href=" https://www.soutenonslecommercelocal.fr/page-platinium" id="btn-modele" class="btngreen" style="text-align: center;  height: 100%;background-color: #EDD23B;width: 54%; padding: 2%; font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif; color: #000 !important; margin-top: 1%;"> ​Modèle</a>
                </div>
            </div>
        </div>

    </div>    
    </div>
    <div>
        <div>
            <div class="text-center" style="position: relative;bottom: 20px;display: flex;flex-direction: row;justify-content: space-around;">
                <i style="font-size:125px;color:#3453A2!important" class="fa fa-arrow-down "></i>
                <i style="font-size:125px;color:#E80EAE!important" class="fa fa-arrow-down "></i>
        </div>
        </div>
    </div>
<div class="row text-center">
    <div class="col-md-12 col-sm-12">
        <h1 class="" style="font-family:libre baskerville,serif;color:#000000;font-style: italic;"><p>Je suis intéressé par l'un ou l'autre abonnement</p><p>et je désire accéder directement</p><p>à la demande de devis !...</p></h1>     
    </div>
    <div class="col-md-12 col-sm-12">

        <?php $id=$objCommercant->IdCommercant;/*echo site_url()."/front/professionnels/inscription?idC=".$id."&?abonnement=premium"*/ ?>
        <a href="<?php echo site_url("front/professionnels/BasicToPremium/").$id?>">
            
            <img src="https://static.wixstatic.com/media/5fa146_e4eb69946a1046938635aa4ed90686a4~mv2.png/v1/fill/w_411,h_145,al_c,usm_0.66_1.00_0.01,enc_auto/Demande_devis.png" class="img img-fluid">
        </a>    
    </div>
    
</div>    
    </div>
                
    </div>
</div>

<div id="comp-l0efuovr" class="_2Hij5" data-testid="richTextElement" style="margin-top: 50;"><p class="font_8" style="font-size:60px; text-align:center;"><span style="color:#E80EAE;"><span style="font-size:60px;"><span style="font-style:italic;"><span style="font-family:libre baskerville,serif;">06.72.05.59.35</span></span></span></span></p>

<p class="font_8" style="text-align:center; font-size:15px;"><span style="color:#E80EAE;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="font-size:25px;">PRIVICONCEPT SAS - Le Magazine Sortez</span></span></span><br>
<span style="color:#000000;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;">427, chemin de Vosgelade, le Mas Raoum, 06140 Vence<br>
WWW.MAGAZINE-SORTEZ.ORG - TÉL. 06.72.05.59.35</span></span></p>

<p class="font_8" style="text-align:center; font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;">Siret : 820 043 693 00010 - Code NAF : 6201Z</span></span></p>

<p class="font_8" style="text-align:center; font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span class="wixGuard">&ZeroWidthSpace;&ZeroWidthSpace;&ZeroWidthSpace;&ZeroWidthSpace;&ZeroWidthSpace;</span></span></span></p>

<p class="font_8" style="text-align:center; font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span class="wixGuard">&ZeroWidthSpace;</span></span></span></p>

<p class="font_8" style="text-align:center; font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span class="wixGuard">&ZeroWidthSpace;</span></span></span></p></div>
</div>
    

    <style type="text/css">
        ._19Et-{
            background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAAB0CAYAAAG3V00GAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAADbxJREFUeNq81MFKA0EQBNA3u+6qQQVvfoUf4f9/jBCJMZrtXHpg9LSRZAqaPgx0TXVTVfxGcVlEHVpwg1vcZ58wNu9rBy444hv7rEOJiBF3eMJz9g1mDEm2Bsck+cIO71nbEhH1t2MztJypolVTFVVVS0kVV0XB29VJIuK1B8nQg+SlB4keh++qpFwwWtr1RD38lH7ZZLTMjTnPjZUfHPCZzt9Xp894yEh5TMKpIVqDNrd22GZ9tCsa/nT/jJVWVWA5AQAA///s1ssKwjAUhOH/2KLYgvj+b1lBaihy3ExgrKt42UgXge4GMuTrxA+pfyq7s06O+u4NShquq3ZSxHwBlsjMAzACZ51RgS1BHnADLsAk6q+V+kp890En3sd9o36j/g+pjy/T8kJ9XY+DLci+UeC0R7jYwJuBErJqEPEnsbK3oJZ/iQdMon72pbhbLcd485pyRX0+AAAA///cmEsOwjAMRO2kLZ8lF+Aa3JO7FhHahAWxGI1CxSYIOZI3UdTancjzXKU+9emFoYNFf61pIzLvKTTfsUqxp9iBRAMZaOiQPPI6IsMC0wjGwywrgD9GSHigixxJGe2kTOvrmzOttSCMl2NtNC6V/12llexZHCwtpVy9FHLyUsjopZCLi0JExE0hbq5WC00ChDaMsLfXlA1zZOcvhqbm5pO8f3BaHCumTDQJR9g7wLRsz4iEM62h1dDjXhH2VpEjgYsb6i5wbq7nDH2TiCRWIhKqMJ4wmugP1MjA7yuhig3W+QkAAP//7JrRboJAEEXvLm6RP+ArfO7//1QjrtIXh16G2W1NBHRSk02I0YQrzDDnrMHDeAKDDWqj/B5jfOnq6ONZYdem38YIt0Yg048Zky8fT7dWouJtiUO4eBMVL4/zz+YRXlnxyGCwyPneDK4youggHXWrVnWsaJDjs4NoBhnI/ekgGUCW9hsKt9Vh465l1QSHsqBqAqu/FPpWD8Lfit3kdXkgvlv7LRLiEQ5eflAXG5jsf2Z/MMjJSxA38iF6CeIGdXsvQT69BIGLIJ68Vu8lSPJYI+GFR/qx9h6DlWUZ4w5wNVbwV1PjwjRGQttEjJ6wtIZxxUC1/UPB2wut2R6inHiLn1193tkXCdGoYPKdDvMd4ITl33TkV850YloofJEVuSk2H+gzokpZseZAdiSRNZH1QVdI+621dRBbFLkC5/uSYJdJB8H2vo26pSwrv4VFKZkUdsCToLN0aE37hB261IjyVgMAjN8AAAD//+xcMW7DMAyk5KVLl36gW8c8onsf0P+/o2InBTRzpIg6AeiUBow4ig0EdxZJ646OuPXaH8Nz9pWYR6QD6zcOXLszsetjOZetdbsOzrdSTTspYXzgjrfWFIdxznWGaJClyNCNWLaBc6w6wFNXziAneYu3Wk/TSWPQXlMYKN7q+oRqy1XCbyAsodnRCau8m1Ep9+DMaCeaIRysyOXdr6sQOTt+1PUsH0WIYsJuX4SkSFg6S8LnA6Fs5SUdiGTUp+IBF+2haP8kwvCBYsCssjrVlmo9661gyEXIpWDIRch3wZCr7P0sGHIR8l4w5CLkpWDIRUgrGHIR8low5CLko2DIRchXwZDrOaQISUZIhaxkhFRST0ZIlb3JkjqRr3NE1b62IP5ZtqgGwmDcG9s9GHpKodU67amHK9XwrIrhSta1VEFtZkAuFCJhwkQuUt0tjT6Rtt4AcSg8Zg6XDMgY4FiCLHVy1M2tP7W2fn2tnTYpSMCRS1Z+l8bSDZCK9Him3Erl/H9Ma2ODBHW2EctdOningXaO3zSxTkK6Ea60owS5TfRskMdEOayc9wpVk6hBdjf9ALt2mOhx921gRL4DJeI2eYbcsSKJA3nF2skZu1tlVKXzugoLnfcLAAD//+ydfY7iMAzF3cLcYY+y9z/WMk32ryLjPn8kzEhuMVJFJVCR/NrENnm/jAayAv+7Ig2vbB8Zgj519fv0mqzI2L8Ez2fEPbsA3UiVreWnZLyrk7NnQZiZ4M8mTmQZqQx0U4pEVDDK77+sXNTS3gWkt6jOWCm+1vesgqAMyQr+JgrG5lTsL+xYzR8iV76vokK/KcJ9ijcE7k1hVO2bUZc8BbkTtiHcg8cqhOKQX35+tqcFtUo6aJsgRq+k/KFjA5V+K8NOsteOOZc9LdTP0p4aPrShVoqVlWVvLnrgQTRUcQub7Gt9K03G57Vkt9dyUCGv9C0wwUfS5OwtEvTeSPduayJZDqqnYUdzSK2BY/Y/kbNlWVa6q6W1kQP+H6LdzVEr20w2dTVLm5WFNQpgF6UgRH7Xd2QuuFql3ieHt9GKnUY8hkVxmO9nxUkOA2lvdXp/XjgY5DJ9ZqpD6CLk2ysJUi7cZIKUpS2ZIGWLTiZIuXCTCVI+9UyCXAUCfyVBashKJkhN6skEqbQ3mSBVGCYTpFonyQSp5mIyQaqtnkyQrwpDLkHKp55MkD8VhlyCFFojmSB/Kwy5BCkaUDJBKgqZBKHiZaUTpOaQZIJUlpVMkKpDkglSlXoyQaqXlUyQ6vaeoA4p8Mz86z34DPg/xMMsjaKXlg8J9gi+Sbse3KWND2URhMYVrWyeAB5iY5SZ9bwWN33uAUNsE8/oiShyM09TdkFGyA7IANrJwGoQg2AS6e5bbyNJz43rDX0ZRViMYQfd5TLgGtLP4p4ctoZfCPvTNSop2heXC0r06s5aT5Th9cBwJEWQ3nONRvptiNN3koPklXCCw51et7iXlFIJE9AAmQsd8bP8N+T1NEIEiXMusLUhOw+cxSN5sM/5nd3BdeRmxPv19t/4R0ca6cMSZ6EjmeE2IAg/ZMA9qsPChoeMr0ZHEqkFBthAsB9ChAdhxEbjgsg7EBGBEP9Em1sWRwwvE8skCClzR1OeGPm0eBDljQCeScuqbs7k7k3sPGvrFAcMZMywPHhZA+cb6dzeDU3oxLi9HvtdO+ffjdQmZ0p7uyOQNl+RIZL2TuSAlCMsxtHU9kpojXcLQxWv4d3B0TbKp7dOfmSriv0JebdpWM3F8cai+t3/AAAA///snU1ynDAQhVuQRda5QG6QQ+QWOVeuG4+ysV2Kpn/F4JmJv1dFgYDBhPSj1epGb+VhnvEfwH8qOGrgp1yz4nPbB+xbJQwE+z8MvBeP3Xrf1fFMqFaVB6uuM0Q9m2DgfEKsGG52OlJvnT2mtlvB6L00YSaKtM4VZ98tiARxPt4zHDF8USJnkZxMUU+eK0Zkrkbpm/OGtwzbmy52S7RF8rn6zEhCG/5R0Sjyow/5PzIpmuRqDFpghFIwZmuUvToFr3XMus93guyGwUV6Lpq8iFUL4Y3QR4SKPFjG28yeCoIcI0jWQ3heIZKHtGpFtNygVkcyyrdEAquuYN4eeANvFvhZcnLMO0Z1KuPf3uVajnLMnFczLm1621nzb0OUPDHEeds2idW1LC+gCT+NKTzN0C25A017Vav9sTJVZkFWRA5LhMojiFbxMBPky+Q1doUUo1HvBkG0buI2/SYqOPNeANmUqeWBu+giKlrXT5yuzGxkLdEPt1KWWiGApxmrGZa8Hh+NXkRXRxvv7cXoSo3XHO/9j0GQWYA4S5CRdC5JxopFzeiq2urZtnY9EV2RZ/YmkjC0zAgXnmMtMI9Gqnpiv+Y1RCGf1S3y9OmjttW1Usk811iL1GoaspqTm0O6bHBejTmi1D7dq7Vu1ooyR3boNRusX4yAPRtjRF73/R6tr0Ki0azMcG8m0BbJa4EekUfGY9x/qLdaj1PVKap8fZMe6p2/Yzv6TVslKXgkv1GpS8JT3G/ot0oay1ArXshbR/uu2m9f3q6+fVcV2W6dBYcAz+llqr9ZLSPpi/fXq1NkUagInoFkN7smk2kAEBCE6X8AcAjChGUAOARhikUAHIIwKSwADkGYxhoAhyBMvA+ARRCkQgDwCYIHAcAhCDEIAA5BGMUCwCEIeRAAHIKQSQfAIQi1WAA4BKGaFwCHIBuPAQCbIF95DADYBPnGYwDAJsh3HgMANkF+8BgAsAnyk8cAgE2Q3zwGAAyCiMgvHgMANkHoYgHgEIQgHQCHIAzzAuAQhEQhAA5BKDUBwCEIxYoAWAQ5aVYTSujBPdA/giDtBiRoJ5AI0n1uY+4Hz1kS+bE+mKrInR3Zjo6tEgQyPb7RZ425J49VtzPtfwgSGfSq1uARmbYqWSHN45GhYpSr8mqr2obh3x+/KPQEPKuqt9klSzKLSFXSQJJzyVHRIIwM2TLuWy1itK8Isic8gCbvvBnrWQ/9bVucaxxRx131Ooh71gjRDniD7Bvdk322ZKBnTfT5NxfnGpEC7jtBRsPblO3R0Del/bbsw/E9WI/b4zXldb9ITk99Nc6BHGteYzU+iMggIvIyGP5oxC/DeS/B+vK6fVGWrrRHws3bncmrAQgC1z3ZrfI8R2bZle7XPl1fpn3W0g3PIkpXTNvf6WYtda/a9MYXxUuI4Tnm31vL7EVm7zB6lcvi0o3u2JWXa1OXKhOMb04cMhNAa2tdKqtbVx0Ju/XIF1gfkVodgdK6O14ccQnaERm6E6z3VjDAOU4R5e1vESo6NxuMZ2KJW+VSwNoQb0QSbzg3Ct5nzyKOwVvneqNXJkEkYYxWuyUIlTH87H10IRfyyASJyNIkn9TLECky+O5cL7yPJvmcwbNk0yHB45PnKbLoIscTavcwZgjwObzPWaQrXeNMY8OQwaMTL8TfAQD2J8EY0cmSPwAAAABJRU5ErkJggg==);
            background-position: 0 58px;
             margin: 0 100px;
             height: 29px;
         }
        #textarea_comp-l0egjtbl {
    width: 88%;
    height: 261px;
    border: 2px solid #E80EAE;
    padding: 1%;
}
#textarea_comp-l0egniff {
    width: 88%;
    height: 100px;
    border: 2px solid #E80EAE;
    padding: 1%;
    margin: 20px;
}
#btnSinscrire {
    background: #E80EAE;
    padding: 1%;
    border-color: #E80EAE;
    font-size: 17px;
    font-weight: bold;
}
#logout {
    background: #E80EAE;
    padding: 1%;
    border-color: #E80EAE;
    font-size: 17px;
    font-weight: bold;
    margin-bottom: 20px;
}

#capt div {
    text-align:center; 
    width: auto !important;
        }
.btngreen {
    margin: 10px 10px 10px 10px;
    color: #000;
    font-family: futura-lt-w01-book, futura-lt-w05-book, sans-serif;
    font-size: 15px;
    font-weight: normal;
    font-style: normal;
    line-height: 1.4em;
    letter-spacing: 0em;
    background-color: #edd23b;
    text-decoration: none;
    padding: 10px 15px 10px 15px;

    }

.btngreen:hover {
    cursor: pointer;
    background-color: #fff !important;
    }
input[type="checkbox"] {
    appearance: none;
    -webkit-appearance: none;
    height: 19px;
    width: 18px;
    background-color: #fff;
    border: #E80EAE solid 3px;
    border-radius: 0px;
    cursor: pointer;
    }
#comp-l0efuohk
    {
    padding-top: 50px;
    }
input[type="checkbox"] {
    appearance: none;
    -webkit-appearance: none;
    height: 19px;
    width: 18px;
    background-color: #fff;
    border: #E80EAE solid 3px;
    border-radius: 0px;
    cursor: pointer;
    }

input[type="checkbox"]:after {
    font-family: "Font Awesome 5 Free";
    font-size: 12px;
    margin-top: -3px;
    font-weight: 900;
    content: "\2713";
    color: #E80EAE;
    display: none;
    }

input[type="checkbox"]:hover {
    border: rgb(128, 210, 224) solid 3px;
    background-color: #fff;

    }

input[type="checkbox"]:checked {
    background-color: #fff;
    }

input[type="checkbox"]:checked:after {
    display: block;
    }
#suprimer_img
    {
    width: 62px;
    background-color: #db1414;
    border-radius: 50%;
    height: 62px;
    }
    </style>

    <?php ///$this->load->view("privicarte/pub_compte", $data); ?>




<? var_dump(); ?>

<?php //$this->load->view("sortez/includes/footer_frontoffice_com", $data); ?>