<?php $data['zUmpty'] = '';?>
<?php $this->load->view("privicarte/includes/main_header", $data); ?>
<style type="text/css">
<!--
@font-face {
    font-family: FuturaBk;
    src: url(<?php echo base_url()."application/resources/fonts/Futura-T-OT-Book_19064.ttf";?>);
}
.publightbox_carte_container {
	background-color: #FFFFFF;
	width: 650px;
	margin-right: auto;
	margin-left: auto;
	padding-right: 15px;
	padding-left: 15px;
	display:table;
	text-align:center;
	font-size: 19px;
	padding-bottom:500px;
}
.title_publightbox_carte {
	font-size: 40px;
	color: #DA1893;
	font-family:"FuturaBk";
	font-weight:bold;
}
.title_publightbox_carte_black {
	font-size: 25px;
	color: #000000;
	font-family:"FuturaBk";
}
.publightbox_carte_head {
	color: #FFFFFF;
	background-color: #000000;
	width: 650px;
	margin-right: auto;
	margin-left: auto;
	display:table;
}
.publightbox_carte_head_right {
	text-align: right;
}
body { margin-top:0 !important;}
.btn_back_link_card {border: 2px solid rgb(255, 255, 255); border-radius: 5px; margin-top: 30px; color:#FFFFFF !important;}
.btn_publightbox_btn, .btn_publightbox_btn:hover, .btn_publightbox_btn:focus, .btn_publightbox_btn:active {
	text-transform: uppercase;
	color: #FFFFFF;
	background-color: #DA1893;
	text-decoration:none;
	border-radius: 10px;
	padding: 15px 25px;
	font-size:22px;
	font-family:Verdana, Arial, Helvetica, sans-serif;
}
.displaytable { display:table;}
.carte_bienvenue {
	width: 400px;
	margin-right: auto;
	margin-left: auto;
}
.margintop15 { margin-top:15px;}
.black_link, .black_link:hover, .black_link:focus, .black_link:active {
	color: #000000;
	text-decoration:underline;
}
.margintop25 { margin-top:25px;}
.margintop50 { margin-top:50px;}
-->
</style>

<script>
function btn_part_privicarte_men() {
	window.open("<?php echo site_url('front/fidelity/menuconsommateurs');?>", "_blank", "toolbar=no, scrollbars=yes, resizable=yes, top=500, left=500, width=650, height=800");
}
function btn_part_privicarte_insc() {
	window.open("<?php echo site_url('front/fidelity/inscription');?>", "_blank", "toolbar=no, scrollbars=yes, resizable=yes, top=500, left=500, width=650, height=800");
}
</script> 

<div class="publightbox_carte_head">
  <div class="col-sm-6"><a href="<?php echo base_url(); ?>"><img alt="logo" src="<?php echo GetImagePath("privicarte/");?>/logo.png"></a></div>
    <div class="col-sm-6 publightbox_carte_head_right"><a href="<?php echo base_url(); ?>" class="btn btn-default-outline btn_back_link_card"> Retour site Privicarte </a></div>
</div>
<div class="publightbox_carte_container">
  <div class="col-sm-12"><img alt="logo" src="<?php echo GetImagePath("privicarte/");?>/publightbox_cart_user.jpg"></div>
  
  <div class="carte_bienvenue padding0 displaytable">
  	<div class="col-sm-2"><img src="<?php echo GetImagePath("privicarte/");?>/user_publightbox.png" alt="user" /></div>
    <div class="col-sm-10">
   	  <div class="title_publightbox_carte">Bienvenue !</div>
      <div class="title_publightbox_carte_black"><?php echo $publightbox_email; ?></div>
    </div>
  </div>
  
  <div class="col-sm-12 title_publightbox_carte padding0 displaytable margintop15">Je poss&egrave;de la carte Privicarte !</div>
  <div class="col-sm-12 padding0 displaytable margintop15">
  	<a href="javascript:void(0);" onclick="btn_part_privicarte_men();" class="btn btn-default-outline btn_publightbox_btn">J'accède à mon compte carte</a>  
  </div>
  <div class="col-sm-12 title_publightbox_carte padding0 displaytable margintop25">Je ne poss&egrave;de pas encore<br/>la carte Privicarte !</div>
  
  <div class="col-sm-12 margintop15">Je m'inscris &agrave; la carte Privicarte et je b&eacute;n&eacute;ficie de la validation des avantages avec une gestion et le suivi en temps r&eacute;el de ma consommation et mes conditions de fid&eacute;lisation sur un compte personnel et s&eacute;curis&eacute;.</div>
  
  <div class="col-sm-2 margintop25"><img src="<?php echo GetImagePath("privicarte/");?>/gratuit_publightbox.png" alt="gratuit_publightbox" /></div>
  <div class="col-sm-12 padding0 displaytable margintop15">
  	<a href="javascript:void(0);" onclick="btn_part_privicarte_insc();" class="btn btn-default-outline btn_publightbox_btn">Je m'inscris</a>  
  </div>
  
  <div class="col-sm-12 margintop50">
  	<a href="#" class="black_link">Conditions g&eacute;n&eacute;rale &agrave; consulter !</a> 
  </div>
</div>

<?php $this->load->view("privicarte/includes/main_footer", $data); ?>