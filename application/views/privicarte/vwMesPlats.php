<?php $data["zTitle"] = 'Mes Annonces'; ?>
<?php
$this->load->view("sortez/includes/backoffice_pro_css", $data);
?>
    <div class="col-lg-12 padding0">
        <div class="col-sm-4 padding0 textalignleft">
            <h1>Liste de mes Plats</h1>
        </div>
        <div class="col-sm-4 padding0 textaligncenter">
            <button id="btnnewreturn" class="btn btn-primary" onclick="document.location='<?php echo base_url() ;?>';">Retour site</button>
            <button id="btnnewreturn" class="btn btn-primary" onclick="document.location='<?php echo site_url("front/utilisateur/contenupro") ;?>';">Retour au menu</button>
            <button id="btnnew" class="btn btn-success" onclick="document.location='<?php echo site_url("front/Plat_du_jour/fichePlat/$idCommercant") ;?>';">Ajouter une plat</button>
            <label for="mail_reservation">Votre Mail pour les reservations</label><input type="email" name="mail_reservation" id="mail_reservation">
        </div>
    </div>





    <div id="divMesAnnonces" class="content" align="center" style="display: table;">
        <form name="frmMesAnnonces" id="frmMesAnnonces" action="" method="POST" enctype="multipart/form-data">

            <div id="container">
                <table cellpadding="1" class="tablesorter">
                    <thead>
                    <tr>
                        <th>Description courte</th>
                        <th>Date debut</th>
                        <th>Date fin</th>
                        <th>Prix</th>
                        <th>Etat</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i_order_partner = 1; ?>
                    <?php foreach($toListeplat as $oListeplat){ ?>
                        <tr>
                            <td><?php echo $oListeplat->description_plat ; ?></td>
                            <td>
                                <?php
                                if (isset($oListeplat->date_debut_plat) AND $oListeplat->date_debut_plat !="00/00/0000" AND $oListeplat->date_debut_plat !=''  AND $oListeplat->date_debut_plat !=null)
                                    echo convert_Sqldate_to_Frenchdate($oListeplat->date_debut_plat) ; ?>
                            </td>
                            <td>
                                <?php
                                if (isset($oListeplat->date_fin_plat) AND $oListeplat->date_debut_plat !="00/00/0000" AND  $oListeplat->date_debut_plat !='' AND $oListeplat->date_debut_plat !=null)
                                    echo convert_Sqldate_to_Frenchdate($oListeplat->date_fin_plat) ; ?>
                            </td>
                            <td><?php
                                if ($oListeplat->prix_plat!=0)
                                    echo $oListeplat->prix_plat ; ?>
                            </td>

                            <td>
                                <?php
                                $zEtat = "";
                                if ($oListeplat->IsActif == 0) $zEtat = "Inactif";
                                else if ($oListeplat->IsActif == 1) $zEtat = "Actif";
                                echo $zEtat;?>
                            </td>

                            <td><a href="<?php echo site_url("front/Plat_du_jour/vwmodifpalt/"  . $oListeplat->id ) ; ?>" title="Modifier"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/update_ico.png"></a></td>
                            <td><a href="<?php echo site_url("front/Plat_du_jour/supprimplat/" . $oListeplat->id.'/'.$oListeplat->IdCommercant); ?>" onclick="if (!confirm('voulez-vous vraiment supprimer cette annoce ?')){ return false ; }" title="Supprimer"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/delete_ico.png"></a></td>
                        </tr>
                        <?php if (isset($oListeplat->order_partner) && $oListeplat->order_partner != "" && $oListeplat->order_partner != NULL) {} else $i_order_partner = $i_order_partner + 1; ?>
                    <?php } ?>
                    </tbody>
                </table>
                <div id="pager" class="pager">
                    <img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
                    <img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
                    <input type="text" class="pagedisplay"/>
                    <img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
                    <img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
                    <select class="pagesize" style="visibility:hidden">
                        <option selected="selected"  value="10">10</option>
                        <option value="20">20</option>
                        <option value="30">30</option>
                        <option  value="40">40</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
<?php //$this->load->view("adminAout2013/includes/vwFooter2013"); ?>