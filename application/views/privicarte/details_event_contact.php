<?php $data["zTitle"] = 'Event details' ?>
<?php $this->load->view("privicarte/includes/main_header", $data); ?>
<?php $this->load->view("privicarte/includes/ready_agenda", $data); ?>


<script type="text/javascript">
$(document).ready(function() {

	$("#contact_recommandation_nom").focusin(function() {	  if ($(this).val()=="Votre nom *") $(this).val('');	});
	$("#contact_recommandation_nom").focusout(function() {	  if ($(this).val()=="") $(this).val('Votre nom *');	});

	$("#contact_recommandation_mail").focusin(function() {	  if ($(this).val()=="Votre courriel *") $(this).val('');	});
	$("#contact_recommandation_mail").focusout(function() {	  if ($(this).val()=="") $(this).val('Votre courriel *');	});
	
	$("#contact_recommandation_mail_ami").focusin(function() {	  if ($(this).val()=="Courriel de votre ami *") $(this).val('');	});
	$("#contact_recommandation_mail_ami").focusout(function() {	  if ($(this).val()=="") $(this).val('Courriel de votre ami *');	});
	
	$("#contact_recommandation_msg").focusin(function() {	  if ($(this).val()=="Votre message *") $(this).val('');	});
	$("#contact_recommandation_msg").focusout(function() {	  if ($(this).val()=="") $(this).val('Votre message *');	});
	
	$("#contact_recommandation_reset").click(function() {
	  $("#contact_recommandation_nom").val('Votre nom *');
	  $("#contact_recommandation_mail_ami").val('Courriel de votre ami *');
	  $("#contact_recommandation_mail").val('Votre courriel *');
	  $("#contact_recommandation_msg").val('Votre message *');
	  $("#spanContactPrivicarteRecommandationForm").html('* champs obligatoires');
	});
	
	$("#contact_recommandation_send").click(function() {
		$("#spanContactPrivicarteRecommandationForm").html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
		var error = 0;
		var contact_recommandation_msg = $("#contact_recommandation_msg").val();
		if (contact_recommandation_msg == '' || contact_recommandation_msg == 'Votre message *') error = 1;
		var contact_recommandation_nom = $("#contact_recommandation_nom").val();
		if (contact_recommandation_nom == '' || contact_recommandation_nom == 'Votre nom *') error = 1;
		var contact_recommandation_mail_ami = $("#contact_recommandation_mail_ami").val();
		if (contact_recommandation_mail_ami == '' || contact_recommandation_mail_ami == 'Courriel de votre ami *') error = 1;
		else if (!validateEmail(contact_recommandation_mail_ami)) error = 3;
		var contact_recommandation_mail = $("#contact_recommandation_mail").val();
		if (contact_recommandation_mail == '' || contact_recommandation_mail == 'Votre courriel *') error = 1;
		else if (!validateEmail(contact_recommandation_mail)) error = 2;
		
		
		if (error == 1) {
			$("#spanContactPrivicarteRecommandationForm").html('<span style="color:#FF0000;">* Les champs sont obligatoires</span>');
		} else if (error == 2) {
			$("#spanContactPrivicarteRecommandationForm").html('<span style="color:#FF0000;">* Adresse email invalide</span>');
			$("#contact_recommandation_mail").css('border-color','#ff0000'); 
			//alert("invalide mail");
		} else if (error == 3) {
			$("#spanContactPrivicarteRecommandationForm").html('<span style="color:#FF0000;">* Adresse email invalide</span>');
			$("#contact_recommandation_mail_ami").css('border-color','#ff0000'); 
		} else {
			$.post(
				"<?php echo site_url("front/professionnels/contact_partner_sendmail/");?>", 
				{
					contact_recommandation_nom:contact_recommandation_nom,
					contact_recommandation_tel:'',
					contact_recommandation_mail:contact_recommandation_mail,
					contact_recommandation_msg:contact_recommandation_msg,
					contact_recommandation_mailto:contact_recommandation_mail_ami
				},
				function( data ) {
				  $("#spanContactPrivicarteRecommandationForm").html(data);
				});
			}
			
			//alert(error);
	});
	
	
	$("#contact_partner_nom").focusin(function() {	  if ($(this).val()=="Votre nom *") $(this).val('');	});
	$("#contact_partner_nom").focusout(function() {	  if ($(this).val()=="") $(this).val('Votre nom *');	});
	
	$("#contact_partner_tel").focusin(function() {	  if ($(this).val()=="Votre numéro de téléphone *") $(this).val('');	});
	$("#contact_partner_tel").focusout(function() {	  if ($(this).val()=="") $(this).val('Votre numéro de téléphone *');	});
	
	$("#contact_partner_mail").focusin(function() {	  if ($(this).val()=="Votre courriel *") $(this).val('');	});
	$("#contact_partner_mail").focusout(function() {	  if ($(this).val()=="") $(this).val('Votre courriel *');	});
	
	$("#contact_partner_msg").focusin(function() {	  if ($(this).val()=="Votre message *") $(this).val('');	});
	$("#contact_partner_msg").focusout(function() {	  if ($(this).val()=="") $(this).val('Votre message *');	});
	
	$("#contact_partner_reset").click(function() {
	  $("#contact_partner_nom").val('Votre nom *');
	  $("#contact_partner_tel").val('Votre numéro de téléphone *');
	  $("#contact_partner_mail").val('Votre courriel *');
	  $("#contact_partner_msg").val('Votre message *');
	  $("#spanContactPartnerForm").html('* champs obligatoires');
	});
	
	$("#contact_partner_send").click(function() {
		var error = 0;
		var contact_partner_nom = $("#contact_partner_nom").val();
		if (contact_partner_nom == '' || contact_partner_nom == 'Votre nom *') error = 1;
		var contact_partner_tel = $("#contact_partner_tel").val();
		if (contact_partner_tel == '' || contact_partner_tel == 'Votre numéro de téléphone *') error = 1;
		var contact_partner_mail = $("#contact_partner_mail").val();
		if (contact_partner_mail == '' || contact_partner_mail == 'Votre courriel *') error = 1;
		if (!validateEmail(contact_partner_mail)) error = 2;
		var contact_partner_msg = $("#contact_partner_msg").val();
		if (contact_partner_msg == '' || contact_partner_msg == 'Votre message *') error = 1;
		$("#spanContactPartnerForm").html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
		
		if (error == 1) {
			$("#spanContactPartnerForm").html('<span style="color:#FF0000;">* Les champs sont obligatoires</span>');
		} else if (error == 2) {
			$("#spanContactPartnerForm").html('<span style="color:#FF0000;">* Adresse email invalide</span>');
			$("#contact_partner_mail").css('border-color','#ff0000'); 
		} else {
			$.post(
				"<?php echo site_url("front/professionnels/contact_partner_sendmail/");?>", 
				{
					contact_partner_nom:contact_partner_nom,
					contact_partner_tel:contact_partner_tel,
					contact_partner_mail:contact_partner_mail,
					contact_partner_msg:contact_partner_msg,
					contact_partner_mailto:"<?php echo $oDetailAgenda->email; ?>"
				},
				function( data ) {
				  $("#spanContactPartnerForm").html(data);
				});
			}
	});


	$("#IdVideoPartnerAgenda").fancybox({
		autoScale : false,
		overlayOpacity      : 0.8, // Set opacity to 0.8
		overlayColor        : "#000000", // Set color to Black
		padding         : 5,
		width         : 520,
		height        : 800,
		transitionIn      : 'elastic',
		transitionOut     : 'elastic'
	});
	$("#IdContactPartnerForm").fancybox({
		autoScale : false,
		overlayOpacity      : 0.8, // Set opacity to 0.8
		overlayColor        : "#000000", // Set color to Black
		padding         : 5,
		width         : 520,
		height        : 800,
		transitionIn      : 'elastic',
		transitionOut     : 'elastic'
	});
	$("#IdRecommandationPartnerForm").fancybox({
		'autoScale' : false,
		'overlayOpacity'      : 0.8, // Set opacity to 0.8
		'overlayColor'        : "#000000", // Set color to Black
		'padding'         : 5,
		'width'         : 520,
		'height'        : 410,
		'transitionIn'      : 'elastic',
		'transitionOut'     : 'elastic'
	});
	$("#addthis_button_pvc").fancybox({
		'autoScale' : false,
		'overlayOpacity'      : 0.8, // Set opacity to 0.8
		'overlayColor'        : "#000000", // Set color to Black
		'padding'         : 5,
		'width'         : 800,
		'height'        : 410,
		'transitionIn'      : 'elastic',
		'transitionOut'     : 'elastic',
		'type'				: 'iframe'
	});
	$("#idFacebookProFormAgenda").fancybox({
		autoScale : false,
		overlayOpacity      : 0.8, // Set opacity to 0.8
		overlayColor        : "#000000", // Set color to Black
		padding         : 5,
		width         : 520,
		height        : 800,
		transitionIn      : 'elastic',
		transitionOut     : 'elastic',
		type          : 'iframe'
	});
});

function validateEmail(sEmail) {
	var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if (filter.test(sEmail)) {
		return true;
	}
	else {
		return false;
	}
}
</script>

<!--Video content-->
<style type="text/css">
<!--
<?php if (isset($contact_display) && $contact_display == "recommandation") { ?>
#divContactRecommandationForm {	display: block;}
#divContactPartnerForm {	display: none;}
#divVideoPartnerAgenda {	display: none;}
<?php } else if (isset($contact_display) && $contact_display == "video") { ?>
#divContactRecommandationForm {	display: none;}
#divContactPartnerForm {	display: none;}
#divVideoPartnerAgenda {	display: block;}
<?php } else { ?>
#divContactRecommandationForm {	display: none;}
#divContactPartnerForm {	display: block;}
#divVideoPartnerAgenda {	display: none;}
<?php } ?>
#navigation_up_down_3107 {display: none;}
table#tableContactPartnerForm td input, table#tableContactPartnerForm td textarea { width:90% !important;}
-->
</style>

<div id="divVideoPartnerAgenda" style="background-color:#000000;">
<table width="100%" id="tableContactPartnerForm" border="0" cellspacing="0" cellpadding="0" style="text-align:center; width:540px; height:472px; background-color:#000000;"><tr><td>
	<?php 
    $link_video_club_agenda = preg_split('[v=]', $oDetailAgenda->video);
    if (isset($link_video_club_agenda[1])) {
		?>
		<object width="540" height="472">
		<param value="http://www.youtube.com/v/<?php echo $link_video_club_agenda[1];?>&autoplay=1&loop=1&showinfo=0&rel=0&fs=1&hd=1" name="movie">
		<param value="true" name="allowFullScreen">
		<param value="always" name="allowscriptaccess">
		<param value="transparent" name="wmode">
		<embed width="540" height="472" allowfullscreen="true" allowscriptaccess="always" wmode="transparent" type="application/x-shockwave-flash" style="width:540px;height:472px;" src="http://www.youtube.com/v/<?php echo $link_video_club_agenda[1];?>&autoplay=0&loop=1&showinfo=0&rel=0&fs=1&hd=1">
		</object>
    <?php } ?>
</td></tr></table>    
</div>



<!--Contact form contet-->
<div id="divContactPartnerForm" style="background-color:#FFFFFF;">
    <form name="formContactPartnerForm" id="formContactPartnerForm" action="#">
        <table width="100%" id="tableContactPartnerForm" border="0" cellspacing="0" cellpadding="0" style="text-align:center; width:350px; height:400px;">
          <tr>
            <td><div style="font-family:arial; font-size:24px; font-weight:bold;">Nous Contacter</div></td>
          </tr>
          <tr>
            <td><input type="text" name="contact_partner_nom" id="contact_partner_nom" value="Votre nom *"/></td>
          </tr>
          <tr>
            <td><input type="text" name="contact_partner_tel" id="contact_partner_tel" value="Votre numéro de téléphone *"/></td>
          </tr>
          <tr>
            <td><input type="text" name="contact_partner_mail" id="contact_partner_mail" value="Votre courriel *"/></td>
          </tr>
          <tr>
            <td><textarea name="contact_partner_msg" id="contact_partner_msg">Votre message *</textarea></td>
          </tr>
          <tr>
            <td><span id="spanContactPartnerForm">* champs obligatoires</span></td>
          </tr>
          <tr>
            <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><input type="button" class="btn btn-default" name="contact_partner_reset" id="contact_partner_reset" value="Retablir"/></td>
                <td><input type="button" class="btn btn-default" name="contact_partner_send" id="contact_partner_send" value="Envoyer"/></td>
              </tr>
            </table>
            </td>
          </tr>
        </table>
    </form>
</div>

<!--Recommandation form contet-->
<div id="divContactRecommandationForm" style="background-color:#FFFFFF; width:350px; height:400px;">
    <form name="formContactPrivicarteForm" id="formContactPrivicarteForm" action="#">
        <table width="100%" id="tableContactPartnerForm" border="0" cellspacing="0" cellpadding="0" style="text-align:center; width:350px; height:400px;">
          <tr>
            <td><div style="font-family:arial; font-size:24px; font-weight:bold;">Recommander &agrave; un ami</div></td>
          </tr>
          <tr>
            <td><input type="text" name="contact_recommandation_nom" id="contact_recommandation_nom" value="Votre nom *"/></td>
          </tr>
          <tr>
            <td><input type="text" name="contact_recommandation_mail_ami" id="contact_recommandation_mail_ami" value="Courriel de votre ami *"/></td>
          </tr>
          <tr>
            <td><input type="text" name="contact_recommandation_mail" id="contact_recommandation_mail" value="Votre courriel *"/></td>
          </tr>
          <tr>
            <td><textarea name="contact_recommandation_msg" id="contact_recommandation_msg">Votre message *</textarea></td>
          </tr>
          <tr>
            <td><span id="spanContactPrivicarteRecommandationForm">* champs obligatoires</span></td>
          </tr>
          <tr>
            <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><input type="button" class="btn btn-default" name="contact_recommandation_reset" id="contact_recommandation_reset" value="Retablir"/></td>
                <td><input type="button" class="btn btn-default" name="contact_recommandation_send" id="contact_recommandation_send" value="Envoyer"/></td>
              </tr>
            </table>
            </td>
          </tr>
        </table>
    </form>
</div>


<?php $this->load->view("privicarte/includes/main_footer", $data); ?>