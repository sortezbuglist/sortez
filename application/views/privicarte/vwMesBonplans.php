<?php $data["zTitle"] = 'Mes Bonplans'; ?>

<?php
$data['empty'] = null;
$this->load->view("sortez/includes/backoffice_pro_css", $data);
?>

<script type="text/javascript" charset="utf-8"> 
	// $(function() {
	// 		$(".tablesorter")
	// 			.tablesorter({widthFixed: true, widgets: ['zebra'], headers: {7: { sorter: false}, 8: {sorter: false} }})
	// 			.tablesorterPager({container: $("#pager")});
	// 	});



	function set_value_field() {
            if($('#is_actif_affichBonplan').is(':checked')){
                $('#is_actif_affichBonplan_value').val(1);
            }else{
                $('#is_actif_affichBonplan_value').val(0);
            }
            }


</script>   


<div class="col-lg-12 padding0" style="width:100%; text-align:center;">
	<div class="col-sm-6 padding0 textalignleft">
		<h1>Liste de mes Bons Plans</h1>
	</div>
	<div class="col-sm-6 padding0 textalignright">
		<button id="btnsitereturn" class="btn btn-primary" onclick="document.location='<?php echo base_url();?>';">Retour site</button>
		<button id="btnnewreturn" class="btn btn-primary" onclick="document.location='<?php echo site_url("front/utilisateur/contenupro") ;?>';">Retour au menu</button>
		<?php if (isset($limit_bonplan_add) && $limit_bonplan_add==1) {
			echo "<span style='color: #FF0000;'>";
            echo "Vous avez atteint vos limites de bonplan : ";
            if (isset($objCommercant->limit_bonplan)) echo $objCommercant->limit_bonplan;
            echo '</span>';
        } else {?>
		<button id="btnnew" class="btn btn-success" onclick="document.location='<?php echo site_url("front/bonplan/ficheBonplan/$idCommercant/0") ;?>';">Ajouter un nouveau bonplan</button>
		<?php }?>
	</div>
</div>


<div id="divMesAnnonces" class="content" align="center" style="display: table;">
        <form name="frmMesAnnonces" id="frmMesAnnonces" action="" method="POST" enctype="multipart/form-data">
			
				
            	<div id="container">
					<table id="example" cellpadding="0" cellspacing="0" border="0" class="tablesorter"s>
						<thead>
							<tr>
								<th>Titre</th>
								<th>Description</th>
								<th>Categorie</th>
								<th>Ville</th>
								<th>Nbr pris</th>
								<th>Date debut</th>
								<th>Date fin</th>
								<th>&nbsp;</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody> 
							<?php $iX = 0 ;
							if(isset($toListeMesBonplans)){
                                foreach($toListeMesBonplans as $oListeMesBonplans){ ?>
                                    <tr class="<?php if ($iX = 0){ echo "gradeX"; }else{ echo "gradeA"; }?>">
                                        <td><?php echo $oListeMesBonplans->bonplan_titre ; ?></td>
                                        <td><?php echo truncate($oListeMesBonplans->bonplan_texte,100,"...") ?></td>
                                        <td><?php if(isset($oListeMesBonplans->categorie_nom)) echo $oListeMesBonplans->categorie_nom ; ?></td>
                                        <td><?php if(isset($oListeMesBonplans->Nom)) echo $oListeMesBonplans->Nom  ; ?></td>
                                        <td class="center"><?php echo $oListeMesBonplans->bonplan_nombrepris  ; ?></td>
                                        <td class="center"><?php if ($oListeMesBonplans->bonplan_type == "1") echo $oListeMesBonplans->bonplan_date_debut; else if ($oListeMesBonplans->bonplan_type == "2") echo $oListeMesBonplans->bp_unique_date_debut;  else if ($oListeMesBonplans->bonplan_type == "3") echo $oListeMesBonplans->bp_multiple_date_debut;?></td>
                                        <td class="center"><?php if ($oListeMesBonplans->bonplan_type == "1") echo $oListeMesBonplans->bonplan_date_fin; else if ($oListeMesBonplans->bonplan_type == "2") echo $oListeMesBonplans->bp_unique_date_fin;  else if ($oListeMesBonplans->bonplan_type == "3") echo $oListeMesBonplans->bp_multiple_date_fin;?></td>
                                        <td><a href="<?php echo site_url("front/bonplan/ficheBonplan/" . $oListeMesBonplans->bonplan_commercant_id . "/" . $oListeMesBonplans->bonplan_id ) ; ?>"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/update_ico.png"></a></td>
                                        <td><a href="<?php echo site_url("front/bonplan/supprimBonplan/" . $oListeMesBonplans->bonplan_id . "/" . $oListeMesBonplans->bonplan_commercant_id) ; ?>" onclick="if (!confirm('voulez-vous vraiment supprimer cette annoce?')){ return false ; }"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/delete_ico.png"></a></td>
                                    </tr>
                                    <?php $iX ++ ;
                                }
							}
							?>
						</tbody>
					</table>
<!--					<div id="pager" class="pager">-->
<!--							<img src="--><?php //echo GetImagePath("front/"); ?><!--/first.png" class="first"/>-->
<!--							<img src="--><?php //echo GetImagePath("front/"); ?><!--/prev.png" class="prev"/>-->
<!--							<input type="text" class="pagedisplay"/>-->
<!--							<img src="--><?php //echo GetImagePath("front/"); ?><!--/next.png" class="next"/>-->
<!--							<img src="--><?php //echo GetImagePath("front/"); ?><!--/last.png" class="last"/>-->
<!--							<select class="pagesize" style="visibility:hidden">-->
<!--								<option selected="selected"  value="10">10</option>-->
<!--								<option value="20">20</option>-->
<!--								<option value="30">30</option>-->
<!--								<option  value="40">40</option>-->
<!--							</select>-->
<!--					</div>-->
                    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
                    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
                    <script type="text/javascript">
                        $( document ).ready(function() {
                            $(document).ready(function() {
                                $('#example').DataTable( {
                                    "pagingType": "full_numbers"
                                } );
                            } );
                        });
                    </script>
				</div>
        </form>
    </div>
