<?php $data["zTitle"] = 'Bonplans' ?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN" "http://www.openmobilealliance.org/tech/DTD/xhtml-mobile11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="Generator" content="Serif WebPlus X5">
<meta http-equiv="X-UA-Compatible" content="IE=EmulationIE8">
<?php if(count ($toBonPlan)>0) { 
	foreach($toBonPlan as $oBonPlan){?>
		<title><?php echo $oBonPlan->bonplan_titre;?></title>
	<?php } ?>
<?php } ?>
<meta content="minimum-scale=1.0, width=device-width" name="viewport">
 <!--Master Page Head-->
<style type="text/css">
body {margin: 0px; padding: 0px;}
.Corps-artistique-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:left; font-weight:400;
}
.Corps-P
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:center; font-weight:400; height:15px;
}
.Corps-P-P0
{
    margin:0.0px 0.0px 0.0px 0.0px; text-align:justify; font-weight:400;
}
.Corps-P-P1
{
    margin:0.0px 0.0px 12.0px 0.0px; text-align:center; font-weight:400;
}
.Corps-P-P2
{
    margin:0.0px 0.0px 4.0px 0.0px; text-align:center; font-weight:400;
}
.Corps-artistique-C
{
    font-family:"Arial", sans-serif; font-weight:700; color:#3653c1; font-size:19.0px; line-height:1.21em;
}
.Corps-artistique-C-C0
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:13.0px; line-height:1.23em;
}
.Corps-artistique-C-C1
{
    font-family:"Arial", sans-serif; font-weight:700; font-size:12.0px; line-height:1.25em;
}
.Corps-C
{
    font-family:"Arial", sans-serif; font-size:11.0px; line-height:1.27em;
}
.Corps-artistique-C-C2
{
    font-family:"Arial", sans-serif; font-size:15.0px; line-height:1.13em;
}
.Corps-C-C0
{
    font-family:"Arial", sans-serif; font-size:12.0px; line-height:1.25em;
}
.Corps-C-C1
{
    font-family:"Arial", sans-serif; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C2
{
    font-family:"Arial", sans-serif; text-decoration:underline; font-size:13.0px; line-height:1.23em;
}
.Corps-C-C3
{
    font-family:"Arial", sans-serif; text-decoration:underline; font-size:12.0px; line-height:1.25em;
}
.Button1,.Button1:link,.Button1:visited{background-position:0px 0px;text-decoration:none;display:block;position:absolute;background-image:url(<?php echo GetImagePath("front/"); ?>/wp6eaf7443_06.png);}
.Button1:focus{outline-style:none;}
.Button1:hover{background-position:0px -70px;}
.Button1:active{background-position:0px -35px;}
.Button1 span,.Button1:link span,.Button1:visited span{color:#333333;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:left;text-transform:none;font-style:normal;left:39px;top:38px;width:221px;height:22px;font-size:17px;display:block;position:absolute;cursor:pointer;}
.Button1:hover span{color:#ffffff;}
</style>
<script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/jspngfix.js"></script>
<link rel="stylesheet" href="bonplanmobile_fichiers/wpstyles.css" type="text/css">
<script type="text/javascript">var blankSrc = "wpscripts/blank.gif";
</script>
</head>

<body style="background-color: transparent; text-align: center; height: 2300px; background-color:#003566;" text="#000000">
<div style="background-color:#cccccc;text-align:left;margin-left:auto;margin-right:auto;position:relative;width:320px;height:2300px;">

<div>
<a href="<?php echo site_url('front/bonplan/'); ?>">
<img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp7cf80293_06.png" width="320" height="59" alt="back">
</a>
</div>

<!--start main info menu-->
<?php $this->load->view("front/vwTitreinfocommercantMobile", $data);?>
<!--end main info menu-->

<!--start main icone menu 2-->
<?php $this->load->view("front/vwMenumainiconeMobile_noslide", $data);?>
<!--end main icone menu 2-->



<img src="<?php echo GetImagePath("front/"); ?>/wp51f75006_06.png" id="qs_1664" alt="" onload="OnLoadPngFix()" style="position: absolute; left: 0px; top: 155px;" height="5" border="0" width="320">


<!--start main logo mobile-->
<?php $this->load->view("front/vwMainLogoMobile", $data);?>
<!--end main logo mobile-->



<?php if(count ($toBonPlan)>0) { 
  		        foreach($toBonPlan as $oBonPlan){?>
<div id="txt_580" style="position:absolute; left:15px; top:351px; width:292px; height: auto; overflow:hidden;">

<div id="bonplan_1" style="width:292px; height: auto;; overflow:hidden; background-image: url(<?php echo GetImagePath("front/"); ?>/wpaa3b69e4_06.png); background-repeat:no-repeat;">
    <div id="txt_580" style="color: #FFFFFF;    font-family: Arial,Helvetica,sans-serif;    font-size: 14px;    font-weight: bold;    height: 100px;    overflow: hidden;    padding: 70px 20px 20px 120px;    width: 150px; text-align:center;">
    
    <table width="100%" height="100" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="middle">
    <?php echo $oBonPlan->bonplan_titre."<br/>";
           //echo '<span style="font-size:9px;">'.$oBonPlan->bonplan_texte."</span>"; 
       ?>
    </td>
  </tr>
</table>
    </div>
	<div style="widows:270px; height:auto; padding-left:10px; padding-right:10px; text-align:center;">
    <span class="Corps-C"><?php echo $oBonPlan->bonplan_texte; ?></span><br />
    <span class="Corps-C">Offre valable du <?php echo translate_date_to_fr($oBonPlan->bonplan_date_debut); ?> au <?php echo translate_date_to_fr($oBonPlan->bonplan_date_fin); ?>.</span><br />
    <span class="Corps-C">
    <?php if  ($oBonPlan->bon_plan_utilise_plusieurs == 0){ ?>
            Offre valable une seule fois
            <?php } else { ?>
            Offre toujours valable
            <?php } ?>
            Non cumulable avec les promotions en cours
    </span>
    </div>
	<div style="widows:290px; height:auto;">
    <table border="0" cellspacing="0" cellpadding="0" style=" text-align:center; margin-left:10px;">
      <tr>
        <td height="40px">
        <?php $utilise  = false;if (isset($id_client) ) { 
		   $oAssocClientBonPlan = $this->mdlcadeau->getClientAssocBonPlan($id_client,$oBonPlan->bonplan_id); 
		   $bon_plan_utilise_plusieurs = 0;						   
			if(isset($oAssocClientBonPlan) && $oAssocClientBonPlan!=null) {
				//print_r($oAssocClientBonPlan);
				$utilise = $oAssocClientBonPlan->utilise;
				if($oBonPlan->bon_plan_utilise_plusieurs == 1){
				 $bon_plan_utilise_plusieurs = 1;
				}
				
			 } 
		}?>


		
        <a href="<?php if($utilise ==1 && $bon_plan_utilise_plusieurs == 0) { echo "Javascript:void();"; } else  { echo site_url("front/fidelisation/demandeBonPlan/".$oBonPlan->bonplan_id);} ?>" id="btn_127" class="Button1" style="width: 263px; height: 35px;"><span></span></a>
        <div id="art_186" style="width:155px;height:18px; z-index:1;  padding-left: 60px;    padding-top: 5px;    position: absolute;">
            <div class="Corps-artistique-P">
                <span class="Corps-artistique-C-C2"> <?php if($utilise ==1 && $bon_plan_utilise_plusieurs == 0) { echo "Vous aviez d&eacute;ja utilis&eacute; ce bon plan" ;} else { echo  "Je valide le bon plan"; } ?></span></div>
        </div>
        
        </td>
      </tr>
      <tr>
        <td height="40px">
        <a href="http://www.proximite-magazine.com/clubnv/fonctionnementbonsplans.html" id="btn_135" class="Button1" style="width: 263px; height: 35px;"><span></span></a>
<div id="art_1212" style="width:219px;height:18px;  padding-left: 40px;    padding-top: 5px;    position: absolute;">
    <div class="Corps-artistique-P">
        <span class="Corps-artistique-C-C2"><a href="http://www.proximite-magazine.com/clubnv/fonctionnementbonsplans.html" style="color: rgb(0, 0, 0); text-decoration: none;">Le fonctionnement du bon plan</a></span></div>
</div>
        </td>
      </tr>
      <tr>
        <td>&nbsp;
        </td>
      </tr>  
      <tr>
        <td>
        
<p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="<?php echo GetImagePath("front/"); ?>/wpbbcdd1ab_06.png" id="pic_1359" alt="" onload="OnLoadPngFix()" height="77" border="0" width="77"></td>
    <td>&nbsp;&nbsp;
    <td/>
    <td>
    <div id="txt_579" style="width:169px;height:96px;overflow:hidden;">
<p class="Corps-P-P0"><span class="Corps-C">Avant de pouvoir valider ce bon plan, vous devez être inscrit sur Privicarte.
    </span></p>
<p class="Corps-P-P0"><span class="Corps-C">L’adhésion est gratuite.</span></p>
<p class="Corps-P-P0"><span class="Corps-C">Votre identifiant et mot de passe vous sera demandé.</span></p>
</div></td>
  </tr>
</table>
</p>

        
        </td>
      </tr>
      <tr>
        <td height="40px">
        <a href="http://www.proximite-magazine.com/clubnv/fonctionnementclub.html" id="btn_136" class="Button1" style="width: 263px; height: 35px;"><span></span></a>
<div id="art_1213" style="width:147px;height:18px; padding-left:70px;    padding-top: 5px;    position: absolute;">
    <div class="Corps-artistique-P">
        <span class="Corps-artistique-C-C2"><a href="http://www.proximite-magazine.com/clubnv/fonctionnementclub.html" style="color: rgb(0, 0, 0); text-decoration: none;">Privicarte</a></span></div>
</div>
        </td>
      </tr>
      <tr>
        <td height="40px">
        <a href="<?php echo site_url("front/particuliers/inscription"); ?>" id="btn_137" class="Button1" style="width: 263px; height: 35px;"><span></span></a>
<div id="art_1214" style="width:98px;height:18px; padding-left:90px;    padding-top: 5px;    position: absolute;">
    <div class="Corps-artistique-P">
        <span class="Corps-artistique-C-C2"><a href="<?php echo site_url("front/particuliers/inscription"); ?>" style="color: rgb(0, 0, 0); text-decoration: none;">Je m’inscris</a></span></div>
</div>
        </td>
      </tr>
    </table>

    <br />
    </div>
</div>
<div style="width:291px; height:24px; overflow:hidden;background-image: url(<?php echo GetImagePath("front/"); ?>/wpaa3b69e4_06_fond.png); background-repeat:no-repeat;">
</div>

<div style="width:291px; height:24px;"></div>

<div id="bonplan_1" style="width:292px; height: auto; padding-top:40px; overflow:hidden; background-image: url(<?php echo GetImagePath("front/"); ?>/wpaa3b69e4_06_2.png); background-repeat:no-repeat;">
<center>
<img src="<?php echo GetImagePath("front/"); ?>/wpf1244832_06.png" id="pic_1360" alt="" onload="OnLoadPngFix()" height="156" border="0" width="262">
</center>
<div style="margin-left:5px; margin-right:5px;">
<p class="Corps-P-P1"><span class="Corps-C-C0">A chaque concrétisation d’un bon plan, vous bénéficiez de 10 points cadeaux, vous
    les cumulez et les transformez en cadeaux quand vous le souhaitez.</span></p>
<p class="Corps-P-P1"><span class="Corps-C-C0">(Les points et les cadeaux sont délivrés par Privicarte).</span></p>
</div>


<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-left:12px;">
  <tr>
    <td height="40">
    <a href="<?php echo site_url("front/cadeau"); ?>" id="btn_140" class="Button1" style="width: 263px; height: 35px;"><span></span></a>
<div id="art_1218" style="width:148px;height:18px;  padding-left: 60px;    padding-top: 5px;    position: absolute;">
    <div class="Corps-artistique-P">
        <span class="Corps-artistique-C-C2"><a href="<?php echo site_url("front/cadeau"); ?>" style="color: rgb(0, 0, 0); text-decoration: none;">Accès au catalogue</a></span></div>
</div>
    </td>
  </tr>
  <tr>
    <td height="40">
    <?php 
	$thisss =& get_instance();
	$thisss->load->library('ion_auth');
	$link_valider_points_cadeaux = '';
	if ($thisss->ion_auth->logged_in()) { 
		$link_valider_points_cadeaux = site_url("front/fidelisation");
	} else {
		$_SESSION['page_from'] = site_url("front/fidelisation");
		$link_valider_points_cadeaux = site_url("connexion");
	}
	?>
    <a href="<?php echo $link_valider_points_cadeaux; ?>" id="btn_141" class="Button1" style="width: 263px; height: 35px;"><span></span></a>
<div id="art_1219" style="width:194px;height:18px;  padding-left: 60px;    padding-top: 5px;    position: absolute;">
    <div class="Corps-artistique-P">
        <span class="Corps-artistique-C-C2"><a href="<?php echo $link_valider_points_cadeaux; ?>" style="color: rgb(0, 0, 0); text-decoration: none;">Valider des points cadeaux</a></span></div>
</div>
    </td>
  </tr>
</table>
<br />

</div>
<div style="width:291px; height:24px; overflow:hidden;background-image: url(<?php echo GetImagePath("front/"); ?>/wpaa3b69e4_06_fond.png); background-repeat:no-repeat;">
</div>

<div style="width:291px; height:24px;"></div>

<div id="txt_632" style="width:290px;height: auto;overflow:hidden; text-align:center;">
<!--start main footer menu-->
<?php $this->load->view("front/vwMenumainfooterMobile", $data);?>
<!--end main footer menu-->
</div>

</div>
<?php }
}?>









</div>

</body></html>

