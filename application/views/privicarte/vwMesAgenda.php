<?php //$data["zTitle"] = 'Agenda'; ?>
<?php //$this->load->view("adminAout2013/includes/vwHeader2013", $data); ?>

<?php
$data['empty'] = null;
$this->load->view("sortez/includes/backoffice_pro_css", $data);
?>
<?php
function SelectOption($prmValue, $prmCurrent) {
    if ($prmValue == $prmCurrent) {
        echo "selected";
    }
}

function EchoWithHighLightWords($prmValue, $prmSearchedWords) {
    $out = $prmValue;
    for ($i = 0; $i < sizeof($prmSearchedWords); $i ++) {
        $out = word_limiter(highlight_phrase($out, $prmSearchedWords[$i], "<span style='background: #FFFF00;'>", "</span>"),10) ;
    }
    echo $out;
}
?>
<script type="text/javascript" charset="utf-8">
	// $(function() {
	// 		$(".tablesorter")
	// 			.tablesorter({widthFixed: true, widgets: ['zebra'], headers: {7: { sorter: false}, 8: {sorter: false} }})
	// 			.tablesorterPager({container: $("#pager")});
	// 	});
		
	function save_order_partner() {
			$('#spanOrderPartner').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
			$.ajax({
				url: '<?php echo base_url();?>front/utilisateur/save_order_partner/',
				data: { 
					<?php foreach($toListeAgenda as $oListeAgenda){ ?>
					inputOrderPartner_<?php echo $oListeAgenda->id; ?>: $('#inputOrderPartner_<?php echo $oListeAgenda->id; ?>').val(), 
					<?php } ?>
					idCommercant: <?php echo $idCommercant; ?>
				},
				dataType: 'html',
				type: 'POST',
				async: true,
				success: function(data){
					$('#spanOrderPartner').html('<img src="<?php echo GetImagePath("front/");?>/btn_new/icon-16-checkin.png" />');
					//window.location.reload();
				}
			});
		}
		
		
</script>
<!-- <form method="post" action="<?php// echo site_url();?>front/bonplan/save_icon_bnp/<? //echo $idCommercant?>" class="row pb-5 shadowed p-2 gli_content d-flex">
	
<div class="row pb-4">
    <div class="col-lg-3"></div>
    <div class="col-sm-3 text-right">
        <input onclick="set_value_field()" type="checkbox" id="is_actif_affichBonplan" name="" <?php //if (isset($infocom) AND $infocom->is_actif_affichBonplan =='1')echo 'checked';?>>
        <input type="hidden" id="is_actif_affichBonplan_value" name="data_bnp[is_actif_affichBonplan]">
        Je veux que le bouton "Agenda" s'affiche sur mon menu de droite
    </div>
    <div class="col-lg-3 text-left">
        <img src="<?php //echo base_url('assets/img/boutique_link.jpg')?>">
    </div>
    <div class="col-lg-3"></div>
</div>

 <div class="col-4 text-center">

        choisissez la couleur du bouton en cliquant sur le panel 
        <input type="color" name="data_bnp[bnp_color]" style="width:150px; height: 150px"  value="<?php// if (isset($infocom->bnp_color)) echo $infocom->bnp_color; ?>">
    </div>
   <div class="col-4">

        <?php //if (isset($infocom) AND $infocom!=null){?>
        <div class="w-100 text-center top_menu_gen">
         Télécharger l'image de personnalisation, Cliquez sur l'icone !...
        </div><?//}else{?><div class="w-100 text-center top_menu_gen">
          Veuiller enregistré la couleur et le titre avant de Télécharger l'image de personnalisation en Cliquez sur l'icone !...
        </div><?//}?>

        <div class="w-100 text-center" id="img_menu_gen_contents">
            <?php //if (isset($infocom->bnp_icon) AND $infocom->bnp_icon !=null && is_file("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/ag_menu/".$infocom->bnp_icon)){ ?>
                <div class="w-100">
                    <img style="max-width: 200px" src="<?php //echo base_url()?>application/resources/front/photoCommercant/imagesbank/<?php //echo $infocom->user_ionauth_id; ?>/ag_menu/<?php// echo $infocom->bnp_icon ?>">
                    <a onclick="delete_jq_bnp_icon(<?php// echo $infocom->IdCommercant; ?>)" class="btn btn-danger mt-3">Supprimer image</a>
                </div>
            <?php //}else{ ?>
                <div class="w-100" id="Menuphoto_menu_gen_container">
                    <img onclick='javascript:window.open("<?php //echo site_url("media/index/".$data_ag->nom_societe."-bnp_icon-photo_menu_gen"); ?>", "", "width=1045, height=675, scrollbars=yes");' src="<?php //echo base_url()?>assets/image/download-icon-png-5.jpg">
                </div>
                <div id="Articleimg_menuphoto_menu_gen_container"></div>
            <?php //} ?>
        </div>
    </div>

    <div class="col-4">
        <div class="w-100 top_menu_gen">
            Précisez le texte
            du bouton
        </div>
        <div class="w-100 text-center menu_txt_container">
            <input maxlength="22" value="<?php //if (isset($infocom->bnp_menu)) echo $infocom->bnp_menu; ?>" type="text" class="form-control input_menu_txt" name="data_bnp[bnp_menu]">
            <?php //if($error_message!=NULL):?>
           <div class="alert alert-danger mt-2" role="alert">
                <strong>Erreur ! </strong><?php //print_r($error_message);?>
            </div>
            <?php //endif;?>
        </div>
    </div>
    <div class="w-100 text-center pt-4 pb-4">
        <input type="submit" class="btn btn-primary" value="Enregistrer"/>
    </div>
</form> -->

    <div class="col-lg-12 padding0" style="width:100%; text-align:center;padding-bottom: 15px !important;">
        <div class="col-sm-6 padding0 textalignleft">
            <h1>Mon Agenda</h1>
        </div>
        <div class="col-sm-6 padding0 textalignright">
            <button class="btn btn-primary" onclick="document.location='<?php echo base_url();?>';">Retour site</button>
            <button class="btn btn-primary" onclick="document.location='<?php echo site_url("front/utilisateur/contenupro") ;?>';">Retour au menu</button>
            <?php if (isset($limit_agenda_add) && $limit_agenda_add==1) {
				echo "<span style='color: #FF0000;'>";
                echo "Vous avez atteint vos limites d'agenda : ";
                if (isset($objCommercant->limit_agenda)) echo $objCommercant->limit_agenda;
                echo '</span>';
            } else {?>
            <button class="btn btn-success" onclick="document.location='<?php echo site_url("front/agendas/fiche/$idCommercant/") ;?>';">Ajouter un événement</button> <form methode='get' action="search.php"></form></div>
        <form id="frmSearch" method="POST" action="<?php echo site_url(); ?>front/utilisateur/agenda/<?php echo $idCommercant ; ?>/<?php if (isset($FilterCol)) echo $FilterCol ; ?>/<?php if (isset($FilterValue)) echo $FilterValue ; ?>/0">

            <form methode='get' action="search.php">
                Recherche : <input type="text" name="txtSearch" id="txtSearch" value="<?php echo $SearchValue; ?>" />&nbsp;
                <input type="submit" name="btnSearch" class="CommandButton" value="GO" id="btnSearch" />
            </form>
            <?php }?>
        </div>
    </div>

    <div id="divMesAnnonces" class="content" align="center" style="display: table;">
        <form name="frmMesAnnonces" id="frmMesAnnonces" action="" method="POST" enctype="multipart/form-data">
			
				
				<div id="container">
					<table id="example" cellpadding="1" class="tablesorter">
						<thead>
							<tr>                    
								<th>Titre</th>
								<th>Description</th>
								<th>Date debut</th>
								<th>Date fin</th>
								<th>Date ajout</th>
								<th>A la Une</th>
								<th>Etat</th>
                                <th>Ordre&nbsp;<a href="javascript:void(0);" id="saveOrderPartner" title="Enregistrer" onclick="javascript:save_order_partner();"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/saveOrderPartner.png"></a>&nbsp;<span id="spanOrderPartner"></span></th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
                        <?php if (count($toListeAgenda)>0) { ?>
                        	<?php $i_order_partner = 1; ?>
							<?php foreach($toListeAgenda as $oListeAgenda){ ?>
								<tr>
									<td><?php if ($highlight == "yes") { EchoWithHighLightWords($oListeAgenda->nom_manifestation, $SearchedWords); } else { echo word_limiter(highlight_phrase(preg_replace("/<[^>]*>/", "",$oListeAgenda->nom_manifestation), $SearchValue, "<span style='background: #FFFF00;'>", "</span>"),10);  } ?></td>
									<td><?php echo truncate(strip_tags($oListeAgenda->description),150," ..."); ?></td>
									<td><?php 
									if (isset($oListeAgenda->date_debut) && convertDateWithSlashes($oListeAgenda->date_debut)!="00/00/0000")
									echo convert_Sqldate_to_Frenchdate($oListeAgenda->date_debut) ; ?></td>
									<td><?php 
									if (isset($oListeAgenda->date_fin) && convertDateWithSlashes($oListeAgenda->date_fin)!="00/00/0000")
									echo convert_Sqldate_to_Frenchdate($oListeAgenda->date_fin) ; ?></td>
									<td><?php 
									if (isset($oListeAgenda->date_depot) && convertDateWithSlashes($oListeAgenda->date_depot)!="00/00/0000")
									echo convert_Sqldate_to_Frenchdate($oListeAgenda->date_depot) ; ?></td>
									<td><?php 
									//echo $oListeAgenda->alaune; 
									if ($oListeAgenda->alaune != "1") echo  "Non"; else echo "Oui";
									?></td>
									<td><?php 
									//echo $oListeAgenda->IsActif; 
									if ($oListeAgenda->IsActif == "1") echo  "Activé";
									if ($oListeAgenda->IsActif == "0") echo  "A valider";
									if ($oListeAgenda->IsActif == "2") echo  "Annulé";
									if ($oListeAgenda->IsActif == "3") echo  "Supprimé";
									?></td>
                                    <td><input name="inputOrderPartner_<?php echo $oListeAgenda->id;?>" id="inputOrderPartner_<?php echo $oListeAgenda->id;?>" type="text" size="3" maxlength="3" value="<?php if (isset($oListeAgenda->order_partner) && $oListeAgenda->order_partner != "" && $oListeAgenda->order_partner != NULL) echo $oListeAgenda->order_partner; else echo $i_order_partner; ?>"/></td>
									<td><a href="<?php echo site_url("front/agendas/fiche/" . $oListeAgenda->IdCommercant . "/" . $oListeAgenda->id ) ; ?>"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/update_ico.png"></a></td>
									<td><a href="<?php echo site_url("front/utilisateur/deleteAgenda/" . $oListeAgenda->id . "/" . $oListeAgenda->IdCommercant) ; ?>" onclick="if (!confirm('voulez-vous vraiment supprimer cette événnement?')){ return false ; }"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/delete_ico.png"></a></td>
								</tr>
                                <?php if (isset($oListeAgenda->order_partner) && $oListeAgenda->order_partner != "" && $oListeAgenda->order_partner != NULL) {} else $i_order_partner = $i_order_partner + 1; ?>
							<?php } ?>
                        <?php } ?>
						</tbody>
					</table>
<!--					<div id="pager" class="pager" style="text-align:center;">-->
<!--							<img src="--><?php //echo GetImagePath("front/"); ?><!--/first.png" class="first"/>-->
<!--							<img src="--><?php //echo GetImagePath("front/"); ?><!--/prev.png" class="prev"/>-->
<!--							<input type="text" class="pagedisplay"/>-->
<!--							<img src="--><?php //echo GetImagePath("front/"); ?><!--/next.png" class="next"/>-->
<!--							<img src="--><?php //echo GetImagePath("front/"); ?><!--/last.png" class="last"/>-->
<!--							<select class="pagesize" style="visibility:hidden">-->
<!--								<option selected="selected"  value="10">10</option>-->
<!--								<option value="20">20</option>-->
<!--								<option value="30">30</option>-->
<!--								<option  value="40">40</option>-->
<!--							</select>-->
<!--					</div>-->
                    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
                    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
                    <script type="text/javascript">
                        $( document ).ready(function() {
                            $(document).ready(function() {
                                $('#example').DataTable( {
                                    "pagingType": "full_numbers"
                                } );
                            } );
                        });
                    </script>
				</div>
        </form>
    </div>
<?php //$this->load->view("adminAout2013/includes/vwFooter2013"); ?>