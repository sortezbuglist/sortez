<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("sortez/includes/header_backoffice_com", $data); ?>

    <style type="text/css">
        .title_agena_perso {
            color: #ffffff;
            font-family: "Arial",sans-serif;
            font-size: 15px;
            font-weight: bold;
            line-height: 1.2em;
            background-color:#45B69E;
            height: 40px;
            padding-left: 15px;
            padding-right: 15px;
            padding-top: 8px;
        }
        .btn_back_menu, .btn_back_menu:hover, .btn_back_menu:focus, .btn_back_menu:active {
            color: #ffffff;
            text-decoration:none; font-weight:bold; font-size:16px; padding:10px 100px; background-color:#45B69E; margin-bottom:40px;
        }
    </style>

    <script type="text/javascript">
        // move data commune **************************************************
        function commune_left_to_right(){
            $("#perso_commune_existant > option:selected").each(function(){
                $(this).remove().appendTo("#perso_commune_send");
            });
        }
        function commune_right_to_left(){
            $("#perso_commune_send > option:selected").each(function(){
                $(this).remove().appendTo("#perso_commune_existant");
            });
        }
        function commune_left_to_right_all(){
            $("#perso_commune_existant > option").each(function(){
                $(this).remove().appendTo("#perso_commune_send");
            });
        }
        function commune_right_to_left_all(){
            $("#perso_commune_send > option").each(function(){
                $(this).remove().appendTo("#perso_commune_existant");
            });
        }
        // move data deposant **************************************************
        function deposant_left_to_right(){
            $("#perso_deposant_existant > option:selected").each(function(){
                $(this).remove().appendTo("#perso_deposant_send");
            });
        }
        function deposant_right_to_left(){
            $("#perso_deposant_send > option:selected").each(function(){
                $(this).remove().appendTo("#perso_deposant_existant");
            });
        }
        function deposant_left_to_right_all(){
            $("#perso_deposant_existant > option").each(function(){
                $(this).remove().appendTo("#perso_deposant_send");
            });
        }
        function deposant_right_to_left_all(){
            $("#perso_deposant_send > option").each(function(){
                $(this).remove().appendTo("#perso_deposant_existant");
            });
        }
        // move data categ **************************************************
        function categ_left_to_right(){
            $("#perso_categ_existant > option:selected").each(function(){
                $(this).remove().appendTo("#perso_categ_send");
            });
        }
        function categ_right_to_left(){
            $("#perso_categ_send > option:selected").each(function(){
                $(this).remove().appendTo("#perso_categ_existant");
            });
        }
        function categ_left_to_right_all(){
            $("#perso_categ_existant > option").each(function(){
                $(this).remove().appendTo("#perso_categ_send");
            });
        }
        function categ_right_to_left_all(){
            $("#perso_categ_send > option").each(function(){
                $(this).remove().appendTo("#perso_categ_existant");
            });
        }

        $(document).ready(function() {

            $("#choix_dossier_perso_check").click(function(){
                if ($('#choix_dossier_perso_check').attr('checked')) {
                    $("#choix_dossier_perso_value").val("1");
                } else {
                    $("#choix_dossier_perso_value").val("0");
                }
                $("#form_choix_dossier_perso_check").submit();
            });

            $(".check_couleur_txtblackwhite_btn").click(function(){
                if ($('.check_couleur_txtblackwhite_btn').attr('checked')) {
                    $("#perso_couleur_txtblackwhite_btn").val("1");
                } else {
                    $("#perso_couleur_txtblackwhite_btn").val("0");
                }
            });

            $("#btn_generer_code").click(function(){

                var perso_commune_send = "";
                $('#perso_commune_send option').each(function() {
                    perso_commune_send = perso_commune_send + $(this).val()+"_";
                });
                var perso_deposant_send = "";
                $('#perso_deposant_send option').each(function() {
                    perso_deposant_send = perso_deposant_send + $(this).val()+"_";
                });
                var perso_categ_send = "";
                $('#perso_categ_send option').each(function() {
                    perso_categ_send = perso_categ_send + $(this).val()+"_";
                });
                var perso_ville_send = "";
                $('#perso_ville_send option').each(function() {
                    perso_ville_send = perso_ville_send + $(this).val()+"_";
                });

                var perso_couleur_cadre = $("#perso_couleur_cadre").val();
                var perso_couleur_titre = $("#perso_couleur_titre").val();
                var perso_couleur_btn = $("#perso_couleur_btn").val();
                var perso_couleur_bg_btn = $("#perso_couleur_bg_btn").val();
                var perso_couleur_txtblackwhite_btn = $("#perso_couleur_txtblackwhite_btn").val();

                var choix_dossier_perso_value = $("#choix_dossier_perso_value").val();
                var dossier_perso_idCommercant = "";
                if (choix_dossier_perso_value == "1") {
                    dossier_perso_idCommercant = $("#dossier_perso_idCommercant").val();
                } else  {
                    dossier_perso_idCommercant = "0";
                }

                var result_to_show = "";

                var iframeURL = '<?php echo base_url(); ?>lannuaire/annuaire_perso/?' ;
                iframeURL += 'zCouleur='+perso_couleur_cadre;
                iframeURL += '&zCouleurTitre='+perso_couleur_titre;
                iframeURL += '&zCouleurTextBouton='+perso_couleur_btn;
                iframeURL += '&zCouleurBgBouton='+perso_couleur_bg_btn;
                iframeURL += '&zCouleurNbBtn='+perso_couleur_txtblackwhite_btn;
                //iframeURL += '&zHost=<?php //echo base_url(); ?>' ;
                iframeURL += '&tiDepartement='+perso_commune_send ;
                iframeURL += '&tiCategorie='+perso_categ_send;
                iframeURL += '&tiVille='+perso_ville_send;



                result_to_show = "\<script type=\"text/javascript\"\> var iframeURL=\'"+iframeURL+"\'\;document.write(\'<iframe style=\"width:650px;height:800px;\" src=\'+ iframeURL +\' framespacing=0 frameborder=0 border=0></iframe>\');\<\/script\>";

                $("#txt_code_valide").val(result_to_show);
            });


            $("#btn_page_test").click(function(){

                var perso_commune_send = "";
                $('#perso_commune_send option').each(function() {
                    perso_commune_send = perso_commune_send + $(this).val()+"_";
                });
                var perso_ville_send = "";
                $('#perso_ville_send option').each(function() {
                    perso_ville_send = perso_ville_send + $(this).val()+"_";
                });
                var perso_categ_send = "";
                $('#perso_categ_send option').each(function() {
                    perso_categ_send = perso_categ_send + $(this).val()+"_";
                });

                var perso_couleur_cadre = $("#perso_couleur_cadre").val();
                var perso_couleur_titre = $("#perso_couleur_titre").val();
                var perso_couleur_btn = $("#perso_couleur_btn").val();
                var perso_couleur_bg_btn = $("#perso_couleur_bg_btn").val();
                var perso_couleur_txtblackwhite_btn = $("#perso_couleur_txtblackwhite_btn").val();

                var choix_dossier_perso_value = $("#choix_dossier_perso_value").val();
                var dossier_perso_idCommercant = "";
                if (choix_dossier_perso_value == "1") {
                    dossier_perso_idCommercant = $("#dossier_perso_idCommercant").val();
                } else  {
                    dossier_perso_idCommercant = "0";
                }


                var iframeURL = '<?php echo base_url(); ?>lannuaire/annuaire_perso/?' ;
                iframeURL += 'zCouleur='+perso_couleur_cadre;
                iframeURL += '&zCouleurTitre='+perso_couleur_titre;
                iframeURL += '&zCouleurTextBouton='+perso_couleur_btn;
                iframeURL += '&zCouleurBgBouton='+perso_couleur_bg_btn;
                iframeURL += '&zCouleurNbBtn='+perso_couleur_txtblackwhite_btn;
                //iframeURL += '&zHost=<?php //echo base_url(); ?>' ;
                iframeURL += '&tiDepartement='+perso_commune_send ;
                iframeURL += '&tiVille='+perso_ville_send ;
                iframeURL += '&tiCategorie='+perso_categ_send;


                window.open(iframeURL);


            });


            $("#btn_validation_depot").click(function(){

                var perso_ville_send = "";
                $('#perso_ville_send option').each(function() {
                    perso_ville_send = perso_ville_send + $(this).val()+"_";
                });
                $("#commercants_IdVille").val(perso_commune_send);

                var perso_departement_send = "";
                $('#perso_departement_send option').each(function() {
                    perso_departement_send = perso_departement_send + $(this).val()+"_";
                });
                $("#commercants_departement_id").val(perso_departement_send);

                var perso_categ_send = "";
                $('#perso_categ_send option').each(function() {
                    perso_categ_send = perso_categ_send + $(this).val()+"_";
                });
                $("#commercants_idCommercant").val(perso_categ_send);

                $("#txt_code_valide").val(base64_encode($("#txt_code_valide").val()));


                //alert(base64_encode($("#txt_code_valide").val()));

                $("#form_agenda_perso_submit").submit();

            });



        });

        function base64_encode (data) {
            // http://kevin.vanzonneveld.net
            // +   original by: Tyler Akins (http://rumkin.com)
            // +   improved by: Bayron Guevara
            // +   improved by: Thunder.m
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Pellentesque Malesuada
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   improved by: Rafał Kukawski (http://kukawski.pl)
            // *     example 1: base64_encode('Kevin van Zonneveld');
            // *     returns 1: 'S2V2aW4gdmFuIFpvbm5ldmVsZA=='
            // mozilla has this native
            // - but breaks in 2.0.0.12!
            //if (typeof this.window['btoa'] === 'function') {
            //    return btoa(data);
            //}
            var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
            var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
                ac = 0,
                enc = "",
                tmp_arr = [];

            if (!data) {
                return data;
            }

            do { // pack three octets into four hexets
                o1 = data.charCodeAt(i++);
                o2 = data.charCodeAt(i++);
                o3 = data.charCodeAt(i++);

                bits = o1 << 16 | o2 << 8 | o3;

                h1 = bits >> 18 & 0x3f;
                h2 = bits >> 12 & 0x3f;
                h3 = bits >> 6 & 0x3f;
                h4 = bits & 0x3f;

                // use hexets to index into b64, and append result to encoded string
                tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
            } while (i < data.length);

            enc = tmp_arr.join('');

            var r = data.length % 3;

            return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);

        }



    </script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:center; margin-bottom:50px;">
        <tr>
            <td>
                <h1 style="display: table; width: 100%; line-height: 60px; padding-bottom: 15px;">
                    Exportation annuaire
                </h1>
                <div style="display: table; width: 100%;">
                    <a class="btn_back_menu btn" href="<?php echo site_url("admin") ;?>" style="padding: 10px 165px; margin-bottom: 0;">Retour menu sortez admin</a>
                </div>
            </td>
        </tr>
    </table>

    <form id="form_agenda_perso_submit" name="form_agenda_perso_submit" action="<?php echo base_url();?>admin/annuaire/personnalisation_save" method="post" enctype="multipart/form-data">
        <input name="agenda_perso[id]" id="agenda_perso_id" type="hidden" value="<?php if (isset($oAgenda_perso->id) && $oAgenda_perso->id!="0") echo $oAgenda_perso->id; else echo "0";?>">

        <div class="title_agena_perso">Choisir les Villes</div>

        <div style="height:auto;">

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="290">
                        <select name="perso_commune_existant" id="perso_commune_existant" size="10" multiple style="width:290px;">
                            <?php foreach($toVille as $oVille){ ?>
                                <option value="<?php echo $oVille->IdVille ; ?>"><?php echo $oVille->Nom; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                    <td width="50" style="text-align:center;">
                        <input name="perso_commune_btn_left_to_right" id="perso_commune_btn_left_to_right" type="button" value=">" onClick="javascript:commune_left_to_right();"><br/>
                        <input name="perso_commune_btn_right_to_left" id="perso_commune_btn_right_to_left" type="button" value="<" onClick="javascript:commune_right_to_left();"><br/>
                        <input name="perso_commune_btn_left_to_right_all" id="perso_commune_btn_left_to_right_all" type="button" value=">>>" onClick="javascript:commune_left_to_right_all();"><br/>
                        <input name="perso_commune_btn_right_to_left_all" id="perso_commune_btn_right_to_left_all" type="button" value="<<<" onClick="javascript:commune_right_to_left_all();">
                    </td>
                    <td width="290" style="text-align:right">
                        <select name="perso_commune_send" id="perso_commune_send" size="10" multiple style="width:290px;">
                            <?php

                            ?>
                        </select>
                        <input name="agenda_perso[idCommune]" id="agenda_perso_idCommune" type="hidden" value="">
                    </td>
                </tr>
            </table>

        </div>

        <div class="title_agena_perso">Choisir les departement</div>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="290">
                    <select name="perso_deposant_existant" id="perso_deposant_existant" size="10" multiple style="width:290px;">
                        <?php foreach($toDepartement as $oDepartement){ ?>
                            <option value="<?php echo $oDepartement->departement_id ; ?>"><?php echo $oDepartement->departement_nom; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td width="50" style="text-align:center;">
                    <input name="perso_deposant_btn_left_to_right" id="perso_deposant_btn_left_to_right" type="button" value=">" onClick="javascript:deposant_left_to_right();"><br/>
                    <input name="perso_deposant_btn_right_to_left" id="perso_deposant_btn_right_to_left" type="button" value="<" onClick="javascript:deposant_right_to_left();"><br/>
                    <input name="perso_deposant_btn_left_to_right_all" id="perso_deposant_btn_left_to_right_all" type="button" value=">>>" onClick="javascript:deposant_left_to_right_all();"><br/>
                    <input name="perso_deposant_btn_right_to_left_all" id="perso_deposant_btn_right_to_left_all" type="button" value="<<<" onClick="javascript:deposant_right_to_left_all();">
                </td>
                <td width="290" style="text-align:right">
                    <select name="perso_deposant_send" id="perso_deposant_send" size="10" multiple style="width:290px;">

                    </select>
                    <input name="agenda_perso[idDeposant]" id="agenda_perso_idDeposant" type="hidden" value="">
                </td>
            </tr>
        </table>

        <div class="title_agena_perso">Choisir les catégories</div>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="290">
                    <select name="perso_categ_existant" id="perso_categ_existant" size="10" multiple style="width:290px;">
                        <?php foreach($toCategorie_principale as $oCategorie_principale){ ?>
                            <option value="<?php echo $oCategorie_principale->IdRubrique ; ?>"><?php echo ucfirst(strtolower($oCategorie_principale->Nom)) ; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td width="50" style="text-align:center;">
                    <input name="perso_categ_btn_left_to_right" id="perso_categ_btn_left_to_right" type="button" value=">" onClick="javascript:categ_left_to_right();"><br/>
                    <input name="perso_categ_btn_right_to_left" id="perso_categ_btn_right_to_left" type="button" value="<" onClick="javascript:categ_right_to_left();"><br/>
                    <input name="perso_categ_btn_left_to_right_all" id="perso_categ_btn_left_to_right_all" type="button" value=">>>" onClick="javascript:categ_left_to_right_all();"><br/>
                    <input name="perso_categ_btn_right_to_left_all" id="perso_categ_btn_right_to_left_all" type="button" value="<<<" onClick="javascript:categ_right_to_left_all();">
                </td>
                <td width="290" style="text-align:right">
                    <select name="perso_categ_send" id="perso_categ_send" size="10" multiple style="width:290px;">
                        <?php
                        if (isset($oAgenda_perso->idCategorie) && $oAgenda_perso->idCategorie!="" && $oAgenda_perso->idCategorie!=NULL) {
                            $oAgenda_perso_idCategorie = substr_replace($oAgenda_perso->idCategorie ,"",-1);
                            $oAgenda_perso_idCategorie_array = explode("_", $oAgenda_perso_idCategorie);
                            for ($k=0;$k<count($oAgenda_perso_idCategorie_array);$k++) {
                                $this->load->model("mdl_categories_agenda");
                                $oAgenda_perso_idCommune_categorie = $this->mdl_categories_agenda->getById($oAgenda_perso_idCategorie_array[$k]);
                                echo '<option value="'.$oAgenda_perso_idCategorie_array[$k].'">'.$oAgenda_perso_idCommune_categorie->category.'</option>';
                            }
                        }
                        ?>
                    </select>
                    <input name="agenda_perso[idCategorie]" id="agenda_perso_idCategorie" type="hidden" value="">
                </td>
            </tr>
        </table>


            <div class="title_agena_perso">Définir les couleurs de votre agenda </div>

            <div>
                <p>Définir la couleur des cadres et boutons </p>
                <p>Cliquez sur le champ indiquant le code couleur, sélectionnez la couleur dans le panneau puis validez en cliquant sur l'icône en bas à droite du panneau de sélection de couleur</p>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>Couleur de fond :</td>
                        <td>
                            <!--<input name="perso_couleur_cadre" id="perso_couleur_cadre" type="text">-->


                            <link rel="stylesheet" href="<?php echo GetJsPath("front/") ; ?>/colorPicker/colorpicker.css" type="text/css" />
                            <!--<link rel="stylesheet" media="screen" type="text/css" href="<?php// echo GetJsPath("front/") ; ?>/colorPicker/layout.css" />-->
                            <style type="text/css">
                                #colorSelector_perso_couleur_cadre,
                                #colorSelector_perso_couleur_titre,
                                #colorSelector_perso_couleur_bg_btn,
                                #colorSelector_perso_couleur_btn {
                                    position: relative;
                                    width: 36px;
                                    height: 36px;
                                    /*background: url(<?php// echo GetJsPath("front/") ; ?>/colorPicker/select2.png);*/
                                }
                                #colorSelector_perso_couleur_cadre div,
                                #colorSelector_perso_couleur_titre div,
                                #colorSelector_perso_couleur_bg_btn div,
                                #colorSelector_perso_couleur_btn div {
                                    position: relative;
                                    width: 28px;
                                    height: 28px;
                                    background: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/select2.png) center;
                                }
                                #colorpickerHolder_perso_couleur_cadre,
                                #colorpickerHolder_perso_couleur_titre,
                                #colorpickerHolder_perso_couleur_bg_btn,
                                #colorpickerHolder_perso_couleur_btn {
                                    width: 356px;
                                    height: 0;
                                    overflow: hidden;
                                    position: relative;
                                }
                                #colorpickerHolder_perso_couleur_cadre .colorpicker,
                                #colorpickerHolder_perso_couleur_titre .colorpicker,
                                #colorpickerHolder_perso_couleur_bg_btn .colorpicker,
                                #colorpickerHolder_perso_couleur_btn .colorpicker  {
                                    background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_background.png);
                                    position: relative;
                                    bottom: 0;
                                    left: 0;
                                }
                                #colorpickerHolder_perso_couleur_cadre .colorpicker_hue div,
                                #colorpickerHolder_perso_couleur_titre .colorpicker_hue div,
                                #colorpickerHolder_perso_couleur_bg_btn .colorpicker_hue div,
                                #colorpickerHolder_perso_couleur_btn .colorpicker_hue div {
                                    background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_indic.gif);
                                }
                                #colorpickerHolder_perso_couleur_cadre .colorpicker_hex,
                                #colorpickerHolder_perso_couleur_titre .colorpicker_hex,
                                #colorpickerHolder_perso_couleur_bg_btn .colorpicker_hex,
                                #colorpickerHolder_perso_couleur_btn .colorpicker_hex {
                                    background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_hex.png);
                                }
                                #colorpickerHolder_perso_couleur_cadre .colorpicker_rgb_r,
                                #colorpickerHolder_perso_couleur_titre .colorpicker_rgb_r,
                                #colorpickerHolder_perso_couleur_bg_btn .colorpicker_rgb_r,
                                #colorpickerHolder_perso_couleur_btn .colorpicker_rgb_r {
                                    background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_rgb_r.png);
                                }
                                #colorpickerHolder_perso_couleur_cadre .colorpicker_rgb_g,
                                #colorpickerHolder_perso_couleur_titre .colorpicker_rgb_g,
                                #colorpickerHolder_perso_couleur_bg_btn .colorpicker_rgb_g,
                                #colorpickerHolder_perso_couleur_btn .colorpicker_rgb_g {
                                    background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_rgb_g.png);
                                }
                                #colorpickerHolder_perso_couleur_cadre .colorpicker_rgb_b,
                                #colorpickerHolder_perso_couleur_titre .colorpicker_rgb_b,
                                #colorpickerHolder_perso_couleur_bg_btn .colorpicker_rgb_b,
                                #colorpickerHolder_perso_couleur_btn .colorpicker_rgb_b {
                                    background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_rgb_b.png);
                                }
                                #colorpickerHolder_perso_couleur_cadre .colorpicker_hsb_s,
                                #colorpickerHolder_perso_couleur_titre .colorpicker_hsb_s,
                                #colorpickerHolder_perso_couleur_bg_btn .colorpicker_hsb_s,
                                #colorpickerHolder_perso_couleur_btn .colorpicker_hsb_s {
                                    background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_hsb_s.png);
                                    display: none;
                                }
                                #colorpickerHolder_perso_couleur_cadre .colorpicker_hsb_h,
                                #colorpickerHolder_perso_couleur_titre .colorpicker_hsb_h,
                                #colorpickerHolder_perso_couleur_bg_btn .colorpicker_hsb_h,
                                #colorpickerHolder_perso_couleur_btn .colorpicker_hsb_h {
                                    background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_hsb_h.png);
                                    display: none;
                                }
                                #colorpickerHolder_perso_couleur_cadre .colorpicker_hsb_b,
                                #colorpickerHolder_perso_couleur_titre .colorpicker_hsb_b,
                                #colorpickerHolder_perso_couleur_bg_btn .colorpicker_hsb_b,
                                #colorpickerHolder_perso_couleur_btn .colorpicker_hsb_b {
                                    background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_hsb_b.png);
                                    display: none;
                                }
                                #colorpickerHolder_perso_couleur_cadre .colorpicker_submit,
                                #colorpickerHolder_perso_couleur_titre .colorpicker_submit,
                                #colorpickerHolder_perso_couleur_bg_btn .colorpicker_submit,
                                #colorpickerHolder_perso_couleur_btn .colorpicker_submit {
                                    background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_submit.png);
                                }
                                #colorpickerHolder_perso_couleur_cadre .colorpicker input,
                                #colorpickerHolder_perso_couleur_titre .colorpicker input,
                                #colorpickerHolder_perso_couleur_bg_btn .colorpicker input,
                                #colorpickerHolder_perso_couleur_btn .colorpicker input {
                                    color: #778398;
                                }
                                #customWidget {
                                    position: relative;
                                    height: 36px;
                                }

                            </style>
                            <script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/colorPicker/colorpicker.js"></script>
                            <script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/colorPicker/eye.js"></script>
                            <!--<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/colorPicker/layout.js?ver=1.0.2"></script>-->
                            <script type="text/javascript">
                                (function($){
                                    var initLayout = function() {
                                        var hash = window.location.hash.replace('#', '');
                                        var currentTab = $('ul.navigationTabs a')
                                            .bind('click', showTab)
                                            .filter('a[rel=' + hash + ']');
                                        if (currentTab.size() == 0) {
                                            currentTab = $('ul.navigationTabs a:first');
                                        }
                                        showTab.apply(currentTab.get(0));

                                        var widt = false;

                                        //colorpickerHolder2******************************************
                                        $('#colorpickerHolder2').ColorPicker({
                                            flat: true,
                                            color: '#00ff00',
                                            onSubmit: function(hsb, hex, rgb) {
                                                $('#colorSelector2 div').css('backgroundColor', '#' + hex);
                                                $('#bandeau_colorSociete').val('#' + hex);
                                                $('#colorpickerHolder2').stop().animate({height:0}, 500); widt = !widt;
                                            }
                                        });
                                        $('#colorpickerHolder2>div').css('position', 'absolute');

                                        $('#colorSelector2').bind('click', function() {
                                            $('#colorpickerHolder2').stop().animate({height: widt ? 0 : 173}, 500);
                                            widt = !widt;
                                        });
                                        //colorpickerHolder_perso_couleur_cadre******************************************
                                        $('#colorpickerHolder_perso_couleur_cadre').ColorPicker({
                                            flat: true,
                                            color: "<?php if(isset($oAgenda_perso->couleur_cadre)) echo '#'.$oAgenda_perso->couleur_cadre; else echo '#FFFFFF'; ?>",
                                            onSubmit: function(hsb, hex, rgb) {
                                                $('#colorSelector_perso_couleur_cadre div').css('backgroundColor', '#' + hex);
                                                $('#perso_couleur_cadre').val(hex);
                                                $('#colorpickerHolder_perso_couleur_cadre').stop().animate({height:0}, 500); widt = !widt;
                                            }
                                        });
                                        $('#colorpickerHolder_perso_couleur_cadre>div').css('position', 'absolute');

                                        $('#colorSelector_perso_couleur_cadre').bind('click', function() {
                                            $('#colorpickerHolder_perso_couleur_cadre').stop().animate({height: widt ? 0 : 173}, 500);
                                            widt = !widt;
                                        });
                                        //colorpickerHolder_perso_couleur_titre******************************************
                                        $('#colorpickerHolder_perso_couleur_titre').ColorPicker({
                                            flat: true,
                                            color: "<?php if(isset($oAgenda_perso->couleur_titre)) echo '#'.$oAgenda_perso->couleur_titre; else echo '#870000'; ?>",
                                            onSubmit: function(hsb, hex, rgb) {
                                                $('#colorSelector_perso_couleur_titre div').css('backgroundColor', '#' + hex);
                                                $('#perso_couleur_titre').val(hex);
                                                $('#colorpickerHolder_perso_couleur_titre').stop().animate({height:0}, 500); widt = !widt;
                                            }
                                        });
                                        $('#colorpickerHolder_perso_couleur_titre>div').css('position', 'absolute');

                                        $('#colorSelector_perso_couleur_titre').bind('click', function() {
                                            $('#colorpickerHolder_perso_couleur_titre').stop().animate({height: widt ? 0 : 173}, 500);
                                            widt = !widt;
                                        });
                                        //colorpickerHolder_perso_couleur_btn******************************************
                                        $('#colorpickerHolder_perso_couleur_btn').ColorPicker({
                                            flat: true,
                                            color: "<?php if(isset($oAgenda_perso->couleur_btn)) echo '#'.$oAgenda_perso->couleur_btn; else echo '#000000'; ?>",
                                            onSubmit: function(hsb, hex, rgb) {
                                                $('#colorSelector_perso_couleur_btn div').css('backgroundColor', '#' + hex);
                                                $('#perso_couleur_btn').val(hex);
                                                $('#colorpickerHolder_perso_couleur_btn').stop().animate({height:0}, 500); widt = !widt;
                                            }
                                        });
                                        $('#colorpickerHolder_perso_couleur_btn>div').css('position', 'absolute');

                                        $('#colorSelector_perso_couleur_btn').bind('click', function() {
                                            $('#colorpickerHolder_perso_couleur_btn').stop().animate({height: widt ? 0 : 173}, 500);
                                            widt = !widt;
                                        });
                                        //colorpickerHolder_perso_couleur_bg_btn******************************************
                                        $('#colorpickerHolder_perso_couleur_bg_btn').ColorPicker({
                                            flat: true,
                                            color: "<?php if(isset($oAgenda_perso->couleur_bg_btn)) echo '#'.$oAgenda_perso->couleur_bg_btn; else echo '#EFF1F2'; ?>",
                                            onSubmit: function(hsb, hex, rgb) {
                                                $('#colorSelector_perso_couleur_bg_btn div').css('backgroundColor', '#' + hex);
                                                $('#perso_couleur_bg_btn').val(hex);
                                                $('#colorpickerHolder_perso_couleur_bg_btn').stop().animate({height:0}, 500); widt = !widt;
                                            }
                                        });
                                        $('#colorpickerHolder_perso_couleur_bg_btn>div').css('position', 'absolute');

                                        $('#colorSelector_perso_couleur_bg_btn').bind('click', function() {
                                            $('#colorpickerHolder_perso_couleur_bg_btn').stop().animate({height: widt ? 0 : 173}, 500);
                                            widt = !widt;
                                        });

                                    };

                                    var showTab = function(e) {
                                        var tabIndex = $('ul.navigationTabs a')
                                            .removeClass('active')
                                            .index(this);
                                        $(this)
                                            .addClass('active')
                                            .blur();
                                        $('div.tab')
                                            .hide()
                                            .eq(tabIndex)
                                            .show();
                                    };

                                    EYE.register(initLayout, 'init');
                                })(jQuery)
                            </script>


                            <input type="hidden" name="agenda_perso[couleur_cadre]" id="perso_couleur_cadre" value="<?php if(isset($oAgenda_perso->couleur_cadre)) echo $oAgenda_perso->couleur_cadre; else echo 'FFFFFF'; ?>" />
                            <div id="colorSelector_perso_couleur_cadre"><div style="background-color: <?php if(isset($oAgenda_perso->couleur_cadre)) echo "#".$oAgenda_perso->couleur_cadre; else echo '#FFFFFF'; ?>; cursor: pointer;"></div></div>
                            <div id="colorpickerHolder_perso_couleur_cadre">
                            </div>


                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Couleur des Titres:</td>
                        <td>
                            <!--<input name="perso_couleur_titre" id="perso_couleur_titre" type="text">-->

                            <input type="hidden" name="agenda_perso[couleur_titre]" id="perso_couleur_titre" value="<?php if(isset($oAgenda_perso->couleur_titre)) echo $oAgenda_perso->couleur_titre; else echo '870000'; ?>" />
                            <input type="hidden" name="agenda_perso[IdCommercant]" id="idcom" value="301299" />

                            <div id="colorSelector_perso_couleur_titre"><div style="background-color: <?php if(isset($oAgenda_perso->couleur_titre)) echo "#".$oAgenda_perso->couleur_titre; else echo '#870000'; ?>; cursor:pointer;"></div></div>
                            <div id="colorpickerHolder_perso_couleur_titre">
                            </div>

                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Couleur des textes :</td>
                        <td>
                            <!--<input name="perso_couleur_btn" id="perso_couleur_btn" type="text">-->

                            <input type="hidden" name="agenda_perso[couleur_btn]" id="perso_couleur_btn" value="<?php if(isset($oAgenda_perso->couleur_btn)) echo $oAgenda_perso->couleur_btn; else echo '000000'; ?>" />
                            <div id="colorSelector_perso_couleur_btn"><div style="background-color: <?php if(isset($oAgenda_perso->couleur_btn)) echo "#".$oAgenda_perso->couleur_btn; else echo '#000000'; ?>; cursor:pointer;"></div></div>
                            <div id="colorpickerHolder_perso_couleur_btn">
                            </div>

                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Couleur des boutons :</td>
                        <td>
                            <input type="hidden" name="agenda_perso[couleur_bg_btn]" id="perso_couleur_bg_btn" value="<?php if(isset($oAgenda_perso->couleur_bg_btn)) echo $oAgenda_perso->couleur_bg_btn; else echo '333333'; ?>" />
                            <div id="colorSelector_perso_couleur_bg_btn"><div style="background-color: <?php if(isset($oAgenda_perso->couleur_bg_btn)) echo "#".$oAgenda_perso->couleur_bg_btn; else echo '#EFF1F2'; ?>; cursor:pointer;"></div></div>
                            <div id="colorpickerHolder_perso_couleur_bg_btn">
                            </div>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Textes des boutons :</td>
                        <td>
                            <input type="hidden" name="agenda_perso[couleur_txtblackwhite_btn]" id="perso_couleur_txtblackwhite_btn" value="<?php if(isset($oAgenda_perso->couleur_txtblackwhite_btn)) echo $oAgenda_perso->couleur_txtblackwhite_btn; else echo '1'; ?>" />
                            <div><input type="radio" name="check_couleur_txtblackwhite_btn" class="check_couleur_txtblackwhite_btn" id="check_couleur_txtblackwhite_btn_white" <?php if(isset($oAgenda_perso->couleur_txtblackwhite_btn) && $oAgenda_perso->couleur_txtblackwhite_btn=='1') echo 'checked'; else if(!isset($oAgenda_perso->couleur_txtblackwhite_btn)) echo 'checked';?>/> En Blanc</div>
                            <div><input type="radio" name="check_couleur_txtblackwhite_btn" class="check_couleur_txtblackwhite_btn" id="check_couleur_txtblackwhite_btn_black" <?php if(isset($oAgenda_perso->couleur_txtblackwhite_btn) && $oAgenda_perso->couleur_txtblackwhite_btn=='0') echo 'checked'; ?>/> En Noir</div>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <p>&nbsp;</p>
            </div>

            <div class="title_agena_perso">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>Copier/coller le code généré sur votre page web</td>
                        <td>
                            <!--<select id="select_type_code" size="1" name="agenda_perso[type_code]" style="text-align:right; float:right; color: #000;">
    <option value="">Choisissez le code ...</option>
    <option value="1" <?php if(isset($oAgenda_perso->type_code) && $oAgenda_perso->type_code=="1") echo 'selected'; ?>> Code HTML</option>
    <option value="2" <?php if(isset($oAgenda_perso->type_code) && $oAgenda_perso->type_code=="2") echo 'selected'; ?>> Code joomla</option>
    <option value="3" <?php if(isset($oAgenda_perso->type_code) && $oAgenda_perso->type_code=="3") echo 'selected'; ?>> Code wordpress</option>
    </select>-->
                        </td>
                    </tr>
                </table>
            </div>



        <div style="text-align:center;">



                <input name="btn_generer_code" id="btn_generer_code" type="button" class="btn_back_menu btn" value="Générez le code html" style="margin: 30px 0px; padding: 10px 235px; width: 100%;"><br/>
                <textarea name="agenda_perso[code]" id="txt_code_valide" cols="" rows="" readonly style="width:100%; height:150px;"><?php
                    if(isset($oAnnuaire_perso->code)) {
                        $code_to_show = $oAnnuaire_perso->code;
                        /*$code_to_show = str_replace('<script type="text/javascript">',' ',$code_to_show);
                        $code_to_show = str_replace('</script>',' ',$code_to_show);*/
                        $code_to_show = base64_decode($code_to_show);
                        echo $code_to_show;
                    }
                    ?></textarea>

                <input name="btn_page_test" id="btn_page_test" type="button" class="btn_back_menu btn" value="Visualisez une page test" style="padding: 10px 225px; width: 100%;"><br/>

            <input name="btn_validation_depot" id="btn_validation_depot" class="btn_back_menu btn" type="button" value="Enregistrer le code" style="margin: 30px 0px; padding: 10px 243px; width: 100%;"><br/>


        </div>

    </form>

    </div>




<?php $this->load->view("frontAout2013/includes/footer_mini_2"); ?>