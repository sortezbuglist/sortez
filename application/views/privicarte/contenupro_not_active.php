<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <title>premium4</title>
    <style type="text/css">
        @font-face{
            font-family: futura;
            src: url('<?php echo base_url('assets/')?>font/futura medium bt.ttf');
        }
        a{
            color: white!important;
        }
    </style>
</head>
<body style="padding:10%;font-family:futura;font-size:115%;text-align:justify">
<div class="container-fluid align-center ">
    <p class="text-center">
        <img src="<?php echo base_url('assets/img/') ?>wpb8bbb83e_06.png" class="mb-4">
        <br/>
        BONJOUR M  <span style="font-size: 25px"><?php echo $toCommercant->Prenom;?>  <?php echo $toCommercant->Nom;?></span>
    </p>
    <? //var_dump($user_ion_auth);?>
</div>

<div>
    <br/>
    <div class="pl-5"><p class="pl-5">Nous vous remercions de votre souscription à notre abonnement Premium.</p></div>
    <br>
    <img class="float-left" src="<?php echo base_url('assets/img/') ?>wp3f6f36cb_06.png">
    <div class="pl-5">
        <p class="pl-5">Nous vous invitons à déposer vos données sur les champs préconfigurés des interfaces intuitives de votre abonnement Premium</p>

        <div  class="pt-2 pl-5">
            <ul class="pl-3" style="padding:0">
                <li>
                    Sur « Mon abonnement et mes coordonnées », vous intégrez les coordonnées de votre établissement (adresse, téléphone, liens divers,  site web et bien plus encore…)
                </li>
                <li>
                    Sur « Mon contenu « Premium », vous précisez votre activité en intégrant vos données (textes, images, vidéo, PDF, document PDF…)
                </li>
            </ul>
            Sur « Mon abonnement et mes coordonnées », vous intégrez les coordonnées de votre établissement (adresse, téléphone, liens divers,  site web et bien plus encore…)
            Sur « Mon contenu « Premium », vous précisez votre activité en intégrant vos données (textes, images, vidéo, PDF, document PDF…)
            A tout moment, vous pouvez visualiser et perfectionner le résultat.
        </div>
        <p style="color:rgb(255,0,255);font-style: italic;" class="pl-5">Si vous rencontrez une difficulté, adressez nous une alerte, notre service vous répondra sous 48h00…</p>
    </div>
</div>

<a href="<?php echo site_url('contenupro')?>"><div class="container p-2 text-center col-5 mt-3 mb-3" style="background:green;color:#fff;font-size:125%">
    Une difficulté… contactez nous !
</div></a>
<a href="<?php echo site_url('auth/logout')?>"><div class="container p-2 text-center col-5 mt-3 mb-3" style="background:red;color:#fff;font-size:125%">
       Déconnexion
    </div></a>

<div>
    <img class="float-left" src="<?php echo base_url('assets/img/') ?>wp3f6f36cb_06.png">
    <div class="pl-5">
        <p class="pl-5">
            Votre page finalisée, vous demandez la validation de votre référencement et l’ouverture du pack de démarrage pour accéder à nos 3 modules test  (article, agenda, e-boutique) en cliquant sur le bouton « Validation de mon référencement Premium »…
            <br>
            <span style="color:rgb(255,0,255);font-style: italic;">Attention : Sortez.org peut refuser sans en donner la justification l’ouverture d’un compte qui ne correspondrait pas à son éthique. (Voir nos conditions générales).</span>
        </p>
        <br>
    </div>
    <img class="float-left" src="<?php echo base_url('assets/img/') ?>wp3f6f36cb_06.png">
    <div class="pl-5">
        <p class="pl-5">
            Dès validation de notre part, vos données seront alors définitivement référencées sur l’annuaire complet de sortez.org et vous pourrez ajouter immédiatement des articles, des événements ou des annonces avec notre pack de démarrage.
        </p>
    </div>
    <br>

    <img class="float-left" src="<?php echo base_url('assets/img/') ?>wp3f6f36cb_06.png">
    <div class="pl-5">
        <p class="pl-5">Nous vous invitons à déposer vos données sur les champs préconfigurés des interfaces intuitives de votre abonnement Premium</p>
    </div>
    <br>

</div>

<div class="text-center p-3 " style="background:rgba(125,125,125,.25)">
    <h3 style="font-weight:bold" class="container-fluid col-10 text-center p-4">CONFIGUREZ VOTRE PAGE<br/>ACCÈS AUX MODULES DE MON ABONNEMENT PREMIUM</h3>

    <div class="container-fluid">
        <img src="<?php echo base_url('assets/img/') ?>wpa03d963a_05_06.jpg" style="width:87%;">
    </div>
    <br><br>
    <div class="row ml-5 mr-5">
        <p style="background:green;color:white;font-size:125%" class="container p-3 col-5 "><a href="<?php echo site_url('contact')?>">Mon abonnement et mes coordonnées</a></p>
        <p style="background:green;color:white;font-size:125%" class="container p-3 col-5 "><a href="<?php echo site_url('contact')?>">Mon contenu « Premium » </a></p>

    </div>

</div>

<div class="text-center p-3 mt-5" style="background:rgba(125,125,125,.25)">
    <h3 style="font-weight:bold" class="container-fluid col-10 text-center p-4">VOUS AVEZ FINALISÉ VOTRE PRÉSENTATION,DEMANDEZ LE RÉFÉRENCEMENT DÉFINITIF DE VOTRE ÉTABLISSEMENT !</h3>

    <div class="container-fluid">
        <img src="<?php echo base_url('assets/img/') ?>wp95c88ad1_05_06.jpg" style="width:87%;">
    </div>
    <br><br>
    <div class="ml-5 mr-5">
        <p style="background:green;color:white;font-size:125%" class="container p-3 col-5 "><a href="<?php echo site_url('contact')?>">Je confirme ma demande</a></p>
        <p style="background:green;color:white;font-size:125%" class="container p-3 col-5 "><a href="<?php echo site_url('contact')?>">Conditions générales</a></p>

    </div>

</div>

<div class="text-center p-3 mt-5" style="background:rgba(125,125,125,.25)">
    <h3 style="font-weight:bold" class="container-fluid col-10 text-center p-4">QUAND VOTRE ABONNEMENT SERA DÉFINITIVEMENT VALIDÉ,VOUS AUREZ ACCÈS SUR UNE NOUVELLE PAGE ADMIN À D’AUTRES MODULES OPTIONNELS GRATUITS OU PAYANTS…</h3>

    <div class="container-fluid">
        <img src="<?php echo base_url('assets/img/') ?>wp67065abb_05_06.jpg" style="width:87%;">
    </div>
    <br><br>
    <div class="ml-5 mr-5">
        <p style="background:green;color:white;font-size:125%" class="container p-3 col-5 "><a href="<?php echo site_url('annonce-infos.html')?>">Plus d’informations</a></p>
    </div>

</div>

</body>
</html>