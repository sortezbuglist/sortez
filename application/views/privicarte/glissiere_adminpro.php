<?php $data["zTitle"] = 'Glissiere' ?>
<?php $this->load->view("privicarte/includes/header_mini_3", $data); ?>

<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>


<style type="text/css">
.stl_long_input_platinum {
	width:413px;
}
.stl_long_input_platinum_td {
	width:180px;
	height:30px;
}
.div_stl_long_platinum {
	background-color:#3653A3;color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 13px;
    font-weight: 700;
    line-height: 1.23em;
    height:40px; padding-top:12px; padding-left:20px; margin-top:12px; margin-bottom:15px;
}
.div_error_taille_3_4 {
	color: #F00;
}
.FieldError {
	color:#FF0000;
	font-weight:bold;
}
</style>


<div style="text-align:left; height:200px;">
  
  
  <div class="col-lg-12" style="padding-top:30px; display:table;">
        <div class="col-lg-6" style="padding:0 !important;"><img src="<?php echo GetImagePath("privicarte/");?>/img_form_data.png" /></div>
        <div class="col-lg-6" style="padding:0 !important; font:arial; font-size:30px; text-align:center; font-weight:bold;">
        <div>Mes Glissi&egrave;res</div>
        <div><a href="<?php echo site_url("front/utilisateur/contenupro");?>" style="text-decoration:none;"><img src="<?php echo GetImagePath("privicarte/");?>/retour_data.png" /></a></div>
        </div>
  </div>
  


<div style=" padding-left:200px;">
<?php if (isset($mssg) && $mssg==1) {?> <div align="center" style="text-align:left; font-style:italic;color:#0C0;">Les modifications ont &eacute;t&eacute; enregistr&eacute;es</div><br/><?php }?>
</div>
<div style=" padding-left:120px;">
<?php if (isset($mssg) && $mssg!=1 && $mssg!="") {?> <div align="center" style="text-align:left; font-style:italic;color:#F00;">Une erreur est constatée, Veuillez  vérifier la conformité de vos données</div><br/><?php }?>
</div>




<div class="col-lg-12">

<form name="frmInscriptionProfessionnel__" id="frmInscriptionProfessionnel__" action="<?php echo site_url("front/professionnels/save_glissiere"); ?>" method="POST" enctype="multipart/form-data">


<input type="hidden" name="Societe[id_glissiere]" id="id_glissiere" value="<?php if(isset($objCommercant->id_glissiere)) echo $objCommercant->id_glissiere; else echo '0';?>"/>


<div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px;">Glissi&egrave;re 1 page présentation 
<span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Societe[isActive_presentation_1]" id="isActive_presentation_1" style="color:#000000;">
        <option value="0" <?php if($objCommercant->isActive_presentation_1=='0') {?>selected="selected"<?php } ?>>non</option>
        <option value="1" <?php if($objCommercant->isActive_presentation_1=='1') {?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
</div>
<table cellspacing="0" cellpadding="0" style=" width:100%;">
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Titre : </label>
    </td>
    <td>
        <input type="text" style="width:413px;" name="Societe[presentation_1_titre]" id="presentation_1_titre" value="<?php if(isset($objCommercant->presentation_1_titre)) echo htmlspecialchars($objCommercant->presentation_1_titre); ?>"/>
    </td>
</tr>
<tr>
    <td colspan="2">
        <label class="label">Contenu : </label>
    </td>
</tr>
<tr>
    <td colspan="2">
        <textarea name="Societe[presentation_1_contenu]" id="presentation_1_contenu"><?php if(isset($objCommercant->presentation_1_contenu)) echo htmlspecialchars($objCommercant->presentation_1_contenu); ?></textarea>
        <?php echo display_ckeditor($ckeditor_presentation_1); ?>
    </td>
</tr>
</table>



<div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px;">Glissi&egrave;re 2 page présentation 
<span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Societe[isActive_presentation_2]" id="isActive_presentation_2" style="color:#000000;">
        <option value="0" <?php if($objCommercant->isActive_presentation_2=='0') {?>selected="selected"<?php } ?>>non</option>
        <option value="1" <?php if($objCommercant->isActive_presentation_2=='1') {?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
</div>
<table cellspacing="0" cellpadding="0" style=" width:100%;">
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Titre : </label>
    </td>
    <td>
        <input type="text" style="width:413px;" name="Societe[presentation_2_titre]" id="presentation_2_titre" value="<?php if(isset($objCommercant->presentation_2_titre)) echo htmlspecialchars($objCommercant->presentation_2_titre); ?>"/>
    </td>
</tr>
<tr>
    <td colspan="2">
        <label class="label">Contenu : </label>
    </td>
</tr>
<tr>
    <td colspan="2">
        <textarea name="Societe[presentation_2_contenu]" id="presentation_2_contenu"><?php if(isset($objCommercant->presentation_2_contenu)) echo htmlspecialchars($objCommercant->presentation_2_contenu); ?></textarea>
        <?php echo display_ckeditor($ckeditor_presentation_2); ?>
    </td>
</tr>
</table>




<div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px;">Glissi&egrave;re 3 page présentation
<span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Societe[isActive_presentation_3]" id="isActive_presentation_3" style="color:#000000;">
        <option value="0" <?php if($objCommercant->isActive_presentation_3=='0') {?>selected="selected"<?php } ?>>non</option>
        <option value="1" <?php if($objCommercant->isActive_presentation_3=='1') {?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
</div>
<table cellspacing="0" cellpadding="0" style=" width:100%;">
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Titre : </label>
    </td>
    <td>
        <input type="text" style="width:413px;" name="Societe[presentation_3_titre]" id="presentation_3_titre" value="<?php if(isset($objCommercant->presentation_3_titre)) echo htmlspecialchars($objCommercant->presentation_3_titre); ?>"/>
    </td>
</tr>
<tr>
    <td colspan="2">
        <label class="label">Contenu : </label>
    </td>
</tr>
<tr>
    <td colspan="2">
        <textarea name="Societe[presentation_3_contenu]" id="presentation_3_contenu"><?php if(isset($objCommercant->presentation_3_contenu)) echo htmlspecialchars($objCommercant->presentation_3_contenu); ?></textarea>
        <?php echo display_ckeditor($ckeditor_presentation_3); ?>
    </td>
</tr>
</table>




<div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px;">Glissi&egrave;re 4 page présentation
<span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Societe[isActive_presentation_4]" id="isActive_presentation_4" style="color:#000000;">
        <option value="0" <?php if($objCommercant->isActive_presentation_4=='0') {?>selected="selected"<?php } ?>>non</option>
        <option value="1" <?php if($objCommercant->isActive_presentation_4=='1') {?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
</div>
<table cellspacing="0" cellpadding="0" style=" width:100%;">
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Titre : </label>
    </td>
    <td>
        <input type="text" style="width:413px;" name="Societe[presentation_4_titre]" id="presentation_4_titre" value="<?php if(isset($objCommercant->presentation_4_titre)) echo htmlspecialchars($objCommercant->presentation_4_titre); ?>"/>
    </td>
</tr>
<tr>
    <td colspan="2">
        <label class="label">Contenu : </label>
    </td>
</tr>
<tr>
    <td colspan="2">
        <textarea name="Societe[presentation_4_contenu]" id="presentation_4_contenu"><?php if(isset($objCommercant->presentation_4_contenu)) echo htmlspecialchars($objCommercant->presentation_4_contenu); ?></textarea>
        <?php echo display_ckeditor($ckeditor_presentation_4); ?>
    </td>
</tr>
</table>




<div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px;">Glissi&egrave;re 1 page 1
<span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Societe[isActive_page1_1]" id="isActive_page1_1" style="color:#000000;">
        <option value="0" <?php if($objCommercant->isActive_page1_1=='0') {?>selected="selected"<?php } ?>>non</option>
        <option value="1" <?php if($objCommercant->isActive_page1_1=='1') {?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
</div>
<table cellspacing="0" cellpadding="0" style=" width:100%;">
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Titre : </label>
    </td>
    <td>
        <input type="text" style="width:413px;" name="Societe[page1_1_titre]" id="page1_1_titre" value="<?php if(isset($objCommercant->page1_1_titre)) echo htmlspecialchars($objCommercant->page1_1_titre); ?>"/>
    </td>
</tr>
<tr>
    <td colspan="2">
        <label class="label">Contenu : </label>
    </td>
</tr>
<tr>
    <td colspan="2">
        <textarea name="Societe[page1_1_contenu]" id="page1_1_contenu"><?php if(isset($objCommercant->page1_1_contenu)) echo htmlspecialchars($objCommercant->page1_1_contenu); ?></textarea>
        <?php echo display_ckeditor($ckeditor_page1_1); ?>
    </td>
</tr>
</table>





<div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px;">Glissi&egrave;re 2 page 1
<span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Societe[isActive_page1_2]" id="isActive_page1_2" style="color:#000000;">
        <option value="0" <?php if($objCommercant->isActive_page1_2=='0') {?>selected="selected"<?php } ?>>non</option>
        <option value="1" <?php if($objCommercant->isActive_page1_2=='1') {?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
</div>
<table cellspacing="0" cellpadding="0" style=" width:100%;">
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Titre : </label>
    </td>
    <td>
        <input type="text" style="width:413px;" name="Societe[page1_2_titre]" id="page1_2_titre" value="<?php if(isset($objCommercant->page1_2_titre)) echo htmlspecialchars($objCommercant->page1_2_titre); ?>"/>
    </td>
</tr>
<tr>
    <td colspan="2">
        <label class="label">Contenu : </label>
    </td>
</tr>
<tr>
    <td colspan="2">
        <textarea name="Societe[page1_2_contenu]" id="page1_2_contenu"><?php if(isset($objCommercant->page1_2_contenu)) echo htmlspecialchars($objCommercant->page1_2_contenu); ?></textarea>
        <?php echo display_ckeditor($ckeditor_page1_2); ?>
    </td>
</tr>
</table>




<div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px;">Glissi&egrave;re 3 page 1
<span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Societe[isActive_page1_3]" id="isActive_page1_3" style="color:#000000;">
        <option value="0" <?php if($objCommercant->isActive_page1_3=='0') {?>selected="selected"<?php } ?>>non</option>
        <option value="1" <?php if($objCommercant->isActive_page1_3=='1') {?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
</div>
<table cellspacing="0" cellpadding="0" style=" width:100%;">
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Titre : </label>
    </td>
    <td>
        <input type="text" style="width:413px;" name="Societe[page1_3_titre]" id="page1_3_titre" value="<?php if(isset($objCommercant->page1_3_titre)) echo htmlspecialchars($objCommercant->page1_3_titre); ?>"/>
    </td>
</tr>
<tr>
    <td colspan="2">
        <label class="label">Contenu : </label>
    </td>
</tr>
<tr>
    <td colspan="2">
        <textarea name="Societe[page1_3_contenu]" id="page1_3_contenu"><?php if(isset($objCommercant->page1_3_contenu)) echo htmlspecialchars($objCommercant->page1_3_contenu); ?></textarea>
        <?php echo display_ckeditor($ckeditor_page1_3); ?>
    </td>
</tr>
</table>





<div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px;">Glissi&egrave;re 4 page 1
<span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Societe[isActive_page1_4]" id="isActive_page1_4" style="color:#000000;">
        <option value="0" <?php if($objCommercant->isActive_page1_4=='0') {?>selected="selected"<?php } ?>>non</option>
        <option value="1" <?php if($objCommercant->isActive_page1_4=='1') {?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
</div>
<table cellspacing="0" cellpadding="0" style=" width:100%;">
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Titre : </label>
    </td>
    <td>
        <input type="text" style="width:413px;" name="Societe[page1_4_titre]" id="page1_4_titre" value="<?php if(isset($objCommercant->page1_4_titre)) echo htmlspecialchars($objCommercant->page1_4_titre); ?>"/>
    </td>
</tr>
<tr>
    <td colspan="2">
        <label class="label">Contenu : </label>
    </td>
</tr>
<tr>
    <td colspan="2">
        <textarea name="Societe[page1_4_contenu]" id="page1_4_contenu"><?php if(isset($objCommercant->page1_4_contenu)) echo htmlspecialchars($objCommercant->page1_4_contenu); ?></textarea>
        <?php echo display_ckeditor($ckeditor_page1_4); ?>
    </td>
</tr>
</table>






<div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px;">Glissi&egrave;re 1 page 2
<span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Societe[isActive_page2_1]" id="isActive_page2_1" style="color:#000000;">
        <option value="0" <?php if($objCommercant->isActive_page2_1=='0') {?>selected="selected"<?php } ?>>non</option>
        <option value="1" <?php if($objCommercant->isActive_page2_1=='1') {?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
</div>
<table cellspacing="0" cellpadding="0" style=" width:100%;">
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Titre : </label>
    </td>
    <td>
        <input type="text" style="width:413px;" name="Societe[page2_1_titre]" id="page2_1_titre" value="<?php if(isset($objCommercant->page2_1_titre)) echo htmlspecialchars($objCommercant->page2_1_titre); ?>"/>
    </td>
</tr>
<tr>
    <td colspan="2">
        <label class="label">Contenu : </label>
    </td>
</tr>
<tr>
    <td colspan="2">
        <textarea name="Societe[page2_1_contenu]" id="page2_1_contenu"><?php if(isset($objCommercant->page2_1_contenu)) echo htmlspecialchars($objCommercant->page2_1_contenu); ?></textarea>
        <?php echo display_ckeditor($ckeditor_page2_1); ?>
    </td>
</tr>
</table>





<div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px;">Glissi&egrave;re 2 page 2
<span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Societe[isActive_page2_2]" id="isActive_page2_2" style="color:#000000;">
        <option value="0" <?php if($objCommercant->isActive_page2_2=='0') {?>selected="selected"<?php } ?>>non</option>
        <option value="1" <?php if($objCommercant->isActive_page2_2=='1') {?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
</div>
<table cellspacing="0" cellpadding="0" style=" width:100%;">
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Titre : </label>
    </td>
    <td>
        <input type="text" style="width:413px;" name="Societe[page2_2_titre]" id="page2_2_titre" value="<?php if(isset($objCommercant->page2_2_titre)) echo htmlspecialchars($objCommercant->page2_2_titre); ?>"/>
    </td>
</tr>
<tr>
    <td colspan="2">
        <label class="label">Contenu : </label>
    </td>
</tr>
<tr>
    <td colspan="2">
        <textarea name="Societe[page2_2_contenu]" id="page2_2_contenu"><?php if(isset($objCommercant->page2_2_contenu)) echo htmlspecialchars($objCommercant->page2_2_contenu); ?></textarea>
        <?php echo display_ckeditor($ckeditor_page2_2); ?>
    </td>
</tr>
</table>




<div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px;">Glissi&egrave;re 3 page 2
<span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Societe[isActive_page2_3]" id="isActive_page2_3" style="color:#000000;">
        <option value="0" <?php if($objCommercant->isActive_page2_3=='0') {?>selected="selected"<?php } ?>>non</option>
        <option value="1" <?php if($objCommercant->isActive_page2_3=='1') {?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
</div>
<table cellspacing="0" cellpadding="0" style=" width:100%;">
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Titre : </label>
    </td>
    <td>
        <input type="text" style="width:413px;" name="Societe[page2_3_titre]" id="page2_3_titre" value="<?php if(isset($objCommercant->page2_3_titre)) echo htmlspecialchars($objCommercant->page2_3_titre); ?>"/>
    </td>
</tr>
<tr>
    <td colspan="2">
        <label class="label">Contenu : </label>
    </td>
</tr>
<tr>
    <td colspan="2">
        <textarea name="Societe[page2_3_contenu]" id="page2_3_contenu"><?php if(isset($objCommercant->page2_3_contenu)) echo htmlspecialchars($objCommercant->page2_3_contenu); ?></textarea>
        <?php echo display_ckeditor($ckeditor_page2_3); ?>
    </td>
</tr>
</table>





<div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px;">Glissi&egrave;re 4 page 2
<span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Societe[isActive_page2_4]" id="isActive_page2_4" style="color:#000000;">
        <option value="0" <?php if($objCommercant->isActive_page2_4=='0') {?>selected="selected"<?php } ?>>non</option>
        <option value="1" <?php if($objCommercant->isActive_page2_4=='1') {?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
</div>
<table cellspacing="0" cellpadding="0" style=" width:100%;">
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Titre : </label>
    </td>
    <td>
        <input type="text" style="width:413px;" name="Societe[page2_4_titre]" id="page2_4_titre" value="<?php if(isset($objCommercant->page2_4_titre)) echo htmlspecialchars($objCommercant->page2_4_titre); ?>"/>
    </td>
</tr>
<tr>
    <td colspan="2">
        <label class="label">Contenu : </label>
    </td>
</tr>
<tr>
    <td colspan="2">
        <textarea name="Societe[page2_4_contenu]" id="page2_4_contenu"><?php if(isset($objCommercant->page2_4_contenu)) echo htmlspecialchars($objCommercant->page2_4_contenu); ?></textarea>
        <?php echo display_ckeditor($ckeditor_page2_4); ?>
    </td>
</tr>
</table>






<div style="height:40px; background-color:#3653A3; margin-top:40px; margin-bottom:20px; text-align:center; padding-top:7px;"><input type="submit" class="btnSinscrire" value="Validation des Modifications" /></div>





</form>






</div>

<?php $this->load->view("privicarte/includes/footer_mini_2"); ?>