<?php $data["zTitle"] = 'Bonplans' ?>


<?php $this->load->view("privicarte/includes/main_header", $data); ?>
<?php $this->load->view("privicarte/includes/ready_bonplans", $data); ?>
<?php $this->load->view("privicarte/includes/main_navbar", $data); ?>
<?php $this->load->view("privicarte/includes/main_logo_dep_com", $data); ?>
<?php $this->load->view("privicarte/includes/main_menu", $data);?>
<?php $this->load->view("privicarte/includes/main_slide", $data); ?>
<?php //$this->load->view("privicarte/includes/main_map", $data); ?>	
<?php $this->load->view("privicarte/includes/main_menu_contents", $data); ?>


<div class="container top_bottom_15" style="background-color: #fff; padding-bottom:40px;">

    <div class="col-sm-3 bonplan_left_filter">
        <?php 
		if (isset($pagecategory) && $pagecategory == 'annonce')
		 $this->load->view("privicarte/includes/searchform_content_annonce", $data); 
		else if (isset($pagecategory) && $pagecategory == 'bonplan')
		 $this->load->view("privicarte/includes/searchform_content_bonplan", $data); 
		else $this->load->view("privicarte/includes/searchform_content_partenaire", $data); 
		?>
    </div>
    
    <div class="col-sm-9 paddingleft0 bonplan_rigth_content paddingright0">
        <?php $this->load->view("privicarte/bonplans_list", $data); ?>
    </div>

</div>



<?php $this->load->view("privicarte/includes/main_footer_link", $data); ?>
<?php $this->load->view("privicarte/includes/main_footer_copyright", $data); ?>
<?php $this->load->view("privicarte/includes/main_footer", $data); ?>