<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Privicarte Contact</title>
  <!-- Bootstrap core CSS -->
	<link href="<?php echo GetCssPath("bootstrap/") ; ?>/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<?php echo GetCssPath("bootstrap/") ; ?>/bootstrap-theme.css" rel="stylesheet" type="text/css">
    
    <script type="text/javascript" src="<?php echo GetJsPath("privicarte/") ; ?>/jquery-1.11.3.min.js"></script>
    
    
    <!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="<?php echo base_url()."application/resources/fancybox"; ?>/lib/jquery.mousewheel-3.0.6.pack.js"></script>

	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="<?php echo base_url()."application/resources/fancybox"; ?>/source/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()."application/resources/fancybox"; ?>/source/jquery.fancybox.css?v=2.1.5" media="screen" />

  

    <script type="text/javascript" src="<?php echo GetJsPath("privicarte/") ; ?>/global.js"></script>
    
	<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	<script type="text/javascript" src="<?php echo GetJsPath("privicarte/") ; ?>/ie-emulation-modes-warning.js"></script>
    
    
  <script type="text/javascript">
  
jQuery(document).ready(function() {

	jQuery("#contact_privicarte_nom").focusin(function() {	  if (jQuery(this).val()=="Votre nom *") jQuery(this).val('');	});
	jQuery("#contact_privicarte_nom").focusout(function() {	  if (jQuery(this).val()=="") jQuery(this).val('Votre nom *');	});
	
	jQuery("#contact_privicarte_tel").focusin(function() {	  if (jQuery(this).val()=="Votre num\351ro de t\351l\351phone *") jQuery(this).val('');	});
	jQuery("#contact_privicarte_tel").focusout(function() {	  if (jQuery(this).val()=="") jQuery(this).val('Votre num\351ro de t\351l\351phone *');	});
	
	jQuery("#contact_privicarte_mail").focusin(function() {	  if (jQuery(this).val()=="Votre courriel *") jQuery(this).val('');	});
	jQuery("#contact_privicarte_mail").focusout(function() {	  if (jQuery(this).val()=="") jQuery(this).val('Votre courriel *');	});
	
	jQuery("#contact_privicarte_msg").focusin(function() {	  if (jQuery(this).val()=="Votre message *") jQuery(this).val('');	});
	jQuery("#contact_privicarte_msg").focusout(function() {	  if (jQuery(this).val()=="") jQuery(this).val('Votre message *');	});
	
	jQuery("#contact_privicarte_reset").click(function() {
	  jQuery("#contact_privicarte_nom").val('Votre nom *');
	  jQuery("#contact_privicarte_tel").val('Votre num\351ro de t\351l\351phone *');
	  jQuery("#contact_privicarte_mail").val('Votre courriel *');
	  jQuery("#contact_privicarte_msg").val('Votre message *');
	  jQuery("#spanContactPrivicarteForm").html('* champs obligatoires');
	  jQuery("#contact_privicarte_send").css('display','block');
	});
	
	jQuery("#contact_privicarte_send").click(function() {
		var error = 0;
		var contact_privicarte_nom = jQuery("#contact_privicarte_nom").val();
		if (contact_privicarte_nom == '' || contact_privicarte_nom == 'Votre nom *') error = 1;
		var contact_privicarte_tel = jQuery("#contact_privicarte_tel").val();
		if (contact_privicarte_tel == '' || contact_privicarte_tel == 'Votre num\351ro de t\351l\351phone *') error = 1;
		var contact_privicarte_mail = jQuery("#contact_privicarte_mail").val();
		if (contact_privicarte_mail == '' || contact_privicarte_mail == 'Votre courriel *') error = 1;
		if (!validateEmail(contact_privicarte_mail)) error = 2;
		var contact_privicarte_msg = jQuery("#contact_privicarte_msg").val();
		if (contact_privicarte_msg == '' || contact_privicarte_msg == 'Votre message *') error = 1;
		jQuery("#spanContactPrivicarteForm").html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
		
		if (error == 1) {
			jQuery("#spanContactPrivicarteForm").html('<span style="color:#FF0000;">* Les champs sont obligatoires</span>');
		} else if (error == 2) {
			jQuery("#spanContactPrivicarteForm").html('<span style="color:#FF0000;">* Adresse email invalide</span>');
			jQuery("#contact_privicarte_mail").css('border-color','#ff0000'); 
		} else {
			$.post(
				"<?php echo site_url("contact/send_email/");?>", 
				{
					contact_privicarte_nom:contact_privicarte_nom,
					contact_privicarte_tel:contact_privicarte_tel,
					contact_privicarte_mail:contact_privicarte_mail,
					contact_privicarte_msg:contact_privicarte_msg,
					contact_privicarte_mailto:"<?php echo $mail_sender_adress; ?>"
				},
				function( data ) {
				  jQuery("#spanContactPrivicarteForm").html(data);
				  jQuery("#contact_privicarte_send").css('display','none');
				});
			}
	});
	
	
});

function validateEmail(sEmail) {
	var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if (filter.test(sEmail)) {
		return true;
	}
	else {
		return false;
	}
}
</script>

<style type="text/css">
table#tableContactPartnerForm td input, table#tableContactPartnerForm td textarea { width:100% !important;}
@media screen and (max-width: 768px) {
    #divContactPrivicarteForm {
        width: 100% !important;
        height:auto !important;
        max-width: 100% !important;
        padding: 15px;
        margin-left: 0 !important;
        margin-right: 0 !important;
    }
    #divContactPrivicarteForm #tableContactPartnerForm {
        width: 100% !important;
        height:auto !important;
        max-width: 100% !important;
        padding-top: 40px;
    }
    #divContactPrivicarteForm #tableContactPartnerForm #spanContactPrivicarteForm {
        height: 50px !important;
        line-height: 50px !important;
    }
    #divContactPrivicarteForm #tableContactPartnerForm div:first-child {
        padding-bottom: 30px;
    }
}
</style>
</head>

<body>
  <!--Contact form contet-->
<div id="divContactPrivicarteForm" style="background-color:#FFFFFF; width:500px; height:400px; margin-left:auto; margin-right:auto;">
    <form name="formContactPrivicarteForm" id="formContactPrivicarteForm" action="#">
        <table width="100%" id="tableContactPartnerForm" border="0" cellspacing="0" cellpadding="0" style="text-align:center; width:500px; height:400px;">
          <tr>
            <td><div style="font-family:arial; font-size:24px; font-weight:bold;">Nous Contacter</div></td>
          </tr>
          <tr>
            <td><input type="text" name="contact_privicarte_nom" id="contact_privicarte_nom" value="Votre nom *" class="form-control"/></td>
          </tr>
          <tr>
            <td><input type="text" name="contact_privicarte_tel" id="contact_privicarte_tel" value="Votre num&eacute;ro de t&eacute;l&eacute;phone *" class="form-control"/></td>
          </tr>
          <tr>
            <td><input type="text" name="contact_privicarte_mail" id="contact_privicarte_mail" value="Votre courriel *" class="form-control"/></td>
          </tr>
          <tr>
            <td><textarea name="contact_privicarte_msg" id="contact_privicarte_msg" class="form-control">Votre message *</textarea></td>
          </tr>
          <tr>
            <td><span id="spanContactPrivicarteForm">* champs obligatoires</span></td>
          </tr>
          <tr>
            <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:center;">
              <tr>
                <td><input type="button" class="btn btn-primary" name="contact_privicarte_reset" id="contact_privicarte_reset" value="Retablir"/></td>
                <td><input type="button" class="btn btn-success" name="contact_privicarte_send" id="contact_privicarte_send" value="Envoyer"/></td>
              </tr>
            </table>
            </td>
          </tr>
        </table>
    </form>
</div>
</body>
</html>



