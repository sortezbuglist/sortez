<?php $data["zTitle"] = 'Inscription des professionnels'; ?>
<?php $this->load->view("privicarte/admin/vwHeader2013", $data); ?>

    <style type="text/css">
        #frmInscriptionProfessionnel fieldset {
            width: 100%;
        }

        #frmInscriptionProfessionnel .inputadmin {
            width: 400px;
        }
    </style>


    <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/fields.check.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('#loading_datefin_plus_un_an').hide();

            jQuery(".btnSinscrire").click(function () {
                var txtError = "";

                var RubriqueSociete = jQuery("#RubriqueSociete").val();
                if (RubriqueSociete == "0" || RubriqueSociete == "") {
                    jQuery("#divErrorRubriqueSociete").html("Un commercant doit être assigné à une rubrique.");
                    jQuery("#divErrorRubriqueSociete").show();
                    txtError += "- Un commercant doit être assigné à une rubrique.<br/>";
                } else {
                    jQuery("#divErrorRubriqueSociete").hide();
                }

                var SousRubriqueSociete = jQuery("#SousRubriqueSociete").val();
                if (SousRubriqueSociete == "0" || SousRubriqueSociete == "") {
                    jQuery("#divErrorSousRubriqueSociete").html("Un commercant doit être assigné à une sous-rubrique.");
                    jQuery("#divErrorSousRubriqueSociete").show();
                    txtError += "- Un commercant doit être assigné à une sous-rubrique.<br/>";
                } else {
                    jQuery("#divErrorSousRubriqueSociete").hide();
                }

                var EmailSociete = jQuery("#EmailSociete").val();
                if (!isEmail(EmailSociete)) {
                    jQuery("#divErrorEmailSociete").html("Ce Courriel n'est pas valide. Veuillez saisir un email valide");
                    jQuery("#divErrorEmailSociete").show();
                    txtError += "- Ce Courriel n'est pas valide. Veuillez saisir un email valide.<br/>";
                } else {
                    jQuery("#divErrorEmailSociete").hide();
                }

                var LoginSociete = jQuery("#LoginSociete").val();
                if (!isEmail(LoginSociete)) {
                    txtError += "- Votre login doit &ecirc;tre un email valide.<br/>";
                    jQuery("#divErrorLoginSociete").html("Votre login doit &ecirc;tre un email valide.");
                    jQuery("#divErrorLoginSociete").show();
                } else {
                    jQuery("#divErrorLoginSociete").hide();
                }


                jQuery("#total_error_admin_fiche_comm").html(txtError);

                if (txtError == "") {
                    jQuery("#frmInscriptionProfessionnel").submit();
                }
            });


            jQuery("#IsActifSociete").click(function () {
                var DateDebut = jQuery("#DateDebut").val();
                var DateFin = jQuery("#DateFin").val();

                if ((DateDebut == "" || DateDebut == null) && (DateFin == "" || DateFin == null)) {
                    //alert("valeur null");
                    DateDebut = "<?php echo convert_Sqldate_to_Frenchdate(date("Y-m-d")); ?>";
                    <?php
                    $current_date = date("Y-m-d");
                    $current_value = explode('-', $current_date);
                    ?>
                    //alert("sqdfqsf "+<?php echo $current_value[0] + 1;?>);
                    <?php
                    $year_datefin = $current_value[0] + 1;
                    $date_fin_value = $year_datefin . "-" . $current_value[1] . "-" . $current_value[2];
                    ?>
                    DateFin = "<?php echo convert_Sqldate_to_Frenchdate($date_fin_value);?>";
                    //alert(DateDebut);
                    //alert(DateFin);
                    jQuery("#DateDebut").val(DateDebut);
                    jQuery("#DateFin").val(DateFin);
                }

            });

            jQuery("#btn_datefin_plus_un_an").click(function () {
                var DateDebut = jQuery("#DateDebut").val();

                jQuery('#loading_datefin_plus_un_an').show();

                jQuery.post('<?php echo site_url("admin/commercants/add_datefin_plus_un_an"); ?>',
                    {DateDebut: DateDebut},
                    function success(data) {
                        jQuery("#DateFin").val(data);
                        jQuery('#loading_datefin_plus_un_an').hide();
                    });

            });

        })
    </script>
    <script type="text/javascript">
        $(function () {
            $("#DateDebut").datepicker({
                dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                monthNames: ['Janvier', 'Fevier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'],
                dateFormat: 'DD, d MM yy',
                autoSize: true
            });
            $("#DateFin").datepicker({
                dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                monthNames: ['Janvier', 'Fevier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juiller', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'],
                dateFormat: 'DD, d MM yy',
                autoSize: true
            });
            $("#DateInscription").datepicker({
                dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                monthNames: ['Janvier', 'Fevier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juiller', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'],
                dateFormat: 'DD, d MM yy',
                autoSize: true
            });
        });
        function effacerPhotoAccueil(_iCommercantId) {
            jQuery.post('<?php echo site_url("admin/commercants/effacerPhotoCommercant"); ?>',
                {iCommercantId: _iCommercantId},
                function success(data) {
                    jQuery("#divPhotoAccueil").hide();
                });
        }
        function effacerPhoto1(_iCommercantId) {
            jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto1Commercant"); ?>',
                {iCommercantId: _iCommercantId},
                function success(data) {
                    jQuery("#divPhoto1").hide();
                });
        }
        function effacerPhoto2(_iCommercantId) {
            jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto2Commercant"); ?>',
                {iCommercantId: _iCommercantId},
                function success(data) {
                    jQuery("#divPhoto2").hide();
                });
        }
        function effacerPhoto3(_iCommercantId) {
            jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto3Commercant"); ?>',
                {iCommercantId: _iCommercantId},
                function success(data) {
                    jQuery("#divPhoto3").hide();
                });
        }
        function effacerPhoto4(_iCommercantId) {
            jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto4Commercant"); ?>',
                {iCommercantId: _iCommercantId},
                function success(data) {
                    jQuery("#divPhoto4").hide();
                });
        }
        function effacerPhoto5(_iCommercantId) {
            jQuery.post('<?php echo site_url("admin/commercants/effacerPhoto5Commercant"); ?>',
                {iCommercantId: _iCommercantId},
                function success(data) {
                    jQuery("#divPhoto5").hide();
                });
        }
        function effacerPdf(_iCommercantId) {
            jQuery.post('<?php echo site_url("admin/commercants/effacerPdfCommercant"); ?>',
                {iCommercantId: _iCommercantId},
                function success(data) {
                    jQuery("#divPdf").hide();
                });
        }

        function deleteFile(_IdCommercant, _FileName) {
            jQuery.ajax({
                url: gCONFIG["SITE_URL"] + 'front/professionnels/delete_files/' + _IdCommercant + '/' + _FileName,
                dataType: 'html',
                type: 'POST',
                async: true,
                success: function (data) {
                    window.location.reload();
                }
            });
        }


        function listeSousRubrique() {

            var irubId = jQuery('#RubriqueSociete').val();
            jQuery.get(
                '<?php echo site_url("front/professionnels/testAjax"); ?>' + '/' + irubId,
                function (zReponse) {
                    // alert (zReponse) ;
                    jQuery('#trReponseRub').html("");
                    jQuery('#trReponseRub').html(zReponse);


                });
        }


        function CP_getDepartement() {
            //alert(jQuery('#CodePostalSociete').val());
            jQuery('#departementCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var CodePostalSociete = jQuery('#CodePostalSociete').val();
            jQuery.post(
                '<?php echo site_url("front/professionnels/departementcp"); ?>',
                {CodePostalSociete: CodePostalSociete},
                function (zReponse) {
                    jQuery('#departementCP_container').html(zReponse);
                });
        }

        function CP_getVille() {
            //alert(jQuery('#CodePostalSociete').val());
            jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var CodePostalSociete = jQuery('#CodePostalSociete').val();
            jQuery.post(
                '<?php echo site_url("front/professionnels/villecp"); ?>',
                {CodePostalSociete: CodePostalSociete},
                function (zReponse) {
                    jQuery('#villeCP_container').html(zReponse);
                });
        }

        function CP_getVille_D_CP() {
            //alert(jQuery('#CodePostalSociete').val());
            jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var CodePostalSociete = jQuery('#CodePostalSociete').val();
            var departement_id = jQuery('#departement_id').val();
            jQuery.post(
                '<?php echo site_url("front/professionnels/villecp"); ?>',
                {CodePostalSociete: CodePostalSociete, departement_id: departement_id},
                function (zReponse) {
                    jQuery('#villeCP_container').html(zReponse);
                });
        }
    </script>


    <div id="divInscriptionProfessionnel" class="content" align="center" style="padding:0 100px;">

        <h1 style="margin:0px; padding-top:20px;">Fiche Professionnel</h1>
        <h2 style="margin:0px;">Validation abonnement</h2>
        <?php if (isset($objCommercant->NomSociete)) echo "<h3 style='margin:0px;'>" . htmlspecialchars($objCommercant->NomSociete) . "</h3>"; ?>


        <p>
            <button class="btn btn-primary"
                    onclick="javascript:window.location.href='<?php echo site_url("admin/home/"); ?>';return false;"
                    style="background:#000000; margin-top:20px;">Retour menu
            </button>
        </p>

        <p>
            <button class="btn btn-primary"
                    onclick="javascript:window.location.href='<?php echo site_url("admin/commercants/liste/"); ?>';return false;"
                    style="background:#000000;">Retour à la liste commercants
            </button>
        </p>

        <?php if (isset($objUserIonAuth_x) && $objUserIonAuth_x->active == "1") {?>
            <div class="col-lg-12"><p><a
                        href="<?php echo site_url("admin/commercants/contenupro") . "/" . $objCommercant->IdCommercant; ?>"
                        class="btn btn-success" style="color: #fff;">Acc&eacute;der &agrave; la gestion des donn&eacute;es
                        de ce Prof&eacute;ssionnel</a></p></div>
        <?php } else if (isset($objUserIonAuth_x) && $objUserIonAuth_x->active != "1" && $objUserIonAuth_x->activation_code != null) { ?>
            <div class="col-lg-12">
                <p style="color:#FF0000;">Inscription pas encore validée, Veuillez activer l'inscription
                    <a href="<?php echo site_url("auth/activate/"). $objUserIonAuth_x->id."/".$objUserIonAuth_x->activation_code; ?>"
                        class="btn btn-success" target="_blank">ICI</a> avant d'accéder aux données !</p>
            </div>
        <?php } ?>



        <?php if (isset($_SESSION['error_fiche_com_admin'])) { ?>
            <div style="text-align:center; color:#FF0000;"><p><?php echo $_SESSION['error_fiche_com_admin']; ?></p>
            </div>
            <?php unset($_SESSION['error_fiche_com_admin']);
        } ?>

        <form name="frmInscriptionProfessionnel" id="frmInscriptionProfessionnel"
              action="<?php echo site_url("admin/commercants/modifier"); ?>" method="POST"
              enctype="multipart/form-data">

            <fieldset style="margin-top:30px;">
                <div class="col-lg-12"
                     style="font-size:20px;background-color:#000;color:#fff; height:40px; padding-top:12px; text-align:left;">
                    Les coordonnées de l'établissement
                </div>
                <div class="col-lg-12" style="background:#E1E1E1; padding-top:15px; padding-bottom:15px;">
                    <table width="100%" cellpadding="0" cellspacing="0" style="text-align:left;">

                        <tr>
                            <td>
                                <label>Rubrique : </label>
                            </td>
                            <td>
                                <select class="inputadmin" name="AssCommercantRubrique[IdRubrique]" id="RubriqueSociete"
                                        onchange="javascript:listeSousRubrique();">
                                    <option value="0">-- Veuillez choisir --</option>
                                    <?php if (sizeof($colRubriques)) { ?>
                                        <?php foreach ($colRubriques as $objRubrique) { ?>
                                            <option
                                                <?php if (isset($objAssCommercantRubrique[0]->IdRubrique) && $objAssCommercantRubrique[0]->IdRubrique == $objRubrique->IdRubrique) { ?>selected="selected"<?php } ?>
                                                value="<?php echo $objRubrique->IdRubrique; ?>"><?php echo $objRubrique->Nom; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                                <div class="FieldError" id="divErrorRubriqueSociete"></div>
                            </td>
                        </tr>
                        <tr id='trReponseRub'>
                            <td>
                                <label>Sous-rubrique : </label>
                            </td>
                            <td>
                                <select class="inputadmin" name="AssCommercantSousRubrique[IdSousRubrique]"
                                        id="SousRubriqueSociete">
                                    <option value="0">-- Veuillez choisir --</option>
                                    <?php if (sizeof($colSousRubriques)) { ?>
                                        <?php foreach ($colSousRubriques as $objSousRubrique) { ?>
                                            <option
                                                <?php if (isset($objAssCommercantSousRubrique[0]->IdSousRubrique) && $objAssCommercantSousRubrique[0]->IdSousRubrique == $objSousRubrique->IdSousRubrique) { ?>selected="selected"<?php } ?>
                                                value="<?php echo $objSousRubrique->IdSousRubrique; ?>"><?php echo $objSousRubrique->Nom; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                                <div class="FieldError" id="divErrorSousRubriqueSociete"></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Nom de la soci&eacute;t&eacute; : </label>
                            </td>
                            <td>
                                <input class="inputadmin" type="text" name="Societe[NomSociete]" id="NomSociete"
                                       value="<?php if (isset($objCommercant->NomSociete)) echo htmlspecialchars($objCommercant->NomSociete); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Siret : </label>
                            </td>
                            <td>
                                <input class="inputadmin" type="text" name="Societe[Siret]" id="SiretSociete"
                                       value="<?php if (isset($objCommercant->Siret)) echo htmlspecialchars($objCommercant->Siret); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Adresse de l'&eacute;tablissement : </label>
                            </td>
                            <td>
                                <input class="inputadmin" type="text" name="Societe[Adresse1]" id="Adresse1Societe"
                                       value="<?php if (isset($objCommercant->Adresse1)) echo htmlspecialchars($objCommercant->Adresse1); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Compl&eacute;ment d'Adresse : </label>
                            </td>
                            <td>
                                <input class="inputadmin" type="text" name="Societe[Adresse2]" id="Adresse2Societe"
                                       value="<?php if (isset($objCommercant->Adresse2)) echo htmlspecialchars($objCommercant->Adresse2); ?>"/>
                            </td>
                        </tr>


                        <tr>
                            <td>Code Postal *</td>
                            <td>
                                <input class="inputadmin" type="text" name="Societe[CodePostal]" id="CodePostalSociete"
                                       value="<?php if (isset($objCommercant->CodePostal)) echo htmlspecialchars($objCommercant->CodePostal); ?>"
                                       onblur="javascript:CP_getDepartement();CP_getVille();"/>
                            </td>
                        </tr>

                        <tr>
                            <td>Departement :</td>
                            <td>
                            <span id="departementCP_container">
                            <select name="Societe[departement_id]" id="departement_id" disabled="disabled"
                                    class="input_width inputadmin" onchange="javascript:CP_getVille_D_CP();">
                                <option value="0">-- Choisir --</option>
                                <?php if (sizeof($colDepartement)) { ?>
                                    <?php foreach ($colDepartement as $objDepartement) { ?>
                                        <option
                                            value="<?php echo $objDepartement->departement_id; ?>"><?php echo $objDepartement->departement_nom; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            </span>
                            </td>
                        </tr>

                        <tr>
                            <td>Ville *</td>
                            <td>
                            <span id="villeCP_container">
                            <input type="text"
                                   value="<?php if (isset($objCommercant->IdVille)) echo $this->mdlville->getVilleById($objCommercant->IdVille)->Nom; ?>"
                                   name="IdVille_Nom_text" id="IdVille_Nom_text" disabled="disabled"
                                   class="input_width inputadmin"/>
                            <input type="hidden"
                                   value="<?php if (isset($objCommercant->IdVille)) echo $objCommercant->IdVille; ?>"
                                   name="Societe[IdVille]" id="VilleSociete"/>
                            </span>
                            </td>
                        </tr>


                        <tr>
                            <td>
                                <label>T&eacute;l&eacute;phone fixe : </label>
                            </td>
                            <td>
                                <input type="text" class="inputadmin" name="Societe[TelFixe]" id="TelFixeSociete"
                                       value="<?php if (isset($objCommercant->TelFixe)) echo htmlspecialchars($objCommercant->TelFixe); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>T&eacute;l&eacute;phone mobile : </label>
                            </td>
                            <td>
                                <input type="text" class="inputadmin" name="Societe[TelMobile]" id="TelMobileSociete"
                                       value="<?php if (isset($objCommercant->TelMobile)) echo htmlspecialchars($objCommercant->TelMobile); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Site Web : </label>
                            </td>
                            <td>
                                <input type="text" class="inputadmin" name="Societe[SiteWeb]" id="SiteWebSociete"
                                       value="<?php if (isset($objCommercant->SiteWeb)) echo htmlspecialchars($objCommercant->SiteWeb); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Courriel : </label>
                            </td>
                            <td>
                                <input type="text" class="inputadmin" name="Societe[Email]" id="EmailSociete"
                                       value="<?php if (isset($objCommercant->Email)) echo htmlspecialchars($objCommercant->Email); ?>"/>
                                <div class="FieldError" id="divErrorEmailSociete"></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Email Identifiant : </label>
                            </td>
                            <td>
                                <?php if (isset($objUserIonAuth_x->username)) echo htmlspecialchars($objUserIonAuth_x->username); ?>
                            </td>

                        </tr>
                        <?php
                            if (isset($objUserIonAuth_x->username)){
                                $thisss = get_instance();
                                $thisss->load->model('mdlcommercant');
                                if(isset($objCommercant->Email)){
                                    $pwdClaire= $thisss->mdlcommercant->get_log_pwd_claire($objCommercant->Email);
                                    if($pwdClaire){

                        ?>
                                    <tr>
                                        <td>
                                            <label>Password :</label>
                                        </td>
                                        <td><?php echo($pwdClaire[0]->password); ?></td>
                                    </tr>
                                    <?php }; ?>
                                <?php //}else{
                                    //echo '' ;
                                }; ?>
                        <?php }; ?>    
                            

                            
                            
                        

                    </table>
                </div>
            </fieldset>
            <fieldset>
                <div class="col-lg-12"
                     style="font-size:20px;background-color:#000;color:#fff;height:40px; padding-top:12px; text-align:left;">
                    Les coordonnées du décideur
                </div>
                <div class="col-lg-12" style="padding-top:15px; padding-bottom:15px;">

                    <table width="100%" cellpadding="0" cellspacing="0" style="text-align:left;">
                        <tr>
                            <td width="300">
                                <label>Civilit&eacute; : </label>
                            </td>
                            <td>
                                <select name="Societe[Civilite]" id="CiviliteResponsableSociete" class="inputadmin">
                                    <option
                                        <?php if (isset($objCommercant->Civilite) && $objCommercant->Civilite == "0") { ?>selected="selected"<?php } ?>
                                        value="0">Monsieur
                                    </option>
                                    <option
                                        <?php if (isset($objCommercant->Civilite) && $objCommercant->Civilite == "1") { ?>selected="selected"<?php } ?>
                                        value="1">Madame
                                    </option>
                                    <option
                                        <?php if (isset($objCommercant->Civilite) && $objCommercant->Civilite == "2") { ?>selected="selected"<?php } ?>
                                        value="2">Mademoiselle
                                    </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Nom : </label>
                            </td>
                            <td>
                                <input type="text" class="inputadmin" name="Societe[Nom]" id="NomResponsableSociete"
                                       value="<?php if (isset($objCommercant->Nom)) echo htmlspecialchars($objCommercant->Nom); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Pr&eacute;nom : </label>
                            </td>
                            <td>
                                <input type="text" class="inputadmin" name="Societe[Prenom]"
                                       id="PrenomResponsableSociete"
                                       value="<?php if (isset($objCommercant->Prenom)) echo htmlspecialchars($objCommercant->Prenom); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Fonction <span class="indication">(G&eacute;rant, Directeur ...)</span>: </label>
                            </td>
                            <td>
                                <input type="text" class="inputadmin" name="Societe[Responsabilite]"
                                       id="ResponsabiliteResponsableSociete"
                                       value="<?php if (isset($objCommercant->Responsabilite)) echo htmlspecialchars($objCommercant->Responsabilite); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Email
                            </td>
                            <td>
                                <input type="text" class="inputadmin" name="Societe[Email_decideur]" id="Email_decideur"
                                       value="<?php if (isset($objCommercant->Email_decideur)) echo htmlspecialchars($objCommercant->Email_decideur); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>T&eacute;l&eacute;phone direct <span class="indication">(fixe ou portable)</span>
                                    : </label>
                            </td>
                            <td>
                                <input type="text" class="inputadmin" name="Societe[TelDirect]"
                                       id="TelDirectResponsableSociete"
                                       value="<?php if (isset($objCommercant->TelDirect)) echo htmlspecialchars($objCommercant->TelDirect); ?>"/>
                            </td>
                        </tr>

                    </table>


                </div>
            </fieldset>
            <fieldset>
                <div class="col-lg-12"
                     style="font-size:20px;background-color:#000;color:#fff;height:40px; padding-top:12px; text-align:left;">
                    Parametres de l'abonnement
                </div>
                <div class="col-lg-12" style="background:#E1E1E1;padding-top:15px; padding-bottom:15px;">

                    <table width="100%" cellpadding="0" cellspacing="0" style="text-align:left;">

                        <tr>
                            <td style="padding-bottom: 40px;">
                                Date d'inscription :
                            </td>
                            <td style="padding-bottom: 40px;">
                                <input type="text" class="inputadmin" name="Societe[datecreation]"
                                       id="DateInscription"
                                       value="<?php if (isset($objCommercant->datecreation)) echo convert_Sqldate_to_Frenchdate(date("Y-m-d",$objCommercant->datecreation)); ?>"/>
                            </td>
                        </tr>

                        <tr>
                            <td style="width:200px;height:50px; vertical-align: top;">
                                <label>Abonnement activ&eacute; : </label>
                            </td>
                            <td style="vertical-align: top;">
                                <input type="checkbox" name="Societe[IsActif]" id="IsActifSociete"
                                       value="1" <?php if (isset($objCommercant->IsActif) && $objCommercant->IsActif == "1") echo 'checked="checked"'; ?> />
                                <input type="hidden" name="Societe[IdCommercant]" id="IdCommercantSociete"
                                       value="<?php if (isset($objCommercant->IdCommercant)) echo htmlspecialchars($objCommercant->IdCommercant); ?>"/>
                            </td>
                        </tr>

                    </table>


                    <table width="100%" cellpadding="0" cellspacing="0" style="text-align:left;">
                        <tr>
                            <td style="width:200px;">
                                <label>Type d'abonnement : </label>
                            </td>
                            <td height="30">
                                <?php
                                //echo $user_groups->id;
                                /*if ($user_groups!="0") {
                                    if ($user_groups->id==3) echo "Basic";
                                    if ($user_groups->id==4) echo "Premium";
                                    if ($user_groups->id==5) echo "Platinum";
                                }*/
                                ?>


                                <select name="Abonnement_ionauth_user" id="AbonnementSociete" style=" font-weight:bold;"
                                        class="inputadmin">
                                    <option value="3" <?php if ($user_groups->id == 3) echo "selected"; ?>>Basic
                                    </option>
                                    <option value="4" <?php if ($user_groups->id == 4) echo "selected"; ?>>Premium
                                    </option>
                                    <option value="5" <?php if ($user_groups->id == 5) echo "selected"; ?>>Platinium
                                    </option>
                                </select>
                            </td>
                        </tr>
                            <tr>
                                <td>
                                    Date d&eacute;but
                                </td>
                                <td>
                                    <input type="text" class="inputadmin" name="AssAbonnementCommercant[DateDebut]"
                                           id="DateDebut"
                                           value="<?php if (isset($objAssCommercantAbonnement[0]->DateDebut)) echo convert_Sqldate_to_Frenchdate($objAssCommercantAbonnement[0]->DateDebut); ?>"/>
                                </td>
                            </tr>
                        <tr>
                            <td>
                                Date fin
                            </td>
                            <td>
                                <input type="text" class="inputadmin" name="AssAbonnementCommercant[DateFin]"
                                       id="DateFin"
                                       value="<?php if (isset($objAssCommercantAbonnement[0]->DateFin)) echo convert_Sqldate_to_Frenchdate($objAssCommercantAbonnement[0]->DateFin); ?>"/>
                                <input type="button" id="btn_datefin_plus_un_an" name="btn_datefin_plus_un_an"
                                       value="Un an de plus"/>
                                <img src="<?php echo base_url(); ?>application/resources/front/images/wait.gif"
                                     alt="loading...." id="loading_datefin_plus_un_an"/>
                            </td>
                        </tr>


                        <tr>
                            <td></td>
                            <td align="left">&nbsp;
                                <!--<input type="button" class="btnSinscrire" value="Modifier" />--></td>
                        </tr>
                    </table>
                </div>
            </fieldset>

            <fieldset>
                <div class="col-lg-12"
                     style="font-size:20px;background-color:#000;color:#fff;height:40px; padding-top:12px; text-align:left;">
                    Modules optionnels demandés
                    <small style="font-size: 16px;"><a href="<?php echo site_url('admin/manage_abonnement'); ?>"
                                                       target="_blank">(list&eacute;s dans Gestion d'abonnement &amp;
                            tarifs)</a></small>
                </div>
                <div class="col-lg-12">

                    <?php if (isset($colDemandeAbonnement)) { ?>
                        <?php foreach ($colDemandeAbonnement as $objDemandeAbonnement) { ?>
                            <div
                                style=" margin:15px 80px; height: auto; padding:10px;border-width:2px; border-style:double; border-color:black; background-color:#E1E1E1;"><?php echo $objDemandeAbonnement->Nom . " : " . $objDemandeAbonnement->Description . " => " . $objDemandeAbonnement->tarif . " &euro;"; ?></div>
                        <?php } ?>
                    <?php } else { ?>
                        <div
                            style=" margin:15px 80px;height:40px; padding-top:10px;border-width:2px; border-style:double; border-color:black; background-color:#E1E1E1;">
                            Aucun module choisi par le proféssionnel pour le moment
                        </div>
                    <?php } ?>

                </div>
            </fieldset>

            <fieldset>
                <div class="col-lg-12"
                     style="font-size:20px;background-color:#000;color:#fff;height:40px; padding-top:12px; text-align:left;">
                    Activation des Modules Professionnels
                </div>
                
                <div class="col-lg-12" style="padding: 15px;background:#E1E1E1;">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>MODULE</th>
                            <th>Activation</th>
                            <th>Unité</th>
                            <th>Dans Sortez</th>
                            <th>Export</th>
                            <th>Limite export</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>ARTICLE</td>
                            <td>
                             <script type="text/javascript">
                                    $(function () {
                                        $("#check_blog").click(function () {
                                            if ($('#check_blog').attr('checked')) {
                                                $("#check_blog_value").val("1");
                                            } else {
                                                $("#check_blog_value").val("0");
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_blog" type="checkbox"
                                       name="check_blog" <?php if (isset($objCommercant->blog) && $objCommercant->blog == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[blog]" id="check_blog_value"
                                       value="<?php if (isset($objCommercant->blog) && $objCommercant->blog == "1") echo '1'; else echo '0'; ?>"/>
                            </td>
                            <td>
                            
<select name="Societe[limit_article]" id="limit_article" style=" font-weight:bold;" class="form-control">
    <option value="5" <?php if ($objCommercant->limit_article == 5) echo "selected"; ?>>5</option>
    <option value="10" <?php if ($objCommercant->limit_article == 10) echo "selected"; ?>>10</option>
    <option value="25" <?php if ($objCommercant->limit_article == 25) echo "selected"; ?>>25</option>
    <option value="50" <?php if ($objCommercant->limit_article == 50) echo "selected"; ?>>50</option>
    <option value="100" <?php if ($objCommercant->limit_article == 100) echo "selected"; ?>>100</option>
    <option value="1000" <?php if ($objCommercant->limit_article == 1000) echo "selected"; ?>>Illimité</option>
</select>
                            
                            </td>
                            <td>
                            <script type="text/javascript">
                                    $(function () {
                                        $("#check_referencement_article").click(function () {
                                            if ($('#check_referencement_article').attr('checked')) {
                                                $("#check_referencement_article_value").val("1");
                                            } else {
                                                $("#check_referencement_article_value").val("0");
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_referencement_article" type="checkbox"
                                       name="check_referencement_article" <?php if (isset($objCommercant->referencement_article) && $objCommercant->referencement_article == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[referencement_article]"
                                       id="check_referencement_article_value"
                                       value="<?php if (isset($objCommercant->referencement_article) && $objCommercant->referencement_article == "1") echo '1'; else echo '0'; ?>"/>
                            </td>
                            <td>
                            <script type="text/javascript">
                                    $(function () {
                                        $("#check_article_export").click(function () {
                                            if ($('#check_article_export').attr('checked')) {
                                                $("#check_article_export_value").val("1");
                                            } else {
                                                $("#check_article_export_value").val("0");
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_article_export" type="checkbox"
                                       name="check_article_export" <?php if (isset($objCommercant->article_export) && $objCommercant->article_export == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[article_export]" id="check_article_export_value"
                                       value="<?php if (isset($objCommercant->article_export) && $objCommercant->article_export == "1") echo '1'; else echo '0'; ?>"/>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>AGENDA</td>
                            <td>
                            <script type="text/javascript">
                                    $(function () {
                                        $("#check_agenda").click(function () {
                                            if ($('#check_agenda').attr('checked')) {
                                                $("#check_agenda_value").val("1");
                                            } else {
                                                $("#check_agenda_value").val("0");
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_agenda" type="checkbox"
                                       name="check_agenda" <?php if (isset($objCommercant->agenda) && $objCommercant->agenda == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[agenda]" id="check_agenda_value"
                                       value="<?php if (isset($objCommercant->agenda) && $objCommercant->agenda == "1") echo '1'; else echo '0'; ?>"/>
                            </td>
                            <td>
                            
                            <select name="Societe[limit_agenda]" id="limit_agenda" style=" font-weight:bold;" class="form-control">
    <option value="5" <?php if ($objCommercant->limit_agenda == 5) echo "selected"; ?>>5</option>
    <option value="10" <?php if ($objCommercant->limit_agenda == 10) echo "selected"; ?>>10</option>
    <option value="25" <?php if ($objCommercant->limit_agenda == 25) echo "selected"; ?>>25</option>
    <option value="50" <?php if ($objCommercant->limit_agenda == 50) echo "selected"; ?>>50</option>
    <option value="100" <?php if ($objCommercant->limit_agenda == 100) echo "selected"; ?>>100</option>
    <option value="1000" <?php if ($objCommercant->limit_agenda == 1000) echo "selected"; ?>>Illimité</option>
</select>
                            
                            </td>
                            <td>
                            <script type="text/javascript">
                                    $(function () {
                                        $("#check_referencement_agenda").click(function () {
                                            if ($('#check_referencement_agenda').attr('checked')) {
                                                $("#check_referencement_agenda_value").val("1");
                                            } else {
                                                $("#check_referencement_agenda_value").val("0");
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_referencement_agenda" type="checkbox"
                                       name="check_referencement_agenda" <?php if (isset($objCommercant->referencement_agenda) && $objCommercant->referencement_agenda == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[referencement_agenda]"
                                       id="check_referencement_agenda_value"
                                       value="<?php if (isset($objCommercant->referencement_agenda) && $objCommercant->referencement_agenda == "1") echo '1'; else echo '0'; ?>"/>
                            </td>
                            <td>
                            <script type="text/javascript">
                                    $(function () {
                                        $("#check_agenda_export").click(function () {
                                            if ($('#check_agenda_export').attr('checked')) {
                                                $("#check_agenda_export_value").val("1");
                                            } else {
                                                $("#check_agenda_export_value").val("0");
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_agenda_export" type="checkbox"
                                       name="check_agenda_export" <?php if (isset($objCommercant->agenda_export) && $objCommercant->agenda_export == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[agenda_export]" id="check_agenda_export_value"
                                       value="<?php if (isset($objCommercant->agenda_export) && $objCommercant->agenda_export == "1") echo '1'; else echo '0'; ?>"/>
                            </td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>FESTIVAL</td>
                            <td>
                            <script type="text/javascript">
                                    $(function () {
                                        $("#check_festival").click(function () {
                                            if ($('#check_festival').attr('checked')) {
                                                $("#check_festival_value").val("1");
                                            } else {
                                                $("#check_festival_value").val("0");
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_festival" type="checkbox"
                                       name="check_festival" <?php if (isset($objCommercant->festival) && $objCommercant->festival == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[festival]" id="check_festival_value"
                                       value="<?php if (isset($objCommercant->festival) && $objCommercant->festival == "1") echo '1'; else echo '0'; ?>"/>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>
                            <script type="text/javascript">
                                    $(function () {
                                        $("#check_festival_export").click(function () {
                                            if ($('#check_festival_export').attr('checked')) {
                                                $("#check_festival_export_value").val("1");
                                            } else {
                                                $("#check_festival_export_value").val("0");
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_festival_export" type="checkbox"
                                       name="check_festival_export" <?php if (isset($objCommercant->festival_export) && $objCommercant->festival_export == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[festival_export]" id="check_festival_export_value"
                                       value="<?php if (isset($objCommercant->festival_export) && $objCommercant->festival_export == "1") echo '1'; else echo '0'; ?>"/>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        

                        <tr>
                          <td>ANNUAIRE</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>
                          <script type="text/javascript">
                                    $(function () {
                                        $("#check_referencement").click(function () {
                                            if ($('#check_referencement').attr('checked')) {
                                                $("#check_referencement_value").val("1");
                                            } else {
                                                $("#check_referencement_value").val("0");
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_referencement" type="checkbox"
                                       name="check_referencement" <?php if (isset($objCommercant->referencement_annuaire) && $objCommercant->referencement_annuaire == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[referencement_annuaire]"
                                       id="check_referencement_value"
                                       value="<?php if (isset($objCommercant->referencement_annuaire) && $objCommercant->referencement_annuaire == "1") echo '1'; else echo '0'; ?>"/>
                          </td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td>BONS PLANS (carte de fidélité)</td>
                          <td>
                          <script type="text/javascript">
                                    $(function () {
                                        $("#check_module_bonplan").click(function () {
                                            if ($('#check_module_bonplan').attr('checked')) {
                                                $("#check_module_bonplan_value").val("1");
                                            } else {
                                                $("#check_module_bonplan_value").val("0");
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_module_bonplan" type="checkbox"
                                       name="check_module_bonplan" <?php if (isset($objCommercant->bonplan) && $objCommercant->bonplan == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[bonplan]" id="check_module_bonplan_value"
                                       value="<?php if (isset($objCommercant->bonplan) && $objCommercant->bonplan == "1") echo '1'; else echo '0'; ?>"/>
                          </td>
                          <td>
                          
                          <select name="Societe[limit_bonplan]" id="limit_bonplan" style=" font-weight:bold;" class="form-control">
    <option value="1" <?php if ($objCommercant->limit_bonplan == 1) echo "selected"; ?>>1</option>
    <option value="5" <?php if ($objCommercant->limit_bonplan == 5) echo "selected"; ?>>5</option>
    <option value="25" <?php if ($objCommercant->limit_bonplan == 25) echo "selected"; ?>>25</option>
    <option value="100" <?php if ($objCommercant->limit_bonplan == 100) echo "selected"; ?>>100</option>
    <option value="1000" <?php if ($objCommercant->limit_bonplan == 1000) echo "selected"; ?>>Illimité</option>
</select>
                          
                          </td>
                          <td>
                          <script type="text/javascript">
                                    $(function () {
                                        $("#check_referencement_bonplan").click(function () {
                                            if ($('#check_referencement_bonplan').attr('checked')) {
                                                $("#check_referencement_bonplan_value").val("1");
                                            } else {
                                                $("#check_referencement_bonplan_value").val("0");
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_referencement_bonplan" type="checkbox"
                                       name="check_referencement_bonplan" <?php if (isset($objCommercant->referencement_bonplan) && $objCommercant->referencement_bonplan == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[referencement_bonplan]"
                                       id="check_referencement_bonplan_value"
                                       value="<?php if (isset($objCommercant->referencement_bonplan) && $objCommercant->referencement_bonplan == "1") echo '1'; else echo '0'; ?>"/>
                          </td>
                          <td>
                            <script type="text/javascript">
                                    $(function () {
                                        $("#check_bonplan_export").click(function () {
                                            if ($('#check_bonplan_export').attr('checked')) {
                                                $("#check_bonplan_export_value").val("1");
                                            } else {
                                                $("#check_bonplan_export_value").val("0");
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_bonplan_export" type="checkbox"
                                       name="check_bonplan_export" <?php if (isset($objCommercant->bonplan_export) && $objCommercant->bonplan_export == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[bonplan_export]" id="check_bonplan_export_value"
                                       value="<?php if (isset($objCommercant->bonplan_export) && $objCommercant->bonplan_export == "1") echo '1'; else echo '0'; ?>"/>
                            </td>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td>BOUTIQUES</td>
                          <td>
                          <script type="text/javascript">
                                    $(function () {
                                        $("#check_module_annonce").click(function () {
                                            if ($('#check_module_annonce').attr('checked')) {
                                                $("#check_module_annonce_value").val("1");
                                            } else {
                                                $("#check_module_annonce_value").val("0");
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_module_annonce" type="checkbox"
                                       name="check_module_annonce" <?php if (isset($objCommercant->annonce) && $objCommercant->annonce == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[annonce]" id="check_module_annonce_value"
                                       value="<?php if (isset($objCommercant->annonce) && $objCommercant->annonce == "1") echo '1'; else echo '0'; ?>"/>
                          </td>
                          <td>
                          
                          <select name="Societe[limit_annonce]" id="limit_annonce" style=" font-weight:bold;" class="form-control">
    <option value="5" <?php if ($objCommercant->limit_annonce == 5) echo "selected"; ?>>5</option>
    <option value="25" <?php if ($objCommercant->limit_annonce == 25) echo "selected"; ?>>25</option>
    <option value="50" <?php if ($objCommercant->limit_annonce == 50) echo "selected"; ?>>50</option>
    <option value="100" <?php if ($objCommercant->limit_annonce == 100) echo "selected"; ?>>100</option>
    <option value="1000" <?php if ($objCommercant->limit_annonce == 1000) echo "selected"; ?>>Illimité</option>
</select>
                          
                          </td>
                          <td>
                          <script type="text/javascript">
                                    $(function () {
                                        $("#check_referencement_annonce").click(function () {
                                            if ($('#check_referencement_annonce').attr('checked')) {
                                                $("#check_referencement_annonce_value").val("1");
                                            } else {
                                                $("#check_referencement_annonce_value").val("0");
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_referencement_annonce" type="checkbox"
                                       name="check_referencement_annonce" <?php if (isset($objCommercant->referencement_annonce) && $objCommercant->referencement_annonce == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[referencement_annonce]"
                                       id="check_referencement_annonce_value"
                                       value="<?php if (isset($objCommercant->referencement_annonce) && $objCommercant->referencement_annonce == "1") echo '1'; else echo '0'; ?>"/>
                           </td>            
                         <td>
                            <script type="text/javascript">
                                    $(function () {
                                        $("#check_annonce_export").click(function () {
                                            if ($('#check_annonce_export').attr('checked')) {
                                                $("#check_annonce_export_value").val("1");
                                            } else {
                                                $("#check_annonce_export_value").val("0");
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_annonce_export" type="checkbox"
                                       name="check_annonce_export" <?php if (isset($objCommercant->annonce_export) && $objCommercant->annonce_export == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[annonce_export]" id="check_annonce_export_value"
                                       value="<?php if (isset($objCommercant->annonce_export) && $objCommercant->annonce_export == "1") echo '1'; else echo '0'; ?>"/>
                            </td>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>FIDELITE</td>
                            <td></td>
                            <td></td>
                            <td>
                            <script type="text/javascript">
                                    $(function () {
                                        $("#check_referencement_fidelite").click(function () {
                                            if ($('#check_referencement_fidelite').attr('checked')) {
                                                $("#check_referencement_fidelite_value").val("1");
                                            } else {
                                                $("#check_referencement_fidelite_value").val("0");
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_referencement_fidelite" type="checkbox"
                                       name="check_referencement_fidelite" <?php if (isset($objCommercant->referencement_fidelite) && $objCommercant->referencement_fidelite == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[referencement_fidelite]"
                                       id="check_referencement_fidelite_value"
                                       value="<?php if (isset($objCommercant->referencement_fidelite) && $objCommercant->referencement_fidelite == "1") echo '1'; else echo '0'; ?>"/>
                            </td>
                            <td>&nbsp;</td>
                            <td></td>
                        </tr>


                        <tr>
                            <td>Gîtes et chambres d’hôtes </td>
                            <td> <script type="text/javascript">
                                    $(function () {
                                        $("#check_referencement_gites").click(function () {
                                            if ($('#check_referencement_gites').attr('checked')) {
                                                $("#check_referencement_gite_value").val(1);
                                            } else {
                                                $("#check_referencement_gite_value").val(0);
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_referencement_gites" type="checkbox"
                                       name="check_referencement_gites" <?php if (isset($objCommercant->referencement_gite) && $objCommercant->referencement_gite == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[referencement_gite]"
                                       id="check_referencement_gite_value"
                                       value="<?php if (isset($objCommercant->referencement_gite) && $objCommercant->referencement_gite == "1") echo '1'; else echo '0'; ?>"/>
                            </td>
                            <td></td>
                            <td>
                                </td>
                            <td>&nbsp;</td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>Module Restaurant </td>
                            <td> <script type="text/javascript">
                                    $(function () {
                                        $("#check_referencement_resto").click(function () {
                                            if ($('#check_referencement_resto').attr('checked')) {
                                                $("#check_referencement_resto_value").val(1);
                                            } else {
                                                $("#check_referencement_resto_value").val(0);
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_referencement_resto" type="checkbox"
                                       name="check_referencement_resto" <?php if (isset($objCommercant->referencement_resto) && $objCommercant->referencement_resto == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[referencement_resto]"
                                       id="check_referencement_resto_value"
                                       value="<?php if (isset($objCommercant->referencement_resto) && $objCommercant->referencement_resto == "1") echo '1'; else echo '0'; ?>"/>
                            </td>
                            <td></td>
                            <td>
                            </td>
                            <td>&nbsp;</td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>Module Newsletters </td>
                            <td> <script type="text/javascript">
                                    $(function () {
                                        $("#check_referencement_news").click(function () {
                                            if ($('#check_referencement_news').attr('checked')) {
                                                $("#check_referencement_news_value").val(1);
                                            } else {
                                                $("#check_referencement_news_value").val(0);
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_referencement_news" type="checkbox"
                                       name="check_referencement_news" <?php if (isset($objCommercant->referencement_news) && $objCommercant->referencement_news == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[referencement_news]"
                                       id="check_referencement_news_value"
                                       value="<?php if (isset($objCommercant->referencement_news) && $objCommercant->referencement_news == "1") echo '1'; else echo '0'; ?>"/>
                            </td>
                            <td></td>
                            <td>
                            </td>
                            <td>&nbsp;</td>
                            <td></td>
                        </tr>

                         <tr>
                            <td>Module nom domaine privé</td>
                            <td> <script type="text/javascript">
                                    $(function () {
                                        $("#check_private_domaine_name").click(function () {
                                            if ($('#check_private_domaine_name').attr('checked')) {
                                                $("#check_private_domaine_name_value").val(1);
                                            } else {
                                                $("#check_private_domaine_name_value").val(0);
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_private_domaine_name" type="checkbox"
                                       name="check_private_domaine_name" <?php if (isset($objCommercant->private_domaine_name) && $objCommercant->private_domaine_name == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[private_domaine_name]"
                                       id="check_private_domaine_name_value"
                                       value="<?php if (isset($objCommercant->private_domaine_name) && $objCommercant->private_domaine_name == "1") echo '1'; else echo '0'; ?>"/>
                            </td>
                            <td></td>
                            <td>
                            </td>
                            <td>&nbsp;</td>
                            <td></td>
                        </tr>
                    <tr>
                            <td>Formulaire commande en ligne</td>
                            <td> <script type="text/javascript">
                                    $(function () {
                                        $("#check_referencement_click").click(function () {
                                            if ($('#check_referencement_click').attr('checked')) {
                                                $("#check_referencement_click_value").val(1);
                                            } else {
                                                $("#check_referencement_click_value").val(0);
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_referencement_click" type="checkbox"
                                       name="check_referencement_click" <?php if (isset($objCommercant->referencement_click) && $objCommercant->referencement_click == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[referencement_click]"
                                       id="check_referencement_click_value"
                                       value="<?php if (isset($objCommercant->referencement_click) && $objCommercant->referencement_click == "1") echo '1'; else echo '0'; ?>"/>
                            </td>
                            <td></td>
                            <td>
                            </td>
                            <td>&nbsp;</td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>Demande de devis</td>
                            <td> <script type="text/javascript">
                                    $(function () {
                                        $("#check_active_devis").click(function () {
                                            if ($('#check_active_devis').attr('checked')) {
                                                $("#check_active_devis_value").val(1);
                                            } else {
                                                $("#check_active_devis_value").val(0);
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_active_devis" type="checkbox"
                                       name="check_active_devis" <?php if (isset($objCommercant->active_devis) && $objCommercant->active_devis == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[active_devis]"
                                       id="check_active_devis_value"
                                       value="<?php if (isset($objCommercant->active_devis) && $objCommercant->active_devis == "1") echo '1'; else echo '0'; ?>"/>
                            </td>
                            <td></td>
                            <td>
                            </td>
                            <td>&nbsp;</td>
                            <td></td>
                        </tr> 

                        <tr>
                            <td>SEO</td>
                            <td> <script type="text/javascript">
                                    $(function () {
                                        $("#check_active_seo").click(function () {
                                            if ($('#check_active_seo').attr('checked')) {
                                                $("#check_active_seo_value").val(1);
                                            } else {
                                                $("#check_active_seo_value").val(0);
                                            }
                                        });

                                    })
                                </script>
                                <input id="check_active_seo" type="checkbox"
                                       name="check_active_seo" <?php if (isset($objCommercant->active_seo) && $objCommercant->active_seo == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="Societe[active_seo]"
                                       id="check_active_seo_value"
                                       value="<?php if (isset($objCommercant->active_seo) && $objCommercant->active_seo == "1") echo '1'; else echo '0'; ?>"/>
                            </td>
                            <td></td>
                            <td>
                            </td>
                            <td>&nbsp;</td>
                            <td></td>
                        </tr>

                        </tbody>
                    </table>
            </div>
            </fieldset>


            <div class="col-lg-12" style="margin:30px 0; text-align:center;">
                <button class="btn btn-primary" style="background:#000000;">Enregistrer</button>
            </div>

            <input type="hidden" name="pdfAssocie" id="pdfAssocie"
                   value="<?php if (isset($objCommercant->Pdf)) echo htmlspecialchars($objCommercant->Pdf); ?>"/>
            <input type="hidden" name="photo1Associe" id="photo1Associe"
                   value="<?php if (isset($objCommercant->Photo1)) echo htmlspecialchars($objCommercant->Photo1); ?>"/>
            <input type="hidden" name="photo2Associe" id="photo2Associe"
                   value="<?php if (isset($objCommercant->Photo2)) echo htmlspecialchars($objCommercant->Photo2); ?>"/>
            <input type="hidden" name="photo3Associe" id="photo3Associe"
                   value="<?php if (isset($objCommercant->Photo3)) echo htmlspecialchars($objCommercant->Photo3); ?>"/>
            <input type="hidden" name="photo4Associe" id="photo4Associe"
                   value="<?php if (isset($objCommercant->Photo4)) echo htmlspecialchars($objCommercant->Photo4); ?>"/>
            <input type="hidden" name="photo5Associe" id="photo5Associe"
                   value="<?php if (isset($objCommercant->Photo5)) echo htmlspecialchars($objCommercant->Photo5); ?>"/>
            <input type="hidden" name="photoAccueilAssocie" id="photoAccueilAssocie"
                   value="<?php if (isset($objCommercant->PhotoAccueil)) echo htmlspecialchars($objCommercant->PhotoAccueil); ?>"/>
        </form>
    </div>

<?php $this->load->view("privicarte/admin/vwFooter2013"); ?>