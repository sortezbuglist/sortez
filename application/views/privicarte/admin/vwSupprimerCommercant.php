<?php $data["zTitle"] = 'Suppression compte professionnel'; ?>
<?php $this->load->view("privicarte/admin/vwHeader2013", $data); ?>
<style type="text/css">
<!--
.col-lg-12 .supprimer_div a {
	color: #FFFFFF !important;
}
-->
</style>


<div class="col-lg-12 supprimer_div">

<h1>Suppression compte professionnel</h1>
<h2><?php if (isset($objCommercant)) echo $objCommercant->NomSociete;?></h2>

<div class="alert alert-danger">
  <strong>Attention !</strong> En supprimant ce compte Commercant, vous supprimer toutes les donn&eacute;es rattach&eacute;es à ce compte.
</div>

<?php if (isset($colBonplan) && count($colBonplan)>0) { ?>
<h3>Bonplan :</h3>
<table class="table-striped table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th scope="col">TITRE</th>
    <th scope="col">DATE FIN</th>
    <th scope="col">QUANTITE DISPO</th>
    <th scope="col">&nbsp;</th>
  </tr>
  <tr>
    <td><?php echo $colBonplan->bonplan_titre; ?></td>
    <td><?php echo convert_datetime_to_fr($colBonplan->bonplan_date_fin); ?></td>
    <td><?php echo $colBonplan->bonplan_quantite; ?></td>
    <td><a href="javascript:void(0);">Voir</a></td>
  </tr>
</table>
<?php } ?>

<?php if (isset($colAnnonces) && count($colAnnonces)>0) { ?>
<h3>Annonces :</h3>
<table class="table-striped table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th scope="col" style="width:600px;">TITRE</th>
    <th scope="col">RUBRIQUE</th>
    <th scope="col">Ville</th>
    <th scope="col">&nbsp;</th>
  </tr>
  <?php foreach ($colAnnonces as $annonce_item) { ?>
  <tr>
    <td><?php echo $annonce_item->texte_courte; ?></td>
    <td><?php echo $annonce_item->rubrique; ?></td>
    <td><?php echo $annonce_item->ville; ?></td>
    <td><a href="javascript:void(0);">Voir</a></td>
  </tr>
  <?php } ?>
</table>
<?php } ?>

<?php if (isset($colAgenda) && count($colAgenda)>0) { ?>
<h3>Agenda :</h3>
<table class="table-striped table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th scope="col" style="width:600px;">TITRE</th>
    <th scope="col">DATE FIN</th>
    <th scope="col">Ville</th>
    <th scope="col">&nbsp;</th>
  </tr>
  <?php foreach ($colAgenda as $agenda_item) { ?>
  <tr>
    <td><?php echo $agenda_item->nom_manifestation; ?></td>
    <td><?php echo convert_datetime_to_fr($agenda_item->date_fin); ?></td>
    <td><?php echo $agenda_item->ville; ?></td>
    <td><a href="javascript:void(0);">Voir</a></td>
  </tr>
  <?php } ?>
</table>
<?php } ?>





<div class="alert alert-danger" style="margin-top:30px;">
  <strong>Etes-vous s&ucirc;r de vouloir supprimer ce compte avec les données !?</strong>
</div>

<div class="row">
    <div class="col-lg-6">
   		<a href="<?php echo site_url("admin/commercants/liste"); ?>" class="btn btn-primary" style="color: #ffffff">Annuler</a>    
    </div>
    <div class="col-lg-6" style=" text-align:right;">
   		<a href="<?php echo site_url("admin/commercants/supprimer_validate/".$objCommercant->IdCommercant); ?>" class="btn btn-danger" style="color: #ffffff">Supprimer definitivement toutes les donn&eacute;es</a>    
    </div>
</div>




</div>

    
<?php $this->load->view("privicarte/admin/vwFooter2013"); ?>