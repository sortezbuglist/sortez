<style type="text/css">
    .grey_bloc_independent {
        background-color: #DFE0E1;
    }

    .videodetector {
        position      : relative;
        width         : 100%;
        height        : 320px;
        padding: 0;
        margin: 10px;
    }

    .videodetector iframe {
        position: absolute;
        top     : 0;
        left    : 0;
        width   : 100%;
        height  : 100%;
    }
    .btn-info,
    .btn-info:hover,
    .btn-info:active,
    .btn-info:visited,
    .btn-info:focus {
        background-color: #3E74EF;
        border-color: #3E74EF;
    }

    .btn-info {
        background-image: -webkit-linear-gradient(top, #2aabd2 0%, #3E74EF 100%);
        background-image: -o-linear-gradient(top, #2aabd2 0%, #3E74EF 100%);
        background-image: -webkit-gradient(linear, left top, left bottom, from(#2aabd2), to(#3E74EF));
        background-image: linear-gradient(to bottom, #2aabd2 0%, #3E74EF 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff5bc0de', endColorstr='#ff2aabd2', GradientType=0);
        filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
        background-repeat: repeat-x;
        border-color: #3E74EF;
    }

    .btn-danger {
        background-image: -webkit-linear-gradient(top, #c12e2a 0%, #FF0000 100%);
        background-image: -o-linear-gradient(top, #c12e2a 0%, #FF0000 100%);
        background-image: -webkit-gradient(linear, left top, left bottom, from(#c12e2a), to(#FF0000));
        background-image: linear-gradient(to bottom, #c12e2a 0%, #FF0000 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffd9534f', endColorstr='#ffc12e2a', GradientType=0);
        filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
        background-repeat: repeat-x;
        border-color: #FF0000;
    }

    .btn-danger,
    .btn-danger:hover,
    .btn-danger:active,
    .btn-danger:visited,
    .btn-danger:focus {
        background-color: #FF0000;
        border-color: #FF0000;
    }

    .link_button {
        background-color: #006699;
        border: 2px solid #003366;
        border-radius: 8px;
        color: #ffffff;
        font-size: 12px;
        padding: 10px 15px;
    }

    .link_button:hover {
        text-decoration: none;
        color: #999999;
    }

    .stl_long_input_platinum {
        width: 413px;
    }

    .stl_long_input_platinum_td {
        width: 180px;
        height: 30px;
    }

    .div_stl_long_platinum {
        background-color: #3653A3;
        color: #FFFFFF;
        font-family: "Arial", sans-serif;
        font-size: 13px;
        font-weight: 700;
        line-height: 1.23em;
        height: 40px;
        padding-top: 12px;
        padding-left: 20px;
        margin-top: 12px;
        margin-bottom: 15px;
    }

    .div_error_taille_3_4 {
        color: #F00;
    }

    .FieldError {
        color: #FF0000;
        font-weight: bold;
    }

    <?php if (!isset($objGlissiere->isActive_presentation_1) || (isset($objGlissiere->isActive_presentation_1) && $objGlissiere->isActive_presentation_1 ==0)) {?>
    #gl1_tittle, #gl1_tittle2, #gl1_content, #gl1_content2, #gl1_img, #gl1_img2, #btn_gli1, #btn_gli2, #alllink, #alllink2 {
        /*//display: none;*/
    }

    <?php } elseif ($objGlissiere->isActive_presentation_1 ==1)  {?>
    #gl1_tittle, #gl1_content, {
        display: block !important;
    }


    <?php } ?>
    <?php if (isset($objGlissiere->isActive_presentation_1) && $objGlissiere->isActive_presentation_1 ==1 AND isset($objGlissiere->nombre_blick_gli_presentation) && $objGlissiere->nombre_blick_gli_presentation ==1)  {?>
    #gl1_tittle, #gl1_content, #glissiere1content, #champ1gli1, {
        display: block;
    }

    #champ2_gli1content {
        display: none;
    }

    <?php } elseif (isset($objGlissiere->isActive_presentation_1) && $objGlissiere->isActive_presentation_1 ==1 AND isset($objGlissiere->nombre_blick_gli_presentation) && $objGlissiere->nombre_blick_gli_presentation ==2){ ?>
    #gl1_tittle, #gl1_content, #glissiere1content, #champ1gli1, #champ2_gli1content {
        display: block;
    }

    <?php }elseif (isset($objGlissiere->isActive_presentation_1) && $objGlissiere->isActive_presentation_1 ==0){ ?>
    #dispnbgli {
        display: none;
    }

    <?php } ?>

    <?php if (isset($objGlissiere->is_activ_btn_glissiere1_champ1) && $objGlissiere->is_activ_btn_glissiere1_champ1==1){?>
    #alllink {
        display: block;
    }

    <?php }elseif (!isset($objGlissiere->is_activ_btn_glissiere1_champ1) || (isset($objGlissiere->is_activ_btn_glissiere1_champ1) && $objGlissiere->is_activ_btn_glissiere1_champ1==0)) {?>
    #alllink {
        display: none;
    }

    <?php } ?>

    <?php if (isset($objGlissiere->is_activ_btn_glissiere1_champ2) && $objGlissiere->is_activ_btn_glissiere1_champ2==1 AND isset($objGlissiere->is_activ_btn_glissiere1_champ1) && $objGlissiere->is_activ_btn_glissiere1_champ1==1){?>
    #alllink2 {
        display: block;
    }

    <?php }elseif (isset($objGlissiere->presentation_1_contenu2) && $objGlissiere->presentation_1_contenu2=='') {?>
    #alllink2, #gl1_content2, #gl1_tittle2, #gl1_img2, #btn_gli2 {
        /*//display: none;*/
    }

    <?php } ?>

    <?php if (isset($objGlissiere->is_activ_btn_glissiere1_champ1) && $objGlissiere->is_activ_btn_glissiere1_champ1==1){?>
    #dispnbgli {
        display: block;
    }

    <?php }elseif (!isset($objGlissiere->is_activ_btn_glissiere1_champ1) || (isset($objGlissiere->is_activ_btn_glissiere1_champ1) && $objGlissiere->is_activ_btn_glissiere1_champ1==0)) {?>
    #dispnbgli {
        display: none;
    }

    <?php } ?>



    .img_tab, .img_tab2 {
        margin-top: 30px;
        margin-bottom: 30px;
    }

    #btn_opt {
        display: none;
    }


    <?php if (!isset($objGlissiere->type_bloc) ||  (isset($objGlissiere->type_bloc) AND ($objGlissiere->type_bloc==0 || $objGlissiere->type_bloc=="") )) { ?>
    #bloc1, #bloc2, #fidelity_config, #bonplan_config {
        display: none;
    }

    <?php } elseif (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc==1){ ?>
    #bloc2, #fidelity_config, #bonplan_config {
        display: none;
    }

    #bloc1 {
        display: block;
    }

    <?php } elseif (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc==2) { ?>
    #bloc1, #bloc2, #fidelity_config {
        display: block;
    }

    <?php } ?>


    /******** START SHOW & HIDE BLOC 1 ACCUEIL*/
    <?php if (!isset($objGlissiere->is_activ_bloc) ||  (isset($objGlissiere->is_activ_bloc) AND ($objGlissiere->is_activ_bloc==0 || $objGlissiere->is_activ_bloc=="") )) { ?>
    #type_bloc_choose { display: none; }
    #bloc1, #bloc2, #fidelity_config, #bonplan_config { display: block; }
    <?php } elseif (isset($objGlissiere->is_activ_bloc) AND $objGlissiere->is_activ_bloc == 1 ){ ?>
    #type_bloc_choose { display: block; }
    <?php } ?>

    <?php if (isset($objGlissiere->is_activ_btn_bloc1) && $objGlissiere->is_activ_btn_bloc1 == 1) { ?>
    #link_bloc1 { display: block; }
    <?php } else { ?>
    #link_bloc1 { display: none; }
    <?php } ?>
    <?php if (isset($objGlissiere->is_activ_btn_bloc2) && $objGlissiere->is_activ_btn_bloc2 == 1) { ?>
    #link_bloc2 { display: block; }
    <?php } else { ?>
    #link_bloc2 { display: none; }
    <?php } ?>
    /******** END SHOW & HIDE BLOC 1 ACCUEIL*/


    <?php if (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc =='1bp' AND $objGlissiere->is_activ_bloc == 1){ ?>
    #bonplan_config {
        display: block;
    }

    #bloc1, #bloc2, #fidelity_config {
        display: none;
    }

    <?php } else { ?>
    #bonplan_config {
        display: none;
    }

    <?php } ?>

    <?php if (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc =='1fd' AND $objGlissiere->is_activ_bloc == 1){ ?>
    #fidelity_config {
        display: block;
    }

    #bloc1, #bloc2, bonplan_config {
        display: none;
    }

    <?php } else { ?>
    #fidelity_config {
        display: none;
    }

    <?php } ?>
    <?php if (!isset($objGlissiere->is_activ_btn_fd) || (isset($objGlissiere->is_activ_btn_fd) AND $objGlissiere->is_activ_btn_fd== 0)) { ?>

    #link_fd {
        display: none;
    }

    <?php } elseif (isset($objGlissiere->is_activ_btn_fd) AND $objGlissiere->is_activ_btn_fd== 1 ){ ?>
    #link_fd {
        display: block;
    }

    <?php } ?>

    <?php if (!isset($objGlissiere->is_activ_btn_bp) || (isset($objGlissiere->is_activ_btn_bp) AND $objGlissiere->is_activ_btn_bp== 0)) { ?>

    #link_bp {
        display: none;
    }

    <?php } elseif (isset($objGlissiere->is_activ_btn_bp) AND $objGlissiere->is_activ_btn_bp== 1 ){ ?>
    #link_bp {
        display: block;
    }

    <?php } ?>
    <?php if (isset($objGlissiere->nombre_blick_gli_presentation) AND $objGlissiere->nombre_blick_gli_presentation ==1){ ?>

    #btn_gli1, #gl1_img, #gl1_tittle, #gl1_content, {
        display: block !important;
    }

    #gl1_tittle2,  {
        display: none !important;
    }

    <?php } elseif (isset($objGlissiere->nombre_blick_gli_presentation) AND $objGlissiere->nombre_blick_gli_presentation ==2){ ?>
    #btn_gli1, #gl1_img, #gl1_tittle, #gl1_content, #gl1_tittle2, #gl1_content2 {
        display: block !important;
    }

    <?php } ?>
    .btn_vert:hover{
        cursor: pointer;
        background-color: rgb(215, 235, 215);
    }

    .rowbtn{
        background-color: rgb(215, 235, 215);
    }
    .btn_vert:active{
        border: double;
        border-color: green;
        background-color: rgb(215, 235, 215);
    }
</style>