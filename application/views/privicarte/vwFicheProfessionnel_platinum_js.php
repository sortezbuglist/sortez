<script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/fields.check.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {

        <?php if (isset($current_page) AND $current_page == 'design'){ ?>
        show_btn_pro_contenus_design();
        <?php }elseif (isset($current_page) AND $current_page == 'home'){ ?>
        show_btn_pro_contenus_home();
        <?php }elseif (isset($current_page) AND $current_page == 'page1'){ ?>
        show_btn_pro_contenus_page1();
        <?php }elseif (isset($current_page) AND $current_page == 'page2'){ ?>
        show_btn_pro_contenus_page2();
        <?php }else{ ?>
        show_btn_pro_contenus_design();
        <?php } ?>
        <?php if (isset($page_data) && $page_data == 'coordonnees'){  ?>
        document.getElementById('blocs4').style.display = 'none';
        document.getElementById('div_pro_geolocalisation').style.display = 'block';
        document.getElementById('div_pro_multimedia').style.display = 'block';
        if(document.getElementById('bloc_4')){
            document.getElementById('bloc_4').style.display = 'none';
        }
        <?php } ?>

        <?php if (isset($user_groups->id) AND $user_groups->id == '5'){ ?>
        <?php if (isset($objGlissiere->isActive_presentation_4) AND $objGlissiere->isActive_presentation_4 == '1' AND isset($objGlissiere->nombre_blick_gli_presentation4) AND $objGlissiere->nombre_blick_gli_presentation4 == '1'){ ?>
        $("#gl4ppre").css('display', 'block');
        $("#glissiere4content").css('display', 'block');
        $("#champ1gli4").css('display', 'block');
        $("#champ2_gli4content").css('display', 'none');
        $("#champ1gli4").addClass('col-lg-12');
        $('#nombre_blick_gli_presentation4').val("1");
        <?php } ?>

        <?php if (isset($objGlissiere->isActive_presentation_4) AND $objGlissiere->isActive_presentation_4 == '1' AND isset($objGlissiere->nombre_blick_gli_presentation4) AND $objGlissiere->nombre_blick_gli_presentation4 == '2'){ ?>
        $("#gl4ppre").css('display', 'block');
        $("#glissiere4content").css('display', 'block');
        $("#champ2_gli4content").css('display', 'block');
        $("#champ1gli4").css('display', 'block');
        $("#champ1gli4").removeClass('col-lg-12');
        $("#champ1gli4").addClass('col-lg-6');
        $('#nombre_blick_gli_presentation4').val("2");
        <?php } ?>

        <?php if (isset($objGlissiere->isActive_presentation_3) AND $objGlissiere->isActive_presentation_3 == '1' AND isset($objGlissiere->nombre_blick_gli_presentation3) AND $objGlissiere->nombre_blick_gli_presentation3 == '1'){ ?>
        $("#glissiere3content").css('display', 'block');
        $("#champ1gli3").css('display', 'block');
        $("#gli3contpre").css('display', 'block');
        $("#champ2_gli3content").css('display', 'none');
        $("#champ1gli3").addClass("col-lg-12");
        <?php } ?>

        <?php if (isset($objGlissiere->isActive_presentation_3) AND $objGlissiere->isActive_presentation_3 == '1' AND isset($objGlissiere->nombre_blick_gli_presentation3) AND $objGlissiere->nombre_blick_gli_presentation3 == '2'){ ?>
        $("#glissiere3content").css('display', 'block');
        $("#champ1gli3").css('display', 'block');
        $("#gli3contpre").css('display', 'block');
        $("#champ2_gli3content").css('display', 'block');
        $("#champ1gli3").removeClass("col-lg-12");
        $("#champ1gli3").addClass("col-lg-6");
        <?php } ?>

        <?php if (isset($objGlissiere->isActive_presentation_2) AND $objGlissiere->isActive_presentation_2 == '1' AND isset($objGlissiere->nombre_blick_gli_presentation2) AND $objGlissiere->nombre_blick_gli_presentation2 == '1'){ ?>
        $("#glissiere2content").css('display', 'block');
        $("#champ2_gli2content").css('display', 'none');
        $("#champ1gli2").addClass('col-lg-12');
        $("#nombre_blick_gli_presentation2").val('1');

        <?php } ?>

        <?php if (isset($objGlissiere->isActive_presentation_2) AND $objGlissiere->isActive_presentation_2 == '1' AND isset($objGlissiere->nombre_blick_gli_presentation2) AND $objGlissiere->nombre_blick_gli_presentation2 == '2'){ ?>
        $("#glissiere2content").css('display', 'block');
        $("#champ2_gli2content").css('display', 'block');
        $("#champ1gli2").removeClass('col-lg-12');
        $("#champ1gli2").addClass('col-lg-6');
        $("#nombre_blick_gli_presentation2").val('2');
        <?php } ?>

        <?php if (isset($objGlissiere->isActive_presentation_1) AND $objGlissiere->isActive_presentation_1 == '1' AND isset($objGlissiere->nombre_blick_gli_presentation) AND $objGlissiere->nombre_blick_gli_presentation == '1'){ ?>
        document.getElementById('glissiere1content').style.display = "block";
        document.getElementById('champ1gli1').style.display = "block";
        document.getElementById('champ2_gli1content').style.display = "none";
        $('#champ1gli1').addClass("col-lg-12");
        $('#nombre_blick_gli_presentation').val('1');

        <?php } ?>

        <?php if (isset($objGlissiere->isActive_presentation_1) AND $objGlissiere->isActive_presentation_1 == '1' AND isset($objGlissiere->nombre_blick_gli_presentation) AND $objGlissiere->nombre_blick_gli_presentation == '2'){ ?>
        document.getElementById('glissiere1content').style.display = "block";
        document.getElementById('champ1gli1').style.display = "block";
        document.getElementById('champ2_gli1content').style.display = "block";
        $('#champ1gli1').removeClass("col-lg-12");
        $('#champ1gli1').addClass("col-lg-6");
        $('#nombre_blick_gli_presentation').val('2');
        <?php } ?>

        //script mamitiana glissiere 02 page 01
        <?php if (isset($objGlissiere->isActive_page1_2) AND $objGlissiere->isActive_page1_2 == '1' AND isset($objGlissiere->nombre_blick_gli_presentation) AND $objGlissiere->nombre_blick_gli_presentation == '1'){ ?>
        document.getElementById('glissiere1content').style.display = "block";
        document.getElementById('champ1gli1').style.display = "block";
        document.getElementById('champ2_gli1content').style.display = "none";
        $('#champ1gli1').addClass("col-lg-12");
        <?php } ?>
        <?php if (isset($objGlissiere->isActive_page1_2) AND $objGlissiere->isActive_presentation_1 == '1' AND isset($objGlissiere->nombre_blick_gli_presentation) AND $objGlissiere->nombre_blick_gli_presentation == '2'){ ?>
        document.getElementById('glissiere1content').style.display = "block";
        document.getElementById('champ1gli1').style.display = "block";
        document.getElementById('champ2_gli1content').style.display = "block";
        $('#champ1gli1').removeClass("col-lg-12");
        $('#champ1gli1').addClass("col-lg-6");
        <?php } ?>

        //script mamitiana glissiere 04 page 01
        <?php if (isset($objGlissiere->isActive_page1_4) AND $objGlissiere->isActive_page1_4 == '1' AND isset($objGlissiere->nombre_blick_gli_presentation) AND $objGlissiere->nombre_blick_gli_presentation == '1'){ ?>
        document.getElementById('glissiere1content').style.display = "block";
        document.getElementById('champ1gli1').style.display = "block";
        document.getElementById('champ2_gli1content').style.display = "none";
        $('#champ1gli1').addClass("col-lg-12");
        <?php } ?>
        <?php if (isset($objGlissiere->isActive_page1_4) AND $objGlissiere->isActive_page1_4 == '1' AND isset($objGlissiere->nombre_blick_gli_presentation) AND $objGlissiere->nombre_blick_gli_presentation == '2'){ ?>
        document.getElementById('glissiere1content').style.display = "block";
        document.getElementById('champ1gli1').style.display = "block";
        document.getElementById('champ2_gli1content').style.display = "block";
        $('#champ1gli1').removeClass("col-lg-12");
        $('#champ1gli1').addClass("col-lg-6");
        <?php } ?>

        //script mamitiana glissiere 03 page 01
        <?php if (isset($objGlissiere->isActive_page1_3) AND $objGlissiere->isActive_page1_3 == '1' AND isset($objGlissiere->nombre_blick_gli_presentation) AND $objGlissiere->nombre_blick_gli_presentation == '1'){ ?>
        document.getElementById('glissiere1content').style.display = "block";
        document.getElementById('champ1gli3p1').style.display = "block";
        document.getElementById('champ2_gli3content_p1').style.display = "none";
        $('#champ1gli1').addClass("col-lg-12");
        <?php } ?>
        <?php if (isset($objGlissiere->isActive_page1_3) AND $objGlissiere->isActive_page1_3 == '1' AND isset($objGlissiere->nombre_blick_gli_presentation) AND $objGlissiere->nombre_blick_gli_presentation == '2'){ ?>
        document.getElementById('glissiere1content').style.display = "block";
        document.getElementById('champ1gli3p1').style.display = "block";
        document.getElementById('champ2_gli3content_p1').style.display = "block";
        $('#champ1gli1').removeClass("col-lg-12");
        $('#champ1gli1').addClass("col-lg-6");
        <?php } ?>

        <?php } ?>
        <?php if ((isset($objGlissiere->nombre_blick_gli_p2) AND $objGlissiere->nombre_blick_gli_p2 == '1') AND (isset($objGlissiere->isActive_page2_1) AND $objGlissiere->isActive_page2_1 == '1'))  {?>


        document.getElementById('gli1p2co').style.display = "block";
        document.getElementById('gli1p2co').style.display = "block";
        document.getElementById('gl1_content').style.display = "block";
        document.getElementById('gl1_p2_content2').style.display = "none";
        $('#nbglissiere1p2').val("1");
        $('#gl1_content_p2').removeClass();
        $('#gl1_content_p2').addClass('col-lg-12');


        <?php } ?>
        <?php if ((isset($objGlissiere->nombre_blick_gli_p2) AND $objGlissiere->nombre_blick_gli_p2 == '2') AND (isset($objGlissiere->isActive_page2_1) AND $objGlissiere->isActive_page2_1 == '1')) {?>
        document.getElementById('gli1p2co').style.display = "block";
        document.getElementById('gli1p2co').style.display = "block";
        document.getElementById('gl1_content').style.display = "block";
        document.getElementById('gl1_p2_content2').style.display = "block";
        $('#nbglissiere1p2').val("2");
        $('#gl1_content_p2').removeClass();
        $('#gl1_content_p2').addClass('col-lg-6');


        <?php } ?>

        <?php if ((isset($objGlissiere->nombre_blick_gli2_p2) AND $objGlissiere->nombre_blick_gli2_p2 == '1') AND (isset($objGlissiere->isActive_page2_2) AND $objGlissiere->isActive_page2_2 == '1')) {?>

        //document.getElementById('g2p2con').style.display = "block";
        document.getElementById('glissiere2contentp2').style.display = "block";
        document.getElementById('champ1_gli2contentp2').style.display = "block";
        document.getElementById('champ2_gli2contentp2').style.display = "none";
        $('#nbglissiere2p2').val("1");
        $('#champ1_gli2contentp2').removeClass();
        $('#champ1_gli2contentp2').addClass('col-lg-12');

        <?php } ?>
        <?php if ((isset($objGlissiere->nombre_blick_gli2_p2) AND $objGlissiere->nombre_blick_gli2_p2 == '2') AND (isset($objGlissiere->isActive_page2_2) AND $objGlissiere->isActive_page2_2 == '1')) {?>

        //document.getElementById('g2p2con').style.display = "block";
        document.getElementById('glissiere2contentp2').style.display = "block";
        document.getElementById('champ2_gli2contentp2').style.display = "block";
        $('#nbglissiere2p2').val("2");
        $('#champ1_gli2contentp2').removeClass();
        $('#champ1_gli2contentp2').addClass('col-lg-6');

        <?php } ?>

        <?php if (isset($objGlissiere->isActive_page2_3) AND $objGlissiere->isActive_page2_3 == '1' AND isset($objGlissiere->nombre_blick_gli_page2_gli3) AND $objGlissiere->nombre_blick_gli_page2_gli3 == '1'){ ?>
        document.getElementById('glissiere3contentp2').style.display = "block";
        document.getElementById('champ2gli3p2').style.display = "block";
        document.getElementById('champ2_gli3content_p2').style.display = "none";
        $('#nbglissiere3p2').val("1");
        $('#champ2gli3p2').removeClass();
        $('#champ2gli3p2').addClass('col-lg-12');

        <?php } ?>
        <?php if (isset($objGlissiere->isActive_page2_3) AND $objGlissiere->isActive_page2_3 == '1' AND isset($objGlissiere->nombre_blick_gli_page2_gli3) AND $objGlissiere->nombre_blick_gli_page2_gli3 == '2'){ ?>
        document.getElementById('glissiere3contentp2').style.display = "block";
        document.getElementById('champ2gli3p2').style.display = "block";
        document.getElementById('champ2_gli3content_p2').style.display = "block";
        $('#nbglissiere3p2').val("2");
        $('#champ2gli3p2').removeClass();
        $('#champ2gli3p2').addClass('col-lg-6');

        <?php } ?>

        <?php if (isset($objGlissiere->isActive_page2_4) AND $objGlissiere->isActive_page2_4 == '1' AND isset($objGlissiere->nombre_blick_gli_page2_gli4) AND $objGlissiere->nombre_blick_gli_page2_gli4 == '1'){ ?>


        document.getElementById('glissiere4contentp2').style.display = "block";
        document.getElementById('champ2gli4p2').style.display = "block";
        document.getElementById('champ2_gli4contentp2').style.display = "none";
        $('#nbglissiere4p2').val("1");
        $('#champ2gli4p2').removeClass();
        $('#champ2gli4p2').addClass('col-lg-12');


        <?php } ?>
        <?php if (isset($objGlissiere->isActive_page2_4) AND $objGlissiere->isActive_page2_4 == '1' AND isset($objGlissiere->nombre_blick_gli_page2_gli4) AND $objGlissiere->nombre_blick_gli_page2_gli4 == '2'){ ?>


        document.getElementById('glissiere4contentp2').style.display = "block";
        document.getElementById('champ2gli4p2').style.display = "block";
        document.getElementById('champ2_gli4contentp2').style.display = "block";
        $('#nbglissiere4p2').val("2");
        $('#champ2gli4p2').removeClass();
        $('#champ2gli4p2').addClass('col-lg-6');


        <?php } ?>
        <?php if (isset($objGlissiere->isActive_page1_4) AND $objGlissiere->isActive_page1_4 == '1' AND isset($objGlissiere->nombre_blick_gli2_p1) AND $objGlissiere->nombre_blick_gli2_p1 == '1'){ ?>


        $("#glissiere4contentp1").css('display', 'block');
        $("#gl4_tittle_p1").css('display', 'block');
        $("#champ1gli4p1").css('display', 'block');
        $("#champ2_gli4contentp1").css('display', 'none');
        $("#champ1gli4p1").removeClass();
        $("#champ1gli4p1").addClass("col-lg-12");
        $("#nombre_gli4_p1").val("1");

        <?php } ?>
        <?php if (isset($objGlissiere->isActive_page1_4) AND $objGlissiere->isActive_page1_4 == '1' AND isset($objGlissiere->nombre_blick_gli2_p1) AND $objGlissiere->nombre_blick_gli2_p1 == '2'){ ?>

        $("#glissiere4contentp1").css('display', 'block');
        $("#gl4_tittle_p1").css('display', 'block');
        $("#champ1gli4p1").css('display', 'block');
        $("#champ2_gli4contentp1").css('display', 'block');
        $("#champ1gli4p1").removeClass();
        $("#champ1gli4p1").addClass("col-lg-6");
        $("#nombre_gli4_p1").val("2");

        <?php } ?>
        <?php if (isset($objGlissiere->isActive_page1_3) AND $objGlissiere->isActive_page1_3 == '1' AND isset($objGlissiere->nombre_blick_gli_page1_gli3) AND $objGlissiere->nombre_blick_gli_page1_gli3 == '1'){ ?>
        $("#glissiere3contentp1").css('display', 'block');
        $("#gl3_tittle_p1").css('display', 'block');
        $("#champ1gli3p1").css('display', 'block');
        $("#champ2_gli3content_p1").css('display', 'none');
        $("#champ1gli3p1").removeClass();
        $("#champ1gli3p1").addClass("col-lg-12");
        $("#nombre_gli3_p1").val("1");

        <?php } ?>
        <?php if (isset($objGlissiere->isActive_page1_3) AND $objGlissiere->isActive_page1_3 == '1' AND isset($objGlissiere->nombre_blick_gli_page1_gli3) AND $objGlissiere->nombre_blick_gli_page1_gli3 == '2'){ ?>


        $("#glissiere3contentp1").css('display', 'block');
        $("#gl3_tittle_p1").css('display', 'block');
        $("#champ1gli3p1").css('display', 'block');
        $("#champ2_gli3content_p1").css('display', 'block');
        $("#champ1gli3p1").removeClass();
        $("#champ1gli3p1").addClass("col-lg-6");
        $("#nombre_gli3_p1").val("2");

        <?php } ?>

        <?php if (isset($objGlissiere->isActive_page1_2) AND $objGlissiere->isActive_page1_2 == '1' AND isset($objGlissiere->nombre_blick_gli2_p1) AND $objGlissiere->nombre_blick_gli2_p1 == '1'){ ?>



        $("#glicomtpre").css('display', 'block');
        $("#gl2_p1_tittle").css('display', 'block');
        $("#glissiere2contentp1").css('display', 'block');
        $("#champ1_gli2contentp1").css('display', 'block');
        $("#champ2_gli2contentp1").css('display', 'none');
        $("#champ1_gli2contentp1").removeClass();
        $("#champ1_gli2contentp1").addClass("col-lg-12");
        $("#nombre_gli2_p1g1").val("1");

    <?php } ?>
    <?php if (isset($objGlissiere->isActive_page1_2) AND $objGlissiere->isActive_page1_2 == '1' AND isset($objGlissiere->nombre_blick_gli2_p1) AND $objGlissiere->nombre_blick_gli2_p1 == '2'){ ?>


    $("#glicomtpre").css('display', 'block');
    $("#gl2_p1_tittle").css('display', 'block');
    $("#champ1_gli2contentp1").css('display', 'block');
    $("#champ2_gli2contentp1").css('display', 'block');
    $("#champ1_gli2contentp1").removeClass();
    $("#champ1_gli2contentp1").addClass("col-lg-6");
    $("#nombre_gli2_p1g1").val("2");

    <?php } ?>

    <?php if (isset($objGlissiere->isActive_page1_1) AND $objGlissiere->isActive_page1_1 == '1' AND isset($objGlissiere->nombre_blick_gli_p1) AND $objGlissiere->nombre_blick_gli_p1 == '1'){ ?>


        $('#gl1_p1_content').removeClass("col-lg-6");
        $('#gl1_p1_content').addClass("col-lg-12");
        $('#gl1_p1_content2').css('display', 'none');
        $('#gli1p1js').val('1');

    <?php } ?>
    <?php if (isset($objGlissiere->isActive_page1_1) AND $objGlissiere->isActive_page1_1 == '1' AND isset($objGlissiere->nombre_blick_gli_p1) AND $objGlissiere->nombre_blick_gli_p1 == '2'){ ?>


        $('#gl1_p1_content2').css('display', 'block');
        $('#gl1_p1_content').removeClass('col-lg-12');
        $('#gl1_p1_content').addClass('col-lg-6');
        $('#gl1_p1_content2').addClass('col-lg-6');
        $('#gli1p1js').val('2');

    <?php } ?>




    });
</script>


<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#isActive_presentation_2").change(function () {
            if (document.getElementById('isActive_presentation_2').value == '0') {
                $("#dispnbgli2").css('display', 'none');
                $("#glissiere2content").css('display', 'none');
            } else {
                $("#dispnbgli2").css('display', 'block');
            }
        });

        $("#nbglissiere2").change(function () {
            if (document.getElementById('nbglissiere2').value === '1') {
                $("#glissiere2content").css('display', 'block');
                $("#champ2_gli2content").css('display', 'none');
            } else if (document.getElementById('nbglissiere2').value === '2') {
                $("#glissiere2content").css('display', 'block');
                $("#champ2_gli2content").css('display', 'block');
            }
            else if (document.getElementById('nbglissiere2').value === '0') {
                $("#glissiere2content").css('display', 'none');
                $("#champ2_gli2content").css('display', 'none');
            }

        });
        $("#1g2_1").click(function () {
            $("#glissiere2content").css('display', 'block');
            $("#champ1gli2").css('display', 'block');
            $("#champ2_gli2content").css('display', 'none');
            $("#champ1gli2").addClass('col-lg-12');
            $("#nombre_blick_gli_presentation2").val('1');
        });

        $("#2g2_1").click(function () {
            $("#glissiere2content").css('display', 'block');
            $("#champ2_gli2content").css('display', 'block');
            $("#champ1gli2").css('display', 'block');
            $("#champ1gli2").removeClass('col-lg-12');
            $("#champ1gli2").addClass('col-lg-6');
            $("#nombre_blick_gli_presentation2").val('2');
        });

        /* $("#1g2_1").click(function () {
             $("#glissiere2content").css('display', 'block');
             $("#champ2_gli2content").css('display', 'none');
         });

         $("#1g2_1").click(function () {
             $("#glissiere2content").css('display', 'block');
             $("#champ2_gli2content").css('display', 'none');
         });*/

        $("#link_btn_2_gli").change(function () {
            if (document.getElementById('link_btn_2_gli').value === '1') {
                $("#alllinkg2").css('display', 'block');

            } else if (document.getElementById('link_btn_2_gli').value === '0') {
                $("#alllinkg2").css('display', 'none');
            }

        });

        $("#link_btn_2_gl2").change(function () {
            if (document.getElementById('link_btn_2_gl2').value === '1') {
                $("#alllink2g2").css('display', 'block');

            } else if (document.getElementById('link_btn_2_gl2').value === '0') {
                $("#alllink2g2").css('display', 'none');
            }

        });

        $("#isActive_presentation_3").change(function () {
            if (document.getElementById('isActive_presentation_3').value === '1') {
                $("#dispnbgli3").css('display', 'block');
            } else if (document.getElementById('isActive_presentation_3').value === '0') {
                $("#dispnbgli3").css('display', 'none');
                $("#glissiere3content").css('display', 'none');
            }
        });

        $("#nbglissiere3").change(function () {

            if (document.getElementById('nbglissiere3').value == 1) {
                $("#glissiere3content").css('display', 'block');
                $("#champ1gli3").css('display', 'block');
                $("#gli3contpre").css('display', 'block');
                $("#champ2_gli3content").css('display', 'none');

            } else if (document.getElementById('nbglissiere3').value == 2) {
                $("#glissiere3content").css('display', 'block');
                $("#champ1gli3").css('display', 'block');
                $("#gli3contpre").css('display', 'block');
                $("#champ2_gli3content").css('display', 'block');
            }
            else if (document.getElementById('nbglissiere3').value == 0) {
                $("#glissiere3content").css('display', 'none');
                $("#champ1gli3").css('display', 'none');
                $("#gli3contpre").css('display', 'none');
                $("#champ2_gli3content").css('display', 'none');
            }

        });
        $("#1g3_1").click(function () {
            $("#glissiere3content").css('display', 'block');
            $("#champ1gli3").css('display', 'block');
            $("#gli3contpre").css('display', 'block');
            $("#champ2_gli3content").css('display', 'none');
            $("#champ1gli3").addClass("col-lg-12");
            $("#nombre_blick_gli_presentation3").val("1");
        });

        $("#2g3_1").click(function () {
            $("#glissiere3content").css('display', 'block');
            $("#champ1gli3").css('display', 'block');
            $("#gli3contpre").css('display', 'block');
            $("#champ2_gli3content").css('display', 'block');
            $("#champ1gli3").removeClass("col-lg-12");
            $("#champ1gli3").addClass("col-lg-6");
            $("#nombre_blick_gli_presentation3").val("2");
        });
        //script mamitiana glissiere 02 page 01

        $("#dispnbgli_1gl").click(function () {

            $("#glicomtpre").css('display', 'block');
            $("#gl2_p1_tittle").css('display', 'block');
            $("#nombre_gli2_p1g1").val("1");
            $("#glissiere2contentp1").css('display', 'block');
            $("#champ1_gli2contentp1").css('display', 'block');
            $("#champ2_gli2contentp1").css('display', 'none');
            $("#champ1_gli2contentp1").removeClass();
            $("#champ1_gli2contentp1").addClass("col-lg-12");


        });

        $("#dispnbgli_2gl").click(function () {
            $("#glissiere2contentp1").css('display', 'block');
            $("#glicomtpre").css('display', 'block');
            $("#gl2_p1_tittle").css('display', 'block');
            $("#champ1_gli2contentp1").css('display', 'block');
            $("#champ2_gli2contentp1").css('display', 'block');
            $("#champ1_gli2contentp1").removeClass();
            $("#champ1_gli2contentp1").addClass("col-lg-6");
            $("#nombre_gli2_p1g1").val("2");
        });

        //script mamitiana glissiere 04 page 01
        $("#dispnbglip4_1gl").click(function () {
            $("#glissiere4contentp1").css('display', 'block');
            $("#gl4_tittle_p1").css('display', 'block');
            $("#champ1gli4p1").css('display', 'block');
            $("#champ2_gli4contentp1").css('display', 'none');
            $("#champ1gli4p1").removeClass();
            $("#champ1gli4p1").addClass("col-lg-12");
            $("#nombre_gli4_p1").val("1");
        });
        $("#dispnbglip4_2gl").click(function () {
            $("#glissiere4contentp1").css('display', 'block');
            $("#gl4_tittle_p1").css('display', 'block');
            $("#champ1gli4p1").css('display', 'block');
            $("#champ2_gli4contentp1").css('display', 'block');
            $("#champ1gli4p1").removeClass();
            $("#champ1gli4p1").addClass("col-lg-6");
            $("#nombre_gli4_p1").val("2");
        });
        //script mamitiana glissiere 03 page 01
        $("#dispnbglip3_1gl").click(function () {
            $("#glissiere3contentp1").css('display', 'block');
            $("#gl3_tittle_p1").css('display', 'block');
            $("#champ1gli3p1").css('display', 'block');
            $("#champ2_gli3content_p1").css('display', 'none');
            $("#champ1gli3p1").removeClass();
            $("#champ1gli3p1").addClass("col-lg-12");
            $("#nombre_gli3_p1").val("1");
        });
        $("#dispnbglip3_2gl").click(function () {
            $("#glissiere3contentp1").css('display', 'block');
            $("#gl3_tittle_p1").css('display', 'block');
            $("#champ1gli3p1").css('display', 'block');
            $("#champ2_gli3content_p1").css('display', 'block');
            $("#champ1gli3p1").removeClass();
            $("#champ1gli3p1").addClass("col-lg-6");
            $("#nombre_gli3_p1").val("2");
        });
        /*$("#1bpg3_1").click(function () {

        });

        $("#1fdg3_1").click(function () {

        });*/


        $("#link_btn_3_gl").change(function () {
            if (document.getElementById('link_btn_3_gl').value === '0') {
                $("#alllinkg3").css('display', 'none');

            } else if (document.getElementById('link_btn_3_gl').value === '1') {
                $("#alllinkg3").css('display', 'block');
            }

        });

        $("#link_btn_3_gl2").change(function () {
            if (document.getElementById('link_btn_3_gl2').value === '1') {
                $("#alllink2g3").css('display', 'block');

            } else if (document.getElementById('link_btn_3_gl2').value === '0') {
                $("#alllink2g3").css('display', 'none');
            }

        });
        $("#isActive_presentation_4").change(function () {
            if (document.getElementById('isActive_presentation_4').value === '1') {
                $("#dispnbgli4").css('display', 'block');

            } else if (document.getElementById('isActive_presentation_4').value === '0') {
                $("#dispnbgli4").css('display', 'none');
                $("#glissiere4content").css('display', 'none');
            }

        });

        $("#nbglissiere4").change(function () {
            if (document.getElementById('nbglissiere4').value === '1') {
                $("#gl4ppre").css('display', 'block');
                $("#glissiere4content").css('display', 'block');
                $("#champ1gli4").css('display', 'block');
                $("#champ2_gli4content").css('display', 'none');

            } else if (document.getElementById('nbglissiere4').value === '2') {
                $("#gl4ppre").css('display', 'block');
                $("#glissiere4content").css('display', 'block');
                $("#champ2_gli4content").css('display', 'block');
                $("#champ1gli4").css('display', 'block');
            }
            else if (document.getElementById('nbglissiere4').value === '0') {
                $("#gl4ppre").css('display', 'none');
                $("#glissiere4content").css('display', 'none');
                $("#champ1gli4").css('display', 'none');
                $("#champ2_gli4content").css('display', 'none');
            }


        });
        $("#1g4_1").click(function () {
            $("#gl4ppre").css('display', 'block');
            $("#glissiere4content").css('display', 'block');
            $("#champ1gli4").css('display', 'block');
            $("#champ2_gli4content").css('display', 'none');
            $("#champ1gli4").addClass('col-lg-12');
            $('#nombre_blick_gli_presentation4').val("1");
        });

        $("#2g4_1").click(function () {

            $("#gl4ppre").css('display', 'block');
            $("#glissiere4content").css('display', 'block');
            $("#champ2_gli4content").css('display', 'block');
            $("#champ1gli4").css('display', 'block');
            $("#champ1gli4").removeClass('col-lg-12');
            $("#champ1gli4").addClass('col-lg-6');
            $('#nombre_blick_gli_presentation4').val("2");

        });
        $("#1bpg4_1").click(function () {

        });
        $("#1fdg4_1").click(function () {

        });

        $("#link_btn_4_gl").change(function () {
            if (document.getElementById('link_btn_4_gl').value === '0') {
                $("#alllinkg4").css('display', 'none');

            } else if (document.getElementById('link_btn_4_gl').value === '1') {
                $("#alllinkg4").css('display', 'block');
            }

        });

        $("#link_btn_4_gl2").change(function () {
            if (document.getElementById('link_btn_4_gl2').value === '1') {
                $("#alllink2g4").css('display', 'block');

            } else if (document.getElementById('link_btn_4_gl2').value === '0') {
                $("#alllink2g4").css('display', 'none');
            }

        });


        /*$("#isActive_page1_4").change(function () {
    if (document.getElementById('isActive_page1_4').value === '1') {
        $("#dispnbgli4p1").css('display', 'inline');

    }else if (document.getElementById('isActive_page1_4').value == '0'){
        $("#dispnbgli4p1").css('display', 'none');
        $("#glissiere4contentp1").css('display', 'none');
    }

    });*/
        //srcipt mamitiana glissiere 02 page 01
        $("#isActive_page1_2").change(function () {
            if (document.getElementById('isActive_page1_2').value === '1') {
                $("#dispnbgli").css('display', 'block');

            } else if (document.getElementById('isActive_page1_2').value === '0') {
                $("#dispnbgli").css('display', 'none');
                $("#glissiere2contentp1").css('display', 'none');
            }
        });

        //script mamitiana page01 glissiere 04
        $("#isActive_page1_4").change(function () {
            if (document.getElementById('isActive_page1_4').value === '1') {
                $("#dispnbglip4").css('display', 'block');

            } else if (document.getElementById('isActive_page1_4').value === '0') {
                $("#nombre_gli2_p1").css('display', 'none');
                $("#dispnbglip4").css('display', 'none');
                $("#glissiere4contentp1").css('display', 'none');
            }
        });
        //script mamitiana page01 glissiere 03
        $("#isActive_page1_3").change(function () {
            if (document.getElementById('isActive_page1_3').value === '1') {
                $("#dispnbglip3").css('display', 'block');
                $("#glissiere3contentp1").css('display', 'none');


            } else if (document.getElementById('isActive_page1_3').value === '0') {
                $("#nombre_gli3_p1").css('display', 'none');
                $("#dispnbglip3").css('display', 'none');
                $("#glissiere3contentp1").css('display', 'none');
            }

        });

        $("#nbglissiere2p1").change(function () {
            if (document.getElementById('nbglissiere2p1').value === '1') {
                $("#glissiere2contentp1").css('display', 'block');
                $("#champ1_gli2contentp1").css('display', 'block');
                $("#champ2_gli2contentp1").css('display', 'none');


            } else if (document.getElementById('nbglissiere2p1').value === '2') {
                $("#glissiere2contentp1").css('display', 'block');
                $("#champ1_gli2contentp1").css('display', 'block');
                $("#champ2_gli2contentp1").css('display', 'block');
            }
            else if (document.getElementById('nbglissiere2p1').value === '0') {
                $("#glissiere2contentp1").css('display', 'none');
                $("#champ1_gli2contentp1").css('display', 'none');
                $("#champ2_gli2contentp1").css('display', 'none');
            }

        });

        $("#link_btn_p1_gli21").change(function () {
            if (document.getElementById('link_btn_p1_gli21').value === '0') {
                $("#alllink2p1p1").css('display', 'none');

            } else if (document.getElementById('link_btn_p1_gli21').value === '1') {
                $("#alllink2p1p1").css('display', 'block');
            }

        });
        $("#link_btn_p1_gli2").change(function () {
            if (document.getElementById('link_btn_p1_gli2').value === '0') {
                $("#alllink_p1g2").css('display', 'none');

            } else if (document.getElementById('link_btn_p1_gli2').value === '1') {
                $("#alllink_p1g2").css('display', 'block');
            }

        });

        $("#link_btn2_p1_gli2").change(function () {
            if (document.getElementById('link_btn2_p1_gli2').value === '0') {
                $("#alllink2p1").css('display', 'none');

            } else if (document.getElementById('link_btn2_p1_gli2').value === '1') {
                $("#alllink2p1").css('display', 'block');
            }

        });


        $("#nbglissiere3p1").change(function () {
            if (document.getElementById('nbglissiere3p1').value === '1') {
                $("#glissiere3contentp1").css('display', 'block');
                $("#champ1gli3p1").css('display', 'block');
                $("#champ2_gli3content_p1").css('display', 'none');


            } else if (document.getElementById('nbglissiere3p1').value === '2') {
                $("#glissiere3contentp1").css('display', 'block');
                $("#champ1gli3p1").css('display', 'block');
                $("#champ2_gli3content_p1").css('display', 'block');
            }
            else if (document.getElementById('nbglissiere3p1').value === '0') {
                $("#glissiere3contentp1").css('display', 'none');
                $("#champ1gli3p1").css('display', 'none');
                $("#champ2_gli3content_p1").css('display', 'none');
            }

        });

        $("#link_btn_3_gl_p1").change(function () {
            if (document.getElementById('link_btn_3_gl_p1').value === '0') {
                $("#alllinkg3p1").css('display', 'none');

            } else if (document.getElementById('link_btn_3_gl_p1').value === '1') {
                $("#alllinkg3p1").css('display', 'block');
            }

        });

        $("#link_btn_3_gl2_p1").change(function () {
            if (document.getElementById('link_btn_3_gl2_p1').value === '0') {
                $("#alllink2g3p1").css('display', 'none');

            } else if (document.getElementById('link_btn_3_gl2_p1').value === '1') {
                $("#alllink2g3p1").css('display', 'block');
            }

        });


        $("#nbglissiere4p1").change(function () {
            if (document.getElementById('nbglissiere4p1').value === '1') {
                $("#glissiere4contentp1").css('display', 'block');
                $("#champ1gli4p1").css('display', 'block');
                $("#champ2_gli4contentp1").css('display', 'none');


            } else if (document.getElementById('nbglissiere4p1').value === '2') {
                $("#glissiere4contentp1").css('display', 'block');
                $("#champ1gli4p1").css('display', 'block');
                $("#champ2_gli4contentp1").css('display', 'block');
            }
            else if (document.getElementById('nbglissiere4p1').value === '0') {
                $("#glissiere4contentp1").css('display', 'none');
                $("#champ1gli4p1").css('display', 'none');
                $("#champ2_gli4contentp1").css('display', 'none');
            }

        });

        $("#link_btn_4_gl_p1").change(function () {
            if (document.getElementById('link_btn_4_gl_p1').value === '0') {
                $("#alllinkg4p1").css('display', 'none');

            } else if (document.getElementById('link_btn_4_gl_p1').value === '1') {
                $("#alllinkg4p1").css('display', 'block');
            }

        });

        $("#link_btn_4_gl2_p1").change(function () {
            if (document.getElementById('link_btn_4_gl2_p1').value === '0') {
                $("#alllink2g4p1").css('display', 'none');

            } else if (document.getElementById('link_btn_4_gl2_p1').value === '1') {
                $("#alllink2g4p1").css('display', 'block');
            }

        });


        $("#isActive_page2_2").change(function () {
            if (document.getElementById('isActive_page2_2').value === '1') {
                $("#dispnbgl2ip2").css('display', 'block');

            } else if (document.getElementById('isActive_page2_2').value === '0') {
                $("#dispnbgl2ip2").css('display', 'none');
                $("#glissiere2contentp2").css('display', 'none');
            }

        });


        $("#link_btn_p2_gli2c2").change(function () {
            if (document.getElementById('link_btn_p2_gli2c2').value === '0') {
                $("#alllink_p2g2").css('display', 'none');

            } else if (document.getElementById('link_btn_p2_gli2c2').value === '1') {
                $("#alllink_p2g2").css('display', 'block');
            }

        });

        $("#link_btn2_p2_gli2").change(function () {
            if (document.getElementById('link_btn2_p2_gli2').value === '0') {
                $("#alllink2p2g2").css('display', 'none');

            } else if (document.getElementById('link_btn2_p2_gli2').value === '1') {
                $("#alllink2p2g2").css('display', 'block');
            }

        });


        $("#isActive_page2_3").change(function () {
            if (document.getElementById('isActive_page2_3').value === '1') {
                $("#dispnbgli3p2").css('display', 'block');

            } else if (document.getElementById('isActive_page2_3').value === '0') {
                $("#dispnbgli3p2").css('display', 'none');
                $("#glissiere3contentp2").css('display', 'none');
            }

        });


        $("#nbglissiere3p2").change(function () {
            if (document.getElementById('nbglissiere3p2').value === '1') {
                $("#glissiere3contentp2").css('display', 'block');
                $("#champ2gli3p2").css('display', 'block');
                $("#champ2_gli3content_p2").css('display', 'none');


            } else if (document.getElementById('nbglissiere3p2').value === '2') {
                $("#glissiere3contentp2").css('display', 'block');
                $("#champ2gli3p2").css('display', 'block');
                $("#champ2_gli3content_p2").css('display', 'block');
            }
            else if (document.getElementById('nbglissiere3p2').value === '0') {
                $("#glissiere3contentp2").css('display', 'none');
                $("#champ2gli3p2").css('display', 'none');
                $("#champ2_gli3content_p2").css('display', 'none');
            }

        });

        $("#link_btn_3_gl_p2").change(function () {
            if (document.getElementById('link_btn_3_gl_p2').value === '0') {
                $("#alllinkg3p2").css('display', 'none');

            } else if (document.getElementById('link_btn_3_gl_p2').value === '1') {
                $("#alllinkg3p2").css('display', 'block');
            }

        });

        $("#link_btn_3_gl2_p2").change(function () {
            if (document.getElementById('link_btn_3_gl2_p2').value === '0') {
                $("#alllink2g3p2").css('display', 'none');

            } else if (document.getElementById('link_btn_3_gl2_p2').value === '1') {
                $("#alllink2g3p2").css('display', 'block');
            }

        });

        $("#isActive_page2_4").change(function () {
            if (document.getElementById('isActive_page2_4').value === '1') {
                $("#dispnbgli4p2").css('display', 'block');

            } else if (document.getElementById('isActive_page2_4').value === '0') {
                $("#dispnbgli4p2").css('display', 'none');
                $("#glissiere4contentp2").css('display', 'none');
            }

        });


        $("#nbglissiere4p2").change(function () {
            if (document.getElementById('nbglissiere4p2').value === '1') {
                $("#glissiere4contentp2").css('display', 'block');
                $("#champ2gli4p2").css('display', 'block');
                $("#champ2_gli4contentp2").css('display', 'none');


            } else if (document.getElementById('nbglissiere4p2').value === '2') {
                $("#glissiere4contentp2").css('display', 'block');
                $("#champ2gli4p2").css('display', 'block');
                $("#champ2_gli4contentp2").css('display', 'block');
            }
            else if (document.getElementById('nbglissiere4p2').value === '0') {
                $("#glissiere4contentp2").css('display', 'none');
                $("#champ2gli4p2").css('display', 'none');
                $("#champ2_gli4contentp2").css('display', 'none');
            }

        });

        $("#link_btn_4_gl_p2").change(function () {
            if (document.getElementById('link_btn_4_gl_p2').value === '0') {
                $("#alllinkg4p2").css('display', 'none');

            } else if (document.getElementById('link_btn_4_gl_p2').value === '1') {
                $("#alllinkg4p2").css('display', 'block');
            }

        });

        $("#link_btn_4_gl2_p2").change(function () {
            if (document.getElementById('link_btn_4_gl2_p2').value === '0') {
                $("#alllink2g4p2").css('display', 'none');

            } else if (document.getElementById('link_btn_4_gl2_p2').value === '1') {
                $("#alllink2g4p2").css('display', 'block');
            }

        });


    });
</script>

<script type="text/javascript">


    function listeSousRubrique() {

        var irubId = jQuery('#RubriqueSociete').val();
        jQuery.get(
            '<?php echo site_url("front/professionnels/testAjax"); ?>' + '/' + irubId,
            function (zReponse) {
                // alert (zReponse) ;
                jQuery('#trReponseRub').html("");
                jQuery('#trReponseRub').html(zReponse);


            });
    }

    function getCP() {
        var ivilleId = jQuery('#VilleSociete').val();
        jQuery.get(
            '<?php echo site_url("front/professionnels/getPostalCode"); ?>' + '/' + ivilleId,
            function (zReponse) {
                jQuery('#CodePostalSociete').val(zReponse);
            });
    }

    function getCP_localisation() {
        var ivilleId = jQuery('#IdVille_localisationSociete').val();
        jQuery.get(
            '<?php echo site_url("front/professionnels/getPostalCode_localisation"); ?>' + '/' + ivilleId,
            function (zReponse) {
                jQuery('#trReponseVille_localisation').html(zReponse);


            });
    }

    function getLatitudeLongitudeLocalisation() {
        var geocoder = new google.maps.Geocoder();
        var adresse_localisationSociete = $("#adresse_localisationSociete").val();
        if (adresse_localisationSociete != "") adresse_localisationSociete = adresse_localisationSociete + ', ';
        var Ville_localisationSociete = $("#IdVille_localisationSociete option:selected").text();
        var codepostal_localisationSociete = $("#codepostal_localisationSociete").val();
        geocoder.geocode({'address': adresse_localisationSociete + Ville_localisationSociete + ', ' + codepostal_localisationSociete + ', fr'}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                //alert("location : " + results[0].geometry.location.lat() + " " +results[0].geometry.location.lng());
                $("#latitudeSociete").val(results[0].geometry.location.lat());
                $("#longitudeSociete").val(results[0].geometry.location.lng());
            } else {
                //alert("Something got wrong " + status);
            }
        });
    }

    function getLatitudeLongitudeAdresse() {
        var geocoder = new google.maps.Geocoder();
        var adresse_localisationSociete = $("#Adresse1Societe").val();
        if (adresse_localisationSociete != "") adresse_localisationSociete = adresse_localisationSociete + ', ';
        var Ville_localisationSociete = $("#VilleSociete option:selected").text();
        var codepostal_localisationSociete = $("#CodePostalSociete").val();
        geocoder.geocode({'address': adresse_localisationSociete + Ville_localisationSociete + ', ' + codepostal_localisationSociete + ', fr'}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                //alert("location : " + results[0].geometry.location.lat() + " " +results[0].geometry.location.lng());
                $("#latitudeSociete").val(results[0].geometry.location.lat());
                $("#longitudeSociete").val(results[0].geometry.location.lng());
            } else {
                //alert("Something got wrong " + status);
            }
        });
    }

    function deleteFile(_IdCommercant, _FileName) {
        jQuery('.loading_' + _FileName).html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        jQuery.ajax({
            url: '<?php echo base_url();?>front/professionnels/delete_files/' + _IdCommercant + '/' + _FileName,
            dataType: 'html',
            type: 'POST',
            async: true,
            success: function (data) {
                jQuery('.loading_' + _FileName).html("");
                ////console.log(data);
                window.location.reload();
            },
            error: function (data) {
                window.location.reload();
                //console.log(data);
            }
        });

    }
    function deleteFileimagebloc(_IdCommercant, _FileName) {
        if (confirm("Voulez-vous supprimer l'image?")) {
            jQuery.ajax({
                url: '<?php echo base_url();?>front/professionnels/deleteimageblocinfo/' + _IdCommercant + '/' + _FileName,
                dataType: 'html',
                type: 'POST',
                async: true,
                success: function (data) {
                    jQuery('.loading_' + _FileName).html("");
                    ////console.log(data);
                    window.location.reload();
                },
                error: function (data) {
                    window.location.reload();
                    //console.log(data);
                }
            });

        }
    }

    jQuery(document).ready(function () {

        var _URL = window.URL || window.webkitURL;

        jQuery("#background_imageSociete").change(function (e) {
            var image, file;
            if ((file = this.files[0])) {
                image = new Image();
                image.onload = function () {
                    //alert("The image width is " +this.width + " and image height is " + this.height);
                    if (this.width > 2025) {
                        alert("La largeur de votre image dépasse le 2024px !");
                        $("#background_imageSociete_checker").val("1");
                    } else if (this.height > 1101) {
                        alert("La hauteur de votre image dépasse le 1100px !");
                        $("#background_imageSociete_checker").val("1");
                    } else {
                        $("#background_imageSociete_checker").val("0");
                    }
                };
                image.src = _URL.createObjectURL(file);
            }
        });

        jQuery("#bandeau_topSociete").change(function (e) {
            var image, file;
            if ((file = this.files[0])) {
                image = new Image();
                image.onload = function () {
                    //alert("The image width is " +this.width + " and image height is " + this.height);
                    if (this.width > 1025) {
                        alert("La largeur de votre image dépasse le 1024px !");
                        $("#bandeau_topSociete_checker").val("1");
                    } else if (this.height > 351) {
                        alert("La hauteur de votre image dépasse le 350px !");
                        $("#bandeau_topSociete_checker").val("1");
                    } else {
                        $("#bandeau_topSociete_checker").val("0");
                    }
                };
                image.src = _URL.createObjectURL(file);
            }
        });

        jQuery("#logoSociete").change(function (e) {
            var image, file;
            if ((file = this.files[0])) {
                image = new Image();
                image.onload = function () {
                    //alert("The image width is " +this.width + " and image height is " + this.height);
                    if (this.width > 271) {
                        alert("La largeur de votre logo dépasse le 270px !");
                        $("#Societelogo_checker").val("1");
                    } else {
                        $("#Societelogo_checker").val("0");
                    }
                };
                image.src = _URL.createObjectURL(file);
            }
        });

        jQuery('#PdfSociete').bind('change', function () {
            if (this.files[0].size > 3005000) {
                alert("La taille de votre fichier PDF dépasse le 3Mb !");
                $("#PdfSociete_checker").val("1");
            } else {
                $("#PdfSociete_checker").val("0");
            }
        });


        jQuery(".btnSinscrire").click(function () {

            jQuery('#div_error_fiche_pro').html('<div style="text-align: center;"><img src="<?php echo GetImagePath("front/");?>/loading.gif" /></div>');

            //alert('test form submit');
            var txtError = "";

            var background_imageSociete_checker = jQuery("#background_imageSociete_checker").val();
            if (background_imageSociete_checker == "1") {
                txtError += "- La taille de votre image page et arrère plan dépasse la largeur ou la hauteur indiquée.<br/>";
                $("#background_imageSociete_checker").css('border-color', 'red');
                $("#background_imageSociete_checker").css('color', 'red');
            } else {
                $("#background_imageSociete_checker").css('border-color', '#E3E1E2');
            }

            var bandeau_topSociete_checker = jQuery("#bandeau_topSociete_checker").val();
            if (bandeau_topSociete_checker == "1") {
                txtError += "- La taille de votre image bannière page de présentation dépasse la largeur ou la hauteur indiquée.<br/>";
                $("#bandeau_topSociete_checker").css('border-color', 'red');
                $("#bandeau_topSociete_checker").css('color', 'red');
            } else {
                $("#bandeau_topSociete_checker").css('border-color', '#E3E1E2');
            }

            var Societelogo_checker = jQuery("#Societelogo_checker").val();
            if (Societelogo_checker == "1") {
                txtError += "- La taille de votre Logo dépasse la largeur indiquée.<br/>";
                $("#Societelogo_checker").css('border-color', 'red');
                $("#Societelogo_checker").css('color', 'red');
            } else {
                $("#Societelogo_checker").css('border-color', '#E3E1E2');
            }

            var PdfSociete_checker = jQuery("#PdfSociete_checker").val();
            if (PdfSociete_checker == "1") {
                txtError += "- La taille de votre fichier PDF dépasse la taille indiquée.<br/>";
                $("#PdfSociete_checker").css('border-color', 'red');
                $("#PdfSociete_checker").css('color', 'red');
            } else {
                $("#PdfSociete_checker").css('border-color', '#E3E1E2');
            }

            var EmailSociete = jQuery("#EmailSociete").val();
            if (!isEmail(EmailSociete)) {
                txtError += "- L'adresse email de votre etablissement n'est pas valide.<br/>";
                $("#EmailSociete").css('border-color', 'red');
            } else {
                $("#EmailSociete").css('border-color', '#E3E1E2');
            }

            <?php if (isset($user_groups) && ($user_groups->id == '5' || $user_groups->id == '4')) { ?>
            var Email_decideurSociete = jQuery("#Email_decideurSociete").val();
            if (!isEmail(Email_decideurSociete)) {
                txtError += "- L'adresse email du decideur n'est pas valide.<br/>";
                $("#Email_decideurSociete").css('border-color', 'red');
            } else {
                $("#Email_decideurSociete").css('border-color', '#E3E1E2');
            }
            <?php } ?>
            /*
        var LoginSociete = jQuery("#LoginSociete").val();
        if(!isEmail(LoginSociete)) {
            txtError += "1";
            jQuery("#divErrorLoginSociete").html("Votre login doit &ecirc;tre un email valide.");
            jQuery("#divErrorLoginSociete").show();
        } else {
            jQuery("#divErrorLoginSociete").hide();
        }
        */

            <?php if ((isset($page_data) && $page_data == 'coordonnees') && (isset($objAbonnementCommercant) && $user_groups->id == '4')) { ?>
            /*ar HorairesSociete = CKEDITOR.instances['HorairesSociete'].getData();
            if(HorairesSociete=="") {
                txtError += "- Veuillez indiquer Heures et jours d'ouvertures<br/>";
                $("#HorairesSociete").css('border-color', 'red');
            } else {
                $("#HorairesSociete").css('border-color', '#E3E1E2');
            }*/
            <?php } else if ((isset($page_data) && $page_data == 'contenus') && (isset($objAbonnementCommercant) && $user_groups->id == '5')) { ?>
            /*var HorairesSociete = CKEDITOR.instances['HorairesSociete'].getData();
            if(HorairesSociete=="") {
                txtError += "- Veuillez indiquer Heures et jours d'ouvertures<br/>";
                $("#HorairesSociete").css('border-color', 'red');
            } else {
                $("#HorairesSociete").css('border-color', '#E3E1E2');
            }*/
            <?php } ?>


            <?php if (isset($page_data) && $page_data == 'contenus') { ?>
            /*var CaracteristiquesSociete = CKEDITOR.instances['CaracteristiquesSociete'].getData();
            if(CaracteristiquesSociete=="") {
                txtError += "- Veuillez indiquer la présentation de votre activité dans Page d'accueil<br/>";
                $("#cke_CaracteristiquesSociete").css('border-color', 'red');
            } else {
                $("#cke_CaracteristiquesSociete").css('border-color', '#E3E1E2');
            }*/

            var Photo1Societe = $("#Photo1Societe").val();
            if (Photo1Societe == "") {
                txtError += "- Veuillez indiquer une photo dans Page d'acceuil<br/>";
                $("#Photo1Societe").css('color', 'red');
            } else {
                $("#Photo1Societe").css('color', '#E3E1E2');
            }

            <?php if (isset($user_groups) && $user_groups->id == '5') { ?>
            /*var logoSociete = $("#logoSociete").val();
            if(logoSociete=="") {
                txtError += "- Veuillez indiquer Votre Logo<br/>";
                $("#logoSociete").css('color', 'red');
            } else {
                $("#logoSociete").css('color', '#E3E1E2');
            }*/
            <?php } ?>

            <?php } ?>


            var qrcode_text_submit = $("#qrcode_text").val();
            var qrcode_text_submit_verify = "";
            jQuery.post(
                '<?php echo site_url("front/professionnels/get_qrcode_text"); ?>',
                {IdCommercant: "<?php echo $objCommercant->IdCommercant; ?>"},
                function (zReponse2) {
                    qrcode_text_submit_verify = zReponse2;

                    if (qrcode_text_submit != qrcode_text_submit_verify) {
                        txtError += "- Veuillez Re-créer le Qrcode avant de valider les modifications !<br/>";
                        $("#qrcode_text").css('color', 'red');
                    } else {
                        $("#qrcode_text").css('color', '#000000');
                    }

                    var subdomain_url_check = $("#subdomain_url_check").val();

                    if (subdomain_url_check == "0") {
                        var subdomain_url_submit = $("#subdomain_url").val();
                        jQuery.post(
                            '<?php echo site_url("front/professionnels/verify_privicarte_subdomain_code"); ?>',
                            {
                                subdomain_url: subdomain_url_submit,
                                IdCommercant: "<?php echo $objCommercant->IdCommercant; ?>"
                            },
                            function (zReponse3) {
                                if (zReponse3 == "1") {
                                    txtError += "- Votre sous-domaine existe déjà !<br/>";
                                    $("#subdomain_url").css('color', 'red');
                                } else {
                                    $("#subdomain_url").css('color', '#000000');
                                }

                                if (txtError == "") {
                                    jQuery("#div_error_fiche_pro").hide();
                                    jQuery("#frmInscriptionProfessionnel").submit();
                                } else {
                                    jQuery("#div_error_fiche_pro").html(txtError);
                                }

                            });
                    } else if (subdomain_url_check == "1") {
                        var subdomain_url_submit = $("#subdomain_url_vsv").val();
                        jQuery.post(
                            '<?php echo site_url("front/professionnels/verify_privicarte_subdomain_code_vsv"); ?>',
                            {
                                subdomain_url: subdomain_url_submit,
                                IdCommercant: "<?php echo $objCommercant->IdCommercant; ?>"
                            },
                            function (zReponse3) {
                                if (zReponse3 == "1") {
                                    txtError += "- Votre sous-domaine existe déjà !<br/>";
                                    $("#subdomain_url_vsv").css('color', 'red');
                                } else {
                                    $("#subdomain_url_vsv").css('color', '#000000');
                                }

                                if (txtError == "") {
                                    jQuery("#div_error_fiche_pro").hide();
                                    jQuery("#frmInscriptionProfessionnel").submit();
                                } else {
                                    jQuery("#div_error_fiche_pro").html(txtError);
                                }

                            });
                    }


                });


        });


        //$('#bandeau_colorSociete').colorPicker();


        $("#adresse_localisation_diffuseur_checkbox").click(function () {
            if ($('#adresse_localisation_diffuseur_checkbox').attr('checked')) {
                $("#adresse_localisation_diffuseur").val("1");
                var adress_organisateur1 = $("#Adresse1Societe").val();
                var adress_organisateur2 = $("#Adresse2Societe").val();
                var IdVille_organisateur = $("#VilleSociete").val();
                var codepostal_organisateur = $("#CodePostalSociete").val();
                $("#adresse_localisationSociete").val(adress_organisateur1 + " " + adress_organisateur2);
                $("#IdVille_localisationSociete").val(IdVille_organisateur);
                $("#codepostal_localisationSociete").val(codepostal_organisateur);
            } else {
                $("#adresse_localisation_diffuseur").val("0");
                $("#adresse_localisationSociete").val("");
                $("#IdVille_localisationSociete").val("0");
                $("#codepostal_localisationSociete").val("");
            }
        });


        <?php if ($objCommercant->qrcode_text == "" || $objCommercant->qrcode_text == NULL) { ?>

        jQuery('#span_generate_qrcode').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        $("input.btnSinscrire").attr("disabled", true);
        var qrcode_text = "http://www.sortez.org/<?php echo $objCommercant->nom_url; ?>";
        jQuery('#qrcode_text').val(qrcode_text);
        jQuery.post(
            '<?php echo site_url("front/professionnels/generate_qrcode"); ?>',
            {qrcode_text: qrcode_text},
            function (zReponse) {
                //alert(zReponse);
                jQuery('#div_qrcode_img').html(zReponse);
                //jQuery('#qrcode_text').val(qrcode_text);
                jQuery.post(
                    '<?php echo site_url("front/professionnels/get_qrcode_img"); ?>',
                    {IdCommercant: "<?php echo $objCommercant->IdCommercant; ?>"},
                    function (zReponse2) {
                        //alert(zReponse2);
                        jQuery('#span_generate_qrcode').html("");
                        jQuery('#qrcode_img').val(zReponse2);
                        $("input.btnSinscrire").attr("disabled", false);
                    });
            });

        <?php } ?>



        //removing divErrorDisplay affter 5 seconds
        /*setTimeout(function () {
            jQuery('#divErrorDisplay').hide();
        }, 4000);*/


        $("#nom_url_updated_pro_check").click(function () {
            if ($('#nom_url_updated_pro_check').attr('checked')) {
                $("#subdomain_url_check").val("0");
            }
        });
        $("#nom_url_updated_pro_check_vsv").click(function () {
            if ($('#nom_url_updated_pro_check_vsv').attr('checked')) {
                $("#subdomain_url_check").val("1");
            }
        });

        $("#nom_url_send_activation_btn").click(function () {
            jQuery('#span_nom_url_send_activation').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            jQuery.post(
                '<?php echo site_url("front/professionnels/send_nom_url_activation"); ?>',
                function (zReponse) {
                    jQuery('#span_nom_url_send_activation').html("Demande envoyée !");
                    jQuery("#nom_url_send_activation_btn").attr("disabled", "disabled");
                });
        });

        $("#subdomain_url").keyup(function () {
            $("#subdomain_url_vsv").val($(this).val());
        });
        $("#subdomain_url_vsv").keyup(function () {
            $("#subdomain_url").val($(this).val());
        });


        $("#isActive_page1_1").change(function () {

            if (document.getElementById('isActive_page1_1').value == 0) {
                document.getElementById('nbglissiere1p1').style.display = "none";
                document.getElementById('glissiere1contentp1').style.display = "none";
            } else if (document.getElementById('isActive_page1_1').value == 1) {
                document.getElementById('nbglissiere1p1').style.display = "block";
                document.getElementById('glissiere1contentp1').style.display = "none";
            }

        });
        $("#gli1_1_p1").click(function () {
            document.getElementById('glissiere1contentp1').style.display = "block";
            document.getElementById('gl1_p1_content').style.display = "block";
            document.getElementById('gl1_p1_content').style.display = "none";
            $('#gli1p1js').val('1');
            $('#gl1_p1_content').addClass('col-lg-12');
            $('#gl1_p1_content').removeClass('col-lg-6');
            $('#gl1_p1_content2').removeClass('col-lg-6');
        });

        $("#gli1_2_p1").click(function () {
            document.getElementById('glissiere1contentp1').style.display = "block";
            document.getElementById('gl1_p1_content2').style.display = "block";
            document.getElementById('gl1_p1_content').style.display = "block";
            $('#gli1p1js').val('2');
            $('#gl1_p1_content').addClass('col-lg-6');
            $('#gl1_p1_content2').addClass('col-lg-6');
            $('#gl1_p1_content').removeClass('col-lg-12');
        });

        $("#nbglissiere1p1").change(function () {
            if (document.getElementById('nbglissiere1p1').value == 1) {
                document.getElementById('btn_gli1_p1').style.display = "block";
                document.getElementById('gl1_p1_img').style.display = "block";
                document.getElementById('gl1_p1_tittle').style.display = "block";
                document.getElementById('glissiere1contentp1').style.display = "block";
                document.getElementById('gl1_p1_tittle2').style.display = "none";
                document.getElementById('gl1_p1_content2').style.display = "none";
                document.getElementById('gl1_p1_img2').style.display = "none";
                document.getElementById('btn_p1_gli2').style.display = "none";
                document.getElementById('alllink2p1').style.display = "none";


            } else if (document.getElementById('nbglissiere1p1').value == 2) {
                document.getElementById('btn_gli1_p1').style.display = "block";
                document.getElementById('gl1_p1_img').style.display = "block";
                document.getElementById('gl1_p1_tittle').style.display = "block";
                document.getElementById('glissiere1contentp1').style.display = "block";
                document.getElementById('gl1_p1_tittle2').style.display = "block";
                document.getElementById('gl1_p1_content2').style.display = "block";
                document.getElementById('gl1_p1_img2').style.display = "block";
                document.getElementById('btn_p1_gli2').style.display = "block";
                document.getElementById('alllink2p1').style.display = "block";
            }
            else if (document.getElementById('nbglissiere1p1').value == 0) {
                document.getElementById('btn_gli1_p1').style.display = "none";
                document.getElementById('gl1_p1_img').style.display = "none";
                document.getElementById('gl1_p1_tittle').style.display = "none";
                document.getElementById('glissiere1contentp1').style.display = "none";
                document.getElementById('gl2_tittle').style.display = "none";
                document.getElementById('gl2_content').style.display = "none";
            }
        });


        $("#isActive_page2_1").change(function () {

            if (document.getElementById('isActive_page2_1').value == 0) {
                document.getElementById('nbglissiere1p2').style.display = "none";
                document.getElementById('dispnbglip2').style.display = "none";
                document.getElementById('glissiere1contentp2').style.display = "none";
            } else if (document.getElementById('isActive_page2_1').value == 1) {
                document.getElementById('nbglissiere1p2').style.display = "block";
                document.getElementById('dispnbglip2').style.display = "block";
            }

        });


        $("#nbglissiere1p2").change(function () {
            if (document.getElementById('nbglissiere1p2').value == 1) {
                document.getElementById('btn_gli1_p2').style.display = "block";
                document.getElementById('gl1_p2_img').style.display = "block";
                document.getElementById('gli1p2co').style.display = 'block';
                document.getElementById('gl1_p2_tittle').style.display = "block";
                document.getElementById('glissiere1contentp2').style.display = "block";
                document.getElementById('gl1_p2_content2').style.display = "none";
                document.getElementById('gl1_p2_img2').style.display = "none";
                document.getElementById('btn_p2_gli2').style.display = "none";
                document.getElementById('alllink2p2').style.display = "none";


            } else if (document.getElementById('nbglissiere1p2').value == 2) {
                document.getElementById('btn_gli1_p2').style.display = "block";
                document.getElementById('gli1p2co').style.display = 'block';
                document.getElementById('gl1_p2_img').style.display = "block";
                document.getElementById('gl1_p2_tittle').style.display = "block";
                document.getElementById('glissiere1contentp2').style.display = "block";
                document.getElementById('gl1_p2_content2').style.display = "block";
                document.getElementById('gl1_p2_img2').style.display = "block";
                document.getElementById('btn_p2_gli2').style.display = "block";
                document.getElementById('alllink2p2').style.display = "block";
            }
            else if (document.getElementById('nbglissiere1p2').value == 0) {
                document.getElementById('btn_gli1_p2').style.display = "none";
                document.getElementById('gli1p2co').style.display = 'none';
                document.getElementById('gl1_p2_img').style.display = "none";
                document.getElementById('gl1_p2_tittle').style.display = "none";
                document.getElementById('glissiere1contentp2').style.display = "none";

            }
        });


        $("#link_btn_p1_gli1").change(function () {

            if (document.getElementById('link_btn_p1_gli1').value == 1) {
                document.getElementById('alllink_p1').style.display = "block";


            }
            else if (document.getElementById('link_btn_p1_gli1').value == 0) {
                document.getElementById('alllink_p1').style.display = "none";

            }

        });
        $("#link_btn_p2_gli1").change(function () {

            if (document.getElementById('link_btn_p2_gli1').value == 1) {
                document.getElementById('alllink_p2').style.display = "block";


            }
            else if (document.getElementById('link_btn_p2_gli1').value == 0) {
                document.getElementById('alllink_p2').style.display = "none";

            }

        });


        $("#link_btn_p2_gli2").change(function () {

            if (document.getElementById('link_btn_p2_gli2').value == 1) {
                document.getElementById('alllink2p2').style.display = "block";

            }
            else if (document.getElementById('link_btn_p2_gli2').value == 0) {
                document.getElementById('alllink2p2').style.display = "none";
            }
        });


        $("#isActive_presentation_1").change(function () {

            if (document.getElementById('isActive_presentation_1').value == 0) {
                document.getElementById('dispnbglipres').style.display = "none";
                document.getElementById('glissiere1content').style.display = "none";
            } else if (document.getElementById('isActive_presentation_1').value == 1) {
                document.getElementById('dispnbglipres').style.display = "block";

            }

        });


        /*$("#nbglissiere1").change(function () {

            if (document.getElementById('nbglissiere1').value == 1) {
                document.getElementById('glissiere1content').style.display = "block";
                document.getElementById('champ1gli1').style.display = "block";
                document.getElementById('champ2_gli1content').style.display = "none";

            } else if (document.getElementById('nbglissiere1').value == 2) {
                document.getElementById('glissiere1content').style.display = "block";
                document.getElementById('champ1gli1').style.display = "block";
                document.getElementById('champ2_gli1content').style.display = "block";
            }
            else if (document.getElementById('nbglissiere1').value == 0) {
                document.getElementById('glissiere1content').style.display = "none";

            }

        });*/

        $("#1g1_1").click(function () {
            document.getElementById('glissiere1content').style.display = "block";
            document.getElementById('champ1gli1').style.display = "block";
            document.getElementById('champ2_gli1content').style.display = "none";
            $('#champ1gli1').addClass("col-lg-12");
            $('#nombre_blick_gli_presentation').val('1');
            document.getElementById('gli1_bonplan_presentation').style.display = "none";
        });

        $("#2g1_1").click(function () {
            document.getElementById('glissiere1content').style.display = "block";
            document.getElementById('champ1gli1').style.display = "block";
            document.getElementById('champ2_gli1content').style.display = "block";
            $('#champ1gli1').removeClass("col-lg-12");
            $('#champ1gli1').addClass("col-lg-6");
            $('#nombre_blick_gli_presentation').val('2');
            document.getElementById('gli1_bonplan_presentation').style.display = "none";
        });


        /*$("#1fdg1_1").click(function () {
            document.getElementById('glissiere1content').style.display = "block";
            document.getElementById('champ1gli1').style.display = "block";
            document.getElementById('champ2_gli1content').style.display = "none";
        });*/

        $("#link_btn_gli1").change(function () {

            if (document.getElementById('link_btn_gli1').value == 1) {
                document.getElementById('alllink').style.display = "block";


            }
            else if (document.getElementById('link_btn_gli1').value == 0) {
                document.getElementById('alllink').style.display = "none";

            }

        });


        $("#link_btn_gli2").change(function () {

            if (document.getElementById('link_btn_gli2').value == 1) {
                document.getElementById('alllink2').style.display = "block";


            }
            else if (document.getElementById('link_btn_gli2').value == 0) {
                document.getElementById('alllink2').style.display = "none";

            }

        });



        <?php if (isset($objGlissiere->bonplan_id) AND $objGlissiere->bonplan_id != null AND $objGlissiere->bonplan_id != ''){?>

        bonplan = document.getElementById('bonplan_select').value;
        document.getElementById('bonplan_div').style.display = "block";
        $.get("<?php echo base_url();?>/front/professionnels/get_bonplan_selected2/" + bonplan, function (data) {
            //alert( "Data Loaded: " + data );
            ////console.log(data);
            $("#bonplan_content_posted").html(data);
        });

        <?php } ?>








        $("#bonplan_select").change(function () {
            $("#bonplan_content_posted").html('');
            if (document.getElementById('bonplan_select').value != 0) {
                bonplan = document.getElementById('bonplan_select').value;
                document.getElementById('bonplan_div').style.display = "block";
                $.get("http://localhost/sortez7/front/professionnels/get_bonplan_selected/" + bonplan, function (data) {
                    //alert( "Data Loaded: " + data );
                    $("#bonplan_content_posted").html(data);
                });
            } else {

                document.getElementById('bonplan_content_posted').style.display = "none";
                document.getElementById('bonplan_div').style.display = "none";
            }
        });


        $("#fidelity_select").change(function () {
            $("#fidelity_content_posted").html('');
            if (document.getElementById('fidelity_select').value != 0) {
                fidelity = document.getElementById('fidelity_select').value;
                document.getElementById('fidelity_div').style.display = "block";
                $.get("http://localhost/sortez7/front/professionnels/get_fidelity_selected/" + fidelity, function (data) {
                    //alert( "Data Loaded: " + data );
                    $("#fidelity_content_posted").html(data);
                });
            } else {

                document.getElementById('fidelity_content_posted').style.display = "none";
                document.getElementById('fidelity_div').style.display = "none";
            }
        });


    });

    /*
$(document).off('blur', '#postcode').on('blur', '#postcode', function(e){link_btn_gli1
if ($('#postcode').val() != "") {
    setTimeout(function() {
        $('#postcode').parent().removeClass("form-error");
        $('#postcode').parent().addClass("form-ok");
        //alert('ok');
    }
    ,500);
}
});
*/

    function CP_getDepartement() {
        //alert(jQuery('#CodePostalSociete').val());
        jQuery('#departementCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        var CodePostalSociete = jQuery('#CodePostalSociete').val();
        jQuery.post(
            '<?php echo site_url("front/professionnels/departementcp"); ?>',
            {CodePostalSociete: CodePostalSociete},
            function (zReponse) {
                jQuery('#departementCP_container').html(zReponse);
            });
    }

    function CP_getVille() {
        //alert(jQuery('#CodePostalSociete').val());
        jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        var CodePostalSociete = jQuery('#CodePostalSociete').val();
        jQuery.post(
            '<?php echo site_url("front/professionnels/villecp"); ?>',
            {CodePostalSociete: CodePostalSociete},
            function (zReponse) {
                jQuery('#villeCP_container').html(zReponse);
            });
    }

    function CP_getVille_localisation() {
        //alert(jQuery('#CodePostalSociete').val());
        jQuery('#villeCP_container_localisation').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        var CodePostalSociete = jQuery('#codepostal_localisationSociete').val();
        jQuery.post(
            '<?php echo site_url("front/professionnels/villecp_localisation"); ?>',
            {CodePostalSociete: CodePostalSociete},
            function (zReponse) {
                jQuery('#villeCP_container_localisation').html(zReponse);
            });
    }

    function CP_getVille_D_CP() {
        //alert(jQuery('#CodePostalSociete').val());
        jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        var CodePostalSociete = jQuery('#CodePostalSociete').val();
        var departement_id = jQuery('#departement_id').val();
        jQuery.post(
            '<?php echo site_url("front/professionnels/villecp"); ?>',
            {CodePostalSociete: CodePostalSociete, departement_id: departement_id},
            function (zReponse) {
                jQuery('#villeCP_container').html(zReponse);
            });
    }

    function open_map_localisation() {
        <?php
        if (is_object($objCommercant) && isset($objCommercant->IdVille_localisation)) {
            if (isset($this->mdlville->getVilleById($objCommercant->IdVille_localisation)->Nom))
                $ville_map_locali = $this->mdlville->getVilleById($objCommercant->IdVille_localisation)->Nom;
        } else $ville_map_locali = '';
        if (!isset($ville_map_locali)) $ville_map_locali = '';
        ?>
        window.open("http://maps.google.fr/maps?f=q&source=s_q&hl=fr&geocode=&q=<?php echo $objCommercant->adresse_localisation . ", " . $objCommercant->codepostal_localisation . " " . $ville_map_locali;?>&aq=0&ie=UTF8&hq=&hnear=<?php echo $objCommercant->adresse_localisation . ", " . $objCommercant->codepostal_localisation . " " . $ville_map_locali;?>&t=m&vpsrc=0", "MsgWindow", "width=800, height=800");
    }


    function verify_privicarte_subdomain() {
        jQuery('#span_verify_subdomain').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        var subdomain_url = jQuery('#subdomain_url').val();
        jQuery.post(
            '<?php echo site_url("front/professionnels/verify_privicarte_subdomain"); ?>',
            {subdomain_url: subdomain_url, IdCommercant: "<?php echo $objCommercant->IdCommercant; ?>"},
            function (zReponse) {
                jQuery('#span_verify_subdomain').html(zReponse);
            });
    }

    function verify_privicarte_subdomain_vsv() {
        jQuery('#span_verify_subdomain_vsv').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        var subdomain_url = jQuery('#subdomain_url_vsv').val();
        jQuery.post(
            '<?php echo site_url("front/professionnels/verify_privicarte_subdomain"); ?>',
            {subdomain_url: subdomain_url, IdCommercant: "<?php echo $objCommercant->IdCommercant; ?>"},
            function (zReponse) {
                jQuery('#span_verify_subdomain_vsv').html(zReponse);
            });
    }


    function generate_qrcode() {
        jQuery('#span_generate_qrcode').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        $("input.btnSinscrire").attr("disabled", true);
        var qrcode_text = jQuery('#qrcode_text').val();
        jQuery.post(
            '<?php echo site_url("front/professionnels/generate_qrcode"); ?>',
            {qrcode_text: qrcode_text},
            function (zReponse) {
                jQuery('#div_qrcode_img').html(zReponse);
                jQuery('#qrcode_text').val(qrcode_text);
                jQuery.post(
                    '<?php echo site_url("front/professionnels/get_qrcode_img"); ?>',
                    {IdCommercant: "<?php echo $objCommercant->IdCommercant; ?>"},
                    function (zReponse2) {
                        jQuery('#span_generate_qrcode').html("");
                        jQuery('#qrcode_img').val(zReponse2);
                        $("input.btnSinscrire").attr("disabled", false);
                    });
            });
    }


    function show_btn_pro_contenus_home() {
        jQuery('#current_page').val('home');
        <?php if (isset($objGlissiere->is_activ_bloc3) AND $objGlissiere->is_activ_bloc3 == '1'){ ?>
        <?php if (isset($objGlissiere->type_bloc3) AND $objGlissiere->type_bloc3 == '1' OR $objGlissiere->type_bloc4 == '2'){ ?>
        jQuery('#bloc_3').css("display", "block");
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc3) AND $objGlissiere->type_bloc3 == '2'){ ?>
        jQuery('#bloc2_3').css("display", "block");
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc3) AND $objGlissiere->type_bloc3 == '1bp'){ ?>
        jQuery('#bonplan_config_3').css('display', 'block');
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc3) AND $objGlissiere->type_bloc3 == '1fd'){ ?>
        jQuery('#fidelity_3_config').css("display", "block");
        <?php } ?>
        <?php } ?>


        <?php if (isset($objGlissiere->is_activ_bloc4) AND $objGlissiere->is_activ_bloc4 == '1'){ ?>
        <?php if (isset($objGlissiere->type_bloc4) AND $objGlissiere->type_bloc4 == '1' OR $objGlissiere->type_bloc4 == '2'){ ?>
        jQuery('#bloc_4').css("display", "block");
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc4) AND $objGlissiere->type_bloc4 == '2'){ ?>
        jQuery('#bloc2_4').css("display", "block");
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc4) AND $objGlissiere->type_bloc4 == '1bp'){ ?>
        jQuery('#bonplan_config_4').css('display', 'block');
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc4) AND $objGlissiere->type_bloc4 == '1fd'){ ?>
        jQuery('#fidelity_4_config').css("display", "block");
        <?php } ?>
        <?php } ?>
        <?php if (isset($objGlissiere->is_activ_bloc) AND $objGlissiere->is_activ_bloc == '1'){ ?>
        <?php if (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc == '1' OR $objGlissiere->type_bloc == '2'){ ?>
        jQuery('#bloc1').css("display", "block");
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc == '2'){ ?>
        jQuery('#bloc2').css("display", "block");
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc == '1bp'){ ?>
        jQuery('#bonplan_config').css('display', 'block');
        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc == '1fd'){ ?>
        jQuery('#fidelity_config').css("display", "block");
        <?php } ?>
        <?php } ?>

        jQuery('#blocss').css("display", "block");

        jQuery('#blocs4').css("display", "block");
        jQuery('#blocs2').css("display", "block");
        jQuery('#blocs3').css("display", "block");
        jQuery('#div_pro_titre_infos').css("display", "none");
        jQuery('#div_pro_titre_home').css("display", "block");
        jQuery('#div_pro_titre_page1').css("display", "none");
        jQuery('#div_pro_titre_page2').css("display", "none");
        jQuery('#blocss_page1').css("display", "none");
        jQuery('#blocs2_page1').css("display", "none");
        jQuery('#blocs3_page1').css("display", "none");
        jQuery('#blocs4_page1').css("display", "none");
        jQuery('#blocss_page2').css("display", "none");
        jQuery('#blocs2_page2').css("display", "none");
        jQuery('#blocs3_page2').css("display", "none");
        jQuery('#blocs4_page2').css("display", "none");

        jQuery('#div_pro_logo').css("display", "none");
        jQuery('#div_pro_fondecran').css("display", "none");
        jQuery('#div_pro_horaires').css("display", "none");
        jQuery('#div_pro_pagepresentation').css("display", "block");
        jQuery('#div_pro_image_presentation').css("display", "block");
        jQuery('#div_pro_entete_banniere').css("display", "none");
        jQuery('#ico_logo_linkiconleft').css("display", "none");
        jQuery('#div_pro_geolocalisation').css("display", "none");
        jQuery('#div_pro_page1').css("display", "none");
        jQuery('#div_pro_page2').css("display", "none");
        jQuery('#div_pro_multimedia').css("display", "none");
        jQuery('#div_pro_glissiere1_home').css("display", "block");
        jQuery('#div_pro_glissiere2_home').css("display", "block");
        jQuery('#div_pro_glissiere3_home').css("display", "block");
        jQuery('#div_pro_glissiere4_home').css("display", "block");
        jQuery('#div_pro_glissiere1_page1').css("display", "none");
        jQuery('#div_pro_glissiere2_page1').css("display", "none");
        jQuery('#div_pro_glissiere3_page1').css("display", "none");
        jQuery('#div_pro_glissiere4_page1').css("display", "none");
        jQuery('#div_pro_glissiere1_page2').css("display", "none");
        jQuery('#div_pro_glissiere2_page2').css("display", "none");
        jQuery('#div_pro_glissiere3_page2').css("display", "none");
        jQuery('#div_pro_glissiere4_page2').css("display", "none");

    }

    function show_btn_pro_contenus_page1() {

        jQuery('#current_page').val('page1');
        jQuery('#bloc_4').css("display", "none");
        jQuery('#bloc2_4').css("display", "none");
        jQuery('#bonplan_config_4').css("display", "none");
        jQuery('#fidelity_4_config').css("display", "none");
        jQuery('#blocss_page1').css("display", "none");
        jQuery('#blocs2_page1').css("display", "none");
        jQuery('#blocs3_page1').css("display", "none");
        jQuery('#blocs4_page1').css("display", "none");
        jQuery('#blocss_page2').css("display", "none");
        jQuery('#blocs2_page2').css("display", "none");
        jQuery('#blocs3_page2').css("display", "none");
        jQuery('#blocs4_page2').css("display", "none");

        jQuery('#bonplan_config_3').css('display', 'none');
        jQuery('#bloc_3').css("display", "none");
        jQuery('#bloc2_3').css("display", "none");
        jQuery('#fidelity_3_config').css("display", "none");

        jQuery('#bloc_2').css('display', 'none');
        jQuery('#bloc2_2').css("display", "none");
        //jQuery('#bonplan_config_2').css("display", "none");
        //jQuery('#fidelity_2_config').css("display", "none");

        jQuery('#bloc1').css('display', 'none');
        jQuery('#bloc2').css("display", "none");
        jQuery('#bonplan_config').css("display", "none");
        jQuery('#fidelity_config').css("display", "none");


        jQuery('#blocss').css("display", "none");


        jQuery('#blocs4').css("display", "none");
        jQuery('#blocs2').css("display", "none");
        jQuery('#blocs3').css("display", "none");
        jQuery('#div_pro_titre_infos').css("display", "none");
        jQuery('#div_pro_titre_home').css("display", "none");
        jQuery('#div_pro_titre_page1').css("display", "block");
        jQuery('#div_pro_titre_page2').css("display", "none");

        jQuery('#div_pro_logo').css("display", "none");
        jQuery('#div_pro_fondecran').css("display", "none");
        jQuery('#div_pro_horaires').css("display", "none");
        jQuery('#div_pro_pagepresentation').css("display", "none");
        jQuery('#div_pro_image_presentation').css("display", "none");
        jQuery('#div_pro_entete_banniere').css("display", "none");
        jQuery('#ico_logo_linkiconleft').css("display", "none");
        jQuery('#div_pro_geolocalisation').css("display", "none");
        jQuery('#div_pro_page1').css("display", "block");
        jQuery('#div_pro_page2').css("display", "none");
        jQuery('#div_pro_multimedia').css("display", "none");
        jQuery('#div_pro_glissiere1_home').css("display", "none");
        jQuery('#div_pro_glissiere2_home').css("display", "none");
        jQuery('#div_pro_glissiere3_home').css("display", "none");
        jQuery('#div_pro_glissiere4_home').css("display", "none");

        jQuery('#blocss_page1').css("display", "block");
        jQuery('#blocs2_page1').css("display", "block");
        jQuery('#blocs3_page1').css("display", "block");
        jQuery('#blocs4_page1').css("display", "block");
        jQuery('#blocss_page1').css("display", "block");
        jQuery('#blocs2_page1').css("display", "block");
        jQuery('#blocs3_page1').css("display", "block");
        jQuery('#blocs4_page1').css("display", "block");
        jQuery('#div_pro_glissiere1_page1').css("display", "block");
        jQuery('#div_pro_glissiere2_page1').css("display", "block");
        jQuery('#div_pro_glissiere3_page1').css("display", "block");
        jQuery('#div_pro_glissiere4_page1').css("display", "block");
        jQuery('#div_pro_glissiere1_page2').css("display", "none");
        jQuery('#div_pro_glissiere2_page2').css("display", "none");
        jQuery('#div_pro_glissiere3_page2').css("display", "none");
        jQuery('#div_pro_glissiere4_page2').css("display", "none");

    }


    function show_btn_pro_contenus_page2() {
        jQuery('#current_page').val('page2');
        jQuery('#bloc_3').css("display", "none");
        jQuery('#bloc2_3').css("display", "none");
        jQuery('#fidelity_config_page2').css("display", "none");

        jQuery('#blocss_page2').css("display", "block");
        jQuery('#blocss_page2').css("display", "block");
        jQuery('#bloc_2').css('display', 'none');
        jQuery('#bloc2_2').css("display", "none");
        //jQuery('#bonplan_config_2').css("display", "none");
        //jQuery('#fidelity_2_config').css("display", "none");
        jQuery('#blocss_page1').css("display", "none");
        jQuery('#blocs2_page1').css("display", "none");
        jQuery('#blocs3_page1').css("display", "none");
        jQuery('#blocs4_page1').css("display", "none");
        jQuery('#blocss_page2').css("display", "none");
        jQuery('#blocs2_page2').css("display", "none");
        jQuery('#blocs3_page2').css("display", "none");
        jQuery('#blocs4_page2').css("display", "none");

        jQuery('#bloc1').css('display', 'none');
        jQuery('#bloc2').css("display", "none");
        jQuery('#bonplan_config').css("display", "none");
        jQuery('#fidelity_config').css("display", "none");

        jQuery('#bloc_4').css("display", "none");
        jQuery('#bloc2_4').css("display", "none");
        jQuery('#bonplan_config_4').css("display", "none");
        jQuery('#fidelity_4_config').css("display", "none");
        jQuery('#bonplan_config_3').css('display', 'none');
        jQuery('#blocss').css("display", "none");
        jQuery('#blocs4').css("display", "none");
        jQuery('#blocs2').css("display", "none");
        jQuery('#blocs3').css("display", "none");
        jQuery('#div_pro_titre_infos').css("display", "none");
        jQuery('#div_pro_titre_home').css("display", "none");
        jQuery('#div_pro_titre_page1').css("display", "none");
        jQuery('#div_pro_titre_page2').css("display", "block");

        jQuery('#div_pro_logo').css("display", "none");
        jQuery('#div_pro_fondecran').css("display", "none");
        jQuery('#div_pro_horaires').css("display", "none");
        jQuery('#div_pro_pagepresentation').css("display", "none");
        jQuery('#div_pro_image_presentation').css("display", "none");
        jQuery('#div_pro_entete_banniere').css("display", "none");
        jQuery('#ico_logo_linkiconleft').css("display", "none");
        jQuery('#div_pro_geolocalisation').css("display", "none");
        jQuery('#div_pro_page1').css("display", "none");
        jQuery('#div_pro_page2').css("display", "block");
        jQuery('#div_pro_multimedia').css("display", "none");
        jQuery('#blocss_page2').css("display", "block");
        jQuery('#blocs2_page2').css("display", "block");
        jQuery('#blocs3_page2').css("display", "block");
        jQuery('#blocs4_page2').css("display", "block");
        jQuery('#div_pro_glissiere1_home').css("display", "none");
        jQuery('#div_pro_glissiere2_home').css("display", "none");
        jQuery('#div_pro_glissiere3_home').css("display", "none");
        jQuery('#div_pro_glissiere4_home').css("display", "none");
        jQuery('#div_pro_glissiere1_page1').css("display", "none");
        jQuery('#div_pro_glissiere2_page1').css("display", "none");
        jQuery('#div_pro_glissiere3_page1').css("display", "none");
        jQuery('#div_pro_glissiere4_page1').css("display", "none");
        jQuery('#div_pro_glissiere1_page2').css("display", "block");
        jQuery('#div_pro_glissiere2_page2').css("display", "block");
        jQuery('#div_pro_glissiere3_page2').css("display", "block");
        jQuery('#div_pro_glissiere4_page2').css("display", "block");
        /*
        jQuery('#blocss_page1').css("display", "block");
        jQuery('#blocs2_page1').css("display", "block");
        jQuery('#blocs3_page1').css("display", "block");
        jQuery('#blocs4_page1').css("display", "block");
        */

    }


    function show_btn_pro_contenus_design() {

        jQuery('#current_page').val('design');
        <?php if (isset($user_groups->id) AND $user_groups->id == '5'){ ?>
        jQuery('#bloc_3').css("display", "none");
        jQuery('#bloc2_3').css("display", "none");
        jQuery('#fidelity_3_config').css("display", "none");
        jQuery('#blocss_page2').css("display", "none");


        jQuery('#bloc_2').css('display', 'none');
        jQuery('#bloc2_2').css("display", "none");
        //jQuery('#bonplan_config_2').css("display", "none");
        jQuery('#fidelity_2_config').css("display", "none");
        jQuery('#blocss_page1').css("display", "none");
        jQuery('#blocs2_page1').css("display", "none");
        jQuery('#blocs3_page1').css("display", "none");
        jQuery('#blocs4_page1').css("display", "none");
        jQuery('#blocss_page2').css("display", "none");
        jQuery('#blocs2_page2').css("display", "none");
        jQuery('#blocs3_page2').css("display", "none");
        jQuery('#blocs4_page2').css("display", "none");

        jQuery('#bloc1').css('display', 'none');
        jQuery('#bloc2').css("display", "none");
        jQuery('#bonplan_config').css("display", "none");
        jQuery('#fidelity_config').css("display", "none");
        jQuery('#bloc_4').css("display", "none");
        jQuery('#bloc2_4').css("display", "none");
        jQuery('#bonplan_config_4').css("display", "none");
        jQuery('#fidelity_4_config').css("display", "none");
        jQuery('#bonplan_config_3').css('display', 'none');

        jQuery('#blocss').css("display", "none");
        jQuery('#blocs3').css("display", "none");
        jQuery('#blocs4').css("display", "none");
        jQuery('#blocs2').css("display", "none");
        <?php } ?>
        jQuery('#div_pro_titre_infos').css("display", "block");
        jQuery('#div_pro_titre_home').css("display", "none");
        jQuery('#div_pro_titre_page1').css("display", "none");
        jQuery('#div_pro_titre_page2').css("display", "none");

        jQuery('#div_pro_logo').css("display", "block");
        jQuery('#div_pro_fondecran').css("display", "block");
        jQuery('#div_pro_horaires').css("display", "block");
        jQuery('#div_pro_pagepresentation').css("display", "none");
        <?php if (($user_groups->id) AND $user_groups->id == '5'){?>
        jQuery('#div_pro_image_presentation').css("display", "none");
        <?php } ?>
        <?php if (($user_groups->id) AND $user_groups->id == '4'){?>
        jQuery('#div_pro_image_presentation').css("display", "block");
        jQuery('#div_pro_pagepresentation').css("display", "block");
        <?php } ?>
        jQuery('#div_pro_entete_banniere').css("display", "block");
        jQuery('#ico_logo_linkiconleft').css("display", "block");
        jQuery('#div_pro_geolocalisation').css("display", "none");
        jQuery('#div_pro_page1').css("display", "none");
        jQuery('#div_pro_page2').css("display", "none");
        jQuery('#div_pro_multimedia').css("display", "none");
        jQuery('#div_pro_glissiere1_home').css("display", "none");
        jQuery('#div_pro_glissiere2_home').css("display", "none");
        jQuery('#div_pro_glissiere3_home').css("display", "none");
        jQuery('#div_pro_glissiere4_home').css("display", "none");
        jQuery('#div_pro_glissiere1_page1').css("display", "none");
        jQuery('#div_pro_glissiere2_page1').css("display", "none");
        jQuery('#div_pro_glissiere3_page1').css("display", "none");
        jQuery('#div_pro_glissiere4_page1').css("display", "none");
        jQuery('#div_pro_glissiere1_page2').css("display", "none");
        jQuery('#div_pro_glissiere2_page2').css("display", "none");
        jQuery('#div_pro_glissiere3_page2').css("display", "none");
        jQuery('#div_pro_glissiere4_page2').css("display", "none");

    }

    $("#blocss").ready(function () {

        /*if (document.getElementById('nbglissiere1p2').value == 1) {
            document.getElementById('btn_gli1_p2').style.display = "block";
            document.getElementById('gl1_p2_img').style.display = "block";
            document.getElementById('gl1_p2_tittle').style.display = "block";
            document.getElementById('glissiere1contentp2').style.display = "block";
            document.getElementById('gl1_p2_content2').style.display = "none";
            document.getElementById('gl1_p2_img2').style.display = "none";
            document.getElementById('btn_p2_gli2').style.display = "none";
            document.getElementById('alllink2p2').style.display = "none";

        } else if (document.getElementById('nbglissiere1p2').value == 2) {
            document.getElementById('btn_gli1_p2').style.display = "block";
            document.getElementById('gl1_p2_img').style.display = "block";
            document.getElementById('gl1_p2_tittle').style.display = "block";
            document.getElementById('glissiere1contentp2').style.display = "block";
            document.getElementById('gl1_p2_content2').style.display = "block";
            document.getElementById('gl1_p2_img2').style.display = "block";
            document.getElementById('btn_p2_gli2').style.display = "block";
            document.getElementById('alllink2p2').style.display = "block";
        }
        else if (document.getElementById('nbglissiere1p2').value == 0) {
            document.getElementById('btn_gli1_p2').style.display = "none";
            document.getElementById('gl1_p2_img').style.display = "none";
            document.getElementById('gl1_p2_tittle').style.display = "none";
            document.getElementById('glissiere1contentp2').style.display = "none";

        }*/


    
        $("#1g1_2").click(function () {
            document.getElementById('gli1p2co').style.display = "block";
            document.getElementById('glissiere1contentp2').style.display = "block";
            document.getElementById('gl1_content_p2').style.display = "block";
            document.getElementById('gl1_p2_content2').style.display = "none";
            $('#nbglissiere1p2').val("1");
            $('#gl1_content_p2').removeClass();
            $('#gl1_content_p2').addClass('col-lg-12');

        });

        $("#2g1_2").click(function () {
            document.getElementById('gli1p2co').style.display = "block";
            document.getElementById('glissiere1contentp2').style.display = "block";
            document.getElementById('gl1_content_p2').style.display = "block";
            document.getElementById('gl1_p2_content2').style.display = "block";
            $('#nbglissiere1p2').val("2");
            $('#gl1_content_p2').removeClass();
            $('#gl1_content_p2').addClass('col-lg-6');
        });

        $("#1g12_2").click(function () {
            //document.getElementById('g2p2con').style.display = "block";
            document.getElementById('glissiere2contentp2').style.display = "block";
            document.getElementById('champ2_gli2contentp2').style.display = "none";
            $('#nbglissiere2p2').val("1");
            $('#champ1_gli2contentp2').removeClass();
            $('#champ1_gli2contentp2').addClass('col-lg-12');

        });

        $("#2g12_2").click(function () {
            //document.getElementById('g2p2con').style.display = "block";
            document.getElementById('glissiere2contentp2').style.display = "block";
            document.getElementById('champ2_gli2contentp2').style.display = "block";
            $('#nbglissiere2p2').val("2");
            $('#champ1_gli2contentp2').removeClass();
            $('#champ1_gli2contentp2').addClass('col-lg-6');

        });

        $("#1g13_2").click(function () {
            document.getElementById('glissiere3contentp2').style.display = "block";
            document.getElementById('champ2gli3p2').style.display = "block";
            document.getElementById('champ2_gli3content_p2').style.display = "none";
            $('#nbglissiere3p2').val("1");
            $('#champ2gli3p2').removeClass();
            $('#champ2gli3p2').addClass('col-lg-12');

        });
        $("#2g13_2").click(function () {
            document.getElementById('glissiere3contentp2').style.display = "block";
            document.getElementById('champ2gli3p2').style.display = "block";
            document.getElementById('champ2_gli3content_p2').style.display = "block";
            $('#nbglissiere3p2').val("2");
            $('#champ2gli3p2').removeClass();
            $('#champ2gli3p2').addClass('col-lg-6');

        });

        $("#1g14_2").click(function () {
            document.getElementById('glissiere4contentp2').style.display = "block";
            document.getElementById('champ2gli4p2').style.display = "block";
            document.getElementById('champ2_gli4contentp2').style.display = "none";
            $('#nbglissiere4p2').val("1");
            $('#champ2gli4p2').removeClass();
            $('#champ2gli4p2').addClass('col-lg-12');

        });
        $("#2g14_2").click(function () {
            document.getElementById('glissiere4contentp2').style.display = "block";
            document.getElementById('champ2gli4p2').style.display = "block";
            document.getElementById('champ2_gli4contentp2').style.display = "block";
            $('#nbglissiere4p2').val("2");
            $('#champ2gli4p2').removeClass();
            $('#champ2gli4p2').addClass('col-lg-6');

        });

    });
    $("#blocss_page1").ready(function () {

        /*if (document.getElementById('nbglissiere1p2').value == 1) {
            document.getElementById('btn_gli1_p2').style.display = "block";
            document.getElementById('gl1_p2_img').style.display = "block";
            document.getElementById('gl1_p2_tittle').style.display = "block";
            document.getElementById('glissiere1contentp2').style.display = "block";
            document.getElementById('gl1_p2_content2').style.display = "none";
            document.getElementById('gl1_p2_img2').style.display = "none";
            document.getElementById('btn_p2_gli2').style.display = "none";
            document.getElementById('alllink2p2').style.display = "none";

        } else if (document.getElementById('nbglissiere1p2').value == 2) {
            document.getElementById('btn_gli1_p2').style.display = "block";
            document.getElementById('gl1_p2_img').style.display = "block";
            document.getElementById('gl1_p2_tittle').style.display = "block";
            document.getElementById('glissiere1contentp2').style.display = "block";
            document.getElementById('gl1_p2_content2').style.display = "block";
            document.getElementById('gl1_p2_img2').style.display = "block";
            document.getElementById('btn_p2_gli2').style.display = "block";
            document.getElementById('alllink2p2').style.display = "block";
        }
        else if (document.getElementById('nbglissiere1p2').value == 0) {
            document.getElementById('btn_gli1_p2').style.display = "none";
            document.getElementById('gl1_p2_img').style.display = "none";
            document.getElementById('gl1_p2_tittle').style.display = "none";
            document.getElementById('glissiere1contentp2').style.display = "none";

        }*/

        $("#1g1_2").click(function () {
            document.getElementById('gli1p2co').style.display = "block";
            document.getElementById('glissiere1contentp2').style.display = "block";
            document.getElementById('gl1_content_p2').style.display = "block";
            document.getElementById('gl1_p2_content2').style.display = "none";
            $('#nbglissiere1p2').val("1");
            $('#gl1_content_p2').removeClass();
            $('#gl1_content_p2').addClass('col-lg-12');

        });

        $("#2g1_2").click(function () {
            document.getElementById('gli1p2co').style.display = "block";
            document.getElementById('glissiere1contentp2').style.display = "block";
            document.getElementById('gl1_content_p2').style.display = "block";
            document.getElementById('gl1_p2_content2').style.display = "block";
            $('#nbglissiere1p2').val("2");
            $('#gl1_content_p2').removeClass();
            $('#gl1_content_p2').addClass('col-lg-6');
        });

        $("#1g12_2").click(function () {
            //document.getElementById('g2p2con').style.display = "block";
            document.getElementById('glissiere2contentp2').style.display = "block";
            document.getElementById('champ2_gli2contentp2').style.display = "none";
            $('#nbglissiere2p2').val("1");
            $('#champ1_gli2contentp2').removeClass();
            $('#champ1_gli2contentp2').addClass('col-lg-12');

        });

        $("#2g12_2").click(function () {
            //document.getElementById('g2p2con').style.display = "block";
            document.getElementById('glissiere2contentp2').style.display = "block";
            document.getElementById('champ2_gli2contentp2').style.display = "block";
            $('#nbglissiere2p2').val("2");
            $('#champ1_gli2contentp2').removeClass();
            $('#champ1_gli2contentp2').addClass('col-lg-6');

        });

        $("#1g13_2").click(function () {
            document.getElementById('glissiere3contentp2').style.display = "block";
            document.getElementById('champ2gli3p2').style.display = "block";
            document.getElementById('champ2_gli3content_p2').style.display = "none";
            $('#nbglissiere3p2').val("1");
            $('#champ2gli3p2').removeClass();
            $('#champ2gli3p2').addClass('col-lg-12');

        });
        $("#2g13_2").click(function () {
            document.getElementById('glissiere3contentp2').style.display = "block";
            document.getElementById('champ2gli3p2').style.display = "block";
            document.getElementById('champ2_gli3content_p2').style.display = "block";
            $('#nbglissiere3p2').val("2");
            $('#champ2gli3p2').removeClass();
            $('#champ2gli3p2').addClass('col-lg-6');

        });

        $("#1g14_2").click(function () {
            document.getElementById('glissiere4contentp2').style.display = "block";
            document.getElementById('champ2gli4p2').style.display = "block";
            document.getElementById('champ2_gli4contentp2').style.display = "none";
            $('#nbglissiere4p2').val("1");
            $('#champ2gli4p2').removeClass();
            $('#champ2gli4p2').addClass('col-lg-12');

        });
        $("#2g14_2").click(function () {
            document.getElementById('glissiere4contentp2').style.display = "block";
            document.getElementById('champ2gli4p2').style.display = "block";
            document.getElementById('champ2_gli4contentp2').style.display = "block";
            $('#nbglissiere4p2').val("2");
            $('#champ2gli4p2').removeClass();
            $('#champ2gli4p2').addClass('col-lg-6');

        });

    });

    $("#isActive_page1_1").ready(function () {

        if (document.getElementById('isActive_page1_1').value == 0) {
            document.getElementById('nbglissiere1p1').style.display = "none";
            document.getElementById('nbglissiere1p1').style.display = "none";
            document.getElementById('glissiere1contentp1').style.display = "none";


        } else if (document.getElementById('isActive_page1_1').value == 1) {
            document.getElementById('nbglissiere1p1').style.display = "block";
            document.getElementById('nbglissiere1p1').style.display = "block";
        }

    });

    $("#isActive_page2_1").ready(function () {

        if (document.getElementById('isActive_page2_1').value == 0) {
            document.getElementById('nbglissiere1p2').style.display = "none";
            document.getElementById('dispnbglip2').style.display = "none";
            document.getElementById('glissiere1contentp2').style.display = "none";


        } else if (document.getElementById('isActive_page2_1').value == 1) {
            document.getElementById('nbglissiere1p2').style.display = "block";
            document.getElementById('dispnbglip2').style.display = "block";
        }

    });


</script>

<script type="text/javascript">
    function sub_link_custom1() {
        var value_link = document.getElementById('htt').value + document.getElementById('link_htt').value;
        $("#link_htt").val(value_link);

    }

    function sub_link_custom2() {
        var value_link = document.getElementById('htt2').value + document.getElementById('link_htt2').value;
        $("#link_htt2").val(value_link);

    }
</script>

