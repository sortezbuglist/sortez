<?php $data["zTitle"] = 'Fid&eacute;lit&eacute;s' ?>


<?php $this->load->view("privicarte/includes/main_header", $data); ?>
<?php $this->load->view("privicarte/includes/ready_fidelity", $data); ?>
<?php $this->load->view("privicarte/includes/main_navbar", $data); ?>
<?php $this->load->view("privicarte/includes/main_logo_dep_com", $data); ?>
<?php $this->load->view("privicarte/includes/main_menu", $data);?>
<?php $this->load->view("privicarte/includes/main_slide", $data); ?>
<?php //$this->load->view("privicarte/includes/main_map", $data); ?>	
<?php $this->load->view("privicarte/includes/main_menu_contents", $data); ?>


<div class="container top_bottom_15" style="background-color: #fff; padding-bottom:40px;">

    <div class="col-sm-3 bonplan_left_filter">
        &nbsp;
		<?php 
		if (isset($pagecategory) && $pagecategory == 'annonce')
		 $this->load->view("privicarte/includes/searchform_content_annonce", $data); 
		else if (isset($pagecategory) && $pagecategory == 'bonplan')
		 $this->load->view("privicarte/includes/searchform_content_bonplan", $data); 
		 else if (isset($pagecategory) && $pagecategory == 'fidelity')
		 $this->load->view("privicarte/includes/searchform_content_fidelity", $data); 
		else $this->load->view("privicarte/includes/searchform_content_partenaire", $data);
		?>
    </div>
    
    <div class="col-sm-9 paddingleft0 bonplan_rigth_content paddingright0">
        <div class="col-xs-12" style="padding:0 0 25px 0;">
		<?php $this->load->view("privicarte/fidelity_list_filter", $data); ?>
        </div>
        <div class="col-xs-12 padding0">
        	<?php $this->load->view("privicarte/fidelity_css_list", $data); ?>
			<?php $this->load->view("privicarte/fidelity_remise_list", $data); ?>
			<?php $this->load->view("privicarte/fidelity_tampon_list", $data); ?>
            <?php $this->load->view("privicarte/fidelity_capital_list", $data); ?>
        </div>
    </div>

</div>



<?php $this->load->view("privicarte/includes/main_footer_link", $data); ?>
<?php $this->load->view("privicarte/includes/main_footer_copyright", $data); ?>
<?php $this->load->view("privicarte/includes/main_footer", $data); ?>