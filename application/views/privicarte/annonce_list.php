<style type="text/css">
    .annonce_list_img img {
        height: 256px !important;
    }

    .annonce_nv_list_livraison_content {
        padding: 0px !important;
    }

    .annonce_list_img {
        float: left;
        position: relative;
        background-color: #FFFFFF;
    }

    .annonce_item_line {
        display: table;
        float: left;
        position: relative;
        margin-bottom: 15px;
        width: 100%;
        background-color: #E1E1E1;
    }

    .annonce_list_details {
        background-color: #FFFFFF;
        float: left;
        position: relative;
    }
</style>

<?php if (!isset($data)) $data['data_init'] = true; ?>

<?php if (isset($_GET['content_only_list']) && $_GET['content_only_list'] == '1') {
} else { ?>

    <div class="col-lg-12" style="padding:0 0 15px 0;">
        <?php $this->load->view("privicarte/annonce_list_filter", $data); ?>
    </div>

<!--<div class="col-lg-12 paddingleft0 paddingright0" style="padding-bottom:15px;">
  <a href="javascript:void(0);"><img src="<?php //echo GetImagePath("privicarte/"); ?>/mode_carte.jpg" width="100%"/></a>
</div>-->

<div id="id_mainbody_main" class="col-lg-12 padding0">

    <?php } ?>

    <?php $ij = 0; ?>
    <div class="col-xs-12 padding0">
        <?php foreach ($toListeAnnonce as $oListeAnnonce){ ?>
        <?php
        if ($oListeAnnonce->etat == 1) $zEtat = "Neuf";
        else if ($oListeAnnonce->etat == 0) $zEtat = "Revente";
        else if ($oListeAnnonce->etat == 2) $zEtat = "Service";
        else $zEtat = "";
        ?>

        <?php
        $thisss =& get_instance();
        $thisss->load->model("ion_auth_used_by_club");
        $ionauth_id = $thisss->ion_auth_used_by_club->get_ion_id_from_commercant_id($oListeAnnonce->annonce_commercant_id);

        $photoCommercant_path = "application/resources/front/photoCommercant/imagesbank/" . $ionauth_id . "/";
        $photoCommercant_path_old = "application/resources/front/images/";
        ?>

        <?php if ($ij == 0 || $ij == 3 || $ij == 6 || $ij == 9 || $ij == 12 || $ij == 15 || $ij == 18 || $ij == 21 || $ij == 24 || $ij == 27 || $ij == 30 || $ij == 33 || $ij == 36) { ?>
    </div>
<div class="col-xs-12 padding0">
<?php } ?>

<div class="col-sm-4 paddingleft0" style="float:left; display:table;">


    <div id="item_line_<?php echo $oListeAnnonce->annonce_id; ?>" class="annonce_item_line">

        <div class="col-sm-12 padding0 annonce_list_img" id="item_list_img_<?php echo $oListeAnnonce->annonce_id; ?>">

            <?php
            $image_home_vignette = "";
            if (isset($oListeAnnonce->annonce_photo1) && $oListeAnnonce->annonce_photo1 != "" && is_file($photoCommercant_path . $oListeAnnonce->annonce_photo1) == true) {
                $image_home_vignette = $oListeAnnonce->annonce_photo1;
            } else if ($image_home_vignette == "" && isset($oListeAnnonce->annonce_photo2) && $oListeAnnonce->annonce_photo2 != "" && is_file($photoCommercant_path . $oListeAnnonce->annonce_photo2) == true) {
                $image_home_vignette = $oListeAnnonce->annonce_photo2;
            } else if ($image_home_vignette == "" && isset($oListeAnnonce->annonce_photo3) && $oListeAnnonce->annonce_photo3 != "" && is_file($photoCommercant_path . $oListeAnnonce->annonce_photo3) == true) {
                $image_home_vignette = $oListeAnnonce->annonce_photo3;
            } else if ($image_home_vignette == "" && isset($oListeAnnonce->annonce_photo4) && $oListeAnnonce->annonce_photo4 != "" && is_file($photoCommercant_path . $oListeAnnonce->annonce_photo4) == true) {
                $image_home_vignette = $oListeAnnonce->annonce_photo4;
            } else if ($image_home_vignette == "" && isset($oListeAnnonce->annonce_photo1) && $oListeAnnonce->annonce_photo1 != "" && is_file($photoCommercant_path_old . $oListeAnnonce->annonce_photo1) == true) {
                $image_home_vignette = $oListeAnnonce->annonce_photo1;
            } else if ($image_home_vignette == "" && isset($oListeAnnonce->annonce_photo2) && $oListeAnnonce->annonce_photo2 != "" && is_file($photoCommercant_path_old . $oListeAnnonce->annonce_photo2) == true) {
                $image_home_vignette = $oListeAnnonce->annonce_photo2;
            } else if ($image_home_vignette == "" && isset($oListeAnnonce->annonce_photo3) && $oListeAnnonce->annonce_photo3 != "" && is_file($photoCommercant_path_old . $oListeAnnonce->annonce_photo3) == true) {
                $image_home_vignette = $oListeAnnonce->annonce_photo3;
            } else if ($image_home_vignette == "" && isset($oListeAnnonce->annonce_photo4) && $oListeAnnonce->annonce_photo4 != "" && is_file($photoCommercant_path_old . $oListeAnnonce->annonce_photo4) == true) {
                $image_home_vignette = $oListeAnnonce->annonce_photo4;
            }
            ?>


            <a onclick='javascript:window.open("<?php echo site_url("front/annonce/detailAnnonce/" . $oListeAnnonce->annonce_id); ?>", "Details annonce", "width=1045, height=800, scrollbars=yes");'
               href='javascript:void(0);' title="<?php echo $oListeAnnonce->texte_courte; ?>">
                <?php
                if ($image_home_vignette != "") {
                    if (isset($image_home_vignette) && $image_home_vignette != "" && is_file($photoCommercant_path . $image_home_vignette) == true)
                        echo '<img src="' . base_url() . $photoCommercant_path . $image_home_vignette . '" width="100%" height="246" border="0" id="pic_965" name="pic_965" title="" alt=""/>';
                    else echo '<img src="' . base_url() . $photoCommercant_path_old . $image_home_vignette . '" width="100%" height="246" border="0" id="pic_965" name="pic_965" title="" alt=""/>';
                } else {
                    $image_home_vignette_to_show = GetImagePath("front/") . "/no_image_boutique.png";
                    echo '<img src="' . $image_home_vignette_to_show . '" width="100%" height="246" border="0" id="pic_965" name="pic_965" title="" alt=""/>';
                }
                ?>
            </a>
            <div
                style="position:absolute; bottom:0; width:100%; height:40px; background:rgba(0, 0, 0, 0.6); color:#FFFFFF; font-weight: normal; text-transform:uppercase; text-align:center; line-height:40px;"><?php echo $oListeAnnonce->rubrique; ?></div>
        </div>

        <div class="col-sm-12 annonce_list_details" id="item_list_details_<?php echo $oListeAnnonce->annonce_id; ?>"
             style="padding-top:10px; padding-bottom:10px; background-color:#E5E5E5;">

            <p style='background-color: transparent;
        color: #000000;
        font-family: "Arial",sans-serif;
        font-size: 16px;
        font-style: normal;
        font-variant: normal;
        font-weight: 700;
        line-height: 20px;
        margin: 0 0 5px 0 !important;
        padding: 0 !important;
        text-decoration: none;
        vertical-align: 0;'><?php echo $oListeAnnonce->subcateg; ?></p>

            <div class="p_annonce_texte_courte_text col-xs-12 padding0" style='background-color: transparent;
    color: #000000;
    font-family: "Arial",sans-serif;
    font-size: 16px;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 20px;
    text-decoration: none;
    text-align:center; height:85px; overflow:hidden;
    vertical-align: 0;'><?php echo $oListeAnnonce->texte_courte; ?></div>


            <div class="col-xs-12 textaligncenter" style="padding:15px;">
                <?php if ((isset($oListeAnnonce->prix_vente) && $oListeAnnonce->prix_vente != 0) || (isset($oListeAnnonce->prix_ancien) && $oListeAnnonce->prix_ancien != 0)) { ?>
                    <span class="p_annonce_list_ancienprix padding0" style='
   	background-color: transparent;
    color: #008000;
    font-family: "Arial",sans-serif;
    font-size: 26.7px;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 33px;
    text-decoration: none;
    vertical-align: 0;
    text-align:center;
    '>
    	<?php if ((isset($oListeAnnonce->prix_vente) && $oListeAnnonce->prix_vente != 0) && (isset($oListeAnnonce->prix_ancien) && $oListeAnnonce->prix_ancien != 0)) { ?>
            <?php echo $oListeAnnonce->prix_ancien; ?> &euro;
        <?php } else { ?>
            <?php echo $oListeAnnonce->prix_ancien; ?><?php //echo $oListeAnnonce->prix_vente ; ?> &euro;
        <?php } ?>
            
 	</span>
                <?php } ?>

                <?php if (isset($oListeAnnonce->prix_vente) && $oListeAnnonce->prix_vente != 0 && isset($oListeAnnonce->prix_ancien) && $oListeAnnonce->prix_ancien != 0) { ?>
                    <span class="p_annonce_list_prixvente padding0" style='
    background-color: transparent;
    color: #000000;
    font-family: "Arial",sans-serif;
    font-size: 24px;
    font-style: normal;
    font-variant: normal;
    font-weight: 700;
    margin: 5px 0 0 0 !important;
    padding: 0px;
    line-height: 30px;
    text-decoration:line-through;
    vertical-align: 0;'><?php echo $oListeAnnonce->prix_vente; ?> &euro;
	</span>
                <?php } ?>
            </div>


            <div class="p_annonce_state_text" style='
    background-color: transparent;
    color: #000000;
    font-family: "Arial",sans-serif;
    font-size: 14px;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 16px; padding:5px 0;
    text-decoration: none; text-align:center;
    vertical-align: 0;
    '><?php if ($zEtat != "") { ?>Etat : <?php echo $zEtat; ?><?php } ?></div>

            <div class="col-xs-8 padding0">
                <div class="col-xs-12 padding0" style='
    background-color: transparent;
    color: #008000;
    font-family: "Arial",sans-serif;
    font-size: 13.3px;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 16px;
    text-decoration: none;
    vertical-align: 0; 
    text-align:left;
      '><?php echo $oListeAnnonce->NomSociete; ?> </div>
                <div class="col-xs-12 padding0" style='background-color: transparent;
    color: #008000;
    font-family: "Arial",sans-serif;
    font-size: 13.3px;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 16px;
    text-decoration: none;
    vertical-align: 0; 
    text-align:left;'><?php echo $oListeAnnonce->ville; ?></div>
                <?php
                $iNombreAnnonceParDiffuseur = 0;
                if (isset($oListeAnnonce->IdCommercant)) {
                    if ($oListeAnnonce->IdCommercant != null && $oListeAnnonce->IdCommercant != 0 && $oListeAnnonce->IdCommercant != "") {
                        $iNombreAnnonceParDiffuseur = $mdlannonce->nombreAnnonceParDiffuseur($oListeAnnonce->IdCommercant);
                    }
                }
                ?>
                <div class="col-xs-12 padding0" style='background-color: transparent;
        color: #000000;
        font-family: "Arial",sans-serif;
        font-size: 13.3px;
        font-style: normal;
        font-variant: normal;
        font-weight: normal;
        line-height: 16px;
        text-decoration: none;
        vertical-align: 0;'><?php echo $iNombreAnnonceParDiffuseur; ?> annonces d&eacute;pos&eacute;es
                </div>
            </div>


            <div class="annonce_list_info_btn padding0 col-xs-4" style="text-align: right;">
                <a onclick='javascript:window.open("<?php echo site_url("front/annonce/detailAnnonce/" . $oListeAnnonce->annonce_id); ?>", "Details annonce", "width=1045, height=800, scrollbars=yes");'
                   href='javascript:void(0);' title="<?php echo $oListeAnnonce->texte_courte; ?>"><img
                        src="<?php echo GetImagePath("privicarte/"); ?>/plus_green.png"></a>
            </div>


        </div>


    </div>

</div>

<?php if ($ij == 2 || $ij == 5 || $ij == 8 || $ij == 11 || $ij == 14 || $ij == 17 || $ij == 20 || $ij == 23 || $ij == 26 || $ij == 29 || $ij == 32 || $ij == 35 || $ij == 38) { ?>
</div>
    <div class="col-xs-12 padding0">
        <?php } ?>

        <?php $ij = $ij + 1; ?>

        <?php } ?>
    </div>

    <?php if (isset($_GET['content_only_list']) && $_GET['content_only_list'] == '1') {
    } else { ?>
</div>
<?php } ?>


<?php if (isset($_GET['content_only_list']) && $_GET['content_only_list'] == '1') {
} else { ?>

    <?php
    $data['empty'] = null;
    if (strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')) {
        $this->load->view("privicarte/annonce_list_listing_ipad.php", $data);
    } else {
        $this->load->view("privicarte/annonce_list_listing.php", $data);
    }
    ?>

    <!--
    <?php if (isset($links_pagination) && $links_pagination != "") { ?>
    <div id="view_pagination_ci">
    <?php echo $links_pagination; ?>
    </div> 
    <?php } ?> -->

<?php } ?>


