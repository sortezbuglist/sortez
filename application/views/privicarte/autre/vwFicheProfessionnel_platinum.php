<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("privicarte/includes/header_mini_2", $data); ?>

    <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/fields.check.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>

    <script type="text/javascript">


        function listeSousRubrique() {

            var irubId = jQuery('#RubriqueSociete').val();
            jQuery.get(
                '<?php echo site_url("front/professionnels/testAjax"); ?>' + '/' + irubId,
                function (zReponse) {
                    // alert (zReponse) ;
                    jQuery('#trReponseRub').html("");
                    jQuery('#trReponseRub').html(zReponse);


                });
        }

        function getCP() {
            var ivilleId = jQuery('#VilleSociete').val();
            jQuery.get(
                '<?php echo site_url("front/professionnels/getPostalCode"); ?>' + '/' + ivilleId,
                function (zReponse) {
                    jQuery('#CodePostalSociete').val(zReponse);
                });
        }

        function getCP_localisation() {
            var ivilleId = jQuery('#IdVille_localisationSociete').val();
            jQuery.get(
                '<?php echo site_url("front/professionnels/getPostalCode_localisation"); ?>' + '/' + ivilleId,
                function (zReponse) {
                    jQuery('#trReponseVille_localisation').html(zReponse);


                });
        }

        function getLatitudeLongitudeLocalisation() {
            var geocoder = new google.maps.Geocoder();
            var adresse_localisationSociete = $("#adresse_localisationSociete").val();
            if (adresse_localisationSociete != "") adresse_localisationSociete = adresse_localisationSociete + ', ';
            var Ville_localisationSociete = $("#IdVille_localisationSociete option:selected").text();
            var codepostal_localisationSociete = $("#codepostal_localisationSociete").val();
            geocoder.geocode({'address': adresse_localisationSociete + Ville_localisationSociete + ', ' + codepostal_localisationSociete + ', fr'}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    //alert("location : " + results[0].geometry.location.lat() + " " +results[0].geometry.location.lng());
                    $("#latitudeSociete").val(results[0].geometry.location.lat());
                    $("#longitudeSociete").val(results[0].geometry.location.lng());
                } else {
                    //alert("Something got wrong " + status);
                }
            });
        }

        function getLatitudeLongitudeAdresse() {
            var geocoder = new google.maps.Geocoder();
            var adresse_localisationSociete = $("#Adresse1Societe").val();
            if (adresse_localisationSociete != "") adresse_localisationSociete = adresse_localisationSociete + ', ';
            var Ville_localisationSociete = $("#VilleSociete option:selected").text();
            var codepostal_localisationSociete = $("#CodePostalSociete").val();
            geocoder.geocode({'address': adresse_localisationSociete + Ville_localisationSociete + ', ' + codepostal_localisationSociete + ', fr'}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    //alert("location : " + results[0].geometry.location.lat() + " " +results[0].geometry.location.lng());
                    $("#latitudeSociete").val(results[0].geometry.location.lat());
                    $("#longitudeSociete").val(results[0].geometry.location.lng());
                } else {
                    //alert("Something got wrong " + status);
                }
            });
        }

        function deleteFile(_IdCommercant, _FileName) {
            jQuery('.loading_' + _FileName).html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            jQuery.ajax({
                url: '<?php echo base_url();?>front/professionnels/delete_files/' + _IdCommercant + '/' + _FileName,
                dataType: 'html',
                type: 'POST',
                async: true,
                success: function (data) {
                    jQuery('.loading_' + _FileName).html("");
                    window.location.reload();
                }
            });

        }

        jQuery(document).ready(function () {

            var _URL = window.URL || window.webkitURL;

            jQuery("#background_imageSociete").change(function (e) {
                var image, file;
                if ((file = this.files[0])) {
                    image = new Image();
                    image.onload = function () {
                        //alert("The image width is " +this.width + " and image height is " + this.height);
                        if (this.width > 2025) {
                            alert("La largeur de votre image dépasse le 2024px !");
                            $("#background_imageSociete_checker").val("1");
                        } else if (this.height > 1101) {
                            alert("La hauteur de votre image dépasse le 1100px !");
                            $("#background_imageSociete_checker").val("1");
                        } else {
                            $("#background_imageSociete_checker").val("0");
                        }
                    };
                    image.src = _URL.createObjectURL(file);
                }
            });

            jQuery("#bandeau_topSociete").change(function (e) {
                var image, file;
                if ((file = this.files[0])) {
                    image = new Image();
                    image.onload = function () {
                        //alert("The image width is " +this.width + " and image height is " + this.height);
                        if (this.width > 1025) {
                            alert("La largeur de votre image dépasse le 1024px !");
                            $("#bandeau_topSociete_checker").val("1");
                        } else if (this.height > 351) {
                            alert("La hauteur de votre image dépasse le 350px !");
                            $("#bandeau_topSociete_checker").val("1");
                        } else {
                            $("#bandeau_topSociete_checker").val("0");
                        }
                    };
                    image.src = _URL.createObjectURL(file);
                }
            });

            jQuery("#logoSociete").change(function (e) {
                var image, file;
                if ((file = this.files[0])) {
                    image = new Image();
                    image.onload = function () {
                        //alert("The image width is " +this.width + " and image height is " + this.height);
                        if (this.width > 271) {
                            alert("La largeur de votre logo dépasse le 270px !");
                            $("#Societelogo_checker").val("1");
                        } else {
                            $("#Societelogo_checker").val("0");
                        }
                    };
                    image.src = _URL.createObjectURL(file);
                }
            });

            jQuery('#PdfSociete').bind('change', function () {
                if (this.files[0].size > 3005000) {
                    alert("La taille de votre fichier PDF dépasse le 3Mb !");
                    $("#PdfSociete_checker").val("1");
                } else {
                    $("#PdfSociete_checker").val("0");
                }
            });


            jQuery(".btnSinscrire").click(function () {

                jQuery('#div_error_fiche_pro').html('<div style="text-align: center;"><img src="<?php echo GetImagePath("front/");?>/loading.gif" /></div>');

                //alert('test form submit');
                var txtError = "";

                var background_imageSociete_checker = jQuery("#background_imageSociete_checker").val();
                if (background_imageSociete_checker == "1") {
                    txtError += "- La taille de votre image page et arrère plan dépasse la largeur ou la hauteur indiquée.<br/>";
                    $("#background_imageSociete_checker").css('border-color', 'red');
                    $("#background_imageSociete_checker").css('color', 'red');
                } else {
                    $("#background_imageSociete_checker").css('border-color', '#E3E1E2');
                }

                var bandeau_topSociete_checker = jQuery("#bandeau_topSociete_checker").val();
                if (bandeau_topSociete_checker == "1") {
                    txtError += "- La taille de votre image bannière page de présentation dépasse la largeur ou la hauteur indiquée.<br/>";
                    $("#bandeau_topSociete_checker").css('border-color', 'red');
                    $("#bandeau_topSociete_checker").css('color', 'red');
                } else {
                    $("#bandeau_topSociete_checker").css('border-color', '#E3E1E2');
                }

                var Societelogo_checker = jQuery("#Societelogo_checker").val();
                if (Societelogo_checker == "1") {
                    txtError += "- La taille de votre Logo dépasse la largeur indiquée.<br/>";
                    $("#Societelogo_checker").css('border-color', 'red');
                    $("#Societelogo_checker").css('color', 'red');
                } else {
                    $("#Societelogo_checker").css('border-color', '#E3E1E2');
                }

                var PdfSociete_checker = jQuery("#PdfSociete_checker").val();
                if (PdfSociete_checker == "1") {
                    txtError += "- La taille de votre fichier PDF dépasse la taille indiquée.<br/>";
                    $("#PdfSociete_checker").css('border-color', 'red');
                    $("#PdfSociete_checker").css('color', 'red');
                } else {
                    $("#PdfSociete_checker").css('border-color', '#E3E1E2');
                }

                var EmailSociete = jQuery("#EmailSociete").val();
                if (!isEmail(EmailSociete)) {
                    txtError += "- L'adresse email de votre etablissement n'est pas valide.<br/>";
                    $("#EmailSociete").css('border-color', 'red');
                } else {
                    $("#EmailSociete").css('border-color', '#E3E1E2');
                }

                <?php if (isset($user_groups) && ($user_groups->id == '5' || $user_groups->id == '4')) { ?>
                var Email_decideurSociete = jQuery("#Email_decideurSociete").val();
                if (!isEmail(Email_decideurSociete)) {
                    txtError += "- L'adresse email du decideur n'est pas valide.<br/>";
                    $("#Email_decideurSociete").css('border-color', 'red');
                } else {
                    $("#Email_decideurSociete").css('border-color', '#E3E1E2');
                }
                <?php } ?>
                /*
            var LoginSociete = jQuery("#LoginSociete").val();
            if(!isEmail(LoginSociete)) {
                txtError += "1";
                jQuery("#divErrorLoginSociete").html("Votre login doit &ecirc;tre un email valide.");
                jQuery("#divErrorLoginSociete").show();
            } else {
                jQuery("#divErrorLoginSociete").hide();
            }
            */

                <?php if ((isset($page_data) && $page_data == 'coordonnees') && (isset($objAbonnementCommercant) && $user_groups->id == '4')) { ?>
                /*ar HorairesSociete = CKEDITOR.instances['HorairesSociete'].getData();
				if(HorairesSociete=="") {
					txtError += "- Veuillez indiquer Heures et jours d'ouvertures<br/>";
					$("#HorairesSociete").css('border-color', 'red');
				} else {
					$("#HorairesSociete").css('border-color', '#E3E1E2');
				}*/
                <?php } else if ((isset($page_data) && $page_data == 'contenus') && (isset($objAbonnementCommercant) && $user_groups->id == '5')) { ?>
                /*var HorairesSociete = CKEDITOR.instances['HorairesSociete'].getData();
				if(HorairesSociete=="") {
					txtError += "- Veuillez indiquer Heures et jours d'ouvertures<br/>";
					$("#HorairesSociete").css('border-color', 'red');
				} else {
					$("#HorairesSociete").css('border-color', '#E3E1E2');
				}*/
                <?php } ?>


                <?php if (isset($page_data) && $page_data == 'contenus') { ?>
                /*var CaracteristiquesSociete = CKEDITOR.instances['CaracteristiquesSociete'].getData();
				if(CaracteristiquesSociete=="") {
					txtError += "- Veuillez indiquer la présentation de votre activité dans Page d'accueil<br/>";
					$("#cke_CaracteristiquesSociete").css('border-color', 'red');
				} else {
					$("#cke_CaracteristiquesSociete").css('border-color', '#E3E1E2');
				}*/

                var Photo1Societe = $("#Photo1Societe").val();
                if (Photo1Societe == "") {
                    txtError += "- Veuillez indiquer une photo dans Page d'acceuil<br/>";
                    $("#Photo1Societe").css('color', 'red');
                } else {
                    $("#Photo1Societe").css('color', '#E3E1E2');
                }

                <?php if (isset($user_groups) && $user_groups->id == '5') { ?>
                /*var logoSociete = $("#logoSociete").val();
				if(logoSociete=="") {
					txtError += "- Veuillez indiquer Votre Logo<br/>";
					$("#logoSociete").css('color', 'red');
				} else {
					$("#logoSociete").css('color', '#E3E1E2');
				}*/
                <?php } ?>

                <?php } ?>


                var qrcode_text_submit = $("#qrcode_text").val();
                var qrcode_text_submit_verify = "";
                jQuery.post(
                    '<?php echo site_url("front/professionnels/get_qrcode_text"); ?>',
                    {IdCommercant: "<?php echo $objCommercant->IdCommercant; ?>"},
                    function (zReponse2) {
                        qrcode_text_submit_verify = zReponse2;

                        if (qrcode_text_submit != qrcode_text_submit_verify) {
                            txtError += "- Veuillez Re-créer le Qrcode avant de valider les modifications !<br/>";
                            $("#qrcode_text").css('color', 'red');
                        } else {
                            $("#qrcode_text").css('color', '#000000');
                        }

                        var subdomain_url_check = $("#subdomain_url_check").val();

                        if (subdomain_url_check == "0") {
                            var subdomain_url_submit = $("#subdomain_url").val();
                            jQuery.post(
                                '<?php echo site_url("front/professionnels/verify_privicarte_subdomain_code"); ?>',
                                {
                                    subdomain_url: subdomain_url_submit,
                                    IdCommercant: "<?php echo $objCommercant->IdCommercant; ?>"
                                },
                                function (zReponse3) {
                                    if (zReponse3 == "1") {
                                        txtError += "- Votre sous-domaine existe déjà !<br/>";
                                        $("#subdomain_url").css('color', 'red');
                                    } else {
                                        $("#subdomain_url").css('color', '#000000');
                                    }

                                    if (txtError == "") {
                                        jQuery("#div_error_fiche_pro").hide();
                                        jQuery("#frmInscriptionProfessionnel").submit();
                                    } else {
                                        jQuery("#div_error_fiche_pro").html(txtError);
                                    }

                                });
                        } else if (subdomain_url_check == "1") {
                            var subdomain_url_submit = $("#subdomain_url_vsv").val();
                            jQuery.post(
                                '<?php echo site_url("front/professionnels/verify_privicarte_subdomain_code_vsv"); ?>',
                                {
                                    subdomain_url: subdomain_url_submit,
                                    IdCommercant: "<?php echo $objCommercant->IdCommercant; ?>"
                                },
                                function (zReponse3) {
                                    if (zReponse3 == "1") {
                                        txtError += "- Votre sous-domaine existe déjà !<br/>";
                                        $("#subdomain_url_vsv").css('color', 'red');
                                    } else {
                                        $("#subdomain_url_vsv").css('color', '#000000');
                                    }

                                    if (txtError == "") {
                                        jQuery("#div_error_fiche_pro").hide();
                                        jQuery("#frmInscriptionProfessionnel").submit();
                                    } else {
                                        jQuery("#div_error_fiche_pro").html(txtError);
                                    }

                                });
                        }


                    });


            });


            //$('#bandeau_colorSociete').colorPicker();


            $("#adresse_localisation_diffuseur_checkbox").click(function () {
                if ($('#adresse_localisation_diffuseur_checkbox').attr('checked')) {
                    $("#adresse_localisation_diffuseur").val("1");
                    var adress_organisateur1 = $("#Adresse1Societe").val();
                    var adress_organisateur2 = $("#Adresse2Societe").val();
                    var IdVille_organisateur = $("#VilleSociete").val();
                    var codepostal_organisateur = $("#CodePostalSociete").val();
                    $("#adresse_localisationSociete").val(adress_organisateur1 + " " + adress_organisateur2);
                    $("#IdVille_localisationSociete").val(IdVille_organisateur);
                    $("#codepostal_localisationSociete").val(codepostal_organisateur);
                } else {
                    $("#adresse_localisation_diffuseur").val("0");
                    $("#adresse_localisationSociete").val("");
                    $("#IdVille_localisationSociete").val("0");
                    $("#codepostal_localisationSociete").val("");
                }
            });


            <?php if ($objCommercant->qrcode_text == "" || $objCommercant->qrcode_text == NULL) { ?>

            jQuery('#span_generate_qrcode').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            $("input.btnSinscrire").attr("disabled", true);
            var qrcode_text = "http://<?php echo $objCommercant->nom_url; ?>.sortez.org";
            jQuery('#qrcode_text').val(qrcode_text);
            jQuery.post(
                '<?php echo site_url("front/professionnels/generate_qrcode"); ?>',
                {qrcode_text: qrcode_text},
                function (zReponse) {
                    //alert(zReponse);
                    jQuery('#div_qrcode_img').html(zReponse);
                    //jQuery('#qrcode_text').val(qrcode_text);
                    jQuery.post(
                        '<?php echo site_url("front/professionnels/get_qrcode_img"); ?>',
                        {IdCommercant: "<?php echo $objCommercant->IdCommercant; ?>"},
                        function (zReponse2) {
                            //alert(zReponse2);
                            jQuery('#span_generate_qrcode').html("");
                            jQuery('#qrcode_img').val(zReponse2);
                            $("input.btnSinscrire").attr("disabled", false);
                        });
                });

            <?php } ?>



            //removing divErrorDisplay affter 5 seconds
            setTimeout(function () {
                jQuery('#divErrorDisplay').hide();
            }, 4000);


            $("#nom_url_updated_pro_check").click(function () {
                if ($('#nom_url_updated_pro_check').attr('checked')) {
                    $("#subdomain_url_check").val("0");
                }
            });
            $("#nom_url_updated_pro_check_vsv").click(function () {
                if ($('#nom_url_updated_pro_check_vsv').attr('checked')) {
                    $("#subdomain_url_check").val("1");
                }
            });

            $("#nom_url_send_activation_btn").click(function () {
                jQuery('#span_nom_url_send_activation').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
                jQuery.post(
                    '<?php echo site_url("front/professionnels/send_nom_url_activation"); ?>',
                    function (zReponse) {
                        jQuery('#span_nom_url_send_activation').html("Demande envoyée !");
                        jQuery("#nom_url_send_activation_btn").attr("disabled", "disabled");
                    });
            });

            $("#subdomain_url").keyup(function () {
                $("#subdomain_url_vsv").val($(this).val());
            });
            $("#subdomain_url_vsv").keyup(function () {
                $("#subdomain_url").val($(this).val());
            });



            $("#isActive_page1_1").change(function () {

                if(document.getElementById('isActive_page1_1').value ==0){
                    document.getElementById('nbglissiere1p1').style.display="none";
                    document.getElementById('dispnbglip1').style.display="none";
                }else if (document.getElementById('isActive_page1_1').value ==1){
                    document.getElementById('nbglissiere1p1').style.display="inline";
                    document.getElementById('dispnbglip1').style.display="inline";
                }

            });


            $("#nbglissiere1p1").change(function () {

                if(document.getElementById('nbglissiere1p1').value == 1){
                    document.getElementById('btn_gli1_p1').style.display="block";
                    document.getElementById('gl1_p1_img').style.display="block";
                    document.getElementById('gl1_p1_tittle').style.display="block";
                    document.getElementById('glissiere1contentp1').style.display="block";
                    document.getElementById('gl1_p1_tittle2').style.display="none";
                    document.getElementById('gl1_p1_content2').style.display="none";
                    document.getElementById('gl1_p1_img2').style.display="none";
                    document.getElementById('btn_p1_gli2').style.display="none";
                    document.getElementById('alllink2p1').style.display="none";

                }else if (document.getElementById('nbglissiere1p1').value ==2){
                    document.getElementById('btn_gli1_p1').style.display="block";
                    document.getElementById('gl1_p1_img').style.display="block";
                    document.getElementById('gl1_p1_tittle').style.display="block";
                    document.getElementById('glissiere1contentp1').style.display="block";
                    document.getElementById('gl1_p1_tittle2').style.display="block";
                    document.getElementById('gl1_p1_content2').style.display="block";
                    document.getElementById('gl1_p1_img2').style.display="block";
                    document.getElementById('btn_p1_gli2').style.display="block";
                    document.getElementById('alllink2p1').style.display="block";
                }
                else if (document.getElementById('nbglissiere1p1').value ==0){
                    document.getElementById('btn_gli1_p1').style.display="none";
                    document.getElementById('gl1_p1_img').style.display="none";
                    document.getElementById('gl1_p1_tittle').style.display="none";
                    document.getElementById('glissiere1contentp1').style.display="none";
                    document.getElementById('gl2_tittle').style.display="none";
                    document.getElementById('gl2_content').style.display="none";
                }

            });

            $("#nbglissiere1").change(function () {

                if(document.getElementById('link_btn_gli1').value == 1){
                    document.getElementById('btn_opt').style.display="block";


                }
                else if (document.getElementById('link_btn_gli1').value ==0){
                    document.getElementById('btn_opt').style.display="none";

                }

            });

            $("#link_btn_p1_gli2").change(function () {

                if(document.getElementById('link_btn_p1_gli2').value == 1){
                    document.getElementById('alllink2p1').style.display="block";


                }
                else if (document.getElementById('link_btn_p1_gli2').value ==0){
                    document.getElementById('alllink2p1').style.display="none";

                }

            });
            $("#link_btn_p1_gli1").change(function () {

                if(document.getElementById('link_btn_p1_gli1').value == 1){
                    document.getElementById('alllink_p1').style.display="block";


                }
                else if (document.getElementById('link_btn_p1_gli1').value ==0){
                    document.getElementById('alllink_p1').style.display="none";

                }

            });


            $("#isActive_presentation_1").change(function () {

                if (document.getElementById('isActive_presentation_1').value == 0) {
                    document.getElementById('dispnbgli').style.display = "none";
                    document.getElementById('glissiere1content').style.display = "none";
                } else if (document.getElementById('isActive_presentation_1').value == 1) {
                    document.getElementById('dispnbgli').style.display = "inline";
                    document.getElementById('glissiere1content').style.display = "block";
                }

            });


            $("#nbglissiere1").change(function () {

                if (document.getElementById('nbglissiere1').value == 1) {
                    document.getElementById('btn_gli1').style.display = "block";
                    document.getElementById('gl1_img').style.display = "block";
                    document.getElementById('gl1_tittle').style.display = "block";
                    document.getElementById('gl1_content').style.display = "block";
                    document.getElementById('gl1_tittle2').style.display = "none";
                    document.getElementById('gl1_content2').style.display = "none";

                } else if (document.getElementById('nbglissiere1').value == 2) {
                    document.getElementById('btn_gli1').style.display = "block";
                    document.getElementById('gl1_img').style.display = "block";
                    document.getElementById('gl1_tittle').style.display = "block";
                    document.getElementById('gl1_content').style.display = "block";
                    document.getElementById('gl1_tittle2').style.display = "block";
                    document.getElementById('gl1_content2').style.display = "block";
                    document.getElementById('gl1_img2').style.display = "block";
                    document.getElementById('btn_gli2').style.display = "block";
                }
                else if (document.getElementById('nbglissiere1').value == 0) {
                    document.getElementById('btn_gli1').style.display = "none";
                    document.getElementById('gl1_img').style.display = "none";
                    document.getElementById('gl1_tittle').style.display = "none";
                    document.getElementById('gl1_content').style.display = "none";
                    document.getElementById('gl2_tittle').style.display = "none";
                    document.getElementById('gl2_content').style.display = "none";
                }

            });

            $("#nbglissiere1").change(function () {

                if (document.getElementById('link_btn_gli1').value == 1) {
                    document.getElementById('btn_opt').style.display = "block";


                }
                else if (document.getElementById('link_btn_gli1').value == 0) {
                    document.getElementById('btn_opt').style.display = "none";

                }

            });

            $("#link_btn_gli1").change(function () {

                if (document.getElementById('link_btn_gli1').value == 1) {
                    document.getElementById('alllink').style.display = "block";


                }
                else if (document.getElementById('link_btn_gli1').value == 0) {
                    document.getElementById('alllink').style.display = "none";

                }

            });


            $("#link_btn_gli2").change(function () {

                if (document.getElementById('link_btn_gli2').value == 1) {
                    document.getElementById('alllink2').style.display = "block";


                }
                else if (document.getElementById('link_btn_gli2').value == 0) {
                    document.getElementById('alllink2').style.display = "none";

                }

            });


            $("#link_btn_bloc1").change(function () {

                if (document.getElementById('link_btn_bloc1').value == 1) {
                    document.getElementById('link_bloc1').style.display = "block";

                }
                else if (document.getElementById('link_btn_bloc1').value == 0) {
                    document.getElementById('link_bloc1').style.display = "none";

                }

            });

            $("#link_btn_bloc2").change(function () {

                if (document.getElementById('link_btn_bloc2').value == 1) {
                    document.getElementById('link_bloc2').style.display = "block";

                }
                else if (document.getElementById('link_btn_bloc2').value == 0) {
                    document.getElementById('link_bloc2').style.display = "none";

                }

            });


            $("#link_btn_fd").change(function () {

                if (document.getElementById('link_btn_fd').value == 1) {
                    document.getElementById('link_fd').style.display = "block";

                }
                else if (document.getElementById('link_btn_fd').value == 0) {
                    document.getElementById('link_fd').style.display = "none";

                }

            });


            $("#link_btn_bp").change(function () {

                if (document.getElementById('link_btn_bp').value == 1) {
                    document.getElementById('link_bp').style.display = "block";

                }
                else if (document.getElementById('link_btn_bp').value == 0) {
                    document.getElementById('link_bp').style.display = "none";

                }

            });


            $("#type_bloc").change(function () {

                if (document.getElementById('type_bloc').value == 1) {
                    document.getElementById('bloc1').style.display = "block";
                    document.getElementById('bloc2').style.display = "none";
                    document.getElementById('bonplan_config').style.display = "none";
                    document.getElementById('fidelity_config').style.display = "none";
                }
                else if (document.getElementById('type_bloc').value == 2) {
                    document.getElementById('bloc1').style.display = "block";
                    document.getElementById('bloc2').style.display = "block";
                    document.getElementById('bonplan_config').style.display = "none";
                    document.getElementById('fidelity_config').style.display = "none";

                } else if (document.getElementById('type_bloc').value == '1bp') {
                    document.getElementById('bonplan_config').style.display = "block";
                    document.getElementById('bloc1').style.display = "none";
                    document.getElementById('bloc2').style.display = "none";
                    document.getElementById('fidelity_config').style.display = "none";


                } else if (document.getElementById('type_bloc').value == '1fd') {

                    document.getElementById('fidelity_config').style.display = "block";
                    document.getElementById('bonplan_config').style.display = "none";
                    document.getElementById('bloc1').style.display = "none";
                    document.getElementById('bloc2').style.display = "none";
                }

            });

            $("#is_activ_bloc").change(function () {

                if (document.getElementById('is_activ_bloc').value == 0) {
                    document.getElementById('type_bloc_choose').style.display = "none";
                    document.getElementById('bloc1').style.display = "none";
                    document.getElementById('bloc2').style.display = "none";
                }
                else if (document.getElementById('is_activ_bloc').value == 1) {
                    document.getElementById('type_bloc_choose').style.display = "block";
                    document.getElementById('bloc1').style.display = "none";
                    document.getElementById('bloc2').style.display = "none";

                }

            });

            <?php if (isset($objGlissiere->bonplan_id) AND $objGlissiere->bonplan_id != null AND $objGlissiere->bonplan_id != ''){?>

            bonplan = document.getElementById('bonplan_select').value;
            document.getElementById('bonplan_div').style.display = "block";
            $.get("<?php echo base_url();?>front/professionnels/get_bonplan_selected2/" + bonplan, function (data) {
                //alert( "Data Loaded: " + data );
                console.log(data);
                $("#bonplan_content_posted").html(data);
            });

            <?php } ?>








            $("#bonplan_select").change(function () {
                // $("#bonplan_content_posted").html('');
                if (document.getElementById('bonplan_select').value != 0) {
                    bonplan = document.getElementById('bonplan_select').value;
                    document.getElementById('bonplan_div').style.display = "block";
                    $.get("http://localhost/sortez7/front/professionnels/get_bonplan_selected/" + bonplan, function (data) {
                        //alert( "Data Loaded: " + data );
                        $("#bonplan_content_posted").html(data);
                    });
                } else {

                    document.getElementById('bonplan_content_posted').style.display = "none";
                    document.getElementById('bonplan_div').style.display = "none";
                }
            });


            $("#fidelity_select").change(function () {
                $("#fidelity_content_posted").html('');
                if (document.getElementById('fidelity_select').value != 0) {
                    fidelity = document.getElementById('fidelity_select').value;
                    document.getElementById('fidelity_div').style.display = "block";
                    $.get("http://localhost/sortez7/front/professionnels/get_fidelity_selected/" + fidelity, function (data) {
                        //alert( "Data Loaded: " + data );
                        $("#fidelity_content_posted").html(data);
                    });
                } else {

                    document.getElementById('fidelity_content_posted').style.display = "none";
                    document.getElementById('fidelity_div').style.display = "none";
                }
            });


        });


        /*
$(document).off('blur', '#postcode').on('blur', '#postcode', function(e){link_btn_gli1
    if ($('#postcode').val() != "") {
        setTimeout(function() {
            $('#postcode').parent().removeClass("form-error");
            $('#postcode').parent().addClass("form-ok");
            //alert('ok');
        }
        ,500);
    }
});
*/

        function CP_getDepartement() {
            //alert(jQuery('#CodePostalSociete').val());
            jQuery('#departementCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var CodePostalSociete = jQuery('#CodePostalSociete').val();
            jQuery.post(
                '<?php echo site_url("front/professionnels/departementcp"); ?>',
                {CodePostalSociete: CodePostalSociete},
                function (zReponse) {
                    jQuery('#departementCP_container').html(zReponse);
                });
        }

        function CP_getVille() {
            //alert(jQuery('#CodePostalSociete').val());
            jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var CodePostalSociete = jQuery('#CodePostalSociete').val();
            jQuery.post(
                '<?php echo site_url("front/professionnels/villecp"); ?>',
                {CodePostalSociete: CodePostalSociete},
                function (zReponse) {
                    jQuery('#villeCP_container').html(zReponse);
                });
        }

        function CP_getVille_localisation() {
            //alert(jQuery('#CodePostalSociete').val());
            jQuery('#villeCP_container_localisation').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var CodePostalSociete = jQuery('#codepostal_localisationSociete').val();
            jQuery.post(
                '<?php echo site_url("front/professionnels/villecp_localisation"); ?>',
                {CodePostalSociete: CodePostalSociete},
                function (zReponse) {
                    jQuery('#villeCP_container_localisation').html(zReponse);
                });
        }

        function CP_getVille_D_CP() {
            //alert(jQuery('#CodePostalSociete').val());
            jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var CodePostalSociete = jQuery('#CodePostalSociete').val();
            var departement_id = jQuery('#departement_id').val();
            jQuery.post(
                '<?php echo site_url("front/professionnels/villecp"); ?>',
                {CodePostalSociete: CodePostalSociete, departement_id: departement_id},
                function (zReponse) {
                    jQuery('#villeCP_container').html(zReponse);
                });
        }

        function open_map_localisation() {
            <?php
            if (is_object($objCommercant) && isset($objCommercant->IdVille_localisation)) {
                if (isset($this->mdlville->getVilleById($objCommercant->IdVille_localisation)->Nom))
                    $ville_map_locali = $this->mdlville->getVilleById($objCommercant->IdVille_localisation)->Nom;
            } else $ville_map_locali = '';
            if (!isset($ville_map_locali)) $ville_map_locali = '';
            ?>
            window.open("http://maps.google.fr/maps?f=q&source=s_q&hl=fr&geocode=&q=<?php echo $objCommercant->adresse_localisation . ", " . $objCommercant->codepostal_localisation . " " . $ville_map_locali;?>&aq=0&ie=UTF8&hq=&hnear=<?php echo $objCommercant->adresse_localisation . ", " . $objCommercant->codepostal_localisation . " " . $ville_map_locali;?>&t=m&vpsrc=0", "MsgWindow", "width=800, height=800");
        }


        function verify_privicarte_subdomain() {
            jQuery('#span_verify_subdomain').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var subdomain_url = jQuery('#subdomain_url').val();
            jQuery.post(
                '<?php echo site_url("front/professionnels/verify_privicarte_subdomain"); ?>',
                {subdomain_url: subdomain_url, IdCommercant: "<?php echo $objCommercant->IdCommercant; ?>"},
                function (zReponse) {
                    jQuery('#span_verify_subdomain').html(zReponse);
                });
        }

        function verify_privicarte_subdomain_vsv() {
            jQuery('#span_verify_subdomain_vsv').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var subdomain_url = jQuery('#subdomain_url_vsv').val();
            jQuery.post(
                '<?php echo site_url("front/professionnels/verify_privicarte_subdomain"); ?>',
                {subdomain_url: subdomain_url, IdCommercant: "<?php echo $objCommercant->IdCommercant; ?>"},
                function (zReponse) {
                    jQuery('#span_verify_subdomain_vsv').html(zReponse);
                });
        }


        function generate_qrcode() {
            jQuery('#span_generate_qrcode').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            $("input.btnSinscrire").attr("disabled", true);
            var qrcode_text = jQuery('#qrcode_text').val();
            jQuery.post(
                '<?php echo site_url("front/professionnels/generate_qrcode"); ?>',
                {qrcode_text: qrcode_text},
                function (zReponse) {
                    jQuery('#div_qrcode_img').html(zReponse);
                    jQuery('#qrcode_text').val(qrcode_text);
                    jQuery.post(
                        '<?php echo site_url("front/professionnels/get_qrcode_img"); ?>',
                        {IdCommercant: "<?php echo $objCommercant->IdCommercant; ?>"},
                        function (zReponse2) {
                            jQuery('#span_generate_qrcode').html("");
                            jQuery('#qrcode_img').val(zReponse2);
                            $("input.btnSinscrire").attr("disabled", false);
                        });
                });
        }


        function show_btn_pro_contenus_home() {
            jQuery('#blocss').css("display", "block");
            jQuery('#div_pro_titre_infos').css("display", "none");
            jQuery('#div_pro_titre_home').css("display", "block");
            jQuery('#div_pro_titre_page1').css("display", "none");
            jQuery('#div_pro_titre_page2').css("display", "none");

            jQuery('#div_pro_logo').css("display", "none");
            jQuery('#div_pro_fondecran').css("display", "none");
            jQuery('#div_pro_horaires').css("display", "none");
            jQuery('#div_pro_pagepresentation').css("display", "block");
            jQuery('#div_pro_image_presentation').css("display", "block");
            jQuery('#div_pro_entete_banniere').css("display", "none");
            jQuery('#div_pro_geolocalisation').css("display", "none");
            jQuery('#div_pro_page1').css("display", "none");
            jQuery('#div_pro_page2').css("display", "none");
            jQuery('#div_pro_multimedia').css("display", "none");
            jQuery('#div_pro_glissiere1_home').css("display", "block");
            jQuery('#div_pro_glissiere2_home').css("display", "block");
            jQuery('#div_pro_glissiere3_home').css("display", "block");
            jQuery('#div_pro_glissiere4_home').css("display", "block");
            jQuery('#div_pro_glissiere1_page1').css("display", "none");
            jQuery('#div_pro_glissiere2_page1').css("display", "none");
            jQuery('#div_pro_glissiere3_page1').css("display", "none");
            jQuery('#div_pro_glissiere4_page1').css("display", "none");
            jQuery('#div_pro_glissiere1_page2').css("display", "none");
            jQuery('#div_pro_glissiere2_page2').css("display", "none");
            jQuery('#div_pro_glissiere3_page2').css("display", "none");
            jQuery('#div_pro_glissiere4_page2').css("display", "none");
        }

        function show_btn_pro_contenus_page1() {
            jQuery('#blocss').css("display", "none");
            jQuery('#div_pro_titre_infos').css("display", "none");
            jQuery('#div_pro_titre_home').css("display", "none");
            jQuery('#div_pro_titre_page1').css("display", "block");
            jQuery('#div_pro_titre_page2').css("display", "none");

            jQuery('#div_pro_logo').css("display", "none");
            jQuery('#div_pro_fondecran').css("display", "none");
            jQuery('#div_pro_horaires').css("display", "none");
            jQuery('#div_pro_pagepresentation').css("display", "none");
            jQuery('#div_pro_image_presentation').css("display", "none");
            jQuery('#div_pro_entete_banniere').css("display", "none");
            jQuery('#div_pro_geolocalisation').css("display", "none");
            jQuery('#div_pro_page1').css("display", "block");
            jQuery('#div_pro_page2').css("display", "none");
            jQuery('#div_pro_multimedia').css("display", "none");
            jQuery('#div_pro_glissiere1_home').css("display", "none");
            jQuery('#div_pro_glissiere2_home').css("display", "none");
            jQuery('#div_pro_glissiere3_home').css("display", "none");
            jQuery('#div_pro_glissiere4_home').css("display", "none");
            jQuery('#div_pro_glissiere1_page1').css("display", "block");
            jQuery('#div_pro_glissiere2_page1').css("display", "block");
            jQuery('#div_pro_glissiere3_page1').css("display", "block");
            jQuery('#div_pro_glissiere4_page1').css("display", "block");
            jQuery('#div_pro_glissiere1_page2').css("display", "none");
            jQuery('#div_pro_glissiere2_page2').css("display", "none");
            jQuery('#div_pro_glissiere3_page2').css("display", "none");
            jQuery('#div_pro_glissiere4_page2').css("display", "none");
        }


        function show_btn_pro_contenus_page2() {
            jQuery('#blocss').css("display", "none");
            jQuery('#div_pro_titre_infos').css("display", "none");
            jQuery('#div_pro_titre_home').css("display", "none");
            jQuery('#div_pro_titre_page1').css("display", "none");
            jQuery('#div_pro_titre_page2').css("display", "block");

            jQuery('#div_pro_logo').css("display", "none");
            jQuery('#div_pro_fondecran').css("display", "none");
            jQuery('#div_pro_horaires').css("display", "none");
            jQuery('#div_pro_pagepresentation').css("display", "none");
            jQuery('#div_pro_image_presentation').css("display", "none");
            jQuery('#div_pro_entete_banniere').css("display", "none");
            jQuery('#div_pro_geolocalisation').css("display", "none");
            jQuery('#div_pro_page1').css("display", "none");
            jQuery('#div_pro_page2').css("display", "block");
            jQuery('#div_pro_multimedia').css("display", "none");
            jQuery('#div_pro_glissiere1_home').css("display", "none");
            jQuery('#div_pro_glissiere2_home').css("display", "none");
            jQuery('#div_pro_glissiere3_home').css("display", "none");
            jQuery('#div_pro_glissiere4_home').css("display", "none");
            jQuery('#div_pro_glissiere1_page1').css("display", "none");
            jQuery('#div_pro_glissiere2_page1').css("display", "none");
            jQuery('#div_pro_glissiere3_page1').css("display", "none");
            jQuery('#div_pro_glissiere4_page1').css("display", "none");
            jQuery('#div_pro_glissiere1_page2').css("display", "block");
            jQuery('#div_pro_glissiere2_page2').css("display", "block");
            jQuery('#div_pro_glissiere3_page2').css("display", "block");
            jQuery('#div_pro_glissiere4_page2').css("display", "block");
        }


        function show_btn_pro_contenus_design() {
            jQuery('#blocss').css("display", "none");
            jQuery('#div_pro_titre_infos').css("display", "block");
            jQuery('#div_pro_titre_home').css("display", "none");
            jQuery('#div_pro_titre_page1').css("display", "none");
            jQuery('#div_pro_titre_page2').css("display", "none");

            jQuery('#div_pro_logo').css("display", "block");
            jQuery('#div_pro_fondecran').css("display", "block");
            jQuery('#div_pro_horaires').css("display", "block");
            jQuery('#div_pro_pagepresentation').css("display", "none");
            jQuery('#div_pro_image_presentation').css("display", "none");
            jQuery('#div_pro_entete_banniere').css("display", "block");
            jQuery('#div_pro_geolocalisation').css("display", "block");
            jQuery('#div_pro_page1').css("display", "none");
            jQuery('#div_pro_page2').css("display", "none");
            jQuery('#div_pro_multimedia').css("display", "block");
            jQuery('#div_pro_glissiere1_home').css("display", "none");
            jQuery('#div_pro_glissiere2_home').css("display", "none");
            jQuery('#div_pro_glissiere3_home').css("display", "none");
            jQuery('#div_pro_glissiere4_home').css("display", "none");
            jQuery('#div_pro_glissiere1_page1').css("display", "none");
            jQuery('#div_pro_glissiere2_page1').css("display", "none");
            jQuery('#div_pro_glissiere3_page1').css("display", "none");
            jQuery('#div_pro_glissiere4_page1').css("display", "none");
            jQuery('#div_pro_glissiere1_page2').css("display", "none");
            jQuery('#div_pro_glissiere2_page2').css("display", "none");
            jQuery('#div_pro_glissiere3_page2').css("display", "none");
            jQuery('#div_pro_glissiere4_page2').css("display", "none");
        }


    </script>

    <script type="text/javascript">
        function sub_link_custom1() {
            var value_link = document.getElementById('htt').value + document.getElementById('link_htt').value;
            $("#link_htt").val(value_link);

        }

        function sub_link_custom2() {
            var value_link = document.getElementById('htt2').value + document.getElementById('link_htt2').value;
            $("#link_htt2").val(value_link);

        }
    </script>

    <style type="text/css">
        .btn-info,
        .btn-info:hover,
        .btn-info:active,
        .btn-info:visited,
        .btn-info:focus {
            background-color: #3E74EF;
            border-color: #3E74EF;
        }
        .btn-info {
            background-image: -webkit-linear-gradient(top, #2aabd2 0%, #3E74EF 100%);
            background-image: -o-linear-gradient(top, #2aabd2 0%, #3E74EF 100%);
            background-image: -webkit-gradient(linear, left top, left bottom, from(#2aabd2), to(#3E74EF));
            background-image: linear-gradient(to bottom, #2aabd2 0%, #3E74EF 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff5bc0de', endColorstr='#ff2aabd2', GradientType=0);
            filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
            background-repeat: repeat-x;
            border-color: #3E74EF;
        }
        .btn-danger {
            background-image: -webkit-linear-gradient(top, #c12e2a 0%, #FF0000 100%);
            background-image: -o-linear-gradient(top, #c12e2a 0%, #FF0000 100%);
            background-image: -webkit-gradient(linear, left top, left bottom, from(#c12e2a), to(#FF0000));
            background-image: linear-gradient(to bottom, #c12e2a 0%, #FF0000 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffd9534f', endColorstr='#ffc12e2a', GradientType=0);
            filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
            background-repeat: repeat-x;
            border-color: #FF0000;
        }
        .btn-danger,
        .btn-danger:hover,
        .btn-danger:active,
        .btn-danger:visited,
        .btn-danger:focus {
            background-color: #FF0000;
            border-color: #FF0000;
        }
        .link_button {
            background-color: #006699;
            border: 2px solid #003366;
            border-radius: 8px;
            color: #ffffff;
            font-size: 12px;
            padding: 10px 15px;
        }

        .link_button:hover {
            text-decoration: none;
            color: #999999;
        }

        .stl_long_input_platinum {
            width: 413px;
        }

        .stl_long_input_platinum_td {
            width: 180px;
            height: 30px;
        }

        .div_stl_long_platinum {
            background-color: #3653A3;
            color: #FFFFFF;
            font-family: "Arial", sans-serif;
            font-size: 13px;
            font-weight: 700;
            line-height: 1.23em;
            height: 40px;
            padding-top: 12px;
            padding-left: 20px;
            margin-top: 12px;
            margin-bottom: 15px;
        }

        .div_error_taille_3_4 {
            color: #F00;
        }

        .FieldError {
            color: #FF0000;
            font-weight: bold;
        }

        <?php if ($objGlissiere->isActive_presentation_1 ==0) {?>
        #gl1_tittle, #gl1_tittle2, #gl1_content, #gl1_content2, #gl1_img, #gl1_img2, #btn_gli1, #btn_gli2, #alllink, #alllink2 {
            display: none;
        }

        <?php }elseif ($objGlissiere->isActive_presentation_1 ==1)  {?>
        #gl1_tittle, #gl1_content {
            display: block;
        }

        <?php } ?>
        <?php if ($objGlissiere->is_activ_btn_glissiere1_champ1==1){?>
        #alllink {
            display: block;
        }

        <?php }elseif ($objGlissiere->is_activ_btn_glissiere1_champ1==0) {?>
        #alllink {
            display: none;
        }

        <?php } ?>

        <?php if ($objGlissiere->is_activ_btn_glissiere1_champ2==1 AND $objGlissiere->is_activ_btn_glissiere1_champ1==1){?>
        #alllink2 {
            display: block;
        }

        <?php }elseif ($objGlissiere->presentation_1_contenu2=='') {?>
        #alllink2, #gl1_content2, #gl1_tittle2, #gl1_img2, #btn_gli2 {
            display: none;
        }

        <?php } ?>

        <?php if ($objGlissiere->presentation_1_contenu2 !='' OR $objGlissiere->is_activ_btn_glissiere1_champ1==1){?>
        #dispnbgli {
            display: inline;
        }

        <?php }elseif ($objGlissiere->is_activ_btn_glissiere1_champ1==0) {?>
        #dispnbgli {
            display: none;
        }

        <?php } ?>

        #gl2_tittle {
            display: none;
        }

        #gl2_content, #gl2_content2 {
            display: none;
        }

        .img_tab, .img_tab2 {
            margin-top: 30px;
            margin-bottom: 30px;
        }

        #btn_opt {
            display: none;
        }

        <?php if (isset($objGlissiere->is_activ_btn_bloc1) AND $objGlissiere->is_activ_btn_bloc1==0  OR $objGlissiere->is_activ_btn_bloc2==''   OR $objGlissiere->is_activ_btn_bloc2=null) { ?>
        #link_bloc1 {
            display: none;
        }

        <?php  }elseif (isset($objGlissiere->is_activ_btn_bloc1) AND $objGlissiere->is_activ_btn_bloc1==1){ ?>
        #link_bloc1 {
            display: block;
        }

        <?php } ?>

        <?php if (isset($objGlissiere->is_activ_btn_bloc2) AND $objGlissiere->is_activ_btn_bloc2==0 OR $objGlissiere->is_activ_btn_bloc2==''   OR $objGlissiere->is_activ_btn_bloc2=null OR $objGlissiere->is_activ_btn_bloc2='0') { ?>
        #link_bloc2 {
            display: none;
        }

        <?php  }elseif (isset($objGlissiere->is_activ_btn_bloc2) AND $objGlissiere->is_activ_btn_bloc2==1 OR $objGlissiere->is_activ_btn_bloc2=='1' ){ ?>
        #link_bloc2 {
            display: block;
        }

        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc==0){ ?>
        #bloc1, #bloc2, #fidelity_config, #bonplan_config {
            display: none;
        }

        <?php }elseif (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc==1){ ?>
        #bloc2, #fidelity_config, #bonplan_config {
            display: none;
        }

        #bloc1 {
            display: block;
        }

        <?php }elseif (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc==2){ ?>
        #bloc1, #bloc2, #fidelity_config {
            display: block;
        }

        <?php } ?>
        <?php if (isset($objGlissiere->is_activ_bloc) AND $objGlissiere->is_activ_bloc == 0 OR $objGlissiere->is_activ_bloc == null OR $objGlissiere->is_activ_bloc =='' ){?>
        #type_bloc_choose {
            display: none;
        }

        #bloc1, #bloc2, #fidelity_config, #bonplan_config {
            display: block;
        }

        <?php }elseif (isset($objGlissiere->is_activ_bloc) AND $objGlissiere->is_activ_bloc == 1 ){ ?>
        #type_bloc_choose {
            display: block;
        }

        <?php } ?>
        <?php if (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc =='1bp' AND $objGlissiere->is_activ_bloc == 1){ ?>
        #bonplan_config {
            display: block;
        }

        #bloc1, #bloc2, #fidelity_config {
            display: none;
        }

        <?php }else{ ?>
        #bonplan_config {
            display: none;
        }

        <?php } ?>

        <?php if (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc =='1fd' AND $objGlissiere->is_activ_bloc == 1){ ?>
        #fidelity_config {
            display: block;
        }

        #bloc1, #bloc2, bonplan_config {
            display: none;
        }

        <?php }else{ ?>
        #fidelity_config {
            display: none;
        }

        <?php } ?>
        <?php if (isset($objGlissiere->is_activ_btn_fd) AND $objGlissiere->is_activ_btn_fd== 0 ){ ?>

        #link_fd {
            display: none;
        }

        <?php }elseif (isset($objGlissiere->is_activ_btn_fd) AND $objGlissiere->is_activ_btn_fd== 1 ){ ?>
        #link_fd {
            display: block;
        }

        <?php } ?>

        <?php if (isset($objGlissiere->is_activ_btn_bp) AND $objGlissiere->is_activ_btn_bp== 0 ){ ?>

        #link_bp {
            display: none;
        }

        <?php }elseif (isset($objGlissiere->is_activ_btn_bp) AND $objGlissiere->is_activ_btn_bp== 1 ){ ?>
        #link_bp {
            display: block;
        }

        <?php } ?>

    </style>


    <div style="text-align:left; height:200px;">


        <div class="col-lg-12" style="display:table;">
            <div class="col-lg-12" style="padding:0 !important; text-align:center; padding-bottom:30px; display: none;">
                <img
                        src="<?php echo GetImagePath("privicarte/"); ?>/img_form_data.png"/></div>
            <div class="col-lg-12"
                 style="padding:0 !important; font-family: Arial; font-size:20px; text-align:center; font-weight:bold;">
                <div style="padding: 15px;">
                    <?php if ($page_data == "contenus") { ?>
                        Mon contenu
                    <?php } else { ?>
                        Mes coordonnées<br/>Mon abonnement
                    <?php } ?>
                </div>
                <div><a href="<?php echo site_url("front/utilisateur/contenupro"); ?>" class="btn btn-primary col-lg-12"
                        style="text-decoration:none;">Retour menu</a></div>
            </div>
        </div>


        <div id="divErrorDisplay">
            <div class="col-lg-12" style="padding-top:30px; text-align:center;">
                <?php if (isset($mssg) && $mssg == 1) { ?>
                    <div style="font-style:italic;color:#0C0;">Les modifications ont &eacute;t&eacute; enregistr&eacute;es</div>
                    <br/><?php } ?>
            </div>
            <div style=" padding-left:120px; padding-top:30px;">
                <?php if (isset($mssg) && $mssg != 1 && $mssg != "") { ?>
                    <div style="font-style:italic;color:#F00;">Une erreur est constatée, Veuillez vérifier la conformité
                        de vos données
                    </div><br/><?php } ?>
            </div>
        </div>

        <?php //if(isset($objCommercant)) {  ?>
        <!--<table  style="text-align:center;">
				<?php // if(isset($show_annonce_btn) && ($show_annonce_btn==1)) { ?>
				<tr>
					<td align="left"><a style="text-decoration:none;" href="<?php // echo site_url("front/annonce/listeMesAnnonces/$objCommercant->IdCommercant");?>"><input type ="button" style="width:200px;"id="btnannonce" value="gestions des annonces" onclick="document.location='<?php // echo site_url("front/annonce/listeMesAnnonces/$objCommercant->IdCommercant");?>';"/>  </a></td>
				</tr>
				<?php // } ?>
				<?php // if(isset($show_bonplan_btn) && ($show_bonplan_btn==1)) { ?>
				 <tr>
					<td align="left"><a  href="<?php // //echo site_url("front/bonplan/listeMesBonplans/$objCommercant->IdCommercant");?>"><input type ="button" style="width:200px;" id="btnbonplan" value="gestions des bons plans" onclick="document.location='<?php // echo site_url("front/bonplan/listeMesBonplans/$objCommercant->IdCommercant");?>';"/> </a></td>
				</tr>
				<?php // //} ?>
			</table>-->
        <?php //} ?>





        <?php if (isset($user_groups) && $user_groups->id == '5') { ?>


            <?php if (isset($page_data) && $page_data == 'coordonnees') echo '<div style="display:none;">'; ?><!--START CONTAINER MON CONTENU-->
            <div class="col-lg-12" style="margin-top:20px; margin-bottom:20px;">
                <a href="javascript:void(0);" class="col-lg-3 btn btn-primary"
                   onclick="javascript:show_btn_pro_contenus_design();">Design</a>
                <a href="javascript:void(0);" class="col-lg-3 btn btn-primary"
                   onclick="javascript:show_btn_pro_contenus_home();">Page d'accueil</a>
                <a href="javascript:void(0);" class="col-lg-3 btn btn-primary"
                   onclick="javascript:show_btn_pro_contenus_page1();">Page 1</a>
                <a href="javascript:void(0);" class="col-lg-3 btn btn-primary"
                   onclick="javascript:show_btn_pro_contenus_page2();">Page 2</a>
            </div>
            <?php if (isset($page_data) && $page_data == 'coordonnees') echo '</div>'; ?><!--END CONTAINER MON CONTENU-->


            <div class="col-lg-12" style="color:#000000; font-size:20px; text-align:center; font-weight:bold;">
                <div id="div_pro_titre_infos"
                     <?php if (isset($page_data) && $page_data == 'coordonnees'){ ?>style="display:none;"<?php } ?>>
                    Design & infos
                </div>
                <div id="div_pro_titre_home" style="display:none;">Page d'accueil</div>
                <div id="div_pro_titre_page1" style="display:none;">Page 1</div>
                <div id="div_pro_titre_page2" style="display:none;">Page 2</div>
            </div>

        <?php } ?>


        <div class="col-lg-12">

            <form name="frmInscriptionProfessionnel" id="frmInscriptionProfessionnel"
                  action="<?php echo site_url("front/professionnels/modifier"); ?>" method="POST"
                  enctype="multipart/form-data">


                <input type="hidden" name="page_data" id="page_data" value="<?php echo $page_data; ?>"/>


                <?php if (isset($page_data) && $page_data == 'coordonnees') echo '<div style="display:none;">'; ?>



                <?php if (isset($objAbonnementCommercant) && $user_groups->id == '5') { ?>
                    <div id="div_pro_fondecran">
                        <div class="div_stl_long_platinum">Image page et arrière plan (Dimensions 2024 pixels x 1100
                            pixels)
                        </div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">

                            <tr>
                                <td class="stl_long_input_platinum_td" style="vertical-align: top;">
                                    <label style="font-size:14px;">Importer votre image : </label>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <?php
                                            $path_img_gallery_bg = 'application/resources/front/photoCommercant/imagesbank/' . $objCommercant->user_ionauth_id . '/bg/';
                                            $path_img_gallery_bg_sample = 'application/resources/front/photoCommercant/imagesbank/bg_sample/';
                                            $path_img_gallery_old = 'application/resources/front/photoCommercant/images/';
                                            ?>

                                            <div id="Articlebackground_image_container">

                                                <a href='javascript:void(0);' title="Photo1" class="btn btn-info"
                                                    <?php if (empty($objCommercant->background_image) || $objCommercant->background_image == "") { ?>
                                                        onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-bg-background_image"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                                    <?php } ?>
                                                   id="Articlebackground_link">Ajouter une image</a>

                                                <a href="javascript:void(0);" class="btn btn-danger"
                                                    <?php if (!empty($objCommercant->background_image)) { ?>
                                                        onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','background_image');"
                                                    <?php } ?>
                                                >Supprimer</a>


                                            </div>

                                            <script type="text/javascript">
                                                jQuery(document).ready(function () {
                                                    jQuery("#Societe_bg_default_image_check").click(function () {
                                                        if (jQuery(this).is(':checked')) {
                                                            jQuery("#Societe_bg_default_image").val("1");
                                                        } else {
                                                            jQuery("#Societe_bg_default_image").val("0");
                                                        }
                                                    });
                                                    jQuery("#Societe_bg_default_color_container_check").click(function () {
                                                        if (jQuery(this).is(':checked')) {
                                                            jQuery("#Societe_bg_default_color_container").val("1");
                                                        } else {
                                                            jQuery("#Societe_bg_default_color_container").val("0");
                                                        }
                                                    });
                                                });
                                            </script>

                                            <div class="col-xs-12 padding0"><img
                                                        src="<?php echo GetImagePath("privicarte/"); ?>/bg_ico.png" alt="bg_ico"
                                                        style="width: 200px;"/></div>
                                            <div style="width:100%; margin-top:30px;">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" name="Societe_bg_default_image_check"
                                                                   <?php if ($objCommercant->bg_default_image == '1') { ?>checked="checked"<?php } ?>
                                                                   id="Societe_bg_default_image_check"/>
                                                            <input type="hidden" name="Societe[bg_default_image]"
                                                                   id="Societe_bg_default_image"
                                                                   value="<?php echo $objCommercant->bg_default_image; ?>"/>
                                                        </td>
                                                        <td>Fond arrière plan Sortez par d&eacute;faut</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox"
                                                                   name="Societe_bg_default_color_container_check"
                                                                   <?php if ($objCommercant->bg_default_color_container == '1') { ?>checked="checked"<?php } ?>
                                                                   id="Societe_bg_default_color_container_check"/>
                                                            <input type="hidden" name="Societe[bg_default_color_container]"
                                                                   id="Societe_bg_default_color_container"
                                                                   value="<?php echo $objCommercant->bg_default_color_container; ?>"/>
                                                        </td>
                                                        <td>Si coché couleur de la page : gris 225</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <?php
                                            if (file_exists($path_img_gallery_bg . $objCommercant->background_image) && isset($objCommercant->background_image) && $objCommercant->background_image != "") {
                                                echo '<img  src="' . base_url() . $path_img_gallery_bg . $objCommercant->background_image . '" width="200"/>';
                                            } else if (file_exists($path_img_gallery_bg_sample . $objCommercant->background_image) && isset($objCommercant->background_image) && $objCommercant->background_image != "") {
                                                echo '<img  src="' . base_url() . $path_img_gallery_bg_sample . $objCommercant->background_image . '" width="200"/>';
                                            } else if (file_exists($path_img_gallery_old . $objCommercant->background_image) && isset($objCommercant->background_image) && $objCommercant->background_image != "") {
                                                echo '<img  src="' . base_url() . $path_img_gallery_old . $objCommercant->background_image . '" width="200"/>';
                                            }
                                            ?>
                                        </div>
                                    </div>


                                </td>
                            </tr>
                        </table>
                    </div>
                <?php } ?>



                <?php if (isset($objAbonnementCommercant) && ($user_groups->id == '5' || $user_groups->id == '4')) { ?>
                    <div id="div_pro_entete_banniere"
                         <?php if (isset($objAbonnementCommercant) && $user_groups->id == '4') { ?>style="background-color:#E1E1E1;"<?php } ?>>

                        <div class="div_stl_long_platinum">Image bannière page de présentation (dimentions 1024 pixels x
                            350 pixels)
                        </div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">

                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label style="font-size:14px;">Intégration d’une image bannière : </label>
                                </td>
                                <td>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div id="Articlebandeau_top_container">
                                                <?php
                                                $path_img_gallery_bn = 'application/resources/front/photoCommercant/imagesbank/' . $objCommercant->user_ionauth_id . '/bn/';
                                                $path_img_gallery_bn_sample = 'application/resources/front/photoCommercant/imagesbank/bn_sample/';
                                                $path_img_gallery_old = 'application/resources/front/photoCommercant/images/';
                                                ?>


                                                <a href='javascript:void(0);' title="Photo1" class="btn btn-info"
                                                    <?php if (empty($objCommercant->bandeau_top) || $objCommercant->bandeau_top == "") { ?>
                                                        onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-bn-bandeau_top"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                                    <?php } ?>
                                                   id="Articlebandeau_link">Ajouter une image</a>

                                                <a href="javascript:void(0);" class="btn btn-danger"
                                                    <?php if (!empty($objCommercant->bandeau_top) && $objCommercant->bandeau_top != "") { ?>
                                                        onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','bandeau_top');"
                                                    <?php } ?>
                                                >Supprimer</a>

                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <?php
                                            if (file_exists($path_img_gallery_bn . $objCommercant->bandeau_top) && $objCommercant->bandeau_top!="") {
                                                echo '<img  src="' . base_url() . $path_img_gallery_bn . $objCommercant->bandeau_top . '" width="200"/>';
                                            } else if (file_exists($path_img_gallery_bn_sample . $objCommercant->bandeau_top) && $objCommercant->bandeau_top!="") {
                                                echo '<img  src="' . base_url() . $path_img_gallery_bn_sample . $objCommercant->bandeau_top . '" width="200"/>';
                                            } else if (file_exists($path_img_gallery_old . $objCommercant->bandeau_top) && $objCommercant->bandeau_top!="") {
                                                echo '<img  src="' . base_url() . $path_img_gallery_old . $objCommercant->bandeau_top . '" width="200"/>';
                                            }
                                            ?>
                                        </div>
                                    </div>

                                </td>
                            </tr>

                            <tr>
                                <td class="stl_long_input_platinum_td">&nbsp;
                                </td>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                           style="margin-top:5px;">
                                        <tr>
                                            <td style="vertical-align: top;"><img
                                                        src="<?php echo GetImagePath("privicarte/"); ?>/bandeau_top_icon.png"
                                                        alt="bandeau_top_icon" width="200"/></td>
                                            <td style="vertical-align: top; padding: 5px">

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <script type="text/javascript">
                                                    jQuery(document).ready(function () {
                                                        jQuery("#bandeau_top_default_Societe_checker").click(function () {
                                                            if (jQuery(this).is(':checked')) {
                                                                jQuery("#bandeau_top_default").val("1");
                                                            } else {
                                                                jQuery("#bandeau_top_default").val("0");
                                                            }
                                                        });
                                                    });
                                                </script>
                                                <input type="checkbox" name="Societebandeau_top_default_checker"
                                                       id="bandeau_top_default_Societe_checker" value=""
                                                       <?php if (isset($objCommercant->bandeau_top_default) && $objCommercant->bandeau_top_default == "1") { ?>checked="checked"<?php } ?>/>
                                                <input type="hidden" name="Societe[bandeau_top_default]"
                                                       id="bandeau_top_default"
                                                       value="<?php if (isset($objCommercant->bandeau_top_default) && $objCommercant->bandeau_top_default == "1") echo "1"; else echo "0"; ?>"/>
                                                Image par d&eacute;faut
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <?php if (isset($objAbonnementCommercant) && $user_groups->id == '5') { ?>
                                <tr>
                                    <td class="stl_long_input_platinum_td">
                                        <label style="font-size:14px;">et définir une couleur (Texte ou fond) : </label>
                                    </td>
                                    <td>

                                        <link rel="stylesheet"
                                              href="<?php echo GetJsPath("front/"); ?>/colorPicker/colorpicker.css"
                                              type="text/css"/>
                                        <!--<link rel="stylesheet" media="screen" type="text/css" href="<?php // echo GetJsPath("front/") ; ?>/colorPicker/layout.css" />-->
                                        <style type="text/css">
                                            #colorSelector2 {
                                                position: relative;
                                                width: 36px;
                                                height: 36px;
                                                /*background: url(




                                            <?php// echo GetJsPath("front/") ; ?>     /colorPicker/select2.png);*/
                                            }

                                            #colorSelector2 div {
                                                position: relative;
                                                width: 28px;
                                                height: 28px;
                                                background: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/select2.png) center;
                                            }

                                            #colorpickerHolder2 {
                                                width: 356px;
                                                height: 0;
                                                overflow: hidden;
                                                position: relative;
                                            }

                                            #colorpickerHolder2 .colorpicker {
                                                background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_background.png);
                                                position: relative;
                                                bottom: 0;
                                                left: 0;
                                            }

                                            #colorpickerHolder2 .colorpicker_hue div {
                                                background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_indic.gif);
                                            }

                                            #colorpickerHolder2 .colorpicker_hex {
                                                background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_hex.png);
                                            }

                                            #colorpickerHolder2 .colorpicker_rgb_r {
                                                background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_rgb_r.png);
                                            }

                                            #colorpickerHolder2 .colorpicker_rgb_g {
                                                background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_rgb_g.png);
                                            }

                                            #colorpickerHolder2 .colorpicker_rgb_b {
                                                background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_rgb_b.png);
                                            }

                                            #colorpickerHolder2 .colorpicker_hsb_s {
                                                background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_hsb_s.png);
                                                display: none;
                                            }

                                            #colorpickerHolder2 .colorpicker_hsb_h {
                                                background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_hsb_h.png);
                                                display: none;
                                            }

                                            #colorpickerHolder2 .colorpicker_hsb_b {
                                                background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_hsb_b.png);
                                                display: none;
                                            }

                                            #colorpickerHolder2 .colorpicker_submit {
                                                background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_submit.png);
                                            }

                                            #colorpickerHolder2 .colorpicker input {
                                                color: #778398;
                                            }

                                            #customWidget {
                                                position: relative;
                                                height: 36px;
                                            }

                                        </style>
                                        <script type="text/javascript"
                                                src="<?php echo GetJsPath("front/"); ?>/colorPicker/colorpicker.js"></script>
                                        <script type="text/javascript"
                                                src="<?php echo GetJsPath("front/"); ?>/colorPicker/eye.js"></script>
                                        <script type="text/javascript"
                                                src="<?php echo GetJsPath("front/"); ?>/colorPicker/layout.js?ver=1.0.2"></script>

                                        <input type="hidden" name="Societe[bandeau_color]" id="bandeau_colorSociete"
                                               value="<?php if (isset($objCommercant->bandeau_color)) echo $objCommercant->bandeau_color; else echo '#3653A3'; ?>"/>
                                        <div id="colorSelector2">
                                            <div style="background-color: <?php if (isset($objCommercant->bandeau_color)) echo $objCommercant->bandeau_color; else echo '#3653A3'; ?>"></div>
                                        </div>
                                        <div id="colorpickerHolder2">
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                <?php } ?>




                <?php if (isset($user_groups) && $user_groups->id == '5') { ?>
                    <div id="div_pro_logo">
                        <div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px;">Votre Logo</div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">
                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    Int&eacute;gration de votre logo
                                    <p style='font-family:"Arial",sans-serif;font-size: 9px;line-height: 1.27em;'>(la
                                        largeur ne doit pas dépasser 270 pixels, fond transparent)</p>
                                </td>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>

                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div id="ArticleLogo_container">
                                                            <?php
                                                            $path_img_gallery_lg = 'application/resources/front/photoCommercant/imagesbank/' . $objCommercant->user_ionauth_id . '/lg/';
                                                            $path_img_gallery_old = 'application/resources/front/photoCommercant/images/';
                                                            ?>
                                                            <a href='javascript:void(0);' title="Photo1"
                                                                <?php if (empty($objCommercant->Logo) || empty($objCommercant->Logo=="")) { ?>
                                                                    onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-lg-Logo"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                                                <?php } ?>
                                                               class="btn btn-info" id="ArticleLogo_link">Ajouter une image</a>
                                                            <a href="javascript:void(0);" class="btn btn-danger"
                                                                <?php if (!empty($objCommercant->Logo) && empty($objCommercant->Logo!="")) { ?>
                                                                    onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Logo');"
                                                                <?php } ?>
                                                            >Supprimer</a>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <?php
                                                        if (file_exists($path_img_gallery_lg . $objCommercant->Logo) && $objCommercant->Logo!="") {
                                                            echo '<img  src="' . base_url() . $path_img_gallery_lg . $objCommercant->Logo . '" width="200"/>';
                                                        } else if (file_exists($path_img_gallery_old . $objCommercant->Logo) && $objCommercant->Logo!="") {
                                                            echo '<img  src="' . base_url() . $path_img_gallery_old . $objCommercant->Logo . '" width="200"/>';
                                                        }
                                                        ?>
                                                    </div>
                                                </div>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <script type="text/javascript">
                                var maxLength = 65;

                                function fn_titre_entete() {
                                    var length = jQuery('#titre_entete').val().length;
                                    var length = maxLength - length;
                                    jQuery('#chars_titre_entete').text(length);
                                }
                            </script>

                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label class="label">D&eacute;signation<br/>de votre activit&eacute; : </label>
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                            <textarea name="Societe[titre_entete]" id="titre_entete"
                                                      class="stl_long_input_platinum form-control" maxlength="65"
                                                      onkeyup="javascript:fn_titre_entete();"
                                                      style="height:30px;margin-top:15px;"><?php if (isset($objCommercant->titre_entete)) echo htmlspecialchars($objCommercant->titre_entete); ?></textarea>
                                                        <br/><span id="chars_titre_entete">65</span> caract&eacute;res
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <?php if ($user_groups->id == '5') { ?>
                                <tr>
                                    <td>
                                        <label for="place_logo">Position du logo</label>
                                    </td>
                                    <td>
                                        <select class="form-control" name="Glissiere[place_logo]">
                                            <option <?php if ($objGlissiere->place_logo == 'g') {
                                                echo 'selected';
                                            } ?> value="g">Placé à gauche
                                            </option>
                                            <option <?php if ($objGlissiere->place_logo == 'c') {
                                                echo 'selected';
                                            } ?> value="c">Placé au centre
                                            </option>
                                            <option <?php if ($objGlissiere->place_logo == 'd') {
                                                echo 'selected';
                                            } ?> value="d">Placé à droite
                                            </option>
                                        </select>
                                    </td>
                                </tr>
                            <?php } ?>

                        </table>
                    </div>
                <?php } ?>



                <?php if (isset($page_data) && $page_data == 'coordonnees') echo '</div>'; ?>




                <?php if (isset($page_data) && $page_data == 'contenus') echo '<div style="display:none;">'; ?>
                <div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px; margin-bottom:0 !important;">
                    Les coordonnées de votre établissement
                </div>
                <?php if (isset($page_data) && $page_data == 'contenus') echo '</div>'; ?>



                <?php if (isset($page_data) && $page_data == 'contenus') echo '<div style="display:none;">'; ?>

                <table width="100%" border="0" cellspacing="0" cellpadding="0"
                       style="padding-left:10px; background-color:#E1E1E1;">


                    <?php if (isset($objAbonnementCommercant) && $user_groups->id == '4') { ?>

                        <script type="text/javascript">
                            var maxLength = 65;

                            function fn_titre_entete() {
                                var length = jQuery('#titre_entete').val().length;
                                var length = maxLength - length;
                                jQuery('#chars_titre_entete').text(length);
                            }
                        </script>

                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">D&eacute;signation<br/>de votre activit&eacute; : </label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                <textarea name="Societe[titre_entete]" id="titre_entete" class="stl_long_input_platinum form-control"
                                                  maxlength="65" onkeyup="javascript:fn_titre_entete();"
                                                  style="height:30px;margin-top:15px;"><?php if (isset($objCommercant->titre_entete)) echo htmlspecialchars($objCommercant->titre_entete); ?></textarea>
                                                    <br/><span id="chars_titre_entete">65</span> caract&eacute;res
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                    <?php } ?>


                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Rubrique : </label>
                        </td>
                        <td>
                            <select name="AssCommercantRubrique[IdRubrique]" onchange="javascript:listeSousRubrique();"
                                    id="RubriqueSociete" class="stl_long_input_platinum">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if (sizeof($colRubriques)) { ?>
                                    <?php foreach ($colRubriques as $objRubrique) { ?>
                                        <option
                                            <?php if (isset($objAssCommercantRubrique[0]->IdRubrique) && $objAssCommercantRubrique[0]->IdRubrique == $objRubrique->IdRubrique) { ?>selected="selected"<?php } ?>
                                            value="<?php echo $objRubrique->IdRubrique; ?>"><?php echo $objRubrique->Nom; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr id='trReponseRub'>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Sous-rubrique : </label>
                        </td>
                        <td>

                            <?php if (isset($objAssCommercantRubrique[0]->IdRubrique) && $objAssCommercantRubrique[0]->IdRubrique != 0 && $objAssCommercantRubrique[0]->IdRubrique != "0" && $objAssCommercantRubrique[0]->IdRubrique != "" && $objAssCommercantRubrique[0]->IdRubrique != NULL) { ?>

                                <?php
                                $this->load->model("SousRubrique");
                                $colSousRubriques = $this->SousRubrique->GetByRubrique($objAssCommercantRubrique[0]->IdRubrique);
                                ?>

                                <select name="AssCommercantSousRubrique[IdSousRubrique]" id="SousRubriqueSociete"
                                        class="stl_long_input_platinum">
                                    <option value="0">-- Veuillez choisir --</option>
                                    <?php if (sizeof($colSousRubriques)) { ?>
                                        <?php foreach ($colSousRubriques as $objSousRubrique) { ?>
                                            <option
                                                <?php if (isset($objAssCommercantSousRubrique[0]->IdSousRubrique) && $objAssCommercantSousRubrique[0]->IdSousRubrique == $objSousRubrique->IdSousRubrique) { ?>selected="selected"<?php } ?>
                                                value="<?php echo $objSousRubrique->IdSousRubrique; ?>"><?php echo $objSousRubrique->Nom; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>

                            <?php } else { ?>

                                <select name="AssCommercantSousRubrique[IdSousRubrique]" id="SousRubriqueSociete"
                                        class="stl_long_input_platinum">
                                    <option value="0">-- Veuillez choisir --</option>
                                    <?php if (sizeof($colSousRubriques)) { ?>
                                        <?php foreach ($colSousRubriques as $objSousRubrique) { ?>
                                            <option
                                                <?php if (isset($objAssCommercantSousRubrique[0]->IdSousRubrique) && $objAssCommercantSousRubrique[0]->IdSousRubrique == $objSousRubrique->IdSousRubrique) { ?>selected="selected"<?php } ?>
                                                value="<?php echo $objSousRubrique->IdSousRubrique; ?>"><?php echo $objSousRubrique->Nom; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>

                            <?php } ?>
                        </td>
                    </tr>


                    <script type="text/javascript">
                        var maxLength_st = 35;

                        function fn_NomSociete() {
                            var length = jQuery('#NomSociete').val().length;
                            var length = maxLength_st - length;
                            jQuery('#chars_NomSociete').text(length);
                        }
                    </script>

                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Nom de la soci&eacute;t&eacute; : </label>
                        </td>
                        <td>
                            <textarea name="Societe[NomSociete]" id="NomSociete" class="stl_long_input_platinum"
                                      maxlength="35" onkeyup="javascript:fn_NomSociete();"
                                      style="height:30px;margin-top:15px;"><?php if (isset($objCommercant->NomSociete)) echo htmlspecialchars($objCommercant->NomSociete); ?></textarea>
                            <br/><span id="chars_NomSociete">35</span> caract&eacute;res
                        </td>
                    </tr>


                    <?php if (isset($user_groups) && ($user_groups->id == '5' || $user_groups->id == '4')) { ?>
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Statut : </label>
                            </td>
                            <td>
                                <input type="text" name="Societe[statut]" id="statutSociete"
                                       value="<?php if (isset($objCommercant->statut)) echo htmlspecialchars($objCommercant->statut); ?>"
                                       class="stl_long_input_platinum"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Capital : </label>
                            </td>
                            <td>
                                <input type="text" name="Societe[capital]" id="capitalSociete"
                                       value="<?php if (isset($objCommercant->capital)) echo htmlspecialchars($objCommercant->capital); ?>"
                                       class="stl_long_input_platinum"/>
                            </td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Siret : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[Siret]" id="SiretSociete"
                                   value="<?php if (isset($objCommercant->Siret)) echo htmlspecialchars($objCommercant->Siret); ?>"
                                   class="stl_long_input_platinum"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Code APE : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[code_ape]" id="CodeApeSociete"
                                   value="<?php if (isset($objCommercant->code_ape)) echo htmlspecialchars($objCommercant->code_ape); ?>"
                                   class="stl_long_input_platinum"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Adresse de l'&eacute;tablissement : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[Adresse1]" id="Adresse1Societe"
                                   value="<?php if (isset($objCommercant->Adresse1)) echo htmlspecialchars($objCommercant->Adresse1); ?>"
                                   class="stl_long_input_platinum"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Compl&eacute;ment d'Adresse : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[Adresse2]" id="Adresse2Societe"
                                   value="<?php if (isset($objCommercant->Adresse2)) echo htmlspecialchars($objCommercant->Adresse2); ?>"
                                   class="stl_long_input_platinum"/>
                        </td>
                    </tr>

                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Code postal : </label>
                        </td>
                        <td id="trReponseVille">
                            <input type="text" name="Societe[CodePostal]" id="CodePostalSociete"
                                   value="<?php if (isset($objCommercant->CodePostal)) echo htmlspecialchars($objCommercant->CodePostal); ?>"
                                   class="stl_long_input_platinum"
                                   onblur="javascript:CP_getDepartement();CP_getVille();"/>
                        </td>
                    </tr>

                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Departement : </label>
                        </td>
                        <td>
    	<span id="departementCP_container">
        <select name="Societe[departement_id]" id="departement_id" class="stl_long_input_platinum"
                onchange="javascript:CP_getVille_D_CP();">
            <option value="0">-- Choisir --</option>
            <?php if (sizeof($colDepartement)) { ?>
                <?php foreach ($colDepartement as $objDepartement) { ?>
                    <option
                        <?php if (isset($objCommercant->departement_id) && $objCommercant->departement_id == $objDepartement->departement_id) { ?>selected="selected"<?php } ?>
                        value="<?php echo $objDepartement->departement_id; ?>"><?php echo $objDepartement->departement_nom; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
        </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Ville : </label>
                        </td>
                        <td>
    	<span id="villeCP_container">
        <input type="text"
               value="<?php if (isset($objCommercant->IdVille)) echo $this->mdlville->getVilleById($objCommercant->IdVille)->Nom; ?>"
               name="IdVille_Nom_text" id="IdVille_Nom_text" disabled="disabled" style="width: 413px;"/>
        <input type="hidden" value="<?php if (isset($objCommercant->IdVille)) echo $objCommercant->IdVille; ?>"
               name="Societe[IdVille]" id="VilleSociete"/>
        </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Latitude : (ex: -18.7972)</label>
                        </td>
                        <td>
                            <input type="text" name="Societe[latitude]" id="latitudeSociete"
                                   value="<?php if (isset($objCommercant->latitude)) echo $objCommercant->latitude; ?>"
                                   class="stl_long_input_platinum"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Longitude : (ex: 47.4761)</label>
                        </td>
                        <td>
                            <input type="text" name="Societe[longitude]" id="longitudeSociete"
                                   value="<?php if (isset($objCommercant->longitude)) echo htmlspecialchars($objCommercant->longitude); ?>"
                                   class="stl_long_input_platinum"/>
                        </td>
                    </tr>


                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">T&eacute;l&eacute;phone fixe : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[TelFixe]" id="TelFixeSociete"
                                   value="<?php if (isset($objCommercant->TelFixe)) echo htmlspecialchars($objCommercant->TelFixe); ?>"
                                   class="stl_long_input_platinum"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">T&eacute;l&eacute;phone mobile : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[TelMobile]" id="TelMobileSociete"
                                   value="<?php if (isset($objCommercant->TelMobile)) echo htmlspecialchars($objCommercant->TelMobile); ?>"
                                   class="stl_long_input_platinum"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">T&eacute;l&eacute;phone Fax : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[fax]" id="faxSociete"
                                   value="<?php if (isset($objCommercant->fax)) echo htmlspecialchars($objCommercant->fax); ?>"
                                   class="stl_long_input_platinum"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Courriel : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[Email]" id="EmailSociete"
                                   value="<?php if (isset($objCommercant->Email)) echo htmlspecialchars($objCommercant->Email); ?>"
                                   class="stl_long_input_platinum"/>
                            <div class="FieldError" id="divErrorEmailSociete"></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Site internet : </label>
                        </td>
                        <td>
                            <div style="font-size:10px; margin-top:15px;">Exemple: http://www.votresite.com/</div>
                            <input type="text" name="Societe[SiteWeb]" id="SiteWeb"
                                   value="<?php if (isset($objCommercant->SiteWeb) && $objCommercant->SiteWeb != "") echo htmlspecialchars($objCommercant->SiteWeb); else echo 'http://www.'; ?>"
                                   class="stl_long_input_platinum"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">URL Espace marchand : </label>
                        </td>
                        <td>
                            <div style="font-size:10px; margin-top:15px;">Exemple: http://www.votresite.com/</div>
                            <input type="text" name="Societe[page_web_marchand]" id="SiteWeb"
                                   value="<?php if (isset($objCommercant->page_web_marchand) && $objCommercant->page_web_marchand != "") echo htmlspecialchars($objCommercant->page_web_marchand); else echo 'http://www.'; ?>"
                                   class="stl_long_input_platinum"/>
                        </td>
                    </tr>

                    <?php if (isset($user_groups) && ($user_groups->id == '5' || $user_groups->id == '4')) { ?>

                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Lien Facebook : </label>
                            </td>
                            <td>
                                <div style="font-size:10px; margin-top:15px;">Exemple:
                                    https://www.facebook.com/sortez.org
                                </div>
                                <input type="text" name="Societe[Facebook]" id="FacebookSociete"
                                       value="<?php if (isset($objCommercant->Facebook)) echo htmlspecialchars($objCommercant->Facebook); ?>"
                                       class="stl_long_input_platinum"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Lien Twitter : </label>
                            </td>
                            <td>
                                <div style="font-size:10px; margin-top:15px;">Exemple:
                                    https://www.twitter.com/sortez_org
                                </div>
                                <input type="text" name="Societe[google_plus]" id="google_plusSociete"
                                       value="<?php if (isset($objCommercant->google_plus)) echo htmlspecialchars($objCommercant->google_plus); ?>"
                                       class="stl_long_input_platinum"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Lien Google + : </label>
                            </td>
                            <td>
                                <div style="font-size:10px; margin-top:15px;">Exemple:
                                    https://plus.google.com/sortez_org
                                </div>
                                <input type="text" name="Societe[Twitter]" id="TwitterSociete"
                                       value="<?php if (isset($objCommercant->Twitter)) echo htmlspecialchars($objCommercant->Twitter); ?>"
                                       class="stl_long_input_platinum"/>
                            </td>
                        </tr>

                    <?php } ?>

                    <?php if (isset($user_groups) && $user_groups->id == '4') { ?><!--Horaires affichés en Premium-->
                    <tr>
                        <td class="stl_long_input_platinum_td" colspan="2">
                            <label class="label">Heures et jours d’ouvertures : </label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <textarea name="Societe[Horaires]"
                                      id="HorairesSociete"><?php if (isset($objCommercant->Horaires)) echo htmlspecialchars($objCommercant->Horaires); ?></textarea>
                            <script>

                                CKEDITOR.replace('HorairesSociete');

                            </script>
                        </td>
                    </tr>
                <?php } ?>

                </table>


                <?php if (isset($page_data) && $page_data == 'contenus') echo '</div>'; ?>




                <?php if (isset($page_data) && $page_data == 'contenus') echo '<div style="display:none;">'; ?>


                <div class="div_stl_long_platinum">Les coordonnées du décideur</div>

                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Civilit&eacute; : </label>
                        </td>
                        <td>
                            <select name="Societe[Civilite]" id="CiviliteResponsableSociete"
                                    class="stl_long_input_platinum">
                                <option
                                    <?php if (isset($objCommercant->Civilite) && $objCommercant->Civilite == "0") { ?>selected="selected"<?php } ?>
                                    value="0">Monsieur
                                </option>
                                <option
                                    <?php if (isset($objCommercant->Civilite) && $objCommercant->Civilite == "1") { ?>selected="selected"<?php } ?>
                                    value="1">Madame
                                </option>
                                <option
                                    <?php if (isset($objCommercant->Civilite) && $objCommercant->Civilite == "2") { ?>selected="selected"<?php } ?>
                                    value="2">Mademoiselle
                                </option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Nom : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[Nom]" id="NomResponsableSociete"
                                   value="<?php if (isset($objCommercant->Nom)) echo htmlspecialchars($objCommercant->Nom); ?>"
                                   class="stl_long_input_platinum"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Pr&eacute;nom : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[Prenom]" id="PrenomResponsableSociete"
                                   value="<?php if (isset($objCommercant->Prenom)) echo htmlspecialchars($objCommercant->Prenom); ?>"
                                   class="stl_long_input_platinum"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Fonction <span class="indication">(G&eacute;rant, Directeur ...)</span>:
                            </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[Responsabilite]" id="ResponsabiliteResponsableSociete"
                                   value="<?php if (isset($objCommercant->Responsabilite)) echo htmlspecialchars($objCommercant->Responsabilite); ?>"
                                   class="stl_long_input_platinum"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">T&eacute;l&eacute;phone direct <span class="indication">(fixe ou portable)</span>
                                : </label>
                        </td>
                        <td>
                            <input type="text" name="Societe[TelDirect]" id="TelDirectResponsableSociete"
                                   value="<?php if (isset($objCommercant->TelDirect)) echo htmlspecialchars($objCommercant->TelDirect); ?>"
                                   class="stl_long_input_platinum"/>
                        </td>
                    </tr>
                    <?php if (isset($user_groups) && ($user_groups->id == '5' || $user_groups->id == '4')) { ?>
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Email Personnel : </label>
                            </td>
                            <td>
                                <input type="text" name="Societe[Email_decideur]" id="Email_decideurSociete"
                                       value="<?php if (isset($objCommercant->Email_decideur)) echo htmlspecialchars($objCommercant->Email_decideur); ?>"
                                       class="stl_long_input_platinum"/>
                                <div class="FieldError" id="divErrorEmail_decideurSociete"></div>
                            </td>
                        </tr>
                    <?php } ?>
                </table>


                <div class="div_stl_long_platinum" style="margin-bottom:0 !important;">Votre abonnement</div>

                <table width="100%" border="0" cellspacing="0" cellpadding="0"
                       style="padding-left:10px; background-color:#E1E1E1;">

                    <tr>
                        <td class="stl_long_input_platinum_td">
                            Date de début
                            :
                        </td>
                        <td>
                            <?php if (isset($objAbonnementCommercant)) echo translate_date_to_fr($objAbonnementCommercant->DateDebut); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">Date de fin :</td>
                        <td>
                            <?php if (isset($objAbonnementCommercant)) echo translate_date_to_fr($objAbonnementCommercant->DateFin); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td"><strong>Votre abonnement : </strong></td>
                        <td>
                            <strong><?php
                                //echo $user_groups->id;
                                if (isset($user_groups)) {
                                    if ($user_groups->id == 3) echo "Basic";
                                    if ($user_groups->id == 4) echo "Premium";
                                    if ($user_groups->id == 5) echo "Platinium";
                                }
                                ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Identifiant : </label>
                        </td>
                        <td>
                            <?php if (isset($objCommercant->Login)) echo htmlspecialchars($objCommercant->Login); ?>
                            <div class="FieldError" id="divErrorLoginSociete"></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:center; padding:15px;">
                            <button class="btn btn-primary"
                                    onclick="javascript:window.location.href='<?php echo site_url("front/professionnels/mon_inscription"); ?>';return false;">
                                D&eacute;tails de votre abonnement
                            </button>
                        </td>
                    </tr>

                    <!--<tr>
    <td>
        <label>Mot de passe : </label>
    </td>
    <td>
        <input type="password" name="Societe[Password]" id="PasswordSociete" value="<?php // if(isset($objCommercant->Password)) echo htmlspecialchars($objCommercant->Password); ?>" class="stl_long_input_platinum" />
    </td>
</tr>-->

                </table>


                <?php if (isset($page_data) && $page_data == 'contenus') echo '</div>'; ?>


                <?php if (isset($page_data) && $page_data == 'coordonnees') echo '<div style="display:none;">'; ?><!--START CONTAINER MON CONTENU-->

                <?php if (isset($user_groups) && $user_groups->id == '5') { ?>
                    <div id="div_pro_horaires">
                        <div class="div_stl_long_platinum">Horaires</div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">
                            <tr>
                                <td class="stl_long_input_platinum_td" colspan="2">
                                    <label class="label">Heures et jours d’ouvertures : </label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <textarea name="Societe[Horaires]"
                                              id="HorairesSociete"><?php if (isset($objCommercant->Horaires)) echo htmlspecialchars($objCommercant->Horaires); ?></textarea>
                                    <script>

                                        CKEDITOR.replace('HorairesSociete');

                                    </script>
                                </td>
                            </tr>
                        </table>
                    </div>
                <?php } ?>


                <div id="div_pro_pagepresentation"
                     <?php if (isset($user_groups) && $user_groups->id == '5') { ?>style="display:none;"<?php } ?>>
                    <div class="div_stl_long_platinum">Page présentation</div>

                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">

                        <tr>
                            <td colspan="2">
                                <textarea name="Societe[Caracteristiques]"
                                          id="CaracteristiquesSociete"><?php if (isset($objCommercant->Caracteristiques)) echo htmlspecialchars($objCommercant->Caracteristiques); ?></textarea>
                                <script>

                                    CKEDITOR.replace('CaracteristiquesSociete');

                                </script>
                            </td>
                        </tr>
                    </table>
                </div>


                <div id="div_pro_image_presentation"
                     <?php if (isset($user_groups) && $user_groups->id == '5') { ?>style="display:none;"<?php } ?>>

                    <div class="div_stl_long_platinum">Intégration d'images</div>
                    <div style="padding:15px;">Vos images doit avoir les proportions 1 x 4/3 (par exemple 640x480,
                        800x600, 1024x768)
                    </div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Photo 1 : </label>
                                <input type="hidden" name="pdfAssocie" id="pdfAssocie"
                                       value="<?php if (isset($objCommercant->Pdf)) echo htmlspecialchars($objCommercant->Pdf); ?>"/>
                                <input type="hidden" name="photologoAssocie" id="LogoAssocie"
                                       value="<?php if (isset($objCommercant->Logo)) echo htmlspecialchars($objCommercant->Logo); ?>"/>
                                <input type="hidden" name="photo1Associe" id="Photo1Associe"
                                       value="<?php if (isset($objCommercant->Photo1)) echo htmlspecialchars($objCommercant->Photo1); ?>"/>
                                <input type="hidden" name="photo2Associe" id="Photo2Associe"
                                       value="<?php if (isset($objCommercant->Photo2)) echo htmlspecialchars($objCommercant->Photo2); ?>"/>
                                <input type="hidden" name="photo3Associe" id="Photo3Associe"
                                       value="<?php if (isset($objCommercant->Photo3)) echo htmlspecialchars($objCommercant->Photo3); ?>"/>
                                <input type="hidden" name="photo4Associe" id="Photo4Associe"
                                       value="<?php if (isset($objCommercant->Photo4)) echo htmlspecialchars($objCommercant->Photo4); ?>"/>
                                <input type="hidden" name="photo5Associe" id="Photo5Associe"
                                       value="<?php if (isset($objCommercant->Photo5)) echo htmlspecialchars($objCommercant->Photo5); ?>"/>
                                <input type="hidden" name="bandeau_topAssocie" id="bandeau_topAssocie"
                                       value="<?php if (isset($objCommercant->bandeau_top)) echo htmlspecialchars($objCommercant->bandeau_top); ?>"/>
                                <input type="hidden" name="background_imageAssocie" id="background_imageAssocie"
                                       value="<?php if (isset($objCommercant->background_image)) echo htmlspecialchars($objCommercant->background_image); ?>"/>
                                <?php
                                $path_img_gallery = 'application/resources/front/photoCommercant/imagesbank/' . $objCommercant->user_ionauth_id . '/';
                                $path_img_gallery_old = 'application/resources/front/photoCommercant/images/';
                                ?>
                            </td>
                            <?php
                            if (isset($mssg) && $mssg != '1') {
                                $img_error_verify_array = preg_split('/-/', $mssg);
                            }

                            ?>
                            <td>

                                <div id="ArticlePhoto1_container">
                                    <?php if (!empty($objCommercant->Photo1)) { ?>
                                        <?php
                                        if (file_exists($path_img_gallery . $objCommercant->Photo1) == true) {
                                            echo '<img  src="' . base_url() . $path_img_gallery . $objCommercant->Photo1 . '" width="200"/>';
                                        } else {
                                            echo '<img  src="' . base_url() . $path_img_gallery_old . $objCommercant->Photo1 . '" width="200"/>';
                                        }
                                        ?>
                                        <a href="javascript:void(0);" class="btn btn-danger"
                                           onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Photo1');">Supprimer</a>
                                    <?php } else { ?>
                                        <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-Photo1"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                           href='javascript:void(0);' title="Photo1" class="btn btn-info"
                                           id="ArticlePhoto1_link">Ajouter la photo1</a>
                                    <?php } ?>
                                </div>
                                <div id="div_error_taille_Photo1"
                                     class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>


                            </td>
                        </tr>

                        <?php if (isset($user_groups) && ($user_groups->id == '5' || $user_groups->id == '4')) { ?>

                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label class="label">Photo 2 : </label>
                                </td>
                                <td>

                                    <div id="ArticlePhoto2_container">
                                        <?php if (!empty($objCommercant->Photo2)) { ?>
                                            <?php
                                            if (file_exists($path_img_gallery . $objCommercant->Photo2) == true) {
                                                echo '<img  src="' . base_url() . $path_img_gallery . $objCommercant->Photo2 . '" width="200"/>';
                                            } else {
                                                echo '<img  src="' . base_url() . $path_img_gallery_old . $objCommercant->Photo2 . '" width="200"/>';
                                            }
                                            ?>
                                            <a href="javascript:void(0);" class="btn btn-danger"
                                               onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Photo2');">Supprimer</a>
                                        <?php } else { ?>
                                            <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-Photo2"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                               href='javascript:void(0);' title="Photo2" class="btn btn-info"
                                               id="ArticlePhoto2_link">Ajouter la photo2</a>
                                        <?php } ?>
                                    </div>
                                    <div id="div_error_taille_Photo2"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo2", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>


                                </td>
                            </tr>
                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label class="label">Photo 3 : </label>
                                </td>
                                <td>

                                    <div id="ArticlePhoto3_container">
                                        <?php if (!empty($objCommercant->Photo3)) { ?>
                                            <?php
                                            if (file_exists($path_img_gallery . $objCommercant->Photo3) == true) {
                                                echo '<img  src="' . base_url() . $path_img_gallery . $objCommercant->Photo3 . '" width="200"/>';
                                            } else {
                                                echo '<img  src="' . base_url() . $path_img_gallery_old . $objCommercant->Photo3 . '" width="200"/>';
                                            }
                                            ?>
                                            <a href="javascript:void(0);" class="btn btn-danger"
                                               onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Photo3');">Supprimer</a>
                                        <?php } else { ?>
                                            <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-Photo3"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                               href='javascript:void(0);' title="Photo3" class="btn btn-info"
                                               id="ArticlePhoto3_link">Ajouter la photo3</a>
                                        <?php } ?>
                                    </div>
                                    <div id="div_error_taille_Photo3"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo3", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>

                                </td>
                            </tr>
                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label class="label">Photo 4 : </label>
                                </td>
                                <td>

                                    <div id="ArticlePhoto4_container">
                                        <?php if (!empty($objCommercant->Photo4)) { ?>
                                            <?php
                                            if (file_exists($path_img_gallery . $objCommercant->Photo4) == true) {
                                                echo '<img  src="' . base_url() . $path_img_gallery . $objCommercant->Photo4 . '" width="200"/>';
                                            } else {
                                                echo '<img  src="' . base_url() . $path_img_gallery_old . $objCommercant->Photo4 . '" width="200"/>';
                                            }
                                            ?>
                                            <a href="javascript:void(0);" class="btn btn-danger"
                                               onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Photo4');">Supprimer</a>
                                        <?php } else { ?>
                                            <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-Photo4"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                               href='javascript:void(0);' title="Photo4" class="btn btn-info"
                                               id="ArticlePhoto4_link">Ajouter la photo4</a>
                                        <?php } ?>
                                    </div>
                                    <div id="div_error_taille_Photo4"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo4", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>

                                </td>
                            </tr>
                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label class="label">Photo 5 : </label>
                                </td>
                                <td>

                                    <div id="ArticlePhoto5_container">
                                        <?php if (!empty($objCommercant->Photo5)) { ?>
                                            <?php
                                            if (file_exists($path_img_gallery . $objCommercant->Photo5) == true) {
                                                echo '<img  src="' . base_url() . $path_img_gallery . $objCommercant->Photo5 . '" width="200"/>';
                                            } else {
                                                echo '<img  src="' . base_url() . $path_img_gallery_old . $objCommercant->Photo5 . '" width="200"/>';
                                            }
                                            ?>
                                            <a href="javascript:void(0);" class="btn btn-danger"
                                               onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Photo5');">Supprimer</a>
                                        <?php } else { ?>
                                            <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-Photo5"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                               href='javascript:void(0);' title="Photo5" class="btn btn-info"
                                               id="ArticlePhoto5_link">Ajouter la photo5</a>
                                        <?php } ?>
                                    </div>
                                    <div id="div_error_taille_Photo5"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo5", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>

                                </td>
                            </tr>

                        <?php } ?>

                    </table>

                </div>


                <?php if (isset($user_groups) && ($user_groups->id == '5' || $user_groups->id == '4')) { ?>


                    <style type="text/css">
                        #table_geolocalisation td {
                            padding: 0 15px !important;
                        }
                    </style>

                    <div id="div_pro_geolocalisation">
                        <div class="div_stl_long_platinum" style="margin-bottom:0 !important;">G&eacute;olocalisation
                        </div>
                        <table width="100%" border="0" id="table_geolocalisation" cellspacing="0" cellpadding="0"
                               style="padding-left:10px; background-color:#E1E1E1;">

                            <tr>
                                <td colspan="2" style="padding: 15px !important;">
                                    <strong>Mon adresse de géolocalisation</strong><br/>La redirection se fera sur
                                    l’adresse précisée sur le module « Localisation » voir ci-dessous
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <input type="checkbox" name="adresse_localisation_diffuseur_checkbox"
                                           id="adresse_localisation_diffuseur_checkbox"
                                           <?php if (isset($objCommercant->adresse_localisation_diffuseur) && $objCommercant->adresse_localisation_diffuseur == "1") { ?>checked<?php } ?>/>
                                    <input type="hidden" name="Societe[adresse_localisation_diffuseur]"
                                           id="adresse_localisation_diffuseur"
                                           value="<?php if (isset($objCommercant->adresse_localisation_diffuseur)) echo htmlspecialchars($objCommercant->adresse_localisation_diffuseur); ?>">
                                    L’adresse est celle précisée par le diffuseur
                                </td>
                            </tr>
                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label class="label">Adresse : </label>
                                </td>
                                <td>
                                    <input type="text" name="Societe[adresse_localisation]"
                                           id="adresse_localisationSociete"
                                           value="<?php if (isset($objCommercant->adresse_localisation)) echo $objCommercant->adresse_localisation; ?>"
                                           class="stl_long_input_platinum form-control"
                                           onChange="getLatitudeLongitudeLocalisation();"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label class="label">Code Postal : </label>
                                </td>
                                <td id="trReponseVille_localisation">
                                    <input type="text" name="Societe[codepostal_localisation]"
                                           id="codepostal_localisationSociete"
                                           value="<?php if (isset($objCommercant->codepostal_localisation)) echo $objCommercant->codepostal_localisation; ?>"
                                           class="stl_long_input_platinum form-control"
                                           onblur="javascript:CP_getVille_localisation();"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label class="label">Ville : </label>
                                </td>
                                <td>
    	<span id="villeCP_container_localisation">

        <input type="hidden"
               value="<?php if (isset($objCommercant->IdVille_localisation)) echo $objCommercant->IdVille_localisation; ?>
" name="Societe[IdVille_localisation]" id="IdVille_localisationSociete"/>

        <input type="text"
               value="<?php if (isset($this->mdlville->getVilleById($objCommercant->IdVille_localisation)->Nom)) echo $this->mdlville->getVilleById($objCommercant->IdVille_localisation)->Nom; ?>
" name="Societe_IdVille_localisation" class="form-control" id="Societe_IdVille_localisation" disabled="disabled" style="width:413px;"/>

        </span>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2" style="text-align:center;">
                                    <a href="javascript:void(0);" onclick="javascript:open_map_localisation();"><img
                                                src="<?php echo GetImagePath("privicarte/"); ?>/img_localisation_detailagenda.png"
                                                alt="localisation"/></a>
                                </td>
                            </tr>


                        </table>
                    </div>

                <?php } ?>

                <?php if (isset($objAbonnementCommercant) && $user_groups->id == '5') { ?>

                    <div id="div_pro_page1" style="display:none;">
                        <div class="div_stl_long_platinum">Infos page 1</div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">

                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label class="label">Titre page 1 <span style="font-size:11px;">(Maximum 18 caractères et espaces)</span>:</label>
                                </td>
                                <td>
                                    <span style="font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#F00;">&nbsp;&nbsp;(si ce champ est vide l'onglet de la page 1 est masqu&eacute;)</span><br/>
                                    <input type="text" name="Societe[labelactivite1]" id="labelactivite1Societe"
                                           value="<?php if (isset($objCommercant->labelactivite1)) echo htmlspecialchars($objCommercant->labelactivite1); ?>"
                                           class="stl_long_input_platinum"/>
                                    <div class="FieldError" id="divErrorEmailSociete"></div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <textarea name="Societe[activite1]"
                                              id="activite1Societe"><?php if (isset($objCommercant->activite1)) echo htmlspecialchars($objCommercant->activite1); ?></textarea>
                                    <script>

                                        CKEDITOR.replace('activite1Societe');

                                    </script>
                                </td>
                            </tr>
                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label class="label">Integration image 1 : </label>
                                </td>
                                <td>

                                    <div id="Articleactivite1_image1_container">
                                        <?php if (!empty($objCommercant->activite1_image1)) { ?>
                                            <?php
                                            if (file_exists($path_img_gallery . $objCommercant->activite1_image1) == true) {
                                                echo '<img  src="' . base_url() . $path_img_gallery . $objCommercant->activite1_image1 . '" width="200"/>';
                                            } else {
                                                echo '<img  src="' . base_url() . $path_img_gallery_old . $objCommercant->activite1_image1 . '" width="200"/>';
                                            }
                                            ?>
                                            <a href="javascript:void(0);" class="btn btn-danger"
                                               onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite1_image1');">Supprimer</a>
                                        <?php } else { ?>
                                            <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-activite1_image1"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                               href='javascript:void(0);' title="activite1_image1"
                                               class="btn btn-info" id="Articleactivite1_image1_link">Ajouter la
                                                photo1</a>
                                        <?php } ?>
                                    </div>
                                    <input type="hidden" name="activite1_image1Associe" id="activite1_image1Associe"
                                           value="<?php if (isset($objCommercant->activite1_image1)) echo htmlspecialchars($objCommercant->activite1_image1); ?>"/>
                                    <div id="div_error_taille_activite1_image1"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite1_image1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>

                                </td>
                            </tr>
                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label class="label">Integration image 2 : </label>
                                </td>
                                <td>
                                    <div id="Articleactivite1_image2_container">
                                        <?php if (!empty($objCommercant->activite1_image2)) { ?>
                                            <?php
                                            if (file_exists($path_img_gallery . $objCommercant->activite1_image2) == true) {
                                                echo '<img  src="' . base_url() . $path_img_gallery . $objCommercant->activite1_image2 . '" width="200"/>';
                                            } else {
                                                echo '<img  src="' . base_url() . $path_img_gallery_old . $objCommercant->activite1_image2 . '" width="200"/>';
                                            }
                                            ?>
                                            <a href="javascript:void(0);" class="btn btn-danger"
                                               onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite1_image2');">Supprimer</a>
                                        <?php } else { ?>
                                            <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-activite1_image2"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                               href='javascript:void(0);' title="activite1_image2"
                                               class="btn btn-info" id="Articleactivite1_image2_link">Ajouter la
                                                photo2</a>
                                        <?php } ?>
                                    </div>
                                    <input type="hidden" name="activite1_image2Associe" id="activite1_image2Associe"
                                           value="<?php if (isset($objCommercant->activite1_image2)) echo htmlspecialchars($objCommercant->activite1_image2); ?>"/>
                                    <div id="div_error_taille_activite1_image2"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite1_image2", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>

                                </td>
                            </tr>
                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label class="label">Integration image 3 : </label>
                                </td>
                                <td>
                                    <div id="Articleactivite1_image3_container">
                                        <?php if (!empty($objCommercant->activite1_image3)) { ?>
                                            <?php
                                            if (file_exists($path_img_gallery . $objCommercant->activite1_image3) == true) {
                                                echo '<img  src="' . base_url() . $path_img_gallery . $objCommercant->activite1_image3 . '" width="200"/>';
                                            } else {
                                                echo '<img  src="' . base_url() . $path_img_gallery_old . $objCommercant->activite1_image3 . '" width="200"/>';
                                            }
                                            ?>
                                            <a href="javascript:void(0);" class="btn btn-danger"
                                               onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite1_image3');">Supprimer</a>
                                        <?php } else { ?>
                                            <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-activite1_image3"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                               href='javascript:void(0);' title="activite1_image3"
                                               class="btn btn-info" id="Articleactivite1_image3_link">Ajouter la
                                                photo3</a>
                                        <?php } ?>
                                    </div>
                                    <input type="hidden" name="activite1_image3Associe" id="activite1_image3Associe"
                                           value="<?php if (isset($objCommercant->activite1_image3)) echo htmlspecialchars($objCommercant->activite1_image3); ?>"/>
                                    <div id="div_error_taille_activite1_image3"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite1_image3", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label class="label">Integration image 4 : </label>
                                </td>
                                <td>
                                    <div id="Articleactivite1_image4_container">
                                        <?php if (!empty($objCommercant->activite1_image4)) { ?>
                                            <?php
                                            if (file_exists($path_img_gallery . $objCommercant->activite1_image4) == true) {
                                                echo '<img  src="' . base_url() . $path_img_gallery . $objCommercant->activite1_image4 . '" width="200"/>';
                                            } else {
                                                echo '<img  src="' . base_url() . $path_img_gallery_old . $objCommercant->activite1_image4 . '" width="200"/>';
                                            }
                                            ?>
                                            <a href="javascript:void(0);" class="btn btn-danger"
                                               onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite1_image4');">Supprimer</a>
                                        <?php } else { ?>
                                            <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-activite1_image4"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                               href='javascript:void(0);' title="activite1_image4"
                                               class="btn btn-info" id="Articleactivite1_image4_link">Ajouter la
                                                photo4</a>
                                        <?php } ?>
                                    </div>
                                    <input type="hidden" name="activite1_image4Associe" id="activite1_image4Associe"
                                           value="<?php if (isset($objCommercant->activite1_image4)) echo htmlspecialchars($objCommercant->activite1_image4); ?>"/>
                                    <div id="div_error_taille_activite1_image4"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite1_image4", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label class="label">Integration image 5 : </label>
                                </td>
                                <td>
                                    <div id="Articleactivite1_image5_container">
                                        <?php if (!empty($objCommercant->activite1_image5)) { ?>
                                            <?php
                                            if (file_exists($path_img_gallery . $objCommercant->activite1_image5) == true) {
                                                echo '<img  src="' . base_url() . $path_img_gallery . $objCommercant->activite1_image5 . '" width="200"/>';
                                            } else {
                                                echo '<img  src="' . base_url() . $path_img_gallery_old . $objCommercant->activite1_image5 . '" width="200"/>';
                                            }
                                            ?>
                                            <a href="javascript:void(0);" class="btn btn-danger"
                                               onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite1_image5');">Supprimer</a>
                                        <?php } else { ?>
                                            <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-activite1_image5"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                               href='javascript:void(0);' title="activite1_image5"
                                               class="btn btn-info" id="Articleactivite1_image5_link">Ajouter la
                                                photo5</a>
                                        <?php } ?>
                                    </div>
                                    <input type="hidden" name="activite1_image5Associe" id="activite1_image5Associe"
                                           value="<?php if (isset($objCommercant->activite1_image5)) echo htmlspecialchars($objCommercant->activite1_image5); ?>"/>
                                    <div id="div_error_taille_activite1_image5"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite1_image5", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                                </td>
                            </tr>

                        </table>
                        <div style="padding:15px;">Vos images doit avoir les proportions 1 x 4/3 (par exemple 640x480,
                            800x600, 1024x768)
                        </div>
                    </div>

                    <div id="div_pro_page2" style="display:none;">
                        <div class="div_stl_long_platinum">Infos page 2</div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">

                            <tr>
                                <td class="stl_long_input_platinum_td">Titre page 2 <span style="font-size:11px;">(Maximum 18 caractères et espaces)</span>:
                                </td>
                                <td>
                                    <span style="font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#F00;">&nbsp;&nbsp;(si ce champ est vide l'onglet de la page 1 est masqu&eacute;)</span><br/>
                                    <input type="text" name="Societe[labelactivite2]" id="labelactivite2Societe"
                                           value="<?php if (isset($objCommercant->labelactivite2)) echo htmlspecialchars($objCommercant->labelactivite2); ?>"
                                           class="stl_long_input_platinum"/>
                                    <div class="FieldError" id="divErrorEmailSociete"></div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <textarea name="Societe[activite2]"
                                              id="activite2Societe"><?php if (isset($objCommercant->activite2)) echo htmlspecialchars($objCommercant->activite2); ?></textarea>
                                    <script>

                                        CKEDITOR.replace('activite2Societe');

                                    </script>
                                </td>
                            </tr>
                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label class="label">Integration image 1 : </label>
                                </td>
                                <td>

                                    <div id="Articleactivite2_image1_container">
                                        <?php if (!empty($objCommercant->activite2_image1)) { ?>
                                            <?php
                                            if (file_exists($path_img_gallery . $objCommercant->activite2_image1) == true) {
                                                echo '<img  src="' . base_url() . $path_img_gallery . $objCommercant->activite2_image1 . '" width="200"/>';
                                            } else {
                                                echo '<img  src="' . base_url() . $path_img_gallery_old . $objCommercant->activite2_image1 . '" width="200"/>';
                                            }
                                            ?>
                                            <a href="javascript:void(0);" class="btn btn-danger"
                                               onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite2_image1');">Supprimer</a>
                                        <?php } else { ?>
                                            <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-activite2_image1"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                               href='javascript:void(0);' title="activite2_image1"
                                               class="btn btn-info" id="Articleactivite2_image1_link">Ajouter la
                                                photo1</a>
                                        <?php } ?>
                                    </div>
                                    <input type="hidden" name="activite2_image1Associe" id="activite2_image1Associe"
                                           value="<?php if (isset($objCommercant->activite2_image1)) echo htmlspecialchars($objCommercant->activite2_image1); ?>"/>
                                    <div id="div_error_taille_activite2_image1"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite2_image1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>

                                </td>
                            </tr>
                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label class="label">Integration image 2 : </label>
                                </td>
                                <td>
                                    <div id="Articleactivite2_image2_container">
                                        <?php if (!empty($objCommercant->activite2_image2)) { ?>
                                            <?php
                                            if (file_exists($path_img_gallery . $objCommercant->activite2_image2) == true) {
                                                echo '<img  src="' . base_url() . $path_img_gallery . $objCommercant->activite2_image2 . '" width="200"/>';
                                            } else {
                                                echo '<img  src="' . base_url() . $path_img_gallery_old . $objCommercant->activite2_image2 . '" width="200"/>';
                                            }
                                            ?>
                                            <a href="javascript:void(0);" class="btn btn-danger"
                                               onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite2_image2');">Supprimer</a>
                                        <?php } else { ?>
                                            <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-activite2_image2"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                               href='javascript:void(0);' title="activite2_image2"
                                               class="btn btn-info" id="Articleactivite2_image2_link">Ajouter la
                                                photo2</a>
                                        <?php } ?>
                                    </div>
                                    <input type="hidden" name="activite2_image2Associe" id="activite2_image2Associe"
                                           value="<?php if (isset($objCommercant->activite2_image2)) echo htmlspecialchars($objCommercant->activite2_image2); ?>"/>
                                    <div id="div_error_taille_activite2_image2"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite2_image2", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label class="label">Integration image 3 : </label>
                                </td>
                                <td>
                                    <div id="Articleactivite2_image3_container">
                                        <?php if (!empty($objCommercant->activite2_image3)) { ?>
                                            <?php
                                            if (file_exists($path_img_gallery . $objCommercant->activite2_image3) == true) {
                                                echo '<img  src="' . base_url() . $path_img_gallery . $objCommercant->activite2_image3 . '" width="200"/>';
                                            } else {
                                                echo '<img  src="' . base_url() . $path_img_gallery_old . $objCommercant->activite2_image3 . '" width="200"/>';
                                            }
                                            ?>
                                            <a href="javascript:void(0);" class="btn btn-danger"
                                               onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite2_image3');">Supprimer</a>
                                        <?php } else { ?>
                                            <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-activite2_image3"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                               href='javascript:void(0);' title="activite2_image3"
                                               class="btn btn-info" id="Articleactivite2_image3_link">Ajouter la
                                                photo3</a>
                                        <?php } ?>
                                    </div>
                                    <input type="hidden" name="activite2_image3Associe" id="activite2_image3Associe"
                                           value="<?php if (isset($objCommercant->activite2_image3)) echo htmlspecialchars($objCommercant->activite2_image3); ?>"/>
                                    <div id="div_error_taille_activite2_image3"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite2_image3", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label class="label">Integration image 4 : </label>
                                </td>
                                <td>
                                    <div id="Articleactivite2_image4_container">
                                        <?php if (!empty($objCommercant->activite2_image4)) { ?>
                                            <?php
                                            if (file_exists($path_img_gallery . $objCommercant->activite2_image4) == true) {
                                                echo '<img  src="' . base_url() . $path_img_gallery . $objCommercant->activite2_image4 . '" width="200"/>';
                                            } else {
                                                echo '<img  src="' . base_url() . $path_img_gallery_old . $objCommercant->activite2_image4 . '" width="200"/>';
                                            }
                                            ?>
                                            <a href="javascript:void(0);" class="btn btn-danger"
                                               onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite2_image4');">Supprimer</a>
                                        <?php } else { ?>
                                            <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-activite2_image4"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                               href='javascript:void(0);' title="activite2_image4"
                                               class="btn btn-info" id="Articleactivite2_image4_link">Ajouter la
                                                photo4</a>
                                        <?php } ?>
                                    </div>
                                    <input type="hidden" name="activite2_image4Associe" id="activite2_image4Associe"
                                           value="<?php if (isset($objCommercant->activite2_image4)) echo htmlspecialchars($objCommercant->activite2_image4); ?>"/>
                                    <div id="div_error_taille_activite2_image4"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite2_image4", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label class="label">Integration image 5 : </label>
                                </td>
                                <td>
                                    <div id="Articleactivite2_image5_container">
                                        <?php if (!empty($objCommercant->activite2_image5)) { ?>
                                            <?php
                                            if (file_exists($path_img_gallery . $objCommercant->activite2_image5) == true) {
                                                echo '<img  src="' . base_url() . $path_img_gallery . $objCommercant->activite2_image5 . '" width="200"/>';
                                            } else {
                                                echo '<img  src="' . base_url() . $path_img_gallery_old . $objCommercant->activite2_image5 . '" width="200"/>';
                                            }
                                            ?>
                                            <a href="javascript:void(0);" class="btn btn-danger"
                                               onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite2_image5');">Supprimer</a>
                                        <?php } else { ?>
                                            <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-co-activite2_image5"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                               href='javascript:void(0);' title="activite2_image5"
                                               class="btn btn-info" id="Articleactivite2_image5_link">Ajouter la
                                                photo5</a>
                                        <?php } ?>
                                    </div>
                                    <input type="hidden" name="activite2_image5Associe" id="activite2_image5Associe"
                                           value="<?php if (isset($objCommercant->activite2_image5)) echo htmlspecialchars($objCommercant->activite2_image5); ?>"/>
                                    <div id="div_error_taille_activite2_image5"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite2_image5", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                                </td>
                            </tr>

                        </table>
                        <div style="padding:15px;">Vos images doit avoir les proportions 1 x 4/3 (par exemple 640x480,
                            800x600, 1024x768)
                        </div>
                    </div>

                <?php } ?>


                <?php if (isset($user_groups) && ($user_groups->id == '5' || $user_groups->id == '4')) { ?>

                    <div id="div_pro_multimedia">
                        <div class="div_stl_long_platinum">Integration Multimedia</div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">
                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label class="label">Lien vid&eacute;o : </label>
                                </td>
                                <td>
                                    <span style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">&nbsp;&nbsp;&nbsp;Format : http://www.youtube.com/watch?v=gvGymDhY49E </span><br/>
                                    <input type="text" name="Societe[Video]" id="VideoSociete"
                                           value="<?php if (isset($objCommercant->Video)) echo htmlspecialchars($objCommercant->Video); ?>"
                                           class="stl_long_input_platinum form-controlss"/>
                                </td>
                            </tr>
                            <tr valign="middle">
                                <td class="stl_long_input_platinum_td" valign="top">
                                    <label class="label">Document PDF : <br/>(max 3MB)</label>
                                </td>
                                <td valign="top">
                                    <?php if (!empty($objCommercant->Pdf)) { ?>
                                        <?php //echo $objCommercant->Pdf; ?>

                                        <?php if ($objCommercant->Pdf != "" && $objCommercant->Pdf != NULL && file_exists("application/resources/front/photoCommercant/images/" . $objCommercant->Pdf) == true) { ?>
                                        <a
                                                href="<?php echo base_url(); ?>application/resources/front/photoCommercant/images/<?php echo $objCommercant->Pdf; ?>"
                                                target="_blank"><img
                                                    src="<?php echo base_url(); ?>application/resources/front/images/pdf_icone.png"
                                                    width="64" alt="PDF"/></a><?php } ?>


                                        <a href="javascript:void(0);"
                                           onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Pdf');"
                                           class="btn btn-danger">Supprimer</a>
                                        <span class="loading_Pdf"></span>
                                        <br/>

                                        <input type="file" name="SocietePdf" id="PdfSociete" value=""
                                               style="display:none;" class="stl_long_input_platinum form-control"/>
                                        <input type="hidden" name="PdfSociete_checker" id="PdfSociete_checker"
                                               value=""/>
                                    <?php } else { ?>
                                        <input type="file" name="SocietePdf" id="PdfSociete" value=""
                                               class="stl_long_input_platinum form-control"/>
                                        <input type="hidden" name="PdfSociete_checker" id="PdfSociete_checker"
                                               value=""/>
                                    <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="stl_long_input_platinum_td">&nbsp;

                                </td>
                                <td>
                                    <span style="font-size:11px"> Préciser le titre de votre document : ex : plaquette commerciale</span><br/>
                                    <input type="text" name="Societe[titre_Pdf]" id="titre_PdfSociete"
                                           value="<?php if (isset($objCommercant->titre_Pdf)) echo htmlspecialchars($objCommercant->titre_Pdf); ?>"
                                           class="stl_long_input_platinum form-control"/>
                                </td>
                            </tr>
                        </table>
                    </div>

                <?php } ?>


                <?php if (isset($page_data) && $page_data == 'coordonnees') echo '</div>'; ?><!--END CONTAINER MON CONTENU-->


                <?php if (isset($page_data) && $page_data == 'contenus') echo '<div style="display:none;">'; ?><!--START CONTAINER COORDONNEES-->


                <?php if (isset($user_groups) && ($user_groups->id == '5' || $user_groups->id == '4')) { ?>

                    <div class="div_stl_long_platinum">Sous-domaine Sortez.org</div>


                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="padding-bottom:10px;">Titre du site</td>
                            <td><input type="text" name="Societe[subdomain_title]" id="subdomain_title"
                                       value="<?php if (isset($objCommercant->subdomain_title)) echo htmlspecialchars($objCommercant->subdomain_title); ?>"
                                       class="stl_long_input_platinum"/></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding-bottom:10px;">Le titre apparaît dans les résultats de
                                recherche et dans la barre de titre du navigateur.
                            </td>
                        </tr>

                    </table>
                    <div class="col-lg-12" style="background-color:#E1E1E1; padding: 15px; margin: 15px 0;">
                        <div class="col-lg-12" style="padding: 15px; font-size: 20px;">Choisissez votre URL à activer
                            !
                        </div>
                        <div class="col-lg-12" style="padding: 0 15px 15px 15px; color: #ff0000;">Note : Un URL ne peut
                            comporter d'éspace ni de caractère spéciaux ni du caractère "_", veuillez utiliser le
                            caractère "-" à la place !
                        </div>
                        <div class="row">
                            <div class="col-xs-1">
                                <input type="radio" id="nom_url_updated_pro_check"
                                       name="nom_url_updated_pro_check" <?php if ((isset($objCommercant->nom_url_check) && $objCommercant->nom_url_check == "0") || !isset($objCommercant->nom_url_check)) echo 'checked'; ?>>
                            </div>
                            <div class="col-xs-11">
                                <table>
                                    <tr>
                                        <td>URL Sortez.org : &nbsp;</td>
                                        <td><input type="text" name="nom_url_updated_pro" id="subdomain_url"
                                                   value="<?php if (isset($objCommercant->nom_url)) echo htmlspecialchars($objCommercant->nom_url); ?>"/><strong>.sortez.org</strong>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-1">
                                <input type="radio" id="nom_url_updated_pro_check_vsv"
                                       name="nom_url_updated_pro_check" <?php if (isset($objCommercant->nom_url_check) && $objCommercant->nom_url_check == "1") echo 'checked'; ?>>
                            </div>
                            <div class="col-xs-11">
                                <table>
                                    <tr>
                                        <td>URL Vivresaville.fr : &nbsp;</td>
                                        <td><input type="text" name="nom_url_updated_pro_vsv" id="subdomain_url_vsv"
                                                   value="<?php if (isset($objCommercant->nom_url_vsv)) echo htmlspecialchars($objCommercant->nom_url_vsv); ?>"/><strong>.vivresaville.fr</strong>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-12" style="padding: 15px;">Note : Il faut 24-48h pour l'activation du
                            sous-domaine après changement !
                        </div>
                        <div class="col-lg-12" style="padding: 15px; text-align: center;"><a href="javascript:void(0);"
                                                                                             id="nom_url_send_activation_btn"
                                                                                             class="btn btn-info">Envoyer
                                une demande d'activation d'URL</a></div>
                        <div id="span_nom_url_send_activation" style="text-align: center;"></div>
                        <input type="hidden" id="subdomain_url_check" name="Societe[nom_url_check]"
                               value="<?php if (isset($objCommercant->nom_url_check)) echo $objCommercant->nom_url_check; else echo '0'; ?>"/>
                    </div>
                    <table style="width: 100%">
                        <tr>
                            <td colspan="2"
                                style="text-align:center;padding-bottom:20px; vertical-align: top; width: 50%;">
                                <a href="javascript:void(0);" onclick="javascript:verify_privicarte_subdomain();"
                                   class="btn btn-primary">Verification Sortez.org</a>
                                <span class="span_verify_subdomain" id="span_verify_subdomain"
                                      style="width:100%; text-align:center;"></span>
                                <div style="width:100%; text-align:center; padding:20px;"><?php if (isset($objCommercant->nom_url)) { ?>
                                    <a
                                            href="http://<?php echo htmlspecialchars($objCommercant->nom_url); ?>.sortez.org"
                                            target="_blank">
                                        http://<?php echo htmlspecialchars($objCommercant->nom_url); ?>
                                        .sortez.org</a><?php } ?></div>
                            </td>
                            <td colspan="2" style="text-align:center;padding-bottom:20px; vertical-align: top;">
                                <a href="javascript:void(0);" onclick="javascript:verify_privicarte_subdomain_vsv();"
                                   class="btn btn-primary">Verification Vivresaville.fr</a>
                                <span class="span_verify_subdomain" id="span_verify_subdomain_vsv"
                                      style="width:100%; text-align:center;"></span>
                                <div style="width:100%; text-align:center; padding:20px;"><?php if (isset($objCommercant->nom_url_vsv)) { ?>
                                    <a
                                            href="http://<?php echo htmlspecialchars($objCommercant->nom_url_vsv); ?>.vivresaville.fr"
                                            target="_blank">
                                        http://<?php echo htmlspecialchars($objCommercant->nom_url_vsv); ?>
                                        .vivresaville.fr</a><?php } ?></div>
                            </td>
                        </tr>
                    </table>


                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Description du site : Quelques phrases décrivant votre page. Elles
                                    sont aussi utilisées lorsque votre page est partagée sur Facebook</label>
                            </td>
                        <tr>
                        </tr>
                        <td>
                            <textarea name="Societe[metadescription]" id="metadescription"
                                      style="width:100%; height:150px;"><?php if (isset($objCommercant->metadescription)) echo htmlspecialchars($objCommercant->metadescription); ?></textarea>
                        </td>
                        </tr>
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Mots-clés du site : Une liste de mots-clés séparés par des virgules
                                    pour votre page. Que doit-on chercher pour trouver cette page ? </label>
                            </td>
                        <tr>
                        </tr>
                        <td>
                            <textarea name="Societe[metatag]" id="metatag"
                                      style="width:100%; height:150px;"><?php if (isset($objCommercant->metatag)) echo htmlspecialchars($objCommercant->metatag); ?></textarea>
                        </td>
                        </tr>
                    </table>

                <?php } ?>


                <div class="div_stl_long_platinum" style="margin-bottom:0 !important;">Création d'un QRCODE</div>

                <div style="width:100%; display:table; background-color:#E1E1E1;">
                    <div class="col-lg-4" style="padding:15px !important; border:thin; height:150px;">
                        <div id="div_qrcode_img" style="width:100%; height:100%; text-align:center;">
                            <?php if (isset($objCommercant->qrcode_img) && $objCommercant->qrcode_img != "") { ?>
                                <img src="<?php echo base_url(); ?>/application/resources/phpqrcode/images/<?php echo $objCommercant->qrcode_img; ?>"
                                     alt="qrcode"/>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="col-lg-8" style="padding:0 !important">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">
                            <tr>
                                <td class="stl_long_input_platinum_td" valign="top">
                                    Préciser l'URL de votre page
                                </td>
                            </tr>
                            <tr>
                                <td class="stl_long_input_platinum_td" valign="top">
                                    <input type="text" name="Societe[qrcode_text]" id="qrcode_text"
                                           value="<?php if (isset($objCommercant->qrcode_text)) echo htmlspecialchars($objCommercant->qrcode_text); ?>"
                                           class="stl_long_input_platinum"/>
                                    <input type="hidden" name="Societe[qrcode_img]" id="qrcode_img"
                                           value="<?php if (isset($objCommercant->qrcode_img)) echo htmlspecialchars($objCommercant->qrcode_img); ?>"
                                           class="stl_long_input_platinum"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>
                                        Par d&eacute;faut l'url correspondante &agrave; votre page d'accueil est
                                        enregistr&eacute;e. Le QRCODE est automatiquement g&eacute;n&eacute;r&eacute; et
                                        se retrouve sur l'ensemble de vos pages &quot;Nos infos sur votre
                                        mobile&quot;.<br/>
                                        Si vous optez pour un sous-domaine ou une url personnalis&eacute;e, n'oubliez
                                        pas d'int&eacute;grer l'adresse et de valider le nouveau QRCODE.
                                    </p>
                                    <p style=" padding: 15px 0;"><a href="javascript:void(0);"
                                                                    onclick="javascript:generate_qrcode();"><img
                                                    src="<?php echo GetImagePath("privicarte/"); ?>/btn_generate_qrcode.png"
                                                    alt="qrcode"/></a><span id="span_generate_qrcode"></span></p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>


                <?php if (isset($page_data) && $page_data == 'contenus') echo '</div>'; ?><!--END CONTAINER COORDONNEES-->


                <!--AJOUT GLISSIERES-->


                <?php if (isset($page_data) && $page_data == 'coordonnees') echo '<div style="display:none;">'; ?><!--START CONTAINER MON CONTENU-->


                <?php
                if (isset($objCommercant->IdCommercant)) {
                    $id_glissiere_getted = $this->mdlglissiere->GetByIdCommercant($objCommercant->IdCommercant)->id_glissiere;
                }
                ?>

                <input type="hidden" name="Societe[id_glissiere]" id="id_glissiere_com"
                       value="<?php if (isset($id_glissiere_getted) && $id_glissiere_getted != "" && $id_glissiere_getted != NULL) echo $id_glissiere_getted; else echo '0'; ?>"/>
                <input type="hidden" name="Glissiere[id_glissiere]" id="id_glissiere_glis"
                       value="<?php if (isset($id_glissiere_getted) && $id_glissiere_getted != "" && $id_glissiere_getted != NULL) echo $id_glissiere_getted; else echo '0'; ?>"/>


                <?php if (isset($user_groups) && $user_groups->id == '5') { ?>
                    <div class="row " id="blocss" style="display:none; margin-top: 20px;">

                        <div class="col-lg-12 text-center"
                             style="background-color: #008000;height: 40px;padding-top: 12px;color: white">AJOUTER DES
                            BLOCS
                            IMAGES ET TEXT COMPL&Eacute;MENTAIRES
                            <span style="float: right">
                        <label for="is_activ_bloc" style="color:#FFFFFF !important;">Activé:</label>

    <select name="Glissiere[is_activ_bloc]" id="is_activ_bloc" style="color:#000000;">
        <option value="1"
                <?php if (isset($objGlissiere->is_activ_bloc) AND $objGlissiere->is_activ_bloc == 1){ ?>selected="selected"<?php } ?> >oui</option>
        <option value="0"
                <?php if (isset($objGlissiere->is_activ_bloc) AND $objGlissiere->is_activ_bloc == 0){ ?>selected="selected"<?php } ?> >non</option>
    </select>
                    </span>
                        </div>
                        <?php
                        $thiss = get_instance();
                        $thiss->load->model('commercant');
                        $objbonplan = $thiss->commercant->get_bonplan_commercant_by_id($objCommercant->IdCommercant);

                        $thiss->load->model('commercant');
                        $objfidelity = $thiss->commercant->get_fidelity_commercant_by_id($objCommercant->IdCommercant);
                        //var_dump(count($objbonplan));die();
                        ?>
                        <div class="col-lg-12 text-center" style="margin-top: 20px" id="type_bloc_choose">
                            <label for="type_bloc">Choisir type de bloc</label>
                            <select name="Glissiere[type_bloc]" class="form-control" id="type_bloc">
                                <option value="0">--veuillez Choisir--</option>
                                <option <?php if (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc == '1') {
                                    echo 'selected';
                                } ?> value="1">1 bloc d'image et 1 bloc de texte
                                </option>
                                <option <?php if (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc == '2') {
                                    echo 'selected';
                                } ?> value="2">2 blocs d'images et 2 blocs de textes
                                </option>
                                *
                                <?php if (count($objbonplan) > 0) { ?>
                                <option <?php if (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc == '1bp') {
                                    echo 'selected';
                                } ?> value="1bp">1 blocs d'image bonplan et 1 blocs de textes bonplan</option><?php } ?>
                                <?php if (count($objfidelity) > 0) { ?>
                                <option <?php if (isset($objGlissiere->type_bloc) AND $objGlissiere->type_bloc == '1fd') {
                                    echo 'selected';
                                } ?> value="1fd">1 blocs d'images de fidélisation et 1 blocs de textes de
                                        fidélisation</option><?php } ?>
                            </select>
                        </div>

                        <div id="bloc1">
                            <div class="col-lg-12" style="margin-top: 30px;margin-bottom: 30px">

                                <div class="pt-5 pb-5">
                                    <div id="bloc1_image1_content">
                                        <?php if (!empty($objGlissiere->bloc_1_image1_content)) { ?>
                                            <?php
                                            if (file_exists($path_img_gallery . $objGlissiere->bloc_1_image1_content) == true) {
                                                echo '<img  src="' . base_url() . $path_img_gallery . $objGlissiere->bloc_1_image1_content . '" width="200"/>';
                                            } else {
                                                echo '<img  src="' . base_url() . $path_img_gallery_old . $objGlissiere->bloc_1_image1_content . '" width="200"/>';
                                            }
                                            ?>
                                            <a href="javascript:void(0);" class="btn btn-danger"
                                               onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','bloc_1_image1_content');">Supprimer</a>
                                        <?php } else { ?>
                                            <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-gli-bloc_1_image1_content"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                               href='javascript:void(0);' title="bloc_1_image1_content"
                                               class="btn btn-info" id="bloc_1_image1_content_link">Ajouter une Photo du
                                                Bloc</a>
                                        <?php } ?>
                                    </div>
                                    <input type="hidden" name="bloc_1_image1_content" id="bloc_1_image1_content"
                                           value="<?php if (isset($objGlissiere->bloc_1_image1_content)) echo htmlspecialchars($objGlissiere->bloc_1_image1_content); ?>"/>
                                    <div id="bloc_1_image1_content_error"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite1_image1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>

                                </div>
                            </div>

                            <div class="  col-lg-12 mt-5 mb-5">

                                <label class="label text-center mt-5 mb-5" for="bloc1_content">Contenu du bloc:</label>
                                <textarea name="Glissiere[bloc1_content]"
                                          id="bloc1_content"><?php if (isset($objGlissiere->bloc1_content)) echo htmlspecialchars($objGlissiere->bloc1_content); ?>
        </textarea>

                                <script>

                                    CKEDITOR.replace('bloc1_content');

                                </script>
                            </div>
                            <div class="  col-lg-12">


                                <div class="col-lg-12" id="btn_bloc1"
                                     style="background-color: #3EB4D7;height: 40px;padding-top: 12px;color: black">
                                    <span style="color: white!important;">Ajouter un Bouton</span> <span
                                            style="float: right;">
                                                            <select name="Glissiere[is_activ_btn_bloc1]"
                                                                    style="margin-right: 10px;color: black!important;"
                                                                    id="link_btn_bloc1">
                                                                <option value="0" <?php if ($objGlissiere->is_activ_btn_bloc1 == 0) {
                                                                    echo 'selected';
                                                                } ?> >non</option>
                                                                <option value="1" <?php if ($objGlissiere->is_activ_btn_bloc1 == 1) {
                                                                    echo 'selected';
                                                                } ?> >oui</option>
                                                            </select></span>
                                </div>

                                <div class="text-center" id="link_bloc1">
                                    <label for="btn_bloc1_content1">Texte du bouton</label>
                                    <input class="form-control" id="btn_bloc1_content1" type="text"
                                           value="<?php if (isset($objGlissiere->btn_bloc1_content1) AND $objGlissiere->btn_bloc1_content1 != "") {
                                               echo $objGlissiere->btn_bloc1_content1;
                                           } ?>" name="Glissiere[btn_bloc1_content1]">
                                    <span class="text-center" style="">
                                                         <label for="valuelink">Lien du bouton</label><select
                                                name="Glissiere[bloc1_existed_link1]" class="form-control"
                                                style="margin-right: 10px;color: black" id="valuelink_bloc1">
                                                                <option <?php if (isset($objGlissiere->bloc1_existed_link1) AND $objGlissiere->bloc1_existed_link1 == '0') {
                                                                    echo 'selected';
                                                                } ?> value="0">Choisir un lien local1</option>
                                                                <option <?php if (isset($objGlissiere->bloc1_existed_link1) AND $objGlissiere->bloc1_existed_link1 == 'p1') {
                                                                    echo 'selected';
                                                                } ?> value="p1">Vers la page 1</option>
                                                                <option <?php if (isset($objGlissiere->bloc1_existed_link1) AND $objGlissiere->bloc1_existed_link1 == 'p2') {
                                                                    echo 'selected';
                                                                } ?> value="p2">Vers la page 2</option>
                                                                <option <?php if (isset($objGlissiere->bloc1_existed_link1) AND $objGlissiere->bloc1_existed_link1 == 'ag') {
                                                                    echo 'selected';
                                                                } ?> value="ag">Mes Agendas</option>
                                                                <option <?php if (isset($objGlissiere->bloc1_existed_link1) AND $objGlissiere->bloc1_existed_link1 == 'art') {
                                                                    echo 'selected';
                                                                } ?> value="art">Mes Articles</option>
                                                                <option <?php if (isset($objGlissiere->bloc1_existed_link1) AND $objGlissiere->bloc1_existed_link1 == 'bp') {
                                                                    echo 'selected';
                                                                } ?> value="bp">Mes Bons Plans</option>
                                                            </select>
            </span><br>

                                    Ou ajouter un lien externe<br>
                                    <div class="col-lg-6 text-left pl-0" style="padding-left: 0px!important;">
                                        <select class="text-center form-control" id="httbloc1">
                                            <option <?php if (preg_match('/http:/', $objGlissiere->bloc1_custom_link1)) {
                                                echo 'selected';
                                            } ?> value="http://">http://
                                            </option>
                                            <option <?php if (preg_match('/http:/', $objGlissiere->bloc1_custom_link1)) {
                                                echo 'selected';
                                            } ?> value="https://">https://
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 text-right pr0" style="padding-right: 0px!important;">
                                        <input onchange="sub_link_custom_bloc1()" name="Glissiere[bloc1_custom_link1]"
                                               class="form-control text-center" type="text" value="" id="link_htt_bloc1"
                                               placeholder="votre liens ici">
                                    </div>
                                </div>


                            </div>
                        </div>


                        <div id="bloc2">
                            <div class=" col-lg-12" style="margin-top: 30px;margin-bottom: 30px">

                                <div class="pt-5 pb-5">
                                    <div id="bloc1_image2_content">
                                        <?php if (!empty($objGlissiere->bloc_1_image2_content)) { ?>
                                            <?php
                                            if (file_exists($path_img_gallery . $objGlissiere->bloc_1_image2_content) == true) {
                                                echo '<img  src="' . base_url() . $path_img_gallery . $objGlissiere->bloc_1_image2_content . '" width="200"/>';
                                            } else {
                                                echo '<img  src="' . base_url() . $path_img_gallery_old . $objGlissiere->bloc_1_image2_content . '" width="200"/>';
                                            }
                                            ?>
                                            <a href="javascript:void(0);" class="btn btn-danger"
                                               onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','bloc_1_image2_content');">Supprimer</a>
                                        <?php } else { ?>
                                            <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-gli-bloc_1_image2_content"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                               href='javascript:void(0);' title="bloc_1_image2_content"
                                               class="btn btn-info" id="bloc_1_image2_content_link">Ajouter une Photo2
                                                du
                                                Bloc</a>
                                        <?php } ?>
                                    </div>
                                    <input type="hidden" name="bloc_1_image2_content" id="bloc_1_image2_content"
                                           value="<?php if (isset($objGlissiere->bloc_1_image2_content)) echo htmlspecialchars($objGlissiere->bloc_1_image2_content); ?>"/>
                                    <div id="bloc_1_image2_content_error"
                                         class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite1_image1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>

                                </div>
                            </div>

                            <div class=" col-lg-12 mt-5 mb-5">

                                <label class="label text-center mt-5 mb-5" for="bloc1_content2">Contenu du bloc:</label>
                                <textarea name="Glissiere[bloc1_content2]"
                                          id="bloc1_content2"><?php if (isset($objGlissiere->bloc1_content)) echo htmlspecialchars($objGlissiere->bloc1_content2); ?>
        </textarea>

                                <script>

                                    CKEDITOR.replace('bloc1_content2');

                                </script>
                            </div>
                            <div class=" col-lg-12">


                                <div class="col-lg-12" id="btn_bloc2"
                                     style="background-color: #3EB4D7;height: 40px;padding-top: 12px;color: black">
                                    <span style="color: white!important;">Ajouter un Bouton</span> <span
                                            style="float: right;">
                                                            <select name="Glissiere[is_activ_btn_bloc2]"
                                                                    style="margin-right: 10px;color: black!important;"
                                                                    id="link_btn_bloc2">
                                                                <option value="0" <?php if (isset($objGlissiere->is_activ_btn_bloc2) AND $objGlissiere->is_activ_btn_bloc2 == 0) {
                                                                    echo 'selected';
                                                                } ?> >non</option>
                                                                <option value="1" <?php if (isset($objGlissiere->is_activ_btn_bloc2) AND $objGlissiere->is_activ_btn_bloc2 == 1) {
                                                                    echo 'selected';
                                                                } ?> >oui</option>

                                                            </select></span>
                                </div>

                                <div class="text-center" id="link_bloc2">
                                    <label for="btn_bloc1_content2">Texte du bouton</label>
                                    <input class="form-control" id="btn_bloc1_content2" type="text"
                                           value="<?php if (isset($objGlissiere->btn_bloc1_content2) AND $objGlissiere->btn_bloc1_content2 != "") {
                                               echo $objGlissiere->btn_bloc1_content2;
                                           } ?>" name="Glissiere[btn_bloc1_content2]">
                                    <span class="text-center" style="">
                                                         <label for="valuelink_bloc1">Lien du bouton</label><select
                                                name="Glissiere[bloc1_existed_link2]" class="form-control"
                                                style="margin-right: 10px;color: black" id="valuelink_bloc1">
                                                                <option <?php if (isset($objGlissiere->bloc1_existed_link2) AND $objGlissiere->bloc1_existed_link2 == '0') {
                                                                    echo 'selected';
                                                                } ?> value="0">Choisir un lien local1</option>
                                                                <option <?php if (isset($objGlissiere->bloc1_existed_link2) AND $objGlissiere->bloc1_existed_link2 == 'p1') {
                                                                    echo 'selected';
                                                                } ?> value="p1">Vers la page 1</option>
                                                                <option <?php if (isset($objGlissiere->bloc1_existed_link2) AND $objGlissiere->bloc1_existed_link2 == 'p2') {
                                                                    echo 'selected';
                                                                } ?> value="p2">Vers la page 2</option>
                                                                <option <?php if (isset($objGlissiere->bloc1_existed_link2) AND $objGlissiere->bloc1_existed_link2 == 'ag') {
                                                                    echo 'selected';
                                                                } ?> value="ag">Mes Agendas</option>
                                                                <option <?php if (isset($objGlissiere->bloc1_existed_link2) AND $objGlissiere->bloc1_existed_link2 == 'art') {
                                                                    echo 'selected';
                                                                } ?> value="art">Mes Articles</option>
                                                                <option <?php if (isset($objGlissiere->bloc1_existed_link2) AND $objGlissiere->bloc1_existed_link2 == 'bp') {
                                                                    echo 'selected';
                                                                } ?> value="bp">Mes Bons Plans</option>
                                                            </select>
            </span><br>

                                    Ou ajouter un lien externe<br>
                                    <div class="col-lg-6 text-left pl-0" style="padding-left: 0px!important;">
                                        <select class="text-center form-control" id="httbloc2">
                                            <option <?php if (preg_match('/http:/', $objGlissiere->bloc1_custom_link2)) {
                                                echo 'selected';
                                            } ?> value="http://">http://
                                            </option>
                                            <option <?php if (preg_match('/http:/', $objGlissiere->bloc1_custom_link2)) {
                                                echo 'selected';
                                            } ?> value="https://">https://
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 text-right pr0" style="padding-right: 0px!important;">
                                        <input onchange="sub_link_custom_bloc1()" name="Glissiere[bloc1_custom_link2]"
                                               class="form-control text-center" type="text" value="" id="link_htt_bloc2"
                                               placeholder="votre liens ici">
                                    </div>
                                </div>


                            </div>
                        </div>

                        <div id="bonplan_config" style="margin-top: 20px">
                            <div class="col-lg-12">
                                <div style="height: 40px;background-color: #3552A1;text-align: center;padding-top: 13px;color: white">
                                    UN BON PLAN ET LE BLOC TEXTE
                                </div>
                            </div>
                            <label class="text-center" for="bonplan_select">Coisir bonplan</label>
                            <select name="Glissiere[bonplan_id]" id="bonplan_select" class="form-control">
                                <option value=""> choisir bonplan</option>
                                <?php if (count($objbonplan) > 1) {
                                    foreach ($objbonplan as $bonplan) { ?>
                                        <option <?php if (isset($objGlissiere->bonplan_id) AND $objGlissiere->bonplan_id == $bonplan->bonplan_id) {
                                            echo 'selected';
                                        } ?> value="<?php echo $bonplan->bonplan_id; ?>"><?php echo $bonplan->bonplan_titre; ?><?php if (date('Y-m-d', $bonplan->bonplan_date_fin ?? $bonplan->bp_multiple_date_fin ?? $bonplan->bp_unique_date_fin) < date('Y-m-d')) {
                                                echo '(Expiré)';
                                            } ?></option>
                                    <?php } ?>
                                <?php } elseif (count($objbonplan) == 1) { ?>
                                    <option <?php if (isset($objGlissiere->bonplan_id) AND $objGlissiere->bonplan_id == $objbonplan->bonplan_id) {
                                        echo 'selected';
                                    } ?> value="<?php echo $objbonplan->bonplan_id; ?>"><?php echo $objbonplan->bonplan_titre; ?><?php if (date('Y-m-d', $bonplan->bonplan_date_fin ?? $bonplan->bp_multiple_date_fin ?? $bonplan->bp_unique_date_fin) < date('Y-m-d')) {
                                            echo '(Expiré)';
                                        } ?></option>
                                <?php } ?>
                            </select>
                            <div class="col-lg-12" id="bonplan_content_posted">

                            </div>
                            <div id="bonplan_div">
                                <label class="label text-center mt-5 mb-5" for="bonplan_content">Contenu du
                                    bonplan:</label>
                                <textarea name="Glissiere[bonplan_content]"
                                          id="bonplan_content"><?php if (isset($objGlissiere->bonplan_content)) echo htmlspecialchars($objGlissiere->bonplan_content); ?>
        </textarea>

                                <script>

                                    CKEDITOR.replace('bonplan_content');

                                </script>
                            </div>
                            <div class="col-lg-12" id="btn_fd"
                                 style="background-color: #3EB4D7;height: 40px;padding-top: 12px;color: black">
                                <span style="color: white!important;">Ajouter un Bouton</span> <span
                                        style="float: right;">
                                                            <select name="Glissiere[is_activ_btn_bp]"
                                                                    style="margin-right: 10px;color: black!important;"
                                                                    id="link_btn_bp">
                                                                <option value="1" <?php if (isset($objGlissiere->is_activ_btn_bp) AND $objGlissiere->is_activ_btn_bp == 1) {
                                                                    echo 'selected';
                                                                } ?> >oui</option>
                                                                <option value="0" <?php if (isset($objGlissiere->is_activ_btn_bp) AND $objGlissiere->is_activ_btn_bp == 0) {
                                                                    echo 'selected';
                                                                } ?> >non</option>
                                                            </select></span>
                            </div>

                            <div class="text-center" id="link_bp">
                                <label for="btn_bp_content">Texte du bouton</label>
                                <input class="form-control" id="btn_bp_content" type="text"
                                       value="<?php if (isset($objGlissiere->btn_bp_content) AND $objGlissiere->btn_bp_content != "") {
                                           echo $objGlissiere->btn_bp_content;
                                       } ?>" name="Glissiere[btn_bp_content]">
                                <span class="text-center" style="">
                                                         <label for="valuelink_bp">Lien du bouton</label><select
                                            name="Glissiere[bp_existed_link]" class="form-control"
                                            style="margin-right: 10px;color: black" id="valuelink_bp">
                                                                <option <?php if (isset($objGlissiere->bp_existed_link) AND $objGlissiere->bp_existed_link == '0') {
                                                                    echo 'selected';
                                                                } ?> value="0">Choisir un lien local1</option>
                                                                <option <?php if (isset($objGlissiere->bp_existed_link) AND $objGlissiere->bp_existed_link == 'p1') {
                                                                    echo 'selected';
                                                                } ?> value="p1">Vers la page 1</option>
                                                                <option <?php if (isset($objGlissiere->bp_existed_link) AND $objGlissiere->bp_existed_link == 'p2') {
                                                                    echo 'selected';
                                                                } ?> value="p2">Vers la page 2</option>
                                                                <option <?php if (isset($objGlissiere->bp_existed_link) AND $objGlissiere->bp_existed_link == 'ag') {
                                                                    echo 'selected';
                                                                } ?> value="ag">Mes Agendas</option>
                                                                <option <?php if (isset($objGlissiere->bp_existed_link) AND $objGlissiere->bp_existed_link == 'art') {
                                                                    echo 'selected';
                                                                } ?> value="art">Mes Articles</option>
                                                                <option <?php if (isset($objGlissiere->bp_existed_link) AND $objGlissiere->bp_existed_link == 'bp') {
                                                                    echo 'selected';
                                                                } ?> value="bp">Mes Bons Plans</option>
                                                            </select>
            </span><br>

                                Ou ajouter un lien externe<br>
                                <div class="col-lg-6 text-left pl-0" style="padding-left: 0px!important;">
                                    <select class="text-center form-control" id="httbp">
                                        <option <?php if (preg_match('/http:/', $objGlissiere->bp_custom_link)) {
                                            echo 'selected';
                                        } ?> value="http://">http://
                                        </option>
                                        <option <?php if (preg_match('/http:/', $objGlissiere->bp_custom_link)) {
                                            echo 'selected';
                                        } ?> value="https://">https://
                                        </option>
                                    </select>
                                </div>
                                <div class="col-lg-6 text-right pr0" style="padding-right: 0px!important;">
                                    <input onchange="sub_link_custom_fd()" name="Glissiere[bp_custom_link]"
                                           class="form-control text-center" type="text"
                                           value="<?php echo $objGlissiere->bp_custom_link; ?>" id="link_htt_bp"
                                           placeholder="votre liens ici">
                                </div>
                            </div>
                        </div>
                        <div id="fidelity_config" style="margin-top: 20px">
                            <div class="col-lg-12">
                                <div style="height: 40px;background-color: #3552A1;text-align: center;padding-top: 13px;color: white">
                                    UN FIDELIT&Eacute; ET LE BLOC TEXTE
                                </div>
                            </div>
                            <label class="text-center" for="fidelity_select">Coisir fidelité</label>
                            <select name="Glissiere[annonce_id]" id="fidelity_select" class="form-control">
                                <option value=""> choisir fidelité</option>
                                <?php if (count($objfidelity) > 1) {
                                    foreach ($objfidelity as $fidelity) { ?>
                                        <option <?php if (isset($objGlissiere->annonce_id) AND $objGlissiere->annonce_id == $fidelity->annonce_id) {
                                            echo 'selected';
                                        } ?> value="<?php echo intval($fidelity->annonce_id); ?>"><?php echo $fidelity->annonce_description_courte ?? ""; ?> </option>
                                    <?php } ?>
                                <?php } elseif (count($objfidelity) == 1) { ?>
                                    <option <?php if (isset($objfidelity->annonce_id) AND $objfidelity->annonce_id == $objfidelity->annonce_id) {
                                        echo 'selected';
                                    } ?> value="<?php echo intval($objfidelity->annonce_id); ?>"><?php echo $objfidelity->annonce_description_courte ?? ""; ?> </option>
                                <?php } ?>
                            </select>


                            <div class="col-lg-12" id="fidelity_content_posted"></div>
                            <div id="fidelity_div">
                                <label class="label text-center mt-5 mb-5" for="fidelity_content">Contenu du
                                    fidelité:</label>
                                <textarea name="Glissiere[fidelity_content]"
                                          id="fidelity_content"><?php if (isset($objGlissiere->fidelity_content)) echo htmlspecialchars($objGlissiere->fidelity_content); ?>
        </textarea>

                                <script>

                                    CKEDITOR.replace('fidelity_content');

                                </script>

                                <div class="col-lg-12" id="btn_fd"
                                     style="background-color: #3EB4D7;height: 40px;padding-top: 12px;color: black">
                                    <span style="color: white!important;">Ajouter un Bouton</span> <span
                                            style="float: right;">
                                                            <select name="Glissiere[is_activ_btn_fd]"
                                                                    style="margin-right: 10px;color: black!important;"
                                                                    id="link_btn_fd">
                                                                <option value="1" <?php if (isset($objGlissiere->is_activ_btn_fd) AND $objGlissiere->is_activ_btn_fd == 1) {
                                                                    echo 'selected';
                                                                } ?> >oui</option>
                                                                <option value="0" <?php if (isset($objGlissiere->is_activ_btn_fd) AND $objGlissiere->is_activ_btn_fd == 0) {
                                                                    echo 'selected';
                                                                } ?> >non</option>
                                                            </select></span>
                                </div>

                                <div class="text-center" id="link_fd">
                                    <label for="btn_fd_content">Texte du bouton</label>
                                    <input class="form-control" id="btn_fd_content" type="text"
                                           value="<?php if (isset($objGlissiere->btn_fd_content) AND $objGlissiere->btn_fd_content != "") {
                                               echo $objGlissiere->btn_fd_content;
                                           } ?>" name="Glissiere[btn_fd_content]">
                                    <span class="text-center" style="">
                                                         <label for="valuelink_fd">Lien du bouton</label><select
                                                name="Glissiere[FD_existed_link]" class="form-control"
                                                style="margin-right: 10px;color: black" id="valuelink_FD">
                                                                <option <?php if (isset($objGlissiere->FD_existed_link) AND $objGlissiere->FD_existed_link == '0') {
                                                                    echo 'selected';
                                                                } ?> value="0">Choisir un lien local1</option>
                                                                <option <?php if (isset($objGlissiere->FD_existed_link) AND $objGlissiere->FD_existed_link == 'p1') {
                                                                    echo 'selected';
                                                                } ?> value="p1">Vers la page 1</option>
                                                                <option <?php if (isset($objGlissiere->FD_existed_link) AND $objGlissiere->FD_existed_link == 'p2') {
                                                                    echo 'selected';
                                                                } ?> value="p2">Vers la page 2</option>
                                                                <option <?php if (isset($objGlissiere->FD_existed_link) AND $objGlissiere->FD_existed_link == 'ag') {
                                                                    echo 'selected';
                                                                } ?> value="ag">Mes Agendas</option>
                                                                <option <?php if (isset($objGlissiere->FD_existed_link) AND $objGlissiere->FD_existed_link == 'art') {
                                                                    echo 'selected';
                                                                } ?> value="art">Mes Articles</option>
                                                                <option <?php if (isset($objGlissiere->FD_existed_link) AND $objGlissiere->FD_existed_link == 'bp') {
                                                                    echo 'selected';
                                                                } ?> value="bp">Mes Bons Plans</option>
                                                            </select>
            </span><br>

                                    Ou ajouter un lien externe<br>
                                    <div class="col-lg-6 text-left pl-0" style="padding-left: 0px!important;">
                                        <select class="text-center form-control" id="httfd">
                                            <option <?php if (preg_match('/http:/', $objGlissiere->fd_custom_link)) {
                                                echo 'selected';
                                            } ?> value="http://">http://
                                            </option>
                                            <option <?php if (preg_match('/http:/', $objGlissiere->fd_custom_link)) {
                                                echo 'selected';
                                            } ?> value="https://">https://
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 text-right pr0" style="padding-right: 0px!important;">
                                        <input onchange="sub_link_custom_fd()" name="Glissiere[fd_custom_link]"
                                               class="form-control text-center" type="text"
                                               value="<?php echo $objGlissiere->fd_custom_link; ?>" id="link_htt_fd"
                                               placeholder="votre liens ici">
                                    </div>
                                </div>


                            </div>


                        </div>

                    </div>
                <?php } ?>

                <div class="row">

                </div>

                <div class="row" id="div_pro_glissiere1_home"
                     <?php if (isset($user_groups) && $user_groups->id == '5') { ?>style="display:none;"<?php } ?>>
                    <div class="col-lg-12"
                         style="background-color: #3653A3;
            color: #FFFFFF;
                    font-size: 13px;
                    font-weight: 700;
                    line-height: 1.23em;
                    margin-top: 12px;
                    margin-bottom: 15px;height: 40px; padding-top: 12px;"><?php if (isset($user_groups) && $user_groups->id == '5') { ?>Glissi&egrave;re 1 page présentation<?php } else { ?>Informations glissi&egrave;re 1<?php } ?>
                        <span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Glissiere[isActive_presentation_1]" id="isActive_presentation_1" style="color:#000000;">
        <option value="0"
                <?php if (isset($objGlissiere->isActive_presentation_1) AND $objGlissiere->isActive_presentation_1 == '0') { ?>selected="selected"<?php } ?>>non</option>
        <option value="1"
                <?php if (isset($objGlissiere->isActive_presentation_1) AND $objGlissiere->isActive_presentation_1 == '1') { ?>selected="selected"<?php } ?>>oui</option>
    </select>
</span><span id="dispnbgli" style=" color:#000000;">
      <label style="padding-left: 70px;color: white!important;" for="nbglissiere1">Nombre de bloc:&nbsp;</label>
      <select id="nbglissiere1">
        <option <?php if ($objGlissiere->presentation_1_contenu2 == '' AND $objGlissiere->is_activ_btn_glissiere1_champ1 == 1) {
            echo 'selected';
        } ?> value="1">1</option>
        <option <?php if ($objGlissiere->presentation_1_contenu2 != '' AND $objGlissiere->is_activ_btn_glissiere1_champ1 == 1) {
            echo 'selected';
        } ?> value="2">2</option>
    </select>
                        </span>
                    </div>


                    <div id="glissiere1content" style="display: block; width:100%;">
                        <div class="col-lg-12" id="gl1_tittle">
                            <div class="stl_long_input_platinum_td">
                                <label class="label">Titre Glissière1: </label>
                            </div>
                            <div>
                                <input class="form-control" type="text" name="Glissiere[presentation_1_titre]"
                                       id="presentation_1_titre"
                                       value="<?php if (isset($objGlissiere->presentation_1_titre)) echo htmlspecialchars($objGlissiere->presentation_1_titre); ?>"/>
                            </div>
                        </div>


                        <div class="img_tab" id="gl1_img">
                            <div class="stl_long_input_platinum_td">
                                <label class="label">Ajouter une image : </label>
                            </div>
                            <div>

                                <div id="Articleactivite1_image1_container">
                                    <?php if (!empty($objGlissiere->presentation_1_image_1)) { ?>
                                        <?php
                                        if (file_exists($path_img_gallery . $objGlissiere->presentation_1_image_1) == true) {
                                            echo '<img  src="' . base_url() . $path_img_gallery . $objGlissiere->presentation_1_image_1 . '" width="200"/>';
                                        } else {
                                            echo '<img  src="' . base_url() . $path_img_gallery_old . $objGlissiere->presentation_1_image_1 . '" width="200"/>';
                                        }
                                        ?>
                                        <a href="javascript:void(0);" class="btn btn-danger"
                                           onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','presentation_1_image_1');">Supprimer</a>
                                    <?php } else { ?>
                                        <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-gli-presentation_1_image_1"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                           href='javascript:void(0);' title="presentation_1_image_1"
                                           class="btn btn-info" id="presentation_1_image_1_link">Ajouter une Photo du
                                            glissière</a>
                                    <?php } ?>
                                </div>
                                <input type="hidden" name="presentation_1_image_1" id="presentation_1_image_1"
                                       value="<?php if (isset($objGlissiere->presentation_1_image_1)) echo htmlspecialchars($objGlissiere->presentation_1_image_1); ?>"/>
                                <div id="div_error_taille_activite1_image1"
                                     class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite1_image1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>

                            </div>
                        </div>


                        <div id="gl1_tittle2">
                            <div colspan="2">
                                <label class="label">Contenu glissière1 champ1: </label>
                            </div>
                        </div>
                        <div id="gl1_content">
                            <div colspan="2">

                                <textarea name="Glissiere[presentation_1_contenu1]"
                                          id="presentation_1_contenu"><?php if (isset($objGlissiere->presentation_1_contenu1)) echo htmlspecialchars($objGlissiere->presentation_1_contenu1); ?></textarea>
                                <script>

                                    CKEDITOR.replace('presentation_1_contenu');

                                </script>
                            </div>

                        </div>
                        <div style="border-bottom: double;padding-bottom: 50px;">
                            <div>
                                <div class="col-lg-12" id="btn_gli1"
                                     style="background-color: #3EB4D7;height: 40px;padding-top: 12px;color: black">
                                    <span style="color: white!important;">Ajouter un Bouton</span> <span
                                            style="float: right;">
                                                            <select name="Glissiere[is_activ_btn_glissiere1_champ1]"
                                                                    style="margin-right: 10px;color: black!important;"
                                                                    id="link_btn_gli1">
                                                                <option <?php if ($objGlissiere->is_activ_btn_glissiere1_champ1 == 0) {
                                                                    echo 'selected';
                                                                } ?> value="0">non</option>
                                                                <option <?php if ($objGlissiere->is_activ_btn_glissiere1_champ1 == 1) {
                                                                    echo 'selected';
                                                                } ?> value="1">oui</option>
                                                            </select></span>
                                </div>
                                <div class="text-center" id="alllink">
                                    <label for="btn_gli1_content1">Texte du bouton</label>
                                    <input class="form-control" id="btn_gli1_content1" type="text"
                                           value="<?php if (isset($objGlissiere->btn_gli1_content1) AND $objGlissiere->btn_gli1_content1 != "") {
                                               echo $objGlissiere->btn_gli1_content1;
                                           } ?>" name="Glissiere[btn_gli1_content1]">
                                    <span class="text-center" style="">
                                                         <label for="valuelink">Lien du bouton</label><select
                                                name="Glissiere[gl1_existed_link1]" class="form-control"
                                                style="margin-right: 10px;color: black" id="valuelink">
                                                                <option <?php if (isset($objGlissiere->gl1_existed_link1) AND $objGlissiere->gl1_existed_link1 == '0') {
                                                                    echo 'selected';
                                                                } ?> value="0">Choisir un lien local1</option>
                                                                <option <?php if (isset($objGlissiere->gl1_existed_link1) AND $objGlissiere->gl1_existed_link1 == 'p1') {
                                                                    echo 'selected';
                                                                } ?> value="p1">Vers la page 1</option>
                                                                <option <?php if (isset($objGlissiere->gl1_existed_link1) AND $objGlissiere->gl1_existed_link1 == 'p2') {
                                                                    echo 'selected';
                                                                } ?> value="p2">Vers la page 2</option>
                                                                <option <?php if (isset($objGlissiere->gl1_existed_link1) AND $objGlissiere->gl1_existed_link1 == 'ag') {
                                                                    echo 'selected';
                                                                } ?> value="ag">Mes Agendas</option>
                                                                <option <?php if (isset($objGlissiere->gl1_existed_link1) AND $objGlissiere->gl1_existed_link1 == 'art') {
                                                                    echo 'selected';
                                                                } ?> value="art">Mes Articles</option>
                                                                <option <?php if (isset($objGlissiere->gl1_existed_link1) AND $objGlissiere->gl1_existed_link1 == 'bp') {
                                                                    echo 'selected';
                                                                } ?> value="bp">Mes Bons Plans</option>
                                                            </select></span><br>
                                    Ou ajouter un lien externe<br>
                                    <div class="col-lg-6 text-left pl-0" style="padding-left: 0px!important;">
                                        <select class="text-center form-control" id="htt">
                                            <option <?php if (preg_match('/http:/', $objGlissiere->gl1_custom_link1)) {
                                                echo 'selected';
                                            } ?> value="http://">http://
                                            </option>
                                            <option <?php if (preg_match('/http:/', $objGlissiere->gl1_custom_link1)) {
                                                echo 'selected';
                                            } ?> value="https://">https://
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 text-right pr0" style="padding-right: 0px!important;">
                                        <input onchange="sub_link_custom1()" name="Glissiere[gl1_custom_link1]"
                                               class="form-control text-center" type="text" value="" id="link_htt"
                                               placeholder="votre liens ici">
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="img_tab" id="gl1_img2">
                            <div style="background-color: #3EB4D7;padding-top: 13px;height: 40px;color: white;margin-bottom: 20px"
                                 class="text-center">Glissière1 champ2
                            </div>
                            <div class="stl_long_input_platinum_td">
                                <label class="label">Ajouter une image : </label>
                            </div>
                            <div>

                                <div id="Articleactivite1_image1_container">
                                    <?php if (!empty($objGlissiere->presentation_1_image_2)) { ?>
                                        <?php
                                        if (file_exists($path_img_gallery . $objGlissiere->presentation_1_image_2) == true) {
                                            echo '<img  src="' . base_url() . $path_img_gallery . $objGlissiere->presentation_1_image_2 . '" width="200"/>';
                                        } else {
                                            echo '<img  src="' . base_url() . $path_img_gallery_old . $objGlissiere->presentation_1_image_2 . '" width="200"/>';
                                        }
                                        ?>
                                        <a href="javascript:void(0);" class="btn btn-danger"
                                           onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','presentation_1_image_2');">Supprimer</a>
                                    <?php } else { ?>
                                        <a onclick='javascript:window.open("<?php echo site_url("media/index/" . $objCommercant->IdCommercant . "-gli-presentation_1_image_2"); ?>", "Commercant image", "width=1045, height=725, scrollbars=yes");'
                                           href='javascript:void(0);' title="presentation_1_image_2"
                                           class="btn btn-info" id="presentation_1_image_2_link">Ajouter une Photo du
                                            glissière</a>
                                    <?php } ?>
                                </div>
                                <input type="hidden" name="presentation_1_image_2" id="presentation_1_image_2"
                                       value="<?php if (isset($objGlissiere->presentation_1_image_2)) echo htmlspecialchars($objGlissiere->presentation_1_image_2); ?>"/>
                                <div id="div_error_taille_activite1_image2"
                                     class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite1_image1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>

                            </div>
                        </div>


                        <div id="gl1_tittle2">
                            <div colspan="2">
                                <label class="label">Contenu glissière1 champ2: </label>
                            </div>
                        </div>
                        <div id="gl1_content2">
                            <div colspan="2">
                                <textarea name="Glissiere[presentation_1_contenu2]"
                                          id="presentation_1_contenu2"><?php if (isset($objGlissiere->presentation_1_contenu2)) echo htmlspecialchars($objGlissiere->presentation_1_contenu2); ?></textarea>
                                <script>

                                    CKEDITOR.replace('presentation_1_contenu2');

                                </script>
                            </div>

                        </div>
                        <div>
                            <div>
                                <div class="col-lg-12" id="btn_gli2"
                                     style="background-color: #3EB4D7;height: 40px;padding-top: 12px;color: black">
                                    <span style="color: white!important;">Ajouter un Bouton</span> <span
                                            style="float: right;">
                                                            <select name="Glissiere[is_activ_btn_glissiere1_champ2]"
                                                                    style="margin-right: 10px;color: black!important;"
                                                                    id="link_btn_gli2">
                                                                <option <?php if ($objGlissiere->is_activ_btn_glissiere1_champ2 == 0) {
                                                                    echo 'selected';
                                                                } ?> value="0">non</option>
                                                                <option <?php if ($objGlissiere->is_activ_btn_glissiere1_champ2 == 1) {
                                                                    echo 'selected';
                                                                } ?> value="1">oui</option>
                                                            </select></span>
                                </div>
                                <div class="text-center" id="alllink2">
                                    <label for="btn_gli1_content2">Texte du bouton</label>
                                    <input class="form-control" id="btn_gli1_content2" type="text"
                                           value="<?php if (isset($objGlissiere->btn_gli1_content2) AND $objGlissiere->btn_gli1_content2 != "") {
                                               echo $objGlissiere->btn_gli1_content2;
                                           } ?>" name="Glissiere[btn_gli1_content2]">
                                    <span class="text-center" style="">
                                                         <label for="valuelink2">Lien du bouton</label>
                                                         <select name="Glissiere[gl1_existed_link2]"
                                                                 class="form-control"
                                                                 style="margin-right: 10px;color: black"
                                                                 id="valuelink2">
                                                                <option <?php if (isset($objGlissiere->gl1_existed_link2) AND $objGlissiere->gl1_existed_link2 == '0') {
                                                                    echo 'selected';
                                                                } ?> value="0">Choisir un lien local2</option>
                                                                <option <?php if (isset($objGlissiere->gl1_existed_link2) AND $objGlissiere->gl1_existed_link2 == 'p1') {
                                                                    echo 'selected';
                                                                } ?> value="p1">Vers la page 1</option>
                                                                <option <?php if (isset($objGlissiere->gl1_existed_link2) AND $objGlissiere->gl1_existed_link2 == 'p2') {
                                                                    echo 'selected';
                                                                } ?> value="p2">Vers la page 2</option>
                                                                <option <?php if (isset($objGlissiere->gl1_existed_link2) AND $objGlissiere->gl1_existed_link2 == 'ag') {
                                                                    echo 'selected';
                                                                } ?> value="ag">Mes Agendas</option>
                                                                <option <?php if (isset($objGlissiere->gl1_existed_link2) AND $objGlissiere->gl1_existed_link2 == 'art') {
                                                                    echo 'selected';
                                                                } ?> value="art">Mes Articles</option>
                                                                <option <?php if (isset($objGlissiere->gl1_existed_link2) AND $objGlissiere->gl1_existed_link2 == 'bp') {
                                                                    echo 'selected';
                                                                } ?> value="bp">Mes Bons Plans</option>
                                                         </select>
                                                     </span><br>
                                    Ou ajouter un lien externe<br>
                                    <div class="col-lg-6 text-left" style="padding-left: 0px!important;">
                                        <select class="form-control text-left" id="htt2">
                                            <option <?php if (preg_match('/http:/', $objGlissiere->gl1_custom_link2)) {
                                                echo 'selected';
                                            } ?> value="http://">http://
                                            </option>
                                            <option <?php if (preg_match('/https:/', $objGlissiere->gl1_custom_link2)) {
                                                echo 'selected';
                                            } ?> value="https://">https://
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 text-right pr-0" style="padding-right: 0px!important;">
                                        <input onchange="sub_link_custom2()" name="Glissiere[gl1_custom_link2]"
                                               class="form-control text-center" type="text" value="" id="link_htt2"
                                               placeholder="votre liens ici">
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>


                <div class="row" id="div_pro_glissiere2_home"
                     <?php if (isset($user_groups) && $user_groups->id == '5') { ?>style="display:none;"<?php } ?>>
                    <div class="col-lg-12"
                         style="background-color: #3653A3;
            color: #FFFFFF;
                    font-size: 13px;
                    font-weight: 700;
                    line-height: 1.23em;
                    margin-top: 12px;
                    margin-bottom: 15px;height: 40px; padding-top: 12px;"><?php if (isset($user_groups) && $user_groups->id == '5') { ?>Glissi&egrave;re 2 page présentation<?php } else { ?>Informations glissi&egrave;re 2<?php } ?>
                        <span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Glissiere[isActive_presentation_2]" id="isActive_presentation_2" style="color:#000000;">
        <option value="0"
                <?php if (isset($objGlissiere->isActive_presentation_2) AND $objGlissiere->isActive_presentation_2 == '0') { ?>selected="selected"<?php } ?>>non</option>
        <option value="1"
                <?php if (isset($objGlissiere->isActive_presentation_2) AND $objGlissiere->isActive_presentation_2 == '1') { ?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
                    </div>
                    <div id="glissiere2content" cellspacing="0" cellpadding="0" style="display: none; width:100%;">
                        <div>
                            <div class="stl_long_input_platinum_td">
                                <label class="label">Titre : </label>
                            </div>
                            <div>
                                <input type="text" style="width:413px;" name="Glissiere[presentation_2_titre]"
                                       id="presentation_2_titre"
                                       value="<?php if (isset($objGlissiere->presentation_2_titre)) echo htmlspecialchars($objGlissiere->presentation_2_titre); ?>"/>
                            </div>
                        </div>
                        <div>
                            <div colspan="2">
                                <label class="label">Contenu glissière2 champ1: </label>
                            </div>
                        </div>
                        <div>
                            <div colspan="2">
                                <textarea name="Glissiere[presentation_2_contenu1]"
                                          id="presentation_2_contenu"><?php if (isset($objGlissiere->presentation_2_contenu1)) echo htmlspecialchars($objGlissiere->presentation_2_contenu1); ?></textarea>
                                <script>

                                    CKEDITOR.replace('presentation_2_contenu');

                                </script>
                            </div>
                        </div>
                        <div>
                            <div colspan="2">
                                <label class="label">Contenu glissière2 champ2 : </label>
                            </div>
                        </div>
                        <div>
                            <div colspan="2">
                                <textarea name="Glissiere[presentation_2_contenu2]"
                                          id="presentation_2_contenu2"><?php if (isset($objGlissiere->presentation_2_contenu2)) echo htmlspecialchars($objGlissiere->presentation_2_contenu2); ?></textarea>
                                <script>

                                    CKEDITOR.replace('presentation_2_contenu2');

                                </script>
                            </div>
                        </div>
                        <div>
                            <div colspan="2">
                                <label class="label">Contenu glissière2 champ3 : </label>
                            </div>
                        </div>
                        <div>
                            <div colspan="2">
                                <textarea name="Glissiere[presentation_2_contenu3]"
                                          id="presentation_2_contenu3"><?php if (isset($objGlissiere->presentation_2_contenu3)) echo htmlspecialchars($objGlissiere->presentation_2_contenu3); ?></textarea>
                                <script>

                                    CKEDITOR.replace('presentation_2_contenu3');

                                </script>
                            </div>
                        </div>
                        <div>
                            <div colspan="2">
                                <label class="label">Contenu glissière2 champ4 : </label>
                            </div>
                        </div>
                        <div>
                            <div colspan="2">
                                <textarea name="Glissiere[presentation_2_contenu4]"
                                          id="presentation_2_contenu4"><?php if (isset($objGlissiere->presentation_2_contenu4)) echo htmlspecialchars($objGlissiere->presentation_2_contenu4); ?></textarea>
                                <script>

                                    CKEDITOR.replace('presentation_2_contenu4');

                                </script>
                            </div>
                        </div>
                    </div>
                </div>


                <div id="div_pro_glissiere3_home"
                     <?php if (isset($user_groups) && $user_groups->id == '5') { ?>style="display:none;"<?php } ?>>
                    <div class="div_stl_long_platinum"
                         style="height: 40px; padding-top: 12px;"><?php if (isset($user_groups) && $user_groups->id == '5') { ?>Glissi&egrave;re 3 page présentation<?php } else { ?>Informations glissi&egrave;re 3<?php } ?>
                        <span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Glissiere[isActive_presentation_3]" id="isActive_presentation_3" style="color:#000000;">
        <option value="0"
                <?php if (isset($objGlissiere->isActive_presentation_3) AND $objGlissiere->isActive_presentation_3 == '0') { ?>selected="selected"<?php } ?>>non</option>
        <option value="1"
                <?php if (isset($objGlissiere->isActive_presentation_3) AND $objGlissiere->isActive_presentation_3 == '1') { ?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
                    </div>
                    <table id="glissiere3content" cellspacing="0" cellpadding="0" style="display: none; width:100%;">
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Titre : </label>
                            </td>
                            <td>
                                <input type="text" style="width:413px;" name="Glissiere[presentation_3_titre]"
                                       id="presentation_3_titre"
                                       value="<?php if (isset($objGlissiere->presentation_3_titre)) echo htmlspecialchars($objGlissiere->presentation_3_titre); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière3 champ1 : </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[presentation_3_contenu1]"
                                          id="presentation_3_contenu"><?php if (isset($objGlissiere->presentation_3_contenu1)) echo htmlspecialchars($objGlissiere->presentation_3_contenu1); ?></textarea>
                                <script>

                                    CKEDITOR.replace('presentation_3_contenu');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière3 champ2: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[presentation_3_contenu2]"
                                          id="presentation_3_contenu2"><?php if (isset($objGlissiere->presentation_3_contenu2)) echo htmlspecialchars($objGlissiere->presentation_3_contenu2); ?></textarea>
                                <script>

                                    CKEDITOR.replace('presentation_3_contenu2');

                                </script>
                            </td>
                        </tr>

                    </table>
                </div>


                <div id="div_pro_glissiere4_home"
                     <?php if (isset($user_groups) && $user_groups->id == '5') { ?>style="display:none;"<?php } ?>>
                    <div class="div_stl_long_platinum"
                         style="height: 40px; padding-top: 12px;"><?php if (isset($user_groups) && $user_groups->id == '5') { ?>Glissi&egrave;re 4 page présentation<?php } else { ?>Informations glissi&egrave;re 4<?php } ?>
                        <span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Glissiere[isActive_presentation_4]" id="isActive_presentation_4" style="color:#000000;">
        <option value="0"
                <?php if (isset($objGlissiere->isActive_presentation_4) AND $objGlissiere->isActive_presentation_4 == '0') { ?>selected="selected"<?php } ?>>non</option>
        <option value="1"
                <?php if (isset($objGlissiere->isActive_presentation_4) AND $objGlissiere->isActive_presentation_4 == '1') { ?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
                    </div>
                    <table id="glissiere4content" cellspacing="0" cellpadding="0" style="display: none; width:100%;">
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Titre : </label>
                            </td>
                            <td>
                                <input type="text" style="width:413px;" name="Glissiere[presentation_4_titre]"
                                       id="presentation_4_titre"
                                       value="<?php if (isset($objGlissiere->presentation_4_titre)) echo htmlspecialchars($objGlissiere->presentation_4_titre); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière4 champ1: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[presentation_4_contenu1]"
                                          id="presentation_4_contenu"><?php if (isset($objGlissiere->presentation_4_contenu1)) echo htmlspecialchars($objGlissiere->presentation_4_contenu1); ?></textarea>
                                <script>

                                    CKEDITOR.replace('presentation_4_contenu');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière4 champ2: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[presentation_4_contenu2]"
                                          id="presentation_4_contenu2"><?php if (isset($objGlissiere->presentation_4_contenu2)) echo htmlspecialchars($objGlissiere->presentation_4_contenu2); ?></textarea>
                                <script>

                                    CKEDITOR.replace('presentation_4_contenu2');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière4 champ3: </label>
                            </td>
                        </tr>
                    </table>
                </div>


                <div id="div_pro_glissiere1_page1" style="display:none;">
                    <div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px;">Glissi&egrave;re 1 page
                        1
                        <span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Glissiere[isActive_page1_1]" id="isActive_page1_1" style="color:#000000;">
        <option value="0"
                <?php if (isset($objGlissiere->isActive_page1_1) AND $objGlissiere->isActive_page1_1 == '0') { ?>selected="selected"<?php } ?>>non</option>
        <option value="1"
                <?php if (isset($objGlissiere->isActive_page1_1) AND $objGlissiere->isActive_page1_1 == '1') { ?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
                    </div>
                    <table cellspacing="0" cellpadding="0" style=" width:100%;">
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Titre : </label>
                            </td>
                            <td>
                                <input type="text" style="width:413px;" name="Glissiere[page1_1_titre]"
                                       id="page1_1_titre"
                                       value="<?php if (isset($objGlissiere->page1_1_titre)) echo htmlspecialchars($objGlissiere->page1_1_titre); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière1 champ1 : </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page1_1_contenu1]"
                                          id="page1_1_contenu"><?php if (isset($objGlissiere->page1_1_contenu1)) echo htmlspecialchars($objGlissiere->page1_1_contenu1); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page1_1_contenu');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière1 champ2 : </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page1_1_contenu2]"
                                          id="page1_1_contenu2"><?php if (isset($objGlissiere->page1_1_contenu2)) echo htmlspecialchars($objGlissiere->page1_1_contenu2); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page1_1_contenu2');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière1 champ3 : </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page1_1_contenu3]"
                                          id="page1_1_contenu3"><?php if (isset($objGlissiere->page1_1_contenu3)) echo htmlspecialchars($objGlissiere->page1_1_contenu3); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page1_1_contenu3');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière1 champ4 : </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page1_1_contenu4]"
                                          id="page1_1_contenu4"><?php if (isset($objGlissiere->page1_1_contenu4)) echo htmlspecialchars($objGlissiere->page1_1_contenu4); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page1_1_contenu4');

                                </script>
                            </td>
                        </tr>
                    </table>
                </div>


                <div id="div_pro_glissiere2_page1" style="display:none;">
                    <div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px;">Glissi&egrave;re 2 page
                        1
                        <span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Glissiere[isActive_page1_2]" id="isActive_page1_2" style="color:#000000;">
        <option value="0"
                <?php if (isset($objGlissiere->isActive_page1_2) AND $objGlissiere->isActive_page1_2 == '0') { ?>selected="selected"<?php } ?>>non</option>
        <option value="1"
                <?php if (isset($objGlissiere->isActive_page1_2) AND $objGlissiere->isActive_page1_2 == '1') { ?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
                    </div>
                    <table cellspacing="0" cellpadding="0" style=" width:100%;">
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Titre : </label>
                            </td>
                            <td>
                                <input type="text" style="width:413px;" name="Glissiere[page1_2_titre]"
                                       id="page1_2_titre"
                                       value="<?php if (isset($objGlissiere->page1_2_titre)) echo htmlspecialchars($objGlissiere->page1_2_titre); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière2 champ1: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page1_2_contenu1]"
                                          id="page1_2_contenu"><?php if (isset($objGlissiere->page1_2_contenu1)) echo htmlspecialchars($objGlissiere->page1_2_contenu1); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page1_2_contenu');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière2 champ2: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page1_2_contenu2]"
                                          id="page1_2_contenu2"><?php if (isset($objGlissiere->page1_2_contenu2)) echo htmlspecialchars($objGlissiere->page1_2_contenu2); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page1_2_contenu2');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière2 champ3: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page1_2_contenu3]"
                                          id="page1_2_contenu3"><?php if (isset($objGlissiere->page1_2_contenu3)) echo htmlspecialchars($objGlissiere->page1_2_contenu3); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page1_2_contenu3');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière2 champ4: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page1_2_contenu4]"
                                          id="page1_2_contenu4"><?php if (isset($objGlissiere->page1_2_contenu4)) echo htmlspecialchars($objGlissiere->page1_2_contenu4); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page1_2_contenu4');

                                </script>
                            </td>
                        </tr>
                    </table>
                </div>


                <div id="div_pro_glissiere3_page1" style="display:none;">
                    <div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px;">Glissi&egrave;re 3 page
                        1
                        <span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Glissiere[isActive_page1_3]" id="isActive_page1_3" style="color:#000000;">
        <option value="0"
                <?php if (isset($objGlissiere->isActive_page1_3) AND $objGlissiere->isActive_page1_3 == '0') { ?>selected="selected"<?php } ?>>non</option>
        <option value="1"
                <?php if (isset($objGlissiere->isActive_page1_3) AND $objGlissiere->isActive_page1_3 == '1') { ?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
                    </div>
                    <table cellspacing="0" cellpadding="0" style=" width:100%;">
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Titre : </label>
                            </td>
                            <td>
                                <input type="text" style="width:413px;" name="Glissiere[page1_3_titre]"
                                       id="page1_3_titre"
                                       value="<?php if (isset($objGlissiere->page1_3_titre)) echo htmlspecialchars($objGlissiere->page1_3_titre); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière4 champ1: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page1_3_contenu1]"
                                          id="page1_3_contenu"><?php if (isset($objGlissiere->page1_3_contenu1)) echo htmlspecialchars($objGlissiere->page1_3_contenu1); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page1_3_contenu');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière4 champ2: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page1_3_contenu2]"
                                          id="page1_3_contenu2"><?php if (isset($objGlissiere->page1_3_contenu2)) echo htmlspecialchars($objGlissiere->page1_3_contenu2); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page1_3_contenu2');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière4 champ3: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page1_3_contenu3]"
                                          id="page1_3_contenu3"><?php if (isset($objGlissiere->page1_3_contenu3)) echo htmlspecialchars($objGlissiere->page1_3_contenu3); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page1_3_contenu3');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière4 champ4: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page1_3_contenu4]"
                                          id="page1_3_contenu4"><?php if (isset($objGlissiere->page1_3_contenu4)) echo htmlspecialchars($objGlissiere->page1_3_contenu4); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page1_3_contenu4');

                                </script>
                            </td>
                        </tr>
                    </table>
                </div>


                <div id="div_pro_glissiere4_page1" style="display:none;">
                    <div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px;">Glissi&egrave;re 4 page
                        1
                        <span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Glissiere[isActive_page1_4]" id="isActive_page1_4" style="color:#000000;">
        <option value="0"
                <?php if (isset($objGlissiere->isActive_page1_4) AND $objGlissiere->isActive_page1_4 == '0') { ?>selected="selected"<?php } ?>>non</option>
        <option value="1"
                <?php if (isset($objGlissiere->isActive_page1_4) AND $objGlissiere->isActive_page1_4 == '1') { ?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
                    </div>
                    <table cellspacing="0" cellpadding="0" style=" width:100%;">
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Titre : </label>
                            </td>
                            <td>
                                <input type="text" style="width:413px;" name="Glissiere[page1_4_titre]"
                                       id="page1_4_titre"
                                       value="<?php if (isset($objGlissiere->page1_4_titre)) echo htmlspecialchars($objGlissiere->page1_4_titre); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière4 champ1: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page1_4_contenu1]"
                                          id="page1_4_contenu"><?php if (isset($objGlissiere->page1_4_contenu1)) echo htmlspecialchars($objGlissiere->page1_4_contenu1); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page1_4_contenu');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière4 champ2: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page1_4_contenu2]"
                                          id="page1_4_contenu2"><?php if (isset($objGlissiere->page1_4_contenu2)) echo htmlspecialchars($objGlissiere->page1_4_contenu2); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page1_4_contenu2');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière4 champ3: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page1_4_contenu3]"
                                          id="page1_4_contenu3"><?php if (isset($objGlissiere->page1_4_contenu3)) echo htmlspecialchars($objGlissiere->page1_4_contenu3); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page1_4_contenu3');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière4 champ4: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page1_4_contenu4]"
                                          id="page1_4_contenu4"><?php if (isset($objGlissiere->page1_4_contenu4)) echo htmlspecialchars($objGlissiere->page1_4_contenu4); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page1_4_contenu4');

                                </script>
                            </td>
                        </tr>
                    </table>
                </div>


                <div id="div_pro_glissiere1_page2" style="display:none;">
                    <div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px;">Glissi&egrave;re 1 page
                        2
                        <span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Glissiere[isActive_page2_1]" id="isActive_page2_1" style="color:#000000;">
        <option value="0"
                <?php if (isset($objGlissiere->isActive_page2_1) AND $objGlissiere->isActive_page2_1 == '0') { ?>selected="selected"<?php } ?>>non</option>
        <option value="1"
                <?php if (isset($objGlissiere->isActive_page2_1) AND $objGlissiere->isActive_page2_1 == '1') { ?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
                    </div>
                    <table cellspacing="0" cellpadding="0" style=" width:100%;">
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Titre : </label>
                            </td>
                            <td>
                                <input type="text" style="width:413px;" name="Glissiere[page2_1_titre]"
                                       id="page2_1_titre"
                                       value="<?php if (isset($objGlissiere->page2_1_titre)) echo htmlspecialchars($objGlissiere->page2_1_titre); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière1 champ1: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page2_1_contenu1]"
                                          id="page2_1_contenu"><?php if (isset($objGlissiere->page2_1_contenu1)) echo htmlspecialchars($objGlissiere->page2_1_contenu1); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page2_1_contenu');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière1 champ2: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page2_1_contenu2]"
                                          id="page2_1_contenu2"><?php if (isset($objGlissiere->page2_1_contenu2)) echo htmlspecialchars($objGlissiere->page2_1_contenu2); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page2_1_contenu2');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière1 champ3: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page2_1_contenu3]"
                                          id="page2_1_contenu3"><?php if (isset($objGlissiere->page2_1_contenu3)) echo htmlspecialchars($objGlissiere->page2_1_contenu3); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page2_1_contenu3');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière1 champ4: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page2_1_contenu4]"
                                          id="page2_1_contenu4"><?php if (isset($objGlissiere->page2_1_contenu4)) echo htmlspecialchars($objGlissiere->page2_1_contenu4); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page2_1_contenu4');

                                </script>
                            </td>
                        </tr>
                    </table>
                </div>


                <div id="div_pro_glissiere2_page2" style="display:none;">
                    <div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px;">Glissi&egrave;re 2 page
                        2
                        <span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Glissiere[isActive_page2_2]" id="isActive_page2_2" style="color:#000000;">
        <option value="0"
                <?php if (isset($objGlissiere->isActive_page2_2) AND $objGlissiere->isActive_page2_2 == '0') { ?>selected="selected"<?php } ?>>non</option>
        <option value="1"
                <?php if (isset($objGlissiere->isActive_page2_2) AND $objGlissiere->isActive_page2_2 == '1') { ?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
                    </div>
                    <table cellspacing="0" cellpadding="0" style=" width:100%;">
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Titre : </label>
                            </td>
                            <td>
                                <input type="text" style="width:413px;" name="Glissiere[page2_2_titre]"
                                       id="page2_2_titre"
                                       value="<?php if (isset($objGlissiere->page2_2_titre)) echo htmlspecialchars($objGlissiere->page2_2_titre); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissiere2 champ1: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page2_2_contenu1]"
                                          id="page2_2_contenu"><?php if (isset($objGlissiere->page2_2_contenu1)) echo htmlspecialchars($objGlissiere->page2_2_contenu1); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page2_2_contenu');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissiere2 champ2: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page2_2_contenu2]"
                                          id="page2_2_contenu2"><?php if (isset($objGlissiere->page2_2_contenu2)) echo htmlspecialchars($objGlissiere->page2_2_contenu2); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page2_2_contenu2');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissiere2 champ3: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page2_2_contenu3]"
                                          id="page2_2_contenu3"><?php if (isset($objGlissiere->page2_2_contenu3)) echo htmlspecialchars($objGlissiere->page2_2_contenu3); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page2_2_contenu3');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissiere2 champ4: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page2_2_contenu4]"
                                          id="page2_2_contenu4"><?php if (isset($objGlissiere->page2_2_contenu4)) echo htmlspecialchars($objGlissiere->page2_2_contenu4); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page2_2_contenu4');

                                </script>
                            </td>
                        </tr>
                    </table>
                </div>


                <div id="div_pro_glissiere3_page2" style="display:none;">
                    <div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px;">Glissi&egrave;re 3 page
                        2
                        <span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Glissiere[isActive_page2_3]" id="isActive_page2_3" style="color:#000000;">
        <option value="0"
                <?php if (isset($objGlissiere->isActive_page2_3) AND $objGlissiere->isActive_page2_3 == '0') { ?>selected="selected"<?php } ?>>non</option>
        <option value="1"
                <?php if (isset($objGlissiere->isActive_page2_3) AND $objGlissiere->isActive_page2_3 == '1') { ?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
                    </div>
                    <table cellspacing="0" cellpadding="0" style=" width:100%;">
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Titre : </label>
                            </td>
                            <td>
                                <input type="text" style="width:413px;" name="Glissiere[page2_3_titre]"
                                       id="page2_3_titre"
                                       value="<?php if (isset($objGlissiere->page2_3_titre)) echo htmlspecialchars($objGlissiere->page2_3_titre); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière3 champ1: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page2_3_contenu1]"
                                          id="page2_3_contenu"><?php if (isset($objGlissiere->page2_3_contenu1)) echo htmlspecialchars($objGlissiere->page2_3_contenu1); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page2_3_contenu');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière3 champ2: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page2_3_contenu2]"
                                          id="page2_3_contenu2"><?php if (isset($objGlissiere->page2_3_contenu2)) echo htmlspecialchars($objGlissiere->page2_3_contenu2); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page2_3_contenu2');

                                </script>
                            </td>
                        </tr>
                    </table>
                </div>


                <div id="div_pro_glissiere4_page2" style="display:none;">
                    <div class="div_stl_long_platinum" style="height: 40px; padding-top: 12px;">Glissi&egrave;re 4 page
                        2
                        <span style="float:right; padding-right:15px;">
<label style="color:#FFFFFF !important;">Active :</label>
    <select name="Glissiere[isActive_page2_4]" id="isActive_page2_4" style="color:#000000;">
        <option value="0"
                <?php if (isset($objGlissiere->isActive_page2_4) AND $objGlissiere->isActive_page2_4 == '0') { ?>selected="selected"<?php } ?>>non</option>
        <option value="1"
                <?php if (isset($objGlissiere->isActive_page2_4) AND $objGlissiere->isActive_page2_4 == '1') { ?>selected="selected"<?php } ?>>oui</option>
    </select>
</span>
                    </div>
                    <table cellspacing="0" cellpadding="0" style=" width:100%;">
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Titre : </label>
                            </td>
                            <td>
                                <input type="text" style="width:413px;" name="Glissiere[page2_4_titre]"
                                       id="page2_4_titre"
                                       value="<?php if (isset($objGlissiere->page2_4_titre)) echo htmlspecialchars($objGlissiere->page2_4_titre); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière4 champ1: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page2_4_contenu1]"
                                          id="page2_4_contenu"><?php if (isset($objGlissiere->page2_4_contenu1)) echo htmlspecialchars($objGlissiere->page2_4_contenu1); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page2_4_contenu');

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="label">Contenu glissière4 champ2: </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea name="Glissiere[page2_4_contenu2]"
                                          id="page2_4_contenu2"><?php if (isset($objGlissiere->page2_4_contenu2)) echo htmlspecialchars($objGlissiere->page2_4_contenu2); ?></textarea>
                                <script>

                                    CKEDITOR.replace('page2_4_contenu2');

                                </script>
                            </td>
                        </tr>
                        </tr>
                    </table>
                </div>


                <?php if (isset($page_data) && $page_data == 'coordonnees') echo '</div>'; ?><!--END CONTAINER MON CONTENU-->


                <div style="height: auto; background-color:#3653A3; margin-top:40px; margin-bottom:20px; text-align:center; padding:7px;">
                    <input type="button" class="btnSinscrire btn btn-info" value="Validation des Modifications"/>
                </div>

                <div id="div_error_fiche_pro" class="div_error_fiche_pro"
                     style="color:#FF0000; font-weight:bold; margin-top:20px; margin-bottom:20px; margin-left:40px; margin-right:40px;"></div>

            </form>


            <div style="text-align: center;">
                <table border="0" cellspacing="5" cellpadding="5" style="text-align:center; width:100%;">
                    <tr>
                        <td>
                            <a href="<?php echo site_url($objCommercant->nom_url . "/presentation"); ?>" target="_blank"
                               onClick="var w = window.open(this.href,'_blank','width=1200,height=1200,menubar=no,resizable=no,scrollbars=yes,status=no,toolbar=no'); if( w != null ){ w.focus(); }; return false;"
                               alt="show">
                                <img src="<?php echo GetImagePath("privicarte/"); ?>/visualisation.png" alt="logo"
                                     title="" style="z-index:1;"/>
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>


<?php $this->load->view("privicarte/includes/footer_mini_2"); ?>