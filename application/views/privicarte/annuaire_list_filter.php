<script type="text/javascript">
function check_zMotCle_annuaire_focus() {
    var searchvalue = $("#inputString_zMotCle").val();
	if (searchvalue=="RECHERCHER") $("#inputString_zMotCle").val("");
}
function check_zMotCle_annuaire_blur() {
	var searchvalue = $("#inputString_zMotCle").val();
	if (searchvalue=="") $("#inputString_zMotCle").val("RECHERCHER");
}
</script>


<div class="col-sm-4 paddingleft0"
<?php
if($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL)
{
    echo 'style="width: 30.93%;"';
}
?>
>
    <?php 
	$this_session =& get_instance();
	$this_session->load->library('session');
	$session_zMotCle_verification = $this_session->session->userdata('zMotCle_x');
	?>
    
    <select id="inputStringAvantageHidden_partenaires" size="1" name="inputStringAvantageHidden_partenaires" class="input_filter_agenda" style="text-indent:30px;">
    	<option value="0">Tous les Avantages</option>
        <option value="1" <?php if (isset($inputAvantagePartenaire) && $inputAvantagePartenaire == '1') {?>selected="selected"<?php }?>>Les bons plans</option>
        <option value="2" <?php if (isset($inputAvantagePartenaire) && $inputAvantagePartenaire == '2') {?>selected="selected"<?php }?>>La fid&eacute;lit&eacute;</option>
        <option value="3" <?php if (isset($inputAvantagePartenaire) && $inputAvantagePartenaire == '3') {?>selected="selected"<?php }?>>Les boutiques en ligne</option>
        <option value="4" <?php if (isset($inputAvantagePartenaire) && $inputAvantagePartenaire == '4') {?>selected="selected"<?php }?>>L'agenda</option>
    </select>
    
    
</div>

<div class="col-sm-4 paddingleft0">
	<select id="inputStringOrderByHidden_partenaires" size="1" name="inputStringOrderByHidden_partenaires" class="input_filter_agenda" style="text-indent:125px;">
        <option value="0">TRIER</option>
        <option value="1" <?php if (isset($iOrderBy) && $iOrderBy == '1') {?>selected="selected"<?php }?>>Les partenaires les plus récents</option>
        <option value="2" <?php if (isset($iOrderBy) && $iOrderBy == '2') {?>selected="selected"<?php }?>>Les partenaires les plus visitées</option>
        <option value="3" <?php if (isset($iOrderBy) && $iOrderBy == '3') {?>selected="selected"<?php }?>>Les partenaires les plus anciens</option>
    </select>
</div>
    
<div class="col-sm-4
<?php
if($_SERVER['SERVER_NAME'] == DOMAIN_SORTEZ_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_SORTEZ_GLOBAL)
{
    echo 'paddingleft0';
}
elseif($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL)
{
    echo 'padding0';
}
?>
">
	<input type="text" name="inputString_zMotCle" id="inputString_zMotCle" value="<?php if (isset($session_zMotCle_verification) && $session_zMotCle_verification != "") echo $session_zMotCle_verification; else echo "RECHERCHER"; ?>" class="input_filter_agenda" style="width:80%; text-align:center;" onfocus="javascript:check_zMotCle_annuaire_focus();" onblur="javascript:check_zMotCle_annuaire_blur();"/><button name="zMotCle_to_check_btn" id="inputString_zMotCle_submit"  class="button_filter_agenda_ok"><img src="<?php echo GetImagePath("privicarte/"); ?>/ok_rechercer.png" alt="rechercher"/></button>
</div>