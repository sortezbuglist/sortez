<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("sortez/includes/header_frontoffice_com", $data); ?>
    <div class="container_inscription_pro">

        <?php $this->load->view("sortez/logo_global", $data); ?>

        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/fields.check.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.6.4.js"></script>

        <script type="text/javascript">


            // Use jQuery via $(...)
            $(document).ready(function () {//debut ready fonction


                <?php if (isset($toCommertantAbonnement)) { ?>
                <?php foreach ($toCommertantAbonnement as $objCommertantAbonnement) { ?>
                //$("#check_abonnement_<?php //echo $objCommertantAbonnement->idAbonnement;?>").attr('checked', true);
                document.getElementById("check_abonnement_<?php echo $objCommertantAbonnement->idAbonnement;?>").checked = true;
                $("#check_div_abonnement_<?php echo $objCommertantAbonnement->idAbonnement;?>").html("<?php echo $this->Abonnement->GetById($objCommertantAbonnement->idAbonnement)->tarif;?> &euro;");
                <?php } ?>
                calcmontantht();
                <?php } ?>


            });


            function calcmontantht() {

                var totalmontantht = 0;
                var montanttva = 0;
                var valtva = 0.20;
                var montantttc = 0;

                var $check_abonnement_list = new Array();
                <?php foreach ($colAbonnements as $objAbonnement) { ?>
                if ($('#check_abonnement_<?php echo $objAbonnement->IdAbonnement;?>').attr('checked'))  totalmontantht += parseInt("<?php echo $objAbonnement->tarif;?>");
                <?php } ?>

                //totalmontantht = check_358_premium_value + check_358_platinium_value + check_358_agenda_plus_value + check_358_web_ref1_value + check_358_web_ref_n_value + check_358_restauration_value;
                //alert(totalmontantht);
                $("#divMontantHT").html(totalmontantht + "€");
                $("#hidemontantht").val(totalmontantht + "€");
                //calcmontanttva (parseInt(totalmontantht), valtva);
                //calcmontantttc (parseInt(totalmontantht), montanttva);
                montanttva = totalmontantht * valtva;
                $("#divMontantTVA").html(_roundNumber(montanttva, 2) + "€");
                $("#hidemontanttva").val(_roundNumber(montanttva, 2) + "€");
                montantttc = totalmontantht + montanttva;
                $("#divMontantTTC").html(montantttc + "€");
                $("#hidemontantttc").val(montantttc + "€");
                $("#montantttcvalue_abonnement").val(montantttc);
            }


            function calcmontanttva(totalmontantht, valtva) {
                montanttva = totalmontantht * valtva;
                $("#divMontantTVA").html(_roundNumber(montanttva, 2) + "€");
            }

            //limit decimal
            function _roundNumber(num, dec) {

                return (parseFloat(num)).toFixed(dec);
            }

            function calcmontantttc(totalmontantht, montanttva) {
                montantttc = totalmontantht + montanttva;
                $("#divMontantTTC").html(montantttc + "€");
                $("#montantttcvalue_abonnement").val(montantttc);
            }

            function click_on_checkbox() {
                <?php foreach ($colAbonnements as $objAbonnement) { ?>
                if ($("#check_abonnement_<?php echo $objAbonnement->IdAbonnement;?>").attr('checked')) {
                    //alert("ok");
                    $("#check_div_abonnement_<?php echo $objAbonnement->IdAbonnement;?>").html("<?php echo $objAbonnement->tarif;?> &euro;");
                    calcmontantht();
                } else {
                    //alert("non ok");
                    $("#check_div_abonnement_<?php echo $objAbonnement->IdAbonnement;?>").html("");
                    calcmontantht();
                }
                <?php } ?>
            }


        </script>

        <script type="text/javascript">var blankSrc = "wpscripts/blank.gif";
        </script>


        <style type="text/css">
            .Normal-C-C1 {
                color: #000054;
                font-family: "Vladimir Script", cursive;
                font-size: 48px;
                line-height: 47px;
            }

            .contect_all_data_pro_subscription {
                margin-left: 15px;
                margin-right: 15px;
            }

            .title_sub_pro {
                color: #FFFFFF;
                font-family: "Arial", sans-serif;
                font-size: 15px;
                padding-bottom: 5px;
                padding-top: 5px;
                font-weight: 700;
                line-height: 1.19em;
                text-align: center;
                margin-top: 15px;
                margin-bottom: 15px;
                background-color: #000000;
            }

            .bloc_sub_pro {
                /*background-color:#3653A2;*/
                padding-bottom: 0px;
                padding-top: 5px;
            }

            .space_sub_pro {
                height: 20px;
            }

            .table_sub_pro {
                color: #000000;
                font-family: "Arial", sans-serif;
                font-size: 13px;
                line-height: 1.23em;
            }

            .table_sub_pro tr {
                height: 35px;
            }

            .table_sub_pro_abonnement td {
                border: 2px solid #000000;
            }

            .input_width {
                width: 400px;
            }

            .td_color_1 {
                background-color: #F4F4F4;
            }

            .td_color_2 {
                background-color: #E5E5E5;
            }

            .td_color_3 {
                background-color: #B6B6B6;
            }
        </style>


        <div style="width:100%; text-align:center;">
            <a href="<?php echo site_url('front/utilisateur/contenupro'); ?>"
               class="btn btn-primary">Retour menu</a>
        </div>


        <div style="text-align:center; color:#006600;"><?php if (isset($msg_abonnement)) echo $msg_abonnement; ?></div>


        <div id="contect_all_data_pro_subscription" class="contect_all_data_pro_subscription">


            <?php //var_dump($toCommertantAbonnement);?>


            <form name="frmAbonnementProfessionnel" id="frmAbonnementProfessionnel"
                  action="<?php echo site_url("front/professionnels/updateabonnement"); ?>" method="POST"
                  accept-charset="UTF-8" target="_self" enctype="multipart/form-data" style="margin:0px;">


                <!--<div class="bloc_sub_pro">
                    <div class="title_sub_pro">Validit&eacute; de votre abonnement</div>
                    <div style="padding:15px;">
                        Abonnement &quot;<strong style="text-transform:uppercase;"><?php echo $toAbonnement->Nom; ?></strong>&quot;
                    </div>
                    <div style="padding:15px;">
                        Type &quot;<strong style="text-transform:uppercase;"><?php echo $toAbonnement->type; ?></strong>&quot; valide
                        du <strong><?php echo translate_date_to_fr($toAbonnement->DateDebut); ?></strong> au
                        <strong><?php echo translate_date_to_fr($toAbonnement->DateFin); ?></strong>
                    </div>
                </div>-->


                <div class="bloc_sub_pro">
                    <div class="title_sub_pro">Votre abonnement et les modules qui vous conviennent</div>


                    <style type="text/css">
                        .table_sub_pro_abonnement td table td {
                            padding: 0 15px;
                        }
                    </style>

                    <div class="table_sub_pro">
                        <div style="text-align:center;margin-left: 0px;">
                            <table width="100%" border="1" cellspacing="0" cellpadding="0"
                                   style="color:#000; text-align:center; border:2px solid;"
                                   class="table_sub_pro_abonnement">
                                <tr style="height:25px;">
                                    <td width="366" class="td_color_2"><strong>Description</strong></td>
                                    <td width="102" class="td_color_2"><strong>Montant ht</strong></td>
                                    <td width="102" class="td_color_1">&nbsp;</td>
                                </tr>

                                <?php
                                /*if (isset($toCommertantAbonnement)) {
                                    $abonnement_asked = array();
                                    foreach ($toCommertantAbonnement as $objCommertantAbonnement) {
                                        $abonnement_asked[] = $objCommertantAbonnement->idAbonnement;
                                    }
                                } */
                                ?>
                                <!--<?php //if (in_array($objAbonnement->IdAbonnement, $abonnement_asked)) {?>checked="checked"<?php //} ?>-->

                                <?php foreach ($colAbonnements as $objAbonnement) { ?>
                                    <tr>
                                        <td align="left" class="td_color_2">

                                            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                                   style="border: none !important">
                                                <tr>
                                                    <td style="border: none !important">
                                                        <input
                                                            id="check_abonnement_<?php echo $objAbonnement->IdAbonnement; ?>"
                                                            type="checkbox" onclick="javascript:click_on_checkbox();"
                                                            name="demande_abonnement[<?php echo $objAbonnement->IdAbonnement; ?>]"/>
                                                        <?php echo $objAbonnement->Nom; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="border: none !important"><?php echo $objAbonnement->Description; ?></td>
                                                </tr>
                                            </table>

                                        </td>
                                        <td class="td_color_2"><?php echo $objAbonnement->tarif; ?> €
                                        </td>
                                        <td class="td_color_1">
                                            <div
                                                id="check_div_abonnement_<?php echo $objAbonnement->IdAbonnement; ?>"></div>
                                        </td>
                                    </tr>
                                <?php } ?>


                                <tr>
                                    <td align="left" class="td_color_1" style="text-align:right;">&nbsp;</td>
                                    <td class="td_color_3">
                                        <strong>Montant ht</strong>
                                    </td>
                                    <td class="td_color_3">
                                        <div id="divMontantHT"></div>
                                        <input type="hidden" id="hidemontantht" class="hidemontantht"
                                               name="Message[montantht]" value="0"/></td>
                                </tr>
                                <tr>
                                    <td align="left" class="td_color_1" style="text-align:right;">&nbsp;</td>
                                    <td class="td_color_3">
                                        <strong>T.V.A 20%</strong>
                                    </td>
                                    <td class="td_color_3">
                                        <div id="divMontantTVA"></div>
                                        <input type="hidden" id="hidemontanttva" class="hidemontanttva"
                                               name="Message[montanttva]" value="0"/></td>
                                </tr>
                                <tr>
                                    <td align="left" class="td_color_1" style="text-align:right;">&nbsp;</td>
                                    <td class="td_color_3">
                                        <strong>Montant TTC</strong>
                                    </td>
                                    <td class="td_color_3">
                                        <div id="divMontantTTC"></div>
                                        <input type="hidden" id="hidemontantttc" class="hidemontantttc"
                                               name="Message[montantttc]" value="0"/></td>
                                </tr>
                            </table>
                        </div>
                    </div>


                </div>
                <div class="space_sub_pro">&nbsp;</div>


                <div class="bloc_sub_pro">
                    <div style='
    font-family: "Arial",sans-serif;
    font-size: 13px;
    line-height: 1.23em;'>
                        Validez votre demande, par retour de mail vous recevez une confirmation de
                        réception.<br/><br/>

                        Sous 24h00 après étude de votre demande et validation de notre service, nous vous adresserons un
                        document conforme à votre organisation administrative (précisez ci-dessous la qualité de ce
                        document et vos conditions de règlement) :
                    </div>
                </div>
                <div class="space_sub_pro">&nbsp;</div>


                <div style='
    font-family: "Arial",sans-serif;
    font-size: 12px; font-weight: bold;
    line-height: 1.23em;'>
                    <table border="0" cellspacing="5" cellpadding="5" style="width: 100%;">
                        <tr>
                            <td>
                                <input id="check_2640" type="checkbox" name="Message[devis]" value="Devis"> Un devis
                            </td>
                            <td>
                                <input id="check_2650" type="checkbox" name="Message[proforma]" value="Proforma"> Une
                                facture proforma
                            </td>
                            <td>
                                <input id="check_2660" type="checkbox" name="Message[facture]" value="Facture"> Une
                                facture
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input id="check_264" type="radio" name="Message[Conditions_paiement]" value="Cheque">
                                Ch&eacute;que
                            </td>
                            <td>
                                <input id="check_265" type="radio" name="Message[Conditions_paiement]" value="Virement">
                                Virement
                            </td>
                            <td>
                                <input id="check_266" type="radio" name="Message[Conditions_paiement]"
                                       value="Carte Bancaire avec Paypal"> Carte Bancaire avec Paypal
                            </td>
                        </tr>
                    </table>
                </div>
                <div style='
    font-family: "Arial",sans-serif;
    font-size: 13px; margin-top:20px;
    line-height: 1.23em;'>
                    Dès réception de votre règlement, nous ouvrirons vos droits et vous confirmerons votre identifiant
                    et mot de passe.
                </div>
                <div class="space_sub_pro">&nbsp;</div>


                <div style="text-align:center; margin-top:20px; margin-bottom:20px;">
                    <input id="btnSinscrire" name="envoyer" value="Validation" type="submit" class="btn btn-success" style="display: block; width: 100%;">
                </div>


            </form>

        </div>


        <?php $this->load->view("privicarte/pub_compte", $data); ?>

    </div>


<?php $this->load->view("sortez/includes/footer_frontoffice_com", $data); ?>