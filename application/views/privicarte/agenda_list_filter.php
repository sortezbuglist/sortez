<script type="text/javascript">
function check_zMotCle_agenda_focus() {
    var searchvalue = $("#zMotCle_to_check").val();
	if (searchvalue=="RECHERCHER") $("#zMotCle_to_check").val("");
	//alert("focus");
}	
function check_zMotCle_agenda_blur() {	
	var searchvalue = $("#zMotCle_to_check").val();
	if (searchvalue=="") $("#zMotCle_to_check").val("RECHERCHER");
	//alert("blur");
}  
</script>


<div class="col-sm-4 paddingleft0" style="height: 60px">   
    <?php 
	$this_session =& get_instance();
	$this_session->load->library('session');
	$session_inputStringQuandHidden_verification = $this_session->session->userdata('inputStringQuandHidden_x');
	$session_zMotCle_verification = $this_session->session->userdata('zMotCle_x');
	?>
    
    <select id="inputStringQuandHidden_to_check" size="1" name="inputStringQuandHidden_to_check" onChange="javascript:inputStringQuandHidden_to_check_change();" class="input_filter_agenda" style="padding-left:15px; cursor:pointer;">
        <option value="0" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="0") {?>selected<?php }?>>Toutes les dates <?php if (isset($toAgndaTout_global)) echo "(".$toAgndaTout_global.")"; else echo "(0)"; ?></option>
        <option value="101" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="101") {?>selected<?php }?>>Aujourd'hui <?php if (isset($toAgndaAujourdhui_global)) echo "(".$toAgndaAujourdhui_global.")"; else echo "(0)"; ?></option>
        <option value="202" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="202") {?>selected<?php }?>>Ce Week-end <?php if (isset($toAgndaWeekend_global)) echo "(".$toAgndaWeekend_global.")"; else echo "(0)"; ?></option>
        <option value="303" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="303") {?>selected<?php }?>>Cette semaine <?php if (isset($toAgndaSemaine_global)) echo "(".$toAgndaSemaine_global.")"; else echo "(0)"; ?></option>
        <option value="404" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="404") {?>selected<?php }?>>Semaine prochaine <?php if (isset($toAgndaSemproch_global)) echo "(".$toAgndaSemproch_global.")"; else echo "(0)"; ?></option>
        <!--<option value="505" <?php // if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="505") {?>selected<?php // }?>>Ce mois <?php // if (isset($toAgndaMois_global)) echo "(".$toAgndaMois_global.")"; else echo "(0)"; ?></option>-->
        <option value="01" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="01") {?>selected<?php }?>>Janvier <?php if (isset($toAgndaJanvier_global)) echo "(".$toAgndaJanvier_global.")"; else echo "(0)"; ?></option>
        <option value="02" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="02") {?>selected<?php }?>>Février <?php if (isset($toAgndaFevrier_global)) echo "(".$toAgndaFevrier_global.")"; else echo "(0)"; ?></option>
        <option value="03" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="03") {?>selected<?php }?>>Mars <?php if (isset($toAgndaMars_global)) echo "(".$toAgndaMars_global.")"; else echo "(0)"; ?></option>
        <option value="04" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="04") {?>selected<?php }?>>Avril <?php if (isset($toAgndaAvril_global)) echo "(".$toAgndaAvril_global.")"; else echo "(0)"; ?></option>
        <option value="05" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="05") {?>selected<?php }?>>Mai <?php if (isset($toAgndaMai_global)) echo "(".$toAgndaMai_global.")"; else echo "(0)"; ?></option>
        <option value="06" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="06") {?>selected<?php }?>>Juin <?php if (isset($toAgndaJuin_global)) echo "(".$toAgndaJuin_global.")"; else echo "(0)"; ?></option>
        <option value="07" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="07") {?>selected<?php }?>>Juillet <?php if (isset($toAgndaJuillet_global)) echo "(".$toAgndaJuillet_global.")"; else echo "(0)"; ?></option>
        <option value="08" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="08") {?>selected<?php }?>>Août <?php if (isset($toAgndaAout_global)) echo "(".$toAgndaAout_global.")"; else echo "(0)"; ?></option>
        <option value="09" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="09") {?>selected<?php }?>>Septembre <?php if (isset($toAgndaSept_global)) echo "(".$toAgndaSept_global.")"; else echo "(0)"; ?></option>
        <option value="10" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="10") {?>selected<?php }?>>Octobre <?php if (isset($toAgndaOct_global)) echo "(".$toAgndaOct_global.")"; else echo "(0)"; ?></option>
        <option value="11" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="11") {?>selected<?php }?>>Novembre <?php if (isset($toAgndaNov_global)) echo "(".$toAgndaNov_global.")"; else echo "(0)"; ?></option>
        <option value="12" <?php if (isset($session_inputStringQuandHidden_verification) && $session_inputStringQuandHidden_verification=="12") {?>selected<?php }?>>Décembre <?php if (isset($toAgndaDec_global)) echo "(".$toAgndaDec_global.")"; else echo "(0)"; ?></option>
    </select>
    
</div>

<div class="col-sm-4 paddingleft0" style="height: 60px">
	<select id="inputStringOrderByHidden_to_check" size="1" name="inputStringOrderByHidden_to_check" onChange="javascript:inputStringOrderByHidden_to_check_change();" class="input_filter_agenda" style="<?php if (isset($iOrderBy) && $iOrderBy!='0' && $iOrderBy!='') {} else {?>background-image:url('<?php echo GetImagePath("privicarte/"); ?>/bg_trier.png'); background-repeat:no-repeat; background-position:center center;<?php }?> padding-left: 15px; cursor:pointer;">
        <option value="0"></option>
        <option value="1" <?php if (isset($iOrderBy) && $iOrderBy == "1") echo 'selected="selected"';?>>Les événements les plus récents</option>
        <option value="2" <?php if (isset($iOrderBy) && $iOrderBy == "2") echo 'selected="selected"';?>>Les événements les plus lus</option>
    </select>
</div>
    
<div class="col-sm-4 paddingleft0" style="height: 60px">    
	<input type="text" name="zMotCle_to_check" id="zMotCle_to_check" value="<?php if (isset($session_zMotCle_verification) && $session_zMotCle_verification != "") echo $session_zMotCle_verification; else echo "RECHERCHER"; ?>" class="input_filter_agenda" style="width:80%; text-align:center;" onfocus="javascript:check_zMotCle_agenda_focus();" onblur="javascript:check_zMotCle_agenda_blur();"/><button name="zMotCle_to_check_btn" id="zMotCle_to_check_btn" onClick="javascript:zMotCle_to_check_click();" class="button_filter_agenda_ok"><img src="<?php echo GetImagePath("privicarte/"); ?>/ok_rechercer.png" alt="rechercher"/></button></p>
    
</div>