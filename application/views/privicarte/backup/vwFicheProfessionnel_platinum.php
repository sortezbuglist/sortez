<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("privicarte/includes/header_mini_2_premium", $data); ?>
<?php $this->load->view("privicarte/vwFicheProfessionnel_platinum_js", $data); ?>
<?php $this->load->view("privicarte/vwFicheProfessionnel_platinum_css", $data); ?>
<script type="text/javascript">
    function check_domain_name(){
        document.getElementById("check_domain_name").style.display = 'block';
    };
    function verifie_domain(){
        var nom_domain=$("#imput_domain_name").val();
        var ver = nom_domain.split('.');
            var nbr = ver.length;
            if(nbr > 1){
               data='nom_domain=www.'+nom_domain;
        jQuery.ajax({
                url: "<?php echo site_url('front/professionnels/check_domain_name');?>",
                dataType: 'text',
                type: 'POST',
                data: data,
                success: function (data) {
                    if(data != '0'){
                        document.getElementById("commande_domain_name").style.display = 'block';
                        document.getElementById("erreur_domain_name").style.display = 'none';    
                    }else{
                        document.getElementById("commande_domain_name").style.display = 'none';
                        document.getElementById("erreur_domain_name").style.display = 'block';                         
                    }
                },
                error: function (data) {
                    console.log(data);
                    alert('Une erreur s\'est produite');
                }
            });
            }else{
                alert('Inserez à nouveau...')
            }
        
    }
    function verifier(){
            var cond = document.getElementById('imput_domain_name').value;
            var ver = cond.split('.');
            var nbr = ver.length;
            if(nbr > 1){
                //alert('Verification...')
            }else{
                alert('Nom de domaine invalide...')
            }
    }


    function commande_domain(){
        var nom_domain=$("#imput_domain_name").val();
        var message =  $("#message_commande").val();
        var  TelFixe =  $("#imput_telFix").val();
        var TelMobile =  $("#imput_telMobile").val();
        $('#command_button').html('Envoie en cours ...');
        // alert(message);
        if(TelFixe!=null || TelFixe!=undefined ){
            var TelCom = TelFixe;
        }else{
            var TelCom = TelMobile;
        }
        data='nom_domain='+nom_domain +'&nom_societe='+'<?php echo $objCommercant->NomSociete ?>'+'&message=' +message+'&mail_societe='+'<?php echo $objCommercant->Email; ?>'+'&IdCommercant='+'<?php echo $objCommercant->IdCommercant; ?>'+'&TelCom='+TelCom;


        jQuery.ajax({
                url: "<?php echo site_url('front/professionnels/commande_domaine');?>",
                dataType: 'text',
                type: 'POST',
                data: data,
                success: function (data) {
                   if (data == 'ok' ){
                     $('#command_button').html('Demande bien envoyé');
                   }else{
                      alert('Une erreur s\'est produite');
                   }
                },
                error: function (data) {
                    // console.log(data);
                    alert('Une erreur s\'est produite');
                }
            });
    };
</script>

    <div style="position: fixed;height: auto; background-color:rgba(0,0,0,0);width: 200px;float:right;z-index: 5;right: 0px">
        <input type="button" class="btnSinscrire btn btn-success" value="Validation" style="width: 180px;"/>
    </div>
    <div class="mt-5" style="position:fixed; background-color: rgba(0,0,0,0);width: 200px;float:right;z-index: 5;right: 0px">

        <a href="<?php echo site_url($objCommercant->nom_url . "/presentation"); ?>" target="_blank" alt="show">
            <img src="<?php echo GetImagePath("privicarte/"); ?>/visualisation.png" alt="logo"
                 title="" style="width: 180px"/>
        </a>

    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12" style="padding:0 !important; text-align:center; padding-bottom:30px; display: none;">
                <img
                        src="<?php echo GetImagePath("privicarte/"); ?>/img_form_data.png"/></div>
            <div class="col-lg-12"
                 style="padding:0 !important; font-family: Arial; font-size:20px; text-align:center; font-weight:bold;">
                <div style="padding: 15px;">
                    <?php if ($page_data == "contenus") { ?>
                        Mon contenu
                    <?php } else { ?>
                        Mes coordonnées - Mon abonnement
                    <?php } ?>
                </div>
                <div class="container-fluid"><a href="<?php echo site_url("front/utilisateur/contenupro"); ?>"
                                                class="btn btn-primary"
                                                style="text-decoration:none;">Retour menu</a></div>
            </div>
        </div>
        <div id="divErrorDisplay" class="container-fluid p-0">
            <?php if (isset($mssg) && $mssg == 1) { ?>
                <div class="container-fluid mb-0 mt-3 text-center alert alert-success" role="alert">
                    Les modifications ont &eacute;t&eacute; enregistr&eacute;es !
                </div>
            <?php } ?>
            <?php if (isset($mssg) && $mssg != 1 && $mssg != "") { ?>
                <div class="container-fluid mb-0 mt-3 text-center alert alert-danger" role="alert">
                    Une erreur est constatée, Veuillez vérifier la conformité de vos données !
                </div>
            <?php } ?>
        </div>


        <?php if (isset($user_groups) && $user_groups->id == '5') { ?>
            <?php if (isset($page_data) && $page_data == 'coordonnees') echo '<div style="display:none;">'; ?><!--START CONTAINER MON CONTENU-->
            <div class="container-fluid p-0" style="margin-top:20px; margin-bottom:20px;">
                <div class="row">
                    <div class="col-lg-3 pr-0">
                        <a href="javascript:void(0);" class="w-100 btn btn-primary"
                           onclick="javascript:show_btn_pro_contenus_design();">Design</a>
                    </div>
                    <div class="col-lg-3 pr-0">
                        <a href="javascript:void(0);" class="w-100 btn btn-primary"
                           onclick="javascript:show_btn_pro_contenus_home();">Page d'accueil</a>
                    </div>
                    <div class="col-lg-3 pr-0">
                        <a href="javascript:void(0);" class="w-100 btn btn-primary"
                           onclick="javascript:show_btn_pro_contenus_page1();">Page 1</a>
                    </div>
                    <div class="col-lg-3">
                        <a href="javascript:void(0);" class="w-100 btn btn-primary"
                           onclick="javascript:show_btn_pro_contenus_page2();" id="show_btn_pro_contenus_page2">Page 2</a>
                    </div>
                </div>
            </div>
            <?php if (isset($page_data) && $page_data == 'coordonnees') echo '</div>'; ?><!--END CONTAINER MON CONTENU-->
            <div class="row">
                <div class="col-lg-12" style="color:#000000; font-size:20px; text-align:center; font-weight:bold;">
                    <div id="div_pro_titre_infos"
                         <?php if (isset($page_data) && $page_data == 'coordonnees'){ ?>style="display:none;"<?php } ?>>
                        Design & infos
                    </div>
                    <div id="div_pro_titre_home" style="display:none;">Page d'accueil</div>
                    <div id="div_pro_titre_page1" style="display:none;">Page 1</div>
                    <div id="div_pro_titre_page2" style="display:none;">Page 2</div>
                </div>
            </div>
        <?php } ?>


        <div class="row">

            <form style="" name="frmInscriptionProfessionnel" id="frmInscriptionProfessionnel" class="container-fluid p-0"
                  action="<?php echo site_url("front/professionnels/modifier"); ?>" method="POST"
                  enctype="multipart/form-data">

                <?php
                $data['page_data']=$page_data;
                $data['objAbonnementCommercant']=$objAbonnementCommercant;
                $data['user_groups']=$user_groups;
                $data['objCommercant']=$objCommercant;
                $data['colRubriques']=$colRubriques;
                $data['objGlissiere']=$objGlissiere;
                $data["objbloc_info"] = $objbloc_info;
                $data['path_img_gallery_old']='application/resources/front/photoCommercant/imagesbank/'.$objCommercant->user_ionauth_id.'/';
                $data['path_img_gallery']='application/resources/front/photoCommercant/imagesbank/'.$objCommercant->user_ionauth_id.'/';
                $this->load->view('privicarte/vwfiche_pro/design_page',$data); ?>
                <?php $this->load->view('privicarte/vwfiche_pro/homepage_global',$data) ?>

                <?php if (isset($objAbonnementCommercant) && $user_groups->id == '5') { ?>

                    <?php $this->load->view('privicarte/vwfiche_pro/page1_global',$data) ?>

                    <?php $this->load->view('privicarte/vwfiche_pro/page2_global',$data) ?>

                <?php } ?>

                <?php if (isset($page_data) && $page_data == 'coordonnees') echo '</div>'; ?><!--END CONTAINER MON CONTENU-->


                <?php if (isset($page_data) && $page_data == 'contenus') echo '<div style="display:none;">'; ?><!--START CONTAINER COORDONNEES-->


                <?php if (isset($user_groups) && ($user_groups->id == '5' || $user_groups->id == '4')) { ?>
                    <?php $this->load->view('privicarte/includes/menu_premium'); ?>
                    <!-- champ domaine sitekey secretkey -->
                    <?php //echo  $objCommercant->private_domaine_name; ?>
                    <?php if(isset($objCommercant->private_domaine_name) && $objCommercant->private_domaine_name=="1" ){?>
                    <div style="background-color: rgb(54, 83, 163);margin-bottom: 0px !important;" class="div_stl_long_platinum">Domaine Sortez.org</div>
                    <div class="p-3" style="background-color: rgb(225, 225, 225);">
                        <div class="row " >
                            <div  class="col-2">
                                <img src="https://www.testpriviconcept.ovh/application/resources/privicarte/images/help.png" alt="localisation" style="width: 30px; height: 30px ;border-radius:25px">
                            </div>
                            <style type="text/css">
                                .i_verify:hover{
                                    cursor: pointer;
                                }
                            </style>

                            
                            <div class="col-4 ">
                                1. LE NOM DE VOTRE SITE: 
                            </div>
                            <div class="col-6 " style="font-size:15px ">
                                Choisir un nom de domaine fait partie des premières étapes dans la
                                création d'un site web. Il s'agit certainement de la plus importante mais
                                aussi de la plus délicate. En effet, votre nom de domaine vas vous représenter 
                                sur le web et participer à l' attractivité de votre activité. Cliquez sur " ? "
                                pour plus d' information
                            </div>
                            </div>
                        
                        <div class="row">
                            <div  class="col-2">
                               
                            </div>

                            
                            <div class="col-4 ">
                                2. VERIFIEZ SA DISPONIBILITE: 
                            </div>
                            
                            <div class="i_verify" id="domain_name" onclick="check_domain_name();" class="col-6">
                                <div style="border: 1px solid #3653A3;border-radius: 5px; background-color: #3653A3; width: 250px;text-align: center; text-transform: uppercase;color: white;font-weight: bold;margin-bottom: 10px">
                                 Je vérifie
                                </div>
                               <div style="display: none" id="check_domain_name">
                                <div class="row">
                                    <div class="col-6">
                                        <input type="text" id="imput_domain_name" placeholder="mon_site.com">
                                        <input type="hidden" id="imput_telFix" value="<?php echo $objCommercant->TelFixe; ?>">
                                        <input type="hidden" id="imput_telMobile" value="<?php echo $objCommercant->TelMobile; ?>">
                                    </div>
                                    <div class="col-6">
                                       <div class="i_verify" id="verifie_domain" onclick="verifie_domain();">
                                        <div onclick="verifier()" style="border: 1px solid #3653A3; margin-left: 50px; border-radius: 5px;background-color: #3653A3; width: 70px  ;text-transform: uppercase;font-weight: bold;color: white">
                                             Vérifier
                                        </div>
                                       </div> 
                                    </div>
                                    
                                </div>
                                    
                                </div>
                                <div style="display: none; color: red;" id="erreur_domain_name">
                                   CE NOM DE DOMAINE EST DEJA PRIS
                                </div>
                             </div>
                            
                        </div>
                        
                        <div class="row" style="display: none" id="commande_domain_name">
                            <div class="row">
                           <div  class="col-2"style="padding-left: 30px">
                                <img src="https://www.testpriviconcept.ovh/application/resources/privicarte/images/help.png" alt="localisation" style="width: 30px; height: 30px ;border-radius:25px ;">
                            </div>
                            <div class="col-4" >
                                3.LE COÛT: 
                            </div>
                            <div class="col-6 " style="font-size:15px ">
                                Votre nom de domaine est hébergé sur notre serveur.L'achat et le renouvellement de votre nom de domaine et de son hébergement est inclut à celui de votre abonnement platinium et en est indissociable.

                                Nota: 3 adresses mails personnalisés sont également fournies (exemple:contact@mon-entreprise.fr)
                            </div>
                            </div>
                            
                            <div class="row ">
                            <div class="col-2">
                                
                            </div>
                            <div class="col-4 ">
                                4. PASSEZ COMMANDE: 
                            </div>
                         
                         <div  class="col-6">
                            
                            <a data-fancybox="" data-animation-duration="500" data-src="#dem_domaine" id="ArticlePhoto3_link" class=""><div class="i_verify"  style="border: 1px solid #3653A3;border-radius: 5px; background-color: #3653A3; width: 250px;;text-decoration-color:white;text-transform: uppercase;color: white;font-weight: bold;text-align: center;">
                                    Remplir le formulaire de la commande
                            </div> </a>
                            <div class="w-50" id="dem_domaine" style="display: none;width: 50%!important;">
                               <div class="row text-center">
                                  <div class="col-lg-12">
                                    <h5 style="font-weight: bold"> MA DEMANDE DE NOM DE DOMAINE </h5>
                   <p>Pour effectuer votre demande de nom de domaine merci de remplir et valider le formulaire ci-dessous</p>
                   <p>Nous vous remercions de bien vouloir nous adresser le formulaire ci-dessous.<br>
                <span style="color: red;text-decoration: underline">Attention :</span> Cette demande ne constitue pas une réservation ferme, notre service réception entrera en contact avec vous dans les meilleurs délais pour valider votre réservation.
                Pour toute réservation dans moins de 24 heures, veuillez nous contacter par téléphone.</p>
                <label class="label" for="sender">Vous:</label>
                <input class='form-control' disabled=true id="sender" type="text" value="<?php echo $objCommercant->NomSociete ?>">
                <label class="label" for="mail_sender">Voutre Mail</label>
                <input class='form-control' disabled=true id="mail_sender" type="text" value="<?php echo $objCommercant->Email ?>">
                <label class="label" for="message_commande">Detailler votre demande</label>
                   <textarea id="message_commande" name="message_commande" class="form-control" style="width: 100%; height: 300px;" ></textarea>
                             <div id="command_button" class="i_verify" onclick="commande_domain()" style=" margin-top: 20px; text-align: center; margin-left: 200px; border: 1px solid #3653A3;border-radius: 5px; background-color: #3653A3; width: 250px;;text-decoration-color:white;text-transform: uppercase;color: white;font-weight: bold;text-align: center;">
                                    Valider ma commande
                            </div>
                     </div>
                     
                          </div>
                        </div>
                         </div>
                         </div>

                        </div>
                    </div>
                    <?php } ?>
                    <div style="background-color: rgb(54, 83, 163)" class="div_stl_long_platinum">Sous-domaine Sortez.org</div>

                    <div class="p-3" style="background-color: rgb(225, 225, 225)">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="label" style="padding-bottom:10px;">Titre du site</td>
                                <td><input type="text" name="Societe[subdomain_title]" id="subdomain_title"
                                           value="<?php if (isset($objCommercant->subdomain_title)) echo htmlspecialchars($objCommercant->subdomain_title); ?>"
                                           class="form-control stl_long_input_platinum"/></td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding-bottom:10px;">Le titre apparaît dans les résultats de
                                    recherche et dans la barre de titre du navigateur.
                                </td>
                            </tr>

                        </table>
                    </div>
                    <div class="col-lg-12" style="background-color:#E1E1E1; padding: 15px; margin: 0px 0;">
                        <div class="col-lg-12" style="padding: 15px; font-size: 20px;">Choisissez votre URL à activer
                            !
                        </div>
                        <div class="col-lg-12" style="padding: 0 15px 15px 15px; color: #ff0000;">Note : Un URL ne peut
                            comporter d'éspace ni de caractère spéciaux ni du caractère "_", veuillez utiliser le
                            caractère "-" à la place !
                        </div>
                        <div class="row pl-3">

                            <div class="col-xs-11">
                                <table>
                                    <tr>
                                        <td><input type="radio" id="nom_url_updated_pro_check"
                                                   name="nom_url_updated_pro_check" <?php if ((isset($objCommercant->nom_url_check) && $objCommercant->nom_url_check == "0") || !isset($objCommercant->nom_url_check)) echo 'checked'; ?>></td>
                                        <td class="label">URL Sortez.org : &nbsp;</td>
                                        <td><input type="text" name="nom_url_updated_pro" id="subdomain_url" class="form-control"
                                                   value="<?php if (isset($objCommercant->nom_url)) echo htmlspecialchars($objCommercant->nom_url); ?>"/>
                                        </td>
                                        <td><strong>.sortez.org</strong></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row pl-3">

                            <div class="col-xs-12">
                                <table>
                                    <tr>
                                        <td><input type="radio" id="nom_url_updated_pro_check_vsv"
                                                   name="nom_url_updated_pro_check" <?php if (isset($objCommercant->nom_url_check) && $objCommercant->nom_url_check == "1") echo 'checked'; ?>></td>
                                        <td class="label">URL Vivresaville.fr : &nbsp;</td>
                                        <td><input type="text" name="nom_url_updated_pro_vsv" id="subdomain_url_vsv" class="form-control"
                                                   value="<?php if (isset($objCommercant->nom_url_vsv)) echo htmlspecialchars($objCommercant->nom_url_vsv); ?>"/>
                                        </td>
                                        <td><strong>.vivresaville.fr</strong></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-12" style="padding: 15px;">Note : Il faut 24-48h pour l'activation du
                            sous-domaine après changement !
                        </div>
                        <div class="col-lg-12" style="padding: 15px; text-align: center;"><a href="javascript:void(0);"
                                                                                             id="nom_url_send_activation_btn"
                                                                                             class="btn btn-info">Envoyer
                                une demande d'activation d'URL</a></div>
                        <div id="span_nom_url_send_activation" style="text-align: center;"></div>
                        <input type="hidden" id="subdomain_url_check" name="Societe[nom_url_check]"
                               value="<?php if (isset($objCommercant->nom_url_check)) echo $objCommercant->nom_url_check; else echo '0'; ?>"/>
                    </div>
                    <table style="width: 100%;background-color: rgb(225, 225, 225)">
                        <tr>
                            <td colspan="2"
                                style="text-align:center;padding-bottom:20px; vertical-align: top; width: 50%;">
                                <a href="javascript:void(0);" onclick="javascript:verify_privicarte_subdomain();"
                                   class="btn btn-primary">Verification Sortez.org</a>
                                <span class="span_verify_subdomain" id="span_verify_subdomain"
                                      style="width:100%; text-align:center;"></span>
                                <div style="width:100%; text-align:center; padding:20px;"><?php if (isset($objCommercant->nom_url)) { ?>
                                    <a
                                            href="http://<?php echo htmlspecialchars($objCommercant->nom_url); ?>.sortez.org"
                                            target="_blank">
                                        http://<?php echo htmlspecialchars($objCommercant->nom_url); ?>
                                        .sortez.org</a><?php } ?></div>
                            </td>
                            <td colspan="2" style="text-align:center;padding-bottom:20px; vertical-align: top;">
                                <a href="javascript:void(0);" onclick="javascript:verify_privicarte_subdomain_vsv();"
                                   class="btn btn-primary">Verification Vivresaville.fr</a>
                                <span class="span_verify_subdomain" id="span_verify_subdomain_vsv"
                                      style="width:100%; text-align:center;"></span>
                                <div style="width:100%; text-align:center; padding:20px;"><?php if (isset($objCommercant->nom_url_vsv)) { ?>
                                    <a
                                            href="http://<?php echo htmlspecialchars($objCommercant->nom_url_vsv); ?>.vivresaville.fr"
                                            target="_blank">
                                        http://<?php echo htmlspecialchars($objCommercant->nom_url_vsv); ?>
                                        .vivresaville.fr</a><?php } ?></div>
                            </td>
                        </tr>
                    </table>

                    <div class="p-3" style="background-color: rgb(225, 225, 225)">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">
                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label class="label">Description du site : Quelques phrases décrivant votre page. Elles
                                        sont aussi utilisées lorsque votre page est partagée sur Facebook</label>
                                </td>
                            <tr>
                            </tr>
                            <td>
                                        <textarea name="Societe[metadescription]" id="metadescription" class="form-control"
                                                  style="width:100%; height:150px;"><?php if (isset($objCommercant->metadescription)) echo htmlspecialchars($objCommercant->metadescription); ?></textarea>
                            </td>
                            </tr>
                            <tr>
                                <td class="stl_long_input_platinum_td">
                                    <label class="label">Mots-clés du site : Une liste de mots-clés séparés par des virgules
                                        pour votre page. Que doit-on chercher pour trouver cette page ? </label>
                                </td>
                            <tr>
                            </tr>
                            <td>
                                        <textarea name="Societe[metatag]" id="metatag" class="form-control"
                                                  style="width:100%; height:150px;"><?php if (isset($objCommercant->metatag)) echo htmlspecialchars($objCommercant->metatag); ?></textarea>
                            </td>
                            </tr>
                        </table>
                    </div>
                <?php } ?>


                <div class="div_stl_long_platinum" style="margin-bottom:0 !important;">Création d'un QRCODE</div>

                <div style="width:100%; display:table; background-color:#E1E1E1;">

                    <div class="col-lg-4" style="padding:15px !important; border:thin; height:150px;">
                        <div id="div_qrcode_img" style="width:100%; height:100%; text-align:center;">
                            <?php if (isset($objCommercant->qrcode_img) && $objCommercant->qrcode_img != "") { ?>
                                <img src="<?php echo base_url(); ?>/application/resources/phpqrcode/images/<?php echo $objCommercant->qrcode_img; ?>"
                                     alt="qrcode"/>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="col-lg-8 p-3" style="background-color: rgb(225, 225, 225)">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">
                            <tr>
                                <td class="label stl_long_input_platinum_td" valign="top">
                                    Préciser l'URL de votre page
                                </td>
                            </tr>
                            <tr>
                                <td class="stl_long_input_platinum_td" valign="top">
                                    <input type="text" name="Societe[qrcode_text]" id="qrcode_text"
                                           value="<?php if (isset($objCommercant->qrcode_text)) echo htmlspecialchars($objCommercant->qrcode_text); ?>"
                                           class="form-control stl_long_input_platinum"/>
                                    <input type="hidden" name="Societe[qrcode_img]" id="qrcode_img"
                                           value="<?php if (isset($objCommercant->qrcode_img)) echo htmlspecialchars($objCommercant->qrcode_img); ?>"
                                           class="form-control stl_long_input_platinum"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>
                                        Par d&eacute;faut l'url correspondante &agrave; votre page d'accueil est
                                        enregistr&eacute;e. Le QRCODE est automatiquement g&eacute;n&eacute;r&eacute; et
                                        se retrouve sur l'ensemble de vos pages &quot;Nos infos sur votre
                                        mobile&quot;.<br/>
                                        Si vous optez pour un sous-domaine ou une url personnalis&eacute;e, n'oubliez
                                        pas d'int&eacute;grer l'adresse et de valider le nouveau QRCODE.
                                    </p>
                                    <p style=" padding: 15px 0;"><a href="javascript:void(0);"
                                                                    onclick="javascript:generate_qrcode();"><img
                                                    src="<?php echo GetImagePath("privicarte/"); ?>/btn_generate_qrcode.png"
                                                    alt="qrcode"/></a><span id="span_generate_qrcode"></span></p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>


                <?php if (isset($page_data) && $page_data == 'contenus') echo '</div>'; ?><!--END CONTAINER COORDONNEES-->


                <!--AJOUT GLISSIERES-->


                <?php if (isset($page_data) && $page_data == 'coordonnees') echo '<div style="display:none;">'; ?><!--START CONTAINER MON CONTENU-->


                <?php
                if (isset($objCommercant->IdCommercant)) {
                    $id_glissiere_getted = $this->mdlglissiere->GetByIdCommercant($objCommercant->IdCommercant)->id_glissiere ?? 0;
                }
                ?>

                <input type="hidden" name="Societe[id_glissiere]" id="id_glissiere_com"
                       value="<?php if (isset($id_glissiere_getted) && $id_glissiere_getted != "" && $id_glissiere_getted != NULL) echo $id_glissiere_getted; else echo '0'; ?>"/>
                <input type="hidden" name="Glissiere[id_glissiere]" id="id_glissiere_glis"
                       value="<?php if (isset($id_glissiere_getted) && $id_glissiere_getted != "" && $id_glissiere_getted != NULL) echo $id_glissiere_getted; else echo '0'; ?>"/>


                <?php if (isset($user_groups) && $user_groups->id == '5' OR $user_groups->id == '4') { ?>
                    <?php $this->load->view('privicarte/vwfiche_pro/bloc1_home',$data) ?>
                <?php } ?>


                <?php if (isset($user_groups) && $user_groups->id == '5' OR $user_groups->id == '4') { ?>
                    <?php $this->load->view('privicarte/vwfiche_pro/bloc2_home',$data) ?>
                <?php } ?>



                <?php if (isset($user_groups) && $user_groups->id == '5' OR $user_groups->id == '4') { ?>
                    <?php $this->load->view('privicarte/vwfiche_pro/bloc3_home',$data) ?>
                <?php } ?>


                <?php if (isset($user_groups) && $user_groups->id == '5' OR $user_groups->id == '4') { ?>
                    <?php $this->load->view('privicarte/vwfiche_pro/bloc4_home',$data) ?>
                <?php } ?>


                    <?php $this->load->view('privicarte/vwfiche_pro/glissire1_home',$data) ?>

                    <?php $this->load->view('privicarte/vwfiche_pro/glissire2_home',$data) ?>

                <!--//////////////////////////////////////////glissiere3//////////////////////////!-->

                    <?php $this->load->view('privicarte/vwfiche_pro/glissire3_home',$data) ?>

                    <?php $this->load->view('privicarte/vwfiche_pro/glissire4_home',$data) ?>


                <!-- debut bande verte page01 -->
                <?php if (isset($user_groups) && $user_groups->id == '5') {
                 ?>
                    <?php  $this->load->view("privicarte/vwfiche_pro/bloc1_page1", $data); ?>
                <?php } ?>
                <?php if (isset($user_groups) && $user_groups->id == '5') { ?>
                    <?php  $this->load->view("privicarte/vwfiche_pro/bloc2_page1", $data); ?>
                <?php } ?>
                <?php if (isset($user_groups) && $user_groups->id == '5') { ?>
                    <?php  $this->load->view("privicarte/vwfiche_pro/bloc3_page1", $data); ?>
                <?php } ?>
                <?php if (isset($user_groups) && $user_groups->id == '5') { ?>
                    <?php  $this->load->view("privicarte/vwfiche_pro/bloc4_page1", $data); ?>
                <?php } ?>
                    
                <!-- fin bande verte page 01  -->

                <?php if (isset($user_groups) && $user_groups->id == '5') { ?>
                    <?php $this->load->view('privicarte/vwfiche_pro/glissire1_page1',$data) ?>
                <?php } ?>

                <?php // gli2 ?>
                <?php if (isset($user_groups) && $user_groups->id == '5') { ?>
                    <?php $this->load->view('privicarte/vwfiche_pro/glissire2_page1',$data) ?>
                <?php } ?>

                <?php if (isset($user_groups) && $user_groups->id == '5') { ?>
                    <?php $this->load->view('privicarte/vwfiche_pro/glissire3_page1',$data) ?>
                <?php } ?>
                <?php if (isset($user_groups) && $user_groups->id == '5') { ?>
                    <?php $this->load->view('privicarte/vwfiche_pro/glissire4_page1',$data) ?>
                <?php } ?>

                <!-- debut bande verte page02 -->
                <?php if (isset($user_groups) && $user_groups->id == '5') {
                 ?>
                    <?php  $this->load->view("privicarte/vwfiche_pro/bloc1_page2", $data); ?>
                <?php } ?>
                <?php if (isset($user_groups) && $user_groups->id == '5') { ?>
                    <?php  $this->load->view("privicarte/vwfiche_pro/bloc2_page2", $data); ?>
                <?php } ?>
                <?php if (isset($user_groups) && $user_groups->id == '5') { ?>
                    <?php  $this->load->view("privicarte/vwfiche_pro/bloc3_page2", $data); ?>
                <?php } ?>
                <?php if (isset($user_groups) && $user_groups->id == '5') { ?>
                    <?php  $this->load->view("privicarte/vwfiche_pro/bloc4_page2", $data); ?>
                <?php } ?>

                <!-- fin bande verte page02 -->


                <?php if (isset($user_groups) && $user_groups->id == '5') { ?>
                    <?php $this->load->view('privicarte/vwfiche_pro/glissire1_page2',$data) ?>
                <?php } ?>


                <?php if (isset($user_groups) && $user_groups->id == '5') { ?>
                    <?php $this->load->view('privicarte/vwfiche_pro/glissire2_page2',$data) ?>
                <?php } ?>


                <?php if (isset($user_groups) && $user_groups->id == '5') { ?>
                    <?php $this->load->view('privicarte/vwfiche_pro/glissire3_page2',$data) ?>
                <?php } ?>
                <?php if (isset($user_groups) && $user_groups->id == '5') { ?>
                    <?php $this->load->view('privicarte/vwfiche_pro/glissire4_page2',$data) ?>
                <?php } ?>

                <?php if (isset($page_data) && $page_data == 'coordonnees') echo '</div>'; ?><!--END CONTAINER MON CONTENU-->


                <div id="div_error_fiche_pro" class="div_error_fiche_pro"
                     style="color:#FF0000; background-color: white; font-weight:bold; margin-left:40px; margin-right:40px;"></div>
            </form>
        </div>
    </div>
<?php $this->load->view("privicarte/includes/footer_mini_2"); ?>