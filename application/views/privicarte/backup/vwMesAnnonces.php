<?php $data["zTitle"] = 'Mes Annonces'; ?>
<?php //$this->load->view("adminAout2013/includes/vwHeader2013", $data); ?>

<?php
$data['empty'] = null;
$this->load->view("sortez/includes/backoffice_pro_css", $data);
?>

<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra'], headers: {7: { sorter: false}, 8: {sorter: false} }})
				.tablesorterPager({container: $("#pager")});
		});
</script>   

<script type="text/javascript">
        function savetextmenuannonce() {
				$('#spantextmenuannonce').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
                var idtextmenuannonce = $('#idtextmenuannonce').val();
                var is_actif_affichBoutique_Linkicon_val=$('#is_actif_affichBoutique_Linkicon_value').val();
                $.ajax({
                    type: "POST",
                    url: '<?php echo base_url(); ?>front/annonce/savetextmenuannonce',
                    data: {idtextmenuannonce: idtextmenuannonce, idCommercant: <?php echo $idCommercant; ?>,is_actif_affichBoutique_Linkicon:is_actif_affichBoutique_Linkicon_val},
                    success: function (data) {
							$('#spantextmenuannonce').html('<span style="color:#006633">Titre Enregistr&eacute; !</span>');
							alert('Enregistrement réussi!')
                    }
                });
			}
		function set_value_field() {
            if($('#is_actif_affichBoutique_Linkicon').is(':checked')){
                $('#is_actif_affichBoutique_Linkicon_value').val(1);
            }else{
                $('#is_actif_affichBoutique_Linkicon_value').val(0);
            }
            }
		function save_order_partner() {
			$('#spanOrderPartner').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
			$.ajax({
				url: '<?php echo base_url();?>front/annonce/save_order_partner/',
				data: { 
					<?php foreach($toListeMesAnnonce as $oListeMesAnnonce){ ?>
					inputOrderPartner_<?php echo $oListeMesAnnonce->annonce_id; ?>: $('#inputOrderPartner_<?php echo $oListeMesAnnonce->annonce_id; ?>').val(), 
					<?php } ?>
					idCommercant: <?php echo $idCommercant; ?>
				},
				dataType: 'html',
				type: 'POST',
				async: true,
				success: function(data){
					$('#spanOrderPartner').html('<img src="<?php echo GetImagePath("front/");?>/btn_new/icon-16-checkin.png" />');
					//window.location.reload();
				}
			});
		}
    </script>
<div class="row p-4">
    <div class="col-sm-12 padding0 text-center">
        <h1>Liste de mes annonces</h1>
    </div>
</div>
<div class="row p-4">
    <div class="col-sm-12 padding0 text-center">
        <button id="btnnewreturn" class="btn btn-primary" onclick="document.location='<?php echo base_url() ;?>';">Retour site</button>
        <button id="btnnewreturn" class="btn btn-primary" onclick="document.location='<?php echo site_url("front/utilisateur/contenupro") ;?>';">Retour au menu</button>
		<?php if (isset($limit_annonce_add) && $limit_annonce_add=='1') { ?>
            <div style="color: #FF0000;"> Vous avez atteint la limite de vos annonces : <?php if (isset($objCommercant->limit_annonce)) echo $objCommercant->limit_annonce; ?></div>
        <?php } else { ?>
            <button id="btnnew" class="btn btn-success" onclick="document.location='<?php echo site_url("front/annonce/ficheAnnonce/$idCommercant") ;?>';">Ajouter une annonce</button>
        <?php } ?>
    </div>
</div>
    <form name="frmtextmenuannonce" id="frmtextmenuannonce" action="" method="POST" enctype="multipart/form-data">
    <div class="row text-center pt-4 pb-0">
    <div class="col-sm-12 text-center">
        <div style="width:100%;">
                            <div class="stl_long_input_platinum_td">
                                <label style="color:#000000;">Titre du menu Notre boutique : </label>
                            </div>

                            <div>
                                <input type="text" name="nametextmenuannonce" id="idtextmenuannonce" value="<?php if(isset($objCommercant->textmenuannonce)) echo htmlspecialchars($objCommercant->textmenuannonce); ?>"  class="stl_long_input_platinum"/>
                            </div>
                            <div class="stl_long_input_platinum_td p-2" >
                                <button name="btntextmenuannonce" id="btntextmenuannonce" class="btn btn-primary" onclick="javascript:savetextmenuannonce();return false;">Enregistrer</button>

                            </div>
                            <div class="stl_long_input_platinum_td p-2" >
                                <p><span style="font-size:11px" id="spantextmenuannonce">"Notre Boutique" est affich&eacute; si le titre du menu est vide</span></p>
                            </div>


        </div>
    </div>

</div>
<div class="row pb-4">
    <div class="col-lg-3"></div>
    <div class="col-sm-3 text-right">
        <input onclick="set_value_field()" type="checkbox" id="is_actif_affichBoutique_Linkicon" name="" <?php if (isset($infocom) AND $infocom->is_actif_affichBoutique_Linkicon =='1')echo 'checked';?>>
        <input type="hidden" id="is_actif_affichBoutique_Linkicon_value" name="is_actif_affichBoutique_Linkicon">
        Je veux que le bouton "Boutique" s'affiche sur mon menu de droite
    </div>
    <div class="col-lg-3 text-left">
        <img src="<?php echo base_url('assets/img/boutique_link.jpg')?>">
    </div>
    <div class="col-lg-3"></div>
</div>
        <div class="row">
            <div class="col-lg-12 text-center">

                <button name="btntextmenuannonce" id="btntextmenuannonce" class="btn btn-primary" onclick="javascript:savetextmenuannonce();return false;">Enregistrer</button>

            </div>
        </div>
        <button name="btntextmenuannonce" id="btntextmenuannonce" class="btn btn-primary" onclick="javascript:savetextmenuannonce();return false;">Enregistrer</button>
    </form>




<div id="divMesAnnonces" class="content" align="center" style="display: table;">
        <form name="frmMesAnnonces" id="frmMesAnnonces" action="" method="POST" enctype="multipart/form-data">
			
 				<div id="container">
					<table cellpadding="1" class="tablesorter">
						<thead>
							<tr>                    
								<th>Description courte</th>
								<th width="300">Description longue</th>
								<th>Date debut</th>
								<th>Date fin</th>
								<th>Prix neuf</th>
								<th>Prix solde</th>
								<th>Etat</th>
                                <th>Ordre d'affichage&nbsp;<a href="javascript:void(0);" id="saveOrderPartner" title="Enregistrer" onclick="javascript:save_order_partner();"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/saveOrderPartner.png"></a>&nbsp;<span id="spanOrderPartner"></span></th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
                        	<?php $i_order_partner = 1; ?>
							<?php foreach($toListeMesAnnonce as $oListeMesAnnonce){ ?>
								<tr>                    
									<td><?php echo $oListeMesAnnonce->annonce_description_courte ; ?></td>
									<td><?php echo truncate(strip_tags($oListeMesAnnonce->annonce_description_longue),100," ...") ; ?></td>
									<td><?php 
									if (isset($oListeMesAnnonce->annonce_date_debut) AND $oListeMesAnnonce->annonce_date_debut !="00/00/0000" AND $oListeMesAnnonce->annonce_date_debut !=''  AND $oListeMesAnnonce->annonce_date_debut !=null)
									echo convert_Sqldate_to_Frenchdate($oListeMesAnnonce->annonce_date_debut) ; ?></td>
									<td><?php 
									if (isset($oListeMesAnnonce->annonce_date_fin) AND $oListeMesAnnonce->annonce_date_debut !="00/00/0000" AND  $oListeMesAnnonce->annonce_date_debut !='' AND $oListeMesAnnonce->annonce_date_debut !=null)
									echo convert_Sqldate_to_Frenchdate($oListeMesAnnonce->annonce_date_fin) ; ?></td>
									<td><?php 
									if ($oListeMesAnnonce->annonce_prix_neuf!=0)
									echo $oListeMesAnnonce->annonce_prix_neuf ; ?></td>
									<td><?php 
									if ($oListeMesAnnonce->annonce_prix_solde!=0)
									echo $oListeMesAnnonce->annonce_prix_solde ; ?></td>
									<td>
									<?php 
									$zEtat = "";
									if ($oListeMesAnnonce->annonce_etat == 0) $zEtat = "Revente";
									else if ($oListeMesAnnonce->annonce_etat == 1) $zEtat = "Neuf";
									else if ($oListeMesAnnonce->annonce_etat == 2) $zEtat = "Service";
									echo $zEtat;?>
                                    </td>
                                    <td><input name="inputOrderPartner_<?php echo $oListeMesAnnonce->annonce_id;?>" id="inputOrderPartner_<?php echo $oListeMesAnnonce->annonce_id;?>" type="text" size="3" maxlength="3" value="<?php if (isset($oListeMesAnnonce->order_partner) && $oListeMesAnnonce->order_partner != "" && $oListeMesAnnonce->order_partner != NULL) echo $oListeMesAnnonce->order_partner; else echo $i_order_partner; ?>"/></td>
									<td><a href="<?php echo site_url("front/annonce/ficheAnnonce/" . $oListeMesAnnonce->annonce_commercant_id . "/" . $oListeMesAnnonce->annonce_id ) ; ?>" title="Modifier"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/update_ico.png"></a></td>
									<td><a href="<?php echo site_url("front/annonce/supprimAnnonce/" . $oListeMesAnnonce->annonce_id . "/" . $oListeMesAnnonce->annonce_commercant_id) ; ?>" onclick="if (!confirm('voulez-vous vraiment supprimer cette annoce ?')){ return false ; }" title="Supprimer"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/delete_ico.png"></a></td>
								</tr>
                                <?php if (isset($oListeMesAnnonce->order_partner) && $oListeMesAnnonce->order_partner != "" && $oListeMesAnnonce->order_partner != NULL) {} else $i_order_partner = $i_order_partner + 1; ?>
							<?php } ?>
						</tbody>
					</table>
					<div id="pager" class="pager">
							<img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
							<img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
							<input type="text" class="pagedisplay"/>
							<img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
							<img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
							<select class="pagesize" style="visibility:hidden">
								<option selected="selected"  value="10">10</option>
								<option value="20">20</option>
								<option value="30">30</option>
								<option  value="40">40</option>
							</select>
					</div>
				</div>
        </form>
    </div>
<?php //$this->load->view("adminAout2013/includes/vwFooter2013"); ?>