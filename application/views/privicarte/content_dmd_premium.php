<style type="text/css">
    <!--
    .txt_intro_dmd_premium {
        padding: 30px;
        font-family: futura_bold;
        font-size: 27px;
        line-height: 33px;
        text-align: center;
        background-color: #E6E6E6;
        color: #000;
    }

    .inscription_reel {
        background-image: url("<?php echo base_url().'application/resources/sortez/images/bg_btn_15j_essai.png';?>");
        background-repeat: no-repeat;
        background-position: top center;
        height: 580px;
        width: 100%;
        display: table;
        text-align: center;
        padding-top: 200px;
    }

    .btn_inscr_dmd_premium {
        background-image: url("<?php echo base_url();?>application/resources/sortez/images/btn_inscr_dmd_premium.png");
        background-repeat: no-repeat;
        display: block;
        min-height: 75px;
        background-position: center;
        line-height: 75px;
        color: #ffffff;
        font-family: Futura_medium;
        text-transform: uppercase;
        font-size: 18px;
    }

    .btn_inscr_dmd_premium:hover, .btn_inscr_dmd_premium:focus, .btn_inscr_dmd_premium:active {
        background-image: url("<?php echo base_url();?>application/resources/sortez/images/btn_inscr_dmd_premium_hover.png");
        color: #ffffff;
        text-decoration: none;
    }

    .inscription_essaie {
        color: #ffffff;
        font-family: Futura_bold;
        text-transform: uppercase;
        font-size: 22px;
        background-color: #DC188E;
        padding: 15px;
        display: table;
        width: 100%;
        text-align: center;
    }
    .inscription_essaie div {
        margin:15px 0;
    }
    .inscription_essaie_btn {
        background-image: url("<?php echo base_url();?>application/resources/sortez/images/inscription_essaie_btn.png");
        background-repeat: no-repeat;
        display: block;
        min-height: 75px;
        background-position: center;
        line-height: 75px;
        color: #ffffff;
        font-family: Futura_medium;
        text-transform: uppercase;
        font-size: 18px;
    }

    .inscription_essaie_btn:hover, .inscription_essaie_btn:focus, .inscription_essaie_btn:active {
        background-image: url("<?php echo base_url();?>application/resources/sortez/images/inscription_essaie_btn_hover.png");
        color: #ffffff;
        text-decoration: none;
    }

    -->
</style>
<div class="col-xs-12 padding0">
    <div class="col-xs-12 txt_intro_dmd_premium">
        &Agrave; TOUT MOMENT, ENRICHISSEZ<br/>
        VOTRE ABONNEMENT PAR L'AJOUT<br/>
        DE FONCTIONS OPTIONNELS<br/>
        ET B&Eacute;N&Eacute;FICIEZ D’OUTILS DE MARKETING SURPRENANTS, ET CL&Eacute;S EN MAIN !…
    </div>

    <div class="inscription_reel">
        <div>
            <a href="<?php echo base_url();?>page83.html" target="_blank" class="btn_inscr_dmd_premium">D&eacute;couvrez les ...</a>
            <!--<a href="javascript:void(0);" onclick="javascript:window.location.href='<?php echo base_url();?>/front/professionnels/mon_inscription';return false;" class="btn_inscr_dmd_premium">D&eacute;couvrez les ...</a>-->
        </div>
        <div>
            <a href="javascript:void(0);" onclick="javascript:window.location.href='<?php echo base_url();?>/front/professionnels/mon_inscription';return false;" class="btn_inscr_dmd_premium">j'acc&egrave;de au formulaire de souscription ...</a>
        </div>
    </div>


    <!--
    <div class="inscription_essaie">
        <div class="col-xs-12">
            <img src="<?php echo base_url()?>application/resources/sortez/images/wpce2843ab_06.png"/>
        </div>
        <div class="col-xs-12">
            AVEC LE PACK PROMO<br>
            BOOSTEZ VOTRE VISIBILITÉ ET VOS VENTES<br>
            EN CRÉANT VOS ACTIONS COMMERCIALES  !...
        </div>
        <?php if (isset($validation_pro)) { ?>
            <?php foreach ($validation_pro as $validation_pro_item) { ?>
                <?php if ($validation_pro_item->id_pack_test == '1' && $validation_pro_item->idCommercant == $iduser) { ?>
                    <div class="alert alert-success alert_validation_pro">Vous avez une demande de validation en attente de confirmation par l'administrateur Sortez</div>
                <?php } ?>
            <?php } ?>
        <?php } ?>
        <div class="col-xs-12">
            <a href="#divDemandeActivationPackPromo" id="btnDemandeActivationPackPromo" class="inscription_essaie_btn">Demande d'essai ...</a>
        </div>
        <div class="col-xs-12">
            <img src="<?php echo base_url()?>application/resources/sortez/images/bar_white_space.png"/>
        </div>



        <div class="col-xs-12">
            <img src="<?php echo base_url()?>application/resources/sortez/images/wp5ccdcf6a_06.png"/>
        </div>
        <div class="col-xs-12">
            AVEC LE PACK INFOS,<br>
            OPTIMISEZ LA DIFFUSION DE VOS ÉVÈNEMENTS<br>
            ET ARTICLES !...
        </div>
        <?php if (isset($validation_pro)) { ?>
            <?php foreach ($validation_pro as $validation_pro_item) { ?>
                <?php if ($validation_pro_item->id_pack_test == '2' && $validation_pro_item->idCommercant == $iduser) { ?>
                    <div class="alert alert-success alert_validation_pro">Vous avez une demande de validation en attente de confirmation par l'administrateur Sortez</div>
                <?php } ?>
            <?php } ?>
        <?php } ?>
        <div class="col-xs-12">
            <a href="#divDemandeActivationPackInfo" id="btnDemandeActivationPackInfo" class="inscription_essaie_btn">Demande d'essai ...</a>
        </div>
        <div class="col-xs-12">
            <img src="<?php echo base_url()?>application/resources/sortez/images/bar_white_space.png"/>
        </div>




        <div class="col-xs-12">
            <img src="<?php echo base_url()?>application/resources/sortez/images/wp52706035_06.png"/>
        </div>
        <div class="col-xs-12">
            AVEC LE PACK GRAPHIQUE PLATINIUM,<br>
            AMÉLIOREZ LA PRÉSENTATION DE VOS PAGES<br>
            ET DISPOSEZ IMMÉDIATEMENT<br>
            D’UN SITE WEB D’UN SIMPLE CLIC !…
        </div>
        <?php if (isset($validation_pro)) { ?>
            <?php foreach ($validation_pro as $validation_pro_item) { ?>
                <?php if ($validation_pro_item->id_pack_test == '3' && $validation_pro_item->idCommercant == $iduser) { ?>
                    <div class="alert alert-success alert_validation_pro">Vous avez une demande de validation en attente de confirmation par l'administrateur Sortez</div>
                <?php } ?>
            <?php } ?>
        <?php } ?>
        <div class="col-xs-12">
            <a href="#divDemandeActivationPackPlatinium" id="btnDemandeActivationPackPlatinium" class="inscription_essaie_btn">Demande d'essai ...</a>
        </div>

    </div>
    -->
</div>