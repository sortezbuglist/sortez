<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("privicarte/includes/header_mini_2", $data); ?>


<div class="container-fluid" style="text-align:center; display: table;">
    <div class="row">
        <div class="col-lg-12">
            <img src="<?php echo base_url("assets/images/femme_affiche.webp")?>" alt="" onload="OnLoadPngFix()"  border="0" height="auto" width="auto">
        </div>

        <div class="col-lg-12">
            <p style='color: #000000;
        font-family: Libre-Baskerville,Sans-serif!important;
        font-size: 40px; display: table; padding-top: 40px;
        line-height: 47px; text-align: center;text-transform: uppercase;margin: auto;'>
                Nous vous remercions<br>pour votre inscription!
            </p>
        </div>

        <div class="col-lg-12">
            <ul style="margin-top: 50px;margin-bottom: 50px;">
                <?php if (isset($txtContenu) && $txtContenu != "") echo $txtContenu; else { ?>
                    <li>Votre Inscription est maintenant en attente de validation par un Administrateur.<br>L'équipe Privicarte vous remercie.</li>
                <?php } ?>
            </ul>
        </div>

        <div class="col-lg-12" style="margin-bottom: 50px;">
            <a href="<?php echo site_url("auth/login") ; ?>">
                <div class="btn btn_retour">Mon compte</div>
            </a>
        </div>

        <div class="col-lg-12 text-center" style="margin-bottom: 200px;">
            <img class="img-fluid" src="<?php echo base_url("assets/soutenons/icon_phone.png") ?>"> <br>

            <img class="img-fluid" src="<?php echo base_url("assets/soutenons/num_phone.png") ?>">
        </div>
    </div>
</div>
<style>
    .col-lg-12.padding0{
        display: none;
    }
    ul li{
        font-family: Futura-LT-Book, Sans-Serif!important;
        font-size: 16px;
        color:#000000;
        text-align: justify;
    }
</style>




<?php $this->load->view("front2013/includes/footer_mini_2"); ?>