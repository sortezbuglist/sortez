<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("sortez/includes/header_backoffice_com", $data); ?>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/fields.check.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript">
        function listeSousRubrique() {

            jQuery('#trReponseRub').html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...." id="img_loaging_agenda_x"/>');

            var irubId = jQuery('#IdCategory').val();
            jQuery.get(
                '<?php echo site_url("front/utilisateur/GetallSubcateg_by_categ"); ?>' + '/' + irubId,
                function (zReponse) {
                    // alert (zReponse) ;
                    jQuery('#trReponseRub').html(zReponse);


                });
            //alert(irubId);
        }
        function getCP() {
            var ivilleId = jQuery('#IdVille').val();
            jQuery.get(
                '<?php echo site_url("front/utilisateur/getPostalCode_localisation"); ?>' + '/' + ivilleId,
                function (zReponse) {
                    jQuery('#codepostal').val(zReponse);
                });
        }

        function getCP_localisation() {
            var ivilleId = jQuery('#IdVille_localisation').val();
            jQuery.get(
                '<?php echo site_url("front/utilisateur/getPostalCode_localisation"); ?>' + '/' + ivilleId,
                function (zReponse) {
                    jQuery('#codepostal_localisation').val(zReponse);
                });
        }

        function deleteFile(_IdAgenda, _FileName) {
            jQuery.ajax({
                url: '<?php echo base_url();?>agenda/delete_files/' + _IdAgenda + '/' + _FileName,
                dataType: 'html',
                type: 'POST',
                async: true,
                success: function (data) {
                    window.location.reload();
                }
            });

        }

        jQuery(document).ready(function () {

            jQuery("#img_loaging_agenda").hide();
            jQuery("#img_loaging_agenda_addbonplan").hide();

            jQuery(".btnSinscrire").click(function () {

                var txtError = "";

                var nom_societe = jQuery("#nom_societe").val();
                if (nom_societe == "" || nom_societe == null) {
                    txtError += "- Veuillez indiquer qui vous-êtes.<br/>";
                    $("#nom_societe").css('border-color', '#FF0000');
                } else {
                    $("#nom_societe").css('border-color', '#FFFFFF');
                }

                var nom_manifestation = jQuery("#nom_manifestation").val();
                if (nom_manifestation == "" || nom_manifestation == null) {
                    txtError += "- Veuillez indiquer un Nom à la manifestion.<br/>";
                    $("#nom_manifestation").css('border-color', '#FF0000');
                } else {
                    $("#nom_manifestation").css('border-color', '#FFFFFF');
                }

                var organisateur = jQuery("#organisateur").val();
                if (organisateur == "" || organisateur == null) {
                    txtError += "- Veuillez indiquer qui est l'Organisateur.<br/>";
                    $("#organisateur").css('border-color', '#FF0000');
                } else {
                    $("#organisateur").css('border-color', '#FFFFFF');
                }

                var email = jQuery("#email").val();
                if (!isEmail(email) || email == "" || email == null) {
                    txtError += "- Veuillez indiquer un adresse email valide.<br/>";
                    $("#email").css('border-color', '#FF0000');
                } else {
                    $("#email").css('border-color', '#FFFFFF');
                }

                var date_depot = jQuery("#date_depot").val();
                if (date_depot == "" || date_depot == null) {
                    txtError += "- Veuillez indiquer la date de dépôt.<br/>";
                    $("#date_depot").css('border-color', '#FF0000');
                } else {
                    $("#date_depot").css('border-color', '#FFFFFF');
                }

                var IdCategory = jQuery("#IdCategory").val();
                if (IdCategory == "" || IdCategory == null || IdCategory == "0") {
                    txtError += "- Veuillez indiquer une categorie.<br/>";
                    $("#IdCategory").css('border-color', '#FF0000');
                } else {
                    $("#IdCategory").css('border-color', '#FFFFFF');
                }

                /*var IdSubcategory = jQuery("#IdSubcategory").val();
                 if(IdSubcategory=="" || IdSubcategory==null || IdSubcategory=="0") {
                 txtError += "- Veuillez indiquer une sous-categorie.<br/>";
                 $("#IdSubcategory").css('border-color', '#FF0000');
                 } else {
                 $("#IdSubcategory").css('border-color', '#FFFFFF');
                 }*/

                var date_debut = jQuery("#date_debut").val();
                if (date_debut == "" || date_debut == null) {
                    txtError += "- Veuillez indiquer la date de début.<br/>";
                    $("#date_debut").css('border-color', '#FF0000');
                } else {
                    $("#date_debut").css('border-color', '#FFFFFF');
                }

                var date_fin = jQuery("#date_fin").val();
                if (date_fin == "" || date_debut == null) {
                    txtError += "- Veuillez indiquer la date de fin.<br/>";
                    $("#date_fin").css('border-color', '#FF0000');
                } else {
                    $("#date_fin").css('border-color', '#FFFFFF');
                }


                if (txtError == "") {
                    jQuery("#frmInscriptionProfessionnel").submit();
                } else {
                    jQuery("#div_error_agenda_submit").html(txtError);
                }
            });


            $("#adresse_localisation_diffuseur_checkbox").click(function () {
                if ($('#adresse_localisation_diffuseur_checkbox').attr('checked')) {
                    $("#adresse_localisation_diffuseur").val("1");
                    var adress_organisateur1 = $("#adresse1").val();
                    var adress_organisateur2 = $("#adresse2").val();
                    var IdVille_organisateur = $("#IdVille").val();
                    var codepostal_organisateur = $("#codepostal").val();
                    $("#adresse_localisation").val(adress_organisateur1 + " " + adress_organisateur2);
                    $("#IdVille_localisation").val(IdVille_organisateur);
                    $("#codepostal_localisation").val(codepostal_organisateur);
                } else {
                    $("#adresse_localisation_diffuseur").val("0");
                    $("#adresse_localisation").val("");
                    $("#IdVille_localisation").val("0");
                    $("#codepostal_localisation").val("");
                }
            });

            $("#diffusion_facebook_checkbox").click(function () {
                if ($('#diffusion_facebook_checkbox').attr('checked')) {
                    $("#diffusion_facebook").val("1");
                } else {
                    $("#diffusion_facebook").val("0");
                }
            });

            $("#diffusion_twitter_checkbox").click(function () {
                if ($('#diffusion_twitter_checkbox').attr('checked')) {
                    $("#diffusion_twitter").val("1");
                } else {
                    $("#diffusion_twitter").val("0");
                }
            });

            $("#diffusion_rss_checkbox").click(function () {
                if ($('#diffusion_rss_checkbox').attr('checked')) {
                    $("#diffusion_rss").val("1");
                } else {
                    $("#diffusion_rss").val("0");
                }
            });


            //checkbox manif_gratuite START
            $("#manif_gratuite_checkbox").click(function () {
                if ($('#manif_gratuite_checkbox').attr('checked')) {
                    $("#manif_gratuite").val("1");
                    //$("#description_tarif").text("Manifestation gratuite");
                    CKEDITOR.instances.description_tarif.insertText("Manifestation gratuite");
                } else {
                    $("#manif_gratuite").val("0");
                    $("#description_tarif").text("");
                }
            });
            //checkbox manif_gratuit END


            //checkbox ajout_bonplan START
            $("#ajout_bonplan_checkbox").click(function () {
                if ($('#ajout_bonplan_checkbox').attr('checked')) {
                    $("#ajout_bonplan").val("1");


                    $("#img_loaging_agenda_addbonplan").show();


                    $.post(
                        '<?php echo site_url(); ?>/agenda/check_bonplan/',
                        {
                            IdCommercant: <?php echo $objCommercant->IdCommercant; ?>
                        },
                        function (zReponse) {
                            //alert ("reponse " + zReponse) ;

                            if (zReponse != "error") {

                                CKEDITOR.instances.conditions_promo.insertText(zReponse);
                                $("#img_loaging_agenda_addbonplan").hide();

                            } else {
                                $("#spam_error_bonplan_agenda").html("Votre bonplan a éxpiré ou vous n'avez pas encore de bonplan !");
                            }
                        });


                } else {
                    $("#ajout_bonplan").val("0");
                    $("#conditions_promo").text("");
                }
            });
            //checkbox ajout_bonplan END


            //radio buttton diffuseur_organisateur START
            $("#diffuseur_organisateur_1").click(function () {
                if ($('#diffuseur_organisateur_1').attr('checked')) {
                    $("#diffuseur_organisateur").val("1");

                    $("#img_loaging_agenda").show();


                    $.post(
                        '<?php echo site_url(); ?>agenda/check_commercant/',
                        {
                            IdCommercant: <?php echo $objCommercant->IdCommercant; ?>
                        },
                        function (zReponse) {
                            //alert ("reponse " + zReponse) ;

                            if (zReponse) {
                                var article_value = zReponse.split("==!!!==");
                                for (i = 0; i < article_value.length; i++) {
                                    var article_value_id = article_value[i].split("!!!==>");
                                    if (article_value_id[0] == "NomSociete") $("#organisateur").val(article_value_id[1]);
                                    if (article_value_id[0] == "Adresse1") $("#adresse1").val(article_value_id[1]);
                                    if (article_value_id[0] == "Adresse2") $("#adresse2").val(article_value_id[1]);
                                    if (article_value_id[0] == "IdVille") $("#IdVille").val(article_value_id[1]);
                                    if (article_value_id[0] == "CodePostal") $("#codepostal").val(article_value_id[1]);
                                    if (article_value_id[0] == "TelFixe") $("#telephone").val(article_value_id[1]);
                                    if (article_value_id[0] == "TelMobile") $("#mobile").val(article_value_id[1]);
                                    if (article_value_id[0] == "fax") $("#fax").val(article_value_id[1]);
                                    if (article_value_id[0] == "Email") $("#email").val(article_value_id[1]);
                                    if (article_value_id[0] == "SiteWeb") $("#siteweb").val(article_value_id[1]);
                                    if (article_value_id[0] == "Facebook") $("#facebook").val(article_value_id[1]);
                                    if (article_value_id[0] == "Twitter") $("#googleplus").val(article_value_id[1]);
                                    if (article_value_id[0] == "google_plus") $("#twitter").val(article_value_id[1]);
                                }

                                $("#img_loaging_agenda").hide();
                            }
                        });


                } else {
                    $("#diffuseur_organisateur").val("0");
                }
            });
            $("#diffuseur_organisateur_0").click(function () {
                if ($('#diffuseur_organisateur_0').attr('checked')) {
                    $("#diffuseur_organisateur").val("0");

                    $("#organisateur").val("");
                    $("#adresse1").val("");
                    $("#adresse2").val("");
                    $("#IdVille").val("0");
                    $("#codepostal").val("");
                    $("#telephone").val("");
                    $("#mobile").val("");
                    $("#fax").val("");
                    $("#email").val("");
                    $("#siteweb").val("http://www.");
                    $("#facebook").val("");
                    $("#googleplus").val("");
                    $("#twitter").val("");

                } else {
                    $("#diffuseur_organisateur").val("1");
                }
            });
            //radio buttton diffuseur_organisateur END


            //radio buttton handicapes
            $("#handicapes_1").click(function () {
                if ($('#handicapes_1').attr('checked')) {
                    $("#handicapes").val("1");
                } else {
                    $("#handicapes").val("0");
                }
            });
            $("#handicapes_0").click(function () {
                if ($('#handicapes_0').attr('checked')) {
                    $("#handicapes").val("0");
                } else {
                    $("#handicapes").val("1");
                }
            });
            //radio buttton parking
            $("#parking_1").click(function () {
                if ($('#parking_1').attr('checked')) {
                    $("#parking").val("1");
                } else {
                    $("#parking").val("0");
                }
            });
            $("#parking_0").click(function () {
                if ($('#parking_0').attr('checked')) {
                    $("#parking").val("0");
                } else {
                    $("#parking").val("1");
                }
            });


            $("#admin_agenda_activation_1").click(function () {
                if ($('#admin_agenda_activation_1').attr('checked')) {
                    $("#IsActif").val("1");
                }
            });
            $("#admin_agenda_activation_0").click(function () {
                if ($('#admin_agenda_activation_0').attr('checked')) {
                    $("#IsActif").val("0");
                }
            });
            $("#admin_agenda_activation_2").click(function () {
                if ($('#admin_agenda_activation_2').attr('checked')) {
                    $("#IsActif").val("2");
                }
            });
            $("#admin_agenda_activation_3").click(function () {
                if ($('#admin_agenda_activation_3').attr('checked')) {
                    $("#IsActif").val("3");
                }
            });

        });

        $(function () {
            $("#date_debut").datepicker({
                dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                monthNames: ['Janvier', 'Févier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                dateFormat: 'DD, d MM yy',
                autoSize: true,
                changeMonth: true,
                changeYear: true,
                yearRange: '1900:2020'
            });
            $("#date_fin").datepicker({
                dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                monthNames: ['Janvier', 'Févier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                dateFormat: 'DD, d MM yy',
                autoSize: true,
                changeMonth: true,
                changeYear: true,
                yearRange: '1900:2020'
            });
            $("#date_depot").datepicker({
                dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                monthNames: ['Janvier', 'Févier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                dateFormat: 'DD, d MM yy',
                autoSize: true,
                changeMonth: true,
                changeYear: true,
                yearRange: '1900:2020'
            });
        });

        function alert_save_agenda_before_view() {
            $("#id_save_agenda_before_view").html("Veuillez enregistrer l'événnement avant de visualiser !&nbsp;&nbsp;&nbsp;");
        }


        function CP_getDepartement() {
            jQuery('#departementCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var CodePostalSociete = jQuery('#codepostal').val();
            jQuery.post(
                '<?php echo site_url("front/professionnels/departementcp_agenda"); ?>',
                {CodePostalSociete: CodePostalSociete},
                function (zReponse) {
                    jQuery('#departementCP_container').html(zReponse);
                });
        }

        function CP_getDepartement_localisation() {
            jQuery('#departementCP_container_localisation').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var CodePostalSociete = jQuery('#codepostal_localisation').val();
            jQuery.post(
                '<?php echo site_url("front/professionnels/departementcp_agenda_localisation"); ?>',
                {CodePostalSociete: CodePostalSociete},
                function (zReponse) {
                    jQuery('#departementCP_container_localisation').html(zReponse);
                });
        }


        function CP_getVille() {
            jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var CodePostalSociete = jQuery('#codepostal').val();
            jQuery.post(
                '<?php echo site_url("front/professionnels/villecp_agenda"); ?>',
                {CodePostalSociete: CodePostalSociete},
                function (zReponse) {
                    jQuery('#villeCP_container').html(zReponse);
                });
        }

        function CP_getVille_localisation() {
            jQuery('#villeCP_container_localisation').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var CodePostalSociete = jQuery('#codepostal_localisation').val();
            jQuery.post(
                '<?php echo site_url("front/professionnels/villecp_agenda_localisation"); ?>',
                {CodePostalSociete: CodePostalSociete},
                function (zReponse) {
                    jQuery('#villeCP_container_localisation').html(zReponse);
                });
        }


        function CP_getVille_D_CP() {
            jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var CodePostalSociete = jQuery('#codepostal').val();
            var departement_id = jQuery('#DepartementAgenda').val();
            jQuery.post(
                '<?php echo site_url("front/professionnels/villecp_agenda"); ?>',
                {CodePostalSociete: CodePostalSociete, departement_id: departement_id},
                function (zReponse) {
                    jQuery('#villeCP_container').html(zReponse);
                });
        }

        function CP_getVille_D_CP_localisation() {
            jQuery('#villeCP_container_localisation').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var CodePostalSociete = jQuery('#codepostal_localisation').val();
            var departement_id = jQuery('#DepartementAgendaLocalisation').val();
            jQuery.post(
                '<?php echo site_url("front/professionnels/villecp_agenda_localisation"); ?>',
                {CodePostalSociete: CodePostalSociete, departement_id: departement_id},
                function (zReponse) {
                    jQuery('#villeCP_container_localisation').html(zReponse);
                });
        }


    </script>

    <style type="text/css">
        .stl_long_input_platinum {
            width: 100%;
        }

        .stl_long_input_platinum_td {
            width: 180px;
            height: 30px;
        }

        .div_stl_long_platinum {
            background-color: #ffcc33;
            color: #ffffff;
            font-family: "Arial", sans-serif;
            font-size: 13px;
            font-weight: bold;
            height: 25px;
            line-height: 1.23em;
            margin: 0;
            padding: 10px 15px;
            display: table;
            width: 100%;;
        }

        .div_error_taille_3_4 {
            color: #F00;
        }

        .FieldError {
            color: #FF0000;
            font-weight: bold;
        }

        .label {
            color: #000;
        }

        .admin_bo_menu {
            display: inline-table;
            height: 50px;
        }

        .btn_adminbp, .btn_adminbp:hover, .btn_adminbp:focus {
            background-color: #FFCC33;
            padding: 10px 30px;
            color: #FFFFFF;
            text-decoration: none;
            font-weight: bold;
        }

        .greybg {
            background-color: #E5E5E5;
            padding: 15px;
            display: table;
            width: 100%;
        }
    </style>


    <div class="col-xs-12 admin_bo_menu textaligncenter">
        <h1>Mon Agenda</h1>
    </div>

    <div class="col-xs-12 padding0">
        <div class="admin_bo_menu"><a href="<?php echo site_url('front/utilisateur/contenupro/'); ?>"
                                      class="btn_adminbp">Retour Menu</a></div>
        <div class="admin_bo_menu" style="min-height: 50px;"><a
                href="<?php echo site_url("front/utilisateur/agenda/" . $objCommercant->IdCommercant); ?>"
                class="btn_adminbp">Retour aux &eacute;vennements</a></div>
        <div class="admin_bo_menu" style="min-height: 50px;"><a
                href="<?php echo site_url("front/utilisateur/ficheAgenda/" . $objCommercant->IdCommercant); ?>"
                class="btn_adminbp">Ajouter un &eacute;vennement</a></div>
    </div>


<?php if (isset($mssg) && $mssg == 1) { ?>
    <div class="col-lg-12" style="padding-top:30px; text-align:center;">
        <div class="alert alert-success"
        ">Les modifications ont &eacute;t&eacute; enregistr&eacute;es
    </div>
    </div>
<?php } ?>


<?php if (isset($mssg) && $mssg != 1 && $mssg != "") { ?>
    <div style=" padding-left:120px; padding-top:30px;">
        <div class="alert alert-danger">Une erreur est constatée, Veuillez vérifier la conformité de vos données</div>
    </div>
<?php } ?>


    <div class="div_stl_long_platinum">Publication</div>

    <div class="col-lg-12 padding0">

        <form name="frmInscriptionProfessionnel" id="frmInscriptionProfessionnel"
              action="<?php echo site_url("front/utilisateur/save"); ?>" method="POST" enctype="multipart/form-data">

            <input type="hidden" name="Agenda[id]" id="idAgenda"
                   value="<?php if (isset($objAgenda->id)) echo htmlspecialchars($objAgenda->id); else echo "0"; ?>">

            <div class="col-xs-12 greybg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:20px;">
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Vous êtes : </label>
                        </td>
                        <td>
                            <?php
                            if (isset($objCommercant->IdVille_localisation) && $objCommercant->IdVille_localisation != "" && $objCommercant->IdVille_localisation != NULL) {
                                $this->load->Model("mdlville");
                                $obj_ville_commercant = $this->mdlville->getVilleById($objCommercant->IdVille_localisation);
                                if (isset($obj_ville_commercant) && count($obj_ville_commercant) != 0) $ville_name_to_show = $obj_ville_commercant->Nom; else $ville_name_to_show = "";
                            } else {
                                $ville_name_to_show = "";
                            }
                            ?>
                            <input type="text" name="Agenda[nom_societe]" id="nom_societe"
                                   value="<?php if (isset($objAgenda->nom_societe) && $objAgenda->nom_societe != "") echo htmlspecialchars($objAgenda->nom_societe); else echo $objCommercant->NomSociete . " " . $objCommercant->codepostal_localisation . " " . $ville_name_to_show; ?>"
                                   class="stl_long_input_platinum form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Nom de la manifestation : </label>
                        </td>
                        <td>
                            <input type="text" name="Agenda[nom_manifestation]" id="nom_manifestation"
                                   value="<?php if (isset($objAgenda->nom_manifestation) && $objAgenda->nom_manifestation != "") echo htmlspecialchars($objAgenda->nom_manifestation); ?>"
                                   class="stl_long_input_platinum form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Le diffuseur est-il organisateur ? </label>
                        </td>
                        <td>
                            <input type="radio" value="1" name="diffuseur_organisateur_radio"
                                   id="diffuseur_organisateur_1"
                                   <?php if (isset($objAgenda->diffuseur_organisateur) && $objAgenda->diffuseur_organisateur == "1") { ?>checked<?php } ?>/>
                            OUI
                            <input type="radio" value="0" name="diffuseur_organisateur_radio"
                                   id="diffuseur_organisateur_0"
                                   <?php if (isset($objAgenda->diffuseur_organisateur) && $objAgenda->diffuseur_organisateur == "0") { ?>checked<?php } ?>/>
                            NON
                            <input type="hidden" name="Agenda[diffuseur_organisateur]" id="diffuseur_organisateur"
                                   value="<?php if (isset($objAgenda->diffuseur_organisateur)) echo htmlspecialchars($objAgenda->diffuseur_organisateur); ?>">
                            &nbsp;<img src="<?php echo base_url(); ?>application/resources/front/images/wait.gif"
                                       alt="loading...." id="img_loaging_agenda"/>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="div_stl_long_platinum">Les coordonnées de l'organisateur</div>

            <div class="col-xs-12 greybg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:20px;">

                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Organisateur : </label>
                        </td>
                        <td>
                            <input type="text" name="Agenda[organisateur]" id="organisateur"
                                   value="<?php if (isset($objAgenda->organisateur) && $objAgenda->organisateur != "") echo htmlspecialchars($objAgenda->organisateur); ?>"
                                   class="stl_long_input_platinum form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Adresse1 : </label>
                        </td>
                        <td>
                            <input type="text" name="Agenda[adresse1]" id="adresse1"
                                   value="<?php if (isset($objAgenda->adresse1)) echo htmlspecialchars($objAgenda->adresse1); ?>"
                                   class="stl_long_input_platinum form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Adresse2 : </label>
                        </td>
                        <td>
                            <input type="text" name="Agenda[adresse2]" id="adresse2"
                                   value="<?php if (isset($objAgenda->adresse2)) echo htmlspecialchars($objAgenda->adresse2); ?>"
                                   class="stl_long_input_platinum form-control"/>
                        </td>
                    </tr>


                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Code Postal : </label>
                        </td>
                        <td id="trReponseVille">
                            <input type="text" name="Agenda[codepostal]" id="codepostal"
                                   value="<?php if (isset($objAgenda->codepostal)) echo $objAgenda->codepostal; ?>"
                                   class="stl_long_input_platinum form-control"
                                   onblur="javascript:CP_getDepartement();CP_getVille();"/>
                        </td>
                    </tr>


                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Departement : </label>
                        </td>
                        <td>
    	<span id="departementCP_container">
        <select name="Agenda[departement_id]" id="DepartementAgenda" disabled="disabled"
                class="stl_long_input_platinum form-control" onchange="javascript:CP_getVille_D_CP();">
            <option value="0">-- Veuillez choisir --</option>
            <?php if (sizeof($colDepartement)) { ?>
                <?php foreach ($colDepartement as $objDepartement) { ?>
                    <option
                        <?php if (isset($objAgenda->departement_id) && $objAgenda->departement_id == $objDepartement->departement_id) { ?>selected="selected"<?php } ?>
                        value="<?php echo $objDepartement->departement_id; ?>"><?php echo $objDepartement->departement_nom . " - " . $objDepartement->departement_code; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
        </span>
                        </td>
                    </tr>


                    <tr>
                        <td>Ville *</td>
                        <td>
    	<span id="villeCP_container">
        <input type="text"
               value="<?php if (isset($objAgenda->IdVille)) echo $this->mdlville->getVilleById($objAgenda->IdVille)->Nom; ?>"
               name="IdVille_Nom_text" id="IdVille_Nom_text" disabled="disabled"
               class="stl_long_input_platinum form-control"/>
        <input type="hidden" value="<?php if (isset($objAgenda->IdVille)) echo $objAgenda->IdVille; ?>"
               name="Agenda[IdVille]" id="IdVille"/>
        </span>
                        </td>
                    </tr>


                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">T&eacute;l&eacute;phone : </label>
                        </td>
                        <td>
                            <input type="text" name="Agenda[telephone]" id="telephone"
                                   value="<?php if (isset($objAgenda->telephone)) echo htmlspecialchars($objAgenda->telephone); ?>"
                                   class="stl_long_input_platinum form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Mobile : </label>
                        </td>
                        <td>
                            <input type="text" name="Agenda[mobile]" id="mobile"
                                   value="<?php if (isset($objAgenda->mobile)) echo htmlspecialchars($objAgenda->mobile); ?>"
                                   class="stl_long_input_platinum form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Fax : </label>
                        </td>
                        <td>
                            <input type="text" name="Agenda[fax]" id="fax"
                                   value="<?php if (isset($objAgenda->fax)) echo htmlspecialchars($objAgenda->fax); ?>"
                                   class="stl_long_input_platinum form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Courriel : </label>
                        </td>
                        <td>
                            <input type="text" name="Agenda[email]" id="email"
                                   value="<?php if (isset($objAgenda->email)) echo htmlspecialchars($objAgenda->email); ?>"
                                   class="stl_long_input_platinum form-control"/>
                            <div class="FieldError" id="divErrorEmailSociete"></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Site internet : </label>
                        </td>
                        <td>
                            <input type="text" name="Agenda[siteweb]" id="siteweb"
                                   value="<?php if (isset($objAgenda->siteweb) && $objAgenda->siteweb != "") echo htmlspecialchars($objAgenda->siteweb); else echo 'http://www.'; ?>"
                                   class="stl_long_input_platinum form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Lien Facebook : </label>
                        </td>
                        <td>
                            <input type="text" name="Agenda[facebook]" id="facebook"
                                   value="<?php if (isset($objAgenda->facebook)) echo htmlspecialchars($objAgenda->facebook); ?>"
                                   class="stl_long_input_platinum form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Lien Twitter : </label>
                        </td>
                        <td>
                            <input type="text" name="Agenda[twitter]" id="twitter"
                                   value="<?php if (isset($objAgenda->twitter)) echo htmlspecialchars($objAgenda->twitter); ?>"
                                   class="stl_long_input_platinum form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Lien Google + : </label>
                        </td>
                        <td>
                            <input type="text" name="Agenda[googleplus]" id="googleplus"
                                   value="<?php if (isset($objAgenda->googleplus)) echo htmlspecialchars($objAgenda->googleplus); ?>"
                                   class="stl_long_input_platinum form-control"/>
                        </td>
                    </tr>
                </table>
            </div>


            <div class="div_stl_long_platinum">Détails sur la manifestation</div>

            <div class="col-xs-12 greybg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:20px;">
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Date de dépôt : </label>
                        </td>
                        <td>
                            <input type="text" name="Agenda[date_depot]" id="date_depot"
                                   value="<?php if (isset($objAgenda->date_depot)) echo convert_Sqldate_to_Frenchdate($objAgenda->date_depot); else echo convert_Sqldate_to_Frenchdate(date("Y-m-d")); ?>"
                                   class="stl_long_input_platinum form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Catégorie : </label>
                        </td>
                        <td>
                            <select name="Agenda[agenda_categid]" onchange="javascript:listeSousRubrique();"
                                    id="IdCategory" class="stl_long_input_platinum form-control">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if (sizeof($colCategorie)) { ?>
                                    <?php foreach ($colCategorie as $objcolCategorie_xx) { ?>
                                        <option <?php if (isset($objAgenda->agenda_categid) && $objAgenda->agenda_categid == $objcolCategorie_xx->agenda_categid) echo 'selected="selected"'; ?>
                                            value="<?php echo $objcolCategorie_xx->agenda_categid; ?>"><?php echo $objcolCategorie_xx->category; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr id='trReponseRub'>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Sous-catégorie : </label>
                        </td>
                        <td>
                            <select name="Agenda[agenda_subcategid]" id="IdSubcategory"
                                    class="stl_long_input_platinum form-control" disabled>
                                <option value="0">-- Veuillez choisir --</option>
                                <?php if (sizeof($colSousCategorie)) { ?>
                                    <?php foreach ($colSousCategorie as $objcolSousCategorie_xx) { ?>
                                        <option <?php if (isset($objAgenda->agenda_subcategid) && $objAgenda->agenda_subcategid == $objcolSousCategorie_xx->agenda_subcategid) echo 'selected="selected"'; ?>
                                            value="<?php echo $objcolSousCategorie_xx->agenda_subcategid; ?>"><?php echo $objcolSousCategorie_xx->subcateg; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>

                    <?php //if (isset($user_groups->id) && $user_groups->id == 3) {} else {?>
                    <!--<tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Intégration à un festival : </label>
                      </td>
                        <td>
                            <select name="Agenda[IdFestival]" id="IdFestival" class="stl_long_input_platinum">
                                <option value="0">-- Veuillez choisir --</option>
                            </select>
                        </td>
                    </tr>-->
                    <?php //} ?>

                    <tr>
                        <td class="stl_long_input_platinum_td" colspan="2">
                            <label class="label">Description de la manifestation : </label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <textarea name="Agenda[description]"
                                      id="description"><?php if (isset($objAgenda->description)) echo htmlspecialchars($objAgenda->description); ?></textarea>
                            <?php echo display_ckeditor($ckeditor0); ?>
                        </td>
                    </tr>
                    <!--<tr>
        <td class="stl_long_input_platinum_td">
            <label class="label">Accès handicapés :</label>
        </td>
        <td>
          <input type="radio" value="1" name="handicapes_radio" id="handicapes_1" <?php if (isset($objAgenda->handicapes) && $objAgenda->handicapes == "1") { ?>checked<?php } ?>/> OUI
          <input type="radio" value="0" name="handicapes_radio" id="handicapes_0" <?php if (isset($objAgenda->handicapes) && $objAgenda->handicapes == "0") { ?>checked<?php } ?>/> NON
          <input type="hidden" name="Agenda[handicapes]" id="handicapes" value="<?php if (isset($objAgenda->handicapes)) echo htmlspecialchars($objAgenda->handicapes); ?>">
        </td>
    </tr>
    <tr> 
        <td class="stl_long_input_platinum_td">
            <label class="label">Parking</label>
        </td>
        <td>
          <input type="radio" value="1" name="parking_radio" id="parking_1" <?php if (isset($objAgenda->parking) && $objAgenda->parking == "1") { ?>checked<?php } ?>/> OUI
          <input type="radio" value="0" name="parking_radio" id="parking_0" <?php if (isset($objAgenda->parking) && $objAgenda->parking == "0") { ?>checked<?php } ?>/> NON
          <input type="hidden" name="Agenda[parking]" id="parking" value="<?php if (isset($objAgenda->parking)) echo htmlspecialchars($objAgenda->parking); ?>">
        </td>
    </tr>-->
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            Date de début *
                            :
                        </td>
                        <td>
                            <input type="text" name="Agenda[date_debut]" id="date_debut"
                                   value="<?php if (isset($objAgenda->date_debut)) echo convert_Sqldate_to_Frenchdate($objAgenda->date_debut); ?>"
                                   class="stl_long_input_platinum form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">Date de fin *:</td>
                        <td>
                            <input type="text" name="Agenda[date_fin]" id="date_fin"
                                   value="<?php if (isset($objAgenda->date_fin)) echo convert_Sqldate_to_Frenchdate($objAgenda->date_fin); ?>"
                                   class="stl_long_input_platinum form-control"/>
                        </td>
                    </tr>
                </table>
            </div>


            <div class="div_stl_long_platinum">Tarif et Horaires</div>
            <div class="col-xs-12 greybg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:20px;">

                    <tr>
                        <td class="stl_long_input_platinum_td" colspan="2">Manifestation gratuite :
                            <input type="checkbox" name="manif_gratuite_checkbox" id="manif_gratuite_checkbox"
                                   <?php if (isset($objAgenda->manif_gratuite) && $objAgenda->manif_gratuite == "1") { ?>checked<?php } ?>/>
                            <input type="hidden" name="Agenda[manif_gratuite]" id="manif_gratuite"
                                   value="<?php if (isset($objAgenda->manif_gratuite)) echo $objAgenda->manif_gratuite; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td" colspan="2">
                            <label class="label">Description des Tarifs et Horaires : </label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <textarea name="Agenda[description_tarif]"
                                      id="description_tarif"><?php if (isset($objAgenda->description_tarif)) echo htmlspecialchars($objAgenda->description_tarif); ?></textarea>
                            <?php echo display_ckeditor($ckeditor1); ?><br/><br/>
                        </td>
                    </tr>


                    <?php if (isset($user_groups->id) && $user_groups->id == 3) {
                    } else { ?>
                    <tr>
                        <td class="stl_long_input_platinum_td">Lien de réservation en ligne :</td>
                        <td>
                            <input type="text" name="Agenda[reservation_enligne]" id="reservation_enligne"
                                   value="<?php if (isset($objAgenda->reservation_enligne)) echo htmlspecialchars($objAgenda->reservation_enligne); ?>"
                                   class="stl_long_input_platinum form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td" colspan="2">Ajouter notre BonPlan :
                            <input type="checkbox" name="ajout_bonplan_checkbox" id="ajout_bonplan_checkbox"
                                   <?php if (isset($objAgenda->ajout_bonplan) && $objAgenda->ajout_bonplan == "1") { ?>checked<?php } ?>/>
                            <input type="hidden" name="Agenda[ajout_bonplan]" id="ajout_bonplan"
                                   value="<?php if (isset($objAgenda->ajout_bonplan)) echo $objAgenda->ajout_bonplan; ?>"/>
                            &nbsp;<img src="<?php echo base_url(); ?>application/resources/front/images/wait.gif"
                                       alt="loading...." id="img_loaging_agenda_addbonplan"/>
                            <span id="spam_error_bonplan_agenda"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td" colspan="2">
                            <label class="label">Si promo : précisez les conditions : </label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <textarea name="Agenda[conditions_promo]"
                                      id="conditions_promo"><?php if (isset($objAgenda->conditions_promo)) echo htmlspecialchars($objAgenda->conditions_promo); ?></textarea>
                            <?php echo display_ckeditor($ckeditor2); ?>
                        </td>
                    </tr>

                </table>
            </div>

            <div class="div_stl_long_platinum">Intégration de documents multimédias</div>

            <div class="col-xs-12 greybg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:20px;">

                    <input type="hidden" name="photo1Associe" id="photo1Associe"
                           value="<?php if (isset($objAgenda->photo1)) echo htmlspecialchars($objAgenda->photo1); ?>"/>
                    <input type="hidden" name="photo2Associe" id="photo2Associe"
                           value="<?php if (isset($objAgenda->photo2)) echo htmlspecialchars($objAgenda->photo2); ?>"/>
                    <input type="hidden" name="photo3Associe" id="photo3Associe"
                           value="<?php if (isset($objAgenda->photo3)) echo htmlspecialchars($objAgenda->photo3); ?>"/>
                    <input type="hidden" name="photo4Associe" id="photo4Associe"
                           value="<?php if (isset($objAgenda->photo4)) echo htmlspecialchars($objAgenda->photo4); ?>"/>
                    <input type="hidden" name="photo5Associe" id="photo5Associe"
                           value="<?php if (isset($objAgenda->photo5)) echo htmlspecialchars($objAgenda->photo5); ?>"/>


                    <?php
                    if (isset($mssg) && $mssg != '1') {
                        $img_error_verify_array = preg_split('/-/', $mssg);
                    }
                    ?>

                    <?php if (isset($objAgenda->id) && $objAgenda->id != '0') { ?>
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Photo 1 : </label>
                            </td>
                            <td>
                                <div id="ArticlePhoto1_container">
                                    <?php if (!empty($objAgenda->photo1)) { ?>
                                        <?php
                                        echo '<img  src="' . base_url() . 'application/resources/front/photoCommercant/imagesbank/' . $objAgenda->IdUsers_ionauth . '/' . $objAgenda->photo1 . '" width="200"/>';
                                        ?>
                                        <a href="javascript:void(0);" class="btn btn-danger"
                                           onclick="deleteFile('<?php echo $objAgenda->id; ?>','photo1');">Supprimer</a>
                                    <?php } else { ?>
                                        <a onclick='javascript:window.open("<?php echo site_url("media/index/?icat=" . $objAgenda->id . "&cat=ag&img=photo1"); ?>", "<?php echo $commercant_url_home; ?>", "width=1045, height=675, scrollbars=yes");'
                                           href='javascript:void(0);' title="Photo 1" class="btn btn-default"
                                           id="ArticlePhoto1_link">Ajouter la photo 1</a>
                                    <?php } ?>
                                </div>
                                <div id="div_error_taille_Photo1"
                                     class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Photo 2 : </label>
                            </td>
                            <td>
                                <div id="ArticlePhoto2_container">
                                    <?php if (!empty($objAgenda->photo2)) { ?>
                                        <?php
                                        echo '<img  src="' . base_url() . 'application/resources/front/photoCommercant/imagesbank/' . $objAgenda->IdUsers_ionauth . '/' . $objAgenda->photo2 . '" width="200"/>';
                                        ?>
                                        <a href="javascript:void(0);" class="btn btn-danger"
                                           onclick="deleteFile('<?php echo $objAgenda->id; ?>','photo2');">Supprimer</a>
                                    <?php } else { ?>
                                        <a onclick='javascript:window.open("<?php echo site_url("media/index/?icat=" . $objAgenda->id . "&cat=ag&img=photo2"); ?>", "<?php echo $commercant_url_home; ?>", "width=1045, height=675, scrollbars=yes");'
                                           href='javascript:void(0);' title="Photo 2" class="btn btn-default"
                                           id="ArticlePhoto2_link">Ajouter la photo 2</a>
                                    <?php } ?>
                                </div>
                                <div id="div_error_taille_Photo2"
                                     class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo2", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Photo 3 : </label>
                            </td>
                            <td>
                                <div id="ArticlePhoto3_container">
                                    <?php if (!empty($objAgenda->photo3)) { ?>
                                        <?php
                                        echo '<img  src="' . base_url() . 'application/resources/front/photoCommercant/imagesbank/' . $objAgenda->IdUsers_ionauth . '/' . $objAgenda->photo3 . '" width="200"/>';
                                        ?>
                                        <a href="javascript:void(0);" class="btn btn-danger"
                                           onclick="deleteFile('<?php echo $objAgenda->id; ?>','photo3');">Supprimer</a>
                                    <?php } else { ?>
                                        <a onclick='javascript:window.open("<?php echo site_url("media/index/?icat=" . $objAgenda->id . "&cat=ag&img=photo3"); ?>", "<?php echo $commercant_url_home; ?>", "width=1045, height=675, scrollbars=yes");'
                                           href='javascript:void(0);' title="Photo 3" class="btn btn-default"
                                           id="ArticlePhoto3_link">Ajouter la photo 3</a>
                                    <?php } ?>
                                </div>
                                <div id="div_error_taille_Photo3"
                                     class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo3", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Photo 4 : </label>
                            </td>
                            <td>
                                <div id="ArticlePhoto4_container">
                                    <?php if (!empty($objAgenda->photo4)) { ?>
                                        <?php
                                        echo '<img  src="' . base_url() . 'application/resources/front/photoCommercant/imagesbank/' . $objAgenda->IdUsers_ionauth . '/' . $objAgenda->photo4 . '" width="200"/>';
                                        ?>
                                        <a href="javascript:void(0);" class="btn btn-danger"
                                           onclick="deleteFile('<?php echo $objAgenda->id; ?>','photo4');">Supprimer</a>
                                    <?php } else { ?>
                                        <a onclick='javascript:window.open("<?php echo site_url("media/index/?icat=" . $objAgenda->id . "&cat=ag&img=photo4"); ?>", "<?php echo $commercant_url_home; ?>", "width=1045, height=675, scrollbars=yes");'
                                           href='javascript:void(0);' title="Photo 4" class="btn btn-default"
                                           id="ArticlePhoto4_link">Ajouter la photo 4</a>
                                    <?php } ?>
                                </div>
                                <div id="div_error_taille_Photo4"
                                     class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo4", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="stl_long_input_platinum_td">
                                <label class="label">Photo 5 : </label>
                            </td>
                            <td>
                                <div id="ArticlePhoto5_container">
                                    <?php if (!empty($objAgenda->photo5)) { ?>
                                        <?php
                                        echo '<img  src="' . base_url() . 'application/resources/front/photoCommercant/imagesbank/' . $objAgenda->IdUsers_ionauth . '/' . $objAgenda->photo5 . '" width="200"/>';
                                        ?>
                                        <a href="javascript:void(0);" class="btn btn-danger"
                                           onclick="deleteFile('<?php echo $objAgenda->id; ?>','photo5');">Supprimer</a>
                                    <?php } else { ?>
                                        <a onclick='javascript:window.open("<?php echo site_url("media/index/?icat=" . $objAgenda->id . "&cat=ag&img=photo5"); ?>", "<?php echo $commercant_url_home; ?>", "width=1045, height=675, scrollbars=yes");'
                                           href='javascript:void(0);' title="Photo 5" class="btn btn-default"
                                           id="ArticlePhoto5_link">Ajouter la photo 5</a>
                                    <?php } ?>
                                </div>
                                <div id="div_error_taille_Photo5"
                                     class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo5", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.'; ?></div>
                            </td>
                        </tr>
                    <?php } else { ?>
                        <tr>
                            <td colspan="2">
                                <label class="alert alert-danger">Veuillez enregistrer votre article avant d'ajouter les
                                    images !</label>
                            </td>
                        </tr>
                    <?php } ?>


                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Lien vers votre vidéo<br/>YouTube ou DailyMotion : </label>
                        </td>
                        <td>
                            <span style="font-family: Verdana, Geneva, sans-serif; font-size:9px;">http://www.youtube.com/watch?v=IMX1M46MQAI</span><br/>
                            <input type="text" name="Agenda[video]" id="video"
                                   value="<?php if (isset($objAgenda->video)) echo htmlspecialchars($objAgenda->video); ?>"
                                   class="stl_long_input_platinum form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Lien vers un fichier audio : </label>
                        </td>
                        <td>
                            <input type="text" name="Agenda[audio]" id="audio"
                                   value="<?php if (isset($objAgenda->audio)) echo htmlspecialchars($objAgenda->audio); ?>"
                                   class="stl_long_input_platinum form-control"/>
                        </td>
                    </tr>
                    <tr valign="middle">
                        <td class="stl_long_input_platinum_td" valign="top">
                            <label class="label">Document PDF : </label>
                            <input type="hidden" name="pdfAssocie" id="pdfAssocie"
                                   value="<?php if (isset($objAgenda->pdf)) echo htmlspecialchars($objAgenda->pdf); ?>"/>
                        </td>
                        <td valign="top">
                            <?php if (!empty($objAgenda->pdf)) { ?>
                                <?php //echo $objAgenda->Pdf; ?>

                                <?php if ($objAgenda->pdf != "" && $objAgenda->pdf != NULL && file_exists("application/resources/front/images/agenda/pdf/" . $objAgenda->pdf) == true) { ?>
                                <a
                                    href="<?php echo base_url(); ?>application/resources/front/images/agenda/pdf/<?php echo $objAgenda->pdf; ?>"
                                    target="_blank"><img
                                        src="<?php echo base_url(); ?>application/resources/front/images/pdf_icone.png"
                                        width="64" alt="PDF"/></a><?php } ?>


                                <a href="javascript:void(0);"
                                   onclick="deleteFile('<?php echo $objAgenda->id; ?>','pdf');">Supprimer</a><br/>

                            <?php } else { ?>
                                <input type="file" name="AgendaPdf" id="AgendaPdf" value=""
                                       class="stl_long_input_platinum"/>
                            <?php } ?>
                        </td>
                    </tr>

                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Titre doc PDF : </label>
                        </td>
                        <td>
                            <input type="text" name="Agenda[titre_pdf]" id="titre_pdf"
                                   value="<?php if (isset($objAgenda->titre_pdf)) echo $objAgenda->titre_pdf; ?>"
                                   class="stl_long_input_platinum form-control"/>
                        </td>
                    </tr>

                    <!--<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Intégration autre document : </label>
        <input type="hidden" name="autre_doc_1Associe" id="autre_doc_1Associe" value="<?php //if(isset($objAgenda->autre_doc_1)) echo htmlspecialchars($objAgenda->autre_doc_1); ?>" />
  </td>
    <td>
        <?php //if(!empty($objAgenda->autre_doc_1)) { ?>
            <?php //echo $objAgenda->autre_doc_1; ?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php //echo $objAgenda->id; ?>','autre_doc_1');">Supprimer</a>
        <?php //} else { ?>
        <input type="file" name="Agendaautre_doc_1" id="Agendaautre_doc_1" value=""  class="stl_long_input_platinum"/>
        <?php //} ?>
        <div id="div_error_taille_autre_doc_1" class="div_error_taille_3_4"><?php //if (isset($img_error_verify_array) && in_array("autre_doc_1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>

<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Titre autre doc : </label>
  </td>
    <td>
    	<input type="text" name="Agenda[titre_autre_doc_1]" id="titre_autre_doc_1" value="<?php //if(isset($objAgenda->titre_autre_doc_1)) echo $objAgenda->titre_autre_doc_1; ?>"  class="stl_long_input_platinum"/>
    </td>
</tr>

<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Intégration autre document : </label>
        <input type="hidden" name="autre_doc_2Associe" id="autre_doc_2Associe" value="<?php //if(isset($objAgenda->autre_doc_2)) echo htmlspecialchars($objAgenda->autre_doc_2); ?>" />
  </td>
    <td>
        <?php //if(!empty($objAgenda->autre_doc_2)) { ?>
            <?php //echo $objAgenda->autre_doc_2; ?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php //echo $objAgenda->id; ?>','autre_doc_2');">Supprimer</a>
        <?php //} else { ?>
        <input type="file" name="Agendaautre_doc_2" id="Agendaautre_doc_2" value=""  class="stl_long_input_platinum"/>
        <?php //} ?>
        <div id="div_error_taille_autre_doc_2" class="div_error_taille_3_4"><?php //if (isset($img_error_verify_array) && in_array("autre_doc_2", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>

<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Titre autre doc : </label>
  </td>
    <td>
    	<input type="text" name="Agenda[titre_autre_doc_2]" id="titre_autre_doc_2" value="<?php //if(isset($objAgenda->titre_autre_doc_2)) echo $objAgenda->titre_autre_doc_2; ?>"  class="stl_long_input_platinum"/>
    </td>
</tr>
-->
                    <?php } ?>

                </table>
            </div>

            <div class="div_stl_long_platinum">Lieu de l'évènnement</div>

            <div class="col-xs-12 greybg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:20px;">
                    <tr>
                        <td class="stl_long_input_platinum_td">L’adresse est-elle celle<br/>précisée par le diffuseur :
                        </td>
                        <td>
                            <input type="checkbox" name="adresse_localisation_diffuseur_checkbox"
                                   id="adresse_localisation_diffuseur_checkbox"
                                   <?php if (isset($objAgenda->adresse_localisation_diffuseur) && $objAgenda->adresse_localisation_diffuseur == "1") { ?>checked<?php } ?>/>
                            <input type="hidden" name="Agenda[adresse_localisation_diffuseur]"
                                   id="adresse_localisation_diffuseur"
                                   value="<?php if (isset($objAgenda->adresse_localisation_diffuseur)) echo htmlspecialchars($objAgenda->adresse_localisation_diffuseur); ?>">
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Nom du lieu : </label>
                        </td>
                        <td>
                            <input type="text" name="Agenda[nom_localisation]" id="nom_localisation"
                                   value="<?php if (isset($objAgenda->nom_localisation)) echo $objAgenda->nom_localisation; ?>"
                                   class="stl_long_input_platinum form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Adresse : </label>
                        </td>
                        <td>
                            <input type="text" name="Agenda[adresse_localisation]" id="adresse_localisation"
                                   value="<?php if (isset($objAgenda->adresse_localisation)) echo $objAgenda->adresse_localisation; ?>"
                                   class="stl_long_input_platinum form-control"/>
                        </td>
                    </tr>


                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Code Postal : </label>
                        </td>
                        <td id="trReponseVille_localisation">
                            <input type="text" name="Agenda[codepostal_localisation]" id="codepostal_localisation"
                                   value="<?php if (isset($objAgenda->codepostal_localisation)) echo $objAgenda->codepostal_localisation; ?>"
                                   class="stl_long_input_platinum form-control"
                                   onblur="javascript:CP_getDepartement_localisation();CP_getVille_localisation();"/>
                        </td>
                    </tr>

                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Departement : </label>
                        </td>
                        <td>
    	<span id="departementCP_container_localisation">
        <select name="Agenda[departement_id_localisation]" id="DepartementAgendaLocalisation" disabled="disabled"
                class="stl_long_input_platinum form-control" onchange="javascript:CP_getVille_D_CP_localisation();">
            <option value="0">-- Veuillez choisir --</option>
            <?php if (sizeof($colDepartement)) { ?>
                <?php foreach ($colDepartement as $objDepartement) { ?>
                    <option
                        <?php if (isset($objAgenda->departement_id_localisation) && $objAgenda->departement_id_localisation == $objDepartement->departement_id) { ?>selected="selected"<?php } ?>
                        value="<?php echo $objDepartement->departement_id; ?>"><?php echo $objDepartement->departement_nom . " - " . $objDepartement->departement_code; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
        </span>
                        </td>
                    </tr>

                    <tr>
                        <td class="stl_long_input_platinum_td">
                            <label class="label">Ville : </label>
                        </td>
                        <td>
    	<span id="villeCP_container_localisation">
        <input type="text"
               value="<?php if (isset($objAgenda->IdVille_localisation)) echo $this->mdlville->getVilleById($objAgenda->IdVille_localisation)->Nom; ?>"
               name="IdVille_Nom_text_localisation" id="IdVille_Nom_text_localisation" disabled="disabled"
               class="stl_long_input_platinum form-control"/>
        <input type="hidden"
               value="<?php if (isset($objAgenda->IdVille_localisation)) echo $objAgenda->IdVille_localisation; ?>"
               name="Agenda[IdVille_localisation]" id="IdVille_localisation"/>
        </span>
                        </td>
                    </tr>


                    <tr>
                        <td colspan="2">
                            <?php if (isset($ville_to_show_onn_map)) $ville_map = $ville_to_show_onn_map; else $ville_map = ''; ?>
                            <?php if (isset($objAgenda->adresse_localisation)) { ?>
                                <iframe width="100%" height="483" frameborder="0" scrolling="no" marginheight="0"
                                        marginwidth="0"
                                        src="http://maps.google.fr/maps?f=q&amp;source=s_q&amp;hl=fr&amp;geocode=&amp;q=<?php echo $objAgenda->adresse_localisation . ", " . $objAgenda->codepostal_localisation . " &nbsp;" . $ville_map; ?>&amp;aq=0&amp;ie=UTF8&amp;hq=&amp;hnear=<?php echo $objAgenda->adresse_localisation . ", " . $objAgenda->codepostal_localisation . " &nbsp;" . $ville_map; ?>&amp;t=m&amp;vpsrc=0&amp;output=embed"></iframe>
                            <?php } ?>
                        </td>
                    </tr>


                </table>
            </div>


            <div class="div_stl_long_platinum col-xs-12">Commentaires Facebook</div>
            <div class="col-xs-12 greybg" style="padding-bottom: 15px;">
                <div class="col-xs-3" style="padding:0; width:50%; float:left;">Activation commentaires Facebook</div>
                <div class="col-xs-9" style="padding:0; text-align:right; width:50%; float:left;">
                    <select id="activ_fb_comment_article" name="Agenda[activ_fb_comment]" style="margin-right: 25px;"
                            class="form-control">
                        <option
                            <?php if (isset($objAgenda->activ_fb_comment) && $objAgenda->activ_fb_comment == "0") { ?>selected="selected"<?php } ?>
                            value="0">NON
                        </option>
                        <option
                            <?php if (isset($objAgenda->activ_fb_comment) && $objAgenda->activ_fb_comment == "1") { ?>selected="selected"<?php } ?>
                            value="1">OUI
                        </option>
                    </select>
                </div>
            </div>


            <div class="div_stl_long_platinum">La Diffusion</div>
            <table style="width: 100%;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Publication</legend>
                            <table border="0" cellspacing="0" cellpadding="0" style="padding-left:20px;">
                                <tr>
                                    <td><input type="radio" name="admin_agenda_activation"
                                               id="admin_agenda_activation_1"
                                               <?php if (isset($objAgenda->IsActif) && $objAgenda->IsActif == "1") { ?>checked<?php } ?>/>
                                    </td>
                                    <td> Actif</td>
                                </tr>
                                <tr>
                                    <td><input type="radio" name="admin_agenda_activation"
                                               id="admin_agenda_activation_0"
                                               <?php if (isset($objAgenda->IsActif) && $objAgenda->IsActif == "0") { ?>checked<?php } ?>/>
                                    </td>
                                    <td> En attente de validation</td>
                                </tr>
                                <tr>
                                    <td><input type="radio" name="admin_agenda_activation"
                                               id="admin_agenda_activation_2"
                                               <?php if (isset($objAgenda->IsActif) && $objAgenda->IsActif == "2") { ?>checked<?php } ?>/>
                                    </td>
                                    <td> Annulé</td>
                                </tr>
                                <tr>
                                    <td><input type="radio" name="admin_agenda_activation"
                                               id="admin_agenda_activation_3"
                                               <?php if (isset($objAgenda->IsActif) && $objAgenda->IsActif == "3") { ?>checked<?php } ?>/>
                                    </td>
                                    <td> Supprimé</td>
                                </tr>


                            </table>
                            <input type="hidden" name="Agenda[IsActif]" id="IsActif"
                                   value="<?php if (isset($objAgenda->IsActif)) echo htmlspecialchars($objAgenda->IsActif); else echo "0"; ?>">
                        </fieldset>
                        <p>&nbsp;</p>
                    </td>
                    <td>

                        <div style="padding:15px; text-align:center;">
                            <button type="button" class="btnSinscrire btn btn-success" value="Validation du dépôt">
                                Enregistrement
                            </button>
                        </div>

                        <div style="text-align:center; margin-top:40px; padding: 15px;" id="id_save_agenda_before_view">
                            <a href="<?php if (isset($objAgenda->id)) echo site_url("agenda/preview_agenda/" . $objAgenda->id); else echo "javascript:alert_save_agenda_before_view();"; ?>" <?php if (isset($objAgenda->id)) echo 'target="_blank"'; ?>><img
                                    src="<?php echo GetImagePath("front/"); ?>/visualisezsite.png"
                                    alt="previsualisatiton_agenda" title="previsualisatiton_agenda"/></a>
                        </div>

                    </td>
            </table>

            <div class="div_stl_long_platinum">Partager cet évènement</div>
            <div style="padding:0 !important; text-align:center ; margin-top:40px ; padding-bottom:35px;"><a
                    href=" https://www.facebook.com/sharer/sharer.php?u={u}"><img
                        src="<?php echo GetImagePath("privicarte/"); ?>/wp9efa6d49_06.png"/></a></div>
            <div style="padding:0 !important; text-align:center ; margin-top:40px ; padding-bottom:35px;"><a
                    href=" https://twitter.com/intent/tweet/?url={url}&text={text}&via={via}"><img
                        src="<?php echo GetImagePath("privicarte/"); ?>/wp9cd46019_06.png"/></a></div>
        </form>


        <div id="div_error_agenda_submit" style="color:#FF0000; text-align:center; width:100%; margin-top:40px;"></div>


    </div>


<?php $this->load->view("frontAout2013/includes/footer_mini_2"); ?>