<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=utf-8" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url('application/views/front_plat_du_jour/admin/css/style.css')?>" type="text/css" />
</head>
<body>
    <form id="form_all_data" method="post" enctype="multipart/form-data" action="<?php echo site_url();?>front/Plat_du_jour/save_entete">

        <input type="hidden" id="activ_glissiere_emporter_value" value="1">
        <input type="hidden" id="activ_glissiere_condition_value" value="0">
        <input type="hidden" id="activ_glissiere_livraison_value" value="0">
        <input type="hidden" id="activ_glissiere_activ_plat1_value" value="0">
        <input type="hidden" id="activ_glissiere_help_value" value="1">
        <input type="hidden" id="activ_glissiere_differe_value" value="1">
        <input type="hidden" id="activ_glissiere_prom_value" value="1">
        <input type="hidden" id="activ_glissiere_activ_plat2_value" value="0">
        <input type="hidden" id="activ_glissiere_activ_plat3_value" value="0">
        <input type="hidden" id="activ_glissiere_activ_plat4_value" value="0">
        <input type="hidden" id="activ_glissiere_activ_plat5_value" value="0">
        <input type="hidden" id="activ_glissiere_activ_plat6_value" value="0">
        <input type="hidden" id="activ_glissiere_activ_plat7_value" value="0">
        <input type="hidden" id="activ_glissiere_activ_plat8_value" value="0">
        <input type="hidden" id="idcommercant_get" name="menu[id_commercant]" value="<?php if (isset($idCommercant) && $idCommercant != "" && $idCommercant != 0){echo $idCommercant;}else{echo "0";} ?>" />

        <style>
            .disabledbutton {
                pointer-events: none!important;
                opacity: 0.4!important;
            }
        </style>
        <div class="container cont_all">
            <div class="row pt-5">
                <div class="col-12 text-center">
                    <a onclick="javascript:void(0)" href="<?php echo site_url('admin'); ?>">
                        <div class="btn btn-secondary">Menu admin</div>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-12 pt-5 text-center">
                    <div class="w-50 text-center m-auto">
                        <div class="row">
                            <div class="col-12 text-center pb-3 lab">
                                Titre du menu " PLATS DU JOUR "
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-9 pr-0">
                                <input value="<?php if (isset($menu->menu_titre)) echo $menu->menu_titre; ?>" type="text" class="form-control" name="menu[menu_titre]">
                            </div>
                            <div class="col-3 pl-0">
                                <button class="btn btn-secondary w-100">Enregistrez</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 pt-3 text-center lab">
                                " PLATS DU JOUR" est affiché si le titre du menu est vide
                            </div>
                        </div>
                    </div>

                </div>
            </div>


<div class="col-lg-3 text-right text-justify">
                <input <?php if (isset($menu->actif_res_plat) AND $menu->actif_res_plat == '1'){echo 'checked';} ?> onclick="
                                                                    if($('#actif_res_plat').is(':checked')){
                                                                        $('#val_check_res').val('1');
                                                                    }else{
                                                                        $('#val_check_res').val('0');
                                                                    }
                                                                " type="checkbox" name="" id="actif_res_plat">Je désire que ce bouton s’intègre sur la colonne des menus de mon site afin de bénéficier du formulaire de réservation en ligne
                <input id="val_check_res" type="hidden" name="menu[actif_res_plat]" value="<?php if (isset($menu->actif_res_plat) AND $menu->actif_res_plat)echo $menu->actif_res_plat ?>">
</div>

   <script type="text/javascript">
    function delete_img(id_com){
        var data = "idcom="+id_com;
        $.ajax({
            type: "POST",
            url: "https://www.sortez.org/front/Plat_du_jour/delete_img/"+id_com,
            data: data ,
            success: function() {
                if (data != "") {
                    var to_open = "'https://www.sortez.org/media/index/"+id_com+"-res_plat-photo_menu_gen"+""+"' , '' ,'width=1045 , height=675, scrollbars=yes'"
                    var to_ch = '<div id="Menuphoto_menu_gen_container" onclick="javascript:window.open('+to_open+');", href="javascript:void(0);" class="w-100" ><img  src="https://www.sortez.org/assets/image/download-icon-png-5.jpg" style="height: 160px"></div>';
                    $("#img_menu_gen_contents").html(to_ch);
               }
            },
            error: function() {
                alert(data);
            }
        });
    }
</script>

<form method="post" action="<?php echo site_url();?>front/annonce/save_icon_btc" class="row pb-5 shadowed p-2 gli_content d-flex">
    <input type="hidden"  value="<?php echo $idCommercant ?>" />
    <div class="col-4 text-center">

        choisissez la couleur du bouton en cliquant sur le panel 
        <input type="color" name="menu[color]" style="width:150px; height: 150px"  value="<?php if (isset($menu->color)) echo $menu->color; ?>">
    </div>

    <div class="col-4">
        <div class="w-100 text-center top_menu_gen">
            Télécharger l'image de personnalisation, Cliquez sur l'icone !...
        </div>
        <div class="w-100 text-center" id="img_menu_gen_contents">
            <?php if (isset($menu->img) AND $menu->img !=null && is_file("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/res_plat/".$menu->img)){ ?>
                <div class="w-100">
                    <img style="max-width: 200px" src="<?php echo base_url()?>application/resources/front/photoCommercant/imagesbank/<?php echo $infocom->user_ionauth_id; ?>/res_plat/<?php echo $menu->img ?>">
                    <a onclick="delete_img(<?php echo $infocom->IdCommercant; ?>)" class="btn btn-danger mt-3">Supprimer image</a>
                </div>
            <?php }else{ ?>
                <div class="w-100" id="Menuphoto_menu_gen_container">
                    <img onclick='javascript:window.open("<?php echo site_url("media/index/".$infocom->IdCommercant."-res_plat-photo_menu_gen"); ?>", "", "width=1045, height=675, scrollbars=yes");' src="<?php echo base_url()?>assets/image/download-icon-png-5.jpg">
                </div>
                <div id="Articleimg_menuphoto_menu_gen_container"></div>
            <?php } ?>
        </div>
    </div>

    <div class="col-4">
        <div class="w-100 top_menu_gen">
            Précisez le texte
            du bouton
        </div>
        <div class="w-100 text-center menu_txt_container">
            <input maxlength="22" value="<?php if (isset($menu->menu_value)) echo $menu->menu_value; ?>" type="text" class="form-control input_menu_txt" name="menu[menu_value]">
            <?php if($error_message!=NULL):?>
           <div class="alert alert-danger mt-2" role="alert">
                <strong>Erreur ! </strong><?php print_r($error_message);?>
            </div>
            <?php endif;?>
        </div>
    </div>
    <div class="w-100 text-center pt-4 pb-4">
        <input type="submit" class="btn btn-secondary  w-50" />
    </div>
</form>

            <div class="row">
                <div class="col-12 pt-5 pb-4 text-center titre_big">
                    CREATION DE PLATS DU JOUR
                </div>
                <div class="col-12 text-center">
                    <div class="w-50 m-auto">
                        <ol class="font_7" style="color:rgb(0, 0, 0); font-family:futura-lt-w01-book,sans-serif; font-size:14px; font-style:normal; font-weight:400;">
                            <li>
                                <p class="font_7"><span style="font-style:normal;"><span style="font-weight:400;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Choisissez les glissières à remplir et activez les ;</span></span></span></span></span></p>
                            </li>
                            <li>
                                <p class="font_7"><span style="font-style:normal;"><span style="font-weight:400;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Choisissez et remplissez les champs préconfigurés ;</span></span></span></span></span></p>
                            </li>
                            <li>
                                <p class="font_7"><span style="font-style:normal;"><span style="font-weight:400;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:futura-lt-w01-book,sans-serif;">N'oubliez pas&nbsp;de valider chaque dépôt pour chaque glissière avec le bouton "Validation" ;</span></span></span></span></span></p>
                            </li>
                            <li>
                                <p class="font_7"><span style="font-style:normal;"><span style="font-weight:400;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Une fois que vous avez intégrer la totalité de vos données, vous pouvez visualiser le formulaire de commande en ligne des consommateurs ;</span></span></span></span></span></p>
                            </li>
                            <li>
                                <p class="font_7"><span style="font-style:normal;"><span style="font-weight:400;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:futura-lt-w01-book,sans-serif;">Soit il est conforme et définitif ou revenez sur ce formulaire pour y apporter les modifications qui s'imposent.</span></span></span></span></span></p>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="container cont_all mb-5">
        <?php $this->load->view('front_plat_du_jour/admin/includes/gli1') ?>
        <?php $this->load->view('front_plat_du_jour/admin/includes/gli2') ?>
        <?php $this->load->view('front_plat_du_jour/admin/includes/gli3') ?>
        <?php $this->load->view('front_plat_du_jour/admin/includes/gli4') ?>
        <?php $this->load->view('front_plat_du_jour/admin/includes/gli5') ?>
        <?php $this->load->view('front_plat_du_jour/admin/includes/gli6') ?>
        <?php $this->load->view('front_plat_du_jour/admin/includes/gli7') ?>
        <?php $this->load->view('front_plat_du_jour/admin/includes/gli8') ?>

        <?php $this->load->view('front_plat_du_jour/admin/includes/generate_qr_code'); ?>

    </div>
<script type="text/javascript">
    function save_plat(id_plat){
        var id_gli = id_plat;
        var idcommercant_get = $('#idcommercant_get').val();
        var title_gli_plat_is_activ = $('#title_gli_'+id_plat+'_is_activ').val();
        var title_gli_plat = $('#title_gli_'+id_plat).val();
        var designation_gli_plat = $('#designation_gli_'+id_plat).val();
        var date1_plat = $('#date1_'+id_plat).val();
        var date2_plat = $('#date2_'+id_plat).val();
        var date3_plat = $('#date3_'+id_plat).val();
        var date4_plat = $('#date4_'+id_plat).val();
        var date5_plat = $('#date5_'+id_plat).val();
        var nombre1_plat = $('#nombre1_'+id_plat).val();
        var nombre2_plat = $('#nombre2_'+id_plat).val();
        var nombre3_plat = $('#nombre3_'+id_plat).val();
        var nombre4_plat = $('#nombre4_'+id_plat).val();
        var nombre5_plat = $('#nombre5_'+id_plat).val();
        var heure_fin_plat = $('#heure_fin_'+id_plat).val();
        var prix_plat = $('#prix_'+id_plat).val();
        var datas = "id_gli="+id_gli+"&idcommercant_get="+idcommercant_get+"&title_gli_plat_is_activ="+title_gli_plat_is_activ+"&title_gli_plat="+title_gli_plat+"&designation_gli_plat="+designation_gli_plat+"&date1_plat="+date1_plat+"&date2_plat="+date2_plat+"&date3_plat="+date3_plat+"&date4_plat="+date4_plat+"&date5_plat="+date5_plat+"&nombre1_plat="+nombre1_plat+"&nombre2_plat="+nombre2_plat+"&nombre3_plat="+nombre3_plat+"&nombre4_plat="+nombre4_plat+"&nombre5_plat="+nombre5_plat+"&heure_fin_plat="+heure_fin_plat+"&prix_plat="+prix_plat;
        if(title_gli_plat != "" && title_gli_plat != undefined && title_gli_plat != null) {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('front/Plat_du_jour/save_detail_plat'); ?>",
                data: datas,
                success: function (data) {
                    console.log(data);
                    var datas = JSON.parse(data);
                    if(datas == "ok"){
                        location.reload();
                    }else{
                        alert("erreur d'enregistrement");
                    }
                },
                error: function () {
                    alert('Erreur');
                }
            });
        }else{
            alert("Le Champ 'Titre du plat du jour' est obligatoire");
            throw new Error("champ non rempli");
        }
    }
    function deleteFile(id,img_file){
        var datas = 'id='+id+'&imgfile='+img_file;
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('front/Plat_du_jour/delete_image'); ?>",
            data: datas,
            success: function (data) {
                console.log(data);
                var datas = JSON.parse(data);
                if(datas == "ok"){
                    location.reload();
                }else{
                    alert("erreur d'enregistrement");
                }
            },
            error: function () {
                alert('Erreur');
            }
        });
    }
    function delete_plat_gli(id_plat){
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('front/Plat_du_jour/delete_content'); ?>",
            data: 'id_plat='+id_plat,
            success: function (data) {
                console.log(data);
                var datas = JSON.parse(data);
                if(datas == "ok"){
                    location.reload();
                }else{
                    alert("erreur d'enregistrement");
                }
            },
            error: function () {
                alert('Erreur');
            }
        });
    }
</script>

</body>
</html>



