<div class="row glissiere_tab">
    <form class="m-0 w-100 row d-flex">
        <div class="col-4">
            <div id="txt_title_gli_plat7" class="text-left title_gli titre_gli_up"><?php if (isset($content_plat7->titre_gli)) {echo $content_plat7->titre_gli;}else{echo 'PLAT DU JOUR 7';} ?></div>
        </div>
        <div class="col-4" id="activ_glissiere_activ_plat7">
            <div class="cercle">
                <div class="text-center up d-none" id="activ_glissiere_activ_plat7_up"><p>fermer</p></div>
                <div class="text-center down" id="activ_glissiere_activ_plat7_down"><p>ouvrir</p></div>
            </div>
        </div>
        <div class="col-4 d-flex">
            <div class="text-right w-75 act_desact_text">Activer/désactiver</div>
            <div class="text-right w-25 titre_gli_up">
                <!--                Activé:-->
                <!--                <select id="title_gli1_is_activ" name="data[is_activ_gli1]">-->
                <!--                    <option value="0">Non</option>-->
                <!--                    <option --><?php //if (isset($content_plat7->is_activ_glissiere) AND $content_plat7->is_activ_glissiere == '1' ) echo "selected='selected'";  ?><!-- value="1">Oui</option>-->
                <!--                </select>-->
                <label class="switch">
                    <input id="is_activ_glissiere_plat7" type="checkbox" <?php if (isset($content_plat7->is_activ_glissiere) AND $content_plat7->is_activ_glissiere == '1') echo 'checked'; ?> >
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </form>
</div>
<input name="plat7[id_gli]" type="hidden" value="plat7">
<input name="plat7[is_activ_glissiere]" id="title_gli_plat7_is_activ" type="hidden" value="<?php if (isset($content_plat7->is_activ_glissiere)){echo $content_plat7->is_activ_glissiere;}else{ echo "0";} ?>">
<div class="row pb-5 shadowed p-2 gli_content d-none" id="activ_glissiere_activ_plat7_content">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <div class="row">
                    <div class="col-12 pl-2">
                        <span class="text_inside_gli pt-2 pl-4">Titre du plat du jour</span>
                        <span class="text_inside_gli pr-4 float-right"><span id="char_true_title_1">70 </span>Caractères</span>
                    </div>
                </div>
                <div class="row pb-2 ml-0 mr-1">
                    <div class="col-12 padded_title">
                        <input name="plat7[titre_gli]" maxlength="70" value="<?php if (isset($content_plat7->titre_gli)) {echo $content_plat7->titre_gli;} ?>" id="title_gli_plat7" class="form-control pr-0 textarea_style padded_title_input" type="text">
                        <input id="hidden_title_gli_plat7" value="" type="hidden">
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <p class="pl-4">Image</p>
                        <?php if(isset($content_plat7) && $content_plat7 != null){ ?>
                            <?php if (!empty($content_plat7->image)) { ?>
                                <a href="javascript:void(0);" class="btn btn-danger"

                                   onclick="deleteFile('<?php echo $content_plat7->id; ?>','<?php echo $content_plat7->image; ?>');">x</a>
                            <?php }else{ ?>
                                <div class="btn btn-success btn_ajout_image" onclick='javascript:window.open("<?php echo site_url("media/index/".$content_plat7->id."-plat_gli-image"); ?>", "", "width=1045, height=675, scrollbars=yes");'>+</div>
                            <?php } ?>
                        <?php }else{ ?>
                            <p class="text_inside_gli">données non enregistrés</p>
                        <?php } ?>
                    </div>
                    <div class="col-6">
                        <div id="PlatPhoto_container">
                            <?php if (!empty($content_plat7->image)) { ?>

                                <?php

                                echo '<img  src="' . base_url() . 'application/resources/front/photoCommercant/imagesbank/' . $IdUsers_ionauth . '/' . $content_plat7->image. '" width="200"/>';

                                ?>

                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="row">
                    <div class="col-12 pl-2">
                        <span class="pt-2 pl-4 text_inside_gli">Désignation</span>
                        <span class="text_inside_gli pr-4 float-right"><span id="char_true_title_1">200 </span>Caractères</span>
                    </div>
                </div>
                <div class="row pb-2 ml-0 mr-1">
                    <div class="col-12 padded_title">
                        <textarea name="plat7[designation]" maxlength="200" id="designation_gli_plat7" class="form-control textarea_style_designation"><?php if (isset($content_plat7->designation)) {echo $content_plat7->designation;} ?></textarea>
                        <input id="hidden_designation_gli_plat7" value="" type="hidden">
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                <p class="pl-4 text_inside_gli">Dates de disponiblité</p>
            </div>
            <div class="col_pers_gli ml-3">
                <input name="plat7[date1]" value="<?php if (isset($content_plat7->date1)) {echo $content_plat7->date1;} ?>" id="date1_plat7" type="date" class="form-control pr-0 textarea_style">
            </div>
            <div class="col_pers_gli">
                <input name="plat7[date2]" value="<?php if (isset($content_plat7->date2)) {echo $content_plat7->date2;} ?>" id="date2_plat7" type="date" class="form-control pr-0 textarea_style">
            </div>
            <div class="col_pers_gli">
                <input name="plat7[date3]" value="<?php if (isset($content_plat7->date3)) {echo $content_plat7->date3;} ?>" id="date3_plat7" type="date" class="form-control pr-0 textarea_style">
            </div>
            <div class="col_pers_gli">
                <input name="plat7[date4]" value="<?php if (isset($content_plat7->date4)) {echo $content_plat7->date4;} ?>" id="date4_plat7" type="date" class="form-control pr-0 textarea_style">
            </div>
            <div class="col_pers_gli">
                <input name="plat7[date5]" value="<?php if (isset($content_plat7->date5)) {echo $content_plat7->date5;} ?>" id="date5_plat7" type="date" class="form-control pr-0 textarea_style">
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                <p class="pl-4 text_inside_gli">Nombre de plats à proposer</p>
            </div>
            <div class="col_pers_gli ml-3">
                <input name="plat7[nombre1]" value="<?php if (isset($content_plat7->nombre1)) {echo $content_plat7->nombre1;} ?>" id="nombre1_plat7" class="form-control textarea_style" type="number" >
            </div>
            <div class="col_pers_gli">
                <input name="plat7[nombre2]" value="<?php if (isset($content_plat7->nombre2)) {echo $content_plat7->nombre2;} ?>" id="nombre2_plat7" class="form-control textarea_style" type="number">
            </div>
            <div class="col_pers_gli">
                <input name="plat7[nombre3]" value="<?php if (isset($content_plat7->nombre3)) {echo $content_plat7->nombre3;} ?>" id="nombre3_plat7" class="form-control textarea_style" type="number">
            </div>
            <div class="col_pers_gli">
                <input name="plat7[nombre4]" value="<?php if (isset($content_plat7->nombre4)) {echo $content_plat7->nombre4;} ?>" id="nombre4_plat7" class="form-control textarea_style" type="number">
            </div>
            <div class="col_pers_gli">
                <input name="plat7[nombre5]" value="<?php if (isset($content_plat7->nombre5)) {echo $content_plat7->nombre5;} ?>" id="nombre5_plat7" class="form-control textarea_style" type="number">
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-6">
                <div class="row">
                    <div class="col-6 pl-5">
                        <span class="text-center text_inside_gli">Précisez l'heure de fin de réservation pour le jour même</span>
                    </div>
                    <div class="col-6">
                        <input name="plat7[heure_fin]" value="<?php if (isset($content_plat7->heure_fin)) {echo $content_plat7->heure_fin;} ?>" id="heure_fin_plat7" type="time" class="form-control textarea_style">
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="row">
                    <div class="col-5">
                        <span class="text-center text_inside_gli">Prix unitaire</span>
                    </div>
                    <div class="col-6">
                        <input name="plat7[prix_plat]" value="<?php if (isset($content_plat7->prix_plat)) {echo $content_plat7->prix_plat;} ?>" id="prix_plat7" type="text" class="form-control textarea_style">
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-2"></div>
            <div class="col-4">
                <button type="button"  onclick="save_plat('plat7')" class="btn btn-success w-100">Valider</button>
            </div>
            <div class="col-4">
                <button type="submit" onclick="delete_plat_gli('<?php echo $content_plat7->id ?>')" class="btn btn-danger w-100">Supprimer</button>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#activ_glissiere_activ_plat7").click(function (){
        if($("#activ_glissiere_activ_plat7_value").val() == 0){
            $("#activ_glissiere_activ_plat7_content").removeClass('d-none');
            $("#activ_glissiere_activ_plat7_content").addClass('d-block');
            $("#activ_glissiere_activ_plat7_up").removeClass('d-none');
            $("#activ_glissiere_activ_plat7_up").addClass('d-block');
            $("#activ_glissiere_activ_plat7_down").removeClass('d-block');
            $("#activ_glissiere_activ_plat7_down").addClass('d-none');
            $("#activ_glissiere_activ_plat7_value").val('1');
        }
        else{
            $("#activ_glissiere_activ_plat7_content").removeClass('d-block');
            $("#activ_glissiere_activ_plat7_content").addClass('d-none');
            $("#activ_glissiere_activ_plat7_down").removeClass('d-none');
            $("#activ_glissiere_activ_plat7_down").addClass('d-block');
            $("#activ_glissiere_activ_plat7_up").removeClass('d-block');
            $("#activ_glissiere_activ_plat7_up").addClass('d-none');
            $("#activ_glissiere_activ_plat7_value").val('0');
        }
    })
    $('#is_activ_glissiere_plat7').change(function(){
        if($("#title_gli_plat7_is_activ").val() == 0){
            $("#title_gli_plat7_is_activ").val('1');
        }else{
            $("#title_gli_plat7_is_activ").val('0');
        }
    })
</script>