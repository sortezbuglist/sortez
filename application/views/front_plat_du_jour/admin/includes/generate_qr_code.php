<div class="row glissiere_tab">
    <div class="row w-100">
        <div id="txt_title_gli1" class="txt_title_gli1_ppl text-justify col-12">
            <div class="">GENEREZ LE QRCODE DE VOTRE PAGE "PLATS DU JOUR"</div>
        </div>
    </div>
</div>
<form action="<?php echo site_url('front/Plat_du_jour/generate_qr_code') ?>" method="post">
    <div class="row gli_content pt-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <button type="submit" class="btn btn-success w-100 mt-5">
                        <?php if(isset($menu->url_qr_code_plat) && $menu->url_qr_code_plat != null && $menu->url_qr_code_plat != ""){ ?>
                            Régénérez le QRCODE
                        <?php } else{ ?>
                            Générez le QRCODE
                        <?php } ?>
                    </button>
                </div>
                <div class="col-lg-4">
                    <div class="row">
                        <div class="m-auto">
                            <?php if(isset($menu->url_qr_code_plat) && $menu->url_qr_code_plat != null && $menu->url_qr_code_plat != ""){ ?>
                                <img class="img-fluid pb-5" src="<?php echo base_url('application/resources/front/photoCommercant/imagesbank/'.$infocom->user_ionauth_id.'/plat_gli_qr_code/'.$menu->url_qr_code_plat); ?>">
                            <?php } else{ ?>
                                <img src="<?php echo base_url('assets/images/qr_right.png') ?>" class="img-fluid pb-5">
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <?php if(isset($menu->url_qr_code_plat) && $menu->url_qr_code_plat != null && $menu->url_qr_code_plat != ""){ ?>
                        <a href="<?php echo base_url('application/resources/front/photoCommercant/imagesbank/'.$infocom->user_ionauth_id.'/plat_gli_qr_code/'.$menu->url_qr_code_plat); ?>" class="btn btn-success w-100 mt-5" download>
                            Exportez l'image
                        </a>
                    <?php } else{ ?>
                        <button type="button" class="btn btn-success w-100 mt-5" disabled>Exportez l'image</button>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value="<?php echo site_url('').$infocom->nom_url.'/reservation_commercants' ?>" name="qr_code_img">
    <input type="hidden" value="<?php echo $infocom->IdCommercant; ?>" name="id_plat_com">
</form>

<div class="row glissiere_tab">
    <div class="row w-100">
        <div id="txt_title_gli1" class="txt_title_gli1_ppl text-justify col-12">
            <div class="">INTEGREZ LES PLATS DU JOUR  SUR VOTRE SITE WEB</div>
        </div>
    </div>
</div>
<div class="row gli_content pt-5">
    <div class="container">
        <p>Intégrez le formulaire en ligne sur une page de votre site web en utilisant le lien ci-dessous.

            Contacter votre webmaster :</p>
    </div>
    <div class="container pb-5">
        <input type="text" class="form-control textarea_style w-100" value="<?php echo site_url('').$infocom->nom_url.'/reservation_commercants?content_only=1' ?>" disabled>
    </div>
</div>
<div class="row">
    <div class="col-6 offset-lg-3 text-center pt-2 pb-2 mb-5">
        <a href='<?php echo site_url('').$infocom->nom_url."/reservation_commercants"; ?>' target="_blank">
            <img src='<?php echo site_url('')."application/resources/privicarte/images/visualisation.png"; ?>' alt="logo" title="" style="width: 180px">
        </a>
    </div>
</div>
