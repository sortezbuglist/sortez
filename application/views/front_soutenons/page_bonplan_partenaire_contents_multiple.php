<?php
$utilise = false;
$bonplan_valide = 0;
if (isset($id_client)) {
    $oAssocClientBonPlan = $this->mdlcadeau->getClientAssocBonPlan($id_client, $oBonPlan->bonplan_id);
    $bon_plan_utilise_plusieurs = 0;
    if (isset($oAssocClientBonPlan) && $oAssocClientBonPlan != null) {
        //print_r($oAssocClientBonPlan);
        $utilise = $oAssocClientBonPlan->utilise;
        $bonplan_valide = $oAssocClientBonPlan->valide;
        if ($oBonPlan->bon_plan_utilise_plusieurs == 1) {
            $bon_plan_utilise_plusieurs = 1;
        }
    }
}
?>


<script type="text/javascript">
    <?php
    $user_agent_browser_mda = $_SERVER['HTTP_USER_AGENT'];
    if (strpos(strtoupper($user_agent_browser_mda), 'FIREFOX') !== false) {
    ?>
    $(function () {
        $("#bp_multiple_date_visit_client").datepicker({
            dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            monthNames: ['Janvier', 'Févier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            dateFormat: 'dd/mm/yy',
            autoSize: true,
            changeMonth: true,
            changeYear: true,
            yearRange: '1900:2020'
        });
    });
    <?php
    }
    ?>

    function save_demandeBonPlan(url) {

        var bp_multiple_date_visit_client = String(jQuery("#bp_multiple_date_visit_client").val());
        if (bp_multiple_date_visit_client != "" && bp_multiple_date_visit_client != "undefined") {
        } else {
            bp_multiple_date_visit_client = "0000-00-00";
        }
        var bp_multiple_nbmax_client = jQuery("#bp_multiple_nbmax_client").val();
        if (bp_multiple_nbmax_client == null || bp_multiple_nbmax_client == "" || bp_multiple_nbmax_client == '0') bp_multiple_nbmax_client = '1';
        var bonplan_id = '<?php echo $oBonPlan->bonplan_id; ?>';
        var bonplan_type = '<?php echo $oBonPlan->bonplan_type; ?>';
        //alert(bp_multiple_date_visit_client);
        document.location = url + "/" + bonplan_type + "/" + bp_multiple_nbmax_client + "/" + bp_multiple_date_visit_client;
    }
</script>


<script type="text/javascript">
    function compte_a_rebours_<?php echo $oBonPlan->bonplan_id; ?>() {
        var compte_a_rebours_<?php echo $oBonPlan->bonplan_id; ?> = document.getElementById("compte_a_rebours_<?php echo $oBonPlan->bonplan_id; ?>");

        var date_actuelle = new Date();
        var date_evenement = new Date("<?php echo $oBonPlan->bp_multiple_date_fin; ?>");
        var total_secondes = (date_evenement - date_actuelle) / 1000;

        var prefixe = "";
        if (total_secondes < 0) {
            prefixe = "Exp "; // On modifie le préfixe si la différence est négatif

            total_secondes = Math.abs(total_secondes); // On ne garde que la valeur absolue

        }

        if (total_secondes > 0) {
            var jours = Math.floor(total_secondes / (60 * 60 * 24));
            var heures = Math.floor((total_secondes - (jours * 60 * 60 * 24)) / (60 * 60));
            minutes = Math.floor((total_secondes - ((jours * 60 * 60 * 24 + heures * 60 * 60))) / 60);
            secondes = Math.floor(total_secondes - ((jours * 60 * 60 * 24 + heures * 60 * 60 + minutes * 60)));

            var et = "et";
            var mot_jour = "jours";
            var mot_heure = "heures,";
            var mot_minute = "minutes,";
            var mot_seconde = "secondes";

            if (jours == 0) {
                jours = '';
                mot_jour = '';
            }
            else if (jours == 1) {
                mot_jour = "jour,";
            }

            if (heures == 0) {
                heures = '';
                mot_heure = '';
            }
            else if (heures == 1) {
                mot_heure = "heure,";
            }

            if (minutes == 0) {
                minutes = '';
                mot_minute = '';
            }
            else if (minutes == 1) {
                mot_minute = "minute,";
            }

            if (secondes == 0) {
                secondes = '';
                mot_seconde = '';
                et = '';
            }
            else if (secondes == 1) {
                mot_seconde = "seconde";
            }

            if (minutes == 0 && heures == 0 && jours == 0) {
                et = "";
            }

            //compte_a_rebours.innerHTML = prefixe + jours + ' ' + mot_jour + ' ' + heures + ' ' + mot_heure + ' ' + minutes + ' ' + mot_minute + ' ' + et + ' ' + secondes + ' ' + mot_seconde;
            //compte_a_rebours_<?php //echo $oBonPlan->bonplan_id ; ?>.innerHTML = prefixe + jours + ' ' + mot_jour + ' ' + heures + ':' + minutes + ':' + secondes ;
            compte_a_rebours_<?php echo $oBonPlan->bonplan_id; ?>.innerHTML = prefixe + jours + ' ' + mot_jour + '<p class="margintop10"><span class="noirblanc">' + heures + '</span>:<span class="noirblanc">' + minutes + '</span>:<span class="noirblanc">' + secondes + '</span></p>';
        }
        else {
            compte_a_rebours_<?php echo $oBonPlan->bonplan_id; ?>.innerHTML = 'Expiré !';
            //alert(date_actuelle+ " "+date_evenement);
        }

        var actualisation = setTimeout("compte_a_rebours_<?php echo $oBonPlan->bonplan_id; ?>();", 1000);
    }
</script>
<style type="text/css">
    <!--
    .bordergrey {
        border: 4px solid #E1E1E1;
        margin-bottom: 15px;
    }

    .bggrey {
        background-color: #E1E1E1;
    }

    .bp_red_value {
        background-color: #DA1893;
        margin-right: auto;
        margin-left: auto;
        height: 60px;
        border-radius: 10px;
        color: #FFFFFF;
    }
    @media screen and (min-width: 992px) {
        .bp_red_value {
            width: 450px;
        }
    }
    .bp_multiple_prix {
        font-size: 40px;
    }

    .bp_desc_cond {
        background-color: transparent;
        color: #000000;
        font-family: "Arial", sans-serif;
        font-size: 18px;
        font-style: italic;
        font-variant: normal;
        font-weight: normal;
        margin: 15px 0;
        text-align: center;
        vertical-align: 0;
    }

    .bp_multiple_value, .bp_multiple_eco {
        font-size: 28px;
        text-transform: uppercase;
    }

    .size14 {
        font-size: 14px;
    }

    .img_bg_bp_multiple {
    <?php if (isset($oBonPlan->activ_bp_multiple_qttprop) && $oBonPlan->activ_bp_multiple_qttprop == "1" && isset($oBonPlan->bp_multiple_qttprop) && intval($oBonPlan->bp_multiple_qttprop) >= 0) { ?> background-image: url(<?php echo GetImagePath("privicarte/");?>/img_bp_multiple_bg_qtt.png);
    <?php } else { ?> background-image: url(<?php echo GetImagePath("privicarte/");?>/img_bp_multiple_bg.png);
    <?php } ?> background-repeat: no-repeat;
        background-position: center center;
        background-size: auto;
        height: 275px;
        font-size: 30px;
        font-weight: bold;
        padding-top: 110px !important;
    }

    .text_red {
        color: #DA1893;
        font-weight: bold;
    }

    -->
</style>


<div class="col-lg-12 padding0 bordergrey">

    <div class="col-lg-12" style='background-color: transparent;
    color: #da1893;
    font-family: "Arial",sans-serif;
    font-size: 24px;
    font-style: normal;
    font-variant: normal;
    font-weight: 700;
    line-height: 22.7px;
    margin: 30px 0;
    text-align: center;
    vertical-align: 0;'>
        <?php echo trim($oBonPlan->bonplan_titre); ?>
    </div>

    <?php
    if (
        (!isset($oBonPlan->bp_multiple_prix) || $oBonPlan->bp_multiple_prix == "0" || $oBonPlan->bp_multiple_prix == NULL || $oBonPlan->bp_multiple_prix == "") &&
        (!isset($oBonPlan->bp_multiple_value) || $oBonPlan->bp_multiple_value == "0" || $oBonPlan->bp_multiple_value == NULL || $oBonPlan->bp_multiple_value == "") &&
        (!isset($oBonPlan->bp_multiple_eco) || $oBonPlan->bp_multiple_eco == "0" || $oBonPlan->bp_multiple_eco == NULL || $oBonPlan->bp_multiple_eco == "")
    ) {
    } else {
        ?>
        <div class="col-lg-12 padding0">
            <div class="bp_red_value textaligncenter row">
                <div class="col-sm-4 bp_multiple_prix"><?php if (isset($oBonPlan->bp_multiple_prix) && $oBonPlan->bp_multiple_prix != "0") echo $oBonPlan->bp_multiple_prix; ?></div>
                <div class="col-sm-4 bp_multiple_value">
                    <div class="col-xs-12 padding0"><?php if (isset($oBonPlan->bp_multiple_value) && $oBonPlan->bp_multiple_value != "0") echo $oBonPlan->bp_multiple_value; ?></div>
                    <div class="col-xs-12 padding0 size14">VALEUR</div>
                </div>
                <div class="col-sm-4 bp_multiple_eco">
                    <div class="col-xs-12 padding0"><?php if (isset($oBonPlan->bp_multiple_eco) && $oBonPlan->bp_multiple_eco != "0") echo $oBonPlan->bp_multiple_eco; ?></div>
                    <div class="col-xs-12 padding0 size14">&Eacute;CONOMIE</div>
                </div>
            </div>
        </div>
    <?php } ?>

    <div class="col-lg-12 padding0 bp_desc_cond">
        <strong>Les conditions d'utilisation
            :</strong><br/><?php if (isset($oBonPlan)) echo $oBonPlan->bonplan_texte; ?>
    </div>

    <?php if (isset($oBonPlan->activ_bp_reserver_paypal) && $oBonPlan->activ_bp_reserver_paypal == "1") { ?>
        <div class="col-lg-12 padding0 textaligncenter">
            <div class="row">
                <div class="col-lg-6 padding0">
                    <?php echo html_entity_decode($oBonPlan->bp_code_btn_paypal); ?>
                </div>
                <div class="col-lg-6 padding0">
                    <?php echo html_entity_decode($oBonPlan->bp_code_panier_paypal); ?>
                </div>
            </div>
        </div>
    <?php } ?>

</div>


<div class="col-lg-12 padding0 bggrey paddingtop30 paddingbottom30 textaligncenter">
    <div class="row">
        <div class="col-sm-6 padding0 img_bg_bp_multiple">
            <?php if (isset($oBonPlan->activ_bp_multiple_qttprop) && $oBonPlan->activ_bp_multiple_qttprop == "1" && isset($oBonPlan->bp_multiple_qttprop) && intval($oBonPlan->bp_multiple_qttprop) >= 0) { ?>
                <?php echo $oBonPlan->bp_multiple_qttprop; ?>
            <?php } ?>
        </div>
        <div class="col-sm-6 padding0">
            <div class="col-xs-12 padding0 bonplan_mobile_comptearebour">
                <img id="time_bg_white" src="<?php echo GetImagePath("privicarte/"); ?>/time_bp_compte_desk.png">
                <span id="compte_a_rebours_<?php echo $oBonPlan->bonplan_id; ?>"><noscript>Fin de l'évènement le 1er janvier 2017.</noscript></span>
                <script type="text/javascript">
                    compte_a_rebours_<?php echo $oBonPlan->bonplan_id; ?>();
                </script>
            </div>
            <div class="col-xs-12 padding0 margintop15">&Eacute;ch&eacute;ance
                : <?php if (isset($oBonPlan->bp_multiple_date_fin) AND $oBonPlan->bp_multiple_date_fin !=null AND $oBonPlan->bp_multiple_date_fin !="" ) echo convert_Sqldate_to_Frenchdate($oBonPlan->bp_multiple_date_fin); ?></div>
            <?php if (isset($oBonPlan->activ_bp_multiple_nbmax) && $oBonPlan->activ_bp_multiple_nbmax == "1" && isset($oBonPlan->bp_multiple_nbmax) && intval($oBonPlan->bp_multiple_nbmax) >= 1) { ?>
                <div class="col-xs-12 padding0 margintop15 marginbottom15 textaligncenter">
                    <div class="col-xs-4 padding0"><img
                                src="<?php echo GetImagePath("privicarte/"); ?>/nb_palce_bp.png"/>
                    </div>
                    <div class="col-xs-4 padding0 textalignleft">Pr&eacute;ciser*<br/>le nombre<br/>de place(s)</div>
                    <div class="col-xs-4 padding0 margintop30">
                        <select name="bp_multiple_nbmax_client" id="bp_multiple_nbmax_client">
                            <?php for ($x = 1; $x <= intval($oBonPlan->bp_multiple_nbmax); $x++) { ?>
                                <option value="<?php echo $x; ?>"><?php echo $x; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            <?php } ?>
            <?php if (isset($oBonPlan->activ_bp_multiple_date_visit) && $oBonPlan->activ_bp_multiple_date_visit == "1") { ?>
                <div class="col-xs-12 padding0 margintop15 marginbottom15 textaligncenter">
                    <div class="col-xs-4 padding0"><img
                                src="<?php echo GetImagePath("privicarte/"); ?>/calendar_ico_bp.png"/></div>
                    <div class="col-xs-8 padding0">
                        <div class="col-xs-12 padding0 textalignleft">Pr&eacute;ciser la date souhait&eacute;e*</div>
                        <div class="col-xs-12 paddingleft0 textalignleft">
                            <input type="date" name="bp_multiple_date_visit_client" id="bp_multiple_date_visit_client"
                                   value="" style="width:100%;"/>
                        </div>
                        <!--<div class="col-xs-12 padding0 textalignleft"><small>*Mentions obligatoires</small></div>-->
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="col-lg-12 padding0 textaligncenter margintop30">


            <?php
            $thisss =& get_instance();
            $thisss->load->library('ion_auth');
            $this->load->model("ion_auth_used_by_club");
            ?>

            <div class="col-lg-12 padding0" style="text-align:center;">

                <?php //if  ($oBonPlan->bonplan_condition_vente == '2' && ($oBonPlan->bonplan_quantite != null && $oBonPlan->bonplan_quantite != "0")){ ?>


                <?php
                if ($thisss->ion_auth->logged_in() && $thisss->ion_auth->in_group(2)) {
                    $user_ion_auth = $thisss->ion_auth->user()->row();
                    $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                    ?>
                    Bonjour <?php echo $user_ion_auth->first_name . " " . $user_ion_auth->last_name; ?><br/>
                    <?php
                    if ($mssg != "bonplan_valide") {
                        if (($utilise == 1 && $bon_plan_utilise_plusieurs == 0) || ($bonplan_valide == 1 && $bon_plan_utilise_plusieurs == 0)) {
                            echo "Vous avez d&eacute;ja valid&eacute; ce BonPlan<br/>";
                        } else {
                            echo "Vous pouvez maintenant valider ce BonPlan<br/>";
                        }
                    }
                    ?>
                <?php } else { ?>

                <?php } ?>



                <?php if (isset($id_client) && isset($mssg) && $mssg == "bonplan_valide") { ?>
                    <?php if ($utilise == 1 && $bon_plan_utilise_plusieurs == 0) { ?>
                        <div class="col-xs-12 alert alert-danger" style="color:#F00; text-align:center">Ce bon plan n'a
                            pu
                            &ecirc;tre valid&eacute;, vous l'avez d&eacute;j&agrave; utilis&eacute; !
                        </div>
                    <?php } else if ($mssg != "0") { ?>
                        <div class="col-xs-12 alert alert-success" style="color:#339900; text-align:center">Votre
                            demande de
                            r&eacute;servation est bien enregistr&eacute;e,<br/>vous allez recevoir sous peu un email de
                            confirmation !
                        </div>
                    <?php } ?>
                <?php } ?>



                <?php if (strtotime($oBonPlan->bp_multiple_date_fin) > strtotime('now')) { /*expired*/ ?>


                    <a style="cursor:pointer;" <?php if ($thisss->ion_auth->logged_in() && $thisss->ion_auth->in_group(2)) {
                        echo 'onclick="javascript:save_demandeBonPlan(\'' . site_url("front/fidelisation/demandeBonPlan/" . $oBonPlan->bonplan_id) . '\');"';
                    } else if (!$thisss->ion_auth->logged_in()) {
                        echo 'onclick="javascript:save_demandeBonPlan(\'' . site_url("front/fidelisation/demandeBonPlan/" . $oBonPlan->bonplan_id) . '\');"';
                    } else {
                        echo 'href="javascript:void(0);"';
                    } ?>
                       id="btn_120" class="Button1" style="width: 180px; height: 36px; text-decoration:none;">
                <span>
                <?php if (($utilise == 1 && $bon_plan_utilise_plusieurs == 0) || ($bonplan_valide == 1 && $bon_plan_utilise_plusieurs == 0)) {
                    echo '<strong>Bon Plan d&eacute;j&agrave; valid&eacute;</strong>';
                } else {
                    echo '<img src="' . GetImagePath("privicarte/") . '/btn_reserver_bp_nv.png" id="btn_reserver_multiple"/>';
                } ?>
                </span>
                    </a>


                    <?php if ($thisss->ion_auth->logged_in() && $thisss->ion_auth->in_group(2)) { ?>
                    <?php } else if ($thisss->ion_auth->logged_in()) echo '<br/><font color="#FF0000">Votre compte ne vous permet pas de valider un BonPlan !</font>'; ?>

                <?php } else echo '<div class="col-xs-12 alert alert-danger" style="color:#F00; text-align:center">Bonplan expir&eacute; !</div>'; /*expired*/ ?>


                <?php //} ?>

            </div>


        </div>
    </div>
</div>


<div class="col-xs-12 bggrey margintop15 paddingtop30 paddingbottom30">
    <div class="col-lg-12 padding0 paddingbottom30">
        <div class="row">
            <div class="col-sm-6 padding0 textaligncenter"><img id="time_bg_white"
                                                                src="<?php echo GetImagePath("privicarte/"); ?>/img_alert_txt_bp.png">
            </div>
            <div class="col-sm-6 padding0 textaligncenter">
                <p class="text_red">
                    Avant de pouvoir valider ce bon plan<br/>
                    le consommateur doit poss&eacute;der<br/>
                    la carte privil&egrave;ge Vivresaville &copy;.
                </p>
                <p>
                    Notre &eacute;tablissement est partenaire de la carte<br/>
                    de fid&eacute;lit&eacute; multi-commerce Sortez<span class="text_red">&copy;</span>.<br/>
                    Un concept f&eacute;d&eacute;rateur pour la dynamisation<br/>
                    et la fid&eacute;lisation du commerce et de l’artisanat.
                </p>
            </div>
        </div>
    </div>
    <div class="col-lg-12 padding0 paddingbottom30">
        <ol>
            <li>Vous n'&ecirc;tes pas encore r&eacute;f&eacute;renc&eacute; sur Sortez<span
                        class="text_red">&copy;</span>. inscrivez vous gratuitement !
            </li>
        </ol>
    </div>
    <div class="col-lg-12 padding0 textaligncenter paddingbottom30">
        <div class="row">
            <div class="col-sm-6 pl-0 pt-0 pr-0 pb-3"><a
                        onclick='javascript:window.open("<?php echo site_url("front/fidelity/inscription"); ?>", "Inscription", "width=800, height=800, scrollbars=yes");'
                        href='javascript:void(0);'><img
                            src="<?php echo GetImagePath("privicarte/"); ?>/img_inscription_bp.png"
                            name="img_inscription_bp" id="img_inscription_bp"></a></div>
            <div class="col-sm-6 pl-0 pt-0 pr-0 pb-3 addtopspace"><a
                        onclick='javascript:window.open("<?php echo site_url("front/fidelity/menuconsommateurs"); ?>", "Inscription", "width=800, height=800, scrollbars=yes");'
                        href='javascript:void(0);'><img
                            src="<?php echo GetImagePath("privicarte/"); ?>/img_acces_compte_bp.png"
                            name="img_acces_compte_bp" id="img_acces_compte_bp"></a></div>
        </div>
    </div>
    <div class="col-lg-12 padding0 textalignleft paddingbottom30">
        <ol>
            <li>D&egrave;s validation de votre inscription, une carte de fid&eacute;lit&eacute; d&eacute;mat&eacute;rialis&eacute;e
                apparaît sur votre compte (ordinateur, tablette et mobile).
            </li>
            <li>En validant cette r&eacute;servation, Sortez<span class="text_red">&copy;</span>. vous confirmera imm&eacute;diatement
                sa validation ou son refus si vous l’avez d&eacute;jà utilis&eacute; dans le cas d’une offre unique.
            </li>
            <li>Votre r&eacute;servation sera enregistr&eacute;e sur votre compte Sortez<span
                        class="text_red">&copy;</span>. "R&eacute;servations en cours"
            </li>
            <li>Une confirmation par mail sera adress&eacute;e au professionnel.</li>
            <li> Pour b&eacute;n&eacute;ficier du meilleur accueil, nous conseillons avant tout d&eacute;placement de
                contacter le professionnel.
            </li>
            <li>Lors de votre d&eacute;placement vous pr&eacute;senterez votre carte (mobile ou carte bancaire).</li>
        </ol>
    </div>
    <div class="col-lg-12 padding0 textaligncenter"><a href="<?php echo base_url(); ?>
" target="_blank"><img src="<?php echo GetImagePath("privicarte/"); ?>/img_sortez_org.png" name="img_sortez_org"
                       id="img_sortez_org"></a></div>
</div>
