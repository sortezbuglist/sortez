<?php
$thisss = &get_instance();
$thisss->load->library('ion_auth');
$this->load->model("ion_auth_used_by_club");
$user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);
if (isset($user_ion_auth_id)) $user_groups = $thisss->ion_auth->get_users_groups($user_ion_auth_id)->result();
else $user_groups = 0;
if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id;
else $group_id_commercant_user = 0;
?>
<div class="row d-none d-lg-block">
    <div class="">
        <div id="menu-partner" class="row front_menu padd_menu">
            <!--            <div class="col-lg-2 col-sm-12 pt-1">-->
            <!--                <a href="--><?php //echo site_url($oInfoCommercant->nom_url . "/presentation_commercants"); 
                                            ?>
            <!--">-->
            <!--                    <button type="button" class="btn btn_menu --><?php //if(isset($check_active) && $check_active == "presentation"){ echo "active_menu_commande"; } 
                                                                                    ?>
            <!--">Page Accueil</button>-->
            <!--                </a>-->
            <!--            </div>-->

            <!--            <div class="col-lg-2 col-sm-12 pt-1">-->
            <!--                <a href="--><?php //echo site_url("/front/fidelity/menuconsommateurs");
                                            ?>
            <!--" target="_blank">-->
            <!--                    <button type="button" class="btn btn_menu">Ma Carte</button>-->
            <!--                </a>-->
            <!--            </div>-->

            <div class="col-lg-2 col-sm-12 pt-1" id="btn-lg">
                <button onclick="translatede()" type="button" class="btn btn_menu" id="google_translate_element" style="position:relative; z-index:1;color: #ffffff!important;font-family: Futura-LT-Book, Sans-Serif!important;">Language</button>
            </div>
            <div class="col-lg-2 col-sm-12 pt-1" id="btn-rec">
                <a href="#divContactRecommandationForm" id="IdRecommandationPartnerForm_main_menu" title="Recommandation">
                    <button type="button" class="btn btn_menu">Recommandez</button>
                </a>
            </div>
            <div class="col-lg-2 offset-lg-1 col-sm-12 pt-1" id="btn-nv-rech">
                <a href="javascript:history.back()">
                    <button type="button" class="btn btn_menu active">Nouvelle recherche</button>
                </a>
            </div>
            <div class="col-lg-2 col-sm-12 pt-1" id="btn-fav">
                <a href="<?php echo site_url("/front/utilisateur/ajout_favoris/" . $oInfoCommercant->IdCommercant); ?>">
                    <button type="button" class="btn btn_menu">+ favori</button>
                </a>
            </div>

            <div class="col-lg-2 col-sm-12 pt-1" id="btn-mcompte">
                <a href="<?php echo site_url("/front/utilisateur/menuconsommateurs"); ?>">
                    <button type="button" class="btn btn_menu">Mon Compte</button>
                </a>
            </div>

        </div>

    </div>
</div>


<div id="divContactRecommandationForm" style="display:none; background-color:#FFFFFF; width: 300px;!important; height:200px;">
    <form name="formContactPrivicarteForm" id="formContactPrivicarteForm" action="#">
        <table width="100%" id="tableContactPartnerForm" border="0" cellspacing="0" cellpadding="0" style="text-align:center; width:100%; height:350px;">
            <tr>
                <td>
                    <div style="font-family:arial; font-size:24px; font-weight:bold;">Recommander &agrave;
                        un ami
                    </div>
                </td>
            </tr>
            <tr>
                <td><input class="form-control pt-2" type="text" name="contact_recommandation_nom" id="contact_recommandation_nom" value="" placeholder="Votre nom *" /></td>
            </tr>
            <tr>
                <td><input class="form-control pt-2" type="text" name="contact_recommandation_mail_ami" id="contact_recommandation_mail_ami" placeholder="Courriel de votre ami *" value="" /></td>
            </tr>
            <tr>
                <td><input class="form-control pt-2" type="text" name="contact_recommandation_mail" id="contact_recommandation_mail" value="" placeholder="Votre courriel *" /></td>
            </tr>
            <tr>
                <td><textarea class="form-control pt-2" name="contact_recommandation_msg" id="contact_recommandation_msg" disabled=true>Bonjour, Je vous recommande cette page : "<?php echo site_url($oInfoCommercant->nom_url . '/presentation_commercants'); ?>"</textarea>
                </td>
            </tr>
            <tr>
                <td><span id="spanContactPrivicarteRecommandationForm">* champs obligatoires</span></td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><input type="button" class="btn btn-default" name="contact_recommandation_reset" id="contact_recommandation_reset" value="Retablir" /></td>
                            <td><input type="button" class="btn btn-default" name="contact_recommandation_send" id="contact_recommandation_send" value="Envoyer" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</div>
<script type="text/javascript">
    function translatede() {
        var h1 = document.getElementsByClassName("goog-te-menu-value")[0]; // Get the first <h1> element in the document
        var att = document.createAttribute("id"); // Create a "class" attribute
        att.value = "doda"; // Set the value of the class attribute
        h1.setAttributeNode(att);
        document.getElementById('doda').click();
        document.getElementById('doda').style.display = 'none';
    }

    function googleTranslateElementInit() {
        new google.translate.TranslateElement({
            pageLanguage: 'fr',
            includedLanguages: 'ar,de,en,es,it,ja,nl,no,pl,pt,ru,sv,zh-CN',
            layout: google.translate.TranslateElement.InlineLayout.SIMPLE,
            autoDisplay: false,
            multilanguagePage: true
        }, 'google_translate_element');
    }
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<script type="text/javascript">
    $("#IdRecommandationPartnerForm_main_menu").fancybox({
        'autoScale': false,
        'overlayOpacity': 0.8, // Set opacity to 0.8
        'overlayColor': "#000000", // Set color to Black
        'padding': 5,
        'width': 520,
        'height': 500,
        'transitionIn': 'elastic',
        'transitionOut': 'elastic'
    });

    $("#contact_recommandation_reset").click(function() {
        $("#contact_recommandation_nom").val('');
        $("#contact_recommandation_mail_ami").val('');
        $("#contact_recommandation_mail").val('');
        $("#spanContactPrivicarteRecommandationForm").html('* champs obligatoires');
    });
    $("#contact_recommandation_send").click(function() {
        $("#spanContactPrivicarteRecommandationForm").html('<img src="<?php echo GetImagePath("front/"); ?>/loading.gif" />');
        var error = 0;
        var contact_recommandation_msg = $("#contact_recommandation_msg").val();
        if (contact_recommandation_msg == '' || contact_recommandation_msg == '') error = 1;
        var contact_recommandation_nom = $("#contact_recommandation_nom").val();
        if (contact_recommandation_nom == '' || contact_recommandation_nom == '') error = 1;
        var contact_recommandation_mail_ami = $("#contact_recommandation_mail_ami").val();
        if (contact_recommandation_mail_ami == '' || contact_recommandation_mail_ami == '') error = 1;
        else if (!validateEmail(contact_recommandation_mail_ami)) error = 3;
        var contact_recommandation_mail = $("#contact_recommandation_mail").val();
        if (contact_recommandation_mail == '' || contact_recommandation_mail == '') error = 1;
        else if (!validateEmail(contact_recommandation_mail)) error = 2;


        if (error == 1) {
            alert("error1");
            $("#spanContactPrivicarteRecommandationForm").html('<span style="color:#FF0000;">* Les champs sont obligatoires</span>');
        } else if (error == 2) {
            alert("error2");
            $("#spanContactPrivicarteRecommandationForm").html('<span style="color:#FF0000;">* Adresse email invalide</span>');
            $("#contact_recommandation_mail").css('border-color', '#ff0000');
            //alert("invalide mail");
        } else if (error == 3) {
            alert("error3");
            $("#spanContactPrivicarteRecommandationForm").html('<span style="color:#FF0000;">* Adresse email invalide</span>');
            $("#contact_recommandation_mail_ami").css('border-color', '#ff0000');
        } else {
            $.post(
                "<?php echo site_url("front/professionnels/recommandation_partner_sendmail/"); ?>", {
                    contact_recommandation_nom: contact_recommandation_nom,
                    contact_recommandation_tel: '',
                    contact_recommandation_mail: contact_recommandation_mail,
                    contact_recommandation_msg: contact_recommandation_msg,
                    contact_recommandation_mailto: contact_recommandation_mail_ami
                },
                function(data) {
                    $("#spanContactPrivicarteRecommandationForm").html(data);
                });
        }

        //alert(error);
    });

    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test($email);
    }
</script>
<style>
    .goog-te-menu-frame.skiptranslate {
        top: 150px !important;
    }

    #google_translate_element {
        color: red !important;
        font-size: 15px !important;
        background: transparent;
         !important;
    }

    .skiptranslate.goog-te-gadget {
        position: relative;
        z-index: 0;
    }

    .padd_menu {
        padding-top: 20px;
        padding-bottom: 20px;
        /* width: 52%;
        margin-left: 22%; */
    }
    }

    .rrow {
        margin-right: 0px;
    }

    .btn_menu {
        border: 2px solid #ffffff !important;
        border-radius: 20px !important;
        color: #ffffff !important;
        width: 111%;
        font-size: 15px;
    }

    .btn_menu:hover {
        background: #323232 !important;
    }

    /*#google_translate_element{
        background: <?php //if (isset($oInfoCommercant->bandeau_color) AND $oInfoCommercant->bandeau_color!=null AND $oInfoCommercant->bandeau_color!= '' AND $group_id_commercant_user == 5){echo $oInfoCommercant->bandeau_color.'!important';}elseif($group_id_commercant_user == 4){echo '#3453a2!important';}else{echo '#3453a2!important';} 
                    ?>;
        /* background: #3453a2; */
    /* color: #ffffff; */
    }

    */ #google_translate_element:hover {
        background: #323232 !important;
        /* color: #ffffff; */
    }

    .goog-te-gadget-simple {
        background-color: transparent !important;
        border-left: none !important;
        border-top: none !important;
        ;
        border-bottom: none !important;
        border-right: none !important;
        font-size: 0px !important;
        display: block !important;
        padding-top: 0px !important;
        padding-bottom: 0px !important;
        cursor: pointer;
        zoom: 1;
    }

    #google_translate_element .skiptranslate .goog-te-gadget-simple {
        height: 0px !important;
    }
</style>