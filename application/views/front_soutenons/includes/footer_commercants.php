<?php
$thisss =& get_instance();
$thisss->load->library('ion_auth');
$this->load->model("ion_auth_used_by_club");
$user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);
if (isset($user_ion_auth_id)) $user_groups = $thisss->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;
?>
<div class="row pt-2" style="margin-top: 100px!important;">
    <?php if(isset($group_id_commercant_user) && $group_id_commercant_user == 5 && $check_active == 'presentation'){ ?>
        <?php $this->load->view('front_soutenons/includes/Newsletter_commercants',$data); ?>
    <?php } ?>
    <div class="col-12 background_footer_comms pt-5 pb-5">
        <div class="col-12">
            <p class="text_label2_1"><?php echo $oInfoCommercant->NomSociete;?></p>
            <p class="text_label2_1"><?php echo $oInfoCommercant->adresse_localisation;?></p>
            <p class="text_label2_1"><?php if(isset($oInfoCommercant->TelFixe) && $oInfoCommercant->TelFixe != null) { ?> Tél : <?php echo $oInfoCommercant->TelFixe; } ?> <?php if(isset($oInfoCommercant->TelMobile) && $oInfoCommercant->TelMobile != null){ ?><?php if(isset($oInfoCommercant->TelFixe) && $oInfoCommercant->TelFixe != null) { ?>-<?php }; ?> Mobile : <?php echo $oInfoCommercant->TelMobile; }?></p>
            <p class="text_label2_1"><?php if(isset($oInfoCommercant->Email) && $oInfoCommercant->Email != null) { ?> Courriel : <?php echo $oInfoCommercant->Email;}?> <?php if(isset($oInfoCommercant->SiteWeb) && $oInfoCommercant->SiteWeb != null) { ?> - Site web : <?php echo $oInfoCommercant->SiteWeb;}?></p>
        </div>
        <div class="col-12">
            <p class="text_label2_2 pt-3">Cette page a&nbsp; été créée et hébergée par la SAS Priviconcept</p>

            <p class="text_label2_2">427, Chemin de Vosgelade, Le Mas Raoum,&nbsp; 06140 Vence<br>Siret : 820 043 693 00010 - Code NAF : 6201Z</p>

            <p class="text_label2_2">Le contenu complémentaire (image et rédactionnel) est sous la responsabilité du partenaire</p>
        </div>
    </div>
</div>
<style>
    .background_footer_comms{
        background: <?php if (isset($oInfoCommercant->bandeau_color) AND $oInfoCommercant->bandeau_color!=null AND $oInfoCommercant->bandeau_color!= '' AND $group_id_commercant_user == 5){echo $oInfoCommercant->bandeau_color;}elseif($group_id_commercant_user == 4){echo '#3453a2';}elseif($group_id_commercant_user == 5 && $oInfoCommercant->bandeau_color=null){echo '#4a1b0b';}else{echo '#3453a2';} ?>;
        /*background: #3453a2;*/
    }
    .text_label2_1{
        font-family: Futura-LT-Book, Sans-Serif;
        font-size: 18px;
        align-self: center;
        text-align: center;
        color: #ffffff;
        letter-spacing: 0.05em;
        margin-bottom: 0px!important;
    }
    #footer_section_copyright{
        margin-top: 0px!important;
    }
    .text_label2_2{
        font-family: Futura-LT-Book, Sans-Serif;
        font-size: 14px;
        align-self: center;
        text-align: center;
        color: #ffffff;
        letter-spacing: 0.05em;
        margin-bottom: 0px!important;
    }
</style>