    <div class="row d-lg-flex content_menu_soutenons">
        <div class="col-lg-2" style="border-right: 1px solid #000000">
            <a href="https://www.soutenonslecommercelocal.fr/">
                Page accueil
            </a>
        </div>
        <div class="col-lg-3" style="border-right: 1px solid #000000">
            <a href="<?php echo site_url('/soutenons/Annuaire_Soutenons')?>" class="<?php if ($zTitle =="Annuaire soutenons"){ echo"active" ;} ?>">
                Recherches par catégories et communes
            </a>
        </div>
        <div class="col-lg-2" style="border-right: 1px solid #000000" >
            <a href="<?php echo site_url('/soutenons/Annonce')?>" class="<?php if ($zTitle =="Boutique soutenons"){ echo"active" ;} ?>">
                Recherches par annonces
            </a>
        </div>
        <div class="col-lg-3" style="border-right: 1px solid #000000" >
            <a href="<?php echo site_url('/soutenons/DealAndFidelite')?>" class="<?php if ($zTitle =="Deals et fidelite"){ echo"active" ;} ?>">
                Recherches par Deals & fidélité
            </a>
        </div>
        <div class="col-lg-2" >
            <div class="dropdown1">
                <span>Plus...</span>
                <div class="dropdown-content1">

                    <a href="#" class="titre_type">Je suis un consommateur</a>
                    <a href="<?php echo site_url('/soutenons/Souscription_consommateurs')?>">Les Avantage</a>
                    <a href="<?php echo site_url('/auth/login')?>" target="_blank">Mon compte</a>
                    <a href="<?php echo site_url('/front/fidelity/menuconsommateurs')?>" target="_blank">Ma carte vivresaville.fr</a>
                    <a href="#" class="titre_type">Je suis un professionnel</a>
                    <a href="<?php echo base_url('/front/professionnels/inscription/') ?>" target="_blank">Souscription</a>
                    <a href="<?php echo site_url('/auth/login')?>" target="_blank">Mon compte</a>
                    <a href="#">L'abonnement "Soutien"</a>
                    <a href="#">L'abonnement "Soutien Plus"</a>
                    <a href="#">Présentation commande</a>
                    <a href="#">Pack infos</a>
                    <a href="#">Pack promos</a>
                    <a href="#">Pack boutique</a>
                    <a href="#">Spécial restauration</a>
                    <a href="#">Spécial Gîtes</a>
                    <a href="#"  class="titre_type">Entête menu</a>
                </div>
            </div>
        </div>
    </div>
