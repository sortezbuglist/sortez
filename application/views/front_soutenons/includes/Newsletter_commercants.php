<div class="content_newsletter" id="id_newsletter">
    <div class="container content_in_container">
        <div class="row">
            <div class="col-lg-6">
                <div class="content_bloc">
                    <div class="content text-center">
                        <img src="<?php echo base_url('assets/soutenons/calculatrice.png') ?>">
                        <div class="text_form">
                            <?php echo $oInfoCommercant->Horaires;?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="content_bloc">
                    <p class="titre_form">Abonnez-vous à notre newsletter</p>
                    <div id="content_message">

                    </div>
                    <div class="input-group mb-3">
                        <input class="form-control" placeholder="Nom *" type="text" id="name_input">
                    </div>
                    <div class="input-group mb-3">
                        <input class="form-control" placeholder="E-mail *" type="email" id="mail_input">
                    </div>
                    <div class="input-group mb-3">
                        <input class="checkbox_content" type="checkbox" aria-label="Checkbox for following text input" id="checkbox_input">
                        <span class="text_accept">J'acceptes les termes et conditions</span>
                    </div>
                    <div id="content_load" style="display: none">
                        <img src="<?php echo base_url('assets/soutenons/ajax-loader.gif'); ?>" class="img-fluid">
                    </div>
                    <button type="submit" class="btn btn_send w-100" id="button_input" disabled>Envoyer</button>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" value="0" id="value_checkbox">
<input type="hidden" value="<?php echo $oInfoCommercant->IdCommercant ?>" id="id_com_set">
<script type="text/javascript">
    $('#checkbox_input').change(function(){
        if($('#value_checkbox').val() == 0){
            $('#value_checkbox').val('1');
            $('#button_input').prop('disabled', false);
        }else{
            $('#value_checkbox').val('0');
            $('#button_input').prop('disabled', true);
        }
    })
    $('#button_input').click(function(){
        $('#content_load').css('display','block');
        var mail = $('#mail_input').val();
        var name = $('#name_input').val();
        var idcom = $('#id_com_set').val();
        if(name == '' || mail == ''){
            $('#content_message').html('<p class="alert alert-danger">Tous les champs sont obligatoire</p>');
        }else{
            if(isEmail(mail) != false){
                var datas = 'nom='+name+'&mail='+mail+'&idcom='+idcom;
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('front_soutenons/Commercant/add_new_client_newsletter'); ?>",
                    data: datas ,
                    success: function(data_saved) {
                        if(data_saved == 1){
                            $('#checkbox_input').click();
                            $('#name_input').val('');
                            $('#mail_input').val('');
                            $('#content_load').css('display','none');
                            $('#content_message').html('<p class="alert alert-success">Vous venez de vous enregistrer</p>');
                        }else{
                            $('#content_load').css('display','none');
                            $('#content_message').html('<p class="alert alert-danger">Une erreur c\'est produite </p>');
                        }
                    },
                    error: function() {
                        $('#content_load').css('display','none');
                        // alert('Erreur');
                    }
                });
            }else{
                $('#content_load').css('display','none');
                $('#content_message').html('<p class="alert alert-danger">mail invalide</p>');
            }
        }
    })
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        console.log(regex.test(email));
        return regex.test(email);
    }
</script>
<style>
    #id_newsletter {
        width: 100%;
        padding-top: 4%;
        padding-bottom: 4%;
        /*background-image: url(https://www.randawilly.ovh/application/resources/front/photoCommercant/imagesbank/1992/bg/94f1335….jpg) !important;*/
        background-repeat: no-repeat !important;
        /*background-attachment: fixed !important;*/
        background-size: 100% 100% !important;
    }
    .btn_send{
        color: #fff;
        font-family: Futura-LT-Book, Sans-Serif;
        background-color : <?php if (isset($oInfoCommercant->bandeau_color) AND $oInfoCommercant->bandeau_color!=null AND $oInfoCommercant->bandeau_color!= '' AND $group_id_commercant_user == 5){echo $oInfoCommercant->bandeau_color;}elseif($group_id_commercant_user == 4){echo '#3453a2';}else{echo '#3453a2';} ?>;
        border-color : <?php if (isset($oInfoCommercant->bandeau_color) AND $oInfoCommercant->bandeau_color!=null AND $oInfoCommercant->bandeau_color!= '' AND $group_id_commercant_user == 5){echo $oInfoCommercant->bandeau_color;}elseif($group_id_commercant_user == 4){echo '#3453a2';}else{echo '#3453a2';} ?>;
    }
    .content_bloc{
        padding: 15px;
        padding-bottom: 10%!important;
        height: 100%;
        background-color: rgba(255,255,255,0.7);
    }
    .titre_form{
        text-align: center;
        color:#373B4D;
        font-size: 22px;
        font-family: Futura-LT-Book, Sans-Serif;
    }
    .text_form{
        font-size:14px;
        color: #000000;
        font-family: Futura-LT-Book, Sans-Serif;
        letter-spacing:0.05em;
        font-weight:400;
        font-style: normal;
    }
    .text_accept{
        padding-top: 3px;
        font: italic normal normal 15px/1.4em futura-lt-w01-book,sans-serif;
        color: #373B4D;
        line-height: 1;
    }
    .checkbox_content{
        width: 20px;
        height: 20px;
    }
</style>