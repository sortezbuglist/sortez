<?php
$thiss = &get_instance();
$thiss->load->library('session');
$thiss->load->model('vsv_ville_other');
$localdata_IdVille = $thiss->session->userdata('localdata_IdVille');
$localdata_IdVille_parent = $thiss->session->userdata('localdata_IdVille_parent');
$localdata_IdVille_all = $thiss->session->userdata('localdata_IdVille_all');
$main_width_device = $thiss->session->userdata('main_width_device');
$main_mobile_screen = false;
$localdata_IdDepartement = $thiss->session->userdata('iDepartementId_x');
$localdata_IdCategory = $thiss->session->userdata('iCategorieId_x');
$localdata_iSubCategorieId = $thiss->session->userdata('iSubCategorieId_x');
$localdata_zMotCle = $thiss->session->userdata('zMotCle_x');
if (!isset($zTitle)) $zTitle = "Annuaire soutenons";
if (isset($main_width_device) && $main_width_device != "" && $main_width_device != "0" && is_numeric($main_width_device) && floatval($main_width_device) <= 991) {
    $main_mobile_screen = true;
} else {
    $main_mobile_screen = false;
}
$thiss->load->model("vivresaville_villes");
$logo_to_show_vsv = base_url() . "assets/ville-test/wpimages/logo_ville_test.png";
if (isset($localdata_IdVille_parent) && $localdata_IdVille_parent != "" && $localdata_IdVille_parent != false) {
    $vsv_id_vivresaville = $thiss->vivresaville_villes->getByIdVille($localdata_IdVille_parent);
    if (isset($vsv_id_vivresaville) && isset($vsv_id_vivresaville->id)) {
        $vsv_object = $thiss->vivresaville_villes->getById($vsv_id_vivresaville->id);
        $vsv_other_ville = $thiss->vsv_ville_other->getByIdVsv($vsv_id_vivresaville->id);
    }
    if (isset($vsv_object) && isset($vsv_object->logo) && file_exists("application/resources/front/images/vivresaville/" . $vsv_object->logo)) {
        $logo_to_show_vsv = base_url() . "application/resources/front/images/vivresaville/" . $vsv_object->logo;
    }
}
?>

<div class="container mt-5 mb-5 pb-5">
    <div class="row" style="background-color: white!important;padding-top: 10px;padding-bottom: 25px">
        <div class="col-lg-12">
            <p class="gros_titre">
                <?php
                if ($zTitle == "Annuaire soutenons") {
                    echo "Recherches par communes & catégories";
                } else if ($zTitle == "Boutique soutenons") {
                    echo "Recherches par annonces";
                } else {
                    echo "Recherches par Deals & fidélité";
                }
                ?>
            </p>
        </div>
        <div class="col-lg-12 text-center">
            <?php if ($main_mobile_screen == false) { ?>
                <div class="container">
                    <div class="row">
                        <div class="container content_soutenons_1">
                            <div class="row">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-4">
                                    <div class="dropdown">
                                        <div class="btn btn-secondary dropdown-toggle border_content_soutenons_dep" type="button" id="Recherches_par_departement" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <?php
                                            if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != 0 && $localdata_IdDepartement != "0" && intval($localdata_IdDepartement) != 9999) {
                                                if (isset($toDepartement) && $toDepartement != "" && $toDepartement != 0 && $toDepartement != "0") {
                                                    foreach ($toDepartement as $oDepartement) {
                                                        if ($oDepartement->departement_id == $localdata_IdDepartement)
                                                            echo $oDepartement->departement_nom . " (" . $oDepartement->nbCommercant . ")";
                                                    }
                                                }
                                            } else echo 'Recherches par département';
                                            ?>
                                        </div>
                                        <div style="font-size:12px;" class="dropdown-menu main_communes_select" id="div_Recherches_par_departement" aria-labelledby="dropdownMenuButton_com">
                                            <?php
                                            if ($zTitle == "Annuaire soutenons" || $zTitle == "Boutique soutenons") { ?>
                                                <?php
                                                if (isset($toDepartement) && $toDepartement != "" && $toDepartement != 0 && $toDepartement != "0") { ?>
                                                    <?php foreach ($toDepartement as $oDepartement) { ?>
                                                        <a class="dropdown-item" href="javascript:void(0);" onclick="javascript:pvc_select_departement(<?php echo $oDepartement->departement_id; ?>);"><?php echo $oDepartement->departement_nom . " (" . $oDepartement->nbCommercant . ")"; ?></a>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <?php foreach ($toDepartement as $toDepartements) { ?>
                                                    <a class="dropdown-item" href="javascript:void(0);" onclick="javascript:pvc_select_departement(<?php echo $toDepartements['departement_id']; ?>);"><?php echo $toDepartements['departement_nom'] . " (" . $toDepartements['nbCommercant'] . ")"; ?></a>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4"></div>
                            </div>
                            <script type="text/javascript">
                                function pvc_select_departement(departement_id) {
                                    $("#inputStringDepartementHidden_partenaires").val(departement_id);
                                    $("#inputStringVilleHidden_partenaires").val(0);
                                }
                            </script>

                            <div class="row">
                                <div class="col-sm-12 col-lg-4 pt-2">
                                    <?php $data['empty'] = null;
                                    $this->load->view("sortez_soutenons/includes/main_communes_select", $data); ?>
                                </div>
                                <div class="col-sm-12 col-lg-4 pt-2">
                                    <div class="dropdown">
                                        <div class="btn btn-secondary dropdown-toggle border_content_soutenons" type="button" id="dropdownMenuButton_com_categ" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Recherche
                                            par
                                            catégories
                                        </div>
                                        <div style="font-size:12px;" class="dropdown-menu main_communes_select" id="<?php if ($zTitle == "Annuaire soutenons") {
                                                                                                                        echo "span_leftcontener2013_form_partenaires";
                                                                                                                    } else if ($zTitle == "Boutique soutenons") {
                                                                                                                        echo "span_leftcontener2013_form_annonces";
                                                                                                                    } else {
                                                                                                                        echo "span_leftcontener2013_form_deal";
                                                                                                                    } ?>" aria-labelledby="dropdownMenuButton_com">
                                            <?php if ($toallcateg != '' && $toallcateg != null) { ?>
                                                <?php foreach ($toallcateg as $toctages) { ?>
                                                    <a class="dropdown-item" href="javascript:void(0);" onclick="javascript:pvc_select_departement(<?php echo $toctages->IdRubrique; ?>);"><?php echo $toctages->rubrique . " (" . $toctages->nbCommercant . ")"; ?></a>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                                <script type="text/javascript">
                                    function show_current_subcategcontent_soutenons(id_sousrubrique) {
                                        <?php if (isset($iDepartementId_x_soutenons) && $iDepartementId_x_soutenons != null) { ?>
                                            $("#inputStringDepartementHidden_partenaires").val("<?php echo $iDepartementId_x_soutenons; ?>");
                                        <?php } ?>
                                        <?php if (isset($localdata_IdCategory) && $localdata_IdCategory != null) { ?>
                                            $("#inputStringHidden").val("<?php echo $localdata_IdCategory; ?>");
                                        <?php } ?>
                                        $("#inputStringHiddenSubCateg").val(id_sousrubrique);
                                        $("#div_subcateg_annuaire_contents").hide();
                                    }
                                </script>

                                <div class="col-sm-12 col-lg-4 pt-2">
                                    <?php
                                    if ($zTitle == "Annuaire soutenons" || $zTitle == "Boutique soutenons") { ?>
                                        <div class="dropdown">
                                            <div class="btn btn-secondary dropdown-toggle border_content_soutenons" type="button" id="dropdownMenuButton_com_sucateg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <?php
                                                if (isset($localdata_iSubCategorieId) && $localdata_iSubCategorieId != "" && $localdata_iSubCategorieId != 0 && $localdata_iSubCategorieId != "0") {
                                                    if (isset($toSubCategorie) && $toSubCategorie != "" && $toSubCategorie != 0 && $toSubCategorie != "0") {
                                                        foreach ($toSubCategorie as $oSubCategorie) {
                                                            if ($oSubCategorie->IdSousRubrique == $localdata_iSubCategorieId)
                                                                echo $oSubCategorie->Nom;
                                                        }
                                                    }
                                                } else echo 'Recherche par sous catégories';

                                                if ($zTitle == "Boutique soutenons") {
                                                    echo 'Recherche par sous catégories';
                                                }
                                                ?>
                                            </div>
                                            <div style="font-size:12px;" class="dropdown-menu main_communes_select" id="div_subcateg_annuaire_contents" aria-labelledby="dropdownMenuButton_com">

                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="dropdown">
                                            <div class="btn btn-secondary dropdown-toggle border_content_soutenons" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Deals
                                                ou
                                                fidélité
                                            </div>
                                            <div style="font-size:12px;" class="dropdown-menu main_communes_select" aria-labelledby="dropdownMenuButton_com">
                                                <a class="dropdown-item" href="#">Toutes
                                                    les
                                                    offres</a>
                                                <a class="dropdown-item" href="#">Les
                                                    deals</a>
                                                <a class="dropdown-item" href="#">La
                                                    fidélité</a>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <?php
                                if ($zTitle == "Annuaire soutenons") { ?>
                                    <div class="col-sm-12 col-lg-4 pt-2">
                                        <input class="btn btn-secondary dropdown-toggle border_content_soutenons" id="zMotcle_value" type="text" placeholder="Recherches par mot clé" value="<?php if (isset($localdata_zMotCle) && $localdata_zMotCle != '') echo $localdata_zMotCle; ?>">
                                    </div>
                                <?php } else if ($zTitle == "Boutique soutenons") { ?>
                                    <div class="col-sm-12 col-lg-4 pt-2">
                                        <div class="dropdown">
                                            <div class="btn btn-secondary dropdown-toggle border_content_soutenons" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Filtrer
                                            </div>
                                            <div style="font-size:12px;" class="dropdown-menu main_communes_select" aria-labelledby="dropdownMenuButton_com">
                                                <a class="dropdown-item" href="javascript:void(0);" onclick="javascript:pvc_select_etat_annonce(1)">Neuf</a>
                                                <a class="dropdown-item" href="javascript:void(0);" onclick="javascript:pvc_select_etat_annonce(0)">Revente</a>
                                                <a class="dropdown-item" href="javascript:void(0);" onclick="javascript:pvc_select_etat_annonce(2)">Service</a>
                                            </div>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        function pvc_select_etat_annonce(getetat) {
                                            $('#iEtat').val(getetat);
                                        }
                                    </script>
                                <?php } else { ?>
                                    <div class="col-sm-12 col-lg-4 pt-2">
                                        <input class="btn btn-secondary dropdown-toggle border_content_soutenons" id="zMotcle_value" type="text" placeholder="Recherches par mot clé" value="<?php if (isset($localdata_zMotCle) && $localdata_zMotCle != '') echo $localdata_zMotCle; ?>">
                                    </div>
                                <?php } ?>
                                <div class="col-sm-12 col-lg-4 pt-2">
                                    <div class="btn btn-secondary reinit_soutenons" onclick="<?php if ($zTitle == "Annuaire soutenons") {
                                                                                                    echo "btn_re_init_annuaire_list();";
                                                                                                } else if ($zTitle == "Boutique soutenons") {
                                                                                                    echo "btn_re_init_annonce_list();";
                                                                                                } else {
                                                                                                    echo "btn_re_init_annonce_list();";
                                                                                                } ?>">
                                        Réinitialisez
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-4 pt-2">
                                    <div class="btn btn-secondary applic_soutenons" onclick="javascript:keyword_search_annuaire_soutenons();">
                                        Appliquez
                                        votre
                                        choix
                                    </div>
                                    <script type="text/javascript">
                                        function submit_search_annonce(idsubcateg) {
                                            $('#inputStringHidden').val(idsubcateg);
                                            $('#div_subcateg_annuaire_contents').hide();
                                        }

                                        function keyword_search_annuaire_soutenons(keyword_value) {
                                            <?php if (isset($iDepartementId_x_soutenons) && $iDepartementId_x_soutenons != null) { ?>
                                                $("#inputStringDepartementHidden_partenaires").val("<?php echo $iDepartementId_x_soutenons; ?>");
                                            <?php } ?>
                                            <?php if (isset($localdata_IdCategory) && $localdata_IdCategory != null) { ?>
                                                $("#inputStringHidden").val("<?php echo $localdata_IdCategory; ?>");
                                            <?php } ?>
                                            $("#frmRecherchePartenaire #zMotCle").val($("#zMotcle_value").val());
                                            $("#frmRecherchePartenaire").submit();
                                        }
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>