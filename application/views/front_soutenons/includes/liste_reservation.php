<?php
$thisss = &get_instance();
$thisss->load->library('ion_auth');
$thisss->load->library('generate_thumb');
$this->load->model("ion_auth_used_by_club");
$user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);
if (isset($user_ion_auth_id)) $user_groups = $thisss->ion_auth->get_users_groups($user_ion_auth_id)->result();
else $user_groups = 0;
if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id;
else $group_id_commercant_user = 0;
$image_path = "application/resources/front/photoCommercant/imagesbank/" . $oInfoCommercant->user_ionauth_id . "/";
?>
<style>
    @media screen and (max-width:768px) {
        .col-sm-12 {
            max-width: 100% !important;
        }
    }
</style>
<div>
    <p style="text-align: center; color: #E80EAE; font-size: 20px; line-height: 0.7em; ">Attention ! la validation de votre réservation s'effectue jour après jour</p>
</div>
<div class="row m-auto" id="content_plat_list">
    <?php foreach ($reservation as $res_list) {
        if (isset($res_list->titre_gli) && $res_list->titre_gli != null && $res_list->titre_gli != "" && $res_list->IdCommercant == $oInfoCommercant->IdCommercant) {
    ?>

            <div class="col-card-sortez col-sm-12 col-md-4 mt-4 mb-3" style="background: #ffffff;">
                <form id="res_client<?php //echo $res_list->id; ?>" method="post" action="<?php echo site_url('front/commercant/valid_reservation_client') ?>">
                    <div style="box-shadow: 0px 2px 2px 2px rgba(0,0,0,0.1)">
                        <div class="p-0">
                            <div class="p-0" style="overflow: hidden;width: 100%;height: 130px;">
                                <a href="#">
                                    <?php if (!empty($res_list->image)) { ?>

                                        <?php
                                        if (isset($image_home_vignette) && $image_home_vignette != null && is_file('application/resources/front/photoCommercant/imagesbank/' . $oInfoCommercant->user_ionauth_id . '/thumbs/thumbnail_' . $res_list->image) == true) {
                                            echo '<img src="' . base_url() . 'application/resources/front/photoCommercant/imagesbank/' . $oInfoCommercant->user_ionauth_id . '/thumbs/thumbnail_' . $res_list->image . '" width="100%"/>';
                                        } else {
                                            $updir = 'application/resources/front/photoCommercant/imagesbank/' . $oInfoCommercant->user_ionauth_id . '/';
                                            $img = $thisss->generate_thumb->makeThumbnails($updir, $res_list->image, $updir);
                                            echo '<img src="' . base_url() . $img . '" width="100%"/>';
                                        }
                                        ?>

                                    <?php } else { ?>
                                        <img src="<?php echo base_url('assets/images/default_plat.png') ?>" width="100%" height="auto" />'
                                    <?php } ?>
                                </a>
                            </div>
                        </div>
                        <div class="p-3">
                            <div class="pt-2 text-center">
                                <p class="title_plat" id="title_plat<? echo $res_list->id_plat;?>"><?php echo $res_list->titre_gli; ?></p>
                                <p class="designation_plat"><?php echo $res_list->designation; ?></p>
                                <p class="prix_plat" id="prix_plat<? echo $res_list->id_plat;?>"><?php echo $res_list->prix_plat; ?> ‎€</p>
                            </div>
                            <div class="pl-2">
                                <div class="row">
                                    <div class="col-lg-7 col-sm-7 col-7"></div>
                                    <div class="col-lg-5 col-sm-5 col-5 text_head1">Nbre dispo</div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-7 col-sm-7 col-7"><span><input type="checkbox" id="date1_<?php echo $res_list->id_plat; ?>"></span> Le
                                        <span class="date_plat">
                                            <?php
                                            $date1 = explode('-', $res_list->date1);
                                            echo $date1[2] . '.' . $date1[1] . '.' . $date1[0];
                                            ?>
                                        </span>
                                    </div>
                                    <div class="col-lg-5 col-sm-5 col-5">
                                        <div class="nbr_plat"><?php echo $res_list->nombre1; ?></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-7 col-sm-7 col-7"><span><input type="checkbox" id="date2_<?php echo $res_list->id_plat; ?>"></span> Le
                                        <span class="date_plat">
                                            <?php
                                            $date2 = explode('-', $res_list->date2);
                                            echo $date2[2] . '.' . $date2[1] . '.' . $date2[0];
                                            ?>
                                        </span>
                                    </div>
                                    <div class="col-lg-5 col-sm-5 col-5">
                                        <div class="nbr_plat"><?php echo $res_list->nombre2; ?></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-7 col-sm-7 col-7"><span><input type="checkbox" id="date3_<?php echo $res_list->id_plat; ?>"></span> Le
                                        <span class="date_plat">
                                            <?php
                                            $date3 = explode('-', $res_list->date3);
                                            echo $date3[2] . '.' . $date3[1] . '.' . $date3[0];
                                            ?>
                                        </span>
                                    </div>
                                    <div class="col-lg-5 col-sm-5 col-5">
                                        <div class="nbr_plat"><?php echo $res_list->nombre3; ?></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-7 col-sm-7 col-7"><span><input type="checkbox" id="date4_<?php echo $res_list->id_plat; ?>"></span> Le
                                        <span class="date_plat">
                                            <?php
                                            $date4 = explode('-', $res_list->date4);
                                            echo $date4[2] . '.' . $date4[1] . '.' . $date4[0];
                                            ?>
                                        </span>
                                    </div>
                                    <div class="col-lg-5 col-sm-5 col-5">
                                        <div class="nbr_plat"><?php echo $res_list->nombre4; ?></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-7 col-sm-7 col-7"><span><input type="checkbox" id="date5_<?php echo $res_list->id_plat; ?>"></span> Le
                                        <span class="date_plat">
                                            <?php
                                            $date5 = explode('-', $res_list->date5);
                                            echo $date5[2] . '.' . $date5[1] . '.' . $date5[0];
                                            ?>
                                        </span>
                                    </div>
                                    <div class="col-lg-5 col-sm-5 col-5">
                                        <div class="nbr_plat"><?php echo $res_list->nombre5; ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <p class="text-center text_alert_plat">
                                    ATTENTION !
                                </p>
                                <p class="text-center text_alert_plat">
                                    Vous devez valider la réservation
                                </p>
                                <p class="text-center text_alert_plat">
                                    pour le jour même avant <?php echo str_replace(':', 'h', $res_list->heure_fin); ?>.
                                </p>
                            </div>
                           <!--  <div class="row pt-3 pb-3">
                                <div class="col-lg-6 col-sm-6 col-6 text_label_plat">Nbre de personnes</div>
                                <div class="col-lg-6 col-sm-6 col-6">
                                    <input type="number" class="form-control textarea_style" id="nbre_pers<?php echo $res_list->id_plat;
                                                                                                            ?>">
                                </div>
                            </div> -->
                            <div class="row pb-3">
                                <div class="col-lg-6 col-sm-6 col-6 text_label_plat">Nbre de plats du jour</div>
                                <div class="col-lg-6 col-sm-6 col-6">
                                    <input type="number" class="form-control textarea_style" id="plat_jour<?php echo $res_list->id_plat; ?>">
                                </div>
                            </div>
                            <!-- <div class="row pb-3">
                                <div class="col-lg-6 col-sm-6 col-6 text_label_plat">Heure réservation</div>
                                <div class="col-lg-6 col-sm-6 col-6">
                                    <input type="time" class="form-control textarea_style" id="heure_res<?php echo $res_list->id_plat; ?>">
                                </div>
                            </div>
                            <div class="row pb-4 pt-2">
                                <div class="col-12 text-center  col-12" style="">
                                    <div style="color: white" id="btnres<?php echo $res_list->id_plat
                                                                        ?>" class="boutoninsc">JE R&Eacute;SERVE</div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </form>
            </div>
            <input type="hidden" id="date1_<?php echo $res_list->id_plat; ?>_value" value="0">
            <input type="hidden" id="date2_<?php echo $res_list->id_plat; ?>_value" value="0">
            <input type="hidden" id="date3_<?php echo $res_list->id_plat; ?>_value" value="0">
            <input type="hidden" id="date4_<?php echo $res_list->id_plat; ?>_value" value="0">
            <input type="hidden" id="date5_<?php echo $res_list->id_plat; ?>_value" value="0">
            <input type="hidden" id="date_selected<?php echo $res_list->id_plat; ?>" value="0">
            <input type="hidden" id="champ_selected<?php echo $res_list->id_plat; ?>" value="0">
            <script type="text/javascript">
                $('#date1_<?php echo $res_list->id_plat; ?>').click(function() {
                    if ($('#date1_<?php echo $res_list->id_plat; ?>_value').val() != 0) {
                        $('#date1_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date1_<?php echo $res_list->id_plat; ?>_value').val('0');
                        $('#date_selected<?php echo $res_list->id_plat; ?>').val('0');
                        $('#champ_selected<?php echo $res_list->id_plat; ?>').val('0');
                    } else {
                        $('#date1_<?php echo $res_list->id_plat; ?>').prop("checked", true);
                        $('#date_selected<?php echo $res_list->id_plat; ?>').val('<?php echo $res_list->date1; ?>');
                        $('#champ_selected<?php echo $res_list->id_plat; ?>').val('nombre1-<?php echo $res_list->nombre1; ?>');
                        $('#date1_<?php echo $res_list->id_plat; ?>_value').val('1');
                        $('#date2_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date2_<?php echo $res_list->id_plat; ?>_value').val('0');
                        $('#date3_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date3_<?php echo $res_list->id_plat; ?>_value').val('0');
                        $('#date4_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date4_<?php echo $res_list->id_plat; ?>_value').val('0');
                        $('#date5_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date5_<?php echo $res_list->id_plat; ?>_value').val('0');
                    }
                })
                $('#date2_<?php echo $res_list->id_plat; ?>').click(function() {
                    if ($('#date2_<?php echo $res_list->id_plat; ?>_value').val() != 0) {
                        $('#date2_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date2_<?php echo $res_list->id_plat; ?>_value').val('0');
                        $('#date_selected<?php echo $res_list->id_plat; ?>').val('0');
                        $('#champ_selected<?php echo $res_list->id_plat; ?>').val('0');
                    } else {
                        $('#date2_<?php echo $res_list->id_plat; ?>').prop("checked", true);
                        $('#date_selected<?php echo $res_list->id_plat; ?>').val('<?php echo $res_list->date2; ?>');
                        $('#champ_selected<?php echo $res_list->id_plat; ?>').val('nombre2-<?php echo $res_list->nombre2; ?>');
                        $('#date2_<?php echo $res_list->id_plat; ?>_value').val('1');
                        $('#date1_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date1_<?php echo $res_list->id_plat; ?>_value').val('0');
                        $('#date3_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date3_<?php echo $res_list->id_plat; ?>_value').val('0');
                        $('#date4_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date4_<?php echo $res_list->id_plat; ?>_value').val('0');
                        $('#date5_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date5_<?php echo $res_list->id_plat; ?>_value').val('0');
                    }
                })
                $('#date3_<?php echo $res_list->id_plat; ?>').click(function() {
                    if ($('#date3_<?php echo $res_list->id_plat; ?>_value').val() != 0) {
                        $('#date3_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date3_<?php echo $res_list->id_plat; ?>_value').val('0');
                        $('#date_selected<?php echo $res_list->id_plat; ?>').val('0');
                        $('#champ_selected<?php echo $res_list->id_plat; ?>').val('0');
                    } else {
                        $('#date3_<?php echo $res_list->id_plat; ?>').prop("checked", true);
                        $('#date_selected<?php echo $res_list->id_plat; ?>').val('<?php echo $res_list->date3; ?>');
                        $('#champ_selected<?php echo $res_list->id_plat; ?>').val('nombre3-<?php echo $res_list->nombre3; ?>');
                        $('#date3_<?php echo $res_list->id_plat; ?>_value').val('1');
                        $('#date1_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date1_<?php echo $res_list->id_plat; ?>_value').val('0');
                        $('#date2_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date2_<?php echo $res_list->id_plat; ?>_value').val('0');
                        $('#date4_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date4_<?php echo $res_list->id_plat; ?>_value').val('0');
                        $('#date5_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date5_<?php echo $res_list->id_plat; ?>_value').val('0');
                    }
                })
                $('#date4_<?php echo $res_list->id_plat; ?>').click(function() {
                    if ($('#date4_<?php echo $res_list->id_plat; ?>_value').val() != 0) {
                        $('#date4_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date4_<?php echo $res_list->id_plat; ?>_value').val('0');
                        $('#date_selected<?php echo $res_list->id_plat; ?>').val('0');
                        $('#champ_selected<?php echo $res_list->id_plat; ?>').val('0');
                    } else {
                        $('#date4_<?php echo $res_list->id_plat; ?>').prop("checked", true);
                        $('#date_selected<?php echo $res_list->id_plat; ?>').val('<?php echo $res_list->date4; ?>');
                        $('#champ_selected<?php echo $res_list->id_plat; ?>').val('nombre4-<?php echo $res_list->nombre4; ?>');
                        $('#date4_<?php echo $res_list->id_plat; ?>_value').val('1');
                        $('#date1_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date1_<?php echo $res_list->id_plat; ?>_value').val('0');
                        $('#date2_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date2_<?php echo $res_list->id_plat; ?>_value').val('0');
                        $('#date3_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date3_<?php echo $res_list->id_plat; ?>_value').val('0');
                        $('#date5_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date5_<?php echo $res_list->id_plat; ?>_value').val('0');
                    }
                })
                $('#date5_<?php echo $res_list->id_plat; ?>').click(function() {
                    if ($('#date5_<?php echo $res_list->id_plat; ?>_value').val() != 0) {
                        $('#date5_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date5_<?php echo $res_list->id_plat; ?>_value').val('0');
                        $('#date_selected<?php echo $res_list->id_plat; ?>').val('0');
                        $('#champ_selected<?php echo $res_list->id_plat; ?>').val('0');
                    } else {
                        $('#date5_<?php echo $res_list->id_plat; ?>').prop("checked", true);
                        $('#date_selected<?php echo $res_list->id_plat; ?>').val('<?php echo $res_list->date5; ?>');
                        $('#champ_selected<?php echo $res_list->id_plat; ?>').val('nombre5-<?php echo $res_list->nombre5; ?>');
                        $('#date5_<?php echo $res_list->id_plat; ?>_value').val('1');
                        $('#date1_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date1_<?php echo $res_list->id_plat; ?>_value').val('0');
                        $('#date2_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date2_<?php echo $res_list->id_plat; ?>_value').val('0');
                        $('#date3_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date3_<?php echo $res_list->id_plat; ?>_value').val('0');
                        $('#date4_<?php echo $res_list->id_plat; ?>').prop("checked", false);
                        $('#date4_<?php echo $res_list->id_plat; ?>_value').val('0');
                    }
                })
            </script>
            <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.countdown.js') ?>"></script>
            <script type="text/javascript">
                // jQuery("#btnres<?php echo $res_list->id_plat; ?>").click(function() {
                //     var nbre_pers_reserved = $("#nbre_pers<?php echo $res_list->id_plat; ?>").val();
                //     var date_selected = $("#date_selected<?php echo $res_list->id_plat; ?>").val();
                //     var champ_selected = $("#champ_selected<?php echo $res_list->id_plat; ?>").val();
                //     var nbre_platDuJour = $("#plat_jour<?php echo $res_list->id_plat; ?>").val();
                //     var heure_reservation = $("#heure_res<?php echo $res_list->id_plat; ?>").val();
                //     if (nbre_pers_reserved != null && nbre_pers_reserved != 0 && nbre_platDuJour != null && nbre_platDuJour != 0 && heure_reservation != null && heure_reservation != 0) {
                //         window.location.href = "<?php// echo base_url() . "front_soutenons/commercant/reservation_plat/" . $res_list->id_plat; ?>" + "/" + nbre_pers_reserved + "/" + nbre_platDuJour + "/" + heure_reservation + "/" + date_selected + "/" + champ_selected;
                //     } else {
                //         alert('Veuillez remplir tous les champs!')
                //     }
                // });
                // $("#btnres").click(function () {


// });
            </script>
    <?php
        }
    }
    ?>
</div>


                        
   
                     <div class="row pb-3">
                        <div class="col-12 text-center  col-12">
                            <div style="color:#E80EAE; font-size: 20px;">
                                Pour préparer votre table le jour prévu,précisez-nous le nombre de personne qui vous accompagne
                               
                            </div>
                            <p>plats du jour et non plats du jour</p>
                            <div class="" style="width: 200px;">
                            <input type="number" class="form-control textarea_style" id="nbre_pers">
                        </div>
                        </div>
                            
                        
                        <div class="col-12 text-center  col-12">
                            <p style="color:#E80EAE; font-size: 20px;">Précisez-nous l'heure de réservation
                            </p>
                            <div class="" style="width: 200px;">
                                <input type="time" class="form-control textarea_style" id="heure_res">
                            </div>
                        </div>
                        

                        <div class="col-12 text-center  col-12" style="">
                            <div style="color: white;width: 200px;" id="btnres" class="boutoninsc" >JE R&Eacute;SERVE</div>
                        </div>
                    </div>
            
<script type="text/javascript">

   $("#btnres").click(function () {

    var tab=[];
<?php foreach ($reservation as $res_list) {?>

  
                var idplat = <? echo $res_list->id_plat; ?>;
                var date_selected = $("#date_selected<?php echo $res_list->id_plat; ?>").val();
                var champ_selected = $("#champ_selected<?php echo $res_list->id_plat; ?>").val();
                var nbre_platDuJour = $("#plat_jour<?php echo $res_list->id_plat; ?>").val();
                var heure_reservation = $("#heure_res").val();
                var nbre_pers_reserved = $("#nbre_pers").val(); 
                var get={
                'id': idplat,
                'date_select':date_selected,
                'champ' : champ_selected,
                'nbr_plat' : nbre_platDuJour,
                'heure' : heure_reservation,
                'nb_perso' : nbre_pers_reserved};

                console.log(get);
                 tab.push(get);
                            
<?php }?>

    // window.location.href = "<? echo base_url('front_soutenons/commercant/test_plat');?>?tableau=" + JSON.stringify(tab);

               console.log(tab);

               $.ajax({
        type: "POST",
        url: "<? echo base_url('front_soutenons/commercant/test_plat');?>",
        data: {"tableau" : JSON.stringify(tab)},
        datatype: "JSON",
        // success:function (result) {

           // $("html").empty();
    // $("html").append(result);
    // console.log(result);

          // }
          // error:function(result) {

          //   console.log('alert');
          // }
        
    });

 });


// let profile = [];
// document.querySelectorAll("form").forEach(f => {
//   let obj = {};
//   f.querySelectorAll("input").forEach(ele => obj[ele.name] = ele.value || "");
//   profile.push(obj);
// });
// console.log(profile);


  //           jQuery("#btnres").click(function () {
  // $('form').each(function() {

  //  var id= <?php echo $res_list->id;?>;

  //      var thisForm = this;
  //               var date_selected=$("#date_selected<?php echo $res_list->id_plat; ?>").val();
  //               var champ_selected=$("#champ_selected<?php echo $res_list->id_plat; ?>").val();
  //               var nbre_platDuJour=$("#plat_jour<?php echo $res_list->id_plat;?>").val();
  //               // var data<?php //echo $res_list->id;?>
                
  //   alert(id, thisForm);



  // });
                // var nbre_pers_reserved=$("#nbre_pers").val();
                // var date_selected=$("#date_selected<?php echo $res_list->id_plat; ?>").val();
                // var champ_selected=$("#champ_selected<?php echo $res_list->id_plat; ?>").val();
                // var nbre_platDuJour=$("#plat_jour<?php echo $res_list->id_plat;?>").val();
                // var heure_reservation= $("#heure_res").val();
                // if(nbre_pers_reserved != null && nbre_pers_reserved != 0 && nbre_platDuJour != null && nbre_platDuJour != 0 && heure_reservation != null && heure_reservation != 0){
                //     // window.location.href = "<?php echo base_url()."front_soutenons/commercant/reservation_plat/".$res_list->id_plat; ?>"+"/"+nbre_pers_reserved+"/"+nbre_platDuJour+"/"+heure_reservation+"/"+date_selected+"/"+champ_selected;

                //     alert(nbre_pers_reserved);
                // }else{
                //     alert('Veuillez remplir tous les champs!')
                // }
            // });
        </script>
<!-- <div class="col-md-9 mx-auto pt-4">
    <p class="text-center text_alert_plat" style="font-size: 20px!important;font-family: libre baskerville,serif;
    font-style: italic;">
        Pour préparer votre table, <br>
        précisez nous le nombre total de personnes qui vous accompagnes?
    </p>
    <p class="text-center" style="font-family: Futura-Lt-Book,Sans-Serif!important;">plats du jour et non plats du jour</p>
    <div class="mx-auto">
        <input type="number" class="form-control textarea_style" style="padding-top: 1%!important;margin: auto;margin-bottom: 3%;width: 15%!important;" id="nbre_pers<?php echo $res_list->id_plat; ?>">
    </div>
</div>
<div style="color: white; width: 25%!important;" id="btnres<?php //echo $res_list->id_plat 
                                                            ?>" class="boutoninsc">Je réserve</div> -->

<style>
    .title_plat {
        height: 30px;
        overflow: hidden;
        font-family: Futura-LT-Book, Sans-serif;
        color: #E80EAE;
        font-weight: normal;
        text-align: center;
        font-size: 17px;
        margin-bottom: 0 !important;
    }

    .designation_plat {
        height: 40px;
        font-family: Futura-LT-Book, sans-serif;
        overflow: hidden;
        text-align: center;
        font-size: 15px;
        color: #605E5E;
        margin-bottom: 0 !important;
    }

    .prix_plat {
        color: #E80EAE;
        font-size: 25px;
        font-family: Futura-LT-Book, sans-serif;
        margin-bottom: 0 !important;
    }

    .nbr_plat {
        font-size: 19px;
        color: #ffffff;
        font-family: Futura-LT-Book, sans-serif;
        background: <?php if (isset($oInfoCommercant->bandeau_color) and $oInfoCommercant->bandeau_color != null and $group_id_commercant_user == 5) {
                        echo $oInfoCommercant->bandeau_color;
                    } elseif ($group_id_commercant_user == 4) {
                        echo '#3453a2';
                    } ?> !important;
        /*background: #3453a2;*/
        text-align: center;
        margin-bottom: 5px;
    }

    .date_plat {
        color: #000000;
        font-family: Futura-LT-Book, Sans-Serif;
        font-size: 14px;
    }

    .text_alert_plat {
        color: #E80EAE;
        font-family: Futura-LT-Book, Sans-Serif;
        font-size: 14px;
        margin-bottom: 0 !important;
    }

    .text_label_plat {
        font-size: 15px;
        font-family: Futura-LT-Book, Sans-serif !important;
        margin-bottom: 0px !important;
        padding: 0 !important;
        padding-left: 10px !important;
    }
</style>

