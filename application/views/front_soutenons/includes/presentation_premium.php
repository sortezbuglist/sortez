<?php
if (isset($oInfoCommercant->id_glissiere) AND $oInfoCommercant->id_glissiere != null) {
    $thi = get_instance();
    $glisieres = $this->load->mdlcommercant->get_glissiere_by_id_com($oInfoCommercant->IdCommercant);

}
?>
<?php
$path_img_gallery_bn = 'application/resources/front/photoCommercant/imagesbank/' . $oInfoCommercant->user_ionauth_id . '/bn/';
?>
<?php
    $thiss = get_instance();
    $video_background = $thiss->load->mdlcommercant->get_video_bg_by_idcom($oInfoCommercant->IdCommercant);
    $path_video_gallery_bg = 'application/resources/front/photoCommercant/imagesbank/' . $oInfoCommercant->user_ionauth_id . '/bgvideo/';
    $path_video_gallery_logo_old = 'application/resources/front/photoCommercant/images/';
    $path_video_gallery_bg_image = 'application/resources/front/photoCommercant/imagesbank/'.$oInfoCommercant->user_ionauth_id.'/bg/';
    $path_video_gallery_logo_new = 'application/resources/front/photoCommercant/imagesbank/'.$oInfoCommercant->user_ionauth_id.'/lg/';
?>
<div id="videoDiv" class="row">
    <div id="videoBlock">
        <?php
            if (file_exists($path_video_gallery_bg . $video_background->background_video) && isset($video_background->background_video) && $video_background->background_video != "") {
        ?>
                <video id="video" preload autoplay muted playsinline loop>
                    <source src="<?php echo base_url() . $path_video_gallery_bg . $video_background->background_video ?>" type="<?php echo $video_background->Type; ?>">
                </video>
        <?php
            }else if (file_exists($path_img_gallery_bn . $oInfoCommercant->bandeau_top) && isset($oInfoCommercant->bandeau_top) && $oInfoCommercant->bandeau_top != "") {
            echo '<img style="max-width:100%;width:100%;height:100%" src="' . base_url() . $path_img_gallery_bn . $oInfoCommercant->bandeau_top . '" "/>';
            }else{
                echo '<img style="max-width:100%;width:100%;height:100%" src="'.base_url('application/resources/privicarte/images/bandeau_top_default.jpg').'" "/>';
            }
        ?>
    </div>
    <div class="image_logo">
        <?php if(file_exists($path_video_gallery_logo_new.$oInfoCommercant->Logo) && isset($oInfoCommercant->Logo) && $oInfoCommercant->Logo != null){ ?>
            <img height="200" src="<?php echo base_url($path_video_gallery_logo_new.$oInfoCommercant->Logo) ?>" style="width: auto">
        <?php }elseif(file_exists($path_video_gallery_logo_old.$oInfoCommercant->Logo) && isset($oInfoCommercant->Logo) && $oInfoCommercant->Logo != null){ ?>
            <img height="200" src="<?php echo base_url($path_video_gallery_logo_old.$oInfoCommercant->Logo) ?>" style="width: auto">
        <?php } ?>
    </div>
    <div id="videoMessage">
        <div class="videoClick" >
            <?php if(isset($oInfoCommercant->adresse_localisation) && $oInfoCommercant->adresse_localisation != null){ ?>
                <p class="localisation"><?php echo $oInfoCommercant->adresse_localisation; ?></p>
            <?php }elseif (isset($oInfoCommercant->Adresse1) && $oInfoCommercant->Adresse1 != null){ ?>
                <p class="localisation"><?php echo $oInfoCommercant->Adresse1; ?></p>
            <?php } ?>
            <p class="telephone">
                <?php if (isset($oInfoCommercant->TelFixe) && $oInfoCommercant->TelFixe != null){ ?>
                <span>Tel: <?php echo $oInfoCommercant->TelFixe; ?> </span>
                <?php } ?>
                <?php if (isset($oInfoCommercant->TelMobile) && $oInfoCommercant->TelMobile != null){ ?>
                <span>
                    <?php if (isset($oInfoCommercant->TelFixe) && $oInfoCommercant->TelFixe != null){ ?>-<?php } ?> Mobile: <?php echo $oInfoCommercant->TelMobile; ?></span>
                <?php } ?>
            </p>
        </div>
    </div>
</div>
<!-- The video test -->
<style type="text/css">
    <?php //if((isset($oInfoCommercant->background_image) && $oInfoCommercant->background_image != null) || (isset($oInfoCommercant->bg_default_image) && $oInfoCommercant->bg_default_image == 0)) ?>
    body{
        /*background-image: url(https://www.randawilly.ovh/application/resources/front/photoCommercant/imagesbank/1992/bg/94f1335….jpg) !important;*/
        background-repeat: no-repeat !important;
        background-attachment: fixed !important;
        background-size: 100% 100%!important;
        <?php if(file_exists($path_video_gallery_bg_image.$oInfoCommercant->background_image) && (isset($oInfoCommercant->background_image) && $oInfoCommercant->background_image != null)){ ?>
            background-image: url(<?php echo base_url($path_video_gallery_bg_image.$oInfoCommercant->background_image) ?>) !important;
        <?php }elseif(file_exists($path_video_gallery_logo_old.$oInfoCommercant->background_image) && (isset($oInfoCommercant->background_image) && $oInfoCommercant->background_image != null)){ ?>
            background-image: url(<?php echo base_url($path_video_gallery_logo_old.$oInfoCommercant->background_image) ?>) !important;
        <?php }else{ ?>
            background-image: url(<?php echo base_url('wpimages/wpe3440087_06.jpg') ?>) !important;
        <?php } ?>
    }
    <?php if((isset($oInfoCommercant->background_image) && $oInfoCommercant->background_image != null) || (isset($oInfoCommercant->bg_default_image) && $oInfoCommercant->bg_default_image != null)){ ?>
    .container{
        background: rgba(255,255,255,0.6)!important;
    }
    .content_in_container,#id_mainbody_main{
        background: transparent!important;
    }
    <?php } ?>
    .image_logo{
        width: 100%;
        top: 20%;
        height: auto;
        position: absolute;
        z-index: 100;
        <?php if ($glisieres->place_logo == 'g') { ?>
            text-align: left;
        <?php } ?>
        <?php if ($glisieres->place_logo == 'c'){ ?>
            text-align: center;
        <?php } ?>
        <?php if ($glisieres->place_logo == 'd'){ ?>
            text-align: right;
        <?php } ?>

    }
    #videoMessage{
        background-color: rgba(0,0,0,<?php echo $glisieres->Transparent_du_banniere; ?>);
    }
    #videoDiv {
        height: 600px;
        overflow: hidden;
        position: relative;
    }
    #videoBlock {
        width: 100%;
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    #videoMessage {
        width: 100%;
        height:100%;
        position: absolute;
        top: 0;
        left: 0;
    }
    #videoMessage{
        position: absolute;
        bottom: 0;
        width: 100%;
        left: 0;
        padding: 0.4em;
        margin: 0;
    }
    #videoMessage {
        text-shadow: 2px 2px 2px #000000;
        color:white;
        z-index:99
    }
    #video{
        width: 100%;
        height: auto;
    }
    .videoClick {
        position: absolute;
        bottom: 0;
        width: 100%;
        margin: 0;
        padding: 0;
        left:0;
        text-align:center
    }
    .videoClick .telephone{
        color:white;
        background-color: rgba(0, 0, 0, 0.5);
        padding-bottom: 15px;
        font-size: 25px;
        cursor:pointer;
        margin: 0;
        font-family:Futura-LT-Book,sans-serif;
    }
    .videoClick .localisation{
        color:white;
        padding-top: 15px;
        background-color: rgba(0, 0, 0, 0.5);
        /* font-size: 25px; */
        font-size:20px;
        cursor:pointer;
        margin: 0;
        font-family:Futura-LT-Book,sans-serif;
    }
</style>