
<?php
$thisss = get_instance();
$thisss->load->model('Mdl_plat_du_jour');
$data_res = $thisss->Mdl_plat_du_jour->get_resinfocom_by_idcom($oInfoCommercant->IdCommercant);
?>
<div class="d-block d-mg-none d-lg-none d-xl-none">
    <div class="col-lg-12 text-center padding0 mt-2 mb-2">
        <?php if (isset($oInfoCommercant->adresse_localisation) and $oInfoCommercant->adresse_localisation != null and isset($oInfoCommercant->codepostal_localisation) and $oInfoCommercant->codepostal_localisation != null) { ?>
            <a title="Localisation"
               data-fancybox
               data-type="iframe"
               data-src="https://maps.google.fr/maps?f=q&amp;source=s_q&amp;hl=fr&amp;geocode=&amp;q=<?php echo $oInfoCommercant->adresse_localisation . ", " . $oInfoCommercant->codepostal_localisation; ?>&amp;aq=0&amp;ie=UTF8&amp;hq=&amp;hnear=<?php echo $oInfoCommercant->adresse_localisation . ", " . $oInfoCommercant->codepostal_localisation; ?>&amp;t=m&amp;vpsrc=0&amp;output=embed"
               id="id_localisation_agenda_1100_mobile"
               class="fancybox_localisation_agenda_1100 various fancybox.iframe"
               style="text-decoration: unset">
                <div class="row mobile_img_square_menu mt-3 pt-3 pb-3 plan">
                    <div class="col-4">
                        <img class="img-fluid w-75"
                             src="<?php echo base_url('/assets/soutenons/plan_mobile.png') ?> "
                             style="margin-left: 5px;">
                    </div>
                    <div class="col-8 text-center pt-4">
                        Visualisez
                        votre
                        plan
                        !
                    </div>
                </div>
            </a>
        <?php } ?>
        <?php if (isset($oInfoCommercant->Video) and $oInfoCommercant->Video != null) { ?>
            <a data-fancybox=""
               data-animation-duration="500"
               data-src="<?php echo $oInfoCommercant->Video; ?>"
               title="Video youtube"
               style="text-decoration: unset">
                <div class="row mobile_img_square_menu mt-3 pt-3 pb-3 video">

                    <div class="col-4">
                        <img class="img-fluid w-75"
                             src="<?php echo base_url('/assets/soutenons/video_mobile.png') ?> "
                             style="margin-left: 5px;">
                    </div>
                    <div class="col-8 text-center pt-4">
                        Visualisez
                        !
                    </div>

                </div>
            </a>
        <?php } ?>
        <?php if (isset($link_doc_pdf_partner_to_show) and $link_doc_pdf_partner_to_show != "#") { ?>
            <a href="<?php echo $link_doc_pdf_partner_to_show; ?>"
               <?php if ($link_doc_pdf_partner_to_show != "#"){ ?>target="_blank"
               title="<?php if (isset($oInfoCommercant->titre_Pdf) && $oInfoCommercant->titre_Pdf != "") echo $oInfoCommercant->titre_Pdf; else echo "Plaquette commerciale PDF"; ?>"
               <?php }else{ ?>title="Pas de fichier pdf pour <?php echo $oInfoCommercant->NomSociete; ?>"<?php } ?>
               style="text-decoration: unset">
                <div class="row mobile_img_square_menu mt-3 pt-3 pb-3 pdf">

                    <div class="col-4">
                        <img class="img-fluid w-75"
                             src="<?php echo base_url('/assets/soutenons/pdf_mobile.png') ?> "
                             style="margin-left: 5px;">
                    </div>
                    <div class="col-8 text-center pt-4">
                        Téléchargez
                        !
                    </div>

                </div>
            </a>
        <?php } ?>
        <?php
        if(!empty($data_res)){ ?>
            <a href="<?php echo base_url('/application/resources/front/images/plat/pdf/'.$data_res->Nom_pdf_menu.'.pdf') ?>"
               target="_blank"
               title="Nos Cartes et Menus"
               style="text-decoration: unset">
                <div style="background-color: #3453A2;" class="row mobile_img_square_menu mt-3 pt-3 pb-3 pdf">
                    <div class="col-4">
                        <img class="img-fluid w-75"
                             src="<?php echo base_url('/assets/soutenons/pdf_mobile.png') ?> "
                             style="margin-left: 5px;">
                    </div>
                    <div class="col-8 text-center pt-4">
                        Téléchargez Nos Cartes et Menus
                        !
                    </div>

                </div>
            </a>
        <?php } ?>
        <a data-fancybox=""
           data-animation-duration="500"
           data-src="#divContactPartnerForm"
           href="javascript:;"
           id="IdCsontactPartnerForm_mobile"
           title="Contactez-nous!"
           style="text-decoration: unset">
            <div class="row mobile_img_square_menu mt-3 pt-3 pb-3 mail_mobile">

                <div class="col-4">
                    <img class="img-fluid w-75"
                         src="<?php echo base_url('/assets/soutenons/mail_mobile.png') ?> "
                         style="margin-left: 5px;">
                </div>
                <div class="col-8 text-center pt-4">
                    Adressez
                    un
                    mail
                    !
                </div>

            </div>
        </a>
        <?php if (isset($oInfoCommercant->SiteWeb) && $oInfoCommercant->SiteWeb != "") { ?>
            <?php
            $file = $oInfoCommercant->SiteWeb;
            $file_headers = @get_headers($file);
            if (!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
                $exists = 0;
            } else {
                $exists = 1;
            }
            if ($exists == 1) {
                ?>
                <a href="<?php echo $oInfoCommercant->SiteWeb; ?>"
                   target="_blank"
                   title="Accés à notre site!"
                   style="text-decoration: unset">
                    <div class="row mobile_img_square_menu mt-3 pt-3 pb-3 bloc_new">

                        <div class="col-4">
                            <img class="img-fluid w-75"
                                 src="<?php echo base_url('/assets/soutenons/bloc_new_mobile.png') ?> "
                                 style="margin-left: 5px;">
                        </div>
                        <div class="col-8 text-center pt-4">
                            Accès
                            à
                            notre
                            site
                            web
                            !
                        </div>

                    </div>
                </a>
            <?php } ?>
        <?php } ?>

        <?php
        $thisss = get_instance();
        $thisss->load->model('Mdl_menu');
        $datamenu = $thisss->Mdl_menu->get_menu_data_by_idcom($oInfoCommercant->IdCommercant);
        if (!empty($datamenu) AND $datamenu->is_activ_default_btn == 1 && $datamenu->image_menu_gen ==null){ ?>
            <a href="<?php echo site_url().$oInfoCommercant->nom_url;?>/menus_commercants"
               title="soutenonslecommercelocal.fr"
               style="text-decoration: unset">
                <div class="row mobile_img_square_menu mt-3 pt-3 pb-3 soutenons_mobile">
                    <div class="col-4">
                        <img class="image_rounded w-75" src="<?php echo base_url("assets/image/default_menu_img.jpg"); ?>">
                    </div>
                    <div class="col-8 text-center pt-4">
                        <?php if (isset($datamenu->menu_value) AND $datamenu->menu_value != null ){echo $datamenu->menu_value;}else{echo "Nos cartes & menus";} ?>
                    </div>
                </div>
            </a>
        <?php } ?>

        <?php if (!empty($datamenu) AND $datamenu->is_activ_default_btn != 1 && $datamenu->image_menu_gen !=null && is_file("application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/menu_icon/".$datamenu->image_menu_gen)){ ?>
            <a href="<?php echo site_url().$oInfoCommercant->nom_url;?>/menus_commercants"
               title="soutenonslecommercelocal.fr"
               style="text-decoration: unset">
                <div class="row mobile_img_square_menu mt-3 pt-3 pb-3 soutenons_mobile">
                    <div class="col-4">
                        <img class="image_rounded w-75" src="<?php echo base_url("application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/menu_icon/".$datamenu->image_menu_gen); ?>">
                    </div>
                    <div class="col-8 text-center pt-4">
                        <?php if (isset($datamenu->menu_value) AND $datamenu->menu_value != null ){echo $datamenu->menu_value;}else{echo "Nos cartes & menus";} ?>
                    </div>
                </div>
            </a>
        <?php } ?>

        <a href="https://www.soutenonslecommercelocal.fr/"
           target="_blank"
           title="soutenonslecommercelocal.fr"
           style="text-decoration: unset">
            <div class="row mobile_img_square_menu mt-3 pt-3 pb-3 soutenons_mobile">

                <div class="col-4">
                    <img class="img-fluid w-75"
                         src="<?php echo base_url('/assets/soutenons/soutenons_mobile.png') ?> "
                         style="margin-left: 5px;">
                </div>
                <div class="col-8 text-center pt-4">
                    Accès
                    au
                    site
                </div>

            </div>
        </a>
    </div>
</div>
<style type="text/css">
    .image_rounded{
        border-radius: 45px;
    }
    .mobile_img_square_menu {
        color: #ffffff;
        font-family: Futura-LT-Book, Sans-Serif;
        font-size: 18px;
    }

    .mobile_img_square_menu.plan {
        background-color: #164F18;
    }

    .mobile_img_square_menu.video {
        background-color: #FF6161;
    }

    .mobile_img_square_menu.mail_mobile {
        background-color: #75CBA8;
    }

    .mobile_img_square_menu.bloc_new {
        background-color: #3453A2;
    }

    .mobile_img_square_menu.soutenons_mobile {
        background-color: #E80EAE;
    }
</style>

<div id="res_gite" style="display: none">
</div>
<div class="w-50" id="res_sej" style="display: none;width: 50%!important;">
    <div class="row text-center">
        <div class="col-lg-12">
            <h5 style="font-weight: bold"> MA DEMANDE DE RÉSERVATION</h5>
            <p>Pour effectuer votre demande de réservation merci de remplir et valider le formulaire ci-dessous</p>
        </div>
    </div>
    <div class="p-3" style="border: double">
        <div class="row pt-4 pb-4">
            <div class="col-lg-12">
                <h6>VOS COORDONNÉES</h6>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <p>Si vous possédez une carte de fidélité vivresaville.fr, précisez son numéro et vos coordonnées seront intégrées automatiquement:</p>
            </div>
            <div class="col-lg-4"><input name="reservation_sejour[num_card]"  class="form-control" type="number" id="num_card" min="0"></div><div class="col-lg-2"><button id="autofill_coordonate_sejour" class="btn btn-success">OK</button></div>
        </div>
        <div class="row pt-2">
            <div class="col-lg-2">
                pays:
            </div>
            <div class="col-lg-4"><input name="reservation_sejour[Pays]" id="Pays_sejour"  class="form-control"  type="text"></div>
        </div>
        <div class="row pt-2">
            <div class="col-lg-2">
                Nom*:
            </div>
            <div class="col-lg-4"><input  name="reservation_sejour[Nom]" id="Nom_sejour" required  class="form-control"  type="text"></div>
            <div class="col-lg-2">Prénom*</div>
            <div class="col-lg-4"><input  name="reservation_sejour[prenom]" id="prenom_sejour" required  class="form-control"  type="text"></div>
        </div>
        <div class="row pt-2">
            <div class="col-lg-2">
                Adresse:
            </div>
            <div class="col-lg-10"><input name="reservation_sejour[Adresse]" id="Adresse_sejour"  class="form-control" type="text"></div>
        </div>
        <div class="row pt-2">
            <div class="col-lg-2">
                Code postal:
            </div>
            <div class="col-lg-4"><input  name="reservation_sejour[code_postal]" id="code_postal_sejour"  class="form-control"  type="text"></div>
            <div class="col-lg-2">ville</div>
            <div class="col-lg-4">
                <select  name="reservation_sejour[id_ville]"  id="id_ville_sejour" class="form-control">
                    <option value="0">--Choisir--</option>
                    <?php foreach ($ville as $villes){ ?>
                        <option value="<?php echo $villes->IdVille ?>"><?php echo $villes->Nom;?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="row pt-2">
            <div class="col-lg-2">
                Email:
            </div>
            <div class="col-lg-4"><input  name="reservation_sejour[mail]" id="mail_sejour"  class="form-control"  type="email"></div>
            <div class="col-lg-2">Mobile*</div>
            <div class="col-lg-4"><input name="reservation_sejour[tel]" id="tel_sejour" required  class="form-control"  type="text"></div>
        </div>
        <div class="row pt-4 pb-4">
            <div class="col-lg-12">
                <h6>VOS INFORMATIONS DE SÉJOUR</h6>
            </div>
        </div>
        <div class="row pt-2">
            <div class="col-lg-2 pt-2">
                Arrivés:
            </div>
            <div class="col-lg-4"><input  name="reservation_sejour[date_debut_res]"  id="date_debut_res_sejour" class="form-control"  type="date"></div>
            <div class="col-lg-2 pt-2">Départ</div>
            <div class="col-lg-4"><input  name="reservation_sejour[date_fin_res]"  id="date_fin_res_sejour" class="form-control"  type="date"></div>
        </div>

        <div class="row pt-2">
            <div class="col-lg-2 pt-2">
                Adultes:
            </div>
            <div class="col-lg-4"><input  name="reservation_sejour[nbre_adulte]" id="nbre_adulte_sejour"  class="form-control" max="10"  type="number"></div>
            <div class="col-lg-2 pt-2">Enfants</div>
            <div class="col-lg-4"><input  name="reservation_sejour[nbre_enfant]" id="nbre_enfant_sejour"  class="form-control" max="10"  type="number"></div>
        </div>
        <div class="row pt-4 pb-4">
            <div class="col-lg-12">
                <h6>VOTRE MESSAGE</h6>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <textarea name="reservation_sejour[message_client]" id="message_client_sejour"  class="form-control" placeholder="Ajouter des informations complémentaires pour préciser votre demande de séjour à votre hôte"></textarea>
            </div>
        </div>
        <div class="row text-center pt-4">
            <div style="color: white" class="col-lg-6 pt-2"><a id="cancel_res_sejour" onclick="window.location.reload()" class="btn btn-danger w-100">ANNULER</a></div>
            <div  style="color: white" class="col-lg-6 pt-2"><a id="submit_res_sejour" class="btn btn-success w-100">ADRESSER CE MESSAGE</a></div>
        </div>
    </div>
    <div class="row p-4">
        <p>Les informations portées sur ce formulaire vous concernant sont à l’usage de notre établissement et de nos prestataires techniques afin de traiter votre demande.<br>

            Vous acceptez notre politique de confidentialité des données personnelles.<br>

            Conformément à la Loi n°78-17 du 6 janvier 1978 modifiée, et au règlement général sur la protection des données, vous disposez d’un droit d’accès, de rectification, d’effacement, d'opposition et de limitation du traitement relatif aux données personnelles qui vous concernent, ainsi que du droit à la portabilité des données et de définition de vos directives relatives à la gestion de vos données après votre décès.
        </p>
        <p>
            Vous pouvez exercer ces droits par email à l'adresse <span style="text-decoration: underline"><?php echo $oInfoCommercant->Email?></span>
        </p>
    </div>
</div>


<div class="w-50" id="res_tab" style="display: none;width: 50%!important;">
    <div class="row text-center">
        <div class="col-lg-12">
            <h5 style="font-weight: bold"> MA DEMANDE DE RÉSERVATION</h5>
            <p>Pour effectuer votre demande de réservation merci de remplir et valider le formulaire ci-dessous</p>
            <p>Nous vous remercions de bien vouloir nous adresser le formulaire ci-dessous.<br>
                <span style="color: red;text-decoration: underline">Attention :</span> cette demande ne constitue pas une réservation ferme, notre service réception entrera en contact avec vous dans les meilleurs délais pour valider votre réservation.
                Pour toute réservation dans moins de 24 heures, veuillez nous contacter par téléphone.</p>
        </div>
    </div>
    <div class="p-3" style="border: double">
        <div class="row pt-4 pb-4">
            <div class="col-lg-12">
                <h6>VOS COORDONNÉES</h6>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <p>Si vous possédez une carte de fidélité vivresaville.fr, précisez son numéro et vos coordonnées seront intégrées automatiquement:</p>
            </div>
            <div class="col-lg-4"><input name="reservation_table[num_card_table]"  class="form-control" type="number" value="0" id="num_card_table"></div><div class="col-lg-2"><button id="autofill_coordonate_table" class="btn btn-success">OK</button></div>
        </div>
        <div class="row pt-2">
            <div class="col-lg-2">
                pays:
            </div>
            <div class="col-lg-4"><input name="reservation_table[Pays]" id="Pays_table"  class="form-control"  type="text"></div>
        </div>
        <div class="row pt-2">
            <div class="col-lg-2">
                Nom*:
            </div>
            <div class="col-lg-4"><input  name="reservation_table[Nom]" id="Nom_table" required  class="form-control"  type="text"></div>
            <div class="col-lg-2">Prénom*</div>
            <div class="col-lg-4"><input  name="reservation_table[prenom]" id="prenom_table" required  class="form-control"  type="text"></div>
        </div>
        <div class="row pt-2">
            <div class="col-lg-2">
                Adresse:
            </div>
            <div class="col-lg-10"><input name="reservation_table[Adresse]" id="Adresse_table"  class="form-control" type="text"></div>
        </div>
        <div class="row pt-2">
            <div class="col-lg-2">
                Code postal:
            </div>
            <div class="col-lg-4"><input  name="reservation_table[code_postal]" id="code_postal_table"  class="form-control"  type="text"></div>
            <div class="col-lg-2">ville</div>
            <div class="col-lg-4">
                <select  name="reservation_table[id_ville]"  id="id_ville_table" class="form-control">
                    <option value="0">--Choisir--</option>
                    <?php foreach ($ville as $villes){ ?>
                        <option value="<?php echo $villes->IdVille ?>"><?php echo $villes->Nom;?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="row pt-2">
            <div class="col-lg-2">
                Email:
            </div>
            <div class="col-lg-4"><input  name="reservation_table[mail]" id="mail_table"  class="form-control"  type="email"></div>
            <div class="col-lg-2">Mobile*</div>
            <div class="col-lg-4"><input name="reservation_table[tel]" id="tel_table" required  class="form-control"  type="text"></div>
        </div>
        <div class="row pt-4 pb-4">
            <div class="col-lg-6">
                <h6>VOTRE RÉSERVATION</h6>
            </div>
            <div class="col-lg-2">
                <span>Date:</span>
            </div>
            <div class="col-lg-4">
                <input type="date" name="reservation_table[date_res]" id="date_res">
            </div>
        </div>
        <div class="row pt-2">
            <div class="col-lg-2 pt-2">
                Midi:
            </div>
            <div class="col-lg-4">
                <select  name="reservation_table[heure_midi]"  id="heure_midi" class="form-control">
                    <option value="">Non</option>
                    <option value="11:00">11:00</option>
                    <option value="11:30">11:30</option>
                    <option value="12:00">12:00</option>
                    <option value="12:30">12:30</option>
                    <option value="13:00">13:00</option>
                    <option value="13:30">13:30</option>
                    <option value="14:00">14:00</option>
                    <option value="14:30">14:30</option>
                    <option value="15:00">15:00</option>
                </select></div>
            <div class="col-lg-2 pt-2">Soir</div>
            <div class="col-lg-4">
                <select  name="reservation_table[heure_soir]"  id="heure_soir" class="form-control">
                    <option value="">Non</option>
                    <option value="19:00">19:00</option>
                    <option value="19:30">19:30</option>
                    <option value="20:00">20:00</option>
                    <option value="20:30">20:30</option>
                    <option value="21:00">21:00</option>
                    <option value="21:30">21:30</option>
                    <option value="22:00">22:00</option>
                </select>
            </div>
        </div>

        <div class="row pt-2">
            <div class="col-lg-2 pt-2">
                Adultes:
            </div>
            <div class="col-lg-4"><input  name="reservation_table[nbre_adulte]" id="nbre_adulte_table"  class="form-control" max="10"  type="number"></div>
            <div class="col-lg-2 pt-2">Enfants</div>
            <div class="col-lg-4"><input  name="reservation_table[nbre_enfant]" id="nbre_enfant_table"  class="form-control" max="10"  type="number"></div>
        </div>
        <div class="row pt-4 pb-4">
            <div class="col-lg-12">
                <h6>VOTRE MESSAGE</h6>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <textarea name="reservation_table[message_client]" id="message_client_table"  class="form-control" placeholder="Ajouter des informations complémentaires pour préciser votre demande de table à votre hôte"></textarea>
            </div>
        </div>
        <div class="row text-center pt-4">
            <div style="color: white" class="col-lg-6 pt-2"><a id="cancel_res_table" onclick="window.location.reload()" class="btn btn-danger w-100">ANNULER</a></div>
            <div  style="color: white" class="col-lg-6 pt-2"><a id="submit_res_table" class="btn btn-success w-100">ADRESSER CE MESSAGE</a></div>
        </div>
    </div>
    <div class="row p-4">
        <p>Les informations portées sur ce formulaire vous concernant sont à l’usage de notre établissement et de nos prestataires techniques afin de traiter votre demande.<br>

            Vous acceptez notre politique de confidentialité des données personnelles.<br>

            Conformément à la Loi n°78-17 du 6 janvier 1978 modifiée, et au règlement général sur la protection des données, vous disposez d’un droit d’accès, de rectification, d’effacement, d'opposition et de limitation du traitement relatif aux données personnelles qui vous concernent, ainsi que du droit à la portabilité des données et de définition de vos directives relatives à la gestion de vos données après votre décès.
        </p>
        <p>
            Vous pouvez exercer ces droits par email à l'adresse <span style="text-decoration: underline"><?php echo $oInfoCommercant->Email?></span>
        </p>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function() {

        $("#contact_partner_nom").focusin(function() {	  if ($(this).val()=="Votre nom *") $(this).val('');	});
        $("#contact_partner_nom").focusout(function() {	  if ($(this).val()=="") $(this).val('Votre nom *');	});

        $("#contact_partner_tel").focusin(function() {	  if ($(this).val()=="Votre numéro de téléphone *") $(this).val('');	});
        $("#contact_partner_tel").focusout(function() {	  if ($(this).val()=="") $(this).val('Votre numéro de téléphone *');	});

        $("#contact_partner_mail").focusin(function() {	  if ($(this).val()=="Votre courriel *") $(this).val('');	});
        $("#contact_partner_mail").focusout(function() {	  if ($(this).val()=="") $(this).val('Votre courriel *');	});

        $("#contact_partner_msg").focusin(function() {	  if ($(this).val()=="Votre message *") $(this).val('');	});
        $("#contact_partner_msg").focusout(function() {	  if ($(this).val()=="") $(this).val('Votre message *');	});

        $("#contact_partner_reset").click(function() {
            $("#contact_partner_nom").val('Votre nom *');
            $("#contact_partner_tel").val('Votre numéro de téléphone *');
            $("#contact_partner_mail").val('Votre courriel *');
            $("#contact_partner_msg").val('Votre message *');
            $("#spanContactPartnerForm").html('* champs obligatoires');
        });

        $("#contact_recommandation_reset_recommandation").click(function() {
            $("contact_recommandation_msg").val('Votre message *');
            $("contact_recommandation_nom").val('Votre nom *');
            $("contact_recommandation_mail_ami").val('Courriel de votre ami *');
            $("contact_recommandation_mail").val('Votre courriel *');
            $("#spanContactPrivicarteRecommandationForm").html('* Les champs sont obligatoires');
        });

        $("#contact_recommandation_reset_recommandationfc").click(function() {
            //alert("tag");
            $("#contact_recommandation_msgfc").val('Votre message *');
            $("#contact_recommandation_nomfc").val('Votre nom *');
            $("#contact_recommandation_mail_amifc").val('Courriel de votre ami *');
            $("#contact_recommandation_mailfc").val('Votre courriel *');
            $("#spanContactPrivicarteRecommandationFormfc").html('* Les champs sont obligatoires');
        });


        var width = $('.g-recaptcha').parent().width();
        if (width < 300) {
            var scale = width / 300;
            $('.g-recaptcha').css('transform', 'scale(' + scale + ')');
            $('.g-recaptcha').css('-webkit-transform', 'scale(' + scale + ')');
            $('.g-recaptcha').css('transform-origin', '0 0');
            $('.g-recaptcha').css('-webkit-transform-origin', '0 0');
            $('.rc-anchor-logo-portrait').css('margin','0');
        }
        $("#contact_partner_send").click(function() {
            var error = 0;
            var contact_partner_nom = $("#contact_partner_nom").val();
            if (contact_partner_nom == '' || contact_partner_nom == 'Votre nom *') error = 1;
            var contact_partner_tel = $("#contact_partner_tel").val();
            if (contact_partner_tel == '' || contact_partner_tel == 'Votre numéro de téléphone *') error = 1;
            var contact_partner_mail = $("#contact_partner_mail").val();
            if (contact_partner_mail == '' || contact_partner_mail == 'Votre courriel *') error = 1;
            if (!validateEmail(contact_partner_mail)) error = 2;
            var contact_partner_msg = $("#contact_partner_msg").val();
            if (contact_partner_msg == '' || contact_partner_msg == 'Votre message *') error = 1;
            $("#spanContactPartnerForm").html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

            if (error == 1) {
                $("#spanContactPartnerForm").html('<span style="color:#FF0000;">* Les champs sont obligatoires</span>');
            } else if (error == 2) {
                $("#spanContactPartnerForm").html('<span style="color:#FF0000;">* Adresse email invalide</span>');
                $("#contact_partner_mail").css('border-color','#ff0000');
            } else {
                $.post(
                    "<?php echo site_url("front/professionnels/contact_partner_sendmail/");?>",
                    {
                        contact_partner_nom:contact_partner_nom,
                        contact_partner_tel:contact_partner_tel,
                        contact_partner_mail:contact_partner_mail,
                        contact_partner_msg:contact_partner_msg,
                        contact_partner_mailto:"<?php echo $oInfoCommercant->Email; ?>"
                    },
                    function( data ) {
                        $("#spanContactPartnerForm").html(data);
                        //setTimeout(function(){ $.fancybox.close(); }, 4000);
                    });
            }
        });

        //fonction envoi mail recommandation
        $("#contact_recommandation_send_recommandationfc").click(function() {
            //alert('tag');
            $("#spanContactPrivicarteRecommandationFormfc").html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            var error = 0;
            var contact_recommandation_msg = $("#contact_recommandation_msgfc").val();
            alert(contact_recommandation_msg);
            if (contact_recommandation_msg == '' || contact_recommandation_msg == 'Votre message *') error = 1;
            var contact_recommandation_nom = $("#contact_recommandation_nomfc").val();
            alert(contact_recommandation_nom);
            if (contact_recommandation_nom == '' || contact_recommandation_nom == 'Votre nom *') error = 1;
            var contact_recommandation_mail_ami = $("#contact_recommandation_mail_amifc").val();
            alert(contact_recommandation_mail_ami);

            if (contact_recommandation_mail_ami == '' || contact_recommandation_mail_ami == 'Courriel de votre ami *') error = 1;
            else if (!validateEmail(contact_recommandation_mail_ami)) error = 3;
            var contact_recommandation_mail = $("#contact_recommandation_mailfc").val();
            if (contact_recommandation_mail == '' || contact_recommandation_mail == 'Votre courriel *') error = 1;
            else if (!validateEmail(contact_recommandation_mail)) error = 2;


            if (error == 1) {
                $("#spanContactPrivicarteRecommandationFormfc").html('<span style="color:#FF0000;">* Les champs sont obligatoires</span>');
            } else if (error == 2) {
                $("#spanContactPrivicarteRecommandationFormfc").html('<span style="color:#FF0000;">* Adresse email invalide</span>');
                $("#contact_recommandation_mailfc").css('border-color','#ff0000');
                //alert("invalide mail");
            } else if (error == 3) {
                $("#spanContactPrivicarteRecommandationFormfc").html('<span style="color:#FF0000;">* Adresse email invalide</span>');
                $("#contact_recommandation_mail_amifc").css('border-color','#ff0000');
            } else {
                $.post(
                    "<?php echo site_url("front/professionnels/recommandation_partner_sendmail/");?>",
                    {
                        contact_recommandation_nom:contact_recommandation_nom,
                        contact_recommandation_tel:'',
                        contact_recommandation_mail:contact_recommandation_mail,
                        contact_recommandation_msg:contact_recommandation_msg,
                        contact_recommandation_mailto:contact_recommandation_mail_ami
                    },
                    function( data ) {
                        $("#spanContactPrivicarteRecommandationFormfc").html(data);
                    });
            }

            //alert(error);
        });


        /*$(".fancybox_localisation_agenda_1100").fancybox();
        $("#id_localisation_agenda_1100").fancybox({
            autoScale : false,
            overlayOpacity      : 0.8, // Set opacity to 0.8
            overlayColor        : "#000000", // Set color to Black
            padding         : 5,
            width         : 520,
            height        : 800,
            transitionIn      : 'elastic',
            transitionOut     : 'elastic',
            type          : 'iframe'
        });
        $("#IdContactPartnerForm").fancybox({
            autoScale : false,
            overlayOpacity      : 0.8, // Set opacity to 0.8
            overlayColor        : "#000000", // Set color to Black
            padding         : 5,
            width         : 520,
            height        : 800,
            transitionIn      : 'elastic',
            transitionOut     : 'elastic'
        });
        $("#IdVideoPartner").fancybox({
            autoScale : false,
            overlayOpacity      : 0.8, // Set opacity to 0.8
            overlayColor        : "#000000", // Set color to Black
            padding         : 5,
            width         : 520,
            height        : 800,
            transitionIn      : 'elastic',
            transitionOut     : 'elastic'
        });
        $("#idFacebookProForm").fancybox({
            autoScale : false,
            overlayOpacity      : 0.8, // Set opacity to 0.8
            overlayColor        : "#000000", // Set color to Black
            padding         : 5,
            width         : 520,
            height        : 800,
            transitionIn      : 'elastic',
            transitionOut     : 'elastic',
            type          : 'iframe'
        });
        $("#idTwitterProForm").fancybox({
            autoScale : false,
            overlayOpacity      : 0.8, // Set opacity to 0.8
            overlayColor        : "#000000", // Set color to Black
            padding         : 5,
            width         : 1000,
            height        : 800,
            transitionIn      : 'elastic',
            transitionOut     : 'elastic',
            type          : 'iframe'
        });
        $("#idGoogleplusProForm").fancybox({
            autoScale : false,
            overlayOpacity      : 0.8, // Set opacity to 0.8
            overlayColor        : "#000000", // Set color to Black
            padding         : 5,
            width         : 1000,
            height        : 800,
            transitionIn      : 'elastic',
            transitionOut     : 'elastic',
            type          : 'iframe'
        });*/

        $("#abonner").click(function(){
            var cap = document.getElementById("g-recaptcha-response").value;
            tester(cap);//21103 //toFirstIdDatatourisme
        });

        function tester(cap) {
            jQuery.ajax({
                type: "POST",
                url: "<?php echo site_url("front/particuliers/test_captcha"); ?>",
                data: 'g-recaptcha-response=' + (cap),
                dataType: "json",
                success: function (data) {


                    if (data.captcha == "OK") {
                        let nom_abonner=$("#nom_abonner").val();
                        let email_abonner=$("#email_abonner").val();
                        let mobile_abonner=$("#mobile_abonner").val();
                        let idcom= "<?php echo $oInfoCommercant->IdCommercant ?>";
                        if (arobaceestbon(email_abonner)==true){


                            if (nom_abonner=='' && email_abonner=='' && email_abonner==''){
                                alert('Veuillez completer les champs obligatoires');
                            }else{
                                data="nom_abonner="+nom_abonner+"&email_abonner="+email_abonner+"&mobile_abonner="+mobile_abonner+"&idcom="+idcom;

                                jQuery.ajax({
                                    url: "<?php echo site_url('front/commercant/get_abonner_news_letter');?>",
                                    dataType: 'text',
                                    type: 'POST',
                                    data: data,
                                    success: function (data) {
                                        if (data==='exist') {
                                            alert("Vous êtes déjà abonnée");
                                        }else if (data==='ok') {
                                            alert("Votre inscription a été éffectuée avec succès");
                                        }else if (data=='ko'){
                                            alert("Une erreur s'est produite");
                                        }
                                    },
                                    error: function (data) {
                                        alert("Une erreur s'est produite");
                                    }
                                });
                            }
                        }else{
                            alert ("Attention:\nErreur de saisie dans votre adresse de messagerie.");
                        }
                    }
                    if (data.captcha == "NO") {
                        alert("captha non valide");
                    }
                },
                error: function (data) {

                    alert("data");
                }
            });
        }



        $("#code_postal_sejour").change(function(){
            let postal_code=$("#code_postal_sejour").val();
            data="code_postal="+postal_code;
            $("#id_ville_sejour").val('');
            jQuery.ajax({
                url: "<?php echo site_url('front/commercant/get_ville_by_code_postal');?>",
                dataType: 'text',
                type: 'POST',
                data: data,
                success: function (data) {
                    $("#id_ville_sejour").val(data);
                },
                error: function (data) {
                    $("#id_ville_sejour").val('');
                }
            });

        });

        $("#code_postal_table").change(function(){
            let postal_code=$("#code_postal_table").val();
            data="code_postal="+postal_code;
            $("#id_ville_table").val('');
            jQuery.ajax({
                url: "<?php echo site_url('front/commercant/get_ville_by_code_postal');?>",
                dataType: 'text',
                type: 'POST',
                data: data,
                success: function (data) {
                    $("#id_ville_table").val(data);
                },
                error: function (data) {
                    $("#id_ville_table").val('');
                }
            });

        });

        $("#autofill_coordonate_table").click(function () {
            var num_card=$("#num_card_table").val();
            data='num_card='+num_card;
            jQuery.ajax({
                url: "<?php echo site_url('front/commercant/get_users_by_id_card');?>",
                dataType: 'text',
                type: 'POST',
                data: data,
                success: function (datasT) {
                    if (datasT != 'no'){
                        res=JSON.parse(datasT);
                        $('#Nom_table').val(res.Nom);
                        $('#Adresse_table').val(res.Adresse);
                        $('#prenom_table').val(res.Prenom);
                        $('#code_postal_table').val(res.CodePostal);
                        $('#mail_table').val(res.Email);
                        $('#id_ville_table').val(res.IdVille);
                        $('#tel_table').val(res.Portable);
                        $('#num_card_table').val(num_card)
                    }
                },
                error: function (data) {
                    console.log(data);
                    alert('Une erreur s\'est produite');
                }
            });
        });

        $("#submit_res_table").click(function() {
            let Id_commercant="<?php echo $oInfoCommercant->IdCommercant; ?>";
            let id_client="<?php if (isset($client) AND $client !=null){echo $client->IdUser;}else{echo 0;} ?>";
            let Pays=$('#Pays_table').val();
            let Nom=$('#Nom_table').val();
            let Adresse=$('#Adresse_table').val();
            let code_postal=$('#code_postal_table').val();
            let mail_se=$('#mail_table').val();
            let prenom=$('#prenom_table').val();
            let idville=$('#id_ville_table').val();
            let tel=$('#tel_table').val();
            let heure_midi=$('#heure_midi').val();
            let heure_soir=$('#heure_soir').val();
            let nbre_adulte=$('#nbre_adulte_table').val();
            let nbre_enfant=$('#nbre_enfant_table').val();
            let num_card=$('#num_card_table').val();
            let message_client=$('#message_client_table').val();
            let date_res=$('#date_res').val();
            if (heure_midi ===""){heure_midi="";}
            if (heure_soir ===""){heure_soir="";}
            if (num_card===""){num_card=0;}
            let data="Id_commercant="+Id_commercant+"&id_client="+id_client+"&Pays="+Pays+"&Nom="+Nom+"&Adresse="+Adresse+"&code_postal="+code_postal+"&mail="+mail_se+"&prenom="+prenom+"&id_ville="+idville+"&tel="+tel+"&heure_midi="+heure_midi+"&heure_soir="+heure_soir+"&nbre_adulte="+nbre_adulte+"&nbre_enfant="+nbre_enfant+"&num_card="+num_card+"&message_client="+message_client+"&date_res="+date_res;
            console.log(data);
            /*if (id_client ==0){
                alert('Connectez-vous a un compte particulier pour pouvoir reserver');
            }else*/ if(  Nom!="" && tel!="" && prenom!="" && message_client!=""){
                if (arobaceestbon(mail_se)===true){
                    jQuery.ajax({
                        url: "<?php echo site_url('front/commercant/submit_res_table');?>",
                        dataType: 'text',
                        type: 'POST',
                        data: data,
                        success: function (data) {
                            if (data=='ok'){
                                alert('Réservation réussi!')
                            }else if (data=='ko'){
                                alert('Une erreur s\'est produite');
                            }
                        },
                        error: function (data) {
                            console.log(data);
                            alert('Une erreur s\'est produite');
                        }
                    });
                }else{
                    alert ("Attention:\nErreur de saisie dans votre adresse de messagerie.");
                }
            }else{
                alert('Veuillez remplir correctement les champs');
            }

        });
        $("#autofill_coordonate_sejour").click(function () {
            var num_card=$("#num_card").val();
            data='num_card='+num_card;
            jQuery.ajax({
                url: "<?php echo site_url('front/commercant/get_users_by_id_card');?>",
                dataType: 'text',
                type: 'POST',
                data: data,
                success: function (datas) {
                    if (datas != 'no'){
                        res=JSON.parse(datas);
                        console.log(res.IdUser);
                        $('#Nom_sejour').val(res.Nom);
                        $('#Adresse_sejour').val(res.Adresse);
                        $('#prenom_sejour').val(res.Prenom);
                        $('#code_postal_sejour').val(res.CodePostal);
                        $('#mail_sejour').val(res.Email);
                        $('#id_ville_sejour').val(res.IdVille);
                        $('#tel_sejour').val(res.Portable);
                        $('#num_card').val(num_card)
                    }
                },
                error: function (data) {
                    console.log(data);
                    alert('Une erreur s\'est produite');
                }
            });
        });

        $("#submit_res_sejour").click(function() {
            let Id_commercant="<?php echo $oInfoCommercant->IdCommercant; ?>";
            let id_client="<?php if (isset($client) AND $client !=null){echo $client->IdUser;}else{echo 0;} ?>";
            let Pays=$('#Pays_sejour').val();
            let Nom=$('#Nom_sejour').val();
            let Adresse=$('#Adresse_sejour').val();
            let code_postal=$('#code_postal_sejour').val();
            let mail_se=$('#mail_sejour').val();
            let prenom=$('#prenom_sejour').val();
            let id_ville=$('#id_ville_sejour').val();
            let tel=$('#tel_sejour').val();
            let date_debut_res=$('#date_debut_res_sejour').val();
            let date_fin_res=$('#date_fin_res_sejour').val();
            let nbre_adulte=$('#nbre_adulte_sejour').val();
            let nbre_enfant=$('#nbre_enfant_sejour').val();
            let num_card=$('#num_card').val();
            if (date_debut_res ===""){date_debut_res="0000-00-00";}
            if (date_fin_res ===""){date_fin_res="0000-00-00";}
            if (num_card===""){num_card=0;}
            let message_client=$('#message_client_sejour').val();
            let data="Id_commercant="+Id_commercant+"&id_client="+id_client+"&Pays="+Pays+"&Nom="+Nom+"&Adresse="+Adresse+"&code_postal="+code_postal+"&mail="+mail_se+"&prenom="+prenom+"&id_ville="+id_ville+"&tel="+tel+"&date_debut_res="+date_debut_res+"&date_fin_res="+date_fin_res+"&nbre_adulte="+nbre_adulte+"&nbre_enfant="+nbre_enfant+"&num_card="+num_card+"&message_client="+message_client;
            console.log(data);
            if (arobaceestbon(mail_se)===true){
                /*if (id_client ==0){
                    alert('Connectez-vous a un compte particulier pour pouvoir reserver');
                }else*/ if (Nom !="" && prenom!="" && tel!="" && message_client!="") {
                    jQuery.ajax({
                        url: "<?php echo site_url('front/commercant/submit_res_sejour');?>",
                        dataType: 'text',
                        type: 'POST',
                        data: data,
                        success: function (data) {
                            if (data=='ok'){
                                alert('Réservation réussi!')
                            }else if (data=='ko'){
                                alert('Une erreur s\'est produite');
                            }
                        },
                        error: function (data) {
                            console.log(data);
                            alert('Une erreur s\'est produite');
                        }
                    });
                }else{
                    alert('Veuillez remplir correctement les champs');
                }

            }else{
                alert ("Attention:\nErreur de saisie dans votre adresse de messagerie.");
            }

        });
    });

    function arobaceestbon(mail_se) {
        adresse = mail_se;
        if (adresse == "") {
            return false; }
        {
            if (adresse != "") {
                var exp = /^[a-z\d_\-]+(\.[\a-z\d\-]+)*@[a-z\d\-]+(\.[a-z\d]+)+$/;
                if (! exp.test(adresse) ) {
                    return false; }
                return true;
            }
        }
    }

    $("#IdAbonnementForm").fancybox({
        'autoScale' : false,
        'overlayOpacity'      : 0.8, // Set opacity to 0.8
        'overlayColor'        : "#000000", // Set color to Black
        'padding'         : 5,
        'width'         : 520,
        'height'        : 410,
        'transitionIn'      : 'elastic',
        'transitionOut'     : 'elastic'
    });
</script>