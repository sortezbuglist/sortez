<style>
    .input_basique{
        height:40px;
        border-radius: 5px;
        padding: 10px;
        border: 2px solid grey;
        width: 100%;
        min-height: 50px;
        font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;        

    }
    td.txt_id {
        font-weight: bold;
        font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;
    }
    #frm_login_mail>table {
        width: 85%;
    }    
    #frm_login_mail{
        padding-top:20px;
    }
    .banniere_fond {
        background-color: #2b7c74;
        /* margin-bottom:20px; */
    }
    .img_fond{
        position:relative;
        top:113px;        
    }
    .text_description{
        text-align: center; 
        padding: 125px 15px 60px 15px;
        font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;
    }
    .text_description h2{
        font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;
    }    
    .text_description h4{
        font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;
        
    }    
    @media screen and (max-width:900px) {
        #corps{
            /* width: 50%!important; */
            text-align:center;
        }
        /* .img_fond{
            width: 100%;
            height: 100%;
        } */
        .text_description{
        text-align: center; 
        padding: 125px 15px 10px 15px;
        font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;
        text-align: center;
        }
        /* .banniere_fond{
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            
        } */
        
    }
</style>
        <table id="corps" align="center" border="0" cellpadding="0" cellspacing="0" width="100%"  style="border: 1px solid #cccccc; border-collapse: collapse;">
            <tr>
                <td style="padding: 10px 0 30px 0;background-color:;" >
                
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="75%"
                          >
                           <tr class="banniere_fond">
                                <td style="text-align: center; padding: 40px 15px 40px 15px; font-family: Arial, sans-serif;">
                                    <img class="img_fond" src="<?php echo base_url("assets/img/basique_ordi.webp") ;?>">
                                </td>
                            </tr>
                            <tr>
                                <td class="text_description">
                                    <!-- <h2>Bonjour Monsieur; Madame</h2> -->
                                    <h2>Madame, Monsieur,</h2>
                                    <h4>Vous avez demandé de recevoir les identifiants de l’établissement suivant :</h4>
                                </td>
                            </tr>
                           <tr>
                                <td style="text-align: center; padding: 0px 15px 0px 15px; font-family: Arial, sans-serif;">
                                      <h2>Madame, Monsieur,</h2>
                                    <h4>Vous avez demandé de recevoir les identifiants de l’établissement suivant :</h4>
                                </td>
                            </tr>
                             <tr>
                                    <td class="txt_id">Nom : '.$login.'</td>                                    
                            </tr>                                 
                                
                            <tr>

                            <td class="txt_id">Adresse  : '.$mdp.'</td>

                            </tr>
                            <tr>

                            <td class="txt_id">Code postale  : '.$mdp.'</td>

                            </tr>
                            <tr>

                            <td class="txt_id">Ville  : '.$mdp.'</td>

                            </tr>
                            
                    </table>
                    <form id="frm_login_mail" name="frm_login_mail" method="post" action="https://www.sortez.org/">
                        <table align="center" cellspacing="20px;">
                            <tr>
                                    <td class="txt_id">Identifiant : </td>
                                    
                            </tr>
                                    
                                
                            <tr>

                            <td class="txt_id">Mots de passe : </td>

                            </tr>

                        </table>
                    </form>
                    <tr>
                        <td align="center"><h4 style="font-size: 15px; font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;">Il vous suffira de vous identifier en cliquant sur le lien suivant pour accéder à votre page administrative :</h4></td>
                         <td><a href="https://www.sortez.org/auth/login">login sortez</a></td>
                    </tr>
                    <tr>
                    <td align="center"><h4 style="font-size: 15px; font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;">Cordialement<br>L’équipe du Magazine Sortez</h4></td>
                    </tr>
                    

                    <tr>
                        <td align="center"><h2 style="font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;">PRIVICONCEPT SAS</h2></td>
                    </tr>
                    <tr>
                        <td align="center"><p style="font-style: italic;"> 
                            L'innovation au service de la vie locale</p>
                            <p>magazine-sortez.org<br>
                            vivresaville.fr - soutenonslecommercelocal.fr</p>
                            <p>427, Chemin de Vosgelade, Le Mas Raoum,  06140 Vence<br>
                                TÉL. 06 72 0 5 59 35<br>
                                Siret 820 043 693 00010<br>
                                Code NAF 6201Z</p><br>
                            <p style="color: #E80EAE; text-decoration: underline;margin-bottom:5px;">Conditions générales - Mentions légales</p>
                        </td>
                    </tr>
                        
                </td>
            </tr>
        </table>
