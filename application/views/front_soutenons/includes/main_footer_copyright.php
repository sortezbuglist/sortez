 <div class="row" id="footer_section_copyright" style="height: auto!important">
 <div style="background-color:#fff;">
    <div class="row text-center">
        <div class="col-lg-12">
            <img src="<?php echo base_url('/assets/soutenons/icon_phone.png');?>" style="margin-top: 20px;" ><br>
            <img src="<?php echo base_url('/assets/soutenons/num_phone.png');?>" style="margin-bottom: 14px;">
        </div>
        <div class="col-lg-6 offset-lg-3 pt-4">
            <div class="row">
                <div class="col-lg-6 offset-lg-2 pt-2 pl-5">
                    <div class="fs1shareButton" style="cursor: pointer">
                        <span class="fs1icon"></span>
                        <span class="fs1label">Partager</span>
                    </div>
                </div>
                <div class="col-lg-8 offset-lg-2 pt-2">
                    <img src="<?php echo base_url('/assets/soutenons/facebook.webp');?>" style="cursor: pointer" />
                </div>
<!--                <div class="col-lg-5 pt-2">-->
<!--                    <div class="row">-->
<!--                        <div class="col-lg-4">-->
<!--                            <img src="--><?php //echo base_url('/assets/soutenons/jaime.png') ?><!--" style="cursor: pointer" />-->
<!--                        </div>-->
<!--                        <div class="col-lg-8 p-0">-->
<!--                            <p style="text-align:justify;cursor:default">223 personnes aiment ça. Soyez le premier parmi vos amis.</p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
        </div>
        <div class="col-lg-12 pt-4 mb-5" style="margin-bottom: 100px!important;">
            <p class="Corps P-3 mb-0"><span class="futura_title">PRIVICONCEPT SAS</span></p>
            <p class="Corps P-3 mb-4"><span class="futura_sous_title">L'innovation au service de la vie locale</span></p>
            <p class="Corps P-3 mb-4"><span class="futura_title">magazine-sortez.org - vivresaville.fr - soutenonslecommercelocal.fr</span></p>
            <p class="Corps P-4 mb-0"><span class="futura_sous_title_2">535 Route des Lucioles, 06560 Sophia Antipolis - TÉL. 04 22 32 70 80</span></p>
            <p class="Corps P-6 mb-0"><span class="futura_sous_title_3">Siret 820 043 693 00010 - Code NAF 6201Z</span></p>
            <p class="Corps P-6 mb-0">
                <a href="#" class="futura_sous_title_3" style="text-decoration: underline; color: #E80EAE;font-size:15px;">Conditions générales - Mentions légales</a>
            </p>
        </div>
    </div>
</div>    
	</div>

 <!-- Return to Top -->
 <a href="javascript:void(0);" id="return-to-top"><i class="icon-chevron-up"></i></a>
 <!-- ICON NEEDS FONT AWESOME FOR CHEVRON UP ICON -->
 <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">


 <div class="container p-0 vsv_top_page_container_main d-sm-none d-md-none d-xl-none">
     <div class="vsv_top_page_btn_main container p-0 w-100">
         <div class="container p-0">
             <div class="row m-0">
                 <div class="col top_link_btn"><a href="javascript:void(0);" id="vsv_top_page_btn_main_btn"><img alt="" src="<?php echo base_url(); ?>application/resources/front/images/vivresaville_custom/burgerIcontop.png" class="img-fluid" width="30"></a></div>
                 <div class="col top_link_txt"><a href="javascript:void(0);" id="vsv_top_page_btn_main_txt">Haut de page</a></div>
             </div>
         </div>
     </div>
 </div>
<style>
    .fs1label{
        line-height: 21px;
        margin-left: 22px;
        padding: 3px 6px;
        color: #fff;
        font-size: 12px;
        text-shadow: 1px 1px 1px #304871;
        border-left: 1px solid #6176a3;
    }
    .fs1icon {
        width: 21px;
        height: 21px;
        background: url(https://static.parastorage.com/services/skins/2.1229.80/images/wysiwyg/core/themes/base/facebooklogo.png) 5px 3px no-repeat;
        border-right: 1px solid #425e85;
        position: absolute;
    }
    .fs1shareButton {
        position: relative!important;
        left: 48%;
        width: 80px;
        height: 22px;
        border-radius: 2px;
        border-width: 1px;
        border-style: solid;
        border-color: #9aabc6 #6f83ad #6176a3;
        background: url(https://static.parastorage.com/services/skins/2.1229.80/images/wysiwyg/core/themes/base/bg_fbshare.png) 0 0 repeat-x;
    }
    .futura_title{
        font-size: 25px;
        font-family: Futura-LT-Book,Sans-Serif!important;
        color: #000000;
        letter-spacing: 0.03em;
        text-align: center;
    }
    .futura_sous_title{
        font-size: 21px;
        font-family: Futura-LT-Book,Sans-Serif!important;
        color: #000000;
        letter-spacing: 0.03em;
        text-align: center;
    }
    .futura_sous_title_2{
        font-size: 18px;
        font-family: Futura-LT-Book,Sans-Serif!important;
        color: #000000;
        letter-spacing: 0.03em;
        text-align: center;
    }
    .futura_sous_title_3{
        font-size: 15px;
        font-family: Futura-LT-Book,Sans-Serif!important;
        color: #000000;
        letter-spacing: 0.03em;
        text-align: center;
    }
</style>

