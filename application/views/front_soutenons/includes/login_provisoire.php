<body id="body-login-form">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="75%">
        <tr>
            <td style="">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="75%">
                    <tr>
                        <td style="text-align: center; padding: 20px 15px 10px 15px; font-family: Arial, sans-serif;" class="banniere-login-basique">
                            <img src="<?= base_url('assets/soutenons/basique ordi.png') ?>">
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center; padding: 0px 15px 0px 15px; font-family: Arial, sans-serif;" class="text-presentation">
                            <h4>Monsieur, madame</h4>
                            <p>Vous trouverez ci-dessous vos identifiants pour accéder directement <br> à votre compte basique pour modifier les données de votre établissement !...</p>
                        </td>
                    </tr>
                    <!-- <tr>
                    <td align="center">
                        <img src="https://www.sortez.org/assets/soutenons/acces_logo.png">
                    </td>
                </tr> -->
                </table>
                <form id="frmConnexion" name="frmConnexion" accept-charset="utf-8" method="post" action="https://www.sortez.org/auth/login">
                    <table align="center">
                        <tr>
                            <td style="padding: 0px 0px 10px 0px;" class="txt-identifiant"><strong>Identifiant provisoire</strong></td>
                            <td style="padding: 0px 0px 10px 8%;" class="txt-password"><strong>Mot de passe provisoire</strong></td>
                        </tr>


                        <tr>
                            <td>
                                <input class="input-text-pswd  input-courriel-login-prov" type="text" name="identity" placeholder='' id="user_login">
                            </td>

                            <td align="right">
                                <input class="input-text-pswd input-pswd-login-prov" type="password" name="password" placeholder=''>
                            </td>
                        </tr>

                        <tr>
                            <td align="center" colspan="2" class="submit-login-basique">
                                <input class="btn-sub-login-prov" type="submit" name="submit" value="Accès direct au formulaire">
                            </td>
                            <td align="center" colspan="2">
                            </td>
                        </tr>
                </form>


    </table>
    <tr>
        <td align="center">
            <h2 style="font-family: futura-lt-w01-book,futura-lt-w05-book,sans-serif;margin-bottom: 0;" class="titre-privi-footer">PRIVICONCEPT SAS</h2>
        </td>
    </tr>
    <tr>
        <td align="center" class="">
            <p style="font-style: italic;"> L'innovation au service de la vie locale</p>
            <p>magazine-sortez.org<br>
                vivresaville.fr - soutenonslecommercelocal.fr</p>
            <p style="margin-bottom: 5px;">427, Chemin de Vosgelade, Le Mas Raoum, 06140 Vence<br>
                TÉL. 06 72 0 5 59 35<br>
                Siret 820 043 693 00010<br>
                Code NAF 6201Z</p>
            <p class="copyright-login-prov">Conditions générales - Mentions légales</p>
        </td>
    </tr>

    </td>
    </tr>
    </table>

    <style>
        @font-face {
            font-family: "futura";
            src: url("<?php echo base_url(); ?>assets/fonts/FuturaLT.eot") format("eot"),
                url("<?php echo base_url(); ?>assets/fonts/FuturaLT.woff") format("woff"),
                url("<?php echo base_url(); ?>assets/fonts/FuturaLT.ttf") format("truetype"),
                url("<?php echo base_url(); ?>assets/fonts/FuturaLT.svg") format("svg");
        }

        div,
        h1,
        h2,
        h3,
        h4,
        h5,
        p,
        span,
        strong,
        a,
        button {
            font-family: 'futura' !important;
        }

        #body-login-form {
            margin: 0;
        }

        .banniere-login-basique>img {
            transform: translateY(100px);
        }

        .text-presentation h4 {
            padding: 130px 0px 35px 0px;
            margin: 0;
            font-size: 1.3rem;
            font-weight: normal;
        }

        .text-presentation p {
            line-height: 25px;
            margin: 0;
            font-size: 1.2rem;
        }

        form#frmConnexion {
            padding-top: 4%;
            width: 85%;
            margin: auto;
        }

        .submit-login-basique {
            padding-top: 4%;
        }

        .btn-sub-login-prov {
            background: #2b7c74;
            border: none;
            padding: 12px;
            color: white;
            cursor: pointer;
            width: 57%;
            min-height: 50px;
            font-weight: 700;
            letter-spacing: 2px;
            font-size: 1.3rem;
        }

        .input-text-pswd {
            padding: 10px;
            border: 2px solid #2b7c74;
            width: 85%;
            min-height: 50px;
            font-family: 'futura';
        }

        #frmConnexion>table {
            width: 80%;
        }

        .copyright-login-prov {
            color: #E80EAE;
            text-decoration: underline;
            padding-bottom: 2%;
            margin: 0;
        }

        .btn-sub-login-prov:hover {
            background: #323232 !important;
        }

        .banniere-login-basique {
            background-color: #2b7c74;
            height: 300px;
        }

        h2.titre-privi-footer {
            padding-top: 4%;
        }



        /*Begin responsive tablette*/

        /*End responsive tablette*/

        /*Begin responsive desktop*/
        @media screen and (max-width:1300px) {
            .btn-sub-login-prov {
                font-size: 1rem;
            }
        }

        @media screen and (min-width:1401px) and (max-width:1600px) {
            .btn-sub-login-prov {
                width: 50%;
            }

            td.txt-identifiant {
                padding-left: 10% !important;
            }

            .input-text-pswd {
                width: 80%;
            }

            .input-courriel-login-prov {
                float: right;
            }

            .submit-login-basique {
                padding-top: 2%;
            }
        }

        @media screen and (min-width:1601px) and (max-width:1800px) {
            .btn-sub-login-prov {
                width: 50%;
            }

            .input-courriel-login-prov {
                float: right;
            }

            td.txt-identifiant {
                padding-left: 8% !important;
            }

            .submit-login-basique {
                padding-top: 2%;
            }

        }

        @media screen and (min-width:1801px) {
            .btn-sub-login-prov {
                width: 48%;
            }

            .input-courriel-login-prov {
                float: right;
            }

            td.txt-identifiant {
                padding-left: 8% !important;
            }

            .submit-login-basique {
                padding-top: 2%;
            }
        }

        /*End responsive desktop*/
    </style>
</body>