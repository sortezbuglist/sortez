<?php //$data["zTitle"] = 'Sortez Agenda | '.$oDetailAnnonce->texte_courte;?>
<?php //$data["zDescription"] = strip_tags($oDetailAnnonce->texte_courte);?>
<?php
//$thisss =& get_instance();
//$thisss->load->model("ion_auth_used_by_club");
//$ionauth_id = $thisss->ion_auth_used_by_club->get_ion_id_from_commercant_id($oDetailAnnonce->annonce_commercant_id) ;
//
//?>
<?php //$img_annonce = "application/resources/front/photoCommercant/imagesbank/".$ionauth_id."/".$oDetailAnnonce->photo1; ?>
<?php //$data["zMetaImage"] = $img_annonce; ?>
<?php //var_dump($oDetailAnnonce); ?>
<script type="text/javascript">
    function compte_a_rebours_<?php echo $oDetailAnnonce->annonce_id; ?>() {
        var compte_a_rebours_<?php echo $oDetailAnnonce->annonce_id; ?> = document.getElementById("compte_a_rebours_<?php echo $oDetailAnnonce->annonce_id; ?>");

        var date_actuelle = new Date();
        var date_evenement = new Date("<?php echo $oDetailAnnonce->annonce_date_fin; ?>");
        var total_secondes = (date_evenement - date_actuelle) / 1000;

        var prefixe = "";
        if (total_secondes < 0) {
            prefixe = "Exp "; // On modifie le préfixe si la différence est négatif

            total_secondes = Math.abs(total_secondes); // On ne garde que la valeur absolue

        }

        if (total_secondes > 0) {
            var jours = Math.floor(total_secondes / (60 * 60 * 24));
            var heures = Math.floor((total_secondes - (jours * 60 * 60 * 24)) / (60 * 60));
            minutes = Math.floor((total_secondes - ((jours * 60 * 60 * 24 + heures * 60 * 60))) / 60);
            secondes = Math.floor(total_secondes - ((jours * 60 * 60 * 24 + heures * 60 * 60 + minutes * 60)));

            var et = "et";
            var mot_jour = "jours";
            var mot_heure = "heures,";
            var mot_minute = "minutes,";
            var mot_seconde = "secondes";

            if (jours == 0) {
                jours = '';
                mot_jour = '';
            } else if (jours == 1) {
                mot_jour = "jour,";
            }

            if (heures == 0) {
                heures = '';
                mot_heure = '';
            } else if (heures == 1) {
                mot_heure = "heure,";
            }

            if (minutes == 0) {
                minutes = '';
                mot_minute = '';
            } else if (minutes == 1) {
                mot_minute = "minute,";
            }

            if (secondes == 0) {
                secondes = '';
                mot_seconde = '';
                et = '';
            } else if (secondes == 1) {
                mot_seconde = "seconde";
            }

            if (minutes == 0 && heures == 0 && jours == 0) {
                et = "";
            }

            //compte_a_rebours.innerHTML = prefixe + jours + ' ' + mot_jour + ' ' + heures + ' ' + mot_heure + ' ' + minutes + ' ' + mot_minute + ' ' + et + ' ' + secondes + ' ' + mot_seconde;
            compte_a_rebours_<?php echo $oDetailAnnonce->annonce_id; ?>.innerHTML = prefixe + jours + ' ' + mot_jour + ' ' + heures + ':' + minutes + ':' + secondes;
        } else {
            compte_a_rebours_<?php echo $oDetailAnnonce->annonce_id; ?>.innerHTML = 'Toujours disponible !';
            //alert(date_actuelle+ " "+date_evenement);
        }

        var actualisation = setTimeout("compte_a_rebours_<?php echo $oDetailAnnonce->annonce_id; ?>();", 1000);
    }
</script>
<style>
    .retour_menu{
        position: relative;
        font-family:Futura-LT-Book, Sans-Serif;
        font-weight:bold;
        color:#3453a2;
        text-decoration:none;
        font-size:15px;
        padding-left: 30px;
    }
    .retour_menu::before{
        content: "\f0a8";
        font-family: FontAwesome;
        font-style: normal;
        font-weight: normal;
        text-decoration: inherit;
        /*--adjust as necessary--*/
        color: #3453a2;
        font-size: 40px;
        padding-right: 0.5em;
        position: absolute;
        top: -20px;
        left: -15px;
    }
    .text_label21 {
        font-family: Futura-LT-Book, Sans-Serif;
        font-weight: bold;
        font-size: 15px;
        color: #3453a2;
        align-self: center;
        text-align: center;
        letter-spacing: 0.05em;
    }
    .text_head23{
        font-family: Futura-LT-Book, Sans-Serif;
        font-weight: bold;
        font-size: 17px;
        color: #ffffff;
        align-self: center;
        text-align: center;
        letter-spacing: 0.05em;
    }
    .text_head24{
        font-family: Futura-LT-Book, Sans-Serif;
        font-weight: bold;
        font-size: 25px;
        color: #ffffff;
        align-self: center;
        text-align: center;
        letter-spacing: 0.05em;
    }
    .description_details{
        position: relative;
        padding-left: 30px;
    }
    .description_details p{
        font-family: Futura-LT-Book, Sans-Serif;
        font-size: 15px;
        color: #000000;
    }
    /*.description_details p::before{*/
    /*    content: "\f111";*/
    /*    font-family: FontAwesome;*/
    /*    font-style: normal;*/
    /*    font-weight: normal;*/
    /*    text-decoration: inherit;*/
    /*    !*--adjust as necessary--*!*/
    /*    color: #000000;*/
    /*    font-size: 8px;*/
    /*    padding-right: 0.5em;*/
    /*    position: absolute;*/
    /*    top: 7px;*/
    /*    left: 10px;*/
    /*}*/
    .btn-warning{
        font-family: Futura-LT-Book, Sans-Serif;
        font-size: 15px;
        color: #3453a2;
        border-radius: 20px;
        font-weight: bold;
        background-image:linear-gradient(#ffffff,#ffc107,#ffffff)!important;
    }
</style>
<div class="container content_in_container">
    <div class="col-lg-12" id="content_text" style="margin: auto">
        <div class="col-lg-12 mb-3">
            <a class="retour_menu" href="<?php echo site_url($oInfoCommercant->nom_url."/annonces_commercants");?>" style="text-decoration: unset!important;" >Retour vers le menu général des annonces</a>
        </div>
        <?php
        $zEtat = "";
        if ($oDetailAnnonce->etat == 0) $zEtat = "Revente";
        else if ($oDetailAnnonce->etat == 1) $zEtat = "Neuf";
        else if ($oDetailAnnonce->etat == 2) $zEtat = "Service";
        ?>
        <p class="text_head pb-2"><span class="text_label21">Etat :</span> <?php echo $zEtat; ?></p>
        <p class="text_head pb-2">
            <span class="text_label21">Annonce N° :</span> <?php echo ajoutZeroPourString($oDetailAnnonce->annonce_id, 6) ?>
            <?php if (convertDateWithSlashes($oDetailAnnonce->annonce_date_debut) != "00/00/0000") { ?>
                du <?php echo convertDateWithSlashes($oDetailAnnonce->annonce_date_debut); ?>
            <?php } ?>
        </p>
        <p class="text_head pb-2"><span class="text_label21">Désignation : </span><?php echo $oDetailAnnonce->texte_courte; ?></p>
        <div class="text_head pb-2 description_details"><?php echo $oDetailAnnonce->texte_longue; ?></div>
        <div class="text_head pb-2">
                <span class="text_label21">Exp: </span>
                <span style="padding-top:5px;" id="compte_a_rebours_<?php echo $oDetailAnnonce->annonce_id; ?>"><noscript>Fin de l'évènement le 1er janvier 2021.</noscript></span>
                <script type="text/javascript">
                    compte_a_rebours_<?php echo $oDetailAnnonce->annonce_id; ?>();
                </script>
        </div>

<!--        --><?php //if (isset($oDetailAnnonce->module_paypal) && $oDetailAnnonce->module_paypal == "1") { ?>
<!--            <table width="100%" border="0" cellspacing="5" cellpadding="5" style="text-align:center; margin:20px 0;">-->
<!--                <tr>-->
<!--                    <td colspan="2"><img src="--><?php //echo GetImagePath("front/"); ?><!--/btn_new/wp6ac3e457_05_06.jpg" alt=""-->
<!--                                         name="pcrv_54gfd43" border="0" id="pcrv_54dfgh43"></td>-->
<!--                </tr>-->
<!--                <tr>-->
<!--                    <td style="padding: 15px">--><?php //echo $oDetailAnnonce->module_paypal_btnpaypal; ?><!--</td>-->
<!--                </tr>-->
<!--                <tr>-->
<!--                    <td>--><?php //echo $oDetailAnnonce->module_paypal_panierpaypal; ?><!--</td>-->
<!--                </tr>-->
<!--            </table>-->
<!--        --><?php //} ?>


    </div>

    <?php if (isset($oDetailAnnonce->retrait_etablissement) && $oDetailAnnonce->retrait_etablissement == "1") { ?>
        <div style="padding-top:15px;text-align: center"><img
                    src="<?php echo GetImagePath("front/"); ?>/btn_new/retrait_etablissement.png"
                    id="retrait_etablissement_img"/></div>
    <?php } ?>

    <?php if (isset($oDetailAnnonce->livraison_domicile) && $oDetailAnnonce->livraison_domicile == "1") { ?>
        <div style="padding-top:15px;text-align: center"><img
                    src="<?php echo GetImagePath("front/"); ?>/btn_new/livraison_domicile.png"
                    id="livraison_domicilet_img"/></div>
    <?php } ?>

    <div class="container content_in_container">
<!--        <div class="col-lg-12 text-center">-->
<!--            <img src="--><?php //echo base_url("/assets/images/infos.png") ?><!--">-->
<!--        </div>-->
        <div class="col-lg-12">
            <div class="row p-3 mt-5 back_barres">
                <div class="col-lg-6 text-left d-flex" style="align-self: center;">
                    <?php  if(isset($oDetailAnnonce->module_paypal) && $oDetailAnnonce->module_paypal != 0 && $oDetailAnnonce->module_paypal != "" && $oDetailAnnonce->module_paypal != null){ ?>
                        <?php
                            if(isset($oDetailAnnonce->module_paypal_btnpaypal) && $oDetailAnnonce->module_paypal_btnpaypal != " " && $oDetailAnnonce->module_paypal_btnpaypal != null){
                                print html_entity_decode($oDetailAnnonce->module_paypal_btnpaypal);
                            }
                            if(isset($oDetailAnnonce->module_paypal_panierpaypal) && $oDetailAnnonce->module_paypal_panierpaypal != " " && $oDetailAnnonce->module_paypal_panierpaypal != null){
                                print html_entity_decode($oDetailAnnonce->module_paypal_panierpaypal);
                            }
                        ?>
                    <?php } ?>
                </div>
                <div class="col-lg-6 text-right">
                    <?php if (isset($oDetailAnnonce->ancien_prix) && $oDetailAnnonce->ancien_prix != 0 && $oDetailAnnonce->ancien_prix != "0") { ?>

                        <span class="text_head23">Tarif: </span>

                        <span class="text_head24">
                        <?php echo $oDetailAnnonce->ancien_prix; ?>
                    </span>
                    <?php } ?>
                </div>
            </div>
        </div>



    </div>

</div>