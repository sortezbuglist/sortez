<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=utf-8" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url('application/views/sortez_soutenons/admin/css/style.css')?>" type="text/css" />
</head>
<body>
    <div class="container content_in_container">
        <div class="col-lg-12">
            <div class="row">
                <?php
                $thiss =& get_instance();
                $thiss->load->model('mdl_ville');
                $ville_data = $thiss->mdl_ville->getVilleById($oInfoCommercant->IdVille);
                ?>
                <div class="col-lg-4 pt-4 ">
                    <p id="nom_etablissement" class="form-control"> <?php if (isset($oInfoCommercant->NomSociete)) echo $oInfoCommercant->NomSociete; ?></p>
                </div>
                <div class="col-lg-8 pt-4 ">
                    <p id="adresse_coms" class="form-control"><?php if (isset($oInfoCommercant->adresse_localisation)) echo $oInfoCommercant->adresse_localisation; ?> </p>
                </div>
                <div class="col-lg-4 ">
                    <p id="code_postal_coms" class="form-control" ><?php if (isset($oInfoCommercant->CodePostal)) echo $oInfoCommercant->CodePostal; ?></p>
                </div>
                <div class="col-lg-4 ">
                    <p id="ville_coms" class="form-control"><?php if (isset($ville_data->ville_nom)) echo $ville_data->ville_nom; ?></p>
                </div>
                <div class="col-lg-4 ">
                    <p id="courriel_coms" class="form-control" ><?php if (isset($oInfoCommercant->Email)) echo $oInfoCommercant->Email; ?></p>
                </div>
                <div class="col-lg-4 ">
                    <p id="numero_telephone" class="form-control"><?php if (isset($oInfoCommercant->TelMobile)) echo $oInfoCommercant->TelMobile; ?></p>
                </div>
                <div class="col-lg-4 ">
                    <p id="siret" class="form-control"><?php if (isset($oInfoCommercant->Siret)) echo $oInfoCommercant->Siret; ?></p>
                </div>
                <div class="col-lg-4 ">
                    <p id="code_ape" class="form-control" ><?php if (isset($oInfoCommercant->code_ape)) echo $oInfoCommercant->code_ape; ?></p>
                </div>
                <div class="col-lg-4 ">
                    <p id="nom_responsable" class="form-control"><?php if (isset($oInfoCommercant->Nom)) echo $oInfoCommercant->Nom; ?></p>
                </div>
                <div class="col-lg-4 ">
                    <p id="prenom_responsable" class="form-control" ><?php if (isset($oInfoCommercant->Prenom)) echo $oInfoCommercant->Prenom; ?></p>
                </div>
                <div class="col-lg-4 ">
                    <p id="titre_responsable" class="form-control"><?php if (isset($oInfoCommercant->Responsabilite)) echo $oInfoCommercant->Responsabilite; ?></p>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <?php
            if(isset($commandes) && $commandes != null && $commandes != ""){
                echo $commandes->condition_content;
            }
            ?>
        </div>
    </div>
</body>
