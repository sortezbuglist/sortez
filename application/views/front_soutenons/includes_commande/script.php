<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.62/pdfmake.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.62/vfs_fonts.js" type="text/javascript"></script>
<script type="text/javascript">
    function doCapture() {
        //
        window.scrollTo(0, 0);
        // html2canvas(document.getElementById("recap_to_export")).then(function (canvas) {
        //
        //     // Get the image data as JPEG and 0.9 quality (0.0 - 1.0)
        //     console.log(canvas.toDataURL("image/jpeg", 0.9));
        // });
        html2canvas(document.querySelector("#infocliall")).then(canvas => {
            // document.body.appendChild(canvas)
            var data = canvas.toDataURL();
            // var docDefinition = {
            //     content: [
            //         {
            //         image: data,
            //         width: 500,
            //         }
            //     ]
            // };
            var send_data = {image: data, width: 500,style: 'para'}
            getvaluetype(send_data);
        });
    }
    function getvaluetype(send_data){
        html2canvas(document.querySelector("#type_vente")).then(canvas => {
            // document.body.appendChild(canvas)
            var data = canvas.toDataURL();
            // var docDefinition = {
            //     content: [
            //         {
            //         image: data,
            //         width: 500,
            //         }
            //     ]
            // };
            var send_data2 = {image: data, width: 500,}
            getvaluegli(send_data,send_data2);
        });
    }
    function getvaluegli(send_data,send_data2){
        html2canvas(document.querySelector("#content_gli")).then(canvas => {
            // document.body.appendChild(canvas)
            var data = canvas.toDataURL();
            // var docDefinition = {
            //     content: [
            //         {
            //         image: data,
            //         width: 500,
            //         }
            //     ]
            // };
            var send_data3 = {image: data, width: 500,}
            get_value_content_recap(send_data,send_data2,send_data3);
        });
    }
    function get_value_content_recap(send_data,send_data2,send_data3){
        var nom = $('#nom_client').val();
        var numero = $('#number_card').val();
        if(nom != "" && nom != null){
            if(numero!= "" && numero != null){
                var addname = 'commande '+nom+' num'+numero;
            }else{
                var addname = 'commande '+nom;
            }
                html2canvas(document.querySelector("#recap_to_export")).then(canvas => {
                    // document.body.appendChild(canvas)
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [
                            {
                                text: 'Voici votre recapitulatif de commande',
                                style: 'header',
                            },
                            send_data,
                            send_data2,
                            send_data3,
                            {
                                image: data,
                                width: 500,
                                style: 'para',
                            }
                        ],
                        styles: {
                            header: {
                                fontSize: 20,
                                bold: true,
                                alignment: 'center',
                                margin: [0, 9, 0, 9]
                            },
                            para: {
                                margin: [0, 9, 0, 9]
                            }
                        }
                    };
                    pdfMake.createPdf(docDefinition).download( addname + ".pdf");
                });
        }else{
            alert('veuillez remplir le formulaire de renseignement!');
        }
    }
    $(document).ready(function(){
        $( "#upload_file_custom" ).submit(function(event) {
            $("#commande_error").html('<div class="w-100 text-center"><img src="/application/resources/sortez/images/loading.gif" alt="loading"/></div>');
            event.preventDefault();
            $.ajax({
                url:"<?php echo site_url('soutenons/Commander/save_command_specific_file')?>",
                method:"POST",
                data:new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                success:function(data)
                {
                    console.log(data);
                    if(data =="ok"){
                        $(document).scrollTop( $("#commande_error").offset().top );
                        $("#commande_error").html('<div class="alert alert-success" role="alert">\n' +
                            '  Votre commande a été enregistré avec success !\n' +
                            '</div>');
                    }else{
                        $(document).scrollTop( $("#commande_error").offset().top );
                        $("#commande_error").html('<div class="alert alert-danger" role="alert">\n' +
                            '  Une erreur a été constatée, veuillez reverifier votre commande !\n' +
                            '</div>');
                    }
                }
            })
        });
        $( "#check_cheque_type" ).change(function() {
            var is_check = document.getElementById("check_cheque_type").checked;
            if(is_check == true){
                $("#type_paiement_custom").val("cheque");
                document.getElementById("check_bank_type").checked = false;
            }
        });
        $( "#check_bank_type" ).change(function() {
            var is_check = document.getElementById("check_bank_type").checked;
            if(is_check == true){
                $("#type_paiement_custom").val("Bancaire");
                document.getElementById("check_cheque_type").checked = false;
            }
        });
    })

    function import_command_file(){
        var id_cli = $("#id_client").val();
        if (id_cli != "0"){
            $("#command_file").click();
        }else{
            alert("vous devez preciser votre numero de carte ou en obtenir un !");
            location.href="#number_card";
        }
    }

    function  submit_custom_form() {
        if ($("#type_paiement_custom").val() == "0" || $("#type_livraison").val() == "Enlèvement ou livraison" || $("#jour_souhaite").val() == "" || $("#command_file").val() == "" ){
            alert("champ obligatoire non rempli !");
        }else{
            var type_bank = document.getElementById('check_bank_type');
            var type_cheque = document.getElementById('check_cheque_type');
            if (type_bank.cheked == false && type_cheque.checked == false){
                alert('Vous devez choisir un type de paiement');
                throw new Error("my error message");
            }
            $("#upload_file_custom").submit();
        }

    }


    function get_users_info() {
        var num = $("#number_card").val();
        var datas = "nom_card="+num;
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('soutenons/Commander/check_user'); ?>",
            data: datas ,
            success: function(data_saved) {
                if (data_saved != 'ko'){
                    //var parsed = JSON.parse(data_saved);
                    //console.log(parsed)
                    //$("#id_client").val(parsed.IdUser);
                    //$("#id_client2").val(parsed.IdUser);
                    //$("#nom_client").val(parsed.Nom);
                    //$("#prenom_client").val(parsed.Prenom);
                    //$("#code_postal_client").val(parsed.CodePostal);
                    //$("#adresse_client").val(parsed.Adresse);
                    //$("#ville_client").val(parsed.IdVille);
                    //if(parsed.Portable != null){
                    //    $("#telephone_client").val(parsed.Portable);
                    //}else{
                    //    $("#telephone_client").val(parsed.Telephone);
                    //}
                    //$("#date_naissance_client").val(parsed.DateNaissance);
                    //if (parsed.Email != null){
                    //    $("#mail_client").val(parsed.Email)
                    //}else{
                    //    $("#mail_client").val(parsed.Login)
                    //}
                    //$("#qr_content").html("<img src='<?php //echo base_url()?>//application/resources/front/images/cards/qrcode_"+parsed.virtual_card_img+"' class='img-fluid'/>")
                    //$("#demande_carte").hide();
                    location.reload();
                }else{
                    alert("Votre carte n'est pas encore valide!");
                }
            },
            error: function() {
                // alert('Erreur');
            }
        });
    }
    function subscribtion() {
        var nom_client = $("#nom_client").val();
        var prenom_client = $("#prenom_client").val();
        var code_postal_client = $("#code_postal_client").val();
        var adresse_client = $("#adresse_client").val();
        var ville_client = $("#ville_client").val();
        var telephone_client = $("#telephone_client").val();
        var date_naissance_client = $("#date_naissance_client").val();
        var mail_client = $("#mail_client").val()

        if ($("#nom_client").val() !=="" && $("#prenom_client").val() !=="" && $("#code_postal_client").val() !=="" && $("#adresse_client").val() !=="" && $("#ville_client").val() !=="" && $("#telephone_client").val() !== "" && $("#mail_client").val() !=""){

            var datasz = "nom_client="+nom_client+"&prenom_client="+prenom_client+"&code_postal_client="+code_postal_client+"&adresse_client="+adresse_client+"&ville_client="+ville_client+"&telephone_client="+telephone_client+"&date_naissance_client="+date_naissance_client+"&mail_client="+mail_client
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('soutenons/Commander/ajouter_compte_part'); ?>",
                data: datasz ,
                success: function(data_saved) {
                    $("#id_client").val(data_saved);
                },
                error: function() {
                    // alert('Erreur');
                }
            });
        }else{
            $("#txt_error_add").html("Les champs sont obligatoires");
            $("#txt_error_add").css("color","red");
        }
    }
    function send_command(){
        <?php if (isset($datacommande) AND $datacommande->is_activ_livr_paypal == '1'){ ?>
        var is_accept_paypal = document.getElementById("accept_paypal").checked;
        var is_pay = 1;
        <?php }else{ ?>
        var is_pay = 0;
        var is_accept_paypal = 0;
        <?php } ?>
        <?php //if ($datacommande->cgv_link != null || $datacommande->cgv_link_livraison != null || $datacommande->cgv_link_differe != null){ ?>
            var is_cond = document.getElementsByClassName('check_condss');
            //var accept_cond_true = '1';
            //console.log(is_cond);
            var accept_cond_true ='0';
            for (i=0;i<is_cond.length;i++){
                if (is_cond[i].checked == true){
                    accept_cond_true = '1';
                    var is_cond_exist = '1';
                }else {
                    var is_cond = "0";
        var is_cond_exist = "0";
                }
            }
                <?php
        // }
        // else
        //     { ?>
        
        <?php //} ?>
        // if (is_pay == '1' && is_accept_paypal == '0' && $("#activ_glissiere_livraison_value").val() ==="1" ){
        //     alert('Vous devez accepter les conditions ci-dessus !');
        //     throw new Error("my error message");
        // }
        if(is_cond_exist == '1' && accept_cond_true == 1){

            if ($("#id_client").val() !== "0"){

                var data_saved = $("#id_client").val();
                /// gli1
                var alll = [];
                var nbre_gli1 = document.getElementsByClassName('nbre_gliss1_qt');
                var id_gli1 = document.getElementsByClassName('id_gliss_1');

                var nbre_gli2 = document.getElementsByClassName('nbre_gliss2_qt');
                var id_gli2 = document.getElementsByClassName('id_gliss_2');

                var nbre_gli3 = document.getElementsByClassName('nbre_gliss3_qt');
                var id_gli3 = document.getElementsByClassName('id_gliss_3');

                var nbre_gli4 = document.getElementsByClassName('nbre_gliss4_qt');
                var id_gli4 = document.getElementsByClassName('id_gliss_4');

                var nbre_gli6 = document.getElementsByClassName('nbre_gliss6_qt');
                var id_gli6 = document.getElementsByClassName('id_gliss_6');

                var nbre_gli7 = document.getElementsByClassName('nbre_gliss7_qt');
                var id_gli7 = document.getElementsByClassName('id_gliss_7');


                if (nbre_gli1.length !== 0){
                    for (i=0;i < nbre_gli1.length;i++){
                        if (nbre_gli1[i].value > 0){
                            var to_append  = {id_produit:id_gli1[i].value, nbre:nbre_gli1[i].value,id_glissiere:1};
                            alll.push(to_append);
                        }
                    }
                }

                /// gli2
                if (nbre_gli2.length !== 0){
                    for (i=0;i < nbre_gli2.length;i++){
                        if (nbre_gli2[i].value > 0){
                            var to_append2  = {id_produit:id_gli2[i].value, nbre:nbre_gli2[i].value,id_glissiere:2};
                            alll.push(to_append2);
                        }
                    }
                    console.log(alll);
                }

                /// gli3
                if (nbre_gli3.length !== 0){
                    for (i=0;i < nbre_gli3.length;i++){
                        if (nbre_gli3[i].value > 0){
                            var to_append3  = {id_produit:id_gli3[i].value, nbre:nbre_gli3[i].value,id_glissiere:3};
                            alll.push(to_append3);
                        }
                    }
                    console.log(alll);
                }

                /// gli4
                if (nbre_gli4.length !== 0){
                    for (i=0;i < nbre_gli4.length;i++){
                        if (nbre_gli4[i].value > 0){
                            var to_append4  = {id_produit:id_gli4[i].value, nbre:nbre_gli4[i].value,id_glissiere:4};
                            alll.push(to_append4);
                        }
                    }
                    console.log(alll);
                }

                /// gli6
                if (nbre_gli6.length !== 0){
                    for (i=0;i < nbre_gli6.length;i++){
                        if (nbre_gli6[i].value > 0){
                            var to_append6  = {id_produit:id_gli6[i].value, nbre:nbre_gli6[i].value,id_glissiere:6};
                            alll.push(to_append6);
                        }
                    }
                    console.log(alll);
                }

                /// gli7
                if (nbre_gli7.length !== 0){
                    for (i=0;i < nbre_gli7.length;i++){
                        if (nbre_gli7[i].value > 0){
                            var to_append7  = {id_produit:id_gli7[i].value, nbre:nbre_gli7[i].value,id_glissiere:7};
                            alll.push(to_append7);
                        }
                    }
                    console.log(alll);
                }
                console.log(alll);

                //var def
                let type = "";
                let jour_enlev = "";
                let heure_emporter = "";

                if($("#activ_glissiere_emporter_value").val() =="0" && $("#activ_glissiere_differe_value").val() =="0" && $("#activ_glissiere_livraison_value").val() =="1" ){

                    type = "Livraison à Domicile";
                    jour_enlev = $("#jour_livraison").val();
                    heure_emporter = $("#heure_livraison").val();

                    if($('#jour_livraison').val() == ""){
                        $("#error_livraison_a_dom").html("Veuillez preciser la date de livraison souhaité");
                        $("#error_livraison_a_dom").css("color","red");
                        location.href = "#error_livraison_a_dom";
                        throw new Error("my error message");
                    }

                }else if($("#activ_glissiere_emporter_value").val() ==="1" && $("#activ_glissiere_differe_value").val() ==="0" && $("#activ_glissiere_livraison_value").val() ==="0"){
                    type = "Enlevement Chez le vendeur";
                    jour_enlev = $("#jour_emporter").val();
                    heure_emporter = $("#heure_enlevement").val();

                    if($('#jour_emporter').val() == ""){
                        $("#emporter_error").html("Veuillez preciser la date de visite");
                        $("#emporter_error").css("color","red");
                        location.href = "#emporter_error";
                        throw new Error("my error message");
                    }

                }else if($("#activ_glissiere_emporter_value").val() ==="0" && $("#activ_glissiere_differe_value").val() ==="1" && $("#activ_glissiere_livraison_value").val() ==="0"){
                    type = "Vente en ligne - paiement differe";
                    jour_enlev = "Vente en ligne - paiement differe";
                    heure_emporter = "Vente en ligne - paiement differe";

                }else if($("#activ_glissiere_emporter_value").val() ==="0" && $("#activ_glissiere_differe_value").val() ==="1" && $("#activ_glissiere_livraison_value").val() ==="0"){
                    type = "";
                    jour_enlev = $("#jour_livraison").val();
                    heure_emporter = $("#heure_livraison").val();
                    $("#choose_livr_error").html("Veuillez Choisir une type de livraison");
                    $("#choose_livr_error").css("color","red");
                    location.href = "#choose_livr_error";
                    throw new Error("my error message");
                }

                let type_payement = $("#type_paiment_cli").val();
                if (type_payement == "" || type_payement == undefined || type_payement == null){
                    alert("Veuillez Choisir une type de paiement");
                    throw new Error("my error message");
                }
                let type_facture_differe = "";
                if ($("#type_facture_differe").val() != ""){
                    type_facture_differe = $("#type_facture_differe").val();
                }else{
                    type_facture_differe = "Le client n'a pas precisé";
                }
                if (heure_emporter == "" || heure_emporter == undefined || heure_emporter ==0){
                    alert($("#heure_enlevement").val() + " + Veuillez preciser l'heure de visite !");
                    throw new Error("my error message");
                }
                var to_prevent = "&type="+type+"&jour_enlev="+jour_enlev+"&heure_emporter="+heure_emporter+"&type_payement="+type_payement+"&type_facture_differe="+type_facture_differe;
                if(alll.length !== 0){
                    $("#commande_error").html('<div class="w-100 text-center"><img src="/application/resources/sortez/images/loading.gif" alt="loading"/></div>');
                    let datas = "allprod="+JSON.stringify(alll)+"&idCom="+"<?php echo $infocom->IdCommercant; ?>"+"&idclient="+data_saved+"&comment="+$("#comment_comm").val()+to_prevent;
                    $.ajax({
                        type: "POST",
                        url: "<?php echo site_url('soutenons/Commander/valid_command'); ?>",
                        data: datas,
                        success: function(data) {
                            console.log(data);
                            if (data == "ok"){
                                $("#commande_error").html("");
                                $("#error_livraison_a_dom").html("");
                                $("#emporter_error").html("");
                                $("#choose_livr_error").html("");
                                $("#txt_error_add").html("");

                                $(document).scrollTop( $("#commande_error").offset().top );
                                $("#commande_error").html('<div class="alert alert-success" role="alert">\n' +
                                    '  Votre commande a été enregistré avec success !\n' +
                                    '</div>');

                            }else if(data == "sokok"){
                                $(document).scrollTop( $("#commande_error").offset().top );
                                $("#commande_error").html('<div class="alert alert-success" role="alert">\n' +
                                    '  Votre commande a été enregistré avec success !\n' +
                                    '</div>');
                            }
                        },
                        error: function() {
                            // alert('Erreur');
                        }
                    });

                }else{

                    $("#commande_error").html("Aucune Produit sélectionné");
                    $("#commande_error").css("color","red");

                }
            }else{

                alert("Veuillez vous souscrire d'abord");
                $("#txt_error_add").html("Veuillez vous souscrire d'abord, C'est gratuit !!");
                $("#txt_error_add").css("color","red");
                location.href = "#txt_error_add";

            }
        }
        else
            {
            alert('Veuillez accepté toutes les conditions et règlements avant de valider votre commande! ')
        }

    }

    function change_total_price1(idprix, idnbre) {
        var get = 0;
        var class_name_prix = document.getElementsByClassName('nbre_gliss1_pr');
        for(i=0; i<class_name_prix.length; i++){
            var qtt = document.getElementsByClassName('nbre_gliss1_qt')[i].value;
            get = get + (parseFloat(class_name_prix[i].value.replace('€ ' , '')) * qtt);
        }
        $("#gli1total").val(get+"€");
        $("#total_mobile_gli1").val(get+"€");
        $("#totalgli1").val(get);
        $("#subs_bottomgli1").val(parseFloat(get).toFixed(2)+"€");
        // alert(parseFloat($("#subs_bottomgli5").val()));
        $('#all_totalised').val(parseFloat($("#totalgli1").val())+parseFloat($("#totalgli2").val())+parseFloat($("#totalgli3").val())+parseFloat($("#totalgli4").val())+parseFloat($("#totalgli6").val())+parseFloat($("#totalgli7").val())+"€")
        if( parseFloat($("#all_totalised").val()) <= parseFloat(<?php echo $datacommande->price_to_reach; ?>)){
            $("#text_warn").html("Désolé ! votre montant d'achat est inférieur, vous ne pouvez pas en bénéficier !...");
            $("#is_graced").val(0);
            $("#is_bravo").removeClass("d-flex");
            $("#is_bravo").addClass("d-none");
        }else{
            $("#text_warn").html("Bravo ! votre montant d'achat est supérieur,vous pouvez en bénéficier !...");
            $("#is_graced").val(1);
            $("#is_bravo").removeClass("d-none");
            $("#is_bravo").addClass("d-flex");
            $("#rest_to_give").val(parseFloat($("#all_totalised").val()) - parseFloat(<?php echo $datacommande->price_to_win; ?>));
        }
        $("#rest_to_give").val(parseFloat($("#rest_to_give").val()).toFixed(2)+"€");
        $('#all_totalised').val(parseFloat($('#all_totalised').val()).toFixed(2)+"€");
    }

    function change_total_price2(idprix, idnbre) {

        var get = 0;
        var class_name_prix = document.getElementsByClassName('nbre_gliss2_pr');
        for(i=0; i<class_name_prix.length; i++){
            var qtt = document.getElementsByClassName('nbre_gliss2_qt')[i].value;
            get = get + (parseFloat(class_name_prix[i].value.replace('€ ' , '')) * qtt);
        }
        $("#gli2total").val(get+"€");
        $("#total_mobile_gli2").val(get+"€");
        $("#totalgli2").val(get);
        $("#subs_bottomgli2").val(parseFloat(get).toFixed(2)+"€");
        $('#all_totalised').val(parseFloat($("#totalgli1").val())+parseFloat($("#totalgli2").val())+parseFloat($("#totalgli3").val())+parseFloat($("#totalgli4").val())+parseFloat($("#totalgli6").val())+parseFloat($("#totalgli7").val())+"€")
        if( parseFloat($("#all_totalised").val()) <= parseFloat(<?php echo $datacommande->price_to_reach; ?>)){
            $("#text_warn").html("Désolé ! votre montant d'achat est inférieur, vous ne pouvez pas en bénéficier !...");
            $("#is_graced").val(0);
            $("#is_bravo").removeClass("d-flex");
            $("#is_bravo").addClass("d-none");
        }else{
            $("#text_warn").html("Bravo ! votre montant d'achat est supérieur,vous pouvez en bénéficier !...");
            $("#is_graced").val(1);
            $("#is_bravo").removeClass("d-none");
            $("#is_bravo").addClass("d-flex");
            $("#rest_to_give").val(parseFloat($("#all_totalised").val()) - parseFloat(<?php echo $datacommande->price_to_win; ?>));
        }
        $("#rest_to_give").val(parseFloat($("#rest_to_give").val()).toFixed(2)+"€");
        $('#all_totalised').val(parseFloat($('#all_totalised').val()).toFixed(2)+"€");
    }

    function change_total_price3(idprix, idnbre) {

        var get = 0;
        var class_name_prix = document.getElementsByClassName('nbre_gliss3_pr');
        for(i=0; i<class_name_prix.length; i++){
            var qtt = document.getElementsByClassName('nbre_gliss3_qt')[i].value;
            get = get + (parseFloat(class_name_prix[i].value.replace('€ ' , '')) * qtt);
        }
        $("#gli3total").val(get+"€");
        $("#total_mobile_gli3").val(get+"€");
        $("#totalgli3").val(get);
        $("#subs_bottomgli3").val(parseFloat(get).toFixed(2)+"€");
        $('#all_totalised').val(parseFloat($("#totalgli1").val())+parseFloat($("#totalgli2").val())+parseFloat($("#totalgli3").val())+parseFloat($("#totalgli4").val())+parseFloat($("#totalgli6").val())+parseFloat($("#totalgli7").val())+"€")
        if( parseFloat($("#all_totalised").val()) <= parseFloat(<?php echo $datacommande->price_to_reach; ?>)){
            $("#text_warn").html("Désolé ! votre montant d'achat est inférieur, vous ne pouvez pas en bénéficier !...");
            $("#is_graced").val(0);
            $("#is_bravo").removeClass("d-flex");
            $("#is_bravo").addClass("d-none");
        }else{
            $("#text_warn").html("Bravo ! votre montant d'achat est supérieur,vous pouvez en bénéficier !...");
            $("#is_graced").val(1);
            $("#is_bravo").removeClass("d-none");
            $("#is_bravo").addClass("d-flex");
            $("#rest_to_give").val(parseFloat($("#all_totalised").val()) - parseFloat(<?php echo $datacommande->price_to_win; ?>));
        }
        $("#rest_to_give").val(parseFloat($("#rest_to_give").val()).toFixed(2)+"€");
        $('#all_totalised').val(parseFloat($('#all_totalised').val()).toFixed(2)+"€");
    }

    function change_total_price4(idprix, idnbre) {

        var get = 0;
        var class_name_prix = document.getElementsByClassName('nbre_gliss4_pr');
        for(i=0; i<class_name_prix.length; i++){
            var qtt = document.getElementsByClassName('nbre_gliss4_qt')[i].value;
            get = get + (parseFloat(class_name_prix[i].value.replace('€ ' , '')) * qtt);
        }
        $("#gli4total").val(get+"€");
        $("#total_mobile_gli4").val(get+"€");
        $("#totalgli4").val(get);
        $("#subs_bottomgli4").val(parseFloat(get).toFixed(2)+"€");
        $('#all_totalised').val(parseFloat($("#totalgli1").val())+parseFloat($("#totalgli2").val())+parseFloat($("#totalgli3").val())+parseFloat($("#totalgli4").val())+parseFloat($("#totalgli6").val())+parseFloat($("#totalgli7").val())+"€")
        if( parseFloat($("#all_totalised").val()) <= parseFloat(<?php echo $datacommande->price_to_reach; ?>)){
            $("#text_warn").html("Désolé ! votre montant d'achat est inférieur, vous ne pouvez pas en bénéficier !...");
            $("#is_graced").val(0);
            $("#is_bravo").removeClass("d-flex");
            $("#is_bravo").addClass("d-none");
        }else{
            $("#text_warn").html("Bravo ! votre montant d'achat est supérieur,vous pouvez en bénéficier !...");
            $("#is_graced").val(1);
            $("#is_bravo").removeClass("d-none");
            $("#is_bravo").addClass("d-flex");
            $("#rest_to_give").val(parseFloat($("#all_totalised").val()) - parseFloat(<?php echo $datacommande->price_to_win; ?>));
        }
        $("#rest_to_give").val(parseFloat($("#rest_to_give").val()).toFixed(2)+"€");
        $('#all_totalised').val(parseFloat($('#all_totalised').val()).toFixed(2)+"€");
    }

    function change_total_price6(idprix, idnbre) {

        var get = 0;
        var class_name_prix = document.getElementsByClassName('nbre_gliss6_pr');
        for(i=0; i<class_name_prix.length; i++){
            var qtt = document.getElementsByClassName('nbre_gliss6_qt')[i].value;
            get = get + (parseFloat(class_name_prix[i].value.replace('€ ' , '')) * qtt);
        }
        $("#gli6total").val(get+"€");
        $("#total_mobile_gli6").val(get+"€");
        $("#totalgli6").val(get);
        $("#subs_bottomgli6").val(parseFloat(get).toFixed(2)+"€");
        $('#all_totalised').val(parseFloat($("#totalgli1").val())+parseFloat($("#totalgli2").val())+parseFloat($("#totalgli3").val())+parseFloat($("#totalgli4").val())+parseFloat($("#totalgli6").val())+parseFloat($("#totalgli7").val())+"€")
        if( parseFloat($("#all_totalised").val()) <= parseFloat(<?php echo $datacommande->price_to_reach; ?>)){
            $("#text_warn").html("Désolé ! votre montant d'achat est inférieur, vous ne pouvez pas en bénéficier !...");
            $("#is_graced").val(0);
            $("#is_bravo").removeClass("d-flex");
            $("#is_bravo").addClass("d-none");
        }else{
            $("#text_warn").html("Bravo ! votre montant d'achat est supérieur,vous pouvez en bénéficier !...");
            $("#is_graced").val(1);
            $("#is_bravo").removeClass("d-none");
            $("#is_bravo").addClass("d-flex");
            $("#rest_to_give").val(parseFloat($("#all_totalised").val()) - parseFloat(<?php echo $datacommande->price_to_win; ?>));
        }
        $("#rest_to_give").val(parseFloat($("#rest_to_give").val()).toFixed(2)+"€");
        $('#all_totalised').val(parseFloat($('#all_totalised').val()).toFixed(2)+"€");
    }

    function change_total_price7(idprix, idnbre) {

        var get = 0;
        var class_name_prix = document.getElementsByClassName('nbre_gliss7_pr');
        for(i=0; i<class_name_prix.length; i++){
            var qtt = document.getElementsByClassName('nbre_gliss7_qt')[i].value;
            get = get + (parseFloat(class_name_prix[i].value.replace('€ ' , '')) * qtt);
        }
        $("#gli7total").val(get+"€");
        $("#total_mobile_gli7").val(get+"€");
        $("#totalgli7").val(get);
        $("#subs_bottomgli7").val(parseFloat(get).toFixed(2)+"€");
        $('#all_totalised').val(parseFloat($("#totalgli1").val())+parseFloat($("#totalgli2").val())+parseFloat($("#totalgli3").val())+parseFloat($("#totalgli4").val())+parseFloat($("#totalgli6").val())+parseFloat($("#totalgli7").val())+"€")
        if( parseFloat($("#all_totalised").val()) <= parseFloat(<?php echo $datacommande->price_to_reach; ?>)){
            $("#text_warn").html("Désolé ! votre montant d'achat est inférieur, vous ne pouvez pas en bénéficier !...");
            $("#is_graced").val(0);
            $("#is_bravo").removeClass("d-flex");
            $("#is_bravo").addClass("d-none");
        }else{
            $("#text_warn").html("Bravo ! votre montant d'achat est supérieur,vous pouvez en bénéficier !...");
            $("#is_graced").val(1);
            $("#is_bravo").removeClass("d-none");
            $("#is_bravo").addClass("d-flex");
            $("#rest_to_give").val(parseFloat($("#all_totalised").val()) - parseFloat(<?php echo $datacommande->price_to_win; ?>));
        }
        $("#rest_to_give").val(parseFloat($("#rest_to_give").val()).toFixed(2)+"€");
        $('#all_totalised').val(parseFloat($('#all_totalised').val()).toFixed(2)+"€");
    }
</script>
<script type="text/javascript">
    $("#id_recap").change(function (){
        if($("#id_recap_value").val() == 0){
            $("#content_recap").removeClass('d-none');
            $("#content_recap").addClass('d-block');
            $("#id_recap_value").val('1');
        }
        else{
            $("#content_recap").removeClass('d-block');
            $("#content_recap").addClass('d-none');
            $("#id_recap_value").val('0');
        }
    });
    $("#is_activ_facture_differe").change(function(){
        if ($('#is_activ_facture_differe').is(":checked"))
        {
            $("#is_activ_facture_differe_hidden").val(1);
        }else{
            $("#is_activ_facture_differe_hidden").val(0);
        }
    });
    $("#is_activ_pro_forma_differe").change(function(){
        if ($('#is_activ_pro_forma_differe').is(":checked"))
        {
            $("#is_activ_pro_forma_differe_hidden").val(1);
        }else{
            $("#is_activ_pro_forma_differe_hidden").val(0);
        }
    });
    $("#is_activ_devis_differe").change(function(){
        if ($('#is_activ_devis_differe').is(":checked"))
        {
            $("#is_activ_devis_differe_hidden").val(1);
        }else{
            $("#is_activ_devis_differe_hidden").val(0);
        }
    });

    $("#type_norm_cheque_livr").click(function (){
        if(document.getElementById('type_norm_bank_livr') != null){
            document.getElementById('type_norm_bank_livr').checked = false;
        }
        if(document.getElementById('accept_paypal') != null){
            document.getElementById('accept_paypal').checked = false;
        }
        $("#type_paiment_cli").val("Chèque dûment rempli et signé");
    });
    $("#accept_paypal").click(function (){
        if(document.getElementById('type_norm_bank_livr') != null){
            document.getElementById('type_norm_bank_livr').checked = false;
        }
        if(document.getElementById('type_norm_cheque_livr') != null){
            document.getElementById('type_norm_cheque_livr').checked = false;
        }
        $("#type_paiment_cli").val("Par Paypal");
    });
    $("#type_norm_bank_livr").click(function (){
        if(document.getElementById('type_norm_cheque_livr') !=null ){
            document.getElementById('type_norm_cheque_livr').checked = false;
        }
        if(document.getElementById('accept_paypal') !=null ){
            document.getElementById('accept_paypal').checked = false;
        }
        $("#type_paiment_cli").val("Carte bancaire avec un terminal de paiement");
    });
    $("#is_activ_devis_differe").click(function (){
        $("#type_facture_differe").val('Devis');
        if(document.getElementById('is_activ_pro_forma_differe') != null){
            document.getElementById('is_activ_pro_forma_differe').checked = false;
        }
        if(document.getElementById('is_activ_facture_differe') != null){
            document.getElementById('is_activ_facture_differe').checked = false;
        }

        // document.getElementById('is_activ_Cheque_differe').checked = false;
        // document.getElementById('is_activ_Virement_differe').checked = false;
        // document.getElementById('is_activ_carte_differe').checked = false;

    });

    $("#is_activ_pro_forma_differe").click(function (){
        $("#type_facture_differe").val('Facture pro forma');
        if(document.getElementById('is_activ_devis_differe') != null){
            document.getElementById('is_activ_devis_differe').checked = false;
        }
        if(document.getElementById('is_activ_facture_differe') != null){
            document.getElementById('is_activ_facture_differe').checked = false;
        }
        // document.getElementById('is_activ_Cheque_differe').checked = false;
        // document.getElementById('is_activ_Virement_differe').checked = false;
        // document.getElementById('is_activ_carte_differe').checked = false;
    });

    $("#is_activ_facture_differe").click(function (){
        $("#type_facture_differe").val('Facture');
        if(document.getElementById('is_activ_devis_differe') != null){
            document.getElementById('is_activ_devis_differe').checked = false;
        }
        if(document.getElementById('is_activ_pro_forma_differe') != null){
            document.getElementById('is_activ_pro_forma_differe').checked = false;
        }
        // document.getElementById('is_activ_Cheque_differe').checked = false;
        // document.getElementById('is_activ_Virement_differe').checked = false;
        // document.getElementById('is_activ_carte_differe').checked = false;
    });

    $("#is_activ_Cheque_differe").click(function (){
        // document.getElementById('is_activ_devis_differe').checked = false;
        // document.getElementById('is_activ_pro_forma_differe').checked = false;
        // document.getElementById('is_activ_facture_differe').checked = false;
        if(document.getElementById('is_activ_Virement_differe') != null){
            document.getElementById('is_activ_Virement_differe').checked = false;
        }
        if(document.getElementById('is_activ_carte_differe') != null){
            document.getElementById('is_activ_carte_differe').checked = false;
        }
        $("#type_paiment_cli").val("Chèque");
    });

    $("#is_activ_Virement_differe").click(function (){
        // document.getElementById('is_activ_devis_differe').checked = false;
        // document.getElementById('is_activ_pro_forma_differe').checked = false;
        if(document.getElementById('is_activ_facture_differe') != null){
            document.getElementById('is_activ_facture_differe').checked = false;
        }
        if(document.getElementById('is_activ_Cheque_differe') != null){
            document.getElementById('is_activ_Cheque_differe').checked = false;
        }
        if(document.getElementById('is_activ_carte_differe') != null){
            document.getElementById('is_activ_carte_differe').checked = false;
        }


        $("#type_paiment_cli").val("Virement");
    });

    $("#is_activ_carte_differe").click(function (){
        // document.getElementById('is_activ_devis_differe').checked = false;
        // document.getElementById('is_activ_pro_forma_differe').checked = false;
        // document.getElementById('is_activ_facture_differe').checked = false;
        if(document.getElementById('is_activ_Cheque_differe') != null){
            document.getElementById('is_activ_Cheque_differe').checked = false;
        }
        if(document.getElementById('is_activ_Virement_differe') != null){
            document.getElementById('is_activ_Virement_differe').checked = false;
        }

        $("#type_paiment_cli").val("Carte bancaire");
    });

    $("#type_norm_cheque").click(function (){
        if(document.getElementById('type_norm_bank') != null) {
            document.getElementById('type_norm_bank').checked = false;
        }
        if(document.getElementById('type_norm_espace') != null) {
            document.getElementById('type_norm_espace').checked = false;
        }
        $("#type_paiment_cli").val("Chèque");
    });
    $("#type_norm_bank").click(function (){
        if(document.getElementById('type_norm_cheque') != null){
            document.getElementById('type_norm_cheque').checked = false;
        }
        if(document.getElementById('type_norm_espace') != null) {
            document.getElementById('type_norm_espace').checked = false;
        }
        $("#type_paiment_cli").val("Carte bancaire");
    });
    $("#type_norm_espace").click(function (){
        if(document.getElementById('type_norm_cheque') != null){
            document.getElementById('type_norm_cheque').checked = false;
        }
        if(document.getElementById('type_norm_bank') != null) {
            document.getElementById('type_norm_bank').checked = false;
        }
        $("#type_paiment_cli").val("Espèces");
    });
    // $("#activ_glissiere_emporter").change(function (){
    //     if($("#value_emp_activ").val() == 0){
    //         $("#emporter_content").removeClass('d-none');
    //         $("#emporter_content").addClass('d-block');
    //         switch_emporter();
    //     }
    //     else{
    //         $("#emporter_content").removeClass('d-block');
    //         $("#emporter_content").addClass('d-none');
    //         switch_emporter();
    //     }
    // })
    // $("#activ_glissiere_livraison").change(function (){
    //     if($("#value_livr_activ").val() == 0){
    //         $("#livraison_content").removeClass('d-none');
    //         $("#livraison_content").addClass('d-block');
    //         switch_livraison();
    //     }
    //     else{
    //         $("#livraison_content").removeClass('d-block');
    //         $("#livraison_content").addClass('d-none');
    //         switch_livraison();
    //     }
    // })
    //
    // $("#activ_glissiere_differe").change(function (){
    //     if($("#value_differe_activ").val() == 0){
    //         $("#differe_content").removeClass('d-none');
    //         $("#differe_content").addClass('d-block');
    //         switch_differe();
    //     }
    //     else{
    //         $("#livraison_content").removeClass('d-block');
    //         $("#livraison_content").addClass('d-none');
    //         switch_differe();
    //     }
    // })

    function check_show_categ_partner(){

    }
</script>