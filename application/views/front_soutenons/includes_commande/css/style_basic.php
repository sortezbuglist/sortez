<style>
       @font-face {
              font-family: "futura";
              src: url("<?php echo base_url(); ?>assets/fonts/FuturaLT.eot") format("eot"),
                     url("<?php echo base_url(); ?>assets/fonts/FuturaLT.woff") format("woff"),
                     url("<?php echo base_url(); ?>assets/fonts/FuturaLT.ttf") format("truetype"),
                     url("<?php echo base_url(); ?>assets/fonts/FuturaLT.svg") format("svg");
       }

       div,
       h1,
       h2,
       h3,
       h4,
       h5,
       p,
       span,
       a,
       button {
              font-family: 'futura' !important;
       }
</style>
<?php echo '<style>

.first_div_menu_menu {
        padding-top: 15px;
        padding-bottom: 15px;
        background: #2b7c74 !important;
        width: 160px!important;
        height: 160px!important;
    }
    #bg_img_left_presentation , #bg_img_right_presentation{

        height: 400px;
        overflow: hidden; 
        background-color:#2b7c74;
 }
 .row.back_menu{
        
        background-color:#2b7c74 !important;
     
 }
 .row.front_menu{
        justify-content: center;
 }
  .btn_menu:hover{
         background: #2b7c74!important;
     }
 #google_translate_element:hover{
         background: #2b7c74!important;
     }
 
 #google_translate_element , .btn_menu{
         background: #2b7c74 !important;
  }
  /*begin Retouche page basique*/
       .row.front_menu {
              max-width: 100%;
       }
       body > div.container.content_in_container > div:nth-child(8) > h3 > span > span > span {
              padding-bottom: 2%;
              display: block;
       }
       .btngreen:hover {
              text-decoration: none;
              color: black;
              border: 1px solid black!important;
       }
       body > div.container.content_in_container > div:nth-child(9) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > p:nth-child(2) {
              padding-top: 10%;
       }
       body > div.container.content_in_container > div:nth-child(9) > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > p:nth-child(2) {
              padding-top: 10%;
       }
       body > div.container.content_in_container > div:nth-child(9) > div:nth-child(3) > div:nth-child(2) > div:nth-child(2) > p:nth-child(2) {
              padding-top: 10%;
       }
       body > div.container.content_in_container > div:nth-child(9) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > h4 {
              margin-top: 1%;
       }
       body > div.container.content_in_container > div:nth-child(9) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > h4 > span {
              font-size: 17px!important;
       }
       body > div.container.content_in_container > div:nth-child(9) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > h4 {
              margin-top: 1%;
       }
       body > div.container.content_in_container > div:nth-child(9) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > h4 > span {
              font-size: 17px!important;
       }
       body > div.container.content_in_container > div:nth-child(9) > div:nth-child(3) > div:nth-child(2) > div:nth-child(1) > h4 {
              margin-top: 1%;
       }
       body > div.container.content_in_container > div:nth-child(9) > div:nth-child(3) > div:nth-child(2) > div:nth-child(1) > h4 > span {
              font-size: 17px!important;
       }
       #comp-kyinsu3d > p:nth-child(2) {
              display: none;
       }
       #comp-kyinsu3d > p:nth-child(4) {
              display: none;
       }
       #comp-kyinsu3d > p:nth-child(1) {
              padding-bottom: 25px;
       }
       #comp-kyinsu3d > p > span{
              color: #6e6666;
       }
       #comp-kyinsu3d > p:nth-child(3) {
              padding-bottom: 1.5%;
       }
       #comp-kyinsu3d > p:nth-child(6) {
              padding-top: 2%;
       }
       ol.font_7.color_14 {
              color: #6e6666;
              padding-left: 3%;
              margin-bottom: 0;
       }
       ol.font_7.color_14 li p {
              color: #6e6666 !important;
       }
       .img_head {
              margin-top: 1% !important;
       }
       body > div.container.content_in_container > div:nth-child(8) > h4 {
              line-height: 2rem;
              margin-bottom: 0;
       }
       #presentation > div > div.col-12.p-4.pt-2 {
              max-height: 174px!important;
       }
       body > div.pt-4 {
              display: none;
       }
       ul#rand_vignette_horizontal-list {
              margin-bottom: 0!important;
              padding-bottom: 0!important;
       }
       .slider_t_container {
              padding-top: 0!important;
       }
       body > div:nth-child(50) > div.row.d-none.d-lg-block > div > div > div.col-lg-2.offset-lg-1.col-sm-12.pt-1 > a > button {
              width: 145px;
              height: 40px;
              margin-left: 21%;
              font-size: 14px;
       }
       button#google_translate_element {
              width: 145px;
              height: 40px;
              margin-left: 0%;
              font-size: 14px!important;
       }
       #IdRecommandationPartnerForm_main_menu > button {
              margin-left: -21%;
              width: 145px;
              height: 40px;
              font-size: 14px!important;
       }
       body > div:nth-child(50) > div.row.d-none.d-lg-block > div > div > div:nth-child(4) > a > button {
              width: 145px;
              height: 40px;
              margin-left: -44%;
              font-size: 14px!important;
       }
       body > div:nth-child(50) > div.row.d-none.d-lg-block > div > div > div:nth-child(5) > a > button {
              margin-left: -68%;
              width: 145px;
              height: 40px;
              font-size: 14px!important;
       }
       #google_translate_element:hover {
              background: #323232 !important;
       }
       #menu-partner .active{
              background-color: #E80EAE!important;
              z-index:999;
       }
       .btn{
              z-index: 1;
       }
       #menu-partner .btn_menu {
              border: none !important;
       }
       #menu-partner .btn_menu:hover {
              border: none!important;
       }
       .second_div_men_menu{
              width: 100px!important;
              height: 100px!important;
              border-radius: 50%!important;
       }
       #rand_vignette_horizontal-list > li:nth-child(3) > div > div {
              background: rgb(141,209,202)!important;
       }
       #rand_vignette_horizontal-list > li:nth-child(4) > div > div {
              background: rgb(141,209,202)!important;
       }
       .menu_menu_cont {
              max-width: 160px!important;
       }
       ul#rand_vignette_horizontal-list li {
              max-width: 160px!important;
       }

       .titre_prest {
              font-size: 50px!important;
       }
       #bg_img_left_presentation, #bg_img_right_presentation {
              min-height: 500px;
       }
       .row.back_menu {
              min-height: 500px!important;
       }
       div#address_vill, #code_post {
              font-size: 17px;
       }
       .text_label_content,.text_label_content > .a_link {
              font-size: 17px!important;
       }
       #three-blocks{
              justify-content: center;
       }
       #three-blocks .number{
              font-family: "Arial" !important;
       }
       #three-blocks .title-bloc{
              display: block;
              text-align: center
       }
       .addthis_inline_share_toolbox {
              text-align: center;
       }
       @media screen and (max-width:991px){
              #bloc-banner-basic{
                     display:block
              }
              div#menu-partner {
                     display: flex;
              }
       }
       @media screen and (max-width:1024px){
              #menu-partner .btn_menu {
                     margin-left: 1%;
              }
       }
       #comp-kzv7arrh {
              margin: 45px 0 33px 0!important;
       }
       #presentation > div > div.col-12.p-4.pt-2 {
              height: auto!important;
       }

       /*Boutton partner*/
       div#btn-lg {
              position: relative;
              left: 14%;
       }
       div#btn-rec {
              position: relative;
              left: 14%;
       }
       div#btn-nv-rech {
              position: relative;
              left: -4%;
       }
       div#btn-fav {
              position: relative;
              left: 2%;
       }
       div#btn-mcompte {
              position: relative;
              left: 2%;
       }


       /*Boutton partenaire*/
       @media screen and (min-width:769px) and (max-width:1170px){
              ul#rand_vignette_horizontal-list li {
                     width: 20%!important;
              }
       }
       /*Begin Responsive mobile*/
       @media screen and (max-width:768px){
              .titre_prest {
                     font-size: 30px!important;
              }
              #presentation > div > div.col-12.p-4.pt-2 {
                     max-height: 300px!important;
               }
              .img {
                     display: block;
                     margin: auto;
                     width: 60%;
              }
              .img_head1 {
                     display: block;
                     margin: auto;
                     width: 50%;
              }
              .text-with-women-img{
                     padding-top: 8%;
              }
              .mobile_img_square_menu {
                     width: 100%;
                     margin: 0;
              }
              div#three-blocks {
                     display: block!important;
                     margin-top: 10%!important;
              }
              .blocs{
                     padding: 50px!important;
                     margin-top: -56px!important;
              }
              .bloc-parent2{
                     margin-top: 10%
              }
              .bloc-paren3{
                     margin-top: 10%
              }
              .btngreen{
                     margin-top: 6%!important;
                     height: 50px!important;
                     width: 50%!important;
              }
              #btn-modele{
                     margin-top: 2%!important;
              }
              #three-blocks .title-bloc {
                     font-size: 30px!important;
              }

              ul#rand_vignette_horizontal-list li {
                     width: 22%!important;
                     margin-top: 1%;
              }
       }
       @media screen and (max-width:450px){
              #img-three-blocs{
                     width:100%;
              }
              .bloc-text-in-three-blocs {
                     margin-top: 0%!important;
              }
       }
       @media screen and (min-width:390px) and (max-width:500px){
              .btngreen {
                     width: 70%!important;
              }
       }
       @media screen and (max-width:389px){
              .btngreen {
                     font-size: 14px!important;
                     width: 90%!important;
              }
       }
       /*Begin Responsive mobile*/
       @media screen and (min-width:730px) and (max-width:1070px){
              .bloc-parent .blocs {
                     margin-top: -56px!important;
              }
       }
       @media screen and (min-width:769px) and (max-width:1070px){
              div#three-blocks {
                     display: block!important;
                     margin-top: 10%!important;
              }
              .blocs{
                     padding: 50px!important;
              }
              .bloc1{
                     margin-top: -8%!important;
              }
              .bloc2 {
                     margin-top: -8%!important;
              }
              .bloc3 {
                     margin-top: -8%!important;
              }
              .bloc-parent2{
                     margin-top: 10%
              }
              .bloc-paren3{
                     margin-top: 10%
              }
              .btngreen{
                     margin-top: 6%!important;
                     height: 50px!important;
                     width: 50%!important;
              }
              #btn-modele{
                     margin-top: 2%!important;
              }
              #three-blocks .title-bloc {
                     font-size: 30px!important;
              }
       }

       @media screen and (min-width:769px) and (max-width:1200px){
              .img {
                     display: block;
                     margin: auto;
                     width: 60%;
              }
              .img_head1 {
                     display: block;
                     margin: auto;
                     width: 35%;
              }
              .text-with-women-img{
                     padding-top: 8%;
              }
       }

       /*Menu partenaire respo desk*/
       @media screen and (max-width:1024px){
              div#btn-nv-rech {
                     position: relative;
                     left: 3%!important;
              }
       }
       @media screen and (min-width:1025px) and (max-width:1070px){
              div#btn-nv-rech {
                     position: relative;
                     left: -1%!important;
              }
       }
       @media screen and (max-width:1200px){
              div#btn-lg {
                     position: relative;
                     left: 10%;
              }
              div#btn-rec {
                     position: relative;
                     left: 11%;
              }
              div#btn-nv-rech {
                     position: relative;
                     left: -5%;
              }
              div#btn-fav {
                     position: relative;
                     left: 3%;
              }
              div#btn-mcompte {
                     position: relative;
                     left: 5%;
              }
       }
       @media screen and (min-width:1400px) and (max-width:1700px){
              div#btn-lg {
                     position: relative;
                     left: 14%;
              }
              div#btn-rec {
                     position: relative;
                     left: 14%;
              }
              div#btn-nv-rech {
                     position: relative;
                     left: -4%;
              }
              div#btn-fav {
                     position: relative;
                     left: 2%;
              }
              div#btn-mcompte {
                     position: relative;
                     left: 2%;
              }
       }
       @media screen and (min-width:1701px) and (max-width:1800px){
              div#btn-lg {
                     position: relative;
                     left: 20%;
              }
              div#btn-rec {
                     position: relative;
                     left: 18%;
              }
              div#btn-nv-rech {
                     position: relative;
                     left: -2%;
              }
              div#btn-fav {
                     position: relative;
                     left: 2%;
              }
              div#btn-mcompte {
                     position: relative;
                     left: 0%;
              }
       }
       @media screen and (min-width:1801px){
              div#btn-lg {
                     position: relative;
                     left: 22%;
              }
              div#btn-rec {
                     position: relative;
                     left: 19%;
              }
              div#btn-nv-rech {
                     position: relative;
                     left: -1%;
              }
              div#btn-fav {
                     position: relative;
                     left: 3%;
              }
              div#btn-mcompte {
                     position: relative;
                     left: 0%;
              }
       }
       @media screen and (min-width:1025px) and (max-width:1070px){
              body > div:nth-child(50) > div.row.d-none.d-lg-block > div > div > div.col-lg-2.offset-lg-1.col-sm-12.pt-1 > a > button {
                     margin-left: 9%;
              }
              #IdRecommandationPartnerForm_main_menu > button {
                     margin-left: -10%;
              }
              body > div:nth-child(50) > div.row.d-none.d-lg-block > div > div > div:nth-child(4) > a > button {
                     margin-left: -20%;
              }
              body > div:nth-child(50) > div.row.d-none.d-lg-block > div > div > div:nth-child(5) > a > button {
                     margin-left: -32%;
              }
       }

  /*end Retouche page basique*/
  

    </style>'
?>