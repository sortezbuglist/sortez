<style>
    @font-face {
        font-family: "futura";
        src: url("<?php echo base_url(); ?>assets/fonts/FuturaLT.eot") format("eot"),
            url("<?php echo base_url(); ?>assets/fonts/FuturaLT.woff") format("woff"),
            url("<?php echo base_url(); ?>assets/fonts/FuturaLT.ttf") format("truetype"),
            url("<?php echo base_url(); ?>assets/fonts/FuturaLT.svg") format("svg");
    }

    div,
    h1,
    h2,
    h3,
    h4,
    h5,
    p,
    span,
    a,
    button {
        font-family: 'futura' !important;
    }
</style>
<?php echo '<style>

.first_div_menu_menu {
        padding-top: 30px;
        padding-bottom: 15px;
        background: #3453a2 ;
        width: 190px;
        height: 100%;
        height: 189.6px !important;
}
#btn_btn_menu_carree{
    width:145px !important;
    height:40px !important;
    font-family: Futura-Lt-book,sans-serif;
    font-size: 14px!important;
    color: #ffffff!important;    
}
#rand_vignette_menu-outer {
    height: auto;
}

.rand_vignette_table {
    /* display: table;    Allow the centering to work 
    margin: 0 auto;*/
}

ul#rand_vignette_horizontal-list {
    min-width: 100%;
    list-style: none;
    padding: 15px 20px;
    /* text-align: center; */
    display:flex;
    flex-direction:row;
    flex-wrap:wrap;
    justify-content: center;
    align-items:center;
}

ul#rand_vignette_horizontal-list li {
    display: inline-block;
    /* max-width: 190px; */
    vertical-align: top;
    /* width: 14.2%; */
    /* margin-top: 4px; */
    /* width: 182px; */
    width: 161px;
    margin-top:5px;
    margin-right:5px;


}

ul#rand_vignette_horizontal-list li .direct_btn_image {
    width: 100%;
}

    .second_div_men_menu {
        /* width: 108px;
        height: 107px; */
        width: 100px;
        height: 100px;        
        overflow: hidden;
        border-radius: 70px;
        margin: auto;
        margin-bottom: 15px;
    }

    .rounded_default {
        height: 100%;
    }

    .first_div_menu_menu {
        padding-top: 30px;
        padding-bottom: 15px;
        background: #3453a2 !important;
        width: 190px;
        height: 100%;
        height: 189.6px !important;

    }
    .first_div_menu_menu1 {        
        padding-top:10px !important;
        padding-bottom: 5px !important;
        background: #3453a2 !important;
        width:160px;
        height: 160px;
        /* width: 180px;         */
        /* width:120px; */
    }    

    .txt_menu_menu {
        color: #FFFFFF;
        font-family: futura-lt-w01-book, sans-serif;
        /* font-size: 13px; */
        font-size:14px;
        
        /* letter-spacing: 0.08em; */
        /* padding-top: 15px; */
    }

    .menu_tope {
        /*width: 19.5%;*/
        /*margin-left: 14px;*/
    }

    .menu_tope_first {
        /*width: 19.5%;*/
        /*margin-left: 10px;*/
    }

    .pointer_cursor:hover {
        cursor: pointer;
    }

    .menu_menu_cont {
        width: 190px;
        display: inline-block;
        cursor: pointer;
    }
    </style>'
?>