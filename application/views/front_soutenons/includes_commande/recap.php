<div id="recap_to_export">
<?php if (isset($data_gli1) && !empty($data_gli1) && isset($title_gli1->is_activ_glissiere) && $title_gli1->is_activ_glissiere != 0) { ?>
<div class="row row glissiere_tab marge_gli_1 pt-4 pb-4 title_gli_sm_height">
    <div class="col-lg-2 offset-lg-2 col-sm-12">
    </div>
    <div id="id_recap" class="col-lg-4 titre_gli_up title_gli text-sm-left text-md-center pt-3 pt-md-0">RECAPITULATIF</div>
    <div class="col-lg-4"></div>
</div>
<div class="container pt-2 d-block m-auto content_in_container" id="content_recap">
    <div class="row" >
        <div class="col-lg-12 mt-4 col-sm-12">
            <div class="m-0">
                <input type="hidden" id="totalgli1" value="0">
                <input value="0" id="totalgli2" type="hidden" />
                <input value="0" id="totalgli3" type="hidden" />
                <input value="0" id="totalgli4" type="hidden" />
                <input value="0" id="totalgli6" type="hidden" />
                <input value="0" id="totalgli7" type="hidden" />

                <input value="0" id="is_activ_facture_differe_hidden" type="hidden" />
                <input value="0" id="is_activ_pro_forma_differe_hidden" type="hidden" />
                <input value="0" id="is_activ_devis_differe_hidden" type="hidden" />

                <input value="0" id="is_activ_Virement_differe_hidden" type="hidden" />
                <input value="0" id="is_activ_carte_differe_hidden" type="hidden" />
                <input value="0" id="is_activ_Cheque_differe_hidden" type="hidden" />

                <input value="" id="type_paiment_cli" type="hidden" />


                <input type="hidden" value="" id="type_facture_differe">


                    <div class="row gli_num_1 recap_commad_item_line" style="<?php if (isset($title_gli1->is_activ_glissiere) AND ($title_gli1->is_activ_glissiere != 0)){echo "display:flex;";}else{ echo "display:none;";} ?>">
                        <div class="col-9" id="gli_1_recap_container">
                            <div class="recap_gli_title gli_1_title">
                                <?php if (isset($title_gli1->titre_glissiere) and $title_gli1->titre_glissiere != null) echo $title_gli1->titre_glissiere;?>
                            </div>
                            <div class="row font-weight-bold">
                                <div class="col-6">Produit</div>
                                <div class="col-3">Prix</div>
                                <div class="col-3">Nombre</div>
                            </div>
                            <?php for ($iii = 0; $iii <= 30; $iii++) { ?>
                                <div id="gli_1_recap_content_<?= $iii;?>"></div>
                            <?php } ?>
                        </div>
                        <div class="col-3">
                            <input value="0€" disabled="disabled" id="subs_bottomgli1" class="w-100 textarea_style_input text_label2 text-center p-2 border-0" type="text" />
                        </div>
                    </div>

                    <div class="row gli_num_2 recap_commad_item_line " style="<?php if (isset($title_gli2->is_activ_glissiere) AND ($title_gli2->is_activ_glissiere != 0)){echo "display:flex;";}else{ echo "display:none;";} ?>">
                        <div class="col-9" id="gli_2_recap_container">
                            <div class="recap_gli_title gli_2_title">
                                <?php if (isset($title_gli2->titre_glissiere) and $title_gli2->titre_glissiere != null) echo $title_gli2->titre_glissiere;?>
                            </div>
                            <div class="row font-weight-bold">
                                <div class="col-6">Produit</div>
                                <div class="col-3">Prix</div>
                                <div class="col-3">Nombre</div>
                            </div>
                            <?php for ($iii = 0; $iii <= 30; $iii++) {?>
                                <div id="gli_2_recap_content_<?= $iii;?>"></div>
                            <?php } ?>
                        </div>
                        <div class="col-3">
                            <input value="0€" disabled="disabled" id="subs_bottomgli2" class="w-100 textarea_style_input text_label2 text-center p-2 border-0" type="text" />
                        </div>
                    </div>

                    <div class="row gli_num_3 recap_commad_item_line " style="<?php if (isset($title_gli3->is_activ_glissiere) AND ($title_gli3->is_activ_glissiere != 0)){ echo "display:flex;";}else{ echo "display:none;";} ?>">
                        <div class="col-9" id="gli_3_recap_container">
                            <div class="recap_gli_title gli_3_title">
                                <?php if (isset($title_gli3->titre_glissiere) and $title_gli3->titre_glissiere != null) echo $title_gli3->titre_glissiere;?>
                            </div>
                            <div class="row font-weight-bold">
                                <div class="col-6">Produit</div>
                                <div class="col-3">Prix</div>
                                <div class="col-3">Nombre</div>
                            </div>
                            <?php for ($iii = 0; $iii <= 30; $iii++) {?>
                                <div id="gli_3_recap_content_<?= $iii;?>"></div>
                            <?php } ?>
                        </div>
                        <div class="col-3">
                            <input value="0€" disabled="disabled" id="subs_bottomgli3" class="w-100 textarea_style_input text_label2 text-center p-2 border-0" type="text" />
                        </div>
                    </div>

                    <div class="row gli_num_4 recap_commad_item_line " style="<?php if (isset($title_gli4->is_activ_glissiere) AND ($title_gli4->is_activ_glissiere != 0)){ echo "display:flex;";}else{ echo "display:none;"; } ?>" >
                        <div class="col-9" id="gli_4_recap_container">
                            <div class="recap_gli_title gli_4_title">
                                <?php if (isset($title_gli4->titre_glissiere) and $title_gli4->titre_glissiere != null) echo $title_gli4->titre_glissiere;?>
                            </div>
                            <div class="row font-weight-bold">
                                <div class="col-6">Produit</div>
                                <div class="col-3">Prix</div>
                                <div class="col-3">Nombre</div>
                            </div>
                            <?php for ($iii = 0; $iii <= 30; $iii++) {?>
                                <div id="gli_4_recap_content_<?= $iii;?>"></div>
                            <?php } ?>
                        </div>
                        <div class="col-3">
                            <input value="0€" disabled="disabled" id="subs_bottomgli4" class="w-100 textarea_style_input text_label2 text-center p-2 border-0" type="text" />
                        </div>
                    </div>

                    <div class="row gli_num_6 recap_commad_item_line " style="<?php if (isset($title_gli6->is_activ_glissiere) AND ($title_gli6->is_activ_glissiere != 0 )){ echo "display:flex;";} else{ echo "display:none;";} ?>">
                        <div class="col-9" id="gli_6_recap_container">
                            <div class="recap_gli_title gli_6_title">
                                <?php if (isset($title_gli6->titre_glissiere) and $title_gli6->titre_glissiere != null) echo $title_gli6->titre_glissiere;?>
                            </div>
                            <div class="row font-weight-bold">
                                <div class="col-6">Produit</div>
                                <div class="col-3">Prix</div>
                                <div class="col-3">Nombre</div>
                            </div>
                            <?php for ($iii = 0; $iii <= 30; $iii++) {?>
                                <div id="gli_6_recap_content_<?= $iii;?>"></div>
                            <?php } ?>
                        </div>
                        <div class="col-3">
                            <input value="0€" disabled="disabled" id="subs_bottomgli6" class="w-100 textarea_style_input text_label2 text-center p-2 border-0" type="text" />
                        </div>
                    </div>

                    <div class="row gli_num_7 recap_commad_item_line " style="<?php if (isset($title_gli7->is_activ_glissiere) AND ($title_gli7->is_activ_glissiere != 0)){ echo "display:flex;"; }else{echo "display:none;"; } ?>" >
                        <div class="col-9" id="gli_7_recap_container">
                            <div class="recap_gli_title gli_7_title">
                                <?php if (isset($title_gli7->titre_glissiere) and $title_gli7->titre_glissiere != null) echo $title_gli7->titre_glissiere;?>
                            </div>
                            <div class="row font-weight-bold">
                                <div class="col-6">Produit</div>
                                <div class="col-3">Prix</div>
                                <div class="col-3">Nombre</div>
                            </div>
                            <?php for ($iii = 0; $iii <= 30; $iii++) {?>
                                <div id="gli_7_recap_content_<?= $iii;?>"></div>
                            <?php } ?>
                        </div>
                        <div class="col-3">
                            <input value="0€" disabled="disabled" id="subs_bottomgli7" class="w-100 textarea_style_input text_label2 text-center p-2 border-0" type="text" />
                        </div>
                    </div>

            </div>
        </div>
        <div class="col-lg-6 offset-lg-3 mt-4 text-center col-sm-12" id="montant_total">
            <div class="row m-0">
                <div class="col-lg-12 pt-2 d-flex back_blue col-sm-12">
                    <label class="text_label_mont text-uppercase">Montant total ttc à régler</label>
                    <span class="egale">
                        <img src="<?php echo base_url('/assets/images/egale.png')?>">
                    </span>
                    <input disabled="disabled" id="all_totalised" class="w-50 back_blue p-2 prix_tot" type="text" value="0€">
                </div>
            </div>
        </div>
    </div>
    <?php if (isset($datacommande->price_to_reach) AND $datacommande->price_to_reach !=null && isset($datacommande->price_to_win) AND $datacommande->price_to_win !=null && $datacommande->is_activ_prom == '1' ){ ?>
    <div class="col-sm-12">
        <div class="row pt-4 mt-3 pb-4 prom_row shadowed">
            <div class="col-lg-3 text-center col-sm-12">
                <img src="<?php echo base_url()?>assets/images/cible.png">
            </div>
            <div class="col-lg-9 col-sm-12">
                <div class="title_prom text-center">PROFITEZ DE NOTRE OFFRE PROMOTIONNELLE</div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-8 col-8 pl-0 text-prom" id="content_sm_remise">
                                Si vous dépassez un
                                achat d'un montant de
                            </div>
                            <div class="col-lg-4 col-4 pl-0 pt-2" id="content_sm_remise">
                                <input value="<?php echo $datacommande->price_to_reach; ?>€" class="w-100 mr-2 input_prom" type="text" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-8 col-8 text-prom">
                                Vous bénéficiez d'une
                                remise immédiate de
                            </div>
                            <div class="col-lg-4 col-4 pl-0 pt-2" id="content_sm_remise">
                                <input value="<?php echo $datacommande->price_to_win; ?>€" class="w-100 input_prom" type="text" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="is_graced" value="0">
                <div class="row pt-4 pb-4">
                    <div id="text_warn" style="line-height:35px;font-size: 25px!important" class="col-lg-12 col-sm-12 text-center title_prom">
                        Désolé ! votre montant d'achat est inférieur,
                        vous ne pouvez pas en bénéficier !...
                    </div>
                </div>
                <div class="row d-none" id="is_bravo">
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-8 col-8 pl-0 pt-2 text-prom" id="content_sm_remise">
                                Remise TTC à déduire
                            </div>
                            <div class="col-lg-4 col-4 pl-0 pt-2" id="content_sm_remise">
                                <input value="<?php echo $datacommande->price_to_win; ?>€" class="w-100 mr-2 input_prom" type="text" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-8 col-8 text-prom" id="content_sm_remise">
                                Nouveau montant
                                total TTC à régler
                            </div>
                            <div class="col-lg-4 col-4 pl-0 pt-2" id="content_sm_remise">
                                <input id="rest_to_give" value="0" class="w-100 input_prom" type="text" disabled>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    <?php } ?>
    <div class="col-lg-12 emp_contentss d-none">
    <?php if (isset($datacommande->is_activ_type_livr_grat) AND $datacommande->is_activ_type_livr_grat == '1'){ ?>
        <div class="d-none emp_contentss">
            <div class="col-lg-12 pt-4 pb-0 pl-0 pr-0">
                <div class="w-100 middle_title">
                    VENTE EN LIGNE A EMPORTER / RETRAIT ET PAIEMENT EN MAGASIN
                </div>
                <div class="w-100 pt-4">
                    <p class="text_head"><?php echo $datacommande->comment_emporter_txt ?? ""; ?></p>
                </div>
            </div>
            <div class="col-lg-12  pt-2 pb-0 pl-0 pr-0">
                <div class="w-100">
                    <span id="text_vent_emport_jour" class="text_head"></span>
                    <span id="text_vent_emport_heure" class="text_head"></span>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){

                $('#jour_emporter').change(function(){
                    var jour = $('#jour_emporter').val();
                    var result = jour.split('-');
                    if(result[1] == '01'){var mois = 'janvier';}else if(result[1] == '02'){var mois = 'février';}else if(result[1] == '02'){var mois = 'février';}else if(result[1] == '03'){var mois = 'mars';}else if(result[1] == '04'){var mois = 'avril';}else if(result[1] == '05'){var mois = 'mai';}else if(result[1] == '06'){var mois = 'juin';}else if(result[1] == '07'){var mois = 'juillet';}else if(result[1] == '08'){var mois = 'août';}else if(result[1] == '09'){var mois = 'septembre';}else if(result[1] == '10'){var mois = 'octobre';}else if(result[1] == '11'){var mois = 'novembre';}else if(result[1] == '12'){var mois = 'décembre';}
                    $('#text_vent_emport_jour').html('Jour et heure souhaitée le <span id="id_jour_souhait">'+result[2]+' '+mois+' '+result[0]+'</span>')
                })
                $('#heure_enlevement').change(function(){
                    var heure = $('#heure_enlevement').val();
                    var heure_get = heure.split(':');
                    $('#text_vent_emport_heure').html(' à <span id="id_heure_souhait">'+heure_get[0]+' heure '+heure_get[1]+ ' minutes </span>')
                })


            })
        </script>
        <div class="pb-4">
<!--            <input type="hidden" id="type_paiment_cli" value="0">-->
            <div class="col-lg-12 cond pt-2">
                <div class="row">
                    <div class="col-lg-3 p-0 col-sm-12 text_head">Conditions de règlement</div>
                    <?php if ($datacommande->is_activ_cheque_enlev =="1" ||  $datacommande->is_activ_cheque_enlev =="1"){ ?>
                        <div class="col-lg-1 col-1">
                            <input id="type_norm_cheque" class="check_cond" type="checkbox">
                        </div>
                        <div class="col-lg-2 col-2 pl-0">
                            <span class="txt_cond text_head" >Chèque</span>
                        </div>
                    <?php } ?>
                    <?php if ($datacommande->is_activ_termin_banc_enlev =="1"  || $datacommande->is_activ_termin_banc_enlev =="1"){ ?>
                        <div class="col-lg-1 col-1">
                            <input id="type_norm_bank" class="check_cond" type="checkbox">
                        </div>
                        <div class="col-lg-2 pl-0 col-3">
                            <span class="txt_cond text_head">Carte bancaire</span>
                        </div>
                    <?php } ?>
                    <?php if ($datacommande->is_activ_espece_enlev =="1"  || $datacommande->is_activ_espece_enlev =="1"){ ?>
                        <div class="col-lg-1 col-1">
                            <input id="type_norm_espace" class="check_cond" type="checkbox">
                        </div>
                        <div class="col-lg-2 pl-0 col-3">
                            <span class="txt_cond text_head">Espèces</span>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
    </div>
    <?php $this->load->view('front_soutenons/includes_commande/livraisons_commande') ?>
    <?php $this->load->view('front_soutenons/includes_commande/differe_commande') ?>
    <?php $this->load->view('front_soutenons/includes_commande/paypal_commande') ?>
    <div class="container m-auto content_in_container">
        <div class="row">
            <div class="row w-100 mt-3" id="row2_sm">
                <div class="col-lg-12 mb-4" id="col_sm">
                    <p class="text_label text-center mb-0">Questions ou Commentaires</p>
                    <textarea id="comment_comm" class="ml-1 form-control textarea_style_input"></textarea>
                </div>
                <div id="commande_error" class="w-100 text-center">

                </div>
            </div>
            <div class="col-lg-12 pl-0">
                <?php if ((isset($datacommande->is_actif_condition) AND $datacommande->is_actif_condition != 0 AND $datacommande->is_actif_condition != null) AND (isset($datacommande->is_accept_condition) AND $datacommande->is_accept_condition != 0 AND $datacommande->is_accept_condition != null)){ ?>
                    <div id="cgv_default" class="w-100 row pt-5 pb-4 pl-2 shadowed d-flex">
                        <div class="col-lg-12 col-12 p-0 pl-1 pt-2 text-left">
                            <input id="accept_cond_gen" class="check_condss" type="checkbox">
                            <span class="text_head">J'accepte les  <a href="<?php echo site_url('front_soutenons/commercant/condition_general/'.$infocom->IdCommercant); ?>" target="_blank" >conditions générales</a> de vente</span>
                        </div>
                        <!--                        <div class="col-5">-->
                        <!--                            <input disabled class="form-control" type="text" value="--><?php //if (isset($datacommande->cgv_link_differe) AND $datacommande->cgv_link_differe != null ){echo $datacommande->cgv_link_differe; } ?><!--">-->
                        <!--                        </div>-->
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="row mb-5" id="row_sm">
            <div class="col-lg-4 offset-lg-4 pl-1 pt-3 text-center col-sm-12">
                <button class="w-100 btn btn_after_input m-auto" id="validate_command" onclick="send_command()">Validation de la commande</button>
            </div>
            <div class="col-lg-12 col-sm-12 mt-3 pl-0">
                <p class="text_head pb-3">Cher client, dès validation de cette commande, vous allez recevoir une confirmation par mail.</p>
                <p class="text_head pb-3">Imprimer là, elle vous sera demandée lors de votre retrait ou livraison, vous devrez alors signer la bonne réception de votre commande.</p>
                <p class="text_head pb-3">Le Qrcode présent sur ce document permettra au professionnel de valider définitivement cette transaction.</p>
                <p class="text_head pb-3">Nous vous remercions pour cette commande, à très bientôt !...</p>
            </div>
<!--            <div class="col-lg-4  pt-3 text-center mobile_button col-sm-12" id="content_btn_validate">-->
<!--                <button class="w-100 btn btn_after_input_other" id="validate_impression" onclick="">Impression du justificatif de réception</button>-->
<!--            </div>-->
<!--            <div class="col-lg-4 pt-3 text-center pr-1 col-sm-12" id="content_btn_validate">-->
<!--                <button onclick="doCapture();" class="w-100 btn btn_after_input_other" id="validate_export" onclick="">Export de la commande en PDF</button>-->
<!--            </div>-->
        </div>
</div>
    <?php if(isset($datacommande->is_activ_livr_paypal) AND $datacommande->is_activ_livr_paypal =='1'){    ?>
        <div class="container m-auto content_in_container">
            <div class="row">
                <div class="col-lg-12 p-2 col-sm-12">
                    <?php if((isset($datacommande->new_paypal_comment) && $datacommande->new_paypal_comment != null)){ ?>
                        <div class="glissiere_tab">
                            <div class="text-center w-100 titre_gli_up title_gli pl-2">Règlement par Paypal</div>
                        </div>
                        <div class="d-flex w-100 row">
                            <div class="col-lg-3 col-sm-12" style="padding-top: 10%">
                                <?php if (isset($datacommande->paypal_content) AND $datacommande->paypal_content !='' AND $datacommande->paypal_content != null) { print  html_entity_decode($datacommande->paypal_content) ;}else{?>
                                    <img class="img-fluid" src="<?php echo base_url('/assets/images/paypal.png') ?>" style="width: 100%">
                                <?php } ?>
                            </div>
                            <div class="col-lg-9 col-sm-12 pt-3">
                                <ol class="font_7" style="color:rgb(0, 0, 0); font-family:futura-lt-w01-book,sans-serif; font-size:15px;">
                                    <li>
                                        <p class="font_7 m-0 pb-2" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Après avoir confirmé votre commande vous effectuer le règlement par carte bancaire. ;</span></span></span></p>
                                    </li>
                                    <li>
                                        <p class="font_7 m-0 pb-2" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Il vous suffira de noter sur le module PAYPAL, le montant total TTC&nbsp;indiqué ci-dessus ;</span></span></span></p>
                                    </li>
                                    <li>
                                        <p class="font_7 m-0 pb-2" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Imprimez et signez le justificatif de bonne réception ;</span></span></span></p>
                                    </li>
                                    <li>
                                        <p class="font_7 m-0 pb-2" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Dès réception, nous vous contacterons pour vous remercier et vous préciser les modalités de votre livraison ;</span></span></span></p>
                                    </li>
                                    <li>
                                        <p class="font_7 m-0 pb-2" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Remettez le justificatif de bonne réception à notre livreur ou dans notre établissement dans le cadre d'une vente à emporter.</span></span></span></p>
                                    </li>
                                </ol>
                            </div>
                        </div>


                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<?php } ?>
</div>
    <?php //$datacommande->is_activ_com_spec = 1;?>
<?php if (isset($datacommande->is_activ_com_spec) AND $datacommande->is_activ_com_spec =="1" ){ ?>
    <div class="container m-auto text-center col-sm-12 d-none d-lg-block content_in_container">
        <img src="<?php echo base_url('/assets/images/william-commande.jpg') ?>" style="width: 75%;">
    </div>
<div class="row glissiere_tab pt-sm-4 pt-3 pb-sm-4 pb-1">
    <div class="col-lg-2 offset-lg-2 col-sm-12">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-4 text-center">
                <label class="switch" id="id_spec_gli">
                    <input type="checkbox">
                    <span class="slider round"></span>
                </label>
            </div>
            <div class="col-md-6 col-sm-6 col-8 p-0">
                <p class="titre_volet">Ouvrir ou fermer ce volet</p>
            </div>
        </div>
    </div>
    <div class="col-lg-4 titre_gli_up title_gli text-center">
        CDE SPECIFIQUE
    </div>
    <div class="col-lg-4">
    </div>
</div>
<form method="post" enctype="multipart/form-data" id="upload_file_custom">
    <?php if (!isMobile()) { ?>
    <div class="container m-auto col-sm-12 d-none content_in_container" id="content_spec">
        <div class="row pt-4">
            <div class="col-lg-3 col-sm-12 text-center">
                <img src="<?php echo base_url('/assets/images/icone-telecharger-doc.webp') ?>">
                <button class="btn btn_download w-100" type="button" onclick="import_command_file()">téléchargement <span class="plus">+</span></button>
                <p class="text_small pt-3">Maximum 15MB</p>
                <input class="d-none" type="file" name="command_file" id="command_file">
                <?php if(isset($IdUser) && $IdUser != null && $IdUser!= ""){ ?>
                    <input type="hidden" name="id_client" id="id_client2" value="<?php echo $IdUser; ?>">
                <?php }else{ ?>
                    <input type="hidden" name="id_client" id="id_client2" value="0">
                <?php } ?>
                <input type="hidden" name="id_com" id="id_com" value="<?php echo $oInfoCommercant->IdCommercant;?>">
                <input type="hidden" name="type_paiement" id="type_paiement_custom" value="0">
            </div>
            <div class="col-lg-9 col-sm-12">
                <label class="text_head">Avec ce module, vous pouvez nous envoyer vos demandes (notes, commandes, ordonnances, documents word ou PDF...) mais aussi vos questions ou commentaires.</label>
                <p class="text_head pt-4">1. Téléchargez votre document ;</p>
                <p class="text_head">2. Précisez votre choix : enlèvement ou livraison, date, heure</p>
                <p class="text_head">3. Et/ou remplissez le champ des commentaires</p>
                <p class="text_head">4. Envoyez</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <label class="col-12 text_label text-center">Questions ou Commentaires</label>
                <textarea name="comment_livr_enlev" id="comment_livr_enlev" class="form-control textarea_style_input"></textarea>
            </div>
        </div>
        <div class="row p-3">
            <div class="col-lg-3 offset-lg-0 pl-0 pr-0 col-sm-8 offset-sm-2">
                <select id="type_livraison" name="type_livraison" class="form-control col-12 textarea_style_input text_head" style="height: 42px;">
                    <option selected>Enlèvement ou livraison</option>
                    <?php if (isset($datacommande->is_activ_comm_enlev) AND $datacommande->is_activ_comm_enlev =="1"){ ?>
                        <option value="Vente à emporter">Vente à emporter</option>
                    <?php } ?>
                    <?php if (isset($datacommande->is_activ_comm_livr_dom) AND $datacommande->is_activ_comm_livr_dom =="1"){ ?>
                        <option value="Livraison à domicile">Livraison à domicile</option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-lg-9 col-sm-12">
                <div class="row">
                    <div class="col-lg-6 d-flex pl-5 pr-0 text-center col-sm-12" id="textarea_sm_livr8">
                        <div class="row">
                            <div class="col-lg-5 d-flex col-sm-12">
                                <label class="text_label pr-1">Jour souhaité </label>
                            </div>
                            <div class="col-lg-7 col-sm-12 pl-0">
                                <input id="jour_souhaite" name="jour_souhaite" class="w-100 form-control textarea_style_input text_head pr-0 pl-1" type="date">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 d-flex pl-4 pr-0 text-center col-sm-12" id="textarea_sm_livr6">
                        <div class="row w-100">
                            <div class="col-lg-6 d-flex col-sm-12">
                                <label class="text_label">Heure souhaitée </label>
                            </div>
                            <div class="col-lg-6 col-sm-12" id="textarea_sm_livr7">
                                <input id="heure_souhaite" name="heure_souhaite" class="w-100 form-control textarea_style_input text_head" type="time">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 pl-0 pt-4 col-sm-12 text-center">
                <img src="<?php echo base_url('/assets/images/zette2.webp') ?>">
            </div>
            <div class="col-lg-8 pt-5 col-sm-12">
                <label class="text_head">Dès réception de votre commande, notre équipe vous contactera et vous précisera les conditions de livraison ou de mise à disposition et son montant.</label>
                <?php if ((isset($datacommande->is_activ_card_bank_bottom) && $datacommande->is_activ_card_bank_bottom == "1") || (isset($datacommande->is_activ_cheque_bottom) && $datacommande->is_activ_cheque_bottom == "1")){ ?>
                    <div class="w-100 pt-3 title_blues">
                        CHOIX DU REGLEMENT
                    </div>
                <?php } ?>
                <?php if (isset($datacommande->is_activ_card_bank_bottom) && $datacommande->is_activ_card_bank_bottom == "1"){ ?>
                    <div class="w-100 d-flex pt-2">
                        <div class="col-lg-1 pl-0 d-flex col-2">
                            <input id="check_bank_type" class="check_cond" type="checkbox" style="align-self: center;width: 50px;height: 50px">
                        </div>
                        <div class="col-lg-11 col-10 pl-0 pt-3 text_head">Par carte bancaire avec un terminal de paiement mobile</div>
                    </div>
                <?php } ?>
                <?php if (isset($datacommande->is_activ_cheque_bottom) && $datacommande->is_activ_cheque_bottom == "1"){ ?>
                    <div class="w-100 d-flex pt-2">
                        <div class="col-lg-1 pl-0 d-flex col-2">
                            <input id="check_cheque_type" class="check_cond" type="checkbox" style="align-self: center;width: 50px;height: 50px">
                        </div>
                        <div class="col-lg-11 col-10 pl-0 text_head">Par chèque bancaire préalablement rempli et signé à l'ordre de notre établissement</div>
                    </div>
                <?php } ?>
            </div>
            <div class="col-lg-6 offset-lg-3 col-sm-6 offset-sm-3">
                <div class="row">
                    <div class="col-lg-6 offset-lg-3 pt-4 col-sm-6">
                        <div onclick="submit_custom_form()" class="w-100 btn btn_after_input">Envoyez</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 pt-3">
                <div class="row">
                    <div class="col-lg-3 d-lg-block d-none">
                        <img src="<?php echo base_url("/assets/images/logo-carte-vivresaville_fr.webp") ?>">
                    </div>
                    <div class="col-lg-6">
                        <p style="font-family: Futura-LT-Book, Sans-Serif;color:#E80EAE;font-size:25px;text-align: center;margin-bottom: 0px!important">Attention!</p>
                        <p style="font-family: Futura-LT-Book, Sans-Serif;color:#2F2E2E;font-size: 15px;text-align: justify;">Pour pouvoir adresser vos documents, vous devez obligatoirement posséder la carte vivresaville.</p>
                        <p style="font-family: Futura-LT-Book, Sans-Serif;color:#2F2E2E;font-size: 15px;text-align: justify;">Pour enregistrer votre carte ou la demander, cliquez sur le bouton ci-contre pour accéder au formulaire.</p>
                    </div>
                    <div class="col-lg-3 pt-5">
                        <a href="#number_card" class="round_arrow"></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 pb-5 pt-2 col-sm-12 content_sm_text">
                <?php if (isset($datacommande->is_activ_comment_bottom) AND $datacommande->is_activ_comment_bottom =="1"){ ?>
                    <label class="text_head">
                        <?php echo $datacommande->comment_comm_spec_bottom_txt ?>
                    </label>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php }else{ ?>
    <div class="modal fade" id="content_specmodal" role="dialog" style="display: none">
        <div class="modal-dialog m-0">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header glissiere_tab">
                    <div class="col-12 text-center">
                        <div class="w-50 d-block m-auto" id="content_btn_gli7">
                            <div style="color: white;font-size: 15px;" class="" data-dismiss="modal" id="close_spec" aria-label="Close">
                                Retour Commande
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row pt-4">
                        <div class="col-lg-3 col-sm-12 text-center">
                            <img src="<?php echo base_url('/assets/images/icone-telecharger-doc.webp') ?>">
                            <button class="btn btn_download w-100" type="button" onclick="import_command_file()">téléchargement <span class="plus">+</span></button>
                            <p class="text_small pt-3">Maximum 15MB</p>
                            <input class="d-none" type="file" name="command_file" id="command_file">
                            <?php if(isset($IdUser) && $IdUser != null && $IdUser!= ""){ ?>
                                <input type="hidden" name="id_client" id="id_client2" value="<?php echo $IdUser; ?>">
                            <?php }else{ ?>
                                <input type="hidden" name="id_client" id="id_client2" value="0">
                            <?php } ?>
                            <input type="hidden" name="id_com" id="id_com" value="<?php echo $oInfoCommercant->IdCommercant;?>">
                            <input type="hidden" name="type_paiement" id="type_paiement_custom" value="0">
                        </div>
                        <div class="col-lg-9 col-sm-12">
                            <label class="text_head">Avec ce module, vous pouvez nous envoyer vos demandes (notes, commandes, ordonnances, documents word ou PDF...) mais aussi vos questions ou commentaires.</label>
                            <p class="text_head pt-4">1. Téléchargez votre document ;</p>
                            <p class="text_head">2. Précisez votre choix : enlèvement ou livraison, date, heure</p>
                            <p class="text_head">3. Et/ou remplissez le champ des commentaires</p>
                            <p class="text_head">4. Envoyez</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label class="col-12 text_label text-center">Questions ou Commentaires</label>
                            <textarea name="comment_livr_enlev" id="comment_livr_enlev" class="form-control textarea_style_input"></textarea>
                        </div>
                    </div>
                    <div class="row p-3">
                        <div class="col-lg-3 offset-lg-0 pl-0 pr-0 col-sm-8 offset-sm-2">
                            <select id="type_livraison" name="type_livraison" class="form-control col-12 textarea_style_input text_head" style="height: 42px;">
                                <option selected>Enlèvement ou livraison</option>
                                <?php if (isset($datacommande->is_activ_comm_enlev) AND $datacommande->is_activ_comm_enlev =="1"){ ?>
                                    <option value="Vente à emporter">Vente à emporter</option>
                                <?php } ?>
                                <?php if (isset($datacommande->is_activ_comm_livr_dom) AND $datacommande->is_activ_comm_livr_dom =="1"){ ?>
                                    <option value="Livraison à domicile">Livraison à domicile</option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-lg-9 col-sm-12">
                            <div class="row">
                                <div class="col-lg-6 d-flex pl-5 pr-0 text-center col-sm-12" id="textarea_sm_livr8">
                                    <div class="row">
                                        <div class="col-lg-5 d-flex col-sm-12">
                                            <label class="text_label pr-1">Jour souhaité </label>
                                        </div>
                                        <div class="col-lg-7 col-sm-12 pl-0">
                                            <input id="jour_souhaite" name="jour_souhaite" class="w-100 form-control textarea_style_input text_head pr-0 pl-1" type="date">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 d-flex pl-4 pr-0 text-center col-sm-12" id="textarea_sm_livr6">
                                    <div class="row w-100">
                                        <div class="col-lg-6 d-flex col-sm-12">
                                            <label class="text_label">Heure souhaitée </label>
                                        </div>
                                        <div class="col-lg-6 col-sm-12" id="textarea_sm_livr7">
                                            <input id="heure_souhaite" name="heure_souhaite" class="w-100 form-control textarea_style_input text_head" type="time">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 pl-0 pt-4 col-sm-12 text-center">
                            <img src="<?php echo base_url('/assets/images/zette2.webp') ?>">
                        </div>
                        <div class="col-lg-8 pt-5 col-sm-12">
                            <label class="text_head">Dès réception de votre commande, notre équipe vous contactera et vous précisera les conditions de livraison ou de mise à disposition et son montant.</label>
                            <?php if ((isset($datacommande->is_activ_card_bank_bottom) && $datacommande->is_activ_card_bank_bottom == "1") || (isset($datacommande->is_activ_cheque_bottom) && $datacommande->is_activ_cheque_bottom == "1")){ ?>
                                <div class="w-100 pt-3 title_blues">
                                    CHOIX DU REGLEMENT
                                </div>
                            <?php } ?>
                            <?php if (isset($datacommande->is_activ_card_bank_bottom) && $datacommande->is_activ_card_bank_bottom == "1"){ ?>
                                <div class="w-100 d-flex pt-2">
                                    <div class="col-lg-1 pl-0 d-flex col-2">
                                        <input id="check_bank_type" class="check_cond" type="checkbox" style="align-self: center;width: 50px;height: 50px">
                                    </div>
                                    <div class="col-lg-11 col-10 pl-0 pt-3 text_head">Par carte bancaire avec un terminal de paiement mobile</div>
                                </div>
                            <?php } ?>
                            <?php if (isset($datacommande->is_activ_cheque_bottom) && $datacommande->is_activ_cheque_bottom == "1"){ ?>
                                <div class="w-100 d-flex pt-2">
                                    <div class="col-lg-1 pl-0 d-flex col-2">
                                        <input id="check_cheque_type" class="check_cond" type="checkbox" style="align-self: center;width: 50px;height: 50px">
                                    </div>
                                    <div class="col-lg-11 col-10 pl-0 text_head">Par chèque bancaire préalablement rempli et signé à l'ordre de notre établissement</div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col-lg-6 offset-lg-3 col-sm-6 offset-sm-3">
                            <div class="row">
                                <div class="col-lg-6 offset-lg-3 pt-4 col-sm-6">
                                    <div onclick="submit_custom_form()" class="w-100 btn btn_after_input">Envoyez</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 pt-3">
                            <div class="row">
                                <div class="col-lg-3 d-lg-block d-none">
                                    <img src="<?php echo base_url("/assets/images/logo-carte-vivresaville_fr.webp") ?>">
                                </div>
                                <div class="col-lg-6">
                                    <p style="font-family: Futura-LT-Book, Sans-Serif;color:#E80EAE;font-size:25px;text-align: center;margin-bottom: 0px!important">Attention!</p>
                                    <p style="font-family: Futura-LT-Book, Sans-Serif;color:#2F2E2E;font-size: 15px;text-align: justify;">Pour pouvoir adresser vos documents, vous devez obligatoirement posséder la carte vivresaville.</p>
                                    <p style="font-family: Futura-LT-Book, Sans-Serif;color:#2F2E2E;font-size: 15px;text-align: justify;">Pour enregistrer votre carte ou la demander, cliquez sur le bouton ci-contre pour accéder au formulaire.</p>
                                </div>
                                <div class="col-lg-3 pt-5">
                                    <a href="#number_card" class="round_arrow"></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 pb-5 pt-2 col-sm-12 content_sm_text">
                            <?php if (isset($datacommande->is_activ_comment_bottom) AND $datacommande->is_activ_comment_bottom =="1"){ ?>
                                <label class="text_head">
                                    <?php echo $datacommande->comment_comm_spec_bottom_txt ?>
                                </label>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php } ?>
    <?php
    if (isMobile()) { ?>
        <script>
            $(document).ready(function(){
                $("#id_spec_gli").click(function(){

                    if($("#id_spec_gli_value").val() == 0){
                        setTimeout(function(){
                            $("#content_specmodal").modal("toggle");
                            $("#id_spec_gli_value").val('1');
                        }, 1000);
                    }
                    else{
                        setTimeout(function(){
                        $("#content_specmodal").hide();
                        $("#id_spec_gli_value").val('0');
                        }, 1000);
                    }
                });
                $("#close_spec").click(function(){
                    $("#content_specmodal").hide();
                    // $("#id_spec_gli").click();
                });
            });
        </script>
    <?php }else{ ?>
        <script>
            $("#id_spec_gli").change(function (){
                if($("#id_spec_gli_value").val() == 0){
                    $("#content_spec").removeClass('d-none');
                    $("#content_spec").addClass('d-block');
                    $("#id_spec_gli_value").val('1');
                }
                else{
                    $("#content_spec").removeClass('d-block');
                    $("#content_spec").addClass('d-none');
                    $("#id_spec_gli_value").val('0');
                }
            });
        </script>
    <?php } ?>
</form>
<?php } ?>
<input id="conditions_terminal_value" type="hidden" value="0">
<input id="conditions_generale_value" type="hidden" value="0">
<input id="conditions_paypal_value" type="hidden" value="0">
<script>
    $('#conditions_terminal').change(function(){
        if($('#conditions_terminal_value').val() == 0){
            $('#conditions_terminal_value').val('1');
        }else{
            $('#conditions_terminal_value').val('0');
        }
    });
    $('#conditions_generale').change(function(){
        if($('#conditions_generale_value').val() == 0){
            $('#conditions_generale_value').val('1');
        }else{
            $('#conditions_generale_value').val('0');
        }
    });
    $('#conditions_paypal').change(function(){
        if($('#conditions_paypal_value').val() == 0){
            $('#conditions_paypal_value').val('1');
        }else{
            $('#conditions_paypal_value').val('0');
        }
    });
</script>
<style>
    .arrow_up{
        content:"\f077"!important;
        font-family: FontAwesome!important;
        color:white;
        font-size: 18px;
        position: absolute;
        top: 10px;
        left: 0;
    }
    .round_arrow{
        padding-top: 40px;
        padding-bottom: 40px;
        padding-left: 50px;
        padding-right: 50px;
        background:rgba(232, 14, 174, 1);
        border-radius: 60px;
        position: relative;
    }
    .round_arrow::after{
        content: "\f106"!important;
        font-family: FontAwesome!important;
        color: white;
        font-size: 60px;
        position: absolute;
        top: 0px;
        left: 31px;
    }
    .modal{
        width: 100%!important;
    }
    .modal-dialog,.modal-content,.modal-header,.modal-body{
        width: 100%!important;
    }
    .modal.fade.show{
        width: 100%!important;
    }
    .recap_commad_item_line {
        border-bottom: solid #ccc;
        padding-top: 15px;
    }
    .recap_gli_title {
        font-size: 20px;
        font-weight: bold;
        text-transform: uppercase;
    }
</style>