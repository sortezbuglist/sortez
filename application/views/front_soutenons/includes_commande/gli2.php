<?php if (isset($data_gli2) && !empty($data_gli2) && isset($title_gli2->is_activ_glissiere) && $title_gli2->is_activ_glissiere != 0) { ?>
    
    <div class="row glissiere_tab marge_gli_1">
        <form class="m-0 w-100 row pt-3 pb-3">
            <div class="col-lg-2 offset-lg-2 col-sm-12">
                <div class="row">
                    <div class="col-lg-6 text-center col-6">
                        <label class="switch" id="activ_glissiere_activ2">
                            <input type="checkbox">
                            <span class="slider round"></span>
                        </label>
                    </div>
                    <div class="col-lg-6 p-0 col-6">
                        <p class="titre_volet">Ouvrir ou fermer ce volet</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div id="txt_title_gli2"
                     class="text-left text-sm-center title_gli titre_gli_up pt-2 pt-sm-0">
                    <?php if (isset($title_gli2->titre_glissiere) and $title_gli2->titre_glissiere != null) {
                        echo $title_gli2->titre_glissiere;
                    } else {
                        echo "TITRE";
                    } ?>
                </div>
            </div>
            <div class="col-lg-2 col-sm-12">
                <div class="row text-right text_head">
                    <div class=" text-center col-lg-6 col-sm-6 p-0">
                        <label>Montant total TTC à régler</label>
                    </div>
                    <div class="col-lg-6 col-sm-6 p-0" id="giltotalcontain">
                        <input id="gli2total" disabled="disabled"
                               class="p-2 ml-1 input_glissiere text-center text_input_montant" type="text" value="0€"/>
                    </div>
                </div>
            </div>
        </form>
    </div>
    
    


    <div class="row pr-0 pb-5 pt-4 d-none" id="activ_glissiere_activ2_content">
        <div class="pl-2 pr-2">
            <div class="row m-0">
                <?php $i2 = 0; foreach ($data_gli2 as $gli2) { ?>
                    <div class="col-lgg-2 pt-4 col-sms-12">
                        <div class="ombre_bloc">
                            <div class="row w-100 img-glisss m-0">
                                <?php if (isset($gli2->image) and is_file("application/resources/front/photoCommercant/imagesbank/" . $infocom->user_ionauth_id . "/soutgli1/" . $gli2->image)) { ?>
                                    <img class="img-fluid" style="width: 100%;"
                                         src="<?php echo base_url("application/resources/front/photoCommercant/imagesbank/" . $infocom->user_ionauth_id . "/soutgli1/" . $gli2->image); ?>">
                                <?php } else { ?>
                                    <img class="img-fluid" style="align-self: center"
                                         src="<?php echo base_url("/assets/images/image_default.png"); ?>">
                                <?php } ?>
                            </div>
                            <div class="col-12 text-center pt-4">

                                    <input class="id_gliss_2" value="<?php echo $gli2->id; ?>" type="hidden"
                                           id="product_gli2_<?php echo $i2; ?>"/>
                                    <?php if (isset($gli2->true_title) and $gli2->true_title != null) { ?>
                                        <p class="titre_categorie text-center" id="gli2_item_title_<?= $i2;?>">
                                            <?php echo $gli2->true_title; ?>
                                        </p>
                                    <?php } ?>
                                    <?php if (isset($gli2->titre) and $gli2->titre != null) { ?>
                                        <p class="description_categorie">
                                            <?php echo $gli2->titre; ?>
                                        </p>
                                    <?php } ?>
                            </div>
                            <div class="col-12 text-center pt-2 pb-5">
                                <div class="row">
                                    <div class="col-6">
                                        <?php if (isset($gli2->prix) and $gli2->prix != null) { ?>
                                            <input id="prix2<?php echo $i2; ?>" type="hidden" value="<?php echo $gli2->prix; ?>">
                                            <label class="col-12 text_label text-left">Prix U.</label>
                                            <div class="col-12 p-0">
                                                <input id="gli2_item_price_<?= $i2;?>" value="<?php echo "€ ".$gli2->prix; ?>"
                                                       class="w-100 nbre_gliss2_pr textarea_style_input text-center prix_u" disabled>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="col-6">
                                        <label class="col-12 text_label">Quantité</label>
                                        <div class="col-12 p-0 pb-2">
                                            <input value="0"
                                                   onchange="change_total_price2('prix2<?php echo $i2 . '\''; ?>,'nbre2<?php echo $i2 . '\''; ?>,'product_gli2_<?php echo $i2 . '\''; ?>);change_recap_gli2_item_<?= $i2;?>();"
                                                   id="nbre2<?php echo $i2; ?>" type="number"
                                                   class="gli2_item_qtt_<?= $i2;?> w-100 nbre_gliss2_qt textarea_style_input p-2 text_bold text-center">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        function change_recap_gli2_item_<?= $i2;?>(){
                            var recap_gli_title = $("#gli2_item_title_<?= $i2;?>").html();
                            var recap_gli_price = $("#gli2_item_price_<?= $i2;?>").val();
                            var recap_gli_qtt = $(".gli2_item_qtt_<?= $i2;?>").val();
                            var gli_recap_contents = '<div class="row">\n' +
                                '                                <div class="col-6">'+recap_gli_title+'</div>\n' +
                                '                                <div class="col-3">'+recap_gli_price+'</div>\n' +
                                '                                <div class="col-3">'+recap_gli_qtt+'</div>\n' +
                                '                            </div>';
                            $("#gli_2_recap_content_<?= $i2;?>").html(gli_recap_contents);
                        }
                    </script>
                    <?php $i2++;
                } ?>
            </div>
        </div>
    </div>

    <div class="modal fade" id="content_gli2" role="dialog" style="height: 1000px;">
        <div class="modal-dialog m-0">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header glissiere_tab">
                    <div class="col-12 text-center">
                        <div class="w-50 d-block m-auto" id="content_btn_gli2">
                            <div style="color: white;font-size: 15px;" class="" data-dismiss="modal" id="close_gli2" aria-label="Close">
                            <img style="width: 10%;height: auto;margin-bottom: 3px;" src="<?php echo base_url("assets/image/backs.png"); ?>"> Retour Commande
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-body" id="modal-scroll2">
                    <div class="row m-0">
                    <div id="txt_title_gli2"
                         class="text-center glissiere_tab pt-4 w-100 title_gli titre_gli_up">
                        <div class="w-100">
                            <div style="width: 50px;height: 50px;" class="chiffre_gli m-auto">2</div>
                        </div>
                        <div class="w-100 pt-2">
                            <?php if (isset($title_gli2->titre_glissiere) and $title_gli2->titre_glissiere != null) {
                                echo $title_gli2->titre_glissiere;
                            } else {
                                echo "TITRE";
                            } ?>
                        </div>
                        <div class="w-100 pt-2" style="font-size: 15px!important;">
                            Montant total TTC à régler
                        </div>
                        <div class="w-100 mt-3" style="font-family:avenir-lt-w01_85-heavy1475544,sans-serif;font-size:29px!important;font-weight: bold">
                            <input style="border: none" id="total_mobile_gli2" disabled="disabled"
                                   class="p-2 ml-1 input_glissiere text-center text_input_montant" type="text" value="0€"/>
                        </div>
                    </div>

                        <?php $i2 = 0; foreach ($data_gli2 as $gli2) { ?>
                            <div class="col-lgg-2 pt-4 col-sms-12 col-md-4">
                                <div class="ombre_bloc">
                                    <div class="row w-100 img-glisss m-0">
                                        <?php if (isset($gli2->image) and is_file("application/resources/front/photoCommercant/imagesbank/" . $infocom->user_ionauth_id . "/soutgli1/" . $gli2->image)) { ?>
                                            <img class="img-fluid" style="width: 100%;"
                                                 src="<?php echo base_url("application/resources/front/photoCommercant/imagesbank/" . $infocom->user_ionauth_id . "/soutgli1/" . $gli2->image); ?>">
                                        <?php } else { ?>
                                            <img class="img-fluid" style="align-self: center"
                                                 src="<?php echo base_url("/assets/images/image_default.png"); ?>">
                                        <?php } ?>
                                    </div>
                                    <div class="col-12 text-center pt-4">

                                        <input class="id_gliss_2" value="<?php echo $gli2->id; ?>" type="hidden"
                                               id="product_gli2_<?php echo $i2; ?>"/>
                                        <?php if (isset($gli2->true_title) and $gli2->true_title != null) { ?>
                                            <p class="titre_categorie text-center" id="gli2_item_title_mobile_<?= $i2;?>">
                                                <?php echo $gli2->true_title; ?>
                                            </p>
                                        <?php } ?>
                                        <?php if (isset($gli2->titre) and $gli2->titre != null) { ?>
                                            <p class="description_categorie">
                                                <?php echo $gli2->titre; ?>
                                            </p>
                                        <?php } ?>
                                    </div>
                                    <div class="col-12 text-center pt-2 pb-5">
                                        <div class="row">
                                            <div class="col-6">
                                                <?php if (isset($gli2->prix) and $gli2->prix != null) { ?>
                                                    <input id="prix2<?php echo $i2; ?>" type="hidden" value="<?php echo $gli2->prix; ?>">
                                                    <label class="col-12 text_label">Prix U.</label>
                                                    <div class="col-12 p-0">
                                                        <input id="gli2_item_price_mobile_<?= $i2;?>" value="<?php echo "€ ".$gli2->prix; ?>"
                                                               class="w-100 nbre_gliss2_pr textarea_style_input text-center prix_u" disabled>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <div class="col-6">
                                                <label class="col-12 text_label">Quantité</label>
                                                <div class="col-12 p-0 pb-2">
                                                    <input value="0"
                                                           onchange="change_total_price2('prix2<?php echo $i2 . '\''; ?>,'nbre2<?php echo $i2 . '\''; ?>,'product_gli2_<?php echo $i2 . '\''; ?>);change_recap_gli2_item_mobile_<?= $i2;?>();"
                                                           id="nbre2<?php echo $i2; ?>" type="number"
                                                           class="gli2_item_qtt_mobile_<?= $i2;?> w-100 nbre_gliss2_qt textarea_style_input p-2 text_bold text-center">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript">
                                function change_recap_gli2_item_mobile_<?= $i2;?>(){
                                    var recap_gli_title = $("#gli2_item_title_mobile_<?= $i2;?>").html();
                                    var recap_gli_price = $("#gli2_item_price_mobile_<?= $i2;?>").val();
                                    var recap_gli_qtt = $(".gli2_item_qtt_mobile_<?= $i2;?>").val();
                                    var gli_recap_contents = '<div class="row">\n' +
                                        '                                <div class="col-6">'+recap_gli_title+'</div>\n' +
                                        '                                <div class="col-3">'+recap_gli_price+'</div>\n' +
                                        '                                <div class="col-3">'+recap_gli_qtt+'</div>\n' +
                                        '                            </div>';
                                    $("#gli_2_recap_content_<?= $i2;?>").html(gli_recap_contents);
                                }
                            </script>
                            <?php $i2++;
                        } ?>
                    </div>
                </div>
            </div>

        </div>
        <style>
            .modal {
                overflow: hidden;
            }
            .modal .modal-body {
                height: 620px;
                overflow: auto;
            }
            .modal .modal-fixed {
                position: fixed;
                z-index : 1052;
                float: left;
                width:auto!important;
                top: 20px;
            }
        </style>
        <script>
            var positionElementInPage = $('#close_gli2').offset().top;
            $('#modal-scroll2').resize(function() {
                positionElementInPage = $('#close_gli2').offset().top;
            });
        </script>
    </div>
    
        <script>
            $("#activ_glissiere_activ2").change(function (){
                if($("#activ_glissiere_activ2_value").val() == 0){
                    $("#activ_glissiere_activ2_content").removeClass('d-none');
                    $("#activ_glissiere_activ2_content").addClass('d-block');
                    $("#activ_glissiere_activ2_value").val('1');
                }
                else{
                    $("#activ_glissiere_activ2_content").removeClass('d-block');
                    $("#activ_glissiere_activ2_content").addClass('d-none');
                    $("#activ_glissiere_activ2_value").val('0');
                }
            })
        </script>
     
<?php } ?>