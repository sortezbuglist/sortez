<?php if (isset($datacommande) AND $datacommande->is_activ_livr_paypal == '1'){ ?>
    <div class="row livr_contentss d-none">
        <div class="mt-4 col-sm-12 pl-0 pt-4">
            <div class="w-100 middle_title pl-4">
                VENTE ET PAIEMENT A LA COMMANDE EN LIGNE AVEC PAYPAL
            </div>
        </div>
    </div>
    <div class="row pt-3 pb-4 shadowed livr_contentss d-none">
        <div class="mt-4 col-sm-12 pl-4 pt-2 text-left text_head">
            <input id="accept_paypal" class="check_cond" type="checkbox">
            <?php echo $datacommande->new_paypal_comment ?? ""; ?>
        </div>
    </div>
<?php } ?>