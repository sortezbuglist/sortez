<?php $data['empty'] = null;  ?>
<?php $data["zTitle"] = 'Mes Annonces' ?>
<?php $this->load->view("privicarte/includes/main_header", $data); ?>

<div class="col-lg-12 padding0" style="text-align: center; display: none;"><img src="<?php echo base_url(); ?>application/resources/sortez/images/logo.png" alt="logo"></div>

<style type="text/css">
    body{
        margin-top: 0px;
    }
    .btnmenu{
        text-align: center;
        margin-top: 39px;
        background-color: #af4106;
        width:300px;
        padding-bottom: 10px;
        padding-top:10px;
        color:white;
        font-size:15px;


    }
    .btn:visited{color: white!important;}
    .btn{color: white!important;}
    .titre{
        text-align: center;
        background-color: #af4106;
        color: white;
        height: 40px;
        line-height: 2.5;
    }
    .contenu{
        background-color: #e1e1e1;
        width: 700px;
        padding-top:10px;
        padding-bottom: 10px;
        margin:auto;
    }
    .btnvalide{
        width: 700px;
    }
    a{
        text-decoration: none;
    }
    .row{
        margin: auto;
    }
</style>
<script type="text/javascript">


    function deleteFile(_IdPlat, Col) {
        jQuery.ajax({
            url: '<?php echo base_url();?>front/plat_du_jour/delete_photo/' + _IdPlat + '/' + Col,
            dataType: 'html',
            type: 'POST',
            async: true,
            success: function (data) {
                window.location.reload();
            }
        });
    }
</script>
<div style="background-color: #fff; padding-bottom:40px;width: 75%;margin: auto">
    <form id="global_form" action="<?php echo site_url('front/Plat_du_jour/save_reservation_commercant_info/'.$idCommercant); ?>" method="POST"  enctype="multipart/form-data">
        <input name="res[id]" type="hidden" value="<?php if (isset($infores->id))echo $infores->id?>">
        <div class="row justify-content-center">
            <a href="<?php echo site_url();?>"><div class="col-sm-12 btnmenu">RETOUR SITE</div></a>
        </div>
        <div class="row justify-content-center">
            <a href="<?php echo site_url('auth/login');?>"><div class="col-sm-12 btnmenu" style="margin-top:20px;!important;">RETOUR MENU</div></a>
        </div>
        <div class="row">

            <input type="hidden" name="res[IdCommercant]" value=" <?php echo $idCommercant; ?>">
            <input type="hidden" name="res[IdUsers_ionauth]" value=" <?php echo $IdUsers_ionauth; ?>">
            <input type="hidden" name="page_parent" value="menu_carte">
        </div>
        <div class="row">
            <div style="width:700px;margin:auto;!important;margin-top: 30px" class="col-sm-12 titre">TELECHARGEMENT DE VOS CARTE ET MENU EN PDF</div>
        </div>
        <div class="row">
            <div class="col-sm-12 contenu justify-content- text-center">
                Précisez le nom de votre document: <span style="color: red">NB:Ne pas mettre des espaces</span>
                <input <?php if (isset($infores->Nom_pdf_menu) AND (file_exists("application/resources/front/images/plat/pdf/" . $infores->Nom_pdf_menu.'.pdf') == true)){echo 'disabled';} ?> value="<?php if (isset($infores->Nom_pdf_menu))echo $infores->Nom_pdf_menu;?>" class="form-control" id="nom_pdf"  type="text" name="res[Nom_pdf_menu]">
                <div>
                    <?php if (!empty($infores->Nom_pdf_menu)) { ?>
                        <?php //echo $infores->Nom_pdf_menu; ?>

                        <?php if ($infores->Nom_pdf_menu != "" && $infores->Nom_pdf_menu != NULL && file_exists("application/resources/front/images/plat/pdf/" . $infores->Nom_pdf_menu.'.pdf') == true) { ?>
                        <a
                            href="<?php echo base_url(); ?>application/resources/front/images/plat/pdf/<?php echo $infores->Nom_pdf_menu; ?>.pdf"
                            target="_blank"><img
                                src="<?php echo base_url(); ?>application/resources/front/images/pdf_icone.png"
                                width="64" alt="PDF"/></a><?php } ?>

                        <a class="btn btn-danger" href="javascript:void(0);"
                           onclick="deleteFile('<?php echo $infores->id; ?>','Nom_pdf_menu');">Supprimer</a><br/>

                    <?php } else { ?>
                        <?php echo form_open_multipart('upload/do_upload');?>
                        <input type="file" name="Nom_pdf_menu" id="PlatPdf" value="" class=""/>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="row p-4">
            <div class="col-lg-3"></div>
            <div class="col-lg-3 text-right text-justify">
                <input <?php if (isset($infores->actif_link_pdf) AND $infores->actif_link_pdf == 'on'){echo 'checked';} ?> onclick="
                                                                    if($('#activ_pdf').is(':checked')){
                                                                        $('#actif_link_pdf').val('on');
                                                                    }else{
                                                                        $('#actif_link_pdf').val('off');
                                                                    }
                                                                " type="checkbox" name="" id="activ_pdf">Je désire que ce bouton s’intègre sur la colonne des menus de mon site afin de bénéficier du formulaire de réservation en ligne
                <input id="actif_link_pdf" type="hidden" name="res[actif_link_pdf]" value="<?php if (isset($infores->actif_link_pdf) AND $infores->actif_link_pdf)echo $infores->actif_link_pdf ?>">
            </div>
            <div class="col-lg-3 text-left text-justify">
                <img style="width: 100%;height: auto" src="<?php echo base_url('assets/img/link_res_pdf.png')?>">
            </div>
            <div class="col-lg-3"></div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center p-4">
                <a onclick="
        if ($('#nom_pdf').val()===''){
            alert('Le nom pdf doit être senseigné');
        }else {$('#global_form').submit()}

    " id="submit_pdf" style="font-size: 15px" class="btn btn-success">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Valider&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
            </div>
        </div>
    </form>
</div>
