<?php $data['empty'] = null;  ?>
<?php $data["zTitle"] = 'Mes Annonces' ?>
<?php $this->load->view("privicarte/includes/main_header", $data); ?>

        <div class="col-lg-12 padding0" style="text-align: center; display: none;"><img src="<?php echo base_url(); ?>application/resources/sortez/images/logo.png" alt="logo"></div>

    <style type="text/css">
        .btnmenu{
            text-align: center;
            margin-top: 39px;
            background-color: #af4106;
            width:300px;
            padding-bottom: 10px;
            padding-top:10px;
            color:white;
            font-size:15px;


        }
        .btn:visited{color: white!important;}
        .btn{color: white!important;}
        .titre{
            text-align: center;
            background-color: #af4106;
            color: white;
            height: 40px;
            line-height: 2.5;
        }
        .contenu{
            background-color: #e1e1e1;
            width: 700px;
            padding-top:10px;
            padding-bottom: 10px;
            margin:auto;
        }
        .btnvalide{
            width: 700px;
        }
        a{
            text-decoration: none;
        }
        .row{
            margin: auto;
        }
    </style>
<script type="text/javascript">


    function deleteFile(_IdPlat, Col) {
        jQuery.ajax({
            url: '<?php echo base_url();?>front/plat_du_jour/delete_photo/' + _IdPlat + '/' + Col,
            dataType: 'html',
            type: 'POST',
            async: true,
            success: function (data) {
                window.location.reload();
            }
        });
    }
</script>
    <div style="background-color: #fff; padding-bottom:40px;width: 75%;margin: auto">
        <form id="global_form" action="<?php echo site_url('front/Plat_du_jour/save_reservation_commercant_info/'.$idCommercant); ?>" method="POST"  enctype="multipart/form-data">
        <input name="res[id]" type="hidden" value="<?php if (isset($infores->id))echo $infores->id?>">
            <div class="row justify-content-center">
            <a href="<?php echo site_url();?>"><div class="col-sm-12 btnmenu">RETOUR SITE</div></a>
        </div>
        <div class="row justify-content-center">
            <a href="<?php echo site_url('auth/login');?>"><div class="col-sm-12 btnmenu" style="margin-top:20px;!important;">RETOUR MENU</div></a>
        </div>

                <div class="row">

                        <input type="hidden" name="res[IdCommercant]" value=" <?php echo $idCommercant; ?>">
                        <input type="hidden" name="res[IdUsers_ionauth]" value=" <?php echo $IdUsers_ionauth; ?>">
                        <input type="hidden" name="page_parent" value="plat_global">
                </div>
            
        <div class="row">
            <link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/") ; ?>/style.css" />
            <link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/") ; ?>/blue/style.css" />
            <link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/") ; ?>/jquery-ui-1.8.4.custom.css" />
            <link rel="stylesheet" media="screen" type="text/css" href="<?php echo GetCssPath("front/") ; ?>/demo_table_jui.css" />
            <script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/jquery.tablesorter.js"></script>
            <script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/jquery.tablesorter.pager.js"></script>
            <script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/ajaxfileupload.js"></script>
            <script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/jquery.ui.core.js"></script>
            <?php $data["zTitle"] = 'Mes Annonces'; ?>
            <?php
            $this->load->view("sortez/includes/backoffice_pro_css", $data);
            ?>
            <div class="row">
                <div class="col-sm-12 p-4 text-center">
                    <h1>Liste de mes Plats</h1>
                </div>
                <div class="col-sm-12 padding0 textaligncenter">
                     <a id="btnnew" class="btn btn-success" onclick="document.location='<?php echo site_url("front/Plat_du_jour/fichePlat/$idCommercant") ;?>';">Ajouter un plat</a>
                </div>
            </div>
            <div id="divMesAnnonces" class="content" align="center" style="display: table;">

                    <div id="container">
                        <table cellpadding="1" class="tablesorter">
                            <thead>
                            <tr>
                                <th>Description courte</th>
                                <th>Date début</th>
                                <th>Date fin</th>
                                <th>Prix</th>
                                <th>Etat</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i_order_partner = 1; ?>
                            <?php foreach($toListeplat as $oListeplat){ ?>
                                <tr>
                                    <td><?php echo $oListeplat->description_plat ; ?></td>
                                    <td>
                                        <?php
                                        if (isset($oListeplat->date_debut_plat) AND $oListeplat->date_debut_plat !="00/00/0000" AND $oListeplat->date_debut_plat !=''  AND $oListeplat->date_debut_plat !=null)
                                            echo convert_Sqldate_to_Frenchdate($oListeplat->date_debut_plat) ; ?>
                                    </td>
                                    <td>
                                        <?php
                                        if (isset($oListeplat->date_fin_plat) AND $oListeplat->date_debut_plat !="00/00/0000" AND  $oListeplat->date_debut_plat !='' AND $oListeplat->date_debut_plat !=null)
                                            echo convert_Sqldate_to_Frenchdate($oListeplat->date_fin_plat) ; ?>
                                    </td>
                                    <td><?php
                                        if ($oListeplat->prix_plat!=0)
                                            echo $oListeplat->prix_plat ; ?>
                                    </td>

                                    <td>
                                        <?php
                                        $zEtat = "";
                                        if ($oListeplat->IsActif == 0) $zEtat = "Inactif";
                                        else if ($oListeplat->IsActif == 1) $zEtat = "Actif";
                                        echo $zEtat;?>
                                    </td>

                                    <td><a href="<?php echo site_url("front/Plat_du_jour/vwmodifpalt/"  . $oListeplat->id ) ; ?>" title="Modifier"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/update_ico.png"></a></td>
                                    <td><a href="<?php echo site_url("front/Plat_du_jour/supprimplat/" . $oListeplat->id.'/'.$oListeplat->IdCommercant); ?>" onclick="if (!confirm('voulez-vous vraiment supprimer cette annoce ?')){ return false ; }" title="Supprimer"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/delete_ico.png"></a></td>
                                </tr>
                                <?php if (isset($oListeplat->order_partner) && $oListeplat->order_partner != "" && $oListeplat->order_partner != NULL) {} else $i_order_partner = $i_order_partner + 1; ?>
                            <?php } ?>
                            </tbody>
                        </table>
                        <div id="pager" class="pager">
                            <img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
                            <img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
                            <input type="text" class="pagedisplay"/>
                            <img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
                            <img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
                            <select class="pagesize" style="visibility:hidden">
                                <option selected="selected"  value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option  value="40">40</option>
                            </select>
                        </div>
                    </div>

            </div>
            <?php //$this->load->view("adminAout2013/includes/vwFooter2013"); ?>
        </div>
        </form>
    </div>
