<?php $data["zTitle"] = 'Mes Plats'; ?>
<?php //$this->load->view("adminAout2013/includes/vwHeader2013", $data); ?>

<?php
$data['empty'] = null;
$this->load->view("sortez/includes/backoffice_pro_css", $data);
?>

    <script type="text/javascript" charset="utf-8">

    </script>

    <script type="text/javascript">

    </script>

    <div class="col-lg-12 padding0">
        <div class="col-sm-4 padding0 textalignleft">
            <h1>Liste de mes plats</h1>
        </div>
        <div class="col-sm-4 padding0 textaligncenter">
            <button id="btnnewreturn" class="btn btn-primary" onclick="document.location='<?php echo base_url() ;?>';">Retour site</button>
            <button id="btnnewreturn" class="btn btn-primary" onclick="document.location='<?php echo site_url("front/utilisateur/contenupro") ;?>';">Retour au menu</button>
            <a style="color: white" id="" class="btn btn-success" href="<?php echo site_url('plat_du_jour/fiche')?>">Ajouter un nouveau plat</a>
        </div>
    </div>

    <div id="divMesAnnonces" class="content" align="center" style="display: table;">
            <div id="container">
                <table cellpadding="1" class="tablesorter">
                    <thead>
                    <tr>
                        <th>Description du plat</th>
                        <th>Date de disponibilité du plat</th>
                        <th>Prix</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i_order_partner = 1; ?>
                    <?php foreach($plat as $oListePlat){ ?>
                        <tr>
                            <td><?php echo $oListePlat->description_plat ; ?></td>
                            <td>
                                <?php
                                if (isset($oListePlat->date_jour) AND $oListePlat->date_jour !="00/00/0000" AND $oListePlat->date_jour !=''  AND $oListePlat->date_jour !=null)
                                    echo convert_Sqldate_to_Frenchdate($oListePlat->date_jour) ; ?></td>
                            <td><a href="<?php echo site_url("front/annonce/vwfiche_plat/" . $oListePlat->id . "/" . $oListePlat->id ) ; ?>" title="Modifier"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/update_ico.png"></a></td>
                            <td><a href="<?php echo site_url("front/annonce/delete_plat/" . $oListePlat->id . "/" . $oListePlat->id) ; ?>" onclick="if (!confirm('voulez-vous vraiment supprimer ce plat ?')){ return false ; }" title="Supprimer"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/delete_ico.png"></a></td>
                        </tr>
                        <?php if (isset($oListePlat->order_partner) && $oListePlat->order_partner != "" && $oListePlat->order_partner != NULL) {} else $i_order_partner = $i_order_partner + 1; ?>
                    <?php } ?>
                    </tbody>
                </table>
                <div id="pager" class="pager">
                    <img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
                    <img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
                    <input type="text" class="pagedisplay"/>
                    <img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
                    <img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
                    <select class="pagesize" style="visibility:hidden">
                        <option selected="selected"  value="10">10</option>
                        <option value="20">20</option>
                        <option value="30">30</option>
                        <option  value="40">40</option>
                    </select>
                </div>
            </div>
    </div>
<?php //$this->load->view("adminAout2013/includes/vwFooter2013"); ?>