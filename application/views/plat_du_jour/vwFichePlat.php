<html>
<head>
    <link href="<?php echo Base_Url("")?> rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style type="text/css">
        .btnmenu{
            text-align: center;
            margin-top: 39px;
            background-color: #af4106;
            width:300px;
            padding-bottom: 10px;
            padding-top:10px;
            color:white;
            font-size:20px;


        }
        .titre{
            text-align: center;
            background-color: #af4106;
            color: white;
            height: 40px;
            line-height: 2.5;
        }
        .contenu{
            background-color: #e1e1e1;
            width: 700px;
            padding-top:10px;
            padding-bottom: 10px;
            margin:auto;
        }
        .btnvalide{
            width: 700px;
        }
        a{
            text-decoration: none;
        }
    </style>
</head>
<body>
<div class="container">

    <div class="row justify-content-center">
        <a href="<?php echo site_url();?>"><div class="col-sm-12 btnmenu">RETOUR SITE</div></a>
    </div>
    <div class="row justify-content-center">
        <a href="<?php echo site_url('auth/login');?>"><div class="col-sm-12 btnmenu" style="margin-top:20px;!important;">RETOUR MENU</div></a>
    </div>
    <div class="row justify-content-center">
        <a href="<?php echo site_url('front/Plat_du_jour/listePlatDuJour/');?>"><div class="col-sm-12 btnmenu" style="margin-top:20px;!important;">RETOUR LISTE</div></a>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-12 titre" style="width:700px;margin:auto;!important;margin-top: 30px">LA RESERVATION DE NOS PLATS DE JOUR</div>
            <div class="contenu col-sm-12" >
                <form action="<?php echo site_url('front/Plat_du_jour/saveplat'); ?>" method="POST">

                    <input type="hidden" name="plat[IdCommercant]" value=" <?php echo $idCommercant; ?>">
                    <input type="hidden" name="plat[IdUsers_ionauth]" value=" <?php echo $IdUsers_ionauth; ?>">
                    <table>
                        <tr>
                            <td>date debut<td><td><input class="form-control"  type="date" name="plat[date_debut_plat]" ><td>
                        </tr>
                        <tr>
                            <td>date fin<td><td><input class="form-control"  type="date" name="plat[date_fin_plat]" ><td>
                        </tr>
                        <tr>
                            <td>Précisez l’heure de debut de réservation<td><td><input class="form-control"  type="time" name="plat[heure_debut_reservation]"><td>
                        </tr>
                        <tr>
                            <td>Précisez l’heure de fin de réservation<td><td><input class="form-control"  type="time" name="plat[heure_fin_reservation]"><td>
                        </tr>
                        <tr>
                            <td>Texte plat du jour<td><td><input class="form-control"  type="text" name="plat[description_plat]" ><td>
                        </tr>
                        <tr>
                            <td>Prix plat du jour<td><td><input class="form-control"  type="number" name="plat[prix_plat]" ><td>
                        </tr>
                        <tr>
                            <td>Nombre de plats du jour proposésr<td><td><input class="form-control"  type="number" name="plat[nbre_plat_propose]"><td>
                        </tr>
                        <tr>
                            <td>Actif<td><td><select class="form-control" name="plat[IsActif]" style="width:250px;height;padding-top:5px;padding-bottom:5px;">
                                    <option value="0" >Inactif</option>
                                    <option value="1" >Actif</option>

                                </select><td>
                        </tr>
                    </table>
                    <div class="p-4" style="color: red">
                        NB : Pour l'image,Vous devez enregistrer d'abord pour pouvoir integrer une image
                    </div>
            </div>
            
            <div class="col-sm-12 contenu text-center pt-4 pb-5" >
            <button onclick="return confirm('Valider les modifications?')" class="btn btn-success" type="submit"> Ajouter un plat du jour</button>
        </div>

    </div>

</div>
</div>

</div>



</form>
</div>

</div>
</body>
</html>