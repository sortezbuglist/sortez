<?php $data['empty'] = null;  ?>
<?php $data["zTitle"] = 'Mes Annonces' ?>
<?php $this->load->view("privicarte/includes/main_header", $data); ?>

<div class="col-lg-12 padding0" style="text-align: center; display: none;"><img src="<?php echo base_url(); ?>application/resources/sortez/images/logo.png" alt="logo"></div>

<style type="text/css">
    body{
        margin-top: 0px;
    }
    .btnmenu{
        text-align: center;
        margin-top: 39px;
        background-color: #af4106;
        width:300px;
        padding-bottom: 10px;
        padding-top:10px;
        color:white;
        font-size:15px;


    }
    .btn:visited{color: white!important;}
    .btn{color: white!important;}
    .titre{
        text-align: center;
        background-color: #af4106;
        color: white;
        height: 40px;
        line-height: 2.5;
    }
    .contenu{
        background-color: #e1e1e1;
        width: 700px;
        padding-top:10px;
        padding-bottom: 10px;
        margin:auto;
    }
    .btnvalide{
        width: 700px;
    }
    a{
        text-decoration: none;
    }
    .row{
        margin: auto;
    }
</style>
<script type="text/javascript">


    function deleteFile(_IdPlat, Col) {
        jQuery.ajax({
            url: '<?php echo base_url();?>front/plat_du_jour/delete_photo/' + _IdPlat + '/' + Col,
            dataType: 'html',
            type: 'POST',
            async: true,
            success: function (data) {
                window.location.reload();
            }
        });
    }
</script>
<div style="background-color: #fff; padding-bottom:40px;width: 75%;margin: auto">
    <form id="global_form" action="<?php echo site_url('front/Plat_du_jour/save_reservation_commercant_info/'.$idCommercant); ?>" method="POST"  enctype="multipart/form-data">
        <input name="res[id]" type="hidden" value="<?php if (isset($infores->id))echo $infores->id?>">
        <div class="row justify-content-center">
            <a href="<?php echo site_url();?>"><div class="col-sm-12 btnmenu">RETOUR SITE</div></a>
        </div>
        <div class="row justify-content-center">
            <a href="<?php echo site_url('auth/login');?>"><div class="col-sm-12 btnmenu" style="margin-top:20px;!important;">RETOUR MENU</div></a>
        </div>
        <div class="row">
            <div style="width:700px;margin:auto;!important;margin-top: 30px" class="col-sm-12 titre">MON ADRESSE MAIL POUR LA R&Eacute;SERVATION</div>
        </div>
        <div class="row p-4">
            <div class="col-lg-3"></div>
            <div class="col-lg-3 text-right text-justify">
                je précise mon adresse mail pour la réception des formulaires de réservation
            </div>
            <div class="col-lg-3 text-left text-justify">
                <input  value="<?php if (isset($infores->mail_reservation_plat)){echo $infores->mail_reservation_plat;}else{echo $infocom->Email; }?>" type="text" id="mail_res" name="res[mail_reservation_plat]" class="form-control">
            </div>
            <div class="col-lg-3"></div>
        </div>
        <div class="row p-4">
            <div class="col-lg-3"></div>
            <div class="col-lg-3 text-right text-justify">
                <input <?php if (isset($infores->actif_link_res_table) AND $infores->actif_link_res_table == 'on'){echo 'checked';} ?> onclick="
                                                                    if($('#activ_res_table').is(':checked')){
                                                                        $('#val_check_res').val('on');
                                                                    }else{
                                                                        $('#val_check_res').val('off');
                                                                    }
                                                                " type="checkbox" name="" id="activ_res_table">Je désire que ce bouton s’intègre sur la colonne des menus de mon site afin de bénéficier du formulaire de réservation en ligne
                <input id="val_check_res" type="hidden" name="res[actif_link_res_table]" value="<?php if (isset($infores->actif_link_res_table) AND $infores->actif_link_res_table)echo $infores->actif_link_res_table ?>">
            </div>
            <div class="col-lg-3 text-left text-justify">
                <img style="width: 100%;height: auto" src="<?php echo base_url('assets/img/btn_res_tab.png')?>">
            </div>
            <div class="col-lg-3">        </div><!-- 


            <div class="col-lg-3">        </div> -->

<div class="row p-4">
    <div class="col-lg-3 text-right text-justify">Précisez le texte du bouton</div>
    <div class="col-lg-3 text-right text-justify">
            <input style="width: 180px;" maxlength="22" value="<?php if (isset($infores->menu_value)) echo $infores->menu_value; ?>" type="text" class="form-control input_menu_txt" name="res[menu_value]">
            </div>
                </div><!-- 
                                             <div class="col-lg-3">        </div>
                                             <div class="col-lg-3">        </div> -->
                                            <div class="row p-4">
                                            <div class="col-lg-3 text-right text-justify">
                                                        choisissez la couleur du bouton en cliquant sur le panel  </div>

                                            <div class="col-lg-3 text-right text-justify">
                                                    <input type="color" name="res[btn_color]" style="width:150px; height: 150px"  value="<?php if (isset($infores->btn_color)) echo $infores->btn_color; ?>"> </div>
                                                        </div>
                                                      </div>
<!--             <div class="col-lg-3">        </div>
            <div class="col-lg-3">        </div> -->
        <div class="row p-4">
            <div class="col-lg-3 text-right text-justify">
            Télécharger l'image de personnalisation, Cliquez sur l'icone !...
            </div>
            <script type="text/javascript">
    function delete_image_res_menu(id_com){
        var data = "idcom="+id_com;
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('front/Plat_du_jour/delete_image_res_menu'); ?>"+id_com,
            data: data ,
            success: function() {
                if (data != "") {
                    var to_open = "'<?php echo site_url() ?>media/index/"+id_com+"-res_menu-photo_menu_gen"+""+"' , '' ,'width=1045 , height=675, scrollbars=yes'"
                    var to_ch = '<div id="Menuphoto_menu_gen_container" onclick="javascript:window.open('+to_open+');", href="javascript:void(0);" class="w-100" ><img  src="<?php echo base_url();?>assets/image/download-icon-png-5.jpg" style="height: 160px"></div>';
                    $("#img_menu_gen_contents").html(to_ch);
               }
            },
            error: function() {
                alert(data);
            }
        });
    }
</script>
            <div class="col-lg-3 text-right text-justify">
            
             <?php  if (isset($infores->image_menu_gen) AND $infores->image_menu_gen !=null && is_file("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/res_menu/".$infores->image_menu_gen)){ ?>
                    <div id="img_menu_gen_contents">
                    <img  style="max-width: 200px" src="<?php echo base_url()?>application/resources/front/photoCommercant/imagesbank/<?php echo $infocom->user_ionauth_id; ?>/res_menu/<?php echo $infores->image_menu_gen ?>">

                    <a onclick="delete_image_res_menu(<?php echo $infocom->IdCommercant; ?>)" class="btn btn-danger mt-3">Supprimer image</a>
                </div> 
            </div>
            <?php }else{ ?>
                <div class="w-100" id="Menuphoto_menu_gen_container">
                    <div class="w-100" id="Menuphoto_menu_gen_container">
                    <img onclick='javascript:window.open("<?php echo site_url("media/index/".$infocom->IdCommercant."-res_menu-photo_menu_gen"); ?>", "", "width=1045, height=675, scrollbars=yes");' src="<?php echo base_url()?>assets/image/download-icon-png-5.jpg">
                </div>
                <div id="Articleimg_menuphoto_menu_gen_container"></div>
                </div> 
            <?php } ?>
        </div>
    

        <div class="row">

            <input type="hidden" name="res[IdCommercant]" value=" <?php echo $idCommercant; ?>">
            <input type="hidden" name="res[IdUsers_ionauth]" value=" <?php echo $IdUsers_ionauth; ?>">
            <input type="hidden" name="page_parent" value="res_form">
        </div>
  

        <div class="row">
            <div style="width:700px;margin:auto;!important;margin-top: 30px" class="col-sm-12 titre justify-content-center">PRECISEZ VOS CRENEAUX HORAIRE DE RESERVATION</div>

            <div class="contenu col-sm-12 justify-content-center" >
                <div class="row" >
                    <div class="col-sm-4">
                        Midi<br>
                        <table align="center">
                            <tr>
                                <td> heure debut</td>
                                <td><select name="res[heure_debut_res_midi]">
                                        <option value="10:00" selected="selected">10:00</option>
                                        <option <?php if (isset($infores->heure_debut_res_midi) AND $infores->heure_debut_res_midi =='10:15')echo 'selected'?> value="10:15" >10:15</option>
                                        <option <?php if (isset($infores->heure_debut_res_midi) AND $infores->heure_debut_res_midi =='10:30')echo 'selected'?> value="10:30">10:30</option>
                                        <option <?php if (isset($infores->heure_debut_res_midi) AND $infores->heure_debut_res_midi =='10:45')echo 'selected'?> value="10:45">10:45</option>
                                        <option <?php if (isset($infores->heure_debut_res_midi) AND $infores->heure_debut_res_midi =='11:00')echo 'selected'?> value="11:00">11:00</option>
                                        <option <?php if (isset($infores->heure_debut_res_midi) AND $infores->heure_debut_res_midi =='11:15')echo 'selected'?> value="11:15" >11:15</option>
                                        <option <?php if (isset($infores->heure_debut_res_midi) AND $infores->heure_debut_res_midi =='11:30')echo 'selected'?> value="11:30">11:30</option>
                                        <option <?php if (isset($infores->heure_debut_res_midi) AND $infores->heure_debut_res_midi =='11:45')echo 'selected'?> value="11:45">11:45</option>
                                        <option <?php if (isset($infores->heure_debut_res_midi) AND $infores->heure_debut_res_midi =='12:00')echo 'selected'?> value="12:00">12:00</option>
                                        <option <?php if (isset($infores->heure_debut_res_midi) AND $infores->heure_debut_res_midi =='12:15')echo 'selected'?> value="12:15" >12:15</option>
                                        <option <?php if (isset($infores->heure_debut_res_midi) AND $infores->heure_debut_res_midi =='12:30')echo 'selected'?> value="12:30">12:30</option>
                                        <option <?php if (isset($infores->heure_debut_res_midi) AND $infores->heure_debut_res_midi =='12:45')echo 'selected'?> value="12:45">12:45</option>
                                        <option <?php if (isset($infores->heure_debut_res_midi) AND $infores->heure_debut_res_midi =='13:00')echo 'selected'?> value="13:00">13:00</option>
                                        <option <?php if (isset($infores->heure_debut_res_midi) AND $infores->heure_debut_res_midi =='13:15')echo 'selected'?> value="13:15" >13:15</option>
                                        <option <?php if (isset($infores->heure_debut_res_midi) AND $infores->heure_debut_res_midi =='130:30')echo 'selected'?>value="13:30">13:30</option>
                                        <option <?php if (isset($infores->heure_debut_res_midi) AND $infores->heure_debut_res_midi =='13:45')echo 'selected'?> value="13:45">13:45</option>
                                        <option <?php if (isset($infores->heure_debut_res_midi) AND $infores->heure_debut_res_midi =='14:00')echo 'selected'?> value="14:00">14:00</option>
                                    </select></td>
                            </tr>
                            <tr>
                                <td>heure fin</td>
                                <td><select name="res[heure_fin_res_midi]">
                                        <option value="10:00" selected="selected">10:00</option>
                                        <option <?php if (isset($infores->heure_fin_res_midi) AND $infores->heure_fin_res_midi =='10:15')echo 'selected'?> value="10:15" >10:15</option>
                                        <option <?php if (isset($infores->heure_fin_res_midi) AND $infores->heure_fin_res_midi =='10:30')echo 'selected'?> value="10:30">10:30</option>
                                        <option <?php if (isset($infores->heure_fin_res_midi) AND $infores->heure_fin_res_midi =='10:45')echo 'selected'?> value="10:45">10:45</option>
                                        <option <?php if (isset($infores->heure_fin_res_midi) AND $infores->heure_fin_res_midi =='11:00')echo 'selected'?> value="11:00">11:00</option>
                                        <option <?php if (isset($infores->heure_fin_res_midi) AND $infores->heure_fin_res_midi =='11:15')echo 'selected'?> value="11:15" >11:15</option>
                                        <option <?php if (isset($infores->heure_fin_res_midi) AND $infores->heure_fin_res_midi =='11:30')echo 'selected'?> value="11:30">11:30</option>
                                        <option <?php if (isset($infores->heure_fin_res_midi) AND $infores->heure_fin_res_midi =='11:45')echo 'selected'?> value="11:45">11:45</option>
                                        <option <?php if (isset($infores->heure_fin_res_midi) AND $infores->heure_fin_res_midi =='12:00')echo 'selected'?> value="12:00">12:00</option>
                                        <option <?php if (isset($infores->heure_fin_res_midi) AND $infores->heure_fin_res_midi =='12:15')echo 'selected'?> value="12:15" >12:15</option>
                                        <option <?php if (isset($infores->heure_fin_res_midi) AND $infores->heure_fin_res_midi =='12:30')echo 'selected'?> value="12:30">12:30</option>
                                        <option <?php if (isset($infores->heure_fin_res_midi) AND $infores->heure_fin_res_midi =='12:45')echo 'selected'?> value="12:45">12:45</option>
                                        <option <?php if (isset($infores->heure_fin_res_midi) AND $infores->heure_fin_res_midi =='13:00')echo 'selected'?> value="13:00">13:00</option>
                                        <option <?php if (isset($infores->heure_fin_res_midi) AND $infores->heure_fin_res_midi =='13:15')echo 'selected'?> value="13:15" >13:15</option>
                                        <option <?php if (isset($infores->heure_fin_res_midi) AND $infores->heure_fin_res_midi =='13:30')echo 'selected'?> value="13:30">13:30</option>
                                        <option <?php if (isset($infores->heure_fin_res_midi) AND $infores->heure_fin_res_midi =='13:45')echo 'selected'?> value="13:45">13:45</option>
                                        <option <?php if (isset($infores->heure_fin_res_midi) AND $infores->heure_fin_res_midi =='14:00')echo 'selected'?> value="14:00">14:00</option>
                                    </select></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-4">
                        Aprè-Midi<br>
                        <table align="center">
                            <tr>
                                <td>heure debut</td>
                                <td><select name="res[heure_debut_res_apmidi]" >
                                        <option value="14:15" selected="selected">14:15</option>
                                        <option <?php if (isset($infores->heure_debut_res_apmidi) AND $infores->heure_debut_res_apmidi =='14:30')echo 'selected'?> value="14:30">14:30</option>
                                        <option <?php if (isset($infores->heure_debut_res_apmidi) AND $infores->heure_debut_res_apmidi =='14:45')echo 'selected'?> value="14:45">14:45</option>
                                        <option <?php if (isset($infores->heure_debut_res_apmidi) AND $infores->heure_debut_res_apmidi =='15:00')echo 'selected'?> value="15:00">15:00</option>
                                        <option <?php if (isset($infores->heure_debut_res_apmidi) AND $infores->heure_debut_res_apmidi =='15:15')echo 'selected'?> value="15:15" >15:15</option>
                                        <option <?php if (isset($infores->heure_debut_res_apmidi) AND $infores->heure_debut_res_apmidi =='15:30')echo 'selected'?> value="15:30">15:30</option>
                                        <option <?php if (isset($infores->heure_debut_res_apmidi) AND $infores->heure_debut_res_apmidi =='15:45')echo 'selected'?> value="15:45">15:45</option>
                                        <option <?php if (isset($infores->heure_debut_res_apmidi) AND $infores->heure_debut_res_apmidi =='16:00')echo 'selected'?> value="16:00">16:00</option>
                                        <option <?php if (isset($infores->heure_debut_res_apmidi) AND $infores->heure_debut_res_apmidi =='16:15')echo 'selected'?> value="16:15" >16:15</option>
                                        <option <?php if (isset($infores->heure_debut_res_apmidi) AND $infores->heure_debut_res_apmidi =='16:30')echo 'selected'?> value="16:30">16:30</option>
                                        <option <?php if (isset($infores->heure_debut_res_apmidi) AND $infores->heure_debut_res_apmidi =='16:45')echo 'selected'?> value="16:45">16:45</option>
                                        <option <?php if (isset($infores->heure_debut_res_apmidi) AND $infores->heure_debut_res_apmidi =='17:00')echo 'selected'?> value="17:00">16:00</option>
                                        <option <?php if (isset($infores->heure_debut_res_apmidi) AND $infores->heure_debut_res_apmidi =='17:15')echo 'selected'?> value="17:15" >17:15</option>
                                        <option <?php if (isset($infores->heure_debut_res_apmidi) AND $infores->heure_debut_res_apmidi =='17:30')echo 'selected'?> value="17:30">17:30</option>
                                        <option <?php if (isset($infores->heure_debut_res_apmidi) AND $infores->heure_debut_res_apmidi =='17:45')echo 'selected'?> value="17:45">17:45</option>
                                        <option <?php if (isset($infores->heure_debut_res_apmidi) AND $infores->heure_debut_res_apmidi =='18:00')echo 'selected'?> value="18:00">18:00</option>
                                        <option <?php if (isset($infores->heure_debut_res_apmidi) AND $infores->heure_debut_res_apmidi =='18:15')echo 'selected'?> value="18:15" >18:15</option>
                                        <option <?php if (isset($infores->heure_debut_res_apmidi) AND $infores->heure_debut_res_apmidi =='18:30')echo 'selected'?> value="18:30">18:30</option>
                                        <option <?php if (isset($infores->heure_debut_res_apmidi) AND $infores->heure_debut_res_apmidi =='18:45')echo 'selected'?> value="18:45">18:45</option>
                                    </select></td>
                            </tr>
                            <tr>
                                <td>heure fin</td>
                                <td><select name="res[heure_fin_res_apmidi]" >
                                        <option <?php if (isset($infores->heure_fin_res_apmidi) AND $infores->heure_fin_res_apmidi =='14:30')echo 'selected'?> value="14:30">14:30</option>
                                        <option <?php if (isset($infores->heure_fin_res_apmidi) AND $infores->heure_fin_res_apmidi =='14:45')echo 'selected'?> value="14:45">14:45</option>
                                        <option <?php if (isset($infores->heure_fin_res_apmidi) AND $infores->heure_fin_res_apmidi =='15:00')echo 'selected'?> value="15:00">15:00</option>
                                        <option <?php if (isset($infores->heure_fin_res_apmidi) AND $infores->heure_fin_res_apmidi =='15:15')echo 'selected'?> value="15:15" >15:15</option>
                                        <option <?php if (isset($infores->heure_fin_res_apmidi) AND $infores->heure_fin_res_apmidi =='15:30')echo 'selected'?> value="15:30">15:30</option>
                                        <option <?php if (isset($infores->heure_fin_res_apmidi) AND $infores->heure_fin_res_apmidi =='15:45')echo 'selected'?> value="15:45">15:45</option>
                                        <option <?php if (isset($infores->heure_fin_res_apmidi) AND $infores->heure_fin_res_apmidi =='16:00')echo 'selected'?> value="16:00">16:00</option>
                                        <option <?php if (isset($infores->heure_fin_res_apmidi) AND $infores->heure_fin_res_apmidi =='16:15')echo 'selected'?> value="16:15" >16:15</option>
                                        <option <?php if (isset($infores->heure_fin_res_apmidi) AND $infores->heure_fin_res_apmidi =='16:30')echo 'selected'?> value="16:30">16:30</option>
                                        <option <?php if (isset($infores->heure_fin_res_apmidi) AND $infores->heure_fin_res_apmidi =='16:45')echo 'selected'?> value="16:45">16:45</option>
                                        <option <?php if (isset($infores->heure_fin_res_apmidi) AND $infores->heure_fin_res_apmidi =='17:00')echo 'selected'?> value="17:00">16:00</option>
                                        <option <?php if (isset($infores->heure_fin_res_apmidi) AND $infores->heure_fin_res_apmidi =='17:15')echo 'selected'?> value="17:15" >17:15</option>
                                        <option <?php if (isset($infores->heure_fin_res_apmidi) AND $infores->heure_fin_res_apmidi =='17:30')echo 'selected'?> value="17:30">17:30</option>
                                        <option <?php if (isset($infores->heure_fin_res_apmidi) AND $infores->heure_fin_res_apmidi =='17:45')echo 'selected'?> value="17:45">17:45</option>
                                        <option <?php if (isset($infores->heure_fin_res_apmidi) AND $infores->heure_fin_res_apmidi =='18:00')echo 'selected'?> value="18:00">18:00</option>
                                        <option <?php if (isset($infores->heure_fin_res_apmidi) AND $infores->heure_fin_res_apmidi =='18:15')echo 'selected'?> value="18:15" >18:15</option>
                                        <option <?php if (isset($infores->heure_fin_res_apmidi) AND $infores->heure_fin_res_apmidi =='18:30')echo 'selected'?> value="18:30">18:30</option>
                                        <option <?php if (isset($infores->heure_fin_res_apmidi) AND $infores->heure_fin_res_apmidi =='18:45')echo 'selected'?> value="18:45">18:45</option>
                                    </select></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-4">
                        Soir<br>
                        <table align="center">
                            <tr>
                                <td>heure debut</td>
                                <td><select name="res[heure_debut_res_soir]" >
                                        <option value="19:00" selected="selected">19:00</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='19:15')echo 'selected'?> value="19:15" >19:15</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='19:30')echo 'selected'?> value="19:30">19:30</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='19:45')echo 'selected'?> value="19:45">19:45</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='20:00')echo 'selected'?> value="20:00">20:00</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='20:15')echo 'selected'?> value="20:15" >20:15</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='20:30')echo 'selected'?> value="20:30">20:30</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='20:45')echo 'selected'?> value="20:45">20:45</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='21:00')echo 'selected'?> value="21:00">21:00</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='21:15')echo 'selected'?> value="21:15" >21:15</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='21:30')echo 'selected'?> value="21:30">21:30</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='21:45')echo 'selected'?> value="21:45">21:45</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='22:00')echo 'selected'?> value="22:00">22:00</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='22:15')echo 'selected'?> value="22:15" >22:15</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='22:30')echo 'selected'?> value="22:30">22:30</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='22:45')echo 'selected'?> value="22:45">22:45</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='23:00')echo 'selected'?> value="23:00">23:00</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='23:15')echo 'selected'?> value="23:15" >23:15</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='23:30')echo 'selected'?> value="23:30">23:30</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='23:45')echo 'selected'?> value="23:45">23:45</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='00:00')echo 'selected'?> value="24:00">00:00</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='00:15')echo 'selected'?> value="24:15" >00:15</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='00:30')echo 'selected'?> value="24:30">00:30</option>
                                        <option <?php if (isset($infores->heure_debut_res_soir) AND $infores->heure_debut_res_soir =='00:45')echo 'selected'?> value="24:45">00:45</option>
                                    </select></td>
                            </tr>
                            <tr>
                                <td>heure fin</td>
                                <td><select name="res[heure_fin_res_soir]" >
                                        <option value="19:00" selected="selected">19:00</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='19:15')echo 'selected'?> value="19:15" >19:15</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='19:30')echo 'selected'?> value="19:30">19:30</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='19:45')echo 'selected'?> value="19:45">19:45</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='20:00')echo 'selected'?> value="20:00">20:00</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='20:15')echo 'selected'?> value="20:15" >20:15</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='20:30')echo 'selected'?> value="20:30">20:30</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='20:45')echo 'selected'?> value="20:45">20:45</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='21:00')echo 'selected'?> value="21:00">21:00</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='21:15')echo 'selected'?> value="21:15" >21:15</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='21:30')echo 'selected'?> value="21:30">21:30</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='21:45')echo 'selected'?> value="21:45">21:45</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='22:00')echo 'selected'?> value="22:00">22:00</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='22:15')echo 'selected'?> value="22:15" >22:15</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='22:30')echo 'selected'?> value="22:30">22:30</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='22:45')echo 'selected'?> value="22:45">22:45</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='23:00')echo 'selected'?> value="23:00">23:00</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='23:15')echo 'selected'?> value="23:15" >23:15</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='23:30')echo 'selected'?> value="23:30">23:30</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='23:45')echo 'selected'?> value="23:45">23:45</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='00:00')echo 'selected'?> value="24:00">00:00</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='00:15')echo 'selected'?> value="24:15" >00:15</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='00:30')echo 'selected'?> value="24:30">00:30</option>
                                        <option <?php if (isset($infores->heure_fin_res_soir) AND $infores->heure_fin_res_soir =='00:45')echo 'selected'?> value="24:45">00:45</option>
                                    </select></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 text-center p-4">
                <a onclick="
                if ($('#mail_res').val()===''){
                    alert('Le champ Adresse Email doit être renseigné');
                }else{
                    adresse = $('#mail_res').val();

                    if (adresse != '') {
                    var exp = /^[a-z\d_\-]+(\.[\a-z\d\-]+)*@[a-z\d\-]+(\.[a-z\d]+)+$/;
                    if (! exp.test(adresse) ) {
                    alert('Veuillez inserer une mail valide'); }
                    $('#global_form').submit();
                    }
                    }


                            " style="font-size: 15px" class="btn btn-success">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Valider&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
            </div>
        </div>
    </form>
    </div> 
</div>
