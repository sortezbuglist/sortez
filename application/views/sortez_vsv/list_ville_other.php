<table class="table table-striped">
    <thead>
    <tr>
        <th>#</th>
        <th>Code postal</th>
        <th>Identification</th>
        <th>Ville référencée</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 1;
    if (isset($ville_data)) {
        foreach ($ville_data as $item) { ?>
            <tr>
                <th scope="row"><?php echo $i; ?></th>
                <td><?php echo $item->postal_code; ?></td>
                <td><?php echo $item->name; ?></td>
                <td></td>
                <td class="">
                    <div class="vsv_modal_container">
                        <a href="javascript:void(0);"
                           id="edit_autre_ville_<?php echo $i;?>"
                           attachmentid="<?php echo base_url(); ?>vivresaville/page/edit_autre_ville/<?php if (isset($item->id_vsv)) echo $item->id_vsv; else echo "0"; ?>/<?php if (isset($item->id)) echo $item->id; else echo "0"; ?>"
                           class="btn btn-primary vsv_modal_link">Modifier</a>
                    </div>
                </td>
                <td class="">
                    <div class="vsv_modal_container">
                        <a href="javascript:void(0);"
                           id="delete_autre_ville_<?php echo $i;?>"
                           attachmentid="<?php echo base_url(); ?>vivresaville/page/delete_autre_ville/<?php if (isset($item->id_vsv)) echo $item->id_vsv; else echo "0"; ?>/<?php if (isset($item->id)) echo $item->id; else echo "0"; ?>"
                           class="btn btn-primary vsv_modal_link">Supprimer</a>
                    </div>
                </td>
            </tr>
            <?php
            $i++;
        }
    } ?>
    </tbody>
</table>

