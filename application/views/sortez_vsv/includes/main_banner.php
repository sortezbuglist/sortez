<?php

$thiss = &get_instance();

$thiss->load->library('session');

$thiss->load->model('vsv_ville_other');

$localdata_IdVille = $thiss->session->userdata('localdata_IdVille');

$localdata_IdVille_parent = $thiss->session->userdata('localdata_IdVille_parent');

$localdata_IdVille_all = $thiss->session->userdata('localdata_IdVille_all');

$main_width_device = $thiss->session->userdata('main_width_device');

$main_mobile_screen = false;

$localdata_IdDepartement = $thiss->session->userdata('iDepartementId_x');

$localdata_IdCategory = $thiss->session->userdata('iCategorieId_x');

$localdata_iSubCategorieId = $thiss->session->userdata('iSubCategorieId_x');

$localdata_zMotCle = $thiss->session->userdata('zMotCle_x');



if (isset($main_width_device) && $main_width_device != "" && $main_width_device != "0" && is_numeric($main_width_device) && floatval($main_width_device) <= 991) {

    $main_mobile_screen = true;

} else {

    $main_mobile_screen = false;

}

$thiss->load->model("vivresaville_villes");

$logo_to_show_vsv = base_url() . "assets/ville-test/wpimages/logo_ville_test.png";

if (isset($localdata_IdVille_parent) && $localdata_IdVille_parent != "" && $localdata_IdVille_parent != false) {

    $vsv_id_vivresaville = $thiss->vivresaville_villes->getByIdVille($localdata_IdVille_parent);

    if (isset($vsv_id_vivresaville) && isset($vsv_id_vivresaville->id)) {

        $vsv_object = $thiss->vivresaville_villes->getById($vsv_id_vivresaville->id);

        $vsv_other_ville = $thiss->vsv_ville_other->getByIdVsv($vsv_id_vivresaville->id);

    }

    if (isset($vsv_object) && isset($vsv_object->logo) && file_exists("application/resources/front/images/vivresaville/" . $vsv_object->logo)) {

        $logo_to_show_vsv = base_url() . "application/resources/front/images/vivresaville/" . $vsv_object->logo;

    }

}

?>



<div class="mt-lg-5 mb-lg-5 main_banner_container_soutenons">

    <div class="" style="background-color: white!important;padding-top: 10px;padding-bottom: 25px">

        <div class="col-lg-12">

            <p class="gros_titre">

                <?php

                if ($zTitle == "Annuaire") {

                    // echo "Les bonnes adresses";

                    echo "Répertoire";

                } else if ($zTitle == "Boutique soutenons") {

                    echo "Recherches par annonces";

                } else if ($zTitle == "Article") {

                    echo "L'actualité & la revue de presse";

                }

                ?>

            </p>

        </div>

        <div class="col-lg-12 text-center">



            <div class="">

                <div class="row">

                    <?php if ($zTitle == "Annuaire") { ?>

                        <div class="containercontainer">

                            <div class="row">

                                <div class="col-lg-4"></div>

                                <div class="col-lg-4">

                                    <div class="dropdown">

                                        <!-- <div class="btn btn-secondary dropdown-toggle border_content_soutenons_dep" type="button" id="Recherches_par_departement" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                            <?php

                                            // if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != 0 && $localdata_IdDepartement != "0" && intval($localdata_IdDepartement) != 9999) {

                                            //     if (isset($toDepartement) && $toDepartement != "" && $toDepartement != 0 && $toDepartement != "0") {

                                            //         foreach ($toDepartement as $oDepartement) {

                                            //             if ($oDepartement->departement_id == $localdata_IdDepartement)

                                            //                 echo $oDepartement->departement_nom; //$oDepartement->nbCommercant

                                            //         }

                                            //     }

                                            // } else echo 'Recherches par département';

                                            ?>

                                        </div> -->

                                        <div style="font-size:12px;" class="dropdown-menu main_communes_select" id="div_Recherches_par_departement" aria-labelledby="dropdownMenuButton_com">

                                            <?php

                                            if ($zTitle == "Annuaire" || $zTitle == "Boutique soutenons" || $zTitle == "Article") {

                                                if (isset($toDepartement) && $toDepartement != "" && $toDepartement != 0 && $toDepartement != "0") {

                                                    foreach ($toDepartement as $oDepartement) {

                                            ?>

                                                        <a class="dropdown-item" id="department_select_id_<?= $oDepartement->departement_id ?>" href="javascript:void(0);" onclick="

                                                                   javascript:pvc_select_departement(<?php echo $oDepartement->departement_id; ?>);

                                                                   javascript:change_commune_by_department_soutenons(<?php echo $oDepartement->departement_id; ?>);

                                                                   javascript:change_categories_by_department_soutenons(<?php echo $oDepartement->departement_id; ?>);

                                                                   javascript:change_subcategories_by_department_soutenons(<?php echo $oDepartement->departement_id; ?>);

                                                                   "><?php echo $oDepartement->departement_nom . " (" . $oDepartement->nbCommercant . ") "; // $oDepartement->nbCommercant 

                                                                        ?></a>

                                                    <?php

                                                    }

                                                }

                                            } else {

                                                foreach ($toDepartement as $toDepartements) {

                                                    ?>

                                                    <a class="dropdown-item" id="department_select_id_<?= $toDepartements['departement_id'] ?>" href="javascript:void(0);" onclick="javascript:pvc_select_departement(<?php echo $toDepartements['departement_id']; ?>);"><?php echo $toDepartements['departement_nom'] . " - " . $toDepartements['nbCommercant']; // $toDepartements['nbCommercant'] 

                                                                                                                                                                                                                                                                            ?></a>

                                            <?php

                                                }

                                            }

                                            ?>

                                        </div>

                                    </div>

                                </div>

                                <div class="col-lg-4"></div>

                            </div>

                            <script type="text/javascript">

                                function pvc_select_departement(departement_id) {

                                    $("#inputStringDepartementHidden_partenaires").val(departement_id);

                                    $("#Recherches_par_departement").html($("#department_select_id_" + departement_id).html());

                                    $("#dropdownMenuButton_com").html("Recherche par communes");

                                    $("#dropdownMenuButton_com_categ").html("Recherche par catégories");

                                    $("#dropdownMenuButton_com_sucateg").html("Recherche par sous catégories");

                                    $("#inputStringVilleHidden_partenaires").val("");

                                    $("#inputStringHidden").val("");

                                    $("#inputStringHiddenSubCateg").val("");

                                }

                            </script>



                            <div class="row pl-0 pr-3 pr-sm-0">

                                <div class="pr-0 pr-sm-3 col-6 col-sm-12 col-lg-4 pt-2">

                                    <?php $data['empty'] = null;

                                    $this->load->view("sortez_vsv/includes/main_communes_select", $data); ?>

                                </div>

                                <div class="pr-0 pr-sm-3 col-6 col-sm-12 col-lg-4 pt-2">

                                    <div class="dropdown">

                                        <div class="btn btn-secondary dropdown-toggle border_content_soutenons" type="text" id="dropdownMenuButton_com_categ" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                            <span class="d-none d-sm-inline-block">Recherche par catégories</span>

                                            <span class="d-contents d-sm-none d-md-none d-ld-none d-xl-none">Catégories</span>

                                        </div>

                                        <div style="font-size:12px;" class="dropdown-menu main_communes_select" id="<?php if ($pagecategory == "annuaire") {

                                                                                                                        echo "span_leftcontener2013_form_partenaires";

                                                                                                                    } else if ($pagecategory == "annonce") {

                                                                                                                        echo "span_leftcontener2013_form_annonces";

                                                                                                                    } else {

                                                                                                                        echo "span_leftcontener2013_form_deal";

                                                                                                                    } ?>" aria-labelledby="dropdownMenuButton_com">

                                            <?php if ($toallcateg != '' && $toallcateg != null) { ?>

                                                <?php foreach ($toallcateg as $toctages) { ?>

                                                    <a class="dropdown-item" href="javascript:void(0);" onclick="javascript:pvc_select_departement(<?php echo $toctages->IdRubrique; ?>);"><?php echo $toctages->rubrique . " (" . $toctages->nbCommercant . ")"; ?></a>

                                                <?php } ?>

                                            <?php } ?>

                                        </div>

                                    </div>

                                </div>



                                <script type="text/javascript">

                                    function show_current_subcategcontent_soutenons(id_sousrubrique) {

                                        $("#inputStringHiddenSubCateg").val(id_sousrubrique);

                                        //$("#frmRecherchePartenaire").submit();

                                        //$("#div_subcateg_annuaire_contents").css("display","none");

                                        $("#dropdownMenuButton_com_sucateg").html($("#vsv_sub_categmain_" + id_sousrubrique).html());

                                    }

                                </script>



                                <div class="pr-0 pr-sm-3 col-6 col-sm-12 col-lg-4 pt-2">

                                    <?php

                                    if ($pagecategory == "annuaire" || $pagecategory == "annonce") { ?>

                                        <div class="dropdown">

                                            <div class="btn btn-secondary dropdown-toggle border_content_soutenons" type="text" id="dropdownMenuButton_com_sucateg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                                <?php

                                                if (isset($localdata_iSubCategorieId) && $localdata_iSubCategorieId != "" && $localdata_iSubCategorieId != 0 && $localdata_iSubCategorieId != "0") {

                                                    if (isset($toSubCategorie) && $toSubCategorie != "" && $toSubCategorie != 0 && $toSubCategorie != "0") {

                                                        foreach ($toSubCategorie as $oSubCategorie) {

                                                            if ($oSubCategorie->IdSousRubrique == $localdata_iSubCategorieId)

                                                                echo $oSubCategorie->Nom;

                                                        }

                                                    }

                                                } else {

                                                ?>

                                                    <span class="d-none d-sm-inline-block">Recherche par sous catégories</span>

                                                    <span class="d-contents d-sm-none d-md-none d-ld-none d-xl-none">Sous catégories</span>

                                                <?php } ?>

                                            </div>

                                            <div style="font-size:12px;" class="dropdown-menu main_communes_select" id="div_subcateg_annuaire_contents" aria-labelledby="dropdownMenuButton_com">

                                            </div>

                                        </div>

                                    <?php } else { ?>

                                        <div class="dropdown">

                                            <div class="btn btn-secondary dropdown-toggle border_content_soutenons" type="text" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                                Deals

                                                ou

                                                fidélité

                                            </div>

                                            <div style="font-size:12px;" class="dropdown-menu main_communes_select" aria-labelledby="dropdownMenuButton_com">

                                                <a class="dropdown-item" href="#">Toutes

                                                    les

                                                    offres</a>

                                                <a class="dropdown-item" href="#">Les

                                                    deals</a>

                                                <a class="dropdown-item" href="#">La

                                                    fidélité</a>

                                            </div>

                                        </div>

                                    <?php } ?>

                                </div>

                                <?php

                                if ($zTitle == "Annuaire") { ?>

                                    <div class="pr-0 pr-sm-3 col-6 col-sm-12 col-lg-4 pt-2">

                                        <input class=" border_content_soutenons" id="zMotcle_value" type="text" placeholder="Recherches par mot clé" value="<?php if (isset($localdata_zMotCle) && $localdata_zMotCle != '') echo $localdata_zMotCle; ?>">

                                    </div>

                                <?php } else if ($zTitle == "Boutique soutenons") { ?>

                                    <div class="pr-0 pr-sm-3 col-6 col-sm-12 col-lg-4 pt-2">

                                        <div class="dropdown">

                                            <div class="btn btn-secondary dropdown-toggle border_content_soutenons" type="text" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                                Filtrer

                                            </div>

                                            <div style="font-size:12px;" class="dropdown-menu main_communes_select" aria-labelledby="dropdownMenuButton_com">

                                                <a class="dropdown-item" href="#">Neuf</a>

                                                <a class="dropdown-item" href="#">Revente</a>

                                                <a class="dropdown-item" href="#">Service</a>

                                            </div>

                                        </div>

                                    </div>

                                <?php } else { ?>

                                    <div class="pr-0 pr-sm-3 col-6 col-sm-12 col-lg-4 pt-2">

                                        <input class=" border_content_soutenons" id="zMotcle_value" type="text" placeholder="Recherches par mot clé" value="<?php if (isset($localdata_zMotCle) && $localdata_zMotCle != '') echo $localdata_zMotCle; ?>">

                                    </div>

                                <?php } ?>

                                <div class="col-sm-12 col-lg-4 pt-2 pr-0 pr-sm-3">

                                    <div class="btn btn-secondary reinit_soutenons"><input id="filter_coronna_ligne" class="check_filter" type="checkbox" />

                                        <!-- <div class="text_input">Coronavirus eto : établissements ouverts</div> -->

                                        <div class="text_input">Restaurants : réservations & plats du jour</div>

                                    </div>

                                </div>

                                <div class="col-sm-12 col-lg-4 text-left pt-2 pr-0 pr-sm-3">

                                    <div class="btn btn-secondary reinit_soutenons">

                                        <input id="filter_com_en_ligne" class="check_filter" type="checkbox" />

                                        <div class="text_input">Formulaires de commande en ligne</div>

                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-sm-12 col-lg-4 offset-lg-2 pt-2">

                                    <div class="btn btn-secondary reinit_soutenons_down w-100" onclick="<?php if ($zTitle == "Annuaire") {

                                                                                                            echo "btn_re_init_annuaire_list();";

                                                                                                        } else if ($zTitle == "Boutique soutenons") {

                                                                                                            echo "btn_re_init_annonce_list();";

                                                                                                        } else {

                                                                                                            echo "btn_re_init_annonce_list();";

                                                                                                        } ?>">

                                        Réinitialisez

                                    </div>

                                </div>

                                <div class="col-sm-12 col-lg-4 pt-2">

                                    <div class="btn btn-secondary applic_soutenons_down w-100" onclick="javascript:form_submit_search_annuaire_soutenons();">

                                        Appliquez

                                        votre

                                        choix

                                        

                                    </div>

                                    <script type="text/javascript">

                                        function form_submit_search_annuaire_soutenons(keyword_value) {

                                            var base_url_visurba = '<?php echo site_url(); ?>';

                                            jQuery("#Id_main_bodylistannuaire_main").html('<div class="text-center w-100"><img src="' + base_url_visurba + 'application/resources/front/images/wait.gif" alt="loading...."/></div>');

                                            $("#frmRecherchePartenaire #zMotCle").val($("#zMotcle_value").val());

                                            

                                            

                                            var inputStringDepartementHidden_partenaires = document.getElementById("inputStringDepartementHidden_partenaires").value;

                                            var inputStringVilleHidden_partenaires = document.getElementById("inputStringVilleHidden_partenaires").value;

                                            var inputStringHidden = document.getElementById("inputStringHidden").value;

                                            var inputStringHiddenSubCateg = document.getElementById("inputStringHiddenSubCateg").value;

                                            var inputStringOrderByHidden = document.getElementById("inputStringOrderByHidden").value;

                                            var zMotCle = document.getElementById("zMotCle").value;                                            

                                            var inputAvantagePartenaire = document.getElementById("inputAvantagePartenaire").value;

                                            var input_is_actif_coronna = document.getElementById("input_is_actif_coronna").value;

                                            var input_is_actif_command = document.getElementById("input_is_actif_command").value;





                                            jQuery.ajax({

                                                method: "POST",

                                                url: '<?php echo site_url("annuaire/index/?content_only_list=1"); ?>',

                                                data: {

                                                    inputStringDepartementHidden: inputStringDepartementHidden_partenaires,

                                                    inputStringVilleHidden: inputStringVilleHidden_partenaires,

                                                    inputStringHidden: inputStringHidden,

                                                    inputStringHiddenSubCateg: inputStringHiddenSubCateg,

                                                    inputStringOrderByHidden: inputStringOrderByHidden,

                                                    zMotCle: zMotCle,

                                                    inputAvantagePartenaire: inputAvantagePartenaire,

                                                    input_is_actif_coronna: input_is_actif_coronna,

                                                    input_is_actif_command: input_is_actif_command

                                                },

                                                dataType: 'html',

                                                success: function(html) {

                                                    jQuery('#Id_main_bodylistannuaire_main').html(html);

                                                    console.log(jQuery('#Id_main_bodylistannuaire_main').html(html));

                                                }

                                            });



                                            //$("#frmRecherchePartenaire").submit();

                                        }



                                        function btn_re_init_annuaire_list() {

                                            document.getElementById("inputStringDepartementHidden_partenaires").value = "";



                                            document.getElementById("inputStringVilleHidden_partenaires").value = "";



                                            document.getElementById("inputStringHidden").value = "";



                                            document.getElementById("inputStringHiddenSubCateg").value = "";



                                            document.getElementById("inputStringOrderByHidden").value = "";



                                            document.getElementById("zMotcle_value").value = "";



                                            document.getElementById("inputAvantagePartenaire").value = "";



                                            $('#dropdownMenuButton_com_categ').html('Recherche par catégories');



                                            $('#Recherches_par_departement').html('Recherches par département');



                                            $('#dropdownMenuButton_com').html('Recherche par communes');



                                            $('#dropdownMenuButton_com_sucateg').html('Recherche par sous catégories');



                                            var getcorona = document.getElementById("input_is_actif_coronna").value;



                                            var getcommande = document.getElementById("input_is_actif_command").value;



                                            if (getcorona == 1) {



                                                $("#filter_coronna_ligne").prop("checked", false);

                                                document.getElementById("input_is_actif_coronna").value = 0;

                                            }



                                            if (getcommande == 1) {

                                                $("#filter_com_en_ligne").prop("checked", false);

                                                document.getElementById("input_is_actif_command").value = 0;

                                            }

                                            form_submit_search_annuaire_soutenons();

                                            // reinitilise category list with complete number

                                            var departement_id = $("#frmRecherchePartenaire #inputStringDepartementHidden_partenaires").val();

                                            

                                            var base_url_visurba = '<?php echo site_url();?>';

                                            jQuery("#span_leftcontener2013_form_partenaires").html('<img src="'+base_url_visurba+'application/resources/front/images/wait.gif" alt="loading...."/>');

                                            jQuery.post(

                                                base_url_visurba+'soutenons/Annuaire_Soutenons/check_category_list/',

                                                {

                                                    inputStringDepartementHidden_partenaires: departement_id,

                                                    inputStringVilleHidden_partenaires: inputStringVilleHidden_partenaires

                                                }

                                                ,

                                                function (zReponse)

                                                {

                                                    if (zReponse == "error") {

                                                        alert("Un probléme est suvenu, veuillez refaire l'opération !");

                                                    } else {

                                                        jQuery("#span_leftcontener2013_form_partenaires").html(zReponse);

                                                    }

                                            });

                                            





                                        }

                                    </script>

                                </div>

                            </div>

                        </div>

                    <?php } elseif ($zTitle == "Article") { ?>

                        <div class="content_soutenons_1">

                            <script type="text/javascript">

                                function pvc_select_departement(departement_id) {

                                    $("#inputStringDepartementHidden_partenaires").val(departement_id);

                                    $("#Recherches_par_departement").html($("#department_select_id_" + departement_id).html());

                                    $("#dropdownMenuButton_com").html("Recherche par communes");

                                    $("#dropdownMenuButton_com_categ").html("Recherche par catégories");

                                    $("#dropdownMenuButton_com_sucateg").html("Recherche par sous catégories");

                                    $("#inputStringVilleHidden_partenaires").val("");

                                    $("#inputStringHidden").val("");

                                    $("#inputStringHiddenSubCateg").val("");

                                }

                            </script>



                            <div class="row">

                                <div class="col-sm-12 col-lg-4 pt-2">

                                    <div class="dropdown">

                                        <div class="btn btn-secondary dropdown-toggle border_content_soutenons_dep" type="button" id="Recherches_par_departement" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                            <?php

                                            if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != 0 && $localdata_IdDepartement != "0" && intval($localdata_IdDepartement) != 9999) {

                                                if (isset($toDepartement) && $toDepartement != "" && $toDepartement != 0 && $toDepartement != "0") {

                                                    foreach ($toDepartement as $oDepartement) {

                                                        if ($oDepartement->departement_id == $localdata_IdDepartement)

                                                            echo $oDepartement->departement_nom; //$oDepartement->nbCommercant

                                                    }

                                                }

                                            } else echo 'Recherches par département';

                                            ?>

                                        </div>

                                        <div style="font-size:12px;" class="dropdown-menu main_communes_select" id="div_Recherches_par_departement" aria-labelledby="dropdownMenuButton_com">

                                            <?php

                                            if ($zTitle == "Article") {

                                                if (isset($toDepartement) && $toDepartement != "" && $toDepartement != 0 && $toDepartement != "0") {

                                                    foreach ($toDepartement as $oDepartement) {

                                            ?>

                                                        <a class="dropdown-item" id="department_select_id_<?= $oDepartement->departement_id ?>" href="javascript:void(0);" onclick="

                                                                           javascript:pvc_select_departement(<?php echo $oDepartement->departement_id; ?>);

                                                                           javascript:change_commune_by_department_soutenons(<?php echo $oDepartement->departement_id; ?>);

                                                                           javascript:change_categories_by_department_soutenons(<?php echo $oDepartement->departement_id; ?>);

                                                                           javascript:change_subcategories_by_department_soutenons(<?php echo $oDepartement->departement_id; ?>);

                                                                           "><?php echo $oDepartement->departement_nom . " (" . $oDepartement->nbCommercant . ") "; // $oDepartement->nbCommercant 

                                                                                ?></a>

                                            <?php

                                                    }

                                                }

                                            }

                                            ?>

                                        </div>

                                    </div>



                                </div>

                                <div class="col-sm-12 col-lg-4 pt-2">

                                    <div class="dropdown">

                                        <div class="btn btn-secondary dropdown-toggle border_content_soutenons_dep" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                            Recherches par partenaire

                                        </div>

                                        <div style="font-size:12px;" class="dropdown-menu main_communes_select" aria-labelledby="dropdownMenuButton_com">

                                            <a class="dropdown-item" href="#">Média 1</a>

                                            <a class="dropdown-item" href="#">Média 2</a>

                                            <a class="dropdown-item" href="#">Média 3</a>

                                        </div>

                                    </div>

                                </div>





                                <script type="text/javascript">

                                    function show_current_subcategcontent_soutenons(id_sousrubrique) {

                                        $("#inputStringHiddenSubCateg").val(id_sousrubrique);

                                        //$("#frmRecherchePartenaire").submit();

                                        //$("#div_subcateg_annuaire_contents").css("display","none");

                                        $("#dropdownMenuButton_com_sucateg").html($("#vsv_sub_categmain_" + id_sousrubrique).html());

                                    }

                                </script>



                                <div class="col-sm-12 col-lg-4 pt-2">

                                    <div class="dropdown">

                                        <div class="btn btn-secondary dropdown-toggle border_content_soutenons_dep" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                            Recherche par média

                                        </div>

                                        <div style="font-size:12px;" class="dropdown-menu main_communes_select" aria-labelledby="dropdownMenuButton_com">

                                            <a class="dropdown-item" href="#">Média 1</a>

                                            <a class="dropdown-item" href="#">Média 2</a>

                                            <a class="dropdown-item" href="#">Média 3</a>

                                        </div>

                                    </div>

                                </div>
                                

                                <div class="col-sm-12 col-lg-4 pt-2">

                                    <!--                                            <input class="btn btn-secondary dropdown-toggle border_content_soutenons"-->

                                    <!--                                                   id="zMotcle_value"-->

                                    <!--                                                   type="text"-->

                                    <!--                                                   placeholder="Recherches par mot clé"-->

                                    <!--                                                   value="--><?php //if (isset($localdata_zMotCle) && $localdata_zMotCle != '') echo $localdata_zMotCle; 

                                                                                                        ?>

                                    <!--">-->

                                    <?php $data['empty'] = null;

                                    $this->load->view("sortez_vsv/includes/main_communes_select", $data); ?>

                                </div>

                                <div class="col-sm-12 col-lg-4 pt-2">

                                    <div class="dropdown">

                                        <div class="btn btn-secondary dropdown-toggle border_content_soutenons" type="button" id="dropdownMenuButton_com_categ" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                            Recherche

                                            par

                                            catégories

                                        </div>

                                        <div style="font-size:12px;" class="dropdown-menu main_communes_select" id="span_leftcontener2013_form_article" aria-labelledby="dropdownMenuButton_com">

                                            <?php if ($toallcateg != '' && $toallcateg != null) { ?>

                                                <?php foreach ($toallcateg as $toctages) { ?>

                                                    <a class="dropdown-item" href="javascript:void(0);" onclick="javascript:pvc_select_departement(<?php echo $toctages->IdRubrique; ?>);"><?php echo $toctages->rubrique . " (" . $toctages->nbCommercant . ")"; ?></a>

                                                <?php } ?>

                                            <?php } ?>

                                        </div>

                                    </div>

                                </div>

                                <div class="col-sm-12 col-lg-4 text-left pt-2">

                                    <input class=" border_content_soutenons" id="zMotcle_value" type="text" placeholder="Recherches par mot clé" value="<?php if (isset($localdata_zMotCle) && $localdata_zMotCle != '') echo $localdata_zMotCle; ?>">

                                    <script type="text/javascript">

                                        function form_submit_search_annuaire_soutenons(keyword_value) {

                                            var base_url_visurba = '<?php echo site_url(); ?>';

                                            jQuery("#Id_main_bodylistannuaire_main").html('<div class="text-center w-100"><img src="' + base_url_visurba + 'application/resources/front/images/wait.gif" alt="loading...."/></div>');

                                            $("#frmRecherchePartenaire #zMotCle").val($("#zMotcle_value").val());



                                            var inputStringDepartementHidden_partenaires = document.getElementById("inputStringDepartementHidden_partenaires").value;

                                            var inputStringVilleHidden_partenaires = document.getElementById("inputStringVilleHidden_partenaires").value;

                                            var inputStringHidden = document.getElementById("inputStringHidden").value;

                                            var inputStringHiddenSubCateg = document.getElementById("inputStringHiddenSubCateg").value;

                                            var inputStringOrderByHidden = document.getElementById("inputStringOrderByHidden").value;

                                            var zMotCle = document.getElementById("zMotCle").value;

                                            var inputAvantagePartenaire = document.getElementById("inputAvantagePartenaire").value;

                                            var input_is_actif_coronna = document.getElementById("input_is_actif_coronna").value;

                                            var input_is_actif_command = document.getElementById("input_is_actif_command").value;

                                            //alert(inputStringDepartementHidden_partenaires);

                                            



                                            jQuery.ajax({

                                                method: "POST",

                                                url: '<?php echo site_url("annuaire/index/?content_only_list=1"); ?>',

                                                data: {

                                                    inputStringDepartementHidden: inputStringDepartementHidden_partenaires,

                                                    inputStringVilleHidden: inputStringVilleHidden_partenaires,

                                                    inputStringHidden: inputStringHidden,

                                                    inputStringHiddenSubCateg: inputStringHiddenSubCateg,

                                                    inputStringOrderByHidden: inputStringOrderByHidden,

                                                    zMotCle: zMotCle,

                                                    inputAvantagePartenaire: inputAvantagePartenaire,

                                                    input_is_actif_coronna: input_is_actif_coronna,

                                                    input_is_actif_command: input_is_actif_command

                                                },

                                                dataType: 'html',

                                                success: function(html) {

                                                    jQuery('#Id_main_bodylistannuaire_main').html(html);

                                                }

                                            });



                                            //$("#frmRecherchePartenaire").submit();

                                        }

                                    </script>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-sm-12 text-right col-lg-6 pt-2">

                                    <div class="btn btn-secondary reinit_soutenons_down" onclick="btn_re_init_annonce_list();">

                                        Réinitialisez

                                    </div>

                                </div>

                                <div class="col-sm-12 col-lg-6 text-left pt-2">

                                    <div class="btn btn-secondary applic_soutenons_down" onclick="javascript:form_submit_search_annuaire_soutenons();">

                                        Appliquez

                                        votre

                                        choix

                                    </div>

                                    <script type="text/javascript">

                                        function form_submit_search_annuaire_soutenons(keyword_value) {

                                            var base_url_visurba = '<?php echo site_url(); ?>';

                                            jQuery("#Id_main_bodylistannuaire_main").html('<div class="text-center w-100"><img src="' + base_url_visurba + 'application/resources/front/images/wait.gif" alt="loading...."/></div>');

                                            $("#frmRecherchePartenaire #zMotCle").val($("#zMotcle_value").val());



                                            var inputStringDepartementHidden_partenaires = document.getElementById("inputStringDepartementHidden_partenaires").value;

                                            var inputStringVilleHidden_partenaires = document.getElementById("inputStringVilleHidden_partenaires").value;

                                            var inputStringHidden = document.getElementById("inputStringHidden").value;

                                            var inputStringHiddenSubCateg = document.getElementById("inputStringHiddenSubCateg").value;

                                            var inputStringOrderByHidden = document.getElementById("inputStringOrderByHidden").value;

                                            var zMotCle = document.getElementById("zMotCle").value;

                                            var inputAvantagePartenaire = document.getElementById("inputAvantagePartenaire").value;

                                            var input_is_actif_coronna = document.getElementById("input_is_actif_coronna").value;

                                            var input_is_actif_command = document.getElementById("input_is_actif_command").value;





                                            //alert(inputStringDepartementHidden_partenaires);



                                            jQuery.ajax({

                                                method: "POST",

                                                url: '<?php echo site_url("annuaire/index/?content_only_list=1"); ?>',

                                                data: {

                                                    inputStringDepartementHidden: inputStringDepartementHidden_partenaires,

                                                    inputStringVilleHidden: inputStringVilleHidden_partenaires,

                                                    inputStringHidden: inputStringHidden,

                                                    inputStringHiddenSubCateg: inputStringHiddenSubCateg,

                                                    inputStringOrderByHidden: inputStringOrderByHidden,

                                                    zMotCle: zMotCle,

                                                    inputAvantagePartenaire: inputAvantagePartenaire,

                                                    input_is_actif_coronna: input_is_actif_coronna,

                                                    input_is_actif_command: input_is_actif_command

                                                },

                                                dataType: 'html',

                                                success: function(html) {

                                                    jQuery('#Id_main_bodylistannuaire_main').html(html);

                                                }

                                            });



                                            //$("#frmRecherchePartenaire").submit();

                                        }

                                    </script>

                                </div>

                            </div>

                        </div>

                    <?php } ?>

                </div>

            </div>



        </div>

    </div>

</div>