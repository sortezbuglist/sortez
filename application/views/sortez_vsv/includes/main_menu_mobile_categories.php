<?php
$this_session = &get_instance();
$this_session->load->library('session');
$session_zMotCle_verification = $this_session->session->userdata('zMotCle_x');
?>
<div class="container" style="background-color: #DA1893">
<div class="row d-lg-none d-MD-none">
<div class="col-12">
    <div class="input-group">
        <input type="text" name="inputString_zMotCle_mobile" id="inputString_zMotCle_mobile"
               value="<?php if (isset($session_zMotCle_verification) && $session_zMotCle_verification != " ") echo $session_zMotCle_verification; ?>"
               class="form-control" placeholder="Mot clé à rechercher" aria-describedby="basic-addon2"/>
        <button name="zMotCle_to_check_btn_mobile" id="inputString_zMotCle_submit_mobile" class="input-group-addon">Rechercher
        </button>
    </div>
    <a style="text-align: center;color: white;font-size: 15px" class="nav-link" href="javascript:void(0);" onclick="btn_re_init_annuaire_list();"
       id="id_init_tab_title" role="tab" data-toggle="tab">Réinitialiser
    </a>
</div>
</div>
</div>