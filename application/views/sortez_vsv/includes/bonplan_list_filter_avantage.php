<?php
$thisss =& get_instance();
$thisss->load->library('session');
$inputString_bonplan_type = $thisss->session->userdata('inputString_bonplan_type_x');
?>

<div class="row">
    <div class="col p-2">

        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:bonplan_filter_triage(0);">
            <input type="radio" class="form-check-input" name="inputStringAvantageHidden_bonplan"
                   id="optionsRadios_0" value="0" checked>
            Tous les Bon plans
        </label>

    </div>
    <div class="col p-2">

        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:bonplan_filter_triage(1);">
            <input type="radio" class="form-check-input" name="inputStringAvantageHidden_bonplan"
                   id="optionsRadios_1" value="1"
                   <?php if (isset($inputString_bonplan_type) && $inputString_bonplan_type == '1') { ?>checked<?php } ?>>
            Les bons plans simples
        </label>

    </div>
    <div class="col p-2">

        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:bonplan_filter_triage(2);">
            <input type="radio" class="form-check-input" name="inputStringAvantageHidden_bonplan"
                   id="optionsRadios_2" value="2"
                   <?php if (isset($inputString_bonplan_type) && $inputString_bonplan_type == '2') { ?>checked<?php } ?>>
            Les bons plans uniques
        </label>

    </div>
    <div class="col p-2">

        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:bonplan_filter_triage(3);">
            <input type="radio" class="form-check-input" name="inputStringAvantageHidden_bonplan"
                   id="optionsRadios_3" value="3"
                   <?php if (isset($inputString_bonplan_type) && $inputString_bonplan_type == '3') { ?>checked<?php } ?>>
            Les bons plans multiples
        </label>

    </div>
</div>