<?php
//LOCALDATA FILTRE
$this_session_localdata =& get_instance();
$this_session_localdata->load->library('session');
$localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
$localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
$iVilleId_x = $this_session_localdata->session->userdata('iVilleId_x');
?>
<div class="container main_nav_bar_rubrique_menu main_rubrique_menu d-lg-none d-xl-none">
    <div class="row">
        <nav class="container navbar navbar-toggleable-md navbar-inverse bg-inverse mb-0 w-100 p-0">
            <button class="navbar-toggler navbar-toggler-left collapsed" type="button" data-toggle="collapse"
                    data-target="#navbarsCommunesDefault" aria-controls="navbarsCommunesDefault" aria-expanded="false"
                    id="navbar_button_communes_link"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><img alt="" src="<?php echo base_url(); ?>application/resources/front/images/vivresaville_custom/burgerIcon.png" class="img-fluid"></span>
            </button>
            <a class="navbar-brand text-right" id="navbar_brand_communes_link" href="javascript:void(0);" onclick="javascript:click_communes_menu_btn();">
                <?php
                if(isset($iVilleId_x) && $iVilleId_x != "" && $iVilleId_x != 0 && $iVilleId_x != "0")
                {
                    foreach($toVille as $oVille){
                        if ($oVille->IdVille == $iVilleId_x)
                            echo $oVille->ville_nom." (".$oVille->nbCommercant.")".'</a>';
                    }
                } else echo 'Toutes les communes';
                ?>
            </a>
            <div class="navbar-collapse collapse" id="navbarsCommunesDefault" aria-expanded="false" style="max-height: 400px; overflow-y: scroll;">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active pl-3 pr-3">
                        <a href="javascript:void(0);" onclick="javascript:pvc_select_commune(0);" class="col nav-link">Toutes les communes</a>
                    </li>
                    <?php
                    if(isset($toVille) && $toVille != "" && $toVille != 0 && $toVille != "0")
                    {
                        foreach($toVille as $oVille){ ?>
                            <li class="nav-item  pl-3 pr-3">
                                <a href="javascript:void(0);" onclick="javascript:pvc_select_commune(<?php echo $oVille->IdVille ; ?>);" class="col nav-link ">
                                    <?php echo $oVille->ville_nom." (".$oVille->nbCommercant.")" ; ?>
                                </a>
                            </li>
                        <?php } ?>
                    <?php }
                    ?>
                </ul>
            </div>
        </nav>
    </div>
</div>
<script type="text/javascript">
    function pvc_select_commune(id_commune){
        $("#inputStringVilleHidden_bonplans").val(id_commune);
        $("#inputStringVilleHidden_partenaires").val(id_commune);
        $("#inputStringVilleHidden_annonces").val(id_commune);
        $("#frmRechercheBonplan").submit();
        $("#frmRecherchePartenaire").submit();
        $("#frmRechercheAnnonce").submit();
    }
    function click_communes_menu_btn() {
        $("#navbar_button_communes_link").click();
    }
</script>