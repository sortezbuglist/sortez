<style>
    .content_menu_soutenons a span{
        color: black;
        text-decoration: none;
        font-family: LibreBaskerville-Bold;
        font-style: italic;
        font-size: 1.1em;
    }
</style>
<div class="content_menu_soutenons d-none d-lg-flex d-xl-flex">
    <div class="col-lg-2 col-sm-6" style="border-right: 1px solid #000000">
        <a href="<?php echo base_url()?>">
            Page accueil
        </a>
    </div>
    <div class="col-lg-2 col-sm-6" style="border-right: 1px solid #000000">
        <a href="<?php echo site_url('annuaire')?>" class="<?php if ($zcontentactive =="annuaire"){ echo"active" ;} ?>">
            <!-- Les bonnes adresses -->Répertoire <span class="<?php if ($zcontentactive =="annuaire"){ echo"active" ;} ?>" id="count_annuaire" style=" color: black;text-decoration: none;font-family: LibreBaskerville-Bold;font-style: italic;font-size: 1.1em;"></span>
        </a>
    </div>
    <div class="col-lg-2 col-sm-6" style="border-right: 1px solid #000000" >
        <a href="<?php echo site_url('article')?>" class="<?php if ($zcontentactive =="article"){ echo"active" ;} ?>">
            L'actualité et la revue de presse <span class="<?php if ($zcontentactive =="article"){ echo"active" ;} ?>" id="count_article" style=" color: black;text-decoration: none;font-family: LibreBaskerville-Bold;font-style: italic;font-size: 1.1em;"></span>
        </a>
    </div>
    <div class="col-lg-2 col-sm-6" style="border-right: 1px solid #000000">
        <a href="<?php echo site_url('agenda')?>" class="<?php if ($zcontentactive =="agenda"){ echo"active" ;} ?>">
            L'agenda événementiel <span class="<?php if ($zcontentactive =="agenda"){ echo"active" ;} ?>" id="count_agenda" style=" color: black;text-decoration: none;font-family: LibreBaskerville-Bold;font-style: italic;font-size: 1.1em;"></span>
        </a>
    </div>
    <div class="col-lg-2 col-sm-6" style="border-right: 1px solid #000000">
        <a href="<?php echo site_url('Boutique')?>" class="<?php if ($zcontentactive =="boutique"){ echo"active" ;} ?>">
           Les Boutiques <span class="<?php if ($zcontentactive =="boutique"){ echo"active" ;} ?>" id="count_annonce" style=" color: black;text-decoration: none;font-family: LibreBaskerville-Bold;font-style: italic;font-size: 1.1em;"></span>
        </a>
    </div>
    <div class="col-lg-2 col-sm-6" id="article_menu">
        <a href="<?php echo site_url('DealAndFidelite')?>" class="<?php if ($zcontentactive =="dealandfidelite"){ echo"active" ;} ?>">
            Deal & fidélité <span class="<?php if ($zcontentactive =="dealandfidelite"){ echo"active" ;} ?>" id="count_deal" style=" color: black;text-decoration: none;font-family: LibreBaskerville-Bold;font-style: italic;font-size: 1.1em;"></span>
        </a>
    </div>
<!--    <div class="col-lg-3 col-sm-6" id="plus_display">-->
<!--        <div class="dropdown1">-->
<!--            <span>Plus...</span>-->
<!--            <div class="dropdown-content1">-->
<!--                <a href="--><?php //echo site_url('/soutenons/Souscription_consommateurs')?><!--">Les Avantage</a>-->
<!--                <a href="--><?php //echo site_url('/auth/login')?><!--" target="_blank">Mon compte</a>-->
<!--                <a href="--><?php //echo site_url('/front/fidelity/menuconsommateurs')?><!--" target="_blank">Ma carte vivresaville.fr</a>-->
<!--                <a href="--><?php //echo base_url('/front/professionnels/inscription/') ?><!--" target="_blank">Souscription</a>-->
<!--                <a href="--><?php //echo site_url('/auth/login')?><!--" target="_blank">Mon compte</a>-->
<!--                <a href="#">L'abonnement "Soutien"</a>-->
<!--                <a href="#">L'abonnement "Soutien Plus"</a>-->
<!--                <a href="#">Présentation commande</a>-->
<!--                <a href="#">Pack infos</a>-->
<!--                <a href="#">Pack promos</a>-->
<!--                <a href="#">Pack boutique</a>-->
<!--                <a href="#">Spécial restauration</a>-->
<!--                <a href="#">Spécial Gîtes</a>-->
<!--                <a href="#"  class="titre_type">Entête menu</a>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
</div>
<?php if(($zcontentactive =="agenda") || ($zcontentactive =="article") || ($zcontentactive =="annuaire")){ ?>
    <style>
        #plus_display{
            display: none;
        }
        .col-lg-20{
            width: 25%;
        }
        #article_menu{
            border-right: none!important;
        }
    </style>
<?php } ?>
<script type="text/javascript">

    //wpRedirectMinScreen(500,900,'https://www.sortez.org/mobile-index.html',0);

    $(document).ready(function() {

        $("a.ActiveButton").bind({ mousedown:function(){if ( $(this).attr('disabled') === undefined ) $(this).addClass('Activated');}, mouseleave:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}, mouseup:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}});

        $.get( "<?php echo site_url('annuaire/get_count_annuaire/'); ?>", function( data ){

            //alert( "Data Loaded: " + data );

            console.log(data);

            $("#count_annuaire").html(data);

        });

    });



    $(document).ready(function() {

        $("a.ActiveButton").bind({ mousedown:function(){if ( $(this).attr('disabled') === undefined ) $(this).addClass('Activated');}, mouseleave:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}, mouseup:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}});

        $.get( "<?php echo site_url('article/get_count_article/'); ?>", function( data ){

            //alert( "Data Loaded: " + data );

            console.log(data);

            $("#count_article").html(data);

        });

    });



    $(document).ready(function() {

        $("a.ActiveButton").bind({ mousedown:function(){if ( $(this).attr('disabled') === undefined ) $(this).addClass('Activated');}, mouseleave:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}, mouseup:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}});

        $.get( "<?php echo site_url('agenda/get_count_agenda/'); ?>", function( data ){

            //alert( "Data Loaded: " + data );

            console.log(data);

            $("#count_agenda").html(data);

        });

    });



    $(document).ready(function() {

        $("a.ActiveButton").bind({ mousedown:function(){if ( $(this).attr('disabled') === undefined ) $(this).addClass('Activated');}, mouseleave:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}, mouseup:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}});

        $.get( "<?php echo site_url('front/bonplan/get_count_bonplan/'); ?>", function( data ){

            //alert( "Data Loaded: " + data );

            console.log(data);

            $("#count_bonplan").html(data);

        });

    });



    $(document).ready(function() {

        $("a.ActiveButton").bind({ mousedown:function(){if ( $(this).attr('disabled') === undefined ) $(this).addClass('Activated');}, mouseleave:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}, mouseup:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}});

        $.get( "<?php echo site_url('front/fidelity/get_count_fidelity/'); ?>", function( data ){

            //alert( "Data Loaded: " + data );

            console.log(data);

            $("#count_fidelity").html(data);

        });

    });



    $(document).ready(function() {

        $("a.ActiveButton").bind({ mousedown:function(){if ( $(this).attr('disabled') === undefined ) $(this).addClass('Activated');}, mouseleave:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}, mouseup:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}});

        $.get( "<?php echo site_url('front/annonce/get_count_annonce/'); ?>", function( data ){

            //alert( "Data Loaded: " + data );

            console.log(data);

            $("#count_annonce").html(data);

        });

    });

    $(document).ready(function() {

        $("a.ActiveButton").bind({ mousedown:function(){if ( $(this).attr('disabled') === undefined ) $(this).addClass('Activated');}, mouseleave:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}, mouseup:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}});

        $.get( "<?php echo site_url('plat_du_jour/get_count_plat/'); ?>", function( data ){

            //alert( "Data Loaded: " + data );

            console.log(data);

            $("#count_plat").html(data);

        });

    });
    $(document).ready(function() {

        $("a.ActiveButton").bind({ mousedown:function(){if ( $(this).attr('disabled') === undefined ) $(this).addClass('Activated');}, mouseleave:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}, mouseup:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}});

        $.get( "<?php echo site_url('soutenons/DealAndFidelite/get_count_deal/'); ?>", function( data ){

            //alert( "Data Loaded: " + data );

            console.log(data);

            $("#count_deal").html(data);

        });

    });

</script>