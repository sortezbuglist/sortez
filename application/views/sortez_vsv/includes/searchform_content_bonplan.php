<?php $data["empty"] = null; ?>

<?php
//LOCALDATA FILTRE
$this_session_localdata =& get_instance();
$this_session_localdata->load->library('session');
$localdata_value = $this_session_localdata->session->userdata('localdata');
$localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
$localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0" && $localdata_IdDepartement != NULL) {
    $this->load->view("privicarte/includes/dropdownMenu_commune", $data);
}
?>


<form name="frmRechercheBonplan" id="frmRechercheBonplan" method="post" class="col"
      action="<?php echo site_url('front/bonplan/index/'); ?>">

    <div class="col-lg-12 main_rubrique_banner_title">LES BONS PLANS : RECHERCHES PAR COMMUNES ET CATÉGORIES </div>
    <div class="row">
        <div class="ui-widget col-lg-12 text-center d-none d-lg-block" style="padding-bottom: 20px">
            <?php $this->load->view('sortez_vsv/includes/main_communes_select');  ?>
        </div>
        <style type="text/css">.dropdown, .dropup{
                text-align: center!important;
            } </style>
    </div>
    <input name="inputStringHidden" id="inputStringHidden" type="hidden" value="1"/>
    <input name="inputStringOrderByHidden" id="inputStringOrderByHidden" type="hidden" value=""/>
    <input name="zMotCle" id="zMotCle" type="hidden" value=""/>
    <input name="inputStringWhereMultiple" id="inputStringWhereMultiple" type="hidden" value="">
    <input name="inputString_bonplan_type" id="inputString_bonplan_type" type="hidden" value="">


    <div class="row navbutton_category_list_filter mt-3 mb-3 pt-2 pb-2 d-lg-none">
        <div class="col text-left" style="padding-left: 12px">
            <a href="javascript:void(0);" onclick="javascript:navbutton_category_link_mobile();"
               class="navbutton_category_link">
                <img alt=""
                     src="<?php echo base_url(); ?>application/resources/front/images/vivresaville_custom/burgerIcon.png"
                     width="30"
                     class="img-fluid">
            </a>
        </div>
        <div class="col text-right">
            <a href="javascript:void(0);" onclick="javascript:navbutton_category_link_mobile();"
               class="navbutton_category_link">Cat&eacute;gories</a>
        </div>
    </div>


    <div id="span_leftcontener2013_form_bonplans" class="row text-center pl-2">

    </div>

    <div id="span_main_rubrique_banner_title" class="main_rubrique_banner_title">Affiner votre recherche</div>

    <!--- à ajouter dans un système d'onglet -->


    <?php
    $thisss =& get_instance();
    $thisss->load->library('session');
    ?>


    <div id="span_subcateg_main_rubrique_banner_title"
         class="row justify-content-center d-none d-md-block d-lg-block d-xl-block">
        <div class="col-xl-10 sub_filter_tab_content ml-auto mr-auto">
            <!--tab system-->
            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <ul class="nav nav-tabs nav-justified" role="tablist">
                        <li class="nav-item pr-2">
                            <a class="nav-link active" href="#tab_subcateg" role="tab"
                               data-toggle="tab" id="id_subcateg_tab_title">Sous-catégorie</a>
                        </li>
                        <li class="nav-item pr-2">
                            <a class="nav-link" href="#tab_filter" id="id_filter_tab_title" role="tab"
                               data-toggle="tab">Filtrer</a>
                        </li>
                        <li class="nav-item pr-2">
                            <a class="nav-link" href="#tab_keyword" id="id_keyword_tab_title" role="tab"
                               data-toggle="tab">Mot clés</a>
                        </li>
                        <li class="nav-item pr-2">
                            <a class="nav-link" href="javascript:void(0);" onclick="btn_re_init_bonplan_list();"
                               id="id_init_tab_title" role="tab" data-toggle="tab">Réinitialiser</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="tab_subcateg">
                    <div class="row m-0" id="div_subcateg_annuaire_contents"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab_filter">
                    <?php $this->load->view("sortez_vsv/includes/bonplan_list_filter_avantage", $data); ?>
                    <?php $this->load->view("sortez_vsv/includes/annuaire_list_filter_triage", $data); ?>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab_keyword">
                    <?php $this->load->view("sortez_vsv/includes/annuaire_list_filter_search", $data); ?>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab_reinit">
                    <div class="row justify-content-center">
                        <div class="col-8">
                            <button class="btn subcateb_filter_banner w-100" onclick="btn_re_init_bonplan_list();">
                                Réinitialiser votre choix !
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="d-md-none d-lg-none d-xl-none">
        <div class="row">
            <div class="col-12 main_subcateg_tab_filter p-0">
                <?php $this->load->view("sortez_vsv/includes/annuaire_list_filter_triage_mobile", $data); ?>
            </div>
        </div>
        <div class="row justify-content-center d-none">
            <div class="col-12 main_subcateg_tab_init">
                <button class="btn subcateb_filter_banner w-100" onclick="btn_re_init_bonplan_list();">R&eacute;initialiser</button>
            </div>
        </div>

        <div class="row">
            <div class="col-12 main_subcateg_tab_search">
                <?php $this->load->view("sortez_vsv/includes/annuaire_list_filter_search_mobile", $data); ?>
            </div>
        </div>
    </div>


</form>


