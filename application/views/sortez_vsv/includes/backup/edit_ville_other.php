<style type="text/css">
    body {
        background: none !important;
    }
</style>

<?php $data["zTitle"] = 'Vivresaville'; ?>
<?php $this->load->view("sortez_vsv/includes/main_header", $data); ?>
<h2>Ajouter une autre térritoire</h2>
<form method="post" id="edit_ville_other_form" class="" action="" enctype="multipart/form-data">
    <div class="form-group">
        <label for="formGroupCodepostal">Code postal</label>
        <input type="text" class="form-control" name="ville_other[postal_code]" id="formGroupCodepostal"
               placeholder="Code postal"
               onkeyup="javascript:vsv_getVille_other_list();"
               value="<?php if (isset($ville_data->postal_code)) echo $ville_data->postal_code; ?>">
    </div>
    <div class="form-group">
        <label for="formGroupVilleId">Ville Corréspondante</label>
        <div class="col-12 p-0" id="vsv_select_ville_other_container">
            <input type="text"
                   value="<?php if (isset($ville_data->id_ville)) echo $this->mdlville->getVilleById($ville_data->id_ville)->Nom; ?>"
                   name="IdVille_Nom_text" id="IdVille_Nom_text" disabled="disabled"
                   class="form-control"/>
            <input type="hidden" class="form-control" name="ville_other[id_ville]" id="formGroupVilleId"
                   value="<?php if (isset($ville_data->id_ville)) echo $ville_data->id_ville; ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="formGroupNomIdentification">Nom d'identification</label>
        <input type="text" class="form-control" name="ville_other[name]" id="formGroupNomIdentification"
               placeholder="Nom" value="<?php if (isset($ville_data->name)) echo $ville_data->name; ?>">
    </div>
    <div class="form-group mt-5">
        <input type="hidden" name="ville_other[id_vsv]" id="formGroup_id_vsv"
               value="<?php if (isset($vsv_data->id)) echo $vsv_data->id; ?>">
        <input type="hidden" name="ville_other[id]" id="formGroup_id_vsv"
               value="<?php if (isset($ville_data->id)) echo $ville_data->id; ?>">
        <a href="javascript:void(0);" onclick="javascript:vsv_form_ville_other_fn();" class="btn btn-success" id="edit_ville_other_submit">Enregistrer</a>
        <div class="col-12 text-center" id="vsv_ville_other_form_error"></div>
    </div>
</form>
<?php $this->load->view("sortez_vsv/includes/main_footer", $data); ?>