<div class="d-none d-lg-block d-xl-block">
    <div class="container_wix text-center">
        <div class="row pt-4 d-lg-flex">

            <div class="col-lg-9 col-sm-12 p-0">
                <div class="container">
                    <div class="row">
                        <div class="col-lgs-3 col-sm-12 p-0 mr-1">
                            <a class="menus_2 btn btn-lg" data-fancybox="" data-animation-duration="500" data-src="#divContactPartnerForm" href="javascript:;" id="IdCsontactPartnerForm" title="Contactez-nous!" style="font-family:Libre-Baskerville-Italic,serif!important;font-size:14px;font-style: italic;font-weight: normal!important;">Contact</a>
                        </div>

<!--                         <div class="col-lgs-2 col-sm-12 p-0 ml-2 mr-2" >
                            <a class="menus_2 btn btn-lg" href="https://www.magazine-sortez.org/newsletter" style="font-family:Libre-Baskerville-Italic,serif!important;font-size:14px;font-style: italic;font-weight: normal!important;">Newsletter</a>
                        </div> -->

                        <div class="col-lgs-3 col-sm-12 p-0 ml-1 mr-1" >
                            <a class="menus_2 btn btn-lg" href="<?php echo site_url("/auth/login");?>" style="font-family:Libre-Baskerville-Italic,serif!important;font-size:14px;font-style: italic;font-weight: normal!important;">Mon compte</a>
                        </div>

                        <div class="col-lgs-3 col-sm-12 p-0 ml-1 mr-1" >
                            <a class="menus_2 btn btn-lg" href="<?php echo site_url(); ?>admin" style="font-family:Libre-Baskerville-Italic,serif!important;font-size:14px;font-style: italic;font-weight: normal!important;">Mes favoris</a>
                        </div>

                        <div class="col-lgs-3 col-sm-12 p-0 ml-1 mr-1" >
                            <a class="menus_2 btn btn-lg" href="#divContactRecommandationForm" id="IdRecommandationPartnerForm_main_menu" title="Recommandation" style="font-family:Libre-Baskerville-Italic,serif!important;font-size:14px;font-style: italic;font-weight: normal!important;">
                                Recommandez
                            </a>
                        </div>
                    </div>
                </div>
            </div>
    <!-- ******************************************************************* -->
            <!-- <div class="col-lgs-3 col-sm-12 ml-2">
                <a class="menus_2 btn btn-lg" href="https://www.soutenonslecommercelocal.fr/" style="font-family:Libre-Baskerville-Italic,serif!important;font-size:14px;font-style: italic;font-weight: normal!important;">soutenonslecommercelocal.fr</a>
            </div> -->
    <!-- ******************************************************************* -->
    <!-- <div class="col-lgs-3 col-sm-12 ml-2">
        <div class="menus_2 btn btn-lg" id="google_translate_element" ></div>
        <script type="text/javascript">
            function googleTranslateElementInit() {
                new google.translate.TranslateElement({
                    pageLanguage: 'fr',
                    includedLanguages: 
                    'de,en,es,fr,it,ja,nl,no,pl,pt,ru,sv,zh-CN',
                    layout: google.translate.TranslateElement.InlineLayout.SIMPLE,
                     autoDisplay: false
                }, 'google_translate_element');
            }
        </script>
        <script type="text/javascript"
                src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script> -->
        <!-- <span><img alt="" src="<?php //echo base_url(); ?>assets/ville-test/wpimages/wp43f61dd7_06.png"></span> -->
    <!-- </div> -->
    <div class="<?php if($is_mobile) echo "hideTranslate";
                         else echo"col-lgs-3 col-sm-12 ml-2";?>">
        <div class="menus_2 btn btn-xs" 
             id="google_translate_element"
             style=
             "font-family:Libre-Baskerville-Italic,serif!important;
             font-size:14px;font-style: italic;font-weight: normal!important;">
        </div>
        <script type="text/javascript">
            function googleTranslateElementInit() {
                new google.translate.TranslateElement({
                    pageLanguage: 'fr',
                    includedLanguages: 
                    'de,en,es,fr,it,ja,nl,no,pl,pt,ru,sv,zh-CN',
                    layout: google.translate.TranslateElement.InlineLayout.SIMPLE,
                    autoDisplay: false
                }, 'google_translate_element');
            }
        </script>
        <script type="text/javascript" 
                src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit">
        </script>
        <!-- <span><img alt="" src="<?php //echo base_url(); ?>assets/ville-test/wpimages/wp43f61dd7_06.png"></span> -->
    </div>

<!--            <div class="col-lg-2 col-sm-12 col-md-2 pr-0" >-->
<!--                <a class="menus_2 btn btn-lg webmaster" href="--><?php //echo site_url('auth/login') ?><!--">Webmaster login</a>-->
<!--            </div>-->

        </div>
        <div class="row pt-5 d-lg-flex mb-4">
            <div class="col-lg-3 mt-4">
                <img src="<?php echo base_url('/assets/index/image/cherie95.jpg') ?>">
<!--                <img src="--><?php //echo base_url('/assets/soutenons/logo_sortez.png') ?><!--">-->
<!--                <img src="--><?php //echo base_url('/assets/soutenons/logo_left.png') ?><!--" style="margin-top: 57px;">-->
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12">
<!--                        <img src="--><?php //echo base_url('/assets/soutenons/logo_soutenons.png') ?><!--">-->
                        <img class="w-75" src="<?php echo base_url('/assets/images/logo-sortez-sept.webp') ?>">
                    </div>
                    <!--<div class="col-lg-12 pt-4 mt-3">
                        <span class="text_soutenons_1">L'essentiel des sorties et des loisirs de Fréjus à Menton</span>
                    </div>-->
<!--                    <div class="col-lg-12">-->
<!--                        <p class="text_soutenons_2 pt-3 pb-2" style="color:#605e5e;">"soutenonslecommercelocal.fr " est une réalisation du magazine Sortez !</p>-->
<!--                    </div>-->
                </div>
            </div>
            <div class="col-lg-3">
                <img src="<?php echo base_url('/assets/soutenons/logo_vivresaville.png') ?>">
<!--                <img src="--><?php //echo base_url('/assets/soutenons/logo_right.png') ?><!--">-->
            </div>
        </div>
    </div>
</div>
<div id="divContactRecommandationForm"  style="display:none; background-color:#FFFFFF; width: 300px;!important; height:200px;">
    <form name="formContactPrivicarteForm" id="formContactPrivicarteForm" action="#">
        <table width="100%" id="tableContactPartnerForm" border="0" cellspacing="0" cellpadding="0"
               style="text-align:center; width:100%; height:350px;">
            <tr>
                <td>
                    <div style="font-family:arial; font-size:24px; font-weight:bold;">Recommander &agrave;
                        un ami
                    </div>
                </td>
            </tr>
            <tr>
                <td><input class="form-control pt-2" type="text" name="contact_recommandation_nom" id="contact_recommandation_nom"
                           value="" placeholder="Votre nom *"/></td>
            </tr>
            <tr>
                <td><input class="form-control pt-2" type="text" name="contact_recommandation_mail_ami"
                           id="contact_recommandation_mail_ami" placeholder="Courriel de votre ami *" value=""/></td>
            </tr>
            <tr>
                <td><input class="form-control pt-2" type="text" name="contact_recommandation_mail" id="contact_recommandation_mail"
                           value="" placeholder="Votre courriel *"/></td>
            </tr>
            <tr>
                <td><textarea class="form-control pt-2" name="contact_recommandation_msg" id="contact_recommandation_msg" disabled>Bonjour, Je vous recommande cette page : "<?php echo site_url();?>"</textarea>
                </td>
            </tr>
            <tr>
                <td><span id="spanContactPrivicarteRecommandationForm">* champs obligatoires</span></td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><input type="button" class="btn btn-default"
                                       name="contact_recommandation_reset" id="contact_recommandation_reset"
                                       value="Retablir"/></td>
                            <td><input type="button" class="btn btn-default"
                                       name="contact_recommandation_send" id="contact_recommandation_send"
                                       value="Envoyer"/></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</div>

<div id="divContactPartnerForm" class="animated-modal" style="background-color:#FFFFFF; display: none;">
    <form name="formContactPartnerForm" id="formContactPartnerForm" action="#">
        <table width="100%" id="tableContactPartnerForm" border="0" cellspacing="0" cellpadding="0" style="text-align:center; width:100%; height:auto;">
            <tr>
                <td><div style="font-family:arial; font-size:24px; font-weight:bold;">Nous Contacter</div></td>
            </tr>
            <tr>
                <td><input type="text" class="form-control mt-2" name="contact_partner_nom" id="contact_partner_nom" placeholder="Votre nom *" value=""/></td>
            </tr>
            <tr>
                <td><input type="text" class="form-control mt-2" name="contact_partner_tel" id="contact_partner_tel" placeholder="Votre numéro de téléphone *" value=""/></td>
            </tr>
            <tr>
                <td><input type="text" class="form-control mt-2" name="contact_partner_mail" id="contact_partner_mail" placeholder="Votre courriel *" value=""/></td>
            </tr>
            <tr>
                <td><textarea class="form-control mt-2" name="contact_partner_msg" id="contact_partner_msg" placeholder="Votre message *" value=""></textarea></td>
            </tr>
            <tr>
                <td><span id="spanContactPartnerForm">* champs obligatoires</span></td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><input type="button" class="btn btn-default" name="contact_partner_reset" id="contact_partner_reset" value="Retablir"/></td>
                            <td><input type="button" class="btn btn-default" name="contact_partner_send" id="contact_partner_send" value="Envoyer"/></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</div>
<style>
    .menus_2{
        border-radius: 25px;
        padding-top: 8px;
        padding-bottom: 8px;
    }
    .col-lgs-2 {
        -webkit-box-flex: 0;
        -webkit-flex: 0 0 18%;
        -ms-flex: 0 0 18%;
        flex: 0 0 18%;
        max-width: 18%;
    }
    .col-lgs-3{
        -webkit-box-flex: 0;
        -webkit-flex: 0 0 24%;
        -ms-flex: 0 0 24%;
        flex: 0 0 24%;
        max-width: 24%;
    }
    @media (max-width: 600px){
        .col-lgs-2 {
            -webkit-box-flex: 0;
            -webkit-flex: 0 0 100%;
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
            padding-right: 15px!important;
            padding-left: 15px!important;
            margin: auto!important;
        }
        .col-lgs-3{
            -webkit-box-flex: 0;
            -webkit-flex: 0 0 100%;
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
        }
    }
</style>
<link  href="<?php echo base_url();?>assets/css/fancy.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.min.js"></script>
<script>
    $("#contact_partner_reset").click(function() {
        $("#contact_partner_nom").val('');
        $("#contact_partner_tel").val('');
        $("#contact_partner_mail").val('');
        $("#contact_partner_msg").val('');
        $("#spanContactPartnerForm").html('* champs obligatoires');
    });

    $("#contact_partner_send").click(function() {
        var error = 0;
        var contact_partner_nom = $("#contact_partner_nom").val();
        if (contact_partner_nom == '' || contact_partner_nom == '') error = 1;
        var contact_partner_tel = $("#contact_partner_tel").val();
        if (contact_partner_tel == '' || contact_partner_tel == '') error = 1;
        var contact_partner_mail = $("#contact_partner_mail").val();
        if (contact_partner_mail == '' || contact_partner_mail == '') error = 1;
        if (!validateEmail(contact_partner_mail)) error = 2;
        var contact_partner_msg = $("#contact_partner_msg").val();
        if (contact_partner_msg == '' || contact_partner_msg == '') error = 1;
        $("#spanContactPartnerForm").html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

        if (error == 1) {
            $("#spanContactPartnerForm").html('<span style="color:#FF0000;">* Les champs sont obligatoires</span>');
        } else if (error == 2) {
            $("#spanContactPartnerForm").html('<span style="color:#FF0000;">* Adresse email invalide</span>');
            $("#contact_partner_mail").css('border-color','#ff0000');
        } else {
            $.post(
                "<?php echo site_url("front/professionnels/contact_partner_sendmail/");?>",
                {
                    contact_partner_nom:contact_partner_nom,
                    contact_partner_tel:contact_partner_tel,
                    contact_partner_mail:contact_partner_mail,
                    contact_partner_msg:contact_partner_msg,
                    contact_partner_mailto:"<?php echo $oInfoCommercant->Email; ?>"
                },
                function( data ) {
                    $("#spanContactPartnerForm").html(data);
                    //setTimeout(function(){ $.fancybox.close(); }, 4000);
                });
        }
    });
    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( $email );
    }
</script>
<script type="text/javascript">

    $("#IdRecommandationPartnerForm_main_menu").fancybox({
        'autoScale' : false,
        'overlayOpacity'      : 0.8, // Set opacity to 0.8
        'overlayColor'        : "#000000", // Set color to Black
        'padding'         : 5,
        'width'         : 520,
        'height'        : 500,
        'transitionIn'      : 'elastic',
        'transitionOut'     : 'elastic'
    });

    $("#contact_recommandation_reset").click(function () {
        $("#contact_recommandation_nom").val('');
        $("#contact_recommandation_mail_ami").val('');
        $("#contact_recommandation_mail").val('');
        $("#spanContactPrivicarteRecommandationForm").html('* champs obligatoires');
    });
    $("#contact_recommandation_send").click(function () {
        $("#spanContactPrivicarteRecommandationForm").html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
        var error = 0;
        var contact_recommandation_msg = $("#contact_recommandation_msg").val();
        if (contact_recommandation_msg == '' || contact_recommandation_msg == '') error = 1;
        var contact_recommandation_nom = $("#contact_recommandation_nom").val();
        if (contact_recommandation_nom == '' || contact_recommandation_nom == '') error = 1;
        var contact_recommandation_mail_ami = $("#contact_recommandation_mail_ami").val();
        if (contact_recommandation_mail_ami == '' || contact_recommandation_mail_ami == '') error = 1;
        else if (!validateEmail(contact_recommandation_mail_ami)) error = 3;
        var contact_recommandation_mail = $("#contact_recommandation_mail").val();
        if (contact_recommandation_mail == '' || contact_recommandation_mail == '') error = 1;
        else if (!validateEmail(contact_recommandation_mail)) error = 2;


        if (error == 1) {
            $("#spanContactPrivicarteRecommandationForm").html('<span style="color:#FF0000;">* Les champs sont obligatoires</span>');
        } else if (error == 2) {
            $("#spanContactPrivicarteRecommandationForm").html('<span style="color:#FF0000;">* Adresse email invalide</span>');
            $("#contact_recommandation_mail").css('border-color', '#ff0000');
            //alert("invalide mail");
        } else if (error == 3) {
            $("#spanContactPrivicarteRecommandationForm").html('<span style="color:#FF0000;">* Adresse email invalide</span>');
            $("#contact_recommandation_mail_ami").css('border-color', '#ff0000');
        } else {
            $.post(
                "<?php echo site_url("front/professionnels/recommandation_partner_sendmail/");?>",
                {
                    contact_recommandation_nom: contact_recommandation_nom,
                    contact_recommandation_tel: '',
                    contact_recommandation_mail: contact_recommandation_mail,
                    contact_recommandation_msg: contact_recommandation_msg,
                    contact_recommandation_mailto: contact_recommandation_mail_ami
                },
                function (data) {
                    $("#spanContactPrivicarteRecommandationForm").html(data);
                });
        }

        //alert(error);
    });
    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( $email );
    }
</script>

<script type="text/javascript">
    function down(){
        document.getElementById("sous_menu_2").style.display("block");
    }
</script>
<script type="text/javascript">
    $(function(){
        $(".hideTranslate").remove();
    })
</script>