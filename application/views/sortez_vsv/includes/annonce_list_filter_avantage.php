<?php
$thisss =& get_instance();
$thisss->load->library('session');
$inputString_annonce_type = $thisss->session->userdata('iEtat_x');
?>

<div class="row">
    <div class="col p-2">

        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annonce_filter_triage();">
            <input type="radio" class="form-check-input" name="inputStringAvantageHidden_bonplan"
                   id="optionsRadios_0" value="" checked>
            Tous
        </label>

    </div>
    <div class="col p-2">

        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annonce_filter_triage(0);">
            <input type="radio" class="form-check-input" name="inputStringAvantageHidden_bonplan"
                   id="optionsRadios_1" value="1"
                   <?php if (isset($inputString_annonce_type) && $inputString_annonce_type == '0') { ?>checked<?php } ?>>
            Reconditionnés
        </label>

    </div>
    <div class="col p-2">

        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annonce_filter_triage(1);">
            <input type="radio" class="form-check-input" name="inputStringAvantageHidden_bonplan"
                   id="optionsRadios_2" value="2"
                   <?php if (isset($inputString_annonce_type) && $inputString_annonce_type == '1') { ?>checked<?php } ?>>
            Neufs
        </label>

    </div>
</div>