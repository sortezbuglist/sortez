<!-- <style>
    .form-control{
        max-width: 500px;
        margin: auto;
        border: 1px solid #E80EAE;
        color: #E80EAE;
        background-color: white;
        text-align: left;
        height: 47px;
    }
</style> -->

<?php

$thiss = &get_instance();

$thiss->load->library('session');

$thiss->load->model('vsv_ville_other');

$main_width_device = $thiss->session->userdata('main_width_device');

$main_mobile_screen = false;

if (isset($main_width_device) && $main_width_device != "" && $main_width_device != "0" && is_numeric($main_width_device) && floatval($main_width_device) <= 991) {

    $main_mobile_screen = true;

} else {

    $main_mobile_screen = false;

}

$thiss->load->model("vivresaville_villes");

$logo_to_show_vsv = base_url() . "assets/ville-test/wpimages/logo_ville_test.png";

if (isset($localdata_IdVille_parent) && $localdata_IdVille_parent != "" && $localdata_IdVille_parent != false) {

    $vsv_id_vivresaville = $thiss->vivresaville_villes->getByIdVille($localdata_IdVille_parent);

    if (isset($vsv_id_vivresaville) && isset($vsv_id_vivresaville->id)) {

        $vsv_object = $thiss->vivresaville_villes->getById($vsv_id_vivresaville->id);

        $vsv_other_ville = $thiss->vsv_ville_other->getByIdVsv($vsv_id_vivresaville->id);

    }

    if (isset($vsv_object) && isset($vsv_object->logo) && file_exists("application/resources/front/images/vivresaville/" . $vsv_object->logo)) {

        $logo_to_show_vsv = base_url() . "application/resources/front/images/vivresaville/" . $vsv_object->logo;

    }

}

?>



<div class="mt-lg-5 mb-lg-5 main_banner_container_soutenons">

    <div class="" style="background-color: white!important;padding-top: 10px;padding-bottom: 25px">

        <div class="col-lg-12">

            <p class="gros_titre">

                L'actualit&eacute;

                &

                la

                revue

                de

                presse 

            </p>

        </div>

        <div class="col-lg-12 text-center">



            <div class="">

                <div class="row">

                    <div class="containercontainer">

                        <div class="row">

                            <div class="col-6 col-sm-12 col-lg-4 pt-2">

                                <div class="dropdown">

                                    <div class="btn btn-secondary dropdown-toggle border_content_soutenons" type="text" id="Recherches_par_departement" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                        <?php

                                        if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != 0 && $localdata_IdDepartement != "0" && intval($localdata_IdDepartement) != 9999) {

                                            if (isset($toDepartement) && $toDepartement != "" && $toDepartement != 0 && $toDepartement != "0") {

                                                foreach ($toDepartement as $oDepartement) {

                                                    if ($oDepartement->departement_id == $localdata_IdDepartement)

                                                        echo $oDepartement->departement_nom; //$oDepartement->nbCommercant

                                                }

                                            }

                                        } else {

                                        ?>

                                            <span class="d-none d-sm-inline-block">Recherche par département</span>

                                            <span class="d-contents d-sm-none d-md-none d-ld-none d-xl-none">Département</span>

                                        <?php } ?>

                                    </div>

                                    <div style="font-size:12px;" class="dropdown-menu main_communes_select" id="div_Recherches_par_departement" aria-labelledby="dropdownMenuButton_com">

                                        <?php

                                        if (isset($toDepartement) && $toDepartement != "" && $toDepartement != 0 && $toDepartement != "0") {

                                            foreach ($toDepartement as $oDepartement) {

                                        ?>

                                                <a class="dropdown-item (<?= $oDepartement->nbCommercant ?>)" id="department_select_id_<?= $oDepartement->departement_id ?>" href="javascript:void(0);" onclick="javascript:change_department_select(<?php echo $oDepartement->departement_id; ?>);"><?php echo $oDepartement->departement_nom; // $oDepartement->nbCommercant 

                                                                                                                                                                                                                                                                                                        ?></a>

                                        <?php

                                            }

                                        }

                                        ?>

                                    </div>

                                </div>

                            </div>

                            <div class="pl-0 pl-sm-3 col-6 col-sm-12 col-lg-4 pt-2">

                                <div class="dropdown">

                                    <div class="btn btn-secondary dropdown-toggle border_content_soutenons" type="text" id="Recherches_par_partenaire" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                        <span class="d-none d-sm-inline-block">Recherche par partenaire</span>

                                        <span class="d-contents d-sm-none d-md-none d-ld-none d-xl-none">Partenaire</span>

                                    </div>

                                    <div style="font-size:12px;" class="dropdown-menu main_communes_select" id="div_Recherches_par_partenaire" aria-labelledby="Recherches_par_partenaire">

                                        <?php

                                        if (isset($tocommercant) && $tocommercant != "" && $tocommercant != 0 && $tocommercant != "0") {

                                            foreach ($tocommercant as $oCommercant) {

                                        ?>

                                                <a class="dropdown-item <?= $oCommercant->nbcommercant ?>" id="partenaire_select_id_<?= $oCommercant->IdCommercant ?>" href="javascript:void(0);" onclick="javascript:change_commercant_select(<?php echo $oCommercant->IdCommercant; ?>);"><?php echo $oCommercant->NomSociete; // $oCommercant->nbCommercant 

                                                                                                                                                                                                                                                                                            ?></a>

                                        <?php

                                            }

                                        }

                                        ?>

                                    </div>

                                </div>

                            </div>



                            <div class="col-6 col-sm-12 col-lg-4 pt-2">

                                <div class="dropdown">

                                    <div class="btn btn-secondary dropdown-toggle border_content_soutenons" type="text" id="Recherches_par_medias" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                        <span class="d-none d-sm-inline-block">Recherche par médias</span>

                                        <span class="d-contents d-sm-none d-md-none d-ld-none d-xl-none">Médias</span>

                                    </div>

                                    <div style="font-size:12px;" class="dropdown-menu main_communes_select" id="div_Recherches_par_medias" aria-labelledby="Recherches_par_medias">

                                        <?php

                                        // if (isset($toArticleTypeListe) && $toArticleTypeListe != "" && $toArticleTypeListe != null && $toArticleTypeListe != "0") {

                                        //     foreach ($toArticleTypeListe as $oArticleTypeListe) {

                                        //         if ($oArticleTypeListe->category != "Nice-Matin") {

                                        ?>

                                        <!-- <a class="dropdown-item" id="articleType_select_id_<?php //echo $oArticleTypeListe->article_typeid 

                                                                                                ?>" href="javascript:void(0);" onclick="javascript:change_media_select('<?php //echo $oArticleTypeListe->category; 

                                                                                    ?>');"><?php //ory;                                                                                        ant;  ?>
                            </a> -->

                                        <?php

                                        //         }

                                        //     }

                                        // }

                                        ?>

                                        <?php if (isset($toCategorie_principale) && $toCategorie_principale != '' && $toCategorie_principale != null) { ?>

                                            <?php foreach ($toCategorie_principale as $toctages) { ?>

                                                <a class="dropdown-item <?= $toctages->nb_article ?>" 

                                                id="articleType_select_id_<?php echo $toctages->agenda_categid; ?>" 

                                                href="javascript:void(0);" onclick="javascript:change_media_select(<?php echo $toctages->agenda_categid; ?>);"><?php echo $toctages->category; //$toctages->nb_article 

                                                                                                                                                                                                                        ?></a>

                                            <?php } ?>

                                        <?php } ?>

                                    </div>

                                </div>

                            </div>



                            <div class="pl-0 pl-sm-3 col-6 col-sm-12 col-lg-4 pt-2">

                                <?php $data['empty'] = null;

                                $this->load->view("sortez_vsv/includes/main_communes_select_article", $data); ?>

                            </div>



                            <div class="col-6 col-sm-12 col-lg-4 pt-2">

                                <div class="dropdown">

                                    <div class="btn btn-secondary dropdown-toggle border_content_soutenons" type="text" id="dropdownMenuButton_com_categ" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                        <span class="d-none d-sm-inline-block">Recherche par catégorie</span>

                                        <span class="d-contents d-sm-none d-md-none d-ld-none d-xl-none">Catégorie</span>

                                    </div>

                                    <div style="font-size:12px;" class="dropdown-menu main_communes_select" id="span_leftcontener2013_form_article" aria-labelledby="dropdownMenuButton_com">

                                        <?php if (isset($toCategorie_principale) && $toCategorie_principale != '' && $toCategorie_principale != null) { ?>

                                            <?php foreach ($toCategorie_principale as $toctages) { ?>

                                                <a class="dropdown-item <?= $toctages->nb_article ?>" 

                                                id= "article_categ_<?php echo $toctages->agenda_categid; ?>"

                                                href="javascript:void(0);" onclick="javascript:change_categ_filters(<?php echo $toctages->agenda_categid; ?>);"><?php echo $toctages->category; //$toctages->nb_article 

                                                                                                                                                                                                                        ?></a>

                                            <?php } ?>

                                        <?php } ?>

                                    </div>

                                </div>

                            </div>

                            <div class="pl-0 pl-sm-3 col-6 col-sm-12 col-lg-4 text-left pt-2">

                                <input class=" dropdown-toggle border_content_soutenons"
                                 id="zMotcle_value" type="text" placeholder="Recherches par mot clé" 
                                 value="<?php if (isset($localdata_zMotCle) && $localdata_zMotCle != '') echo $localdata_zMotCle; ?>">
                                 
                            </div>
                            
                        </div>
                        <!-- ceci est un test de filtre par mickael--> 
                        <? //this->load->view("sortez_vsv/includes/article_list_filter_date", $data); ?>
                        <!-- ceci est un test de filtre par mickael--> 
                        <div class="row">

                            <div class="col-sm-12 col-lg-4 offset-lg-2 pt-2">

                                <div class="btn btn-secondary reinit_soutenons_down w-100" onclick="javascript:btn_re_init_annonce_list();">

                                    Réinitialisez

                                </div>

                            </div>

                            <div class="col-sm-12 col-lg-4 pt-2">

                                <div class="btn btn-secondary applic_soutenons_down w-100" onclick="javascript:form_submit_search_annuaire_soutenons();">

                                    Appliquez

                                    votre

                                    choix

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>



        </div>

    </div>

</div>



<script type="text/javascript">

    function change_categ_filters(id_categ) {

        document.getElementById("inputagenda_categorie").value = id_categ;

        document.getElementById("dropdownMenuButton_com_categ").innerHTML = document.getElementById("article_categ_"+id_categ).innerHTML;

    }



    function change_media_select(id_media) {

        // if (id_media == "Nice Matin") {

        //     document.getElementById("inputagenda_article_type_id").value = "Nice";

        // } else {

        //     document.getElementById("inputagenda_article_type_id").value = id_media;

        // }



        // document.getElementById("Recherches_par_medias").innerHTML = id_media;

        document.getElementById("Recherches_par_medias").innerHTML = document.getElementById("articleType_select_id_"+id_media).innerHTML;

        // console.log(id_media);

        document.getElementById("article_categ_"+id_media).click();

    }



    function form_submit_search_annuaire_soutenons(keyword_value) {

        var base_url_visurba = '<?php echo site_url(); ?>';

        jQuery("#Id_main_bodylistannuaire_main").html('<div class="text-center w-100"><img src="' + base_url_visurba + 'application/resources/front/images/wait.gif" alt="loading...."/></div>');

        $("#frmRecherchePartenaire #zMotCle").val($("#zMotcle_value").val());



        var inputStringDepartementHidden_partenaires = document.getElementById("inputStringDepartementHidden_partenaires").value;

        var inputStringVilleHidden_partenaires = document.getElementById("inputStringVilleHidden_partenaires").value;

        var inputIdCommercant = document.getElementById("inputIdCommercant").value;

        //var inputagenda_article_type_id = document.getElementById("inputagenda_article_type_id").value;

        var inputagenda_categorie = document.getElementById("inputagenda_categorie").value;

        // var inputStringHidden = document.getElementById("inputStringHidden").value;

        // var inputStringHiddenSubCateg = document.getElementById("inputStringHiddenSubCateg").value;

        // var inputStringOrderByHidden = document.getElementById("inputStringOrderByHidden").value;

        var zMotCle = document.getElementById("zMotcle_value").value;

        // var inputAvantagePartenaire = document.getElementById("inputAvantagePartenaire").value;

        // var input_is_actif_coronna = document.getElementById("input_is_actif_coronna").value;

        // var input_is_actif_command = document.getElementById("input_is_actif_command").value;
        // var date_debut = document.getElementById("date_debut").value;
        // var date_fin = document.getElementById("date_fin").value;
        // var inputStringQuandHidden = document.getElementById("inputStringQuandHidden").value;





        jQuery.ajax({

            method: "POST",

            url: '<?php echo site_url("article/liste/?content_only_list=1"); ?>',

            data: {

                inputStringDepartementHidden_partenaires: inputStringDepartementHidden_partenaires,

                inputStringVilleHidden_partenaires: inputStringVilleHidden_partenaires,

                inputIdCommercant: inputIdCommercant,

                //inputagenda_article_type_id: inputagenda_article_type_id,

                inputagenda_categorie: inputagenda_categorie,

                zMotCle: zMotCle,

                // inputStringDatedebutHidden : date_debut,

                // inputStringDatefinHidden : date_fin,
                
                // inputStringQuandHidden : inputStringQuandHidden,


                // input_is_actif_command: input_is_actif_command

            },

            dataType: 'html',

            success: function(html) {

                jQuery('#id_mainbody_main').html(html);

            }

        });



        //$("#frmRecherchePartenaire").submit();

    }

</script>

<script type="text/javascript">

    function show_current_subcategcontent_soutenons(id_sousrubrique) {

        $("#inputStringHiddenSubCateg").val(id_sousrubrique);

        //$("#frmRecherchePartenaire").submit();

        //$("#div_subcateg_annuaire_contents").css("display","none");

        $("#dropdownMenuButton_com_sucateg").html($("#vsv_sub_categmain_" + id_sousrubrique).html());

    }

</script>