<nav class="d-flex d-lg-none d-xl-none navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="<?= site_url();?>">Magazine-Sortez</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?= site_url();?>">Accueil</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= site_url();?>annuaire">Les Bonnes adresses</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= site_url();?>article">L'actualité et la revue de presse</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= site_url();?>agenda">l'agenda événementiel</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://www.magazine-sortez.org/newsletter">Newsletter</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= site_url();?>auth/login">Mon compte</a>
            </li>
            <!-- <li class="nav-item">
                <a class="nav-link" href="http://www.soutenonslecommercelocale.fr">Soutenonslecommercelocale.fr</a>
            </li> -->
            <li class="nav-item">
                <a class="nav-link" href="https://www.magazine-sortez.org/nous-contacter">Contact</a>
            </li>
        </ul>
    </div>
</nav>
<!-- <div class =" " id="<?php //if(!$is_mobile) echo "translateMobileHide";
                        //else echo "translateMobileShow";?>">   
    <div id="google_translate_element" style=" left: 10%;"></div>
</div> -->

<!-- <style type="text/css">
    #translateMobileShow{
        text-align: center;
        border:1px solid grey;
        border-radius: 5px 5px 5px 5px;
        box-shadow: 2px 2px 10px #000;
        margin-top: -5px;
        width: 100%;
    }   
</style> -->

<div class="<?php if(!$is_mobile) echo "translateMobileHide";
                                   else echo "translateMobileShow";?>">
    <div class="container">
        <div class="row justify-content-center">
            <div class ="col-xs-4">
                <div id="google_translate_element"style="/*text-align: center;*/"></div>
            </div>
            <div class="col-xs-4" style="text-align: left;"><i class="fas fa-sort-down"></i></div>
        </div>
    </div>
</div>  
        
<style type="text/css">
    .translateMobileShow{text-align: right;}   
    .d-flex.d-lg-none.d-xl-none.navbar.navbar-expand-lg.navbar-dark.bg-dark{
        margin-bottom: -4px;
    }
</style>
<script type="text/javascript">
    $(function(){
        $(".translateMobileHide").remove();
    })
</script>