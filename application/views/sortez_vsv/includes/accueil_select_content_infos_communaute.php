<div class="container accueil_select_content_2 text-center pt-3">
    <div class="row">
        <div class="col-12">
            <p>
                <a href="<?php echo site_url('vivresaville/ListVille'); ?>"
                   class="vsv_accueil_btn vsv_select_link plus_infos_link">RETOUR PAGE D'ACCUEIL...</a>
            </p>
            <p class="title_1 pt-5 pl-5 pr-5">
                COMMUNAUTÉS DE COMMUNES, INTERCOMMUNALITÉS !...<br/>
                NOUS VOUS PROPOSONS DE FÉDÉRER GRATUITEMENT<br/>
                LES INFORMATIONS INSTITUTIONNELLES ET PRIVÉES<br/>
                DE VOTRE TERRITOIRE !...
            </p>
            <p class="txt_rose_color strong_txt pt-5 pl-5 pr-5">
                Un annuaire complet - L’actualité - Un agenda événementiel<br/>
                Des bonnes affaires - Une carte de fidélité - Des boutiques en ligne
            </p>
            <p class="pt-3 pl-5 pr-5">
                Les données sont déposées par les acteurs administratifs, associatifs et économiques<br/>
                de la cité sur un compte personnel et sécurisé.
            </p>
            <p class="">
                <img
                    src="<?php echo base_url(); ?>application/resources/front/images/vivresaville_custom/wpa89873e4_06.png"
                    class="mt-0 border-0 img-fluid"/>
            </p>
            <p class="title_3 pb-3">
                Les sites s’adaptent à tous les écrans
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <p class="title_1 txt_rose_color title_partenaire pl-5 pr-5 pb-3">
                LES AVANTAGES DE LA COLLECTIVITÉ TERRITORIALE
            </p>
            <p>
            <ul class="accueil_list_ul">
                <li>Un site gratuit</li>
                <li>L’URL du site est personnalisé au nom du territoire ex : www.monterritoire.vivresaville.fr</li>
                <li>Il offre un nouveau réseau dynamique intercommunal mutualiste.</li>
                <li>Il regroupe sur un annuaire local fédérateur l’ensemble des acteurs de la cité et les structures communales et patrimoniales</li>
                <li>Il favorise la diffusion de l’actualité intercommunale (articles et événements)</li>
                <li>Il donne l’accès aux commerces de proximité à des outils marketing individuels et mutualisés (bons plans, conditions de fidélisation, carte de fidélité)</li>
                <li>Il met le e-commerce à la portée de tous (module boutique en ligne avec le règlement par Paypal)</li>
                <li>Il ouvre la porte du web et la mobilité aux associations et professionnels.</li>
                <li>Il stimule l’intérêt intercommunal des administrés pour la vie du territoire.</li>
                <li>Chaque site est multilingue.</li>
                <li>Le site bénéficie d’une visibilité sur les tablettes et mobiles</li>
                <li>Les données des communes et des acteurs de votre territoire sont également référencés sur notre site départemental sortez.org.</li>
            </ul>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-12 pt-5">
            <p class="title_1 title_partenaire pl-5 pr-5">
                NOUS PERSONNALISONS ET FÉDÉRONS SUR UN MÊME SITE VIVRESAVILLE.,<br/>
                LES DONNÉES DÉPOSÉES PAR LES COLLECTIVITÉS, ASSOCIATIONS<br/>
                ET PROFESSIONNELS DE CHAQUE TERRITOIRE...
            </p>
            <p class="pt-3">
                <img
                    src="<?php echo base_url(); ?>application/resources/front/images/vivresaville_custom/wpc7712e8e_05_06.jpg"
                    class="mt-0 border-0 img-fluid"/>
            </p>
            <p class="pt-5 pb-5 text-center">
                <a href="https://spark.adobe.com/page/sZkD4XUv0HdeB/" target="_blank" class="vsv_accueil_btn vsv_select_link" style="width: 200px; display: inline;">PLUS D'INFORMATIONS, VISUALISEZ...</a>
            </p>
        </div>
    </div>
    <div class="row" style="background-color: #E6E6E6;">
        <div class="col-12 pl-4 pr-4 pt-3 pb-3">
            <p class="text-center title_1">
                AVEC NOTRE PACK EXPORT, VOUS INTÈGREZ D’UN SIMPLE CLIC
                SUR VOTRE PROPRE SITE COMMUNAUTAIRE,
                LA TOTALITÉ DES ARTICLES ET ÉVÉNEMENTS DÉPOSÉS
                EN TEMPS RÉEL PAR LES COMMUNES DE VOTRE TERRITOIRE …
            </p>
        </div>
        <div class="col-12">
            <img
                src="<?php echo base_url(); ?>application/resources/front/images/vivresaville_custom/wpea6f0665_05_06.jpg"
                class="border-0 text-center img-fluid"/>
        </div>
        <div class="col-12 pt-3 pb-5 text-center mt-5">
            <a href="https://spark.adobe.com/page/3m6hhEY59AdY5/" target="_blank" class="vsv_accueil_btn vsv_select_link" style="width: 200px; display: inline;">PLUS D'INFORMATIONS...</a>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <p class="title_1 txt_rose_color title_partenaire pl-5 pr-5 pt-5">
                PLUS D’INFORMATIONS, VISUALISEZ NOS PRÉSENTATIONS !…
            </p>
        </div>
    </div>
    <div class="row accueil_visu_presentation pb-5">
        <div class="col-sm-6 p-3">
            <div class="m-3 p-5 visu_communalite_1">
                Découvrez
                vivresaville.fr !
            </div>
            <div class="text-center mt-5">
                <a href="https://spark.adobe.com/page/M6HL2fA3cPUy4/" target="_blank" class="vsv_accueil_btn vsv_select_link " style="width: 200px; display: inline;">VISUALISEZ...</a>
            </div>
        </div>
        <div class="col-sm-6 p-3">
            <div class="m-3 p-5 visu_communalite_2">
                Disposez gratuitement
                sur votre territoire
                d'un site fédérateur !
            </div>
            <div class="text-center mt-5">
                <a href="https://spark.adobe.com/page/sZkD4XUv0HdeB/" target="_blank" class="vsv_accueil_btn vsv_select_link " style="width: 200px; display: inline;">VISUALISEZ...</a>
            </div>
        </div>
    </div>
    <div class="row accueil_visu_presentation pb-5">
        <div class="col-sm-6 p-3">
            <div class="m-3 p-5 visu_communalite_3">
                Développez et maîtrisez
                la diffusion de votre actualité !
            </div>
            <div class="text-center mt-5">
                <a href="https://spark.adobe.com/page/RGN2AX7EqvPb8/" target="_blank" class="vsv_accueil_btn vsv_select_link " style="width: 200px; display: inline;">VISUALISEZ...</a>
            </div>
        </div>
        <div class="col-sm-6 p-3">
            <div class="m-3 p-5 visu_communalite_4">
                Regroupez sur votre site l'actualité de vos communes d'un simple clic !... !
            </div>
            <div class="text-center mt-5">
                <a href="https://spark.adobe.com/page/3m6hhEY59AdY5/" target="_blank" class="vsv_accueil_btn vsv_select_link " style="width: 200px; display: inline;">VISUALISEZ...</a>
            </div>
        </div>
    </div>
</div>