<div class="row pt-0">

    <div class="col p-2">

        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annuaire_filter_triage(1);">
            <input type="radio" class="form-check-input" name="inputStringOrderByHidden_partenaires"
                   id="optionsRadiosTriage_1" value="1"
                   <?php if (isset($iOrderBy) && $iOrderBy == '1') { ?>checked<?php } ?>>
            Les partenaires les plus récents
        </label>

    </div>
    <div class="col p-2">

        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annuaire_filter_triage(2);">
            <input type="radio" class="form-check-input" name="inputStringOrderByHidden_partenaires"
                   id="optionsRadiosTriage_2" value="2"
                   <?php if (isset($iOrderBy) && $iOrderBy == '2') { ?>checked<?php } ?>>
            Les partenaires les plus visitées
        </label>

    </div>
    <!--<div class="col p-2">

        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annuaire_filter_triage(3);">
            <input type="radio" class="form-check-input" name="inputStringOrderByHidden_partenaires"
                   id="optionsRadiosTriage_3" value="3"
                   <?php if (isset($iOrderBy) && $iOrderBy == '3') { ?>checked<?php } ?>>
            Les partenaires les plus anciens
        </label>

    </div>-->
</div>