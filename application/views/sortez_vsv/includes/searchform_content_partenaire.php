<form name="frmRecherchePartenaire"
      id="frmRecherchePartenaire"
      method="post"
      action="<?php echo site_url("annuaire/"); ?>">
    <div class="">
        <input id="inputStringDepartementHidden_partenaires"
               name="inputStringDepartementHidden"
               type="hidden"
               value=""/>
        <input id="inputStringVilleHidden_partenaires"
               name="inputStringVilleHidden"
               type="hidden"
               value=""/>
        <input name="inputStringHidden"
               id="inputStringHidden"
               type="hidden"
               value="">
        <input name="inputStringHiddenSubCateg"
               id="inputStringHiddenSubCateg"
               type="hidden"
               value="">
        <input name="inputStringOrderByHidden"
               id="inputStringOrderByHidden"
               type="hidden"
               value="">
        <input name="zMotCle"
               id="zMotCle"
               type="hidden"
               value="">
        <input name="inputAvantagePartenaire"
               id="inputAvantagePartenaire"
               type="hidden"
               value="">
        <input name="input_is_actif_coronna"
               id="input_is_actif_coronna"
               type="hidden"
               value="">
        <input name="input_is_actif_command"
               id="input_is_actif_command"
               type="hidden"
               value="">
    </div>
</form>


<div class="d-none d-md-block d-lg-block d-xl-block container text-center mb-4">
    <div class="row">
        <div class="col-md-6 p-0">
            <div class="row">
                <div class="col-8 p-0 pt-2 text-right">
                    Accès direct aux pages de présentation
                </div>
                <div class="col-4">
                    <div class="btn_more text-center">
                        +
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 p-0">
            <div class="row">
                <div class="col-4">
                    <div class="btn_more_blue text-center">
                        +
                    </div>
                </div>
                <div class="col-8 p-0 pt-2 text-left">
                    Accès direct aux formulaires de commande en ligne
                </div>
            </div>
        </div>
    </div>
</div>

<div class="d-block d-md-none d-lg-none d-xl-none container text-center mb-4">
    <div class="">
        <div class="col-md-6 p-0">
            <div class="row">
                <div class="col-4">
                    <div class="btn_more text-center">
                        +
                    </div>
                </div>
                <div class="col-8 p-0 pt-2 text-left">
                    Accès direct aux pages de présentation
                </div>
            </div>
        </div>
        <div class="col-md-6 p-0">
            <div class="row">
                <div class="col-4">
                    <div class="btn_more_blue text-center">
                        +
                    </div>
                </div>
                <div class="col-8 p-0 pt-2 text-left">
                    Accès direct aux formulaires de commande en ligne
                </div>
            </div>
        </div>
    </div>
</div>


<style>
    .ico_container_sout_annu {
        width: 1024px;
        margin: 0 auto;
        font-size: 1.2em;
    }
    .main_banner_container_soutenons {
        margin-bottom: 5px !important;
        padding-bottom: 0 !important;
    }
</style>
    
    
