<style type="text/css">
    body {
        background: none !important;
    }
</style>

<?php $data["zTitle"] = 'Vivresaville'; ?>
<?php $this->load->view("sortez_vsv/includes/main_header", $data); ?>
<div class="row ml-0 mr-0 mt-5">
    <div class="alert alert-success" role="alert">
        <strong>Confirmation !</strong> Suppréssion avec succès !
    </div>
</div>
<?php $this->load->view("sortez_vsv/includes/main_footer", $data); ?>