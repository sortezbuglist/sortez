<?php if (!isset($data)) $data['data_init'] = true; ?>



<?php $ij = 0; ?>
<?php
$thisss =& get_instance();
$thisss->load->library('session');
$main_width_device = $thisss->session->userdata('main_width_device');
$main_tablet_screen = false;
if (isset($main_width_device) && $main_width_device != "" && $main_width_device != "0" && is_numeric($main_width_device) && floatval($main_width_device) >= 991 && floatval($main_width_device) <= 1199) {
    $main_tablet_screen = true;
} else {
    $main_tablet_screen = false;
}
?>

<div class="col-12">
    <?php foreach ($toListeAnnonce as $oListeAnnonce) { ?>
        <?php
        if ($oListeAnnonce->etat == 1) $zEtat = "Neuf";
        else if ($oListeAnnonce->etat == 0) $zEtat = "Revente";
        else if ($oListeAnnonce->etat == 2) $zEtat = "Service";
        else $zEtat = "";
        ?>

        <?php
        $thisss =& get_instance();
        $thisss->load->model("ion_auth_used_by_club");
        $ionauth_id = $thisss->ion_auth_used_by_club->get_ion_id_from_commercant_id($oListeAnnonce->annonce_commercant_id);

        $photoCommercant_path = "application/resources/front/photoCommercant/imagesbank/" . $ionauth_id . "/";
        $photoCommercant_path_old = "application/resources/front/images/";
        ?>


        <div class="col-card-sortez p-2 line_number_top_<?= $ij ?> <?= $main_tablet_screen; ?> <?= $main_width_device; ?>">

            <div id="item_line_<?php echo $oListeAnnonce->annonce_id; ?>"
                 class="annonce_item_line main_body_list_row col-12 p-0 pb-3">

                <div class="col-sm-12 p-0 annonce_list_img"
                     id="item_list_img_<?php echo $oListeAnnonce->annonce_id; ?>">

                    <?php
                    $image_home_vignette = "";
                    if (isset($oListeAnnonce->annonce_photo1) && $oListeAnnonce->annonce_photo1 != "" && is_file($photoCommercant_path . $oListeAnnonce->annonce_photo1) == true) {
                        $image_home_vignette = $oListeAnnonce->annonce_photo1;
                    } else if ($image_home_vignette == "" && isset($oListeAnnonce->annonce_photo2) && $oListeAnnonce->annonce_photo2 != "" && is_file($photoCommercant_path . $oListeAnnonce->annonce_photo2) == true) {
                        $image_home_vignette = $oListeAnnonce->annonce_photo2;
                    } else if ($image_home_vignette == "" && isset($oListeAnnonce->annonce_photo3) && $oListeAnnonce->annonce_photo3 != "" && is_file($photoCommercant_path . $oListeAnnonce->annonce_photo3) == true) {
                        $image_home_vignette = $oListeAnnonce->annonce_photo3;
                    } else if ($image_home_vignette == "" && isset($oListeAnnonce->annonce_photo4) && $oListeAnnonce->annonce_photo4 != "" && is_file($photoCommercant_path . $oListeAnnonce->annonce_photo4) == true) {
                        $image_home_vignette = $oListeAnnonce->annonce_photo4;
                    } else if ($image_home_vignette == "" && isset($oListeAnnonce->annonce_photo1) && $oListeAnnonce->annonce_photo1 != "" && is_file($photoCommercant_path_old . $oListeAnnonce->annonce_photo1) == true) {
                        $image_home_vignette = $oListeAnnonce->annonce_photo1;
                    } else if ($image_home_vignette == "" && isset($oListeAnnonce->annonce_photo2) && $oListeAnnonce->annonce_photo2 != "" && is_file($photoCommercant_path_old . $oListeAnnonce->annonce_photo2) == true) {
                        $image_home_vignette = $oListeAnnonce->annonce_photo2;
                    } else if ($image_home_vignette == "" && isset($oListeAnnonce->annonce_photo3) && $oListeAnnonce->annonce_photo3 != "" && is_file($photoCommercant_path_old . $oListeAnnonce->annonce_photo3) == true) {
                        $image_home_vignette = $oListeAnnonce->annonce_photo3;
                    } else if ($image_home_vignette == "" && isset($oListeAnnonce->annonce_photo4) && $oListeAnnonce->annonce_photo4 != "" && is_file($photoCommercant_path_old . $oListeAnnonce->annonce_photo4) == true) {
                        $image_home_vignette = $oListeAnnonce->annonce_photo4;
                    }
                    ?>


                    <a href='<?php echo site_url("front/annonce/detailAnnonce/" . $oListeAnnonce->annonce_id); ?>'
                       target="_blank"
                       title="<?php echo $oListeAnnonce->texte_courte; ?>">
                        <?php
                        if ($image_home_vignette != "") {
                            if (isset($image_home_vignette) && $image_home_vignette != "" && is_file($photoCommercant_path . $image_home_vignette) == true)
                                echo '<img src="' . base_url() . $photoCommercant_path . $image_home_vignette . '" width="100%" height="246" border="0" id="pic_965" name="pic_965" title="" alt=""/>';
                            else echo '<img src="' . base_url() . $photoCommercant_path_old . $image_home_vignette . '" width="100%" height="246" border="0" id="pic_965" name="pic_965" title="" alt=""/>';
                        } else {
                            $image_home_vignette_to_show = base_url()."assets/images/image_loop.jpeg.jpg";
                            echo '<img src="' . $image_home_vignette_to_show . '" width="100%" height="246" border="0" id="pic_965" name="pic_965" title="" alt=""/>';
                        }
                        ?>
                    </a>
                    <div
                            style="position:absolute; bottom:0; width:100%; height:40px; background:rgba(0, 0, 0, 0.6); color:#FFFFFF; font-weight: normal; text-transform:uppercase; text-align:center; line-height:40px;"><?php echo $oListeAnnonce->rubrique; ?></div>
                </div>

                <div class="col-sm-12 annonce_list_details"
                     id="item_list_details_<?php echo $oListeAnnonce->annonce_id; ?>"
                     style="padding-top:10px; padding-bottom:10px; background-color:white!important;">

                    <p style='background-color: transparent;
        color: #000000;
        font-family: "Arial",sans-serif;
        font-size: 16px;
        font-style: normal;
        font-variant: normal;
        font-weight: 700;
        line-height: 20px;
        margin: 0 0 5px 0 !important;
        padding: 0 !important;
        text-decoration: none;
        vertical-align: 0;'><?php if (isset($oListeAnnonce->subcateg)) echo $oListeAnnonce->subcateg; ?></p>

                    <div class="p_annonce_texte_courte_text col-12 p-0"
                         style='background-color: transparent;
    color: #000000;
    font-family: "Arial",sans-serif;
    font-size: 16px;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 20px;
    text-decoration: none;
    text-align:center; height:85px; overflow:hidden;
    vertical-align: 0;'><?php echo $oListeAnnonce->texte_courte; ?></div>


                    <div class="col-xs-12 textaligncenter"
                         style="padding:15px;">
                        <?php if ((isset($oListeAnnonce->prix_vente) && $oListeAnnonce->prix_vente != 0) || (isset($oListeAnnonce->prix_ancien) && $oListeAnnonce->prix_ancien != 0)) { ?>
                            <span class="p_annonce_list_ancienprix padding0"
                                  style='
   	background-color: transparent;
    color: #008000;
    font-family: "Arial",sans-serif;
    font-size: 26.7px;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 33px;
    text-decoration: none;
    vertical-align: 0;
    text-align:center;
    '>
    	<?php if ((isset($oListeAnnonce->prix_vente) && $oListeAnnonce->prix_vente != 0) && (isset($oListeAnnonce->prix_ancien) && $oListeAnnonce->prix_ancien != 0)) { ?>
            <?php echo $oListeAnnonce->prix_ancien; ?> &euro;
        <?php } else { ?>
            <?php echo $oListeAnnonce->prix_ancien; ?><?php //echo $oListeAnnonce->prix_vente ; ?> &euro;
        <?php } ?>

 	</span>
                        <?php } ?>

                        <?php if (isset($oListeAnnonce->prix_vente) && $oListeAnnonce->prix_vente != 0 && isset($oListeAnnonce->prix_ancien) && $oListeAnnonce->prix_ancien != 0) { ?>
                            <span class="p_annonce_list_prixvente padding0"
                                  style='
    background-color: transparent;
    color: #000000;
    font-family: "Arial",sans-serif;
    font-size: 24px;
    font-style: normal;
    font-variant: normal;
    font-weight: 700;
    margin: 5px 0 0 0 !important;
    padding: 0px;
    line-height: 30px;
    text-decoration:line-through;
    vertical-align: 0;'><?php echo $oListeAnnonce->prix_vente; ?>
                                &euro;
	</span>
                        <?php } ?>
                    </div>


                    <div class="p_annonce_state_text"
                         style='
    background-color: transparent;
    color: #000000;
    font-family: "Arial",sans-serif;
    font-size: 14px;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 16px; padding:5px 0;
    text-decoration: none; text-align:center;
    vertical-align: 0;
    '><?php if ($zEtat != "") { ?>Etat : <?php echo $zEtat; ?><?php } ?></div>

                    <div class="col-xs-8 padding0">
                        <div class="col-12 p-0"
                             style='
    background-color: transparent;
    color: #008000;
    font-family: "Arial",sans-serif;
    font-size: 13.3px;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 16px;
    text-decoration: none;
    vertical-align: 0;
    text-align:left;
      '><?php echo $oListeAnnonce->NomSociete; ?> </div>
                        <div class="col-12 p-0"
                             style='background-color: transparent;
    color: #008000;
    font-family: "Arial",sans-serif;
    font-size: 13.3px;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 16px;
    text-decoration: none;
    vertical-align: 0;
    text-align:left;'><?php echo $oListeAnnonce->ville; ?></div>
                        <?php
                        $iNombreAnnonceParDiffuseur = 0;
                        if (isset($oListeAnnonce->IdCommercant)) {
                            if ($oListeAnnonce->IdCommercant != null && $oListeAnnonce->IdCommercant != 0 && $oListeAnnonce->IdCommercant != "") {
                                $iNombreAnnonceParDiffuseur = $mdlannonce->nombreAnnonceParDiffuseur($oListeAnnonce->IdCommercant);
                            }
                        }
                        ?>
                        <div class="col-12 p-0"
                             style='background-color: transparent;
        color: #000000;
        font-family: "Arial",sans-serif;
        font-size: 13.3px;
        font-style: normal;
        font-variant: normal;
        font-weight: normal;
        line-height: 16px;
        text-decoration: none;
        vertical-align: 0;'><?php echo $iNombreAnnonceParDiffuseur; ?>
                            annonces
                            d&eacute;pos&eacute;es
                        </div>
                    </div>


                    <div class="annonce_list_info_btn padding0 col-xs-4"
                         style="text-align: right;">
                        <a href='<?php echo site_url("front/annonce/detailAnnonce/" . $oListeAnnonce->annonce_id); ?>'
                           target="_blank"
                           title="<?php echo $oListeAnnonce->texte_courte; ?>"><img
                                    src="<?php echo GetImagePath("privicarte/"); ?>/plus_green.png"></a>
                    </div>


                </div>


            </div>

        </div>


        <?php $ij = $ij + 1; ?>

    <?php } ?>
</div>


<?php if (isset($_GET['content_only_list']) && $_GET['content_only_list'] == '1') {
} else { ?>
    <div class="container pb-3"
         style="display:table;">
        <?php
        $data['empty'] = null;
        if (strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')) {
            //$this->load->view("sortez_vsv/includes/annonce_list_listing_ipad.php", $data);
            $this->load->view("sortez_vsv/includes/annonce_list_listing.php", $data);
        } else {
            $this->load->view("sortez_vsv/includes/annonce_list_listing.php", $data);
        }
        ?>
    </div>
<?php } ?>


