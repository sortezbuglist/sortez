<style type="text/css">
    @media screen and (max-width: 768px) {
        #span_leftcontener2013_form_partenaires .vsv_subcateg_filter .leftcontener2013title_vs,
		#span_leftcontener2013_form_partenaires .leftcontener2013content div label.subcateb_filter_banner,
		#span_leftcontener2013_form_bonplans .vsv_subcateg_filter .leftcontener2013title_vs,
		#span_leftcontener2013_form_bonplans .leftcontener2013content div label.subcateb_filter_banner,
        #span_leftcontener2013_form_annonces .vsv_subcateg_filter .leftcontener2013title_vs,
		#span_leftcontener2013_form_annonces .leftcontener2013content div label.subcateb_filter_banner
		{
            background-repeat: no-repeat !important;
            background-position: right center ;
            background-size: 3% !important;
			background-position-x: 97%;
		}
        #span_leftcontener2013_form_bonplans .vsv_subcateg_filter .leftcontener2013title_vs,
        #span_leftcontener2013_form_annonces .vsv_subcateg_filter .leftcontener2013title_vs,
        #span_leftcontener2013_form_partenaires .vsv_subcateg_filter .leftcontener2013title_vs {
            background-image: url(<?php echo base_url();?>application/resources/front/images/vivresaville_custom/main_menu_item_img.png) !important;
        }
		#span_leftcontener2013_form_bonplans .leftcontener2013content div label.subcateb_filter_banner,
		#span_leftcontener2013_form_annonces .leftcontener2013content div label.subcateb_filter_banner,
		#span_leftcontener2013_form_partenaires .leftcontener2013content div label.subcateb_filter_banner
		{
			background-image: url(<?php echo base_url();?>application/resources/front/images/vivresaville_custom/main_menu_subcateg_img.png) !important;
		}
		#span_leftcontener2013_form_bonplans .leftcontener2013content div label.subcateb_filter_banner.loading,
		#span_leftcontener2013_form_annonces .leftcontener2013content div label.subcateb_filter_banner.loading,
		#span_leftcontener2013_form_partenaires .leftcontener2013content div label.subcateb_filter_banner.loading
		{
			background-image: url(<?php echo base_url();?>application/resources/sortez/images/loading.gif) !important;
            background-size: 7% !important;
		}
		.main_subcateg_tab_init .subcateb_filter_banner {
			background-image: url(<?php echo base_url();?>application/resources/front/images/vivresaville_custom/main_menu_reinit_all.png) !important;
			background-position: left center;
			background-repeat: no-repeat !important;
			background-size: 20px;
			background-position-x: 18px;
		}
    }
</style>