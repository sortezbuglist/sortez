<!-- includes css js -->
    <?php $this->load->view("sortez_vsv/includes/main_header", $data); ?>
   
<!-- src="https://www.sortez.org/application/resources/front/photoCommercant/imagesbank/1992/80803726-36e6-4bb6-a214-9d474f18a1e5.png" -->



<div id="container" class="photos clearfix">

    <?php
        $dir= dirname(__DIR__,2)."/resources/front/photoCommercant/imagesbank/1992/";

        $listImage = scandir($dir);
        $path = base_url()."application/resources/front/photoCommercant/imagesbank/1992/";
    ?>

    <?php foreach ($data as $event) : ?>
        <?php if (in_array($event->photo1,$listImage)) : ?>
            <div class="photo">
                <a href="http://www.flickr.com/photos/nemoorange/5013039951/">
                    <img src="<?= $path.$event->photo1 ?>" alt="" />
                </a>
                <p class="title_event"><?= $event->nom_manifestation  ?></p>
            </div>
        <? endif ?>
    <?php endforeach ?>

    <!-- <div class="photo">
    <a href="http://www.flickr.com/photos/nemoorange/5013039885/" title="Officer by Dave DeSandro, on Flickr"><img src="http://farm5.static.flickr.com/4131/5013039885_0d16ac87bc.jpg" alt="Officer" /></a>
    <p class="title_event">Title event</p>
</div>

<div class="photo">
    <a href="http://www.flickr.com/photos/nemoorange/5013039583/" title="Tony by Dave DeSandro, on Flickr"><img src="http://farm5.static.flickr.com/4086/5013039583_26717f6e89.jpg" alt="Tony" /></a>
    <p class="title_event">Title event</p>
</div>

<div class="photo">
    <a href="http://www.flickr.com/photos/nemoorange/5013646070/" title="Kendra by Dave DeSandro, on Flickr"><img src="http://farm5.static.flickr.com/4146/5013646070_f1f44b1939.jpg" alt="Kendra" /></a>
    <p class="title_event">Title event</p>
</div>

<div class="photo">
    <a href="http://www.flickr.com/photos/nemoorange/5013039541/" title="Giraffe by Dave DeSandro, on Flickr"><img src="https://static.wixstatic.com/media/5fa146_d0a9e37b185a4751b3dee25d836e7efc~mv2.jpg/v1/fill/w_216,h_299,al_c,q_80,usm_0.66_1.00_0.01/5fa146_d0a9e37b185a4751b3dee25d836e7efc~mv2.jpg" alt="Giraffe" /></a>
    <p class="title_event">Title event</p>
</div>

<div class="photo">
    <a href="http://www.flickr.com/photos/nemoorange/5013039741/" title="Gavin by Dave DeSandro, on Flickr"><img src="http://farm5.static.flickr.com/4153/5013039741_d860fb640b.jpg" alt="Gavin" /></a>
    <p class="title_event">Title event</p>
</div>

<div class="photo">
    <a href="http://www.flickr.com/photos/nemoorange/5013039697/" title="Anita by Dave DeSandro, on Flickr"><img src="https://static.wixstatic.com/media/5fa146_187e0de4b87c43c3abd021d9d21303c4~mv2.jpg/v1/fill/w_216,h_324,al_c,q_80,usm_0.66_1.00_0.01/5fa146_187e0de4b87c43c3abd021d9d21303c4~mv2.jpg" alt="Anita" /></a>
    <p class="title_event">Title event</p>
</div>

<div class="photo">
    <a href="http://www.flickr.com/photos/nemoorange/5013646314/" title="Take My Portrait by Dave DeSandro, on Flickr"><img src="http://farm5.static.flickr.com/4124/5013646314_c7eaf84918.jpg" alt="Take My Portrait" /></a>
    <p class="title_event">Title event</p>
</div>

<div class="photo">
    <a href="http://www.flickr.com/photos/nemoorange/5013040075/" title="Wonder by Dave DeSandro, on Flickr"><img src="http://farm5.static.flickr.com/4089/5013040075_bac12ff74e.jpg" alt="Wonder" /></a>
    <p class="title_event">Title event</p>
</div> -->

</div>
<!-- #container -->

<script src="jquery.js"></script>
<script src="masonry.js"></script>

<script>
    $(function() {

        var $container = $('#container');
        $container.imagesLoaded(function() {
            $container.masonry();
        });


    });
</script>