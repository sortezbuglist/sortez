<?php
$thisss =& get_instance();
$thisss->load->library('session');
$main_width_device = $thisss->session->userdata('main_width_device');
$main_tablet_screen = false;
if (isset($main_width_device) && $main_width_device != "" && $main_width_device != "0" && is_numeric($main_width_device) && floatval($main_width_device) >= 991 && floatval($main_width_device) <= 1199) {
    $main_tablet_screen = true;
} else {
    $main_tablet_screen = false;
}
?>

<?php if (isset($toListeFidelity_remise) && count($toListeFidelity_remise) > 0) { ?>
    <?php foreach ($toListeFidelity_remise as $oListeFidelity_remise) { ?>
        <div class="col-card-sortez p-2 fidelity_item_div">

            <div class="col-12 p-0 main_body_list_row fidelity_item_container">

                <?php
                $objasscommrubr = $this->mdlcommercant->GetRubriqueId($oListeFidelity_remise->IdCommercant);
                if ($objasscommrubr) $objrubr = $this->mdlcategorie->GetById($objasscommrubr->IdRubrique);
                ?>

                <div class="col-12 p-0 fidelity_img_content" style="background-image: url(<?php

                $thisss =& get_instance();
                $thisss->load->model("ion_auth_used_by_club");
                $thisss->load->model("Commercant");
                $ionauth_id = $thisss->ion_auth_used_by_club->get_ion_id_from_commercant_id($oListeFidelity_remise->IdCommercant);
                $oInfoCommercant = $thisss->Commercant->GetById($oListeFidelity_remise->IdCommercant);
                $photoCommercant_path = "application/resources/front/photoCommercant/imagesbank/" . $ionauth_id . "/";
                $photoCommercant_path_old = "application/resources/front/photoCommercant/images/";
                $image_home_vignette = "";
                if (isset($oInfoCommercant->Photo1) && $oInfoCommercant->Photo1 != "" && is_file($photoCommercant_path . $oInfoCommercant->Photo1) == true) {
                    $image_home_vignette = $oInfoCommercant->Photo1;
                } else if ($image_home_vignette == "" && isset($oInfoCommercant->Photo2) && $oInfoCommercant->Photo2 != "" && is_file($photoCommercant_path . $oInfoCommercant->Photo2) == true) {
                    $image_home_vignette = $oInfoCommercant->Photo2;
                } else if ($image_home_vignette == "" && isset($oInfoCommercant->Photo3) && $oInfoCommercant->Photo3 != "" && is_file($photoCommercant_path . $oInfoCommercant->Photo3) == true) {
                    $image_home_vignette = $oInfoCommercant->Photo3;
                } else if ($image_home_vignette == "" && isset($oInfoCommercant->Photo4) && $oInfoCommercant->Photo4 != "" && is_file($photoCommercant_path . $oInfoCommercant->Photo4) == true) {
                    $image_home_vignette = $oInfoCommercant->Photo4;
                } else if ($image_home_vignette == "" && isset($oInfoCommercant->Photo5) && $oInfoCommercant->Photo5 != "" && is_file($photoCommercant_path . $oInfoCommercant->Photo5) == true) {
                    $image_home_vignette = $oInfoCommercant->Photo5;
                } else if ($image_home_vignette == "" && isset($oInfoCommercant->Photo1) && $oInfoCommercant->Photo1 != "" && is_file($photoCommercant_path_old . $oInfoCommercant->Photo1) == true) {
                    $image_home_vignette = $oInfoCommercant->Photo1;
                } else if ($image_home_vignette == "" && isset($oInfoCommercant->Photo2) && $oInfoCommercant->Photo2 != "" && is_file($photoCommercant_path_old . $oInfoCommercant->Photo2) == true) {
                    $image_home_vignette = $oInfoCommercant->Photo2;
                } else if ($image_home_vignette == "" && isset($oInfoCommercant->Photo3) && $oInfoCommercant->Photo3 != "" && is_file($photoCommercant_path_old . $oInfoCommercant->Photo3) == true) {
                    $image_home_vignette = $oInfoCommercant->Photo3;
                } else if ($image_home_vignette == "" && isset($oInfoCommercant->Photo4) && $oInfoCommercant->Photo4 != "" && is_file($photoCommercant_path_old . $oInfoCommercant->Photo4) == true) {
                    $image_home_vignette = $oInfoCommercant->Photo4;
                } else if ($image_home_vignette == "" && isset($oInfoCommercant->Photo5) && $oInfoCommercant->Photo5 != "" && is_file($photoCommercant_path_old . $oInfoCommercant->Photo5) == true) {
                    $image_home_vignette = $oInfoCommercant->Photo5;
                }
                if ($image_home_vignette != "") {
                    if (isset($image_home_vignette) && $image_home_vignette != "" && is_file($photoCommercant_path . $image_home_vignette) == true)
                        echo base_url() . $photoCommercant_path . $image_home_vignette;
                    else echo base_url() . $photoCommercant_path_old . $image_home_vignette;
                } else {
                    echo GetImagePath("front/") . "/no_image_bonplan.png";
                }

                ?>);">
                    <div style="position:absolute; bottom:0; width:100%; height:40px; background:rgba(0, 0, 0, 0.6); color:#FFFFFF; font-weight: normal; text-transform:uppercase; text-align:center; line-height:40px;"><?php echo $oListeFidelity_remise->Nom; ?></div>
                </div>
                <div class="col-12 fidelity_txt_content">

                    <div class="row">
                        <div class="col-12 fidelity_NomSociete"><?php echo $oListeFidelity_remise->NomSociete; ?></div>


                        <div class="col-12 fidelity_ico_sign_content"
                             style="background-image: url(<?php echo GetImagePath("privicarte/"); ?>/ico_sign_remise_nv.png);"><?php echo $oListeFidelity_remise->montant; ?>
                            <span class="fidelity_percent_exposant"><?php if (isset($oListeFidelity_remise->value_type) && $oListeFidelity_remise->value_type == "1") echo '%'; else echo '&euro;'; ?></span>
                        </div>


                        <div class="col-12" style="text-align:justify; padding:15px 15px 0;">
                            <div class="col-12 p-0 fidelity_title"><?php echo $oListeFidelity_remise->description; ?></div>
                            <div class="col-12 p-0 fidelity_description">Expire
                                le <?php echo convert_Sqldate_to_Frenchdate($oListeFidelity_remise->date_fin); ?></div>
                        </div>
                        <div class="col-12 p-0" style="text-align: right;position: absolute;bottom: 5px;right: 5px;">
                            <div class="col-sm-12 fidelity_btn_infos p-0">

                                <a href="<?php echo site_url($oListeFidelity_remise->nom_url . "/notre_fidelisation/remise/" . $oListeFidelity_remise->id); ?>" target="_blank"
                                    title="Fid&eacute;lit&eacute;">
                                    <img src="<?php echo GetImagePath("privicarte/"); ?>/btn_infos_fidelity.png"
                                         alt="Plus infos">
                                </a>

                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    <?php } ?>
<?php } ?>



