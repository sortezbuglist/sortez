<!-- MODIF -->
<style type="text/css">
  
 input[type="checkbox"]:checked:after {

        display: block;

    }
    input[type="checkbox"]:hover {

        border: rgb(128, 210, 224) solid 3px;

        background-color: #fff;



    }
    .form-check.p-2{
      padding: 1.5rem!important;
    }


    input[type="checkbox"]:checked {

        background-color: #fff;

    }

    input[type="checkbox"] {

        appearance: none;

        -webkit-appearance: none;

        height: 19px;

        width: 18px;

        background-color: #fff;

        border: #E80EAE solid 3px;

        border-radius: 0px;

        cursor: pointer;

    }



    input[type="checkbox"]:after {

        font-family: "Font Awesome 5 Free";

        font-size: 12px;

        margin-top: -3px;

        font-weight: 900;

        content: "\2713";

        color: #E80EAE;

        display: none;

    }

</style>
<div class="container m-4">
     
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="text-center" style="font-size:25px; line-height:normal; text-align:center;letter-spacing:0em;font-style:italic;font-family:libre baskerville,serif;color:#221133;">Je choisis un pack un ou plusieurs outils adaptés à mon activité
                            <br>
                            <span style="color:#221133;font-size:20px;font-family:libre baskerville,serif;">(L'abonnement annuel hors taxes varie suivant le nombre d'employés</span></h2>
                    </div>
                    <div class="col-md-12 m-4">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 m-auto p-auto">
                                    <div class="form-check p-2">
                                      <label class="form-check-label" style="font-family:libre baskerville,serif;">
                                        <input type="checkbox" name="choix1[pack_infos]" class="form-check-input" style="cursor:pointer"><span class="ml-5" style="font-weight: 300;">01. Pack infos (actualité et agenda) : Quota de 50 articles & 50 événements</span>
                                      </label>
                                    </div>
                                    <div class="form-check p-2">
                                      <label class="form-check-label" style="font-family:libre baskerville,serif;">
                                        <input type="checkbox" name="choix1[boutiques]" class="form-check-input" style="cursor:pointer">
                                        <span class="ml-5" style="font-weight: 300;">02. Les boutiques en ligne : Quota de 50 annonces</span>
                                        
                                      </label>
                                    </div>
                                    <div class="form-check p-2">
                                      <label class="form-check-label" style="font-family:libre baskerville,serif;">
                                        <input type="checkbox" name="choix1[pack_promos]" class="form-check-input" style="cursor:pointer">
                                        <span class="ml-5" style="font-weight: 300;">03. Pack "Promos" : 5 bons plans et une condition de fidélisation</span>
                                      </label>
                                    </div>
                                    <div class="form-check p-2">
                                      <label class="form-check-label" style="font-family:libre baskerville,serif;">
                                        <input type="checkbox" name="choix1[pack_restaurant]" class="form-check-input" style="cursor:pointer">
                                        <span class="ml-5" style="font-weight: 300;">04. Pack Restaurant : Réservations de tables, plats du jour et menu digital</span>
                                      </label>
                                    </div>
                                    <div class="form-check p-2">
                                      <label class="form-check-label" style="font-family:libre baskerville,serif;">
                                        <input type="checkbox" name="choix1[pack_gites]" class="form-check-input" style="cursor:pointer">
                                        <span class="ml-5" style="font-weight: 300;">05. Pack "Gîtes et chambres d'hôtes" : Réservations et boutique en ligne</span>
                                      </label>
                                    </div>
                                    <div class="form-check p-2">
                                      <label class="form-check-label" style="font-family:libre baskerville,serif;">
                                        <input type="checkbox" name="choix1[module_graphiques]" class="form-check-input" style="cursor:pointer">
                                        <span class="ml-5" style="font-weight: 300;">06. Module graphique "Platinium"</span>
                                      </label>
                                    </div>
                                    <div class="form-check p-2">
                                      <label class="form-check-label" style="font-family:libre baskerville,serif;">
                                        <input type="checkbox" name="choix1[module_referencement]" class="form-check-input" style="cursor:pointer">
                                        <span class="ml-5" style="font-weight: 300;">07. Module de référencement "Google : uniquement avec l'abonnement "Platinium"</span>
                                        
                                      </label>
                                    </div>
                                                                                                                                                
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>
                
    
</div>
<div id="comp-l0efuolx" class="_1giiM">

        <div class="_19Et-"></div>

</div>

<div class="container m-4">
     
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="text-center" style="font-size:25px; line-height:normal; text-align:center;letter-spacing:0em;font-style:italic;font-family:libre baskerville,serif;color:#221133;">Les autres services optionnels
                            <br>
                            <span style="color:#221133;font-size:20px;font-family:libre baskerville,serif;">Plus d'informations : nous contacter au 06.72.05.9.35</span></h2>
                    </div>
                    <div class="col-md-12 m-4">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 m-auto p-auto">
                                    <div class="form-check p-2">
                                      <label class="form-check-label" style="font-family:libre baskerville,serif;">
                                        <input type="checkbox" name="choix2[pack_exports]" class="form-check-input" style="cursor:pointer"><span class="ml-5" style="font-weight: 300;">08. Pack export (sur devis) : agenda, actualité, boutique, bons plans, pack restaurant</span>
                                      </label>
                                    </div>
                                    <div class="form-check p-2">
                                      <label class="form-check-label" style="font-family:libre baskerville,serif;">
                                        <input type="checkbox" name="choix2['aide_creation']" class="form-check-input" style="cursor:pointer">
                                        <span class="ml-5" style="font-weight: 300;">09. Aide à la création de vos pages (sur devis)</span>
                                        
                                      </label>
                                    </div>
                                    <div class="form-check p-2">
                                      <label class="form-check-label" style="font-family:libre baskerville,serif;">
                                        <input type="checkbox" name="choix2[creation_gestion]" class="form-check-input" style="cursor:pointer">
                                        <span class="ml-5" style="font-weight: 300;">10. Création et gestion de vos réseaux sociaux</span>
                                      </label>
                                    </div>
                                    <div class="form-check p-2">
                                      <label class="form-check-label" style="font-family:libre baskerville,serif;">
                                        <input type="checkbox" name="choix2[realisation_video]" class="form-check-input" style="cursor:pointer">
                                        <span class="ml-5" style="font-weight: 300;">11. Réalisation d'une vidéo de présentation </span>
                                      </label>
                                    </div>
                                    <div class="form-check p-2">
                                      <label class="form-check-label" style="font-family:libre baskerville,serif;">
                                        <input type="checkbox" name="choix2[pub_magazine]" class="form-check-input" style="cursor:pointer">
                                        <span class="ml-5" style="font-weight: 300;">12. Publicité sur le Magazine Sortez</span>
                                      </label>
                                    </div>
                                    <div class="form-check p-2">
                                      <label class="form-check-label" style="font-family:libre baskerville,serif;">
                                        <input type="checkbox" name="choix2[impression]" class="form-check-input" style="cursor:pointer">
                                        <span class="ml-5" style="font-weight: 300;">13. L'impression sur toutes ses formes ! Sortez département Print</span>
                                      </label>
                                    </div>
                                    <div class="form-check p-2">
                                      <label class="form-check-label" style="font-family:libre baskerville,serif;">
                                        <input type="checkbox" name="choix2[vsv]" class="form-check-input" style="cursor:pointer">
                                        <span class="ml-5" style="font-weight: 300;">14. vivresaville.fr</span>
                                        
                                      </label>
                                    </div>
                                                                                                                                                
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>
</div>


<div id="comp-l0efuolx" class="_1giiM mb-5">

        <div class="_19Et-"></div>

</div>
<div class="container">
    <div class="row m-auto">
        <div class="col-md-12">
            <p style="font-size:17px; text-align:center;"><span style="font-size:17px;"><span style="color:#E80EAE;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;">Attention : vivresaville.fr peut refuser sans en donner la justification l’ouverture d’un compte</span></span></span></p>
            <p style="font-size:17px; text-align:center;"><span style="font-size:17px;"><span style="color:#E80EAE;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;">qui ne correspondrait pas à son éthique. (<span style="text-decoration:underline;"><a href="https://www.sortez.org/mentions-legales.html" target="_blank" rel="noreferrer noopener">Voir nos conditions générales</a></span>).</span></span></span></p>
            <p><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="color:#000000;"><span class="wixGuard">​</span></span></span></p>            
        </div>
    </div>
    
</div>
<div class="container">
    <div class="row m-auto">
        <div class="col-md-11">
<ol class="font_8" style="color:rgb(0, 0, 0); font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif; font-size:17px;">
                            <li>
                            <p class="font_8" style="font-size:17px;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="color:#000000;"><span style="font-size:17px;">En validant votre demande de devis, vous recevez par retour de mail une confirmation de réception ; </span></span></span></p>
                            </li>
                            <li>
                            <p class="font_8" style="font-size:17px;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="color:#000000;"><span style="font-size:17px;">sous 24h00 après étude de votre demande, nous vous adresserons le devis correspondant ;</span></span></span></p>
                            </li>
                            <li>
                            <p class="font_8" style="font-size:17px;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="color:#000000;"><span style="font-size:17px;">puis notre service vous contactera pour vous demander votre avis et vous préparer les documents conformes à votre organisation administrative (commande, facture-proforma, facture...) ;</span></span></span></p>
                            </li>
                            <li>
                            <p class="font_8" style="font-size:17px;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="color:#000000;"><span style="font-size:17px;">les abonnements annuels sont réglés en totalité sur présentation de la facture par virement bancaire ou paiement en ligne par carte bancaire ;</span></span></span></p>
                            </li>
                            <li>
                            <p class="font_8" style="font-size:17px;"><span style="font-size:17px;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="color:#000000;">dès la confirmation, vous accédez à votre compte en cliquant sur le bouton "Mon compte" qui se trouve en haut de notre page d'accueil ;</span></span></span></p>
                            </li>
                            <li>
                            <p class="font_8" style="font-size:17px;"><span style="font-size:17px;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="color:#000000;">vous confirmez votre identifiant et mot de passe et vous accédez </span></span></span><span style="font-size:17px;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="color:#000000;">sur une interface intuitive ;</span></span></span></p>
                            </li>
                            <li>
                            <p class="font_8" style="font-size:17px;"><span style="font-size:17px;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="color:#000000;">vous pouvez alors, intégrer immédiatement vos données (textes, photos, vidéo, liens, PDF, formulaires, bannière…) ;</span></span></span></p>
                            </li>
                            <li>
                            <p class="font_8" style="font-size:17px;"><span style="font-size:17px;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="color:#000000;">à tout moment, vous visualisez et perfectionnez le résultat ;</span></span></span></p>
                            </li>
                            <li>
                            <p class="font_8" style="font-size:17px;"><span style="font-size:17px;"><span style="font-family:futura-lt-w01-book,futura-lt-w05-book,sans-serif;"><span style="color:#000000;">vos données seront alors définitivement intégrées sur l’annuaire complet de sortez.org.</span></span></span></p>
                            </li>
                            </ol>            
        </div>
    </div>
</div>

<!-- FIN MODIF -->


        
        
            <div style="padding-left: 50px;">
<!--                 <p>
                Attention : notre service peut refuser sans en donner la justification l’ouverture d’un compte qui ne correspondrait pas à <br>

                         son éthique. (Voir nos conditions générales) ;
                </p> -->

                <input type="checkbox" id="validationabonnement" name="validationabonnement" > <span>Je confirme ma demande de 
                            devis à un abonnement vivresaville.fr</span> <br>
                <input type="checkbox" id="confirmation" name="confirmation" required> <span>Le soussigné déclare avoir la faculté d’engager en son nom sa structure dont
                les coordonnées sont précisées ci- dessus.</span> <br>
            </div> 
            <br>

            <div class="FieldError" id="divErrorFrmInscriptionProfessionnel" style="height: auto; color: red; font-family: arial; font-size: 12px; text-align:center;"></div>

            <!-- <div style="text-align: center;" id="capt" class="g-recaptcha" data-sitekey="6Lfjgm0UAAAAAOl1qieKqiWV5gZuSjjAc19jUIRg"></div> -->
<!-- <div style="text-align:center; margin-bottom:40px; margin-top:60px;" id="response"> -->
<!-- message ajax -->
<!-- <div class=" alert alert-info" id="msg"></div> -->

</div>
            <div style="text-align:center; margin-bottom:40px; margin-top:60px;">
                <input id="btnSinscrire" style="width: auto;" name="envoyer" value="ADRESSEZ VOTRE DEMANDE DE DEVIS !" type="button" tabindex="26" class="btn btn-success">
            </div>