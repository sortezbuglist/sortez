<?php $data["zTitle"] = 'Annuaire';?>
<?php $data["slide"] = 'wp41514793_05_06.jpg';?>

<?php
$thisss =& get_instance();
$thisss->load->library('session');
$main_width_device = $thisss->session->userdata('main_width_device');
$main_mobile_screen = false;
if (isset($main_width_device) && $main_width_device != "" && $main_width_device != "0" && is_numeric($main_width_device) && floatval($main_width_device)<=991){
    $main_mobile_screen = true;
} else {
    $main_mobile_screen = false;
}
?>

<?php if (isset($_GET['content_only_list']) && $_GET['content_only_list']=='1') {} else { ?>
    <?php $this->load->view("vivresaville/includes/main_header", $data); ?>
    <?php $this->load->view("vivresaville/includes/annuaire_ready", $data); ?>
    <?php $this->load->view("vivresaville/includes/main_menu", $data); ?>
    <?php $this->load->view("vivresaville/includes/main_banner", $data); ?>
    <?php if ($main_mobile_screen==true) $this->load->view("vivresaville/includes/annuaire_banner", $data); ?>
    <?php $this->load->view("vivresaville/includes/main_rubrique_menu", $data); ?>
    <?php //$this->load->view("vivresaville/includes/main_banner_annuaire", $data) ?>
    <div class="container">
        <div id="vsv_main_body_title" class="row main_body_title justify-content-center pt-lg-3 pt-xl-3">
            <!--<h1>MON ANNUAIRE</h1>-->
        </div>
    </div>
    <?php if ($main_mobile_screen==false) $this->load->view("vivresaville/includes/annuaire_banner", $data); ?>
    <?php //$this->load->view("vivresaville/includes/main_banner_pub_link", $data); ?>

    <?php $this->load->view("vivresaville/includes/main_banner_vos_pub", $data); ?>



<?php } ?>

<?php $this->load->view("vivresaville/annuaire_list", $data); ?>

<?php if (isset($_GET['content_only_list']) && $_GET['content_only_list']=='1') {} else { ?>

    <div class="row">
        <?php
        $data['empty'] = null;
        if (strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')) {
            $this->load->view("vivresaville/includes/annuaire_list_listing_ipad.php", $data);
        } else {
            $this->load->view("vivresaville/includes/annuaire_list_listing.php", $data);
        }
        ?>
    </div>

    <?php $this->load->view("vivresaville/includes/main_footer_link", $data); ?>
    <?php $this->load->view("vivresaville/includes/main_footer_copyright", $data); ?>
    <?php $this->load->view("vivresaville/includes/main_footer", $data); ?>
<?php } ?>