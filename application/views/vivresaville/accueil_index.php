<?php $data["zTitle"] = 'Accueil - Vivresaville';?>

<?php $this->load->view("vivresaville/includes/main_header", $data); ?>
<?php $this->load->view("vivresaville/includes/main_menu", $data); ?>
<?php $this->load->view("vivresaville/includes/main_banner", $data); ?>
<?php $this->load->view("vivresaville/includes/main_rubrique_menu", $data); ?>

<?php $this->load->view("vivresaville/accueil_content_top", $data); ?>
<?php $this->load->view("vivresaville/accueil_content_middle", $data); ?>
<?php $this->load->view("vivresaville/accueil_content_img_btn", $data); ?>
<?php $this->load->view("vivresaville/accueil_content_pub", $data); ?>
<?php $this->load->view("vivresaville/accueil_content_bottom", $data); ?>

<?php $this->load->view("vivresaville/includes/main_footer_link", $data); ?>
<?php $this->load->view("vivresaville/includes/main_footer_copyright", $data); ?>
<?php $this->load->view("vivresaville/includes/main_footer", $data); ?>
