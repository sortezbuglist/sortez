<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#loading_agenda_adresses').hide();
        check_Nb_lien();

        jQuery('#PerPage_btn_next').click(function() {
            var PerPage_init = <?php if (isset($PerPage)) echo $PerPage; else echo 0; ?>;
            var PerPage = jQuery('#PerPage_agenda_adresses').val();
            jQuery('#loading_agenda_adresses').show();
            var PerPage = parseInt(PerPage);
            jQuery.ajax({
                url: '<?php echo site_url("agenda/liste/");?>'+String(PerPage)+'/?content_only_list=1',
                dataType: 'html',
                success: function(html) {
                    jQuery('#id_mainbody_main').html(html);
                    jQuery('#loading_agenda_adresses').hide();
                    jQuery('#PerPage_agenda_adresses').val(parseInt(PerPage)+PerPage_init);
                    check_Nb_lien();
                    $('html, body').animate({
                        scrollTop: $(".container.title_article_main").offset().top
                    }, 2000);
                }
            });
        });

        jQuery('#PerPage_btn_preview').click(function() {
            var PerPage_init = <?php if (isset($PerPage)) echo $PerPage; else echo 0; ?>;
            var PerPage = jQuery('#PerPage_agenda_adresses').val();
            jQuery('#loading_agenda_adresses').show();
            var PerPage = parseInt(PerPage)-(PerPage_init*2);
            jQuery.ajax({
                url: '<?php echo site_url("agenda/liste/");?>'+String(PerPage)+'/?content_only_list=1',
                dataType: 'html',
                success: function(html) {
                    jQuery('#id_mainbody_main').html(html);
                    jQuery('#loading_agenda_adresses').hide();
                    jQuery('#PerPage_agenda_adresses').val(PerPage);
                    check_Nb_lien();
                    $('html, body').animate({
                        scrollTop: $(".container.title_article_main").offset().top
                    }, 2000);
                }
            });
        });

    });

    function check_Nb_lien(){
        var PerPage = jQuery('#PerPage_agenda_adresses').val();

        var Nb_lien_init = <?php if (isset($PerPage)) echo $PerPage; else echo 0; ?>;
        var Nb_lien_total = <?php if (isset($TotalRows)) echo $TotalRows; else echo 0; ?>;

        var val_min_prev = parseInt(PerPage)-parseInt(Nb_lien_init);
        var val_max_next = parseInt(PerPage);
        //alert(String(val_min_prev)+' - '+String(Nb_lien_init));

        if (val_min_prev < parseInt(Nb_lien_init)) {
            jQuery('#PerPage_btn_preview').css('display','none');
        } else {
            jQuery('#PerPage_btn_preview').css('display','inline');
        }
        if (val_max_next >= parseInt(Nb_lien_total)) {
            jQuery('#PerPage_btn_next').css('display','none');
        } else {
            jQuery('#PerPage_btn_next').css('display','inline');
        }
    }

</script>

<div class="row" style="padding: 15px 0;">
    <div class="col-sm-4 textalignleft">
        <button id="PerPage_btn_preview" class="btn btn-success" onclick="return false;">< Page précédente</button>
    </div>
    <div class="col-sm-4 textaligncenter">
        <div id="loading_agenda_adresses" class="" style="text-align:center;"><img src="<?php echo GetImagePath("sortez/");?>/loading.gif" /></div>
    </div>
    <div class="col-sm-4 textalignright">
        <button id="PerPage_btn_next" class="btn btn-success" onclick="return false;">Page suivante ></button>
    </div>
</div>


<input id="PerPage_agenda_adresses" type="hidden" value="<?php if (isset($PerPage)) echo $PerPage; else echo "0"; ?>"/>
