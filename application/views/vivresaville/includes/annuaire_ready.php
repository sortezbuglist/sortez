<script type="text/javascript">

   jQuery(function(){

       jQuery("#inputStringVilleHidden_partenaires").change(function(){

            var inputStringVilleHidden_partenaires =jQuery("#inputStringVilleHidden_partenaires").val();

            check_show_categ_partner(inputStringVilleHidden_partenaires);

           jQuery("#span_leftcontener2013_form_partenaires input:checkbox").attr('checked', false);
           jQuery("#frmRecherchePartenaire").submit();
        });

       jQuery("#inputString_zMotCle_submit").click(function(){

            var inputString_zMotCle =jQuery("#inputString_zMotCle").val();
           jQuery("#zMotCle").val(inputString_zMotCle);

           jQuery("#span_leftcontener2013_form_partenaires input:checkbox").attr('checked', false);
           jQuery("#frmRecherchePartenaire").submit();
        });

       jQuery("#inputStringOrderByHidden_partenaires").change(function(){

            var inputStringOrderByHidden_partenaires =jQuery("#inputStringOrderByHidden_partenaires").val();

           jQuery("#inputStringOrderByHidden").val(inputStringOrderByHidden_partenaires);

           jQuery("#frmRecherchePartenaire").submit();
        });


    });

   function annuaire_filter_triage(filter_value=0){
       jQuery("#inputStringOrderByHidden").val(filter_value);
       jQuery("#frmRecherchePartenaire").submit();
   }

    function annuaire_filter_avantage(filter_value=0){
        jQuery("#inputAvantagePartenaire").val(filter_value);
        jQuery("#frmRecherchePartenaire").submit();
    }

    function show_current_categ_subcateg(IdRubrique){
        jQuery(".leftcontener2013content").hide();
        var id_to_show = "#leftcontener2013content_"+IdRubrique;
        //$(id_to_show).show();
        jQuery("#div_subcateg_annuaire_contents").html("");
        //$(id_to_show).appendTo('#div_subcateg_annuaire_contents');
        jQuery("#div_subcateg_annuaire_contents").append($(id_to_show).html());
        jQuery("#div_subcateg_annuaire_contents").show();
        jQuery("#id_subcateg_tab_title").click();
        jQuery("#tab_subcateg").addClass("show");

        jQuery("#span_leftcontener2013_form_partenaires .leftcontener2013title_vs").each(function() {
            $(this).removeClass("leftcontener2013title_vs_active");
        });
        var class_cubcateg_id = ".subcateg_"+IdRubrique;
        jQuery(class_cubcateg_id).addClass("leftcontener2013title_vs_active");
		//window.location.hash = '#span_main_rubrique_banner_title';
		
		var main_width_device = jQuery(document).width();
		if (main_width_device <= 768) {
			jQuery("#span_leftcontener2013_form_partenaires .leftcontener2013content").css("display", 'none');
			var class_subcateg_content = "#span_leftcontener2013_form_partenaires #leftcontener2013content_"+IdRubrique;
			jQuery(class_subcateg_content).css("display", 'table');
			//alert(main_width_device);
		}
		
    }

    function all_subcategories_func(IdRubrique="0"){

        var main_width_device = jQuery(document).width();
        if (main_width_device <= 768) {
            var selector = "#span_leftcontener2013_form_partenaires #leftcontener2013content_"+IdRubrique+"  input[type=checkbox]";
            jQuery(selector).each(function() {
                jQuery(this).prop( "checked", true );
            });
        } else {
            jQuery("#tab_subcateg  input[type=checkbox]").each(function() {
                jQuery(this).prop( "checked", true );
            });
        }

        submit_search_partner();
    }

    function check_show_categ_partner(inputStringDepartementHidden_partenaires, inputStringVilleHidden_partenaires){

        var base_url_visurba = '<?php echo site_url();?>';

       jQuery("#span_leftcontener2013_form_partenaires").html('<div class="col-12 text-center"><img src="'+base_url_visurba+'application/resources/front/images/wait.gif" alt="loading...."/></div>');

        $.post(
            base_url_visurba+'front/annuaire/check_category_list/',
            {
                inputStringDepartementHidden_partenaires: inputStringDepartementHidden_partenaires,
                inputStringVilleHidden_partenaires: inputStringVilleHidden_partenaires
            }
            ,
            function (zReponse)
            {
                //alert ("reponse <br/><br/>" + zReponse) ;
                if (zReponse == "error") { //an other home page exist yet
                    alert("Un probléme est suvenu, veuillez refaire l'opération !");
                } else {
                   jQuery("#span_leftcontener2013_form_partenaires").html(zReponse);


                   jQuery(".leftcontener2013content").hide();

                    $.post(
                        base_url_visurba+'front/annuaire/check_Idcategory_of_subCategory/'
                        ,
                        function (zReponse)
                        {
                            var article_value = zReponse.split("-");
                            for(i = 0; i < article_value.length; i++) {
                                var id_toclick_vsv = "#vsv_categmain_"+article_value[i]+" .leftcontener2013title_vs";
                                jQuery(id_toclick_vsv).addClass("leftcontener2013title_vs_active");
                                jQuery(id_toclick_vsv).click();
                            }
                        }
                    );


                }
            });


    }


    function redirect_home(){
        var base_url_visurba = '<?php echo site_url();?>';

        document.location.href = base_url_visurba+'front/annuaire/redirect_home/';
    }


    function submit_search_partner(){

        //setTimeout(alert('test'),5000);
        var allvalue = '';
		var main_width_device = jQuery(document).width();
		if (main_width_device <= 768) {
			jQuery("#span_leftcontener2013_form_partenaires .leftcontener2013content .subcateb_filter_banner input:checkbox:checked").each(function(){
				allvalue += ','+$(this).val();
			});
			jQuery("#span_leftcontener2013_form_partenaires .leftcontener2013content div label.subcateb_filter_banner input:checkbox:checked").parent( "label.subcateb_filter_banner" ).each(function(){
				jQuery(this).addClass("loading");
			});
		} else {
			jQuery("#tab_subcateg #div_subcateg_annuaire_contents input:checkbox:checked").each(function(){
				allvalue += ','+$(this).val();
			});
		}
       
        //alert(allvalue);
       jQuery("#inputStringHidden").val(allvalue);
       jQuery("#frmRecherchePartenaire").submit();
    }

   function btn_re_init_annuaire_list(){
       $("#inputStringQuandHidden").val("0");
       $("#inputStringDatedebutHidden").val("");
       $("#inputStringDatefinHidden").val("");
       $("#inputStringDepartementHidden_partenaires").val("0");
       $("#inputStringVilleHidden_partenaires").val("0");
       $("#inputStringHidden").val("");
       $("#inputStringHidden_sub").val("");
       $("#inputStringOrderByHidden").val("");
       $("#zMotCle").val("");

       $("#frmRecherchePartenaire").submit();
   }

</script>