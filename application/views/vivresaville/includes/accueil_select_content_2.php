<div class="container accueil_select_content_2 text-center">
    <div class="row">
        <div class="col-12">
            <p class="title_1 p-5 mb-0">
                CES SITES REGROUPENT ET FÉDÈRENT LES INFORMATIONS<br/>
                DES ACTEURS INSTITUTIONNELS ET PRIVÉS DE CHAQUE SECTEUR !…
            </p>
            <p class="pt-0 pb-0">
                <img
                    src="<?php echo base_url(); ?>wpimages_vsv/wp72ee4a0c_06.png"
                    class="mt-0 border-0 img-fluid"/>
            </p>
            <p class="pt-3">
                <img
                    src="<?php echo base_url(); ?>application/resources/front/images/vivresaville_custom/wpa89873e4_06.png"
                    class="mt-0 border-0 img-fluid"/>
            </p>
            <p class="title_3 pb-3">
                Les sites s’adaptent à tous les écrans
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <p class="title_1 pb-3 pt-5 txt_rose_color">
                L’INNOVATION AU SERVICE DE LA VIE LOCALE !…
            </p>
        </div>
        <div class="col-12 p-0">
            <img
                src="<?php echo base_url(); ?>wpimages_vsv/wp1c4febbe_06.png"
                class="border-0 w-100"/>
        </div>
    </div>
    <div class="row">
        <div class="col p-5">
            <p>
                <ul class="text-left accueil_list_ul">
                <li>Vivre<span class="txt_rose_color">sa</span>ville.fr est un concept de communication mutualisé, innovant et complet. Il est basé sur la création de sites web locaux, personnalisés et gratuits élaborés en partenariat avec les collectivités locales ou territoriales.</li>
                <li>Chaque site Vivre<span class="txt_rose_color">sa</span>ville.fr ouvre gratuitement le référencement des acteurs locaux (collectivités, associations, profes-sionnels) de son secteur.</li>
                <li>Sur un compte personnel et sécurisé, les acteurs locaux déposent et diffusent leurs données en temps réel et d’un simple clic.</li>
                <li>Vivre<span class="txt_rose_color">sa</span>ville.fr favorise leur accès à des outils marketing, clés en mains, gratuits ou optionnels, personnels ou groupés dont certains sont habituellement utilisés par les grandes enseignes.</li>
                <li>Vivre<span class="txt_rose_color">sa</span>ville.fr leur ouvre gratuitement la porte du web et de la mobilité (site web, QRCODE, e.boutique…)</li>
                <li>Vivre<span class="txt_rose_color">sa</span>ville.fr développe le sentiment d’appartenance à une commune, à un territoire</li>
                <li>Les bases de données de Vivre<span class="txt_rose_color">sa</span>ville.fr sont liées à notre site principal sortez.org qui centralise et référence la tota-lité des acteurs sur ses annuaires départementaux.</li>
            </ul>
        </div>
    </div>
    <div class="row" >
        <div class="col-12">
            <p class="title_1 pb-3 pt-5 txt_rose_color">
                DÉCOUVREZ LE DÉTAIL DU FONCTIONNEMENT<br/>
                POUR CHAQUE CATÉGORIE D’ACTEURS...
            </p>
        </div>
        <div class="col-sm-6 p-4">
            <img
                src="<?php echo base_url(); ?>wpimages_vsv/wp816a51eb_06.png"
                class="border-0 w-100"/>
        </div>
        <div class="col-sm-6 mt-5">
            <p class="title_rose_select pb-3">
                VOUS ÊTES<br/>
                UNE COMMUNE ?
            </p>
            <p class="pb-3">
                BÉNÉFICIEZ D’UN SITE GRATUIT<br/>
                FÉDÉRATEUR COMMUNAL...
            </p>
            <p>
                <a href="<?php echo site_url('site-vivresaville/communes.html');?>" class="vsv_accueil_btn vsv_select_link plus_infos_link">PLUS D'INFORMATIONS...</a>
            </p>
        </div>
    </div>
    <div class="row pt-3 pb-3" style="background-color: #ffffff;">
        <div class="col-sm-6 mt-5">
            <p class="title_rose_select pb-3">
                VOUS ÊTES<br/>
                UNE COMMUNAUTÉ DE COMMUNES,<br/>
                UN SIVOM, UNE INTERCOMMUNALITÉ,<br/>
                UNE MÉTROPOLE ?
            </p>
            <p class="pb-3">
                BÉNÉFICIEZ D’OUTILS FÉDÉRATEURS…
            </p>
            <p>
                <a href="<?php echo site_url('site-vivresaville/intercommunalites.html');?>" class="vsv_accueil_btn vsv_select_link plus_infos_link">PLUS D'INFORMATIONS...</a>
            </p>
        </div>
        <div class="col-sm-6 p-4">
            <img
                src="<?php echo base_url(); ?>wpimages_vsv/wpb398a620_06.png"
                class="border-0 w-100"/>
        </div>
    </div>
    <div class="row" >
        <div class="col-sm-6 p-4">
            <img
                src="<?php echo base_url(); ?>application/resources/front/images/vivresaville_custom/wp2e89b220_05_06.jpg"
                class="border-0 w-100"/>
        </div>
        <div class="col-sm-6 mt-5">
            <p class="title_rose_select pb-3">
                VOUS ÊTES<br/>
                UN COMMERÇANT,<br/>
                UN ARTISAN, UNE PME ?
            </p>
            <p class="pb-3">
                RÉFÉRENCEZ GRATUITEMENT<br/>
                VOTRE ÉTABLISSEMENT…
            </p>
            <p>
                <a href="<?php echo site_url('site-vivresaville/professionnels.html');?>" class="vsv_accueil_btn vsv_select_link plus_infos_link">PLUS D'INFORMATIONS...</a>
            </p>
        </div>
    </div>
    <div class="row pt-3 pb-3" style="background-color: #ffffff;">
        <div class="col-sm-6 mt-5">
            <p class="title_rose_select pb-3">
                VOUS ÊTES<br/>
                UN PARTICULIER ?
            </p>
            <p class="pb-3">
                DEMANDEZ GRATUITEMENT<br/>
                LA CARTE PRIVILÈGE SORTEZ<br/>
                ET BÉNÉFICIEZ DE MULTIPLES AVANTAGES…
            </p>
            <p>
                <a href="<?php echo site_url('site-vivresaville/particuliers.html');?>" class="vsv_accueil_btn vsv_select_link plus_infos_link">PLUS D'INFORMATIONS...</a>
            </p>
        </div>
        <div class="col-sm-6 p-4">
            <img
                src="<?php echo base_url(); ?>wpimages_vsv/wp54ef9faf_06.png"
                class="border-0 w-100"/>
        </div>
    </div>
    <div class="row" >
        <div class="col-12 pt-5">
            <img
                src="<?php echo base_url(); ?>wpimages_vsv/wp068e3559_06.png"
                class="border-0 text-center img-fluid"/>
        </div>
        <div class="col-12 pl-4 pr-4 pt-3 pb-3">
            <p class="text-center">
                Vivresaville.fr est une réalisation du magazine Sortez !<br/>
                Les données de la totalité des acteurs sont également référencées en temps réel<br/>
                sur le site départemental du magazine Sortez.
            </p>
        </div>
        <div class="col-12 pl-4 pr-4 pt-3 pb-3">
            <p class="title_rose_select pb-3 text-center">
                DÉCOUVREZ LE 1ER MAGAZINE MENSUEL ET GRATUIT<br/>
                DIFFUSÉ SUR LES ALPES-MARITIMES & MONACO
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col p-0">
            <img
                src="<?php echo base_url(); ?>application/resources/front/images/vivresaville_custom/wp9d128cdd_06.png"
                class="border-0 img-fluid"/>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 p-5">
            <p>
                <a href="https://spark.adobe.com/page/WcFEg1TTyA4T7/" target="_blank" class="vsv_accueil_btn vsv_select_link ">PLUS D'INFOS SUR LE MAGAZINE...</a>
            </p>
        </div>
        <div class="col-sm-6 p-5">
            <p>
                <a href="<?php echo base_url();?>" target="_blank" class="vsv_accueil_btn vsv_select_link ">ACCES A NOTRE SITE WEB...</a>
            </p>
        </div>
    </div>
</div>