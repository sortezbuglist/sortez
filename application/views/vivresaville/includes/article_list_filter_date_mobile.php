<div class="container main_nav_bar_filter_menu main_filter_menu d-xl-none">
    <div class="row">
        <nav class="container navbar navbar-toggleable-md navbar-inverse bg-inverse mb-0 w-100 p-0">
            <button class="navbar-toggler navbar-toggler-right collapsed" type="button" data-toggle="collapse"
                    data-target="#navbarsFilterDefault" aria-controls="navbarsFilterDefault" aria-expanded="false"
                    id="navbar_button_filter_link"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><img alt="" src="<?php echo base_url(); ?>application/resources/front/images/vivresaville_custom/burgerIcon.png" class="img-fluid"></span>
            </button>
            <a class="navbar-brand" id="navbar_brand_filter_link" href="javascript:void(0);">Filtrer</a>

            <div class="navbar-collapse collapse" id="navbarsFilterDefault" aria-expanded="false" style="">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active pl-3 pr-3 <?php if (isset($inputStringOrderByHidden) && $inputStringOrderByHidden == '101') { ?> main_menu_active <?php } ?>">
                        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annuaire_filter_avantage(101);">
                            <input type="radio" class="form-check-input" name="inputStringQuandHidden_check"
                                   id="optionsRadios_0" value="0"
                                   <?php if (isset($inputStringOrderByHidden) && $inputStringOrderByHidden == '101') { ?>checked<?php } ?>>
                            Aujourd'hui
                        </label>
                    </li>
                    <li class="nav-item  pl-3 pr-3 <?php if (isset($inputStringOrderByHidden) && $inputStringOrderByHidden == '303') { ?> main_menu_active <?php } ?>">
                        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annuaire_filter_avantage(303);">
                            <input type="radio" class="form-check-input" name="inputStringQuandHidden_check"
                                   id="optionsRadios_1" value="1"
                                   <?php if (isset($inputStringOrderByHidden) && $inputStringOrderByHidden == '303') { ?>checked<?php } ?>>
                            Cette semaine
                        </label>
                    </li>
                    <li class="nav-item  pl-3 pr-3 <?php if (isset($inputStringOrderByHidden) && $inputStringOrderByHidden == '202') { ?> main_menu_active <?php } ?>">
                        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annuaire_filter_avantage(202);">
                            <input type="radio" class="form-check-input" name="inputStringQuandHidden_check"
                                   id="optionsRadios_2" value="2"
                                   <?php if (isset($inputStringOrderByHidden) && $inputStringOrderByHidden == '202') { ?>checked<?php } ?>>
                            Ce Week-end
                        </label>
                    </li>
                    <li class="nav-item  pl-3 pr-3 <?php if (isset($inputStringOrderByHidden) && $inputStringOrderByHidden == '404') { ?> main_menu_active <?php } ?>">
                        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annuaire_filter_avantage(404);">
                            <input type="radio" class="form-check-input" name="inputStringQuandHidden_check"
                                   id="optionsRadios_3" value="3"
                                   <?php if (isset($inputStringOrderByHidden) && $inputStringOrderByHidden == '404') { ?>checked<?php } ?>>
                            Semaine prochaine
                        </label>
                    </li>
                    <li class="nav-item  pl-3 pr-3 <?php if ((isset($inputStringDatedebutHidden) && $inputStringDatedebutHidden != '' && $inputStringDatedebutHidden != "0000-00-00") ||
                        (isset($inputStringDatefinHidden) && $inputStringDatefinHidden != '' && $inputStringDatefinHidden != "0000-00-00")) { ?> main_menu_active <?php } ?>">
                        <div class="row">
                            <div class="col-12 p-2">
                                <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:article_filter_date_choice();">
                                    <input type="radio" class="form-check-input" name="inputStringQuandHidden_check"
                                           id="input_check_filter_datechoice" value="4"
                                           <?php if ((isset($inputStringDatedebutHidden) && $inputStringDatedebutHidden != '' && $inputStringDatedebutHidden != "0000-00-00") ||
                                           (isset($inputStringDatefinHidden) && $inputStringDatefinHidden != '' && $inputStringDatefinHidden != "0000-00-00")) { ?>checked<?php } ?>>
                                    Filtrer par date
                                </label>
                            </div>
                            <div class="col p-2">
                                <input type="text" id="inputStringDatedebutHidden_check" name="inputStringDatedebutHidden_check" class="form-control tab_filter_date_input" placeholder="Date début (jj/mm/aaaa)" disabled>
                            </div>
                            <div class="col p-2">
                                <input type="text" id="inputStringDatefinHidden_check" name="inputStringDatefinHidden_check" class="form-control tab_filter_date_input" placeholder="Date fin (jj/mm/aaaa)" disabled>
                            </div>
                            <div class="col p-2">
                                <a href="javascript:void(0);" id="input_submit_filter_date_choice" type="button" class="btn btn-secondary w-100 tab_filter_date_input" onclick="javascript:article_filter_date_submit();" disabled>Valider</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>
