<script type="text/javascript">


    $(function(){

        $("#inputStringVilleHidden_annonces").change(function(){

            var inputStringVilleHidden_annonces = $("#inputStringVilleHidden_annonces").val();

            check_show_categ_annonce(inputStringVilleHidden_annonces);
            //alert(inputStringVilleHidden_annonces);

            $("#span_leftcontener2013_form_annonces input:checkbox").attr('checked', false);
            $("#frmRechercheAnnonce").submit();

        });

        $("#inputString_zMotCle_submit").click(function(){

            var inputString_zMotCle = $("#inputString_zMotCle").val();
            $("#zMotCle").val(inputString_zMotCle);

            $("#span_leftcontener2013_form_annonces input:checkbox").attr('checked', false);
            $("#frmRechercheAnnonce").submit();
        });

        $("#inputStringOrderByHidden_partenaires").change(function(){

            var inputStringVilleHidden_annonces = $("#inputStringVilleHidden_annonces").val();
            var inputStringOrderByHidden_partenaires = $("#inputStringOrderByHidden_partenaires").val();

            $("#inputStringOrderByHidden").val(inputStringOrderByHidden_partenaires);

            //check_show_categ_annonce(inputStringVilleHidden_annonces);
            //alert(inputStringVilleHidden_annonces);

            //$("#span_leftcontener2013_form_annonces input:checkbox").attr('checked', false);
            $("#frmRechercheAnnonce").submit();
        });

        $("#inputStringEtatByHidden").change(function(){

            var inputStringEtatByHidden = $("#inputStringEtatByHidden").val();
            var inputStringOrderByHidden_partenaires = $("#inputStringOrderByHidden_partenaires").val();
            var inputString_zMotCle = $("#inputString_zMotCle").val();
            if (inputString_zMotCle == "RECHERCHER") inputString_zMotCle = "";

            $("#iEtat").val(inputStringEtatByHidden);
            $("#inputStringOrderByHidden").val(inputStringOrderByHidden_partenaires);
            $("#zMotCle").val(inputString_zMotCle);

            //$("#span_leftcontener2013_form_annonces input:checkbox").attr('checked', false);
            $("#frmRechercheAnnonce").submit();
        });

    });


    function show_current_categ_subcateg(IdRubrique){
        jQuery(".leftcontener2013content").hide();
        var id_to_show = "#leftcontener2013content_"+IdRubrique;
        //$(id_to_show).show();
        jQuery("#div_subcateg_annuaire_contents").html("");
        //$(id_to_show).appendTo('#div_subcateg_annuaire_contents');
        jQuery("#div_subcateg_annuaire_contents").append($(id_to_show).html());
        jQuery("#div_subcateg_annuaire_contents").show();
        jQuery("#id_subcateg_tab_title").click();
        jQuery("#tab_subcateg").addClass("show");

        jQuery("#span_leftcontener2013_form_annonces .leftcontener2013title_vs").each(function() {
            $(this).removeClass("leftcontener2013title_vs_active");
        });
        var class_cubcateg_id = ".subcateg_"+IdRubrique;
        jQuery(class_cubcateg_id).addClass("leftcontener2013title_vs_active");
		//window.location.hash = '#span_main_rubrique_banner_title';

        var main_width_device = jQuery(document).width();
        if (main_width_device <= 768) {
            jQuery("#span_leftcontener2013_form_annonces .leftcontener2013content").css("display", 'none');
            var class_subcateg_content = "#span_leftcontener2013_form_annonces #leftcontener2013content_"+IdRubrique;
            jQuery(class_subcateg_content).css("display", 'table');
            //alert(main_width_device);
        }
    }

    function all_subcategories_func(IdRubrique="0"){

        var main_width_device = jQuery(document).width();
        if (main_width_device <= 768) {
            var selector = "#span_leftcontener2013_form_annonces #leftcontener2013content_"+IdRubrique+"  input[type=checkbox]";
            jQuery(selector).each(function() {
                jQuery(this).prop( "checked", true );
            });
        } else {
            jQuery("#tab_subcateg  input[type=checkbox]").each(function() {
                jQuery(this).prop( "checked", true );
            });
        }

        submit_search_annonce();
    }


    function check_show_categ_annonce(inputStringDepartementHidden_annnonces, inputStringVilleHidden_annonces){

        var base_url_visurba = '<?php echo site_url();?>';

        $("#span_leftcontener2013_form_annonces").html('<div class="col-12 text-center"><img src="'+base_url_visurba+'application/resources/front/images/wait.gif" alt="loading...."/></div>');

        $.post(
            base_url_visurba+'front/annonce/check_category_list/',
            {
                inputStringDepartementHidden_annnonces: inputStringDepartementHidden_annnonces,
                inputStringVilleHidden_annonces: inputStringVilleHidden_annonces
            }
            ,
            function (zReponse)
            {
                //alert ("reponse <br/><br/>" + zReponse) ;
                if (zReponse == "error") { //an other home page exist yet
                    alert("Un probléme est suvenu, veuillez refaire l'opération !");
                } else {
                    $("#span_leftcontener2013_form_annonces").html(zReponse);

                    $(".leftcontener2013content").hide();

                    $.post(
                        base_url_visurba+'accueil/check_Idcategory_of_subCategory/'
                        ,
                        function (zReponse)
                        {
                            var article_value = zReponse.split("-");
                            for(i = 0; i < article_value.length; i++) {
                                var id_toclick_vsv = "#vsv_categmain_"+article_value[i]+" .leftcontener2013title_vs";
                                jQuery(id_toclick_vsv).addClass("leftcontener2013title_vs_active");
                                jQuery(id_toclick_vsv).click();
                            }
                        }
                    );



                }
            });
    }


    function redirect_annonce(){
        var base_url_visurba = '<?php echo site_url();?>';

        document.location.href = base_url_visurba+'front/annonce/redirect_annonce/';
    }


    function submit_search_annonce(){

        //setTimeout(alert('test'),5000);
        var allvalue = '';
        var main_width_device = jQuery(document).width();
        if (main_width_device <= 768) {
            $("#span_leftcontener2013_form_annonces .leftcontener2013content label input:checkbox:checked").each(function(){
                allvalue += ','+$(this).val();
            });
			jQuery("#span_leftcontener2013_form_annonces .leftcontener2013content div label.subcateb_filter_banner input:checkbox:checked").parent( "label.subcateb_filter_banner" ).each(function(){
				jQuery(this).addClass("loading");
			});
        } else {
            $("#tab_subcateg #div_subcateg_annuaire_contents input:checkbox:checked").each(function(){
                allvalue += ','+$(this).val();
            });
        }
        //alert(allvalue);
        $("#inputStringHidden").val(allvalue);
        $("#frmRechercheAnnonce").submit();
    }

    function btn_re_init_annonce_list(){
        $("#inputStringQuandHidden").val("0");
        $("#inputStringDatedebutHidden").val("");
        $("#inputStringDatefinHidden").val("");
        $("#inputStringDepartementHidden_partenaires").val("0");
        $("#inputStringVilleHidden_partenaires").val("0");
        $("#inputStringHidden").val("");
        $("#inputStringHidden_sub").val("");
        $("#inputStringOrderByHidden").val("");
        $("#zMotCle").val("");

        $("#frmRechercheAnnonce").submit();
    }

</script>