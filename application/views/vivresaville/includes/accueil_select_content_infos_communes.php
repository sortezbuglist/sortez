<div class="container accueil_select_content_2 text-center pt-3">
    <div class="row">
        <div class="col-12">
            <p>
                <a href="<?php echo site_url('vivresaville/ListVille'); ?>"
                   class="vsv_accueil_btn vsv_select_link plus_infos_link">RETOUR PAGE D'ACCUEIL...</a>
            </p>
            <p class="title_1 pt-5 pl-5 pr-5">
                COMMUNES !… <br/>NOUS VOUS PROPOSONS DE CRÉER<br/>
                GRATUITEMENT UN SITE WEB PERSONNALISÉ, FÉDÉRATEUR OÙ SE RETROUVERONT LES INFORMATIONS
                INSTITUTIONNELLES<br/>
                ET PRIVÉES DE VOTRE VILLE !... !...
            </p>
            <p class="txt_rose_color strong_txt pt-5 pl-5 pr-5">
                Un annuaire complet - L’actualité - Un agenda événementiel<br/>
                Des bonnes affaires - Une carte de fidélité - Des boutiques en ligne
            </p>
            <p class="pt-3 pl-5 pr-5">
                Les données sont déposées par les acteurs administratifs, associatifs et économiques<br/>
                de la cité sur un compte personnel et sécurisé.
            </p>
            <p class="">
                <img
                    src="<?php echo base_url(); ?>application/resources/front/images/vivresaville_custom/wpa89873e4_06.png"
                    class="mt-0 border-0 img-fluid"/>
            </p>
            <p class="title_3 pb-3">
                Les sites s’adaptent à tous les écrans
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-12 p-0">
            <img
                src="<?php echo base_url(); ?>application/resources/front/images/vivresaville_custom/wp5dddceb6_05_06.jpg"
                class="border-0 w-100"/>
        </div>
    </div>
    <div class="row">
        <div class="col p-5">
            <p class="title_1 title_partenaire pl-5 pr-5">
                EN DEVENANT PARTENAIRE, LA COMMUNE BÉNÉFICIE DE LA PATERNITÉ<br/>
                D’UNE DYNAMIQUE LOCALE FAVORISANT<br/>
                LES ÉCHANGES INSTITUTIONNELS ET PRIVÉS...
            </p>
            <p class="title_1 txt_rose_color title_partenaire pl-5 pr-5 pt-5 pb-3">
                LES AVANTAGES DE LA COMMUNE
            </p>
            <p>
                <ul class="accueil_list_ul">
                    <li>L’URL du site est personnalisé au nom de la ville ex : www.maville.vivresaville.fr</li>
                    <li>Il offre un nouveau réseau communal mutualiste.</li>
                    <li>Il regroupe sur un annuaire local l’ensemble des acteurs de la cité et les structures communales et patrimoniales</li>
                    <li>Il favorise la diffusion de l’actualité locale (articles et événements)</li>
                    <li>Il donne l’accès aux commerces de proximité  à des outils marketing individuels et mutualisés (bons plans, conditions de fidélisation, carte de fidélité)</li>
                    <li>Il met le e-commerce à la portée de tous (module boutique en ligne avec le règlement par Paypal)</li>
                    <li>Il ouvre la porte du web et la mobilité aux associations et professionnels.</li>
                    <li>Il stimule l’intérêt local des administrés pour la vie de la cité.</li>
                    <li>Chaque site est multilingue.</li>
                    <li>Le site bénéficie d’une visibilité sur les tablettes et mobiles</li>
                    <li>Les données des communes et des acteurs de votre cité sont également référencés sur notre site départemental sortez.org.</li>
                </ul>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <p class="title_1 txt_rose_color title_partenaire pl-5 pr-5">
                PLUS D’INFORMATIONS, VISUALISEZ NOS PRÉSENTATIONS !…
            </p>
        </div>
    </div>
    <div class="row accueil_visu_presentation pb-5">
        <div class="col-sm-6 p-3">
            <div class="m-3 p-5 visu_presentation_left">Disposez gratuitement
            sur votre ville d'un site
            fédérateur !</div>
            <div>
                <a href="https://spark.adobe.com/page/0dLbXBYuhYCXy/" target="_blank" class="vsv_accueil_btn vsv_select_link ">VISUALISEZ...</a>
            </div>
        </div>
        <div class="col-sm-6 p-3">
            <div class="m-3 p-5 visu_presentation_right">Développez et maîtrisez
            la diffusion de votre actualité !</div>
            <div>
                <a href="https://spark.adobe.com/page/RGN2AX7EqvPb8/" target="_blank" class="vsv_accueil_btn vsv_select_link ">VISUALISEZ...</a>
            </div>
        </div>
    </div>
</div>