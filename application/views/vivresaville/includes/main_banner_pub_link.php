<?php
$thiss =& get_instance();
$thiss->load->library('session');
$localdata_IdVille = $thiss->session->userdata('localdata_IdVille');
$thiss->load->model("vivresaville_villes");
if (!isset($localdata_IdVille) || $localdata_IdVille==false) {} else {
    $vsv_id_vivresaville = $thiss->vivresaville_villes->getByIdVille($localdata_IdVille);
    if (isset($vsv_id_vivresaville) && isset($vsv_id_vivresaville->id)) $vsv_object = $thiss->vivresaville_villes->getById($vsv_id_vivresaville->id);
}
?>

<div class="container annuaire_banner_pub_link">
    <div class="row">
        <div class="col-md-4 padding0">
            <a href="<?php if (isset($vsv_object->link_actu_mairie)) echo $vsv_object->link_actu_mairie; else { ?>javascript:void(0);<?php } ?>">
                <img src="<?php echo base_url();?>assets/ville-test/wpimages/annuaire_banner_pub_link1.png" width="410" height="119" alt="" class="img-fluid">
            </a>
        </div>
        <div class="col-md-4 padding0">
            <a href="<?php if (isset($vsv_object->link_home)) echo $vsv_object->link_home; else { ?>javascript:void(0);<?php } ?>">
                <img src="<?php echo base_url();?>assets/ville-test/wpimages/annuaire_banner_pub_link2.png" width="415" height="119" alt="" class="img-fluid">
            </a>
        </div>
        <div class="col-md-4 padding0">
            <a href="<?php if (isset($vsv_object->link_agenda_mairie)) echo $vsv_object->link_agenda_mairie; else { ?>javascript:void(0);<?php } ?>">
                <img src="<?php echo base_url();?>assets/ville-test/wpimages/annuaire_banner_pub_link3.png" width="425" height="119" alt="" class="img-fluid">
            </a>
        </div>
    </div>
</div>