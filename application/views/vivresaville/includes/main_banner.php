<?php
$thiss =& get_instance();
$thiss->load->library('session');
$thiss->load->model('vsv_ville_other');
$localdata_IdVille = $thiss->session->userdata('localdata_IdVille');
$localdata_IdVille_parent = $thiss->session->userdata('localdata_IdVille_parent');
$localdata_IdVille_all = $thiss->session->userdata('localdata_IdVille_all');
$thiss->load->model("vivresaville_villes");
$logo_to_show_vsv = base_url() . "assets/ville-test/wpimages/logo_ville_test.png";
if (isset($localdata_IdVille_parent) && $localdata_IdVille_parent != "" && $localdata_IdVille_parent != false) {
    $vsv_id_vivresaville = $thiss->vivresaville_villes->getByIdVille($localdata_IdVille_parent);
    if (isset($vsv_id_vivresaville) && isset($vsv_id_vivresaville->id)) {
        $vsv_object = $thiss->vivresaville_villes->getById($vsv_id_vivresaville->id);
        $vsv_other_ville = $thiss->vsv_ville_other->getByIdVsv($vsv_id_vivresaville->id);
    }
    if (isset($vsv_object) && isset($vsv_object->logo) && file_exists("application/resources/front/images/vivresaville/" . $vsv_object->logo)) {
        $logo_to_show_vsv = base_url() . "application/resources/front/images/vivresaville/" . $vsv_object->logo;
    }
}
?>

<div class="container main_banner_container align-items-center d-none d-sm-block d-md-block d-lg-block d-xl-block">
    <div class="row">
        <div class="col-md-3 logo_ville" style="/*background-image: url('<?php echo base_url();?>/site2/wpimages/wpb5bf54e2_06.png');*/ background-repeat: no-repeat; background-position: left center; background-size: 150% auto;">
            <table>
                <tr>
                    <td>
                        <img src="<?php echo $logo_to_show_vsv; ?>"
                             alt="logo" id="vsv_img_main_logo" class="img-fluid"/>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-md-6 text-center ">
            <table>
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-12 vsv_main_ville_name_txt logo_vivresaville">
                                <?php if (isset($vsv_object->name_ville)) echo $vsv_object->name_ville; ?>
                            </div>
                            <div class="col-12 p-0 text-center">
                                <?php if (isset($vsv_other_ville) && count($vsv_other_ville) > 0) { ?>
                                    <form class="vsv_other_ville_commune_form" id="vsv_other_ville_commune_form" name="vsv_other_ville_commune_form" method="post" action="<?php  echo site_url("vivresaville/accueil");?>">
                                        <select id="vsv_other_ville_select" name="vsv_other_ville_select"
                                                class="mb-3 p-0 form-control text-center vsv_other_ville_select">
                                            <option value="0" <?php if (isset($localdata_IdVille_all) && $localdata_IdVille_all!="") echo "selected";?>>Toutes les communes</option>
                                            <?php foreach ($vsv_other_ville as $vsv_item) { ?>
                                                <option value="<?php echo $vsv_item->id_ville; ?>" <?php if (isset($localdata_IdVille) && $localdata_IdVille==$vsv_item->id_ville && (!isset($localdata_IdVille_all) || $localdata_IdVille_all==false)) echo "selected";?>><?php echo $vsv_item->name; ?></option>
                                            <?php } ?>
                                        </select>
                                        <input type="hidden" id="vsv_other_ville_currenturl" name="vsv_other_ville_currenturl" value="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>"/>
                                    </form>
                                    <script>$(document).ready(function(){$('#vsv_other_ville_select').change(function(){ vsv_other_ville_commune_form.submit();});});</script>
                                <?php } ?>
                            </div>
                            <div class="col-12 p-0 text-center vsv_referencez_site_ville">
                                <?php
                                $ville_reference_vsv = false;
                                $thisss =& get_instance();
                                $thisss->load->model("vsv_ville_reference");
                                $thisss->load->library('session');
                                $localdata_IdVille = $thisss->session->userdata('localdata_IdVille');
                                $ip_adress = $thisss->input->ip_address();
                                $objVilleReference = $thisss->vsv_ville_reference->getWhere(' ip="'.$ip_adress.'" and id_ville='.$localdata_IdVille);
                                if (isset($objVilleReference) && count($objVilleReference)>0) {
                                    $ville_reference_vsv = true;
                                }
                                ?>
                                <form id="form_id_ville_reference_vsv" class="form_id_ville_reference_vsv" method="post" name="form_id_ville_reference_vsv" action="<?php echo site_url("vivresaville/accueil");?>">
                                    <input type="checkbox" id="id_ville_reference_vsv" name="id_ville_reference_vsv" class="id_ville_reference_vsv" <?php if($ville_reference_vsv) echo 'checked';?> onclick="fn_id_ville_reference_vsv();"/>
                                    <input type="hidden" name="check_id_ville_reference_vsv" id="check_id_ville_reference_vsv" value="<?php if($ville_reference_vsv) echo '1'; else echo '2';?>">
                                    <label>Référencez ce site</label>
                                    <span><a href="#" data-toggle="popover"
                                             data-content="En référençant ce site, ce dernier apparaîtra directement sur votre ordinateur, tablette ou mobile simplement en tapant vivresaville.fr">?</a></span>
                                </form>
                                <script type="application/javascript">
                                    function fn_id_ville_reference_vsv() {
                                        if($('input[name="id_ville_reference_vsv"]').is(':checked')) {
                                            $("#check_id_ville_reference_vsv").val(1);
                                        } else {
                                            $("#check_id_ville_reference_vsv").val(2);
                                        }
                                        $("#form_id_ville_reference_vsv").submit();
                                    }
                                </script>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-md-3 text-center logo_ville d-sm-block d-md-none d-xl-none">
            <img src="<?php echo $logo_to_show_vsv; ?>"
                 alt="logo" id="vsv_img_main_logo" class="img-fluid"/>
        </div>
        <div class="col-md-3 text-center my-auto logo_carte">
            <table>
                <tr>
                    <td>
                        <img
                                src="<?php echo base_url(); ?>assets/ville-test/wpimages/logo_carte_privilege.png"
                                class="img-fluid"
                                alt="carte privilege"/>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>