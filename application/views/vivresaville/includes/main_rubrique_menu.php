<div class="container main_rubrique_menu d-none d-xl-block">
    <div class="row text-center h-100" id="ul_main_menu_rubrique">
        <a href="<?php echo site_url("vivresaville/accueil"); ?>"
           class="col <?php if (isset($main_menu_content) && $main_menu_content == "home") { ?> main_menu_active <?php } ?>"
           onmouseover="hide_preview_background_main_menu_rub(0);"
           onmouseout="hide_preview_background_main_menu_rub_over(0)">Accueil</a>
        <a href="<?php echo site_url("front/annuaire"); ?>"
           class="col <?php if (isset($main_menu_content) && $main_menu_content == "annuaire") { ?> main_menu_active <?php } ?>"
           onmouseover="hide_preview_background_main_menu_rub(1);"
           onmouseout="hide_preview_background_main_menu_rub_over(1)">L'annuaire</a>
        <a href="<?php echo site_url("article"); ?>"
           class="col <?php if (isset($main_menu_content) && $main_menu_content == "article") { ?> main_menu_active <?php } ?>"
           onmouseover="hide_preview_background_main_menu_rub(2);"
           onmouseout="hide_preview_background_main_menu_rub_over(2)">L'actualité</a>
        <a href="<?php echo site_url("agenda"); ?>"
           class="col <?php if (isset($main_menu_content) && $main_menu_content == "agenda") { ?> main_menu_active <?php } ?>"
           onmouseover="hide_preview_background_main_menu_rub(3);"
           onmouseout="hide_preview_background_main_menu_rub_over(3)">L'agenda</a>
        <a href="<?php echo site_url("front/bonplan"); ?>"
           class="col <?php if (isset($main_menu_content) && $main_menu_content == "bonplan") { ?> main_menu_active <?php } ?>"
           onmouseover="hide_preview_background_main_menu_rub(4);"
           onmouseout="hide_preview_background_main_menu_rub_over(4)">Les bons plans</a>
        <a href="<?php echo site_url("front/fidelity"); ?>"
           class="col <?php if (isset($main_menu_content) && $main_menu_content == "fidelite") { ?> main_menu_active <?php } ?>"
           onmouseover="hide_preview_background_main_menu_rub(5);"
           onmouseout="hide_preview_background_main_menu_rub_over(5)">La fidélité</a>
        <a href="<?php echo site_url("front/annonce"); ?>"
           class="col <?php if (isset($main_menu_content) && $main_menu_content == "boutique") { ?> main_menu_active <?php } ?>"
           onmouseover="hide_preview_background_main_menu_rub(6);"
           onmouseout="hide_preview_background_main_menu_rub_over(6)">Les boutiques</a>
    </div>
</div>

<script type="application/javascript">
    $(document).ready(function () {
        $("#ul_main_menu_rubrique a").css("background-image", "url(<?php echo base_url();?>assets/ville-test/wpimages/bg_main_rubrique_menu.png)");
        $("#ul_main_menu_rubrique a:last").css("background-image", "none");
        $("#ul_main_menu_rubrique a.main_menu_active").css("background-image", "none");
    });

    function hide_preview_background_main_menu_rub(nb = 1) {
        var selector = "#ul_main_menu_rubrique a:nth-child(" + nb + ")";
        var num = parseInt(nb) + 1;
        var selector_this = "#ul_main_menu_rubrique a:nth-child(" + num + ")";
        $(selector).css("background-image", "none");
        $(selector_this).css("background-image", "none");
        $("#ul_main_menu_rubrique a:last").css("background-image", "none");
        $("#ul_main_menu_rubrique a.main_menu_active").css("background-image", "none");
    }

    function hide_preview_background_main_menu_rub_over(nb = 1) {
        var selector = "#ul_main_menu_rubrique a:nth-child(" + nb + ")";
        var num = parseInt(nb) + 1;
        var selector_this = "#ul_main_menu_rubrique a:nth-child(" + num + ")";
        $(selector).css("background-image", "url(<?php echo base_url();?>assets/ville-test/wpimages/bg_main_rubrique_menu.png)");
        $(selector_this).css("background-image", "url(<?php echo base_url();?>assets/ville-test/wpimages/bg_main_rubrique_menu.png)");
        $("#ul_main_menu_rubrique a:last").css("background-image", "none");
        $("#ul_main_menu_rubrique a.main_menu_active").css("background-image", "none");
    }
</script>


