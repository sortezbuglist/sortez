<div class="container main_nav_bar_filter_menu main_filter_menu d-xl-none">
    <div class="row">
        <nav class="container navbar navbar-toggleable-md navbar-inverse bg-inverse mb-0 w-100 p-0">
            <button class="navbar-toggler navbar-toggler-right collapsed" type="button" data-toggle="collapse"
                    data-target="#navbarsFilterDefault" aria-controls="navbarsFilterDefault" aria-expanded="false"
                    id="navbar_button_filter_link"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><img alt="" src="<?php echo base_url(); ?>application/resources/front/images/vivresaville_custom/burgerIcon.png" class="img-fluid"></span>
            </button>
            <a class="navbar-brand text-right" id="navbar_brand_filter_link" href="javascript:void(0);">Filtrer</a>

            <div class="navbar-collapse collapse" id="navbarsFilterDefault" aria-expanded="false" style="">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active pl-3 pr-3 <?php if (isset($iOrderBy) && $iOrderBy == '1') { ?> main_menu_active <?php } ?>">
                        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annuaire_filter_triage(1);">
                            <input type="radio" class="form-check-input" name="inputStringOrderByHidden_partenaires"
                                   id="optionsRadiosTriage_1" value="1"
                                   <?php if (isset($iOrderBy) && $iOrderBy == '1') { ?>checked<?php } ?>>
                            Les partenaires les plus récents
                        </label>
                    </li>
                    <li class="nav-item  pl-3 pr-3 <?php if (isset($iOrderBy) && $iOrderBy == '2') { ?> main_menu_active <?php } ?>">
                        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annuaire_filter_triage(2);">
                            <input type="radio" class="form-check-input" name="inputStringOrderByHidden_partenaires"
                                   id="optionsRadiosTriage_2" value="2"
                                   <?php if (isset($iOrderBy) && $iOrderBy == '2') { ?>checked<?php } ?>>
                            Les partenaires les plus visitées
                        </label>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>