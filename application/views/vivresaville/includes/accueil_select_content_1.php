    <div class="container accueil_select_content_1 text-center">
    <div class="row">
        <div class="col-sm-3 d-none d-sm-flex d-md-flex d-xl-flex text-center">
            <span class="align-middle w-100"><img
                src="<?php echo base_url(); ?>wpimages_vsv/wp8319ca07_06.png"
                class="border-0 img-fluid"/></span>
        </div>
        <div class="col-sm-6">
            <p><img
                src="<?php echo base_url(); ?>wpimages_vsv/wpfd757cbc_06.png"
                class="border-0 img-fluid"/></p>
            <p class="title_1">ALPES-MARITIMES & MONACO</p>
            <p class="select_dept_other"><a href="/">Choisissez un autre d&eacute;partement</a></p>
        </div>
        <div class="col-sm-3 text-center">
            <span class="align-middle"><img
                src="<?php echo base_url(); ?>wpimages_vsv/wp8319ca07_06.png"
                class="border-0 img-fluid"/></span>
        </div>
    </div>
    <div class="row vsv_select_form_container">
        <div class="col mt-4">
            <form method="post" action="" name="myform">
                <!--<div class="row vsv_select_content">-->
                <span class="span_vsv_select align-middle">
                <select id="mySelect" onchange="myFunction()" name="id_ville_selected" class="select_list_ville_select pl-5">
                    <option value="0">Choisissez votre ville ou territoire...</option>
                    <?php if (isset($villes) && count($villes)>0) { ?>
                        <?php foreach ($villes as $ville_item) { ?>
                            <option value="<?php echo $ville_item->id;?>"><?php echo $ville_item->name_ville;?></option>
                            <!--<?php
                            $vsv_other_else = $this->vsv_ville_other->getByIdVsv($ville_item->id);
                            if (isset($vsv_other_else) && count($vsv_other_else)>0) {
                                foreach ($vsv_other_else as $vsv_item) {
                                    ?>
                                    <option value="<?php echo $ville_item->id;?>"><?php echo $vsv_item->name;?></option>
                                    <?php
                                }
                            }
                            ?>-->
                        <?php } ?>
                    <?php } ?>
                </select>
                </span>
                <!--</div>-->
            </form>
            <script>$(document).ready(function(){$('#mySelect').change(function(){ myform.submit();});});
            </script>
        </div>
    </div>
</div>
    <style type="text/css">
        body {
            background: url(<?php echo base_url()?>wpimages_vsv/wp4365d025_06.png) no-repeat fixed center top / 100% 100%, url(<?php echo base_url()?>wpimages_vsv/wpe252dc21_06.jpg) no-repeat fixed center top / 100% 100% transparent !important;
        }
    </style>