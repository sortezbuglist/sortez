<script type="text/javascript">

    $(function(){

        $("#inputStringVilleHidden_bonplans").change(function(){

            var inputStringVilleHidden_bonplans = $("#inputStringVilleHidden_bonplans").val();

            check_show_categ_fidelity(inputStringVilleHidden_bonplans);
            //alert(inputStringVilleHidden_bonplans);

            $("#span_leftcontener2013_form_bonplans input:checkbox").attr('checked', false);
            $("#frmRechercheBonplan").submit();
        });

        $("#inputStringDepartementHidden_bonplans").change(function(){

            var inputStringDepartementHidden_bonplans = $("#inputStringDepartementHidden_bonplans").val();

            check_show_categ_fidelity(inputStringDepartementHidden_bonplans);
            //alert(inputStringDepartementHidden_bonplans);

            $("#span_leftcontener2013_form_bonplans input:checkbox").attr('checked', false);
            $("#frmRechercheBonplan").submit();
        });

        $("#inputString_zMotCle_submit").click(function(){

            var inputString_zMotCle = $("#inputString_zMotCle").val();
            if (inputString_zMotCle == "RECHERCHER") inputString_zMotCle = "";
            $("#zMotCle").val(inputString_zMotCle);

            $("#span_leftcontener2013_form_bonplans input:checkbox").attr('checked', false);
            $("#frmRechercheBonplan").submit();
        });


        $("#inputStringOrderByHidden_partenaires").change(function(){

            var inputString_zMotCle = $("#inputString_zMotCle").val();
            if (inputString_zMotCle == "RECHERCHER") inputString_zMotCle = "";
            var inputStringOrderByHidden_partenaires = $("#inputStringOrderByHidden_partenaires").val();

            $("#inputStringOrderByHidden").val(inputStringOrderByHidden_partenaires);
            $("#zMotCle").val(inputString_zMotCle);

            //check_show_categ_fidelity(inputStringVilleHidden_bonplans);
            //alert(inputStringVilleHidden_bonplans);

            //$("#span_leftcontener2013_form_bonplans input:checkbox").attr('checked', false);
            $("#frmRechercheBonplan").submit();
        });


        $("#inputStringWhereMultiple_partenaires").change(function(){

            var inputString_zMotCle = $("#inputString_zMotCle").val();
            if (inputString_zMotCle == "RECHERCHER") inputString_zMotCle = "";
            var inputStringOrderByHidden_partenaires = $("#inputStringOrderByHidden_partenaires").val();
            var inputStringWhereMultiple_partenaires = $("#inputStringWhereMultiple_partenaires").val();

            $("#inputStringOrderByHidden").val(inputStringOrderByHidden_partenaires);
            $("#inputStringWhereMultiple").val(inputStringWhereMultiple_partenaires);
            $("#zMotCle").val(inputString_zMotCle);

            //check_show_categ_fidelity(inputStringVilleHidden_bonplans);
            //alert(inputStringVilleHidden_bonplans);

            //$("#span_leftcontener2013_form_bonplans input:checkbox").attr('checked', false);
            $("#frmRechercheBonplan").submit();
        });

    });


    function show_current_categ_subcateg(IdRubrique){
        jQuery(".leftcontener2013content").hide();
        jQuery(".leftcontener2013title_vs").removeClass("leftcontener2013title_vs_active");
        var id_to_show = "#leftcontener2013content_"+IdRubrique;
        //$(id_to_show).show();
        jQuery("#div_subcateg_annuaire_contents").html("");
        //$(id_to_show).appendTo('#div_subcateg_annuaire_contents');
        jQuery("#div_subcateg_annuaire_contents").append($(id_to_show).html());
        jQuery("#div_subcateg_annuaire_contents").show();
        jQuery("#id_subcateg_tab_title").click();
        jQuery("#tab_subcateg").addClass("show");

        jQuery("#span_leftcontener2013_form_partenaires .leftcontener2013title_vs").each(function() {
            $(this).removeClass("leftcontener2013title_vs_active");
        });
        var class_cubcateg_id = ".subcateg_"+IdRubrique;
        jQuery(class_cubcateg_id).addClass("leftcontener2013title_vs_active");
		//window.location.hash = '#span_main_rubrique_banner_title';

        var main_width_device = jQuery(document).width();
        if (main_width_device <= 768) {
            jQuery("#span_leftcontener2013_form_bonplans .leftcontener2013content").css("display", 'none');
            var class_subcateg_content = "#span_leftcontener2013_form_bonplans #leftcontener2013content_"+IdRubrique;
            jQuery(class_subcateg_content).css("display", 'table');
            //alert(main_width_device);
        }
    }

    function all_subcategories_func(IdRubrique="0"){

        var main_width_device = jQuery(document).width();
        if (main_width_device <= 768) {
            var selector = "#span_leftcontener2013_form_bonplans #leftcontener2013content_"+IdRubrique+"  input[type=checkbox]";
            jQuery(selector).each(function() {
                jQuery(this).prop( "checked", true );
            });
        } else {
            jQuery("#tab_subcateg  input[type=checkbox]").each(function() {
                jQuery(this).prop( "checked", true );
            });
        }

        submit_search_bonplan();
    }


    function check_show_categ_fidelity(inputStringDepartementHidden_bonplans, inputStringVilleHidden_bonplans){

        var base_url_visurba = '<?php echo site_url();?>';

        $("#span_leftcontener2013_form_bonplans").html('<div class="col-12 text-center"><img src="'+base_url_visurba+'application/resources/front/images/wait.gif" alt="loading...."/></div>');

        $.post(
            base_url_visurba+'front/fidelity/check_category_list/',
            {
                inputStringDepartementHidden_bonplans: inputStringDepartementHidden_bonplans,
                inputStringVilleHidden_bonplans: inputStringVilleHidden_bonplans
            }
            ,
            function (zReponse)
            {
                //alert ("reponse <br/><br/>" + zReponse) ;
                if (zReponse == "error") { //an other home page exist yet
                    alert("Un probléme est suvenu, veuillez refaire l'opération !");
                } else {
                    $("#span_leftcontener2013_form_bonplans").html(zReponse);

                    $(".leftcontener2013content").hide();

                    $.post(
                        base_url_visurba+'front/fidelity/check_Idcategory_of_subCategory/'
                        ,
                        function (zReponse)
                        {
                            var article_value = zReponse.split("-");
                            for(i = 0; i < article_value.length; i++) {
                                var id_toclick_vsv = "#vsv_categmain_"+article_value[i]+" .leftcontener2013title_vs";
                                jQuery(id_toclick_vsv).addClass("leftcontener2013title_vs_active");
                                jQuery(id_toclick_vsv).click();
                            }
                        }
                    );


                }
            });
    }


    function redirect_bonplan(){
        var base_url_visurba = '<?php echo site_url();?>';

        document.location.href = base_url_visurba+'front/fidelity/redirect_bonplan/';
    }


    function submit_search_bonplan(){

        //setTimeout(alert('test'),5000);
        var allvalue = '';
		var main_width_device = jQuery(document).width();
        if (main_width_device <= 768) {
            $("#span_leftcontener2013_form_bonplans .leftcontener2013content label input:checkbox:checked").each(function(){
                allvalue += ','+$(this).val();
            });
			jQuery("#span_leftcontener2013_form_bonplans .leftcontener2013content div label.subcateb_filter_banner input:checkbox:checked").parent( "label.subcateb_filter_banner" ).each(function(){
				jQuery(this).addClass("loading");
			});
        } else {
            $("#tab_subcateg #div_subcateg_annuaire_contents input:checkbox:checked").each(function(){
                allvalue += ','+$(this).val();
            });
        }
        //alert(allvalue);
        $("#inputStringHidden").val(allvalue);
        $("#frmRechercheBonplan").submit();
    }

    function btn_re_init_fidelity_list(){
        $("#inputStringQuandHidden").val("0");
        $("#inputStringDatedebutHidden").val("");
        $("#inputStringDatefinHidden").val("");
        $("#inputStringDepartementHidden_partenaires").val("0");
        $("#inputStringVilleHidden_partenaires").val("0");
        $("#inputStringWhereMultiple").val("0");
        $("#inputStringHidden").val("");
        $("#inputStringHidden_sub").val("");
        $("#inputStringOrderByHidden").val("");
        $("#zMotCle").val("");

        $("#frmRechercheBonplan").submit();
    }


</script>