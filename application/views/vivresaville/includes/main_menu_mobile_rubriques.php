<div class="container main_nav_bar_rubrique_menu main_rubrique_menu d-lg-none d-xl-none">
    <div class="row">
        <nav class="container navbar navbar-toggleable-md navbar-inverse bg-inverse mb-0 w-100 p-0">
            <button class="navbar-toggler navbar-toggler-left collapsed" type="button" data-toggle="collapse"
                    data-target="#navbarsRubriquesDefault" aria-controls="navbarsRubriquesDefault" aria-expanded="false"
                    id="navbar_button_rubriques_link"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><img alt="" src="<?php echo base_url(); ?>application/resources/front/images/vivresaville_custom/burgerIcon.png" class="img-fluid"></span>
            </button>
            <a class="navbar-brand text-right" id="navbar_brand_rubriques_link" href="javascript:void(0);">Les annuaires</a>

            <div class="navbar-collapse collapse" id="navbarsRubriquesDefault" aria-expanded="false" style="">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active pl-3 pr-3 <?php if (isset($main_menu_content) && $main_menu_content == "home") { ?> main_menu_active <?php } ?>">
                        <a href="<?php echo site_url("vivresaville/Accueil");?>" class="col nav-link">Accueil</a>
                    </li>
                    <li class="nav-item  pl-3 pr-3 <?php if (isset($main_menu_content) && $main_menu_content == "annuaire") { ?> main_menu_active <?php } ?>">
                        <a href="<?php echo site_url("front/annuaire");?>" class="col nav-link ">L'annuaire</a>
                    </li>
                    <li class="nav-item pl-3 pr-3 <?php if (isset($main_menu_content) && $main_menu_content == "article") { ?> main_menu_active <?php } ?>">
                        <a href="<?php echo site_url("article");?>" class="col nav-link ">L'actualité</a>
                    </li>
                    <li class="nav-item  pl-3 pr-3 <?php if (isset($main_menu_content) && $main_menu_content == "agenda") { ?> main_menu_active <?php } ?>">
                        <a href="<?php echo site_url("agenda");?>" class="col nav-link ">L'agenda</a>
                    </li>
                    <li class="nav-item pl-3 pr-3 <?php if (isset($main_menu_content) && $main_menu_content == "bonplan") { ?> main_menu_active <?php } ?>">
                        <a href="<?php echo site_url("front/bonplan");?>" class="col nav-link ">Les bons plans</a>
                    </li>
                    <li class="nav-item pl-3 pr-3 <?php if (isset($main_menu_content) && $main_menu_content == "fidelite") { ?> main_menu_active <?php } ?>">
                        <a href="<?php echo site_url("front/fidelity");?>" class="col nav-link ">La fidélité</a>
                    </li>
                    <li class="nav-item pl-3 pr-3 <?php if (isset($main_menu_content) && $main_menu_content == "boutique") { ?> main_menu_active <?php } ?>">
                        <a href="<?php echo site_url("front/annonce");?>" class="col nav-link ">Les boutiques</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>