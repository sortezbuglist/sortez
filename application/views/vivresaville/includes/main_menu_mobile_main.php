<?php
$thisss =& get_instance();
$thisss->load->library('session');
$main_width_device = $thisss->session->userdata('main_width_device');
$main_mobile_screen = false;
if (isset($main_width_device) && $main_width_device != "" && $main_width_device != "0" && is_numeric($main_width_device) && floatval($main_width_device)<=1199){
    $main_mobile_screen = true;
} else {
    $main_mobile_screen = false;
}
?>

<?php if ($main_mobile_screen==true) { ?>
<div class="container main_nav_bar_menu d-lg-none d-xl-none">
    <div class="row">
        <nav class="container navbar navbar-toggleable-md navbar-inverse bg-inverse mb-0 w-100 p-0">
            <button class="navbar-toggler navbar-toggler-left collapsed" type="button" data-toggle="collapse"
                    data-target="#navbarsMainMenuDefault" aria-controls="navbarsMainMenuDefault" aria-expanded="false"
                    id="navbar_button_main_link"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><img alt="" src="<?php echo base_url(); ?>application/resources/front/images/vivresaville_custom/burgerIcon.png" class="img-fluid"></span>
            </button>
            <a class="navbar-brand text-right" id="navbar_brand_main_link" href="javascript:void(0);">Menu g&eacute;n&eacute;ral</a>

            <div class="navbar-collapse collapse" id="navbarsMainMenuDefault" aria-expanded="false" style="">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="https://spark.adobe.com/page/8WadzNJv1u3cF/" target="_blank">
                            Informations
                            Consommateurs
                        </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="https://spark.adobe.com/page/9elybiCOnQrOC/" target="_blank">
                            Informations
                            Professionnels
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url('auth'); ?>">
                            Mon compte
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url('front/utilisateur/liste_favoris'); ?>">
                            Mes favoris
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url('front/fidelity/menuconsommateurs'); ?>">
                            Ma Carte
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url('contact'); ?>">
                            Nous contacter
                        </a>
                    </li>
                    <li class="nav-item d-none">
                        <a class="nav-link" href="javascript:void(0);<?php //echo site_url('publightbox'); ?>">
                            Inscription &aacute; la Newsletter
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url("vivresaville/accueil/other_ville");?>">
                            Choisissez une autre ville
                        </a>
                    </li>
                    <li class="nav-item d-none d-xl-flex">
                        <a href="javascript:void(0);"
                           id="vsv_main_link_fb" class="vsv_modal_link nav-link"
                           attachmentid="<?php echo site_url('front/professionnels/FacebookPrivicarteForm'); ?>">
                            <img alt=""
                                 src="<?php echo base_url(); ?>assets/ville-test/wpimages/img_social_menu_fb.png">
                        </a>
                    </li>
                    <li class="nav-item d-none d-xl-flex">
                        <a href="javascript:void(0);"
                           id="vsv_main_link_twt" class="vsv_modal_link nav-link"
                           attachmentid="<?php echo site_url('front/professionnels/TwitterPrivicarteForm'); ?>">
                            <img alt=""
                                 src="<?php echo base_url(); ?>assets/ville-test/wpimages/img_social_menu_twt.png">
                        </a>
                    </li>
                    <li class="nav-item">
                        <div id="google_translate_element_2"></div>
                        <script type="text/javascript">
                            function googleTranslateElementInit() {
                                new google.translate.TranslateElement({
                                    pageLanguage: 'fr',
                                    includedLanguages: 'de,en,es,fr,it,ja,nl,no,pl,pt,ru,sv,zh-CN',
                                    layout: google.translate.TranslateElement.InlineLayout.SIMPLE,
                                    autoDisplay: false
                                }, 'google_translate_element_2');
                            }
                        </script>
                        <script type="text/javascript"
                                src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>
<?php } ?>