<div class="container accueil_content_img_btn text-center">
    <div class="content_img_btn_img_top">
        <div class="row content_img_btn_img_pright" style="">
            <div class="col-md-3 text-center content_img_btn_link_pright">
                <img
                    src="<?php echo base_url(); ?>application/resources/front/images/wp13f30e42_06.png"
                    class="border-0 img-fluid"/>
            </div>
            <div class="col-md-9 content_img_btn_link_pright">
                <div class="col-12">


                    <h2>AVEC NOTRE CARTE PRIVILÈGE, VOUS BÉNÉFICIEZ  :</h2>


                    <ul>
<li>De la gestion automatisée de la validation et réservation des bons plans…</li>

                    <li>De la gestion automatisée des conditions de fidalisation (objectifs, solde en cours)…</li>

                    <li>De la gestion des partenaires favoris…</li>

                    <li>Des tirages au sort pour gagner des entrées ou des places de spectacles…</li>

                    <li>Votre carte sortez est valable sur l’ensemble de nos secteurs référencésdétails sur le site national… www.sortez.org</li>
</ul>

                    <p>La carte et les module de gestion sont accesszible sur votre ordinateur, tabllette et mobile.</p>

                    <p>Si vous ne possédez pas de smartphone, il vous suffira avec votre ordinateur familial d’im-primer votre QRCODE et mails de confirmation et de les présenter lors de vos déplacements chez nos partenaires.</p>

                </div>
                <div class="col-12 text-center">
                    <a href="<?php echo site_url('front/particuliers/inscription'); ?>" class="" target="_blank">
                        <img
                            src="<?php echo base_url(); ?>application/resources/front/images/vsv_inscription.png"
                            class="border-0 img-fluid"/></a>
                </div>
            </div>

        </div>
    </div>
</div>