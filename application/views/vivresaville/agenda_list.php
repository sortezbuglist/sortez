<?php if (isset($_GET['content_only_list']) && $_GET['content_only_list'] == '1') {
} else { ?>
<div id="mob_grey2" class="container paddingright0 pb-3" style="display:table; /*background-color: #ffffff;*/">
    <div id="id_mainbody_main" class="col-lg-12 padding0" style="display:table;">
        <?php } ?>

        <script type="text/javascript">
            var iCategorieId_array = [];
            <?php
            $toAgenda['iCategorieId'] = (array)$toAgenda['iCategorieId'];
            for ($i = 0; $i <  count($toAgenda['iCategorieId']); $i++) {
            ?>
            iCategorieId_array.push("<?php echo $toAgenda['iCategorieId'][$i];?>");
            <?php
            }
            ?>
            jQuery.ajax({
                type: "POST",
                url: "<?php echo site_url("agenda/agendaGetFilter"); ?>",
                data: {
                    iCategorieId: iCategorieId_array,
                    iVilleId: "<?php echo $toAgenda['iVilleId'];?>",
                    iDepartementId: "<?php echo $toAgenda['iDepartementId'];?>",
                    zMotCle: "<?php echo $toAgenda['zMotCle'];?>",
                    iSousCategorieId: "<?php echo $toAgenda['iSousCategorieId'];?>",
                    page_pagination: "<?php echo $toAgenda['page_pagination'];?>",
                    per_page: "<?php echo $toAgenda['per_page'];?>",
                    iOrderBy: "<?php echo $toAgenda['iOrderBy'];?>",
                    inputStringQuandHidden: "<?php echo $toAgenda['inputStringQuandHidden'];?>",
                    inputStringDatedebutHidden: "<?php echo $toAgenda['inputStringDatedebutHidden'];?>",
                    inputStringDatefinHidden: "<?php echo $toAgenda['inputStringDatefinHidden'];?>",
                    inputIdCommercant: "<?php echo $toAgenda['inputIdCommercant'];?>"
                },
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    //alert(data[0].id);
                    //alert(data.length);
                    if(data.length>0){
                        for (i = 0; i < data.length; i++) {
                            jQuery.post(
                                "<?php echo site_url("agenda/agendaGetListItem/");?>",
                                {
                                    datetime_id: data[i].datetime_id,
                                    agenda_id: data[i].id
                                },
                                function (response) {
                                    $("#id_mainbody_main").append(response);
                                });
                        }
                    }
                },
                error: function (data) {
                    console.log(data);
                    alert("ERROR : "+data[0].id);
                }
            });
        </script>

        <!--- ALL AGENDA ITEM LIST HERE --->

        <?php if (isset($_GET['content_only_list']) && $_GET['content_only_list'] == '1') {
        } else { ?>
    </div>
</div>
<?php } ?>


<?php if (isset($_GET['content_only_list']) && $_GET['content_only_list'] == '1') {
} else { ?>

    <div class="container pb-3" style="display:table;">
        <?php
        $data['empty'] = null;
        if (strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')) {
            $this->load->view("sortez_vsv/agenda_liste_content_listing_ipad.php", $data);
        } else {
            $this->load->view("sortez_vsv/agenda_liste_content_listing.php", $data);
        }
        ?>
        <!--<?php if (isset($links_pagination) && $links_pagination != "") { ?>
        <div id="view_pagination_ci">
            <?php echo $links_pagination; ?>
        </div>
    <?php } ?>-->
    </div>

<?php } ?>
