<div id="vsv_categmain_<?php echo $oCategorie_principale->agenda_categid ?>" class="vsv_subcateg_filter col-3 pl-0 pt-0 pr-2 pb-2 m-0"
	 style="padding-bottom: 5px">
	<div
		class="leftcontener2013title_vs btn btn-default bt_categorie_list_vivresaville subcateg_<?php echo $oCategorie_principale->agenda_categid ?>">
		<label class="form-check-label btn subcateb_filter_banner w-100"
			   onClick="javascript:show_current_categ_subcateg(<?php echo $oCategorie_principale->agenda_categid ?>);"><!--submit_search_agenda();-->
         
              <!--<input name="check_main_part[<?php echo $ii_rand?>]" id="check_main_part_<?php echo $oCategorie_principale->agenda_categid?>" type="checkbox" value="<?php echo $oCategorie_principale->agenda_categid?>"
                   <?php 
                    if (isset($iCategorieId_sess) && is_array($iCategorieId_sess)) {
                        if (in_array($oCategorie_principale->agenda_categid, $iCategorieId_sess)) echo 'checked';
                    }
	 ?>
                    style="margin-top:2px;">-->
          
    <?php echo ucfirst(strtolower($oCategorie_principale->category)) . " (" . $oCategorie_principale->nb_article . ")"; ?>
	    </label>
    </div>
</div>

<div class="leftcontener2013content" id="leftcontener2013content_<?php echo $oCategorie_principale->agenda_categid ?>" style="margin-bottom:5px; margin-top:10px;">


    <?php
    $OCommercantSousRubrique = $this->mdl_categories_article->GetArticleSouscategorieByRubrique($oCategorie_principale->agenda_categid, $inputStringQuandHidden_partenaires, $inputStringDatedebutHidden_partenaires, $inputStringDatefinHidden_partenaires, $inputStringDepartementHidden_partenaires, $inputStringVilleHidden_partenaires);

    $session_iSousCategorieId = $this->session->userdata('iSousCategorieId_x');
    if (isset($session_iSousCategorieId)) {
        $iSousCategorieId_sess = $session_iSousCategorieId;
    }
    //var_dump($OCommercantSousRubrique);

    if (count($OCommercantSousRubrique) > 0) {

        //adding 'toutes les sous-categories' button
        if($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL) {
            ?>
            <div class="vsv_subcateg_filter <?php if (count($OCommercantSousRubrique)<=4) echo 'col'; else echo 'col-sm-6 col-md-4 col-xl-2';?> paddingleft0 p-2" style="padding-bottom: 5px">
                <label class="form-check-label btn subcateb_filter_banner w-100 all_subcategories_label" onClick="javascript:all_subcategories_func(<?php echo $oCategorie_principale->agenda_categid ?>);">
                    <span>Toutes</span>
                </label>
            </div>
            <?php
        }
        //end adding 'toutes les sous-categories' button

		$i_rand = 0;
		foreach ($OCommercantSousRubrique as $ObjCommercantSousRubrique) {
            if (isset($ObjCommercantSousRubrique->nb_article)) {/* && $ObjCommercantSousRubrique->nb_article != 0   => nb_article = 0 > when articles don't have date */
				?>
				<div class="vsv_subcateg_filter <?php if (count($OCommercantSousRubrique)<=4) echo 'col'; else echo 'col-sm-6 col-md-4 col-xl-2';?> paddingleft0 p-2" style="padding-bottom: 5px">
				<label class="form-check-label btn subcateb_filter_banner w-100">
					<input onClick="javascript:submit_search_agenda();" name="check_part[<?php echo $i_rand ?>]"
						   id="check_part_<?php echo $ObjCommercantSousRubrique->agenda_subcategid; ?>" type="checkbox"
						   value="<?php echo $ObjCommercantSousRubrique->agenda_subcategid; ?>"
					<?php
					if (isset($iSousCategorieId_sess) && is_array($iSousCategorieId_sess)) {
						if (in_array($ObjCommercantSousRubrique->agenda_subcategid, $iSousCategorieId_sess)) echo 'checked';
					}
                    ?>
                    >
                    <?php
					if ($ObjCommercantSousRubrique->nb_article != '0' && $ObjCommercantSousRubrique->nb_article != 0) $nb_article_fin = $ObjCommercantSousRubrique->nb_article;
                    else $nb_article_fin = $ObjCommercantSousRubrique->nb_article_id;
					echo ucfirst(strtolower(truncate($ObjCommercantSousRubrique->subcateg,20,"..."))) . ' (' . $nb_article_fin . ')';
					?>
					</label>
				</div>
			<?php
			}
		$i_rand++;
		}

        if($_SERVER['SERVER_NAME'] == DOMAIN_VIVRESAVILLE_GLOBAL || $_SERVER['SERVER_NAME'] == DOMAIN_WWW_VIVRESAVILLE_GLOBAL) {
            ?>
            <div class="row justify-content-center m-0 d-md-none d-xl-none">
                <div class="col-12 main_subcateg_tab_init">
                    <button class="btn subcateb_filter_banner w-100" onclick="btn_re_init_annuaire_list();">R&eacute;initialiser</button>
                </div>
            </div>
            <?php
        }
	}
?>
</div>