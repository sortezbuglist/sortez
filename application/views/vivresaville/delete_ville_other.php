<style type="text/css">
    body {
        background: none !important;
    }
</style>

<?php $data["zTitle"] = 'Vivresaville'; ?>
<?php $this->load->view("vivresaville/includes/main_header", $data); ?>
<div class="row m-0">
    <div class="col-12 alert alert-danger" role="alert">
        <strong>Confirmation !</strong> Voulez vous supprimer définitivement l'élément séléctionné ?
    </div>
    <div class="col-12 mt-5 text-center">
        <a href="<?php echo base_url();?>vivresaville/page/delete_autre_ville_confirmed/<?php if (isset($id_vivresaville)) echo $id_vivresaville; ?>/<?php if (isset($id_autre_ville)) echo $id_autre_ville; ?>" class="btn btn-danger">Supprimer</a>
    </div>
</div>
<?php $this->load->view("vivresaville/includes/main_footer", $data); ?>