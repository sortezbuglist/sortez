<div class="container accueil_content_middle" style="background-color: #DA1181;">
    <div class="row justify-content-center pt-5 pb-5 vsv_accueil_title_content">
        <div class="col-12 text-center vsv_accueil_title">
            <h1>JE DÉCOUVRE LES ACTIONS PROMOTIONNELLES<br/>
                DÉPOSÉES PAR LES COMMERÇANTS ET ARTISANS !...
            </h1>
        </div>
    </div>
    <div class="col rubriques_content pl-0 pt-5">
        <div class="row text-center">
            <div class="col-sm-6 pr-0">
                <div class="col-12 p-0 text-center">
                    <?php
                    if (isset($oville->home_photo_article) && $oville->home_photo_bonplan != "0" && $oville->home_photo_bonplan != "" && file_exists("application/resources/front/images/vivresaville/" . $oville->home_photo_bonplan)) { ?>
                        <div class="col-12 p-0 vsv_accueil_rubrique_image text-center">
                            <a href="<?php echo site_url('article');?>" class="">
                                <img
                                    src="<?php echo base_url(); ?>application/resources/front/images/vivresaville/<?php echo $oville->home_photo_bonplan; ?>"
                                    style="width: 100%" class="mt-0 border-0"/></a>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-12 p-3 vsv_accueil_rubrique_title text-center">
                    <p>LES BONS PLANS</p>
                    <p>Découvrez des offres uniques, exceptionnelles, motivantes pour le consommateur.</p>
                </div>
                <div class="col-12 pb-5 text-center vsv_accueil_rubrique_btn">
                    <a href="<?php echo site_url('front/bonplan'); ?>" class="vsv_accueil_btn_black  ml-auto mr-auto">Accès...</a>
                </div>
            </div>
            <div class="col-sm-6 pr-0">
                <div class="col-12 p-0 text-center">
                    <?php
                    if (isset($oville->home_photo_fidelite) && $oville->home_photo_fidelite != "0" && $oville->home_photo_fidelite != "" && file_exists("application/resources/front/images/vivresaville/" . $oville->home_photo_fidelite)) { ?>
                        <div class="col-12 p-0 vsv_accueil_rubrique_image text-center">
                            <a href="<?php echo site_url('article');?>" class="">
                                <img
                                    src="<?php echo base_url(); ?>application/resources/front/images/vivresaville/<?php echo $oville->home_photo_fidelite; ?>"
                                    style="width: 100%" class="mt-0 border-0"/></a>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-12 p-3 vsv_accueil_rubrique_title text-center">
                    <p>LES CONDITIONS DE FIDÉLISATION</p>
                    <p>Découvrez des offres de fidélisation : des remises Cash, des offres de capitalisation ou coups de tampons</p>
                </div>
                <div class="col-12 pb-5 text-center vsv_accueil_rubrique_btn">
                    <a href="<?php echo site_url('front/fidelity'); ?>" class="vsv_accueil_btn_black  ml-auto mr-auto">Accès...</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row text-center pb-5 pt-3">
        <div class="col-12 pr-0">
            <h2>
                POUR EN BÉNÉFICIER, VOUS DEMANDEZ<br/>
                GRATUITEMENT NOTRE CARTE PRIVILÈGE !...<br/>
                DÈS VALIDATION, CETTE DERNIÈRE DÉMATÉRIALISÉE<br/>
                APPARAÎTRA SUR UN COMPTE PERSONNEL ET SÉCURISÉ...
            </h2>
        </div>

    </div>


    <!--<div class="col-sm-4 pr-0">
        <a href="<?php echo site_url('front/particuliers/inscription');?>" class="" target="_blank">
            <img
                src="<?php echo base_url(); ?>application/resources/front/images/vsv_inscription.png"
                class="border-0"/></a>
    </div>-->








</div>