<div class="container accueil_content_top" style="background-color: #ffffff;">
    <div class="row">
        <?php
        if (isset($oville->home_photo_banner) && $oville->home_photo_banner != "0" && $oville->home_photo_banner != "" && file_exists("application/resources/front/images/vivresaville/" . $oville->home_photo_banner)) { ?>
        <a href="<?php echo site_url('front/annuaire');?>" class="w-100"><img
                src="<?php echo base_url(); ?>application/resources/front/images/vivresaville/<?php echo $oville->home_photo_banner; ?>"
                style="width: 100%" class="mt-0 border-0"/></a>
        <?php } ?>
    </div>
    <div class="row justify-content-center mt-5 mb-5">
        <div class="col-12 text-center vsv_accueil_title">
            <h1>JE DÉCOUVRE MON TERRITOIRE</h1>
            <h3>SORTIES, LOISIRS, CULTURE, ARTISANAT, COMMERCES, PATRIMOINE…</h3>
        </div>
        <div class="col-md-4 vsv_accueil_div text-center pt-5">
            <a href="<?php echo site_url('front/annuaire');?>" class="vsv_accueil_btn">Accès à L'annuaire complet...</a>
        </div>
    </div>
    <div class="container pl-0">
    <div class="row">
        <div class="col-sm-4 pr-0">
            <div class="col-12 p-0 text-center">
                <?php
                if (isset($oville->home_photo_article) && $oville->home_photo_article != "0" && $oville->home_photo_article != "" && file_exists("application/resources/front/images/vivresaville/" . $oville->home_photo_article)) { ?>
                <div class="col-12 p-0 vsv_accueil_rubrique_image text-center">
                    <a href="<?php echo site_url('article');?>" class="">
                    <img
                        src="<?php echo base_url(); ?>application/resources/front/images/vivresaville/<?php echo $oville->home_photo_article; ?>"
                        style="width: 100%" class="mt-0 border-0"/></a>
                    </div>
                <?php } ?>
            </div>
            <div class="col-12 p-3 vsv_accueil_rubrique_title text-center">
                JE DÉCOUVRE<br>L’ACTUALITÉ
            </div>
            <div class="col-12 pb-5 text-center vsv_accueil_rubrique_btn">
                <a href="<?php echo site_url('article');?>" class="vsv_accueil_btn">Accès...</a>
            </div>
        </div>
        <div class="col-sm-4 pr-0">
            <div class="col-12 p-0 text-center">
                <?php
                if (isset($oville->home_photo_agenda) && $oville->home_photo_agenda != "0" && $oville->home_photo_agenda != "" && file_exists("application/resources/front/images/vivresaville/" . $oville->home_photo_agenda)) { ?>
                <div class="col-12 p-0 vsv_accueil_rubrique_image text-center">
                    <a href="<?php echo site_url('agenda');?>" class="">
                    <img
                        src="<?php echo base_url(); ?>application/resources/front/images/vivresaville/<?php echo $oville->home_photo_agenda; ?>"
                        style="width: 100%" class="mt-0 border-0"/></a>
                    </div>
                <?php } ?>
            </div>
            <div class="col-12 p-3 vsv_accueil_rubrique_title text-center">
                JE DÉCOUVRE L’AGENDA<br/>
                ÉVÉNEMENTIEL
            </div>
            <div class="col-12 pb-5 text-center vsv_accueil_rubrique_btn">
                <a href="<?php echo site_url('agenda');?>" class="vsv_accueil_btn">Accès...</a>
            </div>
        </div>
        <div class="col-sm-4 pr-0">
            <div class="col-12 p-0 text-center">
                <?php
                if (isset($oville->home_photo_annonce) && $oville->home_photo_annonce != "0" && $oville->home_photo_annonce != "" && file_exists("application/resources/front/images/vivresaville/" . $oville->home_photo_annonce)) { ?>
                <div class="col-12 p-0 vsv_accueil_rubrique_image text-center">
                    <a href="<?php echo site_url('front/annonce');?>" class="">
                    <img
                        src="<?php echo base_url(); ?>application/resources/front/images/vivresaville/<?php echo $oville->home_photo_annonce; ?>"
                        style="width: 100%" class="mt-0 border-0"/></a>
                    </div>
                <?php } ?>
            </div>
            <div class="col-12 p-3 vsv_accueil_rubrique_title text-center">
                JE DÉCOUVRE LES ANNONCES<br/>
                DES PROFESSIONNELS
            </div>
            <div class="col-12 pb-5 text-center vsv_accueil_rubrique_btn">
                <a href="<?php echo site_url('front/annonce');?>" class="vsv_accueil_btn">Accès...</a>
            </div>
        </div>
    </div>
    </div>
</div>