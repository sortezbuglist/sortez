<div class="container main_rubrique_menu  d-lg-block d-xl-block"
     style="border-bottom: double;border-bottom:10px solid #DA1181;">
    <div class="row text-center h-100" id="ul_main_menu_rubrique">
        <a class="col-lg-2 col-sm-4" href="<?php echo site_url("front/annuaire"); ?>" id="ab">
            <div class="cole <?php if (isset($main_menu_content) && $main_menu_content == "annuaire") { ?> main_menu_active <?php } ?>">L'ANNUAIRE
            </div>
        </a>
        <a class="col-lg-2 col-sm-4" href="<?php echo site_url("article"); ?>" id="ab">
            <div class="cole <?php if (isset($main_menu_content) && $main_menu_content == "article") { ?> main_menu_active <?php } ?>">L'ACTUALIT&Eacute;
            </div>
        </a>
        <a class="col-lg-2 col-sm-4" href="<?php echo site_url("agenda"); ?>" id="ab">
            <div class="cole <?php if (isset($main_menu_content) && $main_menu_content == "agenda") { ?> main_menu_active <?php } ?>">L'AGENDA
            </div>
        </a>
        <a class="col-lg-2 col-sm-4" href="<?php echo site_url("front/bonplan"); ?>" id="ab">
            <div class="cole <?php if (isset($main_menu_content) && $main_menu_content == "bonplan") { ?> main_menu_active <?php } ?>">LES BONS PLANS
            </div>
        </a>

        <a class="col-lg-2 col-sm-4" href="<?php echo site_url("front/fidelity"); ?>" id="ab">
            <div class="cole <?php if (isset($main_menu_content) && $main_menu_content == "fidelite") { ?> main_menu_active <?php } ?>">LA FIDELIT&Eacute;
            </div>
        </a>
        <a class="col-lg-2 col-sm-4" href="<?php echo site_url("front/annonce"); ?>" id="ab">
            <div class="cole <?php if (isset($main_menu_content) && $main_menu_content == "boutique") { ?> main_menu_active <?php } ?>">LES BOUTIQUES
            </div>
        </a>

    </div>
</div>


<style type="text/css">

    #ab {
        font-size: 20px;
        text-decoration: none !important;
    }

    .cole {
        background-color: black;
        border-top-right-radius: 15px;
        border-top-left-radius: 15px;
        height: 100%;
        padding-top: 10px;
        color: white;
        font-size: 15px;
        font-weight: bolder;
        box-shadow: 3px 0 grey;
    }

    .cole:hover {
        background-color: #DA1181;

    }
</style>

