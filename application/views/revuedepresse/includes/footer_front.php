<div class="container d-none d-lg-block d-sm-block">
    <div class="row text-center">
        <div class="col-lg-12" style="background-color: #DC188E;"><img class="img-fluid" src="<?php echo base_url();?>essai/wpimages/wp2d2dd156_06.png"></div>
    </div>
    <div class="row pt-3 pb-3" style="background-color: #DC188E!important;color: white;font-size: 15px;">
        <div class="col-lg-3">
            <span class="titre_men">LES ANNUAIRES</span></a><br><br>

            <a id="linken" href="<?php echo site_url('front/annuaire') ?>"><span>&diams;&nbsp;L’annuaire</span></a><br>
            <a id="linken" href="<?php echo site_url('article') ?>"><span>&diams;&nbsp;L’actualité</span></a>
            <ul>
                <a id="linken" href="<?php echo site_url('article') ?>"><li> Les articles de Sortez & partenaires</li></a>
                <a id="linken" href="<?php echo site_url('article') ?>"><li>La revue de presse</li></a>
            </ul>
            <a id="linken" href="<?php echo site_url('agenda') ?>"><span>&diams;&nbsp;L’agenda</span></a>
            <ul>
                <li>L’agenda événementiel</li>
                <a id="linken" href="<?php echo site_url('front/festivals') ?>"><li>Les festivals</li></a>
            </ul>

            <a id="linken" href="<?php echo site_url('front/bonplan') ?>"><span>&diams;&nbsp;Les bons plans</span></a><br>
            <a id="linken" href="<?php echo site_url('front/fidelity') ?>"><span>&diams;&nbsp;La fidélisation</span></a><br>
            <a id="linken" href="<?php echo site_url('front/annonce') ?>"><span>&diams;&nbsp;Les boutiques</span></a>


        </div>
        <div class="col-lg-3">
            <span class="titre_men">LE MAGAZINE SORTEZ</span></a><br><br>
            <a id="linken" href="<?php echo site_url('edition-mois.html') ?>"><span>&diams;&nbsp;L’édition du mois</span></a><br>
            <a id="linken" href="http://fliphtml5.com/bookcase/eirf"><span>&diams;&nbsp;Les archives</span></a><br>
            <a id="linken" href="<?php echo site_url('article') ?>"><span>&diams;&nbsp;Les articles numériaues</span></a><br><br><br>


            <span class="titre_men">MON COMPTE CONSOMMATEUR</span></a><br><br>
            <a id="linken" href="<?php echo site_url('infos-consommateurs.html') ?>"><span>&diams;&nbsp;Infos consommateur</span></a><br>
            <a id="linken" href="<?php echo site_url('front/utilisateur/menuconsommateurs') ?>"><span>&diams;&nbsp;Accès admin</span></a><br>
            <a id="linken" href="<?php echo site_url('front/utilisateur/liste_favoris') ?>"><span>&diams;&nbsp;Mes favoris</span></a><br>
            <a id="linken" href="<?php echo site_url('front/user_fidelity/show_fidelity_card') ?>"><span>&diams;&nbsp;Ma carte</span></a><br>
            <a id="linken" href="<?php echo site_url('front/particuliers/inscription') ?>"><span>&diams;&nbsp;Inscriptions</span></a><br>
            <a id="linken" href="<?php echo site_url('mentions-legales') ?>"><span>&diams;&nbsp;Condition générales</span></a><br>


        </div>
        <div class="col-lg-3">
            <span class="titre_men">MON COMPTE PROFESSIONNEL</span></a><br><br>

            <a id="linken" href="<?php echo site_url('infos-pro') ?>"><span>&diams;&nbsp;Infos professionnel</span></a><br>
            <a id="linken" href="<?php echo site_url('auth/login') ?>"><span>&diams;&nbsp;Accès admin</span></a><br>
            <a id="linken" href="<?php echo site_url('front/professionnels/inscription') ?>"><span>&diams;&nbsp;Inscriptions</span></a><br>
            <a id="linken" href="<?php echo site_url('mentions-legales') ?>"><span>&diams;&nbsp;Conditions générales</span></a><br><br>


            <span class="titre_men">NOS COORDONNÉES</span></a><br><br>

            <a id="linken" href="<?php echo site_url('contact') ?>"><span>&diams;&nbsp;Nous contacter</span></a><br>
            <a id="linken" href="<?php echo site_url('front/particuliers/inscription') ?>"><span>&diams;&nbsp;Mentions l&eacute;gales</span></a><br>
        </div>
        <div class="col-lg-3">
            <span class="titre_men">DOCUMENTATION COMMERCIALE</span></a><br><br>

            <a id="linken" href="<?php echo site_url('infos-consommateurs') ?>"><span>&diams;&nbsp;Infos consommateurs</span></a>
            <ul>
                <li>L’inscription et la carte</li>
            </ul>
            <span>&diams;&nbsp;Infos professionnels « le magazine »</span>
            <ul>
                <a id="linken" href="<?php echo site_url('magazine-sortez.pdf') ?>"><li>Le magazine</li></a>

                <a id="linken" href="<?php echo site_url('Dimensions Pub.pdf') ?>"><li>les dimensions « pub »</li></a>

                <a id="linken" href="<?php echo site_url('Sortez-packarticleok.pdf') ?>"><li>Le pack « articles »</li></a>

                <a id="linken" href="<?php echo site_url('Sortez-packselectionok.pdf') ?>"><li>le pack « sélections »</li></a>
            </ul>
            <span>&diams;&nbsp;Infos professionnels « le web »</span>
            <ul>
                <a id="linken" href="<?php echo site_url('pack-infos.html') ?>"><li>Le pack « infos »</li></a>

                <a id="linken" href="<?php echo site_url('pack-promo.html') ?>"><li>Le pack « promos »</li></a>

                <a id="linken" href="<?php echo site_url('boutiques.html') ?>"><li>Le pack « boutique en ligne »</li></a>

                <a id="linken" href="<?php echo site_url('pack-export.html') ?>"><li> Les packs« Export »</li></a>
            </ul>
            <a id="linken" href="<?php echo site_url('contact') ?>"><span>&diams;&nbsp;La tarification</span></a>

        </div>
        <style type="text/css">
            ul {
                list-style-type: none;
            }
            li:before {
                content: '- ';
            }
            .titre_men{
                font-weight: bold;
                font-size: 16px!important;
            }
            #linken{
                color: white;!important;
            }
            #linken:visited{
                color: white;!important;
            }
            #linken:hover{
                color: grey;!important;
                text-decoration: none;
            }
        </style>
    </div>
    <div class="row text-center">
        <div class="col-lg-12" style="background-color: #DC188E;"><img class="img-fluid" src="<?php echo base_url();?>essai/wpimages/wp2d2dd156_06.png"></div>
    </div>
</div>

