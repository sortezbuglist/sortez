<?php
//LOCALDATA FILTRE
$this_session_localdata =& get_instance();
$this_session_localdata->load->library('session');
$localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
$localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
$iVilleId_x = $this_session_localdata->session->userdata('iVilleId_x');
?>
<div class="dropdown">
    <button style="background-color: white;
border-radius:20px;color: black;border-color:deeppink!important;border-width:medium!important; " class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton_com" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?php
        if(isset($iVilleId_x) && $iVilleId_x != "" && $iVilleId_x != 0 && $iVilleId_x != "0")
        {
            foreach($toVille as $oVille){
                if ($oVille->IdVille == $iVilleId_x)
                    echo $oVille->ville_nom." (".$oVille->nbCommercant.")".'</a>';
            }
        } else echo 'Toutes les communes';
        ?>
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton_com">
        <a class="dropdown-item" href="javascript:void(0);" onclick="javascript:pvc_select_commune(0);">RECHERCHES PAR COMMUNE</a>
        <?php
        if(isset($toVille))
        {
            foreach($toVille as $oVille){ ?>

                <a class="dropdown-item" href="javascript:void(0);" onclick="javascript:pvc_select_commune(<?php echo $oVille->IdVille ; ?>);"><?php echo $oVille->ville_nom." (".$oVille->nbCommercant.")" ; ?></a>
            <?php } ?>
        <?php }
        ?>
    </div>
</div>
<script type="text/javascript">
    function pvc_select_commune(id_commune){
        //alert(id_commune);
        $("#inputStringVilleHidden_bonplans").val(id_commune);
        $("#inputStringVilleHidden_partenaires").val(id_commune);
        $("#inputStringVilleHidden_annonces").val(id_commune);
        $("#frmRechercheBonplan").submit();
        $("#frmRecherchePartenaire").submit();
        $("#frmRechercheAnnonce").submit();

    }
</script>
<style type="text/css">
    .dropdown-menu {
        max-height: 400px;
        overflow-y: scroll;
    }
    #dropdownMenuButton_com {
        width: 290px;
        height: 45px;

    }
    #dropdownMenuButton_com:hover,
    #dropdownMenuButton_com:focus{
        background-color: #fff;
        border-style: double !important;
    }
</style>