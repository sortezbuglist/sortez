<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="keywords" content="sorties, loisirs, concerts, spectacles, théâtres, alpes-maritimes, monaco, menton, nice, cannes, antibes, Théâtres, concerts, spectacles, animations, magazine, sortez, sortir, cannes, alpes-maritimes">
    <meta name="description" content="Le Magazine Sortez : l'essentiel des sorties et des loisirs sur les Alpes-Maritimes et Monaco, animations, concerts, spectacles, vie locale...">
    <meta name="author" content="Priviconcept">


    <meta property="fb:app_id" content="324903384595223" />
    <!--<meta property="fb:app_id" content="1292726270756136" />-->
    <meta property="og:locale" content="fr_FR" />
    <meta property="og:title" content="<?php if (isset($zTitle) && $zTitle != "" && $zTitle != NULL) echo $zTitle; else echo "Le Magazine Sortez"; ?>" />
    <meta property="og:description" content="<?php if (isset($zDescription) && $zDescription != "" && $zDescription != NULL) echo $zDescription; else echo "Sortez : L'essentiel des sorties et des loisirs Alpes-Maritimes et Monaco"; ?>" />
    <meta property="og:url" content='<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>' />
    <meta property="og:site_name" content="Le Magazine Sortez : L'essentiel des sorties et des loisirs Alpes-Maritimes et Monaco" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="<?php if (isset($zMetaImage) && $zMetaImage != "" && $zMetaImage != NULL) echo $zMetaImage; else echo base_url()."application/resources/sortez/images/logo.png"; ?>" />

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@Sortez_org" />
    <meta name="twitter:creator" content="@techsortez"/>
    <meta name="twitter:title" content="<?php if (isset($zTitle) && $zTitle != "" && $zTitle != NULL) echo $zTitle; else echo "Le Magazine Sortez"; ?>" />
    <meta name="twitter:description" content="<?php if (isset($zDescription) && $zDescription != "" && $zDescription != NULL) echo $zDescription; else echo "Sortez : L'essentiel des sorties et des loisirs Alpes-Maritimes et Monaco"; ?>" />
    <meta name="twitter:image" content="<?php if (isset($zMetaImage) && $zMetaImage != "" && $zMetaImage != NULL) echo $zMetaImage; else echo base_url()."application/resources/sortez/images/logo.png"; ?>" />

    <link rel="icon" href="images/favicon.ico">
    <?php if (!isset($data)) $data['data_init'] = true; ?>

    <title><?php if (isset($zTitle) && $zTitle != "" && $zTitle != NULL) echo $zTitle; else echo "Sortez"; ?></title>

    <!-- bootstrap core -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="<?php echo base_url();?>application/views/sortez_vsv/bootstrap/css/bootstrap-datepicker.standalone.min.css" rel="stylesheet" type="text/css">

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url();?>application/views/sortez_vsv/jquery/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url();?>application/views/sortez_vsv/bootstrap/js/popper.min.js"></script>
    <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
    <script src="<?php echo base_url();?>application/views/sortez_vsv/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>application/views/sortez_vsv/bootstrap/js/bootstrap-datepicker-fr.min.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script>
        $(function() {
            window.location.replace("<?php echo $oDetailAgenda->siteweb; ?>");
            $("#skill_input").autocomplete({
                source: "<?php echo base_url('skills/autocompleteData'); ?>",
            });
        });
    </script>
    <script src="<?php echo base_url();?>application/views/sortez_vsv/jquery/jquery-migrate-1.4.1.min.js"></script>
    <?php $this->load->view("sortez_vsv/js/global_js", $data); ?>
    <script type="text/javascript" src="<?php echo base_url();?>application/views/sortez_vsv/js/global.js"></script>
    <!--<link rel="stylesheet" href="//code.jquery.com/mobile/1.5.0-alpha.1/jquery.mobile-1.5.0-alpha.1.min.css">
    <script src="//code.jquery.com/mobile/1.5.0-alpha.1/jquery.mobile-1.5.0-alpha.1.min.js"></script>-->
    <?php $this->load->view("sortez_vsv/css/global_css", $data); ?>
    <link href="<?php echo base_url();?>application/views/sortez_vsv/css/global.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>application/views/sortez_vsv/css/responsive_max_320.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>application/views/sortez_vsv/css/responsive_max_480.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>application/views/sortez_vsv/css/responsive_max_575.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>application/views/sortez_vsv/css/responsive_max_768.css" rel="stylesheet" type="text/css">
    <?php $this->load->view("sortez_vsv/css/responsive_max_768", $data); ?>
    <link href="<?php echo base_url();?>application/views/sortez_vsv/css/responsive_max_991.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>application/views/sortez_vsv/css/responsive_max_1199.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>application/views/sortez_vsv/css/responsive_min_1200.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>application/views/sortez_vsv/css/navbar.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="text-center pt-3 pb-3">
    <?php if (isset($parent_page) AND $parent_page =='revue'){ ?>
<a class="btn btn-info text-center" href="<?php echo site_url('revuedepresse'); ?>">Revenir vers sortez.org</a><br>
    <?php }elseif (isset($parent_page) AND $parent_page =='export'){ ?>
        <a class="btn btn-info text-center" href="<?php echo site_url('article/article_perso/?zCouleur=FFFFFF&amp;zCouleurTitre=0f0e0f&amp;zCouleurTextBouton=000000&amp;zCouleurBgBouton=d91cc9&amp;zCouleurNbBtn=1&amp;tiDepartement=15590_2076_2031_2003_36831_2081_36941_1941_36833_29483_2042_36960_2050_33688_36944_2063_2004_&amp;tiDeposant=300524_300697_300856_301299_&amp;tiCategorie=49_51_56_57_66_67_76_77_79_81_83_86_87_88_90_91_92_93_94_95_97_100_103_104_112_&amp;tiDossperso=0'); ?>">Revenir vers sortez.org</a><br>
  <?php  } else { ?>
        <a class="btn btn-info text-center" href="<?php echo site_url('article'); ?>">Revenir vers sortez.org</a><br>
    <?php } ?>
    <?php //echo $oDetailAgenda->siteweb; ?>
</div>
<div><iframe style="width: 100%;height: 100%; border: none;" src="<?php if (!preg_match('/allocine/',$oDetailAgenda->siteweb)){ echo str_replace("http://","https://",$oDetailAgenda->siteweb);}else{echo str_replace("https://","http://",$oDetailAgenda->siteweb); } ?>"></iframe>
</div>
</body>
</html>