

<div class="leftcontener2013content_head">

<div style='margin-top:30px; margin-bottom:20px; color: #ffffff; font-family: "Vladimir Script",cursive; text-align:center; font-size: 48px; line-height: 47px;'> Edito </div>

<div><img src="<?php echo GetImagePath("front/"); ?>/localdata/cagnescommerces/editeur_cagnescommerces.png" width="184" height="209" alt="editeur" /></div>

<div style='color:#FFF;
    font-family: "Arial",sans-serif;
    font-size: 12px;
    font-weight: 200;
'>
<p>Cagnes-sur-Mer  : Retrouvez  les bonnes   adresses et l&rsquo;agenda de votre ville (commerces, artisanat, services,   culture, vie locale et associative…)</p>
<p>Nous vous souhaitons la bienvenue sur le site de CAGNES-COMMERCES.fr</p>
<p>Ce site est une réalisation du Magazine Proximité.</p>
<p>Depuis plus de 17 années, notre magazine informe ses lecteurs sur les infos essentielles des Alpes-Maritimes &amp; Monaco (sorties, loisirs, vie locale…).</p>
<p>vous trouverez les liens départementaux en haut de cette page:</p>
<ul>
  <li>Le Magazine</li>
  <li>L&rsquo;annuaire 06</li>
  <li>L&rsquo;agenda 06</li>
  <li>Notre sélection 06</li>
</ul>
<p>En créant ce site local, nous confirmons notre vocation d&rsquo;informations de proximité.</p>
<p>Ce site reprend des données locales : des   bonnes adresses, des annonces, des bons plans mais aussi l&rsquo;agenda de   votre ville et bientôt un  blog sur votre ville.</p>
<p>Ce site nous le désirons mutualiste, dynamique. Les commerçants, artisans, associations peuvent se référencer GRATUITEMENT.</p>
<p>Les fonctions et modules varient en fonction des abonnements choisis.</p>
<p>Pour plus d&rsquo;informations, n&rsquo;hésitez pas à nous contacter , ce site est à vous !…</p>
<p>Les données de votre établissement sont également référencés sur notre annuaire et agenda départemental.</p>
</div>

</div>

      
