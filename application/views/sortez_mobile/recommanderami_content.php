<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.js"></script>
<script type="text/javascript">

$(document).ready(function() {

	$("#frmRecomanderAmi").validate({
		rules: {
			zNom:  {
				required: true,
				email: true
			},
			zTelephone: "required",
			zEmail: {
				required: true,
				email: true
			},
			zCommentaire: "required"
		},
		messages: {
			zNom: "Veuillez entrer un adresse mail valide.",
			zTelephone: "Veuillez indiquer votre Nom",
			zEmail: "Veuillez entrer un adresse mail valide.",
			zCommentaire: "Veuillez iniquer votre message."
		}
		
		
	});
	
	
});

</script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">


<style type="text/css">
#frmRecomanderAmi label.error {
	color:#FF0000;
	display:inline;
	margin-left:10px;
	width:auto;
}
</style>

<br/>
<!--<form name="frmNousContacter" id="frmNousContacter" action="<?php// echo site_url("front/annonce/envoiMailNousContacter"); ?>" method="post" enctype="multipart/form-data">-->
<form name="frmRecomanderAmi" id="frmRecomanderAmi" action="<?php echo site_url("front/commercant/envoiMailRecommander/".$IdCommercant."/article"); ?>" method="post" enctype="multipart/form-data">

<table align="center">
<tr>
    <td colspan="2"><span style="font-weight:700; font-size:29.0px; line-height:1.21em; color:#000000;">Recommander &agrave; un ami</span></td>
  </tr>
</table>

<table width="100%" align="center">
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<tr>
    <td><label>Email de votre ami</label></td>
    <td><input type="text" name="zEmail" id="zEmail" class="form-control"/></td>
  </tr>
  <tr>
    <td><label>Votre Email</label></td>
    <td>
    <input type="text" name="zNom" id="zNom" class="form-control"/>
    </td>
  </tr>
  
  <tr>
    <td><label>Votre Nom</label></td>
    <td><input type="text" name="zTelephone" id="zTelephone" class="form-control"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td style="float: right; padding-right: 15px;">
    <button type="button" class="Button1 btn btn-default" onclick="javascript:history.back();">
    Annuler
    </button>
    </td>
    <td>
    <button id="submit" name="submit" type="submit" class="Button1 btn btn-success">
    Valider
    </button>
    
   <!-- <a href="Javascript:void()" class="Button1">Valider</a>-->
    </td>
  </tr>
  <tr style="visibility:hidden;">
    <td>Commentaires</td>
    <td>
    <textarea name="zCommentaire" id="zCommentaire" cols="45" rows="5" style="width:100%;">Ceci est une recommandation de mail venant d'un de vos contacts sur Un article du site Sortez ! <?php echo base_url();?>/article/details/<?php echo $idAgenda;?> </textarea>
    </td>
  </tr>
  
</table>

</form>
