<?php
$base_path_system = str_replace('system/', '', BASEPATH);
include($base_path_system . 'application/views/front2013/includes/url_var_definition.php');
?>
<?php
$this->load->model("user");
$thisss =& get_instance();
$thisss->load->library('ion_auth');
$this->load->model("ion_auth_used_by_club");
$user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oDetailAgenda->IdCommercant);
if (isset($user_ion_auth_id)) $user_groups = $thisss->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;
?>

<?php
$data["zTitle"] = $oDetailAgenda->nom_manifestation;
?>


<!--<script type="text/javascript" src="<?php //echo GetJsPath("front/") ; ?>/wpscripts2013/jquery-1.8.3.js"></script>-->
<script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jquery.ui.core.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" media="screen" type="text/css"
      href="<?php echo GetCssPath("front/"); ?>/jquery-ui-1.8.4.custom.css"/>

<?php $this->load->view("sortez/includes/ready_article", $data); ?>

<div class="col-xs-12 padding0" style="background-color:#FFFFFF; color:#000000;font-size: 19px;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;">

    <div class="col-xs-12 details_event_main_btn_back"><a
            href="<?php echo site_url($commercant_url_home . '/article'); ?>">Retour aux articles du diffuseur</a></div>

    <div
        style="padding-top:40px; text-align:center; width:100%; margin-left: auto; margin-right: auto; padding-bottom:30px; display: table;">
        <?php $this->load->view("sortez/includes/slide_details_article", $data); ?>
    </div>

    <div class="col-xs-12 padding0 agenda_details_ico_menu">
        <div class="col-xs-12 padding0">
            <div class="col-xs-15">
                <?php
                if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) {
                    $iframe_session_navigation_back_link = "javascript:history.back();";
                } else if (isset($iframe_session_navigation) && $iframe_session_navigation == "1") {
                    $iframe_session_navigation_back_link = "javascript:history.back();";
                } else {
                    $iframe_session_navigation_back_link = site_url($oCommercant->nom_url . "/article");
                }
                ?>
                <a href="<?php echo $iframe_session_navigation_back_link; ?>"><img
                        src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mobile_ag_back.png"/></a>
            </div>
            <div class="col-xs-15">
                <a href="<?php echo site_url($commercant_url_video); ?>" title="Video"><img
                        src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mobile_ag_video.png"/></a>
            </div>
            <div class="col-xs-15">
                <?php if (isset($oDetailAgenda->pdf) && $oDetailAgenda->pdf != "") $pdf_agenda_link = $oDetailAgenda->pdf; else $pdf_agenda_link = "javascript:void(0);"; ?>
                <a href="<?php echo $pdf_agenda_link; ?>" title="<?php echo $oDetailAgenda->titre_pdf; ?>"><img
                        src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mobile_ag_pdf.png"/></a>
            </div>
            <div class="col-xs-15">
                <a href="<?php echo site_url($commercant_url_nous_contacter); ?>"><img
                        src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mobile_ag_contact.png"/></a>
            </div>
            <div class="col-xs-15">
                <?php if (isset($oDetailAgenda->facebook) && $oDetailAgenda->facebook != "" && isset($oDetailAgenda->diffusion_facebook) && $oDetailAgenda->diffusion_facebook == "1") $facebook_agenda_link = $oDetailAgenda->facebook; else $facebook_agenda_link = "javascript:void(0);"; ?>
                <a href="<?php echo $facebook_agenda_link; ?>"><img
                        src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mobile_ag_fb.png"/></a>
            </div>

        </div>
        <div class="col-xs-12 padding0">

            <div class="col-xs-15">
                <?php if (isset($oDetailAgenda->twitter) && $oDetailAgenda->twitter != "" && isset($oDetailAgenda->diffusion_twitter) && $oDetailAgenda->diffusion_twitter == "1") $twitter_agenda_link = $oDetailAgenda->twitter; else $twitter_agenda_link = "javascript:void(0);"; ?>
                <a href="<?php echo $twitter_agenda_link; ?>"><img
                        src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mobile_ag_twt.png"/></a>
            </div>
            <div class="col-xs-15">
                <a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;pub=xa-4ab3b0412ad55820"
                   target="_blank" title="Partager">
                    <img src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mobile_ag_share.png"/>
                </a>
            </div>
            <div class="col-xs-15">
                <?php
                if (isset($iframe_session_navigation) && $iframe_session_navigation == "1") {
                } else {
                    ?>
                    <a href="<?php echo site_url('front/commercant/recommanderAmi/article/' . $oDetailAgenda->id); ?>"><img
                            src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mobile_ag_recommand.png"/></a>
                <?php } ?>
            </div>
            <div class="col-xs-15">
                <?php if (isset($oDetailAgenda->siteweb) && $oDetailAgenda->siteweb != "") $siteweb_agenda = $oDetailAgenda->siteweb; else $siteweb_agenda = "javascript:void(0);"; ?>
                <?php if (isset($siteweb_agenda) && $siteweb_agenda != '' && $siteweb_agenda != null && $siteweb_agenda != 'http://www.') { ?>
                    <a href="<?php echo $siteweb_agenda; ?>" title="Site web" target="_blank"><img
                            src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mobile_ag_siteweb.png"/></a>
                <?php } else { ?>
                    <a href="javascript:void(0);" title="Site web"><img
                            src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mobile_ag_siteweb.png"/></a>
                <?php } ?>
            </div>
            <div class="col-xs-15">
                <a href="javascript:window.print();" title="Imprimer">
                    <img src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mobile_ag_print.png"/>
                </a>
            </div>
        </div>
    </div>


    <div class="col-xs-12 details_agenda_contener_head_title"
         style="text-align:center; padding-top:15px; margin-top:0px; margin-bottom:10px; height:100px; display:table;">
        <span class="details_agenda_date" style='font-family: "Arial",sans-serif;
        line-height: 16px;'><?php echo $oDetailAgenda->subcateg; ?><br/>
            <?php if (isset($toArticle_datetime) && count($toArticle_datetime) > 0) { ?>
                <?php foreach ($toArticle_datetime as $objArticle_datetime) { ?>
                    <?php
                    if (isset($objArticle_datetime->date_debut) && $objArticle_datetime->date_debut != "0000-00-00" && ($objArticle_datetime->date_debut == $objArticle_datetime->date_fin)) {
                        echo "<br/>Le " . translate_date_to_fr($objArticle_datetime->date_debut);
                        if (isset($objArticle_datetime->heure_debut) && $objArticle_datetime->heure_debut != "0:00") echo " à " . str_replace(":", "h", $objArticle_datetime->heure_debut);
                    } else {
                        if (isset($objArticle_datetime->date_debut) && $objArticle_datetime->date_debut != "0000-00-00") echo "<br/>Du " . translate_date_to_fr($objArticle_datetime->date_debut);
                        if (isset($objArticle_datetime->date_fin) && $objArticle_datetime->date_fin != "0000-00-00") {
                            if (isset($objArticle_datetime->date_debut) && $objArticle_datetime->date_debut != "0000-00-00") echo " au " . translate_date_to_fr($objArticle_datetime->date_fin);
                            else echo " Jusqu'au " . translate_date_to_fr($objArticle_datetime->date_fin);
                        }
                        if (isset($objArticle_datetime->heure_debut) && $objArticle_datetime->heure_debut != "0:00") echo " à " . str_replace(":", "h", $objArticle_datetime->heure_debut);
                    }
                    ?>
                <?php } ?>
            <?php } ?>
        </span><br/>
        <span class="details_agenda_title" style='font-family: "Arial",sans-serif;
        font-weight: 700;
        line-height: 16px; font-size:27px;'><?php echo $oDetailAgenda->nom_manifestation; ?></span><br/>
        <span class="details_agenda_localisation" style='font-family: "Arial",sans-serif; font-weight:bold;
        line-height: 16px;'><?php echo $oDetailAgenda->ville; ?><br/><?php echo $oDetailAgenda->adresse_localisation; ?>
            - <?php echo $oDetailAgenda->codepostal_localisation; ?></span>
        <p class="details_agenda_organiser">
        <span class="details_agenda_organiser_span" style='font-family: "Arial",sans-serif;
        font-weight: 700;
        line-height: 16px;'>Organisateur : </span>
            <?php echo $oDetailAgenda->organisateur; ?>
            <?php if ($oDetailAgenda->telephone != "") echo "<br />T&eacute;l. " . $oDetailAgenda->telephone; ?>
            <?php if ($oDetailAgenda->mobile != "") echo "<br />Mobile. " . $oDetailAgenda->mobile; ?>
            <?php if ($oDetailAgenda->fax != "") echo "<br />Fax. " . $oDetailAgenda->fax; ?>
        </p>
    </div>


</div>


<div
    style="width:100%; height:auto; float:left;background-color: #ffffff; font-family:Arial, Helvetica, sans-serif;font-size: 12px; color:#000000 !important;">


    <div>

        <div style="width:100%; background-color:#FFFFFF;">
            <div style="
                width: 100%;
                background-color:#6E0036;
            <?php if ($group_id_commercant_user == 4) { ?>
                background-image:url(<?php echo GetImagePath("front/"); ?>/mobile2013/wp5dcf1817_06.png);
            <?php } else { ?>
                <?php if (isset($frompagepartner) && $frompagepartner == "1") { ?>
                    background-image:url(<?php echo GetImagePath("front/"); ?>/mobile2013/wp01d5c4e5_06_mobile.png);
                <?php } else { ?>
                    background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/wp01d5c4e5_06_mobile.png);
                <?php } ?>
            <?php } ?>
                background-position:center top; background-repeat:no-repeat;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="50%" valign="top">


                            <?php
                            $this->load->model("mdlcommercant");
                            $oCommercant = $this->mdlcommercant->infoCommercant($oDetailAgenda->IdCommercant);
                            ?>


                            <div style="background-color:#FFFFFF;">

                                <?php


                                if ($group_id_commercant_user != 3) {
                                    ?>

                                    <table width="100%">
                                        <tr style="text-align:center;">
                                            <td><input type="button" value="Tous les articles de ce d&eacute;posant"
                                                       style=" margin-bottom:20px;"
                                                       onClick="javascript:document.location='<?php echo site_url($oCommercant->nom_url . "/article"); ?>'"
                                                       class="btn btn-default"/></td>
                                        </tr>
                                    </table>

                                <?php } ?>


                            </div>

                        </td>

                    </tr>

                </table>

            </div>
        </div>

        <div style="
	background-color: #FFFFFF;
    padding-top: 5px;
    width: 100%;
">
            <div style='
  background-color: #000000;
    color: #ffffff;
    font-family: "Arial",sans-serif;
    font-size: 20px;
    font-style: normal;
    font-variant: normal;
    font-weight: 700;
    line-height: 30px;
    text-decoration: none;
    vertical-align: 0;
    width:100%;
    padding:15px;
    text-align:center;
    '>
                Description
            </div>
        </div>

        <div class="details_agenda_contener_content_description" style="
	background-color: #FFFFFF;
    padding: 15px;font-size: 18.7px; text-align:justify;
">
<span style='font-family: "Arial",sans-serif;
    font-weight: 700;
    line-height: 1.2em;'><?php echo $oDetailAgenda->nom_manifestation; ?></span><br/>
            <p style="text-align:justify; width:100%;"><?php echo $oDetailAgenda->description; ?></p>
        </div>


        <?php
        if ($oDetailAgenda->reservation_enligne != "") {
            $reservation_enligne_agenda = $oDetailAgenda->reservation_enligne;
            ?>

            <div style='
  	background-color: #000000;
    color: #ffffff;
    font-family: "Arial",sans-serif;
    font-size: 20px;
    font-style: normal;
    font-variant: normal;
    font-weight: 700;
    line-height: 30px;
    text-decoration: none;
    vertical-align: 0;
    width:100%;
    padding:15px;
    text-align:center;
    '>
                Horaires & description
            </div>

            <div style="
	background-color: #FFFFFF;
    padding: 10px;
"><?php echo $oDetailAgenda->description_tarif; ?></div>

            <?php if (isset($group_id_commercant_user) && $group_id_commercant_user != '3') { ?>

                <div
                    class="col-xs-12"><?php echo html_entity_decode(htmlentities($oDetailAgenda->conditions_promo)); ?></div>
                <div class="col-xs-12" style="text-align:center;">
                    <a href="<?php echo $reservation_enligne_agenda; ?>"><img
                            src="<?php echo GetImagePath("privicarte/"); ?>/reservation_online.png" alt="agenda"/></a>
                </div>
                <?php
            }
            ?>


        <?php } ?>


        <div style="width:100%;background-color:#FFF; padding-top:5px;">

            <div style='
    background-color: #000000;
    color: #ffffff;
    font-family: "Arial",sans-serif;
    font-size: 20px;
    font-style: normal;
    font-variant: normal;
    font-weight: 700;
    line-height: 30px;
    text-decoration: none;
    vertical-align: 0;
    width:100%;
    padding:15px;
    text-align:center;
    display:table;
    '>
                Localisation de l'&eacute;v&eacute;nement
            </div>


            <div>
                <p style="margin:5px;">
                    <strong><?php echo $oDetailAgenda->nom_localisation; ?> <?php echo $oDetailAgenda->adresse_localisation; ?>
                        , <?php echo $oDetailAgenda->ville; ?> <?php echo $oDetailAgenda->codepostal_localisation; ?></strong>
                </p>
                <p style="text-align:center;">
                    <?php if (isset($oDetailAgenda->ville)) $ville_map = $oDetailAgenda->ville; else $ville_map = ''; ?>
                    <?php if (isset($oDetailAgenda->adresse_localisation)) { ?>
                        <a href="http://maps.google.fr/maps?f=q&amp;source=s_q&amp;hl=fr&amp;geocode=&amp;q=<?php echo $oDetailAgenda->adresse_localisation . ", " . $oDetailAgenda->codepostal_localisation . " &nbsp;" . $ville_map; ?>&amp;aq=0&amp;ie=UTF8&amp;hq=&amp;hnear=<?php echo $oDetailAgenda->adresse_localisation . ", " . $oDetailAgenda->codepostal_localisation . " &nbsp;" . $ville_map; ?>&amp;t=m&amp;vpsrc=0"
                           target="_blank"><img
                                src="<?php echo GetImagePath("privicarte/"); ?>/img_localisation_detailagenda_sdt.png"
                                alt="localisation" style="width:100%;"></a>
                    <?php } ?>
                </p>
            </div>


        </div>


    </div>


</div>
