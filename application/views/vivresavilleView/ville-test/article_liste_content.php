<style type="text/css">
    .oAgenda_subcateg {
        background-color: transparent;
        color: #000000;
        font-family: "Arial", sans-serif;
        font-size: 16px;
        font-style: normal;
        font-variant: normal;
        font-weight: 700;
        line-height: 16px;
        margin: 0 0 5px 0 !important;
        padding: 0 !important;
        text-decoration: none;
        vertical-align: 0;
    }

    .oAgenda_nom_manifestation {
        background-color: transparent;
        color: #da1893;
        font-family: "Arial", sans-serif;
        font-size: 18px;
        font-style: normal;
        font-variant: normal;
        font-weight: 700;
        line-height: 20px;
        margin: 0 0 5px 0 !important;
        padding: 0 !important;
        text-decoration: none;
        vertical-align: 0;
    }

    .oAgenda_date_debut {
        background-color: transparent;
        color: #000000;
        font-family: "Arial", sans-serif;
        font-size: 14px;
        font-style: normal;
        font-variant: normal;
        font-weight: 700;
        line-height: 20px;
        margin: 0 0 5px 0 !important;
        padding: 0 !important;
        text-decoration: none;
        vertical-align: 0;
    }

    .oAgenda_nom_localisation {
        background-color: transparent;
        color: #000000;
        font-family: "Arial", sans-serif;
        font-size: 14px;
        font-style: normal;
        font-variant: normal;
        font-weight: normal;
        line-height: 20px;
        margin: 0 0 5px 0 !important;
        padding: 0 !important;
        text-decoration: none;
        vertical-align: 0;
    }

    .oAgenda_ville {
        background-color: transparent;
        color: #000000;
        font-family: "Arial", sans-serif;
        font-size: 14px;
        font-style: normal;
        font-variant: normal;
        font-weight: normal;
        line-height: 20px;
        margin: 0 0 5px 0 !important;
        padding: 0 !important;
        text-decoration: none;
        vertical-align: 0;
    }
</style>

<?php if (isset($_GET['content_only_list']) && $_GET['content_only_list'] == '1') {
} else { ?>
<div id="id_mainbody_main" class="col-md-3 padding0" style="display:table;">
    <?php } ?>

    <?php $ij = 0; ?>
    <div class="col-xs-12 padding0">
        <?php foreach ($toAgenda as $oAgenda){ ?>

        <?php
        $photoCommercant_path = "application/resources/front/photoCommercant/imagesbank/" . $oAgenda->IdUsers_ionauth . "/";
        $photoCommercant_path_old = "application/resources/front/images/article/photoCommercant/";
        ?>

        <?php if ($ij == 0 || $ij == 3 || $ij == 6 || $ij == 9 || $ij == 12 || $ij == 15 || $ij == 18 || $ij == 21 || $ij == 24 || $ij == 27 || $ij == 30 || $ij == 33 || $ij == 36) { ?>
    </div>
<div class="col-xs-12 padding0">
<?php } ?>

<div class="col-sm-4 paddingleft0" style="float:left;left:10px;display:table;width:400px;"><!--height:750px; max-height:550px;-->
    <div class="col-lg-12 padding0" style="background-color:#E5E5E5; margin-top:15px; display:table;">

        <div class="col-sm-12 padding0" style="height: 240px; overflow: hidden;">
            <a href="<?php echo site_url("article/details/" . $oAgenda->id); ?>">
                <?php
                $image_home_vignette = "";
                if (isset($oAgenda->photo1) && $oAgenda->photo1 != "" && is_file($photoCommercant_path . $oAgenda->photo1) == true) {
                    $image_home_vignette = $oAgenda->photo1;
                } else if ($image_home_vignette == "" && isset($oAgenda->photo2) && $oAgenda->photo2 != "" && is_file($photoCommercant_path . $oAgenda->photo2) == true) {
                    $image_home_vignette = $oAgenda->photo2;
                } else if ($image_home_vignette == "" && isset($oAgenda->photo3) && $oAgenda->photo3 != "" && is_file($photoCommercant_path . $oAgenda->photo3) == true) {
                    $image_home_vignette = $oAgenda->photo3;
                } else if ($image_home_vignette == "" && isset($oAgenda->photo4) && $oAgenda->photo4 != "" && is_file($photoCommercant_path . $oAgenda->photo4) == true) {
                    $image_home_vignette = $oAgenda->photo4;
                } else if ($image_home_vignette == "" && isset($oAgenda->photo5) && $oAgenda->photo5 != "" && is_file($photoCommercant_path . $oAgenda->photo5) == true) {
                    $image_home_vignette = $oAgenda->photo5;
                } else if ($image_home_vignette == "" && isset($oAgenda->photo1) && $oAgenda->photo1 != "" && is_file($photoCommercant_path_old . $oAgenda->photo1) == true) {
                    $image_home_vignette = $oAgenda->photo1;
                } else if ($image_home_vignette == "" && isset($oAgenda->photo2) && $oAgenda->photo2 != "" && is_file($photoCommercant_path_old . $oAgenda->photo2) == true) {
                    $image_home_vignette = $oAgenda->photo2;
                } else if ($image_home_vignette == "" && isset($oAgenda->photo3) && $oAgenda->photo3 != "" && is_file($photoCommercant_path_old . $oAgenda->photo3) == true) {
                    $image_home_vignette = $oAgenda->photo3;
                } else if ($image_home_vignette == "" && isset($oAgenda->photo4) && $oAgenda->photo4 != "" && is_file($photoCommercant_path_old . $oAgenda->photo4) == true) {
                    $image_home_vignette = $oAgenda->photo4;
                } else if ($image_home_vignette == "" && isset($oAgenda->photo5) && $oAgenda->photo5 != "" && is_file($photoCommercant_path_old . $oAgenda->photo5) == true) {
                    $image_home_vignette = $oAgenda->photo5;
                }

                ////$this->firephp->log($image_home_vignette, 'image_home_vignette');

                //showing category img if all image of agenda is null
                $this->load->model("mdl_categories_agenda");
                $toCateg_for_agenda = $this->mdl_categories_agenda->getById($oAgenda->article_categid);
                if ($image_home_vignette == "" && isset($toCateg_for_agenda->images) && $toCateg_for_agenda->images != "" && is_file("application/resources/front/images/agenda/category/" . $toCateg_for_agenda->images) == true) {
                    echo '<img src="' . GetImagePath("front/") . '/agenda/category/' . $toCateg_for_agenda->images . '" width="100%" style="margin-top: -75%;"/>';
                } else {

                    if ($image_home_vignette != "") {
                        if (isset($image_home_vignette) && $image_home_vignette != "" && is_file($photoCommercant_path . $image_home_vignette) == true)
                            echo '<img src="' . base_url() . $photoCommercant_path . $image_home_vignette . '" width="100%" height="240"/>';
                        else echo '<img src="' . base_url() . $photoCommercant_path_old . $image_home_vignette . '" width="100%" height="240"/>';

                    } else {
                        $image_home_vignette_to_show = GetImagePath("front/") . "/wp71b211d2_06.png";
                        echo '<img src="' . $image_home_vignette_to_show . '" width="100%" height="240"/>';
                    }

                }
                ?>

            </a>
            <div
                style="position:absolute; bottom:0; width:100%; height:40px; background:rgba(0, 0, 0, 0.5); color:#FFFFFF; font-weight: normal; text-transform:uppercase; text-align:center; line-height:40px;"><?php echo $oAgenda->category; ?></div>
        </div>

        <div class="col-sm-12 padding0">
            <div class="col-lg-12 padding0" style="margin-top: 15px">
                <div class="col-sm-12"
                     style="text-align: center; height:165px; float:left; display:table; min-height:165px;">
                    <!--min-height:120px;-->
                    <p class="oAgenda_subcateg"><?php //echo $oAgenda->category ; ?><?php echo $oAgenda->subcateg; ?></p>
                    <p class="oAgenda_nom_manifestation"> <?php echo $oAgenda->nom_manifestation; ?></p>
                    <p class="oAgenda_date_debut">
                        <?php
                        if ($oAgenda->datetime_debut == $oAgenda->datetime_fin) {
                            if (isset($oAgenda->datetime_debut) && $oAgenda->datetime_debut != "0000-00-00") echo "Le " . translate_date_to_fr($oAgenda->datetime_debut);
                            if (isset($oAgenda->datetime_heure_debut) && $oAgenda->datetime_heure_debut != "0:00") echo " à " . str_replace(":", "h", $oAgenda->datetime_heure_debut);
                        }
                        else {
                            if (isset($oAgenda->datetime_debut) && $oAgenda->datetime_debut != "0000-00-00") echo "Du " . translate_date_to_fr($oAgenda->datetime_debut);
                            if (isset($oAgenda->datetime_fin) && $oAgenda->datetime_fin != "0000-00-00") {
                                if (isset($oAgenda->datetime_debut) && $oAgenda->datetime_debut != "0000-00-00") echo "<br/>au " . translate_date_to_fr($oAgenda->datetime_fin);
                                else echo " Jusqu'au " . translate_date_to_fr($oAgenda->datetime_fin);
                            }
                            if (isset($oAgenda->datetime_heure_debut) && $oAgenda->datetime_heure_debut != "0:00") echo " à " . str_replace(":", "h", $oAgenda->datetime_heure_debut);
                        }
                        ?>
                    </p>

                    <?php
                    if (isset($oAgenda->location_id) && $oAgenda->location_id != "0") {
                        $obj_location_article_details = $this->mdl_localisation->getById($oAgenda->location_id);
                        if (isset($obj_location_article_details) && is_object($obj_location_article_details)) {
                            ?>
                            <p class="oAgenda_nom_localisation"><?php echo $obj_location_article_details->location; ?></p>
                            <?php
                            $obj_ville_location_details = $this->mdlville->getVilleById($obj_location_article_details->location_villeid);
                            if (isset($obj_ville_location_details->Nom) && is_object($obj_ville_location_details)) {
                                ?>
                                <p class="oAgenda_ville"><?php echo $obj_ville_location_details->Nom; ?></p>
                                <?php
                            }
                        }
                    }
                    ?>



                </div>
                <div class="col-sm-12 padding0" style="text-align: right;">
                    <div>
                        <?php
                        $bonPlanParCommercant = $this->mdlbonplan->bonPlanParCommercant($oAgenda->IdCommercant);
                        //var_dump($bonPlanParCommercant);
                        $listeAnnonceParCommercant = $this->mdlannonce->listeAnnonceParCommercant($oAgenda->IdCommercant);
                        //var_dump($listeAnnonceParCommercant);
                        $commercant_url_home = $this->Commercant->GetById($oAgenda->IdCommercant)->nom_url;
                        ?>

                        <!--<?php if (count($bonPlanParCommercant) > 0) { ?>
                    	<a href="<?php echo site_url($commercant_url_home); ?>/notre_bonplan" title="Like"><img src="<?php echo GetImagePath("privicarte/"); ?>/ico_like.png" alt="like" style="padding:5px;"/></a>
                    <?php } ?>
                    <?php if (count($listeAnnonceParCommercant) > 0) { ?>
                    	<a href="<?php echo site_url($commercant_url_home); ?>/annonces" title="Euro"><img src="<?php echo GetImagePath("privicarte/"); ?>/ico_euro.png" alt="euro" style="padding:5px;"/></a>
                    <?php } ?>-->


                        <a href="<?php echo site_url("article/details/" . $oAgenda->id); ?>" title="D&eacute;tails"><img
                                src="<?php echo GetImagePath("privicarte/"); ?>/plus.png" alt="infos"
                                style="padding:5px;"/></a>


                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

<?php if ($ij == 2 || $ij == 5 || $ij == 8 || $ij == 11 || $ij == 14 || $ij == 17 || $ij == 20 || $ij == 23 || $ij == 26 || $ij == 29 || $ij == 32 || $ij == 35 || $ij == 38) { ?>
</div>
    <div class="col-xs-12 padding0">
        <?php } ?>

        <?php $ij = $ij + 1; ?>
        <?php } ?>
    </div>
    <?php if (isset($_GET['content_only_list']) && $_GET['content_only_list'] == '1') {
    } else { ?>
</div>


<?php
$data['empty'] = null;
if (strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')) {
    $this->load->view("sortez/article_liste_content_listing_ipad.php", $data);
} else {//sdffds
    $this->load->view("sortez/article_liste_content_listing.php", $data);
}
?>

<?php } ?>

