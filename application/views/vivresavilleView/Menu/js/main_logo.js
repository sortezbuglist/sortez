/**
 * Created by rand on 18/08/2017.
 */
jQuery(document).ready(function () {
    jQuery("#sortez_btn_fb_main").fancybox({
        'autoScale' : false,
        'overlayOpacity'      : 0.8, // Set opacity to 0.8
        'overlayColor'        : "#000000", // Set color to Black
        'padding'         : 5,
        'width'         : 500,
        'height'        : 800,
        'transitionIn'      : 'elastic',
        'transitionOut'     : 'elastic',
        'type'          : 'iframe'
    });
    jQuery("#sortez_btn_contact_main").fancybox({
        'autoScale' : false,
        'overlayOpacity'      : 0.8, // Set opacity to 0.8
        'overlayColor'        : "#000000", // Set color to Black
        'padding'         : 5,
        'width'         : 500,
        'height'        : 500,
        'transitionIn'      : 'elastic',
        'transitionOut'     : 'elastic',
        'type'          : 'iframe'
    });
});