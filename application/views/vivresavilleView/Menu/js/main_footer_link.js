/**
 * Created by rand on 18/08/2017.
 */
$(document).ready(function() {
    $("#IdGooglePrivicarteForm").fancybox({
        'autoScale' : false,
        'overlayOpacity'      : 0.8, // Set opacity to 0.8
        'overlayColor'        : "#000000", // Set color to Black
        'padding'         : 5,
        'width'         : 320,
        'height'        : 215,
        'transitionIn'      : 'elastic',
        'transitionOut'     : 'elastic',
        'type'          : 'iframe'
    });

    $("#IdFacebookPrivicarteForm_share").fancybox({
        'autoScale' : false,
        'overlayOpacity'      : 0.8, // Set opacity to 0.8
        'overlayColor'        : "#000000", // Set color to Black
        'padding'         : 5,
        'width'         : 500,
        'height'        : 800,
        'transitionIn'      : 'elastic',
        'transitionOut'     : 'elastic',
        'type'          : 'iframe'
    });


    $("#IdTwitterPrivicarteForm_share").fancybox({
        'autoScale' : false,
        'overlayOpacity'      : 0.8, // Set opacity to 0.8
        'overlayColor'        : "#000000", // Set color to Black
        'padding'         : 5,
        'width'         : 500,
        'height'        : 800,
        'transitionIn'      : 'elastic',
        'transitionOut'     : 'elastic',
        'type'          : 'iframe'
    });

    $("#IdGoogleplusPrivicarteForm_share").fancybox({
        'autoScale' : false,
        'overlayOpacity'      : 0.8, // Set opacity to 0.8
        'overlayColor'        : "#000000", // Set color to Black
        'padding'         : 5,
        'width'         : 500,
        'height'        : 800,
        'transitionIn'      : 'elastic',
        'transitionOut'     : 'elastic',
        'type'          : 'iframe'
    });

    $("#IdMentionPrivicarteForm").fancybox({
        'autoScale' : false,
        'overlayOpacity'      : 0.8, // Set opacity to 0.8
        'overlayColor'        : "#000000", // Set color to Black
        'padding'         : 5,
        'width'         : 500,
        'height'        : 800,
        'transitionIn'      : 'elastic',
        'transitionOut'     : 'elastic',
        'type'          : 'iframe'
    });


});