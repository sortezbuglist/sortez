<script type="text/javascript">

$(function(){
     
    $("#inputStringVilleHidden_partenaires").change(function(){
            
            var inputStringVilleHidden_partenaires = $("#inputStringVilleHidden_partenaires").val();
           
            check_show_categ_partner(inputStringVilleHidden_partenaires);
            
            $("#span_leftcontener2013_form_partenaires input:checkbox").attr('checked', false);
            $("#frmRecherchePartenaire").submit();
        });

    $("#inputString_zMotCle_submit").click(function(){

        var inputString_zMotCle = $("#inputString_zMotCle").val();
        $("#zMotCle").val(inputString_zMotCle);

        $("#span_leftcontener2013_form_partenaires input:checkbox").attr('checked', false);
        $("#frmRecherchePartenaire").submit();
    });

    $("#inputStringOrderByHidden_partenaires").change(function(){

        var inputStringOrderByHidden_partenaires = $("#inputStringOrderByHidden_partenaires").val();

        $("#inputStringOrderByHidden").val(inputStringOrderByHidden_partenaires);

        $("#frmRecherchePartenaire").submit();
    });

    $("#inputStringAvantageHidden_partenaires").change(function(){

        var inputStringAvantageHidden_partenaires = $("#inputStringAvantageHidden_partenaires").val();

        $("#inputAvantagePartenaire").val(inputStringAvantageHidden_partenaires);

        $("#frmRecherchePartenaire").submit();
    });
   
});


function show_current_categ_subcateg(IdRubrique){

    $(".leftcontener2013content").hide();
    var id_to_show = "#leftcontener2013content_"+IdRubrique;
    $(id_to_show).show();

}

function check_show_categ_partner(inputStringDepartementHidden_partenaires, inputStringVilleHidden_partenaires){
    
    //alert('<?php //echo site_url();?>');
    var base_url_visurba = '<?php echo site_url();?>';
    
    $("#span_leftcontener2013_form_partenaires").html('<center><img src="'+base_url_visurba+'application/resources/front/images/wait.gif" alt="loading...."/></center>');
            
                $.post(
                     base_url_visurba+'front/annuaire/check_category_list/',
                     {
                        inputStringDepartementHidden_partenaires: inputStringDepartementHidden_partenaires,
                        inputStringVilleHidden_partenaires: inputStringVilleHidden_partenaires 
                     }
                     ,
                     function (zReponse)
                     {
                             //alert ("reponse <br/><br/>" + zReponse) ;
                             if (zReponse == "error") { //an other home page exist yet
                                     alert("Un probléme est suvenu, veuillez refaire l'opération !");
                             } else { 
                                     $("#span_leftcontener2013_form_partenaires").html(zReponse);
                                     
                                     
                                        //var O_Dest_xxx = $('.leftcontener2013').height();
                                        var O_Doc_xxxx = $('.maincontener2013').height();
                                        //if (O_Doc_xxx > O_Dest_xxx) {
                                                $('.leftcontener2013').height(O_Doc_xxxx);
                                        //}
                                        //alert(O_Doc_xxxx);


                                        $(".leftcontener2013content").hide();

                                        $.post(
                                            base_url_visurba+'front/annuaire/check_Idcategory_of_subCategory/'
                                            ,
                                            function (zReponse)
                                            {
                                                var article_value = zReponse.split("-");
                                                for(i = 0; i < article_value.length; i++) {
                                                    //var id_to_show = "#leftcontener2013content_"+article_value[i];
                                                    $("#leftcontener2013content_"+article_value[i]).show();
                                                }
                                            }
                                        );

                                     
                             } 
                });
                
                
}


function redirect_home(){
    var base_url_visurba = '<?php echo site_url();?>';
    
    document.location.href = base_url_visurba+'front/annuaire/redirect_home/';
}


function submit_search_partner(){
    
    //setTimeout(alert('test'),5000);
    var allvalue = '';
    $("#span_leftcontener2013_form_partenaires input:checkbox:checked").each(function(){
        allvalue += ','+$(this).val();
    });
    //alert(allvalue);
    $("#inputStringHidden").val(allvalue);
    $("#frmRecherchePartenaire").submit();
}

   
</script>