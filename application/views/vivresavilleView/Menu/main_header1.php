<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" lang="fr">
<head>

    <?php if (!isset($data)) $data['data_init'] = true; ?>

    <title><?php if (isset($zTitle) && $zTitle != "" && $zTitle != NULL) echo $zTitle; else echo "Sortez"; ?></title>


    <!-- Bootstrap core CSS -->
	<link href="<?php echo GetCssPath("bootstrap/") ; ?>/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<?php echo GetCssPath("bootstrap/") ; ?>/bootstrap-theme.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="<?php echo GetJsPath("privicarte/") ; ?>/jquery-1.11.3.min.js"></script>

    <script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    
    <!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="<?php echo base_url()."application/resources/fancybox"; ?>/lib/jquery.mousewheel-3.0.6.pack.js"></script>

	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="<?php echo base_url()."application/resources/fancybox"; ?>/source/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()."application/resources/fancybox"; ?>/source/jquery.fancybox.css?v=2.1.5" media="screen" />



    <script type="text/javascript" src="<?php echo GetJsPath("privicarte/") ; ?>/global.js"></script>
    
	<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	<script type="text/javascript" src="<?php echo GetJsPath("privicarte/") ; ?>/ie-emulation-modes-warning.js"></script>


	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <?php if (isset($currentpage) && $currentpage == "publightbox") {} else $this->load->view("privicarte/includes/global_js", $data); ?>
    
    <?php $this->load->view("privicarte/includes/global_css", $data); ?>
    
    
    <link href="<?php echo GetCssPath("privicarte/") ; ?>/global.css" rel="stylesheet" type="text/css">
    
    <?php 
	//LOCALDATA FILTRE
	$this_session_localdata =& get_instance();
	$this_session_localdata->load->library('session');
	$localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
	$localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
	//LOCALDATA FILTRE
	?>
	
	<?php if ($group_id_commercant_user == 5) { ?>
    <link href="<?php echo GetCssPath("privicarte/") ; ?>/navbar-fixed-top.css" rel="stylesheet" type="text/css">
    
		<?php 
		$base_path_system_rand = str_replace('system/', '', BASEPATH);
		if (isset($oInfoCommercant->background_image) && $oInfoCommercant->background_image != "" && $oInfoCommercant->background_image != NULL && file_exists($base_path_system_rand."/application/resources/front/photoCommercant/images/".$oInfoCommercant->background_image) && $oInfoCommercant->bg_default_image != "1") { ?>
            <style type="text/css">
            body {
            background-image:url("<?php echo base_url()."application/resources/front/photoCommercant/images/".$oInfoCommercant->background_image; ?>") !important;
            background-repeat:no-repeat !important;
            background-attachment:fixed !important;
            background-size: 100% 100% !important;
            }
            </style>
        <?php } ?>
        
    
	<?php } else if((isset($localdata_IdVille) && $localdata_IdVille !="" && $localdata_IdVille !="0" && $localdata_IdVille != NULL && !isset($oInfoCommercant)) || (isset($localdata_IdDepartement) && $localdata_IdDepartement !="" && $localdata_IdDepartement !="0" && $localdata_IdDepartement != NULL && !isset($oInfoCommercant))) { ?>
		
		<style type="text/css">
		<?php if ($localdata_IdVille == "2031") { ?>
			body {background-image:url("<?php echo base_url()."application/resources/sortez/images/bg/bg_cagnes_sur_mer.jpg"; ?>") !important;
            background-repeat:no-repeat !important;
            background-attachment:fixed !important;
            background-size: 100% 100% !important;
			}
		<?php } else if ($localdata_IdVille == "2004") { ?>
			body {background-image:url("<?php echo base_url()."application/resources/sortez/images/bg/bg_villeneuve_loubet.jpg"; ?>") !important;
            background-repeat:no-repeat !important;
            background-attachment:fixed !important;
            background-size: 100% 100% !important;
			}
		<?php } ?>
        </style>
        
	<?php } ?>
    
    
    <?php 
	if ((isset($oInfoCommercant->bg_default_color_container) && $oInfoCommercant->bg_default_color_container == "1" && $group_id_commercant_user == 5) || $group_id_commercant_user == 4) { ?>
		<style type="text/css">
		.bg_gris_225 {
			background-color: #E1E1E1 !important;
		}
		</style>
    <?php } ?>
    
    
    <style type="text/css">
	.link_button {
		background-color: #006699;
		border: 2px solid #003366;
		border-radius: 8px;
		color: #ffffff;
		font-size: 12px;
		padding: 10px 15px;
	}
	<?php if (!isset($oInfoCommercant) || !isset($group_id_commercant_user) || $pagecategory == 'list_favoris' || $pagecategory == 'agenda' || $pagecategory == 'partenaire' || $pagecategory == 'article') {?>
		body { margin-top: 105px;}
	<?php }  ?>
	</style>

    <?php if (isset($pagecategory) && pagecategory == "article") {
        //$this->load->view('css/style_article_list', $data);
    } ?>


    <?php
	$user_agent_browser_mda = $_SERVER['HTTP_USER_AGENT'];
	if (strpos(strtoupper($user_agent_browser_mda), 'FIREFOX') !== false) {
	?>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<?php
    } 
    ?>


    <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/ville-test/wpscripts/wpstyles.css">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/vivresaville/Styles.css">
    <script type="text/javascript"
            src="<?php echo site_url(); ?>assets/ville-test/wpscripts/jquery.wplightbox.js"></script>
    <style>.background {
            background-color: #dc1a95;
            opacity: 0.8;
            filter: alpha(opacity=80);
            color: #fff;
        }

        .butt {
            border-radius: 5px;
            border: none;
        }

        .butt:hover {
            background-color: #dc1a95;
            border-radius: 5px;
            border: none;
            color: #fff;
        }</style>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".mika").hide();
            $(".SOUS-CAT").hide();
            $(".mika2").hide();

            $("#bt1").click(function () {
                $(".SOUS-CAT").toggle();
                $(this).toggleClass("background");
            });

            $("#bt2").click(function () {
                $(".mika").toggle();
                $(this).toggleClass("background");
            });

            $("#bt3").click(function () {
                $(".mika2").toggle();
                $(this).toggleClass("background");
            });
        });
    </script>


</head>
  
  
  
  
  
  
