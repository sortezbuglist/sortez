<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <title>Accueil</title>
    <meta name="generator" content="Serif WebPlus X8 (16,0,4,32)">
    <meta name="viewport" content="width=1250">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url(''); ?>assets/wpscripts/wpstyles.css">
    <style type="text/css">
      @font-face { font-family: 'Futura Md'; src: url('<?php echo site_url(''); ?>assets/wpscripts/wp545f5f8e.ttf'); }
      .P-1 { text-align:justify;line-height:26.7px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:24.0px;vertical-align:0; }
      .P-2 { text-align:justify;margin-left:48.0px;line-height:26.7px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:24.0px;vertical-align:0; }
      .P-3 { text-align:justify;margin-left:48.0px;margin-bottom:13.3px;line-height:26.7px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:24.0px;vertical-align:0; }
      .OBJ-1 { background:transparent url('<?php echo site_url(''); ?>assets/wpimages/wp123ef1da_06.png') no-repeat 0px 0px; }
      .OBJ-2,.OBJ-2:link,.OBJ-2:visited { background-image:url('<?php echo site_url(''); ?>assets/wpimages/wpfa325c8c_06.png');background-repeat:no-repeat;background-position:0px 0px;text-decoration:none;display:block;position:absolute; }
      .OBJ-2:hover { background-position:0px -118px; }
      .OBJ-2:active,a:link.OBJ-2.Activated,a:link.OBJ-2.Down,a:visited.OBJ-2.Activated,a:visited.OBJ-2.Down,.OBJ-2.Activated,.OBJ-2.Down { background-position:0px -59px; }
      .OBJ-2:focus { outline-style:none; }
      button.OBJ-2 { background-color:transparent;border:none 0px;padding:0;display:inline-block;cursor:pointer; }
      button.OBJ-2:disabled { pointer-events:none; }
      .OBJ-2.Inline { display:inline-block;position:relative;line-height:normal; }
      .OBJ-2 span,.OBJ-2:link span,.OBJ-2:visited span { color:#ffffff;font-family:"Futura Md",sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:uppercase;font-style:normal;left:-9px;top:8px;width:665px;height:36px;line-height:36px;font-size:26px;display:block;position:absolute;cursor:pointer; }
      .OBJ-3 { line-height:59px; }
      .OBJ-4,.OBJ-4:link,.OBJ-4:visited { background-image:url('<?php echo site_url(''); ?>assets/wpimages/wp9d0e010c_06.png');background-repeat:no-repeat;background-position:0px 0px;text-decoration:none;display:block;position:absolute; }
      .OBJ-4:hover { background-position:0px -70px; }
      .OBJ-4:active,a:link.OBJ-4.Activated,a:link.OBJ-4.Down,a:visited.OBJ-4.Activated,a:visited.OBJ-4.Down,.OBJ-4.Activated,.OBJ-4.Down { background-position:0px -35px; }
      .OBJ-4.Disabled,a:link.OBJ-4.Disabled,a:visited.OBJ-4.Disabled,a:hover.OBJ-4.Disabled,a:active.OBJ-4.Disabled { background-position:0px -105px; }
      .OBJ-4:focus { outline-style:none; }
      button.OBJ-4 { background-color:transparent;border:none 0px;padding:0;display:inline-block;cursor:pointer; }
      button.OBJ-4:disabled { pointer-events:none; }
      .OBJ-4.Inline { display:inline-block;position:relative;line-height:normal; }
      .OBJ-4 span,.OBJ-4:link span,.OBJ-4:visited span { color:#ffffff;font-family:"Futura Md",sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:uppercase;font-style:normal;left:-19px;top:-2px;width:683px;height:36px;line-height:36px;font-size:26px;display:block;position:absolute;cursor:pointer; }
      .OBJ-5 { line-height:35px; }
      .OBJ-6,.OBJ-6:link,.OBJ-6:visited { background-image:url('<?php echo site_url(''); ?>assets/wpimages/wpcc8a58c2_06.png');background-repeat:no-repeat;background-position:0px 0px;text-decoration:none;display:block;position:absolute; }
      .OBJ-6:hover { background-position:0px -160px; }
      .OBJ-6:active,a:link.OBJ-6.Activated,a:link.OBJ-6.Down,a:visited.OBJ-6.Activated,a:visited.OBJ-6.Down,.OBJ-6.Activated,.OBJ-6.Down { background-position:0px -80px; }
      .OBJ-6.Disabled,a:link.OBJ-6.Disabled,a:visited.OBJ-6.Disabled,a:hover.OBJ-6.Disabled,a:active.OBJ-6.Disabled { background-position:0px -240px; }
      .OBJ-6:focus { outline-style:none; }
      button.OBJ-6 { background-color:transparent;border:none 0px;padding:0;display:inline-block;cursor:pointer; }
      button.OBJ-6:disabled { pointer-events:none; }
      .OBJ-6.Inline { display:inline-block;position:relative;line-height:normal; }
      .OBJ-6 span,.OBJ-6:link span,.OBJ-6:visited span { color:#ffffff;font-family:"Futura Md",sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:uppercase;font-style:normal;left:18px;top:24px;width:313px;height:27px;line-height:27px;font-size:20px;display:block;position:absolute;cursor:pointer; }
      .OBJ-6.Disabled span,a:link.OBJ-6.Disabled span,a:visited.OBJ-6.Disabled span,a:hover.OBJ-6.Disabled span,a:active.OBJ-6.Disabled span { top:22px; }
      .OBJ-7 { line-height:80px; }
      .select{-webkit-border-radius:5px;-moz-border-radius:5px;border-radius: 5px;margin:10px;padding:10px;border:2px solid purple; position:absolute;left:380px;top:175px;width:445px;height:30px;font-style:normal;}
    </style>
    <script type="text/javascript" src="<?php echo site_url(''); ?>assets/wpscripts/jquery.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {
      $("a.ActiveButton").bind({ mousedown:function(){if ( $(this).attr('disabled') === undefined ) $(this).addClass('Activated');}, mouseleave:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}, mouseup:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}});
      });
    </script>
    <link rel="icon" href="favicon.ico" type="image/x-icon" sizes="16x16 32x32 48x48 64x64">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" sizes="16x16 32x32 48x48 64x64">
    <script>$(document).ready(function(){$('#mySelect').change(function(){ myform.submit();});});
</script>


  </head>
  <body style="height:2300px;background:url('<?php echo site_url(''); ?>assets/wpimages/wpe460110f_06.jpg') no-repeat fixed center top / 100% 100% #ffffff;">
    <div id="divMain" style="background:transparent;margin-left:auto;margin-right:auto;position:relative;width:1250px;height:2300px;">
      <!--img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wp607128b5_06.png" style="position:absolute;left:305px;top:179px;width:645px;height:70px;"-->
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wp3740c79d_06.png" style="position:absolute;left:648px;top:108px;width:52px;height:60px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wp0dbeb9c5_06.png" style="position:absolute;left:707px;top:77px;width:14px;height:88px;">
      <img src="<?php echo site_url(''); ?>assets/wpimages/wp56bd5aaf_06.png" alt="" width="89" height="122" style="position:absolute;left:726px;top:43px;width:89px;height:122px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wpf4164a17_06.png" style="position:absolute;left:794px;top:106px;width:51px;height:62px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wp01803494_06.png" style="position:absolute;left:858px;top:153px;width:14px;height:15px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wpfde46596_06.png" style="position:absolute;left:882px;top:62px;width:28px;height:103px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wp6de85fa2_06.png" style="position:absolute;left:920px;top:106px;width:31px;height:60px;">
      <img alt="L’essentiel de la vie communale &amp; commerciale" src="<?php echo site_url(''); ?>assets/wpimages/wp7689c1c4_06.png" style="position:absolute;left:313px;top:267px;width:635px;height:42px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wpc1269fad_06.png" style="position:absolute;left:379px;top:82px;width:14px;height:85px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wp03e63fd8_06.png" style="position:absolute;left:401px;top:112px;width:56px;height:58px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wp99f2d677_06.png" style="position:absolute;left:465px;top:110px;width:33px;height:57px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wp5c8b2561_06.png" style="position:absolute;left:502px;top:110px;width:54px;height:59px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wp3511178e_06.png" style="position:absolute;left:566px;top:122px;width:32px;height:44px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wp5d8eae06_06.png" style="position:absolute;left:602px;top:122px;width:43px;height:44px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wp03e63fd8_06.png" style="position:absolute;left:314px;top:112px;width:56px;height:58px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wp21664050_06.png" style="position:absolute;left:16px;top:331px;width:1220px;height:410px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wp490044f0_06.png" style="position:absolute;left:2px;top:801px;width:1248px;height:1499px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wp8d1aa3ce_06.png" style="position:absolute;left:352px;top:594px;width:648px;height:295px;">
      <div style="position:absolute;left:173px;top:933px;width:919px;height:955px;overflow:hidden;">
        <p class="Corps4 P-1">Avec Vivresaville ! Chaque commune dispose gratuitement d’un site web unique personnalisé, multilingue rassemblant les informations institutionnelles et privées (annuaire complet, articles, agenda, bons, plans, conditions de fidélisation, boutiques en ligne) déposées par les acteurs admistratifs, associatifs et économiques de la cité.</p>
        <p class="Corps4 P-1">Chaque acteur (association, commerçant, artisan, administration) &nbsp;bénéficie d’un kit de démarrage clés en mains et gratuit avec :</p>
        <p class="Corps4 P-2">L’ouverture d’un compte personnel et sécurisé, l’intégration dans des champs préconfigurés de textes, d’images, de liens divers sans faire appel à un webmaster, le référencement des données sur nos annuaires mutualisés</p>
        <p class="Corps4 P-2">La création immédiate d’un site web avec un URL personnalisé et une visibilité sur les ordinateurs, tablettes et mobiles,</p>
        <p class="Corps4 P-2">La présence d’un quota de base pour créer un agenda,des articles et une boutique en ligne.</p>
        <p class="Corps4 P-2"><br></p>
        <p class="Corps4 P-1">Vivresaville est lié au site national du magazine et de la carte Sortez, sur ce dernier, chaque acteur de vivresaville bénéficie du référencement départemental et national de ses données.</p>
        <p class="Corps4 P-1">Une fois inscrit, ils peuvent à tout moment enrichir leur abonnement en souscrivant à des modules optionnels : </p>
        <p class="Corps4 P-2">Augmentation des quotas (articles, événements, annonces). </p>
        <p class="Corps P-3">La créations d’actions promotionnelles avec le module bons plan et fidélité.</p>
        <p class="Corps P-3">La gestion automatisée des transactions et des clients avec la carte Sortez.</p>
        <p class="Corps P-3">L’exportation des données agendas et articles. </p>
        <p class="Corps P-3">L’amélioration graphique de leurs pages et site.</p>
        <p class="Corps P-3">Le référencement de leur site sur les premières pages de GOOGLE).</p>
      </div>
      	<div>
		    <form method="post" action="" name="myform">
    			<select id="mySelect" onchange="myFunction()" name="id_ville_selected" class="select">
                    <option value="0">Choisissez votre ville !</option>
                  <?php if (isset($villes) && count($villes)>0) { ?>
                  <?php foreach ($villes as $ville_item) { ?>
    				<option value="<?php echo $ville_item->id;?>"><?php echo $ville_item->name_ville;?></option>
                      <?php } ?>
                <?php } ?>
    			</select>
  		  </form>
        </div>
      </div>
      <a href="vivresaville.pdf" id="btn_822" class="OBJ-6 ActiveButton OBJ-7" style="position:absolute;left:426px;top:1807px;width:350px;height:80px;">
        <span>Plus&nbsp;d&#39;informations</span>
      </a>

      <div style="position:absolute;left:12px;top:1954px;width:210px;height:190px;">

      </div>
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wpd2604227_06.png" style="position:absolute;left:174px;top:1125px;width:17px;height:16px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wpd2604227_06.png" style="position:absolute;left:174px;top:1218px;width:17px;height:16px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wpd2604227_06.png" style="position:absolute;left:174px;top:1278px;width:17px;height:16px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wpd2604227_06.png" style="position:absolute;left:174px;top:1512px;width:17px;height:16px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wpd2604227_06.png" style="position:absolute;left:174px;top:1550px;width:17px;height:16px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wpd2604227_06.png" style="position:absolute;left:174px;top:1594px;width:17px;height:16px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wpd2604227_06.png" style="position:absolute;left:174px;top:1633px;width:17px;height:16px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wpd2604227_06.png" style="position:absolute;left:174px;top:1670px;width:17px;height:16px;">
      <img alt="" src="<?php echo site_url(''); ?>assets/wpimages/wpd2604227_06.png" style="position:absolute;left:174px;top:1709px;width:17px;height:16px;">
      <div></div>
    </div>
    <script type="text/javascript" src="<?php echo site_url(''); ?>assets/wpscripts/jsMenu.js"></script>
    <script type="text/javascript">
      wpmenustack.setRollovers([['nav_13_B1','nav_13_B1M',{"m_vertical":true}]]);
      wpmenustack.setMenus(['nav_13_B1M'],{"m_minwidth":645,"m_hOffset":0,"m_vOffset":0,"m_vAlignment":1});
    </script>
  </body>
</html>
	

