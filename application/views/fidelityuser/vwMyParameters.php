<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("frontAout2013/includes/header_mini_3", $data); ?>

<?php $this->load->view("fidelity/button_css"); ?>

<script type="text/javascript">
function check_num_card(){
	
	var zErreur = "" ;
	var num_card_verification = $("#num_card_verification").val();
	
	$('#span_check_num_card').html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...."/>');
	
	$.post(
		'<?php echo site_url("front/user_fidelity/check_num_card");?>',
		{ 
		num_card_verification: num_card_verification
		},
		function (zReponse2)
		{
			if (zReponse2 == "1") {
				$('#span_check_num_card').html('<span style="color:#090">Votre carte est maintenant lié à votre compte !</span>');
			} else if (zReponse2 == "2") {
				$('#span_check_num_card').html('<span style="color:#FF0000">Votre carte est déjà lié à votre compte!</span>');
			} else if (zReponse2 == "3") {
				$('#span_check_num_card').html('<span style="color:#FF0000">Votre carte n\a pas encore été activée !</span>');
			} else {
				$('#span_check_num_card').html('<span style="color:#FF0000">Ce numéro de carte est invalide !</span>');
			}
	   });
	
}
</script>


<div style="text-align:center;">

<!--<div style="margin-top:20px; margin-bottom:20px; background-color:#000;"><img src="<?php echo GetImagePath("front/"); ?>/fidelity/img_la_carte.png" width="312" height="97" alt="lacarte" /></div>-->



<div style="text-align:center;">

<form action="" method="post" name="frmverificationcard" id="frmverificationcard">
<table style="text-align:center;" cellpadding="10" cellspacing="10" align="center">
<tr>
    <td class="tdjaune"><a href="<?php echo site_url('front/user_fidelity'); ?>" class="link_btn_jaune">Retour menu</a></td>
  </tr>
  <tr>
  	<td><h2>Lier la carte physique à votre compte</h2></td>
  </tr>
  <tr>
  	<td><img src="<?php echo GetImagePath("front/"); ?>/fidelity/save_card_proxi.png" alt="save_card" /></td>
  </tr>
<tr>
  	<td>
    <input type="text" name="num_card_verification" id="num_card_verification" style="width:300px;"/>
      </td>
  </tr>
  <tr>
    <td class="tdjaune"><a href="javascript:void(0);" name="submit_verification" id="submit_verification" class="link_btn_jaune" onclick="javascript:check_num_card();">Enregistrer</a></td>
  </tr>
  <tr>
  	<td><span id="span_check_num_card"></span></td>
  </tr>
</table>
</form>
</div>

</div>


<?php $this->load->view("frontAout2013/includes/footer_mini_2"); ?>