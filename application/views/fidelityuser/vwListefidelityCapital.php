<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("frontAout2013/includes/header_mini_2", $data); ?>

<?php $this->load->view("fidelity/button_css"); ?>


<div style="text-align:center;">



<script type="text/javascript">
function delete_card(card_id) {
	if (confirm("Voulez-vous supprimer l'élément choisi ?")) {
		document.location = "admin/fidelitydelete/"+card_id;
	}
}
</script>

<p><img src="<?php echo GetImagePath("front/"); ?>/fidelity/img_la_carte.png" width="312" height="97" alt="lacarte" /></p>

<h2><?php echo $pagetitle; ?></h2>

<p><input name="cardadd" type="button" value="Retour menu" onclick="javascript:document.location='<?php echo site_url("front/user_fidelity"); ?>';"/></p>

<table width="100%" border="1" cellpadding="0" cellspacing="0">
  <tr>
    <th>Commercant</th>
    <th>Condition de fidélisation</th>
    <th>Date</th>
    <th>Montant ajouté</th>
    <th>Cumul achats</th>
    <th>Objectif achats</th>
  </tr>
  <?php if (count($oColCard_event_list)>0){?>
     <?php foreach($oColCard_event_list as $colCard_event_list) { ?>
      <tr>
        <td>
		<?php 
		$this->load->model("Commercant");
		$oCommercant = $this->Commercant->GetById($colCard_event_list->id_commercant);
		if (isset($oCommercant) && is_object($oCommercant)) echo $oCommercant->NomSociete;
		?>
        </td>
        
        <td>
		<?php 
		$this->load->model("mdl_card_capital");
		$oCard_tampon_Commercant = $this->mdl_card_capital->getByIdCommercant($colCard_event_list->id_commercant);
		if (isset($oCard_tampon_Commercant) && is_object($oCard_tampon_Commercant)) echo $oCard_tampon_Commercant->description;
		?>
        </td>
        
        <td>
		<?php 
		//echo $colCard_event_list->date_use;
		$date_use_tampon = explode(" ", $colCard_event_list->date_use);
		echo convertDateWithSlashes($date_use_tampon[0])." ".$date_use_tampon[1];
		?>
        </td>
        
        <td><?php echo $colCard_event_list->capital_value_added; ?></td>
        <td><?php echo $colCard_event_list->cumul_capital; ?></td>
        <td><?php echo $colCard_event_list->objectif_capital; ?></td>
        
      </tr>
      <?php } ?>
    <?php } else { ?><strong style="color:#339900;">Pas de carte enregistrée !</strong><?php } ?>
</table>


<?php if (isset($links_pagination) && $links_pagination!="") {?>
<style type="text/css">
#view_pagination_ci strong {
	color:#FF0000;
	font-weight:bold;
}
</style>
<div id="view_pagination_ci" style="text-align:right; font-size:14px; height:20px; vertical-align:central; padding-top:5px; padding-right:20px;">
<span style="font-size:12px;">Pages : </span><?php echo $links_pagination; ?>
</div>
<?php }?>



</div>


<?php $this->load->view("frontAout2013/includes/footer_mini_2"); ?>