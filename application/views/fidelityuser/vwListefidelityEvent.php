<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("frontAout2013/includes/header_mini_3", $data); ?>

<?php $this->load->view("fidelity/button_css"); ?>


<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra']})
				.tablesorterPager({container: $("#pager")});
		});
</script>


<div style="text-align:center;">

<!--<div style="margin-top:20px; margin-bottom:20px; background-color:#000;"><img src="<?php echo GetImagePath("front/"); ?>/fidelity/img_la_carte.png" width="312" height="97" alt="lacarte" /></div>-->


<table width="280" border="0" cellpadding="5" cellspacing="10" style="text-align:center" align="center">
  <tr>
    <td class="tdjaune"><a href="<?php echo site_url('front/user_fidelity'); ?>" class="link_btn_jaune">Retour</a></td>
  </tr>
</table>

<div style="margin-top:30px; margin-bottom:30px; font-size:18px; font-weight:bold; text-height:100px;">Toutes mes actions "fidélit&eacute;" (classement par partenaire)</div>

<div style="text-align:center;">
<table width="100%" border="1" cellpadding="0" cellspacing="0" class="tablesorter" align="center" style="margin-left:3.5px;">
<thead>
  <tr>
  	<th>Derni&eacute;re date</th>
    <th>Partenaires</th>
    <th>Conditions</th>
    <th>Type</th>
    <th>Date limite</th>
    <th>Cumuls</th>
    <th>Objectif</th>
    <th>&nbsp;</th>
  </tr>
</thead>
<tbody>
  <?php if (count($TotalRows)>0){?>
     <?php foreach($TotalRows as $colCard_event_list) { ?>
      <tr>
          <td>
            <?php 
            //echo $colCard_event_list->date_use;
            $date_use_tampon = explode(" ", $colCard_event_list->date_use);
            echo convertDateWithSlashes($date_use_tampon[0])."<br/>".$date_use_tampon[1];
            ?>
        </td>
        <td>
		<?php 
		$this->load->model("Commercant");
		$oCommercant = $this->Commercant->GetById($colCard_event_list->id_commercant);
		if (isset($oCommercant) && is_object($oCommercant)) echo $oCommercant->NomSociete;
		?>
        </td>
        <td>
		<?php 
		$this->load->model("mdlbonplan");
		$oBonplan = $this->mdlbonplan->getById($colCard_event_list->id_bonplan);
		if (isset($oBonplan) && is_object($oBonplan)) echo $oBonplan->bonplan_titre;
		?>
        </td>
        <td>
        <?php 
		if (isset($colCard_event_list->tampon_value_added) && $colCard_event_list->tampon_value_added!="") echo "Tampon"; else if (isset($colCard_event_list->capital_value_added) && $colCard_event_list->capital_value_added!="") echo "Capital";
		?>
        </td>
        <td>
        <?php 
		if (isset($colCard_event_list->tampon_value_added) && $colCard_event_list->tampon_value_added!="") echo convertDateWithSlashes($colCard_event_list->datefin_tampon); 
		else if (isset($colCard_event_list->capital_value_added) && $colCard_event_list->capital_value_added!="") echo convertDateWithSlashes($colCard_event_list->datefin_capital);
		?>
        </td>
        <td>
        <?php 
		if (isset($colCard_event_list->cumul_tampon) && $colCard_event_list->cumul_tampon!="") echo $colCard_event_list->cumul_tampon; else if (isset($colCard_event_list->cumul_capital) && $colCard_event_list->cumul_capital!="") echo $colCard_event_list->cumul_capital;
		?>
        </td>
        <td>
        <?php 
		if (isset($colCard_event_list->objectif_tampon) && $colCard_event_list->objectif_tampon!="") echo $colCard_event_list->objectif_tampon; else if (isset($colCard_event_list->objectif_capital) && $colCard_event_list->objectif_capital!="") echo $colCard_event_list->objectif_capital;
		?>
        </td>
        <td>
          <a href="<?php echo site_url('front/user_fidelity/fidelity_event_details/'.$colCard_event_list->id); ?>">D&eacute;tails</a></td>
      </tr>
      <?php } ?>
    <?php } else { ?><strong style="color:#339900;">Pas d'action enregistrée !</strong><?php } ?>
</tbody>
</table>
</div>

<div id="pager" class="pager">
    <img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
    <img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
    <input type="text" class="pagedisplay"/>
    <img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
    <img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
    <select class="pagesize" style="visibility:hidden">
        <option selected="selected"  value="10">10</option>
        <option value="20">20</option>
        <option value="30">30</option>
        <option  value="40">40</option>
    </select>
</div>


<?php if (isset($links_pagination) && $links_pagination!="") {?>
<!--<style type="text/css">
#view_pagination_ci strong {
	color:#FF0000;
	font-weight:bold;
}
</style>
<div id="view_pagination_ci" style="text-align:right; font-size:14px; height:20px; vertical-align:central; padding-top:5px; padding-right:20px;">
<span style="font-size:12px;">Pages : </span><?php //echo $links_pagination; ?>
</div>-->
<?php }?>



</div>


<?php $this->load->view("frontAout2013/includes/footer_mini_2"); ?>