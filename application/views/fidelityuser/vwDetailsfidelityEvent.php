<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("frontAout2013/includes/header_mini_2", $data); ?>

<?php $this->load->view("fidelity/button_css"); ?>



<div style="text-align:center;">

<div style="margin-top:20px; margin-bottom:20px; background-color:#000;"><img src="<?php echo GetImagePath("front/"); ?>/fidelity/img_la_carte.png" width="312" height="97" alt="lacarte" /></div>


<table width="280" border="0" cellpadding="5" cellspacing="10" style="text-align:center" align="center">
  <tr>
    <td class="tdjaune"><a href="<?php echo site_url('front/user_fidelity/fidelity_event_list'); ?>" class="link_btn_jaune">Retour</a></td>
  </tr>
</table>



<div style="text-align:center;">

<?php if (isset($oFidelityDetails) && is_object($oFidelityDetails)) { ?>

<div style="margin-top:30px; margin-bottom:30px; font-size:18px; font-weight:bold; text-height:100px;">Détails action "fidélit&eacute;" (
<?php 
		if (isset($oFidelityDetails->tampon_value_added) && $oFidelityDetails->tampon_value_added!="") echo "Tampon"; else if (isset($oFidelityDetails->capital_value_added) && $oFidelityDetails->capital_value_added!="") echo "Capital";
		?>
)</div>

<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td><div align="right"><strong>ID :</strong></div></td>
    <td><div align="left"><?php echo $oFidelityDetails->id;?></div></td>
  </tr>
  <tr>
    <td><div align="right"><strong>N° carte : </strong></div></td>
    <td><div align="left">
    <?php 
		$this->load->model("mdl_card");
		$oCardFidelity = $this->mdl_card->getById($oFidelityDetails->id_card);
		if (isset($oCardFidelity) && is_object($oCardFidelity)) echo $oCardFidelity->num_id_card_virtual." - ".$oCardFidelity->num_id_card_physical;
		?>
    </div></td>
  </tr>
  <tr>
    <td><div align="right"><strong>Commercant : </strong></div></td>
    <td><div align="left">
	<?php 
		$this->load->model("Commercant");
		$oCommercant = $this->Commercant->GetById($oFidelityDetails->id_commercant);
		if (isset($oCommercant) && is_object($oCommercant)) echo $oCommercant->NomSociete;
		?>
    </div></td>
  </tr>
  <tr>
    <td><div align="right"><strong>Conditions : </strong></div></td>
    <td><div align="left">
    <?php 
		$this->load->model("mdlbonplan");
		$oBonplan = $this->mdlbonplan->getById($oFidelityDetails->id_bonplan);
		if (isset($oBonplan) && is_object($oBonplan)) echo $oBonplan->bonplan_titre;
		?>
    </div></td>
  </tr>
  <tr>
    <td><div align="right"><strong>Scan de carte : </strong></div></td>
    <td><div align="left"><?php echo $oFidelityDetails->scan_status;?></div></td>
  </tr>
  <tr>
    <td><div align="right"><strong>Valeur ajout&eacute;e : </strong></div></td>
    <td><div align="left">
	<?php 
	if (isset($oFidelityDetails->tampon_value_added) && $oFidelityDetails->tampon_value_added!="") echo $oFidelityDetails->tampon_value_added; else if (isset($oFidelityDetails->capital_value_added) && $oFidelityDetails->capital_value_added!="") echo $oFidelityDetails->capital_value_added;
	?>
    </div></td>
  </tr>
    <tr>
    <td><div align="right"><strong>Date d'utilisation : </strong></div></td>
    <td><div align="left">
    <?php 
	$date_use_tampon = explode(" ", $oFidelityDetails->date_use);
	echo convertDateWithSlashes($date_use_tampon[0])."<br/>".$date_use_tampon[1];
	?>
    </div></td>
  </tr>
    <tr>
    <td><div align="right"><strong>Objectif : </strong></div></td>
    <td><div align="left">
    <?php 
	if (isset($oFidelityDetails->tampon_value_added) && $oFidelityDetails->tampon_value_added!="") echo $oFidelityDetails->objectif_tampon; else if (isset($oFidelityDetails->capital_value_added) && $oFidelityDetails->capital_value_added!="") echo $oFidelityDetails->objectif_capital;
	?>
    </div></td>
  </tr>
    <tr>
    <td><div align="right"><strong>Cumul : </strong></div></td>
    <td><div align="left">
    <?php 
	if (isset($oFidelityDetails->tampon_value_added) && $oFidelityDetails->tampon_value_added!="") echo $oFidelityDetails->cumul_tampon; else if (isset($oFidelityDetails->capital_value_added) && $oFidelityDetails->capital_value_added!="") echo $oFidelityDetails->cumul_capital;
	?>
    </div></td>
  </tr>
    <tr>
    <td><div align="right"><strong>Date limite : </strong></div></td>
    <td><div align="left">
        <?php 
		if (isset($oFidelityDetails->tampon_value_added) && $oFidelityDetails->tampon_value_added!="") echo convertDateWithSlashes($oFidelityDetails->datefin_tampon); 
		else if (isset($oFidelityDetails->capital_value_added) && $oFidelityDetails->capital_value_added!="") echo convertDateWithSlashes($oFidelityDetails->datefin_capital);
		?>
    </div></td>
  </tr>
</table>
<?php } else echo "Erreur : Veuillez reafaire l'op&eacute;ration."; ?>
</div>




</div>


<?php $this->load->view("frontAout2013/includes/footer_mini_2"); ?>