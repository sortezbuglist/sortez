<p>Bonjour,<br/>
    Ceci est une demande de validation d'évènnement de Sortez.org<br/>
    ci-dessous le contenu,
</p>

<p>
    - Le titre de l'évènnement : {mail_article_title}<br/>
    - La date de dépôt : {mail_date_depot}<br/>
    - Le rédacteur : {mail_nom_redacteur} - {mail_contact_redacteur}
</p>

<p>
    Je vous adresse la confirmation de la création de l'évènnement cité en référence.
    <br/>vous pouvez accéder directement à la page administrative correspondante en cliquant sur le lien suivant : <?php echo base_url();?>front/agendas/fiche/{mail_IdCommercant}/{mail_IdArticle}/
    <br/>vous devez vous munir de votre identifiant et mot de passe.
    <br/>je reste à votre disposition pour tous renseignements complémentaires.
</p>

Cordialement,
<br/>le rédacteur
<br/>{mail_nom_redacteur},