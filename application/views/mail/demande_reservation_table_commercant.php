<p>Bonjour,<br/>
    Ceci est une demande de réservation de table de votre client sur Sortez.org<br/>
    ci-dessous le contenu,
</p>

<p>
    - Le Nom du client :".$Nom." ".$prenom."<br/>
    - La date de réservation : ".$date_de_reservation."<br/>
    - La date prevue :  ".$date_res."<br/>
    - L'heure prevue :".$heure_midi."-".$heure_soir."<br/>
    - Téléphone du client;".$tel."<br/>
    - Nombre de personne:".$nbre_adulte." Adulte(s) et ".$nbre_enfant." enfant(s)<br/>
</p>

<p>
    ci-dessous le message du client:<br>
   ".$message_client."
</p>

Cordialement,
<br/>le rédacteur,