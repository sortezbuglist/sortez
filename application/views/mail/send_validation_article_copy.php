<p>Bonjour,<br/>
Ceci est une copie de votre demande de validation d'article Sortez.org<br/>
ci-dessous le contenu,</p>

<p>
- Le titre de l'article : {mail_article_title}<br/>
- La date de d&eacute;p&ocirc;t : {mail_date_depot}<br/>
- Le r&eacute;dacteur : {mail_nom_redacteur} - {mail_contact_redacteur}
</p>

<p>
Je vous adresse la confirmation de la cr&eacute;ation de l'article cit&eacute; en r&eacute;f&eacute;rence.
<br/>vous pouvez acc&eacute;der directement à la page administrative correspondante en cliquant sur le lien suivant : http://sortez.org/front/article/fiche/{mail_IdCommercant}/{mail_IdArticle}/
<br/>vous devez vous munir de votre identifiant et mot de passe.
<br/>je reste à votre disposition pour tous renseignements compl&eacute;mentaires.
</p>

Cordialement,
<br/>le r&eacute;dacteur
<br/>{mail_nom_redacteur},