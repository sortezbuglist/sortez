<?php //$data["zTitle"] = 'Article'; ?>
<?php //$this->load->view("adminAout2013/includes/vwHeader2013", $data); ?>

<?php
$data['empty'] = null;
$this->load->view("sortez/includes/backoffice_pro_css", $data);
?>

<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra'], headers: {7: { sorter: false}, 8: {sorter: false} }})
				.tablesorterPager({container: $("#pager")});
		});
		
	function save_order_partner() {
			$('#spanOrderPartner').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
			$.ajax({
				url: '<?php echo base_url();?>front/utilisateur/save_order_partner/',
				data: { 
					<?php foreach($toListeFestival as $oListeFestival){ ?>
					inputOrderPartner_<?php echo $oListeFestival->id; ?>: $('#inputOrderPartner_<?php echo $oListeFestival->id; ?>').val(), 
					<?php } ?>
					idCommercant: <?php echo $idCommercant; ?>
				},
				dataType: 'html',
				type: 'POST',
				async: true,
				success: function(data){
					$('#spanOrderPartner').html('<img src="<?php echo GetImagePath("front/");?>/btn_new/icon-16-checkin.png" />');
					//window.location.reload();
				}
			});
		}
		
		
</script>



    <div class="col-xs-12 padding0" style="width:100%; text-align:center; padding-bottom: 15px !important;">
        <div class="col-sm-6 padding0 textalignleft">
            <h1>Liste des  Festivals</h1>
        </div>
        <div class="col-sm-6 padding0 textalignright">
            <button class="btn btn-primary" onclick="document.location='<?php echo base_url();?>';">Retour site</button>
            <button class="btn btn-primary" onclick="document.location='<?php echo site_url("front/utilisateur/contenupro") ;?>';">Retour au menu</button>
            <?php if (isset($limit_festival_add) && $limit_festival_add==1) {
				echo "<span style='color: #FF0000;'>";
                echo "Vous avez atteint vos limites pour le festival : ";
                if (isset($objCommercant->limit_festival)) echo $objCommercant->limit_festival;
                echo '</span>';
            } else { ?>
                <button id="btnnew" class="btn btn-success" onclick="document.location='<?php echo site_url("front/festival/fiche/$idCommercant") ;?>';">Ajouter un festival</button>
            <?php }?>
            <?php //echo "/".$limit_article_value."-".$count_toListeArticle_total;?>
        </div>
    </div>





    <div id="divMesAnnonces" class="content" align="center" style="display: table;">
        <form name="frmMesAnnonces" id="frmMesAnnonces" action="" method="POST" enctype="multipart/form-data">
			
            	<div id="container">
					<table cellpadding="1" class="tablesorter">
						<thead>
							<tr>                    
								<th>Titre</th>
								<th>Description</th>
								<th>Date debut</th>
								<th>Date fin</th>
								<th>Date ajout</th>
								<th>A la Une</th>
								<th>Etat</th>
                                <th>Ordre&nbsp;<a href="javascript:void(0);" id="saveOrderPartner" title="Enregistrer" onclick="javascript:save_order_partner();"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/saveOrderPartner.png"></a>&nbsp;<span id="spanOrderPartner"></span></th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody> 
                        <?php if (count($toListeFestival)>0) { ?>
                        	<?php $i_order_partner = 1; ?>
							<?php foreach($toListeFestival as $oListeFestival){ ?>
								<tr>                    
									<td><?php echo $oListeFestival->nom_manifestation ; ?></td>
									<td><?php echo truncate(strip_tags($oListeFestival->description),80," ..."); ?></td>
									<td><?php 
									if (isset($oListeFestival->date_debut) && convertDateWithSlashes($oListeFestival->date_debut)!="00/00/0000")
									echo convert_Sqldate_to_Frenchdate($oListeFestival->date_debut) ; ?></td>
									<td><?php 
									if (isset($oListeFestival->date_fin) && convertDateWithSlashes($oListeFestival->date_fin)!="00/00/0000")
									echo convert_Sqldate_to_Frenchdate($oListeFestival->date_fin) ; ?></td>
									<td><?php 
									if (isset($oListeFestival->date_depot) && convertDateWithSlashes($oListeFestival->date_depot)!="00/00/0000")
									echo convert_Sqldate_to_Frenchdate($oListeFestival->date_depot) ; ?></td>
									<td><?php 
									//echo $oListeArticle->alaune; 
									if ($oListeFestival->alaune != "1") echo  "Non"; else echo "Oui";
									?></td>
									<td><?php 
									//echo $oListeArticle->IsActif; 
									if ($oListeFestival->IsActif == "1") echo  "Activé";
									if ($oListeFestival->IsActif == "0") echo  "A valider";
									if ($oListeFestival->IsActif == "2") echo  "Annulé";
									if ($oListeFestival->IsActif == "3") echo  "Supprimé";
									?></td>
                                    <td><input name="inputOrderPartner_<?php echo $oListeFestival->id;?>" id="inputOrderPartner_<?php echo $oListeFestival->id;?>" type="text" size="3" maxlength="3" value="<?php if (isset($oListeFestival->order_partner) && $oListeFestival->order_partner != "" && $oListeFestival->order_partner != NULL) echo $oListeFestival->order_partner; else echo $i_order_partner; ?>"/></td>
									<td><a href="<?php echo site_url("front/festival/fiche/" . $oListeFestival->IdCommercant . "/" . $oListeFestival->id ) ; ?>"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/update_ico.png"></a></td>
									<td><a href="<?php echo site_url("front/festival/deleteFestival_com/" . $oListeFestival->id . "/" . $oListeFestival->IdCommercant) ; ?>" onclick="if (!confirm('voulez-vous vraiment supprimer cette événnement?')){ return false ; }"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/delete_ico.png"></a></td>
								</tr>
                                <?php if (isset($oListeFestival->order_partner) && $oListeFestival->order_partner != "" && $oListeFestival->order_partner != NULL) {} else $i_order_partner = $i_order_partner + 1; ?>
							<?php } ?>
                        <?php } ?>
						</tbody>
					</table>
					<div id="pager" class="pager" style="text-align:center;">
							<img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
							<img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
							<input type="text" class="pagedisplay"/>
							<img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
							<img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
							<select class="pagesize" style="visibility:hidden">
								<option selected="selected"  value="10">10</option>
								<option value="20">20</option>
								<option value="30">30</option>
								<option  value="40">40</option>
							</select>
					</div>
				</div>
        </form>
    </div>
<?php //$this->load->view("adminAout2013/includes/vwFooter2013"); ?>