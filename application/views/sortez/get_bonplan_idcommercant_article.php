<?php

if (isset($obonplan_IdComemrcant) && count($obonplan_IdComemrcant) > 0) {
    $content_result = '<select id="billeterie_bonplan_id" name="Article[billet_bonplan_id]" class="form-control">';
    $content_result .= '<option value="0">Choisir le bon plan</option>';
    foreach ($obonplan_IdComemrcant as $item) {
        $content_result .= '<option value="' . $item->bonplan_id . '" ';
        if (isset($objArticle->billet_bonplan_id) && $objArticle->billet_bonplan_id != '0' && $objArticle->billet_bonplan_id == $item->bonplan_id) $content_result .= "selected";
        $content_result .= '>' . $item->bonplan_titre . '</option>';
    }
    $content_result .= '</select>';
    echo $content_result;
} else {
    ?>
    <div class="col-xs-12 alert alert-danger">Vous ne disposer pas de bonplan pour le moment !</div>
    <?php
}
?>