<?php //$data["zTitle"] = 'Article'; ?>
<?php //$this->load->view("adminAout2013/includes/vwHeader2013", $data); ?>

<?php
$data['empty'] = null;
$this->load->view("sortez/includes/backoffice_pro_css", $data);
?>

    <script type="text/javascript" charset="utf-8">
        /*$(function() {
                $(".tablesorter")
                    .tablesorter({widthFixed: true, widgets: ['zebra'], headers: {7: { sorter: false}, 8: {sorter: false} }})
                    .tablesorterPager({container: $("#pager")});
            });*/

        function save_order_partner() {
            $('#spanOrderPartner').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');
            $.ajax({
                url: '<?php echo base_url();?>front/utilisateur/save_order_partner/',
                data: {
                    <?php foreach($toListeArticle as $oListeArticle){ ?>
                    inputOrderPartner_<?php echo $oListeArticle->id; ?>: $('#inputOrderPartner_<?php echo $oListeArticle->id; ?>').val(),
                    <?php } ?>
                    idCommercant: <?php echo $idCommercant; ?>
                },
                dataType: 'html',
                type: 'POST',
                async: true,
                success: function(data){
                    $('#spanOrderPartner').html('<img src="<?php echo GetImagePath("front/");?>/btn_new/icon-16-checkin.png" />');
                    //window.location.reload();
                }
            });
        }

      

    </script>

    <div class="col-xs-12 padding0" style="width:100%; text-align:center; padding-bottom: 15px !important;">
        <div class="col-sm-6 padding0 textalignleft">
            <h1>Liste de Mes articles</h1>
        </div>
        <div class="col-sm-6 padding0 textalignright">
            <button class="btn btn-primary" onclick="document.location='<?php echo base_url();?>';">Retour site</button>
            <button class="btn btn-primary" onclick="document.location='<?php echo site_url("front/utilisateur/contenupro") ;?>';">Retour au menu</button>
            <?php if (isset($limit_article_add) && $limit_article_add==1) {
                echo "<span style='color: #FF0000;'>";
                echo "Vous avez atteint vos limites d'article : ";
                if (isset($objCommercant->limit_article)) echo $objCommercant->limit_article;
                echo '</span>';
            } else { ?>
                <button id="btnnew" class="btn btn-success" onclick="document.location='<?php echo site_url("front/articles/fiche/$idCommercant") ;?>';">Ajouter un article</button>
            <?php }?>
            <?php //echo "/".$limit_article_value."-".$count_toListeArticle_total;?>
        </div>
    </div>





    <div id="divMesAnnonces" class="content" align="center" style="display: table;">
        <form name="frmMesAnnonces" id="frmMesAnnonces" action="" method="POST" enctype="multipart/form-data">

            <div id="container">
                <table id="example" cellpadding="1" class="tablesorter">
                    <thead>
                    <tr>
                        <th>Titre</th>
                        <th>Description</th>
                        <th>Date debut</th>
                        <th>Date fin</th>
                        <th>Date ajout</th>
                        <th>A la Une</th>
                        <th>Etat</th>
                        <th>Ordre&nbsp;<a href="javascript:void(0);" id="saveOrderPartner" title="Enregistrer" onclick="javascript:save_order_partner();"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/saveOrderPartner.png"></a>&nbsp;<span id="spanOrderPartner"></span></th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (count($toListeArticle)>0) { ?>
                        <?php $i_order_partner = 1; ?>
                        <?php foreach($toListeArticle as $oListeArticle){ ?>
                            <tr>
                                <td><?php echo $oListeArticle->nom_manifestation ; ?></td>
                                <td><?php echo truncate(strip_tags($oListeArticle->description),80," ..."); ?></td>
                                <td><?php
                                    if (isset($oListeArticle->date_debut) && convertDateWithSlashes($oListeArticle->date_debut)!="00/00/0000")
                                        echo convert_Sqldate_to_Frenchdate($oListeArticle->date_debut) ; ?></td>
                                <td><?php
                                    if (isset($oListeArticle->date_fin) && convertDateWithSlashes($oListeArticle->date_fin)!="00/00/0000")
                                        echo convert_Sqldate_to_Frenchdate($oListeArticle->date_fin) ; ?></td>
                                <td><?php
                                    if (isset($oListeArticle->date_depot) && convertDateWithSlashes($oListeArticle->date_depot)!="00/00/0000")
                                        echo convert_Sqldate_to_Frenchdate($oListeArticle->date_depot) ; ?></td>
                                <td><?php
                                    //echo $oListeArticle->alaune;
                                    if ($oListeArticle->alaune != "1") echo  "Non"; else echo "Oui";
                                    ?></td>
                                <td><?php
                                    //echo $oListeArticle->IsActif;
                                    if ($oListeArticle->IsActif == "1") echo  "Activé";
                                    if ($oListeArticle->IsActif == "0") echo  "A valider";
                                    if ($oListeArticle->IsActif == "2") echo  "Annulé";
                                    if ($oListeArticle->IsActif == "3") echo  "Supprimé";
                                    ?></td>
                                <td><input name="inputOrderPartner_<?php echo $oListeArticle->id;?>" id="inputOrderPartner_<?php echo $oListeArticle->id;?>" type="text" size="3" maxlength="3" value="<?php if (isset($oListeArticle->order_partner) && $oListeArticle->order_partner != "" && $oListeArticle->order_partner != NULL) echo $oListeArticle->order_partner; else echo $i_order_partner; ?>"/></td>
                                <td><a href="<?php echo site_url("front/articles/fiche/" . $oListeArticle->IdCommercant . "/" . $oListeArticle->id ) ; ?>"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/update_ico.png"></a></td>
                                <td><a href="<?php echo site_url("front/articles/deleteArticle_com/" . $oListeArticle->id . "/" . $oListeArticle->IdCommercant) ; ?>" onclick="if (!confirm('voulez-vous vraiment supprimer cette événnement?')){ return false ; }"><img style="border: none;width:15px;" src="<?php echo GetImagePath("privicarte/"); ?>/delete_ico.png"></a></td>
                            </tr>
                            <?php if (isset($oListeArticle->order_partner) && $oListeArticle->order_partner != "" && $oListeArticle->order_partner != NULL) {} else $i_order_partner = $i_order_partner + 1; ?>
                        <?php } ?>
                    <?php } ?>
                    </tbody>
                </table>
<!--                <div id="pager" class="pager" style="text-align:center;">-->
<!--                    <img src="--><?php //echo GetImagePath("front/"); ?><!--/first.png" class="first"/>-->
<!--                    <img src="--><?php //echo GetImagePath("front/"); ?><!--/prev.png" class="prev"/>-->
<!--                    <input type="text" class="pagedisplay"/>-->
<!--                    <img src="--><?php //echo GetImagePath("front/"); ?><!--/next.png" class="next"/>-->
<!--                    <img src="--><?php //echo GetImagePath("front/"); ?><!--/last.png" class="last"/>-->
<!--                    <select class="pagesize" style="visibility:hidden">-->
<!--                        <option selected="selected"  value="300">300</option>-->
<!--                        <option value="20">10</option>-->
<!--                        <option value="20">20</option>-->
<!--                        <option value="30">30</option>-->
<!--                        <option  value="40">40</option>-->
<!--                        <option  value="50">50</option>-->
<!--                        <option  value="60">60</option>-->
<!--                        <option  value="70">70</option>-->
<!--                        <option  value="80">80</option>-->
<!--                        <option  value="90">90</option>-->
<!--                        <option  value="100">100</option>-->
<!--                        <option  value="100">200</option>-->
<!--                        <option  value="300">300</option>-->
<!--                    </select>-->
<!--                </div>-->
                <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
                <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
                <script type="text/javascript">
                    $( document ).ready(function() {
                        $(document).ready(function() {
                            $('#example').DataTable( {
                                "pagingType": "full_numbers"
                            } );
                        } );
                    });
                </script>
            </div>
        </form>
    </div>
<?php //$this->load->view("adminAout2013/includes/vwFooter2013"); ?>