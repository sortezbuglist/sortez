<style type="text/css">
    <!--
    @font-face {
        font-family: futura_medium;
        src: url(<?php echo base_url();?>/application/resources/fonts/futura_medium_bt-webfont.ttf);
    }

    .main_logo_container {
        background-color: #FFFFFF;
        font-family: futura_medium;
    }

    .P-1 {
        background-color: transparent;
        color: #000000;
        font-size: 27px;
        font-style: normal;
        font-variant: normal;
        font-weight: normal;
        line-height: 32px;
        margin-bottom: 6.7px;
        text-align: center;
        vertical-align: 0;
    }

    .Normal22 {
        background-color: transparent;
        color: #000000;
        font-size: 16px;
        font-style: normal;
        font-variant: normal;
        font-weight: normal;
        line-height: 1px;
        margin: 0;
        text-align: left;
        text-indent: 0;
        vertical-align: 0;
    }

    .P-2 {
        background-color: transparent;
        color: #000000;
        font-size: 21px;
        font-style: normal;
        font-variant: normal;
        font-weight: normal;
        line-height: 24px;
        text-align: center;
        vertical-align: 0;
    }

    .P-3 {
        background-color: transparent;
        color: #000000;
        font-size: 16px;
        font-style: normal;
        font-variant: normal;
        font-weight: normal;
        line-height: 24px;
        text-align: center;
        vertical-align: 0;
    }
    #sortez_btn_fb_main img,
    #sortez_btn_contact_main img {
        margin-top: 40px;
    }

    -->
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $("#sortez_btn_fb_main").fancybox({
            'autoScale' : false,
            'overlayOpacity'      : 0.8, // Set opacity to 0.8
            'overlayColor'        : "#000000", // Set color to Black
            'padding'         : 5,
            'width'         : 500,
            'height'        : 800,
            'transitionIn'      : 'elastic',
            'transitionOut'     : 'elastic',
            'type'          : 'iframe'
        });
        $("#sortez_btn_contact_main").fancybox({
            'autoScale' : false,
            'overlayOpacity'      : 0.8, // Set opacity to 0.8
            'overlayColor'        : "#000000", // Set color to Black
            'padding'         : 5,
            'width'         : 500,
            'height'        : 500,
            'transitionIn'      : 'elastic',
            'transitionOut'     : 'elastic',
            'type'          : 'iframe'
        });
    });
</script>


<div class="container main_logo_container padding0 displaytable">
    <div class="col-sm-2" style="text-align: left;">
        <a href="<?php echo site_url('front/professionnels/FacebookPrivicarteForm'); ?>" id="sortez_btn_fb_main"><img
                src="<?php echo GetImagePath("sortez/"); ?>/sortez_btn_fb_main.png" alt="logo" /></a>
    </div>
    <div class="div_logo col-sm-8" style="text-align:center;">
        <a href="<?php echo site_url(); ?>"><img
                src="<?php echo GetImagePath("sortez/"); ?>/logo.png" alt="logo"/></a>
    </div>
    <div class="col-sm-2" style="text-align: right;">
        <a href="<?php echo site_url('contact'); ?>" id="sortez_btn_contact_main"><img
                src="<?php echo GetImagePath("sortez/"); ?>/sortez_btn_contact_main.png" alt="logo" /></a>
    </div>


</div>