<?php if (isset($mssg) && $mssg=="newsletter" && $mssg!="0" && $mssg!=null) { ?>
<style type="text/css">
    .btn_link_rose, .btn_link_rose:hover, .btn_link_rose:focus {
        color: #FFFFFF;
        text-decoration: none;
        background-color: #e553b0;
        margin-top: 10px;
        margin-bottom: 10px;
        padding-top: 10px;
        padding-right: 20px;
        padding-bottom: 10px;
        padding-left: 20px;
        border-radius: 25px;
    }
</style>

<div class="col-xs-12 padding0" style="text-align:center; margin: 30px 0 0;">
    <a href="<?php echo site_url("front/bonplan"); ?>" class="btn_link_rose">Retour &agrave; la liste des
        Bons plans</a>
</div>
<?php } else {} ?>