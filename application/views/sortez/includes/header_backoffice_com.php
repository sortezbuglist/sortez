<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title><?php if (isset($zTitle) && $zTitle != "") echo $zTitle; ?> | Sortez</title>

    <?php
    if (isset($objArticle) AND !empty($objArticle)){
    $data["zTitle"] = 'Sortez Actualité | '.$objArticle->nom_manifestation ?? "Article";
     $data["zDescription"] = strip_tags($objArticle->description) ?? "Article";
    $zDescription = strip_tags($objArticle->description) ?? "Article";
    $data["slide"] = 'wp41514793_05_06.jpg';
        $data["slide"] = 'wp41514793_05_06.jpg';
        $custom_path_file_x = "application/resources/front/photoCommercant/imagesbank/".$objArticle->IdUsers_ionauth."/";
        $custom_path_file ="application/resources/front/photoCommercant/imagesbank/scrapped_image";
         if ($objArticle->photo1 != "" && $objArticle->photo1 != NULL && file_exists($custom_path_file_x.$objArticle->photo1)== true) {
             $data["MetaImage"]=base_url().$custom_path_file_x.$objArticle->photo1 ;
             $zMetaImage =base_url().$custom_path_file_x.$objArticle->photo1 ;
        }elseif ($objArticle->photo1 != "" && $objArticle->photo1 != NULL && file_exists($custom_path_file.$objArticle->photo1)== true){
            $data["MetaImage"] =base_url().$custom_path_file.$objArticle->photo1 ;
            $data["MetaImage"] =base_url().$custom_path_file.$objArticle->photo1 ;
        }
    }
    ?>
    <meta property="fb:app_id" content="324903384595223" />
    <!--<meta property="fb:app_id" content="1292726270756136" />-->
    <meta property="og:locale" content="fr_FR" />
    <meta property="og:title" content="<?php if (isset($zTitle) && $zTitle != "" && $zTitle != NULL) echo $zTitle; else echo "Le Magazine Sortez"; ?>" />
    <meta property="og:description" content="<?php if (isset($zDescription) && $zDescription != "" && $zDescription != NULL) echo $zDescription; else echo "Sortez : L'essentiel des sorties et des loisirs Alpes-Maritimes et Monaco"; ?>" />
    <meta property="og:url" content='<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>' />
    <meta property="og:site_name" content="Le Magazine Sortez : L'essentiel des sorties et des loisirs Alpes-Maritimes et Monaco" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="<?php if (isset($zMetaImage) && $zMetaImage != "" && $zMetaImage != NULL) echo $zMetaImage; else echo "https://www.sortez.org/application/resources/sortez/images/logo.png"; ?>" />

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@Sortez_org" />
    <meta name="twitter:creator" content="@techsortez"/>
    <meta name="twitter:title" content="<?php if (isset($zTitle) && $zTitle != "" && $zTitle != NULL) echo $zTitle; else echo "Le Magazine Sortez"; ?>" />
    <meta name="twitter:description" content="<?php if (isset($zDescription) && $zDescription != "" && $zDescription != NULL) echo $zDescription; else echo "Sortez : L'essentiel des sorties et des loisirs Alpes-Maritimes et Monaco"; ?>" />
    <meta name="twitter:image" content="<?php if (isset($zMetaImage) && $zMetaImage != "" && $zMetaImage != NULL) echo $zMetaImage; else echo "https://www.sortez.org/application/resources/sortez/images/logo.png"; ?>" />

    <?php

    $data['empty'] = null;

    $this->load->view("sortez/includes/backoffice_pro_css", $data);

    ?>



    <?php if (isset($current_page) && $current_page == "subscription_pro") {

        ?>

        <script src="https://code.jquery.com/jquery-1.9.1.js"></script>

        <script src="https://code.jquery.com/jquery-migrate-1.1.0.js"></script>

        <?php

    } else { ?>



        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jspngfix.js"></script>

        <!--<script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jquery-1.4.2.min.js"></script>-->

        <script type="text/javascript" src="<?php echo GetJsPath("privicarte/") ; ?>/jquery-1.11.3.min.js"></script>

        <script src="https://code.jquery.com/jquery-migrate-1.4.1.js"></script>

        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/proximite.js"></script>

        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jquery.ui.datepicker.js"></script>

        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jquery.tablesorter.js"></script>

        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jquery.tablesorter.pager.js"></script>

        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/ajaxfileupload.js"></script>

        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jquery.ui.core.js"></script>


    <script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script> 
        <link rel="stylesheet" media="screen" type="text/css"

              href="<?php echo GetCssPath("front/"); ?>/blue/style.css"/>



        <link rel="stylesheet" media="screen" type="text/css"

              href="<?php echo GetCssPath("front/"); ?>/jquery-ui-1.8.4.custom.css"/>

        <link rel="stylesheet" media="screen" type="text/css"

              href="<?php echo GetCssPath("front/"); ?>/demo_table_jui.css"/>



    <?php } ?>



    <script type="text/javascript" src="https://feather.aviary.com/imaging/v3/editor.js"></script>



    <!-- Latest compiled and minified CSS -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"

          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"

          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">



    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>



    <link type="text/css" href="<?php echo base_url()."application/resources/bootstrap/";?>css/bootstrap-timepicker.min.css"/>

    <script type="text/javascript" src="<?php echo base_url()."application/resources/bootstrap/";?>js/bootstrap-timepicker.min.js"></script>





    <script type="text/javascript">

        //variables globales

        gCONFIG = new Array();

        gCONFIG["BASE_URL"] = "<?php echo base_url(); ?>";

        gCONFIG["SITE_URL"] = "<?php echo site_url(); ?>";

    </script>

    <link href="<?php echo GetCssPath("privicarte/"); ?>/global.css" rel="stylesheet" type="text/css">

    <?php $this->load->view("privicarte/includes/global_css", $data); ?>

</head>



<body>

<div class="container_backoffice_pro">

    <!--<div class="header2013">

        <?php $data["zTitle"] = 'Accueil' ?>

    </div>-->

    <div class="main_container_backoffice_pro" style="padding-bottom:500px;">





        <div class="col-lg-12 padding0" style=" background-color:#ffffff; display:table; width:100%;">

            <div class="col-lg-12 padding0" style="text-align: center; display: none;"><img

                    src="<?php echo base_url(); ?>application/resources/sortez/images/logo.png" alt="logo"></div>

        </div>