<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php if (isset($zTitle) && $zTitle != "") echo $zTitle; ?> | Sortez</title>
    <?php
    $data['empty'] = null;
    $this->load->view("sortez/includes/backoffice_pro_css", $data);
    ?>

    <?php if (isset($current_page) && $current_page == "subscription_pro") { ?>

        <style type="text/css">
            <!--
            body {
                /*background-image: url(<?php echo base_url();?>application/resources/privicarte/images/wpdaf3bfc5_06.jpg) !important;*/
            }

            -->
        </style>

    <?php } else if (isset($current_page) && $current_page == "contenupro") { ?>

        <style type="text/css">
            <!--
            body {
                /*background-image: url(<?php echo base_url();?>application/resources/privicarte/images/wpdaf3bfc5_06.jpg) !important;*/
            }

            -->
        </style>
        <script type="text/javascript" src="<?php echo GetJsPath("privicarte/") ; ?>/jquery-1.11.3.min.js"></script>

        <!-- Add mousewheel plugin (this is optional) -->
        <script type="text/javascript" src="<?php echo base_url()."application/resources/fancybox"; ?>/lib/jquery.mousewheel-3.0.6.pack.js"></script>

        <!-- Add fancyBox main JS and CSS files -->
        <script type="text/javascript" src="<?php echo base_url()."application/resources/fancybox"; ?>/source/jquery.fancybox.js?v=2.1.5"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()."application/resources/fancybox"; ?>/source/jquery.fancybox.css?v=2.1.5" media="screen" />

    <?php } else { ?>

        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jspngfix.js"></script>
        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/proximite.js"></script>
        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jquery.ui.datepicker.js"></script>
        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jquery.tablesorter.js"></script>
        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jquery.tablesorter.pager.js"></script>
        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/ajaxfileupload.js"></script>
        <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/jquery.ui.core.js"></script>

        <link rel="stylesheet" media="screen" type="text/css"
              href="<?php echo GetCssPath("front/"); ?>/blue/style.css"/>

        <link rel="stylesheet" media="screen" type="text/css"
              href="<?php echo GetCssPath("front/"); ?>/jquery-ui-1.8.4.custom.css"/>
        <link rel="stylesheet" media="screen" type="text/css"
              href="<?php echo GetCssPath("front/"); ?>/demo_table_jui.css"/>

    <?php } ?>

    <script type="text/javascript">
        //variables globales
        gCONFIG = new Array();
        gCONFIG["BASE_URL"] = "<?php echo base_url(); ?>";
        gCONFIG["SITE_URL"] = "<?php echo site_url(); ?>";
    </script>
    <link href="<?php echo GetCssPath("privicarte/"); ?>/global.css" rel="stylesheet" type="text/css">
    <?php $this->load->view("privicarte/includes/global_css", $data); ?>


    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

</head>

<body>
<div>
