<style type="text/css">
    <!--
    ul.main_menu_navigation {
        text-transform: uppercase;
        list-style: none;
        height: 55px;
        margin: 0px !important;
        padding-left: 0px;
    }

    .main_menu_navigation li {
        display: inline;
        float: left;
        padding: 19px 15px 14px;
        text-align: center;
        background-image: url("<?php echo GetImagePath("sortez/") ; ?>/main_menu_item_bg.png");
        background-repeat: no-repeat;
        background-position: left center;
        width: 25%;
    }

    .main_menu_navigation li a {
        padding: 0px;
        text-decoration: none;
        color: #000000;
        font-size: 14px;
        font-family: Arial, Helvetica, sans-serif !important;
    }

    .main_menu_navigation li > a {
        padding: 18px 25px 15px !important;
    }

    .main_menu_navigation li > a:hover {
        color: #DA1181;
    }

    .fancybox-inner {
        /*min-height: 800px !important;*/
    }

    .fancybox-image, .fancybox-iframe {
        min-width: 450px !important;
    }

    -->
</style>

<script type="text/javascript">
    $(document).ready(function () {

        //$.noConflict();

        /*$(".fancybox").fancybox({
         autoScale : false,
         overlayOpacity      : 0.8, // Set opacity to 0.8
         overlayColor        : "#000000", // Set color to Black
         padding         : 5,
         height        : 850,
         transitionIn      : 'elastic',
         transitionOut     : 'elastic',
         type          : 'iframe',
         });*/

        $(".fancybox_fonctionnement").fancybox();
        $("#menu_fonctionnement").fancybox({
            autoScale: true,
            autoDimensions: true,
            overlayShow: true,
            centerOnScroll: true,
            overlayOpacity: 0.8, // Set opacity to 0.8
            overlayColor: "#000000", // Set color to Black
            padding: 5,
            width: 500,
            height: 850,
            transitionIn: 'elastic',
            transitionOut: 'elastic',
            type: 'iframe',
        });

        $(".fancybox_mon_compte").fancybox();
        $("#menu_mon_compte").fancybox({
            'autoScale': false,
            'overlayOpacity': 0.8, // Set opacity to 0.8
            'overlayColor': "#000000", // Set color to Black
            'padding': 5,
            'width': 650,
            'height': 850,
            'transitionIn': 'elastic',
            'transitionOut': 'elastic',
            'type': 'iframe',
            'onClosed': function () {
                window.location.reload();
            }
        });

        $(".fancybox_ma_carte").fancybox();
        $("#menu_ma_carte").fancybox({
            'autoScale': false,
            'overlayOpacity': 0.8, // Set opacity to 0.8
            'overlayColor': "#000000", // Set color to Black
            'padding': 5,
            'width': 650,
            'height': 850,
            'transitionIn': 'elastic',
            'transitionOut': 'elastic',
            'type': 'iframe',
            'onClosed': function () {
                window.location.reload();
            }
        });

        $(".fancybox_villes_privicarte").fancybox();
        $("#menu_villes_privicarte").fancybox({
            autoScale: false,
            overlayOpacity: 0.8, // Set opacity to 0.8
            overlayColor: "#000000", // Set color to Black
            padding: 5,
            width: 800,
            height: 800,
            transitionIn: 'elastic',
            transitionOut: 'elastic',
            type: 'iframe',
        });

        $(".fancybox_magazine_sortez").fancybox();
        $("#menu_magazine_sortez").fancybox({
            autoScale: false,
            overlayOpacity: 0.8, // Set opacity to 0.8
            overlayColor: "#000000", // Set color to Black
            padding: 5,
            width: 800,
            height: 800,
            transitionIn: 'elastic',
            transitionOut: 'elastic',
            type: 'iframe',
        });


    });
</script>

<div class="container" style="height:55px;">
    <!--<img src="<?php //echo GetImagePath("privicarte/") ; ?>/test/menu_main_pvc.png">-->

    <?php
    $thisss_ =& get_instance();
    $thisss_->load->library('ion_auth');

    $this_session_localdata =& get_instance();
    $this_session_localdata->load->library('session');
    $publightbox_email = $this_session_localdata->session->userdata('publightbox_email');
    ?>

    <ul class="main_menu_navigation">

        <?php if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] == "sortez.org") { ?>
            <li style="background:none !important;"><a href="/accueil.html">Accueil</a></li>
            <li><a href="/site/annonce-infos.html">informations & inscriptions</a><!-- id="menu_fonctionnement" class="fancybox_fonctionnement fancybox.iframe"-->
            </li>
        <?php } else { ?>
            <li style="background:none !important;"><a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/accueil.html">Accueil</a>
            </li>
            <li><a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/site/annonce-infos.html">informations & inscriptions</a></li><!-- id="menu_fonctionnement"-->
        <?php } ?>
        <li>
            <a href="/auth/">Accès à mon compte</a>
        </li>
        <li><a href="<?php echo site_url("/front/utilisateur/liste_favoris"); ?>">mes favoris</a></li>
    </ul>
</div>