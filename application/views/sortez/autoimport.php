
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script type="text/javascript">

	$(document).ready(function(){
		index = 0
		console.log('importation begin......')
		import_auto(id[index], idV[index], linkrss[index])
	})

	function import_auto(id,idville,source){
		console.log("importation de l'url "+source+" ....")
		get_all(0,id,0,idville,source)
	}

</script>
<body>
<?php
    $CI =& get_instance();
    $CI->load->model("xml_models");
    //var_dump($lien);die;        
    $minMax_flux = $CI->xml_models->getMinMax_flux();
    $totaDatatourisme = count($CI->xml_models->countAllDatatourisme());
    $nbrerss= $CI->xml_models->getmin();

    $iiii = 1;     
?>
    <script type="text/javascript">
    	// lien source xml
		var linkrss = [
			<?php 
				foreach ($allink as $link) {
					echo '
						"'.$link->source.'",
					';
				}
			?>
		]

		// id du lien
		var id = [
			<?php 
				foreach ($allink as $link) {
					echo '
						"'.$link->id.'",
					';
				}
			?>
		]

		//id ville
		var idV = [
			<?php 
				foreach ($allink as $link) {
					echo '
						"'.$link->IdVille.'",
					';
				}
			?>
		]

        function get_all(start,ids,cache=0,idville,source){
            jQuery.ajax({
                type: "POST",
                url: "<?php echo site_url("sortez_pro/Auto_import_Rss/count_data/"); ?>"+ids,
                dataType: "json",
                success: function (data) {
                    if (data != 'ko'){
                        var end=parseInt(data)-1;
                        var link=source;
                        var IdVille=idville;
                        if (end > start){
                            jQuery.ajax({
                                type: "POST",
                                url: "<?php echo site_url("sortez_pro/Auto_import_Rss/savetorsslist"); ?>",
                                dataType: "json",
                                data:"id="+start +"&link="+link+"&IdVille="+IdVille+"&linkid="+ids+"&cache="+cache,
                                success: function (data) {
                                    start +=1;
                                    cache=1;
                                        get_all(start,ids,cache,idville,source);
                                },
                                error: function (data) {
                                    console.log('error');
                                    start +=1;
                                    cache=1;
                                        get_all(start,ids,cache,idville,source);
                                }
                            });
                        }else{
                            
                            var toFirstIdDatatourisme = parseInt(<?php echo $minMax_flux->min_id;?>);
                            var toLastIdDatatourisme = parseInt(<?php echo $minMax_flux->max_id;?>);
                            // var lien = '<?php echo $link->source; ?>'
                            var lien  = source
                            import_datatourisme_func(toFirstIdDatatourisme, toLastIdDatatourisme,lien)
                            console.log("Importation de l'url "+source+" terminé")
                            console.log("Attente de préparation du prochain lien.....")
                            index++
                            if(index < linkrss.length){
                                setTimeout(function(){
                                    import_auto(id[index], idV[index], linkrss[index])
                                }, 15000)
                            }else{
                                console.log('Importation terminé')
                                alert('Importation de tous les données terminée')
                            }
                        }
                    }
                },
                error: function (data) {
                    console.log("L'importation a rencontré une erreur, l'importation du dernier lien va se refaire'")
                    import_auto(id[index], idV[index], linkrss[index])
                }
            });
        }

        function import_datatourisme_func(firstId, lastId,lien) {
            jQuery.ajax({
                type: "POST",
                url: "<?php echo site_url("sortez_pro/Auto_import_Rss/import_datatourisme_func"); ?>",
                data: 'toFirstIdDatatourisme=' + firstId + '&toLastIdDatatourisme=' + lastId  + '&lienrss=' + lien,
                dataType: "json",
                success: function (data) {
                    firstId += 1;
                    if (firstId <= lastId) {
                        import_datatourisme_func(firstId, lastId,lien);
                    }
                },
                error: function (data) {
                    firstId += 1;
                    if (firstId <= lastId) {
                        import_datatourisme_func(firstId, lastId,lien);
                    }
                }
            });
        }

    </script>
</body>