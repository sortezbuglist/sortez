



    function limite_title(textarea, max) {

        if (textarea.value.length >= max) {

            textarea.value = textarea.value.substring(0, max);

        }

        var reste = max - textarea.value.length;

        var affichage_reste = reste + ' caract&egrave;res restants';

        document.getElementById('max_desc_title').innerHTML = affichage_reste;

    }

function listeSousRubrique() {



    jQuery('#trReponseRub').html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...." id="img_loaging_agenda_x"/>');



    var irubId = jQuery('#IdCategory').val();

    jQuery.get(

        '<?php echo site_url("front/festival/GetallSubcateg_by_categ"); ?>' + '/' + irubId,

        function (zReponse) {

            // alert (zReponse) ;

            jQuery('#trReponseRub').html(zReponse);





        });

    //alert(irubId);

}

function getCP() {

    var ivilleId = jQuery('#IdVille').val();

    jQuery.get(

        '<?php echo site_url("front/festival/getPostalCode_localisation"); ?>' + '/' + ivilleId,

        function (zReponse) {

            jQuery('#codepostal').val(zReponse);

        });

}



function getCP_localisation() {

    var ivilleId = jQuery('#IdVille_localisation').val();

    jQuery.get(

        '<?php echo site_url("front/festival/getPostalCode_localisation"); ?>' + '/' + ivilleId,

        function (zReponse) {

            jQuery('#codepostal_localisation').val(zReponse);

        });

}



function deleteFile(_IdFestival, _FileName) {

    jQuery.ajax({

        url: '<?php echo base_url();?>front/festival/delete_files/' + _IdFestivale + '/' + _FileName,

        dataType: 'html',

        type: 'POST',

        async: true,

        success: function (data) {

            window.location.reload();

        }

    });



}



jQuery(document).ready(function () {


    jQuery("#img_loaging_agenda").hide();

    jQuery("#img_loaging_agenda_addbonplan").hide();

    //init add redacteur

    jQuery("#form_add_redacteur_div").hide();

    jQuery("#add_redacteur_form_loading").hide();

    jQuery("#add_redacteur_form_error").hide();

    //init save email validation festival


    jQuery("#form_add_validation_festival_div").hide();

    jQuery("#add_validation_festival_form_loading").hide();

    jQuery("#add_validation_festival_form_error").hide();

    jQuery("#add_validation_festival_form_success").hide();

    //init send validation article

    jQuery("#envoyer_validation_festival_form_loading").hide();

    jQuery("#envoyer_validation_festival_form_error").hide();

    jQuery("#envoyer_validation_festival_form_success").hide();

    //localisation init

    jQuery("#adresse_localisation_diffuseur_loading").hide();





    jQuery(".btnSinscrire").click(function () {


        var txtError = "";


        var nom_societe = jQuery("#nom_societe").val();

        if (nom_societe == "" || nom_societe == null) {

            txtError += "- Veuillez indiquer qui vous-êtes.<br/>";

            $("#nom_societe").css('border-color', '#FF0000');

        } else {

            $("#nom_societe").css('border-color', '#FFFFFF');

        }


        var nom_manifestation = jQuery("#nom_manifestation").val();

        if (nom_manifestation == "" || nom_manifestation == null) {

            txtError += "- Veuillez indiquer un titre à l'article.<br/>";

            $("#nom_manifestation").css('border-color', '#FF0000');

        } else {

            $("#nom_manifestation").css('border-color', '#FFFFFF');

        }


        var email = jQuery("#email").val();

        if (email != "") {

            if (!isEmail(email)) {

                txtError += "- Veuillez indiquer un adresse email valide.<br/>";

                $("#email").css('border-color', '#FF0000');

            } else {

                $("#email").css('border-color', '#FFFFFF');

            }

        }


        var date_depot = jQuery("#date_depot").val();

        if (date_depot == "" || date_depot == null) {

            txtError += "- Veuillez indiquer la date de dépôt.<br/>";

            $("#date_depot").css('border-color', '#FF0000');

        } else {

            $("#date_depot").css('border-color', '#FFFFFF');

        }


        var IdCategory = jQuery("#IdCategory").val();

        if (IdCategory == "" || IdCategory == null || IdCategory == "0") {

            txtError += "- Veuillez indiquer une categorie.<br/>";

            $("#IdCategory").css('border-color', '#FF0000');

        } else {

            $("#IdCategory").css('border-color', '#FFFFFF');

        }
    })



    /*var IdSubcategory = jQuery("#IdSubcategory").val();

     if(IdSubcategory=="" || IdSubcategory==null || IdSubcategory=="0") {

     txtError += "- Veuillez indiquer une sous-categorie.<br/>";

     $("#IdSubcategory").css('border-color', '#FF0000');

     } else {

     $("#IdSubcategory").css('border-color', '#FFFFFF');

     }*/



    /*var date_debut = jQuery("#date_debut").val();

     if(date_debut=="" || date_debut==null) {

     txtError += "- Veuillez indiquer la date de début.<br/>";

     $("#date_debut").css('border-color', '#FF0000');

     } else {

     $("#date_debut").css('border-color', '#FFFFFF');

     }



     var date_fin = jQuery("#date_fin").val();

     if(date_fin=="" || date_debut==null) {

     txtError += "- Veuillez indiquer la date de fin.<br/>";

     $("#date_fin").css('border-color', '#FF0000');

     } else {

     $("#date_fin").css('border-color', '#FFFFFF');

     }*/





    if (txtError == "") {

        jQuery("#frmInscriptionProfessionnel").submit();

    } else {

        jQuery("#div_error_agenda_submit").html(txtError);

    }

});





$("#adresse_localisation_diffuseur_checkbox").click(function () {

    jQuery("#adresse_localisation_diffuseur_loading").show();

    if ($('#adresse_localisation_diffuseur_checkbox').attr('checked')) {

        $("#adresse_localisation_diffuseur").val("1");



        jQuery.ajax({

            url: '<?php echo site_url("front/festival/get_commercant_localisation"); ?>',

            type: "post",

            data: {IdCommercant: "<?php echo $objCommercant->IdCommercant;?>"},

            dataType: 'json',

            success: function (data) {

                if (data != "error") {

                    jQuery("#nom_localisation").val(data.NomSociete);

                    jQuery("#adresse_localisation").val(data.adresse_localisation);

                    jQuery("#codepostal_localisation").val(data.codepostal_localisation);

                    jQuery("#IdVille_localisation").val(data.IdVille_localisation);

                    jQuery.post(

                        '<?php echo site_url("front/festival/get_ville_localisation"); ?>',

                        {IdVille: data.IdVille_localisation},

                        function (zReponse) {

                            jQuery("#IdVille_Nom_text_localisation").val(zReponse);

                            jQuery("#adresse_localisation_diffuseur_loading").hide();

                        });



                } else {

                    jQuery("#add_redacteur_form_error").show();

                    jQuery("#adresse_localisation_diffuseur_loading").hide();

                }

            },

            error: function (data) {

                jQuery("#adresse_localisation_diffuseur_error").show();

                jQuery("#adresse_localisation_diffuseur_error").val("Un erreur s'est produite !");

                jQuery("#adresse_localisation_diffuseur_loading").hide();

            }

        });



    } else {

        $("#adresse_localisation_diffuseur").val("0");

        $("#adresse_localisation").val("");

        $("#IdVille_localisation").val("0");

        $("#codepostal_localisation").val("");

        jQuery("#adresse_localisation_diffuseur_loading").hide();

    }





});



$("#diffusion_facebook_checkbox").click(function () {

    if ($('#diffusion_facebook_checkbox').attr('checked')) {

        $("#diffusion_facebook").val("1");

    } else {

        $("#diffusion_facebook").val("0");

    }

});



$("#diffusion_twitter_checkbox").click(function () {

    if ($('#diffusion_twitter_checkbox').attr('checked')) {

        $("#diffusion_twitter").val("1");

    } else {

        $("#diffusion_twitter").val("0");

    }

});



$("#diffusion_rss_checkbox").click(function () {

    if ($('#diffusion_rss_checkbox').attr('checked')) {

        $("#diffusion_rss").val("1");

    } else {

        $("#diffusion_rss").val("0");

    }

});





$("#diffusion_rss_checkbox").click(function () {

    if ($('#diffusion_rss_checkbox').attr('checked')) {

        $("#diffusion_rss").val("1");

    } else {

        $("#diffusion_rss").val("0");

    }

});



//ADD REDACTEUR START /////////////////////////////////////////////////////////////////////

$("#add_redacteur_form_link_open").click(function () {

    jQuery("#add_redacteur_form_link_add").css('display', 'none');

    jQuery("#add_redacteur_form_link_edit").css('display', 'none');

    jQuery("#add_redacteur_form_link_delete").css('display', 'none');



    jQuery('#add_redacteur_name').val("");

    jQuery('#add_redacteur_id').val("0");



    jQuery("#form_add_redacteur_div").show('blind');

});



$("#add_redacteur_form_cancel").click(function () {

    jQuery("#form_add_redacteur_div").hide('blind');



    jQuery("#add_redacteur_form_link_add").css('display', 'inline');

 if (isset($objFestival->redacteur_id) && $objFestival->redacteur_id != null && $objFestival->redacteur_id != "0"){

        jQuery("#add_redacteur_form_link_edit").css('display', 'inline');

        jQuery("#add_redacteur_form_link_delete").css('display', 'inline');

     } else {

        jQuery("#add_redacteur_form_link_edit").css('display', 'none');

        jQuery("#add_redacteur_form_link_delete").css('display', 'none');

     }

    jQuery("#add_redacteur_form_error").css('display', 'none');

    jQuery("#add_redacteur_form_locading").css('display', 'none');

});



$("#add_redacteur_form_save").click(function () {

    jQuery("#add_redacteur_form_loading").show();

    if (jQuery('#add_redacteur_name').val() == "" || jQuery('#add_redacteur_name').val() == null) {

        jQuery("#add_redacteur_form_error").show();

        jQuery("#add_redacteur_form_locading").css('display', 'none');

        jQuery("#add_redacteur_form_loading").hide();

    } else {

        var add_redacteur_name = jQuery('#add_redacteur_name').val();

        var add_redacteur_id = jQuery('#add_redacteur_id').val();

        jQuery.post(

            '<?php echo site_url("front/festival/add_redacteur_festival"); ?>',

            {

                add_redacteur_name: add_redacteur_name,

                add_redacteur_id: add_redacteur_id,

                IdCommercant: <?php echo $objCommercant->IdCommercant; ?>

    });

        function (zReponse) {  {

            if (zReponse != "error") {

                jQuery("#redacteur_festival option[value='" + zReponse + "']").remove();

                jQuery('#redacteur_article').append(jQuery('<option>', {

                    value: zReponse,

                    text: add_redacteur_name

                }));

                jQuery("#form_add_redacteur_div").hide('blind');

                jQuery("#add_redacteur_form_loading").hide();

                jQuery("#add_redacteur_form_error").hide();

                jQuery("#add_redacteur_form_locading").css('display', 'none');

                jQuery('#redacteur_festival').val(zReponse);



                jQuery("#add_redacteur_form_link_add").css('display', 'inline');

                jQuery("#add_redacteur_form_link_edit").css('display', 'inline');

                jQuery("#add_redacteur_form_link_delete").css('display', 'inline');

            } else {

                jQuery("#add_redacteur_form_error").show();

                jQuery("#add_redacteur_form_loading").hide();

                jQuery("#add_redacteur_form_locading").css('display', 'none');

            }

        };

    }

});

$("#redacteur_article").change(function () {

    jQuery("#add_redacteur_form_locading").css('display', 'inline');

    jQuery.ajax({

        url: '<?php echo site_url("front/festival/check_redacteur_festival"); ?>',

        type: "post",

        data: {redacteur_id: jQuery(this).val()},

        dataType: 'json',

        success: function (data) {

            if (data != "error") {

                jQuery("#add_redacteur_name").val(data.nom);

                jQuery("#add_redacteur_id").val(data.id);



                jQuery("#add_redacteur_form_link_add").css('display', 'inline');

                jQuery("#add_redacteur_form_link_edit").css('display', 'inline');

                jQuery("#add_redacteur_form_link_delete").css('display', 'inline');

                jQuery("#add_redacteur_form_error").hide();



                jQuery("#add_redacteur_form_locading").css('display', 'none');

            } else {

                jQuery("#add_redacteur_form_error").show();

                jQuery("#add_redacteur_form_link_edit").css('display', 'none');

                jQuery("#add_redacteur_form_link_delete").css('display', 'none');

                jQuery("#add_redacteur_form_locading").css('display', 'none');

            }

        },

        error: function (data) {

            jQuery("#add_redacteur_form_error").show();

            jQuery("#add_redacteur_form_link_edit").css('display', 'none');

            jQuery("#add_redacteur_form_link_delete").css('display', 'none');

            jQuery("#add_redacteur_form_locading").css('display', 'none');

        }

    });

});

$("#add_redacteur_form_link_edit").click(function () {

    jQuery("#add_redacteur_form_locading").css('display', 'inline');

    jQuery("#add_redacteur_id").val(jQuery("#redacteur_article").val());

    jQuery.ajax({

        url: '<?php echo site_url("front/articles/check_redacteur_festiival"); ?>',

        type: "post",

        data: {redacteur_id: jQuery("#redacteur_festival").val()},

        dataType: 'json',

        success: function (data) {

            if (data != "error") {

                jQuery("#add_redacteur_name").val(data.nom);

                jQuery("#add_redacteur_id").val(data.id);



                jQuery("#add_redacteur_form_link_add").css('display', 'inline');

                jQuery("#add_redacteur_form_link_edit").css('display', 'inline');

                jQuery("#add_redacteur_form_link_delete").css('display', 'inline');



                jQuery("#add_redacteur_form_locading").css('display', 'none');

                jQuery("#form_add_redacteur_div").show('blind');

            } else {

                jQuery("#add_redacteur_form_error").show();

                jQuery("#add_redacteur_form_locading").css('display', 'none');

            }

        },

        error: function (data) {

            jQuery("#add_redacteur_form_error").show();

            jQuery("#add_redacteur_form_locading").css('display', 'none');

        }

    });

});

$("#add_redacteur_form_link_delete").click(function () {

    if (confirm("Voulez-vous supprimer le redacteur séléctionné ?")) {

        jQuery("#add_redacteur_form_locading").css('display', 'inline');

        var redacteur_id = jQuery("#redacteur_festival").val();

        jQuery.post(

            '<?php echo site_url("front/festival/delete_redacteur_festival"); ?>',

            {

                redacteur_id: redacteur_id

            },

            function (zReponse) {

                //alert(zReponse);

                if (zReponse != "error") {

                    jQuery("#redacteur_festival option[value='" + redacteur_id + "']").remove();

                    jQuery('#redacteur_festival').val("0");



                    jQuery("#add_redacteur_name").val("");

                    jQuery("#add_redacteur_id").val("0");



                    jQuery("#add_redacteur_form_link_add").css('display', 'inline');

                    jQuery("#add_redacteur_form_link_edit").css('display', 'none');

                    jQuery("#add_redacteur_form_link_delete").css('display', 'none');



                    jQuery("#add_redacteur_form_locading").css('display', 'none');

                    jQuery("#form_add_redacteur_div").hide('blind');

                } else {

                    jQuery("#add_redacteur_form_error").show();

                    jQuery("#add_redacteur_form_locading").css('display', 'none');

                }

            });

    }

});

//ADD REDACTEUR END////////////////////////////////////////////////////////////////





//ADD ORGANISER START ***************************************************************

$("#add_organiser_form_link_add").click(function () {

    jQuery("#add_organiser_form_link_add").css('display', 'none');

    jQuery("#add_organiser_form_link_edit").css('display', 'none');

    jQuery("#add_organiser_form_link_save").css('display', 'inline');

    jQuery("#add_organiser_form_link_cancel").css('display', 'inline');

    jQuery("#add_organiser_form_link_delete").css('display', 'none');

    jQuery("#div_add_organiser_content").css('display', 'block');



    //jQuery('#Article_id_organisateur').val("0");

    jQuery('#organisateur').val("");

    jQuery('#adresse1').val("");

    jQuery('#adresse2').val("");

    jQuery('#codepostal').val("");

    //jQuery('#DepartementArticle').val("0");

    jQuery('#IdVille').val("0");

    jQuery('#telephone').val("");

    jQuery('#mobile').val("");

    jQuery('#fax').val("");

    jQuery('#email').val("");

    jQuery('#siteweb').val("");

    jQuery('#facebook').val("");

    jQuery('#twitter').val("");

    jQuery('#googleplus').val("");

});



$("#add_organiser_form_link_cancel").click(function () {

    jQuery("#add_organiser_form_link_add").css('display', 'inline');

<?php if (isset($objFestival->organiser_id) && $objFestival->organiser_id != null && $objFestival->organiser_id != "0") { ?>

        jQuery("#add_organiser_form_link_edit").css('display', 'inline');

    <?php } else { ?>

        jQuery("#add_organiser_form_link_edit").css('display', 'none');

    <?php } ?>

    jQuery("#add_organiser_form_link_save").css('display', 'none');

    jQuery("#add_organiser_form_link_cancel").css('display', 'none');

<?php if (isset($objFestival->organiser_id) && $objFestival->organiser_id != null && $objFestival->organiser_id != "0") { ?>

        jQuery("#add_organiser_form_link_delete").css('display', 'inline');

    <?php } else { ?>

        jQuery("#add_organiser_form_link_delete").css('display', 'none');

    <?php } ?>

    jQuery("#div_add_organiser_content").css('display', 'none');

    jQuery("#add_organiser_form_error").css('display', 'none');

    jQuery("#add_organiser_form_locading").css('display', 'none');

});



$("#add_organiser_form_link_save").click(function () {

    jQuery("#add_organiser_form_locading").css('display', 'inline');

    if (jQuery('#organisateur').val() == "" || jQuery('#organisateur').val() == null) {

        jQuery("#add_organiser_form_error").show();

        jQuery("#add_organiser_form_locading").css('display', 'none');

    } else {

        jQuery.post(

            '<?php echo site_url("front/festival/add_organiser_festival"); ?>',

            {

                IdCommercant: <?php echo $objCommercant->IdCommercant; ?>,

        // Article_id_organisateur: jQuery('#Article_id_organisateur').val(),

        organisateur: jQuery('#organisateur').val(),

            adresse1: jQuery('#adresse1').val(),

            adresse2: jQuery('#adresse2').val(),

            codepostal: jQuery('#codepostal').val(),

            DepartementArticle: jQuery('#DepartementArticle').val(),

            IdVille: jQuery('#IdVille').val(),

            telephone: jQuery('#telephone').val(),

            mobile: jQuery('#mobile').val(),

            fax: jQuery('#fax').val(),

            email: jQuery('#email').val(),

            siteweb: jQuery('#siteweb').val(),

            facebook: jQuery('#facebook').val(),

            twitter: jQuery('#twitter').val(),

            googleplus: jQuery('#googleplus').val()

    }

        function (zReponse) {

            //alert(zReponse);

            if (zReponse != "error") {

                jQuery("#organiser_festival option[value='" + zReponse + "']").remove();

                jQuery('#organiser_ffestival').append(jQuery('<option>', {

                    value: zReponse,

                    text: jQuery('#organisateur').val()

                }));

                jQuery("#add_organiser_form_locading").css('display', 'none');

                jQuery("#add_organiser_form_error").css('display', 'none');

                jQuery('#organiser_festival').val(zReponse);

                jQuery("#add_organiser_form_link_add").css('display', 'inline');

                jQuery("#add_organiser_form_link_edit").css('display', 'inline');

                jQuery("#add_organiser_form_link_save").css('display', 'none');

                jQuery("#add_organiser_form_link_cancel").css('display', 'none');

                jQuery("#add_organiser_form_link_delete").css('display', 'inline');

                jQuery("#div_add_organiser_content").css('display', 'none');

            } else {

                jQuery("#add_organiser_form_error").show();

            }

        });

    }



});



$("#organiser_festival").change(function () {

    jQuery("#add_organiser_form_locading").css('display', 'inline');

    jQuery.ajax({

        url: '<?php echo site_url("front/festival/check_organiser_festival"); ?>',

        type: "post",

        data: {organiser_id: jQuery(this).val()},

        dataType: 'json',

        success: function (data) {

            if (data != "error") {

                jQuery("#organisateur").val(data.name);

                jQuery("#adresse1").val(data.address1);

                jQuery("#adresse2").val(data.address2);

                jQuery("#codepostal").val(data.postal_code);

                jQuery("#adresse_localisation").val(data.department_id);

                jQuery("#codepostal_localisation").val(data.ville_id);

                jQuery("#telephone").val(data.tel);

                jQuery("#mobile").val(data.mobile);

                jQuery("#fax").val(data.fax);

                jQuery("#email").val(data.email);

                jQuery("#siteweb").val(data.website);

                jQuery("#facebook").val(data.facebook);

                jQuery("#twitter").val(data.twitter);

                jQuery("#googleplus").val(data.googleplus);

                jQuery("#add_organiser_form_locading").css('display', 'none');

                jQuery("#add_organiser_form_error").css('display', 'none');

                jQuery("#codepostal").focus();

                jQuery("#codepostal").blur();



                jQuery("#add_organiser_form_link_add").css('display', 'inline');

                jQuery("#add_organiser_form_link_edit").css('display', 'inline');

                jQuery("#add_organiser_form_link_save").css('display', 'none');

                jQuery("#add_organiser_form_link_cancel").css('display', 'none');

                jQuery("#add_organiser_form_link_delete").css('display', 'inline');

            } else {

                jQuery("#add_organiser_form_error").show();

                jQuery("#add_organiser_form_locading").css('display', 'none');

            }

        },

        error: function (data) {

            jQuery("#add_organiser_form_error").show();

            jQuery("#add_organiser_form_locading").css('display', 'none');

        }

    });

});

$("#add_organiser_form_link_edit").click(function () {

    jQuery("#add_organiser_form_locading").css('display', 'inline');

    // jQuery("#Article_id_organisateur").val(jQuery("#organiser_article").val());

    jQuery.ajax({

        url: '<?php echo site_url("front/festival/check_organiser_festival"); ?>',

        type: "post",

        data: {organiser_id: jQuery("#organiser_festival").val()},

        dataType: 'json',

        success: function (data) {

            if (data != "error") {

                jQuery("#organisateur").val(data.name);

                jQuery("#adresse1").val(data.address1);

                jQuery("#adresse2").val(data.address2);

                jQuery("#codepostal").val(data.postal_code);

                jQuery("#adresse_localisation").val(data.department_id);

                jQuery("#codepostal_localisation").val(data.ville_id);

                jQuery("#telephone").val(data.tel);

                jQuery("#mobile").val(data.mobile);

                jQuery("#fax").val(data.fax);

                jQuery("#email").val(data.email);

                jQuery("#siteweb").val(data.website);

                jQuery("#facebook").val(data.facebook);

                jQuery("#twitter").val(data.twitter);

                jQuery("#googleplus").val(data.googleplus);

                jQuery("#add_organiser_form_locading").css('display', 'none');

                jQuery("#add_organiser_form_error").css('display', 'none');

                jQuery("#div_add_organiser_content").css('display', 'block');

                jQuery("#codepostal").focus();

                jQuery("#codepostal").blur();

                jQuery("#add_organiser_form_link_add").css('display', 'none');

                jQuery("#add_organiser_form_link_edit").css('display', 'none');

                jQuery("#add_organiser_form_link_save").css('display', 'inline');

                jQuery("#add_organiser_form_link_cancel").css('display', 'inline');

                jQuery("#add_organiser_form_link_delete").css('display', 'none');

            } else {

                jQuery("#add_organiser_form_error").show();

                jQuery("#add_organiser_form_locading").css('display', 'none');

            }

        },

        error: function (data) {

            jQuery("#add_organiser_form_error").show();

            jQuery("#add_organiser_form_locading").css('display', 'none');

        }

    });

});

$("#add_organiser_form_link_delete").click(function () {

    if (confirm("Voulez-vous supprimer l'organisateur séléctionné ?")) {

        jQuery("#add_organiser_form_locading").css('display', 'inline');

        var organiser_id = jQuery("#organiser_festival").val();

        jQuery.post(

            '<?php echo site_url("front/festival/delete_organiser_festival"); ?>',

            {

                organiser_id: organiser_id

            },

            function (zReponse) {

                //alert(zReponse);

                if (zReponse != "error") {

                    jQuery("#organiser_festival option[value='" + organiser_id + "']").remove();

                    jQuery('#organiser_festival').val("0");

                    jQuery("#add_organiser_form_locading").css('display', 'none');

                    jQuery("#add_organiser_form_error").css('display', 'none');

                    jQuery("#add_organiser_form_link_add").css('display', 'inline');

                    jQuery("#add_organiser_form_link_edit").css('display', 'none');

                    jQuery("#add_organiser_form_link_save").css('display', 'none');

                    jQuery("#add_organiser_form_link_cancel").css('display', 'none');

                    jQuery("#add_organiser_form_link_delete").css('display', 'none');

                    jQuery("#div_add_organiser_content").css('display', 'none');



                    // jQuery('#festival_id_organisateur').val("0");

                    jQuery('#organisateur').val("");

                    jQuery('#adresse1').val("");

                    jQuery('#adresse2').val("");

                    jQuery('#codepostal').val("");

                    jQuery('#DepartementArticle').val("0");

                    jQuery('#IdVille').val("0");

                    jQuery('#telephone').val("");

                    jQuery('#mobile').val("");

                    jQuery('#fax').val("");

                    jQuery('#email').val("");

                    jQuery('#siteweb').val("");

                    jQuery('#facebook').val("");

                    jQuery('#twitter').val("");

                    jQuery('#googleplus').val("");

                } else {

                    jQuery("#add_organiser_form_error").show();

                    jQuery("#add_organiser_form_locading").css('display', 'none');

                }

            });

    }

});

//ADD ORGANISER END ***************************************************************





//	ADD EMAIL VALIDATION FESTIVAL START

$("#add_validation_festival_form_link_open").click(function () {

    jQuery("#add_validation_festival_form_link_add").css('display', 'none');

    jQuery("#add_validation_festival_form_link_edit").css('display', 'none');

    jQuery("#add_validation_festival_form_link_delete").css('display', 'none');



    jQuery('#add_validation_festival_name').val("");

    jQuery('#add_validation_festival_id').val("0");

    jQuery("#form_add_validation_festival_div").show('blind');

});



$("#add_validation_festival_form_cancel").click(function () {

    jQuery("#form_add_validation_festival_div").hide('blind');

});



$("#add_validation_festival_form_save").click(function () {



    jQuery("#add_validation_festival_form_loading").show();



    var add_validation_festival_name = jQuery("#add_validation_festival_name").val();

    var add_validation_festival_email = jQuery("#add_validation_festival_email").val();

    if (add_validation_festival_name == "" || add_validation_festival_name == null || !isEmail(add_validation_festival_email) || add_validation_festival_email == "" || add_validation_festival_email == null) {

        jQuery("#add_validation_festival_name").css('border-color', '#FF0000');

        jQuery("#add_validation_festival_email").css('border-color', '#FF0000');

        jQuery("#add_validation_festival_form_loading").hide();

    } else {



        var add_validation_festival_name = jQuery('#add_validation_festival_name').val();

        var add_validation_festival_email = jQuery('#add_validation_festival_email').val();


        jQuery.post(

            '<?php echo site_url("front/festival/add_validation_festival_function"); ?>',

            {

                add_validation_festival_name: add_validation_festival_name,

                add_validation_festival_email: add_validation_festival_email

            },

            function (zReponse) {

                if (zReponse != "error") {

                    jQuery('#destinataire_validation_festiav').append(jQuery('<option>', {

                        value: zReponse,

                        text: add_validation_festival_name + " / " + add_validation_festival_email

                    }));

                    jQuery("#add_validation_festival_form_loading").hide();

                    jQuery("#add_validation_festival_form_error").hide();

                    jQuery("#form_add_validation_festival_div").hide('blind');

                } else {

                    jQuery("#add_validation_festival_form_error").show();

                    jQuery("#add_validation_festival_form_loading").hide();

                }

            });



    }



});

//	ADD EMAIL VALIDATION ARTICLE END





//	SEND VALIDATION ARTICLE START

$("#link_envoyer_validation_festival").click(function () {



    jQuery("#envoyer_validation_festival_form_loading").show();



    var destinataire_validation_festival = jQuery("#destinataire_validation_festival").val();

    if (destinataire_validation_festival == "" || destinataire_validation_festival == '0' || destinataire_validation_festival == null) {

        jQuery("#destinataire_validation_festival").css('border-color', '#FF0000');

        jQuery("#envoyer_validation_festival_form_loading").hide();

    } else {



        var validation_nom_manifestation = jQuery('#nom_manifestation').val();

        //var validation_description = CKEDITOR.instances['description'].getData();

        var festival_id = "<?php if(isset($objFestival->id)) echo $objFestival->id;?>";

        var festival_date_depot = "<?php if(isset($objFestival->date_depot)) echo convert_Sqldate_to_Frenchdate($objFestival->date_depot);?>";

        var destinataire_validation_article = jQuery("#destinataire_validation_article").val();

        //alert(validation_description);



        jQuery.post(

            '<?php echo site_url("front/festival/envoyer_validation_festival_function"); ?>',

            {

                validation_nom_manifestation: validation_nom_manifestation,

                //validation_description: validation_description,

                festival_id: festival_id,

                festival_date_depot: festival_date_depot,

                destinataire_validation_festival: destinataire_validation_festival

            },*/

        function (zReponse) {

            //alert(zReponse);

            if (zReponse != "error") {

                jQuery("#envoyer_validation_festival_form_success").show();

                jQuery("#envoyer_validation_festival_form_error").hide();

                jQuery("#envoyer_validation_festival_form_loading").hide();

            } else {

                jQuery("#envoyer_validation_festival_form_error").show();

                jQuery("#envoyer_validation_festival_form_success").hide();

                jQuery("#envoyer_validation_festival_form_loading").hide();

            }

        });



    }



});

//	SEND VALIDATION FESTIVAL END





$("#admin_festival_activation_1").click(function () {

    if ($('#admin_festival_activation_1').attr('checked')) {

        $("#IsActif").val("1");

    }

});

$("#admin_festival_activation_0").click(function () {

    if ($('#admin_festival_activation_0').attr('checked')) {

        $("#IsActif").val("0");

    }

});

$("#admin_festival_activation_2").click(function () {

    if ($('#admin_festival_activation_2').attr('checked')) {

        $("#IsActif").val("2");

    }

});

$("#admin_festival_activation_3").click(function () {

    if ($('#admin_festival_activation_3').attr('checked')) {

        $("#IsActif").val("3");

    }

});





jQuery('#location_article').change(function () {

    jQuery("#select_location_form_loading").css("display", "block");

    var location_id = jQuery(this).val();

    if (location_id != "0") {

        jQuery.ajax({

            url: '<?php echo site_url("front/festival/get_location_festival"); ?>',

            type: "post",

            data: {location_id: location_id},

            dataType: 'json',

            success: function (data) {

                if (data != "error") {

                    jQuery("#adresse_localisation").val(data.location_address);

                    jQuery("#codepostal_localisation").val(data.location_postcode);

                    jQuery("#DepartementAgendaLocalisation").val(data.location_departementid);

                    jQuery("#IdVille_localisation").val(data.location_villeid);

                    jQuery('#codepostal_localisation').focus();

                    jQuery('#codepostal_localisation').blur();



                    jQuery("#add_location_id").val(data.location_id);

                    jQuery("#add_location_name").val(data.location);

                    jQuery("#add_location_adress").val(data.location_address);

                    jQuery("#add_location_codepostal").val(data.location_postcode);

                    jQuery("#add_location_departementid").val(data.location_departementid);

                    jQuery("#add_location_villeId").val(data.location_villeid);

                    jQuery('#add_location_codepostal').focus();

                    jQuery('#add_location_codepostal').blur();



                    jQuery.post(

                        '<?php echo site_url("front/articles/get_ville_localisation"); ?>',

                        {IdVille: data.location_villeid},

                        function (zReponse) {

                            jQuery("#IdVille_Nom_text_localisation").val(zReponse);

                            jQuery("#select_location_form_loading").css("display", "none");

                        });



                } else {

                    jQuery("#select_location_form_loading").css("display", "none");

                }

            },

            error: function (data) {

                jQuery("#select_location_form_loading").css("display", "none");

            }

        });

        jQuery("#add_location_form_link_open").css("display", "none");

        jQuery("#edit_location_form_link_open").css("display", "inline-block");

        jQuery("#delete_location_form_link_open").css("display", "inline-block");

    } else {

        jQuery("#adresse_localisation").val("");

        jQuery("#codepostal_localisation").val("");

        jQuery("#DepartementAgendaLocalisation").val("0");

        jQuery("#IdVille_localisation").val("0");

        jQuery("#IdVille_Nom_text_localisation").val("");

        jQuery("#select_location_form_loading").css("display", "none");



        jQuery("#add_location_id").val("");

        jQuery("#add_location_name").val("");

        jQuery("#add_location_adress").val("");

        jQuery("#add_location_codepostal").val("");

        jQuery("#add_location_departementid").val("0");

        jQuery("#add_location_villeId").val("0");



        jQuery("#add_location_form_link_open").css("display", "inline-block");

        jQuery("#edit_location_form_link_open").css("display", "none");

        jQuery("#delete_location_form_link_open").css("display", "none");

    }

});





//radio buttton diffuseur_organisateur START

$("#diffuseur_organisateur_1").click(function () {

    if ($('#diffuseur_organisateur_1').attr('checked')) {

        $("#diffuseur_organisateur").val("1");

        $("#img_loaging_agenda").show();

        $.post(

            '<?php echo site_url(); ?>festival/check_commercant/',

            {

                IdCommercant: <?php echo $objCommercant->IdCommercant; ?>

    },

        function (zReponse) {

            //alert ("reponse " + zReponse) ;

            if (zReponse) {

                var festival_value = zReponse.split("==!!!==");

                for (i = 0; i < festival_value.length; i++) {

                    var festival_value_id = festival_value[i].split("!!!==>");

                    if (festival_value_id[0] == "NomSociete") $("#organisateur").val(festival_value_id[1]);

                    if (festival_value_id[0] == "Adresse1") $("#adresse1").val(festival_value_id[1]);

                    if (article_value_id[0] == "Adresse2") $("#adresse2").val(festival_value_id[1]);

                    if (festival_value_id[0] == "IdVille") $("#IdVille").val(festival_value_id[1]);

                    if (festival_value_id[0] == "CodePostal") $("#codepostal").val(festival_value_id[1]);

                    if (festival_value_id[0] == "TelFixe") $("#telephone").val(festival_value_id[1]);

                    if (festival_value_id[0] == "TelMobile") $("#mobile").val(festival_value_id[1]);

                    if (festival_value_id[0] == "fax") $("#fax").val(festival_value_id[1]);

                    if (festival_value_id[0] == "Email") $("#email").val(festival_value_id[1]);

                    if (festival_value_id[0] == "SiteWeb") $("#siteweb").val(festival_value_id[1]);

                    if (festival_value_id[0] == "Facebook") $("#facebook").val(festival_value_id[1]);

                    if (festival_value_id[0] == "Twitter") $("#googleplus").val(fetival_value_id[1]);

                    if (festival_value_id[0] == "google_plus") $("#twitter").val(festival_value_id[1]);

                }

                $("#img_loaging_agenda").hide();

            }

        });

    } else {

        $("#diffuseur_organisateur").val("0");

    }

});

$("#diffuseur_organisateur_0").click(function () {

    if ($('#diffuseur_organisateur_0').attr('checked')) {

        $("#diffuseur_organisateur").val("0");

        $("#organisateur").val("");

        $("#adresse1").val("");

        $("#adresse2").val("");

        $("#IdVille").val("0");

        $("#codepostal").val("");

        $("#telephone").val("");

        $("#mobile").val("");

        $("#fax").val("");

        $("#email").val("");

        $("#siteweb").val("http://www.");

        $("#facebook").val("");

        $("#googleplus").val("");

        $("#twitter").val("");



    } else {

        $("#diffuseur_organisateur").val("1");

    }

});

//radio buttton diffuseur_organisateur END





//add_location_codepostal

$("#add_location_codepostal").keyup(function () {

    var result = $(this).val();

    $("#codepostal_localisation").val(result);

}).keydown(function () {

    var result = $(this).val();

    $("#codepostal_localisation").val(result);

});





});



$(function () {

    $(".date_debut").datepicker({

        dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],

        dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],

        monthNames: ['Janvier', 'Févier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],

        dateFormat: 'DD, d MM yy',

        autoSize: true,

        changeMonth: true,

        changeYear: true,

        yearRange: '1900:2020'

    });

    $(".date_fin").datepicker({

        dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],

        dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],

        monthNames: ['Janvier', 'Févier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],

        dateFormat: 'DD, d MM yy',

        autoSize: true,

        changeMonth: true,

        changeYear: true,

        yearRange: '1900:2020'

    });

    $("#date_depot").datepicker({

        dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],

        dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],

        monthNames: ['Janvier', 'Févier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],

        dateFormat: 'DD, d MM yy',

        autoSize: true,

        changeMonth: true,

        changeYear: true,

        yearRange: '1900:2020'

    });

});



function alert_save_agenda_before_view() {

    $("#id_save_agenda_before_view").html("Veuillez enregistrer l'article avant de visualiser !&nbsp;&nbsp;&nbsp;");

}





function CP_getDepartement() {

    jQuery('#departementCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

    var CodePostalSociete = jQuery('#codepostal').val();

    jQuery.post(

        '<?php echo site_url("front/festival/departementcp_festival"); ?>',

        {CodePostalSociete: CodePostalSociete},

        function (zReponse) {

            jQuery('#departementCP_container').html(zReponse);

        });

}





function CP_getDepartement_localisation() {

    jQuery('#departementCP_container_localisation').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

    var CodePostalSociete = jQuery('#codepostal_localisation').val();

    jQuery.post(

        '<?php echo site_url("front/festival/departementcp_festival_localisation"); ?>',

        {CodePostalSociete: CodePostalSociete},

        function (zReponse) {

            jQuery('#departementCP_container_localisation').html(zReponse);

        });

}



function CP_getDepartement_add_location() {

    jQuery('#departementCP_container_add_location').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

    var CodePostalSociete = jQuery('#add_location_codepostal').val();

    jQuery.post(

        '<?php echo site_url("front/festival/departementcp_festival_add_location"); ?>',

        {CodePostalSociete: CodePostalSociete},

        function (zReponse) {

            jQuery('#departementCP_container_add_location').html(zReponse);

        });

}



function CP_getVille() {

    jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

    var CodePostalSociete = jQuery('#codepostal').val();

    jQuery.post(

        '<?php echo site_url("front/festival/villecp_festival"); ?>',

        {CodePostalSociete: CodePostalSociete},

        function (zReponse) {

            jQuery('#villeCP_container').html(zReponse);

        });

}



function CP_getVille_localisation() {

    jQuery('#villeCP_container_localisation').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

    var CodePostalSociete = jQuery('#codepostal_localisation').val();

    jQuery.post(

        '<?php echo site_url("front/festival/villecp_festival_localisation"); ?>',

        {CodePostalSociete: CodePostalSociete},

        function (zReponse) {

            jQuery('#villeCP_container_localisation').html(zReponse);

        });

}



function CP_getVille_add_location() {

    jQuery('#villeCP_container_add_location').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

    var CodePostalSociete = jQuery('#add_location_codepostal').val();

    jQuery.post(

        '<?php echo site_url("front/festival/villecp_festival_add_location"); ?>',

        {CodePostalSociete: CodePostalSociete},

        function (zReponse) {

            jQuery('#villeCP_container_add_location').html(zReponse);

        });

}





function CP_getVille_D_CP() {

    jQuery('#villeCP_container').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

    var CodePostalSociete = jQuery('#codepostal').val();

    var departement_id = jQuery('#DepartementAgenda').val();

    jQuery.post(

        '<?php echo site_url("front/festival/villecp_festival"); ?>',

        {CodePostalSociete: CodePostalSociete, departement_id: departement_id},

        function (zReponse) {

            jQuery('#villeCP_container').html(zReponse);

        });

}



function CP_getVille_D_CP_localisation() {

    jQuery('#villeCP_container_localisation').html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

    var CodePostalSociete = jQuery('#codepostal_localisation').val();

    var departement_id = jQuery('#DepartementAgendaLocalisation').val();

    jQuery.post(

        '<?php echo site_url("front/festival/villecp_festival_localisation"); ?>',

        {CodePostalSociete: CodePostalSociete, departement_id: departement_id},

        function (zReponse) {

            jQuery('#villeCP_container_localisation').html(zReponse);

        });

}





</script>


